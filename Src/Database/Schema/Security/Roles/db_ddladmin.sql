EXEC sp_addrolemember N'db_ddladmin', N'AdeWhitaker'
GO
EXEC sp_addrolemember N'db_ddladmin', N'AlexandraMaguire'
GO
EXEC sp_addrolemember N'db_ddladmin', N'AndrewGrant'
GO
EXEC sp_addrolemember N'db_ddladmin', N'AshleyDonson'
GO
EXEC sp_addrolemember N'db_ddladmin', N'BuildManager'
GO
EXEC sp_addrolemember N'db_ddladmin', N'CathalSherry'
GO
EXEC sp_addrolemember N'db_ddladmin', N'ChristianWilliams'
GO
EXEC sp_addrolemember N'db_ddladmin', N'ClaireJarmy'
GO
EXEC sp_addrolemember N'db_ddladmin', N'DanielCorker'
GO
EXEC sp_addrolemember N'db_ddladmin', N'DanielOates'
GO
EXEC sp_addrolemember N'db_ddladmin', N'GavinReynolds'
GO
EXEC sp_addrolemember N'db_ddladmin', N'IanSlack'
GO
EXEC sp_addrolemember N'db_ddladmin', N'JamesLewis'
GO
EXEC sp_addrolemember N'db_ddladmin', N'JosephLamb'
GO
EXEC sp_addrolemember N'db_ddladmin', N'LouisBromilow'
GO
EXEC sp_addrolemember N'db_ddladmin', N'MarkBeaumont'
GO
EXEC sp_addrolemember N'db_ddladmin', N'MatthewHoyle'
GO
EXEC sp_addrolemember N'db_ddladmin', N'NeilAfflick'
GO
EXEC sp_addrolemember N'db_ddladmin', N'NessaGreen'
GO
EXEC sp_addrolemember N'db_ddladmin', N'NicolaDolan'
GO
EXEC sp_addrolemember N'db_ddladmin', N'OlaOladeni'
GO
EXEC sp_addrolemember N'db_ddladmin', N'OwenAshcroft'
GO
EXEC sp_addrolemember N'db_ddladmin', N'PaulLivingstone'
GO
EXEC sp_addrolemember N'db_ddladmin', N'StephenAinsworth'
GO
EXEC sp_addrolemember N'db_ddladmin', N'StephenWiggins'
GO
EXEC sp_addrolemember N'db_ddladmin', N'SusanSwanton'
GO
EXEC sp_addrolemember N'db_ddladmin', N'UmerHamid'
GO
