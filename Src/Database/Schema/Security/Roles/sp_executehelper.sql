CREATE ROLE [sp_executehelper]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'sp_executehelper', N'NeilAfflick'
GO
EXEC sp_addrolemember N'sp_executehelper', N'NicolaDolan'
GO
EXEC sp_addrolemember N'sp_executehelper', N'PaulLivingstone'
GO
EXEC sp_addrolemember N'sp_executehelper', N'ReadOnly'
GO
EXEC sp_addrolemember N'sp_executehelper', N'StephenWiggins'
GO
