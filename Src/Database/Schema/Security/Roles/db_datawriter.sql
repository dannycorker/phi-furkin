EXEC sp_addrolemember N'db_datawriter', N'AdeWhitaker'
GO
EXEC sp_addrolemember N'db_datawriter', N'AlexandraMaguire'
GO
EXEC sp_addrolemember N'db_datawriter', N'AndrewGrant'
GO
EXEC sp_addrolemember N'db_datawriter', N'AshleyDonson'
GO
EXEC sp_addrolemember N'db_datawriter', N'CathalSherry'
GO
EXEC sp_addrolemember N'db_datawriter', N'ChristianWilliams'
GO
EXEC sp_addrolemember N'db_datawriter', N'ClaireJarmy'
GO
EXEC sp_addrolemember N'db_datawriter', N'DanielCorker'
GO
EXEC sp_addrolemember N'db_datawriter', N'DanielOates'
GO
EXEC sp_addrolemember N'db_datawriter', N'GavinReynolds'
GO
EXEC sp_addrolemember N'db_datawriter', N'IanSlack'
GO
EXEC sp_addrolemember N'db_datawriter', N'JamesLewis'
GO
EXEC sp_addrolemember N'db_datawriter', N'JosephLamb'
GO
EXEC sp_addrolemember N'db_datawriter', N'LouisBromilow'
GO
EXEC sp_addrolemember N'db_datawriter', N'MarkBeaumont'
GO
EXEC sp_addrolemember N'db_datawriter', N'MatthewHoyle'
GO
EXEC sp_addrolemember N'db_datawriter', N'NeilAfflick'
GO
EXEC sp_addrolemember N'db_datawriter', N'NessaGreen'
GO
EXEC sp_addrolemember N'db_datawriter', N'OlaOladeni'
GO
EXEC sp_addrolemember N'db_datawriter', N'OwenAshcroft'
GO
EXEC sp_addrolemember N'db_datawriter', N'PaulLivingstone'
GO
EXEC sp_addrolemember N'db_datawriter', N'StephenAinsworth'
GO
EXEC sp_addrolemember N'db_datawriter', N'StephenWiggins'
GO
EXEC sp_addrolemember N'db_datawriter', N'UmerHamid'
GO
