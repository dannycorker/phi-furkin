CREATE ROLE [sp_executeall]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'sp_executeall', N'AdeWhitaker'
GO
EXEC sp_addrolemember N'sp_executeall', N'AlexandraMaguire'
GO
EXEC sp_addrolemember N'sp_executeall', N'AndrewGrant'
GO
EXEC sp_addrolemember N'sp_executeall', N'AndrewHerndlhofer'
GO
EXEC sp_addrolemember N'sp_executeall', N'AquariumNet'
GO
EXEC sp_addrolemember N'sp_executeall', N'AshleyDonson'
GO
EXEC sp_addrolemember N'sp_executeall', N'BuildManager'
GO
EXEC sp_addrolemember N'sp_executeall', N'CathalSherry'
GO
EXEC sp_addrolemember N'sp_executeall', N'ChristianWilliams'
GO
EXEC sp_addrolemember N'sp_executeall', N'ClaireJarmy'
GO
EXEC sp_addrolemember N'sp_executeall', N'ConorReeves'
GO
EXEC sp_addrolemember N'sp_executeall', N'DanielCorker'
GO
EXEC sp_addrolemember N'sp_executeall', N'DanielOates'
GO
EXEC sp_addrolemember N'sp_executeall', N'GavinReynolds'
GO
EXEC sp_addrolemember N'sp_executeall', N'IanSlack'
GO
EXEC sp_addrolemember N'sp_executeall', N'JamesLewis'
GO
EXEC sp_addrolemember N'sp_executeall', N'JosephLamb'
GO
EXEC sp_addrolemember N'sp_executeall', N'LouisBromilow'
GO
EXEC sp_addrolemember N'sp_executeall', N'MarkBeaumont'
GO
EXEC sp_addrolemember N'sp_executeall', N'MatthewHoyle'
GO
EXEC sp_addrolemember N'sp_executeall', N'NeilAfflick'
GO
EXEC sp_addrolemember N'sp_executeall', N'NessaGreen'
GO
EXEC sp_addrolemember N'sp_executeall', N'NicolaDolan'
GO
EXEC sp_addrolemember N'sp_executeall', N'OlaOladeni'
GO
EXEC sp_addrolemember N'sp_executeall', N'OwenAshcroft'
GO
EXEC sp_addrolemember N'sp_executeall', N'PaulLivingstone'
GO
EXEC sp_addrolemember N'sp_executeall', N'StephenAinsworth'
GO
EXEC sp_addrolemember N'sp_executeall', N'StephenWiggins'
GO
EXEC sp_addrolemember N'sp_executeall', N'SusanSwanton'
GO
EXEC sp_addrolemember N'sp_executeall', N'ThomasDoyle'
GO
EXEC sp_addrolemember N'sp_executeall', N'UmerHamid'
GO
GRANT EXECUTE TO [sp_executeall]
