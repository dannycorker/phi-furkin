EXEC sp_addrolemember N'db_datareader', N'AdeWhitaker'
GO
EXEC sp_addrolemember N'db_datareader', N'AlexandraMaguire'
GO
EXEC sp_addrolemember N'db_datareader', N'AndrewGrant'
GO
EXEC sp_addrolemember N'db_datareader', N'AndrewHerndlhofer'
GO
EXEC sp_addrolemember N'db_datareader', N'AquariumNet'
GO
EXEC sp_addrolemember N'db_datareader', N'AshleyDonson'
GO
EXEC sp_addrolemember N'db_datareader', N'BuildManager'
GO
EXEC sp_addrolemember N'db_datareader', N'CathalSherry'
GO
EXEC sp_addrolemember N'db_datareader', N'ChristianWilliams'
GO
EXEC sp_addrolemember N'db_datareader', N'ClaireJarmy'
GO
EXEC sp_addrolemember N'db_datareader', N'DanielCorker'
GO
EXEC sp_addrolemember N'db_datareader', N'DanielOates'
GO
EXEC sp_addrolemember N'db_datareader', N'GavinReynolds'
GO
EXEC sp_addrolemember N'db_datareader', N'IanSlack'
GO
EXEC sp_addrolemember N'db_datareader', N'JamesLewis'
GO
EXEC sp_addrolemember N'db_datareader', N'JosephLamb'
GO
EXEC sp_addrolemember N'db_datareader', N'LouisBromilow'
GO
EXEC sp_addrolemember N'db_datareader', N'MarkBeaumont'
GO
EXEC sp_addrolemember N'db_datareader', N'MatthewHoyle'
GO
EXEC sp_addrolemember N'db_datareader', N'NeilAfflick'
GO
EXEC sp_addrolemember N'db_datareader', N'NessaGreen'
GO
EXEC sp_addrolemember N'db_datareader', N'NicolaDolan'
GO
EXEC sp_addrolemember N'db_datareader', N'OlaOladeni'
GO
EXEC sp_addrolemember N'db_datareader', N'OwenAshcroft'
GO
EXEC sp_addrolemember N'db_datareader', N'PaulLivingstone'
GO
EXEC sp_addrolemember N'db_datareader', N'ReadOnly'
GO
EXEC sp_addrolemember N'db_datareader', N'StephenAinsworth'
GO
EXEC sp_addrolemember N'db_datareader', N'StephenWiggins'
GO
EXEC sp_addrolemember N'db_datareader', N'SusanSwanton'
GO
EXEC sp_addrolemember N'db_datareader', N'ThomasDoyle'
GO
EXEC sp_addrolemember N'db_datareader', N'UmerHamid'
GO
