SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vLeadEventFast
-- =============================================
CREATE VIEW [dbo].[vCaseWithLatestNoteEvent] 
AS

SELECT 
ca.CaseID,
ca.LeadID,
ca.ClientID,
ca.CaseNum,
ca.CaseRef,
ca.ClientStatusID,
ca.AquariumStatusID,
ca.DefaultContactID,
ca.LatestLeadEventID,
ca.LatestInProcessLeadEventID,
ca.LatestOutOfProcessLeadEventID,
ca.LatestNonNoteLeadEventID,
ca.LatestNoteLeadEventID,
le.WhenCreated, 
le.WhoCreated, 
le.FollowupDateTime,
le.WhenFollowedUp, 
le.Cost, 
le.Comments, 
le.EventTypeID, 
le.NoteTypeID, 
convert(char(10), le.WhenCreated, 126) as WhenCreatedDateOnly, 
datediff(dd, le.WhenCreated, dbo.fn_GetDate_Local()) as AgeInDays, 
cp.UserName, 
nt.NoteTypeName, 
ls.StatusName  
FROM dbo.Cases ca WITH (NOLOCK) 
LEFT JOIN dbo.LeadEvent le WITH (NOLOCK) on le.LeadEventID = ca.LatestNoteLeadEventID 
LEFT JOIN dbo.NoteType nt WITH (NOLOCK) on nt.NoteTypeID = le.NoteTypeID 
LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) on ls.StatusID = ca.ClientStatusID 
LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated
GO
GRANT SELECT ON  [dbo].[vCaseWithLatestNoteEvent] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCaseWithLatestNoteEvent] TO [sp_executeall]
GO
