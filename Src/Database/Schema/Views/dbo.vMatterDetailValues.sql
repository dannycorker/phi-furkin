SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- View vMatterDetailValues
-- JWG 2010-06-23 Merged the two halves of the original query together for reduced footprint
-- =============================================
CREATE VIEW [dbo].[vMatterDetailValues]  
AS
SELECT mdv1.MatterDetailValueID, 
m.ClientID, 
m.LeadID, 
m.MatterID, 
df1.DetailFieldID, 
df1.FieldName, 
df1.FieldCaption, 
qt1.Description as FieldFormat, 
CASE qt1.QuestionTypeID WHEN 4 THEN COALESCE (luli1.ItemValue, '') ELSE COALESCE (mdv1.DetailValue, '') END as [DetailValue], 
mdv1.DetailValue as [RawValue], 
CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueInt ELSE mdv1.ValueInt END AS ValueInt, 
CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueMoney ELSE mdv1.ValueMoney END AS ValueMoney, 
CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueDate ELSE mdv1.ValueDate END AS ValueDate, 
CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueDateTime ELSE mdv1.ValueDateTime END AS ValueDateTime, qt1.QuestionTypeID 
FROM dbo.Matter m WITH (NOLOCK) 
INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID -- Join to Lead to get the Lead Type
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 2 AND df1.Enabled = 1 -- Get all enabled fields for this Lead Type
INNER JOIN dbo.QuestionTypes AS qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID -- Get the field format (for info only)
LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = df1.DetailFieldID -- Finally get the value, null if not present
LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt -- Finally, look up the value

/*
/* All Regular fields for this Matter, whether or not the detail value actually exists */
SELECT mdv1.MatterDetailValueID, m.ClientID, m.LeadID, m.MatterID, df1.DetailFieldID, df1.FieldName, df1.FieldCaption, qt1.Description as FieldFormat, IsNull(mdv1.DetailValue, '') as [DetailValue], mdv1.DetailValue as [RawValue], mdv1.ValueInt, mdv1.ValueMoney, mdv1.ValueDate, mdv1.ValueDateTime 
FROM dbo.Matter m WITH (NOLOCK) 
INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID -- Join to Lead to get the Lead Type
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 2 AND df1.Enabled = 1 -- Get all enabled fields for this Lead Type
INNER JOIN dbo.QuestionTypes AS qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID -- Get the field format (for info only)
LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = df1.DetailFieldID -- Finally get the value, null if not present
WHERE (qt1.QuestionTypeID <> 4) -- 4 = LookupList, 14 = ResourceList, 16 = Table
--WHERE (qt1.QuestionTypeID NOT IN (4, 14, 16)) -- 4 = LookupList, 14 = ResourceList, 16 = Table

UNION

/* LookupList values for this Matter, whether or not the detail value actually exists */
SELECT mdv1.MatterDetailValueID, m.ClientID, m.LeadID, m.MatterID, df1.DetailFieldID, df1.FieldName, df1.FieldCaption, qt1.Description as FieldFormat, IsNull(luli1.ItemValue, '') AS [DetailValue], mdv1.DetailValue as [RawValue], luli1.ValueInt, luli1.ValueMoney, luli1.ValueDate, luli1.ValueDateTime  
FROM dbo.Matter m WITH (NOLOCK) 
INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID -- Join to Lead to get the Lead Type 
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 2 AND df1.Enabled = 1 -- Get all enabled fields for this Lead Type
INNER JOIN dbo.QuestionTypes AS qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID -- Get the field format (for info only)
LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = df1.DetailFieldID -- Get the value, null if not present
LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt -- Finally, look up the value
WHERE (qt1.QuestionTypeID = 4) -- 4 = LookupList
*/



GO
GRANT SELECT ON  [dbo].[vMatterDetailValues] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vMatterDetailValues] TO [sp_executeall]
GO
