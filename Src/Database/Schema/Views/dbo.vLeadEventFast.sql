SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- View vLeadEventFast
-- =============================================
CREATE VIEW [dbo].[vLeadEventFast]  
AS

SELECT le.LeadEventID, 
le.ClientID, 
le.LeadID, 
le.WhenCreated, 
le.WhoCreated, 
le.Cost, 
le.Comments, 
le.EventTypeID, 
le.NoteTypeID, 
le.FollowupDateTime, 
le.WhenFollowedUp, 
le.AquariumEventType, 
le.NextEventID, 
le.CaseID, 
le.LeadDocumentID, 
le.NotePriority, 
le.DocumentQueueID, 
le.EventDeleted, 
le.WhoDeleted, 
le.DeletionComments, 
le.ContactID, 
convert(char(10), le.WhenCreated, 126) as WhenCreatedDateOnly, 
datediff(dd, le.WhenCreated, dbo.fn_GetDate_Local()) as AgeInDays, 
cp.UserName 
FROM dbo.LeadEvent le WITH (NOLOCK) 
INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated 
WHERE le.eventdeleted = 0
GO
GRANT SELECT ON  [dbo].[vLeadEventFast] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadEventFast] TO [sp_executeall]
GO
