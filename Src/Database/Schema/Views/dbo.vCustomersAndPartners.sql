SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- View vCustomers
-- =============================================
CREATE VIEW [dbo].[vCustomersAndPartners]  
AS

SELECT c.CustomerID, 
c.ClientID,
c.IsBusiness,
IsNull(t.Title, '') as Title,
c.FirstName,
c.MiddleName,
c.LastName,
c.EmailAddress,
c.DayTimeTelephoneNumber,
c.HomeTelephone,
c.MobileTelephone,
c.CompanyTelephone,
c.WorksTelephone,
c.Address1,
c.Address2,
c.Town,
c.County,
c.PostCode,
c.CountryID, 
IsNull(cou.CountryName, '') as CountryName,
IsNull(cou.Alpha3Code, '') as CountryCode,
c.AquariumStatusID,
c.CompanyName,
c.Occupation,
c.Employer,
c.Fullname,
c.DoNotEmail,
c.DoNotSellToThirdParty,
c.AgreedToTermsAndConditions,
c.DateOfBirth,
c.DefaultContactID,
c.DefaultOfficeID,
c.AddressVerified,
c.SubClientID,
c.CustomerRef,
IsNull(ast.AquariumStatusName, '') as [AquariumStatusName],
p.PartnerID as [PartnerID],
p.UseCustomerAddress as [PartnerUseCustomerAddress],
p.TitleID as [PartnerTitleID],
IsNull(tp.Title, '') as [PartnerTitle],
p.FirstName as [PartnerFirstName],
p.MiddleName as [PartnerMiddleName],
p.LastName as [PartnerLastName],
p.EmailAddress as [PartnerEmailAddress],
p.DayTimeTelephoneNumber as [PartnerDayTimeTelephoneNumber],
p.HomeTelephone as [PartnerHomeTelephone],
p.MobileTelephone as [PartnerMobileTelephone],
p.Address1 as [PartnerAddress1],
p.Address2 as [PartnerAddress2],
p.Town as [PartnerTown],
p.County as [PartnerCounty],
p.PostCode as [PartnerPostCode],
p.Occupation as [PartnerOccupation],
p.Employer as [PartnerEmployer],
p.DateOfBirth as [PartnerDateOfBirth],
p.FullName as [PartnerFullName],
p.CountryID as [PartnerCountryID]
FROM dbo.Customers c WITH (NOLOCK) 
LEFT JOIN dbo.Titles t WITH (NOLOCK) ON t.TitleID = c.TitleID 
LEFT JOIN dbo.Partner p WITH (NOLOCK) ON p.CustomerID = c.CustomerID 
LEFT JOIN dbo.Titles tp WITH (NOLOCK) ON tp.TitleID = p.TitleID 
LEFT JOIN dbo.AquariumStatus ast WITH (NOLOCK) ON ast.AquariumStatusID = c.AquariumStatusID 
LEFT JOIN dbo.Country cou WITH (NOLOCK) ON cou.CountryID = c.CountryID 
WHERE (c.Test = 0 OR c.Test IS NULL) 
AND c.LastName <> 'Test'

GO
GRANT SELECT ON  [dbo].[vCustomersAndPartners] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCustomersAndPartners] TO [sp_executeall]
GO
