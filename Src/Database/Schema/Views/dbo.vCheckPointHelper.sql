SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- View vWrittenPremium 
-- View of written Premium from the Premium Detail History Table 
-- 2018-03-15 GPR Added PremiumCalculationID (DF: 175709)
-- 2018-08-30 GPR Added Commission_Value and Transaction_Commission_Value
-- 2019-10-15 CPS for JIRA LPC-37	| Added QuotePetProductCheckpoint join
--									| Sorted hard-coded adjustment types
--									| Updated IPT to read from the table
--									| Removed in-view calculations because these will be slow AF
-- 2020-01-12 GPR added UW commission value
-- 2020-09-25 GPR added fn_C605_GetDiscountsXML and fn_C605_GetPolicySectionXML in place of CSV versions
-- =============================================
CREATE VIEW [dbo].[vCheckPointHelper]  
AS

	SELECT 
	 wp.WrittenPremiumID as [WrittenPremiumID], -- Join
    ABS(qpcDiscount.Delta) as [AnnualPremiumDiscount], -- Value of Discount
    qpcTrend.Value as [TrendRatingFactor], -- Rating Factor
	CAST(ISNULL(dbo.fn_C605_GetDiscountsXML(wp.MatterID,wp.PremiumCalculationID ),'') as VARCHAR (MAX)) as [DiscountType],
	ISNULL((1.00 - qpcDiscount.Value) * 100.00,0.00) as [TotalDiscountPercentage], -- Percentage of Discount /*GPR 2021-01-11 updated precision*/
	ISNULL(qpcTax.Delta,0.00) as [TaxAmount], -- Tax amount
	ISNULL(CASE WHEN ISNULL(qpcTax1.Value, 0.00) > 0.00 AND ISNULL(qpcTax2.Value, 0.00) > 0.00 THEN 'State and Municipal' WHEN ISNULL(qpcTax1.Value, 0.00) > 0.00 THEN 'State' WHEN ISNULL(qpcTax2.Value, 0.00) > 0.00 THEN 'Municipal'  END, '') AS [TaxType], -- Type: State, Municipal, Both
    ISNULL(qpcTerritory.Value,0.00) as [RatingTerritoryCode], -- Rating Factor
	CAST(dbo.fn_C605_GetPolicySectionXML(mdvScheme.MatterID) as VARCHAR (MAX)) as [Coverages],
	ISNULL(rldvWellness.DetailValue,'') AS [WellnessProduct],
	qpcWellness.Value AS [WellnessCost],
	wp.ClientID,
	mdvscheme.MatterID 
	FROM WrittenPremium wp WITH (NOLOCK) 
	INNER JOIN QuotePetProduct qp WITH (NOLOCK) on  qp.QuotePetProductID = wp.QuotePetProductID
	INNER JOIN QuotePetProductCheckpoint qpcDiscount WITH (NOLOCK) ON qp.QuotePetProductID = qpcDiscount.QuotePetProductID and qpcDiscount.CheckpointName = 'Discounts'
    INNER JOIN QuotePetProductCheckpoint qpcTrend WITH (NOLOCK) ON qp.QuotePetProductID = qpcTrend.QuotePetProductID and qpcTrend.CheckpointName = 'Rating Trend'
	INNER JOIN QuotePetProductCheckpoint qpcExamFee WITH (NOLOCK) ON qp.QuotePetProductID = qpcExamFee.QuotePetProductID and qpcExamFee.CheckpointName = 'Exam Fee Option'
	INNER JOIN QuotePetProductCheckpoint qpcTerritory WITH (NOLOCK) ON qp.QuotePetProductID = qpcTerritory.QuotePetProductID and qpcTerritory.CheckpointName = 'Rating Territory'
	INNER JOIN QuotePetProductCheckpoint qpcTax WITH (NOLOCK) ON qp.QuotePetProductID = qpcTax.QuotePetProductID and qpcTax.CheckpointName = 'Tax'
	INNER JOIN QuotePetProductCheckpoint qpcTax1 WITH (NOLOCK) ON qp.QuotePetProductID = qpcTax1.QuotePetProductID and qpcTax1.CheckpointName = 'TaxLevel1'
	INNER JOIN QuotePetProductCheckpoint qpcTax2 WITH (NOLOCK) ON qp.QuotePetProductID = qpcTax2.QuotePetProductID and qpcTax2.CheckpointName = 'TaxLevel2'
	INNER JOIN MatterDetailValues schm WITH (NOLOCK) on schm.MatterID = wp.MatterID and schm.DetailFieldID = 170034 
	INNER JOIN LeadDetailValues Species WITH (NOLOCK) on species.LeadID = wp.LeadID and Species.DetailFieldID = 144272
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = Species.ValueInt and rldv.DetailFieldID = 144269
	INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON mdvScheme.DetailFieldID IN (145689) AND mdvScheme.ValueInt = schm.ValueInt
	INNER JOIN MatterDetailValues mdvSpecies WITH ( NOLOCK ) ON mdvSpecies.MatterID = mdvScheme.MatterID AND mdvSpecies.DetailFieldID = 177283 and mdvSpecies.ValueInt = rldv.ValueInt/*Scheme Species*/
	LEFT JOIN MatterDetailValues mdvWellness WITH ( NOLOCK ) ON mdvWellness.MatterID = wp.MatterID AND mdvWellness.DetailFieldID = 314235 /*Current Wellness Product*/
	LEFT JOIN ResourceListDetailValues rldvWellness WITH (NOLOCK) on rldvWellness.ResourceListID = mdvWellness.ValueInt and rldvWellness.DetailFieldID = 314005
	INNER JOIN QuotePetProductCheckpoint qpcWellness WITH (NOLOCK) ON qp.QuotePetProductID = qpcWellness.QuotePetProductID and qpcWellness.CheckpointName = 'Wellness Option'

GO
