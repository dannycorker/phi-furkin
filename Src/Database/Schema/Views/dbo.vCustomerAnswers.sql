SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- View vCustomerAnswers
-- =============================================
CREATE VIEW [dbo].[vCustomerAnswers]  
AS
SELECT c.CustomerID,
c.ClientID, 
ca.CustomerQuestionnaireID, 
ca.MasterQuestionID, 
qt.Description as [QuestionType], 
mq.QuestionText, 
mq.QuestionOrder, 
IsNull(IsNull(qpa.AnswerText, ca.Answer), '') as [AnswerText],
COALESCE(ca.Answer, '') as [FreeTextAnswer], 
COALESCE(qpa.AnswerText, '') as [SelectedAnswer],
ca.QuestionPossibleAnswerID, 
mq.QuestionTypeID, 
mq.DefaultAnswerID, 
mq.Mandatory, 
mq.QuestionToolTip, 
mq.Active, 
mq.LinkedQuestionnaireMasterQuestionID 
FROM dbo.Customers c WITH (NOLOCK) 
LEFT JOIN dbo.CustomerQuestionnaires cuq WITH (NOLOCK) ON cuq.CustomerID = c.CustomerID 
LEFT JOIN dbo.CustomerAnswers ca WITH (NOLOCK) ON ca.CustomerQuestionnaireID = cuq.CustomerQuestionnaireID 
LEFT JOIN dbo.MasterQuestions mq WITH (NOLOCK) ON mq.MasterQuestionID = ca.MasterQuestionID 
LEFT JOIN dbo.QuestionPossibleAnswers qpa WITH (NOLOCK) ON qpa.QuestionPossibleAnswerID = ca.QuestionPossibleAnswerID
LEFT JOIN dbo.QuestionTypes qt WITH (NOLOCK) ON mq.QuestionTypeID = qt.QuestionTypeID

GO
GRANT SELECT ON  [dbo].[vCustomerAnswers] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCustomerAnswers] TO [sp_executeall]
GO
