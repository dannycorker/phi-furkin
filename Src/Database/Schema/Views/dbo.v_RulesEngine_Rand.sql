SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_RulesEngine_Rand]
AS

SELECT RAND() [RandomNumber]

GO
GRANT SELECT ON  [dbo].[v_RulesEngine_Rand] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_RulesEngine_Rand] TO [sp_executeall]
GO
