SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C600_Pet]
AS

	SELECT	 p.LeadID
			,p.PolicyNumber
			,p.BreedResourceListID
			,p.Species
			,p.Breed
			,p.[144268]					[PetName]
			,p.[144275]					[PetGender]
			,CAST(p.[144274] as DATE)	[PetDateOfBirth]
			,p.[152783]					[PetNeuteredYesNo]
			,p.[145024]					[PetAgeInMonths]
			,p.[177372]					[MicrochippedYesNo]
			,p.[170030]					[MicrochipNumber]
			,p.[144339]					[PetPurchaseCost]
			,p.[144273]					[PetColour]
			,p.[175496]					[ExistingConditionsYesNo]
	FROM		
		(	
		SELECT l.LeadID, l.LeadRef [PolicyNumber], ldvBreed.ValueInt [BreedResourceListID], liSpecies.ItemValue [Species], rdvBreed.DetailValue [Breed], ldv.DetailFieldID, COALESCE(li.ItemValue,ldv.DetailValue) [DetailValue]
		FROM Lead l WITH ( NOLOCK ) 
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) on ldv.LeadID = l.LeadID
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = ldv.DetailFieldID
		 LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = df.LookupListID AND li.LookupListItemID = ldv.ValueInt
		INNER JOIN LeadDetailValues ldvBreed WITH ( NOLOCK ) on ldvBreed.LeadID = l.LeadID AND ldvBreed.DetailFieldID = 144272 /*Pet Type*/
		 LEFT JOIN ResourceListDetailValues rdvSpecies WITH ( NOLOCK ) on rdvSpecies.ResourceListID = ldvBreed.ValueInt AND rdvSpecies.DetailFieldID = 144269 /*Pet Type*/
		 LEFT JOIN LookupListItems liSpecies WITH ( NOLOCK ) on liSpecies.LookupListItemID = rdvSpecies.ValueInt
		 LEFT JOIN ResourceListDetailValues rdvBreed WITH ( NOLOCK ) on rdvBreed.ResourceListID = ldvBreed.ValueInt AND rdvBreed.DetailFieldID = 144270 /*Breed*/
		WHERE l.LeadTypeID = 1492 /*Policy Admin*/ 
		AND l.ClientID = 433
		AND ldv.DetailFieldID NOT IN ( 144272 ) /*Pet Type*/
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailValue])		
	FOR		
	[DetailFieldID]
		IN ( [144268],[144275],[144274],[152783],[145024],[177372],[170030],[144339],[144273],[175496] )
	) AS p


GO
GRANT SELECT ON  [dbo].[v_C600_Pet] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C600_Pet] TO [sp_executeall]
GO
