SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[v_C321_Policies]
AS
SELECT 
	cu.CustomerID, cu.Fullname Policyholder, cu.Test
	,l.LeadID, l.LeadRef PolicyNumber
	,ldvPetname.DetailValue PetName
	,lliType.ItemValue PetType, rldvBreed.DetailValue PetBreed
	,lliGender.ItemValue PetGender, ldvPetNeuter.DetailValue PetNeuter, ldvPetDOB.ValueDate PetDOB, ldvPetCost.ValueMoney PetCost
	,rldvAffinity.DetailValue Affinity, rldvProduct.DetailValue Product, lliPolType.ItemValue PolicyType
	,ldvIncept.ValueDate InceptionDate, ldvStart.ValueDate PolicyStart, ldvEnd.ValueDate PolicyEnd, lliPolSts.ItemValue PolicyStatus
	,rldvBrandCode.DetailValue BrandCode, rldvBrandName.DetailValue BrandName, rldvBrandShort.DetailValue BrandShortName
	,l.ClientID
FROM 
	Lead l WITH (NOLOCK)
	-- Policyholder
	INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = l.CustomerID
	-- Pet
	INNER JOIN dbo.LeadDetailValues ldvPetname WITH (NOLOCK) ON ldvPetname.LeadID = l.LeadID AND ldvPetname.DetailFieldID = 144268
	INNER JOIN dbo.LeadDetailValues ldvBreed WITH (NOLOCK) ON ldvBreed.LeadID = l.LeadID AND ldvBreed.DetailFieldID = 144272
	LEFT JOIN ResourceListDetailValues rldvType WITH (NOLOCK) ON rldvType.ResourceListID = ldvBreed.ValueInt AND rldvType.DetailFieldID = 144269
	LEFT JOIN LookupListItems lliType WITH (NOLOCK) ON lliType.LookupListItemID = rldvType.ValueInt
	LEFT JOIN ResourceListDetailValues rldvBreed WITH (NOLOCK) ON rldvBreed.ResourceListID = ldvBreed.ValueInt AND rldvBreed.DetailFieldID = 144270
	INNER JOIN dbo.LeadDetailValues ldvPetGender WITH (NOLOCK) ON ldvPetGender.LeadID = l.LeadID AND ldvPetGender.DetailFieldID = 144275
	LEFT JOIN LookupListItems lliGender WITH (NOLOCK) ON lliGender.LookupListItemID = ldvPetGender.ValueInt
	INNER JOIN dbo.LeadDetailValues ldvPetNeuter WITH (NOLOCK) ON ldvPetNeuter.LeadID = l.LeadID AND ldvPetNeuter.DetailFieldID = 152783
	INNER JOIN dbo.LeadDetailValues ldvPetDOB WITH (NOLOCK) ON ldvPetDOB.LeadID = l.LeadID AND ldvPetDOB.DetailFieldID = 144274
	INNER JOIN dbo.LeadDetailValues ldvPetCost WITH (NOLOCK) ON ldvPetCost.LeadID = l.LeadID AND ldvPetCost.DetailFieldID = 144339
	-- Policy
	INNER JOIN dbo.LeadDetailValues ldvCurPol WITH (NOLOCK) ON ldvCurPol.LeadID = l.LeadID AND ldvCurPol.DetailFieldID = 146193
	LEFT JOIN ResourceListDetailValues rldvAffinity WITH (NOLOCK) ON rldvAffinity.ResourceListID = ldvCurPol.ValueInt AND rldvAffinity.DetailFieldID = 144314
	LEFT JOIN ResourceListDetailValues rldvProduct WITH (NOLOCK) ON rldvProduct.ResourceListID = ldvCurPol.ValueInt AND rldvProduct.DetailFieldID = 146200
	LEFT JOIN ResourceListDetailValues rldvPolType WITH (NOLOCK) ON rldvPolType.ResourceListID = ldvCurPol.ValueInt AND rldvPolType.DetailFieldID = 144319
	LEFT JOIN LookupListItems lliPolType WITH (NOLOCK) ON lliPolType.LookupListItemID = rldvPolType.ValueInt
	INNER JOIN dbo.LeadDetailValues ldvIncept WITH (NOLOCK) ON ldvIncept.LeadID = l.LeadID AND ldvIncept.DetailFieldID = 146194
	INNER JOIN dbo.LeadDetailValues ldvStart WITH (NOLOCK) ON ldvStart.LeadID = l.LeadID AND ldvStart.DetailFieldID = 146195
	INNER JOIN dbo.LeadDetailValues ldvEnd WITH (NOLOCK) ON ldvEnd.LeadID = l.LeadID AND ldvEnd.DetailFieldID = 146196
	INNER JOIN dbo.LeadDetailValues ldvStatus WITH (NOLOCK) ON ldvStatus.LeadID = l.LeadID AND ldvStatus.DetailFieldID = 146197
	LEFT JOIN LookupListItems lliPolSts WITH (NOLOCK) ON lliPolSts.LookupListItemID = ldvStatus.ValueInt
	LEFT JOIN dbo.LeadDetailValues ldvBrand WITH (NOLOCK) ON ldvBrand.LeadID = l.LeadID AND ldvBrand.DetailFieldID = 170019
	LEFT JOIN ResourceListDetailValues rldvBrandCode WITH (NOLOCK) ON rldvBrandCode.ResourceListID = ldvBrand.ValueInt AND rldvBrandCode.DetailFieldID = 170017
	LEFT JOIN ResourceListDetailValues rldvBrandName WITH (NOLOCK) ON rldvBrandName.ResourceListID = ldvBrand.ValueInt AND rldvBrandName.DetailFieldID = 170018
	LEFT JOIN ResourceListDetailValues rldvBrandShort WITH (NOLOCK) ON rldvBrandShort.ResourceListID = ldvBrand.ValueInt AND rldvBrandShort.DetailFieldID = 170027
WHERE
	l.ClientID = 321



GO
GRANT SELECT ON  [dbo].[v_C321_Policies] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C321_Policies] TO [sp_executeall]
GO
