SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C00_GenericReportFields]
AS

	SELECT	 dbo.fn_GetDate_Local()						[ThisDateTime]
			,CAST(dbo.fn_GetDate_Local() as DATE)		[ThisDate]
			,CAST(DATEADD(DAY,-(DAY(dbo.fn_GetDate_Local()))+1,dbo.fn_GetDate_Local()) as DATE) [FirstDayOfThisMonth]
			,DATENAME(MONTH,dbo.fn_GetDate_Local())		[ThisMonthName]
			,DATEPART(YEAR,dbo.fn_GetDate_Local())		[ThisYear]
			,CAST(dbo.fn_GetDate_Local()-1 as DATE)		[YesterdayDate]
			,CAST(dbo.fn_GetDate_Local()+1 as DATE)		[TomorrowDate]
			,DB_NAME()						[DatabaseName]
			,''								[EmptyColumn]
			,' '							[SingleSpace]
			,0.00							[EmptyDecimal]
			,'n/a'							[NaColumn]
			,cl.ClientID					[ClientID]
			,cl.CompanyName					[CompanyName]
			,'true'							[True]
			,'false'						[False]
			,'yes'							[Yes]
			,'no'							[No]
	FROM Clients cl WITH ( NOLOCK )
GO
GRANT SELECT ON  [dbo].[v_C00_GenericReportFields] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C00_GenericReportFields] TO [sp_executeall]
GO
