SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C433_SchemeDetails]
AS
	
	SELECT l.LeadID [SchemeLeadID], m.MatterID [SchemeMatterID], rdvProduct.ResourceListID, rdvProduct.DetailValue [ProductName], rdvAffinity.DetailValue [Affinity]
	,ISNULL( (	SELECT MAX(tdv.ValueMoney) FROM TableDetailValues tdv WITH ( NOLOCK ) WHERE tdv.MatterID = m.MatterID AND tdv.DetailFieldID = 176985 /*Limit*/ ),0.00) [ConditionLimit]
	,tdvLimit.ValueMoney [VetFeesLimit]
	,liSpecies.ItemValue [Species]
	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID
	LEFT JOIN MatterDetailValues mdvProduct WITH ( NOLOCK ) on mdvProduct.MatterID = m.MatterID AND mdvProduct.DetailFieldID = 145689 /*Policy Scheme*/
	LEFT JOIN ResourceListDetailValues rdvProduct WITH ( NOLOCK ) on rdvProduct.ResourceListID = mdvProduct.ValueInt AND rdvProduct.DetailFieldID = 146200 /*Product*/
	LEFT JOIN ResourceListDetailValues rdvAffinity WITH ( NOLOCK ) on rdvAffinity.ResourceListID = mdvProduct.ValueInt AND rdvAffinity.DetailFieldID = 144314 /*Affinity*/
	LEFT JOIN MatterDetailValues mdvSpecies WITH ( NOLOCK ) on mdvSpecies.MatterID = m.MatterID AND mdvSpecies.DetailFieldID = 177283 /*Scheme Species*/
	LEFT JOIN LookupListItems liSpecies WITH ( NOLOCK ) on liSpecies.LookupListItemID = mdvSpecies.ValueInt
	INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = m.MatterID AND tr.DetailFieldID = 145692 /*Policy Limits*/
	INNER JOIN TableDetailValues tdvVetFees WITH ( NOLOCK ) on tdvVetFees.TableRowID = tr.TableRowID AND tdvVetFees.DetailFieldID = 144357 /*Policy Section*/
	INNER JOIN TableDetailValues tdvLimit WITH ( NOLOCK ) on tdvLimit.TableRowID = tr.TableRowID AND tdvLimit.DetailFieldID = 144358 /*Sum Insured*/
	WHERE l.LeadTypeID = 1491 /*Schemes*/
	AND tdvVetFees.ResourceListID = dbo.fn_C00_1272_GetVetFeesResourceList(l.ClientID)
	AND cu.Test = 0

GO
GRANT SELECT ON  [dbo].[v_C433_SchemeDetails] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C433_SchemeDetails] TO [sp_executeall]
GO
