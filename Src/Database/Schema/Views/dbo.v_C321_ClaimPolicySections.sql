SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[v_C321_ClaimPolicySections]
AS
/* Comma-concatenated list of policy sections */
SELECT 
	MatterID, [1] + ISNULL(', ' + [2],'') + ISNULL(', ' + [3],'') + ISNULL(', ' + [4],'') + ISNULL(', ' + [5],'') PolicySections
FROM (
	SELECT 
		m.matterid
		, lliSection.ItemValue
		, ROW_NUMBER() OVER(PARTITION BY m.MatterID ORDER BY CASE WHEN lliSection.ItemValue = 'Veterinary fees' THEN 0 ELSE 1 END, lliSection.ItemValue) rn
	FROM 	
		Matter m WITH (NOLOCK)
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355
		INNER JOIN dbo.TableDetailValues tSection WITH (NOLOCK) ON tSection.TableRowID = r.TableRowID AND tSection.DetailFieldID = 144350
		INNER JOIN dbo.ResourceListDetailValues rldvSection WITH (NOLOCK) ON rldvSection.ResourceListID = tSection.ResourceListID AND rldvSection.DetailFieldID = 146189
		--LEFT JOIN dbo.ResourceListDetailValues rldvSubSection WITH (NOLOCK) ON rldvSubSection.ResourceListID = tSection.ResourceListID AND rldvSubSection.DetailFieldID = 146190
		LEFT JOIN LookupListItems lliSection WITH (NOLOCK) ON lliSection.LookupListItemID = rldvSection.ValueInt
		--LEFT JOIN LookupListItems lliSubSection WITH (NOLOCK) ON lliSubSection.LookupListItemID = rldvSubSection.ValueInt
	GROUP BY 
		m.MatterID, lliSection.ItemValue
	) AS src
PIVOT (
	MAX(ItemValue) FOR rn IN ([1],[2],[3],[4],[5])) AS pvt



GO
GRANT SELECT ON  [dbo].[v_C321_ClaimPolicySections] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C321_ClaimPolicySections] TO [sp_executeall]
GO
