SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C433_PolicyDetails]
AS

	SELECT	 l.LeadID
			,l.LeadRef [PolicyNumber]
			,m.MatterID
			,mdvProductRl.ValueInt		[ProductResourceListID]
			,rdvProductName.DetailValue [ProductName]
			,liSchemeCode.ItemValue		[SchemeCode]
			,mdvStatus.ValueInt			[StatusLookupListItemID]
			,liStatus.ItemValue			[Status]
			,mdvInceptionDate.ValueDate	[InceptionDate]
			,mdvStartDate.ValueDate		[StartDate]
			,mdvEndDate.ValueDate		[EndDate]
			,mdvAnnual.ValueMoney		[Annual]
			,mdvFirstMonth.ValueMoney	[FirstMonth]
			,mdvOtherMonth.ValueMoney	[OtherMonth]
			,mdvPremiumCalculationID.ValueInt [PremiumCalculationID]
			,dbo.fn_C00_GetUrl_CustomerLeadDetailsByObjectID(l.LeadID,1) [Url]
	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID 
	INNER JOIN MatterDetailValues mdvProductRl WITH ( NOLOCK ) on mdvProductRl.MatterID = m.MatterID AND mdvProductRl.DetailFieldID = 170034 /*Current Policy*/
	 LEFT JOIN ResourceListDetailValues rdvProductName WITH ( NOLOCK ) on rdvProductName.ResourceListID = mdvProductRl.ValueInt AND rdvProductName.DetailFieldID = 146200 /*Product*/
	 LEFT JOIN ResourceListDetailValues rdvSchemeCode WITH ( NOLOCK ) on rdvSchemeCode.ResourceListID = mdvProductRl.ValueInt AND rdvSchemeCode.DetailFieldID = 144318 /*Scheme Code*/
	 LEFT JOIN LookupListItems liSchemeCode WITH ( NOLOCK ) on liSchemeCode.LookupListItemID = rdvSchemeCode.ValueInt
	 LEFT JOIN MatterDetailValues mdvStatus WITH ( NOLOCK ) on mdvStatus.MatterID = m.MatterID AND mdvStatus.DetailFieldID = 170038 /*Policy Status*/
	 LEFT JOIN LookupListItems liStatus WITH ( NOLOCK ) on liStatus.LookupListItemID = mdvStatus.ValueInt
	 LEFT JOIN MatterDetailValues mdvInceptionDate WITH ( NOLOCK ) on mdvInceptionDate.MatterID = m.MatterID AND mdvInceptionDate.DetailFieldID = 170035 /*Policy Inception Date*/
	 LEFT JOIN MatterDetailValues mdvStartDate WITH ( NOLOCK ) on mdvStartDate.MatterID = m.MatterID AND mdvStartDate.DetailFieldID = 170036 /*Policy Start Date*/
	 LEFT JOIN MatterDetailValues mdvEndDate WITH ( NOLOCK ) on mdvEndDate.MatterID = m.MatterID AND mdvEndDate.DetailFieldID = 170037 /*Policy End Date*/
	 LEFT JOIN MatterDetailValues mdvAnnual WITH ( NOLOCK ) on mdvAnnual.MatterID = m.MatterID AND mdvAnnual.DetailFieldID = 175337 /*Current/New Premium - Annual*/
	 LEFT JOIN MatterDetailValues mdvFirstMonth	WITH ( NOLOCK ) on mdvFirstMonth.MatterID = m.MatterID AND mdvFirstMonth.DetailFieldID = 175338 /*Current/New Premium - First Month*/
	 LEFT JOIN MatterDetailValues mdvOtherMonth	WITH ( NOLOCK ) on mdvOtherMonth.MatterID = m.MatterID AND mdvOtherMonth.DetailFieldID = 175339 /*Current/New Premium - Other Month*/
	 LEFT JOIN MatterDetailValues mdvPremiumCalculationID WITH ( NOLOCK ) on mdvPremiumCalculationID.MatterID = m.MatterID AND mdvPremiumCalculationID.DetailFieldID = 175711 /*Current Premium/New - PremiumCalculationID*/
	 
	WHERE l.LeadTypeID = 1492 /*Policy Admin*/
	AND l.ClientID = 433
GO
GRANT SELECT ON  [dbo].[v_C433_PolicyDetails] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C433_PolicyDetails] TO [sp_executeall]
GO
