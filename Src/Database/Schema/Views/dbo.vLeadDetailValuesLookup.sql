SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vLeadDetailValuesLookup] 
AS

/* Regular field */
SELECT ldv1.[LeadDetailValueID], ldv1.[ClientID], ldv1.[LeadID], ldv1.[DetailFieldID], ldv1.[DetailValue], ldv1.ValueInt, ldv1.ValueMoney, ldv1.ValueDate, ldv1.ValueDateTime 
FROM [dbo].LeadDetailValues ldv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = ldv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
WHERE qt1.QuestionTypeID NOT IN (4, 14, 16)

UNION

/* Lookup Lists */
SELECT ldv1.[LeadDetailValueID], ldv1.[ClientID], ldv1.[LeadID], ldv1.[DetailFieldID], luli1.ItemValue as [DetailValue], luli1.ValueInt, luli1.ValueMoney, luli1.ValueDate, luli1.ValueDateTime 
FROM [dbo].LeadDetailValues ldv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = ldv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
INNER JOIN [dbo].LookupListItems luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt 
WHERE qt1.QuestionTypeID = 4

UNION

/* Resource Lists */
SELECT ldv1.[LeadDetailValueID], ldv1.[ClientID], ldv1.[LeadID], ldv1.[DetailFieldID], rldv1.DetailValue as [DetailValue], rldv1.ValueInt, rldv1.ValueMoney, rldv1.ValueDate, rldv1.ValueDateTime 
FROM [dbo].LeadDetailValues ldv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = ldv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
INNER JOIN [dbo].ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.DetailFieldID = df1.DetailFieldID AND rldv1.ResourceListID = ldv1.ValueInt 
WHERE qt1.QuestionTypeID = 14

UNION

/* Table Lists */
SELECT ldv1.[LeadDetailValueID], ldv1.[ClientID], ldv1.[LeadID], ldv1.[DetailFieldID], tdv1.DetailValue as [DetailValue], tdv1.ValueInt, tdv1.ValueMoney, tdv1.ValueDate, tdv1.ValueDateTime 
FROM [dbo].LeadDetailValues ldv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = ldv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
INNER JOIN [dbo].TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.DetailFieldID = df1.DetailFieldID AND tdv1.TableRowID = ldv1.ValueInt AND tdv1.LeadID = ldv1.LeadID 
WHERE qt1.QuestionTypeID = 16


GO
GRANT SELECT ON  [dbo].[vLeadDetailValuesLookup] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadDetailValuesLookup] TO [sp_executeall]
GO
