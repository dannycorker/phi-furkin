SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE VIEW [dbo].[v_C433_BordereauClaim]
AS


-- CLAIM BORDEREAU

	WITH PremiumMatters AS (
		SELECT polprem.MatterID [PolAdminMatterID], MAX(polprem.TableRowID) [TRID]
		FROM TableRows polprem WITH (NOLOCK) 
		INNER JOIN TableDetailValues pcid WITH (NOLOCK) ON polprem.TableRowID=pcid.TableRowID AND pcid.DetailFieldID=175710
		WHERE polprem.DetailFieldID=170088 
		AND pcid.ValueInt > 0
		GROUP BY polprem.MatterID 
	),
	
	PremiumHistoryRows AS (
		SELECT pm.PolAdminMatterID,ph.TableRowID [TableRowID],sd.DetailValue[StartDate],ed.DetailValue[EndDate],stat.ValueInt[Status],volex.ValueMoney[VolEx] 
		FROM PremiumMatters pm
		INNER JOIN TableRows ph WITH (NOLOCK) ON pm.PolAdminMatterID=ph.MatterID AND ph.DetailFieldID=170033
		INNER JOIN TableDetailValues sd WITH (NOLOCK) ON ph.TableRowID=sd.TableRowID AND sd.DetailFieldID=145663
		INNER JOIN TableDetailValues ed WITH (NOLOCK) ON ph.TableRowID=ed.TableRowID AND ed.DetailFieldID=145664
		INNER JOIN TableDetailValues stat WITH (NOLOCK) ON ph.TableRowID=stat.TableRowID AND stat.DetailFieldID=145666
		INNER JOIN TableDetailValues volex WITH (NOLOCK) ON ph.TableRowID=volex.TableRowID AND volex.DetailFieldID=145667
	),
	
	ClaimMatters AS (
		SELECT pm.PolAdminMatterID,cm.MatterID [ClaimMatterID]
		FROM PremiumMatters pm
		INNER JOIN Matter pam WITH (NOLOCK) ON pm.PolAdminMatterID=pam.MatterID
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromLeadID=pam.LeadID AND ltr.ToLeadTypeID=1490
		INNER JOIN Matter cm WITH (NOLOCK) ON ltr.ToLeadID=cm.LeadID
		
	),
	
	ClaimDetailsData AS (
		SELECT cm.ClaimMatterID, cdd.TableRowID [ClaimDetailTableRowID], 
				ts.DetailValue [TreatmentStart], 
				te.DetailValue [TreatmentEnd], 
				cl.ValueMoney [Claim], 
				sett.ValueMoney [Settle],
				pspsli.ItemValue [PolicySection], 
				pspssli.ItemValue [SubSection],
				CAST(ABS(xs.ValueMoney) AS VARCHAR) [CompExcess],
				CAST(ABS(vxs.ValueMoney) AS VARCHAR) [VolExcess],
				CAST(ABS(coins.ValueMoney) AS VARCHAR) [CoInsurance],
				CAST(ABS(ud.ValueMoney) AS VARCHAR) [UserDeductions],
				mirt.DetailValue [RiskType]
		FROM ClaimMatters cm
		INNER JOIN TableRows cdd WITH (NOLOCK) ON cm.ClaimMatterID=cdd.MatterID AND cdd.DetailFieldID=144355
		INNER JOIN TableDetailValues ts WITH (NOLOCK) ON cdd.TableRowID=ts.TableRowID AND ts.DetailFieldID=144349
		INNER JOIN TableDetailValues te WITH (NOLOCK) ON cdd.TableRowID=te.TableRowID AND te.DetailFieldID=144351
		INNER JOIN TableDetailValues ps WITH (NOLOCK) ON cdd.TableRowID=ps.TableRowID AND ps.DetailFieldID=144350
		INNER JOIN ResourceListDetailValues psps WITH (NOLOCK) ON ps.ResourceListID=psps.ResourceListID AND psps.DetailFieldID=146189
		INNER JOIN LookupListItems pspsli WITH (NOLOCK) ON psps.ValueInt=pspsli.LookupListItemID
		INNER JOIN ResourceListDetailValues pspss WITH (NOLOCK) ON psps.ResourceListID=pspss.ResourceListID AND pspss.DetailFieldID=146190
		INNER JOIN LookupListItems pspssli WITH (NOLOCK) ON pspss.ValueInt=pspssli.LookupListItemID
		INNER JOIN TableDetailValues cl WITH (NOLOCK) ON cdd.TableRowID=cl.TableRowID AND cl.DetailFieldID=144353
		INNER JOIN TableDetailValues sett WITH (NOLOCK) ON cdd.TableRowID=sett.TableRowID AND sett.DetailFieldID=145678
		INNER JOIN TableDetailValues xs WITH (NOLOCK) ON cdd.TableRowID=xs.TableRowID AND xs.DetailFieldID=146406
		INNER JOIN TableDetailValues vxs WITH (NOLOCK) ON cdd.TableRowID=vxs.TableRowID AND vxs.DetailFieldID=158802
		INNER JOIN TableDetailValues coins WITH (NOLOCK) ON cdd.TableRowID=coins.TableRowID AND coins.DetailFieldID=146407
		INNER JOIN TableDetailValues ud WITH (NOLOCK) ON cdd.TableRowID=ud.TableRowID AND ud.DetailFieldID=146179
		INNER JOIN ResourceListDetailValues aqps WITH (NOLOCK) ON pspsli.ItemValue=aqps.DetailValue AND aqps.DetailFieldID=175717
		INNER JOIN ResourceListDetailValues mirt WITH (NOLOCK) ON aqps.ResourceListID=mirt.ResourceListID AND mirt.DetailFieldID=175715
	),
	ClaimStatus AS (
		SELECT cm.ClaimMatterID, mistat.DetailValue [Status], '#' + CONVERT(VARCHAR(10),le.WhenCreated,120) + '#' [StatusDate],
				ROW_NUMBER() OVER(PARTITION BY cm.ClaimMatterID ORDER BY le.WhenCreated DESC) as [RNO]
		FROM ClaimMatters cm
		INNER JOIN Matter m WITH (NOLOCK) ON cm.ClaimMatterID=m.MatterID
		INNER JOIN Cases ca WITH (NOLOCK) ON m.CaseID=ca.CaseID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.CaseID=le.CaseID AND le.EventDeleted=0
		INNER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID=et.EventTypeID AND et.StatusAfterEvent=ca.ClientStatusID AND et.EventTypeID<>150002
		INNER JOIN ResourceListDetailValues aqstat WITH (NOLOCK) ON ca.ClientStatusID=aqstat.ValueInt AND aqstat.DetailFieldID=175721
		INNER JOIN ResourceListDetailValues mistat WITH (NOLOCK) ON aqstat.ResourceListID=mistat.ResourceListID AND mistat.DetailFieldID=175719
	),
	AdviceDate AS (
		SELECT cm.ClaimMatterID, '#' + CONVERT(VARCHAR(10),le.WhenCreated,120) + '#' [AdviceDate],
				ROW_NUMBER() OVER(PARTITION BY cm.ClaimMatterID ORDER BY le.WhenCreated DESC) as [RNO]
		FROM ClaimMatters cm
		INNER JOIN Matter m WITH (NOLOCK) ON cm.ClaimMatterID=m.MatterID
		INNER JOIN Cases ca WITH (NOLOCK) ON m.CaseID=ca.CaseID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.CaseID=le.CaseID AND le.EventDeleted=0 AND le.EventTypeID=150003
	)
	
	SELECT 
	affSC.ValueInt																	[AffinityShortCode],
	c.CustomerID																	[CustomerID],
	l.LeadID																		[PolAdminLeadID],
	cm.PolAdminMatterID																[PolAdminMatterID],
	phr.TableRowID																	[PolHistoryTableRowID],
	cm.ClaimMatterID																[ClaimMatterID],
	cdd.ClaimDetailTableRowID														[ClaimTableRowID],
	cdd.TreatmentStart																[StartDate],
	cdd.TreatmentEnd																[EndDate],
	CAST(cm.ClaimMatterID AS VARCHAR) 
		+ '/' 
		+ CAST(cdd.ClaimDetailTableRowID AS VARCHAR)								[ClaimKey],
	CAST(cm.PolAdminMatterID AS VARCHAR) + '/' 
		+ CAST(phr.TableRowID AS VARCHAR) + '/' 
		+ cdd.RiskType
																					[RiskKey],
	cm.ClaimMatterID																[ClaimReference],
	cdd.PolicySection 
		+ CASE WHEN LEN(cdd.SubSection) < 2 THEN '' ELSE '/' + cdd.SubSection END	[ClaimElementCode],
	'GBP'																			[CurrencyCode],
	'AA110121TIS-MHE'																[AlphaReference],
	'#' + cdd.TreatmentStart + '#'													[LossPeriodFrom],
	'#' + cdd.TreatmentEnd + '#'													[LossPeriodTo],
	ISNULL(ad.AdviceDate,'')														[AdviceDate],
	firstc.DetailValue																[CauseofLoss],
	cs.Status																		[Status],
	cs.StatusDate																	[StatusDate],
	ISNULL(cdd.CompExcess,'')														[DeductibleCompulsoryExcess],
	ISNULL(cdd.VolExcess,'')														[DeductibleVoluntaryExcess],
	ISNULL(cdd.CoInsurance,'')														[DeductibleCoInsurance],
	descol.DetailValue																[Narrative],
	ISNULL(itli.ItemValue,'')														[IncidentType],
	ISNULL(ctli.ItemValue,'')														[ClaimType],
	CASE WHEN dol.DetailValue='' THEN 'NA' ELSE '#' + dol.DetailValue + '#' END		[DateofDeathLoss],
	ISNULL(bpli.ItemValue,'')														[BodyPart],
	ISNULL(drli.ItemValue,'')														[DeclineReason],
	CASE WHEN rtvet.DetailValue='' THEN rtph.DetailValue ELSE rtvet.DetailValue END	[AdditionalInformation],
	ISNULL(cdd.Claim,'')															[PaidIndemnity],
	ISNULL(cdd.CompExcess,'')														[PaidConsumedDeductibleCompulsoryExcess],
	ISNULL(cdd.VolExcess,'')														[PaidConsumedDeductibleVoluntaryExcess],
	ISNULL(cdd.CoInsurance,'')														[PaidConsumedDeductibleCoInsurance],
	ISNULL(cdd.UserDeductions,'')													[DeductibleUserDeductions],
	'?'																				[PaidAdminCosts],
	0																				[ReserveIndemnity],
	0																				[ReserveConsumedDeductibleCompulsoryExcess],
	0																				[ReserveConsumedDeductibleVoluntaryExcess],
	0																				[ReserveConsumedDeductibleCoInsurance],
	0																				[ReserveDeductibleUserDeductions],
	0																				[ReservePaidAdminCosts]

	FROM Customers c WITH (NOLOCK) 
	INNER JOIN CustomerDetailValues aff WITH (NOLOCK) ON c.CustomerID=aff.CustomerID AND aff.DetailFieldID=170144
	INNER JOIN ResourceListDetailValues affSC WITH (NOLOCK) ON aff.ValueInt=affSC.ResourceListID AND affSC.DetailFieldID=170127
	INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
	INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID
	INNER JOIN ClaimMatters cm WITH (NOLOCK) ON m.MatterID=cm.ClaimMatterID
	INNER JOIN ClaimDetailsData cdd WITH (NOLOCK) ON cm.ClaimMatterID=cdd.ClaimMatterID
	INNER JOIN PremiumHistoryRows phr WITH (NOLOCK) ON cm.PolAdminMatterID=phr.PolAdminMatterID 
				AND cdd.TreatmentStart>=phr.StartDate
				AND cdd.TreatmentEnd<=phr.EndDate
	-- Claim Fields (Optional/Specific)
	INNER JOIN MatterDetailValues ailment WITH (NOLOCK) ON cm.ClaimMatterID=ailment.MatterID AND ailment.DetailFieldID=144504
	INNER JOIN ResourceListDetailValues it WITH (NOLOCK) ON ailment.ValueInt=it.ResourceListID AND it.DetailFieldID=162655
	LEFT JOIN LookupListItems itli WITH (NOLOCK) ON it.ValueInt=itli.LookupListItemID
	INNER JOIN MatterDetailValues ct WITH (NOLOCK) ON cm.ClaimMatterID=ct.MatterID AND ct.DetailFieldID=144483
	LEFT JOIN LookupListItems ctli WITH (NOLOCK) ON ct.ValueInt=ctli.LookupListItemID
	INNER JOIN MatterDetailValues dol WITH (NOLOCK) ON cm.ClaimMatterID=dol.MatterID AND dol.DetailFieldID=144892
	INNER JOIN MatterDetailValues bp WITH (NOLOCK) ON cm.ClaimMatterID=bp.MatterID AND bp.DetailFieldID=144333
	LEFT JOIN LookupListItems bpli WITH (NOLOCK) ON bp.ValueInt=bpli.LookupListItemID
	INNER JOIN MatterDetailValues dr WITH (NOLOCK) ON cm.ClaimMatterID=dr.MatterID AND dr.DetailFieldID=146184
	LEFT JOIN LookupListItems drli WITH (NOLOCK) ON dr.ValueInt=drli.LookupListItemID
	INNER JOIN MatterDetailValues rtvet WITH (NOLOCK) ON cm.ClaimMatterID=rtvet.MatterID AND rtvet.DetailFieldID=152876
	INNER JOIN MatterDetailValues rtph WITH (NOLOCK) ON cm.ClaimMatterID=rtph.MatterID AND rtph.DetailFieldID=152875
	-- Claim Fields (Mandatory )
	INNER JOIN ClaimStatus cs WITH (NOLOCK) ON cm.ClaimMatterID=cs.ClaimMatterID AND cs.RNO=1
	LEFT JOIN AdviceDate ad ON cm.ClaimMatterID=ad.ClaimMatterID AND ad.RNO=1
	INNER JOIN ResourceListDetailValues firstc WITH (NOLOCK) ON ailment.ValueInt=firstc.ResourceListID AND firstc.DetailFieldID=144340
	INNER JOIN MatterDetailValues descol WITH (NOLOCK) ON cm.ClaimMatterID=descol.MatterID AND descol.DetailFieldID=144332
	WHERE  
	-- not test
	c.Test=0







GO
GRANT SELECT ON  [dbo].[v_C433_BordereauClaim] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C433_BordereauClaim] TO [sp_executeall]
GO
