SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- View vCustomers
-- =============================================
CREATE VIEW [dbo].[vDataLoaderFile]  
AS

	SELECT 
	
	dlf.DataLoaderFileID, 
	dlf.ClientID, 
	dlf.DataLoaderMapID, 
	dlf.FileStatusID, 
	dlf.SourceFileNameAndPath, 
	dlf.TargetFileName, 
	dlf.TargetFileLocation, 
	dlf.FileFormatID, 
	dlf.ScheduledDateTime, 
	dlf.DataLoadedDateTime, 
	
	dlfs.FileStatusName, 
	dlfs.FileStatusDescription,
	
	dlff.FileFormatName,
	dlff.FileFormatDescription, 
	
	dlm.MapName, 
	dlm.MapDescription, 
	dlm.LeadTypeID, 
	dlm.EnabledForUse,
	dlm.Deleted 
	
	FROM dbo.DataLoaderFile dlf (nolock) 
	INNER JOIN dbo.DataLoaderFileStatus dlfs (nolock) on dlfs.DataLoaderFileStatusID = dlf.FileStatusID 
	INNER JOIN dbo.DataLoaderFileFormat dlff (nolock) on dlff.DataLoaderFileFormatID = dlf.FileFormatID 
	LEFT JOIN dbo.DataLoaderMap dlm (nolock) on dlm.DataLoaderMapID = dlf.DataLoaderMapID 

GO
GRANT SELECT ON  [dbo].[vDataLoaderFile] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vDataLoaderFile] TO [sp_executeall]
GO
