SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C600_CoverPeriodRiskFactors]
AS

	SELECT	 p.MatterID
			,p.TableRowID
			,CAST([179419] as INT)			[PremiumTableRowID]
			,[179420]						[Species]
			,[179421]						[Breed]
			,[179422]						[Gender]
			,[179423]						[Neutered]
			,CAST([179424] as DATE)			[DateOfBirth]
			,[179425]						[PurchasePrice]
			,[179431]						[PostCode]
			,[179426]						[ViciousTendencies]
			,[179433]						[Rescue]
			,CAST([179427] as DATETIME)		[DateTimeRecorded]
	FROM		
		(	
		SELECT m.MatterID, tr.TableRowID, tdv.DetailFieldID, tdv.DetailValue
		FROM Customers cu WITH ( NOLOCK ) 
		INNER JOIN Lead l WITH ( NOLOCK ) on l.CustomerID = cu.CustomerID 
		INNER JOIN Cases c WITH ( NOLOCK ) on c.LeadID = l.LeadID 
		INNER JOIN Matter m WITH ( NOLOCK ) on m.CaseID = c.CaseID 
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = m.MatterID AND tr.DetailFieldID = 179429 /*Cover Period Risk Factors*/
		INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
		WHERE l.LeadTypeID = 1492 /*Policy Admin*/
		AND tr.ClientID = 433
		AND df.DetailFieldID IN ( 179419,179420,179421,179422,179423,179424,179425,179426,179427,179431,179433 )
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailValue])		
	FOR		
	[DetailFieldID]
		IN ( [179419],[179420],[179421],[179422],[179423],[179424],[179425],[179426],[179427],[179431],[179433] )
	) AS p



GO
GRANT SELECT ON  [dbo].[v_C600_CoverPeriodRiskFactors] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C600_CoverPeriodRiskFactors] TO [sp_executeall]
GO
