SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- View vCustomerQuestionnaires
-- =============================================
CREATE VIEW [dbo].[vCustomerQuestionnaires]  
AS
SELECT c.CustomerID,
c.ClientID, 
cuq.CustomerQuestionnaireID, 
cuq.SubmissionDate, 
cuq.TrackingID, 
cuq.Referrer, 
cuq.SearchTerms, 
clq.ClientQuestionnaireID, 
clq.QuestionnaireTitle, 
tr.TrackingName
FROM dbo.Customers c WITH (NOLOCK) 
LEFT JOIN dbo.CustomerQuestionnaires cuq WITH (NOLOCK) ON cuq.CustomerID = c.CustomerID 
LEFT JOIN dbo.ClientQuestionnaires clq WITH (NOLOCK) ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID 
LEFT JOIN dbo.Tracking tr WITH (NOLOCK) ON tr.TrackingID = cuq.TrackingID

GO
GRANT SELECT ON  [dbo].[vCustomerQuestionnaires] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCustomerQuestionnaires] TO [sp_executeall]
GO
