SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[v_C600_QuotesAndSales]
AS

	SELECT
		  qv.Species
		 ,qv.PetName
		 ,qv.PetDoB
		 ,DATEDIFF(MONTH,qv.PetDoB,qv.QuoteDate) [PetAge]
		 ,qv.QuoteXml.value('(//StartDate)[1]','DATETIME') [CoverStartDate]
		 ,qv.Breed
		 ,'' BreedCode
		 ,'' [FirstParentBreed]
		 ,'' [SecondParentBreed]
		 ,UPPER(qv.Gender) [Gender]
		 ,qv.Postcode [PetPostcode]
		 ,NULLIF(qv.QuoteXml.value('(GetQuoteValuesRequest/CustomerInfo/DateOfBirth)[1]','DATE'),'1900-01-01') [ApplicantDoB]
		 ,'' /*qv.Postcode*/ [ApplicantPostcode]
		 ,NULLIF(qv.QuoteXml.value('(//CustomerId)[1]','INT'),0) [ExistingCustomerID]
		 ,CASE WHEN qv.IsNeutered = 'Y' THEN 'Yes' ELSE 'No' END [IsNeutered]
		 ,CASE WHEN qv.PetXml.value('(//HasMicrochip)[1]','VARCHAR(5)') = 'True' THEN 'Yes' ELSE 'No' END [HasMicrochip]
		 ,qv.PetPrice
		 ,CASE WHEN uqp.ViciousBit = 1 THEN 'Yes' ELSE 'No' END [Vicious]
		 ,CASE WHEN uqp.WorkingDogBit = 1 THEN 'Yes' ELSE 'No' END [WorkingDog]
		 ,CASE WHEN uqp.RescueBit = 1 THEN 'Yes' ELSE 'No' END [Rescue]
		 ,'' [Distributor]
		 ,'' [Channel]
		 ,qv.QuoteSessionID
		 ,qv.QuoteDate
		 ,'' [CommissionCode]
		 ,'' [DistributorProfile]
		 ,'' [BranchCode]
		 ,qv.ResponseXml.value('(//Excess/Selected)[1]','[MONEY]') [Excess]
		 ,CASE WHEN uqp.ViciousBit = 1 THEN 'No' ELSE 'Yes' END [TPLIncluded]
		 ,CASE WHEN uqp.PreExistingBit = 1 THEN 'Yes' ELSE 'No' END [UnderwritingDecision]
		 ,CASE WHEN qs.BuyDate is NULL THEN 'No' ELSE 'Yes' END [Purchased]
		 ,CAST(qs.BuyDate as DATE) [PurchaseDate]
		 ,qs.MatterIDs [PolicyNumbers]
		 ,qs.CustomerID
		 ,CONVERT(VARCHAR,qs.BuyDate,14) [PurchaseTime]
		 --,qv.ResponseXml.value('(//ProductId)[1]','INT') [ProductID]
		 ,qv.ResponseXml.value('(//ProductName)[1]','VARCHAR(2000)') [ProductName]
		 ,qv.ResponseXml.value('(//AnnualPremium)[1]','DECIMAL(18,2)') [AnnualPremium]
		 ,qv.ResponseXml.value('(//Discount)[1]','DECIMAL(18,2)') [DiscountMonths]
		 ,qv.ResponseXml.value('(//PremiumLessDiscount)[1]','DECIMAL(18,2)') [PremiumAfterDiscount]
		 --,qv.PetXml
		 --,qv.QuoteXml
		 --,qv.ResponseXml
		 ,qv.QuoteXml.value('(//ClientPersonnelId)[1]','INT') [ClientPersonnelID]
	FROM _C600_QuoteValues qv WITH ( NOLOCK )
	INNER JOIN _C600_QuoteSessions qs WITH ( NOLOCK ) on qs.QuoteSessionID = qv.QuoteSessionID
	OUTER APPLY dbo.fn_C600_PetXml_UnderwritingQuestionsPivot(qv.PetXml) uqp
	WHERE qv.QuoteDate > '2017-10-02'


GO
GRANT SELECT ON  [dbo].[v_C600_QuotesAndSales] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C600_QuotesAndSales] TO [sp_executeall]
GO
