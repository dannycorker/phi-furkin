SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C321_ClaimPayments]
AS

/* Payment rows */
WITH Payment AS (
SELECT 
	r.MatterID
	, tAmount.TableRowID PaymentRowID
	, tPayee.DetailValue Payee 
	, ISNULL(tAmount.ValueMoney, 0) * CASE tRowType.ValueInt WHEN 0 THEN 1 ELSE -1 END PaidAmount
	, tPaymentCreated.ValueDate CreatedDate
	, tPaymentApproved.ValueDate ApprovedDate
	, tPayRunDate.ValueDate PaymentRunDate
	, tPaymentResponse.ValueDate PaymentResponseDate
	, tPaymentStopped.ValueDate PaymentStoppedDate
	, tPaymentRun.ValueInt PaymentRunNumber
	, tChequeNumber.DetailValue ChequeNumber
	, CASE tRowType.ValueInt WHEN 0 THEN 'Payment' WHEN 1 THEN 'Refund' WHEN 2 THEN 'Cancellation' ELSE '?' END PaymentType
FROM 
	TableRows r WITH (NOLOCK)
	LEFT JOIN dbo.TableDetailValues tPayee WITH (NOLOCK) ON tPayee.TableRowID = r.TableRowID and tPayee.DetailFieldID = 154490
	INNER JOIN dbo.TableDetailValues tRowType WITH (NOLOCK) ON tRowType.TableRowID = r.TableRowID and tRowType.DetailFieldID = 154517
	INNER JOIN dbo.TableDetailValues tAmount WITH (NOLOCK) ON tAmount.TableRowID = r.TableRowID and tAmount.DetailFieldID = 154518
	LEFT JOIN dbo.TableDetailValues tPaymentCreated WITH (NOLOCK) ON tPaymentCreated.TableRowID = r.TableRowID and tPaymentCreated.DetailFieldID = 158479
	LEFT JOIN dbo.TableDetailValues tPaymentApproved WITH (NOLOCK) ON tPaymentApproved.TableRowID = r.TableRowID and tPaymentApproved.DetailFieldID = 159407
	LEFT JOIN dbo.TableDetailValues tPayRunDate WITH (NOLOCK) ON tPayRunDate.TableRowID = r.TableRowID and tPayRunDate.DetailFieldID = 154521
	LEFT JOIN dbo.TableDetailValues tPaymentResponse WITH (NOLOCK) ON tPaymentResponse.TableRowID = r.TableRowID and tPaymentResponse.DetailFieldID = 158539
	LEFT JOIN dbo.TableDetailValues tPaymentStopped WITH (NOLOCK) ON tPaymentStopped.TableRowID = r.TableRowID and tPaymentStopped.DetailFieldID = 159289
	LEFT JOIN dbo.TableDetailValues tPaymentRun WITH (NOLOCK) ON tPaymentRun.TableRowID = r.TableRowID and tPaymentRun.DetailFieldID = 162637
	LEFT JOIN dbo.TableDetailValues tChequeNumber WITH (NOLOCK) ON tChequeNumber.TableRowID = r.TableRowID and tChequeNumber.DetailFieldID = 162638
WHERE 
	r.DetailFieldID = 154485
	--AND tPayRunDate.ValueDate IS Not NULL -- Paid
)

/* View output */
SELECT
	m.ClientID
	,py.* 
	, CASE WHEN py.PaidAmount > 0 THEN 'Cheque' ELSE '' END PaymentMethod
FROM 
	Matter m WITH (NOLOCK)
	INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID
	LEFT JOIN Payment py ON py.MatterID = m.MatterID
WHERE 
	cu.Test = 0




GO
GRANT SELECT ON  [dbo].[v_C321_ClaimPayments] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C321_ClaimPayments] TO [sp_executeall]
GO
