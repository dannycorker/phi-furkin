SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- View vLeadDetailValues
-- =============================================
CREATE VIEW [dbo].[vLeadDetailValues]  
AS
/* All Regular fields for this Lead, whether or not the detail value actually exists */
SELECT ldv1.LeadDetailValueID, l.ClientID, l.LeadID, df1.DetailFieldID, df1.FieldName, df1.FieldCaption, qt1.Description as FieldFormat, IsNull(ldv1.DetailValue, '') as [DetailValue], ldv1.DetailValue as [RawValue], ldv1.ValueInt, ldv1.ValueMoney, ldv1.ValueDate, ldv1.ValueDateTime 
FROM dbo.Lead l WITH (NOLOCK) 
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 1 AND df1.Enabled = 1 -- Get all enabled fields for this Lead Type
INNER JOIN dbo.QuestionTypes AS qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID -- Get the field format (for info only)
LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.LeadID = l.LeadID AND ldv1.DetailFieldID = df1.DetailFieldID -- Finally get the value, null if not present
WHERE (qt1.QuestionTypeID <> 4) -- 4 = LookupList, 14 = ResourceList, 16 = Table
--WHERE (qt1.QuestionTypeID NOT IN (4, 14, 16)) -- 4 = LookupList, 14 = ResourceList, 16 = Table

UNION

/* LookupList values for this Lead, whether or not the detail value actually exists */
SELECT ldv1.LeadDetailValueID, l.ClientID, l.LeadID, df1.DetailFieldID, df1.FieldName, df1.FieldCaption, qt1.Description as FieldFormat, IsNull(luli1.ItemValue, '') AS [DetailValue], ldv1.DetailValue as [RawValue], luli1.ValueInt, luli1.ValueMoney, luli1.ValueDate, luli1.ValueDateTime  
FROM dbo.Lead l WITH (NOLOCK) 
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 1 AND df1.Enabled = 1 -- Get all enabled fields for this Lead Type
INNER JOIN dbo.QuestionTypes AS qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID -- Get the field format (for info only)
LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.LeadID = l.LeadID AND ldv1.DetailFieldID = df1.DetailFieldID -- Get the value, null if not present
LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt -- Finally, look up the value 
WHERE (qt1.QuestionTypeID = 4) -- 4 = LookupList



GO
GRANT SELECT ON  [dbo].[vLeadDetailValues] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadDetailValues] TO [sp_executeall]
GO
