SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDetailValues]
AS
SELECT  MatterDetailValueID as DetailValueID,
		ClientID,
		LeadID,
		MatterID,
		DetailFieldID,
		DetailValue,
		ErrorMsg,
		OriginalDetailValueID, 
		ValueInt, 
		ValueMoney, 
		ValueDate, 
		ValueDateTime 
FROM    MatterDetailValues (nolock) 
UNION ALL
SELECT  LeadDetailValueID as DetailValueID,
		ClientID,
		LeadID,
		NULL as MatterID,
		DetailFieldID,
		DetailValue,
		ErrorMsg,
		OriginalDetailValueID, 
		ValueInt, 
		ValueMoney, 
		ValueDate, 
		ValueDateTime 
FROM    LeadDetailValues (nolock) 


GO
GRANT SELECT ON  [dbo].[vDetailValues] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vDetailValues] TO [sp_executeall]
GO
