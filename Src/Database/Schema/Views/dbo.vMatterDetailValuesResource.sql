SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- View vMatterDetailValuesResource
-- =============================================
CREATE VIEW [dbo].[vMatterDetailValuesResource]  
AS
/* Resource Lists only */
SELECT mdv1.MatterDetailValueID, m.ClientID, m.LeadID, m.MatterID, rldv1.ResourceListID, df2.DetailFieldID, df2.FieldName, df2.FieldCaption, qt2.Description as FieldFormat, COALESCE(luli1.ItemValue, rldv1.DetailValue, '') AS [DetailValue], mdv1.DetailValue as [RawValue], rldv1.ValueInt, rldv1.ValueMoney, rldv1.ValueDate, rldv1.ValueDateTime, df1.DetailFieldID as [MatterDetailFieldID], df1.FieldName as [MatterDetailFieldName], df1.FieldCaption as [MatterDetailFieldCaption] 
FROM dbo.Matter m WITH (NOLOCK) 
INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID -- Join to Lead to get the Lead Type 
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 2 AND df1.Enabled = 1 -- Get all enabled fields for this Lead Type
LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = df1.DetailFieldID -- Get the value, null if not present
LEFT JOIN dbo.DetailFields AS df2 WITH (NOLOCK) ON df2.DetailFieldPageID = df1.ResourceListDetailFieldPageID AND df2.Enabled = 1 -- Get all enabled fields for this Lead Type
LEFT JOIN dbo.ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.ResourceListID = mdv1.ValueInt AND df2.DetailFieldID = rldv1.DetailFieldID  
LEFT JOIN dbo.QuestionTypes AS qt2 WITH (NOLOCK) ON qt2.QuestionTypeID = df2.QuestionTypeID -- Get the field format (for info only)
LEFT JOIN dbo.LookupListItems luli1 WITH (NOLOCK) ON luli1.LookupListID = df2.LookupListID AND luli1.LookupListItemID = rldv1.ValueInt 
WHERE df1.QuestionTypeID = 14 

GO
GRANT SELECT ON  [dbo].[vMatterDetailValuesResource] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vMatterDetailValuesResource] TO [sp_executeall]
GO
