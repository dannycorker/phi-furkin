SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vLeadEventFull
-- =============================================
CREATE VIEW [dbo].[vLeadEventFull]  
AS
SELECT le.LeadEventID, 
le.ClientID, 
le.LeadID, 
le.WhenCreated, 
le.WhoCreated, 
le.Cost, 
le.Comments, 
le.EventTypeID, 
le.NoteTypeID, 
le.FollowupDateTime, 
le.WhenFollowedUp, 
le.AquariumEventType, 
le.NextEventID, 
le.CaseID, 
le.LeadDocumentID, 
le.NotePriority, 
le.DocumentQueueID, 
le.EventDeleted, 
le.WhoDeleted, 
le.DeletionComments, 
le.ContactID, 
et.EventTypeName, 
et.EventTypeDescription, 
et.Enabled,  
et.StatusAfterEvent, 
et.AquariumEventAfterEvent, 
et.AquariumEventSubtypeID, 
et.EventSubtypeID, 
et.DocumentTypeID, 
et.LeadTypeID, 
et.AllowCustomTimeUnits, 
et.InProcess, 
et.KeyEvent, 
est.EventSubtypeName, 
ls.StatusName as [ClientStatus],  
aet.AquariumEventTypeName, 
aest.AquariumEventSubtypeName,
ast.AquariumStatusName, 
dt.DocumentTypeName, 
nt.NoteTypeName, 
convert(char(10), le.WhenCreated, 126) as WhenCreatedDateOnly, 
datediff(dd, le.WhenCreated, dbo.fn_GetDate_Local()) as AgeInDays, 
cp.UserName 
FROM dbo.LeadEvent le WITH (NOLOCK) 
INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated 
INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID 
INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID 
LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = et.StatusAfterEvent 
LEFT JOIN dbo.AquariumEventType aet WITH (NOLOCK) ON aet.AquariumEventTypeID = et.AquariumEventAfterEvent 
LEFT JOIN dbo.AquariumEventSubtype aest WITH (NOLOCK) ON aest.AquariumEventSubtypeID = et.AquariumEventSubtypeID 
LEFT JOIN dbo.AquariumStatus ast WITH (NOLOCK) ON ast.AquariumStatusID = aet.AquariumStatusAfterEvent 
LEFT JOIN dbo.DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = et.DocumentTypeID
LEFT JOIN dbo.NoteType nt WITH (NOLOCK) ON nt.NoteTypeID = le.NoteTypeID 
WHERE le.eventdeleted = 0
GO
GRANT SELECT ON  [dbo].[vLeadEventFull] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadEventFull] TO [sp_executeall]
GO
