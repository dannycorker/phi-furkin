SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- View vWrittenPremium 
-- View of written Premium from the Premium Detail History Table 
-- 2018-03-15 GPR Added PremiumCalculationID (DF: 175709)
-- 2018-08-30 GPR Added Commission_Value and Transaction_Commission_Value
-- 2019-10-15 CPS for JIRA LPC-37	| Added QuotePetProductCheckpoint join
--									| Sorted hard-coded adjustment types
--									| Updated IPT to read from the table
--									| Removed in-view calculations because these will be slow AF
-- 2020-01-12 GPR added UW commission value
-- =============================================
CREATE VIEW [dbo].[vWrittenPremium]  
AS

	SELECT	 ClientID
			,CustomerID
			,LeadID
			,CaseID
			,MatterID 
			,LeadRef																						as [PolicyNumber]
			,TableRowID																						as [Premium_Detail_History_TableRow]
			,ISNULL([175377],'')																			as [Historical_Policy_TableRows]
			,ISNULL([175398],'')																			as [Adjustment_Type]
			,ISNULL([175398_ID],0)																			as [Adjustment_Type_LuliID]
			,ISNULL([175348],'')																			as [Adjustment_Event]
			,ISNULL([175376],'')																			as [Adjustment_EventTypeID]
			,ISNULL([175722],'')																			as [Adjustment_Status]
			,ISNULL([175722_ID],0)																			as [Adjustment_Status_LuliID]
			,CONVERT(DATETIME,NULLIF([175400],''))															as [Adjustment_Application_Date] /*Date and Time Logged*/
			,CONVERT(DATE	,NULLIF([175396],''))															as [Adjustment_Requested_Date]
			,CONVERT(DATE	,NULLIF([175346],''))															as [Adjustment_Effective_From]
			,CONVERT(DATE	,NULLIF([175347],''))															as [Adjustment_Effective_To]
			,CONVERT(MONEY	,ISNULL([175397],''))															as [Adjustment_Amount]
			,CONVERT(MONEY	,ISNULL([177899],''))															as [Adjustment_AfterDiscount]
			,CONVERT(MONEY	,ISNULL([175397],'')/GrossMultiplier)											as [Adjustment_Amount_Net]
			,CONVERT(MONEY	,CONVERT(MONEY,ISNULL([175397],'')) - CONVERT(MONEY,ISNULL([175397],'')/GrossMultiplier)) as [IPT]
			,CONVERT(MONEY	,ISNULL([177689],''))															as [TPL_Split_Gross]
			,CONVERT(MONEY	,ISNULL([177690],''))															as [TPL_Split_IPT]
			,CONVERT(MONEY	,ISNULL([177688],''))															as [TPL_Split_Net]
			,CONVERT(MONEY	,ISNULL([175397],'')) - CONVERT(MONEY,ISNULL([177689],''))						as [VetFees_Gross]
			,CONVERT(MONEY	,ISNULL([175397],'')/GrossMultiplier) - CONVERT(MONEY,ISNULL([177688],''))		as [VetFees_Net]
			,CONVERT(MONEY	,CONVERT(MONEY,ISNULL([175397],'')) - CONVERT(MONEY,ISNULL([175397],'')/GrossMultiplier)) - CONVERT(MONEY,ISNULL([177690],'')) as [VetFees_IPT]
			,CONVERT(MONEY	,ISNULL([177903],''))															as [Adjustment_Admin_Fee]
			,CONVERT(MONEY	,ISNULL([175349],''))															as [Annual_Premium_After_Adjustment]
			,CONVERT(MONEY	,ISNULL([175350],''))															as [First_Monthly_Gross_Payment]
			,CONVERT(MONEY	,ISNULL([175351],''))															as [Recurring_Monthly_Gross_Payment]
			,ISNULL([175371],'')																			as [Cancellation_Reason]
			,CONVERT(MONEY	,ISNULL([175373],''))															as [Discount_Applied_To_Adjustment]
			,CONVERT(INT	,NULLIF([175709],''))															as [Premium_CalculationID]
			,CONVERT(MONEY	,ISNULL([175375],''))															as [Annualised_IPT]
			,CONVERT(MONEY	,ISNULL([175349],'')/GrossMultiplier)											as [Annual_NET_Premium_After_Adjustment]
			,CONVERT(MONEY	,ISNULL([175349],'')) - CONVERT(MONEY,ISNULL([175349],'')/GrossMultiplier)		as [Annual_IPT_After_Adjustment]
			,CONVERT(MONEY	,ISNULL([180170],''))															as [Commission_Value]
			,CONVERT(MONEY	,ISNULL([180171],''))															as [Transaction_Commission_Value]
			,CONVERT(MONEY	,ISNULL([180292],''))															as [IPT_Rate]
			,CONVERT(INT	,NULLIF([180291],''))															as [QuotePetProductID]
			,CONVERT(MONEY	,ISNULL([180307],''))															as [UW_Commission]
		   FROM	
		(	
		SELECT	 c.ClientID
				,tr.TableRowID
				,CASE WHEN t.N = 2 THEN CONVERT(VARCHAR,li.LookupListItemID) ELSE ISNULL(li.ItemValue,tdv.DetailValue) END [DetailValue]
				,CASE WHEN t.N = 2 THEN CONVERT(VARCHAR,tdv.DetailFieldID) + '_ID' ELSE CONVERT(VARCHAR,tdv.DetailFieldID) END [DetailFieldID]
				,c.CustomerID
				,l.LeadID
				,m.MatterID
				,ca.CaseID
				,l.LeadRef
				,NULLIF(tdvIpt.ValueMoney,0) [GrossMultiplier]
		FROM Customers c					WITH (NOLOCK) 
		INNER JOIN Lead l					WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca					WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter m					WITH (NOLOCK) ON ca.CaseID=m.CaseID 
		INNER JOIN TableRows tr				WITH (NOLOCK) ON tr.MatterID=m.MatterID AND tr.DetailFieldID=175336 /*Premium Detail History*/
		INNER JOIN TableDetailValues tdv	WITH (NOLOCK) ON tr.TableRowID=tdv.TableRowID AND tdv.DetailFieldID IN (175397,175400,175398,175377,177689,177690,175346,175347,175348,175349,175350,175351,175371,175373,175375,175376,175396,175722,177903,177688,175709, 177899, 180170, 180171, 180292,180291, 180307)
		INNER JOIN DetailFields df			WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
		 LEFT JOIN LookupListItems li		WITH (NOLOCK) on li.LookupListID = df.LookupListID and li.LookupListItemID = tdv.ValueInt
		INNER JOIN Tally t					WITH (NOLOCK) on (t.N = 1 or ( t.N <=2 AND li.LookupListItemID is not NULL) ) -- Show two rows for lookup lists so we can give the ID and the value
		 LEFT JOIN TableDetailValues tdvIpt WITH (NOLOCK) on tdvIpt.TableRowID = tr.TableRowID AND tdvIpt.DetailFieldID = 180292 /*IPT Rate*/
		WHERE  c.Test = 0
		)
		AS ToPivot		
	PIVOT		
	(		
		MAX([DetailValue])		
	FOR		
	[DetailFieldID]
		IN ( [175397],[175400],[175398],[175377],[177689],[177690],[175346],[175347],[175348],[175349],[175350],[175351],[175371],[175373],[175375],[175376],[175396],[175722],[177903],[177688], [175709], [177899], [180170], [180171], [180292], [175398_ID], [175722_ID] ,[180291], [180307])
	) AS Pivoted


GO
GRANT SELECT ON  [dbo].[vWrittenPremium] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vWrittenPremium] TO [sp_executeall]
GO
