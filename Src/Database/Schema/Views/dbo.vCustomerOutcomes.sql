SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vCustomerOutcomes
-- =============================================
CREATE VIEW [dbo].[vCustomerOutcomes]  
AS
SELECT c.CustomerID,
c.ClientID, 
cuq.CustomerQuestionnaireID, 
cuq.SubmissionDate, 
clq.ClientQuestionnaireID, 
clq.QuestionnaireTitle, 
o.OutcomeID, 
o.OutcomeName,
o.OutcomeDescription
FROM dbo.Customers c WITH (NOLOCK) 
LEFT JOIN dbo.CustomerQuestionnaires cuq WITH (NOLOCK) ON cuq.CustomerID = c.CustomerID 
LEFT JOIN dbo.ClientQuestionnaires clq WITH (NOLOCK) ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID 
LEFT JOIN dbo.CustomerOutcomes co WITH (NOLOCK) ON co.CustomerID = c.CustomerID AND co.ClientQuestionnaireID = cuq.ClientQuestionnaireID 
LEFT JOIN dbo.Outcomes o WITH (NOLOCK) ON o.OutcomeID = co.OutcomeID


GO
GRANT SELECT ON  [dbo].[vCustomerOutcomes] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCustomerOutcomes] TO [sp_executeall]
GO
