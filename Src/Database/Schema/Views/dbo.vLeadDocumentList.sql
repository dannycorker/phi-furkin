SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* Create a new view with dummy logic to get the docs from different places */
CREATE VIEW [dbo].[vLeadDocumentList] 
AS
	
	/* All current documents */
	SELECT
		ld.LeadDocumentID,
		ld.ClientID,
		ld.LeadID,
		ld.DocumentTypeID,
		ld.LeadDocumentTitle,
		ld.UploadDateTime,
		ld.WhoUploaded,
		COALESCE(fs.DocumentBLOB, CAST(0x AS VARBINARY)) AS DocumentBLOB,
		ld.FileName,
		fs.EmailBLOB,
		ld.DocumentFormat,
		ld.EmailFrom,
		ld.EmailTo,
		ld.CcList,
		ld.BccList,
		ld.ElectronicSignatureDocumentKey,
		ld.Encoding,
		ld.ContentFormat,
		ld.ZipFormat,
		ld.DocumentBlobSize, 
		ld.EmailBlobSize,
		ld.DocumentDatabaseID,
		ld.WhenArchived,
		ld.DocumentTypeVersionID
	FROM dbo.LeadDocument ld WITH (NOLOCK) 
	INNER JOIN dbo.LeadDocumentFS fs WITH (NOLOCK) ON fs.LeadDocumentID = ld.LeadDocumentID 

GO
GRANT SELECT ON  [dbo].[vLeadDocumentList] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadDocumentList] TO [sp_executeall]
GO
