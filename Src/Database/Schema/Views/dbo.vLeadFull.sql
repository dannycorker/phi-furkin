SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vLeadFull
-- =============================================
CREATE VIEW [dbo].[vLeadFull]  
AS

SELECT 
l.LeadID, 
l.ClientID, 
l.LeadRef, 
l.CustomerID, 
l.LeadTypeID, 
l.AquariumStatusID, 
l.ClientStatusID, 
l.BrandNew, 
l.Assigned, 
l.AssignedTo, 
l.AssignedBy, 
l.AssignedDate, 
l.RecalculateEquations, 
l.WhenCreated, 
lt.LeadTypeName, 
lt.LeadTypeDescription, 
lt.MatterDisplayName, 
lt.LastReferenceInteger, 
lt.ReferenceValueFormatID, 
lt.IsBusiness, 
lt.LeadDisplayName, 
lt.CaseDisplayName, 
lt.CustomerDisplayName, 
lt.LeadDetailsTabImageFileName, 
lt.CustomerTabImageFileName, 
lt.UseEventCosts, 
lt.UseEventUOEs, 
lt.UseEventDisbursements, 
lt.UseEventComments, 
cp.ClientPersonnelID, 
cp.ClientOfficeID, 
cp.TitleID, 
cp.FirstName, 
cp.MiddleName, 
cp.LastName, 
cp.JobTitle, 
cp.ClientPersonnelAdminGroupID, 
cp.MobileTelephone, 
cp.HomeTelephone, 
cp.OfficeTelephone, 
cp.OfficeTelephoneExtension, 
cp.EmailAddress, 
cp.ChargeOutRate, 
cp.UserName, 
cp.AttemptedLogins, 
cp.AccountDisabled, 
cp.ManagerID, 
ls.StatusID, 
ls.StatusName, 
ls.StatusDescription, 
ast.AquariumStatusName, 
ast.AquariumStatusDescription, 
ast.Usable, 
col.ClientOfficeLeadID, 
co.BuildingName, 
co.Address1, 
co.Address2, 
co.Town, 
co.County, 
co.Country, 
co.PostCode, 
co.CountryID, 
IsNull(cou.CountryName, '') as CountryName,
IsNull(cou.Alpha3Code, '') as CountryCode,
co.OfficeTelephone as ClientOfficeTelephone, 
co.OfficeTelephoneExtension as ClientOfficeTelephoneExtension 
FROM dbo.Lead l WITH (NOLOCK) 
INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID 
LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = l.AssignedTo 
LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = l.ClientStatusID 
LEFT JOIN dbo.AquariumStatus ast WITH (NOLOCK) ON ast.AquariumStatusID = l.AquariumStatusID 
LEFT JOIN dbo.ClientOfficeLead col WITH (NOLOCK) ON col.LeadID = l.LeadID 
LEFT JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = col.ClientOfficeID 
LEFT JOIN dbo.Country cou WITH (NOLOCK) ON cou.CountryID = co.CountryID 


GO
GRANT SELECT ON  [dbo].[vLeadFull] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadFull] TO [sp_executeall]
GO
