SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vCustomers
-- =============================================
CREATE VIEW [dbo].[vDataLoaderLog]  
AS

	SELECT 
	
	dll.DataLoaderLogID,
	dll.ClientID,
	dll.DataLoaderMapID,
	dll.DataLoaderMapSectionID,
	dll.DataLoaderFileID,
	dll.DataLoaderFieldDefinitionID,
	dll.DataLoaderObjectFieldID,
	dll.DetailFieldID,
	dll.ObjectName,
	dll.RowIndex,
	dll.ColIndex,
	dll.NodeName,
	dll.[Message],
	dll.LogLevel,
	CASE dll.LogLevel WHEN 1 THEN 'Warning' WHEN 2 THEN 'Error' WHEN 3 THEN 'Fatal Error' ELSE 'Information' END as [LogLevelMeaning],  
	dlms.Notes,
	
	dlfd.AllowErrors,
	dlfd.Keyword, 
	dlfd.IsMatchField, 
	
	dlof.FieldName, 
	dlof.HelperText
	
	FROM dbo.DataLoaderLog dll (nolock) 
	INNER JOIN dbo.DataLoaderMapSection dlms (nolock) on dlms.DataLoaderMapSectionID = dll.DataLoaderMapSectionID 
	LEFT JOIN dbo.DataLoaderFieldDefinition dlfd (nolock) on dlfd.DataLoaderFieldDefinitionID = dll.DataLoaderFieldDefinitionID 
	LEFT JOIN dbo.DataLoaderObjectField dlof (nolock) on dlof.DataLoaderObjectFieldID = dll.DataLoaderObjectFieldID 

GO
GRANT SELECT ON  [dbo].[vDataLoaderLog] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vDataLoaderLog] TO [sp_executeall]
GO
