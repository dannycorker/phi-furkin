SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- View vLeadDetailValuesResource
-- =============================================
CREATE VIEW [dbo].[vLeadDetailValuesResource]
AS
/* Resource Lists only */
SELECT ldv1.LeadDetailValueID, l.ClientID, l.LeadID, rldv1.ResourceListID, df2.DetailFieldID, df2.FieldName, df2.FieldCaption, qt2.Description AS FieldFormat, COALESCE (luli1.ItemValue, rldv1.DetailValue, '') AS DetailValue, ldv1.DetailValue AS RawValue, ldv1.ValueInt, ldv1.ValueMoney, ldv1.ValueDate, ldv1.ValueDateTime, df1.DetailFieldID AS MatterDetailFieldID, df1.FieldName AS MatterDetailFieldName, df1.FieldCaption AS MatterDetailFieldCaption, ldv1.DetailFieldID AS LeadDetailFieldID
FROM dbo.Lead AS l WITH (NOLOCK) 
INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.LeadTypeID = l.LeadTypeID AND df1.LeadOrMatter = 1 AND df1.Enabled = 1 
LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.LeadID = l.LeadID AND ldv1.DetailFieldID = df1.DetailFieldID 
LEFT JOIN dbo.DetailFields AS df2 WITH (NOLOCK) ON df2.DetailFieldPageID = df1.ResourceListDetailFieldPageID AND df2.Enabled = 1 
LEFT JOIN dbo.ResourceListDetailValues AS rldv1 WITH (NOLOCK) ON rldv1.ResourceListID = ldv1.ValueInt AND df2.DetailFieldID = rldv1.DetailFieldID 
LEFT JOIN dbo.QuestionTypes AS qt2 WITH (NOLOCK) ON qt2.QuestionTypeID = df2.QuestionTypeID 
LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df2.LookupListID AND luli1.LookupListItemID = rldv1.ValueInt
WHERE df1.QuestionTypeID = 14

GO
GRANT SELECT ON  [dbo].[vLeadDetailValuesResource] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadDetailValuesResource] TO [sp_executeall]
GO
