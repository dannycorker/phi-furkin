SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vLeadFast
-- =============================================
CREATE VIEW [dbo].[vLeadFast]  
AS

SELECT 
l.LeadID, 
l.ClientID, 
l.LeadRef, 
l.CustomerID, 
l.LeadTypeID, 
l.AquariumStatusID, 
l.ClientStatusID, 
l.BrandNew, 
l.Assigned, 
l.AssignedTo, 
l.AssignedBy, 
l.AssignedDate, 
l.RecalculateEquations, 
l.WhenCreated, 
lt.LeadTypeName, 
lt.LeadTypeDescription, 
cp.ClientPersonnelID, 
cp.FirstName, 
cp.MiddleName, 
cp.LastName, 
cp.UserName 
FROM dbo.Lead l WITH (NOLOCK) 
INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID 
LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = l.AssignedTo 


GO
GRANT SELECT ON  [dbo].[vLeadFast] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadFast] TO [sp_executeall]
GO
