SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- View vLeadEventNextEvent
-- =============================================
CREATE VIEW [dbo].[vLeadEventNextEvent]  
AS
SELECT le.LeadEventID, 
le.ClientID, 
le.LeadID, 
le.WhenCreated, 
le.WhoCreated, 
le.EventTypeID, 
le.NoteTypeID, 
le.FollowupDateTime, 
le.WhenFollowedUp, 
le.CaseID, 
le.LeadDocumentID, 
le.DocumentQueueID, 
le.ContactID, 
et.EventTypeName, 
et.EventTypeDescription, 
et.EventSubtypeID, 
et.DocumentTypeID, 
et.LeadTypeID, 
et.InProcess, 
et.KeyEvent, 
cp.UserName, 

nextEvent.LeadEventID as [NextLeadEventID], 
nextEvent.WhenCreated as [NextWhenCreated], 
nextEvent.WhoCreated as [NextWhoCreated], 
nextEvent.EventTypeID as [NextEventTypeID],
nextEvent.NoteTypeID as [NextNoteTypeID], 
nextEvent.FollowupDateTime as [NextFollowupDateTime], 
nextEvent.WhenFollowedUp as [NextWhenFollowedUp], 
nextEvent.LeadDocumentID as [NextLeadDocumentID], 
nextEvent.DocumentQueueID as [NextDocumentQueueID], 
nextEvent.ContactID as [NextContactID], 
nextEventType.EventTypeName as [NextEventTypeName], 
nextEventType.EventTypeDescription as [NextEventTypeDescription], 
nextEventType.EventSubtypeID as [NextEventSubtypeID], 
nextEventType.DocumentTypeID as [NextDocumentTypeID], 
nextEventType.InProcess as [NextInProcess], 
nextEventType.KeyEvent as [NextKeyEvent], 
nextWhoCreated.UserName as [NextUserName], 

nextNextEvent.LeadEventID as [NextNextLeadEventID], 
nextNextEvent.EventTypeID as [NextNextEventTypeID]

FROM dbo.LeadEvent le WITH (NOLOCK) 
INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated 
INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID 

LEFT JOIN dbo.LeadEvent nextEvent WITH (NOLOCK) ON nextEvent.LeadEventID = le.NextEventID AND nextEvent.eventdeleted = 0
LEFT JOIN dbo.EventType nextEventType WITH (NOLOCK) ON nextEventType.EventTypeID = nextEvent.EventTypeID 
LEFT JOIN dbo.ClientPersonnel nextWhoCreated WITH (NOLOCK) ON nextWhoCreated.ClientPersonnelID = nextEvent.WhoCreated 

LEFT JOIN dbo.LeadEvent nextNextEvent WITH (NOLOCK) ON nextNextEvent.LeadEventID = nextEvent.NextEventID AND nextNextEvent.eventdeleted = 0 
LEFT JOIN dbo.EventType nextNextEventType WITH (NOLOCK) ON nextNextEventType.EventTypeID = nextNextEvent.EventTypeID 

WHERE le.eventdeleted = 0

GO
GRANT SELECT ON  [dbo].[vLeadEventNextEvent] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadEventNextEvent] TO [sp_executeall]
GO
