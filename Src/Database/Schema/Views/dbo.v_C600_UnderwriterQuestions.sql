SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[v_C600_UnderwriterQuestions]
AS

	SELECT	 p.LeadID
			,[147276-Customer Comment]	[RescueComment]
			,[147276-Customer Response]	[RescueYesNo]
			,[147280-Customer Comment]	[PreviousConditionsComment]
			,[147280-Customer Response]	[PreviousConditionsYesNo]
			,[147277-Customer Comment]	[WorkingDogComment]
			,[147277-Customer Response]	[WorkingDogYesNo]
			,[147279-Customer Comment]	[TakeDogToWorkComment]
			,[147279-Customer Response]	[TakeDogToWorkYesNo]
			,[149241-Customer Comment]	[ViciousComment]
			,[149241-Customer Response]	[ViciousYesNo]
	FROM		
		(	
		SELECT l.LeadID, CONVERT(VARCHAR,tdvResource.ResourceListID) + '-' + df.FieldName [FieldName], ISNULL(li.ItemValue,tdvResponse.DetailValue) [DetailValue]
		FROM Lead l WITH ( NOLOCK ) 
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.LeadID = l.LeadID AND tr.DetailFieldID = 177128 /*Existing Conditions / Underwriter Questions*/
		INNER JOIN TableDetailValues tdvResource WITH ( NOLOCK ) on tdvResource.TableRowID = tr.TableRowID AND tdvResource.DetailFieldID = 177126 /*Existing Condition*/
		INNER JOIN TableDetailValues tdvResponse WITH ( NOLOCK ) on tdvResponse.TableRowID = tr.TableRowID AND tdvResponse.DetailFieldID IN ( 177305,177306 ) /*Customer Response*/
		LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = tdvResponse.ValueInt AND tdvResponse.DetailFieldID = 177306 /*Customer Response*/
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdvResponse.DetailFieldID
		WHERE l.LeadTypeID = 1492 /*Policy Admin*/
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailValue])		
	FOR		
	[FieldName]
		IN ( [147276-Customer Comment],[147276-Customer Response],[147280-Customer Comment],[147280-Customer Response],[147277-Customer Comment],[147277-Customer Response],[147279-Customer Comment],[147279-Customer Response],[149241-Customer Comment],[149241-Customer Response],[147275-Customer Comment],[147275-Customer Response],[147278-Customer Comment],[147278-Customer Response],[147281-Customer Comment],[147281-Customer Response] )
	) AS p
GO
GRANT SELECT ON  [dbo].[v_C600_UnderwriterQuestions] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C600_UnderwriterQuestions] TO [sp_executeall]
GO
