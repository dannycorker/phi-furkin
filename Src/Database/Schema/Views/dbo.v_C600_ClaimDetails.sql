SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[v_C600_ClaimDetails]
AS

	SELECT	 lPolicy.LeadID			[PolicyLeadID]
			,lPolicy.LeadRef		[PolicyNumber]
			,l.LeadID				[ClaimLeadID]
			,c.CaseID
			,m.MatterID
			,ls.StatusName
			,C.ClientStatusID
			,mdvType.ValueInt						[ClaimTypeID]
			,liType.ItemValue						[ClaimType]
			,liAccIllness.ItemValue					[AccidentOrIllness]
			,mdvAilment.ValueInt					[AilmentResourceListID]
			,rdvAilment.DetailValue					[Ailment]
			,mdvTotalClaimAmount.ValueMoney			[TotalClaimAmount]
			,mdvTotalPaid.ValueMoney				[TotalPaid]
			,mdvDatePaid.ValueDate					[DatePaid]
			,mdvTreatmentStart.ValueDate			[TreatmentStart]
			,mdvTreatmentEnd.ValueDate				[TreatmentEnd]
			,mdvDateOfLoss.ValueDate				[DateofLoss]
			,mdvFormReceived.ValueDate				[ClaimFormReceived]
			,liBodyPart.ItemValue					[BodyPart]
			,NULLIF(rdvCondCode.DetailValue,'0')	[ConditionCode]

			,tdvParen.ValueInt				[ParentClaimID]
			,dbo.fn_C00_GetUrl_CustomerLeadDetailsByObjectID(l.LeadID,1) [Url]

	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID 
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
	INNER JOIN LeadTypeRelationship ltr WITH ( NOLOCK ) on ltr.ToLeadID = l.LeadID AND ltr.ToLeadTypeID = l.LeadTypeID AND ltr.ToMatterID = m.MatterID
	INNER JOIN Lead lPolicy WITH ( NOLOCK ) on lPolicy.LeadID = ltr.FromLeadID AND lPolicy.LeadTypeID = 1492 /*Policy Admin*/
	 LEFT JOIN LeadStatus ls WITH (NOLOCK) on ls.StatusID = c.ClientStatusID
	 LEFT JOIN MatterDetailValues mdvType WITH ( NOLOCK ) on mdvType.MatterID = m.MatterID AND mdvType.DetailFieldID = 144483 /*Claim Type*/
	 LEFT JOIN LookupListItems liType WITH ( NOLOCK ) on liType.LookupListItemID = mdvType.ValueInt
	 LEFT JOIN MatterDetailValues mdvAccIllness WITH ( NOLOCK ) on mdvAccIllness.MatterID = m.MatterID AND mdvAccIllness.DetailFieldID = 175576 /*Accident/Illness*/
	 LEFT JOIN LookupListItems liAccIllness WITH ( NOLOCK ) on liAccIllness.LookupListItemID = mdvAccIllness.ValueInt
	 LEFT JOIN MatterDetailValues mdvAilment WITH ( NOLOCK ) on mdvAilment.MatterID = m.MatterID AND mdvAilment.DetailFieldID = 144504 /*Ailment*/
	 LEFT JOIN ResourceListDetailValues rdvAilment WITH ( NOLOCK ) on rdvAilment.ResourceListID = mdvAilment.ValueInt AND rdvAilment.DetailFieldID = 144340 /*1st Cause*/
	 LEFT JOIN MatterDetailValues mdvTotalClaimAmount WITH ( NOLOCK ) on mdvTotalClaimAmount.MatterID = m.MatterID AND mdvTotalClaimAmount.DetailFieldID = 149850 /*Total Claim Amount*/
	 LEFT JOIN MatterDetailValues mdvTotalPaid WITH ( NOLOCK ) on mdvTotalPaid.MatterID = m.MatterID AND mdvTotalPaid.DetailFieldID = 148375 /*Total Settlement*/
	 LEFT JOIN MatterDetailValues mdvTreatmentStart WITH ( NOLOCK ) on mdvTreatmentStart.MatterID = m.MatterID AND mdvTreatmentStart.DetailFieldID = 144366 /*Treatment Start*/
	 LEFT JOIN MatterDetailValues mdvTreatmentEnd WITH ( NOLOCK ) on mdvTreatmentEnd.MatterID = m.MatterID AND mdvTreatmentEnd.DetailFieldID = 145674 /*Treatment End*/
	 LEFT JOIN MatterDetailValues mdvDateOfLoss WITH ( NOLOCK ) on mdvDateOfLoss.MatterID = m.MatterID AND mdvDateOfLoss.DetailFieldID = 144892 /*Date of Loss*/
	 LEFT JOIN MatterDetailValues mdvFormReceived WITH ( NOLOCK ) on mdvFormReceived.MatterID = m.MatterID AND mdvFormReceived.DetailFieldID = 144520 /*Date claim form received*/
	 LEFT JOIN MatterDetailValues mdvBodyPart WITH ( NOLOCK ) on mdvBodyPart.MatterID = m.MatterID AND mdvBodyPart.DetailFieldID = 144333 /*Body Part*/
	 LEFT JOIN LookupListItems liBodyPart WITH ( NOLOCK ) on liBodyPart.LookupListItemID = mdvBodyPart.ValueInt
	 LEFT JOIN MatterDetailValues mdvDatePaid WITH ( NOLOCK ) on mdvDatePaid.MatterID = m.MatterID AND mdvDatePaid.DetailFieldID = 162644 /*Date payment confirmed*/
	 LEFT JOIN ResourceListDetailValues rdvCondCode WITH ( NOLOCK ) on rdvCondCode.ResourceListID = mdvAilment.ValueInt AND rdvCondCode.DetailFieldID = 162736 /*VeNom Data Dictionary Code*/
	 LEFT JOIN TableDetailValues tdvRelat WITH ( NOLOCK ) on tdvRelat.LeadID = l.LeadID AND tdvRelat.DetailFieldID = 146355 /*Claim ID*/ AND tdvRelat.ValueInt = m.MatterID
	 LEFT JOIN TableDetailValues tdvParen WITH ( NOLOCK ) on tdvParen.TableRowID = tdvRelat.TableRowID AND tdvParen.DetailFieldID = 146356 /*Parent Claim ID*/
	WHERE l.LeadTypeID = 1490 /*Pet Claim*/
	AND l.ClientID = dbo.fnGetPrimaryClientID()

GO
GRANT SELECT ON  [dbo].[v_C600_ClaimDetails] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C600_ClaimDetails] TO [sp_executeall]
GO
