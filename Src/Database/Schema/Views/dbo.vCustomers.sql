SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- View vCustomers
-- =============================================
CREATE VIEW [dbo].[vCustomers]  
AS

SELECT c.CustomerID, 
c.ClientID,
c.IsBusiness,
IsNull(t.Title, '') as Title,
c.FirstName,
c.MiddleName,
c.LastName,
c.EmailAddress,
c.DayTimeTelephoneNumber,
c.HomeTelephone,
c.MobileTelephone,
c.CompanyTelephone,
c.WorksTelephone,
c.Address1,
c.Address2,
c.Town,
c.County,
c.PostCode,
c.CountryID, 
IsNull(cou.CountryName, '') as CountryName,
IsNull(cou.Alpha3Code, '') as CountryCode,
c.AquariumStatusID,
c.CompanyName,
c.Occupation,
c.Employer,
c.Fullname,
c.DoNotEmail,
c.DoNotSellToThirdParty,
c.AgreedToTermsAndConditions,
c.DateOfBirth,
c.DefaultContactID,
c.DefaultOfficeID,
c.AddressVerified,
c.SubClientID,
c.CustomerRef,
IsNull(ast.AquariumStatusName, '') as [AquariumStatusName],
c.Test
FROM dbo.Customers c WITH (NOLOCK) 
LEFT JOIN dbo.Titles t WITH (NOLOCK) ON t.TitleID = c.TitleID 
LEFT JOIN dbo.AquariumStatus ast WITH (NOLOCK) ON ast.AquariumStatusID = c.AquariumStatusID 
LEFT JOIN dbo.Country cou WITH (NOLOCK) ON cou.CountryID = c.CountryID 
WHERE (c.Test = 0 OR c.Test IS NULL) 
AND c.LastName <> 'Test'




GO
GRANT SELECT ON  [dbo].[vCustomers] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCustomers] TO [sp_executeall]
GO
