SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vMatterDetailValuesLookup] 
AS

/* Regular field */
SELECT mdv1.[MatterDetailValueID], mdv1.[ClientID], mdv1.[MatterID], mdv1.[DetailFieldID], mdv1.[DetailValue], mdv1.ValueInt, mdv1.ValueMoney, mdv1.ValueDate, mdv1.ValueDateTime 
FROM [dbo].MatterDetailValues mdv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = mdv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
WHERE qt1.QuestionTypeID NOT IN (4, 14, 16)

UNION

/* Lookup Lists */
SELECT mdv1.[MatterDetailValueID], mdv1.[ClientID], mdv1.[MatterID], mdv1.[DetailFieldID], luli1.ItemValue as [DetailValue], luli1.ValueInt, luli1.ValueMoney, luli1.ValueDate, luli1.ValueDateTime 
FROM [dbo].MatterDetailValues mdv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = mdv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
INNER JOIN [dbo].LookupListItems luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt 
WHERE qt1.QuestionTypeID = 4

UNION

/* Resource Lists */
SELECT mdv1.[MatterDetailValueID], mdv1.[ClientID], mdv1.[MatterID], mdv1.[DetailFieldID], rldv1.DetailValue as [DetailValue], rldv1.ValueInt, rldv1.ValueMoney, rldv1.ValueDate, rldv1.ValueDateTime 
FROM [dbo].MatterDetailValues mdv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = mdv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
INNER JOIN [dbo].ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.DetailFieldID = df1.DetailFieldID AND rldv1.ResourceListID = mdv1.ValueInt 
WHERE qt1.QuestionTypeID = 14

UNION

/* Table Lists */
SELECT mdv1.[MatterDetailValueID], mdv1.[ClientID], mdv1.[MatterID], mdv1.[DetailFieldID], tdv1.DetailValue as [DetailValue], tdv1.ValueInt, tdv1.ValueMoney, tdv1.ValueDate, tdv1.ValueDateTime 
FROM [dbo].MatterDetailValues mdv1 WITH (NOLOCK)
INNER JOIN [dbo].DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = mdv1.DetailFieldID 
INNER JOIN [dbo].QuestionTypes qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID
INNER JOIN [dbo].TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.DetailFieldID = df1.DetailFieldID AND tdv1.TableRowID = mdv1.ValueInt AND tdv1.MatterID = mdv1.MatterID 
WHERE qt1.QuestionTypeID = 16


GO
GRANT SELECT ON  [dbo].[vMatterDetailValuesLookup] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vMatterDetailValuesLookup] TO [sp_executeall]
GO
