SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_C433_ClaimDetailsData]
AS

	SELECT	 p.TableRowID
			,p.MatterID
			,CAST(p.[144349] as DATE)			[TreatmentStart]
			,CAST(p.[144350] as INT)			[PolicySectionResourceListID]
			,liSection.ItemValue				[Section]
			,liSubSection.ItemValue				[SubSection]
			,CAST(p.[144351] as DATE)			[TreatmentEnd]
			,CAST(p.[144352] as DECIMAL(18,2))	[Total]
			,CAST(p.[144353] as DECIMAL(18,2))	[Claim]
			,CAST(p.[145678] as DECIMAL(18,2))	[Settle]
			,CAST(p.[146179] as DECIMAL(18,2))	[UserDeductions]
			,CAST(p.[146406] as DECIMAL(18,2))	[Excess]
			,CAST(p.[146407] as DECIMAL(18,2))	[CoIns]
			,CAST(p.[146408] as DECIMAL(18,2))	[Limit]
			,CAST(p.[147001] as DECIMAL(18,2))	[NoCover]
			,CAST(p.[147434] as DECIMAL(18,2))	[Rebate]
			,CAST(p.[148399] as INT)			[LimitReachedResourceListID]
			,CAST(p.[149778] as INT)			[ClaimRowTypeID]
			,li.ItemValue						[ClaimRowType]
			,CAST(p.[158802] as DECIMAL(18,2))	[VolExcess]
			,CAST(p.[179568] as DECIMAL(18,2))	[RemainingLimit]
			,CAST(p.[179564] as DECIMAL(18,2))	[Coinsurance]
	FROM		
		(
		SELECT   tr.TableRowID
				,tr.MatterID
				,df.DetailFieldID
				,ISNULL( CONVERT(VARCHAR,tdv.ResourceListID) , REPLACE(tdv.DetailValue,',','') ) [DetailValue]
		FROM TableRows tr WITH ( NOLOCK )
		INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
		WHERE tr.DetailFieldID = 144355 /*Claim Details Data*/
		AND tdv.DetailFieldID IN ( 144349,144350,144351,144352,144353,145678,146179,146406,146407,146408,147001,147434,148399,149778,158802,179568,179564 )
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailValue])		
	FOR		
	[DetailFieldID]
		IN ( [144349],[144350],[144351],[144352],[144353],[145678],[146179],[146406],[146407],[146408],[147001],[147434],[148399],[149778],[158802],[179568],[179564] )
	) AS p
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = p.[149778]
	LEFT JOIN ResourceListDetailValues rdvSection WITH ( NOLOCK ) on rdvSection.ResourceListID = p.[144350] AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
	LEFT JOIN LookupListItems liSection WITH ( NOLOCK ) on liSection.LookupListItemID = rdvSection.ValueInt
	LEFT JOIN ResourceListDetailValues rdvSubSection WITH ( NOLOCK ) on rdvSubSection.ResourceListID = rdvSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190 /*Sub-Section*/
	LEFT JOIN LookupListItems liSubSection WITH ( NOLOCK ) on liSubSection.LookupListItemID = rdvSubSection.ValueInt
GO
GRANT SELECT ON  [dbo].[v_C433_ClaimDetailsData] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[v_C433_ClaimDetailsData] TO [sp_executeall]
GO
