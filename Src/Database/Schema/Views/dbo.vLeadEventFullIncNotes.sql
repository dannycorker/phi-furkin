SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- View vLeadEventFullIncNotes
-- =============================================
CREATE VIEW [dbo].[vLeadEventFullIncNotes]  
AS
SELECT le.LeadEventID, 
le.ClientID, 
le.LeadID, 
le.WhenCreated, 
le.WhoCreated, 
le.Cost, 
le.Comments, 
le.EventTypeID, 
le.NoteTypeID, 
le.FollowupDateTime, 
le.WhenFollowedUp, 
le.AquariumEventType, 
le.NextEventID, 
le.CaseID, 
le.LeadDocumentID, 
le.NotePriority, 
le.DocumentQueueID, 
le.EventDeleted, 
le.WhoDeleted, 
le.DeletionComments, 
le.ContactID, 
et.EventTypeName, 
et.EventTypeDescription, 
et.Enabled,  
et.StatusAfterEvent, 
et.AquariumEventAfterEvent, 
et.AquariumEventSubtypeID, 
et.EventSubtypeID, 
et.DocumentTypeID, 
et.LeadTypeID, 
et.AllowCustomTimeUnits, 
et.InProcess, 
et.KeyEvent, 
est.EventSubtypeName, 
ls.StatusName AS [ClientStatus],  
aet.AquariumEventTypeName, 
aest.AquariumEventSubtypeName,
ast.AquariumStatusName, 
dt.DocumentTypeName, 
nt.NoteTypeName, 
CONVERT(CHAR(10), le.WhenCreated, 126) AS WhenCreatedDateOnly, 
DATEDIFF(dd, le.WhenCreated, dbo.fn_GetDate_Local()) AS AgeInDays, 
cp.UserName 
FROM dbo.LeadEvent le WITH (NOLOCK) 
INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated 
LEFT JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID 
LEFT JOIN dbo.EventSubtype est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID 
LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = et.StatusAfterEvent 
LEFT JOIN dbo.AquariumEventType aet WITH (NOLOCK) ON aet.AquariumEventTypeID = et.AquariumEventAfterEvent 
LEFT JOIN dbo.AquariumEventSubtype aest WITH (NOLOCK) ON aest.AquariumEventSubtypeID = et.AquariumEventSubtypeID 
LEFT JOIN dbo.AquariumStatus ast WITH (NOLOCK) ON ast.AquariumStatusID = aet.AquariumStatusAfterEvent 
LEFT JOIN dbo.DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = et.DocumentTypeID
LEFT JOIN dbo.NoteType nt WITH (NOLOCK) ON nt.NoteTypeID = le.NoteTypeID 
WHERE le.EventDeleted = 0
GO
GRANT SELECT ON  [dbo].[vLeadEventFullIncNotes] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vLeadEventFullIncNotes] TO [sp_executeall]
GO
