SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- View vLeadEventFast
-- =============================================
CREATE VIEW [dbo].[vCaseWithLatestNonNoteEvent] 
AS

SELECT 
ca.CaseID,
ca.LeadID,
ca.ClientID,
ca.CaseNum,
ca.CaseRef,
ca.ClientStatusID,
ca.AquariumStatusID,
ca.DefaultContactID,
ca.LatestLeadEventID,
ca.LatestInProcessLeadEventID,
ca.LatestOutOfProcessLeadEventID,
ca.LatestNonNoteLeadEventID,
ca.LatestNoteLeadEventID,
le.WhenCreated, 
le.WhoCreated, 
le.FollowupDateTime,
le.WhenFollowedUp, 
le.Cost, 
le.Comments, 
le.EventTypeID, 
le.NoteTypeID, 
convert(char(10), le.WhenCreated, 126) as WhenCreatedDateOnly, 
datediff(dd, le.WhenCreated, dbo.fn_GetDate_Local()) as AgeInDays, 
cp.UserName, 
et.EventTypeName, 
ls.StatusName  
FROM dbo.Cases ca WITH (NOLOCK) 
LEFT JOIN dbo.LeadEvent le WITH (NOLOCK) on le.LeadEventID = ca.LatestNonNoteLeadEventID 
LEFT JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID 
LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) on ls.StatusID = ca.ClientStatusID 
LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated
GO
GRANT SELECT ON  [dbo].[vCaseWithLatestNonNoteEvent] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[vCaseWithLatestNonNoteEvent] TO [sp_executeall]
GO
