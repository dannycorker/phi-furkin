SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-30
-- Description:	Cancel all Group Rights etc from temp/work table
--              By calling this without any mention of @UserID, this has
--              the added bonus of clearing out all User edits left open
--              overnight when this is called by the housekeeping batch job.
-- =============================================
CREATE PROCEDURE [dbo].[CancelGroupRightsForEditing]
	@GroupID int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE GroupRightsDynamicEditing 
	WHERE ClientPersonnelAdminGroupID = @GroupID

	DELETE GroupFunctionControlEditing
	WHERE ClientPersonnelAdminGroupID = @GroupID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CancelGroupRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CancelGroupRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CancelGroupRightsForEditing] TO [sp_executeall]
GO
