SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-24
-- Description:	Set SqlQuery Editing in progress mode
--				Pass 1 to begin editing, 2 to commit, 3 to cancel
-- JWG 2012-08-09 Pass new LockAllTables flag to the editing record
-- =============================================
CREATE PROCEDURE [dbo].[SqlQueryEditingSetInProgress]
	@WhatToDo int, /* 1 = begin editing, 2 = commit changes, 3 = cancel changes */
	@WhoIsEditing int,
	@SqlQueryID int = NULL,
	@SqlQueryEditingID int = NULL,
	@FolderID int = NULL,
	@Action varchar(20) = 'Add', /* Add, Edit, Copy or Run (view-only) */
	@NewSqlQueryID int = NULL,
	@ParentQueryID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

    -- Use the @WhatToDo flag to determine which branch to follow
	IF @WhatToDo = 1
	BEGIN

		-- Set EditInProgress
		IF @Action = 'Add'
		BEGIN
			-- Create new query from scratch
			INSERT INTO SqlQueryEditing(ClientID, WhoIsEditing, EditStartedAt, FolderID, ParentQueryID)
			SELECT ClientID, @WhoIsEditing, dbo.fn_GetDate_Local(), @FolderID, @ParentQueryID
			FROM ClientPersonnel 
			WHERE ClientPersonnel.ClientPersonnelID = @WhoIsEditing

			SELECT @SqlQueryEditingID = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN
			-- Edit/Copy or Run
			-- If this is an existing query, as opposed to a brand new one,
			-- then copy across all the existing details
			INSERT INTO SqlQueryEditing(ClientID, SqlQueryID, WhoIsEditing, EditStartedAt, QueryText, QueryTitle, LeadTypeID, FolderID, ParentQueryID, LockAllTables)
			SELECT ClientID, CASE WHEN @Action = 'Run' THEN NULL ELSE @SqlQueryID END, @WhoIsEditing, dbo.fn_GetDate_Local(), QueryText, QueryTitle, LeadTypeID, FolderID, ParentQueryID, LockAllTables 
			FROM SqlQuery 
			WHERE QueryID = @SqlQueryID

			SELECT @SqlQueryEditingID = SCOPE_IDENTITY()

			-- Set up all the editing tables with all the mapped IDs etc
			EXEC CopySqlQueryForEditing @SqlQueryID, @SqlQueryEditingID
		END

		-- Return the new EditingID to the calling program
		SELECT @SqlQueryEditingID

	END
	ELSE
	BEGIN

		IF @WhatToDo = 2
		BEGIN
		    -- Commit work in progress and clear flags
			EXEC CommitSqlQueryForEditing @SqlQueryID, @SqlQueryEditingID, @NewSqlQueryID
		END
		ELSE
		BEGIN
		    -- Cancel work in progress and clear flags
			EXEC CancelSqlQueryForEditing @SqlQueryID, @SqlQueryEditingID
		END
	END

	SELECT @SqlQueryEditingID as [SqlQueryEditingID]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingSetInProgress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingSetInProgress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingSetInProgress] TO [sp_executeall]
GO
