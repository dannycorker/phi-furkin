SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptPositionTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_GetByScriptPositionTargetID]
(

	@ScriptPositionTargetID int   
)
AS


				SELECT
					[ScriptPositionTargetID],
					[ScriptID],
					[SectionID],
					[ClientID],
					[PositionName],
					[LinkText]
				FROM
					[dbo].[ScriptPositionTarget] WITH (NOLOCK) 
				WHERE
										[ScriptPositionTargetID] = @ScriptPositionTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_GetByScriptPositionTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_GetByScriptPositionTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_GetByScriptPositionTargetID] TO [sp_executeall]
GO
