SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-09-29
-- Description:	CustomViewRouting_Update
-- =============================================
CREATE PROCEDURE [dbo].[CustomViewRouting_Update]
	@CustomViewRouteID int,
	@ClientID int,
	@DetailFieldSubTypeID int,
	@LeadTypeID int,
	@PageID int,
	@FieldID int,
	@MapsToFieldID int,
	@MapsToClientID int
AS
BEGIN

	SET NOCOUNT ON;

UPDATE TOP (1) [CustomViewRouting]
SET [ClientID] = @ClientID
,[DetailFieldSubTypeID] = @DetailFieldSubTypeID
,[LeadTypeID] = @LeadTypeID
,[PageID] = @PageID
,[FieldID] = @FieldID
,[MapsToFieldID] = @MapsToFieldID
,[MapsToClientID] = @MapsToClientID
FROM [CustomViewRouting]
WHERE CustomViewRouteID = @CustomViewRouteID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[CustomViewRouting_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomViewRouting_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomViewRouting_Update] TO [sp_executeall]
GO
