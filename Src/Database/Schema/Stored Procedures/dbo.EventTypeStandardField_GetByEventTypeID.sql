SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeStandardField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_GetByEventTypeID]
(

	@EventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeStandardFieldID],
					[EventTypeID],
					[DataLoaderObjectFieldID],
					[ClientID],
					[Mandatory]
				FROM
					[dbo].[EventTypeStandardField] WITH (NOLOCK) 
				WHERE
					[EventTypeID] = @EventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_GetByEventTypeID] TO [sp_executeall]
GO
