SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryGrouping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryGrouping_GetBySqlQueryID]
(

	@SqlQueryID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryGroupingID],
					[ClientID],
					[SqlQueryID],
					[GroupByClause],
					[HavingClause],
					[HavingColumnID],
					[HavingCriteria1],
					[HavingCriteria2],
					[TempHavingColumnID],
					[TempHavingID],
					[TempGroupingID]
				FROM
					[dbo].[SqlQueryGrouping] WITH (NOLOCK) 
				WHERE
					[SqlQueryID] = @SqlQueryID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_GetBySqlQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryGrouping_GetBySqlQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_GetBySqlQueryID] TO [sp_executeall]
GO
