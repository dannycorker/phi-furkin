SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-01-25
-- Description:	Add an asset to an irish insolvency customer
-- =============================================
CREATE PROCEDURE [dbo].[IISSA_XML_AddAsset]

	 @CustomerID		INT
	,@AssetTypeName		VARCHAR(100)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @TableRowID				INT
			,@ClientID					INT
			,@LeadTypeID				INT = 10
			,@ThirdPartyFieldGroupID	INT = 69
			
	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH ( NOLOCK )
	WHERE cu.CustomerID = @CustomerID
			
	DECLARE  @TypeToFieldMapping		TABLE	 ( AssetTypeName VARCHAR(2000), TableDetailFieldID INT )
	INSERT	 @TypeToFieldMapping		VALUES	 ( 'Principal Private Residence'		, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 956 ) )
												,( 'Investment Property'				, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 969 ) )
												,( 'Plant, Equipment, Tools'			, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 986 ) )
												,( 'Vehicles'							, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 996 ) )
												,( 'Stock in trade'						, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1012) )
												,( 'Money owed to you'					, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1017) )
												,( 'Bank or building society accounts'	, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1028) )
												,( 'Credit Union Shares/Investments'	, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1042) )
												,( 'Cash on hand'						, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1055) )
												,( 'Prospective Assets'					, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1060) )
												,( 'Other'								, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 1074) )
	

	/*
	Find the appropriate detail field
	Create a table row and pick up its ID
	find appropriate column mappings
	populate the table row based on passed in values
	*/
	
	/*
	TODO.  Complete third party mapping
	*/
	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[IISSA_XML_AddAsset] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IISSA_XML_AddAsset] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IISSA_XML_AddAsset] TO [sp_executeall]
GO
