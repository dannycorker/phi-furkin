SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentGateway_Insert]
(

	@PaymentGatewayID int    OUTPUT,

	@Gateway varchar (100)  ,

	@Name varchar (100)  ,

	@Description varchar (300)  ,

	@Url varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[PaymentGateway]
					(
					[Gateway]
					,[Name]
					,[Description]
					,[Url]
					)
				VALUES
					(
					@Gateway
					,@Name
					,@Description
					,@Url
					)
				-- Get the identity value
				SET @PaymentGatewayID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Insert] TO [sp_executeall]
GO
