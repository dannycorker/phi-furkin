SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PanelItemTypes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemTypes_Find]
(

	@SearchUsingOR bit   = null ,

	@PanelItemTypeID int   = null ,

	@PanelItemTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PanelItemTypeID]
	, [PanelItemTypeName]
    FROM
	[dbo].[PanelItemTypes] WITH (NOLOCK) 
    WHERE 
	 ([PanelItemTypeID] = @PanelItemTypeID OR @PanelItemTypeID IS NULL)
	AND ([PanelItemTypeName] = @PanelItemTypeName OR @PanelItemTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PanelItemTypeID]
	, [PanelItemTypeName]
    FROM
	[dbo].[PanelItemTypes] WITH (NOLOCK) 
    WHERE 
	 ([PanelItemTypeID] = @PanelItemTypeID AND @PanelItemTypeID is not null)
	OR ([PanelItemTypeName] = @PanelItemTypeName AND @PanelItemTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemTypes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Find] TO [sp_executeall]
GO
