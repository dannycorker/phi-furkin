SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-30
-- Description:	Cancel all User Rights etc from temp/work table
-- =============================================
CREATE PROCEDURE [dbo].[CancelUserRightsForEditing]
	-- Add the parameters for the stored procedure here
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE UserRightsDynamicEditing 
	WHERE ClientPersonnelID = @UserID

	DELETE UserFunctionControlEditing
	WHERE ClientPersonnelID = @UserID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[CancelUserRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CancelUserRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CancelUserRightsForEditing] TO [sp_executeall]
GO
