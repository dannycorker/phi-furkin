SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateClientAreaMailingList]

@ClientAreaMailingListID int,
@Name nvarchar(200), 
@Email nvarchar(255),
@PostCode nvarchar(10),
@OutcomeID int, 
@YellowPagesAreaCode nvarchar(10),
@ClientID int,
@ClientQuestionnaireID int,
@OnHold bit

as

Update ClientAreaMailingLists
SET ClientAreaMailingLists.Name = @Name,
Email=@Email,
PostCode=@PostCode,
OutcomeID = @OutcomeID,
YellowPagesAreaCode = @YellowPagesAreaCode,
ClientID = @ClientID,
ClientQuestionnaireID = @ClientQuestionnaireID,
OnHold = @OnHold
where ClientAreaMailingListID = @ClientAreaMailingListID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientAreaMailingList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientAreaMailingList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientAreaMailingList] TO [sp_executeall]
GO
