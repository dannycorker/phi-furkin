SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- ALTER date: 2011-03-03
-- Description:	PAF address lookup (UK)
-- =============================================
CREATE PROCEDURE [dbo].[GetAddressList]
	@ClientID INT = NULL,
	@ClientPersonnelID INT = NULL,
	@ClientQuestionnaireID INT = NULL,
	@AquariumCountryID INT = 232,		/* 232=UK by default */
	@ModuleID INT = 3,					/* eCatcher by default */
	@AddressLine1 VARCHAR(50) = NULL,
	@PostCode VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Call the common proc on AquariusMaster database.
		This returns a result set to the calling app, and also a success or failure code
		which we can then use to decide whether or not to charge the client for the lookup.
	*/
	DECLARE @LookupSuccess INT = -1, 
	@SourceAquariumOptionID INT = 51,		/* Address Lookup */
	@TypeOfLogEntry VARCHAR(6),
	@LogMessage VARCHAR(100)
	
	/* Get ClientID from any available source if it is not present */
	IF @ClientID IS NULL AND @ClientPersonnelID IS NOT NULL
	BEGIN
		SELECT @ClientID = c.ClientID 
		FROM dbo.ClientPersonnel c WITH (NOLOCK) 
		WHERE c.ClientPersonnelID = @ClientPersonnelID 
	END
	IF @ClientID IS NULL AND @ClientPersonnelID IS NOT NULL
	BEGIN
		SELECT @ClientID = c.ClientID 
		FROM dbo.ClientQuestionnaires c WITH (NOLOCK) 
		WHERE c.ClientQuestionnaireID = @ClientQuestionnaireID 
	END
	
	/* Must have ClientID to perform the lookup */
	IF @ClientID IS NOT NULL
	BEGIN
		
		/* Attempt the address lookup on the AquariusMaster database */
		EXEC @LookupSuccess = AquariusMaster.dbo.GetAddressList @AquariumCountryID, @AddressLine1, @PostCode
		
		/* Charge the client one click if the call succeeded */
		IF @LookupSuccess = 1
		BEGIN
		
			/* Charge the appropriate client for the lookup (calculates the charge automatically) */
			EXEC dbo.ClientBillingDetail__Insert @ClientID, @SourceAquariumOptionID, @ModuleID, NULL, @ClientPersonnelID 
			
			SELECT @TypeOfLogEntry = 'Info', @LogMessage = 'Success'
			
		END
		ELSE
		BEGIN
			IF @LookupSuccess = 0
			BEGIN
				
				/* No charge, just prepare to log the failure */
				SELECT @TypeOfLogEntry = 'Error', @LogMessage = 'Invalid address (no charge)'
				
			END
			ELSE
			BEGIN
				
				/* No charge, just prepare to log the failure */
				SELECT @TypeOfLogEntry = 'Error', @LogMessage = 'Server error (no charge)'
				
			END
		END
	END
	ELSE
	BEGIN
		
		/* No charge, just prepare to log the failure */
		SELECT @TypeOfLogEntry = 'Error', @LogMessage = 'No ClientID available! Refused (no charge)'
		
	END
	
	SELECT @LogMessage += ' for lookup Line1: ' + COALESCE(@AddressLine1, '') + ' Postcode: ' + @PostCode
	
	/* Log the outcome */
	EXEC dbo._C00_Logit @TypeOfLogEntry, 'GetAddressList', 'AquariusMaster.dbo.GetAddressList', @LogMessage, @ClientPersonnelID 
	
	
	/* Return the outcome to the calling app or proc */
	RETURN @LookupSuccess
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetAddressList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAddressList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAddressList] TO [sp_executeall]
GO
