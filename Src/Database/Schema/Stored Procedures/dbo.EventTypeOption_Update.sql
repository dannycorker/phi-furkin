SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeOption_Update]
(

	@EventTypeOptionID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@AquariumOptionID int   ,

	@OptionValue varchar (50)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeOption]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[AquariumOptionID] = @AquariumOptionID
					,[OptionValue] = @OptionValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[SourceID] = @SourceID
				WHERE
[EventTypeOptionID] = @EventTypeOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Update] TO [sp_executeall]
GO
