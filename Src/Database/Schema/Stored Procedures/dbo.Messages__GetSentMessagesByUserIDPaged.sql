SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 15/08/2011
-- Description:	Gets the Inbox messages paged for the given
---             ClientPersonnelID or PortalUserID
-- =============================================
CREATE PROCEDURE [dbo].[Messages__GetSentMessagesByUserIDPaged]

	@ClientPersonnelID INT = NULL,
	@PortalUserID INT = NULL,
	@PageIndex INT,
	@PageSize INT   

AS
BEGIN
	
	DECLARE @PageLowerBound INT
	DECLARE @PageUpperBound INT
		
	SET @PageLowerBound = @PageSize * @PageIndex
	SET @PageUpperBound = @PageLowerBound + @PageSize

	IF @PageIndex>0
	BEGIN
		SET @PageLowerBound = @PageLowerBound + 1
	END

	;WITH InnerSql AS 
	(
		SELECT *, 
		ROW_NUMBER() OVER(ORDER BY DateSent DESC) AS rn 
		FROM [dbo].[Messages] WITH (NOLOCK)
		WHERE (ClientPersonnelIDFrom = CAST(ISNULL(@ClientPersonnelID,-1) AS INT)) OR (PortalUserIDFrom = CAST(ISNULL(@PortalUserID,-1) AS INT))
	)	
	SELECT i.[MessageID],
			i.[ClientPersonnelIDTo],
			i.[ClientPersonnelIDFrom],
			i.[DateSent],
			i.[DateRead],
			i.[Status],
			i.[Subject],
			i.[MessageText],
			i.[DateReplied],
			i.[DateForwarded],
			i.[PreviousMessageID],
			i.[ClientID],
			i.[PortalUserIDTo],
			i.[PortalUserIDFrom]
	FROM InnerSql i 
	WHERE i.rn BETWEEN @PageLowerBound AND @PageUpperBound 
			
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Messages__GetSentMessagesByUserIDPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages__GetSentMessagesByUserIDPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages__GetSentMessagesByUserIDPaged] TO [sp_executeall]
GO
