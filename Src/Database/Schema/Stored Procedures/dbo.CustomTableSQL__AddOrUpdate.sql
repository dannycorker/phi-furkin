SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-20
-- Description:	Add/update custom table SQL
-- =============================================
CREATE PROCEDURE [dbo].[CustomTableSQL__AddOrUpdate]
	@ClientID INT,
	@DetailFieldID INT,
	@HeaderColumns VARCHAR(2000),
	@SQLQueryID INT,
	@RTFBorderOn BIT,
	@RTFTwips INT,
	@HTMLTableProperties VARCHAR(200),
	@AutoSizeColumns BIT
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (
		SELECT *
		FROM CustomTableSQL c WITH (NOLOCK)
		WHERE c.DetailFieldID = @DetailFieldID
	)
	BEGIN
	
		UPDATE CustomTableSQL 
		SET HeaderColumns = @HeaderColumns, SQLQueryID = @SQLQueryID, RTFBorderOn = @RTFBorderOn, RTFTwips = @RTFTwips, HTMLTableProperties = @HTMLTableProperties, AutoSizeColumns = @AutoSizeColumns
		FROM CustomTableSQL 
		WHERE DetailFieldID = @DetailFieldID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.CustomTableSQL (ClientID, DetailFieldID, HeaderColumns, SQLQueryID, RTFBorderOn, RTFTwips, HTMLTableProperties, AutoSizeColumns)
		VALUES (@ClientID, @DetailFieldID, @HeaderColumns, @SQLQueryID, @RTFBorderOn, @RTFTwips, @HTMLTableProperties, @AutoSizeColumns)
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL__AddOrUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQL__AddOrUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL__AddOrUpdate] TO [sp_executeall]
GO
