SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[SMSStopConfig_GetByClientID](
@ClientID int
)
as 
begin

	SELECT TOP 1 cast(s.isLeadTypeSpecific  as int) [isLeadTypeSpecific]
	FROM SMSStopConfig s with(nolock) 
	WHERE s.ClientID = @ClientID

end
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfig_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStopConfig_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfig_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfig_GetByClientID] TO [sp_executehelper]
GO
