SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-09-03
-- Description:	Updates the AutomatedTaskCommandControl Table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AutomatedTaskCommandControl_Update] 
	@AutomatedTaskCommandControlID INT,
	--@ClientID INT,
	--@JobName VARCHAR(250),
	--@JobNotes VARCHAR(2000),
	--@FiringOrder INT,
	--@SuppressAllLaterJobs BIT,
	--@IsConfigFileRequired BIT
	--@ConfigFileName VARCHAR(50),
	--@IsBatFileRequired BIT,
	@BatFileName VARCHAR(50)
	--@Enabled BIT,
	--@MaxWaitTimeMS INT,
	--@TaskID INT,
	--@OutputExtension VARCHAR(4)
	
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [AutomatedTaskCommandControl] 
	SET 
	--ClientID=@ClientID,JobName=@JobName
	--,JobNotes= @JobNotes
	--,FiringOrder=@FiringOrder
	--,SuppressAllLaterJobs=@SuppressAllLaterJobs
	--,
	--IsConfigFileRequired=@IsConfigFileRequired
	--,ConfigFileName=@ConfigFileName
	--,IsBatFileRequired=@IsBatFileRequired
	BatFileName=@BatFileName
	--,[Enabled]=@Enabled
	--,MaxWaitTimeMS=@MaxWaitTimeMS
	--,TaskID=@TaskID
	--,OutputExtension=@OutputExtension
	WHERE AutomatedTaskCommandControlID = @AutomatedTaskCommandControlID
	

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandControl_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AutomatedTaskCommandControl_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandControl_Update] TO [sp_executeall]
GO
