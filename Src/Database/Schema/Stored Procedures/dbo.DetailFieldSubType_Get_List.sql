SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DetailFieldSubType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldSubType_Get_List]

AS


				
				SELECT
					[DetailFieldSubTypeID],
					[DetailFieldSubTypeName],
					[DetailFieldSubTypeDescription],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[DetailFieldSubType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldSubType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Get_List] TO [sp_executeall]
GO
