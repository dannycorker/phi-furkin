SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_Insert]
(

	@RPIFieldID int   ,

	@RPIFieldName varchar (50)  ,

	@RPIFieldDescription varchar (500)  ,

	@IsEnabled bit   ,

	@QuestionTypeID int   ,

	@DetailFieldSubtypeID tinyint   ,

	@LookupListID int   ,

	@FieldOrder int   ,

	@FieldSize int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@RPIFieldGroupID int   
)
AS


				
				INSERT INTO [dbo].[RPIField]
					(
					[RPIFieldID]
					,[RPIFieldName]
					,[RPIFieldDescription]
					,[IsEnabled]
					,[QuestionTypeID]
					,[DetailFieldSubtypeID]
					,[LookupListID]
					,[FieldOrder]
					,[FieldSize]
					,[WhenCreated]
					,[WhenModified]
					,[RPIFieldGroupID]
					)
				VALUES
					(
					@RPIFieldID
					,@RPIFieldName
					,@RPIFieldDescription
					,@IsEnabled
					,@QuestionTypeID
					,@DetailFieldSubtypeID
					,@LookupListID
					,@FieldOrder
					,@FieldSize
					,@WhenCreated
					,@WhenModified
					,@RPIFieldGroupID
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Insert] TO [sp_executeall]
GO
