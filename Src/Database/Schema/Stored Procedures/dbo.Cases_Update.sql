SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Cases table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_Update]
(

	@CaseID int   ,

	@LeadID int   ,

	@ClientID int   ,

	@CaseNum int   ,

	@CaseRef varchar (250)  ,

	@ClientStatusID int   ,

	@AquariumStatusID int   ,

	@DefaultContactID int   ,

	@LatestLeadEventID int   ,

	@LatestInProcessLeadEventID int   ,

	@LatestOutOfProcessLeadEventID int   ,

	@LatestNonNoteLeadEventID int   ,

	@LatestNoteLeadEventID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@ProcessStartLeadEventID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Cases]
				SET
					[LeadID] = @LeadID
					,[ClientID] = @ClientID
					,[CaseNum] = @CaseNum
					,[CaseRef] = @CaseRef
					,[ClientStatusID] = @ClientStatusID
					,[AquariumStatusID] = @AquariumStatusID
					,[DefaultContactID] = @DefaultContactID
					,[LatestLeadEventID] = @LatestLeadEventID
					,[LatestInProcessLeadEventID] = @LatestInProcessLeadEventID
					,[LatestOutOfProcessLeadEventID] = @LatestOutOfProcessLeadEventID
					,[LatestNonNoteLeadEventID] = @LatestNonNoteLeadEventID
					,[LatestNoteLeadEventID] = @LatestNoteLeadEventID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[ProcessStartLeadEventID] = @ProcessStartLeadEventID
				WHERE
[CaseID] = @CaseID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Update] TO [sp_executeall]
GO
