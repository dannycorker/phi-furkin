SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderObjectAction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectAction_Delete]
(

	@DataLoaderObjectActionID int   
)
AS


				DELETE FROM [dbo].[DataLoaderObjectAction] WITH (ROWLOCK) 
				WHERE
					[DataLoaderObjectActionID] = @DataLoaderObjectActionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectAction_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Delete] TO [sp_executeall]
GO
