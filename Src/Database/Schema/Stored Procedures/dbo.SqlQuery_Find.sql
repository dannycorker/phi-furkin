SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQuery table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_Find]
(

	@SearchUsingOR bit   = null ,

	@QueryID int   = null ,

	@ClientID int   = null ,

	@QueryText varchar (MAX)  = null ,

	@QueryTitle varchar (250)  = null ,

	@AutorunOnline tinyint   = null ,

	@OnlineLimit int   = null ,

	@BatchLimit int   = null ,

	@SqlQueryTypeID int   = null ,

	@FolderID int   = null ,

	@IsEditable bit   = null ,

	@IsTemplate bit   = null ,

	@IsDeleted bit   = null ,

	@WhenCreated datetime   = null ,

	@CreatedBy int   = null ,

	@OwnedBy int   = null ,

	@RunCount int   = null ,

	@LastRundate datetime   = null ,

	@LastRuntime int   = null ,

	@LastRowcount int   = null ,

	@MaxRuntime int   = null ,

	@MaxRowcount int   = null ,

	@AvgRuntime int   = null ,

	@AvgRowcount int   = null ,

	@Comments varchar (2000)  = null ,

	@WhenModified datetime   = null ,

	@ModifiedBy int   = null ,

	@LeadTypeID int   = null ,

	@ParentQueryID int   = null ,

	@IsParent bit   = null ,

	@SqlQueryTemplateID int   = null ,

	@OutputFormat varchar (20)  = null ,

	@ShowInCustomSearch bit   = null ,

	@OutputFileExtension varchar (50)  = null ,

	@OutputSeparatorCharmapID tinyint   = null ,

	@OutputEncapsulatorCharmapID tinyint   = null ,

	@SuppressHeaderRow bit   = null ,

	@LockAllTables bit   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QueryID]
	, [ClientID]
	, [QueryText]
	, [QueryTitle]
	, [AutorunOnline]
	, [OnlineLimit]
	, [BatchLimit]
	, [SqlQueryTypeID]
	, [FolderID]
	, [IsEditable]
	, [IsTemplate]
	, [IsDeleted]
	, [WhenCreated]
	, [CreatedBy]
	, [OwnedBy]
	, [RunCount]
	, [LastRundate]
	, [LastRuntime]
	, [LastRowcount]
	, [MaxRuntime]
	, [MaxRowcount]
	, [AvgRuntime]
	, [AvgRowcount]
	, [Comments]
	, [WhenModified]
	, [ModifiedBy]
	, [LeadTypeID]
	, [ParentQueryID]
	, [IsParent]
	, [SqlQueryTemplateID]
	, [OutputFormat]
	, [ShowInCustomSearch]
	, [OutputFileExtension]
	, [OutputSeparatorCharmapID]
	, [OutputEncapsulatorCharmapID]
	, [SuppressHeaderRow]
	, [LockAllTables]
	, [SourceID]
    FROM
	[dbo].[SqlQuery] WITH (NOLOCK) 
    WHERE 
	 ([QueryID] = @QueryID OR @QueryID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([QueryText] = @QueryText OR @QueryText IS NULL)
	AND ([QueryTitle] = @QueryTitle OR @QueryTitle IS NULL)
	AND ([AutorunOnline] = @AutorunOnline OR @AutorunOnline IS NULL)
	AND ([OnlineLimit] = @OnlineLimit OR @OnlineLimit IS NULL)
	AND ([BatchLimit] = @BatchLimit OR @BatchLimit IS NULL)
	AND ([SqlQueryTypeID] = @SqlQueryTypeID OR @SqlQueryTypeID IS NULL)
	AND ([FolderID] = @FolderID OR @FolderID IS NULL)
	AND ([IsEditable] = @IsEditable OR @IsEditable IS NULL)
	AND ([IsTemplate] = @IsTemplate OR @IsTemplate IS NULL)
	AND ([IsDeleted] = @IsDeleted OR @IsDeleted IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([OwnedBy] = @OwnedBy OR @OwnedBy IS NULL)
	AND ([RunCount] = @RunCount OR @RunCount IS NULL)
	AND ([LastRundate] = @LastRundate OR @LastRundate IS NULL)
	AND ([LastRuntime] = @LastRuntime OR @LastRuntime IS NULL)
	AND ([LastRowcount] = @LastRowcount OR @LastRowcount IS NULL)
	AND ([MaxRuntime] = @MaxRuntime OR @MaxRuntime IS NULL)
	AND ([MaxRowcount] = @MaxRowcount OR @MaxRowcount IS NULL)
	AND ([AvgRuntime] = @AvgRuntime OR @AvgRuntime IS NULL)
	AND ([AvgRowcount] = @AvgRowcount OR @AvgRowcount IS NULL)
	AND ([Comments] = @Comments OR @Comments IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ParentQueryID] = @ParentQueryID OR @ParentQueryID IS NULL)
	AND ([IsParent] = @IsParent OR @IsParent IS NULL)
	AND ([SqlQueryTemplateID] = @SqlQueryTemplateID OR @SqlQueryTemplateID IS NULL)
	AND ([OutputFormat] = @OutputFormat OR @OutputFormat IS NULL)
	AND ([ShowInCustomSearch] = @ShowInCustomSearch OR @ShowInCustomSearch IS NULL)
	AND ([OutputFileExtension] = @OutputFileExtension OR @OutputFileExtension IS NULL)
	AND ([OutputSeparatorCharmapID] = @OutputSeparatorCharmapID OR @OutputSeparatorCharmapID IS NULL)
	AND ([OutputEncapsulatorCharmapID] = @OutputEncapsulatorCharmapID OR @OutputEncapsulatorCharmapID IS NULL)
	AND ([SuppressHeaderRow] = @SuppressHeaderRow OR @SuppressHeaderRow IS NULL)
	AND ([LockAllTables] = @LockAllTables OR @LockAllTables IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QueryID]
	, [ClientID]
	, [QueryText]
	, [QueryTitle]
	, [AutorunOnline]
	, [OnlineLimit]
	, [BatchLimit]
	, [SqlQueryTypeID]
	, [FolderID]
	, [IsEditable]
	, [IsTemplate]
	, [IsDeleted]
	, [WhenCreated]
	, [CreatedBy]
	, [OwnedBy]
	, [RunCount]
	, [LastRundate]
	, [LastRuntime]
	, [LastRowcount]
	, [MaxRuntime]
	, [MaxRowcount]
	, [AvgRuntime]
	, [AvgRowcount]
	, [Comments]
	, [WhenModified]
	, [ModifiedBy]
	, [LeadTypeID]
	, [ParentQueryID]
	, [IsParent]
	, [SqlQueryTemplateID]
	, [OutputFormat]
	, [ShowInCustomSearch]
	, [OutputFileExtension]
	, [OutputSeparatorCharmapID]
	, [OutputEncapsulatorCharmapID]
	, [SuppressHeaderRow]
	, [LockAllTables]
	, [SourceID]
    FROM
	[dbo].[SqlQuery] WITH (NOLOCK) 
    WHERE 
	 ([QueryID] = @QueryID AND @QueryID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([QueryText] = @QueryText AND @QueryText is not null)
	OR ([QueryTitle] = @QueryTitle AND @QueryTitle is not null)
	OR ([AutorunOnline] = @AutorunOnline AND @AutorunOnline is not null)
	OR ([OnlineLimit] = @OnlineLimit AND @OnlineLimit is not null)
	OR ([BatchLimit] = @BatchLimit AND @BatchLimit is not null)
	OR ([SqlQueryTypeID] = @SqlQueryTypeID AND @SqlQueryTypeID is not null)
	OR ([FolderID] = @FolderID AND @FolderID is not null)
	OR ([IsEditable] = @IsEditable AND @IsEditable is not null)
	OR ([IsTemplate] = @IsTemplate AND @IsTemplate is not null)
	OR ([IsDeleted] = @IsDeleted AND @IsDeleted is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([OwnedBy] = @OwnedBy AND @OwnedBy is not null)
	OR ([RunCount] = @RunCount AND @RunCount is not null)
	OR ([LastRundate] = @LastRundate AND @LastRundate is not null)
	OR ([LastRuntime] = @LastRuntime AND @LastRuntime is not null)
	OR ([LastRowcount] = @LastRowcount AND @LastRowcount is not null)
	OR ([MaxRuntime] = @MaxRuntime AND @MaxRuntime is not null)
	OR ([MaxRowcount] = @MaxRowcount AND @MaxRowcount is not null)
	OR ([AvgRuntime] = @AvgRuntime AND @AvgRuntime is not null)
	OR ([AvgRowcount] = @AvgRowcount AND @AvgRowcount is not null)
	OR ([Comments] = @Comments AND @Comments is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([ModifiedBy] = @ModifiedBy AND @ModifiedBy is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ParentQueryID] = @ParentQueryID AND @ParentQueryID is not null)
	OR ([IsParent] = @IsParent AND @IsParent is not null)
	OR ([SqlQueryTemplateID] = @SqlQueryTemplateID AND @SqlQueryTemplateID is not null)
	OR ([OutputFormat] = @OutputFormat AND @OutputFormat is not null)
	OR ([ShowInCustomSearch] = @ShowInCustomSearch AND @ShowInCustomSearch is not null)
	OR ([OutputFileExtension] = @OutputFileExtension AND @OutputFileExtension is not null)
	OR ([OutputSeparatorCharmapID] = @OutputSeparatorCharmapID AND @OutputSeparatorCharmapID is not null)
	OR ([OutputEncapsulatorCharmapID] = @OutputEncapsulatorCharmapID AND @OutputEncapsulatorCharmapID is not null)
	OR ([SuppressHeaderRow] = @SuppressHeaderRow AND @SuppressHeaderRow is not null)
	OR ([LockAllTables] = @LockAllTables AND @LockAllTables is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Find] TO [sp_executeall]
GO
