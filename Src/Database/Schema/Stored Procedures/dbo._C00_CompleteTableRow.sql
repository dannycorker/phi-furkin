SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 08-Aug-2011
-- Description:	Proc to add all columns that don't already exist to a tablerow
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CompleteTableRow]
(
	@TableRowID INT,
	@MatterID INT = NULL,
	@DenyEdit INT = 0,
	@DenyDelete INT = 0
)

AS
BEGIN

	DECLARE @LeadOrMatter INT

	SET NOCOUNT ON;
	
	SELECT @LeadOrMatter=df.LeadOrMatter FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) ON tr.DetailFieldID=df.DetailFieldID
	WHERE tr.TableRowID=@TableRowID

	/* Create all necessary Table Row Values records that don't already exist for Matter*/
	IF @LeadOrMatter=2 AND @MatterID IS NOT NULL
	BEGIN
		INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,LeadID,MatterID,ClientID,EncryptedValue)
		SELECT tr.TableRowID, NULL,dftdv.detailfieldid, '', tr.LeadID,@MatterID, tr.ClientID, NULL
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN detailfields dftr WITH (NOLOCK) ON tr.detailfieldid = dftr.detailfieldid AND dftr.LeadOrMatter = 2
		INNER JOIN detailfields dftdv WITH (NOLOCK) ON dftdv.detailfieldpageid = dftr.TableDetailFieldPageID
		WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.MatterID = @MatterID AND tdv.DetailFieldID = dftdv.DetailFieldID AND tdv.TableRowID = tr.TableRowID) 
		AND tr.tablerowid = @TableRowID
	END
	
	
	/* Create all necessary Table Row Values records that don't already exist for Lead*/
	IF @LeadOrMatter=1
	BEGIN
		INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,LeadID,MatterID,ClientID,EncryptedValue)
		SELECT tr.TableRowID, NULL,dftdv.detailfieldid, '', tr.LeadID,NULL, tr.ClientID, NULL
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN detailfields dftr WITH (NOLOCK) ON tr.detailfieldid = dftr.detailfieldid AND dftr.LeadOrMatter = 1
		INNER JOIN detailfields dftdv WITH (NOLOCK) ON dftdv.detailfieldpageid = dftr.TableDetailFieldPageID
		WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.LeadID = tr.LeadID AND tdv.DetailFieldID = dftdv.DetailFieldID AND tdv.TableRowID = tr.TableRowID) 
		AND tr.tablerowid = @TableRowID
	END
	
	/* Create all necessary Table Row Values records that don't already exist for Customer*/
	IF @LeadOrMatter=10
	BEGIN
		INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,CustomerID,ClientID,EncryptedValue)
		SELECT tr.TableRowID, NULL,dftdv.detailfieldid, '', tr.CustomerID, tr.ClientID, NULL
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN detailfields dftr WITH (NOLOCK) ON tr.detailfieldid = dftr.detailfieldid AND dftr.LeadOrMatter = 10
		INNER JOIN detailfields dftdv WITH (NOLOCK) ON dftdv.detailfieldpageid = dftr.TableDetailFieldPageID
		WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.CustomerID = tr.CustomerID AND tdv.DetailFieldID = dftdv.DetailFieldID AND tdv.TableRowID = tr.TableRowID) 
		AND tr.tablerowid = @TableRowID
	END
	
	/* Create all necessary Table Row Values records that don't already exist for Case*/ -- added tmd 03/6/2016
	IF @LeadOrMatter=11
	BEGIN
		INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,ClientID,EncryptedValue)
		SELECT tr.TableRowID, NULL,dftdv.detailfieldid, '', tr.ClientID, NULL
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN detailfields dftr WITH (NOLOCK) ON tr.detailfieldid = dftr.detailfieldid AND dftr.LeadOrMatter = 11
		INNER JOIN detailfields dftdv WITH (NOLOCK) ON dftdv.detailfieldpageid = dftr.TableDetailFieldPageID
		WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.ClientID = tr.ClientID AND tdv.DetailFieldID = dftdv.DetailFieldID AND tdv.TableRowID = tr.TableRowID) 
		AND tr.tablerowid = @TableRowID
	END
	
	/* Create all necessary Table Row Values records that don't already exist for Client*/ -- added dcm 28/4/2014
	IF @LeadOrMatter=12
	BEGIN
		INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,ClientID,EncryptedValue)
		SELECT tr.TableRowID, NULL,dftdv.detailfieldid, '', tr.ClientID, NULL
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN detailfields dftr WITH (NOLOCK) ON tr.detailfieldid = dftr.detailfieldid AND dftr.LeadOrMatter = 12
		INNER JOIN detailfields dftdv WITH (NOLOCK) ON dftdv.detailfieldpageid = dftr.TableDetailFieldPageID
		WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.ClientID = tr.ClientID AND tdv.DetailFieldID = dftdv.DetailFieldID AND tdv.TableRowID = tr.TableRowID) 
		AND tr.tablerowid = @TableRowID
	END
	
	/* Create all necessary Table Row Values records that don't already exist for Contact*/
	IF @LeadOrMatter=14
	BEGIN
		INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, ContactID, ClientID, EncryptedValue)
		SELECT tr.TableRowID, NULL,dftdv.detailfieldid, '', tr.ContactID, tr.ClientID, NULL
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN detailfields dftr WITH (NOLOCK) ON tr.detailfieldid = dftr.detailfieldid AND dftr.LeadOrMatter = 14
		INNER JOIN detailfields dftdv WITH (NOLOCK) ON dftdv.detailfieldpageid = dftr.TableDetailFieldPageID
		WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.DetailFieldID = dftdv.DetailFieldID AND tdv.TableRowID = tr.TableRowID) 
		AND tr.tablerowid = @TableRowID
	END

	UPDATE TableRows SET DenyDelete=@DenyDelete,DenyEdit=@DenyEdit WHERE TableRowID=@TableRowID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CompleteTableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CompleteTableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CompleteTableRow] TO [sp_executeall]
GO
