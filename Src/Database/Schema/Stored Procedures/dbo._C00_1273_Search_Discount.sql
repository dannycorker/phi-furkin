SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get all the base discounts
-- Mods:
--	2015-05-14 DCM Ticket #31733 Entire contents moved to new proc to facilitate renaming in prelive
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Search_Discount]
(
	@ClientId INT,
	@ClientPersonnelId INT,
	@Filter VARCHAR(50)
)
AS
BEGIN

	EXEC _C600_PA_Search_Discount @ClientId, @ClientPersonnelId, @Filter
		
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Search_Discount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Search_Discount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Search_Discount] TO [sp_executeall]
GO
