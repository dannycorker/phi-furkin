SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-21
-- Description:	Saves a new claim to the db
-- =============================================
CREATE PROCEDURE [dbo].[_C00_VetEnvoy_SaveNewClaim]
(
	@ClientID INT,
	@ConversationID VARCHAR(50),
	@Xml XML,
	@Debug BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON;
		
	DECLARE @AqAutomation INT = 14646,
			@Date DATE
			
	SELECT @Date = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	
	-- Might need to wrap this in a tran and should probably hand off to _C{@ClientID}_VetEnvoy_SaveNewClaim procs
	
	DECLARE @PolicyRef VARCHAR(2000)
	;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim')
	SELECT @PolicyRef = @Xml.value('(/InsuranceClaim/InfoFromPolicyHolder/PolicyDetails/PolicyNumber)[1]', 'varchar(2000)')
	
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	WHERE l.ClientID = 224
	AND l.LeadRef = @PolicyRef
	--AND c.Test = 0
	
	IF @Debug = 1
	BEGIN
	
		SELECT @PolicyRef AS PolicyRef, @LeadID AS LeadID
	
	END
	
	IF @LeadID IS NULL
	BEGIN
		RETURN
	END
	
	-- Check for existing matters
	DECLARE @MatterCount INT,
			@MatterID INT,
			@CreateMatter BIT = 0,
			@CaseID INT
			
	SELECT @MatterCount = COUNT(*) 
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	IF @Debug = 1
	BEGIN
		SELECT @MatterCount AS MatterCount
	END
	
	IF @MatterCount = 1
	BEGIN
		
		SELECT @MatterID = MatterID, @CaseID = CaseID
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		
		-- Work out if we need to reuse
		DECLARE @EventCount INT
		SELECT @EventCount = COUNT(*) 
		FROM dbo.LeadEvent WITH (NOLOCK) 
		WHERE CaseID = @CaseID
		
		DECLARE @DescriptionValue VARCHAR(2000)
		SELECT @DescriptionValue = DetailValue
		FROM dbo.MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 144332
		
		IF @EventCount > 1 OR @DescriptionValue IS NOT NULL
		BEGIN
			SELECT @CreateMatter = 1 
		END
		
	END
	ELSE
	BEGIN
		SELECT @CreateMatter = 1
	END
	
	IF @Debug = 1
	BEGIN
		SELECT @CreateMatter AS CreateMatter
	END
	
	DECLARE @Conditions TABLE
	(
		ID INT IDENTITY,
		ConditionCode VARCHAR(2000),
		Description VARCHAR(2000),
		DateOfLoss VARCHAR(2000),
		TreatmentFrom VARCHAR(2000),
		TreatmentTo VARCHAR(2000),
		InvoiceItems XML,
		TotalExVAT VARCHAR(2000),
		VAT VARCHAR(2000),
		GrandTotal VARCHAR(2000)
	)
	
	
	DECLARE @ClaimItems TABLE
	(
		ID INT,
		Description VARCHAR(2000),
		Type VARCHAR(2000),
		AmountExVAT MONEY,
		InvoiceNumber VARCHAR(2000),
		Date VARCHAR(2000),
		VAT MONEY,
		Quantity VARCHAR(2000)
	)
	DECLARE @TableRowsInserted TABLE
	(
		SourceID INT,
		TableRowID INT
	)
	DECLARE @FieldIDs TABLE
	(
		FieldID INT
	)
	INSERT @FieldIDs (FieldID) VALUES
	(144350), -- Policy sections
	(144349), -- From
	(144351), -- To
	(149778), -- Row type
	(144353)  -- Claimed
	
	DECLARE @ClinicalHistory TABLE
	(
		ID INT IDENTITY,
		Date VARCHAR(2000),
		Time VARCHAR(2000),
		EnteredBy VARCHAR(2000),
		TextEntry VARCHAR(2000)
	)
	DECLARE @HistoryFieldIDs TABLE
	(
		FieldID INT
	)
	INSERT @HistoryFieldIDs (FieldID) VALUES
	(162666), -- Date
	(162667), -- Time
	(162668), -- Entered By
	(162669)  -- Text Entry
	
	
	;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim'),
	RawData AS 
	(
		SELECT	node.value('Date[1]', 'VARCHAR(2000)') AS Date,
				node.value('Time[1]', 'VARCHAR(2000)') AS Time,
				node.value('EnteredBy[1]', 'VARCHAR(2000)') AS EnteredBy,
				node.value('TextEntry[1]', 'VARCHAR(2000)') AS TextEntry
		FROM @Xml.nodes('//InfoFromVet/AnimalClinicalHistory/Entry') AS received (node)
	)
	
	INSERT INTO @ClinicalHistory (Date, Time, EnteredBy, TextEntry)
	SELECT *
	FROM RawData
	
	IF @Debug = 1
	BEGIN
	
		SELECT *
		FROM @ClinicalHistory
	
	END
	

	;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim'),
	RawData AS 
	(
		SELECT	node.value('ConditionCode[1]', 'VARCHAR(2000)') AS ConditionCode,
				node.value('DiagnosisOrSigns[1]', 'VARCHAR(2000)') AS Description,
				node.value('Started[1]', 'VARCHAR(2000)') AS DateOfLoss,
				node.value('(TreatmentDates/DateFrom)[1]', 'VARCHAR(2000)') AS TreatmentFrom,
				node.value('(TreatmentDates/DateTo)[1]', 'VARCHAR(2000)') AS TreatmentTo,
				node.query('Financial/InvoiceItems')  AS InvoiceItems,
				node.value('(Financial/TotalExVAT)[1]', 'VARCHAR(2000)') AS TotalExVAT,
				node.value('(Financial/VAT)[1]', 'VARCHAR(2000)') AS VAT,
				node.value('(Financial/TotalIncVat)[1]', 'VARCHAR(2000)') AS GrandTotal		
		FROM @Xml.nodes('//InfoFromVet/Conditions/Condition') AS received (node)
	)
	
	INSERT INTO @Conditions (ConditionCode, Description, DateOfLoss, TreatmentFrom, TreatmentTo, InvoiceItems, TotalExVAT, VAT, GrandTotal)
	SELECT *
	FROM RawData
	
	IF @Debug = 1
	BEGIN
	
		SELECT *
		FROM @Conditions
	
	END
	
	
	DECLARE @Count INT,
			@Index INT,
			@ConditionCode VARCHAR(2000),
			@Description VARCHAR(2000),
			@DateOfLoss VARCHAR(2000),
			@TreatmentFrom VARCHAR(2000),
			@TreatmentTo VARCHAR(2000),
			@InvoiceItems XML,
			@TotalExVAT VARCHAR(2000),
			@VAT VARCHAR(2000),
			@GrandTotal VARCHAR(2000)
			
	SELECT @Count =  COUNT(*), @Index = 0
	FROM @Conditions
	
	WHILE @Index < @Count
	BEGIN
		
		-- Clear out the claim items
		DELETE @ClaimItems
		DELETE @TableRowsInserted
		
		
		SELECT @Index = @Index + 1
		
		SELECT	@ConditionCode = ConditionCode,
				@Description = Description,
				@DateOfLoss = DateOfLoss,
				@TreatmentFrom = TreatmentFrom,
				@TreatmentTo = TreatmentTo,
				@InvoiceItems = InvoiceItems,
				@TotalExVAT = TotalExVAT,
				@VAT = VAT,
				@GrandTotal = GrandTotal
		FROM @Conditions
		WHERE ID = @Index
		
		IF @Debug = 0
		BEGIN
			IF @CreateMatter = 1
			BEGIN
			
				EXEC @MatterID = dbo._C00_CreateNewCaseForLead @LeadID, @AqAutomation
				SELECT @CaseID = CaseID
				FROM dbo.Matter WITH (NOLOCK) 
				WHERE MatterID = @MatterID
					
			END
		END
		
		SELECT @CreateMatter = 1
		
		-- Set the basic details
		IF @Debug = 0
		BEGIN
		
			-- Apply the process start event
			EXEC dbo._C00_AddProcessStart @CaseID, 14646
		
			-- Save the extra fields needed for SAP refunds
			EXEC dbo._C00_SimpleValueIntoField 162628, @ConversationID, @MatterID
			-- Claim papers received
			EXEC _C00_SimpleValueIntoField 144520, @Date, @MatterID
			-- Claim details entered
			EXEC _C00_SimpleValueIntoField 153017, @Date, @MatterID
			EXEC _C00_SimpleValueIntoField 144892, @DateOfLoss, @MatterID
			EXEC _C00_SimpleValueIntoField 144332, @Description, @MatterID
			EXEC _C00_SimpleValueIntoField 144483, '44356', @MatterID -- Claim type New
			
			EXEC _C00_SimpleValueIntoField 144366, @TreatmentFrom, @MatterID
			EXEC _C00_SimpleValueIntoField 145674, @TreatmentTo, @MatterID
			EXEC _C00_SimpleValueIntoField 149850, @GrandTotal, @MatterID
		
		END
		
		
		
		;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim'),
		RawData AS 
		(
			SELECT	ROW_NUMBER() OVER(ORDER BY node) as rn,
					node.value('Description[1]', 'VARCHAR(2000)') AS Description,
					node.value('Type[1]', 'VARCHAR(2000)') AS Type,
					node.value('AmountExVAT[1]', 'MONEY') AS AmountExVAT,
					node.value('InvoiceNumber[1]', 'VARCHAR(2000)') AS InvoiceNumber,
					node.value('Date[1]', 'VARCHAR(2000)') AS Date,
					node.value('VAT[1]', 'MONEY') AS VAT,
					node.value('Quantity[1]', 'VARCHAR(2000)') AS Quantity	
			FROM @InvoiceItems.nodes('//Item') AS received (node)
		)
		
		INSERT INTO @ClaimItems (ID, Description, Type, AmountExVAT, InvoiceNumber, Date, VAT, Quantity)
		SELECT *
		FROM RawData
		
		IF @Debug = 1
		BEGIN
		
			SELECT *
			FROM @ClaimItems
		
		END
		ELSE
		BEGIN
		
			INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowsInserted
			SELECT 224, @MatterID, 144355, 16157, ID
			FROM @ClaimItems
			
			
			-- TODO:	Need to work out if these items are in a coinsurance period... or do it later in the calc?
			--			Default policy section to vet fees and allow the user to override?
			
			INSERT dbo.TableDetailValues (ClientID, DetailFieldID, LeadID, MatterID, TableRowID, DetailValue, ResourceListID)
			SELECT 224, f.FieldID, @LeadID, @MatterID, t.TableRowID, 
				CASE
					WHEN f.FieldID = 144350 THEN NULL
					WHEN f.FieldID = 144349 THEN c.Date
					WHEN f.FieldID = 144351 THEN c.Date
					WHEN f.FieldID = 149778 THEN '50537'
					WHEN f.FieldID = 144353 THEN CAST(ISNULL(c.AmountExVAT, 0) + ISNULL(c.VAT, 0) AS VARCHAR)
				END AS Value,
				CASE
					WHEN f.FieldID = 144350 THEN '75577'
					ELSE NULL
				END AS RLID	
			FROM @ClaimItems c
			INNER JOIN @TableRowsInserted t ON c.ID = t.SourceID
			CROSS JOIN @FieldIDs f
			
			
			DELETE @TableRowsInserted
			
			INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowsInserted
			SELECT 224, @MatterID, 162665, 16188, ID
			FROM @ClinicalHistory
			
			INSERT dbo.TableDetailValues (ClientID, DetailFieldID, LeadID, MatterID, TableRowID, DetailValue)
			SELECT 224, f.FieldID, @LeadID, @MatterID, t.TableRowID, 
				CASE
					WHEN f.FieldID = 162666 THEN c.Date
					WHEN f.FieldID = 162667 THEN c.Time
					WHEN f.FieldID = 162668 THEN c.EnteredBy
					WHEN f.FieldID = 162669 THEN c.TextEntry
				END AS Value
			FROM @ClinicalHistory c
			INNER JOIN @TableRowsInserted t ON c.ID = t.SourceID
			CROSS JOIN @HistoryFieldIDs f
			
		
		END

	END
	
	
	IF @Debug = 0
	BEGIN
		
		-- Save the actioned date
		INSERT INTO dbo.TableDetailValues  (TableRowID, DetailFieldID, ClientID, DetailValue)
		SELECT r.TableRowID, 162662, 224, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvConversationID WITH (NOLOCK) ON r.TableRowID = tdvConversationID.TableRowID AND tdvConversationID.DetailFieldID = 162658
		WHERE r.ClientID = 224
		AND tdvConversationID.DetailValue = @ConversationID
		
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_SaveNewClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_VetEnvoy_SaveNewClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_SaveNewClaim] TO [sp_executeall]
GO
