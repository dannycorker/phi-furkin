SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderFieldDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_Get_List]

AS


				
				SELECT
					[DataLoaderFieldDefinitionID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderMapSectionID],
					[DataLoaderObjectFieldID],
					[DetailFieldID],
					[DetailFieldAlias],
					[NamedValue],
					[Keyword],
					[DataLoaderKeywordMatchTypeID],
					[RowRelativeToKeyword],
					[ColRelativeToKeyword],
					[SectionRelativeRow],
					[SectionAbsoluteCol],
					[ValidationRegex],
					[Equation],
					[IsMatchField],
					[DecodeTypeID],
					[DefaultLookupItemID],
					[SourceDataLoaderFieldDefinitionID],
					[Notes],
					[AllowErrors]
				FROM
					[dbo].[DataLoaderFieldDefinition] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Get_List] TO [sp_executeall]
GO
