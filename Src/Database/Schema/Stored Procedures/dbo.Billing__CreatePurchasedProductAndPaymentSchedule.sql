SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-10-17
-- Description:	Core Create PP and PPPS
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreatePurchasedProductAndPaymentSchedule]
	@ClientID INT,
	@CustomerID INT,
	@AccountID INT,
	@PaymentFrequencyID INT,
	@PurchaseDate DATETIME,
	@ValidFrom DATETIME,
	@ValidTo DATETIME,
	@PreferredPaymentDay INT,
	@FirstPaymentDate DATE,
	@RemainderUpFront BIT,
	@ProductName VARCHAR(250),
	@ProductDescription VARCHAR(2000),
	@PremiumNet NUMERIC(18,2),
	@PremiumTax NUMERIC(18,2),
	@PremiumGross NUMERIC(18,2),
	@RuleSetID INT,
	@ProductCostBreakdown XML,
	@ObjectID INT,
	@ObjectTypeID INT,
	@WhoCreated INT,
	@PremiumCalculationDetailID INT,
	@AdminFee INT,
	@ClientAccountID INT,
	@CurrencyID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PurchasedProductID INT

	INSERT INTO dbo.PurchasedProduct (ClientID, CustomerID, AccountID, PaymentFrequencyID, NumberOfInstallments, ProductPurchasedOnDate, ValidFrom, ValidTo, PreferredPaymentDay, FirstPaymentDate, RemainderUpFront, ProductName, ProductDescription, ProductCostNet, ProductCostVAT, ProductCostGross, ProductCostCalculatedWithRuleSetID, ProductCostCalculatedOn, ProductCostBreakdown, ObjectID, ObjectTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID, PremiumCalculationDetailID, ProductAdditionalFee, ClientAccountID, CurrencyID)
	SELECT	@ClientID, 
			@CustomerID, 
			@AccountID, --- must get
			@PaymentFrequencyID, 
			--CASE WHEN @PaymentFrequencyID = 4 THEN 12 ELSE 1 END, -- NumberOfInstallments (billing system will use its default for this.  CPS 2017-06-02 (it appears not to be using its default, so populate this)
			CASE @PaymentFrequencyID
				WHEN 4 THEN 12
				WHEN 7 THEN 26
			ELSE 1
			END, -- NumberOfInstallments (billing system will use its default for this. CPS 2017-06-02 (it appears not to be using its default, so populate this)
			@PurchaseDate,			    -- ProductPurchasedOnDate
			@ValidFrom,					-- = start date
			@ValidTo,					-- = end date - 1 
			@PreferredPaymentDay,		 
			@FirstPaymentDate,			-- use date of next regular payment calculation
			@RemainderUpFront,			-- from client config data 
			@ProductName,
			@ProductDescription,		-- ProductDescription
			@PremiumNet,				-- ProductCostNet
			@PremiumTax,				-- ProductCostVAT 
			@PremiumGross,				-- ProductCostGross
			@RuleSetID,					-- ProductCostCalculatedWithRuleSetID
			dbo.fn_GetDate_Local(),					-- ProductCostCalculatedOn
			@ProductCostBreakdown, 
			@ObjectID,					-- ObjectID
			@ObjectTypeID,				-- ObjectTypeID 
			@WhoCreated,				-- WhoCreated
			dbo.fn_GetDate_Local(), 
			@WhoCreated,				-- WhoModified
			dbo.fn_GetDate_Local(),
			NULL,NULL,NULL,				-- PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID
			@PremiumCalculationDetailID,
			@AdminFee,
			@ClientAccountID,
			@CurrencyID
			

	SELECT @PurchasedProductID = SCOPE_IDENTITY()

	-- Create purchased product schedule
	EXEC PurchasedProduct__CreatePaymentSchedule @PurchasedProductID	
	
	RETURN @PurchasedProductID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePurchasedProductAndPaymentSchedule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreatePurchasedProductAndPaymentSchedule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePurchasedProductAndPaymentSchedule] TO [sp_executeall]
GO
