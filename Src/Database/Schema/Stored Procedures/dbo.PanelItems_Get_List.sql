SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PanelItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_Get_List]

AS


				
				SELECT
					[PanelItemID],
					[ClientID],
					[PanelItemName],
					[PanelID],
					[PanelItemTypeID],
					[CreatedBy],
					[QueryID],
					[IsGlobal],
					[XAxisColumn],
					[Orientation],
					[Margin]
				FROM
					[dbo].[PanelItems] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Get_List] TO [sp_executeall]
GO
