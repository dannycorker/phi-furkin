SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Contact table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact_Find]
(

	@SearchUsingOR bit   = null ,

	@ContactID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@TitleID int   = null ,

	@Firstname varchar (100)  = null ,

	@Middlename varchar (100)  = null ,

	@Lastname varchar (100)  = null ,

	@Fullname varchar (201)  = null ,

	@EmailAddressWork varchar (100)  = null ,

	@EmailAddressOther varchar (100)  = null ,

	@DirectDial varchar (100)  = null ,

	@MobilePhoneWork varchar (100)  = null ,

	@MobilePhoneOther varchar (100)  = null ,

	@Address1 varchar (100)  = null ,

	@Address2 varchar (100)  = null ,

	@Town varchar (100)  = null ,

	@County varchar (100)  = null ,

	@Postcode varchar (100)  = null ,

	@Country varchar (100)  = null ,

	@OfficeID int   = null ,

	@DepartmentID int   = null ,

	@JobTitle varchar (100)  = null ,

	@Notes varchar (255)  = null ,

	@CountryID int   = null ,

	@LanguageID int   = null ,

	@WhenModified datetime   = null ,

	@WhoModified int   = null ,

	@Longitude numeric (25, 18)  = null ,

	@Latitude numeric (25, 18)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ContactID]
	, [ClientID]
	, [CustomerID]
	, [TitleID]
	, [Firstname]
	, [Middlename]
	, [Lastname]
	, [Fullname]
	, [EmailAddressWork]
	, [EmailAddressOther]
	, [DirectDial]
	, [MobilePhoneWork]
	, [MobilePhoneOther]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [OfficeID]
	, [DepartmentID]
	, [JobTitle]
	, [Notes]
	, [CountryID]
	, [LanguageID]
	, [WhenModified]
	, [WhoModified]
	, [Longitude]
	, [Latitude]
    FROM
	[dbo].[Contact] WITH (NOLOCK) 
    WHERE 
	 ([ContactID] = @ContactID OR @ContactID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([TitleID] = @TitleID OR @TitleID IS NULL)
	AND ([Firstname] = @Firstname OR @Firstname IS NULL)
	AND ([Middlename] = @Middlename OR @Middlename IS NULL)
	AND ([Lastname] = @Lastname OR @Lastname IS NULL)
	AND ([Fullname] = @Fullname OR @Fullname IS NULL)
	AND ([EmailAddressWork] = @EmailAddressWork OR @EmailAddressWork IS NULL)
	AND ([EmailAddressOther] = @EmailAddressOther OR @EmailAddressOther IS NULL)
	AND ([DirectDial] = @DirectDial OR @DirectDial IS NULL)
	AND ([MobilePhoneWork] = @MobilePhoneWork OR @MobilePhoneWork IS NULL)
	AND ([MobilePhoneOther] = @MobilePhoneOther OR @MobilePhoneOther IS NULL)
	AND ([Address1] = @Address1 OR @Address1 IS NULL)
	AND ([Address2] = @Address2 OR @Address2 IS NULL)
	AND ([Town] = @Town OR @Town IS NULL)
	AND ([County] = @County OR @County IS NULL)
	AND ([Postcode] = @Postcode OR @Postcode IS NULL)
	AND ([Country] = @Country OR @Country IS NULL)
	AND ([OfficeID] = @OfficeID OR @OfficeID IS NULL)
	AND ([DepartmentID] = @DepartmentID OR @DepartmentID IS NULL)
	AND ([JobTitle] = @JobTitle OR @JobTitle IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([Longitude] = @Longitude OR @Longitude IS NULL)
	AND ([Latitude] = @Latitude OR @Latitude IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ContactID]
	, [ClientID]
	, [CustomerID]
	, [TitleID]
	, [Firstname]
	, [Middlename]
	, [Lastname]
	, [Fullname]
	, [EmailAddressWork]
	, [EmailAddressOther]
	, [DirectDial]
	, [MobilePhoneWork]
	, [MobilePhoneOther]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [OfficeID]
	, [DepartmentID]
	, [JobTitle]
	, [Notes]
	, [CountryID]
	, [LanguageID]
	, [WhenModified]
	, [WhoModified]
	, [Longitude]
	, [Latitude]
    FROM
	[dbo].[Contact] WITH (NOLOCK) 
    WHERE 
	 ([ContactID] = @ContactID AND @ContactID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([Firstname] = @Firstname AND @Firstname is not null)
	OR ([Middlename] = @Middlename AND @Middlename is not null)
	OR ([Lastname] = @Lastname AND @Lastname is not null)
	OR ([Fullname] = @Fullname AND @Fullname is not null)
	OR ([EmailAddressWork] = @EmailAddressWork AND @EmailAddressWork is not null)
	OR ([EmailAddressOther] = @EmailAddressOther AND @EmailAddressOther is not null)
	OR ([DirectDial] = @DirectDial AND @DirectDial is not null)
	OR ([MobilePhoneWork] = @MobilePhoneWork AND @MobilePhoneWork is not null)
	OR ([MobilePhoneOther] = @MobilePhoneOther AND @MobilePhoneOther is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([Postcode] = @Postcode AND @Postcode is not null)
	OR ([Country] = @Country AND @Country is not null)
	OR ([OfficeID] = @OfficeID AND @OfficeID is not null)
	OR ([DepartmentID] = @DepartmentID AND @DepartmentID is not null)
	OR ([JobTitle] = @JobTitle AND @JobTitle is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([Longitude] = @Longitude AND @Longitude is not null)
	OR ([Latitude] = @Latitude AND @Latitude is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Find] TO [sp_executeall]
GO
