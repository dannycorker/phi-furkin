SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SMSQuestionnaire table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_Delete]
(

	@SMSQuestionnaireID int   
)
AS


				DELETE FROM [dbo].[SMSQuestionnaire] WITH (ROWLOCK) 
				WHERE
					[SMSQuestionnaireID] = @SMSQuestionnaireID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Delete] TO [sp_executeall]
GO
