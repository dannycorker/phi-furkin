SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-06-03
-- Description:	Gets the creator of a Lead (by LeadEvent) 
--              Defaults to ProcessStart event sub type.
-- =============================================
CREATE PROCEDURE [dbo].[GetLeadCreator]
	@LeadID int,
	@EventSubtypeID int = 10
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1 LeadEvent.WhoCreated 
	FROM LeadEvent 
	INNER JOIN EventType ON LeadEvent.EventTypeID = EventType.EventTypeID 
	WHERE LeadEvent.LeadID = @LeadID 
	AND EventType.EventSubtypeID = @EventSubtypeID 
	ORDER BY LeadEvent.WhenCreated
END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadCreator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadCreator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadCreator] TO [sp_executeall]
GO
