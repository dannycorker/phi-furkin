SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptLock table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptLockID int   = null ,

	@ScriptID int   = null ,

	@ClientID int   = null ,

	@WhoIsEditing int   = null ,

	@EditStartedAt datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptLockID]
	, [ScriptID]
	, [ClientID]
	, [WhoIsEditing]
	, [EditStartedAt]
    FROM
	[dbo].[ScriptLock] WITH (NOLOCK) 
    WHERE 
	 ([ScriptLockID] = @ScriptLockID OR @ScriptLockID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([WhoIsEditing] = @WhoIsEditing OR @WhoIsEditing IS NULL)
	AND ([EditStartedAt] = @EditStartedAt OR @EditStartedAt IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptLockID]
	, [ScriptID]
	, [ClientID]
	, [WhoIsEditing]
	, [EditStartedAt]
    FROM
	[dbo].[ScriptLock] WITH (NOLOCK) 
    WHERE 
	 ([ScriptLockID] = @ScriptLockID AND @ScriptLockID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([WhoIsEditing] = @WhoIsEditing AND @WhoIsEditing is not null)
	OR ([EditStartedAt] = @EditStartedAt AND @EditStartedAt is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Find] TO [sp_executeall]
GO
