SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SMSQuestionnaire table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_Update]
(

	@SMSQuestionnaireID int   ,

	@ClientID int   ,

	@Title varchar (250)  ,

	@Description varchar (2000)  ,

	@Enabled bit   ,

	@ToTarget varchar (250)  ,

	@CreateCustomerIfNotFound bit   ,

	@UseSentimentAnalysis bit   ,

	@RuleSetID int   ,

	@IsVoice bit   ,

	@Timeout int   ,

	@Repeat int   ,

	@RepeatPrefix varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SMSQuestionnaire]
				SET
					[ClientID] = @ClientID
					,[Title] = @Title
					,[Description] = @Description
					,[Enabled] = @Enabled
					,[ToTarget] = @ToTarget
					,[CreateCustomerIfNotFound] = @CreateCustomerIfNotFound
					,[UseSentimentAnalysis] = @UseSentimentAnalysis
					,[RuleSetID] = @RuleSetID
					,[IsVoice] = @IsVoice
					,[Timeout] = @Timeout
					,[Repeat] = @Repeat
					,[RepeatPrefix] = @RepeatPrefix
				WHERE
[SMSQuestionnaireID] = @SMSQuestionnaireID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Update] TO [sp_executeall]
GO
