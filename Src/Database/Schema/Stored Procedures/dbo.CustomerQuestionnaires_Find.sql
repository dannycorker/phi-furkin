SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CustomerQuestionnaires table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerQuestionnaires_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomerQuestionnaireID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@CustomerID int   = null ,

	@SubmissionDate datetime   = null ,

	@TrackingID int   = null ,

	@Referrer varchar (512)  = null ,

	@SearchTerms varchar (512)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomerQuestionnaireID]
	, [ClientQuestionnaireID]
	, [CustomerID]
	, [SubmissionDate]
	, [TrackingID]
	, [Referrer]
	, [SearchTerms]
	, [ClientID]
    FROM
	[dbo].[CustomerQuestionnaires] WITH (NOLOCK) 
    WHERE 
	 ([CustomerQuestionnaireID] = @CustomerQuestionnaireID OR @CustomerQuestionnaireID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([SubmissionDate] = @SubmissionDate OR @SubmissionDate IS NULL)
	AND ([TrackingID] = @TrackingID OR @TrackingID IS NULL)
	AND ([Referrer] = @Referrer OR @Referrer IS NULL)
	AND ([SearchTerms] = @SearchTerms OR @SearchTerms IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomerQuestionnaireID]
	, [ClientQuestionnaireID]
	, [CustomerID]
	, [SubmissionDate]
	, [TrackingID]
	, [Referrer]
	, [SearchTerms]
	, [ClientID]
    FROM
	[dbo].[CustomerQuestionnaires] WITH (NOLOCK) 
    WHERE 
	 ([CustomerQuestionnaireID] = @CustomerQuestionnaireID AND @CustomerQuestionnaireID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([SubmissionDate] = @SubmissionDate AND @SubmissionDate is not null)
	OR ([TrackingID] = @TrackingID AND @TrackingID is not null)
	OR ([Referrer] = @Referrer AND @Referrer is not null)
	OR ([SearchTerms] = @SearchTerms AND @SearchTerms is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerQuestionnaires_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Find] TO [sp_executeall]
GO
