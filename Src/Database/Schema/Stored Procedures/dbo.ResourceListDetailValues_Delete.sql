SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ResourceListDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_Delete]
(

	@ResourceListDetailValueID int   
)
AS


				DELETE FROM [dbo].[ResourceListDetailValues] WITH (ROWLOCK) 
				WHERE
					[ResourceListDetailValueID] = @ResourceListDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Delete] TO [sp_executeall]
GO
