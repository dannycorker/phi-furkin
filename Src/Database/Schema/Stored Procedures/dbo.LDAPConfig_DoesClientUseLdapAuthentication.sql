SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records with a specific client id from the LDAPConfig table
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LDAPConfig_DoesClientUseLdapAuthentication]
	@ClientID INT
AS
	DECLARE @IsTrue BIT
	SET @IsTrue = 0x0
	IF EXISTS(SELECT * FROM [dbo].[LDAPConfig] WITH (NOLOCK) WHERE Enabled = 1 AND ClientID = @ClientID)
	BEGIN
		SET @IsTrue = 0x1
	END
		
	SELECT @IsTrue

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_DoesClientUseLdapAuthentication] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_DoesClientUseLdapAuthentication] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_DoesClientUseLdapAuthentication] TO [sp_executeall]
GO
