SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-21
-- Description:	Identify objects that our Migration tables don't know about
-- Mods:
-- 2015-03-16 DCM Added 'T', 'F', etc types
-- =============================================
CREATE PROCEDURE [dbo].[_C600_temp_MigrationObjectIdentification] 
	@Type CHAR(1) = 'A',				/* All (A) , Tables (T), Deleted tables (D) Functions (F), Procs (P) */ 
	@AutoPopulate BIT = 0,				/* Create records automatically? */
	@ObjectName VARCHAR(250) = NULL,	/* Allows correction of one object at a time */
	@TableType CHAR(2) = NULL			/* 	RC=RegularWithClientID,
											BC=BespokeWithClientID,
											AS=AquariumStaticData
										*/
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @Type IN ('A','T')
	BEGIN
		;WITH MissingTables AS 
		(
			SELECT ist.* 
			FROM INFORMATION_SCHEMA.TABLES ist 
			WHERE ist.TABLE_TYPE <> 'VIEW' 
			AND NOT EXISTS (
				SELECT * 
				FROM MigrationTable mt 
				WHERE mt.TableName = ist.TABLE_NAME 
			)
		)
		
		SELECT 'Missing Tables' AS [Missing Tables], 'RC=RegularWithClientID' AS [Predicted TYPE], mt.* 
		FROM MissingTables mt 
		WHERE EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = mt.TABLE_NAME
			AND isc.COLUMN_NAME = 'ClientID'
		) 
		AND (@ObjectName IS NULL OR mt.TABLE_NAME = @ObjectName)		/* Let the user pick one table */
		AND (@TableType IS NULL OR @TableType = 'RC')					/* Let the user specify the type manually for all matching tables */
		
		UNION
		
		SELECT 'Missing Tables' AS [Missing Tables], 'BC=BespokeWithClientID' AS [Predicted TYPE], mt.* 
		FROM MissingTables mt 
		WHERE mt.TABLE_NAME LIKE '_C[0-9]_%' 
		AND (@ObjectName IS NULL OR mt.TABLE_NAME = @ObjectName)		/* Let the user pick one table */
		AND (@TableType IS NULL OR @TableType = 'BC')					/* Let the user specify the type manually for all matching tables */
		
		UNION
		
		SELECT 'Missing Tables' AS [Missing Tables], 'AS=AquariumStaticData' AS [Predicted TYPE], mt.* 
		FROM MissingTables mt 
		WHERE mt.TABLE_NAME NOT LIKE '_C[0-9]_%' 
		AND NOT EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = mt.TABLE_NAME
			AND isc.COLUMN_NAME = 'ClientID'
		) 
		AND (@ObjectName IS NULL OR mt.TABLE_NAME = @ObjectName)		/* Let the user pick one table */
		AND (@TableType IS NULL OR @TableType = 'AS')					/* Let the user specify the type manually for all matching tables */
	END
	
	IF @Type IN ('A','D')
	BEGIN
		SELECT 'Deleted Tables' AS [Deleted Tables], mt.* 
		FROM dbo.MigrationTable mt WITH (NOLOCK) 
		WHERE NOT EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.TABLES ist 
			WHERE ist.TABLE_NAME = mt.TableName 
		)
		ORDER BY mt.TableName 
	END
	
	/* Check for any functions that aren't handled in our MigrationProc table */
	IF @Type IN ('A','F')
	BEGIN
		SELECT * 
		FROM INFORMATION_SCHEMA.ROUTINES isr 
		LEFT JOIN dbo.MigrationFunction m ON m.FunctionName = isr.ROUTINE_NAME
		WHERE isr.ROUTINE_TYPE = 'FUNCTION' 
		AND isr.ROUTINE_NAME LIKE 'fn[_]C%'
		AND m.FunctionName IS NULL
	END
	
	/* Check for any procs that aren't handled in our MigrationProc table */
	IF @Type IN ('A','P')
	BEGIN
		SELECT * 
		FROM INFORMATION_SCHEMA.ROUTINES isr 
		LEFT JOIN dbo.MigrationProc m on m.ProcName = isr.ROUTINE_NAME 
		WHERE isr.ROUTINE_TYPE = 'PROCEDURE' 
		AND isr.ROUTINE_NAME LIKE '[_]C[0-9]%'
		AND isr.ROUTINE_NAME NOT LIKE '[_]C00[_]%'
		AND m.ProcName IS NULL 
	END
	

	IF @AutoPopulate = 1
	BEGIN
		
		/*
			Regular tables with a ClientID column
		*/
		INSERT INTO [dbo].[MigrationTable]
		([TableName]
		,[FKToClientIDTableName]
		,[ClientID]
		,[DeleteText]
		,[IsClearedDown]
		,[IsComplete]
		,[AlwaysDrop]
		,[NeverDrop]
		,[Notes]
		,[ExecutionOrder]
		,[ClientGoneDropText]
		,[ClientGoneDeleteText]
		,[DeleteThisManyAtATime]
		,[IsEnum]
		,[IsAquariumStaticData]
		,[IgnoreForRepopulation]
		,[CompareStandalone]
		,[ManualUpdateStandalone]
		,[CustomerIDIfSpecific]
		,[LeadIDIfSpecific]
		,[CaseIDIfSpecific]
		,[MatterIDIfSpecific]
		) 
		OUTPUT inserted.* 
		SELECT
		ist.TABLE_NAME,
		'',
		0,
		'DELETE dbo.[' + table_name + '] WHERE [ClientID] NOT IN (0, @ClientID)', 
		0,
		0,
		0,
		0,
		'Delete by ClientID',
		500,
		NULL,
		'DELETE dbo.[' + table_name + '] WHERE [ClientID] = @ClientID', 
		NULL, 
		0,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		0,
		0,
		0
		FROM INFORMATION_SCHEMA.TABLES ist 
		/* No MigrationTable entry, ClientID column, not a VIEW, not called _C123_Table etc */
		WHERE NOT EXISTS (
			SELECT * 
			FROM MigrationTable mt 
			WHERE mt.TableName = ist.TABLE_NAME 
		)
		AND EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = ist.TABLE_NAME
			AND isc.COLUMN_NAME = 'ClientID'
		) 
		AND ist.TABLE_TYPE <> 'VIEW' 
		AND ist.TABLE_NAME NOT LIKE '_C[0-9]%' 
		AND (@ObjectName IS NULL OR ist.TABLE_NAME = @ObjectName)
		AND (@TableType IS NULL OR @TableType = 'RC')
		ORDER BY ist.TABLE_NAME
		
		
		/* 
			_C123_ Client specific tables 
		*/
		INSERT INTO dbo.MigrationTable 
		([TableName]
		,[FKToClientIDTableName]
		,[ClientID]
		,[DropText]
		,[IsClearedDown]
		,[IsComplete]
		,[AlwaysDrop]
		,[NeverDrop]
		,[Notes]
		,[ExecutionOrder]
		,[ClientGoneDropText]
		,[ClientGoneDeleteText]
		,[DeleteThisManyAtATime]
		,[IsEnum]
		,[IsAquariumStaticData]
		,[IgnoreForRepopulation]
		,[CompareStandalone]
		,[ManualUpdateStandalone]
		,[CustomerIDIfSpecific]
		,[LeadIDIfSpecific]
		,[CaseIDIfSpecific]
		,[MatterIDIfSpecific]
		)
		OUTPUT inserted.* 
		SELECT 
		ist.TABLE_NAME,
		'',
		CONVERT(INT, SUBSTRING(ist.TABLE_NAME, 3, CHARINDEX('_', ist.TABLE_NAME, 3)-3)),
		'DROP TABLE dbo.' + table_name,
		0,
		0,
		0,
		0,
		'Bespoke Table', 
		0,
		'DROP TABLE dbo.' + table_name,
		NULL,
		NULL, 
		0,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		0,
		0,
		0 
		FROM INFORMATION_SCHEMA.TABLES ist 
		/* No MigrationTable entry, not a VIEW, is called _C123_Table etc */
		WHERE NOT EXISTS (
			SELECT * 
			FROM MigrationTable mt 
			WHERE mt.TableName = ist.TABLE_NAME 
		)
		AND ist.TABLE_TYPE <> 'VIEW' 
		AND ist.TABLE_NAME LIKE '_C[0-9]_%' 
		AND (@ObjectName IS NULL OR ist.TABLE_NAME = @ObjectName)
		AND (@TableType IS NULL OR @TableType = 'BC')


		/* 
			Aquarium Static Data 
		*/
		INSERT INTO dbo.MigrationTable 
		([TableName]
		,[FKToClientIDTableName]
		,[ClientID]
		,[IsClearedDown]
		,[IsComplete]
		,[AlwaysDrop]
		,[NeverDrop]
		,[Notes]
		,[IsEnum]
		,[IsAquariumStaticData]
		,[IgnoreForRepopulation]
		,[CompareStandalone]
		,[ManualUpdateStandalone]
		,[CustomerIDIfSpecific]
		,[LeadIDIfSpecific]
		,[CaseIDIfSpecific]
		,[MatterIDIfSpecific]
		)
		OUTPUT inserted.* 
		SELECT
		ist.TABLE_NAME,
		'',
		0,
		0,
		0,
		0,
		1,
		'Aquarium Static Data',
		0,
		1,
		NULL,
		1,
		0,
		0,
		0,
		0,
		0
		FROM INFORMATION_SCHEMA.TABLES ist 
		/* No MigrationTable entry, no ClientID column, not a VIEW, not called _C123_Table etc */
		WHERE NOT EXISTS (
			SELECT * 
			FROM MigrationTable mt 
			WHERE mt.TableName = ist.TABLE_NAME 
		)
		AND NOT EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = ist.TABLE_NAME
			AND isc.COLUMN_NAME = 'ClientID'
		) 
		AND ist.TABLE_TYPE <> 'VIEW' 
		AND ist.TABLE_NAME NOT LIKE '_C[0-9]%' 
		AND (@ObjectName IS NULL OR ist.TABLE_NAME = @ObjectName)
		AND (@TableType IS NULL OR @TableType = 'AS')
		ORDER BY ist.TABLE_NAME
						
		
		/* 
			Add missing Functions 
		*/
		;WITH InnerSql AS 
		(
			SELECT isr.ROUTINE_NAME 
			FROM INFORMATION_SCHEMA.ROUTINES isr 
			LEFT JOIN dbo.MigrationFunction m on m.FunctionName = isr.ROUTINE_NAME 
			WHERE isr.ROUTINE_TYPE = 'FUNCTION' 
			AND isr.ROUTINE_NAME LIKE 'fn[_]C%' 
			AND m.FunctionName IS NULL 
		)
		INSERT INTO dbo.MigrationFunction (FunctionName, DropText, ClientID, IsComplete)
		SELECT i.ROUTINE_NAME, 'DROP FUNCTION [dbo].[' + i.ROUTINE_NAME + ']', CONVERT(int, substring(i.ROUTINE_NAME, 5, CHARINDEX('_', i.ROUTINE_NAME, 4)-5)), 0
		FROM InnerSql i 
		
		/* 
			Add missing Procs 
		*/
		;WITH InnerSql AS 
		(
			SELECT isr.ROUTINE_NAME 
			FROM INFORMATION_SCHEMA.ROUTINES isr 
			LEFT JOIN dbo.MigrationProc m on m.ProcName = isr.ROUTINE_NAME 
			WHERE isr.ROUTINE_TYPE = 'PROCEDURE' 
			AND isr.ROUTINE_NAME LIKE '[_]C[0-9]%'
			AND isr.ROUTINE_NAME NOT LIKE '[_]C00[_]%'
			AND m.ProcName IS NULL 
		)
		INSERT INTO dbo.MigrationProc (ProcName, DropText, ClientID, IsComplete)
		SELECT i.ROUTINE_NAME, 'DROP PROC [dbo].[' + i.ROUTINE_NAME + ']', CONVERT(int, substring(i.ROUTINE_NAME, 3, CHARINDEX('_', i.ROUTINE_NAME, 2)-3)), 0
		FROM InnerSql i 

		/* 
			Delete from MigrationTable where the real tables have been dropped now 
		*/
		DELETE dbo.MigrationTable 
		FROM dbo.MigrationTable mt 
		WHERE NOT EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.TABLES ist 
			WHERE ist.TABLE_NAME = mt.TableName 
		)
		
		/* 
			Delete from MigrationFunction where the real functions have been dropped now 
		*/
		DELETE dbo.MigrationFunction 
		FROM dbo.MigrationFunction mt 
		WHERE NOT EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.ROUTINES ist 
			WHERE ist.ROUTINE_TYPE = 'FUNCTION' 
			AND ist.ROUTINE_NAME = mt.FunctionName 
		)
		
		/* 
			Delete from MigrationProc where the real procs have been dropped now 
		*/
		DELETE dbo.MigrationProc 
		FROM dbo.MigrationProc mt 
		WHERE NOT EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.ROUTINES ist 
			WHERE ist.ROUTINE_TYPE = 'PROCEDURE' 
			AND ist.ROUTINE_NAME = mt.ProcName 
		)
		
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_temp_MigrationObjectIdentification] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_temp_MigrationObjectIdentification] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_temp_MigrationObjectIdentification] TO [sp_executeall]
GO
