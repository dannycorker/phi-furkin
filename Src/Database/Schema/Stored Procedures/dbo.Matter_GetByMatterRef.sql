SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Matter table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_GetByMatterRef]
(

	@MatterRef varchar (100)  
)
AS


				SELECT
					[MatterID],
					[ClientID],
					[MatterRef],
					[CustomerID],
					[LeadID],
					[MatterStatus],
					[RefLetter],
					[BrandNew],
					[CaseID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[Matter] WITH (NOLOCK) 
				WHERE
										[MatterRef] = @MatterRef
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByMatterRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_GetByMatterRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByMatterRef] TO [sp_executeall]
GO
