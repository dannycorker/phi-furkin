SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryCriteriaSetMember table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryCriteriaSetMemberID int   = null ,

	@ClientID int   = null ,

	@SqlQueryID int   = null ,

	@SqlQueryCriteriaSetID int   = null ,

	@SqlQueryColumnID int   = null ,

	@Criteria1 varchar (250)  = null ,

	@Criteria2 varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryCriteriaSetMemberID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryCriteriaSetID]
	, [SqlQueryColumnID]
	, [Criteria1]
	, [Criteria2]
    FROM
	[dbo].[SqlQueryCriteriaSetMember] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryCriteriaSetMemberID] = @SqlQueryCriteriaSetMemberID OR @SqlQueryCriteriaSetMemberID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryID] = @SqlQueryID OR @SqlQueryID IS NULL)
	AND ([SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID OR @SqlQueryCriteriaSetID IS NULL)
	AND ([SqlQueryColumnID] = @SqlQueryColumnID OR @SqlQueryColumnID IS NULL)
	AND ([Criteria1] = @Criteria1 OR @Criteria1 IS NULL)
	AND ([Criteria2] = @Criteria2 OR @Criteria2 IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryCriteriaSetMemberID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryCriteriaSetID]
	, [SqlQueryColumnID]
	, [Criteria1]
	, [Criteria2]
    FROM
	[dbo].[SqlQueryCriteriaSetMember] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryCriteriaSetMemberID] = @SqlQueryCriteriaSetMemberID AND @SqlQueryCriteriaSetMemberID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryID] = @SqlQueryID AND @SqlQueryID is not null)
	OR ([SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID AND @SqlQueryCriteriaSetID is not null)
	OR ([SqlQueryColumnID] = @SqlQueryColumnID AND @SqlQueryColumnID is not null)
	OR ([Criteria1] = @Criteria1 AND @Criteria1 is not null)
	OR ([Criteria2] = @Criteria2 AND @Criteria2 is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Find] TO [sp_executeall]
GO
