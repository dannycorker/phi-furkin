SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2018-04-10
-- Description:	Billing Housekeeping to Mark claim rows as paid. Temp proc until we can replace the basic table at Matter level
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_Claims_Housekeeping]
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,MatterID,ClientID) 
	SELECT r.TableRowID,154521,CONVERT(DATE,dbo.fn_GetDate_Local(),121),cp.RelatedObjectID,r.ClientID
	FROM CustomerPaymentSchedule cp WITH ( NOLOCK ) 
	INNER JOIN TableRows r WITH ( NOLOCK ) on r.TableRowID = cp.SourceID /*TableRow claim created from*/ 
	LEFT JOIN TableDetailValues tdvPaymentRun WITH ( NOLOCK ) on tdvPaymentRun.TableRowId = r.TableRowID AND tdvPaymentRun.DetailFieldID = 154521
	INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = cp.RelatedObjectID
	INNER JOIN Lead l WITH ( NOLOCK ) on l.LeadID = m.LeadID
	WHERE cp.PaymentStatusID in (2,6) 
	AND ISNULL(tdvPaymentRun.ValueDate,'0') = '0' 
	AND l.LeadTypeID = 1490 /*Ensure this is a claims Matter*/
	and cp.RelatedObjectTypeID = 2 
	and cp.ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_Claims_Housekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_Claims_Housekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_Claims_Housekeeping] TO [sp_executeall]
GO
