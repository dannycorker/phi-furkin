SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeHelperField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeHelperField_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeHelperFieldID],
					[EventTypeID],
					[DetailFieldID],
					[LeadTypeID],
					[ClientID],
					[ForceEditableOverride]
				FROM
					[dbo].[EventTypeHelperField] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_GetByClientID] TO [sp_executeall]
GO
