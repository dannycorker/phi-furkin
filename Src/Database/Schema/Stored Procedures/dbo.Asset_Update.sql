SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Asset table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Asset_Update]
(

	@AssetID int   ,

	@ClientID int   ,

	@AssetTypeID int   ,

	@AssetSubTypeID int   ,

	@LocationID int   ,

	@AssetName varchar (255)  ,

	@Note varchar (255)  ,

	@SecureNote varchar (255)  ,

	@SecureUserName varchar (255)  ,

	@SecurePassword varchar (255)  ,

	@SecureOther varchar (255)  ,

	@Version varchar (255)  ,

	@ValidFrom datetime   ,

	@ValidTo datetime   ,

	@ReminderDue datetime   ,

	@ReminderTimeUnitID int   ,

	@ReminderTimeUnitQuantity int   ,

	@ReminderNotificationGroupID int   ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Asset]
				SET
					[ClientID] = @ClientID
					,[AssetTypeID] = @AssetTypeID
					,[AssetSubTypeID] = @AssetSubTypeID
					,[LocationID] = @LocationID
					,[AssetName] = @AssetName
					,[Note] = @Note
					,[SecureNote] = @SecureNote
					,[SecureUserName] = @SecureUserName
					,[SecurePassword] = @SecurePassword
					,[SecureOther] = @SecureOther
					,[Version] = @Version
					,[ValidFrom] = @ValidFrom
					,[ValidTo] = @ValidTo
					,[ReminderDue] = @ReminderDue
					,[ReminderTimeUnitID] = @ReminderTimeUnitID
					,[ReminderTimeUnitQuantity] = @ReminderTimeUnitQuantity
					,[ReminderNotificationGroupID] = @ReminderNotificationGroupID
					,[Enabled] = @Enabled
					,[Deleted] = @Deleted
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[AssetID] = @AssetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_Update] TO [sp_executeall]
GO
