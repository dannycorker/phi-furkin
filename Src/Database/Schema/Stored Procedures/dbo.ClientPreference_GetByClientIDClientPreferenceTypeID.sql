SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPreference table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreference_GetByClientIDClientPreferenceTypeID]
(

	@ClientID int   ,

	@ClientPreferenceTypeID int   
)
AS


				SELECT
					[ClientPreferenceID],
					[ClientID],
					[ClientPreferenceTypeID],
					[PreferenceValue]
				FROM
					[dbo].[ClientPreference] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [ClientPreferenceTypeID] = @ClientPreferenceTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_GetByClientIDClientPreferenceTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreference_GetByClientIDClientPreferenceTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_GetByClientIDClientPreferenceTypeID] TO [sp_executeall]
GO
