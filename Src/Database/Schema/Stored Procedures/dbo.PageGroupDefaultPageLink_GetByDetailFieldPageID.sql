SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PageGroupDefaultPageLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroupDefaultPageLink_GetByDetailFieldPageID]
(

	@DetailFieldPageID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PageGroupDefaultPageLinkID],
					[ClientID],
					[PageGroupID],
					[DetailFieldPageID],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[PageGroupDefaultPageLink] WITH (NOLOCK) 
				WHERE
					[DetailFieldPageID] = @DetailFieldPageID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_GetByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroupDefaultPageLink_GetByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_GetByDetailFieldPageID] TO [sp_executeall]
GO
