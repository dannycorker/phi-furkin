SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Inserts a record in CustomerMatchKey
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMatchKey__Insert]

	@ClientID INT,
	@CustomerID INT,
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250)

AS
BEGIN

	IF @DateOfBirth = ''
	BEGIN
	
		SET @DateOfBirth = NULL
	
	END

	INSERT INTO CustomerMatchKey (ClientID, CustomerID, Firstname, Lastname, Address1, Postcode, DateOfBirth)
	VALUES (@ClientID, @CustomerID, @Firstname, @Lastname, @Address1, @Postcode, @DateOfBirth)
	
	SELECT SCOPE_IDENTITY()
	


END



GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMatchKey__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__Insert] TO [sp_executeall]
GO
