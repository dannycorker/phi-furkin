SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------
-- Date Created: 02 August 2007
-- Date Updated: 07 July 2015
-- Created By: Alistair Jones
-- Purpose: Select record detail from the EventTypeMandatoryField table through a foreign key
-- 07/07/2015 update: Returns ForcedOverride as well by A.P
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField__GetDetailByEventTypeID]
(
	@EventTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					EventTypeMandatoryField.[EventTypeMandatoryFieldID],
					EventTypeMandatoryField.[EventTypeID],
					DetailFields.[FieldName],
					EventTypeMandatoryField.[DetailFieldID],
					EventTypeMandatoryField.[LeadTypeID],
					EventTypeMandatoryField.[ClientID],
					EventTypeMandatoryField.[ForceEditableOverride]
				FROM
					dbo.[EventTypeMandatoryField]
					inner join dbo.[DetailFields] on dbo.[EventTypeMandatoryField].DetailFieldID = dbo.[DetailFields].DetailFieldID
				WHERE
					EventTypeMandatoryField.[EventTypeID] = @EventTypeID
				ORDER BY
					DetailFields.[FieldName]
					
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField__GetDetailByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField__GetDetailByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField__GetDetailByEventTypeID] TO [sp_executeall]
GO
