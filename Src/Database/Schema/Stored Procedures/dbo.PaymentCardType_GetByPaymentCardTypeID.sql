SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PaymentCardType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentCardType_GetByPaymentCardTypeID]
(

	@PaymentCardTypeID int   
)
AS


				SELECT
					[PaymentCardTypeID],
					[PaymentCardType],
					[DisplayOrder]
				FROM
					[dbo].[PaymentCardType] WITH (NOLOCK) 
				WHERE
										[PaymentCardTypeID] = @PaymentCardTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_GetByPaymentCardTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentCardType_GetByPaymentCardTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_GetByPaymentCardTypeID] TO [sp_executeall]
GO
