SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AquariumOptionType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOptionType_Find]
(

	@SearchUsingOR bit   = null ,

	@AquariumOptionTypeID int   = null ,

	@AquariumOptionTypeName varchar (50)  = null ,

	@AquariumOptionTypeDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AquariumOptionTypeID]
	, [AquariumOptionTypeName]
	, [AquariumOptionTypeDescription]
    FROM
	[dbo].[AquariumOptionType] WITH (NOLOCK) 
    WHERE 
	 ([AquariumOptionTypeID] = @AquariumOptionTypeID OR @AquariumOptionTypeID IS NULL)
	AND ([AquariumOptionTypeName] = @AquariumOptionTypeName OR @AquariumOptionTypeName IS NULL)
	AND ([AquariumOptionTypeDescription] = @AquariumOptionTypeDescription OR @AquariumOptionTypeDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AquariumOptionTypeID]
	, [AquariumOptionTypeName]
	, [AquariumOptionTypeDescription]
    FROM
	[dbo].[AquariumOptionType] WITH (NOLOCK) 
    WHERE 
	 ([AquariumOptionTypeID] = @AquariumOptionTypeID AND @AquariumOptionTypeID is not null)
	OR ([AquariumOptionTypeName] = @AquariumOptionTypeName AND @AquariumOptionTypeName is not null)
	OR ([AquariumOptionTypeDescription] = @AquariumOptionTypeDescription AND @AquariumOptionTypeDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOptionType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Find] TO [sp_executeall]
GO
