SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptSectionNavigationCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria_Update]
(

	@ScriptSectionNavigationCriteriaID int   ,

	@ClientID int   ,

	@FromStepSectionID int   ,

	@ToStepSectionID int   ,

	@WizardButton varchar (12)  ,

	@DetailFieldID int   ,

	@CriteriaOperator varchar (12)  ,

	@Value varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptSectionNavigationCriteria]
				SET
					[ClientID] = @ClientID
					,[FromStepSectionID] = @FromStepSectionID
					,[ToStepSectionID] = @ToStepSectionID
					,[WizardButton] = @WizardButton
					,[DetailFieldID] = @DetailFieldID
					,[CriteriaOperator] = @CriteriaOperator
					,[Value] = @Value
				WHERE
[ScriptSectionNavigationCriteriaID] = @ScriptSectionNavigationCriteriaID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Update] TO [sp_executeall]
GO
