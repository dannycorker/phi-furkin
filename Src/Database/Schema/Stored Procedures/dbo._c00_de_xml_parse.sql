SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--CREATE 
CREATE PROCEDURE [dbo].[_c00_de_xml_parse]
(
    @req_xml XML,
    @key varchar(50) = NULL,
    @p_entity varchar(50) = NULL,
    @p_key varchar(50) = NULL,
    @rn int = 0
) AS
BEGIN
    DECLARE @WorkTable TABLE (child_xml xml, rn int)

    SELECT @key = ISNULL(@key, NEWID())

    -- output current level
    SELECT  
        @key [key],
        CAST(t.c.query('local-name(..)') AS VARCHAR(50)) [entity],
        CAST(t.c.query('local-name(.)') AS VARCHAR(50)) [col],
        t.c.value('.','VARCHAR(MAX)') [val],
        @p_entity [p_entity],
        @p_key [p_key],
        @rn [rn]
    FROM    @req_xml.nodes('/*/@*') as t(c)

    -- set current level entity/key
    SELECT TOP (1)
        @p_entity = CAST(t.c.query('local-name(..)') AS VARCHAR(50))
    FROM @req_xml.nodes('/*/@*') as t(c)
    
    -- loop and recurse each child entity
    INSERT INTO @WorkTable (child_xml, rn)
    SELECT	
        t.c.query('.') [child_xml],
        ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) [rn]
    FROM	@req_xml.nodes('*/*') as t(c)

    WHILE EXISTS (SELECT * FROM @WorkTable)
    BEGIN
        SELECT  TOP(1) 
            @req_xml = [child_xml]
        FROM    @WorkTable 
        ORDER BY [rn]

        SELECT @rn += 1
        EXEC [dbo].[_c00_de_xml_parse] @req_xml, null, @p_entity, @key, @rn

        DELETE [ds] FROM (SELECT TOP(1) * FROM @WorkTable ORDER BY [rn]) [ds]
    END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_c00_de_xml_parse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_c00_de_xml_parse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_c00_de_xml_parse] TO [sp_executeall]
GO
