SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-03
-- Description:	Get DataloaderMap Layout
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetDataLoaderMapLayout] 
	@ClientID INT,
	@MapID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @cols VARCHAR(MAX)=''
	
	SELECT @cols = @cols + ',' + QUOTENAME(dbo.fnRefLetterFromCaseNum(dlm.SectionAbsoluteCol+1))
	FROM dbo.DataLoaderFieldDefinition dlm
	LEFT JOIN DataLoaderObjectField dlof WITH (NOLOCK) ON dlof.DataLoaderObjectFieldID = dlm.DataLoaderObjectFieldID
	LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dlm.DetailFieldID
	WHERE DataLoaderMapID = @MapID
	AND dlm.ClientID = @ClientID
	AND dlm.SectionAbsoluteCol IS NOT NULL
	AND NOT EXISTS (
		SELECT *
		FROM DataLoaderFieldDefinition dlf2 WITH (NOLOCK)
		WHERE dlf2.DataLoaderMapID = dlm.DataLoaderMapID
		AND dlf2.SectionAbsoluteCol = dlm.SectionAbsoluteCol
		AND dlf2.DataLoaderFieldDefinitionID < dlm.DataLoaderFieldDefinitionID
	)
	ORDER BY dlm.SectionAbsoluteCol

	DECLARE @query VARCHAR(MAX)

	SELECT @cols=STUFF(@cols, 1, 1, '')
	
	SET @query = 'SELECT * From ('
	
	SELECT @query = @query+ '
		SELECT DISTINCT dbo.fnRefLetterFromCaseNum(dlm.SectionAbsoluteCol+1) AS [Column Letter], COALESCE(df.FieldCaption, dlm.NamedValue, dlof.FieldName) AS [Data Required]
		FROM dbo.DataLoaderFieldDefinition dlm
		LEFT JOIN DataLoaderObjectField dlof WITH (NOLOCK) ON dlof.DataLoaderObjectFieldID = dlm.DataLoaderObjectFieldID
		LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dlm.DetailFieldID
		WHERE DataLoaderMapID = ' + CONVERT(VARCHAR,@MapID) + '
		AND dlm.SectionAbsoluteCol IS NOT NULL
		'
		
	SELECT @query += ') src
	PIVOT (MAX([Data Required]) for [Column Letter] in (' + @cols + '))	as pvt
	'

	EXEC(@query)

	PRINT @query
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataLoaderMapLayout] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetDataLoaderMapLayout] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataLoaderMapLayout] TO [sp_executeall]
GO
