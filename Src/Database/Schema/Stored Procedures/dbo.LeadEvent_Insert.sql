SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_Insert]
(

	@LeadEventID int    OUTPUT,

	@ClientID int   ,

	@LeadID int   ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@Cost money   ,

	@Comments varchar (2000)  ,

	@EventTypeID int   ,

	@NoteTypeID int   ,

	@FollowupDateTime datetime   ,

	@WhenFollowedUp datetime   ,

	@AquariumEventType int   ,

	@NextEventID int   ,

	@CaseID int   ,

	@LeadDocumentID int   ,

	@NotePriority int   ,

	@DocumentQueueID int   ,

	@EventDeleted bit   ,

	@WhoDeleted int   ,

	@DeletionComments varchar (2000)  ,

	@ContactID int   ,

	@BaseCost money   ,

	@DisbursementCost money   ,

	@DisbursementDescription varchar (200)  ,

	@ChargeOutRate money   ,

	@UnitsOfEffort int   ,

	@CostEnteredManually bit   ,

	@IsOnHold bit   ,

	@HoldLeadEventID int   
)
AS


				
				INSERT INTO [dbo].[LeadEvent]
					(
					[ClientID]
					,[LeadID]
					,[WhenCreated]
					,[WhoCreated]
					,[Cost]
					,[Comments]
					,[EventTypeID]
					,[NoteTypeID]
					,[FollowupDateTime]
					,[WhenFollowedUp]
					,[AquariumEventType]
					,[NextEventID]
					,[CaseID]
					,[LeadDocumentID]
					,[NotePriority]
					,[DocumentQueueID]
					,[EventDeleted]
					,[WhoDeleted]
					,[DeletionComments]
					,[ContactID]
					,[BaseCost]
					,[DisbursementCost]
					,[DisbursementDescription]
					,[ChargeOutRate]
					,[UnitsOfEffort]
					,[CostEnteredManually]
					,[IsOnHold]
					,[HoldLeadEventID]
					)
				VALUES
					(
					@ClientID
					,@LeadID
					,@WhenCreated
					,@WhoCreated
					,@Cost
					,@Comments
					,@EventTypeID
					,@NoteTypeID
					,@FollowupDateTime
					,@WhenFollowedUp
					,@AquariumEventType
					,@NextEventID
					,@CaseID
					,@LeadDocumentID
					,@NotePriority
					,@DocumentQueueID
					,@EventDeleted
					,@WhoDeleted
					,@DeletionComments
					,@ContactID
					,@BaseCost
					,@DisbursementCost
					,@DisbursementDescription
					,@ChargeOutRate
					,@UnitsOfEffort
					,@CostEnteredManually
					,@IsOnHold
					,@HoldLeadEventID
					)
				-- Get the identity value
				SET @LeadEventID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Insert] TO [sp_executeall]
GO
