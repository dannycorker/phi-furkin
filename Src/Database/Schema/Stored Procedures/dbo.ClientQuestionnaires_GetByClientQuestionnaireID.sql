SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientQuestionnaires table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaires_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SELECT
					[ClientQuestionnaireID],
					[ClientID],
					[QuestionnaireTitle],
					[QuestionnaireDescription],
					[QuestionsPerPage],
					[QuestionnaireLogo],
					[QuestionnaireLogoFileName],
					[QuestionnaireFlowDiagram],
					[QuestionnaireIntroductionText],
					[HideIntro],
					[QuestionnaireFooterText],
					[HideFooter],
					[QuestionnaireAddressLookup],
					[Published],
					[CustomerInformationAtStart],
					[LinkedQuestionnaireClientQuestionnaireID],
					[QuestionnaireFooterIframe],
					[QuestionnaireHeaderIframe],
					[QuestionnaireFooterInternal],
					[QuestionnaireHeaderInternal],
					[QuestionnaireFooterIframeHeight],
					[QuestionnaireHeaderIframeHeight],
					[QuestionnaireFooterIframeWidth],
					[QuestionnaireHeaderIframeWidth],
					[QuestionnaireBackGroundImage],
					[QuestionnaireBackGroundImageFileName],
					[DefaultEmailAddress],
					[MailingListType],
					[FrameMode],
					[LayoutCss],
					[LayoutCssFileName],
					[ImportDirectlyIntoLeadManager],
					[RunAsClientPersonnelID],
					[RememberAnswers],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ClientQuestionnaires] WITH (NOLOCK) 
				WHERE
										[ClientQuestionnaireID] = @ClientQuestionnaireID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_GetByClientQuestionnaireID] TO [sp_executeall]
GO
