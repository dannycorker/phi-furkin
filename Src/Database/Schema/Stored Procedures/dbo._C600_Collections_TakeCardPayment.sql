SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Dave Morgan
-- Create date: 29/09/2016
-- Description:	Picks the first card payment available and queues it. If there are more it adds the queue event again
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_TakeCardPayment]
(
	@LeadEventID INT,
	@MatterID INT,
	@IsPayment BIT
)
AS
BEGIN

--declare @matterID int = 1306,
--		@LeadEventID INT = 19276,
--		@IsPayment INT = 1

	SET NOCOUNT ON;
	
DECLARE @LogEntry VARCHAR(2000)
SELECT @LogEntry='Args: LeadEventID - ' + CONVERT(VARCHAR(30),@LeadEventID) + 
				'; MatterID - ' + CONVERT(VARCHAR(30),@MatterID)  + 
				'; IsPayment - ' + CONVERT(VARCHAR(30),@IsPayment) 
EXEC _C00_LogIt 'Info', '_C600_Collections_TakeCardPayment', 'Collecting Test Data', @LogEntry, 2372 

	DECLARE @Payments TABLE ( PaymentScheduleID INT, MatterID INT, CardNumber VARCHAR(20), ExpMonth VARCHAR(2), ExpYear VARCHAR(4), CardAuth VARCHAR(2000), CardName VARCHAR(100), Done INT )

	DECLARE 
			@AccountID INT,
			@CardAuth VARCHAR(2000), 
			@CardName VARCHAR(100),
			@CardNumber VARCHAR(20), 
			@ClientID INT,
			@DetailValue VARCHAR(200), 
			@ExpMonth VARCHAR(2), 
			@ExpYear VARCHAR(4), 
			@LeadTypeID INT,
			@ObjectID INT,
			@PaymentScheduleID INT,
			@PaymentTypeID VARCHAR(30),
			@SettlementDate DATE

	
	SELECT @LeadTypeID=l.LeadTypeID, @ClientID=l.ClientID 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=m.LeadID
	WHERE MatterID=@MatterID
	
	INSERT INTO @Payments ( PaymentScheduleID, MatterID, CardNumber, ExpMonth, ExpYear, CardAuth, CardName, Done )
	SELECT cps.CustomerPaymentScheduleID, m.MatterID, 
	ctnum.DetailValue,
	RIGHT('0' + CONVERT(VARCHAR(2),DATEPART(MONTH,ctexp.ValueDate)),2),
	CONVERT(VARCHAR(4),DATEPART(YEAR,ctexp.ValueDate)),
	ctauth.DetailValue,
	ctname.DetailValue, 0
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
	INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID
	LEFT JOIN MatterDetailValues suspended WITH (NOLOCK) ON m.MatterID=suspended.MatterID AND suspended.DetailFieldID=175275
	INNER JOIN MatterDetailValues paymeth WITH (NOLOCK) ON m.MatterID=paymeth.MatterID AND paymeth.DetailFieldID=170115
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToMatterID=m.MatterID AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1493
	INNER JOIN Matter pam WITH (NOLOCK) ON ltr.FromMatterID=pam.MatterID
	INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON pam.MatterID=pstat.MatterID AND pstat.DetailFieldID=170038
	INNER JOIN Cases pac WITH (NOLOCK) ON pam.CaseID=pac.CaseID
	INNER JOIN MatterDetailValues aid WITH (NOLOCK) ON m.MatterID=aid.MatterID AND aid.DetailFieldID=176973
	INNER JOIN Account a WITH (NOLOCK) ON aid.ValueInt=a.AccountID
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON a.AccountID=cps.AccountID
	INNER JOIN MatterDetailValues ctnum WITH (NOLOCK) ON m.MatterID=ctnum.MatterID AND ctnum.DetailFieldID=170193
	INNER JOIN MatterDetailValues ctexp WITH (NOLOCK) ON m.MatterID=ctexp.MatterID AND ctexp.DetailFieldID=170194
	INNER JOIN MatterDetailValues ctauth WITH (NOLOCK) ON m.MatterID=ctauth.MatterID AND ctauth.DetailFieldID=170191
	INNER JOIN MatterDetailValues ctname WITH (NOLOCK) ON m.MatterID=ctname.MatterID AND ctname.DetailFieldID=170192
	WHERE  c.Test=0 AND c.ClientID=@ClientID
	-- and not cancelled
	AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=pac.CaseID AND le.EventTypeID=150145 AND le.WhenFollowedUp IS NULL AND le.EventDeleted=0 )
	-- and we are doing collections for this customer
	AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=ca.CaseID AND le.EventDeleted=0 AND le.EventTypeID=155198 )
	-- and collections are not on hold
	AND ( suspended.ValueInt <> 5144 OR suspended.ValueInt IS NULL )
	-- and policy status is live
	AND pstat.ValueInt=43002
	-- and payment schedule entry has not already been queued in the GL
	AND cps.CustomerLedgerID IS NULL
	-- and payment date is before or the same as settlement day
	AND cps.ActualCollectionDate <= CONVERT(DATE,dbo.fn_GetDate_Local())
	-- and just for this matter
	AND m.MatterID = @MatterID
	-- and payment value is not zero
	AND ( (cps.PaymentGross>0 AND @IsPayment=1) OR
		  (cps.PaymentGross<0 AND @IsPayment=0) )
		  
	SELECT TOP 1 @MatterID=p.MatterID, 
				 @PaymentScheduleID=p.PaymentScheduleID,
				 @CardNumber=CardNumber, 
				 @ExpMonth=ExpMonth, 
				 @ExpYear=ExpYear, 
				 @CardAuth=CardAuth, 
				 @CardName=CardName
	FROM @Payments p 
	WHERE p.Done=0
	ORDER BY p.PaymentScheduleID
	
	DECLARE @PaymentTypes TABLE ( PaymentType INT, PaymentDescription VARCHAR(500), RNO INT )
	INSERT INTO @PaymentTypes
	SELECT ppps.PurchasedProductPaymentScheduleTypeID,
		   pname.DetailValue + ' - ' +
		   CASE WHEN pppst.PurchasedProductPaymentScheduleTypeName IS NULL THEN 'Scheduled' 
				ELSE pppst.PurchasedProductPaymentScheduleTypeName END,
		   ROW_NUMBER() OVER(PARTITION BY ppps.PurchasedProductPaymentScheduleTypeID, pname.DetailValue ORDER BY ppps.CustomerPaymentScheduleID ASC) as [RNO] 
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	LEFT JOIN PurchasedProductPaymentScheduleType pppst WITH (NOLOCK) ON ppps.PurchasedProductPaymentScheduleTypeID=pppst.PurchasedProductPaymentScheduleTypeID
	INNER JOIN MatterDetailValues ppid WITH (NOLOCK) ON ppps.PurchasedProductID=ppid.ValueInt AND ppid.DetailFieldID=177074
	INNER JOIN Matter m WITH (NOLOCK) ON ppid.MatterID=m.MatterID
	INNER JOIN LeadDetailValues pname WITH (NOLOCK) ON m.LeadID=pname.LeadID AND pname.DetailFieldID=144268
	WHERE ppps.CustomerPaymentScheduleID=@PaymentScheduleID
	
	SELECT @DetailValue=NULL
	SELECT @DetailValue = COALESCE(@DetailValue + ', ', '') + p.PaymentDescription + ' (' + CAST(p.RNO AS VARCHAR) + ' item)'  
	FROM @PaymentTypes p
	WHERE NOT EXISTS ( SELECT * FROM @PaymentTypes p2 WHERE p.RNO < p2.RNO AND p.PaymentDescription=p2.PaymentDescription )
	ORDER BY PaymentDescription

	UPDATE LeadEvent Set Comments = Comments 
										+ CASE WHEN Comments='' THEN '' ELSE ' ' END 
										+ 'Collecting CustomerPaymentScheduleID: ' 
										+  CAST(@PaymentScheduleID AS VARCHAR)
	WHERE LeadEventID=@LeadEventID

	-- clear helper or update fields
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4453)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PaymentScheduleID, @MatterID -- CustomerPaymentScheduleID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4459)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @DetailValue, @MatterID -- Description
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4464)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @CardName, @MatterID -- CardName
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4465)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @CardNumber, @MatterID -- Number
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4466)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @ExpMonth, @MatterID -- ExpiryMonth
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4467)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @ExpYear, @MatterID -- ExpiryYear
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4469)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @CardAuth, @MatterID -- AuthCode
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4505)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @MatterID, @MatterID -- ObjectID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4506)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, 2, @MatterID -- ObjectTypeID

	UPDATE @Payments SET Done=1 WHERE PaymentScheduleID=@PaymentScheduleID
	
	SELECT @AccountID=AccountID 
	FROM CustomerPaymentSchedule WITH (NOLOCK) 
	WHERE CustomerPaymentScheduleID=@PaymentScheduleID
	
	EXEC Account__SetDateAndAmountOfNextPayment @AccountID	

	-- BEU uncomment when tested	
	---- add event again to do next payment
	--IF EXISTS ( SELECT * FROM @Payments WHERE Done=0 )
	--BEGIN
	
	--	INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount,EarliestRunDateTime)
	--	SELECT TOP 1 le.ClientID, m.CustomerID, le.LeadID, le.CaseID, LeadEventID, EventTypeID, dbo.fn_GetDate_Local(), le.WhoCreated, EventTypeID, le.WhoCreated, -1, 5, 0, 5, DATEADD(SECOND,15,dbo.fn_GetDate_Local())
	--	FROM LeadEvent le WITH (NOLOCK) 
	--	INNER JOIN Matter m WITH (NOLOCK) ON le.CaseID=m.CaseID
	--	WHERE LeadEventID=@LeadEventID

	--END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_TakeCardPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_TakeCardPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_TakeCardPayment] TO [sp_executeall]
GO
