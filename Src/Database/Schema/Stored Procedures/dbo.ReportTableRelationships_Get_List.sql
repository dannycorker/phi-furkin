SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ReportTableRelationships table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTableRelationships_Get_List]

AS


				
				SELECT
					[ReportTableRelationshipID],
					[TableFrom],
					[TableTo],
					[TableJoin],
					[Description],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ReportTableRelationships] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTableRelationships_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Get_List] TO [sp_executeall]
GO
