SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-19
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[SMSStopConfigText_Upsert]
	
	@SMSStopConfigTextID int = null
	, @ClientID int 
	, @OutgoingPhoneNumber varchar(200)
	, @SMSTextToAppend varchar(2000)
	, @LeadTypeID int = null

AS
BEGIN

	IF @SMSStopConfigTextID > 0 -- Update existing
	BEGIN
		
		UPDATE TOP (1) s
		SET s.ClientID = @ClientID
		, s.OutgoingPhoneNumber = @OutgoingPhoneNumber
		, s.SMSTextToAppend = @SMSTextToAppend
		, s.LeadTypeID = @LeadTypeID
		FROM SMSStopConfigText s 
		WHERE s.SMSStopConfigTextID = @SMSStopConfigTextID

	END
	ELSE -- Need to insert one
	BEGIN
		
		INSERT INTO SMSStopConfigText (ClientID, OutgoingPhoneNumber, SMSTextToAppend, LeadTypeID)
		VALUES (@ClientID, @OutgoingPhoneNumber, @SMSTextToAppend, @LeadTypeID)

	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_Upsert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStopConfigText_Upsert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_Upsert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_Upsert] TO [sp_executehelper]
GO
