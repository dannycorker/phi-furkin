SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-06-2016
-- Description:	Gets a customers ledger by customer id 
-- =============================================
CREATE PROCEDURE [dbo].[CustomerLedger__GetByCustomerID]

	@CustomerID INT,
	@ClientID INT	

AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT * FROM CustomerLedger WITH (NOLOCK) WHERE CustomerID=@CustomerID AND ClientID=@ClientID
	ORDER BY TransactionDate DESC    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerLedger__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerLedger__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerLedger__GetByCustomerID] TO [sp_executeall]
GO
