SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
	Jim Green 2010-07-16

	This proc looks for a specific LeadEvent that failed to delete properly, and tries again with all the right params.
*/

CREATE PROCEDURE [dbo].[RedeleteLeadEvent] 
	
	@LeadEventID int, 
	@ShowComments bit = 0 
AS 
BEGIN

	DECLARE @UserID int, 
	@DeletionComments varchar(2000), 
	@FullRights tinyint = 4 
	
	SELECT @UserID = le.WhoDeleted, 
	@DeletionComments = le.DeletionComments 
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID  

	IF @UserID > 0
	BEGIN 
		EXEC dbo.DeleteLeadEvent @LeadEventID, @UserID, @DeletionComments, @FullRights, @ShowComments
	END

END





GO
GRANT VIEW DEFINITION ON  [dbo].[RedeleteLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RedeleteLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RedeleteLeadEvent] TO [sp_executeall]
GO
