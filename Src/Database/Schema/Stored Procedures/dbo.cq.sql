SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-02-28
-- Description:	List all Client Questionnaire records for a client
-- =============================================
CREATE PROCEDURE [dbo].[cq] 
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS ( SELECT * FROM dbo.ClientOption co WITH (NOLOCK) WHERE co.ClientID = @ClientID and co.eCatcherOff = 1 )
	BEGIN
		SELECT 'IMPORTANT!  This Client has eCatcher Switched off in their ClientOptions record.  There is a Setup + Monthly Charge for eCatcher' [Important!]
	END

	;with submitted as 
	(
	SELECT cuq.ClientQuestionnaireID, COUNT(cuq.CustomerQuestionnaireID) [Submitted], MIN(cuq.SubmissionDate) [First], MAX(cuq.SubmissionDate) [Most Recent]
	FROM dbo.CustomerQuestionnaires cuq WITH (NOLOCK) 
	WHERE cuq.ClientID = @ClientID 
	GROUP BY cuq.ClientQuestionnaireID
	)
	SELECT cq.ClientQuestionnaireID [QuestionnaireID], cq.QuestionnaireTitle [Title], cp.UserName [Run As], count(t.TrackingID) [Tracking URLs], cq.ImportDirectlyIntoLeadManager [Import], cq.RememberAnswers, s.Submitted, s.First, s.[Most Recent], 'aq 1,13,' + CONVERT(varchar,cq.ClientQuestionnaireID) [Describe], 'https://www.aquarium-software.com/QuestionnairePreview.aspx?id=' + CONVERT(varchar,cq.ClientQuestionnaireID) [Preview URL]
	FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = cq.RunAsClientPersonnelID 
	LEFT JOIN dbo.Tracking t WITH (NOLOCK) on t.ClientQuestionnaireID = cq.ClientQuestionnaireID 
	LEFT JOIN submitted s on s.ClientQuestionnaireID = cq.ClientQuestionnaireID 
	WHERE cq.ClientID = @ClientID 
	GROUP BY cq.ClientQuestionnaireID, cq.QuestionnaireTitle, cp.UserName, cq.ImportDirectlyIntoLeadManager, s.Submitted, s.First, s.[Most Recent] 	, cq.RememberAnswers
	ORDER BY cq.QuestionnaireTitle
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[cq] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cq] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cq] TO [sp_executeall]
GO
