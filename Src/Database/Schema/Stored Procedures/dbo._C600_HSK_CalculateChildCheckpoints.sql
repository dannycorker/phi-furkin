SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



 
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-12-14
-- Description:	Housekeeping routine to store child checkpoint values
--				Temporary fix until hierarchical checkpoint storage added to rules engine
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROC [dbo].[_C600_HSK_CalculateChildCheckpoints]
(
	@FromDate DATE
)
AS
BEGIN

--declare
--	@FromDate DATE = '2016-12-05'

	
	SET NOCOUNT ON;
	
	DECLARE  @CalculateType					INT
			,@ClientID						INT = dbo.fnGetPrimaryClientID()
			,@CustomerID					INT
			,@FirstRenewalPremiumPreRenewal INT = 0
			,@IllnessRuleSetID				INT
			,@LeadID						INT
			,@MatterID						INT
			,@OriginalPCID					INT
			,@CaseID						INT
			,@PDHTableRowID					INT
			,@PremiumCalculationID			INT
			,@ProductBaseRuleSetID			INT
			,@Status						INT
			,@TableRowID					INT
			,@StartDate						DATE = NULL
			,@TermsDate						DATE = NULL
			,@ToDate						VARCHAR(10)
			
	DECLARE @CandidateRows TABLE 
	( 
		TableRowID INT, 
		MatterID INT, 
		OriginalPCID INT, 
		IPTRuleID INT, 
		TermsDate DATE, 
		StartDate DATE, 
		IllnessRuleSetID INT, 
		ProductBaseRuleSetID INT, 
		Done INT )
		
	-- Get the premium metrics
	DECLARE @PremiumData TABLE
	(
		TermsDate DATE,
		SchemeID INT,
		PolicyMatterID INT,
		RuleSetID INT,
		AnnualPremium MONEY,
		Discount MONEY,
		PremiumLessDiscount MONEY,
		FirstMonthly MONEY,
		RecurringMonthly MONEY,
		Net MONEY,
		Commission MONEY,
		GrossNet MONEY,
		DiscountGrossNet MONEY,
		PAFIfBeforeIPT MONEY,
		PAFBeforeIPTGrossNet MONEY,
		IPT MONEY,
		IPTGrossNet MONEY,
		PAFIfAfterIPT MONEY,
		GrossGross MONEY,
		PremiumCalculationID INT		
	)	

	-- find candidate rulesets ( have IPT but no breed risk and created after From Date, exclude quotes ) 
	INSERT INTO @CandidateRows
	SELECT tr.TableRowID, tr.MatterID, pcd.PremiumCalculationDetailID, pcdv.RuleID, termstart.ValueDate, polstart.ValueDate, NULL, NULL, 0
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN TableDetailValues pcid WITH (NOLOCK) ON tr.TableRowID=pcid.TableRowID AND pcid.DetailFieldID=175709
	INNER JOIN PremiumCalculationDetail pcd WITH (NOLOCK) ON pcid.ValueInt=pcd.PremiumCalculationDetailID 
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pcd.PremiumCalculationDetailID=pcdv.PremiumCalculationDetailID
	INNER JOIN MatterDetailValues termstart WITH (NOLOCK) ON tr.MatterID=termstart.MatterID AND termstart.DetailFieldID=176925
	INNER JOIN MatterDetailValues polstart WITH (NOLOCK) ON tr.MatterID=polstart.MatterID AND polstart.DetailFieldID=170036
	WHERE tr.DetailFieldID=175336
	AND pcd.WhenCreated > @FromDate
	AND pcdv.RuleCheckpoint='IPT'
	AND NOT EXISTS (
		SELECT * FROM PremiumCalculationDetailValues pcdv2 WITH (NOLOCK) 
		WHERE pcd.PremiumCalculationDetailID=pcdv2.PremiumCalculationDetailID
		AND pcdv2.RuleCheckpoint='Illness Breed Group Relativity'
	)

	-- find rulesetIDs & original GUID for candidates
	UPDATE cr
	SET IllnessRuleSetID=irp.Value, ProductBaseRuleSetID=brp.Value
	FROM @CandidateRows cr 
	INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON cr.IPTRuleID=r.RuleID 
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON r.RuleSetID=rs.RuleSetID
	INNER JOIN RulesEngine_Rules br WITH (NOLOCK) ON rs.RuleSetID=br.RuleSetID AND br.Name='Base premium'
	INNER JOIN RulesEngine_RuleParameters brp WITH (NOLOCK) ON br.RuleID=brp.RuleID AND brp.Name LIKE '%Product A Base' 
	INNER JOIN RulesEngine_RuleSets irs WITH (NOLOCK) ON brp.Value=irs.RuleSetID
	INNER JOIN RulesEngine_Rules ir WITH (NOLOCK) ON irs.RuleSetID=ir.RuleSetID AND ir.Name='Illness Net'
	INNER JOIN RulesEngine_RuleParameters irp WITH (NOLOCK) ON ir.RuleID=irp.RuleID AND irp.Name LIKE '%Risk Type Premium: Illness' 
--SELECT * FROM @CandidateRows
	-- loop through rating and updating GUID	
	WHILE EXISTS ( SELECT * FROM @CandidateRows WHERE Done=0 )
	BEGIN
	
		SELECT TOP 1 @TableRowID=TableRowID, 
						@MatterID=MatterID, 
						@OriginalPCID=OriginalPCID, 
						@TermsDate=TermsDate, 
						@StartDate=StartDate, 
						@IllnessRuleSetID=IllnessRuleSetID, 
						@ProductBaseRuleSetID=ProductBaseRuleSetID 
		FROM @CandidateRows WHERE DONE=0
		
		-- BEU This needs fixing to make renewal calcs work properly (renewal = 2)
		SELECT @CalculateType=1
			
		SELECT @CustomerID = m.CustomerID, @LeadID = m.LeadID, @CaseID = m.CaseID 
		FROM Matter m 
		WHERE m.MatterID = @MatterID	
			
		SELECT @FromDate=@TermsDate
		
		IF @CalculateType = 2  -- renewal (this needs fixing)
			SELECT @TermsDate=@FromDate, @StartDate=@FromDate
		
		-- set FirstRenewalPremiumPreRenewal flag if fromdate= historical policy enddate
		IF EXISTS 
		( 
			SELECT * 
			FROM dbo.TableDetailValues ed WITH (NOLOCK) 
			WHERE ed.MatterID = @MatterID
			AND ed.DetailFieldID = 145664 
			AND ed.ValueDate = @FromDate 
		)
		BEGIN
			SELECT @FirstRenewalPremiumPreRenewal=1
			SELECT @TermsDate=@FromDate
			SELECT @StartDate=@FromDate
		END

		-- set the new business override if renewal
		DECLARE @Overrides dbo.tvpIntVarcharVarchar
		IF @CalculateType=2 OR @FirstRenewalPremiumPreRenewal=1
		BEGIN
			INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
			(2, 'NewBusiness', '0')
		END
		-- always pass the MatterID in case it is needed
		INSERT INTO @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2,'RenewalMatterID',CAST(@MatterID AS VARCHAR))
			
		DELETE FROM @PremiumData
		INSERT @PremiumData
		EXEC _C00_1273_Policy_CalculatePremium @CaseID, @StartDate, @TermsDate, NULL, @Overrides, @FromDate, @ProductBaseRuleSetID	

		SELECT @PremiumCalculationID=PremiumCalculationID FROM @PremiumData 

		UPDATE PremiumCalculationDetailValues
		SET PremiumCalculationDetailID=@OriginalPCID
		WHERE PremiumCalculationDetailID=@PremiumCalculationID
			
		DELETE FROM @PremiumData
		INSERT @PremiumData
		EXEC _C00_1273_Policy_CalculatePremium @CaseID, @StartDate, @TermsDate, NULL, @Overrides, @FromDate, @IllnessRuleSetID	

		SELECT @PremiumCalculationID=PremiumCalculationID FROM @PremiumData 

		UPDATE PremiumCalculationDetailValues
		SET PremiumCalculationDetailID=@OriginalPCID
		WHERE PremiumCalculationDetailID=@PremiumCalculationID

		UPDATE @CandidateRows Set Done=1 WHERE TableRowID=@TableRowID
	
	END
		
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HSK_CalculateChildCheckpoints] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_HSK_CalculateChildCheckpoints] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HSK_CalculateChildCheckpoints] TO [sp_executeall]
GO
