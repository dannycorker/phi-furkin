SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-04
-- Description:	Calls the fnGetUserLeadCaseAccess function to get the list of cases for this Lead, 
--				excluding any that this user implicitly cannot see
-- =============================================
CREATE PROCEDURE [dbo].[GetUserLeadCaseAccess]
	@UserID int,
	@LeadID int
AS
BEGIN
	SET NOCOUNT ON;
	
	
	SELECT * FROM dbo.fnGetUserLeadCaseAccess(@UserID, @LeadID)
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserLeadCaseAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserLeadCaseAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserLeadCaseAccess] TO [sp_executeall]
GO
