SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 02-08-2016
-- Description:	Gets a list of configured payment gate ways for the give client
-- =============================================
CREATE PROCEDURE [dbo].[ClientPaymentGateway__GetByClientID]

	@ClientID INT

AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT pg.Gateway, pg.Name, cg.* FROM ClientPaymentGateway cg
	INNER JOIN dbo.PaymentGateway pg WITH (NOLOCK) on pg.PaymentGatewayID = cg.PaymentGatewayID
	WHERE cg.ClientID=@ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPaymentGateway__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByClientID] TO [sp_executeall]
GO
