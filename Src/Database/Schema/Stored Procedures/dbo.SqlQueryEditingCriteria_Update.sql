SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditingCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_Update]
(

	@SqlQueryEditingCriteriaID int   ,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryEditingTableID int   ,

	@SqlQueryEditingColumnID int   ,

	@CriteriaText varchar (1000)  ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  ,

	@CriteriaName varchar (250)  ,

	@SubQueryID int   ,

	@SubQueryLinkType varchar (50)  ,

	@ParamValue varchar (250)  ,

	@IsSecurityClause bit   ,

	@CriteriaSubstitutions varchar (2000)  ,

	@IsParameterizable bit   ,

	@Comparison varchar (50)  ,

	@IsJoinClause bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditingCriteria]
				SET
					[ClientID] = @ClientID
					,[SqlQueryEditingID] = @SqlQueryEditingID
					,[SqlQueryEditingTableID] = @SqlQueryEditingTableID
					,[SqlQueryEditingColumnID] = @SqlQueryEditingColumnID
					,[CriteriaText] = @CriteriaText
					,[Criteria1] = @Criteria1
					,[Criteria2] = @Criteria2
					,[CriteriaName] = @CriteriaName
					,[SubQueryID] = @SubQueryID
					,[SubQueryLinkType] = @SubQueryLinkType
					,[ParamValue] = @ParamValue
					,[IsSecurityClause] = @IsSecurityClause
					,[CriteriaSubstitutions] = @CriteriaSubstitutions
					,[IsParameterizable] = @IsParameterizable
					,[Comparison] = @Comparison
					,[IsJoinClause] = @IsJoinClause
				WHERE
[SqlQueryEditingCriteriaID] = @SqlQueryEditingCriteriaID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Update] TO [sp_executeall]
GO
