SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Country table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_GetByAlpha3Code]
(

	@Alpha3Code char (3)  
)
AS


				SELECT
					[CountryID],
					[CountryName],
					[CountryNamePlain],
					[Alpha2Code],
					[Alpha3Code],
					[NumericCode],
					[ISO3166Dash2Code],
					[WhenCreated],
					[WhenModified],
					[UsePostcode],
					[PostcodeRegex],
					[DiallingCode]
				FROM
					[dbo].[Country] WITH (NOLOCK) 
				WHERE
										[Alpha3Code] = @Alpha3Code
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_GetByAlpha3Code] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_GetByAlpha3Code] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_GetByAlpha3Code] TO [sp_executeall]
GO
