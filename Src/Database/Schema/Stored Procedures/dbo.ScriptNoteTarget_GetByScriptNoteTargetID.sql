SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptNoteTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_GetByScriptNoteTargetID]
(

	@ScriptNoteTargetID int   
)
AS


				SELECT
					[ScriptNoteTargetID],
					[ScriptID],
					[ClientID],
					[NoteTypeID],
					[PriorityID],
					[ApplyToAllCases]
				FROM
					[dbo].[ScriptNoteTarget] WITH (NOLOCK) 
				WHERE
										[ScriptNoteTargetID] = @ScriptNoteTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_GetByScriptNoteTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_GetByScriptNoteTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_GetByScriptNoteTargetID] TO [sp_executeall]
GO
