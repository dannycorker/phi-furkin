SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-05-11
-- Description:	List all client options
-- =============================================
CREATE PROCEDURE [dbo].[clop] 
	@ClientID int = null,
	@SortBy int = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF @SortBy = 1
	BEGIN

		SELECT * 
		FROM dbo.ClientOption clop WITH (NOLOCK) 
		WHERE (@ClientID IS NULL OR clop.ClientID = @ClientID)
		ORDER BY 1 DESC

	END
	ELSE
	BEGIN

		SELECT * 
		FROM dbo.ClientOption clop WITH (NOLOCK) 
		WHERE (@ClientID IS NULL OR clop.ClientID = @ClientID)
		ORDER BY 1

	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[clop] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[clop] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[clop] TO [sp_executeall]
GO
