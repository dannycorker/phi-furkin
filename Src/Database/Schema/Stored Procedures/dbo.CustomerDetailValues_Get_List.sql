SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CustomerDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerDetailValues_Get_List]

AS


				
				SELECT
					[CustomerDetailValueID],
					[ClientID],
					[CustomerID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[CustomerDetailValues] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerDetailValues_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_Get_List] TO [sp_executeall]
GO
