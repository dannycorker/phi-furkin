SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack (copy of RulesEngine_ParameterPreSet_GetByClientID)
-- Create date: 2016-08-12
-- Description:	Returns all rule parameter presets
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterPreSet_Get]
(
	@ClientID INT,
	@RuleID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @ChangeSetID INT,
			@RuleSetID INT
	
	SELECT	@ChangeSetID = rs.ChangeSetID,
			@RuleSetID = rs.RuleSetID
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON r.RuleSetID = rs.RuleSetID
	WHERE r.RuleID = @RuleID

	SELECT	RuleParameterPreSetID, Name, ParameterTypeID, Value, DataTypeID, ClientID, Parent, SourceID
	FROM	dbo.RulesEngine_RuleParameters_PreSet WITH (NOLOCK)
	WHERE	ClientID = @ClientID
	UNION 
	SELECT	(rs.RuleSetID * -1) AS RuleParameterPreSetID, 'Rule Set: ' + Name AS Name, 3 AS ParameterTypeID, 
			CAST(rs.RuleSetID AS VARCHAR) AS Value, 2 AS DataTypeID, ClientID, NULL AS Parent, NULL AS SourceID
	FROM	dbo.RulesEngine_RuleSets rs WITH (NOLOCK)
	WHERE	ClientID = @ClientID
	AND		rs.ChangeSetID = @ChangeSetID
	AND		rs.RuleSetID <> @RuleSetID
	ORDER BY Name
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterPreSet_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterPreSet_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterPreSet_Get] TO [sp_executeall]
GO
