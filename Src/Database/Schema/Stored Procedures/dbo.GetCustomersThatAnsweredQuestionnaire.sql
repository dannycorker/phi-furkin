SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/****** Object:  Stored Procedure dbo.GetCustomersThatAnsweredQuestionnaire    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[GetCustomersThatAnsweredQuestionnaire] @ClientQuestionnaireID int

AS

SELECT     dbo.Customers.CustomerID, dbo.Customers.ClientID, dbo.Customers.TitleID, dbo.Customers.FirstName, dbo.Customers.MiddleName, 
dbo.Customers.LastName, dbo.Customers.EmailAddress, 
dbo.Customers.DayTimeTelephoneNumber, dbo.Customers.DayTimeTelephoneNumberVerifiedAndValid, 
dbo.Customers.HomeTelephone, dbo.Customers.HomeTelephoneVerifiedAndValid, 
dbo.Customers.MobileTelephone, dbo.Customers.MobileTelephoneVerifiedAndValid, 
dbo.Customers.Address1, dbo.Customers.Address2, dbo.Customers.Town, dbo.Customers.County, 
dbo.Customers.PostCode, dbo.Customers.Test, dbo.Customers.CompanyName, 
dbo.Customers.CompanyTelephone, dbo.Customers.CompanyTelephoneVerifiedAndValid, 
dbo.Customers.WorksTelephone, dbo.Customers.WorksTelephoneVerifiedAndValid, 
dbo.Customers.Occupation, dbo.Customers.Employer, dbo.CustomerQuestionnaires.CustomerQuestionnaireID, dbo.CustomerQuestionnaires.ClientQuestionnaireID, 
HasDownloaded, DownloadedOn, Occupation, Employer, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, PhoneNumbersVerifiedOn,
dbo.Customers.DateOfBirth, dbo.Customers.IsBusiness, dbo.Customers.DefaultContactID
FROM         dbo.Customers WITH (NOLOCK) INNER JOIN
                      dbo.CustomerQuestionnaires WITH (NOLOCK) ON dbo.Customers.CustomerID = dbo.CustomerQuestionnaires.CustomerID
WHERE     (dbo.CustomerQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomersThatAnsweredQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomersThatAnsweredQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomersThatAnsweredQuestionnaire] TO [sp_executeall]
GO
