SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AquariumEventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_Insert]
(

	@AquariumEventSubtypeID int   ,

	@AquariumEventTypeID int   ,

	@AquariumEventSubtypeName varchar (100)  ,

	@AquariumEventSubtypeDescription varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@ClientID int   ,

	@StoredProcedure varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[AquariumEventSubtype]
					(
					[AquariumEventSubtypeID]
					,[AquariumEventTypeID]
					,[AquariumEventSubtypeName]
					,[AquariumEventSubtypeDescription]
					,[WhenCreated]
					,[WhenModified]
					,[ClientID]
					,[StoredProcedure]
					)
				VALUES
					(
					@AquariumEventSubtypeID
					,@AquariumEventTypeID
					,@AquariumEventSubtypeName
					,@AquariumEventSubtypeDescription
					,@WhenCreated
					,@WhenModified
					,@ClientID
					,@StoredProcedure
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Insert] TO [sp_executeall]
GO
