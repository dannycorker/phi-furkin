SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex ELger
-- Create date: 2009-01-11
-- Description:	Statistics on Usage
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Statistics] 
	@StatisticName VARCHAR(100) = NULL,
	@Month INT = 1,
	@IgnoreDocs INT = 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastMonthCutoffStart DATETIME, @LastMonthCutoffEnd DATETIME

	SELECT @LastMonthCutoffStart = DATEADD(mm, -@Month, dbo.fn_GetDate_Local())

	SELECT @LastMonthCutoffStart = w.[Date]
	FROM WorkingDays w 
	WHERE w.[Day] = 1 
	AND w.[Month] = DATEPART(mm, @LastMonthCutoffStart)
	AND w.[Year] = DATEPART(yy, @LastMonthCutoffStart)

	SELECT @LastMonthCutoffEnd = MAX(w.[Date]) + ' 23:59:59'
	FROM WorkingDays w 
	WHERE w.[Month] = DATEPART(mm, @LastMonthCutoffStart)
	AND w.[Year] = DATEPART(yy, @LastMonthCutoffStart)

	IF @StatisticName IS NULL
		BEGIN

		DECLARE @Instructions TABLE (
			RowID INT,
			Instructions VARCHAR(100)
			)

		INSERT INTO @Instructions(RowID,Instructions)
		SELECT 1, 'Use Either:'
		UNION
		SELECT 2, 'Users'
		UNION
		SELECT 3, 'CTM'
		UNION
		SELECT 4, 'SMS'
		UNION
		SELECT 5, 'Cases'
		UNION
		SELECT 6, 'LeadDocumentSpaceUsed - Date Not Applicable'
		UNION
		SELECT 6, 'As the first param'
		UNION
		SELECT 7, 'Then the number of months ago you want the data for'
		UNION 
		SELECT 8, 'EG _C00_Statistics ''Cases'', 0'
		UNION 
		SELECT 9, 'For Cases created so far this month'

		SELECT Instructions
		FROM @Instructions
		ORDER BY RowID

		END

	IF @StatisticName = 'Users'
		BEGIN

		SELECT ISNULL(Clients.CompanyName, 'Total') AS [Company Name], COUNT(*) AS [COUNT OF Users]
		FROM ClientPersonnel WITH (NOLOCK) 
		INNER JOIN Clients WITH (NOLOCK) ON Clients.ClientID = ClientPersonnel.ClientID
		WHERE EXISTS(
		SELECT TOP 1 *
		FROM leadevent WITH (NOLOCK) 
		WHERE whocreated = clientpersonnel.clientpersonnelid
		AND leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd
		)
		AND clientpersonnel.emailaddress NOT LIKE '%@aquarium-software.com'
		GROUP BY Clients.CompanyName
		WITH CUBE

		END

	IF @StatisticName = 'CTM'
		BEGIN

		SELECT ISNULL(clients_from.CompanyName, 'Total') AS [FROM Client], ISNULL(clients_to.CompanyName, 'Total') AS [TO Client], COUNT(*) AS [Number OF Cases Transferred]
		FROM casetransfermapping ctm WITH (NOLOCK) 
		INNER JOIN clients clients_from WITH (NOLOCK) ON clients_from.clientid = ctm.clientid
		INNER JOIN clients clients_to WITH (NOLOCK) ON clients_to.clientid = ctm.newclientid
		INNER JOIN lead WITH (NOLOCK) ON lead.leadid = ctm.newleadid AND lead.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd
		INNER JOIN cases WITH (NOLOCK) ON cases.leadid = lead.leadid
		GROUP BY clients_from.CompanyName, clients_to.CompanyName
		--order by clients_from.CompanyName
		WITH CUBE

		END

	IF @StatisticName = 'SMS'
		BEGIN

		/*SELECT isnull(Clients.CompanyName, 'Total') as [Company Name], sum(floor((convert(numeric,DATALENGTH(leaddocument.emailblob))/4)/160)+1) as 'Number of SMS Pages Sent x months ago'
		FROM EventType with (nolock) 
		INNER JOIN LeadEvent with (nolock)  ON LeadEvent.EventTypeID = EventType.EventTypeID and leadevent.whencreated between @LastMonthCutoffStart and @LastMonthCutoffEnd
		INNER JOIN Clients with (nolock)  ON LeadEvent.ClientID = Clients.ClientID
		inner join leaddocument with (nolock) on leaddocument.LeadDocumentID = leadevent.leaddocumentid
		WHERE EventSubTypeID = 13
		Group By Clients.CompanyName
		with cube*/
			
			/* JWG 2011-06-20 LeadDocumentFS */
			/* JWG 2012-07-12 LeadDocument again */
			SELECT ISNULL(cl.CompanyName, 'Total') AS [Company Name], SUM(FLOOR((ld.EmailBLOBSize - 1)/160)+1) AS 'Number of SMS Pages Sent x months ago'
			FROM dbo.EventType et WITH (NOLOCK) 
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND le.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd
			INNER JOIN dbo.Clients cl WITH (NOLOCK) ON le.ClientID = cl.ClientID
			INNER JOIN dbo.LeadDocument ld WITH (NOLOCK) ON ld.LeadDocumentID = le.leaddocumentid
			WHERE EventSubTypeID = 13
			GROUP BY cl.CompanyName 
			WITH CUBE
			
		END

	IF @StatisticName = 'Cases'
		BEGIN

		SELECT ISNULL(clients.CompanyName, 'Total') AS [Company Name] , COUNT(*) AS [Number OF Cases]
		FROM clients WITH (NOLOCK) 
		INNER JOIN lead WITH (NOLOCK) ON lead.clientid = clients.clientid AND lead.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd
		INNER JOIN cases WITH (NOLOCK) ON cases.leadid = lead.leadid
		INNER JOIN customers WITH (NOLOCK) ON customers.customerid = lead.customerid AND (customers.test = 0 OR customers.test IS NULL)
		GROUP BY clients.CompanyName
		WITH CUBE

		END
	IF @StatisticName = 'IncomingDocs'
		BEGIN

		SELECT ISNULL(Clients.CompanyName, 'Total') AS [Company Name], COUNT(*) AS 'Number of Incoming Events last month'
		FROM EventType WITH (NOLOCK) 
		INNER JOIN LeadEvent  WITH (NOLOCK) ON LeadEvent.EventTypeID = EventType.EventTypeID AND leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd
		INNER JOIN Clients  WITH (NOLOCK) ON LeadEvent.ClientID = Clients.ClientID
		WHERE EventSubTypeID IN (3,5)
		GROUP BY Clients.CompanyName
		WITH CUBE
		
		END

	IF @StatisticName = 'Faxs'
		BEGIN

		SELECT ISNULL(Clients.CompanyName, 'Total') AS [Company Name], COUNT(*) AS 'Number of Incoming Events last month'
		FROM EventType WITH (NOLOCK) 
		INNER JOIN LeadEvent WITH (NOLOCK)  ON LeadEvent.EventTypeID = EventType.EventTypeID AND leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd
		INNER JOIN Clients WITH (NOLOCK)  ON LeadEvent.ClientID = Clients.ClientID
		WHERE EventSubTypeID IN (14)
		GROUP BY Clients.CompanyName
		WITH CUBE
		
		END

	IF @StatisticName = 'LeadDocumentSpaceUsed'
		BEGIN

		/*select isnull(Clients.CompanyName, 'Total') as [Company Name], convert(varchar,convert(money,isnull((sum(((convert(numeric,datalength(leaddocument.documentblob)))/1024)/1024)/1024)+(sum(((convert(numeric,datalength(leaddocument.emailblob)))/1024)/1024)/1024),0))) + ' GB'
		from leaddocument with (nolock)
		inner join clients with (nolock) on clients.clientid = leaddocument.clientid
		group by Clients.CompanyName
		with cube*/
		
			/* JWG 2011-06-20 LeadDocumentFS */
			/* JWG 2012-07-12 LeadDocument again */
			/*SELECT isnull(cl.CompanyName, 'Total') as [Company Name], CAST((SUM(isnull(fs.DocumentBLOBSize, 0) + isnull(fs.EmailBLOBSize, 0))/1024/1024/1024) AS VARCHAR) + ' GB' AS [Size In GB]
			FROM dbo.LeadDocumentFS fs WITH (NOLOCK) 
			INNER JOIN dbo.Clients cl WITH (NOLOCK) ON cl.ClientID = fs.ClientID
			GROUP BY cl.CompanyName 
			WITH CUBE*/
			;WITH InnerSql AS 
			(
				SELECT ISNULL(cl.CompanyName, 'Total') AS [Company Name], SUM(CAST(ISNULL(ld.DocumentBLOBSize, 0) AS BIGINT) + CAST(ISNULL(ld.EmailBLOBSize, 0) AS BIGINT))/1024/1024 AS [SIZE IN MB]
				FROM dbo.LeadDocument ld WITH (NOLOCK) 
				INNER JOIN dbo.Clients cl WITH (NOLOCK) ON cl.ClientID = ld.ClientID
				GROUP BY cl.CompanyName 
				WITH CUBE
			)
			SELECT cl.ClientID, i.[Company Name], i.[Size IN MB], i.[Size IN MB]/1024 AS [SIZE IN GB] 
			FROM InnerSql i 
			LEFT JOIN dbo.Clients cl WITH (NOLOCK) ON cl.CompanyName = i.[Company Name]
			ORDER BY cl.ClientID

		END
	IF @StatisticName = 'DocumentTemplates'
		BEGIN

		SELECT ISNULL(Clients.CompanyName, 'Total') AS [Company Name], CONVERT(VARCHAR,CONVERT(MONEY,ISNULL((SUM(((CONVERT(NUMERIC,DATALENGTH(documenttype.template)))/1024)/1024)/1024),0))) + ' GB'
		FROM documenttype WITH (NOLOCK)
		INNER JOIN clients WITH (NOLOCK) ON clients.clientid = documenttype.clientid
		GROUP BY Clients.CompanyName
		WITH CUBE

		END
	IF @StatisticName = 'Complete'
	BEGIN

	DECLARE @SeedID INT

	EXEC @SeedID = dbo.GetWorkSeedID '[_C00_Statistics]'

	INSERT INTO Work1(WorkSeedID, Value1, Value2)
	SELECT @SeedID, ClientID, CompanyName 
	FROM Clients WITH (NOLOCK)

	--Select 'Value 3 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	UPDATE Work1 
	SET Value3 = V2.Counter
	FROM Work1 w1
	CROSS JOIN (SELECT COUNT(*) AS Counter, Clients.ClientID FROM Clients WITH (NOLOCK) INNER JOIN LeadEvent WITH (NOLOCK) ON LeadEvent.ClientID = Clients.ClientID AND leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd GROUP BY Clients.ClientID) AS V2
	WHERE w1.Value1 = V2.ClientID
	AND WorkSeedID = @SeedID

	--Select 'Value 3 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	--Select 'Value 4 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	UPDATE Work1 
	SET Value4 = V2.Counter
	FROM Work1 w1
	CROSS JOIN (SELECT COUNT(*) AS Counter, Clients.ClientID FROM Clients WITH (NOLOCK) INNER JOIN Lead ON Lead.ClientID = Clients.ClientID AND lead.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd GROUP BY Clients.ClientID) AS V2
	WHERE w1.Value1 = V2.ClientID
	AND WorkSeedID = @SeedID

	--Select 'Value 4 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	IF @IgnoreDocs = 0
	BEGIN

		--Select 'Value 5 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

		/*Update Work1 
		Set Value5 = V2.Counter
		From Work1 w1
		Cross Join (select convert(varchar,convert(money,isnull((sum(((convert(numeric,datalength(leaddocument.documentblob)))/1024)/1024)/1024)+(sum(((convert(numeric,datalength(leaddocument.emailblob)))/1024)/1024)/1024),0))) as Counter, leaddocument.ClientID from leaddocument with (nolock) inner join leadevent with (nolock) on leadevent.leaddocumentid = leaddocument.leaddocumentid WHERE leadevent.whencreated between @LastMonthCutoffStart and @LastMonthCutoffEnd group by LeadDocument.ClientID) as V2
		Where w1.Value1 = V2.ClientID
		AND WorkSeedID = @SeedID*/

		/* JWG 2011-06-20 LeadDocumentFS */
		/* JWG 2012-07-12 LeadDocument again */
		UPDATE Work1 
		SET Value5 = V2.Counter
		FROM Work1 w1
		CROSS JOIN (
			SELECT CAST(SUM(ISNULL(ld.DocumentBLOBSize, 0) + ISNULL(ld.EmailBLOBSize, 0))/1024/1024/1024 AS VARCHAR) AS Counter , ld.ClientID 
			FROM dbo.LeadDocument ld WITH (NOLOCK) 
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.LeadDocumentID = ld.LeadDocumentID 
			WHERE le.WhenCreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd 
			GROUP BY ld.ClientID
		) AS V2
		WHERE w1.Value1 = V2.ClientID
		AND WorkSeedID = @SeedID

		--Select 'Value 5 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	END

	--Update Work1 
	--Set Value5 = V2.[Count of Users]
	--From Work1 w1
	--Cross Join (exec [_C00_Statistics] 'Users', 1) as V2
	--Where w1.Value1 = V2.[Company Name]

	--Select 'Value 6 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	UPDATE Work1 
	SET Value6 = V2.Counter
	FROM Work1 w1
	CROSS JOIN (SELECT COUNT(*) AS [Counter], ClientPersonnel.ClientID FROM ClientPersonnel WITH (NOLOCK) WHERE EXISTS(SELECT * FROM leadevent WITH (NOLOCK) WHERE whocreated = clientpersonnel.clientpersonnelid AND leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd) AND clientpersonnel.emailaddress NOT LIKE '%@aquarium-software.com' GROUP BY ClientPersonnel.ClientID) AS V2 
	WHERE w1.Value1 = V2.ClientID
	AND WorkSeedID = @SeedID

	--Select 'Value 6 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	--Select 'Value 7 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	UPDATE Work1 
	SET Value7 = V2.Counter
	FROM Work1 w1
	CROSS JOIN (SELECT COUNT(*) AS Counter, LeadEvent.ClientID FROM LeadEvent WITH (NOLOCK) WHERE Comments LIKE 'Auto-generated by AquariumNet Batch Scheduler%' AND leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd GROUP BY LeadEvent.ClientID) AS V2
	WHERE w1.Value1 = V2.ClientID
	AND WorkSeedID = @SeedID

	--Select 'Value 7 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	--Select 'Value 8 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	UPDATE Work1 
	SET Value8 = V2.Counter
	FROM Work1 w1
	CROSS JOIN (SELECT COUNT(*) AS Counter, LeadEvent.ClientID FROM LeadEvent WITH (NOLOCK) INNER JOIN EventType et ON et.EventTypeID = LeadEvent.EventTypeID AND et.EventSubTypeID = 11 WHERE leadevent.whencreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd GROUP BY LeadEvent.ClientID) AS V2
	WHERE w1.Value1 = V2.ClientID
	AND WorkSeedID = @SeedID

	--Select 'Value 8 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	/*
	Update Work1 
	Set Value9 = OpenedCases.Counter
	From Work1 
	Inner Join (Select Count(*) as Counter, lestart.ClientID as [ClientID]
	From tabledetailvalues tdv1 with (nolock) 
	INNER JOIN tabledetailvalues tdv2 with (nolock) on tdv2.tablerowid = tdv1.tablerowid and tdv2.detailfieldid = 26056 --EndEvent
	INNER JOIN tabledetailvalues tdv3 with (nolock) on tdv3.tablerowid = tdv1.tablerowid and tdv3.detailfieldid = 26059 --LeadTypeID
	LEFT JOIN tabledetailvalues tdv4 with (nolock) on tdv4.leadid = tdv1.leadid and tdv4.detailfieldid = 27276 --Table Detail Field ID
	LEFT JOIN tabledetailvalues tdv5 with (nolock) on tdv5.tablerowid = tdv4.tablerowid and tdv5.detailfieldid = 27277 --Negative Exclusion Value
	inner join leadevent lestart with (nolock) on lestart.eventtypeid = tdv1.DetailValue and lestart.WhenCreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd And lestart.EventDeleted = 0-- start event 
	LEFT JOIN tabledetailvalues tdv6 with (nolock) on tdv6.leadid = lestart.leadid and tdv6.detailfieldid = tdv4.detailvalue and tdv6.detailvalue <> tdv5.detailvalue
	Inner Join Lead l on l.LeadID = lestart.leadid
	Inner Join Customers c ON c.CustomerID = l.CustomerID and (c.Test = 0 or c.Test IS NULL)
	where tdv1.detailfieldid = 26055 --StartEvent
	group by lestart.ClientID) as OpenedCases on OpenedCases.ClientID = Work1.Value1
	Where WorkSeedID = @SeedID

	Update Work1 
	Set Value10 = MaintCases.Counter
	From Work1 
	Inner Join (Select Count(*) as Counter, lestart.ClientID as [ClientID]
	From leadevent lestart with (nolock) 
	LEFT JOIN tabledetailvalues tdv4 with (nolock) on tdv4.detailfieldid = 27276 --Table Detail Field ID
	LEFT JOIN tabledetailvalues tdv5 with (nolock) on tdv5.tablerowid = tdv4.tablerowid and tdv5.detailfieldid = 27277 --Negative Exclusion Value
	LEFT JOIN tabledetailvalues tdv6 with (nolock) on tdv6.leadid = lestart.leadid and tdv6.detailfieldid = tdv4.detailvalue and tdv6.detailvalue <> tdv5.detailvalue
	Inner Join Lead l with (nolock)  on l.LeadID = lestart.leadid
	Inner Join Customers c with (nolock)  ON c.CustomerID = l.CustomerID and (c.Test = 0 or c.Test IS NULL)
	where lestart.eventtypeid IN (
		Select tdv1.DetailValue 
		From tabledetailvalues tdv1 with (nolock) 
		Where tdv1.detailfieldid = 26055) -- start event
	And (
	exists(
	select *
	from leadevent with (nolock)
	where leadevent.caseid = lestart.caseid
	and leadevent.eventtypeid IN (Select tdv2.detailvalue From tabledetailvalues tdv2 with (nolock) Where tdv2.detailfieldid = 26056) --EndEvent
	and leadevent.WhenCreated BETWEEN @LastMonthCutoffStart AND @LastMonthCutoffEnd And leadevent.EventDeleted = 0
	and datediff(dd,lestart.WhenCreated, leadevent.WhenCreated) > 30
	)
	or
	(
	lestart.WhenCreated < @LastMonthCutoffStart AND lestart.EventDeleted = 0
	and not exists(
	select *
	from leadevent with (nolock)
	where leadevent.caseid = lestart.caseid
	and leadevent.eventtypeid IN (Select tdv2.detailvalue From tabledetailvalues tdv2 with (nolock) Where tdv2.detailfieldid = 26056) --EndEvent
	)
	)
	)
	Group By lestart.ClientID) as MaintCases on MaintCases.ClientID = Work1.Value1
	Where WorkSeedID = @SeedID	AND WorkSeedID = @SeedID
	*/

	--Select 'Value 9 Start', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	UPDATE Work1
	SET Value11 = V2.Counter
	FROM Work1 w1
	CROSS JOIN (SELECT COUNT(*) AS [Counter], ClientPersonnel.ClientID FROM ClientPersonnel WITH (NOLOCK) WHERE AccountDisabled = 0 AND clientpersonnel.emailaddress NOT LIKE '%@aquarium-software.com' GROUP BY ClientPersonnel.ClientID) AS V2 
	WHERE w1.Value1 = V2.ClientID
	AND WorkSeedID = @SeedID

	--Select 'Value 9 End', DATEADD(ms, 1, dbo.fn_GetDate_Local())

	SELECT Value1 AS ClientID, Value2 AS [Company Name], CASE ISNULL(Value3,1) WHEN 1 THEN CASE ISNULL(Value10,1) WHEN 1 THEN ISNULL(Value3, 'Dead? Disable users to remove from report') ELSE CONVERT(VARCHAR,0) END ELSE CONVERT(VARCHAR,Value3) END AS [Events], ISNULL(Value4, '0') AS [Leads Created], ISNULL(Value5, '0.00')  + ' GB' AS [Spaced Used BY Documents], ISNULL(Work1.Value6, '0') AS [Active Useres], Value11 AS [Enabled Users (Total)], ISNULL(CONVERT(NUMERIC(10,2),ISNULL((CONVERT(NUMERIC(10,2),Value3)-CONVERT(NUMERIC(10,2),ISNULL(Work1.Value7, '0'))), '0')/(CASE Value6 WHEN 0 THEN 1 ELSE CONVERT(NUMERIC(10,2),Value6) END)), '0') AS [Events Per USER],	ISNULL(Value7, '0') AS [Automated Events], CONVERT(NUMERIC(10,2),ISNULL(CONVERT(NUMERIC(10,2),Value8), '0.00')) AS [Cases Closed], CONVERT(NUMERIC(10,2),ISNULL(ISNULL(CONVERT(NUMERIC(10,2),Value8), '0')/CASE Value6 WHEN 0 THEN 1 ELSE CONVERT(NUMERIC(10,2),Value6) END, '0.00')) AS [Cases Closed Per USER], ISNULL(Value9, '0') AS [Number OF Newly Opened Chargable Cases], ISNULL(Value10, '0') AS [Number OF Maintenance Cases]
	FROM Work1
	WHERE Value2 NOT LIKE '%Aquarium%'
	--and Value6 IS NOT NULL
	--AND Value3 IS NOT NULL
	AND WorkSeedID = @SeedID
	AND Value11 IS NOT NULL
	AND Value11 <> 0
	
	/* Cascade delete */
	DELETE WorkSeed 
	WHERE WorkSeedID = @SeedID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Statistics] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Statistics] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Statistics] TO [sp_executeall]
GO
