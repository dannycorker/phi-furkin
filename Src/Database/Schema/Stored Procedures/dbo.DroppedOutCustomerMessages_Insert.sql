SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DroppedOutCustomerMessages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerMessages_Insert]
(

	@DroppedOutCustomerMessageID int    OUTPUT,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@EmailFromAddress varchar (255)  ,

	@EmailSubject varchar (500)  ,

	@Email varchar (MAX)  ,

	@SmsMessage varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[DroppedOutCustomerMessages]
					(
					[ClientID]
					,[ClientQuestionnaireID]
					,[EmailFromAddress]
					,[EmailSubject]
					,[Email]
					,[SmsMessage]
					)
				VALUES
					(
					@ClientID
					,@ClientQuestionnaireID
					,@EmailFromAddress
					,@EmailSubject
					,@Email
					,@SmsMessage
					)
				-- Get the identity value
				SET @DroppedOutCustomerMessageID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerMessages_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Insert] TO [sp_executeall]
GO
