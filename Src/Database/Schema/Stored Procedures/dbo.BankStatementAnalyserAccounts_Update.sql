SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the BankStatementAnalyserAccounts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BankStatementAnalyserAccounts_Update]
(

	@BankStatementAnalyserAccountID int   ,

	@OriginalBankStatementAnalyserAccountID int   ,

	@ClientID int   ,

	@BankStatementPageCount int   ,

	@CostPerPage money   ,

	@LastResetDate datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[BankStatementAnalyserAccounts]
				SET
					[BankStatementAnalyserAccountID] = @BankStatementAnalyserAccountID
					,[ClientID] = @ClientID
					,[BankStatementPageCount] = @BankStatementPageCount
					,[CostPerPage] = @CostPerPage
					,[LastResetDate] = @LastResetDate
				WHERE
[BankStatementAnalyserAccountID] = @OriginalBankStatementAnalyserAccountID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStatementAnalyserAccounts_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Update] TO [sp_executeall]
GO
