SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-03-02
-- Description:	Reconcile Account (TDV Only)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReconcileBankTDV]
@ClientID int,
@CustomerRefFieldID int, /*AKA Paris Number Field*/
@PaymentImportLeadID int,
@BankRefTableFieldID int,
@LoadIf2CardsFieldID int,
@NextPaymentDueFieldID int,
@ClientContributionTableFieldID int,
@ClientContributionHistoryDateFieldID int,
@ClientContributionHistoryValueFieldID int,
@ClientContributionHistoryNextPaymentFieldID int,
@UnreconcliedTableDetailFieldID int,
@PaymentImportTableDetailFieldID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @TestID INT = 0, 
			@ClientContributionTableFieldPageID int,
			@UnreconcliedTableDetailFieldPageID int,
			@FromTableRowID INT, 
			@Text VARCHAR(255), 
			@LeadID INT,
			@MatterID INT,
			@TableRowID INT,
			@NextPaymentDueDate DATETIME, 
			@ParisNo VARCHAR(100),
			@ContributionValue MONEY,
			@MultipleCards bit = 0,
			@ParisNumberRowCount int,
			@NationalInsuranceNo BIT,
			@Now DATETIME = dbo.fn_GetDate_Local()

	/*Get the first table row from thr Payment imports table*/
	SELECT TOP 1 @FromTableRowID = tr.TableRowID
	FROM TableRows tr WITH (NOLOCK)
	WHERE tr.DetailFieldID = @PaymentImportTableDetailFieldID
	AND ClientID = @ClientID
	ORDER BY tr.TableRowID
	
	/*Get the client contribution page from the field*/
	SELECT @ClientContributionTableFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @ClientContributionTableFieldID

	/*Get the unreconciled table page id from the field id*/
	SELECT @UnreconcliedTableDetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @UnreconcliedTableDetailFieldID

	/*For each row that is found do:*/
	WHILE @FromTableRowID > @TestID
	BEGIN

		/*Clear the variables for each loop*/
		SELECT @ParisNo = NULL, @ContributionValue = NULL, @MatterID = NULL, @LeadID = NULL, @NextPaymentDueDate = NULL, @MultipleCards = 0

		/*Get the reference and value for the row*/
		SELECT @ParisNo = rtrim(ltrim(tdv.DetailValue)), @ContributionValue = tdv_value.DetailValue
		FROM TableDetailValues tdv WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv_value WITH (NOLOCK) ON tdv_value.TableRowID = tdv.TableRowID and tdv_value.DetailFieldID = 144935
		WHERE tdv.TableRowID = @FromTableRowID
		AND tdv.DetailFieldID = 144936
		AND tdv.ClientID = @ClientID
		
		/*Try to get the matter id from the paris number*/
		SELECT @MatterID = MatterID, @LeadID = LeadID
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE DetailFieldID = @CustomerRefFieldID
		AND DetailValue = @ParisNo
		
		SELECT @ParisNumberRowCount = @@ROWCOUNT
		
		IF @ParisNumberRowCount = 0
		BEGIN
		
			/*Paris Number could not be matched at all. We should therefor check the bank ref to paris number table*/
			SELECT @ParisNo = tdv_pno.DetailValue
			FROM TableDetailValues tdv_bankref WITH (NOLOCK) 
			INNER JOIN TableDetailValues tdv_pno WITH (NOLOCK) ON tdv_pno.TableRowID = tdv_bankref.TableRowID and tdv_pno.DetailFieldID = 144944
			WHERE tdv_bankref.LeadID = @PaymentImportLeadID 
			AND tdv_bankref.DetailFieldID = @BankRefTableFieldID
			and tdv_bankref.DetailValue = @ParisNo
		
			/*Try to get the matter id from the alternate paris number*/
			SELECT @MatterID = MatterID, @LeadID = LeadID
			FROM MatterDetailValues mdv WITH (NOLOCK)
			WHERE DetailFieldID = @CustomerRefFieldID
			AND DetailValue = @ParisNo
		
			SELECT @ParisNumberRowCount = @@ROWCOUNT

		END
		
		IF @ParisNumberRowCount > 1
		BEGIN
		
			/*If there are multiple records returned see if you can find one that has the "use this card" field ticked*/
			SELECT @MatterID = mdv.MatterID, @LeadID = mdv.LeadID
			FROM MatterDetailValues mdv WITH (NOLOCK)
			INNER JOIN MatterDetailValues mdv_loadif2cards WITH (NOLOCK) ON mdv_loadif2cards.MatterID = mdv.MatterID and mdv_loadif2cards.DetailFieldID = @LoadIf2CardsFieldID and mdv_loadif2cards.ValueInt = 1
			WHERE mdv.DetailFieldID = @CustomerRefFieldID
			AND mdv.DetailValue = @ParisNo
		
			IF @@ROWCOUNT <> 1
			BEGIN
			
				SELECT @MultipleCards = 1
			
			END
		
		END
		ELSE 
		IF @MatterID IS NULL AND @MultipleCards = 0
		BEGIN

			/*Matter still could not be found. Try breaking the ref up by spaces*/
			SELECT @MatterID = MatterID, @LeadID = LeadID
			FROM MatterDetailValues mdv WITH (NOLOCK)
			WHERE DetailFieldID = @CustomerRefFieldID
			AND DetailValue IN (SELECT * FROM dbo.fnTableOfValues (@ParisNo,' '))			

			IF @@ROWCOUNT > 0
			BEGIN
			
				/*Using the broken up paris number try finding the one with this card ticked*/
				SELECT @MatterID = mdv.MatterID, @LeadID = mdv.LeadID
				FROM MatterDetailValues mdv WITH (NOLOCK)
				INNER JOIN MatterDetailValues mdv_loadif2cards WITH (NOLOCK) ON mdv_loadif2cards.MatterID = mdv.MatterID and mdv_loadif2cards.DetailFieldID = @LoadIf2CardsFieldID and mdv_loadif2cards.ValueInt = 1
				WHERE mdv.DetailFieldID = @CustomerRefFieldID
				AND mdv.DetailValue IN (SELECT * FROM dbo.fnTableOfValues (@ParisNo,' '))	
			
				IF @@ROWCOUNT <> 1
				BEGIN
				
					SELECT @MultipleCards = 1
				
				END
			
			END

		END
		
		DECLARE @DateToCompare DATE = @Now
		IF @ClientID = 226
		BEGIN
			/*Added by CS 2012-04-24*/
			SELECT @DateToCompare = DATEADD(DAY, 3, @Now)
		END

		SELECT TOP 1 @NextPaymentDueDate = tdv.ValueDate
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.MatterID = @MatterID
		AND tdv.DetailFieldID = @NextPaymentDueFieldID
		AND tdv.ValueDate > @DateToCompare /*CS 2012-04-24*/
		ORDER BY tdv.ValueDate

		/*SELECT @TestID, @FromTableRowID, @MatterID, @ParisNo, @ContributionValue, @NextPaymentDueDate*/

		IF @MatterID IS NOT NULL AND @ParisNo IS NOT NULL AND @ContributionValue IS NOT NULL AND @NextPaymentDueDate IS NOT NULL AND @MultipleCards = 0
		BEGIN

			/*Add Client Contribution Row to Client Matter*/
			INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyDelete)
			SELECT @ClientID, @LeadID, @MatterID, @ClientContributionTableFieldID, @ClientContributionTableFieldPageID, 1

			SELECT @TableRowID = SCOPE_IDENTITY()

			INSERT INTO TableDetailValues (ClientID, LeadID, MatterID, TableRowID, ResourceListID, DetailFieldID, DetailValue)
			VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, NULL, @ClientContributionHistoryDateFieldID, CONVERT(VARCHAR(10), dbo.fn_GetDate_Local(), 120)),
					(@ClientID, @LeadID, @MatterID, @TableRowID, NULL, @ClientContributionHistoryValueFieldID, CONVERT(VARCHAR(10),@ContributionValue)),
					(@ClientID, @LeadID, @MatterID, @TableRowID, NULL, @ClientContributionHistoryNextPaymentFieldID, CONVERT(VARCHAR(10), @NextPaymentDueDate, 120))

			UPDATE TableRows
			SET DetailFieldID = 145017, DetailFieldPageID = 16247, DenyDelete = 1, DenyEdit = 1 /*Reconciled Payments*/
			WHERE TableRowID = @FromTableRowID

		END
		ELSE
		BEGIN
		
			UPDATE TableRows
			SET DetailFieldID = 145016, DetailFieldPageID = 16246
			WHERE TableRowID = @FromTableRowID

		END
		
		
		SELECT @TestID = @FromTableRowID

		SELECT TOP 1 @FromTableRowID = tr.TableRowID
		FROM TableRows tr WITH (NOLOCK)
		WHERE tr.DetailFieldID = @PaymentImportTableDetailFieldID
		AND ClientID = @ClientID
		AND tr.TableRowID > @FromTableRowID
		ORDER BY tr.TableRowID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReconcileBankTDV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReconcileBankTDV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReconcileBankTDV] TO [sp_executeall]
GO
