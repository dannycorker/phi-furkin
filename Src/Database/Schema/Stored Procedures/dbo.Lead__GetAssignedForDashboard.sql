SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-07-01
-- Description:	Gets a list of Assigned Leads for the current User
-- JWG 2011-05-23 For Quadra, don't show completed leads unless explicitly requested.
-- ACE 2016-01-25 Added overrides
-- =============================================
CREATE PROCEDURE [dbo].[Lead__GetAssignedForDashboard] 
(
	@ClientPersonnelID int,
	@LeadTypeID int = null,
	@AquariumStatusID int = null
)	
AS
BEGIN
	SET NOCOUNT ON;
	
	/* 
		For some clients (just Quadra initially) only show open leads by default, not closed. 
		This can be overridden with the explicit search option of @AquariumStatusID = 4 if required.
	*/
	DECLARE @ClientID int,
			@ClientPersonnelAdminGroupID int,
			@SQLOverrideQuery NVARCHAR(MAX)
			
	SELECT @ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID, @ClientID = cp.ClientID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID

	SELECT TOP (1) @SQLOverrideQuery = sq.QueryText
	FROM Overrides ovr WITH (NOLOCK)
	INNER JOIN SqlQuery sq WITH (NOLOCK) ON sq.QueryID = ovr.QueryID AND sq.ClientID = @ClientID
	WHERE ovr.ClientID = @ClientID
	AND ovr.OverrideTypeID = 3 /*Leads ASSIGNED TO YOU override*/
	AND ovr.ClientPersonnelAdminGroupID IN (@ClientPersonnelAdminGroupID, -1)
	ORDER BY ovr.ClientPersonnelAdminGroupID DESC 
	/*ORDER DESC So we get our group first, if no group set for me then use fallback (IE -1)*/
	
	IF @SQLOverrideQuery IS NOT NULL
	BEGIN
	
		EXECUTE sp_executesql 
			@SQLOverrideQuery, 
			N'@ClientPersonnelID INT, @LeadTypeID INT, @AquariumStatusID INT, @ClientID INT, @ClientPersonnelAdminGroupID INT', 
			@ClientPersonnelID, @LeadTypeID, @AquariumStatusID, @ClientID, @ClientPersonnelAdminGroupID

	END
	ELSE
	IF EXISTS (SELECT * FROM dbo.ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @ClientPersonnelID AND cp.ClientID = 173)
	BEGIN
		SELECT
			l.LeadID,
			c.CustomerID,
			c.Fullname AS [Name],
			lt.LeadTypeName AS [LeadType],
			l.AssignedDate As [DateAssigned],
			aqs.AquariumStatusName,
			c.IsBusiness
		FROM Lead l WITH (NOLOCK)
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = l.AssignedTo
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
		INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
		INNER JOIN AquariumStatus aqs WITH (NOLOCK) ON aqs.AquariumStatusID = l.AquariumStatusID
		WHERE cp.ClientPersonnelID = @ClientPersonnelID
		AND ((@LeadTypeID IS NULL) OR (l.LeadTypeID = @LeadTypeID))
		AND (
				/* Lead status exactly matches the filter specified (including closed leads)... */
				(@AquariumStatusID = l.AquariumStatusID)
				OR
				/* ...or no filter specified, in which case show everything except closed leads */
				(@AquariumStatusID IS NULL AND (l.AquariumStatusID IS NULL OR l.AquariumStatusID <> 4))
		)
		ORDER BY l.AssignedDate DESC
	END
	ELSE
	BEGIN
		SELECT
			l.LeadID,
			c.CustomerID,
			c.Fullname AS [Name],
			lt.LeadTypeName AS [LeadType],
			l.AssignedDate As [DateAssigned],
			aqs.AquariumStatusName,
			c.IsBusiness
		FROM Lead l WITH (NOLOCK)
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = l.AssignedTo
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
		INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
		INNER JOIN AquariumStatus aqs WITH (NOLOCK) ON aqs.AquariumStatusID = l.AquariumStatusID
		WHERE cp.ClientPersonnelID = @ClientPersonnelID
		AND ((@LeadTypeID IS NULL) OR (l.LeadTypeID = @LeadTypeID))
		AND ((@AquariumStatusID IS NULL) OR (l.AquariumStatusID = @AquariumStatusID))
		ORDER BY l.AssignedDate DESC
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetAssignedForDashboard] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__GetAssignedForDashboard] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetAssignedForDashboard] TO [sp_executeall]
GO
