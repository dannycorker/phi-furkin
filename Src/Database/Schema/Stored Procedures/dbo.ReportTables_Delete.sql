SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReportTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTables_Delete]
(

	@ReportTableID int   
)
AS


				DELETE FROM [dbo].[ReportTables] WITH (ROWLOCK) 
				WHERE
					[ReportTableID] = @ReportTableID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTables_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Delete] TO [sp_executeall]
GO
