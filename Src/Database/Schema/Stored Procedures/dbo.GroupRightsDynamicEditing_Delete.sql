SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the GroupRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamicEditing_Delete]
(

	@GroupRightsDynamicEditingID int   
)
AS


				DELETE FROM [dbo].[GroupRightsDynamicEditing] WITH (ROWLOCK) 
				WHERE
					[GroupRightsDynamicEditingID] = @GroupRightsDynamicEditingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamicEditing_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Delete] TO [sp_executeall]
GO
