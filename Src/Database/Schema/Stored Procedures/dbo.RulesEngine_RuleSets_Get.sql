SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Ian Slack (copy of RulesEngine_RuleSets_GetRuleSetsByClientID)
-- Create date:   2013-05-29
-- Description:   Returns all rule sets for a specific client
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSets_Get]
(
      @ClientID INT,
      @ChangeSetID INT
)
AS
BEGIN
      
	SET NOCOUNT ON;
      
	SELECT      
			rs.RuleSetID, rs.Name, rs.InputTypeID, dtIn.Name AS InputType, rs.OutputTypeID, dtOut.Name AS OutputType,
			rs.WhenCreated, cpCreate.UserName AS WhoCreated, rs.WhenModified, cpMod.UserName AS WhoModified, rs.Description,
			rs.ExposeForUse, ISNULL(rs.RuleSetTypeID, 0) AS RuleSetTypeID,
			(SELECT COUNT(*) FROM dbo.RulesEngine_Rules RE_R WITH (NOLOCK) WHERE RE_R.RuleSetID = rs.RuleSetID ) AS RulesCount,
			-- IS: 2016-11-24 THIS CAN CAUSE AN EXCEPTION, IF THE EXECUTION PLAN THINKS Value IS NOT AN INT 
			--(SELECT COUNT(*) FROM dbo.RulesEngine_RuleParameters WHERE ParameterTypeID = 3 AND Value = rs.RuleSetID) AS NumReferences
			(SELECT COUNT(*) FROM dbo.RulesEngine_RuleParameters WHERE ParameterTypeID = 3 AND Value = CAST(rs.RuleSetID AS VARCHAR)) AS NumReferences
	FROM dbo.RulesEngine_RuleSets rs WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_DataTypes dtIn WITH (NOLOCK) ON rs.InputTypeID = dtIn.TypeID
	INNER JOIN dbo.RulesEngine_DataTypes dtOut WITH (NOLOCK) ON rs.OutputTypeID = dtOut.TypeID
	INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON rs.WhoCreated = cpCreate.ClientPersonnelID
	INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON rs.WhoModified = cpMod.ClientPersonnelID
	--LEFT JOIN RuleSetUsage u ON rs.RuleSetID = u.RuleSetID
	WHERE rs.ClientID = @ClientID
	AND rs.ChangeSetID = @ChangeSetID
	ORDER BY rs.Name

	SELECT 'Premiums Engine' AS Title, '#/' AS Path
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSets_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_Get] TO [sp_executeall]
GO
