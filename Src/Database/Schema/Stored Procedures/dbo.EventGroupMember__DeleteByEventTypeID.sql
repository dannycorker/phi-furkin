SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-11-2012
-- Description:	Deletes event group members
-- =============================================
CREATE PROCEDURE [dbo].[EventGroupMember__DeleteByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	DELETE FROM EventGroupMember 
	WHERE EventTypeID=@EventTypeID AND ClientID=@ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroupMember__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember__DeleteByEventTypeID] TO [sp_executeall]
GO
