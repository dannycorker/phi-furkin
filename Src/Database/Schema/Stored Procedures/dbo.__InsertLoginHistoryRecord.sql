SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 4th April 2012
-- Description:	Inserts a login history record
-- =============================================
CREATE PROCEDURE [dbo].[__InsertLoginHistoryRecord]
	@UserName VARCHAR(255),
	@LoginDateTime DATETIME,
	@AppLogin BIT,
	@IsSuccess BIT,
	@ClientID INT,
	@ClientPersonnelID INT,
	@AttemptedLogins INT,
	@AccountDisabled INT,
	@Notes VARCHAR(2000),
	@ThirdPartySystemID INT,
	@UserIPDetails VARCHAR(50) = NULL,
	@IsLogout BIT = NULL

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[LoginHistory]
		( 
			UserName,
			LoginDateTime,
			AppLogin, 
			IsSuccess,
			ClientID, 
			ClientPersonnelID,
			AttemptedLogins,
			AccountDisabled,
			Notes,
			ThirdPartySystemID,
			HostName,
			UserIPDetails,
			IsLogout
		)
		VALUES
		(
			@UserName,
			@LoginDateTime,
			@AppLogin,
			@IsSuccess,
			@ClientID,
			@ClientPersonnelID,
			@AttemptedLogins,
			@AccountDisabled,
			@Notes,
			@ThirdPartySystemID,			
			HOST_NAME(),
			@UserIPDetails,
			@IsLogout
		)	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertLoginHistoryRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__InsertLoginHistoryRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertLoginHistoryRecord] TO [sp_executeall]
GO
