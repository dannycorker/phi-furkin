SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 17/03/2010
-- Description:	Gets all the logon messages relevant for a particular user
-- =============================================
CREATE PROCEDURE [dbo].[GetLogonMessagesByClientPersonnelID]
	-- Add the parameters for the stored procedure here
	@ClientPersonnelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ClientPersonnelAdminGroupID int
	DECLARE @ClientID int
	
	SELECT @ClientPersonnelAdminGroupID = 
		ClientPersonnelAdminGroupID, @ClientID = ClientID
		FROM ClientPersonnel WITH (NOLOCK) 
		WHERE ClientPersonnelID = @ClientPersonnelID

    -- Insert statements for procedure here
	SELECT *
	FROM dbo.SystemMessageAssignment sma WITH (NOLOCK)
	INNER JOIN dbo.SystemMessage sm WITH (NOLOCK) ON sm.SystemMessageID = sma.SystemMessageID
	WHERE sma.ClientPersonnelID = @ClientPersonnelID
	AND sm.Enabled = 1
	
	UNION
	
	SELECT *
	FROM dbo.SystemMessageAssignment sma WITH (NOLOCK)
	INNER JOIN dbo.SystemMessage sm WITH (NOLOCK) ON sm.SystemMessageID = sma.SystemMessageID	WHERE sma.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	AND sm.Enabled = 1
	
	UNION
	
	SELECT *
	FROM dbo.SystemMessageAssignment sma WITH (NOLOCK)
	INNER JOIN dbo.SystemMessage sm WITH (NOLOCK) ON sm.SystemMessageID = sma.SystemMessageID	WHERE sma.ClientID = @ClientID
	AND sma.ClientPersonnelAdminGroupID IS NULL
	AND sma.ClientPersonnelID IS NULL
	AND sm.Enabled = 1
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetLogonMessagesByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLogonMessagesByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLogonMessagesByClientPersonnelID] TO [sp_executeall]
GO
