SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeMandatoryEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_GetByEventTypeMandatoryEventID]
(

	@EventTypeMandatoryEventID int   
)
AS


				SELECT
					[EventTypeMandatoryEventID],
					[ClientID],
					[LeadTypeID],
					[ToEventTypeID],
					[RequiredEventID]
				FROM
					[dbo].[EventTypeMandatoryEvent] WITH (NOLOCK) 
				WHERE
										[EventTypeMandatoryEventID] = @EventTypeMandatoryEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_GetByEventTypeMandatoryEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_GetByEventTypeMandatoryEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_GetByEventTypeMandatoryEventID] TO [sp_executeall]
GO
