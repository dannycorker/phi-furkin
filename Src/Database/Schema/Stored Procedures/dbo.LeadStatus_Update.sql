SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_Update]
(

	@StatusID int   ,

	@ClientID int   ,

	@StatusName varchar (50)  ,

	@StatusDescription varchar (250)  ,

	@SourceID int   ,

	@LeadTypeID int   ,

	@IsShared bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadStatus]
				SET
					[ClientID] = @ClientID
					,[StatusName] = @StatusName
					,[StatusDescription] = @StatusDescription
					,[SourceID] = @SourceID
					,[LeadTypeID] = @LeadTypeID
					,[IsShared] = @IsShared
				WHERE
[StatusID] = @StatusID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Update] TO [sp_executeall]
GO
