SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChartSeries table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_Delete]
(

	@ChartSeriesID int   
)
AS


				DELETE FROM [dbo].[ChartSeries] WITH (ROWLOCK) 
				WHERE
					[ChartSeriesID] = @ChartSeriesID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Delete] TO [sp_executeall]
GO
