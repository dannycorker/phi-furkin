SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_Delete]
(

	@ClientStyleID int   
)
AS


				DELETE FROM [dbo].[ClientStyle] WITH (ROWLOCK) 
				WHERE
					[ClientStyleID] = @ClientStyleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Delete] TO [sp_executeall]
GO
