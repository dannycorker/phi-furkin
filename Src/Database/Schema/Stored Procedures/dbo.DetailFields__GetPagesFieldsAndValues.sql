SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 31/03/2016
-- Description:	Gets the pages, fields and values for the read only view in AQ4
	-- AMG 2016-04-06: Added MaintainHistory
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetPagesFieldsAndValues]

	@DetailFieldSubTypeID INT, -- this is lead or matter etc.,
	@ClientID INT,
	@LeadTypeID INT,
	@ObjectID INT

AS
BEGIN

	SET NOCOUNT ON;

	IF @DetailFieldSubTypeID = 10 -- Customer level
	BEGIN
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, cdv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(cdv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = df.DetailFieldID AND cdv.CustomerID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND cdv.ValueInt = li.LookupListItemID
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC	
	END

	IF @DetailFieldSubTypeID = 1  -- Lead level
	BEGIN
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, ldv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(ldv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = df.DetailFieldID AND ldv.LeadID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND ldv.ValueInt = li.LookupListItemID		
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC
	END
	
	IF @DetailFieldSubTypeID = 11  -- Case level
	BEGIN
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, csdv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(csdv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN CaseDetailValues csdv WITH (NOLOCK) ON csdv.DetailFieldID = df.DetailFieldID AND csdv.CaseID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND csdv.ValueInt = li.LookupListItemID		
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC
	END

	IF @DetailFieldSubTypeID = 2  -- Matter level
	BEGIN	
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, mdv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(mdv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = df.DetailFieldID AND mdv.MatterID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND mdv.ValueInt = li.LookupListItemID		
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC	
	END
	
	IF @DetailFieldSubTypeID = 12  -- Client level
	BEGIN	
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, cldv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(cldv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN ClientDetailValues cldv WITH (NOLOCK) ON cldv.DetailFieldID = df.DetailFieldID AND cldv.ClientID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND cldv.ValueInt = li.LookupListItemID		
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC	
	END	
	
	IF @DetailFieldSubTypeID = 13  -- Client Personnel level
	BEGIN	
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, cpdv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(cpdv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN ClientPersonnelDetailValues cpdv WITH (NOLOCK) ON cpdv.DetailFieldID = df.DetailFieldID AND cpdv.ClientPersonnelID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND cpdv.ValueInt = li.LookupListItemID		
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC	
	END	
	
	IF @DetailFieldSubTypeID = 14  -- Contact level
	BEGIN	
		SELECT dp.DetailFieldPageID, dp.PageCaption, dp.PageOrder, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, cndv.DetailValue) AS DetailValue, COALESCE(df.Encrypt, 0) AS Encrypt, COALESCE(cndv.EncryptedValue,'') AS EncryptedValue
		FROM DetailFieldPages dp WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldPageID = dp.DetailFieldPageID AND df.Enabled=1 AND (df.Hidden=0 OR df.Hidden IS NULL)
		LEFT JOIN ContactDetailValues cndv WITH (NOLOCK) ON cndv.DetailFieldID = df.DetailFieldID AND cndv.ContactID = @ObjectID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND cndv.ValueInt = li.LookupListItemID
		WHERE dp.ClientID=@ClientID AND dp.LeadOrMatter=@DetailFieldSubTypeID AND dp.Enabled=1 AND dp.LeadTypeID=@LeadTypeID
		ORDER BY dp.PageOrder, dp.PageCaption, df.FieldOrder ASC	
	END		

END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetPagesFieldsAndValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetPagesFieldsAndValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetPagesFieldsAndValues] TO [sp_executeall]
GO
