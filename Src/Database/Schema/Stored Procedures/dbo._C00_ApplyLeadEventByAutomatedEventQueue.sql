SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-09-22
-- Description:	Apply an event using the AutomatedEventQueue
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue]
	@LeadEventID INT,
	@AutomatedEventTypeID INT,
	@WhoCreated INT,
	@ThreadToFollowUp INT = NULL  -- 0=All, -1=None, NULL attempts to find an EventChoice before defaulting to None.
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @NewLeadEventID INT
	
	INSERT INTO dbo.AutomatedEventQueue (
		ClientID,
		CustomerID,
		LeadID,
		CaseID,
		FromLeadEventID,
		FromEventTypeID,
		WhenCreated,
		WhoCreated,
		AutomatedEventTypeID,
		RunAsUserID,
		ThreadToFollowUp,
		InternalPriority,
		BumpCount,
		ErrorCount
	)
	SELECT 
		l.ClientID,
		l.CustomerID,
		l.LeadID,
		le.CaseID,
		le.LeadEventID,
		le.EventTypeID,
		dbo.fn_GetDate_Local(),
		@WhoCreated,
		@AutomatedEventTypeID,
		@WhoCreated,
		COALESCE(@ThreadToFollowUp, ec.ThreadNumber, -1), 
		1,
		0 as BumpCount,
		0 as ErrorCount
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID
	LEFT JOIN dbo.EventChoice ec WITH (NOLOCK) on ec.EventTypeID = le.EventTypeID and ec.NextEventTypeID = @AutomatedEventTypeID
	WHERE le.LeadEventID = @LeadEventID
	
	SELECT @NewLeadEventID = SCOPE_IDENTITY()
	
	RETURN @NewLeadEventID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] TO [sp_executeall]
GO
