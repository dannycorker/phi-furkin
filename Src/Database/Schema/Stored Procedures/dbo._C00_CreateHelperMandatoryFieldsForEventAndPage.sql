SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-10-22
-- Description:	Take all fields on a page and attach them to an event
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateHelperMandatoryFieldsForEventAndPage]
(
@EventTypeID INT,
@DetailFieldPageID INT,
@Mandatory BIT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @RowCount INT

	IF @Mandatory = 1
	BEGIN
		INSERT EventTypeMandatoryField ( ClientID, LeadTypeID, EventTypeID, DetailFieldID )
		SELECT df.ClientID, et.LeadTypeID, @EventTypeID, df.DetailFieldID
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.EventType et WITH (NOLOCK) on et.ClientID = df.ClientID
		WHERE df.DetailFieldPageID = @DetailFieldPageID
		and et.EventTypeID = @EventTypeID
		and not exists ( SELECT * 
		                 FROM EventTypeMandatoryField smf WITH (NOLOCK) 
		                 WHERE smf.DetailFieldID = df.DetailFieldID 
		                 and smf.EventTypeID = et.EventTypeID )

		SELECT @RowCount = @@ROWCOUNT		                 
	END
	ELSE
	BEGIN
		INSERT EventTypeHelperField ( ClientID, LeadTypeID, EventTypeID, DetailFieldID )
		SELECT df.ClientID, et.LeadTypeID, @EventTypeID, df.DetailFieldID
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.EventType et WITH (NOLOCK) on et.ClientID = df.ClientID
		WHERE df.DetailFieldPageID = @DetailFieldPageID
		and et.EventTypeID = @EventTypeID
		and not exists ( SELECT * 
		                 FROM EventTypeHelperField smf WITH (NOLOCK) 
		                 WHERE smf.DetailFieldID = df.DetailFieldID 
		                 and smf.EventTypeID = et.EventTypeID )

		SELECT @RowCount = @@ROWCOUNT
	END

	SELECT convert(varchar,@RowCount) + CASE @Mandatory WHEN 1 THEN ' Mandatory fields created.' ELSE ' Helper fields created.' END [Success]
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateHelperMandatoryFieldsForEventAndPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateHelperMandatoryFieldsForEventAndPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateHelperMandatoryFieldsForEventAndPage] TO [sp_executeall]
GO
