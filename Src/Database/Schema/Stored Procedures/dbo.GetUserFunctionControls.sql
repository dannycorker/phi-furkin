SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-24
-- Description:	Get all function control records for this User
-- =============================================
CREATE PROCEDURE [dbo].[GetUserFunctionControls]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@LeadTypeID int

	-- TODO: remove the nullability of @LeadTypeID,
	-- TODO: remove  OR @LeadTypeID IS NULL from both clauses below
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @GroupID int

	SELECT @GroupID = ClientPersonnelAdminGroupID
	FROM ClientPersonnel 
	WHERE ClientPersonnelID = @UserID

    -- Insert statements for procedure here
	SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID, gfc.LeadTypeID 
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND (IsNull(gfc.LeadTypeID, @LeadTypeID) = @LeadTypeID OR @LeadTypeID < 0 OR @LeadTypeID IS NULL)
	AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
	UNION
	SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID, ufc.LeadTypeID 
	FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
	WHERE ufc.ClientPersonnelID = @UserID
	AND (IsNull(ufc.LeadTypeID, @LeadTypeID) = @LeadTypeID OR @LeadTypeID < 0 OR @LeadTypeID IS NULL)

END








GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserFunctionControls] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserFunctionControls] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserFunctionControls] TO [sp_executeall]
GO
