SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReportTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTables_Update]
(

	@ReportTableID int   ,

	@ReportTableName varchar (50)  ,

	@DisplayOrder int   ,

	@RequiresClientSecurity bit   ,

	@IsLookupTable bit   ,

	@ShowAquariumValues bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReportTables]
				SET
					[ReportTableName] = @ReportTableName
					,[DisplayOrder] = @DisplayOrder
					,[RequiresClientSecurity] = @RequiresClientSecurity
					,[IsLookupTable] = @IsLookupTable
					,[ShowAquariumValues] = @ShowAquariumValues
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ReportTableID] = @ReportTableID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTables_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Update] TO [sp_executeall]
GO
