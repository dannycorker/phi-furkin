SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeletePhoneNumberVerification] @PhoneNumberVerificationID int
AS
Delete From PhoneNumberVerifications
Where PhoneNumberVerificationID = @PhoneNumberVerificationID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeletePhoneNumberVerification] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeletePhoneNumberVerification] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeletePhoneNumberVerification] TO [sp_executeall]
GO
