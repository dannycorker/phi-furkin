SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderSectionLocaterKeyword table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderSectionLocaterKeywordID int   = null ,

	@ClientID int   = null ,

	@DataLoaderMapID int   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@DataLoaderMapSectionID int   = null ,

	@Keyword varchar (250)  = null ,

	@DataLoaderKeywordMatchTypeID int   = null ,

	@KeywordNotes varchar (2000)  = null ,

	@RelativeRow int   = null ,

	@AbsoluteCol int   = null ,

	@IsForSectionStart bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderSectionLocaterKeywordID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderObjectTypeID]
	, [DataLoaderMapSectionID]
	, [Keyword]
	, [DataLoaderKeywordMatchTypeID]
	, [KeywordNotes]
	, [RelativeRow]
	, [AbsoluteCol]
	, [IsForSectionStart]
    FROM
	[dbo].[DataLoaderSectionLocaterKeyword] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderSectionLocaterKeywordID] = @DataLoaderSectionLocaterKeywordID OR @DataLoaderSectionLocaterKeywordID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DataLoaderMapID] = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	AND ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([DataLoaderMapSectionID] = @DataLoaderMapSectionID OR @DataLoaderMapSectionID IS NULL)
	AND ([Keyword] = @Keyword OR @Keyword IS NULL)
	AND ([DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID OR @DataLoaderKeywordMatchTypeID IS NULL)
	AND ([KeywordNotes] = @KeywordNotes OR @KeywordNotes IS NULL)
	AND ([RelativeRow] = @RelativeRow OR @RelativeRow IS NULL)
	AND ([AbsoluteCol] = @AbsoluteCol OR @AbsoluteCol IS NULL)
	AND ([IsForSectionStart] = @IsForSectionStart OR @IsForSectionStart IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderSectionLocaterKeywordID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderObjectTypeID]
	, [DataLoaderMapSectionID]
	, [Keyword]
	, [DataLoaderKeywordMatchTypeID]
	, [KeywordNotes]
	, [RelativeRow]
	, [AbsoluteCol]
	, [IsForSectionStart]
    FROM
	[dbo].[DataLoaderSectionLocaterKeyword] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderSectionLocaterKeywordID] = @DataLoaderSectionLocaterKeywordID AND @DataLoaderSectionLocaterKeywordID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DataLoaderMapID] = @DataLoaderMapID AND @DataLoaderMapID is not null)
	OR ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([DataLoaderMapSectionID] = @DataLoaderMapSectionID AND @DataLoaderMapSectionID is not null)
	OR ([Keyword] = @Keyword AND @Keyword is not null)
	OR ([DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID AND @DataLoaderKeywordMatchTypeID is not null)
	OR ([KeywordNotes] = @KeywordNotes AND @KeywordNotes is not null)
	OR ([RelativeRow] = @RelativeRow AND @RelativeRow is not null)
	OR ([AbsoluteCol] = @AbsoluteCol AND @AbsoluteCol is not null)
	OR ([IsForSectionStart] = @IsForSectionStart AND @IsForSectionStart is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Find] TO [sp_executeall]
GO
