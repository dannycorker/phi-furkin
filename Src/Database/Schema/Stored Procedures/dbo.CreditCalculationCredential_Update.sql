SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CreditCalculationCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_Update]
(

	@CreditCalculationCredentialID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@SecretKey varchar (32)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CreditCalculationCredential]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[SecretKey] = @SecretKey
				WHERE
[CreditCalculationCredentialID] = @CreditCalculationCredentialID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Update] TO [sp_executeall]
GO
