SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/09/2013
-- Description:	Inserts the question into the sms survey table for sms survey customer create
-- Modified By PR on 23/01/2014 - Added the following SMS Survey Result columns:-
--									Language, Sentiment Polarity, Sentiment Confidence, Mixed Sentiment, Rules Engine Score
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SmsSurveyTableInsertQuestion]

	@SMSQuestionID INT,
	@LeadTypeID INT, 
	@LeadID INT,
	@MatterID INT,
	@ClientID INT,
	@SMS_Text VARCHAR(2000)
	
AS
BEGIN
		DECLARE @EventTypeID INT	
		DECLARE @TableDetailFieldID INT	
		DECLARE @DetailFieldPageID INT
		DECLARE @TableRowID INT
		
		SELECT @EventTypeID = qet.EventTypeID
		FROM SMSQuestionEventType qet WITH (NOLOCK) 
		WHERE qet.SMSQuestionID = @SMSQuestionID
		
		SELECT TOP 1 @TableDetailFieldID = t.DetailFieldID
		FROM ThirdPartyFieldMapping t WITH (NOLOCK) 
		WHERE ThirdPartyFieldGroupID = 24
		AND t.ThirdPartyFieldID = 372
		AND t.LeadTypeID = @LeadTypeID
		
		SELECT @DetailFieldPageID = d.DetailFieldPageID
		FROM DetailFields d WITH (NOLOCK)
		WHERE d.DetailFieldID = @TableDetailFieldID
		
		INSERT INTO TableRows(ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID)
		SELECT @ClientID, @LeadID, @MatterID, @TableDetailFieldID, @DetailFieldPageID

		SELECT @TableRowID = SCOPE_IDENTITY()

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
		SELECT @TableRowID, t.ColumnFieldID, CASE t.ThirdPartyFieldID 
								WHEN 372 THEN CONVERT(VARCHAR(20), dbo.fn_GetDate_Local(),120) 
								WHEN 373 THEN CONVERT(VARCHAR(2000),@EventTypeID)
								WHEN 374 THEN CONVERT(VARCHAR(2000),@SMSQuestionID)
								WHEN 375 THEN CONVERT(VARCHAR(2000),@SMS_Text) 
								WHEN 376 THEN '' 
								WHEN 377 THEN '' 
								WHEN 378 THEN '' 
								WHEN 379 THEN '' 
								WHEN 384 THEN ''
								WHEN 385 THEN ''
								WHEN 820 THEN ''
								WHEN 821 THEN ''
								WHEN 822 THEN ''
								WHEN 823 THEN ''
								WHEN 824 THEN ''
								END, @LeadID, @MatterID, @ClientID
		FROM ThirdPartyFieldMapping t WITH (NOLOCK) 
		WHERE t.ThirdPartyFieldGroupID = 24 AND t.ThirdPartyFieldID IN (372,373,374,375,376,377,378,379,384,385,820,821,822,823,824)
		AND t.LeadTypeID = @LeadTypeID 
		AND ColumnFieldID IS NOT NULL
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SmsSurveyTableInsertQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SmsSurveyTableInsertQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SmsSurveyTableInsertQuestion] TO [sp_executeall]
GO
