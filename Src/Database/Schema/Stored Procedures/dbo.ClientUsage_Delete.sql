SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientUsage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientUsage_Delete]
(

	@ClientID int   
)
AS


				DELETE FROM [dbo].[ClientUsage] WITH (ROWLOCK) 
				WHERE
					[ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientUsage_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Delete] TO [sp_executeall]
GO
