SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the OutcomeDelayedEmails table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeDelayedEmails_Find]
(

	@SearchUsingOR bit   = null ,

	@OutcomeDelayedEmailID int   = null ,

	@CustomerID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@DateOutcomeEmailSent datetime   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OutcomeDelayedEmailID]
	, [CustomerID]
	, [ClientQuestionnaireID]
	, [DateOutcomeEmailSent]
	, [ClientID]
    FROM
	[dbo].[OutcomeDelayedEmails] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeDelayedEmailID] = @OutcomeDelayedEmailID OR @OutcomeDelayedEmailID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([DateOutcomeEmailSent] = @DateOutcomeEmailSent OR @DateOutcomeEmailSent IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OutcomeDelayedEmailID]
	, [CustomerID]
	, [ClientQuestionnaireID]
	, [DateOutcomeEmailSent]
	, [ClientID]
    FROM
	[dbo].[OutcomeDelayedEmails] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeDelayedEmailID] = @OutcomeDelayedEmailID AND @OutcomeDelayedEmailID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([DateOutcomeEmailSent] = @DateOutcomeEmailSent AND @DateOutcomeEmailSent is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeDelayedEmails_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Find] TO [sp_executeall]
GO
