SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Paul Richardson>
-- Create date: <17th September 2009>
-- Description:	<Used for Dynamic Searching of Customers>
-- Updates:		18/09/09 - CT - Added paging facility using ROW_NUMBER() (still in beta!)
-- =============================================
CREATE PROCEDURE [dbo].[Customers__SpecialisedSearch]

@ClientID VARCHAR(12), 
@Title VARCHAR(MAX) = NULL,
@FullName VARCHAR(MAX) = NULL,
@DateOfBirth VARCHAR(MAX) = NULL,
@Address1 VARCHAR(MAX) = NULL,
@County VARCHAR(MAX) = NULL,
@Postcode VARCHAR(MAX) = NULL,
@Telephone VARCHAR(MAX) = NULL,
@EmailAddress VARCHAR(MAX) = NULL,
@StartRow INT = NULL,
@EndRow INT = NULL

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @DetailFieldID INT
	IF @ClientID=226
	BEGIN
		set @DetailFieldID = 144983 /**/
	END
	ELSE IF @ClientID = 252
	BEGIN
		set @DetailFieldID = 157355 /*SWIFT Number*/
	END
	ELSE
	BEGIN 
		set @DetailFieldID = 87168
	END
	
	DECLARE @query VARCHAR(MAX)
	
	SET @query = '
	SELECT * FROM
	(
	SELECT ROW_NUMBER() OVER (order by Customers.CustomerID) as RowNumber, 
	dbo.Titles.Title, dbo.Customers.*, dbo.Lead.LeadID, cs.CaseID, mdv.DetailValue 
	FROM dbo.Customers WITH (NOLOCK) 
	INNER JOIN dbo.Lead WITH (NOLOCK) ON dbo.Customers.CustomerID = dbo.Lead.CustomerID 
	INNER JOIN dbo.Titles WITH (NOLOCK) ON dbo.Customers.TitleID = dbo.Titles.TitleID 
	INNER JOIN dbo.Cases cs WITH (NOLOCK) ON cs.LeadID = dbo.Lead.LeadID 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = cs.CaseID 
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID=' + CONVERT(VARCHAR, @DetailFieldID) + ' 
	where dbo.customers.ClientID=' + @ClientID + ' and dbo.Lead.LeadTypeID IN(1194,1196) and dbo.Customers.Test=0 '  
	
	--PR 25-04-2013 - Added LeadType into criteria for Client 252 only LeadTypeID=1194
	/*CS 2014-02-20 CS Included Payments Import lead type*/

	IF(@Title IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and dbo.Customers.TitleID in (' + @Title + ')'
	END
	
	IF(@DateOfBirth IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and DateOfBirth in (' + @DateOfBirth + ')'
	END

	IF(@Address1 IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and Address1 in (' + @Address1 + ')'
	END
	
	IF(@County IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and County in (' + @County + ')'
	END
	
	IF(@Postcode IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and PostCode in (' + @Postcode + ')'
	END
	
	IF(@Telephone IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and (HomeTelephone in (' + @Telephone + ')'
		SELECT @query = @query + ' or DayTimeTelephoneNumber in (' + @Telephone + ')'
		SELECT @query = @query + ' or MobileTelephone in (' + @Telephone + ')'
		SELECT @query = @query + ' or CompanyTelephone in (' + @Telephone + ')'
		SELECT @query = @query + ' or WorksTelephone in (' + @Telephone + '))'
	END
	
	IF(@EmailAddress IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and EmailAddress in (' + @EmailAddress + ')'
	END
	
	IF(@FullName IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' and ' + @FullName
	END
	SELECT @query = @query + ' ) As Results'
	
	/* Filter the result set to a subset of rows if paging is required */
	IF (@StartRow IS NOT NULL AND @EndRow IS NOT NULL)
	BEGIN
		SELECT @query = @query + ' WHERE RowNumber Between ' + CONVERT(VARCHAR(10), @StartRow) + ' AND ' + CONVERT(VARCHAR(10), @EndRow)
	END
		
	EXECUTE(@query)
	

END






GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__SpecialisedSearch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__SpecialisedSearch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__SpecialisedSearch] TO [sp_executeall]
GO
