SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-06
-- Description:	Gets all the claim deduction rows from all linked matters
-- Used by:		DF 147723
-- Updates:		SB	2012-03-29	Change to select the treatment dates from the matter instead of the table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetClaimDeductions] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	tdvPolicy.ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, llReason.ItemValue AS Reason, tdvAmount.ValueMoney AS Amount,
			tdvItemNotIncluded.DetailValue AS ItemNotCovered
	FROM dbo.TableRows r WITH (NOLOCK)
	INNER JOIN dbo.TableDetailValues tdvRow WITH (NOLOCK) ON r.TableRowID = tdvRow.TableRowID AND tdvRow.DetailFieldID = 147299
	INNER JOIN dbo.TableDetailValues tdvPolicy WITH (NOLOCK) ON tdvPolicy.TableRowID = tdvRow.ValueInt AND tdvPolicy.DetailFieldID = 144350
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON rdvSection.ResourceListID = tdvPolicy.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON rdvSubSection.ResourceListID = tdvPolicy.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvTreatmentStart WITH (NOLOCK) ON tdvPolicy.TableRowID = tdvTreatmentStart.TableRowID AND tdvTreatmentStart.DetailFieldID = 144349 
	INNER JOIN dbo.TableDetailValues tdvReason WITH (NOLOCK) ON r.TableRowID = tdvReason.TableRowID AND tdvReason.DetailFieldID = 147300
	INNER JOIN dbo.LookupListItems llReason WITH (NOLOCK) ON tdvReason.ValueInt = llReason.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 147301
	LEFT JOIN dbo.TableDetailValues tdvItemNotIncluded WITH (NOLOCK) ON r.TableRowID = tdvItemNotIncluded.TableRowID AND tdvItemNotIncluded.DetailFieldID = 148432
	WHERE r.MatterID = @MatterID
	AND r.DetailFieldID = 147302
	AND r.DetailFieldPageID = 16157
	ORDER BY tdvTreatmentStart.ValueDate DESC, tdvPolicy.TableRowID, tdvAmount.TableRowID
	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetClaimDeductions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetClaimDeductions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetClaimDeductions] TO [sp_executeall]
GO
