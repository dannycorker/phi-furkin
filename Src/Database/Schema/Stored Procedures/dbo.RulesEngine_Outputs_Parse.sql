SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Ian Slack
-- Create date:   2018-08-17
-- Description:   Returns parsed import
-- 2018-09-12	GPR created to C600 from C433
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Outputs_Parse]
(
	@ClientID INT,
	@UserID INT,
	@ParentID INT,
	@ImportType VARCHAR(250),
	@ImportXml XML,
	@FileName VARCHAR(250),
	@Mode VARCHAR(50) = 'CSV'
)
AS
BEGIN
      
	SET NOCOUNT ON;


	DECLARE @ParsedCSV VARCHAR(MAX) ='',
			@TotalRows INT = 0,
			@ErrorCount INT = 0,
			@SuccessCount INT = 0,
			@ImportID INT = 0

	EXEC _C00_LogItXML @ClientID, @ParentID, @ImportType, @ImportXml
	
	SELECT @ImportID = @@IDENTITY

	SELECT	ISNULL(T.c.value('col0[1]','VARCHAR(250)'),'') AS Transform,
			ISNULL(T.c.value('col1[1]','VARCHAR(250)'),'') AS Value,
			ISNULL(T.c.value('col2[1]','VARCHAR(250)'),'') AS ParameterOption,
			ISNULL(T.c.value('col3[1]','VARCHAR(250)'),'') AS Coordinates,
			ROW_NUMBER() OVER(ORDER BY T.c) RowID
			INTO #REParse
	FROM	@ImportXml.nodes('/table/row') T(c)
	
	SELECT	po.RuleParameterID, '|'+CAST(po.ParameterOptionID AS VARCHAR)+'|' ParameterOptionMatch
			INTO #ParamMatch
	FROM	RulesEngine_ParameterOptions po WITH (NOLOCK)
	INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = po.RuleParameterID
	WHERE	rp.RuleID = @ParentID
	
	SELECT	
		row.*,
		CASE
			WHEN NOT EXISTS 
				(
					SELECT * 
					FROM	#ParamMatch
					WHERE '|'+Coordinates+'|' LIKE '%'+ParameterOptionMatch+'%'
				) THEN 'parameter/option missmatch'
			WHEN rt.Name IS NULL THEN 'transform not found'
			ELSE ''
		END Error
		INTO #REParsed
	FROM	#REParse row
	INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = @ParentID AND r.ClientID = @ClientID
	LEFT JOIN dbo.RulesEngine_Transforms rt WITH (NOLOCK) ON rt.Name = row.Transform

	IF @Mode = 'CSV'
	BEGIN
		SELECT @ParsedCSV = '"Transform","Value","ParameterOption","Coordinates","Error"'+CHAR(13)+CHAR(10)
		
		SELECT	@ParsedCSV += '"'+ Transform +'","'+ Value +'","'+ ParameterOption +'","'+ Coordinates +'","'+ Error +'"'+CHAR(13)+CHAR(10),
				@TotalRows += 1,
				@ErrorCount += CASE WHEN Error = '' THEN 0 ELSE 1 END,
				@SuccessCount += CASE WHEN Error = '' THEN 1 ELSE 0 END
		FROM	#REParsed row
		WHERE	row.RowID > 1
		
		SELECT @ImportID ImportID, @ParsedCSV ResultCSV, @TotalRows TotalRows, @ErrorCount ErrorCount, @SuccessCount SuccessCount	
	END
	ELSE
	BEGIN
		SELECT	*
		FROM	#REParsed row
		WHERE	row.RowID > 1
	END

	DROP TABLE #REParse
	DROP TABLE #ParamMatch
	DROP TABLE #REParsed
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Outputs_Parse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Outputs_Parse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Outputs_Parse] TO [sp_executeall]
GO
