SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteAllCustomerOutcomes] @ClientQuestionnaireID int

AS

Delete From CustomerOutcomes
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteAllCustomerOutcomes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteAllCustomerOutcomes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteAllCustomerOutcomes] TO [sp_executeall]
GO
