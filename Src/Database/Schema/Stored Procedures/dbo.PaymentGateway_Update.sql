SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentGateway_Update]
(

	@PaymentGatewayID int   ,

	@Gateway varchar (100)  ,

	@Name varchar (100)  ,

	@Description varchar (300)  ,

	@Url varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PaymentGateway]
				SET
					[Gateway] = @Gateway
					,[Name] = @Name
					,[Description] = @Description
					,[Url] = @Url
				WHERE
[PaymentGatewayID] = @PaymentGatewayID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Update] TO [sp_executeall]
GO
