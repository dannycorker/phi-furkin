SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTask_Delete]
(

	@TaskID int   
)
AS


				DELETE FROM [dbo].[AutomatedTask] WITH (ROWLOCK) 
				WHERE
					[TaskID] = @TaskID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Delete] TO [sp_executeall]
GO
