SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description: Updates a NotificationRecipient row
-- =============================================
CREATE PROCEDURE [dbo].[NotificationRecipient__Update]

	@NotificationRecipientID INT,
	@NotificationID INT,
    @ClientID INT,
    @SubClientID INT,
    @ClientPersonnelID INT,
    @NotificationStatusID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [NotificationRecipient]
	SET [NotificationID] = @NotificationID,
        [ClientID] = @ClientID,
		[SubClientID] = @SubClientID,
		[ClientPersonnelID] = @ClientPersonnelID,
        [NotificationStatusID] = @NotificationStatusID
	WHERE [NotificationRecipientID]= @NotificationRecipientID
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRecipient__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationRecipient__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRecipient__Update] TO [sp_executeall]
GO
