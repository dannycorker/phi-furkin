SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-16
-- Description:	Get Documents for Processing by document blob utility
-- =============================================
CREATE PROCEDURE [dbo].[DocumentBlob__GetDocumentsToProcess]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT dt.DocumentTypeID
	FROM DocumentType dt WITH (NOLOCK)
	WHERE dt.ClientID = @ClientID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBlob__GetDocumentsToProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBlob__GetDocumentsToProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBlob__GetDocumentsToProcess] TO [sp_executeall]
GO
