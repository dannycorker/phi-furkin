SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-01-11
-- Description: Copy a DataloaderMap
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DataloaderMap_Copy] 
	@MapIDToCopy	INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @NewMapID		INT

	INSERT DataLoaderMap ([ClientID],[MapName],[MapDescription],[WhenCreated],[CreatedBy],[LastUpdated],[UpdatedBy],[EnabledForUse],[Deleted],[LeadTypeID],[UseDefaultLeadForCustomer],[UseDefaultCaseForLead],[UseDefaultMatterForCase])
		SELECT dm.Clientid,dm.MapName,dm.MapDescription,dm.WhenCreated,dm.CreatedBy,dm.LastUpdated,dm.UpdatedBy,dm.EnabledForUse,dm.Deleted,dm.LeadTypeID,dm.UseDefaultLeadForCustomer,dm.UseDefaultCaseForLead,dm.UseDefaultMatterForCase
		FROM  DataLoaderMap dm WITH (NOLOCK)
		WHERE dm.DataLoaderMapID = @MapIDToCopy

	SELECT @NewMapID = SCOPE_IDENTITY()

	/*Order Old sectionIDs with a tally, for pairing with New sectionIDs*/
	DECLARE @OldDataloaderMapSectionID dbo.tvpIDValue
	INSERT INTO @OldDataloaderMapSectionID	
		SELECT dms.DataLoaderMapSectionID,  ROW_NUMBER() OVER ( partition by 1 order by dms.DataLoaderMapSectionID asc )
		FROM DataLoaderMapSection dms WITH (NOLOCK) 
		WHERE dms.DataLoaderMapID = @MapIDToCopy
		GROUP BY dms.DataLoaderMapSectionID
		Order by dms.DataLoaderMapSectionID asc 

	/*Store newly created SectionIDs in a Temporary table*/
	DECLARE @NewSectionsDirty table (SectionID INT)
	INSERT INTO DataLoaderMapSection ([ClientID],[DataLoaderMapID],[DataLoaderObjectTypeID],[DataLoaderObjectActionID],[DataLoaderSectionLocaterTypeID],[DetailFieldSubTypeID],[HasAquariumID],[NumberOfFooterRowsToSkip],[ImportIntoLeadManager],[IsFixedLengthSection],[FixedLengthNumberOfRows],[IsMandatory],[IsMultipleAllowedWithinFile],[Notes],[TableDetailFieldID],[ResourceListDetailFieldID])
			OUTPUT inserted.DataLoaderMapSectionID
			INTO @NewSectionsDirty
		SELECT dms.Clientid,@NewMapID,dms.DataLoaderObjectTypeID,dms.DataLoaderObjectActionID,dms.dataloadersectionlocatertypeid,dms.detailfieldsubtypeid,dms.HasAquariumID,dms.NumberOfFooterRowsToSkip,dms.ImportIntoLeadManager,dms.IsFixedLengthSection,dms.FixedLengthNumberOfRows,dms.IsMandatory,dms.IsMultipleAllowedWithinFile,dms.Notes,dms.TableDetailFieldID,dms.ResourceListDetailFieldID
		FROM  DataLoaderMapSection dms WITH (NOLOCK)
		WHERE dms.DataLoaderMapID = @MapIDToCopy
		order by dms.DataLoaderMapSectionID asc

	/*Order New SectionsIDs with a tally, for pairing with the old sectionIDs*/
	DECLARE @NewDataLoaderMapSectionID dbo.tvpIDValue
	INSERT INTO @NewDataLoaderMapSectionID
		SELECT dms.SectionID, ROW_NUMBER() OVER ( partition by 1 order by dms.sectionID asc )
		FROM  @NewSectionsDirty dms 
		order by dms.SectionID asc
		
	INSERT INTO DataLoaderFieldDefinition ([ClientID],[DataLoaderMapID],[DataLoaderObjectTypeID],[DataLoaderMapSectionID],[DataLoaderObjectFieldID],[DetailFieldID],[DetailFieldAlias],[NamedValue],[Keyword],[DataLoaderKeywordMatchTypeID],[RowRelativeToKeyword],[ColRelativeToKeyword],[SectionRelativeRow],[SectionAbsoluteCol],[ValidationRegex],[Equation],[IsMatchField],[DecodeTypeID],[DefaultLookupItemID],[SourceDataLoaderFieldDefinitionID],[Notes],[AllowErrors])
		SELECT dfd.ClientID,@NewMapID,dfd.DataLoaderObjectTypeID
		,NEW.AnyID /*New DataloaderMapSectionID's*/
		,dfd.DataLoaderObjectFieldID,dfd.DetailFieldID,dfd.DetailFieldAlias,dfd.NamedValue,dfd.Keyword,dfd.DataLoaderKeywordMatchTypeID,dfd.RowRelativeToKeyword,dfd.ColRelativeToKeyword,dfd.SectionRelativeRow,dfd.SectionAbsoluteCol,dfd.ValidationRegex,dfd.Equation,dfd.IsMatchField,dfd.DecodeTypeID,dfd.DefaultLookupItemID,dfd.SourceDataLoaderFieldDefinitionID,dfd.Notes,dfd.AllowErrors
		FROM DataLoaderFieldDefinition dfd WITH (NOLOCK) 
		INNER JOIN @OldDataloaderMapSectionID Old on Old.AnyID=dfd.DataLoaderMapSectionID
		INNER JOIN @NewDataLoaderMapSectionID New on New.AnyValue = Old.AnyValue
		WHERE dfd.DataLoaderMapID = @MapIDToCopy
		order by dfd.DataLoaderMapSectionID asc
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DataloaderMap_Copy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DataloaderMap_Copy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DataloaderMap_Copy] TO [sp_executeall]
GO
