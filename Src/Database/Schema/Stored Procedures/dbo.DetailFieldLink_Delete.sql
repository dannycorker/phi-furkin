SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailFieldLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_Delete]
(

	@DetailFieldLinkID int   
)
AS


				DELETE FROM [dbo].[DetailFieldLink] WITH (ROWLOCK) 
				WHERE
					[DetailFieldLinkID] = @DetailFieldLinkID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Delete] TO [sp_executeall]
GO
