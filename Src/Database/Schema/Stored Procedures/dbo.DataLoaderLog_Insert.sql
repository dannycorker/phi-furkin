SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderLog table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderLog_Insert]
(

	@DataLoaderLogID int    OUTPUT,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderMapSectionID int   ,

	@DataLoaderFileID int   ,

	@DataLoaderFieldDefinitionID int   ,

	@DataLoaderObjectFieldID int   ,

	@DetailFieldID int   ,

	@ObjectName varchar (250)  ,

	@RowIndex int   ,

	@ColIndex int   ,

	@NodeName varchar (2000)  ,

	@Message varchar (2000)  ,

	@LogLevel int   
)
AS


				
				INSERT INTO [dbo].[DataLoaderLog]
					(
					[ClientID]
					,[DataLoaderMapID]
					,[DataLoaderMapSectionID]
					,[DataLoaderFileID]
					,[DataLoaderFieldDefinitionID]
					,[DataLoaderObjectFieldID]
					,[DetailFieldID]
					,[ObjectName]
					,[RowIndex]
					,[ColIndex]
					,[NodeName]
					,[Message]
					,[LogLevel]
					)
				VALUES
					(
					@ClientID
					,@DataLoaderMapID
					,@DataLoaderMapSectionID
					,@DataLoaderFileID
					,@DataLoaderFieldDefinitionID
					,@DataLoaderObjectFieldID
					,@DetailFieldID
					,@ObjectName
					,@RowIndex
					,@ColIndex
					,@NodeName
					,@Message
					,@LogLevel
					)
				-- Get the identity value
				SET @DataLoaderLogID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderLog_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Insert] TO [sp_executeall]
GO
