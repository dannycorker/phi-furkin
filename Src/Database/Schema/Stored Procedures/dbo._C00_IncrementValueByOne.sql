SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-03-31
-- Description:	Take value from field and add 1 then put it back
-- =============================================
CREATE PROCEDURE [dbo].[_C00_IncrementValueByOne]
@CaseID int,
@DetailFieldID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @DetailFieldSubType int,
			@LeadID int,
			@CustomerID int
	
	Select @DetailFieldSubType = df.LeadOrMatter
	From DetailFields df WITH (NOLOCK)
	Where df.DetailFieldID = @DetailFieldID
	
	SELECT @LeadID = m.LeadID, @CustomerID = m.CustomerID
	From Matter m WITH (NOLOCK)
	Where m.CaseID = @CaseID
	
	exec dbo._C00_CreateDetailFields @DetailFieldID, @LeadID
	
	IF @DetailFieldSubType = 1
	BEGIN
	
		UPDATE LeadDetailValues
		Set DetailValue = ISNULL(ValueInt, 0) + 1
		From LeadDetailValues
		Where LeadID = @LeadID
		AND DetailFieldID = @DetailFieldID
	
	END

	IF @DetailFieldSubType = 2
	BEGIN
	
		UPDATE MatterDetailValues
		Set DetailValue = ISNULL(ValueInt, 0) + 1
		From MatterDetailValues
		INNER JOIN Matter m ON m.MatterID = MatterDetailValues.MatterID and m.CaseID = @CaseID
		Where DetailFieldID = @DetailFieldID
	
	END

	IF @DetailFieldSubType = 10
	BEGIN
	
		UPDATE CustomerDetailValues
		Set DetailValue = CASE ValueInt WHEN 0 THEN 1 ELSE ISNULL(ValueInt, 1) END + 1
		From CustomerDetailValues
		Where CustomerDetailValues.DetailFieldID = @DetailFieldID
		AND CustomerDetailValues.CustomerID = @CustomerID
	
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IncrementValueByOne] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_IncrementValueByOne] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IncrementValueByOne] TO [sp_executeall]
GO
