SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-18
-- Description:	Load items for bulk evaludations
-- IS	2016-08-17	Added Evaluate Ruleset By RuleSet/ChangeSet/ByEffectiveDate
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_TestFile_Load]
(
	@ClientID INT, 
	@UserID INT, 
	@RuleSetID INT, 
	@ImportXml XML,
	@FileName VARCHAR(250)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	exec _C00_LogItXML @ClientID, @RuleSetID, 'RulesEngine_TestFile_Load', @ImportXml
	
	DECLARE @NewTestFileID INT = 0
	
	INSERT INTO dbo.RulesEngine_TestFile (ClientID, ChangeSetID, RuleSetID, FileName, WhoCreated)
	SELECT	@ClientID, rs.ChangeSetID, rs.RuleSetID, @FileName, @UserID
	FROM	RulesEngine_RuleSets rs WITH (NOLOCK) 
	WHERE	RuleSetID = @RuleSetID
	AND		ClientID = @ClientID
	
	SELECT @NewTestFileID = SCOPE_IDENTITY()

	INSERT INTO dbo.RulesEngine_TestFileItem (TestFileID, ClientID, Input, CustomerID, LeadID, CaseID, MatterID, EvaluateByEffectiveDate, EvaluateByChangeSetID)
	SELECT	TOP 100
			@NewTestFileID,
			@ClientID,
			NULLIF(Input, ''),
			NULLIF(CustomerID, ''),
			NULLIF(LeadID, ''),
			NULLIF(CaseID, ''),
			NULLIF(MatterID, ''),
			NULLIF(EvaluateByEffectiveDate,''),
			NULLIF(EvaluateByChangeSetID,'')
	FROM	[dbo].[fn_RulesEngine_TestFile_Parse](@ClientID, @UserID, @RuleSetID, @ImportXml) row
	WHERE	row.RowID >= 1 -- ignore the header, start at 1
	
	SELECT @NewTestFileID TestFileID, @@ROWCOUNT RowsImported

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Load] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_TestFile_Load] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Load] TO [sp_executeall]
GO
