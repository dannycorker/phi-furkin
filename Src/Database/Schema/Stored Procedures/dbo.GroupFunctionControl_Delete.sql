SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the GroupFunctionControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControl_Delete]
(

	@GroupFunctionControlID int   
)
AS


				DELETE FROM [dbo].[GroupFunctionControl] WITH (ROWLOCK) 
				WHERE
					[GroupFunctionControlID] = @GroupFunctionControlID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControl_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_Delete] TO [sp_executeall]
GO
