SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DroppedOutCustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerQuestionnaires_Delete]
(

	@CustomerQuestionnaireID int   
)
AS


				DELETE FROM [dbo].[DroppedOutCustomerQuestionnaires] WITH (ROWLOCK) 
				WHERE
					[CustomerQuestionnaireID] = @CustomerQuestionnaireID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerQuestionnaires_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Delete] TO [sp_executeall]
GO
