SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentMRTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentMRTarget_Insert]
(

	@DocumentMRTargetID int    OUTPUT,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@Notes varchar (250)  ,

	@DocumentTypeVersionID int   
)
AS


				
				INSERT INTO [dbo].[DocumentMRTarget]
					(
					[ClientID]
					,[DocumentTypeID]
					,[Target]
					,[DetailFieldID]
					,[ColumnField]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[ColumnFieldAlias]
					,[ObjectName]
					,[PropertyName]
					,[Notes]
					,[DocumentTypeVersionID]
					)
				VALUES
					(
					@ClientID
					,@DocumentTypeID
					,@Target
					,@DetailFieldID
					,@ColumnField
					,@TemplateTypeID
					,@DetailFieldAlias
					,@ColumnFieldAlias
					,@ObjectName
					,@PropertyName
					,@Notes
					,@DocumentTypeVersionID
					)
				-- Get the identity value
				SET @DocumentMRTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_Insert] TO [sp_executeall]
GO
