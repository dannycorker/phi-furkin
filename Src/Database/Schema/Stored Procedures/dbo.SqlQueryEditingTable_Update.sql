SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditingTable table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingTable_Update]
(

	@SqlQueryEditingTableID int   ,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryTableName varchar (50)  ,

	@TableAlias varchar (50)  ,

	@TableDisplayOrder int   ,

	@JoinType varchar (25)  ,

	@JoinText varchar (500)  ,

	@JoinTableID int   ,

	@JoinRTRID int   ,

	@RealSqlQueryTableID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditingTable]
				SET
					[ClientID] = @ClientID
					,[SqlQueryEditingID] = @SqlQueryEditingID
					,[SqlQueryTableName] = @SqlQueryTableName
					,[TableAlias] = @TableAlias
					,[TableDisplayOrder] = @TableDisplayOrder
					,[JoinType] = @JoinType
					,[JoinText] = @JoinText
					,[JoinTableID] = @JoinTableID
					,[JoinRTRID] = @JoinRTRID
					,[RealSqlQueryTableID] = @RealSqlQueryTableID
				WHERE
[SqlQueryEditingTableID] = @SqlQueryEditingTableID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Update] TO [sp_executeall]
GO
