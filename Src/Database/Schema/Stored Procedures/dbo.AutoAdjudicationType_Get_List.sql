SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AutoAdjudicationType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_Get_List]

AS


				
				SELECT
					[AutoAdjudicationTypeID],
					[AutoAdjudicationTypeName]
				FROM
					[dbo].[AutoAdjudicationType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Get_List] TO [sp_executeall]
GO
