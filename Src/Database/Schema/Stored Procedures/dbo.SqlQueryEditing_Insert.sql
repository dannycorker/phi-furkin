SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_Insert]
(

	@SqlQueryEditingID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@WhoIsEditing int   ,

	@EditStartedAt datetime   ,

	@QueryText varchar (MAX)  ,

	@QueryTitle varchar (250)  ,

	@LeadTypeID int   ,

	@FolderID int   ,

	@ParentQueryID int   ,

	@LockAllTables bit   
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditing]
					(
					[ClientID]
					,[SqlQueryID]
					,[WhoIsEditing]
					,[EditStartedAt]
					,[QueryText]
					,[QueryTitle]
					,[LeadTypeID]
					,[FolderID]
					,[ParentQueryID]
					,[LockAllTables]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@WhoIsEditing
					,@EditStartedAt
					,@QueryText
					,@QueryTitle
					,@LeadTypeID
					,@FolderID
					,@ParentQueryID
					,@LockAllTables
					)
				-- Get the identity value
				SET @SqlQueryEditingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Insert] TO [sp_executeall]
GO
