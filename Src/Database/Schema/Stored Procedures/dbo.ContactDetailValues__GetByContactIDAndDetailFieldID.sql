SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[ContactDetailValues__GetByContactIDAndDetailFieldID]
(
	@ContactID int,
	@DetailFieldID int	
)
AS


				SET ANSI_NULLS OFF
				
				SELECT 
					[ContactDetailValueID],
					[ClientID],
					[ContactID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ContactDetailValues]
				WHERE
					[ContactID] = @ContactID AND
					[DetailFieldID] = @DetailFieldID 					
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON




GO
GRANT VIEW DEFINITION ON  [dbo].[ContactDetailValues__GetByContactIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ContactDetailValues__GetByContactIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ContactDetailValues__GetByContactIDAndDetailFieldID] TO [sp_executeall]
GO
