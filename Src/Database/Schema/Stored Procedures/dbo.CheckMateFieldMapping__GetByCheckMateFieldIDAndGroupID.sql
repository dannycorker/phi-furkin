SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CheckMateFieldMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


Create PROCEDURE [dbo].[CheckMateFieldMapping__GetByCheckMateFieldIDAndGroupID]
(

	@CheckMateFieldID int,
	@CheckMateFieldGroupID int,
	@ClientID int
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[CheckMateFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[CheckMateFieldID],
					[FieldID],
					[ColumnFieldID],
					[DataLoaderObjectTypeID],
					[CheckMateFieldGroupID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[CheckMateFieldMapping]
				WHERE
					[CheckMateFieldID] = @CheckMateFieldID AND
					[CheckMateFieldGroupID] = @CheckMateFieldGroupID AND
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping__GetByCheckMateFieldIDAndGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping__GetByCheckMateFieldIDAndGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping__GetByCheckMateFieldIDAndGroupID] TO [sp_executeall]
GO
