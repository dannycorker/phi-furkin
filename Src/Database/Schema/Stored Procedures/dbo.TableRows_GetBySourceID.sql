SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TableRows table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[TableRowID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailFieldPageID],
					[DenyEdit],
					[DenyDelete],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID],
					[SourceID]
				FROM
					[dbo].[TableRows] WITH (NOLOCK) 
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_GetBySourceID] TO [sp_executeall]
GO
