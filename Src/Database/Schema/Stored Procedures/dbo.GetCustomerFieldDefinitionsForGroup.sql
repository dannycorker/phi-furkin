SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetCustomerFieldDefinitionsForGroup]

@ClientQuestionnaireID int,
@FieldAtBeginningOfQuestionnaire bit

AS

SELECT CustomerInformationFieldDefinitionID, ClientQuestionnaireID, FieldAtBeginningOfQuestionnaire, OrdinalPosition, FieldName, FieldLabel, Mandatory, VerifyPhoneNumber, ClientID 
FROM CustomerInformationFieldDefinitions
WHERE (ClientQuestionnaireID = @ClientQuestionnaireID) AND (FieldAtBeginningOfQuestionnaire = @FieldAtBeginningOfQuestionnaire)
ORDER BY OrdinalPosition



GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerFieldDefinitionsForGroup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerFieldDefinitionsForGroup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerFieldDefinitionsForGroup] TO [sp_executeall]
GO
