SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson/Alex Elger
-- Create date: 2013-03-23
-- Description:	Add a row after receiving an SMS
-- Modified By PR on 23/01/2014 - Added the following SMS Survey Result columns:-
--									Language, Sentiment Polarity, Sentiment Confidence, Mixed Sentiment, Rules Engine Score
--				2015-07-06	SB	Change to include voice events
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PostSMSSurveyTableInsert]
	
	@LeadEventID INT
	
AS
BEGIN

DECLARE	@LeadID INT,
			@EventTypeID INT,
			@CustomerID INT,
			@ClientID INT,
			@CaseID INT,
			@WhoCreated INT,
			@MatterID INT,
			@LeadDocumentID INT,
			@TableRowID INT,
			@SMS_Text VARCHAR(2000),
			@EventSubTypeID INT,
			@DetailFieldPageID INT,
			@TableDetailFieldID INT,
			@LeadTypeID INT


	/*Get standard variables*/
	SELECT TOP 1 
		@LeadID = LE.LeadID, 
		@EventTypeID = LE.EventTypeID, 
		@CustomerID = m.CustomerID, 
		@ClientID = m.ClientID, 
		@CaseID = LE.CaseID, 
		@WhoCreated = LE.WhoCreated, 
		@MatterID = M.MatterID,
		@LeadDocumentID = LE.LeadDocumentID,
		@LeadTypeID = l.LeadTypeID
	FROM dbo.LeadEvent LE WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = LE.LeadID
	INNER JOIN dbo.Matter M WITH (NOLOCK) ON M.CaseID = le.CaseID	
	WHERE LE.LeadEventID = @LeadEventID 
	ORDER BY m.MatterID DESC

	SELECT @SMS_Text = REPLACE(CONVERT(VARCHAR(MAX),CONVERT(VARBINARY(MAX), l.EmailBLOB)), 'Â','')
	FROM vLeadDocumentList l WITH (NOLOCK) 
	WHERE LeadDocumentID=@LeadDocumentID
	
	SELECT @EventSubTypeID=EventSubtypeID 
	FROM EventType WITH (NOLOCK) 
	WHERE EventTypeID=@EventTypeID
	
	IF @EventSubtypeID IN (13, 26) -- SMS OUT Event and voice event
	BEGIN
		
		DECLARE @SMSQuestionID INT
		
		SELECT @SMSQuestionID = qet.SMSQuestionID
		FROM SMSQuestionEventType qet WITH (NOLOCK) 
		WHERE qet.EventTypeID = @EventTypeID
		
		SELECT TOP 1 @TableDetailFieldID = t.DetailFieldID
		FROM ThirdPartyFieldMapping t WITH (NOLOCK) 
		WHERE ThirdPartyFieldGroupID = 24
		AND t.ThirdPartyFieldID = 372
		AND t.LeadTypeID = @LeadTypeID
		
		SELECT @DetailFieldPageID = d.DetailFieldPageID
		FROM DetailFields d WITH (NOLOCK)
		WHERE d.DetailFieldID = @TableDetailFieldID
		
		INSERT INTO TableRows(ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID)
		SELECT @ClientID, @LeadID, @MatterID, @TableDetailFieldID, @DetailFieldPageID
			
		SELECT @TableRowID = SCOPE_IDENTITY()

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
		SELECT @TableRowID, t.ColumnFieldID, CASE t.ThirdPartyFieldID 
								WHEN 372 THEN CONVERT(VARCHAR(20), dbo.fn_GetDate_Local(),120) 
								WHEN 373 THEN CONVERT(VARCHAR(2000),@EventTypeID)
								WHEN 374 THEN CONVERT(VARCHAR(2000),@SMSQuestionID)
								WHEN 375 THEN CONVERT(VARCHAR(2000),@SMS_Text) 
								WHEN 376 THEN '' 
								WHEN 377 THEN '' 
								WHEN 378 THEN '' 
								WHEN 379 THEN '' 
								WHEN 384 THEN ''
								WHEN 385 THEN ''
								WHEN 820 THEN ''
								WHEN 821 THEN ''
								WHEN 822 THEN ''
								WHEN 823 THEN ''
								WHEN 824 THEN ''
								END, @LeadID, @MatterID, @ClientID
		FROM ThirdPartyFieldMapping t WITH (NOLOCK) 
		WHERE t.ThirdPartyFieldGroupID = 24 AND t.ThirdPartyFieldID IN (372,373,374,375,376,377,378,379,384,385,820,821,822,823,824)
		AND t.LeadTypeID = @LeadTypeID 
		AND ColumnFieldID IS NOT NULL
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PostSMSSurveyTableInsert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PostSMSSurveyTableInsert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PostSMSSurveyTableInsert] TO [sp_executeall]
GO
