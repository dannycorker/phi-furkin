SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureStatus_Update]
(

	@EsignatureStatusID int   ,

	@EsignatureStatusDescription varchar (50)  ,

	@EsignatureStatus varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EsignatureStatus]
				SET
					[EsignatureStatusDescription] = @EsignatureStatusDescription
					,[EsignatureStatus] = @EsignatureStatus
				WHERE
[EsignatureStatusID] = @EsignatureStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Update] TO [sp_executeall]
GO
