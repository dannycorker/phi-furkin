SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUserOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_Update]
(

	@PortalUserOptionID int   ,

	@ClientID int   ,

	@PortalUserID int   ,

	@PortalUserOptionTypeID int   ,

	@OptionValue varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalUserOption]
				SET
					[ClientID] = @ClientID
					,[PortalUserID] = @PortalUserID
					,[PortalUserOptionTypeID] = @PortalUserOptionTypeID
					,[OptionValue] = @OptionValue
				WHERE
[PortalUserOptionID] = @PortalUserOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Update] TO [sp_executeall]
GO
