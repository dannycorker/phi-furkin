SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Patch table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Patch_Insert]
(

	@PatchID int    OUTPUT,

	@PatchDate datetime   ,

	@PatchVersion varchar (50)  
)
AS


				
				INSERT INTO [dbo].[Patch]
					(
					[PatchDate]
					,[PatchVersion]
					)
				VALUES
					(
					@PatchDate
					,@PatchVersion
					)
				-- Get the identity value
				SET @PatchID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Patch_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Insert] TO [sp_executeall]
GO
