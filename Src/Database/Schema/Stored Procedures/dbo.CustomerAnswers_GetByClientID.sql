SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerAnswers table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerAnswerID],
					[CustomerQuestionnaireID],
					[MasterQuestionID],
					[Answer],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[CustomerAnswers] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByClientID] TO [sp_executeall]
GO
