SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the MasterQuestions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_Insert]
(

	@MasterQuestionID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@QuestionTypeID int   ,

	@QuestionText varchar (MAX)  ,

	@QuestionOrder int   ,

	@DefaultAnswerID int   ,

	@Mandatory bit   ,

	@QuestionToolTip varchar (255)  ,

	@LinkedQuestionnaireMasterQuestionID int   ,

	@Active bit   ,

	@MasterQuestionStatus int   ,

	@TextboxHeight int   ,

	@ClientID int   ,

	@AnswerPosition int   ,

	@DisplayAnswerAs int   ,

	@NumberOfAnswersPerRow int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@QuestionPossibleAnswerSortOrder int   
)
AS


				
				INSERT INTO [dbo].[MasterQuestions]
					(
					[ClientQuestionnaireID]
					,[QuestionTypeID]
					,[QuestionText]
					,[QuestionOrder]
					,[DefaultAnswerID]
					,[Mandatory]
					,[QuestionToolTip]
					,[LinkedQuestionnaireMasterQuestionID]
					,[Active]
					,[MasterQuestionStatus]
					,[TextboxHeight]
					,[ClientID]
					,[AnswerPosition]
					,[DisplayAnswerAs]
					,[NumberOfAnswersPerRow]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[QuestionPossibleAnswerSortOrder]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@QuestionTypeID
					,@QuestionText
					,@QuestionOrder
					,@DefaultAnswerID
					,@Mandatory
					,@QuestionToolTip
					,@LinkedQuestionnaireMasterQuestionID
					,@Active
					,@MasterQuestionStatus
					,@TextboxHeight
					,@ClientID
					,@AnswerPosition
					,@DisplayAnswerAs
					,@NumberOfAnswersPerRow
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@QuestionPossibleAnswerSortOrder
					)
				-- Get the identity value
				SET @MasterQuestionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Insert] TO [sp_executeall]
GO
