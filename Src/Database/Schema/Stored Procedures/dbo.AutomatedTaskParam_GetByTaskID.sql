SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskParam table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_GetByTaskID]
(

	@TaskID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AutomatedTaskParamID],
					[TaskID],
					[ClientID],
					[ParamName],
					[ParamValue]
				FROM
					[dbo].[AutomatedTaskParam] WITH (NOLOCK) 
				WHERE
					[TaskID] = @TaskID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByTaskID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_GetByTaskID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByTaskID] TO [sp_executeall]
GO
