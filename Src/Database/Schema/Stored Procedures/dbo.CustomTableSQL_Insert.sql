SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-08
-- Description:	Insert into CustomTableSQL
-- =============================================
CREATE PROCEDURE [dbo].[CustomTableSQL_Insert]
	@ClientID int,
	@DetailFieldID int,
	@QueryText varchar,
	@HeaderColumns varchar(2000),
	@SQLQueryID int,
	@RTFBorderOn bit,
	@RTFTwips int,
	@HTMLTableProperties varchar(200),
	@AutoSizeColumns bit

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [CustomTableSQL]
			   ([ClientID]
			   ,[DetailFieldID]
			   ,[QueryText]
			   ,[HeaderColumns]
			   ,[SQLQueryID]
			   ,[RTFBorderOn]
			   ,[RTFTwips]
			   ,[HTMLTableProperties]
			   ,[AutoSizeColumns])
		 VALUES(@ClientID
			   ,@DetailFieldID
			   ,@QueryText
			   ,@HeaderColumns
			   ,@SQLQueryID
			   ,@RTFBorderOn
			   ,@RTFTwips
			   ,@HTMLTableProperties
			   ,@AutoSizeColumns)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQL_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL_Insert] TO [sp_executeall]
GO
