SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-16
-- Description:	Get Fields For Simple Uploader
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetFieldsForSimpleMapCreator]
	@ClientID INT,
	@LeadTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Fields TABLE (FieldOrder INT, FieldID VARCHAR(200), FriendlyName VARCHAR(2000))

	/*Create DataLoaderFileID, DataLoaderMapID and DateLoaded at the lowest level...*/
	IF NOT EXISTS (
		SELECT *
		FROM DetailFieldPages dfp WITH (NOLOCK)
		WHERE dfp.LeadTypeID = @LeadTypeID
		AND dfp.ClientID = @ClientID
		AND dfp.PageCaption = 'Internal'
	)
	BEGIN
	
		INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled, ResourceList, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, IsShared)
		VALUES (@ClientID, 2, @LeadTypeID, 'Internal', 'Internal', 99, 0, NULL, NULL, NULL, dbo.fn_GetDate_Local(), NULL, dbo.fn_GetDate_Local(), 0)
	
	END
	
	IF NOT EXISTS (
		SELECT *
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.LeadTypeID = @LeadTypeID
		AND df.ClientID = @ClientID
		AND df.FieldCaption = 'DataLoaderFileID'
	)
	BEGIN

		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView, ObjectTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, DetailFieldStyleID, Hyperlink, IsShared)
		SELECT ClientID, LeadOrMatter, 'DataLoaderFileID', 'DataLoaderFileID', 1, 0, 0, NULL, LeadTypeID, 1, DetailFieldPageID, 1, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, NULL, '', 0
		FROM DetailFieldPages dfp WITH (NOLOCK)
		WHERE dfp.LeadTypeID = @LeadTypeID
		AND dfp.ClientID = @ClientID
		AND dfp.PageCaption = 'Internal'
	
	END
	
	IF NOT EXISTS (
		SELECT *
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.LeadTypeID = @LeadTypeID
		AND df.ClientID = @ClientID
		AND df.FieldCaption = 'DataLoaderMapID'
	)
	BEGIN

		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView, ObjectTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, DetailFieldStyleID, Hyperlink, IsShared)
		SELECT ClientID, LeadOrMatter, 'DataLoaderMapID', 'DataLoaderMapID', 1, 0, 0, NULL, LeadTypeID, 1, DetailFieldPageID, 1, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, NULL, '', 0
		FROM DetailFieldPages dfp WITH (NOLOCK)
		WHERE dfp.LeadTypeID = @LeadTypeID
		AND dfp.ClientID = @ClientID
		AND dfp.PageCaption = 'Internal'
	
	END
	
	IF NOT EXISTS (
		SELECT *
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.LeadTypeID = @LeadTypeID
		AND df.ClientID = @ClientID
		AND df.FieldCaption = 'DateLoaded'
	)
	BEGIN

		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView, ObjectTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, DetailFieldStyleID, Hyperlink, IsShared)
		SELECT ClientID, LeadOrMatter, 'DateLoaded', 'DateLoaded', 1, 0, 0, NULL, LeadTypeID, 1, DetailFieldPageID, 1, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, NULL, '', 0
		FROM DetailFieldPages dfp WITH (NOLOCK)
		WHERE dfp.LeadTypeID = @LeadTypeID
		AND dfp.ClientID = @ClientID
		AND dfp.PageCaption = 'Internal'
	
	END
	
	INSERT INTO @Fields (FieldOrder, FieldID, FriendlyName)
	SELECT ROW_NUMBER() OVER (ORDER BY dt.ProcessingOrder, d.FieldName), CONVERT(VARCHAR,d.DataLoaderObjectTypeID) + '.'  + CONVERT(VARCHAR,d.DataLoaderObjectFieldID), dt.ObjectTypeName + '.' + d.FieldName
	FROM DataLoaderObjectField d WITH (NOLOCK)
	INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = d.DataLoaderObjectTypeID
	WHERE d.DataLoaderObjectTypeID IN (1,2,3,4, 10,14)

	INSERT INTO @Fields (FieldOrder, FieldID, FriendlyName)
	SELECT 999, '-1.' + CONVERT(VARCHAR,df.DetailFieldID), dfst.DetailFieldSubTypeName + '.' + df.FieldCaption
	FROM DetailFields df WITH (NOLOCK)
	LEFT JOIN DetailFieldSubType dfst WITH (NOLOCK) ON dfst.DetailFieldSubTypeID = df.LeadOrMatter
	WHERE df.ClientID = @ClientID
	AND (df.LeadTypeID = @LeadTypeID OR df.LeadTypeID < 12)
	AND LeadOrMatter IN (10,1,2,14) /*Customer, Lead, Matter, Contact*/
	
	SELECT *
	FROM @Fields f
	ORDER BY f.FieldOrder, f.FriendlyName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFieldsForSimpleMapCreator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetFieldsForSimpleMapCreator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFieldsForSimpleMapCreator] TO [sp_executeall]
GO
