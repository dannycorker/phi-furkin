SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-01
-- Description:	Called from SAE to calc monthly premium
-- Mods
-- 2017-01-05 DCM Refined selection of policy history table row id
-- 2017-07-11 CPS stamp LeadEvent comments
-- 2017-07-12 JEL Added consideration for TPL section amounts into premium detail history table 
-- 2017-07-25 CPS populate TableRows.SourceID with @LeadEventID
-- 2017-08-14 CPS populate NewBusiness field in @Overrides
-- 2017-08-21 CPS move Admin Fee outside of the "Dog Only" block
-- 2017-10-06 JEL Removed adjustment Application date setting for new buisness as now done on Apply Available discounts
-- 2017-10-17 Update CoverPeriodSequenceNumber
-- 2018-02-27 GPR added MultiPet Discount flag/relvalue at New and Renewal (yes/no) for C600
-- 2017-08-14 GPR populate NewBusiness field as 0 in @Overrides	
-- 2018-04-10 CW Add OMF status to filter for cancelation variables selection	
-- 2018-04-23 GPR added code to section NonWPAffecting and WPAffecting Discounts	
-- 2018-06-04 GPR added code from 433 to raise an error if any mandatory checkpoints are not matched		
-- 2018-08-30 GPR added call to C600_UpdatePDHwithCommissionValues					
-- 2019-05-31 ALM added insert into for Adjustment Application Date, ticket #57093		
-- 2019-09-03 JML #59303 Amended variable populating 175337
-- 2019-10-11 GPR removed redundent code blocks (previously commented out and not used in C60X build) for LPC-35
-- 2019-10-15 CPS for JIRA LPC-35 | Removed _C600_PA_Policy_SaveCoverPeriodRiskData as this is now held in QuotePetProduct
-- 2019-10-15 CPS for JIRA LPC-37 | Removed Cover Period Sequence Number
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- 2020-08-20 ALM | Updated for US Cancellation Logic for PPET-110
-- =============================================
CREATE PROC [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod]
(
	@MatterID INT,
	@LeadEventID INT,
	@OverrideCalculationDate DATE,
	@CalculateTypeID INT = 1, -- 1=monthly premium, 
						 --2=renewal, 
						 --3=MTA, 
						 --4=Quote (during customer matching), 
						 --5=cancellation, 
						 --6=annual premium, 
						 --7=reinstatement, 
						 --8=new policy, 
						 --9=pre MTA
	@DisplayOnly INT = 0,   -- for re-rating just add the policy premiums history row for user display
	@OutputData XML = NULL OUTPUT 
)
AS
BEGIN

	PRINT Object_Name(@@ProcID) + ' @MatterID = ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') 
							+ ',@LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
							+ ',@OverrideCalculationDate = ''' + ISNULL(CONVERT(VARCHAR,@OverrideCalculationDate,121),'NULL') + ''''
							+ ',@CalculateTypeID = ' + ISNULL(CONVERT(VARCHAR,@CalculateTypeID),'NULL')
							+ ',@DisplayOnly = ' + ISNULL(CONVERT(VARCHAR,@DisplayOnly),'NULL')

	RAISERROR( '<br><br><font color="red">Error:We should be using the _Evaluated version of this procedure</font>', 16, 1 )
	RETURN

	--declare
	--	@MatterID INT = 4178,
	--	@LeadEventID INT = 43908, 
	--	@OverrideCalculationDate DATE = '2016-11-01',
	--	@CalculateTypeID INT=5, -- 1=monthly premium, 2=renewal, 3=MTA, 4=Quote, 5=cnacellation 
	--	@DisplayOnly INT=0   -- for re-rating just add the policy premiums history row for user display

	
	SET NOCOUNT ON;
	
	DECLARE  @AdjustmentType				INT
			,@AqAutomationID				INT = 58552
			,@ClientID						INT= dbo.fnGetPrimaryClientID()
			,@CustomerID					INT
			,@FirstRenewalPremiumPreRenewal	INT = 0
			,@FromDate						DATE
			,@LeadID						INT
			,@CaseID						INT
			,@PDHTableRowID					INT
			,@OldwpID					INT
			,@Status						INT
			,@StartDate						DATE = NULL
			,@TermsDate						DATE = NULL
			,@ToDate						VARCHAR(10)
			,@InceptionDate					DATE
			,@CancelDate					DATE
			,@EventComments					VARCHAR(2000) = ''
			,@AdminFee						DECIMAL (18,2) 
			,@WhoCreated					INT
			,@PolStart						DATE
			,@PolEnd						DATE
			,@IPTRate						DECIMAL (18,2) 
			,@MultipetRel					DECIMAL (18,2) /*GPR 2020-02-24 changed to DECIMAL from FLOAT*/
			,@Multipet						INT
			,@IsMultipet					INT
			,@PolicyYear					INT
			,@OverrideChangeSetID			INT
			,@PremiumCalculationOutputXml	XML
			,@QuoteSessionID				INT
			,@QuotePetProductID				INT
			,@RulesetToUse					INT
			,@Df175396						VARCHAR(200)
			,@DF175397						VARCHAR(200)
			,@DF175400						VARCHAR(10)
			,@DF175347						VARCHAR(10)
			,@AffinityID					INT
			,@DaysInYear					INT
			,@PurchasedProductID			INT
			,@AnnualCommissionValue			MONEY
			,@TranCommissionValue			MONEY
			,@CommissionPercentageValue		INT /*GPR 2020-01-12*/
			,@GrossDiscountedPremium		MONEY /*GPR 2020-01-12*/
			,@DaysRemaining					INT
			,@RawPremium					NUMERIC(18,2)
			,@AdjustmentTypeID				INT
			,@WrittenPremiumID	INT,
			@AnnualPremium DECIMAL(18, 2),
			@Premium DECIMAL(18, 2),
			@FirstMonthly DECIMAL(18, 2),
			@RecurringMonthly DECIMAL(18, 2),
			@Discount DECIMAL(18, 2) = 0,
			@SubTotal DECIMAL(18, 2),
			@Net DECIMAL(18, 2),
			@Commission DECIMAL(18, 2) = 0,
			@Taxes DECIMAL(18, 2) = 0,
			@GrandTotal DECIMAL(18, 2),
			@GrossNet DECIMAL(18, 2),
			@DiscountGrossNet DECIMAL(18, 2),
			@PAFIfBeforeIPT DECIMAL(18, 2),
			@PAFBeforeIPTGrossNet DECIMAL(18, 2),
			@IPT DECIMAL(18, 2),
			@IPTGrossNet DECIMAL(18, 2),
			@PAFIfAfterIPT MONEY,
			@GrossGross DECIMAL(18, 2),
			@EventName VARCHAR(250)='Unknown',
			@EventTypeID INT,
			@IsAnnual INT,
			@PremiumCalculationID INT,
			@RuleSetID INT,
			@AdjustmentAmount	DECIMAL(18,2),
			@CancellationReason INT, 
			@State VARCHAR(10), 
			@CancellationType INT, 
			@DaysIntoPolicyYear INT, 
			@IsRenewal BIT, 
			@CoolingOffPeriod INT, 
			@CountryID INT,
			@County VARCHAR(100)

	DECLARE @TableValuesToInsert TABLE (DetailFieldID INT, DetailValue VARCHAR(1000))

	SELECT @WhoCreated = le.WhoCreated,
			@EventName=CASE WHEN et.EventTypeID = 155196 THEN 'New Policy' ELSE et.EventTypeName END,
			@EventTypeID = et.EventTypeID,
			@AdjustmentType= CASE et.EventTypeID 
								WHEN 155196 THEN 74527 /*Create linked leads and complete set-up*/
								WHEN 155235 THEN 74528
								WHEN 155262 THEN 74529  -- cancelled from PA
								WHEN 155374 THEN 74529  -- cancelled from collections
								WHEN 156748 THEN 74529  -- cancelled from claims
								WHEN 155278 THEN 74532
								WHEN 155303 THEN 74531
								ELSE 0 END
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID
	WHERE le.LeadEventID = @LeadEventID 

	SELECT @EventComments += CHAR(13)+CHAR(10) + OBJECT_NAME(@@ProcID) + CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Calculate Type: ' + ISNULL(CONVERT(VARCHAR,@CalculateTypeID),'NULL') + CHAR(13)+CHAR(10)

	SELECT @CustomerID = m.CustomerID, 
			@LeadID = m.LeadID, 
			@CaseID = m.CaseID, 
			@CountryID = cu.CountryID,
			@County = cu.County
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID 
	WHERE m.MatterID = @MatterID	

	IF @CalculateTypeID IN (1,6) -- this is a regular monthly premium calc
	BEGIN
	
		DECLARE @LogEntry VARCHAR(2000)
		SELECT @LogEntry='Args: Called with abolished calculation type - ' + CONVERT(VARCHAR(30),@CalculateTypeID) + 
						'; for MatterID - ' + CONVERT(VARCHAR(30),@MatterID) 
		EXEC _C00_LogIt 'Info', '_C00_1273_Policy_CalculatePremiumForPeriod', 'Warning', @LogEntry, 2372 
				
	END
	ELSE  -- not regular payment 
	BEGIN
	
		SELECT @FromDate=@OverrideCalculationDate
		
		IF @CalculateTypeID = 2
		BEGIN

			SELECT @TermsDate=@FromDate, @StartDate=@FromDate

		END
	
	END
	
	-- set FirstRenewalPremiumPreRenewal flag if fromdate= historical policy enddate
	IF EXISTS 
	( 
		SELECT *
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.ValidTo = @FromDate 
	)
	BEGIN

		SELECT @FirstRenewalPremiumPreRenewal = 1
		SELECT @TermsDate = @FromDate
		SELECT @StartDate = @FromDate

	END

	-- set the new business override if renewal
	DECLARE @Overrides dbo.tvpIntVarcharVarchar

	IF @CalculateTypeID = 2 OR @FirstRenewalPremiumPreRenewal = 1
	BEGIN

		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) 
		VALUES	(2, 'NewBusiness', '0'),
				(2, 'YearStart', CONVERT(VARCHAR, @StartDate, 120)) /*GPR 2019-04-26 added for #1656*/ 

	END
	ELSE
	IF @CalculateTypeID=8 /*Q&B*/ /*CPS 2017-08-14 for #45019*/
	BEGIN

		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) 
		VALUES (2, 'NewBusiness', '1')

	END
	
	/* GPR 2018-12-11 for C600 Defect 1268
	IF theres more than 1 historical policy tablerow then it's not new business!*/
	SELECT @PolicyYear = COUNT(tr.TableRowID) 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.DetailFieldID = 170033 
	AND tr.MatterID = @MatterID
	
	IF @CalculateTypeID IN (3,9)  AND @PolicyYear > 1 /*More than 1 historical Policy row then you're not new business*/
	BEGIN

		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) 
		VALUES (2, 'NewBusiness', '0')		

	END

	IF @CalculateTypeID IN (3,9)  AND @PolicyYear = 1 /*1 historical Policy row then you are new business*/
	BEGIN

		INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
		VALUES (2, 'NewBusiness', '1')		

	END

	/*GPR 2020-10-18 MultiPet Override*/
	IF @CalculateTypeID IN (3 /*MTA*/,9 /*Pre MTA*/)
	BEGIN
	
		SELECT @Multipet = ISNULL(dbo.fnGetSimpleDvAsInt(179923, @MatterID),5145)
					
		IF @Multipet = 5145 -- No and do not add.
		BEGIN
				INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
				(4, 'fn_C00_1273_RulesEngine_PetCount', '1')
		END
		ELSE -- Yes   ...and do not remove
		BEGIN
				INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
				(4, 'fn_C00_1273_RulesEngine_PetCount', '2')
		END
			
	END

	IF NOT EXISTS ( SELECT * 
	                FROM @Overrides o 
	                WHERE o.AnyValue1 = '144269' )
	BEGIN

		/*CPS 2017-08-14 for #45019.  Need SpeciesID for custom rule sets*/
		DECLARE @SpeciesId		INT 
		SELECT @SpeciesId = dbo.fnGetRlDvAsInt(144272 /*Pet Type*/,144269 /*Species*/,@CaseID)
		
		IF @SpeciesId <> 0
		BEGIN

			INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
			VALUES (5, '144269', CAST(@SpeciesID as VARCHAR))
		
		END

	END
	
	-- always pass the MatterID in case it is needed
	INSERT INTO @Overrides (AnyID, AnyValue1, AnyValue2) 
	VALUES (2,'RenewalMatterID',CAST(@MatterID AS VARCHAR))
	
	/*GPR 2019-03-07 Override EvaluateByChangeSetID*/	
	SELECT @OverrideChangeSetID = dbo.fnGetSimpleDvAsInt(180235,@MatterID) 

	/*First check that this is a valid ChangeSetID*/
	IF EXISTS(SELECT * FROM RulesEngine_ChangeSets rc WHERE rc.ChangeSetID = @OverrideChangeSetID)
	BEGIN 

		INSERT INTO @Overrides (AnyID, AnyValue1, AnyValue2)
		VALUES (1, 'EvaluateByChangeSetID', CONVERT(VARCHAR,@OverrideChangeSetID))

	END
	
	IF ISNULL(@OverrideChangeSetID,0) = 0 OR @OverrideChangeSetID = '' /*GPR 2019-06-25 #57818*/
	BEGIN

		IF @CalculateTypeID = 2
		BEGIN

			SELECT @RulesetToUse = dbo.fnGetSimpleDvAsInt(180234,@MatterID)

		END
		ELSE
		BEGIN

			SELECT @RulesetToUse = dbo.fnGetSimpleDvAsInt(180232,@MatterID)

		END

		/*GPR 2019-03-05 for #55470 Insert into Overrides*/
		INSERT INTO @Overrides (AnyID, AnyValue1, AnyValue2)
		SELECT 1, 'EvaluateByChangeSetID', CONVERT(VARCHAR,rs.ChangeSetID)
		FROM RulesEngine_RuleSets rs WITH (NOLOCK) 
		WHERE rs.RuleSetID = @RulesetToUse 

	END
		
	-- Get the premium metrics
	DECLARE @PremiumData TABLE
	(
		TermsDate DATE,
		SchemeID INT,
		PolicyMatterID INT,
		RuleSetID INT,
		AnnualPremium MONEY,
		Discount MONEY,
		PremiumLessDiscount MONEY,
		FirstMonthly MONEY,
		RecurringMonthly MONEY,
		Net MONEY,
		Commission MONEY,
		GrossNet MONEY,
		DiscountGrossNet MONEY,
		PAFIfBeforeIPT MONEY,
		PAFBeforeIPTGrossNet MONEY,
		IPT MONEY,
		IPTGrossNet MONEY,
		PAFIfAfterIPT MONEY,
		GrossGross MONEY,
		PremiumCalculationID INT
	)

	IF @CalculateTypeID NOT IN (5,7)
	BEGIN

		SELECT @EventComments = '<br><br><font color="red">Error: All calculations should be using the offline rater. LeadEventID ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL') + ', MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + '</font>'
		RAISERROR( @EventComments, 16, 1 )
		RETURN

	END

	IF @CalculateTypeID IN (5,7)
	BEGIN 
		
		SELECT TOP 1 
			@FromDate				= 	wp.ValidFrom, 			
			@Status					=	wp.Active,			
			@AnnualPremium			=	wp.AnnualPriceForRiskGross,			
			/*
			@FirstMonthly			=	wp.?,
			@RecurringMonthly		=	wp.?,	
			*/
			@IPT					=	wp.AnnualPriceForRiskNationalTax,
			@PremiumCalculationID	=	wp.PremiumCalculationID
			/*@PDHTableRowID			=	wp.,	*/
			/*@DiscountGrossNet		=	wp.Disc?,*/
			/*@Discount				=	Discount.DetailValue*/
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.Active = 1
		ORDER BY wp.WrittenPremiumID DESC

	END
	ELSE
	BEGIN
	
		SELECT 	@AnnualPremium = PremiumLessDiscount,
				@FirstMonthly = FirstMonthly,
				@RecurringMonthly = RecurringMonthly,
				@Net=Net,
				@Commission=Commission,
				@GrossNet=GrossNet,
				@Discount=Discount,
				@DiscountGrossNet=DiscountGrossNet,
				@PAFIfBeforeIPT=PAFIfBeforeIPT,
				@PAFBeforeIPTGrossNet=PAFBeforeIPTGrossNet,
				@IPT=IPT,
				@IPTGrossNet=IPTGrossNet,
				@PAFIfAfterIPT=PAFIfAfterIPT,
				@GrossGross=GrossGross,
				@PremiumCalculationID=PremiumCalculationID,
				@RuleSetID = RuleSetID
		FROM @PremiumData

		/*GPR 2019-10-10 for LPC-36*/
		SELECT @PremiumCalculationOutputXml = pcd.ProductCostBreakdown
		FROM PremiumCalculationDetail pcd WITH ( NOLOCK )
		WHERE pcd.PremiumCalculationDetailID = @PremiumCalculationID

		SELECT @QuoteSessionID = ovr.AnyValue2
		FROM @Overrides ovr
		WHERE ovr.AnyValue1 = '@QuoteSessionID'
		AND dbo.fnIsInt(ovr.AnyValue2) = 1

		UPDATE qpp
		SET EvaluatedXml = @PremiumCalculationOutputXml
		FROM QuotePetProduct qpp WITH (NOLOCK) 
		WHERE qpp.QuotePetProductID = @QuotePetProductID

	END
	
	/*GPR 2018-06-04 move from 433 to 600*/	
	/*CPS 2017-10-31 for TON130*/
	/*Raise an error if any mandatory checkpoints are not matched*/
	DECLARE @ErrorMessage VARCHAR(2000)

	SELECT @ErrorMessage = ISNULL(@ErrorMessage,'Error during premium calculation.' + CHAR(13)+CHAR(10)) 
						 + CHAR(13)+CHAR(10) + fn.ErrorMessage
	FROM dbo.fn_C600_RulesEngineMandatoryCheckpointCheck(@PremiumCalculationID) fn
	
	IF @ErrorMessage <> '' AND @CaseID <> 0
	BEGIN

		EXEC _C00_AddANote @CaseID, 947 /*Premium Calculation Error Identified*/, @ErrorMessage, 0, @WhoCreated, 0, 1
		EXEC dbo._C00_SimpleValueIntoField 180044, 'true', @MatterID, @WhoCreated /*MTA Calculation Error Identified*/
		EXEC dbo._C00_SimpleValueIntoField 177911, 76438 /*Calculation Error*/, @MatterID, @WhoCreated /*Select reason for aborting MTA*/

	END
 
	PRINT '@PremiumCalculationID = ' + ISNULL(CONVERT(VARCHAR,@PremiumCalculationID),'NULL')

	SELECT @EventComments += '  Annual Premium: ' + ISNULL(CONVERT(VARCHAR,@AnnualPremium),'NULL') + CHAR(13)+CHAR(10)
							  +  CASE WHEN @AnnualPremium <> @DiscountGrossNet 
									THEN '  After Discount: ' + ISNULL(CONVERT(VARCHAR,@DiscountGrossNet),'NULL') + CHAR(13)+CHAR(10) 
									ELSE '' END

	SELECT TOP (1) @OldwpID = wp.WrittenPremiumID
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.MatterID = @MatterID
	AND wp.Active = 1
	ORDER BY wp.WrittenPremiumID DESC

	/* 
		Setting 'from' and 'to' dates:
			First row in the table: From = premium start date & To = blank
			A quote: From = premium start date & To = blank
			An adjustment:  To (on last row) = adjustment date - 1 day; From (on new row) = adjustment date
			A renewal:  To (on last row) = renewal date - 1 day; From (on new row) = renewal date
			Cancellation: To (on last row of table) = cancellation date; no new row
			Re-instatement: blank out To on last row of table (handled elsewhere)
			 
	*/
		
	IF @EventTypeID = 155196 /*Create linked leads and complete set-up*/
	BEGIN

		EXEC dbo._C00_SimpleValueIntoField 178144, @RuleSetID, @MatterID, @WhoCreated /*New Business RuleSetID*/

	END
		
	IF @OldwpID IS NULL /*first row*/ OR @CalculateTypeID IN (4,8) /*quote, new policy*/
	BEGIN
			
		SELECT @FromDate=dbo.fn_C00_1273_GetPremiumCalculationStartDate (@CaseID,dbo.fnGetDvAsDate(170036,@CaseID)),
				@Status=CASE WHEN @CalculateTypeID = 8 THEN 72326 ELSE 72329 END
		
	END
	ELSE 
	IF @CalculateTypeID IN (2) -- Renewal
	BEGIN
		
		/*GPR 2019-03-05 for #55470*/
		EXEC dbo._C00_SimpleValueIntoField 180234, @RuleSetID, @MatterID, @AqAutomationID -- RuleSetID To EvaluateBy Renewal Window
		
		SELECT @FromDate=dbo.fnGetDv (175307,@CaseID)
		SELECT @ToDate=CONVERT(VARCHAR(10),DATEADD(DAY,-1,@FromDate),120),
				@Status=72329
		
	END
	ELSE IF @CalculateTypeID IN (3,9) -- MTA or pre MTA
	BEGIN
		
		SELECT @FromDate=dbo.fnGetSimpleDvAsDate (175442,@MatterID) /* GPR 2018-04-19 changed GetDv to GetSimpleDv, and @CaseID to @MatterID */
		SELECT @ToDate=CONVERT(VARCHAR(10),DATEADD(DAY,-1,@FromDate),120),
				@Status= CASE WHEN @CalculateTypeID=3 THEN 72326 ELSE 74514 END
		
		-- if we are applying the MTA, mark the pre MTA row as not applied
		IF @CalculateTypeID=3
		BEGIN

			UPDATE MatterDetailValues
			SET DetailValue=72328 /*Not applied*/
			WHERE DetailFieldID=175722 /*Status*/
			AND MatterID=@MatterID 
			AND ValueInt=74514

		END
			
	END
	ELSE 
	IF @CalculateTypeID = 5 -- cancellation
	BEGIN
		
		SELECT @InceptionDate = dbo.fnGetSimpleDvAsDate(170035,@MatterID),
				@FromDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID),
				@CancellationReason = dbo.fnGetSimpleDvAsInt(170054,@MatterID),
				@StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)
		
		/*2020-08-20 ALM PPET-110*/
		IF @CountryID = 233 /*United States*/
		BEGIN

			SELECT @State = cu.County 
			FROM Customers cu WITH (NOLOCK) 
			WHERE cu.CustomerID = @CustomerID 

			SELECT @CancellationType = 
				CASE 
					WHEN @CancellationReason IN (69907,69911,72031,72032,72033,72035,72037,72038,74421,74519,74521,74523,74525,75135,75136,75141) THEN 1 
					ELSE 2 
				END

			SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())

			SELECT @IsRenewal = 
				CASE 
					WHEN COUNT (*) > 1 THEN 1 
					ELSE 0 
				END 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID

			SELECT @CoolingOffPeriod = [dbo].[fn_C00_USCancellationCoolingOffPeriod] (@State, @CancellationType, @IsRenewal, @DaysIntoPolicyYear)

			SELECT @ToDate= 
				CASE 
					WHEN @InceptionDate > ISNULL(@FromDate,dbo.fn_GetDate_Local()) THEN @InceptionDate 
					WHEN DATEDIFF(DAY,@InceptionDate,ISNULL(@FromDate,dbo.fn_GetDate_Local()) ) <= @CoolingOffPeriod THEN @InceptionDate
					ELSE @FromDate 
				END

		END
		ELSE
		BEGIN
			
			/*Original logic*/
			SELECT @ToDate= CASE 
				WHEN @InceptionDate > ISNULL(@FromDate,dbo.fn_GetDate_Local()) THEN @InceptionDate 
				WHEN DATEDIFF(DAY,@InceptionDate,ISNULL(@FromDate,dbo.fn_GetDate_Local()) ) <= b.CoolingOffPeriod THEN @InceptionDate
				ELSE @FromDate END 
			FROM BillingConfiguration b WITH (NOLOCK) 
			WHERE b.ClientID = @ClientID

		END
			
		SELECT	@Status = 72326 /*Live*/
		
	END
	ELSE IF @CalculateTypeID = 7 -- reinstatement
	BEGIN
		
		SELECT @FromDate=dbo.fnGetDv (170055,@CaseID), -- from date = cancellation date
				@Status = 72326 /*Live*/
		
	END
		
	-- Update To date in existing row
	IF @CalculateTypeID IN (3,5) AND @DisplayOnly=0
	BEGIN

		UPDATE wp
		SET ValidTo = @ToDate
		FROM WrittenPremium wp
		WHERE wp.WrittenPremiumID = @OldwpID


	END
			
	-- Update Status in existing row
	IF @CalculateTypeID IN (3,5,7) AND @DisplayOnly=0
	BEGIN

		UPDATE wp
		SET Active = 0
		FROM WrittenPremium wp
		WHERE wp.WrittenPremiumID = @OldwpID

	END
		
	EXEC dbo._C00_SimpleValueIntoField  176175, @PremiumCalculationID, @MatterID 
		
	-- get matching policy history row id
	SELECT @PDHTableRowID=dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow (@CaseID, @FromDate)
	IF @PDHTableRowID IS NULL 
	BEGIN

		-- @FromDate is outside of the range of any historical policy entry, so default to the latest.
		SELECT TOP 1 @PDHTableRowID=tr.TableRowID 
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.DetailFieldID = 170033 
		AND MatterID = @MatterID
		ORDER BY tr.TableRowID DESC
			
		-- or give up 
		SELECT @PDHTableRowID=ISNULL(@PDHTableRowID,0)
		
	END
		
	IF @CalculateTypeID IN (1,8)
	BEGIN

		SELECT @AdminFee = dbo.fn_C600_GetAdminFee(@MatterID) /*CPS 2017-08-21 move Admin Fee outside of the "Dog Only" block*/

	END
		
	IF @AnnualPremium = 0 or @AnnualPremium is NULL
	BEGIN

		--DECLARE @ErrorMessage VARCHAR(2000)
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  AnnualPremium is ' + ISNULL(CONVERT(VARCHAR,@AnnualPremium),'NULL') 
								+ ' for PremiumCalculationID ' + ISNULL(CONVERT(VARCHAR,@PremiumCalculationID),'NULL') + '</font>'
								+ ', MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL')
								+ ', CalculateType ' + ISNULL(CONVERT(VARCHAR,@CalculateTypeID),'NULL')
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN

	END
	ELSE
	BEGIN

		PRINT '@AnnualPremium = ' + ISNULL(CONVERT(VARCHAR,@AnnualPremium),'NULL')

	END
				
	/*Written premium for Dogs needs to add a split in insurance between the primary insurer and TPL insurer*/
	IF @CalculateTypeID IN (2,6,8,7,5) AND EXISTS (
		SELECT * 
		FROM LeadDetailValues ldv with (NOLOCK)
		INNER JOIN ResourceListDetailValues rldv with (NOLOCK) on rldv.ResourceListID = ldv.ValueInt AND rldv.DetailFieldID = 144269
		Where ldv.DetailFieldID = 144272
		AND rldv.ValueInt = 42989
		AND ldv.LeadID = @LeadID
	) 
	BEGIN 
					
		IF @CalculateTypeID IN (8,7) /*IF this is new buisness then get the tpl from the current client level*/ 
		BEGIN
			
			SELECT TOP 1 @IPTRate = pcdv.RuleTransformValue
			FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
			WHERE pcdv.RuleCheckpoint = 'IPT'
			AND  pcdv.PremiumCalculationDetailID = @PremiumCalculationID
				
		END
		ELSE 
		BEGIN 
				
			SELECT @PolStart = p.ValidFrom, @PolEnd = p.ValidTo 
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.PurchasedProductID = mdv.ValueInt 
			Where mdv.MatterID = @MatterID 
			AND mdv.DetailFieldID = 177074
				
		END
						   
	END

	IF @CalculateTypeID IN (2,8) -- renewal, new policy
	BEGIN

		SELECT @Df175396 = CONVERT(VARCHAR(10),@FromDate,120),
				@DF175397 = CAST(ISNULL(@DiscountGrossNet,@AnnualPremium) AS VARCHAR(30))
		
	END

	IF @CalculateTypeID IN (2) -- renewal only /*ALM 2019-05-31 for #57093*/
	BEGIN

		SELECT @DF175400 = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)

	END

	IF @CalculateTypeID=5 -- set the To date on the new row for cancellations
	BEGIN

		SELECT @DF175347 = @ToDate

	END
		
	IF @CalculateTypeID IN (3,4,7,8) AND @DisplayOnly=0 -- quote, MTA & reinstatement; update current & previous premium fields
	BEGIN

		IF @CalculateTypeID = 3
		BEGIN

			SELECT @AdjustmentAmount = @DiscountGrossNet - ( dbo.fnGetSimpleDvAsMoney(175337,@MatterID) /*Current/New Premium - Annual*/ - dbo.fnGetSimpleDvAsMoney(177484,@MatterID) /*Initial Admin Fee Charged*/ )
			SELECT @EventComments += 'Adjustment Amount = ' + ISNULL(CONVERT(VARCHAR,@AdjustmentAmount),'NULL') + ' (' + ISNULL(CONVERT(VARCHAR,@DiscountGrossNet),'NULL') + ' - ' + ISNULL(dbo.fnGetSimpleDv(175337,@MatterID),'NULL') + ')' + CHAR(13)+CHAR(10)
			
			SELECT @DF175397 = @AdjustmentAmount

		END
		
		EXEC dbo._C00_CreateDetailFields '175337,175338,175339,175340,175341,175342,175711,175712', @LeadID
			
		-- copy current to previous
		EXEC _C600_CopyFieldToField 175337,175340,@MatterID
		EXEC _C600_CopyFieldToField 175338,175341,@MatterID
		EXEC _C600_CopyFieldToField 175339,175342,@MatterID
		EXEC _C600_CopyFieldToField 175711,175712,@MatterID
			
		-- update current
		EXEC dbo._C00_SimpleValueIntoField 175337, /*@DiscountGrossNet*/@AnnualPremium	, @MatterID,@AqAutomationID /*Current/New Premium - Annual*/--JML amended from @DiscountGrossNet to @AnnualPremium  ZD#59303
		EXEC dbo._C00_SimpleValueIntoField 175338, @FirstMonthly		, @MatterID , @AqAutomationID /*Current/New Premium - First Month*/
		EXEC dbo._C00_SimpleValueIntoField 175339, @RecurringMonthly	, @MatterID , @AqAutomationID /*Current/New Premium - Other Month*/
		EXEC dbo._C00_SimpleValueIntoField 175711, @PremiumCalculationID, @MatterID , @AqAutomationID /*Current Premium/New - PremiumCalculationID*/
		EXEC dbo._C00_SimpleValueIntoField 177898, @DiscountGrossNet	, @MatterID , @AqAutomationID /*Current/New Premium - After Discount*/ 
			
	END
	ELSE 
	IF @CalculateTypeID IN (2) -- renewal;  update renewal fields (don't update any for cancellation (5) )
	BEGIN
		
		EXEC dbo._C00_CreateDetailFields '175443,175444,175445', @LeadID
		
		EXEC dbo._C00_SimpleValueIntoField 175443, @DiscountGrossNet, @MatterID ,@AqAutomationID
		EXEC dbo._C00_SimpleValueIntoField 175444, @FirstMonthly	, @MatterID ,@AqAutomationID
		EXEC dbo._C00_SimpleValueIntoField 175445, @RecurringMonthly, @MatterID ,@AqAutomationID
			
	END

	/* GPR 2018-02-27 MultiPet New Business(8) Renewal(2) */
	/* Write the multipet relativity and applied status (yes/no) to fields */
	IF @CalculateTypeID IN (8,2) /*IF this is new buisness look at the MultipetRel - if MultiPet discount is applied flag on Policy*/ 
	BEGIN

		SELECT TOP 1 @MultipetRel=pcdv.RuleTransformValue
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.RuleCheckpoint = 'MultiPet Relativity' -- MultiPet Relativity, Multipet Discount, Apply Available MultiPet Discount
		AND  pcdv.PremiumCalculationDetailID = @PremiumCalculationID

		IF @MultipetRel = 1
		BEGIN

			EXEC dbo._C00_SimpleValueIntoField 179923, 5145, @MatterID,@AqAutomationID	-- 	MultiPet Discount For Period = No	
			EXEC dbo._C00_SimpleValueIntoField 179924, @MultipetRel, @MatterID,@AqAutomationID	-- 	MultiPet Relativity

		END
				
		IF @MultipetRel < 1
		BEGIN	

			EXEC dbo._C00_SimpleValueIntoField 179923, 5144, @MatterID,@AqAutomationID	-- 	MultiPet Discount For Period = Yes
			EXEC dbo._C00_SimpleValueIntoField 179924, @MultipetRel, @MatterID,@AqAutomationID	-- 	MultiPet Relativity

		END
				
	END

	IF @CalculateTypeID IN (2,3,5,6,8) AND @DisplayOnly = 0
	BEGIN

		SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID)

		SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsInt(313814,@AffinityID)

		SELECT TOP (1)	 @DaysInYear = DATEDIFF(DAY,pp.ValidFrom,pp.ValidTo) + 1
						,@PurchasedProductID = pp.PurchasedProductID
		FROM PurchasedProduct pp WITH (NOLOCK) 
		WHERE pp.ObjectID = @MatterID
		AND @DF175347 between pp.ValidFrom AND pp.ValidTo
		ORDER BY pp.PurchasedProductID DESC	

		IF @@ROWCOUNT = 0 or @PolEnd is NULL
		BEGIN

			SELECT @DaysInYear = DATEDIFF(DAY,mdv.ValueDate,DATEADD(DAY,-1,DATEADD(YEAR,1,mdv.ValueDate)) /*Policy Start Date*/) + 1
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			WHERE mdv.MatterID = @MatterID
			AND mdv.DetailFieldID = 170036 /*Policy Start Date*/

		END

		SELECT @AnnualCommissionValue /*Broker Commission Value*/ = (@DiscountGrossNet / 100) * (100 - @CommissionPercentageValue /*UW Commission Value*/)

		SELECT @DaysRemaining = DATEDIFF(DAY,@Df175396, @DF175347)+1

		SELECT @TranCommissionValue	= (@AnnualCommissionValue)* ( CAST(@DaysRemaining as MONEY) / CAST(@DaysInYear as MONEY) )

		SELECT @RawPremium = ISNULL(@DiscountGrossNet,0.00) - ( ISNULL(@IPT,0.00)) /*+ ISNULL(tdvAdmin.ValueMoney,0.00) )*/ /*2020-12-03 ACE this vlaue isnt being set...*/

	END

	INSERT INTO @TableValuesToInsert (DetailFieldID, DetailValue)
	VALUES  (175446,CONVERT(VARCHAR(16),dbo.fn_GetDate_Local(),120)),  -- date time of rating
			(175346,CONVERT(VARCHAR(10),@FromDate,120)),
			(175347,''),
			(175348,@EventName),
			(175722,CAST(@Status				AS VARCHAR(30))),	/*Status*/
			(175349,CAST(@AnnualPremium			AS VARCHAR(30))),	/*Annual*/
			(175350,CAST(@FirstMonthly			AS VARCHAR(30))),	/*First Month*/
			(175351,CAST(@RecurringMonthly		AS VARCHAR(30))),	/*Other Month*/
			(175375,CAST(@IPT					AS VARCHAR(30))),	/*IPT*/
			(175709,CAST(@PremiumCalculationID	AS VARCHAR(30))),	/*PremiumCalculationID*/
			(175398,CAST(@AdjustmentType		AS VARCHAR(30))),	/*Adjustment Type*/
			(175376,CAST(@EventTypeID			AS VARCHAR(30))),	/*Event Type ID*/
			(175377,CAST(@PDHTableRowID			AS VARCHAR(30))),	/*Historic Pol TRID*/
			(177899,CAST(@DiscountGrossNet		AS VARCHAR(30))),	/*Premium less Discount*/
			(175373,CAST(@Discount				AS VARCHAR(30))),	/*Discount*/
			(177903,CAST(@AdminFee				AS VARCHAR(20))), /*Adjustment Admin Fee*/
			(175396,@Df175396),  -- adjustment date
			(175397,@DF175397),   -- adjustment amount
			(175400, @DF175400), -- adjustment application date
			(175347, @DF175347), -- Cover To
			(180170, CONVERT(VARCHAR(100), @AnnualCommissionValue)),
			(180171, CONVERT(VARCHAR(100), @TranCommissionValue)),
			(180307, CONVERT(VARCHAR(100), @RawPremium))

	IF @DisplayOnly = 0
	BEGIN

		/*
			1 = Monthly, What is monthly?!?
			2 = renewal, 
			3 = MTA, 
			4 = Quote (during customer matching), 
			5 = cancellation, 
			6 = annual premium, 
			7 = reinstatement, 
			8 = new policy, 
			9 = pre MTA
		*/

		SELECT @AdjustmentTypeID = CASE @CalculateTypeID 
				WHEN 1 THEN 1
				WHEN 2 THEN 3
				WHEN 3 THEN 2
				WHEN 4 THEN 1
				WHEN 5 THEN 4
				WHEN 6 THEN 1
				WHEN 7 THEN 6
				WHEN 8 THEN 1
				WHEN 9 THEN 2
				END

		INSERT INTO WrittenPremium (ClientID, CustomerID, LeadID, MatterID, PurchasedProductID, RatingDateTime, 
				PremiumCalculationID, ValidFrom, ValidTo, AdjustmentTypeID, 
				Active, AdjustmentRequestDate, 
				AnnualPriceForRiskGross, AnnualPriceForRiskNET, AnnualPriceForRiskNationalTax, 
				AnnualAdminFee, AnnualDiscountCommissionSacrifice, AnnualDiscountPremiumAffecting, 
				AdjustmentValueGross, AdjustmentValueNET, AdjustmentValueNationalTax, AdjustmentAdminFee, 
				AdjustmentDiscountCommissionSacrifice, AdjustmentDiscountCommissionPremium, AdjustmentBrokerCommission, 
				AdjustmentUnderwriterCommission, AdjustmentSourcecommission, CancellationReason, AdditionalDetailXML, 
				Comments, WhoCreated, WhenCreated, WhoChanged, WhenChanged, SourceID, ProvinceState)
		VALUES (@ClientID, 
				@CustomerID, 
				@LeadID, 
				@MatterID, 
				@PurchasedProductID, 
				dbo.fn_GetDate_Local(), 
				@PremiumCalculationID, 
				@FromDate /*[ValidFrom]*/, 
				@DF175347 /* [ValidTo]*/, 
				@AdjustmentTypeID /*[AdjustmentTypeID]*/, 
				1 /*[Active]*/, 
				dbo.fn_GetDate_Local() /*[AdjustmentRequestDate]*/, 
				0.00 /*[AnnualPriceForRiskGross]*/, 
				0.00 /*[AnnualPriceForRiskNET]*/, 
				0.00 /*[AnnualPriceForRiskNationalTax]*/, 
				0.00 /*[AnnualAdminFee]*/, 
				0.00 /*[AnnualDiscountCommissionSacrifice]*/, 
				0.00 /*[AnnualDiscountPremiumAffecting]*/, 
				0.00 /*[AdjustmentValueGross]*/, 
				0.00 /*[AdjustmentValueNET]*/, 
				0.00 /*[AdjustmentValueNationalTax]*/, 
				0.00 /*[AdjustmentAdminFee]*/, 
				0.00 /*[AdjustmentDiscountCommissionSacrifice]*/, 
				0.00 /*[AdjustmentDiscountCommissionPremium]*/, 
				0.00 /*[AdjustmentBrokerCommission]*/, 
				0.00 /*[AdjustmentUnderwriterCommission]*/, 
				0.00 /*[AdjustmentSourcecommission]*/, 
				'' /*[CancellationReason]*/, 
				NULL /*[AdditionalDetailXML]*/, 
				'' /*[Comments]*/, 
				@WhoCreated /*[WhoCreated]*/, 
				dbo.fn_GetDate_Local() /*[WhenCreated]*/, 
				@WhoCreated /*[WhoChanged]*/, 
				dbo.fn_GetDate_Local() /*[WhenChanged]*/, 
				NULL /*[SourceID]*/,
				@County /*ProvinceState*/
				)

		SELECT @WrittenPremiumID = SCOPE_IDENTITY()

	END

	SELECT @OutputData = (
		SELECT *
		FROM @TableValuesToInsert i
		FOR XML PATH ('Data')
	)

	SELECT @EventComments += '  WrittenPremiumID ' + ISNULL(CONVERT(VARCHAR,@WrittenPremiumID),'NULL') + CHAR(13)+CHAR(10)
	/*GPR 2019-10-10 for LPC-36*/  +  '  QuotePetProductID ' + ISNULL(CONVERT(VARCHAR,@QuotePetProductID),'NULL') + CHAR(13)+CHAR(10)

	EXEC _C00_SetLeadEventComments  @LeadEventID, @EventComments, 1
	
	RETURN @WrittenPremiumID
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod] TO [sp_executeall]
GO
