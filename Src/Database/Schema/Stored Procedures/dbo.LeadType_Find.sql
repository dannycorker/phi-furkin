SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadTypeID int   = null ,

	@ClientID int   = null ,

	@LeadTypeName varchar (50)  = null ,

	@LeadTypeDescription varchar (255)  = null ,

	@Enabled bit   = null ,

	@Live bit   = null ,

	@MatterDisplayName varchar (100)  = null ,

	@LastReferenceInteger int   = null ,

	@ReferenceValueFormatID int   = null ,

	@IsBusiness bit   = null ,

	@LeadDisplayName varchar (100)  = null ,

	@CaseDisplayName varchar (100)  = null ,

	@CustomerDisplayName varchar (100)  = null ,

	@LeadDetailsTabImageFileName varchar (255)  = null ,

	@CustomerTabImageFileName varchar (255)  = null ,

	@UseEventCosts bit   = null ,

	@UseEventUOEs bit   = null ,

	@UseEventDisbursements bit   = null ,

	@UseEventComments bit   = null ,

	@AllowMatterPageChoices bit   = null ,

	@AllowLeadPageChoices bit   = null ,

	@IsRPIEnabled bit   = null ,

	@FollowupWorkingDaysOnly bit   = null ,

	@MatterLastReferenceInteger int   = null ,

	@MatterReferenceValueFormatID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@CalculateAllMatters bit   = null ,

	@SmsGatewayID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadTypeID]
	, [ClientID]
	, [LeadTypeName]
	, [LeadTypeDescription]
	, [Enabled]
	, [Live]
	, [MatterDisplayName]
	, [LastReferenceInteger]
	, [ReferenceValueFormatID]
	, [IsBusiness]
	, [LeadDisplayName]
	, [CaseDisplayName]
	, [CustomerDisplayName]
	, [LeadDetailsTabImageFileName]
	, [CustomerTabImageFileName]
	, [UseEventCosts]
	, [UseEventUOEs]
	, [UseEventDisbursements]
	, [UseEventComments]
	, [AllowMatterPageChoices]
	, [AllowLeadPageChoices]
	, [IsRPIEnabled]
	, [FollowupWorkingDaysOnly]
	, [MatterLastReferenceInteger]
	, [MatterReferenceValueFormatID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [CalculateAllMatters]
	, [SmsGatewayID]
    FROM
	[dbo].[LeadType] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeName] = @LeadTypeName OR @LeadTypeName IS NULL)
	AND ([LeadTypeDescription] = @LeadTypeDescription OR @LeadTypeDescription IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Live] = @Live OR @Live IS NULL)
	AND ([MatterDisplayName] = @MatterDisplayName OR @MatterDisplayName IS NULL)
	AND ([LastReferenceInteger] = @LastReferenceInteger OR @LastReferenceInteger IS NULL)
	AND ([ReferenceValueFormatID] = @ReferenceValueFormatID OR @ReferenceValueFormatID IS NULL)
	AND ([IsBusiness] = @IsBusiness OR @IsBusiness IS NULL)
	AND ([LeadDisplayName] = @LeadDisplayName OR @LeadDisplayName IS NULL)
	AND ([CaseDisplayName] = @CaseDisplayName OR @CaseDisplayName IS NULL)
	AND ([CustomerDisplayName] = @CustomerDisplayName OR @CustomerDisplayName IS NULL)
	AND ([LeadDetailsTabImageFileName] = @LeadDetailsTabImageFileName OR @LeadDetailsTabImageFileName IS NULL)
	AND ([CustomerTabImageFileName] = @CustomerTabImageFileName OR @CustomerTabImageFileName IS NULL)
	AND ([UseEventCosts] = @UseEventCosts OR @UseEventCosts IS NULL)
	AND ([UseEventUOEs] = @UseEventUOEs OR @UseEventUOEs IS NULL)
	AND ([UseEventDisbursements] = @UseEventDisbursements OR @UseEventDisbursements IS NULL)
	AND ([UseEventComments] = @UseEventComments OR @UseEventComments IS NULL)
	AND ([AllowMatterPageChoices] = @AllowMatterPageChoices OR @AllowMatterPageChoices IS NULL)
	AND ([AllowLeadPageChoices] = @AllowLeadPageChoices OR @AllowLeadPageChoices IS NULL)
	AND ([IsRPIEnabled] = @IsRPIEnabled OR @IsRPIEnabled IS NULL)
	AND ([FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly OR @FollowupWorkingDaysOnly IS NULL)
	AND ([MatterLastReferenceInteger] = @MatterLastReferenceInteger OR @MatterLastReferenceInteger IS NULL)
	AND ([MatterReferenceValueFormatID] = @MatterReferenceValueFormatID OR @MatterReferenceValueFormatID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([CalculateAllMatters] = @CalculateAllMatters OR @CalculateAllMatters IS NULL)
	AND ([SmsGatewayID] = @SmsGatewayID OR @SmsGatewayID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadTypeID]
	, [ClientID]
	, [LeadTypeName]
	, [LeadTypeDescription]
	, [Enabled]
	, [Live]
	, [MatterDisplayName]
	, [LastReferenceInteger]
	, [ReferenceValueFormatID]
	, [IsBusiness]
	, [LeadDisplayName]
	, [CaseDisplayName]
	, [CustomerDisplayName]
	, [LeadDetailsTabImageFileName]
	, [CustomerTabImageFileName]
	, [UseEventCosts]
	, [UseEventUOEs]
	, [UseEventDisbursements]
	, [UseEventComments]
	, [AllowMatterPageChoices]
	, [AllowLeadPageChoices]
	, [IsRPIEnabled]
	, [FollowupWorkingDaysOnly]
	, [MatterLastReferenceInteger]
	, [MatterReferenceValueFormatID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [CalculateAllMatters]
	, [SmsGatewayID]
    FROM
	[dbo].[LeadType] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeName] = @LeadTypeName AND @LeadTypeName is not null)
	OR ([LeadTypeDescription] = @LeadTypeDescription AND @LeadTypeDescription is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Live] = @Live AND @Live is not null)
	OR ([MatterDisplayName] = @MatterDisplayName AND @MatterDisplayName is not null)
	OR ([LastReferenceInteger] = @LastReferenceInteger AND @LastReferenceInteger is not null)
	OR ([ReferenceValueFormatID] = @ReferenceValueFormatID AND @ReferenceValueFormatID is not null)
	OR ([IsBusiness] = @IsBusiness AND @IsBusiness is not null)
	OR ([LeadDisplayName] = @LeadDisplayName AND @LeadDisplayName is not null)
	OR ([CaseDisplayName] = @CaseDisplayName AND @CaseDisplayName is not null)
	OR ([CustomerDisplayName] = @CustomerDisplayName AND @CustomerDisplayName is not null)
	OR ([LeadDetailsTabImageFileName] = @LeadDetailsTabImageFileName AND @LeadDetailsTabImageFileName is not null)
	OR ([CustomerTabImageFileName] = @CustomerTabImageFileName AND @CustomerTabImageFileName is not null)
	OR ([UseEventCosts] = @UseEventCosts AND @UseEventCosts is not null)
	OR ([UseEventUOEs] = @UseEventUOEs AND @UseEventUOEs is not null)
	OR ([UseEventDisbursements] = @UseEventDisbursements AND @UseEventDisbursements is not null)
	OR ([UseEventComments] = @UseEventComments AND @UseEventComments is not null)
	OR ([AllowMatterPageChoices] = @AllowMatterPageChoices AND @AllowMatterPageChoices is not null)
	OR ([AllowLeadPageChoices] = @AllowLeadPageChoices AND @AllowLeadPageChoices is not null)
	OR ([IsRPIEnabled] = @IsRPIEnabled AND @IsRPIEnabled is not null)
	OR ([FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly AND @FollowupWorkingDaysOnly is not null)
	OR ([MatterLastReferenceInteger] = @MatterLastReferenceInteger AND @MatterLastReferenceInteger is not null)
	OR ([MatterReferenceValueFormatID] = @MatterReferenceValueFormatID AND @MatterReferenceValueFormatID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([CalculateAllMatters] = @CalculateAllMatters AND @CalculateAllMatters is not null)
	OR ([SmsGatewayID] = @SmsGatewayID AND @SmsGatewayID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Find] TO [sp_executeall]
GO
