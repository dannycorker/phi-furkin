SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Reset Followupdatetime for open leadevents
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ResetFollowUp]
(
	@LeadEventID INT,
	@AddOrReset VARCHAR(100),
	@TimeUnits INT,
	@NumberOfTimeUnits INT,
	@DoNotUpdateEventTypes VARCHAR(1000)
)


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID INT, 
		MatterID INT 
	);

	DECLARE	@LeadID INT,
			@ClientID INT,
			@CaseID INT

	SELECT
	@LeadID = LeadEvent.LeadID,
	@ClientID = LeadEvent.ClientID,
	@CaseID = LeadEvent.CaseID
	FROM leadevent
	WHERE leadevent.leadeventid = @LeadEventID 

	IF @AddOrReset = 'Add'
	BEGIN

		UPDATE LeadEvent
		SET LeadEvent.FollowupDateTime = [dbo].[fnAddTimeUnitsToDate](LeadEvent.followupdatetime, @TimeUnits, @NumberOfTimeUnits)	
		FROM LeadEvent
		INNER JOIN EventType et ON et.EventTypeID = LeadEvent.EventTypeID
		LEFT JOIN fnTableOfIDsFromCSV(@DoNotUpdateEventTypes) f ON f.anyid = et.Eventtypeid 
		WHERE LeadEvent.CaseID = @CaseID
		AND WhenFollowedUp IS NULL
		AND et.InProcess = 1
		AND f.anyid IS NULL

	END

	IF @AddOrReset = 'Reset'
	BEGIN

		UPDATE LeadEvent
		SET LeadEvent.FollowupDateTime = [dbo].[fnAddTimeUnitsToDate](dbo.fn_GetDate_Local(), @TimeUnits, @NumberOfTimeUnits)	
		FROM LeadEvent
		INNER JOIN EventType et ON et.EventTypeID = LeadEvent.EventTypeID
		LEFT JOIN fnTableOfIDsFromCSV(@DoNotUpdateEventTypes) f ON f.anyid = et.Eventtypeid 
		WHERE LeadEvent.CaseID = @CaseID
		AND WhenFollowedUp IS NULL
		AND et.InProcess = 1
		AND f.anyid IS NULL

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ResetFollowUp] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ResetFollowUp] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ResetFollowUp] TO [sp_executeall]
GO
