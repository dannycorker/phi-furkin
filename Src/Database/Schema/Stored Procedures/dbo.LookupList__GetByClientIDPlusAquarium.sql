SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-03
-- Description:	Merge Aquarium lists with those for the specific ClientID passed in
-- Modified:	2014-07-15	SB	Updated to allow shared 
-- =============================================
CREATE PROCEDURE [dbo].[LookupList__GetByClientIDPlusAquarium]
(
	@ClientID int   
)
AS
BEGIN

	SELECT *
	FROM
		dbo.fnLookupListShared(@ClientID)
	WHERE
		[ClientID] = @ClientID OR 
		([ClientID] = 0 AND LeadTypeID IS NULL) -- Don't return shared lookup lists that have not been linked to this client

	ORDER BY ClientID, [LookupListName] 
	
	SELECT @@ROWCOUNT

END


GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList__GetByClientIDPlusAquarium] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList__GetByClientIDPlusAquarium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList__GetByClientIDPlusAquarium] TO [sp_executeall]
GO
