SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Chart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_Update]
(

	@ChartID int   ,

	@QueryID int   ,

	@ClientID int   ,

	@ChartTitle varchar (150)  ,

	@ChartDescription varchar (MAX)  ,

	@ChartTypeID int   ,

	@XAxisColumn varchar (100)  ,

	@CreatedBy int   ,

	@CreatedOn datetime   ,

	@LastEditedBy int   ,

	@LastEditedOn datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Chart]
				SET
					[QueryID] = @QueryID
					,[ClientID] = @ClientID
					,[ChartTitle] = @ChartTitle
					,[ChartDescription] = @ChartDescription
					,[ChartTypeID] = @ChartTypeID
					,[XAxisColumn] = @XAxisColumn
					,[CreatedBy] = @CreatedBy
					,[CreatedOn] = @CreatedOn
					,[LastEditedBy] = @LastEditedBy
					,[LastEditedOn] = @LastEditedOn
				WHERE
[ChartID] = @ChartID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Update] TO [sp_executeall]
GO
