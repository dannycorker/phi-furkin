SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentStandardTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_GetByDocumentTypeVersionID]
(

	@DocumentTypeVersionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentStandardTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes],
					[IsSpecial],
					[ExcelSheetLocation],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentStandardTarget] WITH (NOLOCK) 
				WHERE
					[DocumentTypeVersionID] = @DocumentTypeVersionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByDocumentTypeVersionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_GetByDocumentTypeVersionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByDocumentTypeVersionID] TO [sp_executeall]
GO
