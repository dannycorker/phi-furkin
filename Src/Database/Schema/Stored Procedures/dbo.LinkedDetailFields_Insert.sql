SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LinkedDetailFields table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LinkedDetailFields_Insert]
(

	@LinkedDetailFieldID int    OUTPUT,

	@LeadTypeIDTo int   ,

	@LeadTypeIDFrom int   ,

	@DetailFieldIDTo int   ,

	@DetailFieldIDFrom int   ,

	@DisplayOnPageID int   ,

	@FieldOrder int   ,

	@Enabled bit   ,

	@History bit   ,

	@LeadOrMatter int   ,

	@FieldName varchar (100)  ,

	@LeadLinkedTo int   ,

	@LeadLinkedFrom int   ,

	@IncludeLinkedField bit   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[LinkedDetailFields]
					(
					[LeadTypeIDTo]
					,[LeadTypeIDFrom]
					,[DetailFieldIDTo]
					,[DetailFieldIDFrom]
					,[DisplayOnPageID]
					,[FieldOrder]
					,[Enabled]
					,[History]
					,[LeadOrMatter]
					,[FieldName]
					,[LeadLinkedTo]
					,[LeadLinkedFrom]
					,[IncludeLinkedField]
					,[ClientID]
					)
				VALUES
					(
					@LeadTypeIDTo
					,@LeadTypeIDFrom
					,@DetailFieldIDTo
					,@DetailFieldIDFrom
					,@DisplayOnPageID
					,@FieldOrder
					,@Enabled
					,@History
					,@LeadOrMatter
					,@FieldName
					,@LeadLinkedTo
					,@LeadLinkedFrom
					,@IncludeLinkedField
					,@ClientID
					)
				-- Get the identity value
				SET @LinkedDetailFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LinkedDetailFields_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Insert] TO [sp_executeall]
GO
