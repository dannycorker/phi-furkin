SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectTypeRelationship table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_GetByObjectTypeRelationshipID]
(

	@ObjectTypeRelationshipID int   
)
AS


				SELECT
					[ObjectTypeRelationshipID],
					[FromObjectTypeID],
					[ToObjectTypeID],
					[RelationshipTypeID],
					[RelationshipName]
				FROM
					[dbo].[ObjectTypeRelationship] WITH (NOLOCK) 
				WHERE
										[ObjectTypeRelationshipID] = @ObjectTypeRelationshipID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_GetByObjectTypeRelationshipID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_GetByObjectTypeRelationshipID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_GetByObjectTypeRelationshipID] TO [sp_executeall]
GO
