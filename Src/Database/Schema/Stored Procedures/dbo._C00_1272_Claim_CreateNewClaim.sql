SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-08-26
-- Description:	Creates a new claim case and matter
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_CreateNewClaim] 
(
	@LeadID INT,
	@ClientPersonnelID INT
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CaseID INT,
			@MatterID INT
			
	EXEC @MatterID = dbo._C00_CreateNewCaseForLead @LeadID, @ClientPersonnelID
	
	SELECT @CaseID = CaseID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	EXEC dbo._C00_AddProcessStart @CaseID, @ClientPersonnelID
	
	SELECT @CaseID AS CaseID, @MatterID AS MatterID
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_CreateNewClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_CreateNewClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_CreateNewClaim] TO [sp_executeall]
GO
