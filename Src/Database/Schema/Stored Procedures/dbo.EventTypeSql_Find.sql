SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeSql table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeSqlID int   = null ,

	@ClientID int   = null ,

	@EventTypeID int   = null ,

	@PostUpdateSql varchar (MAX)  = null ,

	@IsNativeSql bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeSqlID]
	, [ClientID]
	, [EventTypeID]
	, [PostUpdateSql]
	, [IsNativeSql]
    FROM
	[dbo].[EventTypeSql] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeSqlID] = @EventTypeSqlID OR @EventTypeSqlID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([PostUpdateSql] = @PostUpdateSql OR @PostUpdateSql IS NULL)
	AND ([IsNativeSql] = @IsNativeSql OR @IsNativeSql IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeSqlID]
	, [ClientID]
	, [EventTypeID]
	, [PostUpdateSql]
	, [IsNativeSql]
    FROM
	[dbo].[EventTypeSql] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeSqlID] = @EventTypeSqlID AND @EventTypeSqlID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([PostUpdateSql] = @PostUpdateSql AND @PostUpdateSql is not null)
	OR ([IsNativeSql] = @IsNativeSql AND @IsNativeSql is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Find] TO [sp_executeall]
GO
