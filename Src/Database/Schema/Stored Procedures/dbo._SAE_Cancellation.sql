SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2020-01-29
-- Description:	Sql After Event - Cancellation
-- 2020-02-13 CPS for JIRA AAG-91	| Change the CalculatePremiumForPeriod call to the _Evaluated version
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- 2020-08-18 ACE Removed redundant lead table/leadtypeid variable in initial select
-- 2021-01-20 ACE FURKIN-130 - Add Enrollment Fee
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Cancellation]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PolicyLeadID					INT
			,@ColLeadID						INT
			,@WorkflowTaskID				INT
			,@LogEntry						VARCHAR(2000)
			,@PurchasedProductID			INT
			,@CancellationDate				DATE
			,@AccountID						INT
			,@RenewalTableRowID				INT
			,@ValueDate						DATE
			,@TableRowID					INT
			,@ValueMoney					MONEY
			,@PremiumNetLogged				NUMERIC(18,2)
			,@PremiumTaxLogged				NUMERIC(18,2)
			,@PremiumGrossLogged			NUMERIC(18,2)
			,@VarcharDate					VARCHAR(10)
			,@AdjustmentDate				DATE
			,@PurchasedProductPaymentScheduleID	INT
			,@CustomerPaymentScheduleID		INT
			,@AmountPaidToDate				MONEY
			,@PPPSID						INT
			,@AdjustmentAmountGross			NUMERIC(18,2)
			,@AdjustmentAmountNet			NUMERIC(18,2)
			,@AdjustmentAmountTax			NUMERIC(18,2)
			,@CoverStart					DATE
			,@CoverEnd						DATE
			,@FinalPaymentAmountGross		NUMERIC(18,2)
			,@FinalPaymentAmountNet			NUMERIC(18,2)
			,@FinalPaymentAmountTax			NUMERIC(18,2)
			,@AdjustmentAmount				MONEY
			,@Adjustment					DECIMAL(18,2)
			,@NewWritten					DECIMAL (18,2) 
			,@NewTableRowID					DECIMAL (18,2) 
			,@VarcharDateTime				VARCHAR(19)
			,@DetailValue					VARCHAR(2000)
			,@ClaimLeadID					INT
			,@PremiumGross					DECIMAL (18,2)
			,@ClientAccountID				INT
			,@NewLeadEventID				INT
			,@CommissionValue				MONEY
			,@DailyCommission				MONEY
			,@DaysRemaining					INT
			,@StartDate						DATE
			,@CommissionClawback			MONEY
			,@PaymentTypeID					INT
			,@Overrides						dbo.tvpIntVarcharVarchar
			,@WrittenPremiumID				INT
			,@AdjustmentNet					DECIMAL (18,2)
			,@PolicyOneVisionID				INT
			,@WellnessOneVisionID			INT
			,@ExamFeeOneVisionID			INT
			,@TransactionFeeLogged			NUMERIC(18,2)
			,@AccountTypeID					INT
			,@NumberOfinstallments			INT
			,@PaymentTypeLuli				INT
			,@PaymentMethod					INT
			,@Refundable					INT
			,@StateCode						VARCHAR(100)
			,@GETDATENowLocal				DATETIME = dbo.fn_GetDate_Local()
			,@PurchasedProductPaymentScheduleParentID INT
			,@TransactionFee				DECIMAL(18,2)
			,@CancellationAdjustmentGross	DECIMAL(18,2)
			,@CancellationAdjustmentNet		DECIMAL(18,2)
			/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
			,@EnrollmentFeeRefundNet		NUMERIC(18,2)
			,@EnrollmentFeeRefundTax		NUMERIC(18,2)
			,@EnrollmentFeeRefundGross		NUMERIC(18,2)
			,@ProjectedCancellationDate		DATE
			,@CollectionsMatterID			INT
			,@ValueInt						INT
			,@Now							DATE = dbo.fn_GetDate_Local()

			DECLARE @InsertedPPPS TABLE (PPPSID INT)

			DECLARE @InsertedCPS TABLE (CustomerPaymentScheduleID INT, PurchasedProductPaymentScheduleID INT)

			DECLARE @AdjDate TABLE (AdjDate DATE)

			DECLARE @NotesToDelete TABLE (leadeventID INT, done INT) 

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
				 @LeadID		= le.LeadID
				,@EventTypeID	= le.EventTypeID
				,@CustomerID	= m.CustomerID
				,@ClientID		= m.ClientID
				,@CaseID		= le.CaseID
				,@WhoCreated	= le.WhoCreated
				,@MatterID		= m.MatterID
				,@WhenCreated	= le.WhenCreated
				,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0)
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID	
	
/* ===================================================================================================================== */

	/*Make sure the workflow tasks are completed for the collections workflow*/ 
	/*We can't use standard method for this as it is a cross lead type workflow that doesn't have a process*/ 
	IF @EventTypeID IN (155261)
	BEGIN 

		IF EXISTS (
			SELECT * 
			FROM Lead l with (NOLOCK) 
			WHERE l.LeadTypeID = 1493
			and l.LeadID = @LeadID
		) 
		BEGIN 
		
			SELECT @PolicyLeadID = ISNULL(dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@LeadID),0) 

		END 

		IF EXISTS (
			SELECT * 
			FROM Lead l with (NOLOCK) 
			WHERE l.LeadTypeID = 1490
			and l.LeadID = @LeadID
		) 
		BEGIN 
		
			SELECT @ColLeadID = ISNULL(dbo.fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin(@LeadID),0) 

		END 

		SELECT @WorkflowTaskID =  w.WorkflowTaskID 
		FROM workflowtask w WITH (NOLOCK) 
		WHERE w.LeadID IN (@LeadID,@ColLeadID,@PolicyLeadID) 
		AND w.WorkflowGroupID = 1233
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID,w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, @WhoCreated,@GETDATENowLocal, 'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE w.LeadID IN (@LeadID,@ColLeadID,@PolicyLeadID) 
		AND w.WorkflowGroupID = 1233 
		AND w.WorkflowTaskID = @WorkflowTaskID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 

	END 

	/*ALM 2021-03-29 FURKIN-424 Move Projected Cancellation Date to Cancellation Date field*/
	IF @EventTypeID = 155374 
	BEGIN 

		SELECT @ProjectedCancellationDate = dbo.fnGetSimpleDvAsDate (315274, @MatterID)

		EXEC _C00_SimpleValueIntoField 170055, @ProjectedCancellationDate, @MatterID 

	END

	/* Apply cancelled policy processing */
	IF @EventTypeID IN (155266, -- Apply refund (policy cancelled)
						155374, -- Policy cancelled by collections process
						156748, -- policy cancelled from claims
						156892, -- PH EM Policy Cancelled Collection Declined
						156891) -- PH LET Policy Cancelled Collection Declined
	BEGIN
		
		IF @EventTypeID=155266
		BEGIN
			
			PRINT 'Queue'
			EXEC dbo._C600_PA_QueueCancellation  @LeadEventID, @MatterID
			PRINT '</ br>'
			PRINT 'Queued'
			PRINT '</ br>'

			-- banner note
			-- AJH 21/08/2017 create correct variant of banner note depending on type of adjustment value
			SELECT @LogEntry = 'Policy Cancelled. Adjustment value is GBP' + dbo.fnGetDv (175379, @CaseID) +
				CASE 
						WHEN dbo.fnGetDvAsMoney (175379, @CaseID)=0 
						THEN ' (zero refund because payment has not yet been taken or monthly payer).'
						WHEN dbo.fnGetDvAsMoney (175379, @CaseID)<0 
						THEN ' (refund is due to customer).'
						ELSE ' (payment is due from customer).'
				END
									
		END

		IF @EventTypeID in (155374,156748) /*Policy cancelled by collections process*/ /*Policy cancelled by claims process*/
		BEGIN
		
			-- get note text from collections lead
			SELECT @LogEntry=dbo.fnGetDv (175476,m.CaseID)
			FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN Matter m WITH (NOLOCK) ON ltr.ToMatterID=m.MatterID
			WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
			
			SELECT @LogEntry = 'Policy Cancelled. ' + ISNULL(@LogEntry,'')
			
			-- update cancellation reason
			IF dbo.fnGetSimpleDvAsInt(170054,@MatterID) <> 69911 
			BEGIN

			IF dbo.fnGetSimpleDvAsInt(170114,@MatterID) = 76618 /*Premium Funder*/
				BEGIN
					EXEC _C00_SimpleValueIntoFieldLuli  170054, 'Cancelled by Premium Funder', @MatterID, @AqAutomation
				END
				ELSE
				BEGIN
					EXEC _C00_SimpleValueIntoField  170054, 72040 /*Cancelled by CL*/, @MatterID, @AqAutomation -- cancelled by company
				END
			END	
			
			/*Have we already prepped a renewal for this policy?*/ 
			IF EXISTS (SELECT * FROM PurchasedProduct p with (NOLOCK) 
						INNER JOIN MatterDetailValues  mdv with (NOLOCK) on mdv.MatterID = p.ObjectID
						Where p.ObjectID = @MatterID 
						AND mdv.DetailFieldID = 170055 /*Cancel Date*/ 
						AND p.ValidFrom >= mdv.ValueDate
						AND p.ValidFrom > CAST(@GETDATENowLocal as DATE) 
			) 
			BEGIN 

				SELECT @PurchasedProductID = NULL 
				SELECT @PurchasedProductID = p.PurchasedProductID, 
						@CancellationDate = mdv.ValueDate, 
						@AccountID = p.AccountID,
						@AccountTypeID = a.AccountTypeID,
						@NumberOfinstallments = p.NumberOfInstallments
				FROM PurchasedProduct p with (NOLOCK) 
				INNER JOIN MatterDetailValues  mdv with (NOLOCK) on mdv.MatterID = p.ObjectID
				INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = p.AccountID
				Where p.ObjectID = @MatterID 
				AND mdv.DetailFieldID = 170055 /*Cancel Date*/ 
				AND p.ValidFrom >= mdv.ValueDate

				AND p.ValidFrom > CAST(@GETDATENowLocal as DATE) 
	
				/*Updare The PurchasedProduct's end date and set it's schedules to ignore*/ 
				UPDATE PurchasedProduct 
				SET ValidTo = @CancellationDate 
				WHERE PurchasedProductID = @PurchasedProductID 

				UPDATE PurchasedProductPaymentSchedule 
				SET PaymentStatusID = 3 
				WHERE PurchasedProductID = @PurchasedProductID 
				AND PaymentStatusID IN (1,5)

				/*Deal with the WP*/
				/*find the renewal Table Row*/ 
				SELECT @RenewalTableRowID = tr.tableRowID, @ValueDate = frm.ValueDate 
				FROM TableRows tr WITH (NOLOCK)
				INNER JOIN TableDetailValues adj with (NOLOCK) on adj.TableROwID = tr.TableRowID AND adj.DetailFIeldID = 175398 /*Adjustment Type*/ 
				INNER JOIN tabledetailValues frm with (NOLOCK) on frm.TableRowID = tr.TableRowID AND frm.DetailFieldID = 175346 /*Adjustment From Date*/
				WHERE tr.MatterID = @MatterID 
				AND tr.DetailFieldID = 175336
				AND adj.ValueINT = 74528 /*Renewal*/ 
				AND frm.ValueDate >= @CancellationDate	

				PRINT @RenewalTableROwID 
				/*Set the to Date on renewal Table row*/ 
				EXEC _C00_SimpleValueINTOField 175347,@ValueDate,@RenewalTableRowID 

				EXEC TableRows_Insert  @TableRowID OUTPUT, @ClientID,NULL,@MatterID,175336,19011,1,1,NULL,NULL,NULL,NULL,@RenewalTableRowID
	
				PRINT @TableRowID 

				/*Insert the same values as the renewal row except for the premium and commision*/ 
				INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,MatterID,ClientID)
				SELECT @TableRowID, tdv.DetailFieldID,tdv.DetailValue, tdv.MatterID,tdv.ClientID 
				FROM TableDetailValues tdv with (NOLOCK) 
				WHERE tdv.TableRowID = @RenewalTableRowID 
				AND DetailFieldID NOT IN (180170,180171,175397,177899)

				/*Set the adjustment amount to be negative*/
	
				SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(175397,@RenewalTableRowID) * -1) 
				EXEC _C00_SimpleValueIntoField 175397,@ValueMoney,@TableRowID

				SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(177899,@RenewalTableRowID) * -1) 
				EXEC _C00_SimpleValueIntoField 177899,@ValueMoney,@TableRowID	

				/*Check the customer has not already paid their renewal*/
				IF EXISTS ( 
					SELECT * 
					FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) /* SLACKY-PERF-20190705 */
					WHERE ppps.PurchasedProductID = @PurchasedProductID
					AND ppps.PaymentStatusID = 6 /*Paid*/
				)
				BEGIN

					SELECT @PremiumNetLogged = SUM(ppps.PaymentNet), @PremiumTaxLogged = SUM(ppps.PaymentVAT), @PremiumGrossLogged = SUM(ppps.PaymentGross), @TransactionFeeLogged = SUM(ppps.TransactionFee)
					FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) /* SLACKY-PERF-20190705 */
					WHERE ppps.PurchasedProductID = @PurchasedProductID
					AND ppps.PaymentStatusID = 6 /*Paid*/
					AND ppps.PurchasedProductPaymentScheduleTypeID <> 5 /*Transaction Fee (Enrollment Fee)*/

					SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
							THEN 69930 /*Direct Debit*/
							ELSE 69931 /*Credit Card*/
							END

					SELECT @PaymentMethod = CASE @NumberOfInstallments WHEN 1
							THEN 69944 /*Annually*/
							ELSE 69943 /*Monthly*/
							END

					SELECT @StateCode = c.County
					FROM Customers c WITH (NOLOCK)
					WHERE c.CustomerID = @CustomerID

					SELECT @Refundable = dbo._C605_ReturnTransactionRefundableByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod)

					/*Default to paying the fee (IF not set pay)*/
					IF ISNULL(@Refundable, 0) = 5145 /*No*/
					BEGIN

						/*If the transaction fee is not refundable */
						SELECT @TransactionFeeLogged = 0

					END

					/*2020-09-18 ACE Need to remove the TF or it'll be added twice.. Once in the proc and once here.*/
					SELECT @PremiumGrossLogged = @PremiumGrossLogged - ISNULL(@TransactionFeeLogged, 0.00)

					/*Flip all values negative for TR proc.*/
					SELECT @PremiumNetLogged = -@PremiumNetLogged,
							@PremiumTaxLogged = -@PremiumTaxLogged,
							@PremiumGrossLogged = -@PremiumGrossLogged

					/*2020-09-15 ACE call transactional refund proc..*/
					EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, @PremiumNetLogged, @PremiumTaxLogged, @PremiumGrossLogged,
									1 /*Premium*/, @GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal, 7, @WhoCreated, 0, NULL, @TransactionFeeLogged, '_SAE_Cancellation_1'

					/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
					SELECT	@EnrollmentFeeRefundNet = SUM(s.PaymentNet)*-1.00,
							@EnrollmentFeeRefundTax = SUM(s.PaymentVat)*-1.00,
							@EnrollmentFeeRefundGross = SUM(s.PaymentGross)*-1.00
					FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
					WHERE s.PurchasedProductID = @PurchasedProductID
					AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
					AND s.PaymentStatusID = 6 /*Paid only!*/

					PRINT '@AccountTypeID'
					PRINT @AccountTypeID

					EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'Type', 'Card', @WhoCreated
					EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', '@EnrollmentFeeRefundGross', @EnrollmentFeeRefundGross, @WhoCreated

					PRINT '@EnrollmentFeeRefundGross'
					PRINT @EnrollmentFeeRefundGross

					/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
					IF @EnrollmentFeeRefundGross <> 0.00
					BEGIN

						/*Make sure we havent queued this up already*/
						IF NOT EXISTS (
							SELECT *
							FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
							WHERE s.PurchasedProductID = @PurchasedProductID
							AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
							AND s.PaymentStatusID = 1 /*New*/
						)
						BEGIN

							EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'PurchasedProductPaymentSchedule', 'NoNew', @WhoCreated
		
							PRINT 'Create Enrollment Fee Refund'

							/*2021-01-22 ACE call transactional refund proc..*/
							EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, @EnrollmentFeeRefundNet, @EnrollmentFeeRefundTax, @EnrollmentFeeRefundGross,
											1 /*Premium*/, @GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal, 5, @WhoCreated, 0, NULL, @TransactionFeeLogged, '_SAE_Cancellation_2'

						END

					END
					/*End add enrollment fee*/

					/*
					INSERT INTO PurchasedProductPaymentSchedule([ClientID], [CustomerID], [AccountID], [PurchasedProductID], [ActualCollectionDate], [CoverFrom], [CoverTo], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], [PurchasedProductPaymentScheduleTypeID], [PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID], TransactionFee)
					OUTPUT inserted.PurchasedProductPaymentScheduleID INTO @InsertedPPPS (PPPSID)
					SELECT @ClientID, @CustomerID, @AccountID, @PurchasedProductID, @GETDATENowLocal, 
						@GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal,
						-@PremiumNetLogged , -@PremiumTaxLogged, -@PremiumGrossLogged, 1, NULL AS [CustomerLedgerID], @GETDATENowLocal AS [ReconciledDate], NULL AS [CustomerPaymentScheduleID], @WhoCreated, @GETDATENowLocal, NULL AS [PurchasedProductPaymentScheduleParentID], 7 AS [PurchasedProductPaymentScheduleTypeID], NULL AS [PaymentGroupedIntoID], NULL AS [ContraCustomerLedgerID], @ClientAccountID, @WhoCreated, GETDATE () AS [WhenModified], NULL AS [SourceID], @TransactionFeeLogged*-1

					SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()


					INSERT INTO CustomerPaymentSchedule ([ClientID], [CustomerID], [AccountID], [ActualCollectionDate], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [WhoCreated], [WhenCreated], [SourceID], [RelatedObjectID], [RelatedObjectTypeID], [ClientAccountID], [WhoModified], [WhenModified])
					OUTPUT inserted.CustomerPaymentScheduleID, inserted.SourceID INTO @InsertedCPS(CustomerPaymentScheduleID, PurchasedProductPaymentScheduleID)
					SELECT pps.ClientID, pps.CustomerID, pps.AccountID, pps.ActualCollectionDate, pps.[PaymentDate], pps.[PaymentNet], pps.[PaymentVAT], pps.[PaymentGross], pps.[PaymentStatusID], pps.[CustomerLedgerID], pps.[ReconciledDate], pps.[WhoCreated], pps.[WhenCreated], pps.PurchasedProductPaymentScheduleID AS [SourceID], @MatterID, 2, pps.[ClientAccountID], pps.[WhoModified], pps.[WhenModified]
					FROM @InsertedPPPS ipps
					INNER JOIN PurchasedProductPaymentSchedule pps ON pps.PurchasedProductPaymentScheduleID = ipps.PPPSID

					SELECT @CustomerPaymentScheduleID = SCOPE_IDENTITY()

					UPDATE pps
					SET CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
					FROM PurchasedProductPaymentSchedule pps 
					INNER JOIN @InsertedCPS p ON p.PurchasedProductPaymentScheduleID = pps.PurchasedProductPaymentScheduleID
					*/

				END

			END

		END

		IF @EventTypeID IN (156891,155374,156748) -- cancelled with collections declined
		BEGIN

			-- set all PPPS rows from 'New' to 'Ignore' & rebuild the CPS		
			SELECT @PurchasedProductID=pp.PurchasedProductID, @AccountID=pp.AccountID 
			FROM MatterDetailValues ppid WITH (NOLOCK) 
			INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON ppid.ValueInt=pp.PurchasedProductID
			WHERE ppid.MatterID=@MatterID 
			AND ppid.DetailFieldID=177074
			
			/*
				Policy cancelled by collections process
				PH LET Policy Cancelled Collection Declined
				PH EM Policy Cancelled Collection Declined			
			*/
			IF @EventTypeID IN (156891,155374)
			BEGIN

				/*2018-07-02 ACE - Get the amount paid to work out when the cover finishes...*/
				SELECT @AmountPaidToDate = SUM(ppps.PaymentGross)
				FROM MatterDetailValues mpp WITH (NOLOCK) 
				INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON mpp.ValueInt=ppps.PurchasedProductID
				WHERE mpp.MatterID=@MatterID 
				AND mpp.DetailFieldID=177074
				AND ppps.PaymentStatusID IN (2,6) -- processed, paid
				-- and a later 'failed' payment record does not exist for this orginal payment schedule entry
				AND NOT EXISTS (
					SELECT * 
					FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
					WHERE ppps2.PurchasedProductPaymentScheduleParentID=ppps.PurchasedProductPaymentScheduleParentID 
					AND ppps2.PaymentStatusID=4
					AND ppps2.PurchasedProductPaymentScheduleID>ppps.PurchasedProductPaymentScheduleID
				)	 

				/*GPR 2019-12-19 for LPC-258*/
				IF (SELECT [dbo].[fnGetSimpleDvAsInt] (170114, @MatterID)) <> 76618 /*NOT Premium Funding*/
				BEGIN

					INSERT INTO @AdjDate
					EXEC _C00_GetCoverToDateFromPaidAmount @PurchasedProductID, @AmountPaidToDate

					SELECT @AdjustmentDate = s.AdjDate
					FROM @AdjDate s
				
				END
				ELSE
				BEGIN
					SELECT @AdjustmentDate= @GETDATENowLocal
				END

				IF @AdjustmentDate IS NOT NULL
				BEGIN

					SELECT TOP 1 @PPPSID = ppps.PurchasedProductPaymentScheduleID
					FROM MatterDetailValues mpp WITH (NOLOCK) 
					INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON mpp.ValueInt=ppps.PurchasedProductID
					WHERE mpp.MatterID=@MatterID 
					AND mpp.DetailFieldID=177074
					AND ppps.PaymentStatusID IN (2,6) -- processed, paid
					-- and a later 'failed' payment record does not exist for this orginal payment schedule entry
					AND NOT EXISTS (
						SELECT * 
						FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
						WHERE ppps2.PurchasedProductPaymentScheduleParentID=ppps.PurchasedProductPaymentScheduleParentID 
						AND ppps2.PaymentStatusID=4
						AND ppps2.PurchasedProductPaymentScheduleID>ppps.PurchasedProductPaymentScheduleID
					)
					ORDER BY ppps.CoverTo DESC

					UPDATE ppps
					SET CoverTo = @AdjustmentDate
					FROM PurchasedProductPaymentSchedule ppps
					WHERE ppps.PurchasedProductPaymentScheduleID = @PPPSID

				END

			END

			-- set cancellation date back to last paid up cover period
			IF @AdjustmentDate IS NULL -- no payments have been made, go back to the policy start date
				SELECT @AdjustmentDate=dbo.fnGetDvAsDate(170036,@CaseID)

			SELECT @VarcharDate=CONVERT(VARCHAR(10),@AdjustmentDate,120)

			IF @EventTypeID IN (156748, 150106) /*GPR 2018-07-24 added EventTypeID for 	Mandate failed - Cancel cover and send notification*/
			BEGIN

				SELECT @AdjustmentDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID)
				/*In the 1272 SAE the date is completed. However we keep overwriting it with garbage..*/
				SELECT @VarcharDate=CONVERT(VARCHAR(10),@AdjustmentDate,120)

				SELECT @AdjustmentAmountGross = d.AdjustmentGross, @AdjustmentAmountNet = d.AdjustmentNet, @AdjustmentAmountTax = d.AdjustmentVAT, @CoverStart = d.CoverFrom, @CoverEnd = d.CoverTo
				FROM dbo.fn_C00_CancellationAdjustmentByDailyPremium(@MatterID, @AdjustmentDate) d

				SELECT @FinalPaymentAmountGross = @AdjustmentAmountGross,
						@FinalPaymentAmountNet = @AdjustmentAmountNet,
						@FinalPaymentAmountTAX = @AdjustmentAmountTax

				IF EXISTS (
					SELECT *
					FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) /* SLACKY-PERF-20190705 */
					WHERE PaymentStatusID IN (2,6)
					AND PurchasedProductID=@PurchasedProductID
					AND PaymentGross > 0.00
				)
				BEGIN

					SELECT @AdjustmentAmountGross = (ISNULL(SUM(pp.PaymentGross),0) - @AdjustmentAmountGross)*-1.00, 
							@AdjustmentAmountNet = (ISNULL(SUM(pp.PaymentNet),0) - @AdjustmentAmountNet)*-1.00,
							@AdjustmentAmountTax = (ISNULL(SUM(pp.PaymentVAT),0) - @AdjustmentAmountTax)*-1.00
					FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) /* SLACKY-PERF-20190705 */
					WHERE PaymentStatusID=1 
					AND PurchasedProductID=@PurchasedProductID
					AND PaymentGross > 0.00

				END
				ELSE
				BEGIN

					SELECT @AdjustmentAmountGross = (SUM(pp.ProductCostGross) - @AdjustmentAmountGross)*-1.00,
							@AdjustmentAmountNet = (SUM(pp.ProductCostNet) - @AdjustmentAmountNet)*-1.00,
							@AdjustmentAmountTax = (SUM(pp.ProductCostVAT) - @AdjustmentAmountTax)*-1.00
					FROM PurchasedProduct pp WITH (NOLOCK) /* SLACKY-PERF-20190705 */
					WHERE PurchasedProductID=@PurchasedProductID

				END

				SELECT @AdjustmentAmount = @AdjustmentAmountGross

				EXEC dbo._C00_SimpleValueIntoField 177404, @AdjustmentAmountGross, @MatterID, @WhoCreated -- adjustment amount

			END
			ELSE
			BEGIN

				EXEC dbo._C00_SimpleValueIntoField 170055, @VarcharDate, @MatterID, @AqAutomation -- cancellation date
				/*To Track the new value of written, first pick up how much the customer has paid for*/ 
					
				/*We need to track what the new WP is for this policy*/ 
				SELECT  @Adjustment = (p.ProductCostGross  - ISNULL(@AmountPaidToDate,0)) * -1, @NewWritten = p.ProductCostGross
				FROM PurchasedProduct p WITH (NOLOCK) 
				WHERE p.PurchasedProductID = @PurchasedProductID

				SELECT @NewWritten = @NewWritten + @Adjustment /*Adjustment is negative*/ 
				
				EXEC dbo._C00_SimpleValueIntoField 177404, @Adjustment, @MatterID, @WhoCreated -- adjustment amount
			
			END

			-- create new policy detail history row for the cancellation
			-- 2020-02-13 CPS for JIRA AAG-91	| Change the CalculatePremiumForPeriod call to the _Evaluated version
			--EXEC @NewTableRowID=dbo._C00_1273_Policy_CalculatePremiumForPeriod @MatterID, @Leadeventid, @VarcharDate, 5 
			EXEC @NewTableRowID=dbo._C00_1273_Policy_CalculatePremiumForPeriod_Evaluated @LeadEventID, 0, NULL, @Overrides, 5, 0
			
			SELECT TOP 1 @PurchasedProductPaymentScheduleID = pp.PurchasedProductPaymentScheduleID
			FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK)
			WHERE PaymentStatusID=1 
			AND PurchasedProductID=@PurchasedProductID
			AND PaymentGross > 0.00

			UPDATE PurchasedProductPaymentSchedule
			SET PaymentStatusID=3
			WHERE PaymentStatusID=1 
			AND PurchasedProductID=@PurchasedProductID
			AND PaymentGross > 0.00
			
			/*
				2018-07-05
				ACE - What a lot of messing about..
			*/
			IF @FinalPaymentAmountGross <> 0.00
			BEGIN

				/* 2020-09-16 ACE - Replaced with Billing__CreateTransactionalRefunds
				INSERT INTO PurchasedProductPaymentSchedule (ClientID,CustomerID,AccountID,PurchasedProductID,ActualCollectionDate,CoverFrom,CoverTo,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,CustomerPaymentScheduleID,WhoCreated,WhenCreated,PurchasedProductPaymentScheduleParentID, ClientAccountID, PurchasedProductPaymentScheduleTypeID)
				SELECT pp.ClientID,
					pp.CustomerID,
					pp.AccountID,
					pp.PurchasedProductID,pp.ActualCollectionDate,
					@CoverStart,@CoverEnd,
					pp.PaymentDate,@FinalPaymentAmountNet, @FinalPaymentAmountTAX, @FinalPaymentAmountGross,1,NULL AS [CustomerLedgerID],	
					NULL,NULL,58552,@GETDATENowLocal,pp.PurchasedProductPaymentScheduleParentID, pp.ClientAccountID,7
				FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
				WHERE pp.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
				*/

				SELECT @PurchasedProductPaymentScheduleParentID 
				FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
				WHERE pp.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

				EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, 
								@FinalPaymentAmountNet, @FinalPaymentAmountTAX, @FinalPaymentAmountGross,
								1 /*Premium*/, @CoverStart, @CoverEnd, @GETDATENowLocal, 7, 
								@WhoCreated, 0, @PurchasedProductPaymentScheduleParentID, @TransactionFeeLogged, '_SAE_Cancellation_3'

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				SELECT	@EnrollmentFeeRefundNet = SUM(s.PaymentNet)*-1.00,
						@EnrollmentFeeRefundTax = SUM(s.PaymentVat)*-1.00,
						@EnrollmentFeeRefundGross = SUM(s.PaymentGross)*-1.00
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
				WHERE s.PurchasedProductID = @PurchasedProductID
				AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
				AND s.PaymentStatusID = 6 /*Paid only!*/

				PRINT '@AccountTypeID'
				PRINT @AccountTypeID

				EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'Type', 'Card', @WhoCreated
				EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', '@EnrollmentFeeRefundGross', @EnrollmentFeeRefundGross, @WhoCreated

				PRINT '@EnrollmentFeeRefundGross'
				PRINT @EnrollmentFeeRefundGross

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				IF @EnrollmentFeeRefundGross <> 0.00
				BEGIN

					/*Make sure we havent queued this up already*/
					IF NOT EXISTS (
						SELECT *
						FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
						WHERE s.PurchasedProductID = @PurchasedProductID
						AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
						AND s.PaymentStatusID = 1 /*New*/
					)
					BEGIN

						EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'PurchasedProductPaymentSchedule', 'NoNew', @WhoCreated
		
						PRINT 'Create Enrollment Fee Refund'

						/*2021-01-22 ACE call transactional refund proc..*/
						EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, @EnrollmentFeeRefundNet, @EnrollmentFeeRefundTax, @EnrollmentFeeRefundGross,
										1 /*Premium*/, @GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal, 5, @WhoCreated, 0, NULL, @TransactionFeeLogged, '_SAE_Cancellation_4'

					END

				END
				/*End add enrollment fee*/
			END

			UPDATE PurchasedProduct 
			SET ValidTo = @AdjustmentDate 
			Where PurchasedProductID = @PurchasedProductID
			
			EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @AdjustmentDate, @AqAutomation

		END

		---- update adjustment values
		--SELECT TOP 1 @TableRowID=tr.TableRowID FROM TableRows tr WITH (NOLOCK)
		--INNER JOIN TableDetailValues adt WITH (NOLOCK) ON tr.TableRowID=adt.TableRowID AND adt.DetailFieldID=175398 
		--WHERE tr.DetailFieldID=175336 EXEC /*Premium Detail History*/
		--	AND adt.ValueInt= 74529
		--	AND adt.MatterID=@MatterID
		--ORDER BY tr.TableRowID DESC

		--SELECT @VarcharDateTime=CONVERT(VARCHAR(19),@GETDATENowLocal,121)
		--EXEC dbo._C00_SimpleValueIntoField 175400, @VarcharDateTime, @TableRowID, @WhoCreated	-- adjustment application date

		--SELECT @VarcharDate=dbo.fnGetDv(170055,@CaseID), @ValueMoney=dbo.fnGetDv(177404,@CaseID), @DetailValue=dbo.fnGetDv(170054,@CaseID)
		--EXEC dbo._C00_SimpleValueIntoField 175396, @VarcharDate, @TableRowID, @WhoCreated -- adjustment date	
		--EXEC dbo._C00_SimpleValueIntoField 175397, @ValueMoney, @TableRowID, @WhoCreated -- adjustment amount
		--EXEC dbo._C00_SimpleValueIntoField 175371, @DetailValue, @TableRowID, @WhoCreated	-- cancellation reason
		--EXEC dbo._C00_SimpleValueIntoField 177899, @NewWritten, @TableRowID, @WhoCreated -- New Written 

		SELECT @ClaimLeadID = dbo.fn_C00_1272_GetClaimLeadFromPolicyLead(@LeadID)

		/*Remove the note added to stop claims while cancellation in progress*/ 
		INSERT INTO @NotesToDelete (leadeventID, done)
		SELECT le.LeadEventID, 0 
		FROM LeadEvent le WITH (NOLOCK)
		WHERE le.LeadID = @ClaimLeadID
		AND le.NoteTypeID = 930
		
		WHILE EXISTS (
						SELECT * 
						FROM @NotesToDelete 
						WHERE done = 0
					 ) 
		BEGIN
		
			SELECT top 1 @NewLeadEventID = LeadeventID 
			FROM @NotesToDelete 
			Where done = 0
		
			EXEC DeleteLeadEvent  @NewLeadEventID, @AqAutomation, '', '4', 0
		
			UPDATE @NotesToDelete 
			SET done = 1 
			WHERE LeadEventID = @NewLeadEventID 
		
		END 
		
		SELECT @VarcharDate = CONVERT(VARCHAR,COALESCE(@CancellationDate,@AdjustmentDate,@GETDATENowLocal),121)

		-- update policy end dates
		EXEC dbo._C00_SimpleValueIntoField 170037, @VarcharDate, @MatterID, @AqAutomation	
		EXEC dbo._C00_SimpleValueIntoField 177287, @VarcharDate, @MatterID, @AqAutomation	-- covered until date	
		SELECT @TableRowID=dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow (@CaseID, @VarcharDate)

		IF @TableRowID <> 0
		BEGIN

			EXEC dbo._C00_SimpleValueIntoField 145664, @VarcharDate, @TableRowID, @AqAutomation		

		END
		
		-- Set the Policy Status field to 'Cancelled'
		EXEC _C00_SimpleValueIntoField 170038,43003,@MatterID
		
		-- send BACS 0C or updated collection details, as necessary
		EXEC _C600_BACS_Send_0C @MatterID
		
	END

	/*GPR 2020-09-18 for PPET-405*/
	IF @EventTypeID IN (150145)/*Policy Cancelled*/
	BEGIN
		
		SELECT TOP (1) @TableRowID= tr.TableRowID 
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.MatterID = @MatterID 
		AND tr.DetailFieldID = 170033 
		ORDER BY tr.TableRowID DESC
		/*Policy History*/

		EXEC [dbo].[_C600_UpdateWellnessHistory_AdjustEndDate] @TableRowID

		/*ALM 2021-02-25 FURKIN-268*/
		SELECT @PurchasedProductID=pp.PurchasedProductID, @AccountID=pp.AccountID 
		FROM MatterDetailValues ppid WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON ppid.ValueInt=pp.PurchasedProductID
		WHERE ppid.MatterID=@MatterID 
		AND ppid.DetailFieldID=177074

		SELECT @AdjustmentDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID)

		EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @AdjustmentDate, @AqAutomation

	END

	IF @EventTypeID IN (150145, 155374, 156748) /*Policy Cancelled (PA, Collections, Claims)*/
	BEGIN
		
		/*Get the latest commission TableRowID - there should be a row for NB + each MTA*/
		SELECT TOP (1) @TableRowID = tr.TableRowID 
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.MatterID = @MatterID 
		AND tr.DetailFieldID = 180121 
		ORDER BY tr.TableRowID DESC
		/*Commission*/
		
		IF @TableRowID IS NOT NULL
		BEGIN
		
			/*Get the cancelation Date for this Policy*/
			SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID)
		
			/*Add the Cancellation Date to the TableRow*/
			EXEC _C00_SimpleValueIntoField 180127,@ValueDate,@TableRowID
			
			/*GPR 2018-08-01 C600, PM163 Find latest Commission Row and update with Commission Clawback Amount*/
			/*Get commission value*/
			SELECT @CommissionValue = dbo.fnGetSimpleDvAsMoney(180129,@TableRowID)
			/*Divide by 365 to get daily value of commission - no requirement to consider leap years*/
			SELECT @DailyCommission = (@CommissionValue / 365)
			/*365 minus days used (diff between start and cancellation date)*/
			
			SELECT @CancellationDate = dbo.fnGetSimpleDvAsDate(180127,@TableRowID) -- CancellationDate
			SELECT @StartDate = dbo.fnGetSimpleDvAsDate(180126,@TableRowID) -- StartDate
			
			SELECT @DaysRemaining = (365 - DATEDIFF(DAY,@StartDate,@CancellationDate))
			/*Daily value times days remaining*/
			SELECT @CommissionClawback = @DailyCommission * @DaysRemaining
			/*Add the commission clawback amount to the TableRow*/
			EXEC _C00_SimpleValueIntoField 180128,@CommissionClawback,@TableRowID

		END
		
	END

	IF @EventTypeID IN (150145) /* Policy cancelled */
	BEGIN
	
		IF NOT EXISTS 
		( 
			SELECT * 
			FROM LeadEvent le WITH (NOLOCK) 
			WHERE le.CaseID = @CaseID 
			AND le.EventDeleted = 0 
			AND le.EventTypeID = 150145 
			AND le.WhenFollowedUp IS NULL 
			AND le.WhenCreated > CONVERT(DATE, @GETDATENowLocal)
		)
		BEGIN
		
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event multiple times in quick succession</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )

		END

		
		SELECT TOP 1 
			@PremiumGross			=	wp.AnnualPriceForRiskGross,			
			@WrittenPremiumID		=	wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.Active = 1
		ORDER BY wp.WrittenPremiumID DESC
		
		SELECT @PurchasedProductID = dbo.fnGetSimpleDvAsInt(177074,@MatterID) 
		/*JEL 2019-02-13 ticket 55222 Update the valid to date as we do this on the Collections version of this process*/ 
		SELECT @CancellationDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID) 
		
		Update PurchasedProduct 
		SET ValidTo = @CancellationDate
		Where PurchasedProductID = @PurchasedProductID
		
		SELECT @VarcharDate = CONVERT(VARCHAR,@CancellationDate,121)
		EXEC dbo._C00_SimpleValueIntoField 170037, @VarcharDate, @MatterID, @WhoCreated /*Policy End Date*/

		/*GPR 2020-12-16 Commented out:*/
		--/*Sum up the ignored rows*/ 		
		--SELECT @Adjustment = SUM(ppps.PaymentGross) - SUM(ppps.TransactionFee) /*GPR 2020-12-15 minus transfee*/, @AdjustmentNet = SUM(ppps.PaymentNet)
		--FROM Purchasedproductpaymentschedule ppps WITH (NOLOCK) 
		--where ppps.PaymentStatusID = 3 
		--and ppps.PurchasedProductPaymentScheduleTypeID = 1 
		--and ppps.purchasedproductID = @PurchasedProductID
		

		--DECLARE @TransactionFee DECIMAL(18,2)
		--SELECT @TransactionFee = SUM(ISNULL(TransactionFee,0.00)) /*GPR 2020-12-15 remove TransactionFee from AdjustmentValueGross for SDPRU-192*/
		--FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		--WHERE ppps.PurchasedProductID = @PurchasedProductID
		--AND ppps.CustomerID = @CustomerID
		--AND ppps.PaymentStatusID IN (2, 6) /* Processed, Paid */

		--/*Find the cancellation adjustment and subtract it from the effect to written*/ 
		--SELECT @Adjustment = ISNULL(@Adjustment,0) - ISNULL(SUM(ppps.PaymentGross) - @TransactionFee /*GPR 2020-12-15*/,0),@AdjustmentNet = ISNULL(@AdjustmentNet,0) - ISNULL(SUM(ppps.PaymentNet) + @TransactionFee /*GPR 2020-12-15*/,0)
		--FROM Purchasedproductpaymentschedule ppps WITH (NOLOCK) 
		--where ppps.PaymentStatusID <> 3 
		--and ppps.PurchasedProductPaymentScheduleTypeID = 7
		--and ppps.purchasedproductID = @PurchasedProductID

		SELECT @PurchasedProductID = dbo.fnGetSimpleDvAsInt(177074,@MatterID) 
		/*JEL 2019-02-13 ticket 55222 Update the valid to date as we do this on the Collections version of this process*/ 
		SELECT @CancellationDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID) 
		
		----/*Sum up the ignored rows*/ 		
		----SELECT @Adjustment = SUM(ppps.PaymentGross) - SUM(ppps.TransactionFee) /*GPR 2020-12-15 minus transfee*/, @AdjustmentNet = SUM(ppps.PaymentNet)
		----FROM Purchasedproductpaymentschedule ppps WITH (NOLOCK) 
		----where ppps.PaymentStatusID = 3 
		----and ppps.PurchasedProductPaymentScheduleTypeID = 1 
		----and ppps.purchasedproductID = @PurchasedProductID

		----SELECT @StateCode = usa.StateCode
		----FROM Customers c WITH (NOLOCK)
		----INNER JOIN StateByZip z WITH (NOLOCK) ON z.Zip = c.Postcode
		----INNER JOIN UnitedStates usa WITH (NOLOCK) ON usa.StateID = z.StateID
		----WHERE c.CustomerID = @CustomerID

		----SELECT @AccountID=AccountID FROM PurchasedProduct WITH (NOLOCK) WHERE PurchasedProductID=@PurchasedProductID

		----SELECT @AccountTypeID = a.AccountTypeID
		----FROM Account a WITH (NOLOCK) 
		----WHERE a.AccountID = @AccountID

		----SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
		----		THEN 69930 /*Direct Debit*/
		----		ELSE 69931 /*Credit Card*/
		----		END

		----SELECT @PaymentMethod = CASE pp.PaymentFrequencyID WHEN 4 THEN 69943 /*Monthly*/ ELSE 69944 /*Annually*/ END
		----FROM PurchasedProduct pp WITH (NOLOCK) WHERE pp.PurchasedProductID=@PurchasedProductID

		----SELECT @TransactionFee = ISNULL(dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod), 0.00)

		----SELECT @CancellationAdjustmentGross = SUM(ppps.PaymentGross * -1) - @TransactionFee, @CancellationAdjustmentNet = SUM(ppps.PaymentNet * -1) - @TransactionFee
		----FROM Purchasedproductpaymentschedule ppps WITH (NOLOCK) 
		----where ppps.PaymentStatusID <> 3 
		----and ppps.PurchasedProductPaymentScheduleTypeID = 7
		----and ppps.purchasedproductID = @PurchasedProductID
	
		----/*Find the cancellation adjustment and subtract it from the effect to written*/ 
		----SELECT @Adjustment = ISNULL(@Adjustment,0.00) + ISNULL(@CancellationAdjustmentGross,0.00) 
		----,@AdjustmentNet = ISNULL(@AdjustmentNet,0.00) + ISNULL(@CancellationAdjustmentNet, 0.00)


		/* GPR 2020-12-17 for SDPRU-192
		1. Identify current purchased product (to get the validity dates)
		2. SUM AdjustmentValueGross from WrittenPremium where the ValidFrom is greater than or equal to your PurchasedProduct FROM Date (can’t rely on ValidTo because we might have already ended the PurchasedProduct), don’t forget to exclude the latest Cancellation row (in-flight), this output should = PolicyPremium
		3. Divide by 365 (Aquarium always works on a 365 day Policy term) = DailyPremium
		4. Lookup Policy StartDate and EndDate (Cancellation Date), the DIFF in days is days used
		5. Subtract (DaysUsed * DailyPremium) from PolicyPremium
		6. Now multiply by -1 to make negative
		*/
		DECLARE @ValidFrom DATE
			,@UsedProduct INT
			,@UnusedProduct INT
			,@TotalPremium DECIMAL(18,2)
			,@TotalPremiumNet DECIMAL(18,2)
			,@TotalPremiumTaxLevel1 DECIMAL(18,4)
			,@TotalPremiumTaxLevel2 DECIMAL(18,2)
			,@DailyPremium DECIMAL(18,6)
			,@DailyPremiumNet DECIMAL(18,6)
			,@DailyPremiumTaxLevel1 DECIMAL(18,6) /*ALM 2021-03-01 Updated from 18,2 FURKIN-326*/ /*Set to 18,6*/
			,@DailyPremiumTaxLevel2 DECIMAL(18,2)
			,@CurrentNationalTaxRate DECIMAL(18,2)
			,@CurrentLocalTaxRate DECIMAL(18,2)
			,@NationalTaxGrossMultiplier DECIMAL(18,2)
			,@LocalTaxGrossMultiplier DECIMAL(18,2)
			,@AdjustmentNationalTax DECIMAL(18,2)
			,@AdjustmentLocalTax MONEY
			,@NetPlusNationalTax MONEY
			,@CommissionPercentageValue DECIMAL(18,2)
			,@AdjustmentBrokerCommission MONEY
			,@AdjustmentUWCommission MONEY
			,@AffinityID INT

		SELECT @ValidFrom = p.ValidFrom ,@UsedProduct = DATEDIFF(DAY,@ValidFrom,@CancellationDate)
		FROM PurchasedProduct p WITH (NOLOCK) 
		WHERE p.PurchasedProductID = @PurchasedProductID

		SELECT TOP(1) @WrittenPremiumID = wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4 /*Cancellation*/
		ORDER BY wp.WrittenPremiumID DESC

		SELECT @TotalPremium = SUM(AdjustmentValueGross), @TotalPremiumNET = SUM(AdjustmentValueNet), @TotalPremiumTaxLevel1 = SUM(AdjustmentValueNationalTax), @TotalPremiumTaxLevel2 = SUM(AdjustmentValueLocalTax)
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.WrittenPremiumID <> @WrittenPremiumID
		AND wp.ValidFrom >= @ValidFrom

		SELECT @DailyPremium = @TotalPremium / 365
		SELECT @DailyPremiumNet = @TotalPremiumNet / 365
		SELECT @DailyPremiumTaxLevel1 = @TotalPremiumTaxLevel1 / 365
		SELECT @DailyPremiumTaxLevel2 = @TotalPremiumTaxLevel2 / 365

		SELECT @UnusedProduct = 365 - @UsedProduct

		SELECT @Adjustment = @DailyPremium * @UnusedProduct
		SELECT @AdjustmentNet = @DailyPremiumNet * @UnusedProduct
		SELECT @AdjustmentNationalTax = ISNULL(@DailyPremiumTaxLevel1,0.00) * @UnusedProduct
		SELECT @AdjustmentLocalTax = ISNULL(@DailyPremiumTaxLevel2,0.00) * @UnusedProduct

		/*GPR/ALM*/EXEC _C00_LogIt 'Info','_SAE_Cancellation','@UnusedProduct',@UnusedProduct,58550
		/*GPR/ALM*/EXEC _C00_LogIt 'Info','_SAE_Cancellation','@DailyPremiumTaxLevel1',@DailyPremiumTaxLevel1,58550
		/*GPR/ALM*/EXEC _C00_LogIt 'Info','_SAE_Cancellation','@AdjustmentNationalTax',@AdjustmentNationalTax,58550

		SET @AdjustmentNationalTax = ROUND(@AdjustmentNationalTax, 2, 1) /*ALM 2021-03-01 FURKIN-326*/

		/*GPR/ALM*/EXEC _C00_LogIt 'Info','_SAE_Cancellation','@AdjustmentNationalTax',@AdjustmentNationalTax,58550

		SELECT @Adjustment = @Adjustment * -1
		SELECT @AdjustmentNet = @AdjustmentNet * -1
		SELECT @AdjustmentNationalTax = @AdjustmentNationalTax * -1 /*ALM 2021-03-02 FURKIN-326*/

		/*GPR/ALM*/EXEC _C00_LogIt 'Info','_SAE_Cancellation','@AdjustmentNationalTax',@AdjustmentNationalTax,58550

		/*Write the cancellation reason*/ 
		SELECT @DetailValue = dbo.fnGetSimpleDVLuli(170054,@MatterID) 

		
		--/*GPR 2020-04-25 - At time of coding, ACT,AU has a duty of 0.00 so the gross multiplier will be 1.00 when ISNULL to 0.00*/
		--SELECT @CurrentNationalTaxRate = ISNULL(wp.AnnualNationalTaxRate,0.00), @CurrentLocalTaxRate = ISNULL(wp.AnnualLocalTaxRate,0.00)
		--FROM WrittenPremium wp WITH (NOLOCK)
		--WHERE wp.WrittenPremiumID = @WrittenPremiumID 
		
		--IF @CurrentNationalTaxRate <> 0 
		--BEGIN 

		--	SELECT @NationalTaxGrossMultiplier = (@CurrentNationalTaxRate / 100.00)+1
		--	SELECT @LocalTaxGrossMultiplier = (@CurrentLocalTaxRate / 100.00)+1 
		--	/*NationalTax is applied to the Net value*/
		--	SELECT @AdjustmentNationalTax = @AdjustmentNet * @NationalTaxGrossMultiplier - @AdjustmentNet
		--	SELECT @AdjustmentNationalTax = @AdjustmentNationalTax


		--	/*LocalTax/StampDuty is applied to the output value of Net + NationalTax */
		--	SELECT @NetPlusNationalTax = @AdjustmentNet * @NationalTaxGrossMultiplier
		--	SELECT @AdjustmentLocalTax = @NetPlusNationalTax * @LocalTaxGrossMultiplier - @NetPlusNationalTax
		--	SELECT @AdjustmentLocalTax = @AdjustmentLocalTax

		--END

		/*GPR 2020-05-12 for JIRA SDAAG-21 | make sure we total up*/
		--SELECT @Adjustment = @AdjustmentNet + ISNULL(@AdjustmentLocalTax,0.00) + ISNULL(@AdjustmentNationalTax,0.00)

		/*ALM 2021-02-05 FURKIN-212*/
		SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144, @CustomerID)

		/*GPR 2020-04-27 - Commission: Broker and Underwriter*/
		/*GPR 2021-04-13 | Updated for FURKIN-493 - Adjustment Broker/UW Commission Values*/
		SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsMoney(313814,@AffinityID) /*Underwriter Commission Percentage Value*/
		SELECT @AdjustmentBrokerCommission /*Broker Commission Value*/ = (@AdjustmentNet * ((100.00 - @CommissionPercentageValue)/100.00))
		SELECT @AdjustmentUWCommission /*UW Commission Value*/ = (@AdjustmentNet - (@AdjustmentNet * ((100.00 - @CommissionPercentageValue)/100.00)))
	
		/*JEL 2020-04-24 | AdjustmentGross, AdjustmentNet, CancellationReason */
		/*GPR 2020-04-25 | NationalTax, LocalTax */
		/*GPR 2021-04-13 | Updated for FURKIN-493 - Adjustment Broker/UW Commission Values*/
		UPDATE WrittenPremium
		SET AdjustmentValueGross = @Adjustment , AdjustmentValueNet = @AdjustmentNet, CancellationReason = @DetailValue, AdjustmentValueNationalTax = ISNULL(@AdjustmentNationalTax,0.00), AdjustmentValueLocalTax = ISNULL(@AdjustmentLocalTax,0.00), AdjustmentBrokerCommission = @AdjustmentBrokerCommission, AdjustmentUnderwriterCommission = @AdjustmentUWCommission
		WHERE WrittenPremiumID = @WrittenPremiumID
			
		/*Have we already prepped a renewal for this policy?*/ 
		IF EXISTS (
			SELECT * 
			FROM PurchasedProduct p with (NOLOCK) 
			INNER JOIN MatterDetailValues mdv with (NOLOCK) on mdv.MatterID = p.ObjectID
			Where p.ObjectID = @MatterID 
			AND mdv.DetailFieldID = 170055 /*Cancel Date*/ 
			AND p.ValidFrom >= mdv.ValueDate
			AND p.ValidFrom > CAST(@GETDATENowLocal as DATE) 
			/*JEL 2019-07-04 Added for CR44, as we now need to allow future cancellations if before policy start, ensure we have an earlier PP before bothering woth renewal adjustments*/ 
			AND EXISTS (
				SELECT * FROM PurchasedProduct pp with (NOLOCK) 
				WHERE pp.ObjectID = @MatterID 
				AND pp.PurchasedProductID < p.PurchasedProductID
				AND pp.ValidFrom < p.ValidFrom
			)
		) 
		BEGIN 

			SELECT @PurchasedProductID = NULL 
			SELECT @PurchasedProductID = p.PurchasedProductID, @CancellationDate = mdv.ValueDate, @AccountID = p.AccountID 
			FROM PurchasedProduct p with (NOLOCK) 
			INNER JOIN MatterDetailValues mdv with (NOLOCK) on mdv.MatterID = p.ObjectID
			Where p.ObjectID = @MatterID 
			AND mdv.DetailFieldID = 170055 /*Cancel Date*/ 
			AND p.ValidFrom >= mdv.ValueDate
			AND p.ValidFrom > CAST(@GETDATENowLocal as DATE) 
	
			/*Updare The PurchasedProduct's end date and set it's schedules to ignore*/ 
			UPDATE PurchasedProduct 
			SET ValidTo = @CancellationDate 
			WHERE PurchasedProductID = @PurchasedProductID 

			UPDATE PurchasedProductPaymentSchedule 
			SET PaymentStatusID = 3 
			WHERE PurchasedProductID = @PurchasedProductID 
			AND PaymentStatusID IN (1,5)

			/*Deal with the WP*/
			/*find the renewal Table Row*/ 
			SELECT @RenewalTableRowID = tr.tableRowID, @ValueDate = frm.ValueDate 
			FROM TableRows tr WITH (NOLOCK)
			INNER JOIN TableDetailValues adj with (NOLOCK) on adj.TableROwID = tr.TableRowID AND adj.DetailFIeldID = 175398 /*Adjustment Type*/ 
			INNER JOIN tabledetailValues frm with (NOLOCK) on frm.TableRowID = tr.TableRowID AND frm.DetailFieldID = 175346 /*Adjustment From Date*/
			WHERE tr.MatterID = @MatterID 
			AND tr.DetailFieldID = 175336
			AND adj.ValueINT = 74528 /*Renewal*/ 
			AND frm.ValueDate >= @CancellationDate	

			PRINT @RenewalTableROwID 
			/*Set the to Date on renewal Table row*/ 
			EXEC _C00_SimpleValueINTOField 175347,@ValueDate,@RenewalTableRowID 

			EXEC TableRows_Insert  @TableRowID OUTPUT, @ClientID,NULL,@MatterID,175336,19011,1,1,NULL,NULL,NULL,NULL,@RenewalTableRowID

			PRINT @TableRowID 

			/*Insert the same values as the renewal row except for the premium and commision*/ 
			INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,MatterID,ClientID)
			SELECT @TableRowID, tdv.DetailFieldID,tdv.DetailValue, tdv.MatterID,tdv.ClientID 
			FROM TableDetailValues tdv with (NOLOCK) 
			WHERE tdv.TableRowID = @RenewalTableRowID 
			AND DetailFieldID NOT IN (180170,180171,175397,177899)

			/*Set the adjustment amount to be negative*/

			SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(175397,@RenewalTableRowID) * -1) 
			EXEC _C00_SimpleValueIntoField 175397,@ValueMoney,@TableRowID

			SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(177899,@RenewalTableRowID) * -1) 
			EXEC _C00_SimpleValueIntoField 177899,@ValueMoney,@TableRowID

			/*If they had already paid by card (no mandates can be in place), add the refund*/ 

			SELECT @ClientAccountID = a.ClientAccountID 
			FROM Account a with (NOLOCK) 
			WHERE a.AccountID = @AccountID 

			IF EXISTS ( 
				SELECT * 
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) /* SLACKY-PERF-20190705 */
				WHERE ppps.PurchasedProductID = @PurchasedProductID
				AND ppps.PaymentStatusID = 6 /*Paid*/
			)
			BEGIN

				SELECT @PremiumNetLogged = SUM(ppps.PaymentNet), @PremiumTaxLogged = SUM(ppps.PaymentVAT), @PremiumGrossLogged = SUM(ppps.PaymentGross)
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) /* SLACKY-PERF-20190705 */
				WHERE ppps.PurchasedProductID = @PurchasedProductID
				AND ppps.PaymentStatusID = 6 /*Paid*/

				/*Flip all values negative for TR proc.*/
				SELECT @PremiumNetLogged = -@PremiumNetLogged,
						@PremiumTaxLogged = -@PremiumTaxLogged,
						@PremiumGrossLogged = -@PremiumGrossLogged

				/*ALM 2021-02-25 Updated @FinalPaymentAmountNet, @FinalPaymentAmountTAX, @FinalPaymentAmountGross to @PremiumNetLogged, @PremiumTaxLogged, @PremiumGrossLogged*/
				EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, 
								@PremiumNetLogged, @PremiumTaxLogged, @PremiumGrossLogged,
								1 /*Premium*/, @GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal, 7, 
								@WhoCreated, 0, @PurchasedProductPaymentScheduleParentID, @TransactionFeeLogged, '_SAE_Cancellation_5'

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				SELECT	@EnrollmentFeeRefundNet = SUM(s.PaymentNet)*-1.00,
						@EnrollmentFeeRefundTax = SUM(s.PaymentVat)*-1.00,
						@EnrollmentFeeRefundGross = SUM(s.PaymentGross)*-1.00
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
				WHERE s.PurchasedProductID = @PurchasedProductID
				AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
				AND s.PaymentStatusID = 6 /*Paid only!*/

				PRINT '@AccountTypeID'
				PRINT @AccountTypeID

				EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'Type', 'Card', @WhoCreated
				EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', '@EnrollmentFeeRefundGross', @EnrollmentFeeRefundGross, @WhoCreated

				PRINT '@EnrollmentFeeRefundGross'
				PRINT @EnrollmentFeeRefundGross

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				IF @EnrollmentFeeRefundGross <> 0.00
				BEGIN

					/*Make sure we havent queued this up already*/
					IF NOT EXISTS (
						SELECT *
						FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
						WHERE s.PurchasedProductID = @PurchasedProductID
						AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
						AND s.PaymentStatusID = 1 /*New*/
					)
					BEGIN

						EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'PurchasedProductPaymentSchedule', 'NoNew', @WhoCreated
		
						PRINT 'Create Enrollment Fee Refund'

						/*2021-01-22 ACE call transactional refund proc..*/
						EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, @EnrollmentFeeRefundNet, @EnrollmentFeeRefundTax, @EnrollmentFeeRefundGross,
										1 /*Premium*/, @GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal, 5, @WhoCreated, 0, NULL, 0, '_SAE_Cancellation_6'

					END

				END
				/*End add enrollment fee*/

				/*
				INSERT INTO PurchasedProductPaymentSchedule([ClientID], [CustomerID], [AccountID], [PurchasedProductID], [ActualCollectionDate], [CoverFrom], [CoverTo], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], [PurchasedProductPaymentScheduleTypeID], [PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID])
				OUTPUT inserted.PurchasedProductPaymentScheduleID INTO @InsertedPPPS (PPPSID)
				SELECT @ClientID, @CustomerID, @AccountID, @PurchasedProductID, @GETDATENowLocal, 
						@GETDATENowLocal, @GETDATENowLocal, @GETDATENowLocal,
						-@PremiumNetLogged , 
						-@PremiumTaxLogged, 
						-@PremiumGrossLogged, 
						1, NULL AS [CustomerLedgerID], @GETDATENowLocal AS [ReconciledDate], NULL AS [CustomerPaymentScheduleID], 
						@WhoCreated, @GETDATENowLocal, NULL AS [PurchasedProductPaymentScheduleParentID], 7 AS [PurchasedProductPaymentScheduleTypeID], 
						NULL AS [PaymentGroupedIntoID], NULL AS [ContraCustomerLedgerID], @ClientAccountID, @WhoCreated, 
						dbo.fn_GetDate_Local () AS [WhenModified], NULL AS [SourceID]

				SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()

				INSERT INTO CustomerPaymentSchedule ([ClientID], [CustomerID], [AccountID], [ActualCollectionDate], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [WhoCreated], [WhenCreated], [SourceID], [RelatedObjectID], [RelatedObjectTypeID], [ClientAccountID], [WhoModified], [WhenModified])
				OUTPUT inserted.CustomerPaymentScheduleID, inserted.SourceID INTO @InsertedCPS(CustomerPaymentScheduleID, PurchasedProductPaymentScheduleID)
				SELECT pps.ClientID, pps.CustomerID, pps.AccountID, pps.ActualCollectionDate, pps.[PaymentDate], pps.[PaymentNet], pps.[PaymentVAT], pps.[PaymentGross], pps.[PaymentStatusID], pps.[CustomerLedgerID], pps.[ReconciledDate], pps.[WhoCreated], pps.[WhenCreated], pps.PurchasedProductPaymentScheduleID AS [SourceID], @MatterID, 2, pps.[ClientAccountID], pps.[WhoModified], pps.[WhenModified]
				FROM @InsertedPPPS ipps
				INNER JOIN PurchasedProductPaymentSchedule pps ON pps.PurchasedProductPaymentScheduleID = ipps.PPPSID

				SELECT @CustomerPaymentScheduleID = SCOPE_IDENTITY()

				UPDATE pps
				SET CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
				FROM PurchasedProductPaymentSchedule pps 
				INNER JOIN @InsertedCPS p ON p.PurchasedProductPaymentScheduleID = pps.PurchasedProductPaymentScheduleID
				*/

			END

		END 

		/*ALM 2020-07-02 AAG-969*/
		SELECT @WellnessOneVisionID = OneVisionID
		FROM OneVision ov WITH (NOLOCK) 
		WHERE ov.PAMatterID = @MatterID
		AND ov.PolicyTermType = 'Wellness'

		SELECT @ExamFeeOneVisionID = OneVisionID
		FROM OneVision ov WITH (NOLOCK) 
		WHERE ov.PAMatterID = @MatterID
		AND ov.PolicyTermType = 'ExamFee'

		SELECT @PolicyOneVisionID = OneVisionID
		FROM OneVision ov WITH (NOLOCK) 
		WHERE ov.PAMatterID = @MatterID
		AND ov.PolicyTermType = 'Policy'

		IF @WellnessOneVisionID IS NOT NULL 
		BEGIN
			
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID

		END

		IF @ExamFeeOneVisionID IS NOT NULL 
		BEGIN
			
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID

		END

		IF @PolicyOneVisionID IS NOT NULL 
		BEGIN 

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'RollbackPolicyTerms', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID
			
			WAITFOR DELAY '00:00:05'

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'RollbackPolicy', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID

		END

	END

	/*GPR 2019-12-02 Premium Funding - Cancellation*/
	IF @EventTypeID = 150145 /*Policy cancelled*/
	BEGIN

		IF dbo.fnGetSimpleDvAsInt(170114,@MatterID) = 76618 /*Premium Funding*/
		BEGIN
		
			SELECT @PaymentTypeID = 11 /*Premium Funding*/

			SELECT TOP 1 @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
			FROM CustomerPaymentSchedule c WITH (NOLOCK)
			INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID 
			INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = p.PurchasedProductID
			WHERE c.CustomerID = @CustomerID
			AND c.PaymentStatusID = 1
			AND p.PurchasedProductPaymentScheduleTypeID IN  (7) /*Cancellation Adjustment*/ 
			AND pp.ObjectID = @MatterID 

			IF @CustomerPaymentScheduleID IS NOT NULL
			BEGIN

				EXEC Billing__CreatePaymentForPremiumFunding @CustomerPaymentScheduleID, @PaymentTypeID, 'Premium Funding Payment', @LeadEventID, @WhoCreated /*LPC-167 / LPC-68*/
		
			END

		END

	END

	/*ALM 2021-03-29 FURKIN-424 Move Projected Cancellation Date to Cancellation Date field*/
	IF @EventTypeID = 155374 
	BEGIN 

		SELECT @ProjectedCancellationDate = dbo.fnGetSimpleDvAsDate (315274, @MatterID)

		EXEC _C00_SimpleValueIntoField 170055, @ProjectedCancellationDate, @MatterID 

	END

	IF @EventTypeID = 155374 /*Policy cancelled by collections process*/
	BEGIN
		/*2021-04-08 GPR/ALM for FURKIN-474 - Update CaseStatus on Collections LeadType*/		
		SELECT @CollectionsMatterID = [dbo].[fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin](@MatterID)

		SELECT @NewLeadEventID = c.LatestNonNoteLeadEventID, @LeadID = m.LeadID, @CaseID = c.CaseID, @ValueInt = le.EventTypeID FROM Matter m WITH (NOLOCK)
		INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID
		INNER JOIN LeadEvent le WITH (NOLOCK) on le.LeadEventID = c.LatestNonNoteLeadEventID
		WHERE m.MatterID = @CollectionsMatterID
               
		EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @NewLeadEventID, 160534, @WhoCreated, 1
	
	END	


/*2021-04-09 GPR/ALM*/
	/*FURKIN-425 - Auto Cancel - Write Off Adjustment Value*/
	/*Failed - Written Off (PaymentStatusID: 11)*/
	IF @EventTypeID = 155374 -- Policy cancelled by collections process
	BEGIN

		SELECT @PurchasedProductID = p.PurchasedProductID
		FROM PurchasedProduct p WITH (NOLOCK) 
		WHERE p.ObjectID = @MatterID

		SELECT @PurchasedProductPaymentScheduleID = (SELECT TOP(1) ppps.PurchasedProductPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PurchasedProductPaymentScheduleTypeID = 1 -- Scheduled
		AND PaymentStatusID = 4 -- Failed
		ORDER BY 1 DESC)

		SELECT @CustomerPaymentScheduleID = CustomerPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		UPDATE ppps
		SET PaymentStatusID = 11
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		UPDATE cps
		SET PaymentStatusID = 11
		FROM CustomerPaymentSchedule cps WITH (NOLOCK)
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

	END

	/*2021-04-09 GPR/ALM*/
	/*FURKIN-429 - [Pet Deceased][Ensure uncollected premiums are written off]*/
	/*Written Off (PaymentStatusID: 10)*/
	IF dbo.fnGetSimpleDvAsInt(170054, @MatterID) = 69911 -- Cancellation Reason = Pet deceased
	BEGIN
		-- Find PPPS/CPS record for Cancellation Adjustment and set PaymentStatusID to 10 (Written Off)
		SELECT @PurchasedProductID = p.PurchasedProductID
		FROM PurchasedProduct p WITH (NOLOCK) 
		WHERE p.ObjectID = @MatterID

		SELECT @PurchasedProductPaymentScheduleID = (SELECT TOP(1) ppps.PurchasedProductPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PurchasedProductPaymentScheduleTypeID = 7 -- Cancellation Adjustment
		AND PaymentStatusID = 1 -- New
		AND ppps.PaymentGross > 0.00
		ORDER BY 1 DESC)

		SELECT @CustomerPaymentScheduleID = CustomerPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
		
		UPDATE ppps
		SET PaymentStatusID = 10
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		UPDATE cps
		SET PaymentStatusID = 10
		FROM CustomerPaymentSchedule cps WITH (NOLOCK)
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

	END
	

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Cancellation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Cancellation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Cancellation] TO [sp_executeall]
GO
