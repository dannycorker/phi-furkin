SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_Delete]
(

	@SqlQueryCriteriaSetMemberID int   
)
AS


				DELETE FROM [dbo].[SqlQueryCriteriaSetMember] WITH (ROWLOCK) 
				WHERE
					[SqlQueryCriteriaSetMemberID] = @SqlQueryCriteriaSetMemberID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Delete] TO [sp_executeall]
GO
