SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderKeywordMatchType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderKeywordMatchType_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderKeywordMatchTypeID int   = null ,

	@MatchTypeName varchar (250)  = null ,

	@MatchTypeDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderKeywordMatchTypeID]
	, [MatchTypeName]
	, [MatchTypeDescription]
    FROM
	[dbo].[DataLoaderKeywordMatchType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID OR @DataLoaderKeywordMatchTypeID IS NULL)
	AND ([MatchTypeName] = @MatchTypeName OR @MatchTypeName IS NULL)
	AND ([MatchTypeDescription] = @MatchTypeDescription OR @MatchTypeDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderKeywordMatchTypeID]
	, [MatchTypeName]
	, [MatchTypeDescription]
    FROM
	[dbo].[DataLoaderKeywordMatchType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID AND @DataLoaderKeywordMatchTypeID is not null)
	OR ([MatchTypeName] = @MatchTypeName AND @MatchTypeName is not null)
	OR ([MatchTypeDescription] = @MatchTypeDescription AND @MatchTypeDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderKeywordMatchType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Find] TO [sp_executeall]
GO
