SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2013-02-19
-- Description:	Reverse (as much as possible of) DeleteLeadEvent
-- =============================================
CREATE PROCEDURE [dbo].[UndeleteLeadEvent]
	@LeadEventID INT, 
	@ClientID INT,
	@SuppressMessages BIT = 0
AS
BEGIN
	/*
		Undelete a LeadEvent that was deleted in error.
		
		1) Mark the LeadEvent record as not deleted
		2) LeadEventThreadCompletion needs 2 fixes:
			a) Follow up the old record
			b) Create the new record
		3) Cases needs the appropriate LatestInProc etc values to be set
		
		This script does not do:
			SAE
			EventTypeAutomatedEvent
			EventTypeAutomatedWorkflow
			Case Status
			Lead Status
			Close off multiple LeadEventThreadCompletion records (a JumpTo event applied by the app or scheduler might have done this originally).
	*/
	
	SET NOCOUNT ON;
	
	/* Declare local variables */
	DECLARE 
	@EventTypeID INT,
	@LeadID INT,
	@CaseID INT, 
	@EventDeleted BIT,
	@EventSubtypeID INT,
	@TestClientID INT,
	@WhenCreated DATETIME
	
	/* Get details of the deleted LeadEvent record */
	SELECT @EventTypeID = le.EventTypeID, 
	@LeadID = le.LeadID, 
	@CaseID = le.CaseID, 
	@TestClientID = le.ClientID, 
	@EventDeleted = le.EventDeleted,
	@EventSubtypeID = et.EventSubtypeID,
	@WhenCreated = le.WhenCreated
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
	WHERE le.LeadEventID = @LeadEventID 
	
	/* Show variables if running in user-friendly mode */
	IF @SuppressMessages = 0
	BEGIN
		SELECT @EventTypeID as EventTypeID, 
		@LeadID as LeadID, 
		@CaseID as CaseID, 
		@LeadEventID as LeadEventID,
		@ClientID as ClientID,
		@EventDeleted as EventDeleted,
		@EventSubtypeID as EventSubtypeID,
		@WhenCreated as WhenCreated
	END
	
	/* Check that the event is actually deleted */
	IF @EventDeleted = 1 AND @ClientID = @TestClientID
	BEGIN
		
		/* Change the LeadEvent deleted status first of all. Keep the comments for posterity and checking. */
		UPDATE dbo.LeadEvent 
		SET EventDeleted = 0, WhoDeleted = NULL /*, DeletionComments = NULL */
		WHERE LeadEventID = @LeadEventID
		
		/* Use EventChoice to deduce which thread should be closed off again */
		UPDATE dbo.LeadEventThreadCompletion 
		SET ToLeadEventID = @LeadEventID, 
		ToEventTypeID = @EventTypeID 
		FROM dbo.LeadEventThreadCompletion letc 
		INNER JOIN dbo.EventChoice ec ON ec.EventTypeID = letc.FromEventTypeID
			AND ec.ThreadNumber = letc.ThreadNumberRequired
			AND ec.NextEventTypeID = @EventTypeID
		WHERE letc.CaseID = @CaseID 
		AND letc.ToLeadEventID IS NULL	/* Don't keep updating records that are already complete. */
		AND ec.ClientID NOT IN (183)	/* SB 2011-03-24 Prevent this running for BM (183) where we have multiple paths with the same event types. JWG 2011-04-14 Ditto for Redress Claims (150, 197) */

		/* Create new threads if required */
		INSERT INTO LeadEventThreadCompletion (ClientID, LeadID, CaseID, FromEventTypeID, FromLeadEventID, ThreadNumberRequired, ToEventTypeID, ToLeadEventID)
		SELECT DISTINCT ec.ClientID, @LeadID, @CaseID, ec.EventTypeID, @LeadEventID, ec.ThreadNumber, NULL, NULL  
		FROM EventChoice ec WITH (NOLOCK) 
		WHERE ec.EventTypeID = @EventTypeID 
		AND ec.EscalationEvent = 0 -- Don't create threads for escalation event choices.  These are usually thread 1 anyway, as it happens.
		AND NOT EXISTS (
			SELECT * 
			FROM LeadEventThreadCompletion WITH (NOLOCK) 
			WHERE CaseID = @CaseID
			AND FromLeadEventID = @LeadEventID
		)
		
		/* Set LatestInProcessLeadEventID etc for the Case */
		UPDATE dbo.Cases 
		SET LatestLeadEventID = @LeadEventID, 
		LatestInProcessLeadEventID = @LeadEventID,
		LatestNonNoteLeadEventID = @LeadEventID, 
		ProcessStartLeadEventID = CASE 
										/* Process start added for the first time */
										WHEN ProcessStartLeadEventID IS NULL AND @EventSubtypeID = 10 AND @EventDeleted = 0 THEN @LeadEventID 
										
										/* No change to the existing value */
										ELSE ProcessStartLeadEventID 
								  END 
		WHERE CaseID = @CaseID
		
		/* A user or batch job might have added the deleted event closing ALL threads at the time.  Allow the current user to re-create this effect if required. */
		IF @SuppressMessages = 0
		BEGIN
			
			/* LeadEventThreadCompletion closure */
			SELECT 'UPDATE dbo.LeadEventThreadCompletion SET ToLeadEventID = ' + CAST(@LeadEventID AS VARCHAR) + ', ToEventTypeID = ' + CAST(@EventTypeID AS VARCHAR) + ' WHERE LeadEventThreadCompletionID = ' + CAST(letc.LeadEventThreadCompletionID AS VARCHAR) AS [JumpTo Helpers If Applicable]
			FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK) 
			WHERE letc.CaseID = @CaseID 
			AND letc.ToLeadEventID IS NULL 
			AND letc.FromLeadEventID <> @LeadEventID
			UNION ALL
			/* LeadEvent followup */
			SELECT 'UPDATE dbo.LeadEvent SET NextEventID = ' + CAST(@LeadEventID AS VARCHAR) + ', WhenFollowedUp = ''' + CONVERT(VARCHAR, @WhenCreated, 120) + ''' WHERE LeadEventID = ' + CAST(letc.FromLeadEventID AS VARCHAR) 
			FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK) 
			WHERE letc.CaseID = @CaseID 
			AND letc.ToLeadEventID IS NULL
			AND letc.FromLeadEventID <> @LeadEventID
			
			/* Show threads and events as they stand now */
			EXEC dbo.letc @LeadID, @CaseID
			
		END
		
	END
	ELSE
	BEGIN
		IF @SuppressMessages = 0
		BEGIN
			SELECT 'EventDeleted = ' + CAST(@EventDeleted AS VARCHAR) + ', @ClientID = ' + CAST(@ClientID AS VARCHAR) + ', TestClientID = ' + CAST(@TestClientID AS VARCHAR) AS [User Error]
		END
	END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[UndeleteLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UndeleteLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UndeleteLeadEvent] TO [sp_executeall]
GO
