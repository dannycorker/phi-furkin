SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FunctionType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_GetByParentFunctionTypeID]
(

	@ParentFunctionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FunctionTypeID],
					[ModuleID],
					[ParentFunctionTypeID],
					[FunctionTypeName],
					[Level],
					[IsVirtual],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[FunctionType] WITH (NOLOCK) 
				WHERE
					[ParentFunctionTypeID] = @ParentFunctionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_GetByParentFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_GetByParentFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_GetByParentFunctionTypeID] TO [sp_executeall]
GO
