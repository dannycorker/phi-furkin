SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReportTableRelationships table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTableRelationships_Find]
(

	@SearchUsingOR bit   = null ,

	@ReportTableRelationshipID int   = null ,

	@TableFrom varchar (50)  = null ,

	@TableTo varchar (50)  = null ,

	@TableJoin varchar (2000)  = null ,

	@Description varchar (250)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReportTableRelationshipID]
	, [TableFrom]
	, [TableTo]
	, [TableJoin]
	, [Description]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ReportTableRelationships] WITH (NOLOCK) 
    WHERE 
	 ([ReportTableRelationshipID] = @ReportTableRelationshipID OR @ReportTableRelationshipID IS NULL)
	AND ([TableFrom] = @TableFrom OR @TableFrom IS NULL)
	AND ([TableTo] = @TableTo OR @TableTo IS NULL)
	AND ([TableJoin] = @TableJoin OR @TableJoin IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReportTableRelationshipID]
	, [TableFrom]
	, [TableTo]
	, [TableJoin]
	, [Description]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ReportTableRelationships] WITH (NOLOCK) 
    WHERE 
	 ([ReportTableRelationshipID] = @ReportTableRelationshipID AND @ReportTableRelationshipID is not null)
	OR ([TableFrom] = @TableFrom AND @TableFrom is not null)
	OR ([TableTo] = @TableTo AND @TableTo is not null)
	OR ([TableJoin] = @TableJoin AND @TableJoin is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTableRelationships_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Find] TO [sp_executeall]
GO
