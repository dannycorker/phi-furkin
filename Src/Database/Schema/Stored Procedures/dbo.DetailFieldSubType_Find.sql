SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailFieldSubType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldSubType_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailFieldSubTypeID tinyint   = null ,

	@DetailFieldSubTypeName varchar (200)  = null ,

	@DetailFieldSubTypeDescription varchar (500)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailFieldSubTypeID]
	, [DetailFieldSubTypeName]
	, [DetailFieldSubTypeDescription]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[DetailFieldSubType] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldSubTypeID] = @DetailFieldSubTypeID OR @DetailFieldSubTypeID IS NULL)
	AND ([DetailFieldSubTypeName] = @DetailFieldSubTypeName OR @DetailFieldSubTypeName IS NULL)
	AND ([DetailFieldSubTypeDescription] = @DetailFieldSubTypeDescription OR @DetailFieldSubTypeDescription IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailFieldSubTypeID]
	, [DetailFieldSubTypeName]
	, [DetailFieldSubTypeDescription]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[DetailFieldSubType] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldSubTypeID] = @DetailFieldSubTypeID AND @DetailFieldSubTypeID is not null)
	OR ([DetailFieldSubTypeName] = @DetailFieldSubTypeName AND @DetailFieldSubTypeName is not null)
	OR ([DetailFieldSubTypeDescription] = @DetailFieldSubTypeDescription AND @DetailFieldSubTypeDescription is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldSubType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Find] TO [sp_executeall]
GO
