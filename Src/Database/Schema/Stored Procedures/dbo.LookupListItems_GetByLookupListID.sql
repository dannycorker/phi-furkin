SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupListItems table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_GetByLookupListID]
(

	@LookupListID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LookupListItemID],
					[LookupListID],
					[ItemValue],
					[ClientID],
					[Enabled],
					[SortOrder],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID],
					[IsShared]
				FROM
					[dbo].[LookupListItems] WITH (NOLOCK) 
				WHERE
					[LookupListID] = @LookupListID
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetByLookupListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_GetByLookupListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetByLookupListID] TO [sp_executeall]
GO
