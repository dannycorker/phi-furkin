SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ClientPaymentGateway__GetByClientPaymentGatewayID]

	@ClientPaymentGatewayID INT

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @RowCount INT

	SELECT * FROM ClientPaymentGateway WITH (NOLOCK) 
	WHERE ClientPaymentGatewayID = @ClientPaymentGatewayID    

	SELECT @RowCount = @@ROWCOUNT

	EXEC _C00_LogIt 'Debug','ClientPaymentGateway__GetByClientPaymentGatewayID', '@ClientPaymentGatewayID = ', @ClientPaymentGatewayID, @RowCount
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByClientPaymentGatewayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPaymentGateway__GetByClientPaymentGatewayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByClientPaymentGatewayID] TO [sp_executeall]
GO
