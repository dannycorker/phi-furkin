SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderMap table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap__GetUndeletedByClientID]
(
	@ClientID int   
)
AS
				SET ANSI_NULLS OFF
	
	SELECT		dlm.[DataLoaderMapID],
				dlm.[ClientID],
				dlm.[MapName],
				dlm.[MapDescription],
				dlm.[WhenCreated],
				cpc.[UserName] AS [CreatedBy],
				dlm.[LastUpdated],
				cpu.[UserName] AS [UpdatedBy],
				dlm.[EnabledForUse],
				dlm.[Deleted],
				lt.[LeadTypeName] AS LeadTypeName,
				dlm.[UseDefaultLeadForCustomer],
				dlm.[UseDefaultCaseForLead],
				dlm.[UseDefaultMatterForCase]
	FROM		[dbo].[DataLoaderMap] dlm
	INNER JOIN	ClientPersonnel cpc ON cpc.ClientPersonnelID = dlm.[CreatedBy]
	INNER JOIN	ClientPersonnel cpu ON cpu.ClientPersonnelID = dlm.[UpdatedBy]
	INNER JOIN	LeadType lt ON lt.LeadTypeID = dlm.LeadTypeID
	WHERE		dlm.[ClientID] = @ClientID
	AND			dlm.[Deleted] <> 1
	
	Select @@ROWCOUNT as [rows]
	SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap__GetUndeletedByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap__GetUndeletedByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap__GetUndeletedByClientID] TO [sp_executeall]
GO
