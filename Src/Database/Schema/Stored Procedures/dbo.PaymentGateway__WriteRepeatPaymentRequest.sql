SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-05-20
-- Description:	WriteRepeatPaymentRequest
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteRepeatPaymentRequest]
	@ClientID INT,
	@CustomerID INT,
	@RepeatPaymentRequestVendorTxCode VARCHAR(2000),
	@RepeatPaymentRequestAmount VARCHAR(2000),
	@RepeatPaymentRequestCurrency VARCHAR(2000),
	@RepeatPaymentRequestDescription VARCHAR(2000),
	@RepeatPaymentRequestRelatedVPSTxId VARCHAR(2000),
	@RepeatPaymentRequestRelatedVendorTxCode VARCHAR(2000),
	@RepeatPaymentRequestRelatedSecurityKey VARCHAR(2000),
	@RepeatPaymentRequestRelatedTxAuthNo VARCHAR(2000),
	@RepeatPaymentRequestDeliverySurname VARCHAR(2000),
	@RepeatPaymentRequestDeliveryFirstnames VARCHAR(2000),
	@RepeatPaymentRequestDeliveryAddress1 VARCHAR(2000),
	@RepeatPaymentRequestDeliveryCity VARCHAR(2000),
	@RepeatPaymentRequestDeliveryPostCode VARCHAR(2000),
	@RepeatPaymentRequestDeliveryCountry VARCHAR(2000)
AS BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TableDetailFieldID INT,
			@TableDetailFieldPageID INT,
			@TableRowID INT

	SELECT TOP (1) @TableDetailFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ClientID = @ClientID 
	AND tpfm.ThirdPartyFieldID = 534
	
	SELECT @TableDetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @TableDetailFieldID

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
	VALUES (@ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID)

	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 534 THEN @RepeatPaymentRequestVendorTxCode
																	WHEN 535 THEN @RepeatPaymentRequestAmount
																	WHEN 536 THEN @RepeatPaymentRequestCurrency
																	WHEN 537 THEN @RepeatPaymentRequestDescription
																	WHEN 538 THEN @RepeatPaymentRequestRelatedVPSTxId
																	WHEN 539 THEN @RepeatPaymentRequestRelatedVendorTxCode
																	WHEN 540 THEN @RepeatPaymentRequestRelatedSecurityKey
																	WHEN 541 THEN @RepeatPaymentRequestRelatedTxAuthNo
																	WHEN 542 THEN @RepeatPaymentRequestDeliverySurname
																	WHEN 543 THEN @RepeatPaymentRequestDeliveryFirstnames
																	WHEN 544 THEN @RepeatPaymentRequestDeliveryAddress1
																	WHEN 545 THEN @RepeatPaymentRequestDeliveryCity
																	WHEN 546 THEN @RepeatPaymentRequestDeliveryPostCode
																	WHEN 547 THEN @RepeatPaymentRequestDeliveryCountry
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (534,535,536,537,538,539,540,541,542,543,544,545,546,547)

	DECLARE @RepeatPaymentVendorTxIDDetailFieldID INT	
	SELECT @RepeatPaymentVendorTxIDDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=622
	EXEC _C00_SimpleValueIntoField @RepeatPaymentVendorTxIDDetailFieldID, @RepeatPaymentRequestVendorTxCode, @CustomerID

	SELECT @TableRowID TableRowID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRepeatPaymentRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteRepeatPaymentRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRepeatPaymentRequest] TO [sp_executeall]
GO
