SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-04
-- Description:	Get Stuff to show in dashboard stats
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DashManagement]
	@ClientID INT,
	@ClientPersonnelID INT,
	@ClientPersonnelAdminGroupID INT,
	@DashboardGroupID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT di.DashboardItemID, di.ShowAs, di.Title, di.QueryID, di.TransposeRequired, di.ChartType
	FROM DashboardGroup dg WITH (NOLOCK)
	INNER JOIN DashboardItems di WITH (NOLOCK) ON di.DashboardGroupID = dg.DashboardGroupID
	WHERE dg.ClientID = @ClientID 
	AND (dg.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID OR dg.ClientPersonnelAdminGroupID IS NULL)
	AND dg.DashboardGroupID = @DashboardGroupID
	AND di.Enabled = 1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DashManagement] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DashManagement] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DashManagement] TO [sp_executeall]
GO
