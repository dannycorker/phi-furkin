SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeOption_GetByEventTypeOptionID]
(

	@EventTypeOptionID int   
)
AS


				SELECT
					[EventTypeOptionID],
					[ClientID],
					[EventTypeID],
					[AquariumOptionID],
					[OptionValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[EventTypeOption] WITH (NOLOCK) 
				WHERE
										[EventTypeOptionID] = @EventTypeOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_GetByEventTypeOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeOption_GetByEventTypeOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_GetByEventTypeOptionID] TO [sp_executeall]
GO
