SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-12-10
-- Description:	List all DataLoader activity
-- =============================================
CREATE PROCEDURE [dbo].[dlf] 
	@ClientID int = NULL, 
	@DaysAgo int = 1
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 'Pending Import' as [Pending Import], dlfs.FileStatusName, dlf.* 
	FROM dbo.DataLoaderFile dlf (nolock) 
	INNER JOIN dbo.DataLoaderFileStatus dlfs (nolock) on dlfs.DataLoaderFileStatusID = dlf.FileStatusID
	WHERE (dlf.ClientID = @ClientID OR @ClientID IS NULL) 
	AND (dlf.DataLoadedDateTime IS NULL)
	ORDER BY dlf.ScheduledDateTime 
	
	SELECT 'Recently Loaded' as [Recently Loaded], dlfs.FileStatusName, dlf.* 
	FROM dbo.DataLoaderFile dlf (nolock) 
	INNER JOIN dbo.DataLoaderFileStatus dlfs (nolock) on dlfs.DataLoaderFileStatusID = dlf.FileStatusID
	WHERE (dlf.ClientID = @ClientID OR @ClientID IS NULL) 
	AND dlf.DataLoadedDateTime > dateadd(dd, -@DaysAgo, dbo.fn_GetDate_Local())
	ORDER BY dlf.DataLoadedDateTime 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[dlf] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dlf] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dlf] TO [sp_executeall]
GO
