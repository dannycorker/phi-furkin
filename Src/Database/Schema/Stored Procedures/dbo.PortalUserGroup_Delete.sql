SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalUserGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroup_Delete]
(

	@PortalUserGroupID int   
)
AS


				DELETE FROM [dbo].[PortalUserGroup] WITH (ROWLOCK) 
				WHERE
					[PortalUserGroupID] = @PortalUserGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Delete] TO [sp_executeall]
GO
