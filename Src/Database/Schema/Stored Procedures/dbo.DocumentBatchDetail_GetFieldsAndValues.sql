SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2013-01-18
-- Description:	Retrieve the list of mandatory / helper field id's
--              together with the value.

-- Parameters:  @DocumentBatchId
--              Reference to the specific document 
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail_GetFieldsAndValues]
	@DocumentBatchDetailId INT
AS
BEGIN

DECLARE @QuestionsXml XML

SET @QuestionsXml = (
	SELECT DocumentMetaData 
	FROM DocumentBatchDetail WITH (NOLOCK)
	WHERE DocumentBatchDetailID = @DocumentBatchDetailId
	FOR XML AUTO, ROOT('root')
)

SELECT
	col.value('@Id', 'int') Id,
	col.value('.', 'varchar(2000)') Value
FROM
	@QuestionsXml.nodes('//Document/MetaData/Item') as Tab(col)

END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_GetFieldsAndValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail_GetFieldsAndValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_GetFieldsAndValues] TO [sp_executeall]
GO
