SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Clients table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_Update]
(

	@ClientID int   ,

	@CompanyName varchar (100)  ,

	@WebAddress varchar (255)  ,

	@IPAddress varchar (50)  ,

	@DefaultEmailAddress varchar (255)  ,

	@ClientTypeID int   ,

	@AllowSMS bit   ,

	@SecurityCode varchar (36)  ,

	@LeadsBelongToOffices bit   ,

	@UseEventCosts bit   ,

	@UseEventUOEs bit   ,

	@UseEventDisbursements bit   ,

	@UseEventComments bit   ,

	@VerifyAddress bit   ,

	@UseTapi bit   ,

	@UseRPI bit   ,

	@UseGBAddress bit   ,

	@UsePinpoint bit   ,

	@UseSage bit   ,

	@UseSAS bit   ,

	@UseCreditCalculation bit   ,

	@FollowupWorkingDaysOnly bit   ,

	@AddLeadByQuestionnaire bit   ,

	@UseIncendia bit   ,

	@LanguageID int   ,

	@CountryID int   ,

	@UseMobileInterface bit   ,

	@UseGBValidation bit   ,

	@CurrencyID int   ,

	@AllowSmsCommandProcessing bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Clients]
				SET
					[CompanyName] = @CompanyName
					,[WebAddress] = @WebAddress
					,[IPAddress] = @IPAddress
					,[DefaultEmailAddress] = @DefaultEmailAddress
					,[ClientTypeID] = @ClientTypeID
					,[AllowSMS] = @AllowSMS
					,[SecurityCode] = @SecurityCode
					,[LeadsBelongToOffices] = @LeadsBelongToOffices
					,[UseEventCosts] = @UseEventCosts
					,[UseEventUOEs] = @UseEventUOEs
					,[UseEventDisbursements] = @UseEventDisbursements
					,[UseEventComments] = @UseEventComments
					,[VerifyAddress] = @VerifyAddress
					,[UseTapi] = @UseTapi
					,[UseRPI] = @UseRPI
					,[UseGBAddress] = @UseGBAddress
					,[UsePinpoint] = @UsePinpoint
					,[UseSage] = @UseSage
					,[UseSAS] = @UseSAS
					,[UseCreditCalculation] = @UseCreditCalculation
					,[FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly
					,[AddLeadByQuestionnaire] = @AddLeadByQuestionnaire
					,[UseIncendia] = @UseIncendia
					,[LanguageID] = @LanguageID
					,[CountryID] = @CountryID
					,[UseMobileInterface] = @UseMobileInterface
					,[UseGBValidation] = @UseGBValidation
					,[CurrencyID] = @CurrencyID
					,[AllowSmsCommandProcessing] = @AllowSmsCommandProcessing
				WHERE
[ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Update] TO [sp_executeall]
GO
