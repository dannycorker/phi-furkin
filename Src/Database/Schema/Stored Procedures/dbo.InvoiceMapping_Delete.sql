SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the InvoiceMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMapping_Delete]
(

	@InvoiceMappingID int   
)
AS


				DELETE FROM [dbo].[InvoiceMapping] WITH (ROWLOCK) 
				WHERE
					[InvoiceMappingID] = @InvoiceMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Delete] TO [sp_executeall]
GO
