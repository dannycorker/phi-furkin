SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardon
-- Create date: 17/04/2015
-- Description:	Gets the script lock information for the given script
-- =============================================
CREATE PROCEDURE [dbo].[ScriptLock__GetMetaData]
	@ScriptID INT
AS
BEGIN
	SET NOCOUNT ON;

    SELECT sl.*, cp.FirstName, cp.LastName, cp.JobTitle FROM ScriptLock sl WITH (NOLOCK) 
	INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID=sl.WhoIsEditing
	WHERE sl.ScriptID=@ScriptID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock__GetMetaData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock__GetMetaData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock__GetMetaData] TO [sp_executeall]
GO
