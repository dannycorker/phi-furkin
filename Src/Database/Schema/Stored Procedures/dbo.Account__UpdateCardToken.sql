SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-06-2016
-- Description:	Updates account card token information
-- =============================================
CREATE PROCEDURE [dbo].[Account__UpdateCardToken]
	@AccountID INT,
	@ClientID INT,
	@CardToken VARCHAR(250) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE Account
	SET CardToken=@CardToken
	WHERE AccountID=@AccountID AND ClientID=@ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__UpdateCardToken] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__UpdateCardToken] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__UpdateCardToken] TO [sp_executeall]
GO
