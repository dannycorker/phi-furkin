SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-27
-- Description:	Common routine for LeadDocument/LeadDocumentFS creation. Requires far fewer params than LeadDocument_Insert.
-- JWG 2012-07-12 Need all columns in here for LeadDocument again, except for the blobs and archive details
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateLeadDocument] 
	/* Mandatory Params */
	@ClientID int, 
	@LeadID int, 
	@DocumentBLOB varbinary(max), 
	/* Optional Params */
	@LeadDocumentTitle varchar(1000) = '', 
	@DocumentTypeID int = NULL, 
	@UploadDateTime datetime = NULL, 
	@WhoUploaded int, 
	@FileName varchar(255), 
	@EmailBLOB varbinary(max) = NULL, 
	@DocumentFormat varchar(24) = NULL, 
	@EmailFrom varchar(512) = NULL,
	@EmailTo varchar(MAX) = NULL, 
	@CcList varchar(MAX) = NULL, 
	@BccList varchar(MAX) = NULL, 
	@ElectronicSignatureDocumentKey varchar(50) = NULL, 
	@Encoding varchar(50) = NULL, 
	@ContentFormat varchar(50) = NULL, 
	@ZipFormat varchar(50) = NULL, 
	@DocumentTypeVersionID int = NULL 

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadDocumentID int

	INSERT INTO [dbo].[LeadDocument]
	(
		ClientID, 
		LeadID, 
		DocumentTypeID, 
		LeadDocumentTitle, 
		UploadDateTime, 
		WhoUploaded, 
		DocumentBLOB, 
		FileName, 
		EmailBLOB, 
		DocumentFormat, 
		EmailFrom, 
		EmailTo, 
		CcList, 
		BccList, 
		ElectronicSignatureDocumentKey, 
		Encoding, 
		ContentFormat, 
		ZipFormat, 
		DocumentBlobSize, 
		EmailBlobSize, 
		DocumentDatabaseID, 
		WhenArchived, 
		DocumentTypeVersionID		
	)
	VALUES
	(
		@ClientID,
		@LeadID,
		@DocumentTypeID, 
		@LeadDocumentTitle, 
		@UploadDateTime, 
		@WhoUploaded, 
		CAST('' AS VARBINARY), -- DocumentBLOB
		@FileName, 
		NULL, -- EmailBLOB
		@DocumentFormat, 
		@EmailFrom, 
		@EmailTo, 
		@CcList, 
		@BccList, 
		@ElectronicSignatureDocumentKey, 
		@Encoding, 
		@ContentFormat, 
		@ZipFormat, 
		DATALENGTH(@DocumentBLOB), --DocumentBlobSize, 
		DATALENGTH(@EmailBlob), --EmailBlobSize, 
		NULL, -- DocumentDatabaseID
		NULL, -- WhenArchived
		@DocumentTypeVersionID
	)

	
	-- Get the identity value
	SET @LeadDocumentID = SCOPE_IDENTITY()

	SET IDENTITY_INSERT [dbo].[LeadDocumentFS] ON
	
	INSERT INTO [dbo].[LeadDocumentFS]
		([LeadDocumentID]
		,[ClientID]
		,[LeadID]
		,[DocumentTypeID]
		,[LeadDocumentTitle]
		,[UploadDateTime]
		,[WhoUploaded]
		,[DocumentBLOB]
		,[FileName]
		,[EmailBLOB]
		,[DocumentFormat]
		,[EmailFrom]
		,[EmailTo]
		,[CcList]
		,[BccList]
		,[ElectronicSignatureDocumentKey]
		,[Encoding]
		,[ContentFormat]
		,[ZipFormat]
		)
	VALUES
		(@LeadDocumentID
		,@ClientID
		,@LeadID
		,@DocumentTypeID
		,@LeadDocumentTitle
		,@UploadDateTime
		,@WhoUploaded
		,@DocumentBLOB
		,@FileName
		,@EmailBLOB
		,@DocumentFormat
		,@EmailFrom
		,@EmailTo
		,@CcList
		,@BccList
		,@ElectronicSignatureDocumentKey
		,@Encoding
		,@ContentFormat
		,@ZipFormat
	)
		
	SET IDENTITY_INSERT [dbo].[LeadDocumentFS] OFF
	
	RETURN @LeadDocumentID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateLeadDocument] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateLeadDocument] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateLeadDocument] TO [sp_executeall]
GO
