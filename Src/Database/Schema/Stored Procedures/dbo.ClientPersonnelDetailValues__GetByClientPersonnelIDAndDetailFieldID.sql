SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[ClientPersonnelDetailValues__GetByClientPersonnelIDAndDetailFieldID]
(
	@ClientPersonnelID int,
	@DetailFieldID int	
)
AS


				SET ANSI_NULLS OFF
				
				SELECT 
					[ClientPersonnelDetailValueID],
					[ClientPersonnelID],
					[ClientID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ClientPersonnelDetailValues]
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID AND
					[DetailFieldID] = @DetailFieldID 					
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues__GetByClientPersonnelIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelDetailValues__GetByClientPersonnelIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues__GetByClientPersonnelIDAndDetailFieldID] TO [sp_executeall]
GO
