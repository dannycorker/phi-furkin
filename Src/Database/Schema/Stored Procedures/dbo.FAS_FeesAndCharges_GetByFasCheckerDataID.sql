SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FAS_FeesAndCharges table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_FeesAndCharges_GetByFasCheckerDataID]
(

	@FasCheckerDataID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FasFeesAndChargesID],
					[FasCheckerDataID],
					[Description],
					[Amount],
					[WhenPaid]
				FROM
					[dbo].[FAS_FeesAndCharges] WITH (NOLOCK) 
				WHERE
					[FasCheckerDataID] = @FasCheckerDataID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_GetByFasCheckerDataID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_FeesAndCharges_GetByFasCheckerDataID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_GetByFasCheckerDataID] TO [sp_executeall]
GO
