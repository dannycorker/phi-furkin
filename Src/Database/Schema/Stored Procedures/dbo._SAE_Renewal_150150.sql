SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Expire Policy
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Renewal_150150]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PurchasedProductID			INT
			,@AccountID						INT
			,@CardTransactionID				INT
			,@TableRowID					INT
			,@EndDate						DATE
			,@Now							DATETIME = dbo.fn_GetDate_Local()
			,@PolicyStartDate				DATE
			,@RenewalTableRowID				INT
			,@ValueDate						DATE
			,@ValueMoney					MONEY
			,@OldPurchasedProductID			INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

			
	/* Policy Expired */
	IF @EventTypeID IN (150150)
	BEGIN
	
		/*We have already prepped a renewal for this policy so set the schedule to ignored and add a row to premium detail history*/ 
		/*GPR 2019-12-23 assign r.TableRowID to @TableRowID for SDLPC-40*/
		--SELECT @TableRowID = (SELECT TOP 1 r.TableRowID--, tdvStatus.ValueInt
		--FROM dbo.TableRows r WITH (NOLOCK) 
		--LEFT  JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 145666 /*Status*/
		--WHERE tdvStatus.ValueInt = 43002 /*Live*/
		--AND r.MatterID=@MatterID
		--ORDER BY r.TableRowID DESC)

		--/*Update the last Row to be lapsed*/ 
		--IF @TableRowID <> 0
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 145666, 43004 , @TableRowID , @WhoCreated /*Status => "Lapsed"*/
		--END


		/*GPR 2020-06-19 Set all WrittenPremium rows to inactive*/
		DECLARE @WrittenPremiumID INT

		UPDATE WrittenPremium
		SET Active = 0
		WHERE MatterID = @MatterID

		/*GPR 2020-06-19 Find the Renewal row and change the type to Lapsed*/

		SELECT TOP (1)
		@WrittenPremiumID = wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 3 /*Renewal*/
		ORDER BY wp.WrittenPremiumID DESC	

		UPDATE WrittenPremium
		SET AdjustmentTypeID = 5 /*Lapse*/
		WHERE WrittenPremiumID = @WrittenPremiumID
		AND MatterID = @MatterID

		SELECT @PurchasedProductID = NULL 
		SELECT @PurchasedProductID = p.PurchasedProductID, @EndDate = mdv.ValueDate, @AccountID = p.AccountID 
		FROM PurchasedProduct p with (NOLOCK) 
		INNER JOIN MatterDetailValues  mdv with (NOLOCK) on mdv.MatterID = p.ObjectID
		Where p.ObjectID = @MatterID 
		AND mdv.DetailFieldID = 170037 /*End Date*/ 
		AND p.ValidFrom >= mdv.ValueDate

	
		/*Updare The PurchasedProduct's end date and set it's schedules to ignore*/ 
		UPDATE PurchasedProduct 
		SET ValidTo = CAST(DATEADD(DAY,-1,@Now) as DATE)
		WHERE PurchasedProductID = @PurchasedProductID 

		UPDATE PurchasedProductPaymentSchedule 
		SET PaymentStatusID = 3 
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PaymentStatusID IN (1,5)

		--JML #59394 added CPS rebuild
		EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @PolicyStartDate, @AqAutomation

		/*GPR 2020-06-19 commented out*/
		--/*Deal with the WP*/
		--/*find the renewal Table Row*/ 
		--SELECT TOP 1  @RenewalTableRowID = tr.tableRowID, @ValueDate = frm.ValueDate FROM TableRows tr WITH (NOLOCK)
		--INNER JOIN TableDetailValues adj with (NOLOCK) on adj.TableROwID = tr.TableRowID AND adj.DetailFIeldID = 175398 /*Adjustment Type*/ 
		--INNER JOIN tabledetailValues frm with (NOLOCK) on frm.TableRowID = tr.TableRowID AND frm.DetailFieldID = 175346 /*Adjustment From Date*/
		--WHERE tr.MatterID = @MatterID 
		--AND tr.DetailFieldID = 175336
		--AND adj.ValueINT IN (74528,74527) /*Renewal*/ 
		----AND frm.ValueDate >= @EndDate /*GPR 2019-12-12 SDLPC-40*/
		--ORDER by tr.TableRowID DESC 

		--PRINT @RenewalTableRowID 
		--/*Set the to Date on renewal Table row*/ 
		--IF @RenewalTableRowID <> 0
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 175347,@ValueDate,@RenewalTableRowID /*Cover To*/
		--END

		EXEC TableRows_Insert  @TableRowID OUTPUT, @ClientID,NULL,@MatterID,175336,19011,1,1,NULL,NULL,NULL,NULL,@RenewalTableRowID
	
		PRINT @TableRowID 

		/*Insert the same values as the renewal row except for the premium and commision*/ 
		INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,MatterID,ClientID)
		SELECT @TableRowID, tdv.DetailFieldID,tdv.DetailValue, tdv.MatterID,tdv.ClientID FROM TableDetailValues tdv with (NOLOCK) 
		WHERE tdv.TableRowID = @RenewalTableRowID 
		AND DetailFieldID NOT IN (180170,180171,175397,177899,175398)

		/*Set the adjustment amount to be negative*/
	
		--SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(175397,@RenewalTableRowID) * -1) 
		--EXEC _C00_SimpleValueIntoField 175397,@ValueMoney,@TableRowID

		--SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(177899,@RenewalTableRowID) * -1) 
		--EXEC _C00_SimpleValueIntoField 177899,@ValueMoney,@TableRowID
		
		--EXEC _C00_SimpleValueIntoField 175398,76523,@TableRowID

		/*Update the policy status to Lapsed*/ /*added back in NG 20200622 after Renewals review*/
		EXEC _C00_SimpleValueIntoField 170038 , 43004 , @MatterID , @WhoCreated
		
		/*Invalidate any uncollected card transactions*/ 
		SELECT @CardTransactionID = ct.CardTransactionID 
		FROM CardTransaction ct WITH (NOLOCK)
		WHERE ct.ObjectID = @MatterID 
		AND ct.Description = 'Renewal Stub '
		AND ct.ErrorCode IS NULL
		AND ct.ErrorMessage IS NULL

		UPDATE CardTransaction 
		SET ErrorMessage = 'Invalid', ErrorCode = 'Invalid', Description = 'Renewal stub removed after lapse'
		WHERE CardTransactionID = @CardTransactionID

		SELECT TOP 1 @OldPurchasedProductID = pp.PurchasedProductID
		FROM PurchasedProduct pp with (NOLOCK)  
		WHERE pp.ObjectID = @MatterID 
		AND pp.PurchasedProductID <> @PurchasedProductID 
		Order by pp.PurchasedProductID Desc 

		EXEC _C00_SimpleValueIntoField 177074,@OldPurchasedProductID,@MatterID, @WhoCreated /*Purchased Policy ID on Pol Admin--aq 1,2,177074*/

		/*Set the Historical Policy row to Not live*/ 
		UPDATE TableDetailValues 
		SET DetailValue = 43004
		WHERE MatterID = @MatterID 
		AND ValueINT = 43002
		AND DetailFieldID = 145666

		/*If they had already paid by card (no mandates can be in place), add the refund*/ 

	END			


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_150150] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Renewal_150150] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_150150] TO [sp_executeall]
GO
