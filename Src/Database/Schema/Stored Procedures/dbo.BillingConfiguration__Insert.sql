SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 27-07-2016
-- Description:	Inserts a billing configuration record
-- =============================================
CREATE PROCEDURE [dbo].[BillingConfiguration__Insert] 

	@BillingConfigurationID INT OUTPUT,
	@ClientID INT, 
	@RemainderUpFront BIT, 
	@RegularPaymentWait INT, 
	@OneOffAdjustmentWait INT
	
AS
BEGIN
		
	SET NOCOUNT ON;
	
	INSERT INTO BillingConfiguration (ClientID, RemainderUpFront, RegularPaymentWait, OneOffAdjustmentWait)
	VALUES (@ClientID, @RemainderUpFront, @RegularPaymentWait, @OneOffAdjustmentWait)
		
	SELECT @BillingConfigurationID = SCOPE_IDENTITY()
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillingConfiguration__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__Insert] TO [sp_executeall]
GO
