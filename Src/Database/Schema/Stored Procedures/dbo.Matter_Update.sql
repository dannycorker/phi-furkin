SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Matter table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_Update]
(

	@MatterID int   ,

	@ClientID int   ,

	@MatterRef varchar (100)  ,

	@CustomerID int   ,

	@LeadID int   ,

	@MatterStatus tinyint   ,

	@RefLetter varchar (3)  ,

	@BrandNew bit   ,

	@CaseID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime2   ,

	@WhoModified int   ,

	@WhenModified datetime2   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Matter]
				SET
					[ClientID] = @ClientID
					,[MatterRef] = @MatterRef
					,[CustomerID] = @CustomerID
					,[LeadID] = @LeadID
					,[MatterStatus] = @MatterStatus
					,[RefLetter] = @RefLetter
					,[BrandNew] = @BrandNew
					,[CaseID] = @CaseID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[SourceID] = @SourceID
				WHERE
[MatterID] = @MatterID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Update] TO [sp_executeall]
GO
