SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetCountsForLeadImport] @ClientID int AS

SELECT
	COUNT(ClientQuestionnaires.ClientQuestionnaireID) AS LeadCount,
	ClientQuestionnaires.ClientQuestionnaireID AS CQID,
	ClientQuestionnaires.QuestionnaireTitle AS CQTitle
FROM	
	ClientQuestionnaires
INNER JOIN
	CustomerQuestionnaires ON ClientQuestionnaires.ClientQuestionnaireID = CustomerQuestionnaires.ClientQuestionnaireID
INNER JOIN
	Customers ON CustomerQuestionnaires.CustomerID = Customers.CustomerID
WHERE
	(ClientQuestionnaires.ClientID = @ClientID)
GROUP BY
	ClientQuestionnaires.ClientQuestionnaireID,
	ClientQuestionnaires.QuestionnaireTitle,
	Customers.AquariumStatusID
HAVING
	(Customers.AquariumStatusID = 1)
ORDER BY
	ClientQuestionnaires.QuestionnaireTitle



GO
GRANT VIEW DEFINITION ON  [dbo].[GetCountsForLeadImport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCountsForLeadImport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCountsForLeadImport] TO [sp_executeall]
GO
