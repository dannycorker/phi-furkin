SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddPhoneVerificationForCustomer]

@ClientQuestionnaireID int,
@CustomerID int

as

insert into PhoneNumberVerifications (ClientQuestionnaireID,CustomerID)
values (@ClientQuestionnaireID, @CustomerID)



GO
GRANT VIEW DEFINITION ON  [dbo].[AddPhoneVerificationForCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddPhoneVerificationForCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddPhoneVerificationForCustomer] TO [sp_executeall]
GO
