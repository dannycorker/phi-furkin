SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FieldValidation table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_GetByFieldID]
(

	@FieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FieldValidationID],
					[ClientID],
					[FieldID],
					[ValidationGroup],
					[ValidationType],
					[MaxValue],
					[MinValue],
					[ErrorMessage],
					[AccessLevelRestrictions]
				FROM
					[dbo].[FieldValidation] WITH (NOLOCK) 
				WHERE
					[FieldID] = @FieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_GetByFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_GetByFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_GetByFieldID] TO [sp_executeall]
GO
