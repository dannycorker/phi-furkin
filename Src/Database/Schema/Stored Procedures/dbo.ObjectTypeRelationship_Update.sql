SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ObjectTypeRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_Update]
(

	@ObjectTypeRelationshipID int   ,

	@FromObjectTypeID int   ,

	@ToObjectTypeID int   ,

	@RelationshipTypeID int   ,

	@RelationshipName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ObjectTypeRelationship]
				SET
					[FromObjectTypeID] = @FromObjectTypeID
					,[ToObjectTypeID] = @ToObjectTypeID
					,[RelationshipTypeID] = @RelationshipTypeID
					,[RelationshipName] = @RelationshipName
				WHERE
[ObjectTypeRelationshipID] = @ObjectTypeRelationshipID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Update] TO [sp_executeall]
GO
