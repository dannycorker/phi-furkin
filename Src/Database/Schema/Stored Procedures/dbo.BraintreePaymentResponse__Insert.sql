SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-02-11
-- Description:	Insert Braintree response
-- =============================================
CREATE PROCEDURE [dbo].[BraintreePaymentResponse__Insert]
	@CardTransactionID INT,
	@ResponseXML XML
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @BraintreePaymentResponseID INT

	INSERT INTO dbo.BraintreePaymentResponse ([CardTransactionID], [ResponseXML], [WhenCreated])
	VALUES (@CardTransactionID, @ResponseXML, dbo.fn_GetDate_Local())

	SELECT @BraintreePaymentResponseID = SCOPE_IDENTITY()

	SELECT @BraintreePaymentResponseID

	RETURN @BraintreePaymentResponseID

END
GO
