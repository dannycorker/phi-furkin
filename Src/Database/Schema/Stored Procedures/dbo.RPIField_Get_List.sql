SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the RPIField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_Get_List]

AS


				
				SELECT
					[RPIFieldID],
					[RPIFieldName],
					[RPIFieldDescription],
					[IsEnabled],
					[QuestionTypeID],
					[DetailFieldSubtypeID],
					[LookupListID],
					[FieldOrder],
					[FieldSize],
					[WhenCreated],
					[WhenModified],
					[RPIFieldGroupID]
				FROM
					[dbo].[RPIField] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Get_List] TO [sp_executeall]
GO
