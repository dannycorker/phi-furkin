SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetCountsForLeadImportSum] 
	@ClientID int 
AS
BEGIN
/*2012-11-14 Nolocked By CS*/

	SELECT COUNT(ClientQuestionnaires.ClientQuestionnaireID) AS LeadCount
	FROM ClientQuestionnaires WITH (NOLOCK) 
	INNER JOIN CustomerQuestionnaires WITH (NOLOCK)  ON ClientQuestionnaires.ClientQuestionnaireID = CustomerQuestionnaires.ClientQuestionnaireID
	INNER JOIN Customers WITH (NOLOCK)  ON CustomerQuestionnaires.CustomerID = Customers.CustomerID
	WHERE (ClientQuestionnaires.ClientID = @ClientID)
	AND (Customers.AquariumStatusID = 1)
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetCountsForLeadImportSum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCountsForLeadImportSum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCountsForLeadImportSum] TO [sp_executeall]
GO
