SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitle table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitle_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SELECT
					[LeadTypeID],
					[PageTitle]
				FROM
					[dbo].[LeadTypePageTitle] WITH (NOLOCK) 
				WHERE
										[LeadTypeID] = @LeadTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitle_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_GetByLeadTypeID] TO [sp_executeall]
GO
