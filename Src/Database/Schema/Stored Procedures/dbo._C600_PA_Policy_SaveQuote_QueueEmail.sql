SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================		
-- Author:	Cathal Sherry
-- Create date: 2017-01-30
-- Description:	Queue an email for a saved quote
-- 2017-08-14	AHOD	Updated with CP Configuration
-- 2017-08-17	AHOD	Updated to use Database Specific Config Table
-- 2018-05-25	GPR		Updated to shred out the ProductID and Recurring Monthly
-- 2018-05-30	GPR		Updated to ISNULL around certain values where the node is named differently for EQB
-- 2018-08-17	UAH		Added lines 70 to 144. Now there is a way to collate a list of pet names/products and monthly figures into a single multipet email. 
-- 2018-08-23	UAH		Added a @StringOfPetNames to collate a list of comma seperated petnames for the external saved quote email
-- 2018-08-30	UAH		Added extra fields to format HTML multipets before it gets saved in the basic table ready for Aliasing.
-- 2018-09-14	GPR		Updated front-size for Umer (Defect #946)
-- 2018-09-21   JEL don't add table rows endlessly if the user clicks the button more than once*/ 
-- 2021-04-07	ACE pass in clientpersonnelid and use if if available.
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Policy_SaveQuote_QueueEmail]
(		
	@SavedQuoteKey	VARCHAR(100),
	@ClientPersonnelID	INT = -2
)		
AS		
BEGIN

	DECLARE  @SavedQuoteID					INT
			,@LeadID						INT
			,@CustomerID					INT
			,@ClientID						INT = dbo.fnGetPrimaryClientID()
			,@SavedQuoteLeadTypeID			INT = 1505
			,@RunAsUserID					INT = CASE WHEN @ClientPersonnelID > 0 THEN @ClientPersonnelID ELSE 58552 END /*Aquarium Robot*/
			,@CaseID						INT
			,@SavedQuoteXml					XML
			,@SavedQuoteXMLForMultiPetEmail XML  -- UH 16_08_2018 - Due to the fact that the saved quote XML does not hold the individual monthly price for multipet, we will need to draw these values from the PetQuoteDefinitionXML Column in the saved quote table
			,@PetDefXML						XML --UH 16_08_2018 - Stores chopped XML Pet Definition nodes
			,@NumberOfPets					INT
			,@SELECTedProductRLID			INT
			,@SELECTedProductName			VARCHAR(2000)
			,@PetNameFromPetDef				VARCHAR(2000)
			,@StringOfPetNames				VARCHAR(2000) = '' --initialising this variable as an empty string as we cant add a string to a null value
			,@ProductAndPetNameConcat		VARCHAR(2000)--used to construct HTML before storing it in a basic table
			,@MonthlyPriceConcat			VARCHAR(2000)
			,@RecurringMonthlyPrice			MONEY
			,@FirstMonthlyPrice				MONEY
			,@GreaterPrice					MONEY
			,@TableRowID					INT
			,@EmailAddress					VARCHAR(2000)
			,@SecretQuestionID				INT
			,@SecretQuestion				VARCHAR(2000)
			,@SecretAnswer					VARCHAR(2000)
			,@DateTimeVarchar				VARCHAR(23)
			,@ExpiryDateTimeVarchar			VARCHAR(23)
			,@NewLeadCreated				BIT = 0
			,@TitlesLookupListID			INT = 2359 /*Aquarium Titles*/
			,@QuoteSessionID				VARCHAR(2000)
			,@BrandID						INT
			,@Separator						VARCHAR(10) = '<br>'/*testing - Nessa*/ --CHAR(13) + CHAR(10) /*GPR 2021-04-28 for FURKIN-557*/
			,@RetrievalLink					VARCHAR(500)
			,@Now VARCHAR(50) = CONVERT(VARCHAR,DATEADD(MINUTE,30,dbo.fn_GetDate_Local()),121) /*GPR 2021-05-07*/ /*NG updated to 30 minutes for SDFURPHI-62*/
	
	DECLARE @DetailValueData dbo.tvpDetailValueData
			
	SELECT	 @CustomerID = dbo.fn_C600_GetDatabaseSpecificConfigValue ('SavedQuoteCustomerID') /*CustomerID to hold Saved Quote leads - AHOD Updated 2017-08-17*/
	
	/*Find the SavedQuote record using the passed key*/
	SELECT	 @SavedQuoteID			= sq.SavedQuoteID
			,@SavedQuoteXml			= sq.SavedQuoteXML
			,@EmailAddress			= sq.EmailAddress
			,@SecretQuestionID		= sq.SecretQuestionID
			,@SecretAnswer			= sq.SecretQuestionValue
			,@DateTimeVarchar		= CONVERT(VARCHAR,sq.WhenCreated,121)
			,@ExpiryDateTimeVarchar	= ISNULL(CONVERT(VARCHAR,sq.WhenExpire,121),'NULL')
			,@QuoteSessionID		= ISNULL(CONVERT(VARCHAR,sq.QuoteSessionID),'NULL')
	FROM _C600_SavedQuote sq WITH ( NOLOCK )
	WHERE sq.SavedQuoteKey = @SavedQuoteKey

	/* 2018-11-22 UAH if this is set to 1, that means the email is already in the process of being sent.*/
	IF EXISTS
	(
		SELECT * FROM _C600_SavedQuote sq WITH ( NOLOCK ) 
		WHERE sq.SavedQuoteID = @SavedQuoteID
		AND (sq.EmailQueued = 0 OR sq.EmailQueued IS NULL)
	)
	BEGIN 
		  
		/* 2018-08-17 UAH SELECT the petQuoteDefinitionXML column value into a variable*/
		--SELECT @SavedQuoteXMLForMultiPetEmail =  sqm.PetQuoteDefinitionXml FROM _C600_SavedQuote sqm WITH (NOLOCK)  WHERE sqm.SavedQuoteKey = @SavedQuoteKey 
		
		/*GPR 2021-04-28 for FURKIN-557*/
		SELECT @SavedQuoteXMLForMultiPetEmail =  sqm.SavedQuoteXML FROM _C600_SavedQuote sqm WITH (NOLOCK)  WHERE sqm.SavedQuoteKey = @SavedQuoteKey 
		
		/*If the Variable is not null, Split out each of the petinfo blocks as an xml strings*/
		IF (@SavedQuoteXMLForMultiPetEmail IS NOT NULL) OR (@SavedQuoteXMLForMultiPetEmail.exist('*') <> 0) --or the nodes do not exist UH 2018-08-17
		BEGIN

			--Declare a table which stores each 'Pet Definition' XML block in its own row.
			DECLARE @PetInfo TABLE
			(
				  ID INT IDENTITY,
				  Xml XML
			)
      
			--Store each 'Pet Definition' XML block in its own row.
			INSERT @PetInfo (Xml)
			SELECT      node.query('.')
			--FROM @SavedQuoteXMLForMultiPetEmail.nodes('//PetDefinition') AS pet(node)
			FROM @SavedQuoteXMLForMultiPetEmail.nodes('//PetInfo') AS pet(node) /*GPR 2021-04-28 for FURKIN-557*/

			--SELECT the count of pets and store this in a variable
			SELECT @NumberOfPets = COUNT(*) FROM @PetInfo 

			 --SELECT the first element in the @petinfo table
			DECLARE @ID INT 
			SELECT TOP 1  @ID = p.ID FROM @PetInfo p 
			ORDER by p.ID asc

			/*JEL 2018-09-21 don't add table rows endlessly if the user clicks the button more than once*/ 
			IF EXISTS (SELECT * FROM TableRows t WITH ( NOLOCK ) 
				 WHERE t.LeadID = @LeadID 
				 and t.DetailFieldID = 180164) 
			BEGIN 
	
			DELETE FROM TableDetailValues 
			WHERE LeadID = @LeadID 
			AND DetailFieldID IN (180161, 180162, 180163, 180172, 180173) 
	
			DELETE FROM TableRows 
			WHERE LeadID = @LeadID 
			and DetailFieldID = 180164
	
		END 
	
		--While the ID is less than or equal to the number of pets
		WHILE @ID <= @NumberOfPets
		BEGIN 
			--SELECT the first xml block
			SELECT @PetDefXML	 = p.xml FROM @PetInfo p 
			WHERE p.ID = @ID 

			--Shred the name for the current xml block in the while loop
			--SELECT @PetNameFromPetDef = @PetDefXML.value('(PetDefinition/Name)[1]', 'VARCHAR(2000)') 
			SELECT @PetNameFromPetDef = @PetDefXML.value('(PetInfo/PetName)[1]', 'VARCHAR(2000)') /*GPR 2021-04-28 for FURKIN-557*/

			--add it to the string of pet names for all the elements except the last one 
			SELECT @StringOfPetNames = @StringOfPetNames +  @PetNameFromPetDef + @Separator /*GPR 2021-04-28 for FURKIN-557 added @Separator*/ 
			WHERE @ID <> @NumberOfPets

			--if the numberofPets is more than 1, add the word ' and' between the current list of pet names and the last pet name so we get the output  e.g 'petname1, petname2, petname3, and petname4
			IF @NumberOfPets > 1
			BEGIN
				SELECT @StringOfPetNames += @Separator + @PetNameFromPetDef  /*GPR 2021-04-28 for FURKIN-557 added @Separator*/
				WHERE @ID = @NumberOfPets
			END
			ELSE
			BEGIN
				SELECT @StringOfPetNames +=  @PetNameFromPetDef 
				WHERE @ID = @NumberOfPets
			END
	
			/*NG 2021-05-10 for FURKIN-557 amended string to ignore doubling of separator before last name*/
			SELECT  @StringOfPetNames = REPLACE(@StringOfPetNames, '<br><br>', '<br>');

			--Store the SELECTed product for the current pet in its own variable
			SELECT @SELECTedProductRLID = @PetDefXML.value('(PetDefinition/SELECTedProductId)[1]', 'INT')

			--Store the brand id in its own variable
			--SELECT @BrandID =  @SavedQuoteXMLForMultiPetEmail.value('(QuoteDefinition/BrandId)[1]', 'int')
			
			/*GPR 2021-04-28 for FURKIN-557*/
			SELECT @BrandID =  @SavedQuoteXMLForMultiPetEmail.value('(BuyPolicyRequest/BrandingId)[1]', 'int')

			SELECT @RetrievalLink =	CASE @BrandID
									WHEN 2000184	THEN CONCAT('furkin/',CAST(@QuoteSessionID AS VARCHAR))
									WHEN 2002314	THEN CONCAT('phi-direct/',CAST(@QuoteSessionID AS VARCHAR))
									ELSE CONCAT('aquarium/',CAST(@QuoteSessionID AS VARCHAR))
									END
			EXEC _C00_SimpleValueIntoField 315923, @RetrievalLink, @LeadID

			--Get the recurring monthly value within the Pet Premium Node where @SELECTedProductRLID ID = PolicySchemeId node value
			SELECT @RecurringMonthlyPrice =	 b.value('(RecurringMonthly)[1]','MONEY') 
			FROM @PetDefXML.nodes('(PetDefinition/Premiums/PetPremium)') a(b) 
			WHERE b.value('(PolicySchemeId)[1]','INT') = @SELECTedProductRLID

			--Get the First monthly value within the Pet Premium Node where @SELECTedProductRLID ID = PolicySchemeId node value
			SELECT @FirstMonthlyPrice=	 b.value('(FirstMonth)[1]','MONEY') 
			FROM @PetDefXML.nodes('(PetDefinition/Premiums/PetPremium)') a(b) 
			WHERE b.value('(PolicySchemeId)[1]','INT') = @SELECTedProductRLID

			--Determine whether the recurring or first month is greater 
			IF @RecurringMonthlyPrice >= @FirstMonthlyPrice
			BEGIN
				SELECT @GreaterPrice = @RecurringMonthlyPrice
			END
			ELSE
			BEGIN
				SELECT @GreaterPrice = @FirstMonthlyPrice
			END

			--SELECT the name of the product based on the resource list ID of the SELECTed product.
			SELECT  @SELECTedProductName = DetailValue FROM ResourceListDetailValues WITH (NOLOCK) WHERE detailfieldid = 180060
			AND ResourceListID = @SELECTedProductRLID
				
			--If the brand is ASDA, make the product color green. If not, keep the colour blue
			IF @BrandID <> 157876
			BEGIN
			  -- Concatanate The Name of the pet and the SELECTed product name with a break in the middle 
			SELECT @ProductAndPetNameConcat =  '<div style="padding:10px"><font size="8" face="Tahoma"><span style="color: rgb(102, 102, 102);font-size:22px">' +@PetNameFromPetDef + '</span><br/>' + '<span style="color: rgb(3, 127, 170); font-size: 20px; line-height: 30px;"><span style="line-height: 30px; font-size: 20px;">' + @SELECTedProductName + '</span></span>' + '</font></div>'
			END 
			ELSE
			BEGIN
				SELECT @ProductAndPetNameConcat =  '<div style="padding:10px"><font size="8" face="Tahoma"><span style="color: rgb(102, 102, 102);font-size:22px">' +@PetNameFromPetDef + '</span><br/>' + '<span style="color: #009943; font-size: 20px; line-height: 30px;"><span style="line-height: 30px; font-size: 20px;">' + @SELECTedProductName + '</span></span>' + '</font></div>'
			END

			--Concatenate the monthly price with 'per month' text preceded by the '£' sign
			SELECT @MonthlyPriceConcat = '<div style="padding:10px"><font size="8" face="Tahoma"><span style="color: white;font-size:26px;font-weight: bold;">£' + CONVERT(varchar(20), @GreaterPrice, 1)  + '</span><br/><span style="color: rgb(255, 255, 255); font-size: 12px; line-height: 30px;"><span style="font-size: 18px; line-height: 30px;">per month</span></span></font></div>'

			--find out the leadid relating to this saved quote
			SELECT @LeadID = ldv.LeadID
			FROM LeadDetailValues ldv WITH ( NOLOCK )
			WHERE ldv.DetailFieldID = 178411 /*SavedQuoteID*/
			AND ldv.ValueInt = @SavedQuoteID

			--If not, create me a LeadID
			IF @LeadID is NULL or @LeadID = 0
			BEGIN
			
				EXEC @LeadID = _C00_CreateNewLeadForCust @CustomerID, @SavedQuoteLeadTypeID, @ClientID, @RunAsUserID, 1, 0, 0
				SELECT @NewLeadCreated = 1
		
			END
		 
			--create a blank table row in the basic table you have created
			INSERT INTO TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID, DenyDelete,DenyEdit)
			VALUES(@ClientID,@LeadID,180164,19153,1,1)

			--get the id of the table row 
			SELECT @TableRowID = SCOPE_IDENTITY()
		
			--insert the pet name, productName, recurringMonthlyPrice
			EXEC dbo._C00_SimpleValueIntoField 180161, @PetNameFromPetDef, @TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180162, @SELECTedProductName, @TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180163, @RecurringMonthlyPrice, @TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180172, @ProductAndPetNameConcat, @TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180173, @MonthlyPriceConcat, @TableRowID


			--Increment the ID and return to the top of the while if the ID is less than the count.
			SELECT @ID = @ID + 1 

		END

		--Now that the while loop has finished and we have our string of pet names, stamp this into the pet name field on the SavedQuote LeadDetails page
		EXEC dbo._C00_SimpleValueIntoField 178416, @StringOfPetNames, @LeadID

		--stamp 'ready to send' with the value 1
		EXEC dbo._C00_SimpleValueIntoField 180182, 1, @LeadID

		--set the 'EmailQueued' flag to '1' once the lead has been created and the xml has been shredded out. 
		--At this point, the email is ready to be sent as the data is all stored in Aquarium. This flag will 
		--be set back to '0' when the email event  has been triggered on the SAE. 
		UPDATE _C600_SavedQuote
		SET EmailQueued = 1 
		WHERE SavedQuoteID = @SavedQuoteID

	END
	


	/*Secret questions and their IDs are returned with the XML response but as a single string each*/
	DECLARE @SecretQuestions TABLE ( RawString VARCHAR(2000), DelimiterCharindex INT, SecretQuestionID INT, SecretQuestion VARCHAR(2000) )
	INSERT @SecretQuestions ( RawString )
	SELECT	 b.value('(.)[1]','VARCHAR(2000)')				[RawString]
	FROM @SavedQuoteXml.nodes('//SecretQuestion/string') a(b)	

	/*Find the position of the delimiter*/
	UPDATE s 
	SET DelimiterCharindex = CHARINDEX('¬',s.RawString,1)
	FROM @SecretQuestions s 
	WHERE s.RawString like '%\¬%' escape '\' /*I don't know whether ¬ is a special character, but escape just in case*/

	UPDATE s 
	SET  SecretQuestionID	= RIGHT(s.RawString,LEN(s.RawString)-s.DelimiterCharindex)
		,SecretQuestion		= LEFT(s.RawString,s.DelimiterCharindex-1)
	FROM @SecretQuestions s 
	WHERE s.DelimiterCharindex > 0
	AND dbo.fnIsInt(RIGHT(s.RawString,LEN(s.RawString)-s.DelimiterCharindex)) = 1

	/*Pick up the question text*/
	SELECT @SecretQuestion = sq.SecretQuestion
	FROM @SecretQuestions sq
	WHERE sq.SecretQuestionID = @SecretQuestionID

	IF @SecretQuestion = '' or @SecretQuestion is NULL
	BEGIN
		SELECT @SecretQuestion = 'QuestionID ' + ISNULL(CONVERT(VARCHAR,@SecretQuestionID),'NULL') + ' not mapped!'
	END

	/*Check whether there's already a Lead for this saved quote*/
	SELECT @LeadID = ldv.LeadID
	FROM LeadDetailValues ldv WITH ( NOLOCK )
	WHERE ldv.DetailFieldID = 178411 /*SavedQuoteID*/
	AND ldv.ValueInt = @SavedQuoteID
	
	/*If not, create one*/
	IF @LeadID is NULL or @LeadID = 0
	BEGIN
		EXEC @LeadID = _C00_CreateNewLeadForCust @CustomerID, @SavedQuoteLeadTypeID, @ClientID, @RunAsUserID, 1, 0, 0
		SELECT @NewLeadCreated = 1
	END
	
	/*Pick up case and matter details as needed*/
	SELECT TOP 1 @CaseID = m.CaseID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.LeadID = @LeadID
	ORDER BY m.MatterID
	
	/*For that lead, collect the appropriate values to create the quote email*/
	DECLARE	 @QuoteReference		VARCHAR(2000)
			,@PetName							VARCHAR(2000)
			,@Title										VARCHAR(2000)
			,@TitleID									VARCHAR(2000)
			,@FirstName							VARCHAR(2000)
			,@LastName							VARCHAR(2000)
			,@Postcode							VARCHAR(2000)
			,@PolicyStartDate					VARCHAR(2000)
			,@BrandingID						INT
			,@Monthly MONEY -- GPR 2018-05-25 C600
			,@ProductID INT  -- GPR 2018-05-25 C600

	SELECT	 @QuoteReference	=	CONVERT(VARCHAR,@SavedQuoteID)
			,@TitleID					=	@SavedQuoteXml.value('(//TitleId)[1]'	,'INT')
			,@FirstName			=	@SavedQuoteXml.value('(//FirstName)[1]'	,'VARCHAR(2000)')
			,@LastName			=	@SavedQuoteXml.value('(//LastName)[1]'	,'VARCHAR(2000)')
			,@Postcode			=	@SavedQuoteXml.value('(//Postcode)[1]'	,'VARCHAR(2000)')
			,@PolicyStartDate	= CONVERT(VARCHAR,ISNULL(@SavedQuoteXml.value('(//StartDate)[1]','DATE'),@SavedQuoteXml.value('(//QuoteStartDate)[1]','DATE')) ,121) -- EQB = QuoteStartDate, IQB = StartDate
			,@BrandingID		=	@SavedQuoteXml.value('(//BrandingId)[1]', 'INT')
			,@Monthly = @SavedQuoteXml.value('(//RecurringMonthly)[1]'	,'DECIMAL') -- GPR 2018-05-25 C600
			,@ProductID = @SavedQuoteXml.value('(//SELECTedProductId)[1]'	,'INT') -- GPR 2018-05-25 C600

		--Do a check to see if @stringofpetnames is = ''
		IF @StringOfPetNames = ''
		BEGIN
			--If yes, find the count of pets
			--Loop through the pets and assign each petname to @stringofpetnames#
			--Declare a table which stores each 'Pet Definition' XML block in its own row.
			DECLARE @PetInfo2 TABLE
			(
			      ID INT IDENTITY,
			      Xml XML
			)
			
			--Store each 'Pet Definition' XML block in its own row.
			INSERT @PetInfo2 (Xml)
			SELECT      node.query('.')
			FROM @SavedQuoteXML.nodes('//PetInfo') AS pet(node)
			
			SELECT @NumberOfPets = COUNT(*) FROM @PetInfo 

			 --SELECT the first element in the @petinfo table
			SELECT TOP 1  @ID = p.ID FROM @PetInfo p 
			ORDER by p.ID asc

			--While the ID is less than or equal to the number of pets
			WHILE @ID <= @NumberOfPets
			BEGIN 
				--SELECT the first xml block
				SELECT @PetDefXML	 = p.xml FROM @PetInfo2 p 
				WHERE p.ID = @ID 

				--Shred the name for the current xml block in the while loop
				SELECT @PetNameFromPetDef = @PetDefXML.value('(//PetName)[1]', 'VARCHAR(2000)') 

				--add it to the string of pet names for all the elements except the last one 
				SELECT @StringOfPetNames = @StringOfPetNames +  @PetNameFromPetDef + ', ' 
				WHERE @ID <> @NumberOfPets

				--if the numberofPets is more than 1, add the word ' and' between the current list of pet names and the last pet name so we get the output  e.g 'petname1, petname2, petname3, and petname4
				IF @NumberOfPets > 1
				BEGIN
					SELECT @StringOfPetNames += 'and ' + @PetNameFromPetDef 
					WHERE @ID = @NumberOfPets
				END
				ELSE
				BEGIN
					SELECT @StringOfPetNames +=  @PetNameFromPetDef 
					WHERE @ID = @NumberOfPets
				END

				-- our string output will now be like "petname1, petname2, petname3, and petname4", we want to remove the comma after the second to last pet name e.g "petname3, and petname4" now translates to "petname3 and petname4"
				SELECT  @StringOfPetNames = REPLACE(@StringOfPetNames, ', and', ' and');

				--Increment the ID and return to the top of the while if the ID is less than the count.
				SELECT @ID = @ID + 1 

			END

			EXEC dbo._C00_SimpleValueIntoField 178416, @StringOfPetNames, @LeadID
	
			--stamp 'ready to send' with the value 1
			EXEC dbo._C00_SimpleValueIntoField 180182, 1, @LeadID

	END


	SELECT @Title = CONVERT(VARCHAR,li.LookupListItemID)
	FROM Titles t WITH ( NOLOCK ) 
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.ItemValue = t.Title AND li.LookupListID = @TitlesLookupListID
	WHERE t.TitleID = @TitleID


	/*Don't stamp a title of "unknown" or NULL*/
	IF @Title is NULL or @Title = 'Unknown' or @Title = '16509' /*Unknown*/
	BEGIN
		SELECT @Title = ''
	END
	
	/*Populate those values (mapped at Lead-level by AHH)*/
	INSERT @DetailValueData ( ClientID, DetailFieldSubType, ObjectID, DetailFieldID, DetailValueID, DetailValue )
	SELECT df.ClientID, df.LeadOrMatter, @LeadID, df.DetailFieldID, ISNULL(ldv.LeadDetailValueID,-1)
		, COALESCE( CASE 
					WHEN df.DetailFieldID =  178410 THEN CAST(@QuoteReference as VARCHAR)		--Quote Reference
					WHEN df.DetailFieldID =  178411 THEN CAST(@QuoteReference as VARCHAR)				--SavedQuoteID
					WHEN df.DetailFieldID =  178412 THEN @SavedQuoteKey				--SavedQuoteKey
					WHEN df.DetailFieldID =  178413 THEN CAST(@DateTimeVarchar as VARCHAR)				--Quote Saved DateTime
					WHEN df.DetailFieldID =  178414 THEN CAST(@ExpiryDateTimeVarchar as VARCHAR)			--Quote Expiry DateTime
					WHEN df.DetailFieldID =  178415 THEN CAST(@QuoteSessionID as VARCHAR)				--QuoteSessionID
					WHEN df.DetailFieldID =  178416 THEN @StringOfPetNames					--Pet Name
					WHEN df.DetailFieldID =  178418 THEN CAST(@Title as VARCHAR)							--Title
					WHEN df.DetailFieldID =  178419 THEN @FirstName					--First Name
					WHEN df.DetailFieldID =  178420 THEN @LastName					--Surname
					WHEN df.DetailFieldID =  178421 THEN @Postcode					--Postcode
					WHEN df.DetailFieldID =  178422 THEN @EmailAddress				--Email address
					WHEN df.DetailFieldID =  178423 THEN @SecretQuestion			--Secret Question
					WHEN df.DetailFieldID =  178424 THEN @SecretAnswer				--Secret Answer
					WHEN df.DetailFieldID =  180026 THEN CAST(@ProductID AS VARCHAR)-- ProductID GPR
					WHEN df.DetailFieldID =  180027 THEN CAST(@Monthly AS VARCHAR) -- Recurring monthly amount GPR
					WHEN df.DetailFieldID =  179911 THEN CAST(@BrandingID as VARCHAR)	
					WHEN df.DetailFieldID = 179910 THEN CAST(@PolicyStartDate as VARCHAR) -- Policy Start Date
					END 
				, ldv.DetailValue
			, '' )
	FROM DetailFields df WITH ( NOLOCK ) 
	LEFT JOIN LeadDetailValues ldv WITH ( NOLOCK ) on ldv.LeadID = @LeadID AND ldv.DetailFieldID = df.DetailFieldID
	WHERE df.DetailFieldPageID = 19153 /*Quotation Details*/
	AND df.QuestionTypeID NOT IN ( 25 ) /*Bon't bother for headers*/

	/*Save and maintain history*/
	IF EXISTS ( SELECT * FROM @DetailValueData )
	BEGIN
		DECLARE @BlackHole TABLE ( PassedID INT, DetailValueID INT )
		INSERT @BlackHole ( PassedID, DetailValueID )
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID
	END

	EXEC _C00_SimpleValueIntoField 315922,@Now,@LeadID /*GPR 2021-05-07*/

	EXEC _C00_SimpleValueIntoField 315924, @NumberOfPets, @LeadID /*NG 2021-04-29 for FURKIN-557*/

	/*Use the AutomatedEventQueue to apply process start because LeadEvents are slooooow...*/
	IF @NewLeadCreated = 1
	BEGIN
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
		SELECT c.ClientID, @CustomerID, c.LeadID, c.CaseID, @SavedQuoteID, 157680, dbo.fn_GetDate_Local(), @RunAsUserID, 157680, @RunAsUserID, 0, 5, 0, 0 /*Process Start*/
		FROM Cases c WITH ( NOLOCK )
		WHERE c.CaseID = @CaseID
	END
	
	DECLARE @BatchDateTime DATETIME = DATEADD(Second,30,dbo.fn_GetDate_Local())	 
	/*commenting out - we want the batch to run on the schedule we have set*/
	/*
	EXEC AutomatedTask__RunNow 21695, 0, @BatchDateTime, 0 /*Send emails for updated quotes*/
	*/
	
	PRINT 'SavedQuote LeadID ' + ISNULL(CONVERT(VARCHAR,@LeadID),'NULL')

	END 	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuote_QueueEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_SaveQuote_QueueEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuote_QueueEmail] TO [sp_executeall]
GO
