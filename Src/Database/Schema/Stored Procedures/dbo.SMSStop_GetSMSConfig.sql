SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--USE [Aquarius]
--GO
--/****** Object:  StoredProcedure [dbo].[SMSStop_GetSMSConfig]    Script Date: 2018-04-17 21:37:45 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


---- =============================================
---- Author:		Thomas Doyle
---- Create date: 2018-04-10
---- Description:	get config for SMS text - should only get called if SMSSTOP Client option is enabled
---- =============================================

CREATE PROCEDURE [dbo].[SMSStop_GetSMSConfig]
(

	  @ClientID			int	 --= 4
	, @EventTypeID		int --= 172999
	, @DocumentTypeID	int --= 70007
	, @LeadTypeID		int --= 1475
	, @CaseID			int = null
	
)

AS
BEGIN
	SET NOCOUNT ON;

	/* Defaults to client wide! */

	DECLARE @SurveyNumber	varchar(50) -- this is used to map an SMS "STOP" response from a customer to a client and a leadtype
	, @SMSTextToAppend		varchar(2000)  = ''
	, @OptOutSMSEventTypeID int  = 0 
	, @EventSubtypeID int
	, @AppendText bit = 1 
	, @SMSQuestionID int 
	, @SMSQuestionnaireID int
	, @SMSQuestionOrder int
	, @MinQuestionOrder int
	, @isClientWideOptOut bit = 1 

	select @isClientWideOptOUT = case S.isLeadTypeSpecific when 0 then 1 else 0 end
	from smsstopconfig s with(nolock) 
	where s.clientid = @ClientID

	/* TODO
		Work out if this SMS event is appropriate to append the SMS STOP text to
	 */
	 SELECT @SMSQuestionID = sqe.SMSQuestionID 
	 , @SMSQuestionnaireID = sq.SMSQuestionnaireID
	 , @SMSQuestionOrder = sq.QuestionOrder
	 FROM [dbo].[SMSQuestionEventType] SQe With(nolock) 
	 INNER JOIN SMSQuestion SQ with(nolock) on sq.SMSQuestionID = sqe.SMSQuestionID
	 WHERE sqe.EventTypeID = @EventTypeID

	 IF @SMSQuestionID > 0  AND @SMSQuestionnaireID > 0 
	 BEGIN
		
		SELECT @MinQuestionOrder = min(sq.QuestionOrder)
		FROM [dbo].[SMSQuestion] sq with(nolock) 
		WHERE sq.SMSQuestionnaireID = @SMSQuestionnaireID
		AND sq.Enabled = 1

		/* is this the first question in the survey? */
		IF @MinQuestionOrder != @SMSQuestionOrder
		BEGIN
			
			/* If not the first question, do not append text to the sms */
			SET @AppendText = 0; 
		END
	 END

	IF @AppendText = 1 
	BEGIN

		if @isclientwideoptout = 1
		begin

			SELECT top 1 @SMSTextToAppend = ' ' + s.smsTextToAppend
			FROM smsstopconfigtext s with(nolock)
			where s.clientid = @ClientID
			order by s.smsstopconfigtextid desc

		end
		else 
		begin

			SELECT top 1 @SMSTextToAppend = ' ' + s.smsTextToAppend
			FROM  smsstopconfigtext s with(nolock)
			where s.clientid = @ClientID
			and s.leadTypeID = @LeadTypeID
			order by s.smsstopconfigtextid desc

		end

		--IF @SurveyNumber is null set @SurveyNumber = '07860025328'

		--SELECT @SMSTextToAppend = 'To stop receiving these messages text STOP to ' + @SurveyNumber

	END
	
	SELECT @SMSTextToAppend [SMSTextToAppend]


END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_GetSMSConfig] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStop_GetSMSConfig] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_GetSMSConfig] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_GetSMSConfig] TO [sp_executehelper]
GO
