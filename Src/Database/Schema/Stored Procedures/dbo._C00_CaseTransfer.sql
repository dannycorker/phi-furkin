SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-09-10
-- Description:	Case Transfer By SAE
-- 2010-04-19 JWG Added WhoCreated
-- 2010-09-21 JWG Added fnRefLetterFromCaseNum for leads with more than 26 matters
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CaseTransfer] 
@LeadEventID int, 
@ClientRelationshipID int, 
@CopyAliasedFields bit,
@AllowMultipleTransfers BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ClientID int,
			@CustomerID int,
			@LeadID int,
			@CaseID int,
			@MatterID int,
			@LeadTypeID int,
			@DestinationClientID int,
			@DestinationCustomerID int,
			@DestinationLeadID int,
			@DestinationCaseID int,
			@DestinationMatterID int,
			@DestinationLeadTypeID int,
			@MatterCount int,
			@TestID int,
			@MatterRef varchar(10),
			@WhoCreated int,
			@CaseTransferDocID int,
			@FromEventTypeID int,
			@ToEventTypeID int,
			@LastOnly bit,
			@ClearRefs bit,
			@RefUser varchar(100),
			@MaintainWhenCreated bit = 0
	
	Select	@ClientID = l.ClientID,
			@CustomerID = l.CustomerID,
			@LeadID = c.LeadID,
			@CaseID = c.CaseID,
			@WhoCreated = le.WhoCreated
	From Leadevent le with (nolock)
	Inner Join Cases c with (nolock) on c.CaseID = le.CaseID
	Inner Join Lead l with (nolock) on l.LeadID = c.LeadID
	Where le.LeadEventID = @LeadEventID

	Select	@DestinationClientID = ReceivingClientID, 
			@DestinationLeadTypeID = IncomingLeadTypeID,
			@LeadTypeID = OutgoingLeadTypeID,
			@ClearRefs = ClearLeadRefs
	From ClientRelationship with (nolock)
	Where ClientRelationshipID = @ClientRelationshipID
	
	SELECT 
		@DestinationClientID			[@DestinationClientID],
		@DestinationLeadTypeID			[@DestinationLeadTypeID],
		@LeadTypeID						[@LeadTypeID],
		@ClearRefs						[@ClearRefs],
		@ClientID						[@ClientID]		
				
	SELECT @MaintainWhenCreated = CASE WHEN @ClientRelationshipID IN(94) THEN 1 ELSE 0 END /*CS for ticket #20216*/
	
	If	((
		Select COUNT(*)
		From CaseTransferMapping ctm with (nolock)
		Where ctm.CaseID = @CaseID
		and ctm.CaseTransferStatusID = 1
		and ctm.ClientRelationshipID not in(93,94,99)
		) = 0 OR @AllowMultipleTransfers = 1)
	Begin
		SELECT 'No CaseTransferStatus = 1 records'

		/* If Client ID Is not the same */
		If @DestinationClientID <> @ClientID
		Begin
			SELECT '@DestinationClientID <> @ClientID'

			/* Check for existing customer */
			Select @DestinationCustomerID = isnull(ctm.NewCustomerID,0), @DestinationLeadID = isnull(NewLeadID,0)
			From CaseTransferMapping ctm with (nolock)
			Where ctm.LeadID = @LeadID
			and ctm.NewLeadTypeID = @DestinationLeadTypeID

			--Select @DestinationCustomerID

			/* Create lead for exsiting customer */
			If @DestinationCustomerID IS NOT NULL
			Begin

				exec @DestinationLeadID = dbo._C00_CreateNewLeadForCust @DestinationCustomerID, @DestinationLeadTypeID, @DestinationClientID, NULL /* Don't know IDs of users at other client */

			End
			
			/* Create Customrer on Client System (If not exists) */
			If @DestinationCustomerID IS NULL
			Begin
			
				exec @DestinationLeadID = dbo._C00_CreateCustomerFromCustomer @CustomerID, @DestinationClientID, @DestinationLeadTypeID, NULL /* Don't know IDs of users at other client */

				IF @ClearRefs = 0
				Begin
				
					Select @RefUser = LeadRef
					From Lead
					Where LeadID = @LeadID
				
					Update Lead 
					Set LeadRef = @RefUser
					where LeadID = @DestinationLeadID
				
				End				
			End

			select @DestinationCustomerID = CustomerID, @DestinationCaseID = CaseID
			From Matter with (nolock)
			Where LeadID = @DestinationLeadID
			Order By MatterID desc
			
		End

		If @DestinationClientID = @ClientID
		Begin
			SELECT '@DestinationClientID = @ClientID'

			Select @DestinationCustomerID = @CustomerID

			Select @DestinationLeadID = LeadID 
			From Lead with (nolock)
			Where CustomerID = @CustomerID
			and Lead.LeadTypeID = @DestinationLeadTypeID
			and @ClientRelationshipID <> 88 /*CS 2012-07-10*/

			If @DestinationLeadID Is Not NULL and @ClientRelationshipID <> 88 /*CS 2012-07-10*/
			Begin

				exec dbo._C00_CreateNewCaseForLead @DestinationLeadID, NULL /* Don't know IDs of users at other client */

			End

			If @DestinationLeadID Is NULL and @ClientRelationshipID <> 88 /*CS 2012-07-10*/
			Begin

				exec @DestinationLeadID = dbo._C00_CreateNewLeadForCust @DestinationCustomerID, @DestinationLeadTypeID, @DestinationClientID, NULL /* Don't know IDs of users at other client */

			End
			
			If @ClientRelationshipID = 88 /*CS 2012-07-10*/
			BEGIN

				exec @DestinationLeadID = dbo._C00_CreateNewLeadForCust @DestinationCustomerID, @DestinationLeadTypeID, @DestinationClientID, NULL, 1 /* CS 2012-07-11 */

			END

		End

		Select top 1 @DestinationCaseID = CaseID, @DestinationMatterID = MatterID
		From Matter with (nolock)
		Where LeadID = @DestinationLeadID
		Order By MatterID Desc

		Select @MatterCount = 1
		
		Select top 1 @MatterID = MatterID
		From Matter with (nolock)
		Where CaseID = @CaseID
		Order By MatterID Asc
		
		Select @TestID = 0
		
		While (@MatterID > 0 and @MatterID > @TestID)
		Begin
		
			If @MatterCount <> 1
			Begin
			
				select @MatterRef = dbo.fnRefLetterFromCaseNum(1 + count(*)) 
				from matter with (nolock)
				where leadid = @DestinationLeadID

				Insert Into Matter(CaseID, ClientID, CustomerID, LeadID, MatterRef, MatterStatus, RefLetter)
				Select @DestinationCaseID, @DestinationClientID, @DestinationCustomerID, @DestinationLeadID, '', '',@MatterRef

				Select @DestinationMatterID = SCOPE_IDENTITY()

				Select @DestinationCaseID = CaseID
				From Matter with (nolock)
				Where MatterID = @DestinationMatterID
				Order By MatterID Desc
		
			End
			
			Select top 1 @DestinationMatterID = MatterID
			From Matter
			Where CaseID = @DestinationCaseID
			order by MatterID desc

			Select @MatterCount = @MatterCount + 1
			
			/* Add CTM Record */
			INSERT INTO CaseTransferMapping(ClientRelationshipID,ClientID,CustomerID,LeadID,CaseID,MatterID,NewClientID,NewCustomerID,NewLeadID,NewCaseID,NewMatterID,CaseTransferStatusID,NewLeadTypeID)
			Select @ClientRelationshipID,@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@DestinationClientID,@DestinationCustomerID,@DestinationLeadID,@DestinationCaseID,@DestinationMatterID,1,@DestinationLeadTypeID

			/* Copy Matter Detail Values By Field Map */
			Insert Into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
			Select ctm.NewClientID, @DestinationLeadID, @DestinationMatterID, ctdfm.DestinationDetailFieldID, DetailValue
			From MatterDetailValues mdv
			Inner Join CaseTransferMapping ctm on ctm.MatterID = mdv.MatterID
			Inner Join _CaseTransferDetailFieldMap ctdfm on ctdfm.ClientRelationshipID = ctm.ClientRelationshipID and ctdfm.OriginatingDetailFieldID = mdv.DetailFieldID
			Inner Join DetailFields df on df.DetailFieldID = ctdfm.DestinationDetailFieldID and df.LeadOrMatter = 2
			Where mdv.MatterID =  @MatterID
			and mdv.DetailValue <> ''
			And Not Exists (
				Select *
				From CaseTransferRestrictedField ctrf 
				Where ctrf.ClientRelationshipID = ctm.ClientRelationshipID
				And ctrf.DetailFieldID = mdv.DetailFieldID
			)
			and not exists(
				select *
				From MatterDetailValues mdvt
				Where mdvt.MatterID = @DestinationMatterID
				And mdvt.DetailFieldID = ctdfm.DestinationDetailFieldID
			)
			and mdv.DetailValue <> ''
			
			/* Insert into MatterdetailValues from Lead Detail Values (Added BY ACE 2010-07-26) */
			Insert Into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
			Select ctm.NewClientID, @DestinationLeadID, @DestinationMatterID, ctdfm.DestinationDetailFieldID, DetailValue
			From LeadDetailValues ldv
			Inner Join CaseTransferMapping ctm on ctm.LeadID = ldv.LeadID
			Inner Join _CaseTransferDetailFieldMap ctdfm on ctdfm.ClientRelationshipID = ctm.ClientRelationshipID and ctdfm.OriginatingDetailFieldID = ldv.DetailFieldID
			Inner Join DetailFields df on df.DetailFieldID = ctdfm.DestinationDetailFieldID and df.LeadOrMatter = 2
			Where ldv.LeadID = @LeadID
			and ldv.DetailValue <> ''
			And Not Exists (
				Select *
				From CaseTransferRestrictedField ctrf 
				Where ctrf.ClientRelationshipID = ctm.ClientRelationshipID
				And ctrf.DetailFieldID = ldv.DetailFieldID
			)
			and not exists(
				select *
				From LeadDetailValues ldvt
				Where ldvt.LeadID = @DestinationLeadID
				And ldvt.DetailFieldID = ctdfm.DestinationDetailFieldID
			)
			and ldv.DetailValue <> ''

			--Select @DestinationLeadID, @DestinationMatterID, @MatterID

			IF @ClearRefs = 0
			Begin
			
				Select @RefUser = MatterRef
				From Matter
				Where MatterID = @MatterID
			
				Update Matter 
				Set MatterRef = @RefUser
				where MatterID = @DestinationMatterID
			
			End
				/* Copy Matter Detail Values By Alias */
			If @CopyAliasedFields = 1
			Begin

				Insert Into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
				Select dfb.ClientID, @DestinationLeadID, @DestinationMatterID, dfb.DetailFieldID, DetailValue
				From MatterDetailValues
				Inner Join DetailFieldAlias dfa On dfa.DetailFieldID = MatterDetailValues.DetailFieldID and dfa.LeadTypeID = @LeadTypeID
				Inner Join DetailFields df on df.DetailFieldID = dfa.DetailFieldID and df.LeadOrMatter = 2
				Inner Join DetailFieldAlias dfb On dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID = @DestinationLeadTypeID
				Inner Join DetailFields df1 on df1.DetailFieldID = dfb.DetailFieldID and df1.QuestionTypeID <> 4  and df1.LeadOrMatter = 2 /* JWG 2011-07-25 LeadOrMatter must match! */
				Where MatterDetailValues.MatterID =  @MatterID
				and MatterDetailValues.DetailValue <> ''
				and not exists(
					select *
					From MatterDetailValues mdvt
					Where mdvt.MatterID = @DestinationMatterID
					And mdvt.DetailFieldID = dfb.DetailFieldID
				)
				And Not Exists (
					Select *
					From CaseTransferRestrictedField ctrf 
					Where ctrf.DetailFieldID = MatterDetailValues.DetailFieldID
					and ctrf.ClientRelationshipID = @ClientRelationshipID 
				)
			and MatterDetailValues.DetailValue <> ''

			End

			Select @TestID = @MatterID

			Select top 1 @MatterID = MatterID
			From Matter with (nolock)
			Where CaseID = @CaseID
			and MatterID > @MatterID
			Order By MatterID Asc
		
		End

		/* Copy Lead Detail Values By Field Map */
		Insert Into LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		Select ctm.NewClientID, @DestinationLeadID, ctdfm.DestinationDetailFieldID, DetailValue
		From LeadDetailValues ldv
		Inner Join CaseTransferMapping ctm on ctm.LeadID = ldv.LeadID
		Inner Join _CaseTransferDetailFieldMap ctdfm on ctdfm.ClientRelationshipID = ctm.ClientRelationshipID and ctdfm.OriginatingDetailFieldID = ldv.DetailFieldID
		Inner Join DetailFields df on df.DetailFieldID = ctdfm.DestinationDetailFieldID and df.LeadOrMatter = 1
		Where ldv.LeadID =  @LeadID
		and ldv.DetailValue <> ''
		And Not Exists (
			Select *
			From CaseTransferRestrictedField ctrf 
			Where ctrf.DetailFieldID = ldv.DetailFieldID
			and ctrf.ClientRelationshipID = @ClientRelationshipID 
		)

		If @CopyAliasedFields = 1
		Begin
		
			/* Copy Lead Detail Values*/
			Insert Into LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
			Select dfb.ClientID, @DestinationLeadID, dfb.DetailFieldID, DetailValue
			From LeadDetailValues
			Inner Join DetailFieldAlias dfa On dfa.DetailFieldID = LeadDetailValues.DetailFieldID and dfa.LeadTypeID = @LeadTypeID
			Inner Join DetailFields df on df.DetailFieldID = dfa.DetailFieldID and df.LeadOrMatter = 1
			Inner Join DetailFieldAlias dfb On dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID = @DestinationLeadTypeID
			Inner Join DetailFields df2 on df2.DetailFieldID = dfb.DetailFieldID and df2.LeadOrMatter = 1 /* JWG 2011-07-25 LeadOrMatter must match! */
			Where LeadDetailValues.LeadID = @LeadID
			and LeadDetailValues.DetailValue <> ''
			and not exists(
				select *
				From LeadDetailValues ldvt
				Where ldvt.LeadID = @DestinationLeadID
				And ldvt.DetailFieldID = dfb.DetailFieldID
			)
			And Not Exists (
				Select *
				From CaseTransferRestrictedField ctrf 
				Where ctrf.DetailFieldID = LeadDetailValues.DetailFieldID
				and ctrf.ClientRelationshipID = @ClientRelationshipID 
			)

		End

		/* Fix Lookup List Items */
		Delete matterdetailvalues
		from matterdetailvalues
		inner join matter m on m.matterid = matterdetailvalues.matterid and m.caseid = @DestinationCaseID
		inner join detailfields df on df.detailfieldid = matterdetailvalues.detailfieldid and df.questiontypeid = 4
		Where isnumeric(matterdetailvalues.detailvalue) = 0 and matterdetailvalues.ValueInt IS NULL
		and encryptedvalue is null

		insert into matterdetailvalues(clientid, leadid, matterid, detailfieldid, detailvalue, errormsg)
		select ctm.NewClientID, ctm.NewLeadID, ctm.NewMatterID, df1_toclient.detailfieldid, luli1_toclient.lookuplistitemid, mdv_fromclient.errormsg
		from detailfields df_fromclient
		inner join casetransfermapping ctm on ctm.caseid = @CaseID
		inner join matterdetailvalues mdv_fromclient on mdv_fromclient.detailfieldid = df_fromclient.detailfieldid and mdv_fromclient.matterid = ctm.matterid
		inner join detailfieldalias dfa_fromclient on dfa_fromclient.detailfieldid = df_fromclient.detailfieldid and df_fromclient.leadormatter = 2
		inner join detailfieldalias dfa2_toclient on dfa2_toclient.DetailFieldAlias = dfa_fromclient.DetailFieldAlias and dfa2_toclient.detailfieldid <> dfa_fromclient.detailfieldid
		inner join detailfields df1_toclient on df1_toclient.detailfieldid = dfa2_toclient.detailfieldid and df1_toclient.questiontypeid = 4 and df1_toclient.leadormatter = 2
		inner join lookuplistitems luli1_toclient on luli1_toclient.lookuplistid = df1_toclient.lookuplistid
		/*
			JWG 2009-12-07 replaced this line, as it crashes when mdv_fromclient.detailvalue contains bad data (eg Kerobo dataloaded data)
			inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then mdv_fromclient.detailvalue else '0' end*/
		inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then mdv_fromclient.ValueInt else 0 end
		where df_fromclient.questiontypeid = 4
		and mdv_fromclient.DetailValue <> ''
		And Not Exists (
		Select *
		From CaseTransferRestrictedField ctrf 
		Where ctrf.DetailFieldID = mdv_fromclient.DetailFieldID
		and ctrf.ClientRelationshipID = @ClientRelationshipID 
		)
		and not exists(
			select *
			From MatterDetailValues mdvt
			Where mdvt.MatterID = @DestinationMatterID
			And mdvt.DetailFieldID = df1_toclient.detailfieldid
		)
		and mdv_fromclient.DetailValue <> ''

		insert into matterdetailvalues(clientid, leadid, matterid, detailfieldid, detailvalue, errormsg)
		select ctm.NewClientID, ctm.NewLeadID, ctm.NewMatterID, df1_toclient.detailfieldid, luli1_toclient.lookuplistitemid, ldv_fromclient.errormsg
		from detailfields df_fromclient
		inner join casetransfermapping ctm on ctm.caseid = @CaseID
		inner join leaddetailvalues ldv_fromclient on ldv_fromclient.detailfieldid = df_fromclient.detailfieldid and ldv_fromclient.leadid = ctm.leadid
		inner join detailfieldalias dfa_fromclient on dfa_fromclient.detailfieldid = df_fromclient.detailfieldid 
		inner join detailfieldalias dfa2_toclient on dfa2_toclient.DetailFieldAlias = dfa_fromclient.DetailFieldAlias and dfa2_toclient.detailfieldid <> dfa_fromclient.detailfieldid
		inner join detailfields df1_toclient on df1_toclient.detailfieldid = dfa2_toclient.detailfieldid 
		inner join lookuplistitems luli1_toclient on luli1_toclient.lookuplistid = df1_toclient.lookuplistid
		inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then ldv_fromclient.valueint else 0 end
		where df_fromclient.questiontypeid = 4
		and ldv_fromclient.DetailValue <> ''
		and df_fromclient.leadormatter = 1
		and df1_toclient.leadormatter = 2
		And Not Exists (
		Select *
		From CaseTransferRestrictedField ctrf 
		Where ctrf.DetailFieldID = ldv_fromclient.DetailFieldID
		and ctrf.ClientRelationshipID = @ClientRelationshipID 
		)
		and not exists(
			select *
			From MatterDetailValues mdvt
			Where mdvt.MatterID = @DestinationMatterID
			And mdvt.DetailFieldID = df1_toclient.detailfieldid
		)

		insert into Leaddetailvalues(clientid, leadid, detailfieldid, detailvalue, errormsg)
		select distinct ctm.NewClientID, ctm.NewLeadID, df1_toclient.detailfieldid, luli1_toclient.lookuplistitemid, ldv_fromclient.errormsg
		from detailfields df_fromclient
		inner join casetransfermapping ctm on ctm.caseid = @CaseID
		inner join leaddetailvalues ldv_fromclient on ldv_fromclient.detailfieldid = df_fromclient.detailfieldid and ldv_fromclient.leadid = ctm.leadid
		inner join detailfieldalias dfa_fromclient on dfa_fromclient.detailfieldid = df_fromclient.detailfieldid 
		inner join detailfieldalias dfa2_toclient on dfa2_toclient.DetailFieldAlias = dfa_fromclient.DetailFieldAlias and dfa2_toclient.detailfieldid <> dfa_fromclient.detailfieldid
		inner join detailfields df1_toclient on df1_toclient.detailfieldid = dfa2_toclient.detailfieldid 
		inner join lookuplistitems luli1_toclient on luli1_toclient.lookuplistid = df1_toclient.lookuplistid
		inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then ldv_fromclient.valueint else 0 end
		where df_fromclient.questiontypeid = 4
		and ldv_fromclient.DetailValue <> ''
		and df_fromclient.leadormatter = 1
		and df1_toclient.leadormatter = 1
		And Not Exists (
		Select *
		From CaseTransferRestrictedField ctrf 
		Where ctrf.DetailFieldID = ldv_fromclient.DetailFieldID
		and ctrf.ClientRelationshipID = @ClientRelationshipID 
		)
		and not exists(
			select *
			From Leaddetailvalues mdvt
			Where mdvt.LeadID = @DestinationLeadID
			And mdvt.DetailFieldID = df1_toclient.detailfieldid
		)
			
		/* Copy Lead Docs By Map */
		/* Need MapID, From EventtypeID, To EventTypeID, LastOnly */
		Select top 1 @CaseTransferDocID = CaseTransferDocID, @TestID = 0, @FromEventTypeID = FromEventTypeID, @ToEventTypeID = ToEventTypeID, @WhoCreated = ApplyEventAs, @LastOnly = LastOnly
		From CaseTransferDocuments
		Where ClientRelationshipID = @ClientRelationshipID
		Order BY CaseTransferDocID

		While @CaseTransferDocID > 0 and @CaseTransferDocID > @TestID 
		Begin

			exec dbo._C00_CaseTransferDocs @CaseID, @FromEventTypeID, @ToEventTypeID, @WhoCreated, @LastOnly, @MaintainWhenCreated /*CS for ticket #20216*/

			Select @TestID = @CaseTransferDocID		
		
			Select top 1 @CaseTransferDocID = CaseTransferDocID, @FromEventTypeID = FromEventTypeID, @ToEventTypeID = ToEventTypeID, @WhoCreated = ApplyEventAs, @LastOnly = LastOnly
			From CaseTransferDocuments
			Where ClientRelationshipID = @ClientRelationshipID
			And CaseTransferDocID > @CaseTransferDocID
			Order BY CaseTransferDocID

		END

	End

	Update LeadEvent
	Set Comments = 'Created new lead '+ CONVERT(varchar,@DestinationLeadID) + ' ' + CHAR(12) + CHAR(13) + Comments
	Where LeadEventID = @LeadEventID

	return @DestinationCaseID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransfer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CaseTransfer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransfer] TO [sp_executeall]
GO
