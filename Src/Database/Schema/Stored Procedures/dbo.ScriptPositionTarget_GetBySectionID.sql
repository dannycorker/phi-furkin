SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptPositionTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_GetBySectionID]
(

	@SectionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptPositionTargetID],
					[ScriptID],
					[SectionID],
					[ClientID],
					[PositionName],
					[LinkText]
				FROM
					[dbo].[ScriptPositionTarget] WITH (NOLOCK) 
				WHERE
					[SectionID] = @SectionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_GetBySectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_GetBySectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_GetBySectionID] TO [sp_executeall]
GO
