SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Dashboard table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_Update]
(

	@DashboardID int   ,

	@DashboardName nchar (10)  ,

	@DashboardDescription nchar (10)  ,

	@OwnerID nchar (10)  ,

	@DashboardLayoutID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Dashboard]
				SET
					[DashboardName] = @DashboardName
					,[DashboardDescription] = @DashboardDescription
					,[OwnerID] = @OwnerID
					,[DashboardLayoutID] = @DashboardLayoutID
					,[ClientID] = @ClientID
				WHERE
[DashboardID] = @DashboardID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Update] TO [sp_executeall]
GO
