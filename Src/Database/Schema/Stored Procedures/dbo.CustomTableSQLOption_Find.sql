SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CustomTableSQLOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomTableSQLOption_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomTableSQLOptionID int   = null ,

	@ClientID int   = null ,

	@DetailFieldID int   = null ,

	@OptionType varchar (100)  = null ,

	@OptionValue varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomTableSQLOptionID]
	, [ClientID]
	, [DetailFieldID]
	, [OptionType]
	, [OptionValue]
    FROM
	[dbo].[CustomTableSQLOption] WITH (NOLOCK) 
    WHERE 
	 ([CustomTableSQLOptionID] = @CustomTableSQLOptionID OR @CustomTableSQLOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([OptionType] = @OptionType OR @OptionType IS NULL)
	AND ([OptionValue] = @OptionValue OR @OptionValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomTableSQLOptionID]
	, [ClientID]
	, [DetailFieldID]
	, [OptionType]
	, [OptionValue]
    FROM
	[dbo].[CustomTableSQLOption] WITH (NOLOCK) 
    WHERE 
	 ([CustomTableSQLOptionID] = @CustomTableSQLOptionID AND @CustomTableSQLOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([OptionType] = @OptionType AND @OptionType is not null)
	OR ([OptionValue] = @OptionValue AND @OptionValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQLOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Find] TO [sp_executeall]
GO
