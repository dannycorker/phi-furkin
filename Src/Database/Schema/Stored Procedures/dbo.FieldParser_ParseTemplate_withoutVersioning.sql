SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 -- Stored Procedure
 
 -- =============================================
 -- Author:		Jim Green
 -- Create date: 2010-07-22
 -- Description:	Parse all objects for a template. Returns a table of substitutions to make.
 -- JWG 2010-12-24 Added simple multiple-recipient email address handling
 -- JWG 2011-01-11 Added Customer/Case/Client/ClientPersonnel/ContactDetailValues
 -- JWG 2012-01-16 Added SubClientCompanyName (for HR)
 -- PR  2013-04-29 Added Hyperlink detail field types
 -- PR  2014-06-24 Added format column
 -- JWG 2014-06-24 #27329 Added translations throughout
 -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types
 -- =============================================
 CREATE PROCEDURE [dbo].[FieldParser_ParseTemplate_withoutVersioning]
 @ClientPersonnelID int,
 @LeadID int,
 @CaseID int,
 @DocumentTypeID int = null,
 @PageTitleLeadTypeID int = null,
 @HyperlinkDetailFieldID int = null
 --WITH RECOMPILE
 AS
 BEGIN
 SET NOCOUNT ON;
 
 DECLARE @LeadTypeID int,
 @ClientID int,
 @MatterID int,
 @ClientOfficeID int,
 @CaseOwnerID int,
 @CaseOwnerManagerID int,
 @ClientPersonnelManagerID int,
 @ClientPersonnelManagerOfficeID int,
 @ContactID int,
 @CustomerID int,
 @LanguageID int = 1,
 @CustomerOfficeID int,
 @DepartmentID int,
 @PartnerID int,
 /* Email-out multiple-recipient substitutions */
 @LeadOrMatterDetailFieldID int,
 @LeadOrMatter int,
 @ColumnDetailFieldID int,
 @DMRTID int = 0,
 @DMRTarget varchar(250),
 @EmailAddressList varchar(2000) = '',
 @SubClientCompanyName varchar(100) = '' ,
 @UnsubscribeWording VARCHAR(2000) = 'Unsubscribe'
 
 
 DECLARE @Subs TABLE (
 LeadOrMatter tinyint null,
 MatterID int null,
 MultiMatter bit null,
 DetailFieldID int null,
 RawName varchar(2000),
 [Target] varchar(2000),
 RealValue varchar(2000),
 IsSpecial bit,
 ExcelSheetLocation varchar(50),
 Format VARCHAR(2000)
 )
 
 
 SELECT @ClientID = cp.ClientID,
 @ClientOfficeID = cp.ClientOfficeID,
 @ClientPersonnelManagerID = cp.ManagerID,
 @ClientPersonnelManagerOfficeID = cpm.ClientOfficeID
 FROM dbo.ClientPersonnel cp WITH (NOLOCK)
 LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = cp.ManagerID
 WHERE cp.ClientPersonnelID = @ClientPersonnelID
 
 /* -- JWG 2015-02-25 #31028 Bugfix aliased hyperlinks at Customer level as Helper fields, where LeadID is not set */
 IF @LeadID = 0 AND @CaseID > 0 AND @HyperlinkDetailFieldID > 0
 BEGIN
 SELECT @LeadID = ca.LeadID
 FROM dbo.Cases ca WITH (NOLOCK)
 WHERE ca.CaseID = @CaseID
 END
 
 /*
 Get TOP (1) MatterID for the case.
 This is the default matter in case the template uses matter fields but
 the client happens to have multiple matters per case.
 */
 SELECT TOP (1)
 @ContactID = c.DefaultContactID,
 @CustomerID = c.CustomerID,
 @LanguageID = ISNULL(c.LanguageID, 1), /* JWG 2014-06-24 #27329 Default to English */
 @MatterID = m.MatterID,
 @CustomerOfficeID = c.DefaultOfficeID,
 @LeadTypeID = l.LeadTypeID,
 @PartnerID = p.PartnerID,
 @CaseOwnerID = l.AssignedTo,
 @CaseOwnerManagerID = cpm.ManagerID
 FROM dbo.Lead l WITH (NOLOCK)
 INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
 INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID /*AND ca.CaseID = @CaseID  */
 INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = ca.CaseID
 LEFT JOIN dbo.[Partner] p WITH (NOLOCK) ON p.CustomerID = c.CustomerID
 LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = l.AssignedTo
 WHERE ca.CaseID = @CaseID/*l.LeadID = @LeadID */
 ORDER BY m.RefLetter
 
 IF @LeadTypeID = 1875
 BEGIN
 
 SELECT @UnsubscribeWording = 'Abbestellen'
 
 END
 
 /*******************************************************************************************************************/
 /*                                                                                                                 */
 /*                                                   DOCUMENT TYPE                                                 */
 /*                                                                                                                 */
 /*******************************************************************************************************************/
 IF @DocumentTypeID IS NOT NULL
 BEGIN
 /* LeadDetailValues */
 /* Fields may refer to a LookupList, in which case get the LookupListItems.ItemValue instead of the int. */
 /* JWG 2014-06-24 #27329 LEFT JOIN dbo.Translation throughout, in case the LookupListItem needs to be shown in a foreign language. */
 /* This is done by the COALESCE (t.Translation, luli1.ItemValue, '') in the SELECT statement. */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 1 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE convert(varchar, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 1
 LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased LeadDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 1 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE convert(varchar, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 1
 LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 /* MatterDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 2 as [LeadOrMatter],
 COALESCE(mdv1.MatterID, @MatterID) as [MatterID],
 CASE WHEN ddft.[Target] like '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 2
 LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID
 AND mdv1.LeadID = @LeadID
 AND (
 mdv1.MatterID = @MatterID
 OR (
 ddft.[Target] like '\[\[!%' ESCAPE '\'
 AND mdv1.MatterID IN (
 SELECT m.MatterID
 FROM dbo.Matter m WITH (NOLOCK)
 WHERE m.CaseID = @CaseID
 )
 )
 )
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased MatterDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 2 as [LeadOrMatter],
 COALESCE(mdv1.MatterID, @MatterID) as [MatterID],
 CASE WHEN ddft.[Target] like '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 2
 LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID
 AND mdv1.LeadID = @LeadID
 AND (
 mdv1.MatterID = @MatterID
 OR (
 ddft.[Target] like '\[\[!%' ESCAPE '\'
 AND mdv1.MatterID IN (
 SELECT m.MatterID
 FROM dbo.Matter m WITH (NOLOCK)
 WHERE m.CaseID = @CaseID
 )
 )
 )
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 /* CustomerDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 10 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 10
 LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased CustomerDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 10 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 and dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 10
 LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 /* CaseDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 11 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 11
 LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased CaseDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 11 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID and dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 11
 LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 /* ClientDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 12 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 12
 LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased ClientDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 12 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 1 and dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 12
 LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 /* ClientPersonnelDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 13 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 13
 LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased ClientPersonnelDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 13 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 2 and dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 13
 LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 /* ContactDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 14 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 14
 LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 /* Aliased ContactDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format)
 SELECT 14 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0,
 ddft.ExcelSheetLocation,
 ddft.Format
 FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 3 and dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 14
 LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.DocumentTypeID = @DocumentTypeID
 
 
 END
 
 
 /*******************************************************************************************************************/
 /*                                                                                                                 */
 /*                                                LEAD TYPE PAGE TITLE                                             */
 /*                                                                                                                 */
 /*******************************************************************************************************************/
 IF @PageTitleLeadTypeID IS NOT NULL
 BEGIN
 /* LeadDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 1 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE convert(varchar, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 1
 LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased LeadDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 1 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE convert(varchar, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 1
 LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 
 /* MatterDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 2 as [LeadOrMatter],
 COALESCE(mdv1.MatterID, @MatterID) as [MatterID],
 CASE WHEN ddft.[Target] like '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 2
 LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] like '\[\[!%' ESCAPE '\')
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased MatterDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 2 as [LeadOrMatter],
 COALESCE(mdv1.MatterID, @MatterID) as [MatterID],
 CASE WHEN ddft.[Target] like '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 2
 LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] like '\[\[!%' ESCAPE '\')
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 
 /* CustomerDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 10 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 10
 LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased CustomerDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 10 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 10
 LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 
 /* CaseDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 11 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 11
 LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased CaseDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 11 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 11
 LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 
 /* ClientDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 12 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 12
 LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased ClientDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 12 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 12
 LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 
 /* ClientPersonnelDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 13 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 13
 LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased ClientPersonnelDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 13 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 13
 LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 
 /* ContactDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 14 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 14
 LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 /* Aliased ContactDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 14 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE convert(varchar, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END as [DetailValue],
 0
 FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 14
 LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID
 LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt
 LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
 WHERE ddft.LeadTypeID = @PageTitleLeadTypeID
 
 END
 
 
 /*******************************************************************************************************************/
 /*                                                                                                                 */
 /*											  HYPERLINK DETAIL FIELD TYPES										   */
 /*                                                                                                                 */
 /*******************************************************************************************************************/
 
 IF @HyperlinkDetailFieldID IS NOT NULL
 BEGIN
 
 /* LeadDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 1 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (ldv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 1
 LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased LeadDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 1 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (ldv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 1
 LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* MatterDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 2 as [LeadOrMatter],
 COALESCE(mdv1.MatterID, @MatterID) as [MatterID],
 CASE WHEN ddft.[Target] like '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (mdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 2
 LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] like '\[\[!%' ESCAPE '\')
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased MatterDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 2 as [LeadOrMatter],
 COALESCE(mdv1.MatterID, @MatterID) as [MatterID],
 CASE WHEN ddft.[Target] like '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (mdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 2
 LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] like '\[\[!%' ESCAPE '\')
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* CustomerDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 10 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') as [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 10
 LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased CustomerDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 10 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 10
 LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* CaseDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 11 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 11
 LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased CaseDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 11 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 11
 LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* ClientDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 12 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 12
 LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased ClientDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 12 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 1 AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 12
 LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* ClientPersonnelDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 13 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 13
 LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased ClientPersonnelDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 13 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 2 AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 13
 LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* ContactDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 14 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 ddft.[DetailFieldID],
 ddft.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 14
 LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 /* Aliased ContactDetailValues */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial)
 SELECT 14 as [LeadOrMatter],
 0 as [MatterID],
 0 as [MultiMatter],
 df1.[DetailFieldID],
 df1.[DetailFieldID],
 ddft.[Target],
 COALESCE (cdv1.DetailValue, '') AS [DetailValue],
 0
 FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK)
 INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 3 AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types
 INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 14
 LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID
 WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 
 END
 
 /*******************************************************************************************************************/
 /*                                                                                                                 */
 /*                                                    COMMON CODE                                                  */
 /*                                                                                                                 */
 /*******************************************************************************************************************/
 /* Cases */
 IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Cases')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotCases(@CaseID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Cases')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotCases(@CaseID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Cases')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotCases(@CaseID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName
 END
 
 /* ClientOffices */
 IF @ClientOfficeID > 0
 BEGIN
 IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientOffices')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClientOffices(@ClientOfficeID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientOffices')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientOffices(@ClientOfficeID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientOffices')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientOffices(@ClientOfficeID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* ClientOffices - Manager's Office */
 IF @ClientPersonnelManagerOfficeID > 0
 BEGIN
 IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientOffices')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClientOffices(@ClientPersonnelManagerOfficeID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientOffices')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientOffices(@ClientPersonnelManagerOfficeID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientOffices')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientOffices(@ClientPersonnelManagerOfficeID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* ClientPersonnel */
 IF @ClientPersonnelID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwner%'
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwner%'
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwner%'
 END
 END
 /* ClientPersonnel - Manager */
 IF @ClientPersonnelManagerID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelManagerID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%'
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelManagerID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%'
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelManagerID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%'
 END
 END
 /* ClientPersonnel - CaseOwner (Lead.AssignedTo */
 IF @CaseOwnerID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] like '[[]!CaseOwner%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] like '[[]!CaseOwner%'
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] like '[[]!CaseOwner%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] like '[[]!CaseOwner%'
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] like '[[]!CaseOwner%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] like '[[]!CaseOwner%'
 END
 END
 /* ClientPersonnel - CaseOwnerManager (Lead.AssignedTo */
 IF @CaseOwnerManagerID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] like '[[]!CaseOwnerManager%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerManagerID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] like '[[]!CaseOwnerManager%'
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] like '[[]!CaseOwnerManager%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerManagerID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] like '[[]!CaseOwnerManager%'
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] like '[[]!CaseOwnerManager%')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerManagerID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] like '[[]!CaseOwnerManager%'
 END
 END
 /* Clients */
 IF @ClientID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Clients')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotClients(@ClientID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Clients')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClients(@ClientID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Clients')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotClients(@ClientID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* Contact */
 IF @ContactID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Contact')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotContact(@ContactID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Contact')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotContact(@ContactID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Contact')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotContact(@ContactID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* CustomerOffice */
 IF @CustomerOfficeID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'CustomerOffice')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotCustomerOffice(@CustomerOfficeID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'CustomerOffice')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotCustomerOffice(@CustomerOfficeID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'CustomerOffice')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotCustomerOffice(@CustomerOfficeID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* Customers */
 IF @CustomerID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Customers')
 BEGIN
 /* For SMS templates only, keep mobile number in [square brackets] for the app */
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], CASE
 WHEN dst.[Target] like '\[\[!Mobile\]\]' escape '\' AND dst.TemplateTypeID = 5 AND dst.IsSpecial = 0
 THEN '[' + f.RealValue + ']'
 WHEN dst.[Target] like '\[\[!Email\]\]' escape '\' AND dst.TemplateTypeID IN (5, 6, 7) AND dst.IsSpecial = 0
 THEN '[' + f.RealValue + ']'
 ELSE f.RealValue END,
 dst.IsSpecial,
 dst.ExcelSheetLocation
 --SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial
 FROM dbo.fnUnpivotCustomers(@CustomerID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Customers')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotCustomers(@CustomerID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Customers')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotCustomers(@CustomerID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* Department */
 IF @DepartmentID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Department')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotDepartment(@DepartmentID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Department')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotDepartment(@DepartmentID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Department')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotDepartment(@DepartmentID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName
 END
 END
 /* DocumentType - no implementation for PageTitleLeadTypeID and HyperlinkDetailFieldID here */
 IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'DocumentType')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotDocumentType(@DocumentTypeID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'DocumentType' AND dst.PropertyName = f.RawColumnName
 END
 
 
 /* Lead - LeadID is always present */
 IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Lead')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotLead(@LeadID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Lead')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotLead(@LeadID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Lead')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotLead(@LeadID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName
 END
 
 /* LeadType - always present. Even if @PageTitleLeadTypeID is passed in, it will be the same as @LeadTypeID */
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'LeadType')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotLeadType(@LeadTypeID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'LeadType')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotLeadType(@LeadTypeID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'LeadType')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotLeadType(@LeadTypeID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName
 END
 
 /* Matter */
 IF @MatterID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Matter')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotMatter(@MatterID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName
 AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\' /* Exclude multi [[!MatterID]] and [[!MatterRef]] from this single result set */
 
 /* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT 2, m.MatterID, 1, 'MatterID', dst.[Target], m.MatterID, 0, dst.ExcelSheetLocation
 FROM dbo.DocumentStandardTarget dst WITH (NOLOCK)
 CROSS JOIN dbo.Matter m WITH (NOLOCK)
 WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\'
 AND m.CaseID = @CaseID
 
 /* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT 2, m.MatterID, 1, 'MatterRef', dst.[Target], COALESCE(m.MatterRef, ''), 0, dst.ExcelSheetLocation
 FROM dbo.DocumentStandardTarget dst WITH (NOLOCK)
 CROSS JOIN dbo.Matter m WITH (NOLOCK)
 WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\'
 AND m.CaseID = @CaseID
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotMatter(@MatterID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName
 AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\' /* Exclude multi [[!MatterID]] and [[!MatterRef]] from this single result set */
 
 /* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial)
 SELECT 2, m.MatterID, 1, 'MatterID', dst.[Target], m.MatterID, 0
 FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK)
 CROSS JOIN dbo.Matter m WITH (NOLOCK)
 WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\'
 AND m.CaseID = @CaseID
 
 /* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial)
 SELECT 2, m.MatterID, 1, 'MatterRef', dst.[Target], COALESCE(m.MatterRef, ''), 0
 FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK)
 CROSS JOIN dbo.Matter m WITH (NOLOCK)
 WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\'
 AND m.CaseID = @CaseID
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotMatter(@MatterID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName
 AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\' /* Exclude multi [[!MatterID]] and [[!MatterRef]] from this single result set */
 
 /* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial)
 SELECT 2, m.MatterID, 1, 'MatterID', dst.[Target], m.MatterID, 0
 FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK)
 CROSS JOIN dbo.Matter m WITH (NOLOCK)
 WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\'
 AND m.CaseID = @CaseID
 
 /* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
 INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial)
 SELECT 2, m.MatterID, 1, 'MatterRef', dst.[Target], COALESCE(m.MatterRef, ''), 0
 FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK)
 CROSS JOIN dbo.Matter m WITH (NOLOCK)
 WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\'
 AND m.CaseID = @CaseID
 END
 END
 
 
 /* Partner */
 IF @PartnerID > 0
 BEGIN
 IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Partner')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.fnUnpivotPartner(@PartnerID) f
 INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName
 END
 IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Partner')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotPartner(@PartnerID) f
 INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName
 END
 IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Partner')
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.RawColumnName, dst.[Target], f.RealValue, 0
 FROM dbo.fnUnpivotPartner(@PartnerID) f
 INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName
 END
 END
 
 
 /* Special Fields - these all have a blank ObjectName */
 /*INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT dst.[Target], dst.[Target], ''
 FROM dbo.DocumentStandardTarget dst WITH (NOLOCK)
 WHERE dst.DocumentTypeID = @DocumentTypeID
 AND dst.ObjectName = ''*/
 
 
 IF @DocumentTypeID > 0
 BEGIN
 /*
 Substitute Lead security hash code into hyperlinks etc
 'http://www.thejetservice.com/supplierProfile.html?pl=true&cid=1255477&h=0x01765441913909.00'
 */
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT dst.[Target], dst.[Target], dbo.fnHashLeadID(@LeadID), dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.DocumentStandardTarget dst WITH (NOLOCK)
 WHERE dst.DocumentTypeID = @DocumentTypeID
 AND dst.[Target] LIKE '\[!LeadSecurityHash\]' ESCAPE '\'
 
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT dst.[Target], dst.[Target], '<a href=' + dbo.fnGenerateUnsubscribeLink(@LeadID) + '>' + @UnsubscribeWording + '</a>', dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.DocumentStandardTarget dst WITH (NOLOCK)
 WHERE dst.DocumentTypeID = @DocumentTypeID
 AND dst.[Target] LIKE '\[!UnsubscribeLink\]' ESCAPE '\'
 END
 
 
 /*
 SubClient is an unusual requirement - parse the CompanyName if applicable.
 */
 IF EXISTS
 (
 SELECT *
 FROM dbo.DocumentStandardTarget f WITH (NOLOCK)
 WHERE f.ObjectName = 'SubClient'
 AND f.PropertyName = 'CompanyName'
 AND f.DocumentTypeID = @DocumentTypeID
 )
 BEGIN
 /* Get the SubClientID from the Customer and look up the name in the SubClient table */
 SELECT @SubClientCompanyName = sc.CompanyName
 FROM dbo.Customers c WITH (NOLOCK)
 INNER JOIN dbo.SubClient sc WITH (NOLOCK) ON sc.SubClientID = c.SubClientID
 WHERE c.CustomerID = @CustomerID
 
 /* Pretend that this is a standard substitution */
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT f.[Target], f.[Target], IsNull(@SubClientCompanyName, ''), 0
 FROM dbo.DocumentStandardTarget f WITH (NOLOCK)
 WHERE f.ObjectName = 'SubClient'
 AND f.PropertyName = 'CompanyName'
 AND f.DocumentTypeID = @DocumentTypeID
 END
 
 
 /* Missing Fields - anything that failed to be resolved by the select statements above (eg this Customer has no Partner) */
 IF @DocumentTypeID > 0
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation)
 SELECT dst.[Target], dst.[Target], '', dst.IsSpecial, dst.ExcelSheetLocation
 FROM dbo.DocumentStandardTarget dst WITH (NOLOCK)
 WHERE dst.DocumentTypeID = @DocumentTypeID
 AND NOT EXISTS
 (
 SELECT *
 FROM @Subs s
 WHERE s.[Target] = dst.[Target]
 )
 END
 IF @PageTitleLeadTypeID > 0
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT dst.[Target], dst.[Target], '', 0
 FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK)
 WHERE dst.LeadTypeID = @PageTitleLeadTypeID
 AND NOT EXISTS
 (
 SELECT *
 FROM @Subs s
 WHERE s.[Target] = dst.[Target]
 )
 END
 IF @HyperlinkDetailFieldID > 0
 BEGIN
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT dst.[Target], dst.[Target], '', 0
 FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK)
 WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID
 AND NOT EXISTS
 (
 SELECT *
 FROM @Subs s
 WHERE s.[Target] = dst.[Target]
 )
 END
 
 
 /* Set Titles to real values from IDs */
 UPDATE @Subs
 SET RealValue = t.Title
 FROM @Subs s
 INNER JOIN dbo.Titles t ON t.TitleID = s.RealValue
 WHERE s.[RawName] = 'TitleID'
 
 
 /* Multiple Recipient substitutions for email out TO, CC and BCC fields */
 IF @ClientID IN (4, 180)
 BEGIN
 WHILE @DMRTID IS NOT NULL
 BEGIN
 SELECT TOP 1
 @LeadOrMatterDetailFieldID = dmrt.DetailFieldID,
 @ColumnDetailFieldID = dmrt.ColumnField,
 @LeadOrMatter = df.LeadOrMatter,
 @DMRTID = dmrt.DocumentMRTargetID,
 @DMRTarget = dmrt.[Target]
 FROM dbo.DocumentMRTarget dmrt WITH (NOLOCK)
 INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dmrt.DetailFieldID
 WHERE dmrt.DocumentTypeID = @DocumentTypeID
 AND dmrt.TemplateTypeID = 5 /* Email Out */
 AND dmrt.DocumentMRTargetID > @DMRTID
 ORDER BY dmrt.DocumentMRTargetID ASC
 
 IF @@ROWCOUNT = 1
 BEGIN
 /*
 Create a list of email addresses in the form:
 
 jim.green@aquarium-software.com,nessa.green@aquarium-software.com,
 
 The trailing comma will be removed right at the end
 */
 
 /* Lead TableDetailValues*/
 IF @LeadOrMatter = 1
 BEGIN
 SELECT @EmailAddressList += tdv.DetailValue + ','
 FROM dbo.TableRows tr WITH (NOLOCK)
 INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
 WHERE tr.LeadID = @LeadID
 AND tr.DetailFieldID = @LeadOrMatterDetailFieldID
 END
 
 /* Matter TableDetailValues*/
 IF @LeadOrMatter = 2
 BEGIN
 SELECT @EmailAddressList += tdv.DetailValue + ','
 FROM dbo.Matter m WITH (NOLOCK)
 INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID AND tr.DetailFieldID = @LeadOrMatterDetailFieldID
 INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
 WHERE m.CaseID = @CaseID
 END
 
 /* Remove trailing comma */
 IF LEN(@EmailAddressList) > 0
 BEGIN
 SELECT @EmailAddressList = LEFT(@EmailAddressList, LEN(@EmailAddressList) - 1)
 END
 
 /* Pretend that this is a standard substitution */
 INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial)
 SELECT @DMRTarget, @DMRTarget, REPLACE(@DMRTarget, @DMRTarget, @EmailAddressList), 0
 END
 ELSE
 BEGIN
 SET @DMRTID = NULL
 END
 END
 END
 
 
 /* Insert any missing values now */
 /*SELECT DISTINCT s.*, datalength(s.[Target]) as [TargetLength]
 FROM @Subs s
 ORDER BY datalength([Target]) desc, [DetailFieldID], [MatterID] asc*/
 SELECT DISTINCT s.*, datalength(s.[Target]) as [TargetLength], ISNULL(s.ExcelSheetLocation, '') as [ExcelSortOrder]
 FROM @Subs s
 ORDER BY datalength([Target]) DESC, [DetailFieldID] ASC, [ExcelSortOrder] ASC, [MatterID] ASC
 END
 
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_ParseTemplate_withoutVersioning] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldParser_ParseTemplate_withoutVersioning] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_ParseTemplate_withoutVersioning] TO [sp_executeall]
GO
