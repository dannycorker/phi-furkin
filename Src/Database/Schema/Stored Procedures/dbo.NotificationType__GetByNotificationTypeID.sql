SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Gets a Notification Type by its identity
-- =============================================
CREATE PROCEDURE [dbo].[NotificationType__GetByNotificationTypeID]

	@NotificationTypeID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * FROM NotificationType WITH (NOLOCK) 
	WHERE NotificationTypeID = @NotificationTypeID
	

END





GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__GetByNotificationTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationType__GetByNotificationTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__GetByNotificationTypeID] TO [sp_executeall]
GO
