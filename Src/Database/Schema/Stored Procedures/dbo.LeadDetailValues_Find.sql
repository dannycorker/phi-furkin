SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadDetailValues table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDetailValues_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadDetailValueID int   = null ,

	@ClientID int   = null ,

	@LeadID int   = null ,

	@DetailFieldID int   = null ,

	@DetailValue varchar (2000)  = null ,

	@ErrorMsg varchar (1000)  = null ,

	@OriginalDetailValueID int   = null ,

	@OriginalLeadID int   = null ,

	@EncryptedValue varchar (3000)  = null ,

	@ValueInt int   = null ,

	@ValueMoney money   = null ,

	@ValueDate date   = null ,

	@ValueDateTime datetime2   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadDetailValueID]
	, [ClientID]
	, [LeadID]
	, [DetailFieldID]
	, [DetailValue]
	, [ErrorMsg]
	, [OriginalDetailValueID]
	, [OriginalLeadID]
	, [EncryptedValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [SourceID]
    FROM
	[dbo].[LeadDetailValues] WITH (NOLOCK) 
    WHERE 
	 ([LeadDetailValueID] = @LeadDetailValueID OR @LeadDetailValueID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([DetailValue] = @DetailValue OR @DetailValue IS NULL)
	AND ([ErrorMsg] = @ErrorMsg OR @ErrorMsg IS NULL)
	AND ([OriginalDetailValueID] = @OriginalDetailValueID OR @OriginalDetailValueID IS NULL)
	AND ([OriginalLeadID] = @OriginalLeadID OR @OriginalLeadID IS NULL)
	AND ([EncryptedValue] = @EncryptedValue OR @EncryptedValue IS NULL)
	AND ([ValueInt] = @ValueInt OR @ValueInt IS NULL)
	AND ([ValueMoney] = @ValueMoney OR @ValueMoney IS NULL)
	AND ([ValueDate] = @ValueDate OR @ValueDate IS NULL)
	AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadDetailValueID]
	, [ClientID]
	, [LeadID]
	, [DetailFieldID]
	, [DetailValue]
	, [ErrorMsg]
	, [OriginalDetailValueID]
	, [OriginalLeadID]
	, [EncryptedValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [SourceID]
    FROM
	[dbo].[LeadDetailValues] WITH (NOLOCK) 
    WHERE 
	 ([LeadDetailValueID] = @LeadDetailValueID AND @LeadDetailValueID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([DetailValue] = @DetailValue AND @DetailValue is not null)
	OR ([ErrorMsg] = @ErrorMsg AND @ErrorMsg is not null)
	OR ([OriginalDetailValueID] = @OriginalDetailValueID AND @OriginalDetailValueID is not null)
	OR ([OriginalLeadID] = @OriginalLeadID AND @OriginalLeadID is not null)
	OR ([EncryptedValue] = @EncryptedValue AND @EncryptedValue is not null)
	OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
	OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
	OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
	OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues_Find] TO [sp_executeall]
GO
