SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the ClientQuestionnaires table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaires_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ClientQuestionnaireID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([ClientQuestionnaireID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ClientQuestionnaireID]'
				SET @SQL = @SQL + ' FROM [dbo].[ClientQuestionnaires] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[ClientQuestionnaireID], O.[ClientID], O.[QuestionnaireTitle], O.[QuestionnaireDescription], O.[QuestionsPerPage], O.[QuestionnaireLogo], O.[QuestionnaireLogoFileName], O.[QuestionnaireFlowDiagram], O.[QuestionnaireIntroductionText], O.[HideIntro], O.[QuestionnaireFooterText], O.[HideFooter], O.[QuestionnaireAddressLookup], O.[Published], O.[CustomerInformationAtStart], O.[LinkedQuestionnaireClientQuestionnaireID], O.[QuestionnaireFooterIframe], O.[QuestionnaireHeaderIframe], O.[QuestionnaireFooterInternal], O.[QuestionnaireHeaderInternal], O.[QuestionnaireFooterIframeHeight], O.[QuestionnaireHeaderIframeHeight], O.[QuestionnaireFooterIframeWidth], O.[QuestionnaireHeaderIframeWidth], O.[QuestionnaireBackGroundImage], O.[QuestionnaireBackGroundImageFileName], O.[DefaultEmailAddress], O.[MailingListType], O.[FrameMode], O.[LayoutCss], O.[LayoutCssFileName], O.[ImportDirectlyIntoLeadManager], O.[RunAsClientPersonnelID], O.[RememberAnswers], O.[SourceID], O.[WhoCreated], O.[WhenCreated], O.[WhoModified], O.[WhenModified]
				FROM
				    [dbo].[ClientQuestionnaires] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[ClientQuestionnaireID] = PageIndex.[ClientQuestionnaireID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ClientQuestionnaires] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_GetPaged] TO [sp_executeall]
GO
