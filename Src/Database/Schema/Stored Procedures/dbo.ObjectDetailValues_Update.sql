SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ObjectDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectDetailValues_Update]
(

	@ObjectDetailValueID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@ObjectID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ObjectDetailValues]
				SET
					[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[ObjectID] = @ObjectID
					,[DetailFieldID] = @DetailFieldID
					,[DetailValue] = @DetailValue
					,[ErrorMsg] = @ErrorMsg
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
				WHERE
[ObjectDetailValueID] = @ObjectDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_Update] TO [sp_executeall]
GO
