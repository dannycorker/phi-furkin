SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DocumentTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_Get_List]

AS


				
				SELECT
					[DocumentTargetControlID],
					[ClientID],
					[DocumentTypeID],
					[LastParsed]
				FROM
					[dbo].[DocumentTargetControl] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Get_List] TO [sp_executeall]
GO
