SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-09-2011
-- Description:	Gets enable pages in order by type
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetPages]

	@LeadTypeID int,
	@LeadOrMatter int,
	@ClientID int

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT DetailFieldPageID, PageCaption FROM DetailFieldPages WITH (NOLOCK) 
	WHERE LeadTypeID = @LeadTypeID and Enabled=1 and LeadOrMatter = @LeadOrMatter and ClientID = @ClientID
	ORDER BY PageOrder asc

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetPages] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetPages] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetPages] TO [sp_executeall]
GO
