SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PanelItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_Delete]
(

	@PanelItemID int   
)
AS


				DELETE FROM [dbo].[PanelItems] WITH (ROWLOCK) 
				WHERE
					[PanelItemID] = @PanelItemID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Delete] TO [sp_executeall]
GO
