SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderKeywordMatchType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderKeywordMatchType_Delete]
(

	@DataLoaderKeywordMatchTypeID int   
)
AS


				DELETE FROM [dbo].[DataLoaderKeywordMatchType] WITH (ROWLOCK) 
				WHERE
					[DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderKeywordMatchType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Delete] TO [sp_executeall]
GO
