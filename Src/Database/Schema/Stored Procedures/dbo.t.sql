SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:			Alex Elger
-- Create date:		2013-11-07
-- Description:		Return table row stuff
-- =============================================
CREATE PROCEDURE [dbo].[t]
(
	@TableRowID INT
)
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

      SELECT *
      FROM TableRows t WITH (NOLOCK)
      INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID
      WHERE t.TableRowID = @TableRowID

      SELECT tdv.DetailValue, df.FieldCaption, tdv.*, df.*
      FROM TableDetailValues tdv WITH (NOLOCK)
      INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID
      WHERE tdv.TableRowID = @TableRowID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[t] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[t] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[t] TO [sp_executeall]
GO
