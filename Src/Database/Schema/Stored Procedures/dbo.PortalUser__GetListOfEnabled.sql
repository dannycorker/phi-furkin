SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 09/07/09
-- Description:	Gets a helpful list of Portal Users to be used to populate a Dropdown
-- =============================================
CREATE PROCEDURE [dbo].[PortalUser__GetListOfEnabled]
(
	@ClientID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
				pu.PortalUserID,
				pu.CustomerID,
				pu.Username,
				pu.ClientPersonnelID,
				cp.UserName AS Impersonates,
				pu.[Enabled],
				pu.AttemptedLogins
	FROM		PortalUser AS pu
	INNER JOIN	ClientPersonnel AS cp ON pu.ClientPersonnelID = cp.ClientPersonnelID
	WHERE		(pu.ClientID = @ClientID)
	AND			(pu.Enabled = 1)
	AND			(cp.AccountDisabled = 0)
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetListOfEnabled] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser__GetListOfEnabled] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetListOfEnabled] TO [sp_executeall]
GO
