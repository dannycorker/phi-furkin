SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the GroupFunctionControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControl_Update]
(

	@GroupFunctionControlID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants bit   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[GroupFunctionControl]
				SET
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ModuleID] = @ModuleID
					,[FunctionTypeID] = @FunctionTypeID
					,[HasDescendants] = @HasDescendants
					,[RightID] = @RightID
					,[LeadTypeID] = @LeadTypeID
				WHERE
[GroupFunctionControlID] = @GroupFunctionControlID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControl_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_Update] TO [sp_executeall]
GO
