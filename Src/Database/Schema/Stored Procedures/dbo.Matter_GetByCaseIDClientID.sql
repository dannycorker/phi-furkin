SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Matter table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_GetByCaseIDClientID]
(

	@CaseID int   ,

	@ClientID int   
)
AS


				SELECT
					[MatterID],
					[ClientID],
					[MatterRef],
					[CustomerID],
					[LeadID],
					[MatterStatus],
					[RefLetter],
					[BrandNew],
					[CaseID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[Matter] WITH (NOLOCK) 
				WHERE
										[CaseID] = @CaseID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByCaseIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_GetByCaseIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByCaseIDClientID] TO [sp_executeall]
GO
