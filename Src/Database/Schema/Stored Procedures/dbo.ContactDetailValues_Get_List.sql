SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ContactDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ContactDetailValues_Get_List]

AS


				
				SELECT
					[ContactDetailValueID],
					[ClientID],
					[ContactID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ContactDetailValues] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ContactDetailValues_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ContactDetailValues_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ContactDetailValues_Get_List] TO [sp_executeall]
GO
