SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*
----------------------------------------------------------------------------------------------------

-- Created By: Jim Green
-- Purpose: Select records from the UserFunctionControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing__GetByParentFunctionTypeID]
(
	@ClientPersonnelID int,
	@ParentFunctionTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					*
				FROM
					[dbo].[UserFunctionControlEditing]
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
				AND [FunctionTypeID] IN (SELECT FunctionTypeID FROM FunctionType WHERE ParentFunctionTypeID = @ParentFunctionTypeID)
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			








GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing__GetByParentFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing__GetByParentFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing__GetByParentFunctionTypeID] TO [sp_executeall]
GO
