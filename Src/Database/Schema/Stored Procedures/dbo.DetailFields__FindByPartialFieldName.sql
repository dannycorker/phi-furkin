SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16/02/2015
-- Description:	Find a detail field based upon a partial field name
-- Modified By PR: 19/02/2015 do not return tables, basic tables and resource lists. 
-- This will change when tables are added to scripting.
-- Modified By PR: 15/05/2015 Can search for a specific question type
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__FindByPartialFieldName]
	@ClientID INT,
	@FieldName VARCHAR(50),
	@LeadTypeID INT,
	@LockToQuestionTypeID INT = 0
AS
BEGIN

	SET NOCOUNT ON;

	IF @LockToQuestionTypeID=0
	BEGIN

		SELECT * FROM DetailFields WITH (NOLOCK) 
		WHERE 
			ClientID=@ClientID AND 
			(LeadTypeID=@LeadTypeID OR LeadTypeID=0) AND
			QuestionTypeID NOT IN (19,16,14) AND -- do not return basic tables, tables and resource lists
			FieldName LIKE '%' + @FieldName + '%'
		
	END
	ELSE
	BEGIN
		SELECT * FROM DetailFields WITH (NOLOCK) 
		WHERE 
			ClientID=@ClientID AND 
			(LeadTypeID=@LeadTypeID OR LeadTypeID=0) AND
			QuestionTypeID NOT IN (19,16,14) AND -- do not return basic tables, tables and resource lists
			FieldName LIKE '%' + @FieldName + '%' AND
			QuestionTypeID = @LockToQuestionTypeID
	
	END
		  
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindByPartialFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__FindByPartialFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindByPartialFieldName] TO [sp_executeall]
GO
