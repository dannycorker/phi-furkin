SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-22
-- Description:	Create any missing fields for this lead, by type if specified
-- =============================================
CREATE PROCEDURE [dbo].[CreateAllMissingMatterDetailValues] 
	@MatterID int,
	@QuestionTypeID int = null
AS
BEGIN
	SET NOCOUNT ON;

	-- Get the Matter and the Lead, use them to get all the possible DetailFields for the LeadType.
	-- Filter to just enabled fields, and optionally of the type specified (Equation etc),
	-- that don't already exist for the LeadID passed in.
	INSERT INTO dbo.MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
	SELECT df.ClientID, l.LeadID, @MatterID, df.DetailFieldID, '' 
	FROM dbo.Matter m 
	INNER JOIN dbo.Lead l ON l.LeadID = m.LeadID 
	INNER JOIN dbo.DetailFields df ON df.LeadTypeID = l.LeadTypeID AND df.Enabled = 1 
	WHERE m.MatterID = @MatterID 
	AND df.LeadOrMatter = 2 
	AND (@QuestionTypeID IS NULL OR df.QuestionTypeID = @QuestionTypeID) 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.MatterDetailValues mdv_existing 
		WHERE mdv_existing.MatterID = @MatterID 
		AND mdv_existing.DetailFieldID = df.DetailFieldID 
	)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CreateAllMissingMatterDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreateAllMissingMatterDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreateAllMissingMatterDetailValues] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[CreateAllMissingMatterDetailValues] TO [sp_executehelper]
GO
