SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-13
-- Description:	Get Sub Options for a menu
-- =============================================
CREATE PROCEDURE [dbo].[UserGroupMenuSubOption__GetByMenuOptionID]
	@UserGroupMenuOptionID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM UserGroupMenuSubOption u
	WHERE u.UserGroupMenuOptionID = @UserGroupMenuOptionID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption__GetByMenuOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption__GetByMenuOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption__GetByMenuOptionID] TO [sp_executeall]
GO
