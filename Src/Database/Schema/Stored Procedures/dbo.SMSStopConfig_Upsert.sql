SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-19
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[SMSStopConfig_Upsert]
	
	@ClientID INT
	,@isLeadTypeSpecific bit

AS
BEGIN


	DECLARE @SMSStopConfigID int

	SELECT @SMSStopConfigID = MIN(s.SMSStopConfigID) -- Should only be 1
	FROM SMSStopConfig s with(nolock)
	WHERE S.ClientID = @ClientID

	IF @SMSStopConfigID > 0 -- Update existing
	BEGIN
		
		UPDATE TOP (1) s
		SET s.isLeadTypeSpecific = @isLeadTypeSpecific
		FROM SMSStopConfig s 
		WHERE s.SMSStopConfigID = @SMSStopConfigID

	END
	ELSE -- Need to insert one
	BEGIN
		
		INSERT INTO SMSStopConfig (ClientID, isLeadTypeSpecific)
		VALUES (@ClientID, @isLeadTypeSpecific)

	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfig_Upsert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStopConfig_Upsert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfig_Upsert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfig_Upsert] TO [sp_executehelper]
GO
