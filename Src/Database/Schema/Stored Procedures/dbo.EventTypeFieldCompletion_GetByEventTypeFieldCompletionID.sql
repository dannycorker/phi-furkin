SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeFieldCompletion table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_GetByEventTypeFieldCompletionID]
(

	@EventTypeFieldCompletionID int   
)
AS


				SELECT
					[EventTypeFieldCompletionID],
					[ClientID],
					[EventTypeID],
					[DetailFieldID],
					[InsertOptionID],
					[DeleteOptionID]
				FROM
					[dbo].[EventTypeFieldCompletion] WITH (NOLOCK) 
				WHERE
										[EventTypeFieldCompletionID] = @EventTypeFieldCompletionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_GetByEventTypeFieldCompletionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_GetByEventTypeFieldCompletionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_GetByEventTypeFieldCompletionID] TO [sp_executeall]
GO
