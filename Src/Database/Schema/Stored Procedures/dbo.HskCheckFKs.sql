SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2019-10-15
-- Description:	Check for untrusted foreign keys
-- =============================================
CREATE PROCEDURE [dbo].[HskCheckFKs]
	@HideKnownIssues BIT = 1
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OBJECT_NAME(parent_object_id) as [Table], name, object_id, modify_date, is_not_trusted 
	FROM sys.foreign_keys 
	WHERE is_not_trusted = 1 
	AND (@HideKnownIssues = 0 OR name NOT LIKE '%ScriptDetailFieldTarget%')
END
GO
GRANT VIEW DEFINITION ON  [dbo].[HskCheckFKs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HskCheckFKs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HskCheckFKs] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[HskCheckFKs] TO [sp_executehelper]
GO
