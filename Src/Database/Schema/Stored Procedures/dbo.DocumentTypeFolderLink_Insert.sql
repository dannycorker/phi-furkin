SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentTypeFolderLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink_Insert]
(

	@DocumentTypeID int   ,

	@FolderID int   
)
AS


				
				INSERT INTO [dbo].[DocumentTypeFolderLink]
					(
					[DocumentTypeID]
					,[FolderID]
					)
				VALUES
					(
					@DocumentTypeID
					,@FolderID
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Insert] TO [sp_executeall]
GO
