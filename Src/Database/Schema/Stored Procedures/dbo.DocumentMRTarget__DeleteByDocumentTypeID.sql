SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Paul Richardson>
-- Create date: <14-07-2010>
-- Description:	<Deletes the targets for the given document type id>
-- =============================================
CREATE PROCEDURE [dbo].[DocumentMRTarget__DeleteByDocumentTypeID]
(
	@DocumentTypeID int   
)
AS


				DELETE FROM [dbo].[DocumentMRTarget] WITH (ROWLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID AND DocumentTypeVersionID IS NULL




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget__DeleteByDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget__DeleteByDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget__DeleteByDocumentTypeID] TO [sp_executeall]
GO
