SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Contact table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact_GetByContactID]
(

	@ContactID int   
)
AS


				SELECT
					[ContactID],
					[ClientID],
					[CustomerID],
					[TitleID],
					[Firstname],
					[Middlename],
					[Lastname],
					[Fullname],
					[EmailAddressWork],
					[EmailAddressOther],
					[DirectDial],
					[MobilePhoneWork],
					[MobilePhoneOther],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Postcode],
					[Country],
					[OfficeID],
					[DepartmentID],
					[JobTitle],
					[Notes],
					[CountryID],
					[LanguageID],
					[WhenModified],
					[WhoModified],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Contact] WITH (NOLOCK) 
				WHERE
										[ContactID] = @ContactID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_GetByContactID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact_GetByContactID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_GetByContactID] TO [sp_executeall]
GO
