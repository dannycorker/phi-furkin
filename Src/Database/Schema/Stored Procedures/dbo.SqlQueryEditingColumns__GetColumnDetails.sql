SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-08-25
-- Description:	Loads all the columns used by a query and gets the extra join information required
-- JWG 2012-08-14 Added IsJoinClause
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__GetColumnDetails]
(
	@SqlQueryEditingID int   
)
AS

DECLARE @ClientID INT,
		@IsChildQuery BIT

SELECT @ClientID = e.ClientID, @IsChildQuery = CASE WHEN ParentQueryID IS NULL THEN 0 ELSE 1 END
FROM dbo.SqlQueryEditing e WITH (NOLOCK) 
WHERE e.SqlQueryEditingID = @SqlQueryEditingID

IF @IsChildQuery = 1
BEGIN

	SELECT	c.SqlQueryEditingColumnID, 'Parent Query' AS TableAlias,  
			CASE WHEN c.CompleteOutputText LIKE '%(%' THEN c.CompleteOutputText ELSE c.ColumnNaturalName END AS ColumnName, 
			c.ColumnAlias, c.ShowColumn, c.SortType, c.SortOrder, c.ShowColumn, f.CriteriaText, c.SqlQueryEditingTableID, 
			CAST(0 AS BIT) as IsJoinClause,
			CAST(0 AS BIT) as IsJoinClauseEditable
	FROM dbo.SqlQueryEditingColumns c WITH (NOLOCK)
	LEFT JOIN SqlQueryEditingCriteria f WITH (NOLOCK) ON c.SqlQueryEditingColumnID = f.SqlQueryEditingColumnID 
	LEFT JOIN dbo.SqlQueryEditingTable t WITH (NOLOCK) ON c.SqlQueryEditingTableID = t.SqlQueryEditingTableID
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID
	AND CASE WHEN c.CompleteOutputText LIKE '%(%' THEN c.CompleteOutputText ELSE c.ColumnNaturalName END NOT IN ('Password','Salt','SourceID','EncryptedValue')
	ORDER BY c.DisplayOrder		
	
END
ELSE
BEGIN

	SELECT	c.SqlQueryEditingColumnID, 
			CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END AS TableAlias,  
			CASE WHEN c.CompleteOutputText LIKE '%(%' THEN c.CompleteOutputText ELSE c.ColumnNaturalName END AS ColumnName, 
			c.ColumnAlias, c.ShowColumn, c.SortType, c.SortOrder, c.ShowColumn, f.CriteriaText, c.SqlQueryEditingTableID, 
			CAST(
				CASE 
					/* Force IsJoinClause checkbox if the table is LEFT JOINed and the column is an ID column (which we auto-LEFT JOIN anyway) */
					WHEN t.JoinType = 'LEFT JOIN' AND c.ColumnNaturalName LIKE '%ID' THEN 1 
					/* Show other columns as however the user set them */
					ELSE ISNULL(f.IsJoinClause, 0) 
				END 
			AS BIT) as IsJoinClause,
			CASE 
				/* Allow users to see the IsJoinClause checkbox if the table is LEFT JOINed and the column is not an ID column (which we auto-LEFT JOIN anyway) */
				WHEN t.JoinType = 'LEFT JOIN' AND c.ColumnNaturalName NOT LIKE '%ID' THEN 1 
				ELSE 0 
			END as IsJoinClauseEditable
	FROM dbo.SqlQueryEditingColumns c WITH (NOLOCK)
	INNER JOIN dbo.SqlQueryEditingTable t WITH (NOLOCK) ON c.SqlQueryEditingTableID = t.SqlQueryEditingTableID
	LEFT JOIN SqlQueryEditingCriteria f WITH (NOLOCK) ON c.SqlQueryEditingColumnID = f.SqlQueryEditingColumnID 
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID
	AND CASE WHEN c.CompleteOutputText LIKE '%(%' THEN c.CompleteOutputText ELSE c.ColumnNaturalName END NOT IN ('Password','Salt','SourceID','EncryptedValue')
	ORDER BY c.DisplayOrder		
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__GetColumnDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__GetColumnDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__GetColumnDetails] TO [sp_executeall]
GO
