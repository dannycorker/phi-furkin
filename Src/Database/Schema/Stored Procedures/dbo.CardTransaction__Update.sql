SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 30/08/2016
-- Description:	Card Transaction update
-- Modified By PR: 01/09/2016 Added CardTypeID
-- Modified By PR: 02/11/2017 Added New columns
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__Update]
	@CardTransactionID INT, 
	@ClientID INT, 
	@CustomerID INT, 
	@ObjectID INT = NULL, 
	@ObjectTypeID INT = NULL, 
	@PurchasedProductPaymentScheduleID INT = NULL, 
	@CustomerPaymentScheduleID INT = NULL, 
	@ClientPaymentGatewayID INT = NULL, 
	@PaymentTypeID INT = NULL,
	@PreValidate BIT = NULL, 
	@OrderID VARCHAR(100) = NULL, 
	@ReferenceNumber VARCHAR(50) = NULL, 
	@Description VARCHAR(500) = NULL, 
	@Amount NUMERIC(18,2) = NULL, 
	@CurrencyID INT = NULL, 
	@FirstName VARCHAR(100) = NULL, 
	@LastName VARCHAR(100) = NULL, 
	@CardName VARCHAR(300) = NULL, 
	@Number VARCHAR(19) = NULL, 
	@ExpiryMonth INT  = NULL, 
	@ExpiryYear INT = NULL, 
	@Code VARCHAR(4)= NULL, 
	@AuthCode VARCHAR(250) = NULL, 
	@AvsCode VARCHAR(250) = NULL, 
	@AvsIndicator INT= NULL, 
	@Address VARCHAR(200)= NULL, 
	@Address2 VARCHAR(200)= NULL, 
	@City VARCHAR(200)= NULL, 
	@StateProvince VARCHAR(200)= NULL, 
	@Country VARCHAR(50)= NULL, 
	@ZipPostal VARCHAR(50)= NULL, 
	@BankAccountType VARCHAR(200)= NULL,  
	@BankCode VARCHAR(10)=NULL, 
	@BankName VARCHAR(250)=NULL,  
	@Company VARCHAR(250)=NULL, 
	@GatewayCustomerID VARCHAR(50) = NULL, 
	@TransactionID VARCHAR(50)=NULL, 
	@TransactionTypeID INT = NULL, 
	@Email VARCHAR(255) = NULL, 
	@Fax VARCHAR(50) = NULL, 
	@Phone VARCHAR(50) = NULL, 
	@Partner VARCHAR(20) = NULL, 
	@Certificate VARCHAR(500) = NULL, 
	@ClientIP VARCHAR(15) = NULL, 
	@Referrer VARCHAR(MAX) = NULL, 
	@ErrorCode VARCHAR(100)= NULL, 
	@ErrorMessage VARCHAR(500) = NULL, 
	@WhoCreated INT = NULL, 
	@WhenCreated DATETIME = NULL,
	@CardTypeID INT = NULL,
	@AccountID INT = NULL,
	@StatusMsg VARCHAR(250) = NULL,
	@CustomerRefNum VARCHAR(250) = NULL,
	@ParentID INT = NULL
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @CaseID int 
		  , @Comments Varchar(2000)

	SELECT @CaseID = m.CaseID
	FROM Matter m with(nolock) 
	WHERE MatterID = @ObjectID

	SELECT @Comments = 'CardTransactionID: ' + CAST(@CardTransactionID as varchar(2000)) + ' ErrorMessage: ' + isnull(@ErrorMessage, '')

	IF @CaseID > 0 AND LEN(@ErrorMessage) > 1 
	BEGIN

		EXEC LeadEvent_EasyInsert 130837 /*Payment Failure*/, @CaseID, @Comments

	END

	--INSERT INTO CardTransactionWhatIsGoingOn (CardTransactionID, ChangeSource, ChangeType, ClientPaymentGateWayID, MessageText, WhenChanged)
	--VALUES (@CardTransactionID, 'CardTransaction__Update', 'Update', @ClientPaymentGatewayID, '', dbo.fn_GetDate_Local())

	UPDATE CardTransaction
	SET	ClientID = @ClientID, 
		CustomerID = @CustomerID, 
		ObjectID = @ObjectID, 
		ObjectTypeID = @ObjectTypeID, 
		PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID, 
		CustomerPaymentScheduleID = @CustomerPaymentScheduleID, 
		ClientPaymentGatewayID = @ClientPaymentGatewayID, 
		PaymentTypeID = @PaymentTypeID,
		PreValidate = @PreValidate, 
		OrderID = @OrderID, 
		ReferenceNumber = @ReferenceNumber, 
		Description = @Description, 
		Amount = @Amount, 
		CurrencyID = @CurrencyID, 
		FirstName = @FirstName, 
		LastName = @LastName, 
		CardName = @CardName, 
		Number = @Number, 
		ExpiryMonth = @ExpiryMonth, 
		ExpiryYear = @ExpiryYear, 
		Code = @Code, 
		AuthCode = @AuthCode, 
		AvsCode = @AvsCode, 
		AvsIndicator = @AvsIndicator, 
		Address = @Address, 
		Address2 = @Address2, 
		City = @City, 
		StateProvince = @StateProvince, 
		Country = @Country, 
		ZipPostal = @ZipPostal, 
		BankAccountType = @BankAccountType,  
		BankCode = @BankCode, 
		BankName = @BankName,  
		Company = @Company, 
		GatewayCustomerID = @GatewayCustomerID, 
		TransactionID = @TransactionID, 
		TransactionTypeID = @TransactionTypeID, 
		Email = @Email, 
		Fax = @Fax, 
		Phone = @Phone, 
		Partner = @Partner, 
		Certificate = @Certificate, 
		ClientIP = @ClientIP, 
		Referrer = @Referrer, 
		ErrorCode = @ErrorCode, 
		ErrorMessage = @ErrorMessage, 
		WhoCreated = @WhoCreated, 
		WhenCreated = WhenCreated,
		CardTypeID = @CardTypeID,
		AccountID = @AccountID,
		StatusMsg = @StatusMsg,
		CustomerRefNum = @CustomerRefNum,
		ParentID = @ParentID
	WHERE CardTransactionID=@CardTransactionID
    	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__Update] TO [sp_executeall]
GO
