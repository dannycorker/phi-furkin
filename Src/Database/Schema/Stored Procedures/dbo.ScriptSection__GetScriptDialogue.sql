SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson 
-- Create date: 21-10-2014
-- Description:	Gets a set of script sections together with the renderable html dialogue
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSection__GetScriptDialogue]
	@ScriptID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT ss.*, sd.HtmlDialogue FROM ScriptSection ss WITH (NOLOCK) 
	INNER JOIN ScriptDialogue sd WITH (NOLOCK) on sd.ScriptSectionID = ss.ScriptSectionID
	WHERE ss.ScriptID=@ScriptID
	ORDER BY ss.SectionOrder ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__GetScriptDialogue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection__GetScriptDialogue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__GetScriptDialogue] TO [sp_executeall]
GO
