SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Language table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language_Find]
(

	@SearchUsingOR bit   = null ,

	@LanguageID int   = null ,

	@LanguageName varchar (50)  = null ,

	@Alpha2Code char (2)  = null ,

	@Alpha3Code char (3)  = null ,

	@NativeName nvarchar (50)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LanguageID]
	, [LanguageName]
	, [Alpha2Code]
	, [Alpha3Code]
	, [NativeName]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[Language] WITH (NOLOCK) 
    WHERE 
	 ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([LanguageName] = @LanguageName OR @LanguageName IS NULL)
	AND ([Alpha2Code] = @Alpha2Code OR @Alpha2Code IS NULL)
	AND ([Alpha3Code] = @Alpha3Code OR @Alpha3Code IS NULL)
	AND ([NativeName] = @NativeName OR @NativeName IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LanguageID]
	, [LanguageName]
	, [Alpha2Code]
	, [Alpha3Code]
	, [NativeName]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[Language] WITH (NOLOCK) 
    WHERE 
	 ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([LanguageName] = @LanguageName AND @LanguageName is not null)
	OR ([Alpha2Code] = @Alpha2Code AND @Alpha2Code is not null)
	OR ([Alpha3Code] = @Alpha3Code AND @Alpha3Code is not null)
	OR ([NativeName] = @NativeName AND @NativeName is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Find] TO [sp_executeall]
GO
