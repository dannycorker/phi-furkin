SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CaseTransferRestrictedField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_Delete]
(

	@CaseTransferRestrictedFieldID int   
)
AS


				DELETE FROM [dbo].[CaseTransferRestrictedField] WITH (ROWLOCK) 
				WHERE
					[CaseTransferRestrictedFieldID] = @CaseTransferRestrictedFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Delete] TO [sp_executeall]
GO
