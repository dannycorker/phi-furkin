SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 20/08/2016
-- Description:	Inserts a card transaction record
-- Modified By PR: 01/09/2016 Added CardTypeID
-- Modified By PR: 27/10/2016 Added StatusMsg
-- Modified By PR: 27/10/2016 Added CustomerRefNum
-- Modified By PR: 16/11/2016 Added ParentID
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__Insert]
	@ClientID INT, 
	@CustomerID INT, 
	@ObjectID INT = NULL, 
	@ObjectTypeID INT = NULL, 
	@PurchasedProductPaymentScheduleID INT = NULL, 
	@CustomerPaymentScheduleID INT = NULL, 
	@ClientPaymentGatewayID INT = NULL, 
	@PaymentTypeID INT = NULL,
	@PreValidate BIT = NULL, 
	@OrderID VARCHAR(100) = NULL, 
	@ReferenceNumber VARCHAR(50) = NULL, 
	@Description VARCHAR(500) = NULL, 
	@Amount NUMERIC(18,2) = NULL, 
	@CurrencyID INT = NULL, 
	@FirstName VARCHAR(100) = NULL, 
	@LastName VARCHAR(100) = NULL, 
	@CardName VARCHAR(300) = NULL, 
	@Number VARCHAR(19) = NULL, 
	@ExpiryMonth INT  = NULL, 
	@ExpiryYear INT = NULL, 
	@Code VARCHAR(4)= NULL, 
	@AuthCode VARCHAR(250) = NULL, 
	@AvsCode VARCHAR(250) = NULL, 
	@AvsIndicator INT= NULL, 
	@Address VARCHAR(200)= NULL, 
	@Address2 VARCHAR(200)= NULL, 
	@City VARCHAR(200)= NULL, 
	@StateProvince VARCHAR(200)= NULL, 
	@Country VARCHAR(50)= NULL, 
	@ZipPostal VARCHAR(50)= NULL, 
	@BankAccountType VARCHAR(200)= NULL,  
	@BankCode VARCHAR(10)=NULL, 
	@BankName VARCHAR(250)=NULL,  
	@Company VARCHAR(250)=NULL, 
	@GatewayCustomerID VARCHAR(50) = NULL, 
	@TransactionID VARCHAR(50)=NULL, 
	@TransactionTypeID INT = NULL, 
	@Email VARCHAR(255) = NULL, 
	@Fax VARCHAR(50) = NULL, 
	@Phone VARCHAR(50) = NULL, 
	@Partner VARCHAR(20) = NULL, 
	@CERTIFICATE VARCHAR(500) = NULL, 
	@ClientIP VARCHAR(15) = NULL, 
	@Referrer VARCHAR(MAX) = NULL, 
	@ErrorCode VARCHAR(100)= NULL, 
	@ErrorMessage VARCHAR(500) = NULL, 
	@WhoCreated INT = NULL, 
	@WhenCreated DATETIME = NULL,
	@CardTypeID INT = NULL,
	@StatusMsg VARCHAR(250) = NULL,
	@CustomerRefNum VARCHAR(250) = NULL,
	@ParentID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CardTransactionID INT

	INSERT INTO CardTransaction (ClientID, CustomerID, ObjectID, ObjectTypeID, PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID, ClientPaymentGatewayID, PaymentTypeID, PreValidate, OrderID, ReferenceNumber, Description, Amount, CurrencyID, FirstName, LastName, CardName, Number, ExpiryMonth, ExpiryYear, Code, AuthCode, AvsCode, AvsIndicator, Address, Address2, City, StateProvince, Country, ZipPostal, BankAccountType, BankCode, BankName, Company, GatewayCustomerID, TransactionID, TransactionTypeID, Email, Fax, Phone, Partner, CERTIFICATE, ClientIP, Referrer, ErrorCode, ErrorMessage, WhoCreated, WhenCreated, CardTypeID, StatusMsg, CustomerRefNum, ParentID )
	VALUES (@ClientID, @CustomerID, @ObjectID, @ObjectTypeID, @PurchasedProductPaymentScheduleID, @CustomerPaymentScheduleID, @ClientPaymentGatewayID, @PaymentTypeID, @PreValidate, @OrderID, @ReferenceNumber, @Description, @Amount, @CurrencyID, @FirstName, @LastName, @CardName, @Number, @ExpiryMonth, @ExpiryYear, @Code, @AuthCode, @AvsCode, @AvsIndicator, @Address, @Address2, @City, @StateProvince, @Country, @ZipPostal, @BankAccountType, @BankCode, @BankName, @Company, @GatewayCustomerID, @TransactionID, @TransactionTypeID, @Email, @Fax, @Phone, @Partner, @Certificate, @ClientIP, @Referrer, @ErrorCode, @ErrorMessage, @WhoCreated, @WhenCreated, @CardTypeID, @StatusMsg, @CustomerRefNum, @ParentID)
	
	SELECT @CardTransactionID = SCOPE_IDENTITY() 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__Insert] TO [sp_executeall]
GO
