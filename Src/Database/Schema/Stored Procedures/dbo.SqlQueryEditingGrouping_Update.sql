SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditingGrouping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingGrouping_Update]
(

	@SqlQueryEditingGroupingID int   ,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@GroupByClause varchar (2000)  ,

	@HavingClause varchar (500)  ,

	@HavingColumnID int   ,

	@HavingCriteria1 varchar (250)  ,

	@HavingCriteria2 varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditingGrouping]
				SET
					[ClientID] = @ClientID
					,[SqlQueryEditingID] = @SqlQueryEditingID
					,[GroupByClause] = @GroupByClause
					,[HavingClause] = @HavingClause
					,[HavingColumnID] = @HavingColumnID
					,[HavingCriteria1] = @HavingCriteria1
					,[HavingCriteria2] = @HavingCriteria2
				WHERE
[SqlQueryEditingGroupingID] = @SqlQueryEditingGroupingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingGrouping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_Update] TO [sp_executeall]
GO
