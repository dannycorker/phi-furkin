SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClassNodeAuth table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_Insert]
(

	@ClassNodeAuthID int    OUTPUT,

	@ClientID int   ,

	@ClassNodeID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@RightsLevel int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Deleted bit   
)
AS


				
				INSERT INTO [dbo].[ClassNodeAuth]
					(
					[ClientID]
					,[ClassNodeID]
					,[ClientPersonnelAdminGroupID]
					,[RightsLevel]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[Deleted]
					)
				VALUES
					(
					@ClientID
					,@ClassNodeID
					,@ClientPersonnelAdminGroupID
					,@RightsLevel
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@Deleted
					)
				-- Get the identity value
				SET @ClassNodeAuthID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Insert] TO [sp_executeall]
GO
