SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadViewHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_Update]
(

	@LeadViewHistoryID int   ,

	@ClientPersonnelID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@WhenViewed datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadViewHistory]
				SET
					[ClientPersonnelID] = @ClientPersonnelID
					,[ClientID] = @ClientID
					,[LeadID] = @LeadID
					,[WhenViewed] = @WhenViewed
				WHERE
[LeadViewHistoryID] = @LeadViewHistoryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Update] TO [sp_executeall]
GO
