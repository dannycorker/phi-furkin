SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SystemMessageAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_Get_List]

AS


				
				SELECT
					[SystemMessageAssignmentID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[SystemMessageID]
				FROM
					[dbo].[SystemMessageAssignment] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Get_List] TO [sp_executeall]
GO
