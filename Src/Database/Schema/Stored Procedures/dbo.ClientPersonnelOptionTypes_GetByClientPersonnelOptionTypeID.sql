SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelOptionTypes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptionTypes_GetByClientPersonnelOptionTypeID]
(

	@ClientPersonnelOptionTypeID int   
)
AS


				SELECT
					[ClientPersonnelOptionTypeID],
					[OptionTypeName]
				FROM
					[dbo].[ClientPersonnelOptionTypes] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_GetByClientPersonnelOptionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptionTypes_GetByClientPersonnelOptionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_GetByClientPersonnelOptionTypeID] TO [sp_executeall]
GO
