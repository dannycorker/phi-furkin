SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/04/2016
-- Description:	Inserts a record into the call back queue
-- =============================================
CREATE PROCEDURE [dbo].[CallbackQueue__Insert]
	
	@ClientID INT, 
	@CustomerID INT = NULL, 
	@CallSid VARCHAR(50), 
	@FromPhoneNumber VARCHAR(25), 
	@DateCreated DATETIME
	
AS
BEGIN
		
	SET NOCOUNT ON;
	
    INSERT INTO CallbackQueue
		(ClientID,CustomerID,CallSid,FromPhoneNumber,DateCreated)
    VALUES
        (@ClientID, @CustomerID, @CallSid,@FromPhoneNumber,@DateCreated)
        
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CallbackQueue__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CallbackQueue__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CallbackQueue__Insert] TO [sp_executeall]
GO
