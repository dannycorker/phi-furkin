SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 02/11/09
-- Description:	Gets all Leads Assigned by ClientPersonnelID
-- =============================================
CREATE PROCEDURE [dbo].[Lead__GetAssignedByClientPersonnelID]
	@FromRow int,
	@ToRow int,
	@ClientID int,
	@ClientPersonnelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    WITH results as (
	SELECT TOP 100 PERCENT
	l.LeadID,
	l.CustomerID,
	l.LeadTypeID,
	c.Fullname,
	lt.LeadTypeDescription,
	cp.UserName AS AssignedTo,
	CASE WHEN cq.SubmissionDate IS NULL THEN 'Manual Lead'
	ELSE CONVERT(varchar,cq.SubmissionDate,121)
	END AS SubmissionDate,
	ROW_NUMBER() OVER (ORDER BY l.LeadID) as RN
	FROM Lead l
	INNER JOIN Customers c (NOLOCK) ON c.CustomerID = l.CustomerID
	INNER JOIN LeadType lt (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
	INNER JOIN ClientPersonnel cp (NOLOCK) ON cp.ClientPersonnelID = l.AssignedTo
	LEFT JOIN CustomerQuestionnaires cq (NOLOCK) ON cq.CustomerID = l.CustomerID
	WHERE l.ClientID = @ClientID
	AND l.Assigned = 'true'
	AND l.AssignedTo = @ClientPersonnelID
	ORDER BY LeadID
)
SELECT * FROM results --WHERE results.RN BETWEEN @FromRow and @ToRow
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetAssignedByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__GetAssignedByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetAssignedByClientPersonnelID] TO [sp_executeall]
GO
