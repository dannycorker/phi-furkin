SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReportTableRelationships table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTableRelationships_Delete]
(

	@ReportTableRelationshipID int   
)
AS


				DELETE FROM [dbo].[ReportTableRelationships] WITH (ROWLOCK) 
				WHERE
					[ReportTableRelationshipID] = @ReportTableRelationshipID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTableRelationships_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Delete] TO [sp_executeall]
GO
