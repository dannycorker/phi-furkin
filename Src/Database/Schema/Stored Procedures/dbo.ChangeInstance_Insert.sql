SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChangeInstance table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeInstance_Insert]
(

	@ChangeInstanceID int    OUTPUT,

	@ClientID int   ,

	@ChangeDefinitionID int   ,

	@ChangeTypeID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Deleted bit   
)
AS


				
				INSERT INTO [dbo].[ChangeInstance]
					(
					[ClientID]
					,[ChangeDefinitionID]
					,[ChangeTypeID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[Deleted]
					)
				VALUES
					(
					@ClientID
					,@ChangeDefinitionID
					,@ChangeTypeID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@Deleted
					)
				-- Get the identity value
				SET @ChangeInstanceID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeInstance_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Insert] TO [sp_executeall]
GO
