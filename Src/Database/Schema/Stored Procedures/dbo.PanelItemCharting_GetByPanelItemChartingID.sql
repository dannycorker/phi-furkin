SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PanelItemCharting table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_GetByPanelItemChartingID]
(

	@PanelItemChartingID int   
)
AS


				SELECT
					[PanelItemChartingID],
					[ClientID],
					[PanelItemChartingName],
					[PanelID],
					[CreatedBy],
					[ChartID]
				FROM
					[dbo].[PanelItemCharting] WITH (NOLOCK) 
				WHERE
										[PanelItemChartingID] = @PanelItemChartingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_GetByPanelItemChartingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_GetByPanelItemChartingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_GetByPanelItemChartingID] TO [sp_executeall]
GO
