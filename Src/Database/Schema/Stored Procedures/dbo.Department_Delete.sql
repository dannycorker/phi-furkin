SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Department table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department_Delete]
(

	@DepartmentID int   
)
AS


				DELETE FROM [dbo].[Department] WITH (ROWLOCK) 
				WHERE
					[DepartmentID] = @DepartmentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Delete] TO [sp_executeall]
GO
