SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-03-26
-- Description:	Get Bank Holidays for the year
-- =============================================
CREATE PROCEDURE [dbo].[bh]
	@Year SMALLINT = NULL, 
	@ShowNextNewYearsDay BIT = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @Year IS NULL
	BEGIN
		SELECT @Year = DATEPART(YEAR, dbo.fn_GetDate_Local())
	END
	
	SELECT wd.CharDate, DATENAME(WEEKDAY, wd.CharDate), m.Month3Text, wd.Day 
	FROM dbo.WorkingDays wd WITH (NOLOCK) 
	INNER JOIN dbo.Months m WITH (NOLOCK) ON m.MonthID = wd.Month 
	WHERE ((wd.Year = @Year) OR (@ShowNextNewYearsDay = 1 AND wd.Year = @Year+1 AND wd.Month = 1 AND wd.DayNumber < 5) ) 
	AND wd.IsBankHoliday = 1 
	ORDER BY wd.Date ASC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[bh] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[bh] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[bh] TO [sp_executeall]
GO
