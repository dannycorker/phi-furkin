SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the TextToSpeak table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TextToSpeak_Update]
(

	@TextToSpeakID int   ,

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@PageNumber int   ,

	@SpeakText varchar (512)  ,

	@IsShown bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[TextToSpeak]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[ClientID] = @ClientID
					,[PageNumber] = @PageNumber
					,[SpeakText] = @SpeakText
					,[IsShown] = @IsShown
				WHERE
[TextToSpeakID] = @TextToSpeakID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TextToSpeak_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Update] TO [sp_executeall]
GO
