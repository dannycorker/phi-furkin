SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:			Alex Elger
-- Create date:		2010-03-30
-- Description:		Split matter into new case
-- Modified by:		Aaran Gravestock
-- Modified date:	2013-08-09
-- Modification description:	Added code to update CASES
--								LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, 
--								LatestNoteLeadEventID, LatestNonNoteLeadEventID, ProcessStartLeadEventID,
--								WhoCreated, WhenCreated, WhoModified, WhenModified
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SplitMatterToNewCase]

@MatterID tvpint READONLY

AS
BEGIN

	SET NOCOUNT ON

	-- Set context info to allow access to table
	DECLARE @ContextInfo VARBINARY(100) = CAST('NoSAE' AS VARBINARY)
	SET CONTEXT_INFO @ContextInfo


	declare @CaseID int,
			@NewCaseID int,
			@LeadID int,
			@ClientStatusID int,
			@AquariumStatusID int,
			@MatterStatus int,
			@DefaultContactID int,
			@CaseNum int,
			@CustomerID int,
			@ClientID int

	declare @LeadEventCopyTo table (ToInt int identity(1,1), NewLeadEventID int)
	declare @LeadEventCopyFrom table (FromInt int identity(1,1), OldLeadEventID int)

	Select  @LeadID = m.LeadID,
			@ClientStatusID = c.ClientStatusID,
			@AquariumStatusID = c.AquariumStatusID,
			@MatterStatus = m.MatterStatus,
			@DefaultContactID = DefaultContactID,
			@CaseID = c.CaseID
	From Matter m 
	Inner Join Cases c on c.CaseID = m.CaseID
	INNER JOIN @MatterID m2 ON m2.AnyID = m.MatterID

	Select top 1 @CaseNum = CaseNum, 
				 @CustomerID = Matter.CustomerID,
				 @ClientID = Lead.ClientID
	From Cases 
	Inner Join Matter On Matter.CaseID = Cases.CaseID
	Inner Join Lead on Lead.LeadID = Cases.LeadID
	Where Cases.LeadID = @LeadID
	Order By Cases.CaseID Desc

	/* Create Case*/
	INSERT INTO Cases (LeadID,ClientID,CaseNum,CaseRef,ClientStatusID,AquariumStatusID,DefaultContactID)
	VALUES (@LeadID,@ClientID,@CaseNum + 1,'Case ' + convert(varchar,(@CaseNum + 1)),@ClientStatusID,@AquariumStatusID,@DefaultContactID)

	Select @NewCaseID = SCOPE_IDENTITY()

	Update Matter
	Set CaseID = @NewCaseID
	FROM Matter 
	INNER JOIN @MatterID m ON m.AnyID = Matter.MatterID
	
	Insert Into LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
	Select ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, @NewCaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID
	From LeadEvent le 
	Where CaseID = @CaseID
	order by le.LeadEventID ASC

	Insert Into @LeadEventCopyTo (NewLeadEventID)
	Select LeadEventID
	From LeadEvent le 
	Where CaseID = @NewCaseID
	order by le.LeadEventID ASC

	Insert Into @LeadEventCopyFrom (OldLeadEventID)
	Select LeadEventID
	From LeadEvent le 
	Where CaseID = @CaseID
	order by le.LeadEventID ASC

	Update LeadEvent
	Set NextEventID = NewLeadEventID
	From LeadEvent
	Inner Join @LeadEventCopyFrom lecf on lecf.OldLeadEventID = LeadEvent.NextEventID
	Inner Join @LeadEventCopyTo etct on etct.ToInt = lecf.FromInt
	Where CaseID = @NewCaseID

	/*2015-07-24 ACE Remove any LETC records as the trigger is creating them incorrectly #30479*/
	DELETE FROM LeadEventThreadCompletion
	WHERE CaseID = @NewCaseID

	/*2015-02-10 ACE Bring LETC over as well*/
	INSERT INTO LeadEventThreadCompletion (ClientID, LeadID, CaseID, FromEventTypeID, FromLeadEventID, ThreadNumberRequired, ToEventTypeID, ToLeadEventID)
	SELECT ClientID, LeadID, @NewCaseID, FromEventTypeID, l_from_to.NewLeadEventID, ThreadNumberRequired, ToEventTypeID, l_to_to.NewLeadEventID
	FROM LeadEventThreadCompletion letc WITH (NOLOCK)
	LEFT JOIN @LeadEventCopyFrom l_from ON l_from.OldLeadEventID = letc.FromLeadEventID
	LEFT JOIN @LeadEventCopyTo l_from_to ON l_from_to.ToInt = l_from.FromInt
	LEFT JOIN @LeadEventCopyFrom l_to ON l_to.OldLeadEventID = letc.ToLeadEventID
	LEFT JOIN @LeadEventCopyTo l_to_to ON l_to_to.ToInt = l_to.FromInt
	WHERE letc.CaseID = @CaseID

	--AMG 20130809 - added code to populate 
	--	Cases.LatestLeadEventID
	;WITH LatestEvent AS (
		SELECT TOP 1 latest.LeadEventID, latest.CaseID, latest.WhoCreated, latest.WhenCreated
		FROM LeadEvent latest WITH (NOLOCK)
		WHERE latest.CaseID = @NewCaseID
		AND latest.EventDeleted = 0
		ORDER BY latest.LeadEventID DESC
	)
	UPDATE dbo.Cases
	SET LatestLeadEventID = le.LeadEventID,
	WhoModified = le.WhoCreated,
	WhenModified = le.WhenCreated
	FROM Cases ca WITH (NOLOCK)
	INNER JOIN LatestEvent le ON ca.CaseID = le.CaseID
	WHERE ca.CaseID = @NewCaseID
	
	--	Cases.LatestInProcessLeadEventID
	;WITH LatestIP (LeadEventID, CaseID) AS (
		SELECT TOP 1 latest.LeadEventID, latest.CaseID
		FROM LeadEvent latest WITH (NOLOCK)
		INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = latest.EventTypeID AND et.InProcess = 1
		WHERE latest.CaseID = @NewCaseID
		AND latest.EventDeleted = 0
		ORDER BY latest.LeadEventID DESC
	)
	UPDATE dbo.Cases
	SET LatestInProcessLeadEventID = le.LeadEventID
	FROM dbo.Cases ca WITH (NOLOCK)
	INNER JOIN LatestIP le ON ca.CaseID = le.CaseID
	WHERE ca.CaseID = @NewCaseID
	
	
	--	Cases.LatestOutOfProcessLeadEventID
	;WITH LatestOOP(LeadEventID, CaseID) AS (		
		SELECT TOP 1 latest.LeadEventID, latest.CaseID
		FROM LeadEvent latest WITH (NOLOCK)
		INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = latest.EventTypeID AND et.InProcess <> 1
		WHERE latest.CaseID = @NewCaseID
		AND latest.EventDeleted = 0
		ORDER BY latest.LeadEventID DESC
	)
	UPDATE dbo.Cases
	SET LatestOutOfProcessLeadEventID = le.LeadEventID
	FROM dbo.Cases ca WITH (NOLOCK)
	INNER JOIN LatestOOP le ON ca.CaseID = le.CaseID
	WHERE ca.CaseID = @NewCaseID
	

	--	Cases.LatestNoteLeadEventID
	;WITH LatestNote(LeadEventID, CaseID) AS (
		SELECT TOP 1 latest.LeadEventID, latest.CaseID
		FROM LeadEvent latest WITH (NOLOCK)
		WHERE latest.CaseID = @NewCaseID
		AND latest.EventDeleted = 0
		AND latest.NoteTypeID IS NOT NULL
		ORDER BY latest.LeadEventID DESC
	)
	UPDATE dbo.Cases
	SET LatestNoteLeadEventID = le.LeadEventID
	FROM dbo.Cases ca WITH (NOLOCK)
	INNER JOIN LatestNote le ON ca.CaseID = le.CaseID
	WHERE ca.CaseID = @NewCaseID
	
	
	--	Cases.LatestNonNoteLeadEventID
	;WITH LatestNonNote(LeadEventID, CaseID) AS (
		SELECT TOP 1 latest.LeadEventID, latest.CaseID
		FROM LeadEvent latest WITH (NOLOCK)
		WHERE latest.CaseID = @NewCaseID
		AND latest.EventDeleted = 0
		AND latest.NoteTypeID IS NULL
		ORDER BY latest.LeadEventID DESC
	)
	UPDATE dbo.Cases
	SET LatestNonNoteLeadEventID = le.LeadEventID
	FROM dbo.Cases ca WITH (NOLOCK)
	INNER JOIN LatestNonNote le ON ca.CaseID = le.CaseID
	WHERE ca.CaseID = @NewCaseID
	
	
	-- Cases.ProcessStartLeadEventID
	;WITH ProcessStartEvent(LeadEventID, CaseID, WhoCreated, WhenCreated) AS (
		SELECT TOP 1 latest.LeadEventID, latest.CaseID, latest.WhoCreated, latest.WhenCreated
		FROM LeadEvent latest WITH (NOLOCK)
		INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = latest.EventTypeID
		WHERE latest.CaseID = @NewCaseID
		AND latest.EventDeleted = 0
		AND et.EventSubtypeID = 10
		ORDER BY latest.LeadEventID
	)
	UPDATE dbo.Cases
	SET ProcessStartLeadEventID = le.LeadEventID,
	WhoCreated = le.WhoCreated,
	WhenCreated = le.WhenCreated
	FROM dbo.Cases ca WITH (NOLOCK)
	INNER JOIN ProcessStartEvent le ON ca.CaseID = le.CaseID
	WHERE ca.CaseID = @NewCaseID
		
	
	
	SELECT @NewCaseID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SplitMatterToNewCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SplitMatterToNewCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SplitMatterToNewCase] TO [sp_executeall]
GO
