SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2011-04-04
-- Description:	Aquarium index management (reorganize, rebuild etc)
-- =============================================
CREATE PROCEDURE [dbo].[AquariumIndexHousekeeping] 
	@Debug bit = 0,
	@AdminUserID int = 2795
AS
BEGIN

	/* Give this script the highest priority there is, where the scale is -10 (lowest) to 10 (highest) */
	SET DEADLOCK_PRIORITY 10;
	
	SET NOCOUNT ON

	DECLARE @LoopCount int = 0, 
	@LoopMax int, 
	@TableName varchar(500),
	@IndexName varchar(500),
	@ProgressInfo varchar(1500),
	@TableID bigint,
	@IndexID int,
	@Fragmentation float,
	@TimerStart datetime,
	@TimerStop datetime,
	@SqlCommandToApply varchar(2000)
	
	/* Log progress throughout */
	EXEC dbo._C00_LogIt 'Info', 'AquariumIndexHousekeeping', 'Start', 'Start of index housekeeping', @AdminUserID 
	
	/* Start by clearing out the table completely */
	TRUNCATE TABLE dbo.HskIndexUsage

	/* Populate the table with every index in the database */
	INSERT INTO dbo.HskIndexUsage (TableName, TableID, IsPK, TableRowCount, IndexName, IndexOrderID, IndexPageCount, UserSeeks, UserScans, UserLookups, UserUpdates, LastChecked) 
	SELECT so.name,
	so.object_id,
	si.is_primary_key,
	ddps.row_count,
	si.name,
	si.index_id,
	ddps.in_row_data_page_count,
	ddius.USER_SEEKS,
	ddius.USER_SCANS,
	ddius.USER_LOOKUPS,
	ddius.USER_UPDATES,
	dbo.fn_GetDate_Local() 
	FROM sys.indexes AS si  
	INNER JOIN sys.objects AS so ON si.OBJECT_ID = so.OBJECT_ID  
	INNER JOIN sys.dm_db_partition_stats AS ddps ON si.OBJECT_ID = ddps.OBJECT_ID  AND si.index_id = ddps.index_id 
	LEFT JOIN SYS.DM_DB_INDEX_USAGE_STATS AS ddius ON sI.OBJECT_ID = ddius.OBJECT_ID AND sI.INDEX_ID = ddius.INDEX_ID 
	WHERE so.is_ms_shipped = 0 
	AND si.name IS NOT NULL
	ORDER BY so.name, si.index_id
	
	/* Remove duplicates, keeping the ones with the highest page count from the DMVs above */
	;WITH InnerSql AS 
	(
		SELECT h.AnyID, h.TableName, h.IndexName, ROW_NUMBER() OVER(PARTITION BY h.TableName, h.IndexName ORDER BY h.IndexPageCount DESC) as rn
		FROM dbo.HskIndexUsage h 
	)
	DELETE dbo.HskIndexUsage 
	FROM dbo.HskIndexUsage h 
	INNER JOIN InnerSql i ON i.AnyID = h.AnyID 
	WHERE i.rn > 1 
	
	SELECT @LoopMax = COUNT(*) 
	FROM dbo.HskIndexUsage

	/* Get stats for each index one at a time */
	WHILE @LoopCount < @LoopMax
	BEGIN
		
		SELECT @LoopCount += 1
		
		SELECT TOP (1)  
		@TableName = h.TableName,
		@IndexName = h.IndexName,
		@TableID = h.TableID, 
		@IndexID = h.IndexOrderID 
		FROM dbo.HskIndexUsage h 
		WHERE h.FragmentationPctBefore IS NULL  
		
		/* Time how long it takes to get the stats, just for interest */
		SELECT @TimerStart = DATEADD(ms, 3, dbo.fn_GetDate_Local())
		
		IF @Debug = 1
		BEGIN
			SELECT @ProgressInfo = @TableName + ' : ' + @IndexName + ' starting at ' + CONVERT(varchar, @TimerStart, 120)
			
			PRINT @ProgressInfo
		END
		
		/* Get fragmentation stats for this index */
		SELECT @Fragmentation = ddips.avg_fragmentation_in_percent 
		FROM sys.dm_db_index_physical_stats (DB_ID(), @TableID, @IndexID , NULL, N'Limited') ddips 
		
		SELECT @TimerStop = DATEADD(ms, 3, dbo.fn_GetDate_Local())
		
		/* Record the stats in the table */
		UPDATE dbo.HskIndexUsage 
		SET FragmentationPctBefore = ISNULL(@Fragmentation, 0), 
		LookupTimeInSeconds = DATEDIFF(second, @TimerStart, @TimerStop)
		WHERE TableName = @TableName 
		AND IndexName = @IndexName
		
	END

	/* Log progress throughout */
	EXEC dbo._C00_LogIt 'Debug', 'AquariumIndexHousekeeping', 'Stats', 'Finished gathering stats for each index', @AdminUserID 

	/* 
		Decide what to do with each index:  
		If < 10% fragmented, do nothing
		10% to 30% reorganise
		If more than 30% then full rebuild
	*/
	UPDATE dbo.HskIndexUsage
	SET CommandToApply = 'ALTER INDEX ' + IndexName + ' ON dbo.' + TableName + 
		/*CASE 
			/* Rebuild when badly fragmented */
			WHEN FragmentationPctBefore > 30 THEN ' REBUILD WITH (ONLINE = OFF, MAXDOP = 1) ' 
			/* Rebuild when somewhat fragmented but table is very large */
			/* This is forcing the DB to grow every day, wasting several GB each time it does. JWG 2011-11-16. */
			WHEN TableRowCount > 10000000 THEN ' REBUILD WITH (ONLINE = OFF, MAXDOP = 1) ' 
			/* Reorganise when somewhat fragmented and table is not very large */
			ELSE ' REORGANIZE ' 
			END */
			 ' REORGANIZE ' 
			+ '-- RowCount = ' + CAST(TableRowCount AS VARCHAR(12)) + ', Frag = ' + CAST(CAST(FragmentationPctBefore AS INT) AS VARCHAR) 
	WHERE FragmentationPctBefore > 14 
	/* Ignore tiny tables */
	AND TableRowCount > 2000 
	/* PK and GUID indexes on FILESTREAM tables */
	AND NOT (
		TableName = 'LeadDocumentFS' AND IndexOrderID < 3
	)
	/* PK and GUID indexes on FILESTREAM tables */
	AND NOT (
		TableName = 'UploadedFile' AND IndexOrderID < 3
	)

	/* Specify a sensible order in which to apply the rebuilds (PKs before other indexes) */
	;WITH InnerSql AS 
	(
		SELECT h.TableID, h.IndexOrderID, ROW_NUMBER() OVER(ORDER BY h.TableName, h.IndexOrderID) as rn 
		FROM dbo.HskIndexUsage h 
		WHERE h.CommandToApply > '' 
	)
	UPDATE dbo.HskIndexUsage 
	SET CommandOrder = i.rn
	FROM InnerSql i 
	INNER JOIN dbo.HskIndexUsage h2 ON h2.TableID = i.TableID AND h2.IndexOrderID = i.IndexOrderID 


	IF @Debug = 1
	BEGIN
		/* Just print the commands */
		SELECT h.CommandToApply 
		FROM dbo.HskIndexUsage h 
		WHERE h.CommandOrder > 0 
		ORDER BY h.CommandOrder 
	END
	ELSE
	BEGIN
		
		/* Reset loop counters */
		SELECT @LoopCount = 0,
		@LoopMax = COUNT(*) 
		FROM dbo.HskIndexUsage h 
		WHERE h.CommandToApply > '' 
		
		/* Loop through each index where there is work to be done */
		WHILE @LoopCount < @LoopMax
		BEGIN
			
			SELECT @LoopCount += 1
			
			SELECT TOP (1) 
			@TableName = h.TableName,
			@IndexName = h.IndexName,
			@TableID = h.TableID, 
			@IndexID = h.IndexOrderID, 
			@SqlCommandToApply = h.CommandToApply 
			FROM dbo.HskIndexUsage h 
			WHERE h.CommandOrder = @LoopCount 
			
			/***************************************************************/
			/***************************************************************/
			/* MUST NOT REBUILD CLUSTERED INDEX ON LEADDOCUMENTFS EVER !!! */
			/***************************************************************/
			/***************************************************************/
			IF (@TableName = 'LeadDocumentFS' AND @IndexID < 3) OR (@TableName = 'UploadedFile' AND @IndexID < 3)
			BEGIN
				/* 
					FILESTREAM tables are special cases.
					If you try to defrag the primary key, it creates a new copy of every existing document.
					This is allegedly fixed in R2 but we are not using that version.
				*/
				PRINT 'FILESTREAM PKs and GUID UQs cannot be defragged on this version of Sql Server 2008 SP2'

				/* Log progress throughout */
				EXEC dbo._C00_LogIt 'Debug', 'AquariumIndexHousekeeping', 'Ignoring FILESTREAM indexes', @IndexName, @AdminUserID 
			END
			ELSE
			BEGIN
				SELECT @TimerStart = DATEADD(ms, 3, dbo.fn_GetDate_Local())

				/* Log progress throughout */
				EXEC dbo._C00_LogIt 'Debug', 'AquariumIndexHousekeeping', @SqlCommandToApply, @IndexName, @AdminUserID 
				
				/* Run the command to rebuild/reorganise */
				EXEC(@SqlCommandToApply)
				
				SELECT @TimerStop = DATEADD(ms, 3, dbo.fn_GetDate_Local())
				
				/* Check the new fragmentation level to see if it was all worthwhile */
				SELECT @Fragmentation = ddips.avg_fragmentation_in_percent 
				FROM sys.dm_db_index_physical_stats (DB_ID(), @TableID, @IndexID , NULL, N'Limited') ddips 
				
				/* Update the table with the new fragmentation level and how long it took to do */
				UPDATE dbo.HskIndexUsage 
				SET FragmentationPctAfter = @Fragmentation,
				DefragTimeInSeconds = DATEDIFF(second, @TimerStart, @TimerStop)
				WHERE CommandOrder = @LoopCount
				
			END
		END
		
	END

	/* Log progress throughout */
	EXEC dbo._C00_LogIt 'Info', 'AquariumIndexHousekeeping', 'End', 'End of index housekeeping', @AdminUserID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumIndexHousekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumIndexHousekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumIndexHousekeeping] TO [sp_executeall]
GO
