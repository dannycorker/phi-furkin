SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ResourceList table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceList_Find]
(

	@SearchUsingOR bit   = null ,

	@ResourceListID int   = null ,

	@ClientID int   = null ,

	@DetailFieldPageID int   = null ,

	@ResourceListHelperCode varchar (100)  = null ,

	@ResourceListHelperName varchar (250)  = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ResourceListID]
	, [ClientID]
	, [DetailFieldPageID]
	, [ResourceListHelperCode]
	, [ResourceListHelperName]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ResourceList] WITH (NOLOCK) 
    WHERE 
	 ([ResourceListID] = @ResourceListID OR @ResourceListID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([ResourceListHelperCode] = @ResourceListHelperCode OR @ResourceListHelperCode IS NULL)
	AND ([ResourceListHelperName] = @ResourceListHelperName OR @ResourceListHelperName IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ResourceListID]
	, [ClientID]
	, [DetailFieldPageID]
	, [ResourceListHelperCode]
	, [ResourceListHelperName]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ResourceList] WITH (NOLOCK) 
    WHERE 
	 ([ResourceListID] = @ResourceListID AND @ResourceListID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([ResourceListHelperCode] = @ResourceListHelperCode AND @ResourceListHelperCode is not null)
	OR ([ResourceListHelperName] = @ResourceListHelperName AND @ResourceListHelperName is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Find] TO [sp_executeall]
GO
