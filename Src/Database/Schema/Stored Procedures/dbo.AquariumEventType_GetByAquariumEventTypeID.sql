SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumEventType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventType_GetByAquariumEventTypeID]
(

	@AquariumEventTypeID int   
)
AS


				SELECT
					[AquariumEventTypeID],
					[AquariumEventTypeName],
					[AquariumEventTypeDescription],
					[AquariumStatusAfterEvent],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[AquariumEventType] WITH (NOLOCK) 
				WHERE
										[AquariumEventTypeID] = @AquariumEventTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_GetByAquariumEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventType_GetByAquariumEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_GetByAquariumEventTypeID] TO [sp_executeall]
GO
