SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the BillStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillStatus_Update]
(

	@BillStatusID int   ,

	@Name varchar (50)  ,

	@Description varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[BillStatus]
				SET
					[Name] = @Name
					,[Description] = @Description
				WHERE
[BillStatusID] = @BillStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Update] TO [sp_executeall]
GO
