SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2014-04-07
-- Description:	List documents for extraction
-- JWG 2014-06-02 Add a range for LeadEventIDs so that we can run this in chunks 
-- ACE 2014-07-10 Added Rownumber to the SP and batch so you can tell where the chunk is up to
-- ACE 2014-07-18 Updated to replace / and \ from file names.

-- =============================================
CREATE PROCEDURE [dbo].[_C00_ListDocumentsForExtraction]
	@ClientID INT, 
	@LimitRowsReturned INT = 1,			/* Safety net */
	@FirstLeadEventID INT = 0,			/* Optional start point */
	@LastLeadEventID INT = 2147483647,	/* Optional end point */
	@EventTypeID INT = NULL				/* Optional EventType Only*/
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TOP (@LimitRowsReturned) 
	m.LeadID, 
	m.MatterID, 
	le.EventTypeID, 
	le.LeadDocumentID, 
	'root' as [FolderStructure], 
	REPLACE(REPLACE(CASE WHEN ISNULL(ld.DocumentBlobSize, 0) = 0 THEN 'NULL' ELSE CONVERT(VARCHAR,m.LeadID) + '_' + CONVERT(VARCHAR,m.MatterID)  + '_' + CONVERT(VARCHAR,le.EventTypeID) + '_' + REPLACE(REPLACE(CONVERT(VARCHAR,le.whencreated,120), ':', ''), ' ', '') + ISNULL(ld.FileName, '.' + CASE ISNULL(ld.EmailBlobSize, 0) WHEN 0 THEN ld.DocumentFormat ELSE 'RTF' END) END, '/', ''), '\', '') AS [Filename], 
	ld.ZipFormat, 
	REPLACE(REPLACE(CASE WHEN ISNULL(ld.EmailBlobSize, 0) = 0 THEN 'NULL' ELSE CONVERT(VARCHAR,m.LeadID) + '_' + CONVERT(VARCHAR,m.MatterID)  + '_' + CONVERT(VARCHAR,le.EventTypeID) + '_' + REPLACE(REPLACE(CONVERT(VARCHAR,le.whencreated,120), ':', ''), ' ', '') + ISNULL(ld.FileName, '.' + ld.DocumentFormat) END, '/', ''), '\', '') AS [EmailFilename],  
	ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS [RowID]
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID 
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID AND c.Test = 0 
	INNER JOIN LeadDocument ld WITH (NOLOCK) ON ld.LeadDocumentID = le.LeadDocumentID 
	WHERE le.ClientID = @ClientID 
	AND le.NoteTypeID IS NULL -- Exclude notes 
	AND le.EventDeleted = 0 
	AND le.LeadEventID BETWEEN @FirstLeadEventID AND @LastLeadEventID
	AND (le.EventTypeID = @EventTypeID OR @EventTypeID IS NULL)
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ListDocumentsForExtraction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ListDocumentsForExtraction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ListDocumentsForExtraction] TO [sp_executeall]
GO
