SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 09/07/09
-- Description:	Gets a helpful list of Portal Users to be used to populate a Grid
-- JWG 2014-05-13 #26507 LatestRecordsFirst 
-- =============================================
CREATE PROCEDURE [dbo].[PortalUser__GetListForGrid]
(
	@ClientID int
)
AS
BEGIN

	SET NOCOUNT ON;

    SELECT
		pu.PortalUserID,
		pu.CustomerID,
		pu.Username,
		pu.ClientPersonnelID,
		cp.UserName AS Impersonates,
		pu.[Enabled],
		pu.AttemptedLogins,
		pug.PortalUserGroupID,
		pug.GroupName
	FROM
		PortalUser AS pu WITH (NOLOCK) 
	INNER JOIN
		ClientPersonnel cp WITH (NOLOCK) ON pu.ClientPersonnelID = cp.ClientPersonnelID
	LEFT JOIN
		PortalUserGroup pug WITH (NOLOCK) ON pug.PortalUserGroupID = pu.PortalUserGroupID
	WHERE
		(pu.ClientID = @ClientID)
	/* JWG 2014-05-13 #26507 LatestRecordsFirst. Actually just order by username in this instance, rather than ID which is pretty useless. */
	ORDER BY pu.Username
		
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetListForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser__GetListForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetListForGrid] TO [sp_executeall]
GO
