SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ActiveSession table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_GetBySessionID]
(

	@SessionID varchar (50)  
)
AS


				SELECT
					[SessionID],
					[EmailAddress]
				FROM
					[dbo].[ActiveSession]
				WHERE
					[SessionID] = @SessionID
			Select @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetBySessionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_GetBySessionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetBySessionID] TO [sp_executeall]
GO
