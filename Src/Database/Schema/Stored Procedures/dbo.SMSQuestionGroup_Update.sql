SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SMSQuestionGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_Update]
(

	@SMSQuestionGroupID int   ,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@GroupName varchar (250)  ,

	@SMSQuestionOrderID int   ,

	@GroupOrder int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SMSQuestionGroup]
				SET
					[ClientID] = @ClientID
					,[SMSQuestionnaireID] = @SMSQuestionnaireID
					,[GroupName] = @GroupName
					,[SMSQuestionOrderID] = @SMSQuestionOrderID
					,[GroupOrder] = @GroupOrder
				WHERE
[SMSQuestionGroupID] = @SMSQuestionGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Update] TO [sp_executeall]
GO
