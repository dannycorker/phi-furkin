SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the GroupRightsDynamicEditing table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamicEditing_Find]
(

	@SearchUsingOR bit   = null ,

	@GroupRightsDynamicEditingID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@FunctionTypeID int   = null ,

	@LeadTypeID int   = null ,

	@ObjectID int   = null ,

	@RightID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [GroupRightsDynamicEditingID]
	, [ClientPersonnelAdminGroupID]
	, [FunctionTypeID]
	, [LeadTypeID]
	, [ObjectID]
	, [RightID]
    FROM
	[dbo].[GroupRightsDynamicEditing] WITH (NOLOCK) 
    WHERE 
	 ([GroupRightsDynamicEditingID] = @GroupRightsDynamicEditingID OR @GroupRightsDynamicEditingID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([FunctionTypeID] = @FunctionTypeID OR @FunctionTypeID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ObjectID] = @ObjectID OR @ObjectID IS NULL)
	AND ([RightID] = @RightID OR @RightID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [GroupRightsDynamicEditingID]
	, [ClientPersonnelAdminGroupID]
	, [FunctionTypeID]
	, [LeadTypeID]
	, [ObjectID]
	, [RightID]
    FROM
	[dbo].[GroupRightsDynamicEditing] WITH (NOLOCK) 
    WHERE 
	 ([GroupRightsDynamicEditingID] = @GroupRightsDynamicEditingID AND @GroupRightsDynamicEditingID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([FunctionTypeID] = @FunctionTypeID AND @FunctionTypeID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ObjectID] = @ObjectID AND @ObjectID is not null)
	OR ([RightID] = @RightID AND @RightID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamicEditing_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Find] TO [sp_executeall]
GO
