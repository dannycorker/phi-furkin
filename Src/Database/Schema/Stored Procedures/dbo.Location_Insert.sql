SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Location_Insert]
(

	@LocationID int    OUTPUT,

	@ClientID int   ,

	@LocationName varchar (255)  ,

	@Note varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[Location]
					(
					[ClientID]
					,[LocationName]
					,[Note]
					,[Enabled]
					,[Deleted]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@LocationName
					,@Note
					,@Enabled
					,@Deleted
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @LocationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Location_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Insert] TO [sp_executeall]
GO
