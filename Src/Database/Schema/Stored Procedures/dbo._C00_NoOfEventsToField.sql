SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-12-21
-- Description:	Set Field Value to Number of Events Applied
-- =============================================
CREATE PROCEDURE [dbo].[_C00_NoOfEventsToField]
@LeadEventID int,
@EventTypeIDToSum int,
@InsertintoDetailFieldID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @LeadID int,
			@CaseID int,
			@CountOfEvents int

	SELECT @LeadID = le.LeadID, @CaseID = le.CaseID
	From LeadEvent le WITH (NOLOCK)
	WHERE le.LeadEventID = @LeadEventID

	SELECT @CountOfEvents = COUNT(*)
	From LeadEvent le WITH (NOLOCK)
	Where le.CaseID = @CaseID
	and le.EventTypeID = @EventTypeIDToSum
	and le.EventDeleted = 0

	exec dbo._C00_ValueIntoField @LeadEventID, @InsertintoDetailFieldID, @CountOfEvents

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_NoOfEventsToField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_NoOfEventsToField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_NoOfEventsToField] TO [sp_executeall]
GO
