SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateClientDefaultEmailAddress]

@ClientID int,
@Email nvarchar(255)

as

Update Clients
SET DefaultEmailAddress = @Email
where ClientID = @ClientID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientDefaultEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientDefaultEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientDefaultEmailAddress] TO [sp_executeall]
GO
