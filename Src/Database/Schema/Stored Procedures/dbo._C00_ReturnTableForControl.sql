SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-06-17
-- Description:	Return a table for a custom control to be 
--				able to render and select/approve
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReturnTableForControl]
	@DetailFieldID INT, 
	@CustomerID INT = NULL,
	@LeadID INT = 0, 
	@CaseID INT = NULL,
	@MatterID INT = 0,
	@ClientID INT,
	@FieldTypeID INT /*871 or 873*/
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE 
	@PrintOnly BIT = 0,
	@PivotOutput BIT = 1,
	@DetailFieldSubtypeID INT = NULL,
	@ClientPersonnelID INT = NULL,
	@FieldCaption VARCHAR(100),
	@TableDetailFieldPageID INT,
	@cols VARCHAR(MAX),
	@fieldIdents VARCHAR(MAX),
	@rlfieldIdents VARCHAR(MAX),
	@query VARCHAR(MAX) = '',
	@rlQuery VARCHAR(MAX) = '',
	@llQuery VARCHAR(MAX) = '',
	@rlllQuery VARCHAR(MAX) = '',
	@basicQuery VARCHAR(MAX),
	@ResourceTable INT,
	@ResourceListDetailFieldPageID INT,
	@ResourceListDetailFieldID INT,
	@pvtStatement VARCHAR(MAX) = '',
	@queryheader VARCHAR(MAX) = '',
	@LeadOrMatterClause VARCHAR(MAX),
	@LookupListColumns INT,
	@DFPString varchar(100),
	@DFString varchar(100)
	
	SELECT @DetailFieldID = tpm_destination.DetailFieldID
	FROM ThirdPartyFieldMapping tpm_source WITH (NOLOCK)
	INNER JOIN ThirdPartyFieldMapping tpm_destination WITH (NOLOCK) ON tpm_destination.LeadTypeID = tpm_source.LeadTypeID AND tpm_destination.ClientID = tpm_source.ClientID AND tpm_destination.ThirdPartyFieldID = CASE @FieldTypeID WHEN 871 THEN 868 ELSE 873 END
	INNER JOIN DetailFields df_source WITH (NOLOCK) ON df_source.DetailFieldID = tpm_source.DetailFieldID
	INNER JOIN DetailFields df_destination WITH (NOLOCK) ON df_destination.DetailFieldID = tpm_destination.DetailFieldID AND df_destination.LeadOrMatter = df_source.LeadOrMatter
	WHERE tpm_source.DetailFieldID = @DetailFieldID 
	AND tpm_source.ThirdPartyFieldID = @FieldTypeID

	/*Get the field level, use the ClientID as an extra check.*/
	SELECT @DetailFieldSubtypeID = df.LeadOrMatter, @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID
	AND df.ClientID = @ClientID
	
	IF @DetailFieldSubtypeID > 0
	BEGIN
	
		/*Set the ID to be the appropriate one*/
		SELECT df.FieldCaption AS [TableName], 'Authorise' AS [AuthText], 'Decline' AS [DeclineText]
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldID = @DetailFieldID
		
		IF @MatterID > 0
		BEGIN
			SET @LeadOrMatterClause = 'MatterID = ' + CAST(@MatterID AS VARCHAR)
		END
		ELSE
		IF @CaseID > 0
		BEGIN
			SET @LeadOrMatterClause = 'CaseID = ' + CAST(@CaseID AS VARCHAR)
		END
		ELSE
		IF @LeadID > 0
		BEGIN
			SET @LeadOrMatterClause = 'LeadID = ' + CAST(@LeadID AS VARCHAR)
		END
		ELSE
		IF @CustomerID > 0
		BEGIN
			SET @LeadOrMatterClause = 'CustomerID = ' + CAST(@CustomerID AS VARCHAR)
		END
		ELSE
		IF @ClientPersonnelID > 0
		BEGIN
			SET @LeadOrMatterClause = 'ClientPersonnelID = ' + CAST(@ClientPersonnelID AS VARCHAR)
		END
		ELSE
		IF @ClientID > 0
		BEGIN
			SET @LeadOrMatterClause = 'ClientID = ' + CAST(@ClientID AS VARCHAR)
		END
	/* 
		IF @MatterID > 0
		BEGIN
			SET @LeadOrMatterClause = 'MatterID = ' + CAST(@MatterID as varchar)
		END
		ELSE
		BEGIN
			IF @MatterIDList IS NULL
			BEGIN
				SET @LeadOrMatterClause = 'LeadID = ' + CAST(@LeadID as varchar)
			END
			ELSE
			BEGIN	
				SET @LeadOrMatterClause = 'MatterID IN (' + @MatterIDList + ') '
			END
		END
	*/	
		SELECT @TableDetailFieldPageID = df.TableDetailFieldPageID
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldID = @DetailFieldID

		IF EXISTS(SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.QuestionTypeID = 14) 
		BEGIN
			SET @ResourceTable = 1
		END
		
		IF EXISTS(SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.QuestionTypeID IN (2, 4)) 
		BEGIN
			SET @LookupListColumns = 1
		END

		SET @basicQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, ISNULL(CASE df.Lookup WHEN 1 THEN luli.ItemValue ELSE tdv.DetailValue END, '''') as DetailValue
			FROM dbo.TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + CAST(@TableDetailFieldPageID AS VARCHAR) + '
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv.ValueInt AND df.Lookup = 1
			WHERE tdv.' + @LeadOrMatterClause 

			

		/* This seems to be done below now
		IF @PivotOutput = 1 
		BEGIN
			SELECT @pvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'
		END	*/

		IF @ResourceTable = 1
		BEGIN

			SELECT @ResourceListDetailFieldPageID = ResourceListDetailFieldPageID, @ResourceListDetailFieldID = DetailFieldID
			FROM dbo.DetailFields df WITH (NOLOCK)
			WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
			AND df.QuestionTypeID = 14

			SELECT @cols = COALESCE(@cols + ',[' + FieldCaption + ']', '[' + FieldCaption + ']'),
				   @rlfieldIdents = COALESCE(@rlfieldIdents + ',' + CAST(DetailFieldID AS VARCHAR(50)), CAST(DetailFieldID AS VARCHAR(50)))
			FROM dbo.DetailFields df WITH (NOLOCK)		
			WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
			AND df.Enabled = 1
			ORDER BY FieldOrder

			SET @rlQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, CASE df.Lookup WHEN 1 THEN luli.ItemValue ELSE rldv.DetailValue END as DetailValue
			FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK)
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID AND df.DetailFieldID IN (' + @rlfieldIdents + ')
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.ResourceListID = rldv.ResourceListID AND tdv.DetailFieldID = ' + CAST(@ResourceListDetailFieldID AS VARCHAR) + ' AND tdv.' + @LeadOrMatterClause + '
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = rldv.ValueInt AND df.Lookup = 1
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) 
			
		END 
		
		SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + ']', '[' + df.FieldCaption + ']'),
			   @fieldIdents = COALESCE(@fieldIdents + ',' + CAST(df.DetailFieldID AS VARCHAR(50)), CAST(df.DetailFieldID AS VARCHAR(50)))
		FROM dbo.DetailFields df WITH (NOLOCK)  
		WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
		AND df.Enabled = 1
		AND df.QuestionTypeID <> 14
		ORDER BY df.FieldOrder 
				
		IF @PivotOutput = 1 
		BEGIN
			SET @queryheader = 'SELECT pvt.TableRowID, ' + @cols + ' FROM ('
		END
		
		IF @rlQuery != ''
		BEGIN
			SELECT @query = @rlQuery
		END
		
		IF @BasicQuery != ''
		BEGIN
			IF @query != ''
			BEGIN
				SELECT @query = @query + '
				 UNION 
				'
			END
			SELECT @query = @query + @BasicQuery	
		END
		
		IF @llQuery != ''
		BEGIN
			IF @query != ''
			BEGIN
				SELECT @query = @query + '
				 UNION 
				'
			END
			SELECT @query = @query + @llQuery	
		END
		
		IF @rlllQuery != ''
		BEGIN
			IF @query != ''
			BEGIN
				SELECT @query = @query + '
				 UNION 
				'
			END
			SELECT @query = @query + @rlllQuery	
		END
		
		
		IF @PivotOutput = 1 BEGIN
			SELECT @PvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'	
		END
		

		SELECT @query = @queryheader + @query + @PvtStatement

		IF @PrintOnly = 1
		BEGIN
			PRINT @query
		END
		ELSE
		BEGIN
		
			EXECUTE(@query)	 
			
		END
	
	END
	ELSE
	BEGIN
	
		SELECT '' AS [ ]
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReturnTableForControl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReturnTableForControl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReturnTableForControl] TO [sp_executeall]
GO
