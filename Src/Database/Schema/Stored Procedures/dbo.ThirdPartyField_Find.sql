SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyFieldID int   = null ,

	@ThirdPartySystemId int   = null ,

	@FieldName varchar (250)  = null ,

	@FieldDescription varchar (250)  = null ,

	@IsEnabled bit   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyFieldID]
	, [ThirdPartySystemId]
	, [FieldName]
	, [FieldDescription]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyField] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldID] = @ThirdPartyFieldID OR @ThirdPartyFieldID IS NULL)
	AND ([ThirdPartySystemId] = @ThirdPartySystemId OR @ThirdPartySystemId IS NULL)
	AND ([FieldName] = @FieldName OR @FieldName IS NULL)
	AND ([FieldDescription] = @FieldDescription OR @FieldDescription IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyFieldID]
	, [ThirdPartySystemId]
	, [FieldName]
	, [FieldDescription]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyField] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldID] = @ThirdPartyFieldID AND @ThirdPartyFieldID is not null)
	OR ([ThirdPartySystemId] = @ThirdPartySystemId AND @ThirdPartySystemId is not null)
	OR ([FieldName] = @FieldName AND @FieldName is not null)
	OR ([FieldDescription] = @FieldDescription AND @FieldDescription is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Find] TO [sp_executeall]
GO
