SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 24-03-2014
-- Description:	Gets DiaryReminders for the give DiaryAppointment
-- =============================================
CREATE PROCEDURE [dbo].[OutlookAppointments_GetAttendees]
	
	@ClientID INT,
	@DiaryAppointmentID INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT cp.EmailAddress, cp.FirstName, cp.LastName, r.* 
	FROM DiaryReminder r WITH (NOLOCK) 
	INNER JOIN ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID=r.ClientPersonnelID
	WHERE r.DiaryAppointmentID=@DiaryAppointmentID AND r.ClientID=@ClientID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[OutlookAppointments_GetAttendees] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutlookAppointments_GetAttendees] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutlookAppointments_GetAttendees] TO [sp_executeall]
GO
