SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeDefinition table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDefinition_GetByWhoModified]
(

	@WhoModified int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChangeDefinitionID],
					[ClientID],
					[ChangeDescription],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[RequiredDate],
					[Deleted]
				FROM
					[dbo].[ChangeDefinition] WITH (NOLOCK) 
				WHERE
					[WhoModified] = @WhoModified
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_GetByWhoModified] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDefinition_GetByWhoModified] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_GetByWhoModified] TO [sp_executeall]
GO
