SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-19
-- Description:	Run batch jobs based on events 
--				2009-11-30 AE  Removed Enabled = 1, as clients etc are having difficulties turning tasks off.
--				2010-07-07 JWG Added a clause to allow the task to be triggered sometimes even if AddTimeDelayIfAboutToRun=FALSE 
--                         and there is a NextRunDateTime, because a handful of clients set the job to run every day as well as run on demand.
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedTaskFromLeadEvent] 
	@LeadEventID int,
	@EventTypeID int = null -- This will be looked up if null 
AS
BEGIN

	SET NOCOUNT ON;

    -- If all IDs are known then we have slightly less work to do,
	-- otherwise look them up from the LeadEvent record.
	IF @EventTypeID IS NULL
	BEGIN

		SELECT @EventTypeID = EventTypeID 
		FROM LeadEvent WITH (NOLOCK) 
		WHERE LeadEventID = @LeadEventID 

	END

	IF @EventTypeID IS NOT NULL
	BEGIN

		UPDATE AutomatedTask 
		SET NextRunDateTime = dateadd(ss, e.TimeDelayInSeconds, dbo.fn_GetDate_Local()) 
		FROM dbo.AutomatedTask a 
		INNER JOIN dbo.EventTypeAutomatedTask e ON e.AutomatedTaskID = a.TaskID 
		WHERE e.EventTypeID = @EventTypeID 
		AND (
				(e.AddTimeDelayIfAboutToRun = 1)  -- Always override any other pending runs
				OR
				(a.NextRunDateTime IS NULL)       -- Not set to run
				OR
				(datediff(ss, dbo.fn_GetDate_Local(), a.NextRunDateTime) > e.TimeDelayInSeconds AND a.RepeatTimeQuantity IS NOT NULL AND a.RepeatTimeUnitsID IS NOT NULL)       -- The job is set to run regularly, so let it run now if there is time
			)

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTaskFromLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTaskFromLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTaskFromLeadEvent] TO [sp_executeall]
GO
