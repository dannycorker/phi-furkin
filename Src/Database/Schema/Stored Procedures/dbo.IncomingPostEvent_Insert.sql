SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the IncomingPostEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEvent_Insert]
(

	@IncomingPostEventID int    OUTPUT,

	@EventDateTime datetime   ,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@MatterID int   
)
AS


				
				INSERT INTO [dbo].[IncomingPostEvent]
					(
					[EventDateTime]
					,[ClientID]
					,[CustomerID]
					,[LeadID]
					,[CaseID]
					,[MatterID]
					)
				VALUES
					(
					@EventDateTime
					,@ClientID
					,@CustomerID
					,@LeadID
					,@CaseID
					,@MatterID
					)
				-- Get the identity value
				SET @IncomingPostEventID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEvent_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Insert] TO [sp_executeall]
GO
