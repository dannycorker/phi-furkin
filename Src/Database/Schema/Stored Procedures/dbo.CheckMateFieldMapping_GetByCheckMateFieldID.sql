SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CheckMateFieldMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_GetByCheckMateFieldID]
(

	@CheckMateFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CheckMateFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[CheckMateFieldID],
					[FieldID],
					[ColumnFieldID],
					[DataLoaderObjectTypeID],
					[CheckMateFieldGroupID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[CheckMateFieldMapping] WITH (NOLOCK) 
				WHERE
					[CheckMateFieldID] = @CheckMateFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_GetByCheckMateFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_GetByCheckMateFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_GetByCheckMateFieldID] TO [sp_executeall]
GO
