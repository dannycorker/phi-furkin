SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetCustomerOutcomes] @CustomerID int, @ClientQuestionnaireID int

AS

SELECT     dbo.Outcomes.OutcomeID, dbo.Outcomes.ClientQuestionnaireID, dbo.Outcomes.OutcomeName, dbo.Outcomes.OutcomeDescription, 
                      dbo.Outcomes.SmsWhenFound, dbo.Outcomes.mobileNumber, dbo.Outcomes.EmailCustomer, dbo.Outcomes.CustomersEmail, 
                      dbo.Outcomes.ThankYouPage, dbo.Outcomes.EmailFromAddress, dbo.Outcomes.SmsToCustomer, dbo.CustomerOutcomes.CustomerID, 
                      dbo.CustomerOutcomes.ClientQuestionnaireID, dbo.Outcomes.ClientID 
FROM         dbo.Outcomes WITH (NOLOCK) INNER JOIN
                      dbo.CustomerOutcomes WITH (NOLOCK) ON dbo.Outcomes.OutcomeID = dbo.CustomerOutcomes.OutcomeID
WHERE     (dbo.CustomerOutcomes.CustomerID = @CustomerID) AND (dbo.CustomerOutcomes.ClientQuestionnaireID = @ClientQuestionnaireID)


GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerOutcomes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerOutcomes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerOutcomes] TO [sp_executeall]
GO
