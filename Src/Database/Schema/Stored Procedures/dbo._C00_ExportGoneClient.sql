SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-07-17
-- Description:	Export client (Client Gone)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ExportGoneClient]
	@ClientID INT,
	@LeadTypeID INT,
	@RecordCount INT = 1000
AS
BEGIN

	SET NOCOUNT ON;


	/*
	Extract all data for Client @ClientID
	CS 2013-11-27
	*/

	DECLARE @dSQL			VARCHAR(MAX),
			@AnyID			INT,
			@AnyVarchar		VARCHAR(MAX) = ''

	DECLARE @SampleMatters TABLE (MatterID INT)
	INSERT @SampleMatters (MatterID)
	SELECT TOP(@RecordCount) m.MatterID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.LeadID and l.LeadTypeID = @LeadTypeID
	WHERE m.ClientID = @ClientID
	ORDER BY m.CustomerID

	SELECT @AnyVarchar = @AnyVarchar + '[' + CASE df.LeadOrMatter WHEN 1 THEN 'ldv_' WHEN 2 THEN 'mdv_' ELSE 'cdv_' END  + df.FieldCaption + '],'
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.ClientID = @ClientID
	and df.LeadOrMatter IN(1,2,10)
	and df.LeadTypeID IN(@LeadTypeID, 0) /*0 = Customer*/
	and df.Enabled = 1
	and not exists ( SELECT * FROM Detailfields df2 WITH (NOLOCK) WHERE df2.DetailFieldID > df.DetailFieldID and df2.Enabled = 1 and df2.LeadOrMatter = df.LeadOrMatter and df2.LeadTypeID = df.LeadTypeID and df2.FieldCaption = df.FieldCaption )

	SELECT @AnyVarchar = LEFT(@AnyVarchar,LEN(@AnyVarchar)-1) /*Strip off the trailing comma*/

	PRINT @AnyVarchar

	SELECT @dSQL = 
	'
	DECLARE @SampleMatters TABLE (MatterID INT)
	INSERT @SampleMatters (MatterID)
	SELECT TOP(' + CONVERT(VARCHAR,@RecordCount) + ') m.MatterID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.LeadID and l.LeadTypeID = ' + CONVERT(VARCHAR,@LeadTypeID) + '  
	WHERE m.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	ORDER BY m.CustomerID

	;With AllDetailValues as 
	(
	SELECT sm.MatterID, mdv.DetailFieldID, ISNULL(li.ItemValue, mdv.DetailValue) [DetailValue], ''mdv_'' + df.FieldCaption [FieldCaption]
	FROM @SampleMatters sm 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = sm.MatterID
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = mdv.DetailFieldID
	LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = mdv.ValueInt and li.LookupListID = df.LookupListID

	UNION

	SELECT sm.MatterID, ldv.DetailFieldID, ISNULL(li.ItemValue, ldv.DetailValue) [DetailValue], ''ldv_'' + df.FieldCaption [FieldCaption]
	FROM @SampleMatters sm 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = sm.MatterID
	INNER JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) on ldv.LeadID = m.LeadID
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = ldv.DetailFieldID
	LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = ldv.ValueInt and li.LookupListID = df.LookupListID

	UNION

	SELECT sm.MatterID, cdv.DetailFieldID, ISNULL(li.ItemValue, cdv.DetailValue) [DetailValue], ''cdv_'' + df.FieldCaption [FieldCaption]
	FROM @SampleMatters sm 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = sm.MatterID
	INNER JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = m.CustomerID
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = cdv.DetailFieldID
	LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = cdv.ValueInt and li.LookupListID = df.LookupListID
	)
	SELECT TOP(' + CONVERT(VARCHAR,@RecordCount) + ') *
	FROM 
	( 
		SELECT cu.CustomerID, t.Title, cu.FirstName, cu.MiddleName, cu.LastName, pt.Title [PartnerTitle], p.FirstName [PartnerFirstName], p.LastName [PartnerLastName], cu.HomeTelephone, cu.MobileTelephone, cu.Address1, cu.Address2, cu.Town, cu.PostCode, cu.EmailAddress, cu.DateOfBirth, cu.employer, p.Address1 [PartnerAddress1], p.Address2 [PartnerAddress2], p.Town [PartnerTown], p.County [PartnerCounty], p.PostCode [PartnerPostCode], p.DateOfBirth [PartnerDateOfBirth],m.LeadID, l.LeadRef,m.CaseID, m.MatterID, m.MatterRef, ls.StatusName, et.EventTypeName [LatestEvent], av.FieldCaption, av.DetailValue
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN dbo.Customers cu WITH (NOLOCK) on cu.CustomerID = m.CustomerID and cu.Test = 0
		LEFT JOIN Partner p WITH (NOLOCK) on p.CustomerID = cu.CustomerID
		LEFT JOIN Titles pt WITH (NOLOCK) on pt.TitleID = p.TitleID
		LEFT JOIN Titles t WITH (NOLOCK) on t.TitleID = cu.TitleID
		INNER JOIN AllDetailValues av on av.MatterID = m.MatterID
		INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.LeadID and l.LeadTypeID = ' + CONVERT(VARCHAR,@LeadTypeID) + '
		INNER JOIN dbo.Cases c WITH (NOLOCK) on c.CaseID = m.CaseID 
		LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) on ls.StatusID = c.ClientStatusID
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadEventID = c.LatestNonNoteLeadEventID
		INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID
		INNER JOIN @SampleMatters sm on sm.MatterID = m.MatterID
		WHERE m.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '	
	)
	AS ToPivot 
	PIVOT 
	( 
	Max([DetailValue]) 
	FOR 
	[FieldCaption]
	IN (' + @AnyVarchar + ')
	) AS Pivoted
	ORDER BY CustomerID
	'
	EXEC (@dSQL) 

	/*Event History*/
	SELECT c.LeadID, c.CaseID, c.CaseNum, le.WhenCreated, le.EventTypeID, ISNULL(et.EventTypeName,'NOTE: ' + nt.NoteTypeName) [Type], le.LeadDocumentID, le.FollowupDateTime, le.Comments
	FROM dbo.Customers cu WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.CustomerID = cu.CustomerID 
	INNER JOIN dbo.Cases c WITH (NOLOCK) on c.LeadID = l.LeadID 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.CaseID = c.CaseID 
	LEFT JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID
	LEFT JOIN NoteType nt WITH (NOLOCK) on nt.NoteTypeID = le.NoteTypeID
	INNER JOIN @SampleMatters sm on sm.MatterID = m.MatterID
	WHERE cu.Test = 0
	and l.LeadTypeID = @LeadTypeID
	ORDER BY l.CustomerID, l.LeadID, c.CaseID, le.WhenCreated

	/*Extract all tables using dSQL pivots*/
	DECLARE @TableFields TABLE ( DetailFieldID INT, Name VARCHAR(100), DetailFieldPageID INT, DetailFieldSubtype VARCHAR(100), FieldList VARCHAR(MAX) , Done BIT )
	INSERT @TableFields ( DetailFieldID, Name, DetailFieldPageID, DetailFieldSubtype, FieldList, Done )
	SELECT df.DetailFieldID, df.FieldCaption, df.TableDetailFieldPageID, st.DetailFieldSubTypeName, '', 0
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN DetailFieldSubType st WITH (NOLOCK) on st.DetailFieldSubTypeID = df.LeadOrMatter
	WHERE df.LeadTypeID IN(@LeadTypeID,0)
	and df.ClientID = @ClientID
	and df.QuestionTypeID in(16,19)
	and exists ( SELECT * FROM DetailFields df2 WITH (NOLOCK) WHERE df2.DetailFieldPageID = df.TableDetailFieldPageID )

	SELECT tf.DetailFieldID [TableFieldID], tf.Name, tf.DetailFieldSubtype
	FROM @TableFields tf

	WHILE EXISTS ( SELECT * FROM @TableFields tf WHERE tf.Done = 0 )
	BEGIN
		SELECT @AnyVarchar = ''
		
		/*Pick up the table to action*/
		SELECT top(1) @AnyID = tf.DetailFieldID
		FROM @TableFields tf 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldPageID = tf.DetailFieldPageID
		WHERE tf.Done = 0 
		
		/*list its fields*/
		SELECT @AnyVarchar = @AnyVarchar + '[' + CONVERT(VARCHAR,df.FieldCaption) + '],'
		FROM @TableFields tf 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldPageID = tf.DetailFieldPageID
		WHERE tf.DetailFieldID = @AnyID
			
		/*Strip the trailing comma*/	
		SELECT @AnyVarchar = LEFT(@AnyVarchar,LEN(@AnyVarchar)-1)
		UPDATE tf
		SET FieldList = @AnyVarchar
		FROM @TableFields tf 
		WHERE tf.DetailFieldID = @AnyID
		
		SELECT @dSQL =
	'
	SELECT ''' + tf.Name + ''' [TableType], *
	FROM		
		(	
		SELECT TOP(' + CONVERT(VARCHAR,@RecordCount) + ') tr.' + tf.DetailFieldSubtype + 'ID, tr.TableRowID, df.FieldCaption, isnull(li.ItemValue, tdv.DetailValue) [DetailValue]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
		LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt and df.LookupListID = li.LookupListID
		WHERE tr.DetailFieldID = ' + CONVERT(VARCHAR,tf.DetailFieldID) + ' 
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailValue])		
	FOR		
	[FieldCaption]
		IN ( ' + tf.FieldList + ' )
	) AS Pivoted
	ORDER BY [' + tf.DetailFieldSubtype + 'ID], [TableRowID]'	
		FROM @TableFields tf
		WHERE tf.DetailFieldID = @AnyID
		
	--	PRINT @dSql
		
		EXEC (@dSQL)
		
		UPDATE tf
		SET Done = 1
		FROM @TableFields tf 
		WHERE tf.DetailFieldID = @AnyID
		
		SELECT @dSQL = NULL, @AnyID = NULL, @AnyVarchar = NULL
	END


	/*Pull out diary appointments*/
	SELECT da.DiaryAppointmentID, da.DiaryAppointmentTitle [Title], da.DiaryAppointmentText [Message], cp.UserName, da.DueDate, da.EndDate, da.ClientID, da.CaseID
	FROM DiaryAppointment da WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = da.CaseID 
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.LeadID and l.LeadTypeID = @LeadTypeID
	INNER JOIN @SampleMatters sm on sm.MatterID = m.MatterID
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = da.CreatedBy
	WHERE da.ClientID = @ClientID


	/*Extract all ResourceLists*/
	DECLARE @ResourceListPages dbo.tvpIntInt
	INSERT @ResourceListPages (ID1, ID2)
	SELECT DISTINCT df.DetailFieldPageID, 0
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.QuestionTypeID = 14
	and ( df.LeadTypeID IN(@LeadTypeID,0) ) /*0 = customer*/
	and df.ClientID = @ClientID

	WHILE EXISTS ( SELECT * FROM @ResourceListPages rp WHERE rp.ID2 = 0 )
	BEGIN
		SELECT top(1) @AnyID = rp.ID1
		FROM @ResourceListPages rp
		WHERE rp.ID2 = 0
		
		SELECT @dSQL = 'EXEC rldv null, null, ' + CONVERT(VARCHAR,@AnyID)
		EXEC (@dSQL)
		
		PRINT @dSQL
		
		UPDATE rp
		SET ID2 = 1
		FROM @ResourceListPages rp
		WHERE rp.ID1 = @AnyID
		
		SELECT @dSQL = '',	@AnyID = NULL
	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ExportGoneClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ExportGoneClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ExportGoneClient] TO [sp_executeall]
GO
