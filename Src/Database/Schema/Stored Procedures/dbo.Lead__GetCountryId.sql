SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 03/03/2020
-- Description:	Gets the country identity for the given customer
-- =============================================
CREATE PROCEDURE [dbo].[Lead__GetCountryId]
	@LeadID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @CustomerID INT
	
	SELECT @CustomerID=CustomerID 
	FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

	SELECT CountryID FROM  Customers WITH (NOLOCK) 
	WHERE CustomerID=@CustomerID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetCountryId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__GetCountryId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetCountryId] TO [sp_executeall]
GO
