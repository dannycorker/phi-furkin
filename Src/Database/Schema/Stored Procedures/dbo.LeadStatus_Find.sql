SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@StatusID int   = null ,

	@ClientID int   = null ,

	@StatusName varchar (50)  = null ,

	@StatusDescription varchar (250)  = null ,

	@SourceID int   = null ,

	@LeadTypeID int   = null ,

	@IsShared bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [StatusID]
	, [ClientID]
	, [StatusName]
	, [StatusDescription]
	, [SourceID]
	, [LeadTypeID]
	, [IsShared]
    FROM
	dbo.fnLeadStatusShared(@ClientID)
    WHERE 
	 ([StatusID] = @StatusID OR @StatusID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([StatusName] = @StatusName OR @StatusName IS NULL)
	AND ([StatusDescription] = @StatusDescription OR @StatusDescription IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [StatusID]
	, [ClientID]
	, [StatusName]
	, [StatusDescription]
	, [SourceID]
	, [LeadTypeID]
	, [IsShared]
    FROM
	dbo.fnLeadStatusShared(@ClientID) 
    WHERE 
	 ([StatusID] = @StatusID AND @StatusID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([StatusName] = @StatusName AND @StatusName is not null)
	OR ([StatusDescription] = @StatusDescription AND @StatusDescription is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Find] TO [sp_executeall]
GO
