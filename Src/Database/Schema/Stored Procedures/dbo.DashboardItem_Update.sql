SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DashboardItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItem_Update]
(

	@DashboardItemID int   ,

	@DashboardID int   ,

	@DashboardItemTypeID int   ,

	@ItemID int   ,

	@ClientID int   ,

	@LayoutPosition int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DashboardItem]
				SET
					[DashboardID] = @DashboardID
					,[DashboardItemTypeID] = @DashboardItemTypeID
					,[ItemID] = @ItemID
					,[ClientID] = @ClientID
					,[LayoutPosition] = @LayoutPosition
				WHERE
[DashboardItemID] = @DashboardItemID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItem_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Update] TO [sp_executeall]
GO
