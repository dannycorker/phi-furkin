SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Buy policy
-- Mods:
--  2014-12-19 DCM changed policy number to MatterID
-- 2015-02-24 DCM added microchip number
--	2015-05-14 DCM Ticket #31733 Entire contents moved to new proc to facilitate renaming in prelive
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_BuyPolicy]
(
	@XmlRequest XML,
	@Debug BIT = 0
)
AS
BEGIN

 EXEC _C600_PA_Policy_BuyPolicy @XmlRequest, @Debug	
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_BuyPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_BuyPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_BuyPolicy] TO [sp_executeall]
GO
