SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UploadedFile table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_GetByWhoModified]
(

	@WhoModified int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[UploadedFileID],
					[ClientID],
					[FolderID],
					[UploadedFileName],
					[UploadedFileTitle],
					[UploadedFileDescription],
					[UploadedFile],
					[UploadedFileGUID],
					[UploadedFileSize],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[UploadedFile] WITH (NOLOCK) 
				WHERE
					[WhoModified] = @WhoModified
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_GetByWhoModified] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_GetByWhoModified] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_GetByWhoModified] TO [sp_executeall]
GO
