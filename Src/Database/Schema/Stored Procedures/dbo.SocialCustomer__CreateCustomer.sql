SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2015
-- Description:	Creates a customer, lead, case, matter, process start and social customer 
-- =============================================
CREATE PROCEDURE [dbo].[SocialCustomer__CreateCustomer]

	@ClientID INT,
	@LeadTypeID INT,
	@SocialTypeID INT,
	@ScreenName VARCHAR(250),
	@SocialUserID VARCHAR(50)		

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UserID INT
	EXEC @UserID = dbo.fn_C00_GetAutomationUser @ClientID
	
	DECLARE @CustomerID INT	
	EXEC @CustomerID = dbo._C00_CreateNewCustomer @ClientID, @LeadTypeID, 0, @ScreenName, '', '', '', '', '', '', '', '', '', '', @UserID, '', 232, 1, 1, 1, ''
		
	DECLARE @When DATETIME
	SELECT @When = dbo.fn_GetDate_Local();
	
	IF @CustomerID>0	
	BEGIN
				
		EXEC dbo.SocialCustomer__Insert @ClientID, @CustomerID, @SocialTypeID, @ScreenName, @SocialUserID, @When, @UserID, @When, @UserID
		
	END
	
	SELECT m.CustomerID, m.LeadID, m.CaseID, m.MatterID, sc.SocialCustomerID
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN dbo.SocialCustomer sc WITH (NOLOCK) ON sc.CustomerID = m.CustomerID AND sc.SocialUserID=@SocialUserID
	WHERE m.CustomerID=@CustomerID
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__CreateCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialCustomer__CreateCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__CreateCustomer] TO [sp_executeall]
GO
