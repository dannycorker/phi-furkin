SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Titles table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Titles_GetByTitleID]
(

	@TitleID int   
)
AS


				SELECT
					[TitleID],
					[Title]
				FROM
					[dbo].[Titles] WITH (NOLOCK) 
				WHERE
										[TitleID] = @TitleID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_GetByTitleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Titles_GetByTitleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_GetByTitleID] TO [sp_executeall]
GO
