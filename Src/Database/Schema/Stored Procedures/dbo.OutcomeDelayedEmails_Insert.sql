SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the OutcomeDelayedEmails table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeDelayedEmails_Insert]
(

	@OutcomeDelayedEmailID int    OUTPUT,

	@CustomerID int   ,

	@ClientQuestionnaireID int   ,

	@DateOutcomeEmailSent datetime   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[OutcomeDelayedEmails]
					(
					[CustomerID]
					,[ClientQuestionnaireID]
					,[DateOutcomeEmailSent]
					,[ClientID]
					)
				VALUES
					(
					@CustomerID
					,@ClientQuestionnaireID
					,@DateOutcomeEmailSent
					,@ClientID
					)
				-- Get the identity value
				SET @OutcomeDelayedEmailID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeDelayedEmails_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Insert] TO [sp_executeall]
GO
