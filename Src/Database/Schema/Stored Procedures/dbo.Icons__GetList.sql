SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-13
-- Description:	Get a list of icons
-- =============================================
CREATE PROCEDURE [dbo].[Icons__GetList]
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @Icons TABLE (Icon VARCHAR(100))

	INSERT INTO @Icons (Icon)
	VALUES ('sort-date.png'),
			('leads.gif'),
			('barchart.gif'),
			('database_add.png'),
			('users.gif'),
			('admin.gif'),
			('cart.png'),
			('map-pin.png'),
			('magnifier-zoom.png'),
			('diary.gif'),
			('add.png'),
			('add_blue.png'),
			('edit_company.gif')

	SELECT *
	FROM @Icons

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Icons__GetList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Icons__GetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Icons__GetList] TO [sp_executeall]
GO
