SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-02
-- Description:	List all Lead fields for a lead or type
-- =============================================
CREATE PROCEDURE [dbo].[c] 
	@CustomerID int = NULL,
	@LeadID int = NULL,
	@ClientID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	/* Must give us something to go on! */
	IF @CustomerID IS NULL AND @LeadID IS NULL AND @ClientID IS NULL
	BEGIN
		SELECT 'Please supply at least one of @CustomerID, @LeadID or @ClientID'
		
		RETURN
	END
	
	/* Grab a sample Customer if only ClientID was provided */
	IF @CustomerID IS NULL AND @LeadID IS NULL
	BEGIN
		SELECT TOP 1 @CustomerID = CustomerID 
		FROM dbo.Customers c (nolock) 
		WHERE c.ClientID = @ClientID
		ORDER BY c.CustomerID DESC
	END
	
	/* Get Customer from Lead if only LeadID was provided */
	IF @CustomerID IS NULL AND @LeadID IS NOT NULL
	BEGIN
		SELECT @CustomerID = CustomerID 
		FROM dbo.Lead l (nolock) 
		WHERE l.LeadID = @LeadID 
	END
	
	IF @CustomerID IS NOT NULL
	BEGIN

		/* Show Customer details */
		IF EXISTS ( SELECT * FROM Customers c WITH (NOLOCK)  WHERE c.CustomerID = @CustomerID and c.Test = 1 )
		BEGIN
			SELECT 'THIS IS A TEST CUSTOMER' [TEST]
		END
		
		SELECT * 
		FROM dbo.vCustomersIncTest c WITH (NOLOCK)  
		WHERE c.CustomerID = @CustomerID 
	
		IF @LeadID IS NOT NULL
		BEGIN 
			/* Show specific Lead details */
			EXEC dbo.l @LeadID
		END
		ELSE
		BEGIN
			/* Show all Lead for this Customer */
			SELECT l.*, lt.* 
			FROM dbo.Lead l WITH (NOLOCK) 
			INNER JOIN dbo.LeadType lt WITH (NOLOCK)  on lt.LeadTypeID = l.LeadTypeID 
			WHERE l.CustomerID = @CustomerID 
			ORDER BY l.LeadID 
		END
		
		/* Contacts */
		SELECT * 
		FROM dbo.Contact c WITH (NOLOCK) 
		WHERE c.CustomerID = @CustomerID 
		
		/* Questionnaire Details */
		SELECT * 
		FROM dbo.vCustomerQuestionnaires c WITH (NOLOCK) 
		WHERE c.CustomerID = @CustomerID 

		SELECT * 
		FROM dbo.vCustomerAnswers c WITH (NOLOCK) 
		WHERE c.CustomerID = @CustomerID 		
	END

END







GO
GRANT VIEW DEFINITION ON  [dbo].[c] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[c] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[c] TO [sp_executeall]
GO
