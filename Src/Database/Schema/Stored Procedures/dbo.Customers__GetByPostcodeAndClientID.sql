SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-04-2016
-- Description:	Gets a list of customers via postcode
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByPostcodeAndClientID]

	@ClientID INT,
	@Postcode VARCHAR(200)

AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND PostCode Like '%' + @Postcode + '%'
    	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByPostcodeAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByPostcodeAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByPostcodeAndClientID] TO [sp_executeall]
GO
