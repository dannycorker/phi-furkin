SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 16/02/2015
-- Description:	Find a basic table detail field based upon a partial field name
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__FindBasicTableByPartialFieldName]
	@ClientID INT,
	@FieldName VARCHAR(50),
	@LeadTypeID INT	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM DetailFields WITH (NOLOCK) 
	WHERE 
		ClientID=@ClientID AND 
		(LeadTypeID=@LeadTypeID OR LeadTypeID=0) AND
		QuestionTypeID = 19 AND -- only return basic tables
		FieldName LIKE '%' + @FieldName + '%'
		  
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindBasicTableByPartialFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__FindBasicTableByPartialFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindBasicTableByPartialFieldName] TO [sp_executeall]
GO
