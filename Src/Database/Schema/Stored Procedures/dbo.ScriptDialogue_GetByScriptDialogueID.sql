SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptDialogue table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_GetByScriptDialogueID]
(

	@ScriptDialogueID int   
)
AS


				SELECT
					[ScriptDialogueID],
					[ScriptSectionID],
					[ClientID],
					[Dialogue],
					[HtmlDialogue],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified]
				FROM
					[dbo].[ScriptDialogue] WITH (NOLOCK) 
				WHERE
										[ScriptDialogueID] = @ScriptDialogueID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_GetByScriptDialogueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_GetByScriptDialogueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_GetByScriptDialogueID] TO [sp_executeall]
GO
