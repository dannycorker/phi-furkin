SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the UserFunctionControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_Update]
(

	@UserFunctionControlID int   ,

	@ClientPersonnelID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants int   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[UserFunctionControl]
				SET
					[ClientPersonnelID] = @ClientPersonnelID
					,[ModuleID] = @ModuleID
					,[FunctionTypeID] = @FunctionTypeID
					,[HasDescendants] = @HasDescendants
					,[RightID] = @RightID
					,[LeadTypeID] = @LeadTypeID
				WHERE
[UserFunctionControlID] = @UserFunctionControlID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Update] TO [sp_executeall]
GO
