SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-29
-- Description:	Copy all Group Rights etc to temp/work table
-- =============================================
CREATE PROCEDURE [dbo].[CommitGroupRightsForEditing]
	-- Add the parameters for the stored procedure here
	@GroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

	DELETE GroupRightsDynamic 
	WHERE ClientPersonnelAdminGroupID = @GroupID

	DELETE GroupFunctionControl
	WHERE ClientPersonnelAdminGroupID = @GroupID

	INSERT GroupFunctionControl (ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)
	SELECT ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID
	FROM GroupFunctionControlEditing
	WHERE ClientPersonnelAdminGroupID = @GroupID

	INSERT GroupRightsDynamic (ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
	SELECT ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID
	FROM GroupRightsDynamicEditing
	WHERE ClientPersonnelAdminGroupID = @GroupID

	DELETE GroupRightsDynamicEditing 
	WHERE ClientPersonnelAdminGroupID = @GroupID

	DELETE GroupFunctionControlEditing
	WHERE ClientPersonnelAdminGroupID = @GroupID

	EXEC ClearRedundantUserRights @GroupID

	COMMIT

END










GO
GRANT VIEW DEFINITION ON  [dbo].[CommitGroupRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CommitGroupRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CommitGroupRightsForEditing] TO [sp_executeall]
GO
