SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptDialogue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_Get_List]

AS


				
				SELECT
					[ScriptDialogueID],
					[ScriptSectionID],
					[ClientID],
					[Dialogue],
					[HtmlDialogue],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified]
				FROM
					[dbo].[ScriptDialogue] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Get_List] TO [sp_executeall]
GO
