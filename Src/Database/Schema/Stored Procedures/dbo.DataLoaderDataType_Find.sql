SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderDataType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDataType_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderDataTypeID int   = null ,

	@DataTypeName varchar (250)  = null ,

	@DataTypeDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderDataTypeID]
	, [DataTypeName]
	, [DataTypeDescription]
    FROM
	[dbo].[DataLoaderDataType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderDataTypeID] = @DataLoaderDataTypeID OR @DataLoaderDataTypeID IS NULL)
	AND ([DataTypeName] = @DataTypeName OR @DataTypeName IS NULL)
	AND ([DataTypeDescription] = @DataTypeDescription OR @DataTypeDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderDataTypeID]
	, [DataTypeName]
	, [DataTypeDescription]
    FROM
	[dbo].[DataLoaderDataType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderDataTypeID] = @DataLoaderDataTypeID AND @DataLoaderDataTypeID is not null)
	OR ([DataTypeName] = @DataTypeName AND @DataTypeName is not null)
	OR ([DataTypeDescription] = @DataTypeDescription AND @DataTypeDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDataType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Find] TO [sp_executeall]
GO
