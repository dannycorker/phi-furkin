SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteQuestionPossibleAnswer    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[DeleteQuestionPossibleAnswer] @QuestionPossibleAnswerID int
AS
Delete QuestionPossibleAnswers
where QuestionPossibleAnswerID = @QuestionPossibleAnswerID





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteQuestionPossibleAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteQuestionPossibleAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteQuestionPossibleAnswer] TO [sp_executeall]
GO
