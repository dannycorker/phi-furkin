SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-05-18
-- Description: Rename scripting menu options (replaces them)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UserGroupMenuOption_Scripting_Rename]

	@ClientID INT 
	,@NewNameScripting		VARCHAR(200) 
	,@NewNameViewScriptSubMenu VARCHAR(200)
	,@NewNameEditScriptSubMenu VARCHAR(200) 

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ClientPersonnelAdminGroupID INT =406  /*  null = all | 1=> Admin  |  2=> Normal Users  */
		, @MasterPageName VARCHAR(20) = 'leadmanager'
		, @PanelItemAction VARCHAR(10) = 'HIDE'
		, @PanelItemName VARCHAR(50) = 'pnlScripting'--'pnlScripting_Text'
		, @PanelItemCaption VARCHAR(50) ='Scripting' /*This is the name displayed on the page*/
		, @PanelItemIcon VARCHAR(50) = 'leads.gif'
		, @PanelItemURL VARCHAR(250) = ''
		, @WhoCreated INT   = 25984
		, @WhenCreated DATETIME = dbo.fn_GetDate_Local()
		, @WhoModified INT = 25984
		, @WhenModified DATETIME = dbo.fn_GetDate_Local()
		, @Comments VARCHAR(250) ='Scripting Tool HIDE default'
		, @ItemOrder INT = 0
		, @ShowInFancyBox BIT = 0
		
	DECLARE @UserGroupMenuOptionID INT = 0

	SELECT @UserGroupMenuOptionID = mu.UserGroupMenuOptionID FROM  UserGroupMenuOption mu WITH (NOLOCK) 
	WHERE mu.ClientID=@ClientID
	AND mu.PanelItemName = 'pnlScripting'
	and mu.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	order by mu.UserGroupMenuOptionID desc
	
	/*If there are any existing menu options remove them*/
	WHILE @UserGroupMenuOptionID > 0 
	BEGIN
		
		/*Delete existing option*/
		EXEC dbo.UserGroupMenuOption_Delete @UserGroupMenuOptionID
		
		SELECT @UserGroupMenuOptionID = 0
		
		SELECT TOP 1 @UserGroupMenuOptionID = mu.UserGroupMenuOptionID
		FROM  UserGroupMenuOption mu WITH (NOLOCK) 
		WHERE mu.ClientID=@ClientID
		AND mu.PanelItemName = 'pnlScripting'
		and mu.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
		
	END

	/*Add new HIDDEN option*/
	EXECUTE UserGroupMenuOption_Insert @UserGroupMenuOptionID,@ClientPersonnelAdminGroupID  ,@ClientID  ,@MasterPageName  ,@PanelItemAction  ,@PanelItemName  ,@PanelItemCaption  ,@PanelItemIcon  ,@PanelItemURL  ,@WhoCreated ,@WhenCreated  ,@WhoModified  ,@WhenModified  ,@Comments  ,@ItemOrder  ,@ShowInFancyBox

	/*Add new VISIBLE option*/
	SELECT @PanelItemAction = 'ADD',@PanelItemCaption = @NewNameScripting

	EXECUTE UserGroupMenuOption_Insert @UserGroupMenuOptionID,@ClientPersonnelAdminGroupID  ,@ClientID  ,@MasterPageName  ,@PanelItemAction  ,@PanelItemName  ,@PanelItemCaption  ,@PanelItemIcon  ,@PanelItemURL  ,@WhoCreated ,@WhenCreated  ,@WhoModified  ,@WhenModified  ,@Comments  ,@ItemOrder  ,@ShowInFancyBox

	/*Select the new UserGroupMenuOptionID*/
	SELECT TOP 1 @UserGroupMenuOptionID = mu.UserGroupMenuOptionID 
	FROM UserGroupMenuOption mu WITH (NOLOCK) 
	WHERE mu.ClientID=@ClientID
	AND mu.PanelItemName = 'pnlScripting'
	AND mu.PanelItemAction = 'ADD'
	and mu.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	order by mu.UserGroupMenuOptionID desc
	
	DECLARE @UserGroupMenuSubOptionID INT
	SELECT @PanelItemName =  @NewNameViewScriptSubMenu
	, @PanelItemCaption = @NewNameViewScriptSubMenu
	,@PanelItemURL = 'https://scripting.aquarium-software.com/'
	DECLARE @SubItemOrder INT = 1

	/*Insert view script submenu option*/
	EXECUTE [UserGroupMenuSubOption_Insert] @UserGroupMenuSubOptionID OUTPUT,@UserGroupMenuOptionID  ,@ClientID  ,@ClientPersonnelAdminGroupID  ,@PanelItemName  ,@PanelItemCaption  ,@PanelItemURL  ,@ShowInFancyBox  ,@SubItemOrder

	SELECT @PanelItemName = @NewNameEditScriptSubMenu
	,@PanelItemCaption = @NewNameEditScriptSubMenu
	,@PanelItemURL = 'https://scripting.aquarium-software.com/editor'
	,@SubItemOrder = 2

	/*Insert the Editor submenuoption*/
	EXECUTE [UserGroupMenuSubOption_Insert] @UserGroupMenuSubOptionID OUTPUT,@UserGroupMenuOptionID  ,@ClientID  ,@ClientPersonnelAdminGroupID  ,@PanelItemName  ,@PanelItemCaption  ,@PanelItemURL  ,@ShowInFancyBox  ,@SubItemOrder

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UserGroupMenuOption_Scripting_Rename] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UserGroupMenuOption_Scripting_Rename] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UserGroupMenuOption_Scripting_Rename] TO [sp_executeall]
GO
