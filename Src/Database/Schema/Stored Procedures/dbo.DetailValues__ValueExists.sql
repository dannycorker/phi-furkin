SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
* Author: 	Ben Crinion
* Date:		24 April 2007
* Purpose:	Find out if a value exists for a 
*		particular lead or matter id
*/
CREATE PROCEDURE [dbo].[DetailValues__ValueExists]
	@DetailFieldId int,
	@LeadId int = NULL,
	@MatterId int = NULL
AS


if(@LeadID is Null)

Select top 1 MatterDetailValueID 
	from MatterDetailValues 
	where DetailFieldID = @DetailFieldID 
	AND	
	MatterID = @MatterId

else

Select top 1 LeadDetailValueID 
	from LeadDetailValues 
	where DetailFieldID = @DetailFieldID 
	AND	
	LeadID = @LeadId



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues__ValueExists] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValues__ValueExists] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues__ValueExists] TO [sp_executeall]
GO
