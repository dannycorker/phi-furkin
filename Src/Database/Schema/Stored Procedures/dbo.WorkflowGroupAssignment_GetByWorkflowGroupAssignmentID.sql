SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowGroupAssignment table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupAssignmentID]
(

	@WorkflowGroupAssignmentID int   
)
AS


				SELECT
					[WorkflowGroupAssignmentID],
					[WorkflowGroupID],
					[ClientPersonnelID],
					[Priority],
					[ClientID]
				FROM
					[dbo].[WorkflowGroupAssignment] WITH (NOLOCK) 
				WHERE
										[WorkflowGroupAssignmentID] = @WorkflowGroupAssignmentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupAssignmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupAssignmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupAssignmentID] TO [sp_executeall]
GO
