SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[UpdateOutcomeDelayedEmails]

@OutcomeDelayedEmailID int,
@CustomerID int,
@ClientQuestionnaireID int,
@DateOutcomeEmailSent datetime,
@ClientID int = null

AS

Update OutcomeDelayedEmails
Set CustomerID = @CustomerID,
ClientQuestionnaireID = @ClientQuestionnaireID,
DateOutcomeEmailSent  = @DateOutcomeEmailSent
Where OutcomeDelayedEmailID = @OutcomeDelayedEmailID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcomeDelayedEmails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateOutcomeDelayedEmails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcomeDelayedEmails] TO [sp_executeall]
GO
