SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-02-2014
-- Description:	Gets a rules engine SmsFieldMap record by the RuleParameterID
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_SmsFieldMap_GetByRuleParameterID]
	@RuleParameterID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM RulesEngine_SmsFieldMap WITH (NOLOCK) 
	WHERE RuleParameterID = @RuleParameterID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_SmsFieldMap_GetByRuleParameterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_SmsFieldMap_GetByRuleParameterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_SmsFieldMap_GetByRuleParameterID] TO [sp_executeall]
GO
