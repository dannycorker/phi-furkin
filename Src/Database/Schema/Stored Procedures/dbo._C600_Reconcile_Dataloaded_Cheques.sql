SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Umer Hamid
-- Create date: 2018-07-03
-- Description:	Once Cheque Claims have been issued then all the claim numbers are dataloaded with there cheque numbers. 
--				Straight after the Dataload, Batch 22879 runs which calls this proc. 
--				This proc tries to match processed cheque claims with printed cheques that have been issued by table row ID. 
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Reconcile_Dataloaded_Cheques] 

AS
BEGIN

	SET NOCOUNT ON;

	--Declare Timeout Limit
	DECLARE @CutOff DATETIME = DATEADD(Minute,4,CURRENT_TIMESTAMP)


	/*
	Declare a temp table and store all the records that have been inserted into the cheques imported table (180087). 
	All of these records have been dataloaded at some point by a claims handler
	*/
	DECLARE @RowsToInsert TABLE (TableRowID INT, 
								 CustomerID INT, 
								 LeadID INT, 
								 ClaimID INT,
								 PaymentDataTableRow INT, 
								 PayeeFirstName VARCHAR(100),
								 PayeeLastName VARCHAR(100),
								 Amount MONEY,
								 PaymentRef VARCHAR(50),
								 ChequeNo VARCHAR(50),
								 DataloaderFileID INT,
								 DateProcessed DATE, 
								 ReconcileStatus INT, 
								 PassFail VARCHAR(10))

	/*Insert all of these rows within a temporary table. 
	Inserted 2 extra column called called ReconcileStatus and PassFail and then set it to 0 for now and blank for PassFail.
	The reason it is 0 is because we will loop through each row in the while loop below where the status is 0. 
	Once every row has a value set to 1 in the 'Reconcile Status' column, there will be nothing 
	left to process  
	*/ 
	
	INSERT INTO @RowsToInsert ( TableRowID, 
								CustomerID, 
								LeadID , 
								ClaimID,
								PaymentDataTableRow, 
								PayeeFirstName,
								PayeeLastName,
								Amount,
								PaymentRef,
								ChequeNo,
								DataloaderFileID,
								DateProcessed, 
								ReconcileStatus, 
								PassFail)
	SELECT tr.TableRowID, 
		   tdv.ValueInt as CustomerID ,
		   tdv1.ValueInt as LeadID,
		   tdv2.ValueInt as ClaimID,
		   tdv3.ValueInt as PaymentDataTableRowID, 
		   tdv4.DetailValue as PayeeFirstName,
		   tdv5.DetailValue as PayeeLastName,
		   tdv6.ValueMoney as Amount,
		   tdv7.DetailValue as PaymentRef,
		   tdv8.DetailValue as ChequeNo,
		   tdv9.ValueInt as DataloaderFileID,
		   tdv10.ValueDate as DateProcessed,
		   0, --add blank status column
		   ' '
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 180079 -- customerID
	INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 180080 --leadID
	INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 180081 --ClaimId
	INNER JOIN dbo.TableDetailValues tdv3 WITH (NOLOCK) on tdv3.TableRowID = tr.TableRowID and tdv3.DetailFieldID = 180088 --PaymentDataTRID
	INNER JOIN dbo.TableDetailValues tdv4 WITH (NOLOCK) on tdv4.TableRowID = tr.TableRowID and tdv4.DetailFieldID = 180082 --PayeeFirstName
	INNER JOIN dbo.TableDetailValues tdv5 WITH (NOLOCK) on tdv5.TableRowID = tr.TableRowID and tdv5.DetailFieldID = 180083 --PayeeLastName
	INNER JOIN dbo.TableDetailValues tdv6 WITH (NOLOCK) on tdv6.TableRowID = tr.TableRowID and tdv6.DetailFieldID = 180084 --Amount	
	INNER JOIN dbo.TableDetailValues tdv7 WITH (NOLOCK) on tdv7.TableRowID = tr.TableRowID and tdv7.DetailFieldID = 180085 --PaymentRef	
	INNER JOIN dbo.TableDetailValues tdv8 WITH (NOLOCK) on tdv8.TableRowID = tr.TableRowID and tdv8.DetailFieldID = 180086 --ChequeNumber	
	INNER JOIN dbo.TableDetailValues tdv9 WITH (NOLOCK) on tdv9.TableRowID = tr.TableRowID and tdv9.DetailFieldID = 180089 --DataloaderFileID
	INNER JOIN dbo.TableDetailValues tdv10 WITH (NOLOCK) on tdv10.TableRowID = tr.TableRowID and tdv10.DetailFieldID = 180090 --DateProcessed
	WHERE tr.DetailFieldID = 180087 -- Cheques Imported

	/*Loop through all the rows of your temp table and then if there is a match then match it on the 'Payment Data Table Row ID' column 
	and move over to the cheques reconciled table.
	else if there is not a match then move it over tpo the Cheques Failed Table. 
	(Change the label detailfield id of the table row in order to transfer tables)
	Stamp the extra column you have created with either a pass or a fail message
	*/

	
	WHILE EXISTS (SELECT * FROM @RowsToInsert a Where a.ReconcileStatus = 0) and CURRENT_TIMESTAMP < @CutOff /*Cutoff incase of timeouts*/ 
	BEGIN 

		DECLARE @TableRowID INT, 
				@PaymentDataTableRowID INT,
				@ChequeNumber VARCHAR(50)
		
		--As the loop begins, select the top 1 in the temp table that has not been processed (Where reconcileStatus = 0)	
		SELECT TOP 1 @TableRowID = a.TableRowID FROM @RowsToInsert a 
		where a.ReconcileStatus = 0

		--Place the paymentDataTableRowID in its own variable 
		SELECT TOP 1 @PaymentDataTableRowID = a.PaymentDataTableRow FROM @RowsToInsert a 
		where a.ReconcileStatus = 0 
		and a.TableRowID = @TableRowID

		SELECT TOP 1 @ChequeNumber = a.ChequeNo FROM @RowsToInsert a 
		where a.ReconcileStatus = 0 
		and a.TableRowID = @TableRowID
		
		--Try and match the 'payment data table row id' within 'Cheques Imported' 180087 to the payment data table row ID on claim matter level
		IF EXISTS( SELECT tr.TableRowID FROM TableRows tr WITH (NOLOCK) 
			   WHERE tr.DetailFieldID = 154485
			   AND tr.TableRowID = @PaymentDataTableRowID )
		BEGIN

			--If the table row matches then move it to the 180091 ('Cheques Reconciled' table). To do this we change the detail field of the table row.
			UPDATE TableRows 
			SET DetailFieldID = 180091
			WHERE TableRowID = @TableRowID 

			--Insert the cheque number in the payment data table at claim matter level on the payments page
			EXEC dbo._C00_SimpleValueIntoField 162638, @ChequeNumber, @PaymentDataTableRowID

			--Update the temp table to display if the cheque reconciled
			UPDATE @RowsToInsert  
			SET PassFail = 'PASSED'
			WHERE TableRowID = @TableRowID 
			
		END 
	
		ELSE 
		BEGIN
			--If there is no match, move the tablerow to 180092 ('Cheques Failed' table)
			UPDATE TableRows 
			SET DetailFieldID = 180092
			WHERE TableRowID = @TableRowID 

			--Update the temp table to display if the cheque reconciled
			UPDATE @RowsToInsert  
			SET PassFail = 'FAILED'
			WHERE TableRowID = @TableRowID 

		END

		--Update the reconciled status to 1 for this row so that it has been marked as processed 
		UPDATE @RowsToInsert  
		set ReconcileStatus = 1
		WHERE TableRowID = @TableRowID 
		
	END


	--/*Trigger the batch again if we still have rows to rec*/ 	
	IF EXISTS (SELECT * FROM @RowsToInsert a Where a.ReconcileStatus = 0)  
	BEGIN	
		
		EXEC AutomatedTask__RunNow  22879,0,@CutOff,0  
	END
	

	--Output the result set of the temporary table 
	SELECT *
	FROM @RowsToInsert
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Reconcile_Dataloaded_Cheques] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Reconcile_Dataloaded_Cheques] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Reconcile_Dataloaded_Cheques] TO [sp_executeall]
GO
