SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-30
-- Description:	Inserts or updates a parameter option
-- Mods:
--	2016-06-29 DCM default Val1 for operators 16 & 17
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterOptions_InsertUpdateOption]
(
	@ParameterOptionID INT,
	@OperatorID INT,
	@Val1 VARCHAR(50),
	@Val2 VARCHAR(50),
	@RuleParameterID INT,
	@ClientID INT,
	@UserID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	-- default Val1 for operators 16 & 17 (Is blank & Is Not Blank)
	IF @OperatorID IN (16,17) SELECT @Val1=''
	
	IF @ParameterOptionID > 0
	BEGIN
	
		UPDATE dbo.RulesEngine_ParameterOptions
		SET OperatorID = @OperatorID,
			Val1 = @Val1, 
			Val2 = @Val2,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID
		WHERE ParameterOptionID = @ParameterOptionID
	
	END
	ELSE
	BEGIN
		
		DECLARE @MaxOrder INT
		SELECT @MaxOrder = MAX(o.OptionOrder)
		FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
		WHERE o.RuleParameterID = @RuleParameterID
		
		INSERT INTO dbo.RulesEngine_ParameterOptions (RuleParameterID, OperatorID, Val1, Val2, OptionOrder, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@RuleParameterID, @OperatorID, @Val1, @Val2, ISNULL(@MaxOrder, 0) + 1, @ClientID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID)
		
		SELECT @ParameterOptionID = SCOPE_IDENTITY()
	
	END

	EXEC dbo.RulesEngine_ParameterOptions_GetOptionsByParameterID @ClientID, @RuleParameterID, @ParameterOptionID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_InsertUpdateOption] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterOptions_InsertUpdateOption] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_InsertUpdateOption] TO [sp_executeall]
GO
