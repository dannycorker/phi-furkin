SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupRightsDynamic table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamic_GetByRightID]
(

	@RightID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[GroupRightsDynamicID],
					[ClientPersonnelAdminGroupID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[GroupRightsDynamic] WITH (NOLOCK) 
				WHERE
					[RightID] = @RightID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_GetByRightID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamic_GetByRightID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_GetByRightID] TO [sp_executeall]
GO
