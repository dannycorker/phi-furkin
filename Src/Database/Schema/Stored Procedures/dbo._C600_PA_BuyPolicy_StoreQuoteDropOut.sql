SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2018-05-30
-- Description:	Store details of a drop out from EQB to send drop out email 
-- Mods:
-- CPS 2018-07-23 added NOLOCK to SavedQuote 
-- NG  2019-07-03 rewrite to create single row per session & add fields needed for email CR057
-- NG  2019-07-03 old version stored in I:\LIVE\Project Maddison\Change requests post go live 2019
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================

CREATE PROCEDURE [dbo].[_C600_PA_BuyPolicy_StoreQuoteDropOut]
(
	@Xml XML
)
AS
	BEGIN

	DECLARE 
	@ClientID			INT = dbo.fnGetPrimaryClientID(), 
	@RunAsUserID		INT,
	@CustomerID			INT = 8912 /*The Customer for LeadType ID 1507 - External Q&B Dropout*/, 
	@LeadID				INT,
	@MatterID			INT,
	@CustomerFirstName	VARCHAR (100), 
	@SessionID			INT,
	@Email				VARCHAR (100),
	@Brand				INT,
	@SavedQuoteXML		xml,
	@PetName			VARCHAR (100),
	@PetNameFromXML		VARCHAR(2000),
	@NumberOfPets		INT,
	@StringOfPetNames	VARCHAR(2000) = '', --initialising this variable as an empty string as we cant add a string to a null value
	@LoopCount			INT
					
	SELECT @RunAsUserID	= dbo.fn_C00_GetAutomationUser(@ClientID)

	SELECT	@CustomerFirstName = @Xml.value('(BuyPolicyRequest/CustomerInfo/FirstName)[1]', 'VARCHAR(100)'),
			@SessionID = @Xml.value('(BuyPolicyRequest/SessionId)[1]', 'INT'),
			@Email = @Xml.value('(BuyPolicyRequest/CustomerInfo/Email)[1]', 'VARCHAR(100)'),
			@Brand = @Xml.value('(BuyPolicyRequest/BrandingId)[1]', 'INT')
				
	EXEC @MatterID = _C00_CreateNewLeadForCust  @CustomerID,1507,@ClientID,@RunAsUserID,1,1,1

	SELECT @LeadID = m.LeadID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID = @MatterID 
				
	EXEC _C00_SimpleValueIntoField 180028, @CustomerFirstName, @LeadID, @RunAsUserID /*CustomerName*/
	EXEC _C00_SimpleValueIntoField 180033, @SessionID, @LeadID, @RunAsUserID /*SessionID*/
	EXEC _C00_SimpleValueIntoField 180032, @Email, @LeadID, @RunAsUserID /*Customer Email*/
	EXEC _C00_SimpleValueIntoField 180265, @Brand, @LeadID, @RunAsUserID /*Brand*/

	/*create pet list NG 2019-07-01 for CR059*/

	/*Get the SavedQuoteXML for the selected sessionID*/
	SELECT @SavedQuoteXML = sqm.SavedQuoteXML
	FROM _C600_SavedQuote sqm WITH (NOLOCK)
	WHERE sqm.QuoteSessionID = @SessionID -- 1158294 used for testing
	 
	/*Create temp table to store data from XML*/
	DECLARE @PetInfo TABLE
	(
		PetName VARCHAR (2000)
		,IsProcessed BIT
	)

	INSERT @PetInfo (PetName, IsProcessed)
	SELECT Petinf.value('(PetName)[1]', 'VARCHAR(2000)'), 0
	FROM @SavedQuoteXML.nodes ('/BuyPolicyRequest/PetQuotes/PetQuote/PetInfo') AS n(Petinf)
	
	/*Count the table rows to get total number of pets*/
	SELECT @NumberOfPets = COUNT(*), @LoopCount = 0
	FROM @PetInfo 

	/*Select the first unprocessed row in the @petinfo table*/
	SELECT TOP 1 @PetNameFromXML = p.PetName
	FROM @PetInfo p where p.IsProcessed = 0
	ORDER by p.PetName asc

	/*if there is only one pet then set the name string to the pet name*/
	IF @NumberOfPets = 1

	BEGIN
		SELECT @StringOfPetNames = @PetNameFromXML
	END
	ELSE
	/*go into a loop if there is more than one pet - Loop count set at beginning of loop
	While the loop count is less than the number of pets*/
	BEGIN
		WHILE @LoopCount < @NumberOfPets
		BEGIN 
			/*If this is the last loop, add ' and ' plus the pet name to the list*/
			/*If this is not the penultimate loop, add the pet name plus a comma to the list*/
			/*If this is the penultimate loop, add the pet name with no comma*/
			SELECT @LoopCount +=1 -- to give number of the loop we're entering

			IF @LoopCount = @NumberOfPets
				BEGIN
					Select @StringOfPetNames += ' and ' + @PetNameFromXML 
				END
			ELSE
				BEGIN
			 		Select @StringOfPetNames +=  @PetNameFromXML
					IF @LoopCount < @NumberOfPets -1
						BEGIN
						Select @StringOfPetNames += ', '
						END
				END
			
		UPDATE @PetInfo
		SET IsProcessed = 1
		WHERE PetName = @PetNameFromXML

		SELECT TOP 1 @PetNameFromXML  = p.PetName
		FROM @PetInfo p where p.IsProcessed = 0
		ORDER by p.PetName asc

		END

	END
	
	/*Now that the while loop has finished and we have our string of pet names, stamp this into the pet name field on the External Q&B Dropout LeadDetails page*/
	EXEC dbo._C00_SimpleValueIntoField 180035, @StringOfPetNames, @LeadID

	
/*think this needs changing - this restarts the batch*/
	IF EXISTS 
	(
	SELECT sq.SavedQuoteXML
	FROM dbo._C600_SavedQuote sq WITH (NOLOCK)
	INNER JOIN dbo._C600_QuoteSessions qs WITH (NOLOCK) on sq.QuoteSessionID = qs.QuoteSessionID
	WHERE sq.WhenCreated  < DATEADD(minute, -20, dbo.fn_GetDate_Local())	-- 20 mins ago
	AND NOT EXISTS (SELECT * FROM LeadDetailValues ldv WITH ( NOLOCK ) where ldv.DetailFieldID = 180033 and ldv.DetailValue = sq.QuoteSessionID) -- no lead for this session in Ext Q&B Dropout LT
	AND NOT EXISTS (SELECT * FROM LeadDetailValues ldvsq WITH ( NOLOCK ) where ldvsq.DetailFieldID = 178415 and ldvsq.DetailValue = sq.QuoteSessionID) -- no lead for this session in Saved Quote LT
	AND qs.BuyDate IS NULL
	AND sq.WhenExpire > dbo.fn_GetDate_Local()
	AND sq.AggregatorID IS NULL
	)
	BEGIN 
	
		EXEC dbo.AutomatedTask__RunNow 22867,0,NULL,0 /*Import Not Saved Quotes for Drop Out email*/
	
	END 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_BuyPolicy_StoreQuoteDropOut] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_BuyPolicy_StoreQuoteDropOut] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_BuyPolicy_StoreQuoteDropOut] TO [sp_executeall]
GO
