SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardon
-- Create date: 14-03-2012
-- Description:	Gets a list of notification recipients by client personnelid and status
-- Added notification id 
-- =============================================
CREATE PROCEDURE [dbo].[NotificationRecipient__GetByClientPersonnelIDAndStatus]

	@ClientPersonnelID int,
	@NotificationStatusID int,
	@NotificationID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	[NotificationRecipientID],
			[NotificationID],
			[ClientID],
			[SubClientID],
			[ClientPersonnelID],
			[NotificationStatusID]
	FROM [NotificationRecipient] WITH (NOLOCK) 	
	WHERE ClientPersonnelID = @ClientPersonnelID
	AND NotificationStatusID = @NotificationStatusID
	AND NotificationID = @NotificationID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRecipient__GetByClientPersonnelIDAndStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationRecipient__GetByClientPersonnelIDAndStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRecipient__GetByClientPersonnelIDAndStatus] TO [sp_executeall]
GO
