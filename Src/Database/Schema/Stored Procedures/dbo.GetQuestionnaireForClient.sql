SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetQuestionnaireForClient    Script Date: 08/09/2006 12:22:51 ******/

CREATE PROCEDURE [dbo].[GetQuestionnaireForClient] @ClientQuestionnaireID int AS
SELECT MasterQuestions.MasterQuestionID, MasterQuestions.ClientQuestionnaireID,MasterQuestions.QuestionTypeID, 
MasterQuestions.QuestionText, MasterQuestions.QuestionToolTip, QuestionTypes.Name, .QuestionTypes.Description, MasterQuestions.Mandatory, MasterQuestions.ClientID 
FROM MasterQuestions INNER JOIN QuestionTypes ON MasterQuestions.QuestionTypeID = QuestionTypes.QuestionTypeID
WHERE MasterQuestions.ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireForClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionnaireForClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireForClient] TO [sp_executeall]
GO
