SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SMSQuestion table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_Find]
(

	@SearchUsingOR bit   = null ,

	@SMSQuestionID int   = null ,

	@ClientID int   = null ,

	@SMSQuestionnaireID int   = null ,

	@Question varchar (960)  = null ,

	@QuestionOrder int   = null ,

	@DocumentTypeID int   = null ,

	@Enabled bit   = null ,

	@Timeout int   = null ,

	@Repeat int   = null ,

	@RepeatPrefix varchar (2000)  = null ,

	@DetailFieldToSave int   = null ,

	@EvaluateResponse bit   = null ,

	@VoiceAction int   = null ,

	@RedirectTo varchar (50)  = null ,

	@GatherNumDigits int   = null ,

	@RecordingMaxLength int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SMSQuestionID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [Question]
	, [QuestionOrder]
	, [DocumentTypeID]
	, [Enabled]
	, [Timeout]
	, [Repeat]
	, [RepeatPrefix]
	, [DetailFieldToSave]
	, [EvaluateResponse]
	, [VoiceAction]
	, [RedirectTo]
	, [GatherNumDigits]
	, [RecordingMaxLength]
    FROM
	[dbo].[SMSQuestion] WITH (NOLOCK) 
    WHERE 
	 ([SMSQuestionID] = @SMSQuestionID OR @SMSQuestionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SMSQuestionnaireID] = @SMSQuestionnaireID OR @SMSQuestionnaireID IS NULL)
	AND ([Question] = @Question OR @Question IS NULL)
	AND ([QuestionOrder] = @QuestionOrder OR @QuestionOrder IS NULL)
	AND ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Timeout] = @Timeout OR @Timeout IS NULL)
	AND ([Repeat] = @Repeat OR @Repeat IS NULL)
	AND ([RepeatPrefix] = @RepeatPrefix OR @RepeatPrefix IS NULL)
	AND ([DetailFieldToSave] = @DetailFieldToSave OR @DetailFieldToSave IS NULL)
	AND ([EvaluateResponse] = @EvaluateResponse OR @EvaluateResponse IS NULL)
	AND ([VoiceAction] = @VoiceAction OR @VoiceAction IS NULL)
	AND ([RedirectTo] = @RedirectTo OR @RedirectTo IS NULL)
	AND ([GatherNumDigits] = @GatherNumDigits OR @GatherNumDigits IS NULL)
	AND ([RecordingMaxLength] = @RecordingMaxLength OR @RecordingMaxLength IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SMSQuestionID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [Question]
	, [QuestionOrder]
	, [DocumentTypeID]
	, [Enabled]
	, [Timeout]
	, [Repeat]
	, [RepeatPrefix]
	, [DetailFieldToSave]
	, [EvaluateResponse]
	, [VoiceAction]
	, [RedirectTo]
	, [GatherNumDigits]
	, [RecordingMaxLength]
    FROM
	[dbo].[SMSQuestion] WITH (NOLOCK) 
    WHERE 
	 ([SMSQuestionID] = @SMSQuestionID AND @SMSQuestionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SMSQuestionnaireID] = @SMSQuestionnaireID AND @SMSQuestionnaireID is not null)
	OR ([Question] = @Question AND @Question is not null)
	OR ([QuestionOrder] = @QuestionOrder AND @QuestionOrder is not null)
	OR ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Timeout] = @Timeout AND @Timeout is not null)
	OR ([Repeat] = @Repeat AND @Repeat is not null)
	OR ([RepeatPrefix] = @RepeatPrefix AND @RepeatPrefix is not null)
	OR ([DetailFieldToSave] = @DetailFieldToSave AND @DetailFieldToSave is not null)
	OR ([EvaluateResponse] = @EvaluateResponse AND @EvaluateResponse is not null)
	OR ([VoiceAction] = @VoiceAction AND @VoiceAction is not null)
	OR ([RedirectTo] = @RedirectTo AND @RedirectTo is not null)
	OR ([GatherNumDigits] = @GatherNumDigits AND @GatherNumDigits is not null)
	OR ([RecordingMaxLength] = @RecordingMaxLength AND @RecordingMaxLength is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Find] TO [sp_executeall]
GO
