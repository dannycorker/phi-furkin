SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserGroupPanelItemChartingAccess table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPanelItemChartingID]
(

	@PanelItemChartingID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalUserGroupPanelItemChartingAccessID],
					[ClientID],
					[PortalUserGroupID],
					[PanelItemChartingID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupPanelItemChartingAccess] WITH (NOLOCK) 
				WHERE
					[PanelItemChartingID] = @PanelItemChartingID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPanelItemChartingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPanelItemChartingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPanelItemChartingID] TO [sp_executeall]
GO
