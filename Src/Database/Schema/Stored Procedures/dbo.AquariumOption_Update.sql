SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AquariumOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_Update]
(

	@AquariumOptionID int   ,

	@OriginalAquariumOptionID int   ,

	@AquariumOptionTypeID int   ,

	@AquariumOptionName varchar (50)  ,

	@AquariumOptionDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AquariumOption]
				SET
					[AquariumOptionID] = @AquariumOptionID
					,[AquariumOptionTypeID] = @AquariumOptionTypeID
					,[AquariumOptionName] = @AquariumOptionName
					,[AquariumOptionDescription] = @AquariumOptionDescription
				WHERE
[AquariumOptionID] = @OriginalAquariumOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Update] TO [sp_executeall]
GO
