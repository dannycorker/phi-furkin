SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-12-04
-- Description:	Gets data for new Premium Funding application renewal quote.
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalQuote]
	@MatterID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @BaseTransactionAmount DECIMAL(18,2)

	SELECT @BaseTransactionAmount = ppp.PaymentGross
	FROM PurchasedProduct p WITH ( NOLOCK ) 
	INNER JOIN PurchasedProductPaymentSchedule ppp WITH ( NOLOCK ) on ppp.PurchasedProductID = p.PurchasedProductID 
	INNER JOIN PurchasedProductPaymentScheduleType ppt WITH ( NOLOCK ) on ppt.PurchasedProductPaymentScheduleTypeID = ppp.PurchasedProductPaymentScheduleTypeID
	WHERE p.ObjectID = @MatterID
	AND ppp.PaymentStatusID in (1) 
	AND ppp.PurchasedProductPaymentScheduleTypeID in (1) 
	AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH ( NOLOCK ) WHERE p.PurchasedProductPaymentScheduleParentID = ppp.PurchasedProductPaymentScheduleID
	AND  ppp.PurchasedProductPaymentScheduleID <>  p.PurchasedProductPaymentScheduleID)

	SELECT 
	ISNULL(@BaseTransactionAmount,0.00) AS BaseTransactionAmount,
	customer.CustomerID AS CustomerId,
	customer.EmailAddress AS Email,
	customer.TitleID AS Title,
	customer.FirstName AS Firstname,
	customer.LastName AS Lastname,
	customer.PostCode AS Postcode,
	0 AS PrmeiumFunderRequestID,
	0 AS PremiumFunderResponseID
	FROM Matter matter WITH (NOLOCK)
	INNER JOIN Customers customer WITH (NOLOCK) ON customer.CustomerID = matter.CustomerID
	WHERE matter.MatterID = @MatterID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalQuote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalQuote] TO [sp_executeall]
GO
