SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Department table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department_Insert]
(

	@DepartmentID int    OUTPUT,

	@ClientID int   ,

	@CustomerID int   ,

	@DepartmentName varchar (100)  ,

	@DepartmentDescription varchar (100)  
)
AS


				
				INSERT INTO [dbo].[Department]
					(
					[ClientID]
					,[CustomerID]
					,[DepartmentName]
					,[DepartmentDescription]
					)
				VALUES
					(
					@ClientID
					,@CustomerID
					,@DepartmentName
					,@DepartmentDescription
					)
				-- Get the identity value
				SET @DepartmentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Insert] TO [sp_executeall]
GO
