SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Partner table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Partner_GetByCountryID]
(

	@CountryID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PartnerID],
					[CustomerID],
					[ClientID],
					[UseCustomerAddress],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[HomeTelephone],
					[MobileTelephone],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[Occupation],
					[Employer],
					[DateOfBirth],
					[FullName],
					[CountryID],
					[WhenModified],
					[WhoModified],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Partner] WITH (NOLOCK) 
				WHERE
					[CountryID] = @CountryID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_GetByCountryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner_GetByCountryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_GetByCountryID] TO [sp_executeall]
GO
