SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventSubtypeDefault table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_Delete]
(

	@EventSubtypeDefaultID int   
)
AS


				DELETE FROM [dbo].[EventSubtypeDefault] WITH (ROWLOCK) 
				WHERE
					[EventSubtypeDefaultID] = @EventSubtypeDefaultID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Delete] TO [sp_executeall]
GO
