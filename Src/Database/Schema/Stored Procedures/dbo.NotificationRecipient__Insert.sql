SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Inserts a notification recipient record and returns the NotificationRecipientID
-- =============================================
CREATE PROCEDURE [dbo].[NotificationRecipient__Insert]

	@NotificationRecipientID INT OUTPUT,
	@NotificationID INT,
	@ClientID INT,
	@SubClientID INT,
	@ClientPersonnelID INT,
	@NotificationStatusID INT

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [NotificationRecipient]
           ([NotificationID]
           ,[ClientID]
           ,[SubClientID]
           ,[ClientPersonnelID]
           ,[NotificationStatusID])
     VALUES
           (@NotificationID, 
            @ClientID, 
            @SubClientID,
            @ClientPersonnelID,
            @NotificationStatusID)
                        	
	SELECT SCOPE_IDENTITY() NotificationRecipientID          

END




GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRecipient__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationRecipient__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRecipient__Insert] TO [sp_executeall]
GO
