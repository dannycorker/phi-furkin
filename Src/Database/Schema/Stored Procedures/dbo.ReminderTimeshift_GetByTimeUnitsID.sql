SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReminderTimeshift table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReminderTimeshift_GetByTimeUnitsID]
(

	@TimeUnitsID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ReminderTimeshiftID],
					[ReminderTimeshiftName],
					[TimeUnitsID],
					[TimeUnitsQuantity],
					[DisplayOrder],
					[TimeUnitsQuantityInMinutes]
				FROM
					[dbo].[ReminderTimeshift] WITH (NOLOCK) 
				WHERE
					[TimeUnitsID] = @TimeUnitsID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_GetByTimeUnitsID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReminderTimeshift_GetByTimeUnitsID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_GetByTimeUnitsID] TO [sp_executeall]
GO
