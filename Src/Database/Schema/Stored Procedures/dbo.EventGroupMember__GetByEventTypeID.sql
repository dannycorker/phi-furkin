SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-11-05
-- Description:	Gets the event group members by event type
-- =============================================
CREATE PROCEDURE [dbo].[EventGroupMember__GetByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	SELECT eg.EventGroupName, egm.* FROM EventGroupMember egm WITH (NOLOCK) 
	INNER JOIN dbo.EventGroup eg WITH (NOLOCK) ON eg.EventGroupID = egm.EventGroupID
	WHERE egm.EventTypeID=@EventTypeID AND egm.ClientID=@ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember__GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroupMember__GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember__GetByEventTypeID] TO [sp_executeall]
GO
