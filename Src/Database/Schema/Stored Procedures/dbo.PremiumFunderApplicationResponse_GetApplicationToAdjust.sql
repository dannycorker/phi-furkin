SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-11-29
-- Description:	Gets application response by Policy Admin MatterID for MTA Adjustment and checks that it's not cancelled
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplicationResponse_GetApplicationToAdjust]
	@MatterID INT
AS
BEGIN
		
	SET NOCOUNT ON;

												/*Testing*/
	DECLARE @AdjustmentAmount DECIMAL(18,2)		-- = 10.00
	DECLARE	@AdjustmentType VARCHAR(20)			-- = 'Reduction'
	DECLARE	@EffectiveDate DATE					= dbo.fn_GetDate_Local()

	SELECT @AdjustmentAmount = ppp.PaymentGross
	FROM PurchasedProduct p WITH ( NOLOCK ) 
	INNER JOIN PurchasedProductPaymentSchedule ppp WITH ( NOLOCK ) on ppp.PurchasedProductID = p.PurchasedProductID 
	INNER JOIN PurchasedProductPaymentScheduleType ppt WITH ( NOLOCK ) on ppt.PurchasedProductPaymentScheduleTypeID = ppp.PurchasedProductPaymentScheduleTypeID
	WHERE p.ObjectID = @MatterID
	AND ppp.PaymentStatusID in (1,4) 
	AND ppp.PurchasedProductPaymentScheduleTypeID in (6) 
	AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH ( NOLOCK ) WHERE p.PurchasedProductPaymentScheduleParentID = ppp.PurchasedProductPaymentScheduleID
	AND  ppp.PurchasedProductPaymentScheduleID <>  p.PurchasedProductPaymentScheduleID)


	/*GPR 2019-12-11 - Added to resolve LPC 4.1.8 PCL post-release testing*/
	IF @AdjustmentAmount IS NULL
	BEGIN
		SELECT @AdjustmentAmount = ppp.PaymentGross
		FROM PurchasedProduct p WITH ( NOLOCK ) 
		INNER JOIN PurchasedProductPaymentSchedule ppp WITH ( NOLOCK ) on ppp.PurchasedProductID = p.PurchasedProductID 
		INNER JOIN PurchasedProductPaymentScheduleType ppt WITH ( NOLOCK ) on ppt.PurchasedProductPaymentScheduleTypeID = ppp.PurchasedProductPaymentScheduleTypeID
		WHERE p.ObjectID = @MatterID
		AND ppp.PaymentStatusID in (6)  /*Paid cancellation for this Policy - the Event choice for cancellation adjustment will take place directly after the Policy is set to cancelled*/
		AND ppp.PurchasedProductPaymentScheduleTypeID in (7) /*Cancellation Adjustment*/
		AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH ( NOLOCK ) WHERE p.PurchasedProductPaymentScheduleParentID = ppp.PurchasedProductPaymentScheduleID
		AND  ppp.PurchasedProductPaymentScheduleID <>  p.PurchasedProductPaymentScheduleID)
	END

	IF @AdjustmentAmount > 0
	BEGIN
		SELECT @AdjustmentType = 'Addition'
	END
	ELSE
	BEGIN
		SELECT @AdjustmentType = 'Reduction'
	END

	SELECT petpolicy.*, loanApplication.*, ISNULL(ABS(@AdjustmentAmount),0.00) AdjustmentAmount, @AdjustmentType AdjustmentType, @EffectiveDate EffectiveDate 
	FROM PremiumFunderApplicationPolicy petpolicy WITH (NOLOCK)
	INNER JOIN PremiumFunderApplicationResponse loanApplication WITH (NOLOCK) ON loanApplication.PremiumFunderRequestID = petpolicy.PremiumFunderRequestID
	WHERE petpolicy.PAMatterID = @MatterID
	AND loanApplication.PremiumFunderRequestID NOT IN /*exclude if this policies application is already cancelled*/
		(
			SELECT cancelAgreement.PremiumFunderRequestID 
			FROM PremiumFunderCancelAgreementResponse cancelAgreement WITH (NOLOCK) 
			WHERE cancelAgreement.MatterID = petpolicy.PAMatterID
		)
	ORDER BY 1 DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetApplicationToAdjust] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplicationResponse_GetApplicationToAdjust] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetApplicationToAdjust] TO [sp_executeall]
GO
