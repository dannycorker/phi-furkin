SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Paul Richardson
-- Create date: 15/06/2016
-- Description:   Creates a payment schedule:
--	Responsible for backing up the PurchasedProductPaymentSchedule and the CustomerPaymentSchedule
--  and Creating the PurchasedProductPaymentSchedule and the CustomerPaymentSchedule
-- Mods
-- 2016-08-15 DCM Use default number of increments if not set
-- 2016-08-30 DCM Set PaymentStatusID to 1 (new) on all new rows
-- 2016-09-12 DCM Make @WhenCreated full datetime
-- 2016-09-16 DCM Added setting PurchasedProductPaymentScheduleParentID
-- 2016-09-16 DCM Added PurchasedProductPaymentScheduleTypeID
-- 2016-09-22 DCM Added setting ActualCollectionDate
-- 2016-10-04 ACE Fixed cover from and to dates
-- 2016-11-30 DCM Ensure scheduled payment date falls within period being covered
-- 2016-12-19 DCM Updated vat remiainder calc to use @RemainderPaymentVAT
-- 2016-12-20 ACE Updated the other vat remiainder calc to use @RemainderPaymentVAT
-- 2017-11-13 CPS Raise an error if the ValidFrom date on the Purchased Product is NULL
-- 2017-01-31 ACE Added Account ID Filter to update
-- 2017-03-21 ACE Updated wait type
-- 2017-03-22 ACE Fixed CPS generation
-- 2017-08-23 CPS updated call to fn_C00_CalculateActualCollectionDate
-- 2018-03-20 JEL Added Object Type ID and consideration for ClientAccount Table 
-- 2018-04-03 ACE Added in the setting of expiry date for bank account type records
-- 2018-06-08 JEL Changed releated ObjectID to Collections matter 
-- 2019-01-08 GPR Updated procedure to update logic for first Actual Collection Date
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProduct__CreatePaymentSchedule]
      @PurchasedProductID INT,
      @Debug INT = NULL 
AS
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Success BIT
      
      BEGIN TRY  
      
            DECLARE @ScheduleID INT = 1,
                        @ClientID INT,
                        @AccountID INT,
                        @TestID INT = 0,  
                        @FirstPaymentDate DATE,
                        @ValidFrom DATE, 
                        @ValidTo DATE,
                        @PreferredPaymentDay INT, 
                        @ProductCostGross NUMERIC(18,2),
                        @ProductCostVAT NUMERIC(18,2),
                        @PaymentFrequencyID INT,
                        @NumberOfInstallments INT = 12,
                        @RemainderUpFront BIT,
                        @CustomerID INT,
                        @WhoCreated INT,
                        @Payment NUMERIC(36,18),
                        @RoundedPayment NUMERIC(18,2),
                        @RoundedCost NUMERIC(18,2),
                        @RemainderPayment NUMERIC(18,2),
                        @PaymentVAT NUMERIC(36,18),
                        @RoundedPaymentVAT NUMERIC(18,2),
                        @RoundedCostVAT NUMERIC(18,2),
                        @RemainderPaymentVAT NUMERIC(18,2),
                        @PurchasedProductPaymentScheduleID INT,
                        @CoverFromDay TINYINT,
                        @ErrorMessage	VARCHAR(MAX),
                        @LogEntry VARCHAR(2000),
                        @ClientAccountID INT,
						@MatterID INT, 
						@ObjectID INT,
						@ObjectTypeID INT ,
						@LastPaymentDate DATE, 
						@PaymentMethod INT,
						@ValueDate DATE,
						@CountryID INT,
						@StateCode VARCHAR(10),
						@TransactionFee NUMERIC(18,2),
						@YearlyTransactionFee NUMERIC(18,2),
						@PaymentTypeLuli INT, /*Card/DD*/
						@PaymentPeriodLuli INT, /*Annually/Monthly*/
						@SkipFirstMonth BIT
            
            SELECT      @ClientID = product.ClientID,
                        @AccountID = product.AccountID, 
                        @FirstPaymentDate = product.FirstPaymentDate,
                        @ValidFrom = product.ValidFrom,
                        @ValidTo = product.ValidTo,
                        
                        @PreferredPaymentDay = product.PreferredPaymentDay,
                        @ProductCostGross = product.ProductCostGross,
                        @ProductCostVAT = product.ProductCostVAT,
                        @PaymentFrequencyID = product.PaymentFrequencyID,
                        @NumberOfInstallments = product.NumberOfInstallments,
                        @RemainderUpFront = product.RemainderUpFront,
                        @CustomerID = product.CustomerID,
                        @WhoCreated = product.WhoCreated,
                        @ClientAccountID = product.ClientAccountID,
						@ObjectID = a.ObjectID,
						@ObjectTypeID = a.ObjectTypeID ,
						@PaymentMethod=a.AccountTypeID ,
						@MatterID = product.ObjectID

            FROM PurchasedProduct product WITH (NOLOCK)
            INNER JOIN Account a WITH (NOLOCK) on a.AccountID = product.AccountID
            WHERE product.PurchasedProductID = @PurchasedProductID

			SELECT @StateCode = c.County
			FROM Customers c WITH (NOLOCK)
			WHERE c.CustomerID = @CustomerID

            /*CPS 2017-11-13 prevent creation of garbage schedules*/
            IF @ValidFrom is NULL
            BEGIN
				SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  Cannot create payment schedule because ValidFrom date is missing on PurchasedProductID ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductID),'NULL') + '</font>'
				RAISERROR( @ErrorMessage, 16, 1 )
				RETURN
            END
            
            DECLARE @WhenCreated DATETIME = dbo.fn_GetDate_Local() -- current date time for when created
            
            -- ensure number of increments is set
            IF @NumberOfInstallments IS NULL
            BEGIN
            
                  SELECT @NumberOfInstallments=pf.DefaultInstallmentLength 
                  FROM PaymentFrequency pf WITH (NOLOCK) 
                  WHERE pf.PaymentFrequencyID=@PaymentFrequencyID
                  
            END

            -- performs backup of payment schedules if required
            EXEC PurchasedProductPaymentScheduleHistory__Backup @PurchasedProductID, @WhoCreated, 1
            EXEC CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 1
            
            DECLARE @Schedule TABLE
            (
                  ID INT,
                  DATE DATE,
                  ToDate DATE
            )
          
            DECLARE @PaymentSchedule TABLE
            (
                  ID INT,
                  DATE DATE
            )
          
          SELECT @CoverFromDay = DATEPART(DD, @ValidFrom)	   

		  INSERT @PaymentSchedule (ID, DATE) -- Creates the dates for the payment schedule (ensure payment date falls within cover period)
			SELECT * FROM dbo.fnCreateSchedule(4, @NumberOfInstallments, @ValidFrom, 
								CASE WHEN CAST(DATEPART(DAY,@ValidFrom) AS TINYINT) > CAST(@PreferredPaymentDay AS TINYINT)
								THEN
									DATEADD(MONTH,1,@ValidFrom)
								ELSE 
									DATEADD(DD,-1,@ValidFrom)
								END, 
								CAST(@PreferredPaymentDay AS TINYINT))
          
			INSERT @Schedule (ID, DATE) -- Creates the dates for the cover schedule
			SELECT * FROM dbo.fnCreateSchedule(4, @NumberOfInstallments, @ValidFrom, DATEADD(DD,-1,@ValidFrom), CAST(@CoverFromDay AS TINYINT))

          
          IF @Debug = 1 
          BEGIN 
          
				SELECT * 
				FROM @Schedule	
				
				SELECT dbo.fnAddWorkingDays(p.DATE,1)  ,  p.* 
				FROM @PaymentSchedule p
          
          END 
          
          
            IF @NumberOfInstallments = 1
            BEGIN             
                  -- Single payment for a year's cover
                  UPDATE s
                  SET ToDate = @ValidTo
                  FROM @Schedule s
                  
            END
            ELSE
            BEGIN
                  -- payment schedule with multiple installments
                  UPDATE s
                  SET ToDate = DATEADD(DAY, -1, DATEADD(MONTH, 1, s.Date))
                  FROM @Schedule s
            END
            
            SELECT @Payment = (@ProductCostGross / @NumberOfInstallments)
            SELECT @RoundedPayment = ROUND(@Payment, 2)
            SELECT @RoundedCost = @RoundedPayment * @NumberOfInstallments
            SELECT @RemainderPayment = @ProductCostGross - @RoundedCost
            
            SELECT @PaymentVAT = (@ProductCostVAT / @NumberOfInstallments)
            SELECT @RoundedPaymentVAT = ROUND(@PaymentVAT, 2)
            SELECT @RoundedCostVAT = @RoundedPaymentVAT * @NumberOfInstallments
            SELECT @RemainderPaymentVAT = @ProductCostVAT - @RoundedCostVAT
            
			SELECT @PaymentTypeLuli = CASE WHEN @PaymentMethod = 1
					THEN 69930 /*Direct Debit*/
					ELSE 69931 /*Credit Card*/
					END

			SELECT @PaymentMethod = CASE @NumberOfInstallments
										WHEN 1 THEN 69944 /*Annually*/
										WHEN 26 THEN 293321 /*Fortnightly*/
										ELSE 69943 /*Monthly*/
									END

			SELECT @TransactionFee = ISNULL(dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod),0)



			IF @PaymentMethod = 69943
			BEGIN 
				/*Default to skip*/
				SELECT @SkipFirstMonth = 1
			END 

			IF @PaymentMethod = 1 /*DD*/
			BEGIN

				IF EXISTS (
					SELECT *
					FROM PurchasedProduct pp WITH (NOLOCK)
					WHERE pp.ObjectID = @MatterID
					AND pp.PurchasedProductID < @PurchasedProductID
				)
				BEGIN

					/*Yes it's been set before. But its more efficient to do an exists than a not.*/
					SELECT @SkipFirstMonth = 1

				END
				ELSE
				BEGIN

					SELECT @SkipFirstMonth = 0

				END

			END

            -- creates the purchased product payment schedule
            WHILE @ScheduleID > @TestID AND @ScheduleID <= @NumberOfInstallments
            BEGIN

				INSERT INTO PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, CoverFrom, CoverTo, ActualCollectionDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, PurchasedProductPaymentScheduleTypeID, WhoCreated, WhenCreated,ClientAccountID, TransactionFee)
				SELECT @ClientID, 
					@CustomerID, 
					@AccountID, 
					@PurchasedProductID, 
					CONVERT(VARCHAR(10),s.[DATE],120) /*CoverFrom*/, 
					CONVERT(VARCHAR(10),s.ToDate,120) /*CoverTo*/, 
					/*JEL 2020-04-24 Don't adjust CCs for working days*/
					CONVERT(VARCHAR(10),p.[DATE],120)  /*ActualCollectionDate*/, 
					(@RoundedPayment-@RoundedPaymentVAT),
					@RoundedPaymentVAT, 
					CASE WHEN @TestID = 0 AND @SkipFirstMonth = 1 THEN @RoundedPayment ELSE @RoundedPayment + @TransactionFee END,	 
					1, 
					1, 
					@WhoCreated, 
					@WhenCreated, 
					@ClientAccountID,
					CASE WHEN @TestID = 0 AND @SkipFirstMonth = 1 THEN 0.00 ELSE @TransactionFee END AS [TransactionFee]
				FROM @Schedule s 
				INNER JOIN @PaymentSchedule p ON p.ID = s.ID
				WHERE s.ID = @ScheduleID
				AND NOT EXISTS (
					SELECT *
					FROM PurchasedProductPaymentSchedule pps_existing WITH (NOLOCK)
					WHERE pps_existing.AccountID = @AccountID 
					AND pps_existing.PurchasedProductID = @PurchasedProductID
					AND pps_existing.CoverFrom = CONVERT(VARCHAR(10),s.[DATE],120)
				)
                                                            
				SELECT @TestID = @TestID + 1
				SELECT @ScheduleID = @ScheduleID + 1            

            END
            
            IF @RemainderUpFront=1  -- take the remainder up front
            BEGIN
					/*2017-03-10 ACE Dont update payments that have already happened!*/
                  SELECT TOP 1 @PurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID
                  FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
                  WHERE PurchasedProductID = @PurchasedProductID
                  AND PaymentStatusID = 1
                  ORDER BY ActualCollectionDate ASC
                  
                  UPDATE PurchasedProductPaymentSchedule
                  SET PaymentGross =  CASE WHEN @SkipFirstMonth = 1 THEN (@RoundedPayment + @RemainderPayment) ELSE (@RoundedPayment + @RemainderPayment + @TransactionFee) END,
                        PaymentVAT = (@RoundedPaymentVAT + @RemainderPaymentVAT),
                        PaymentNet = ((@RoundedPayment + @RemainderPayment) - (@RoundedPaymentVAT + @RemainderPaymentVAT))
                  WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
            END
            ELSE -- take the remainder at the end
            BEGIN
                  SELECT TOP 1 @PurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID
                  FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
                  WHERE PurchasedProductID = @PurchasedProductID
                  AND PaymentStatusID = 1
                  ORDER BY ActualCollectionDate DESC
                  
                  UPDATE PurchasedProductPaymentSchedule
                  SET   PaymentGross =  CASE WHEN @SkipFirstMonth = 1 THEN (@RoundedPayment + @RemainderPayment) ELSE (@RoundedPayment + @RemainderPayment + @TransactionFee) END,
                        PaymentVAT = (@RoundedPaymentVAT + @RemainderPaymentVAT),
                        PaymentNet = ((@RoundedPayment + @RemainderPayment) - (@RoundedPaymentVAT + @RemainderPaymentVAT))
                  WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
            END
            
            /* 
                  Set the actual collection date 
                  1. For card payments on new schedules, the actual collection day = payment day
                  2. For direct debit payments on new schedules, add mandate wait to the first payment & weekend/bank holiday wait to all 
            
            */ 
            
                  
            DECLARE @ActualDates TABLE ( ActualDate DATE, PPPSID INT, Done INT, RNO INT )
            DECLARE @ActualDate DATE, @RNO INT, @WaitType INT
            
            INSERT INTO @ActualDates 
            SELECT ActualCollectionDate, PurchasedProductPaymentScheduleID, 0, ROW_NUMBER() OVER(PARTITION BY AccountID ORDER BY PaymentDate ASC) as [RNO]
            FROM PurchasedProductPaymentSchedule WITH (NOLOCK)
            WHERE PurchasedProductID = @PurchasedProductID
                       
            IF @Debug = 1 
            BEGIN 
            
				SELECT * 
				FROM @ActualDates
				
            END 

			/*GPR 2020-03-02 for AAG-202*/
			/*ACE 2020-08-17 Moved outside the loop as the ClientID wont change*/
			SELECT @CountryID = cl.CountryID 
			FROM Clients cl WITH (NOLOCK)
			WHERE cl.ClientID = @ClientID
	
            WHILE EXISTS ( SELECT * FROM @ActualDates WHERE Done = 0 )
            BEGIN
				  
				  IF @PaymentMethod = 2
				  BEGIN 
					 
					 /*Whether monthly or annual, cards will have actual collection and payment dates set to the same.*/
					 UPDATE pps
					 SET PaymentDate= ActualCollectionDate 
					 FROM PurchasedProductPaymentSchedule pps 
					 Inner Join PurchasedProduct p on p.PurchasedProductID = pps.PurchasedProductID
					 WHERE p.PurchasedProductID = @PurchasedProductID

					 UPDATE @ActualDates SET Done = 1 
					

				  --    /*JEL 2019-01-28 FIX for ticket 54560 -- Where a multipet policy is purchase where 1 policy starts in the future and 1 starts today PPS built form CPS, PPS cannot have  have a payment date prior to policy start. This 
					 -- Means cps does not group and card response does not reconcile. 
					 -- So set the payment and collection date for cards as @Today in all instances as cards are same day*/ 
					 -- UPDATE pps
					 -- SET PaymentDate=  CAST(dbo.fn_GetDate_Local() as DATE), ActualCollectionDate = CAST(dbo.fn_GetDate_Local() as DATE) 
					 -- FROM PurchasedProductPaymentSchedule pps
					 -- WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

					 --UPDATE @ActualDates SET Done = 1 WHERE PPPSID=@PurchasedProductPaymentScheduleID 
				  END
				  ELSE 
				  BEGIN

					SELECT TOP 1 @ActualDate=ActualDate, @PurchasedProductPaymentScheduleID = PPPSID, @RNO=RNO 
					FROM @ActualDates 
					WHERE Done = 0 
					ORDER BY RNO 

					IF @CountryID NOT IN (14, /*Australia*/ 233 /*USA*/)
					BEGIN

						/*If we have already sent the mandate, we don't need to apply the mandate wait*/
						IF @RNO = 1 AND EXISTS (
						SELECT * 
						FROM AccountMandate a with (NOLOCK) 
						WHERE a.AccountID = @AccountID
						)
						BEGIN 

							SELECT @RNO = 2

						END
					END
						/*Ignore the requested collection date for the first collection*/ 
						IF @CountryID = 233 and @RNO = 1 
						BEGIN 

							Update ppps 
							SET ActualCollectionDate = DATEADD(DAY,1,dbo.fn_GetDate_Local()) 
							FROM PurchasedProductPaymentSchedule ppps 
							Where ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

						END
						

					  /* 2017-03-21 ACE Force it to mandate wait type 4 as there are issues with payments in the past not being re-calculated properly*/
					  /* 2018-04-30 change this back so we can have dates that mean something in the ppps (Schedule Date = send file, Collection Date = appears in account)*/
					  IF @RNO=1 -- mandate wait
							SELECT @WaitType=4
					  ELSE -- no wait
							SELECT @WaitType=5
                
                  
					  UPDATE pps
					  SET PaymentDate= CASE @RNO WHEN 1 THEN dbo.fn_C00_CalculateActualCollectionDate(@AccountID,@PaymentMethod,@ValidFrom,@WaitType ,pps.PaymentGross)
					  ELSE  /*dbo.fn_C00_CalculateActualCollectionDate(@AccountID,@PaymentMethod,@ActualDate,@WaitType ,pps.PaymentGross)*/ ActualCollectionDate END
					  FROM PurchasedProductPaymentSchedule pps
					  WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
                  
					  UPDATE @ActualDates SET Done = 1 WHERE PPPSID=@PurchasedProductPaymentScheduleID 
			     
					  /*For the first collection, we need to adjust the actual collection date to be three days after the mandate. So pass this through the same logic as the ActualCollectionDate*/ 
					  /*2018-06-11 ACE Only set it to 3 working days for BACS..*/
					  IF @RNO = 1 AND @PaymentMethod = 1
					  BEGIN 
					  
						  /*JEL Hacky mc hacky hack hack*/ 
						  /*When creating a renewal for a migrated policy, we won't send the mandate until the policy starts. We can identify a policy of this sort by 
						  there being no rows in the historical policy table that marked as 'not migrated', if this is the case, then simply force the first PPPS dates to be 10 working days
						  on from policy start (it looks like 11, but we send the mandates a day late*/  
						  IF NOT EXISTS ( SELECT * FROM TableRows tr with (NOLOCK) 
										  INNER JOIN TableDetailValues tdv with (NOLOCK) on tdv.TableRowID = tr.TableRowID
										  WHERE tr.DetailFieldID = 170033
										  AND tdv.DetailFieldID = 180216
										  AND tr.MatterID = @MatterID /*will be the matterID*/  
										  AND tdv.ValueInt = 0)
						  BEGIN 
					  
							  /*JEL 2018-01-23 final fix for 1338*/ 
							  /*If the prefered collection date for the migration month is after the bacs wait period then we use this rather than simply adding the mandate wait*/ 
							  DECLARE @YearPart VARCHAR(4) = CAST(DATEPART(Year, @ValidFrom) AS VARCHAR)
							  DECLARE @MonthPart VARCHAR(2) = CAST(DATEPART(Month, @ValidFrom) AS VARCHAR)
							  DECLARE @DayPart VARCHAR(2)
                          
							  SELECT @DayPart = CASE WHEN dbo.fnGetSimpleDvAsInt(170168,@MatterID) <= 9 THEN '0' + dbo.fnGetSimpleDv(170168,@MatterID) ELSE dbo.fnGetSimpleDvAsInt(170168,@MatterID) END
                          
							  SELECT @ValueDate = CAST(@YearPart + '-' + @MonthPart + '-' + @DayPart AS VARCHAR)	
						
							  IF @ValueDate < dbo.fnAddWorkingDays(@ValidFrom,13)
							  BEGIN 

								  UPDATE pps
								  SET ActualCollectionDate = dbo.fnAddWorkingDays(@ValidFrom,13), PaymentDate = dbo.fnAddWorkingDays(@ValidFrom,11)
								  FROM PurchasedProductPaymentSchedule pps
								  WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

							  END 
							  ELSE 
							  BEGIN 
						 
						 		  UPDATE pps
								  SET ActualCollectionDate = CASE dbo.fnIsWorkingDay (@ValueDate) WHEN 1 THEN @ValueDate ELSE dbo.fnAddWorkingDays(@ValueDate,1) END
								  FROM PurchasedProductPaymentSchedule pps
								  WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

								  UPDATE pps
								  SET  PaymentDate = dbo.fnAddWorkingDays(ActualCollectionDate,-2)
								  FROM PurchasedProductPaymentSchedule pps
								  WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

							  END
						  END
						  ELSE 
						  BEGIN 
					 
							  /*2018-08-31 ACE #52038 - As the BACS mandates are being submitted a day later we will need to set the first payment date to be a day later.*/
							  UPDATE pps
							  SET ActualCollectionDate = dbo.fnAddWorkingDays(pps.PaymentDate,3), PaymentDate = dbo.fnAddWorkingDays(pps.PaymentDate,1)
							  FROM PurchasedProductPaymentSchedule pps
							  WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
						 END 		  					
					  END	
				   END	
            END 
            
            IF @Debug = 1 
            BEGIN 
            
				SELECT * 
				FROM PurchasedProductPaymentSchedule pps
                WHERE pps.PurchasedProductID = @PurchasedProductID
				
            END 
            
            --create customer payment schedule 
            --if same date but different account then new entry
            --if same date and same account then 1 entry
            /*2017-03-22 ACE Fixed cps generation as the payments that have failed/worked are/will be re-used*/
			/*2018-07-11 ACE - RelatedObjectTypeID was being set by @ObjectID, incorrect. Needs to come from Account.ObjectID*/
            INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, PaymentDate, ActualCollectionDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, WhoCreated, WhenCreated, ClientAccountID, RelatedObjectID, RelatedObjectTypeID)
            SELECT @ClientID, @CustomerID, pps.AccountID, PaymentDate, ActualCollectionDate, SUM(PaymentNet) AS PaymentNet, SUM(PaymentVAT) AS PaymentVAT, SUM(PaymentGross) AS PaymentGross, 1, @WhoCreated, @WhenCreated, pps.ClientAccountID, a.ObjectID, 2
            FROM dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK)
			INNER JOIN Account a WITH ( NOLOCK ) on a.AccountID = pps.AccountID 
            WHERE (pps.CustomerID = @CustomerID) -- removed CustomerPaymentScheduleID Check
            AND pps.PaymentStatusID = 1 /*New*/
            GROUP BY PaymentDate, pps.AccountID, ActualCollectionDate , pps.ClientAccountID, a.ObjectID
            
            --updates the purchased product payment schedule with the newly created customer payment schedule identitys
            UPDATE PurchasedProductPaymentSchedule
            SET PurchasedProductPaymentSchedule.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
            FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
            INNER JOIN dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK) ON pps.PaymentDate = cps.PaymentDate AND pps.CustomerID=cps.CustomerID
            WHERE cps.CustomerID=@CustomerID AND cps.CustomerLedgerID IS NULL
            AND pps.AccountID = cps.AccountID
            AND 
            (pps.CustomerPaymentScheduleID IS NULL 
            OR 
            NOT EXISTS (
				SELECT *
				FROM CustomerPaymentSchedule cps2 WITH (NOLOCK)
				WHERE cps2.CustomerPaymentScheduleID = pps.CustomerPaymentScheduleID
            ))

            -- set the parentID to itself for the newly created rows
            UPDATE PurchasedProductPaymentSchedule
            SET PurchasedProductPaymentScheduleParentID = PurchasedProductPaymentScheduleID
            WHERE PurchasedProductPaymentScheduleParentID IS NULL
           
			SELECT @YearlyTransactionFee= SUM(pp.TransactionFee) 
			FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
			WHERE pp.PurchasedProductID = @PurchasedProductID
		          
            UPDATE PurchasedProduct
            SET PaymentScheduleSuccessfullyCreated=1, PaymentScheduleCreatedOn=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120), ProductAdditionalFee = @YearlyTransactionFee
            WHERE PurchasedProductID=@PurchasedProductID

      
			SELECT @LastPaymentDate = MAX(ppps.ActualCollectionDate)
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
			WHERE ppps.AccountID = @AccountID

			IF (SELECT a.AccountTypeID FROM Account a WHERE a.AccountID = @AccountID) = 1
			BEGIN

				/*2018-04-03 Set the account to expire 10 days after the last payment, this way we will send an AUDDIS file with a 0C record...*/
				UPDATE a
				SET ExpiryDate = dbo.fnAddWorkingDays(@LastPaymentDate,10) 
				FROM Account a 
				WHERE a.AccountID = @AccountID

			END
      
            SET @Success = 1
      
      END TRY  
      BEGIN CATCH  
      
			SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
				RAISERROR( @LogEntry, 16, 1 )

            UPDATE PurchasedProduct
            SET PaymentScheduleSuccessfullyCreated=0, PaymentScheduleCreatedOn=NULL
            WHERE PurchasedProductID=@PurchasedProductID
            
            SET @Success = 0
            
      END CATCH  
      
      SELECT @Success Success
      
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProduct__CreatePaymentSchedule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProduct__CreatePaymentSchedule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProduct__CreatePaymentSchedule] TO [sp_executeall]
GO
