SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetEquation] @EquationID int
AS

SELECT     EquationID, EquationName, Equation, ClientQuestionnaireID, ClientID 
FROM         Equations
WHERE EquationID = @EquationID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEquation] TO [sp_executeall]
GO
