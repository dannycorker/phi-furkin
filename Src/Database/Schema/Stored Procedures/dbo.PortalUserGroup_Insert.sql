SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalUserGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroup_Insert]
(

	@PortalUserGroupID int    OUTPUT,

	@ClientID int   ,

	@GroupName varchar (50)  ,

	@GroupDescription varchar (200)  ,

	@ShowGroupMessage bit   ,

	@MessageTitle varchar (MAX)  ,

	@MessageBody varchar (MAX)  ,

	@HideOwnCustomer bit   
)
AS


				
				INSERT INTO [dbo].[PortalUserGroup]
					(
					[ClientID]
					,[GroupName]
					,[GroupDescription]
					,[ShowGroupMessage]
					,[MessageTitle]
					,[MessageBody]
					,[HideOwnCustomer]
					)
				VALUES
					(
					@ClientID
					,@GroupName
					,@GroupDescription
					,@ShowGroupMessage
					,@MessageTitle
					,@MessageBody
					,@HideOwnCustomer
					)
				-- Get the identity value
				SET @PortalUserGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Insert] TO [sp_executeall]
GO
