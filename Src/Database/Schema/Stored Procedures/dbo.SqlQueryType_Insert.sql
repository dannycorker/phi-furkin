SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryType_Insert]
(

	@SqlQueryTypeID int    OUTPUT,

	@TypeName nchar (50)  
)
AS


				
				INSERT INTO [dbo].[SqlQueryType]
					(
					[TypeName]
					)
				VALUES
					(
					@TypeName
					)
				-- Get the identity value
				SET @SqlQueryTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Insert] TO [sp_executeall]
GO
