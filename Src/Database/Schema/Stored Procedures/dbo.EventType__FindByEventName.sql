SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 12/11/2014
-- Description:	Finds an event type via its name and lead type
-- =============================================
CREATE PROCEDURE [dbo].[EventType__FindByEventName]
	@ClientID INT,
	@EventTypeName VARCHAR(50),
	@LeadTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM EventType WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND EventTypeName = @EventTypeName AND LeadTypeID=@LeadTypeID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__FindByEventName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__FindByEventName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__FindByEventName] TO [sp_executeall]
GO
