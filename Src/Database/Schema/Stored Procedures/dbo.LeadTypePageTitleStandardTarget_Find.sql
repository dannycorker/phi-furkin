SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadTypePageTitleStandardTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadTypePageTitleStandardTargetID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@Target varchar (250)  = null ,

	@ObjectName varchar (250)  = null ,

	@PropertyName varchar (250)  = null ,

	@TemplateTypeID int   = null ,

	@Notes varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadTypePageTitleStandardTargetID]
	, [ClientID]
	, [LeadTypeID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [TemplateTypeID]
	, [Notes]
    FROM
	[dbo].[LeadTypePageTitleStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypePageTitleStandardTargetID] = @LeadTypePageTitleStandardTargetID OR @LeadTypePageTitleStandardTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadTypePageTitleStandardTargetID]
	, [ClientID]
	, [LeadTypeID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [TemplateTypeID]
	, [Notes]
    FROM
	[dbo].[LeadTypePageTitleStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypePageTitleStandardTargetID] = @LeadTypePageTitleStandardTargetID AND @LeadTypePageTitleStandardTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Find] TO [sp_executeall]
GO
