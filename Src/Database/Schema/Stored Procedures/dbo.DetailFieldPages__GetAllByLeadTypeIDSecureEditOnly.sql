SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:  Jim Green
-- Purpose: Select records from the DetailFieldPages that this user is allowed to see
-- but this time, only return pages they can EDIT
-- SB  2014-07-10 Updated to use view which includes shared pages
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages__GetAllByLeadTypeIDSecureEditOnly]
(

	@LeadTypeID int,
	@UserID int  
)
AS


				SET ANSI_NULLS OFF
				
				DECLARE @ClientID INT
				SELECT @ClientID = ClientID
				FROM dbo.LeadType WITH (NOLOCK) 
				WHERE LeadTypeID = @LeadTypeID
				
				SELECT DISTINCT dfp.*
				FROM
					dbo.fnDetailFieldPagesShared(@ClientID) dfp 
				INNER JOIN fnDetailFieldPagesSecure(@UserID, @LeadTypeID) f ON dfp.DetailFieldPageID = f.objectid
				WHERE
					[LeadTypeID] = @LeadTypeID
				AND Enabled = 1
				AND f.rightid > 1
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByLeadTypeIDSecureEditOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetAllByLeadTypeIDSecureEditOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByLeadTypeIDSecureEditOnly] TO [sp_executeall]
GO
