SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SearchQuery table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_Find]
(

	@SearchUsingOR bit   = null ,

	@SearchQueryID int   = null ,

	@ClientID int   = null ,

	@SubClientID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientPersonnelID int   = null ,

	@SearchQueryName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SearchQueryID]
	, [ClientID]
	, [SubClientID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [SearchQueryName]
    FROM
	[dbo].[SearchQuery] WITH (NOLOCK) 
    WHERE 
	 ([SearchQueryID] = @SearchQueryID OR @SearchQueryID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([SearchQueryName] = @SearchQueryName OR @SearchQueryName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SearchQueryID]
	, [ClientID]
	, [SubClientID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [SearchQueryName]
    FROM
	[dbo].[SearchQuery] WITH (NOLOCK) 
    WHERE 
	 ([SearchQueryID] = @SearchQueryID AND @SearchQueryID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([SearchQueryName] = @SearchQueryName AND @SearchQueryName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Find] TO [sp_executeall]
GO
