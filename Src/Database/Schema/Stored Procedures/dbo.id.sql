SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-01-09
-- Description:	Take a given ID and match against Customer / Lead / Case / Matter / Batch Job / Event Type etc etc records
-- CPS 2012-05-12 - Added extra types and the EXISTS checks to stop overloading the output
-- CPS 2017-06-12 - Added LookupListItem decoding to ResourceListDetailValues
-- =============================================
CREATE PROCEDURE [dbo].[id] 
	@ID int, 
	@ClientID int = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM dbo.Customers cu WITH (NOLOCK) WHERE cu.CustomerID = @ID and (cu.ClientID = @ClientID or @ClientID is null) ) 
	BEGIN 
		SELECT 'c ' + convert(varchar,@ID) [Describe],* FROM dbo.Customers cu WITH (NOLOCK) WHERE cu.CustomerID = @ID and (cu.ClientID = @ClientID or @ClientID is null)  
	END 
	IF EXISTS (SELECT * FROM dbo.Lead l WITH (NOLOCK) WHERE (l.LeadID = @ID or l.LeadRef = convert(varchar,@ID) ) and (l.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT * FROM dbo.Lead l WITH (NOLOCK) WHERE (l.LeadID = @ID or l.LeadRef = convert(varchar,@ID) ) and (l.ClientID = @ClientID or @ClientID is null)
	END 
	IF EXISTS (SELECT * FROM dbo.Cases c WITH (NOLOCK) WHERE c.CaseID = @ID and (c.ClientID = @ClientID or @ClientID is null) ) 
	BEGIN 
		SELECT 'ca ' + convert(varchar,@ID) [Describe],* FROM dbo.Cases c WITH (NOLOCK) WHERE c.CaseID = @ID and (c.ClientID = @ClientID or @ClientID is null)  
	END 
	IF EXISTS (SELECT * FROM dbo.Matter m WITH (NOLOCK) WHERE m.MatterID = @ID and (m.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'm ' + convert(varchar,@ID) [Describe],* FROM dbo.Matter m WITH (NOLOCK) WHERE m.MatterID = @ID and (m.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.AutomatedTask at WITH (NOLOCK) WHERE at.TaskID = @ID and (at.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,1,' + convert(varchar,@ID) [Describe],* FROM dbo.AutomatedTask at WITH (NOLOCK) WHERE at.TaskID = @ID and (at.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID = @ID and (df.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,2,' + convert(varchar,@ID) [Describe],* FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID = @ID and (df.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.EventType et WITH (NOLOCK) WHERE et.EventTypeID= @ID and (et.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,3,' + convert(varchar,@ID) [Describe],* FROM dbo.EventType et WITH (NOLOCK) WHERE et.EventTypeID= @ID and (et.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID= @ID and (le.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,4,' + convert(varchar,@ID) [Describe],* FROM dbo.LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID= @ID and (le.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.SqlQuery sq WITH (NOLOCK) WHERE sq.QueryID = @ID and (sq.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,5,' + convert(varchar,@ID) [Describe],* FROM dbo.SqlQuery sq WITH (NOLOCK) WHERE sq.QueryID = @ID and (sq.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.DocumentType dt WITH (NOLOCK) WHERE dt.DocumentTypeID = @ID and (dt.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,6,' + convert(varchar,@ID) [Describe],* FROM dbo.DocumentType dt WITH (NOLOCK) WHERE dt.DocumentTypeID = @ID and (dt.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @ID and (cp.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT 'aq 1,8,' + convert(varchar,@ID) [Describe],* FROM dbo.ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @ID and (cp.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.LeadStatus ls WITH (NOLOCK) WHERE ls.StatusID = @ID and (ls.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT * FROM dbo.LeadStatus ls WITH (NOLOCK) WHERE ls.StatusID = @ID and (ls.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS (SELECT * FROM dbo.LookupListItems luli WITH (NOLOCK) WHERE luli.LookupListItemID = @ID and (luli.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT * FROM dbo.LookupListItems luli WITH (NOLOCK) WHERE luli.LookupListItemID = @ID and (luli.ClientID = @ClientID or @ClientID is null) 
		
		SELECT 'OtherListItems' [OtherListItems],* 
		FROM LookupListItems li WITH ( NOLOCK ) 
		WHERE li.LookupListID = ( SELECT luli.LookupListID FROM dbo.LookupListItems luli WITH (NOLOCK) WHERE luli.LookupListItemID = @ID and (luli.ClientID = @ClientID or @ClientID is null)  )
	END 
	IF EXISTS ( SELECT * FROM DetailFieldPages dp WITH (NOLOCK) WHERE dp.DetailFieldPageID = @ID and (dp.ClientID = @ClientID or @ClientID is null) )
	BEGIN
		SELECT * ,
		'https://www.aquarium-software.com/DetailFieldList.aspx?MenuID=pnl5m&ltid=' + convert(varchar,dp.LeadTypeID) + '&lom=2&dfp=' + convert(varchar,dp.DetailFieldPageID) [Edit URL]
		FROM DetailFieldPages dp WITH (NOLOCK) 
		WHERE dp.DetailFieldPageID = @ID
		
		IF 4 = ( SELECT dp.LeadOrMatter FROM DetailFieldPages dp WITH (NOLOCK) WHERE dp.DetailFieldPageID = @ID )
		BEGIN
			SELECT 'EXEC rldv null,null,' + convert(varchar,@ID) [Show Resource Lists]
		END
		
		SELECT df.DetailFieldPageID,df.DetailFieldID, df.FieldName, df.FieldCaption, qt.Name [Question Type],df.FieldOrder, df.Enabled, df.Hidden
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN QuestionTypes qt WITH (NOLOCK) on qt.QuestionTypeID = df.QuestionTypeID
		WHERE df.DetailFieldPageID = @ID
		ORDER BY df.Enabled desc, df.Hidden,df.FieldOrder
	END
	IF EXISTS (SELECT * FROM dbo.TableRows tr WITH (NOLOCK) WHERE tr.TableRowID = @ID and (tr.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT df.FieldName,tr.* FROM dbo.TableRows tr WITH (NOLOCK) INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tr.DetailFieldID WHERE tr.TableRowID = @ID and (tr.ClientID = @ClientID or @ClientID is null) 

		/*CS 2015-06-15*/
		SELECT tr.TableRowID, tdv.TableDetailValueID,df2.DetailFieldID, df2.FieldName, tdv.DetailValue, tdv.ResourceListID, '' [-], 'EXEC dbo._C00_SimpleValueIntoField ' + convert(varchar,df2.DetailFieldID) + ', ''' + ISNULL(tdv.DetailValue,'') + ''', ' + convert(varchar,@ID) [Update]
		FROM TableRows tr WITH ( NOLOCK ) 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tr.DetailFieldID 
		INNER JOIN DetailFields df2 WITH ( NOLOCK ) on df2.DetailFieldPageID = df.TableDetailFieldPageID
		LEFT JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = df2.DetailFieldID
		WHERE tr.TableRowID = @ID
	END 
	IF EXISTS (SELECT * FROM dbo.LeadType lt WITH (NOLOCK) WHERE lt.LeadTypeID = @ID and (lt.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT * FROM dbo.LeadType lt WITH (NOLOCK) WHERE lt.LeadTypeID = @ID and (lt.ClientID = @ClientID or @ClientID is null) 
	END 
	IF EXISTS ( SELECT * FROM MasterQuestions mq WITH (NOLOCK) WHERE mq.MasterQuestionID = @ID and ( mq.ClientID = @ClientID or @ClientID is null ) )
	BEGIN
		SELECT * 
	    FROM MasterQuestions mq WITH (NOLOCK) 
		WHERE mq.MasterQuestionID = @ID
		
		SELECT fl.MasterQuestionID, fl.DetailFieldLinkID, df.DetailFieldID, df.FieldName
		FROM DetailFieldLink fl WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = fl.DetailFieldID 
		WHERE fl.MasterQuestionID = @ID
		
		IF 4 = ( SELECT mq.QuestionTypeID FROM MasterQuestions mq WITH (NOLOCK) WHERE mq.MasterQuestionID = @ID )
		BEGIN
			SELECT pa.MasterQuestionID, pa.QuestionPossibleAnswerID, pa.AnswerText, pa.SourceID
			FROM QuestionPossibleAnswers pa WITH (NOLOCK) 
			WHERE pa.MasterQuestionID = @ID
		END
	END
	IF EXISTS (SELECT * FROM dbo.ResourceList rl WITH (NOLOCK) WHERE rl.ResourceListID = @ID and (rl.ClientID = @ClientID or @ClientID is null)) 
	BEGIN 
		SELECT rdv.ClientID,p.PageName [ResourceList Name], rdv.ResourceListID, df.DetailFieldID, df.FieldName, rdv.DetailValue, li.ItemValue
		FROM dbo.ResourceListDetailValues rdv WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = rdv.DetailFieldID
		INNER JOIN dbo.DetailFieldPages p WITH (NOLOCK) on p.DetailFieldPageID = df.DetailFieldPageID
		LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = rdv.ValueInt AND li.LookupListID = df.LookupListID
		WHERE rdv.ResourceListID = @ID
		and (rdv.ClientID = @ClientID or @ClientID is null)
		ORDER BY df.FieldOrder
	END 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[id] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[id] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[id] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[id] TO [sp_executehelper]
GO
