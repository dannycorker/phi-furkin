SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-02-10
-- Description:	Applies a lead event, and follows up a previous event if one is specified
-- 2013-07-09 SB/JG New option to follow up all events for the case
-- 2014-09-19 SB Fix bug with WhenFollowed up being set incorrectly on LeadEvent while LETC still not followed up
-- 2021-01-27 CPS for Testing | If we have explicitly passed in -1, don't follow anything up
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ApplyLeadEvent]

	@LeadEvent dbo.tvpLeadEvent READONLY,
	@FollowUpAllThreads BIT = 0
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE	@EventCountWithFollowUp INT
	
	DECLARE @LeadEventIDs TABLE 
	(
		ID INT IDENTITY,
		LeadEventID INT
	)
	
	DECLARE @NewlyAddedLeadEventIDs TABLE 
	(
		ID INT IDENTITY,
		ClientID INT,
		LeadID INT,
		CaseID INT,
		EventTypeID INT,
		LeadEventID INT
	)
	
	/*
		2013-07-09 SB/JG New option to follow up all events for the case.
		Bypass all checks about which events should follow up which existing ones.
	*/
	IF @FollowUpAllThreads = 1
	BEGIN
		
		/* Add the new event(s) and capture the ID(s) in a table variable */
		INSERT LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
		OUTPUT inserted.ClientID, inserted.LeadID, inserted.CaseID, inserted.EventTypeID, inserted.LeadEventID INTO @NewlyAddedLeadEventIDs
		SELECT ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID
		FROM @LeadEvent
		ORDER BY LeadEventID
		
		/*
			Now update all other LeadEventThreadCompletion records for this case, marking them as followed up.
			Exclude the event(s) that we have just added though.
		*/
		UPDATE LeadEventThreadCompletion
		SET ToLeadEventID = new.LeadEventID, 
		ToEventTypeID = new.EventTypeID 
		FROM @NewlyAddedLeadEventIDs new
		INNER JOIN dbo.LeadEventThreadCompletion tc WITH (NOLOCK)
			ON new.ClientID = tc.ClientID
			AND new.LeadID = tc.LeadID
			AND new.CaseID = tc.CaseID
		WHERE tc.ToLeadEventID IS NULL
			AND tc.FromLeadEventID NOT IN (SELECT new.LeadEventID FROM @NewlyAddedLeadEventIDs) 
		
		/*
			Update the old LeadEvent records here to show which new ones followed them up.
		*/
		UPDATE le 
		SET WhenFollowedUp = le2.WhenCreated, NextEventID = le2.LeadEventID 
		FROM dbo.LeadEvent le 
		INNER JOIN dbo.LeadEventThreadCompletion letc ON letc.CaseID = le.CaseID 
			AND letc.FromLeadEventID = le.LeadEventID 
			AND letc.ToLeadEventID IS NOT NULL 
		INNER JOIN dbo.LeadEvent le2 ON le2.LeadEventID = letc.ToLeadEventID AND le2.EventDeleted = 0 
		WHERE le.WhenFollowedUp IS NULL 
		AND le.EventDeleted = 0 
		AND le.CaseID IN 
		(
			SELECT DISTINCT leTVP.CaseID
			FROM @LeadEvent leTVP 
		)
		
		/* Return just the sequence number and the new LeadEventID from the recently added events */
		SELECT new.ID, new.LeadEventID 
		FROM @NewlyAddedLeadEventIDs new 
		
	END
	ELSE
	BEGIN
	
		/* Original code */
		SELECT @EventCountWithFollowUp = COUNT(*)
		FROM @LeadEvent
		WHERE LeadEventIDToFollowUp IS NOT NULL
		
		DECLARE @MatchingFollowUp INT
		SELECT @MatchingFollowUp = COUNT(*) 
		FROM @LeadEvent le
		INNER JOIN dbo.LeadEventThreadCompletion tc WITH (NOLOCK)
			ON le.ClientID = tc.ClientID
			AND le.LeadID = tc.LeadID
			AND le.CaseID = tc.CaseID
			AND le.LeadEventIDToFollowUp = tc.FromLeadEventID
		INNER JOIN dbo.EventChoice ec WITH (NOLOCK) 
			ON le.ClientID = ec.ClientID
			AND tc.FromEventTypeID = ec.EventTypeID
			AND le.EventTypeID = ec.NextEventTypeID
			AND tc.ThreadNumberRequired = ec.ThreadNumber
			
		IF @EventCountWithFollowUp != @MatchingFollowUp OR @FollowUpAllThreads = -1 -- 2021-01-27 CPS for Testing | If we have explicitly passed in -1, don't follow anything up
		BEGIN
			SELECT *
			FROM @LeadEventIDs
			WHERE 1 = 2
		END
		ELSE 
		BEGIN
		
			INSERT LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
			OUTPUT inserted.LeadEventID INTO @LeadEventIDs
			SELECT ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID
			FROM @LeadEvent
			ORDER BY LeadEventID
		
			UPDATE tc
			SET tc.ToLeadEventID = id.LeadEventID
			FROM @LeadEvent le
			INNER JOIN dbo.LeadEventThreadCompletion tc WITH (NOLOCK)
				ON le.ClientID = tc.ClientID
				AND le.LeadID = tc.LeadID
				AND le.CaseID = tc.CaseID
				AND (le.LeadEventIDToFollowUp = tc.FromLeadEventID)
			INNER JOIN dbo.EventChoice ec WITH (NOLOCK) 
				ON le.ClientID = ec.ClientID
				AND tc.FromEventTypeID = ec.EventTypeID
				AND le.EventTypeID = ec.NextEventTypeID
				AND tc.ThreadNumberRequired = ec.ThreadNumber
			INNER JOIN @LeadEventIDs id ON le.LeadEventID = id.ID
			
			/*
				Update the old LeadEvent records here to show which new ones followed them up.
			*/
			UPDATE le 
			SET WhenFollowedUp = le2.WhenCreated, NextEventID = le2.LeadEventID 
			FROM dbo.LeadEvent le 
			INNER JOIN dbo.LeadEventThreadCompletion letc ON letc.CaseID = le.CaseID 
				AND letc.FromLeadEventID = le.LeadEventID 
				AND letc.ToLeadEventID IS NOT NULL 
			INNER JOIN dbo.LeadEvent le2 ON le2.LeadEventID = letc.ToLeadEventID AND le2.EventDeleted = 0 
			WHERE le.WhenFollowedUp IS NULL 
			AND le.EventDeleted = 0 
			AND le.CaseID IN -- Updating any lead events that should be followed up... kind of what the housekeeping will do anyway but no harm done.
			(
				SELECT DISTINCT leTVP.CaseID
				FROM @LeadEvent leTVP 
			)
			AND NOT EXISTS -- Prevent setting as followed up if there are letc threads still open
			(
				SELECT *
				FROM dbo.LeadEventThreadCompletion letc2 WITH (NOLOCK) 
				WHERE letc2.FromLeadEventID = le.LeadEventID
				AND letc2.ToLeadEventID IS NULL 
			) 
			
			SELECT * 
			FROM @LeadEventIDs
			
		END
		
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ApplyLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ApplyLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ApplyLeadEvent] TO [sp_executeall]
GO
