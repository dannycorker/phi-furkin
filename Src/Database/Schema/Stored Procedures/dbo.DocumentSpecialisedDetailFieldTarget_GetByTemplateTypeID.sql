SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentSpecialisedDetailFieldTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget_GetByTemplateTypeID]
(

	@TemplateTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentSpecialisedDetailFieldTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[RowField],
					[Notes],
					[ExcelSheetLocation],
					[Format],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentSpecialisedDetailFieldTarget] WITH (NOLOCK) 
				WHERE
					[TemplateTypeID] = @TemplateTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByTemplateTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
