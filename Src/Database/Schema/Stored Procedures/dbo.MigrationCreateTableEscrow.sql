SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-11-16
-- Description:	Generate a script to create troublesome tables.  
-- Based on this article: http://www.sqlservercentral.com/scripts/Create+DDL+sql+statements/65863/ 
-- =============================================
CREATE PROCEDURE [dbo].[MigrationCreateTableEscrow]
(
	@TargetDatabaseName SYSNAME = NULL, 
	@ClientIDList VARCHAR(MAX) = NULL 
)
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @Schema_Name SYSNAME = 'dbo',
            @SqlStatement VARCHAR(MAX),
            @TableName SYSNAME = NULL
	
	DECLARE @TableStatements TABLE (AnyID INT IDENTITY(1, 1), LineCommand VARCHAR(MAX))
	
	DECLARE @TrickyTables TABLE (TableName SYSNAME)
	
	INSERT @TrickyTables(TableName) 
	SELECT mt.TableName 
	FROM dbo.MigrationTable mt WITH (NOLOCK) 
	WHERE mt.UseBCP = 1 
	AND mt.TableName NOT IN ('LeadDocumentFS', 'UploadedFile') /* These tables are scripted manually for now due to the FILESTREAM and UNIQUEIDENTIFIER columns */
	AND (mt.ClientID = 0 OR mt.ClientID IN (SELECT * FROM dbo.fnTableOfValues(@ClientIDList,'_')))
	ORDER BY mt.TableName 
	
	/* Loop round all tables?  Or just the biggest ones? Or one at a time? */	
	WHILE EXISTS(SELECT TOP 1 * FROM @TrickyTables)
	BEGIN
		
		SELECT TOP 1 @TableName = tt.TableName 
		FROM @TrickyTables tt 
	
		SELECT @SqlStatement = 'CREATE TABLE ' + CASE WHEN @TargetDatabaseName IS NULL THEN '' ELSE QUOTENAME(@TargetDatabaseName) + '.' END + QUOTENAME(@Schema_Name) + '.' + QUOTENAME(@TableName) + '('

	
		;WITH InnerSql AS 
		(
			SELECT isc.COLUMN_NAME AS [FieldName],
			CASE WHEN isc.IS_NULLABLE = 'YES' THEN 1 ELSE 0 END AS [IsNullable],
			isc.DATA_TYPE AS [DataType],
			CAST(isc.CHARACTER_MAXIMUM_LENGTH AS INT) AS [MaxLength],
			CAST(isc.NUMERIC_PRECISION AS INT) AS [NumericPrecision],
			CAST(isc.NUMERIC_SCALE AS INT) AS [NumericScale],
			isc.DOMAIN_NAME AS [DomainName],
			isc.COLUMN_NAME + ',' AS [FieldListingName],
			scc.definition AS [ComputedColumnDefinition],
			CASE WHEN ic.object_id IS NULL THEN 0 ELSE 1 END AS [IdentityColumn],
			CAST(ISNULL(ic.seed_value,0) AS INT) AS [IdentitySeed],
			CAST(ISNULL(ic.increment_value,0) AS INT) AS [IdentityIncrement],
			CASE WHEN isc.DATA_TYPE NOT IN ('TEXT') AND st.collation_name IS NOT NULL THEN 1 ELSE 0 END AS [IsCharColumn],
			ROW_NUMBER() OVER(ORDER BY isc.ORDINAL_POSITION) AS [ColumnOrder]
			FROM INFORMATION_SCHEMA.COLUMNS isc
			INNER JOIN sys.columns sc ON isc.TABLE_NAME = OBJECT_NAME(sc.object_id) AND isc.COLUMN_NAME = sc.Name
			INNER JOIN sys.types st ON COALESCE(isc.DOMAIN_NAME, isc.DATA_TYPE) = st.name
			LEFT JOIN sys.identity_columns ic ON isc.TABLE_NAME = OBJECT_NAME(ic.object_id) AND isc.COLUMN_NAME = ic.Name
			LEFT JOIN sys.objects dobj ON dobj.object_id = sc.default_object_id AND dobj.type = 'D'
			LEFT JOIN sys.computed_columns scc ON scc.object_id = sc.object_id AND scc.column_id = sc.column_id
			WHERE isc.TABLE_NAME = @TableName
		)
		SELECT @SqlStatement += CHAR(10) + CHAR(9) + QUOTENAME(FieldName) + ' ' +
			CASE 
				WHEN ComputedColumnDefinition IS NOT NULL THEN ' AS ' + ComputedColumnDefinition 
				ELSE UPPER(DataType) 
					+ CASE WHEN IsCharColumn = 1 OR DataType IN ('Varbinary') THEN '(' + CASE WHEN MaxLength = -1 THEN 'MAX' ELSE CAST(MaxLength AS VARCHAR(10)) END + ')' ELSE '' END 
					+ CASE WHEN IdentityColumn = 1 THEN ' IDENTITY(' + CAST(IdentitySeed AS VARCHAR(5))+ ',' + CAST(IdentityIncrement AS VARCHAR(5)) + ')' ELSE '' END 
					+ CASE WHEN IsNullable = 1 THEN ' NULL ' ELSE ' NOT NULL ' END 
				END
			+ ','
		FROM InnerSql i 
		/*
			It would be nice to finish this off with: 
				ORDER BY i.ColumnOrder
			but that actually stops the SELECT @SqlStatement += trick from working. It only selects the last column then. 
		*/

		SELECT @SqlStatement = LEFT(@SqlStatement, LEN(@SqlStatement) - 1) + ')'
	            
		/* Put the CREATE TABLE statement into a table variable so we can select the results back out to the calling script */
		INSERT @TableStatements(LineCommand)
		VALUES (@SqlStatement)

		/* Cater for multiple tables */
		INSERT @TableStatements(LineCommand)
		VALUES ('
		GO
		')
	
		/* Remove this table from the to-do list */
		DELETE @TrickyTables 
		WHERE TableName = @TableName
	
	END
	
	
	/* Select the results back out to the calling script */
	SELECT LineCommand  
	FROM @TableStatements
	ORDER BY AnyID


END








GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationCreateTableEscrow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MigrationCreateTableEscrow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationCreateTableEscrow] TO [sp_executeall]
GO
