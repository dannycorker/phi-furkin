SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget_Insert]
(

	@DocumentSpecialisedDetailFieldTargetID int    OUTPUT,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@RowField int   ,

	@Notes varchar (250)  ,

	@ExcelSheetLocation varchar (50)  ,

	@Format varchar (2000)  ,

	@DocumentTypeVersionID int   
)
AS


				
				INSERT INTO [dbo].[DocumentSpecialisedDetailFieldTarget]
					(
					[ClientID]
					,[DocumentTypeID]
					,[Target]
					,[DetailFieldID]
					,[ColumnField]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[ColumnFieldAlias]
					,[RowField]
					,[Notes]
					,[ExcelSheetLocation]
					,[Format]
					,[DocumentTypeVersionID]
					)
				VALUES
					(
					@ClientID
					,@DocumentTypeID
					,@Target
					,@DetailFieldID
					,@ColumnField
					,@TemplateTypeID
					,@DetailFieldAlias
					,@ColumnFieldAlias
					,@RowField
					,@Notes
					,@ExcelSheetLocation
					,@Format
					,@DocumentTypeVersionID
					)
				-- Get the identity value
				SET @DocumentSpecialisedDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Insert] TO [sp_executeall]
GO
