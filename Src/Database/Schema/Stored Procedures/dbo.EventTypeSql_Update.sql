SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeSql table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_Update]
(

	@EventTypeSqlID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@PostUpdateSql varchar (MAX)  ,

	@IsNativeSql bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeSql]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[PostUpdateSql] = @PostUpdateSql
					,[IsNativeSql] = @IsNativeSql
				WHERE
[EventTypeSqlID] = @EventTypeSqlID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Update] TO [sp_executeall]
GO
