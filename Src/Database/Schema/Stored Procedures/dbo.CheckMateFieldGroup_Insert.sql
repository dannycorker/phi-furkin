SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CheckMateFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldGroup_Insert]
(

	@CheckMateFieldGroupID int    OUTPUT,

	@GroupName varchar (50)  ,

	@GroupDescription varchar (250)  
)
AS


				
				INSERT INTO [dbo].[CheckMateFieldGroup]
					(
					[GroupName]
					,[GroupDescription]
					)
				VALUES
					(
					@GroupName
					,@GroupDescription
					)
				-- Get the identity value
				SET @CheckMateFieldGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Insert] TO [sp_executeall]
GO
