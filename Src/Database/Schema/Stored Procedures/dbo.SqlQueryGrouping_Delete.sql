SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryGrouping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryGrouping_Delete]
(

	@SqlQueryGroupingID int   
)
AS


				DELETE FROM [dbo].[SqlQueryGrouping] WITH (ROWLOCK) 
				WHERE
					[SqlQueryGroupingID] = @SqlQueryGroupingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryGrouping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Delete] TO [sp_executeall]
GO
