SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestionGroupMember table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_GetBySMSQuestionID]
(

	@SMSQuestionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSQuestionGroupMemberID],
					[ClientID],
					[SMSQuestionnaireID],
					[SMSQuestionID],
					[SMSQuestionGroupID]
				FROM
					[dbo].[SMSQuestionGroupMember] WITH (NOLOCK) 
				WHERE
					[SMSQuestionID] = @SMSQuestionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionID] TO [sp_executeall]
GO
