SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailFieldLink table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailFieldLinkID int   = null ,

	@ClientID int   = null ,

	@DetailFieldID int   = null ,

	@MasterQuestionID int   = null ,

	@LeadTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailFieldLinkID]
	, [ClientID]
	, [DetailFieldID]
	, [MasterQuestionID]
	, [LeadTypeID]
    FROM
	[dbo].[DetailFieldLink] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldLinkID] = @DetailFieldLinkID OR @DetailFieldLinkID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([MasterQuestionID] = @MasterQuestionID OR @MasterQuestionID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailFieldLinkID]
	, [ClientID]
	, [DetailFieldID]
	, [MasterQuestionID]
	, [LeadTypeID]
    FROM
	[dbo].[DetailFieldLink] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldLinkID] = @DetailFieldLinkID AND @DetailFieldLinkID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([MasterQuestionID] = @MasterQuestionID AND @MasterQuestionID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Find] TO [sp_executeall]
GO
