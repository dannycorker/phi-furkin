SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SearchQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_Delete]
(

	@SearchQueryID int   
)
AS


				DELETE FROM [dbo].[SearchQuery] WITH (ROWLOCK) 
				WHERE
					[SearchQueryID] = @SearchQueryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Delete] TO [sp_executeall]
GO
