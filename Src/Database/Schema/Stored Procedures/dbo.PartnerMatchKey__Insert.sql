SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-01-2013
-- Description:	Inserts a record in PartnerMatchKey
-- =============================================
CREATE PROCEDURE [dbo].[PartnerMatchKey__Insert]

	@ClientID INT,
	@PartnerID INT,
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250)

AS
BEGIN

	IF @DateOfBirth = ''
	BEGIN
	
		SET @DateOfBirth = NULL
	
	END

	INSERT INTO PartnerMatchKey (ClientID, PartnerID, Firstname, Lastname, Address1, Postcode, DateOfBirth)
	VALUES (@ClientID, @PartnerID, @Firstname, @Lastname, @Address1, @Postcode, @DateOfBirth)
	
	SELECT SCOPE_IDENTITY()
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PartnerMatchKey__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__Insert] TO [sp_executeall]
GO
