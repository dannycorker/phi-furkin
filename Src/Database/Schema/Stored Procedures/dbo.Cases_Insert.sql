SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Cases table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_Insert]
(

	@CaseID int    OUTPUT,

	@LeadID int   ,

	@ClientID int   ,

	@CaseNum int   ,

	@CaseRef varchar (250)  ,

	@ClientStatusID int   ,

	@AquariumStatusID int   ,

	@DefaultContactID int   ,

	@LatestLeadEventID int   ,

	@LatestInProcessLeadEventID int   ,

	@LatestOutOfProcessLeadEventID int   ,

	@LatestNonNoteLeadEventID int   ,

	@LatestNoteLeadEventID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@ProcessStartLeadEventID int   
)
AS


				
				INSERT INTO [dbo].[Cases]
					(
					[LeadID]
					,[ClientID]
					,[CaseNum]
					,[CaseRef]
					,[ClientStatusID]
					,[AquariumStatusID]
					,[DefaultContactID]
					,[LatestLeadEventID]
					,[LatestInProcessLeadEventID]
					,[LatestOutOfProcessLeadEventID]
					,[LatestNonNoteLeadEventID]
					,[LatestNoteLeadEventID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[ProcessStartLeadEventID]
					)
				VALUES
					(
					@LeadID
					,@ClientID
					,@CaseNum
					,@CaseRef
					,@ClientStatusID
					,@AquariumStatusID
					,@DefaultContactID
					,@LatestLeadEventID
					,@LatestInProcessLeadEventID
					,@LatestOutOfProcessLeadEventID
					,@LatestNonNoteLeadEventID
					,@LatestNoteLeadEventID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@ProcessStartLeadEventID
					)
				-- Get the identity value
				SET @CaseID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Insert] TO [sp_executeall]
GO
