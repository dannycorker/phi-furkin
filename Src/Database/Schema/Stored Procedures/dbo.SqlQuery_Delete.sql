SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_Delete]
(

	@QueryID int   
)
AS


				DELETE FROM [dbo].[SqlQuery] WITH (ROWLOCK) 
				WHERE
					[QueryID] = @QueryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Delete] TO [sp_executeall]
GO
