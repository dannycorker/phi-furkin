SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LookupList table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_Find]
(

	@SearchUsingOR bit   = null ,

	@LookupListID int   = null ,

	@LookupListName varchar (500)  = null ,

	@LookupListDescription varchar (2000)  = null ,

	@ClientID int   = null ,

	@Enabled bit   = null ,

	@SortOptionID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@LeadTypeID int   = null ,

	@IsShared bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LookupListID]
	, [LookupListName]
	, [LookupListDescription]
	, [ClientID]
	, [Enabled]
	, [SortOptionID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [LeadTypeID]
	, [IsShared]
    FROM
	dbo.fnLookupListShared(@ClientID)
    WHERE 
	 ([LookupListID] = @LookupListID OR @LookupListID IS NULL)
	AND ([LookupListName] = @LookupListName OR @LookupListName IS NULL)
	AND ([LookupListDescription] = @LookupListDescription OR @LookupListDescription IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([SortOptionID] = @SortOptionID OR @SortOptionID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LookupListID]
	, [LookupListName]
	, [LookupListDescription]
	, [ClientID]
	, [Enabled]
	, [SortOptionID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [LeadTypeID]
	, [IsShared]
    FROM
	dbo.fnLookupListShared(@ClientID) 
    WHERE 
	 ([LookupListID] = @LookupListID AND @LookupListID is not null)
	OR ([LookupListName] = @LookupListName AND @LookupListName is not null)
	OR ([LookupListDescription] = @LookupListDescription AND @LookupListDescription is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([SortOptionID] = @SortOptionID AND @SortOptionID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Find] TO [sp_executeall]
GO
