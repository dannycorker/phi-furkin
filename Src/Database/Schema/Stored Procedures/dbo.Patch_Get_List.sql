SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Patch table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Patch_Get_List]

AS


				
				SELECT
					[PatchID],
					[PatchDate],
					[PatchVersion]
				FROM
					[dbo].[Patch] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Patch_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Get_List] TO [sp_executeall]
GO
