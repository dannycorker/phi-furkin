SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AutomatedTaskResult table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_Insert]
(

	@AutomatedTaskResultID int    OUTPUT,

	@TaskID int   ,

	@ClientID int   ,

	@RunDate datetime   ,

	@Description varchar (2500)  ,

	@RecordCount int   ,

	@Complete bit   
)
AS


				
				INSERT INTO [dbo].[AutomatedTaskResult]
					(
					[TaskID]
					,[ClientID]
					,[RunDate]
					,[Description]
					,[RecordCount]
					,[Complete]
					)
				VALUES
					(
					@TaskID
					,@ClientID
					,@RunDate
					,@Description
					,@RecordCount
					,@Complete
					)
				-- Get the identity value
				SET @AutomatedTaskResultID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Insert] TO [sp_executeall]
GO
