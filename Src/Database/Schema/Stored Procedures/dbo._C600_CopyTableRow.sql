SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-12
-- Description:	Copy a table row and return new TableRowID
--				(hacked version of Jim's _C00_CopyTableRows)
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CopyTableRow]
	@TRIDToCopy INT,
	@NewDetailFieldID int = null,
	@NewDetailFieldPageID int = null
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @NewTRID INT

	/* Declare a table to hold the new values and the old ones they map to */
	DECLARE @NewTableRows TABLE (NewTableRowID int, OldTableRowID int)
	
	/* Create the new rows first, using DetailFieldPageID to hold the old RowID temporarily */
	INSERT INTO dbo.TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID)
	OUTPUT inserted.TableRowID, inserted.DetailFieldPageID INTO @NewTableRows (NewTableRowID, OldTableRowID)
	SELECT trOld.ClientID, trOld.LeadID, trOld.MatterID, COALESCE(@NewDetailFieldID, trOld.DetailFieldID), trOld.TableRowID, trOld.DenyEdit, trOld.DenyDelete, trOld.CustomerID, trOld.CaseID, trOld.ClientPersonnelID, trOld.ContactID 
	FROM dbo.TableRows trOld 
	WHERE trOld.TableRowID=@TRIDToCopy

	/* Copy all the values across */
	INSERT INTO dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, EncryptedValue, ErrorMsg, CustomerID, CaseID, ClientPersonnelID, ContactID)
	SELECT ntr.NewTableRowID, tdvOld.ResourceListID, tdvOld.DetailFieldID, tdvOld.DetailValue, tdvOld.LeadID, tdvOld.MatterID, tdvOld.ClientID, tdvOld.EncryptedValue, tdvOld.ErrorMsg, tdvOld.CustomerID, tdvOld.CaseID, tdvOld.ClientPersonnelID, tdvOld.ContactID 
	FROM @NewTableRows ntr 
	INNER JOIN dbo.TableDetailValues tdvOld WITH (NOLOCK) ON tdvOld.TableRowID = ntr.OldTableRowID
	
	/* Now set the DetailFieldPageID values to the as appropriate */
	IF @NewDetailFieldPageID IS NULL
	BEGIN
		/* Use the old value */
		UPDATE trNew 
		SET DetailFieldPageID = trOld.DetailFieldPageID
		FROM @NewTableRows ntr
		INNER JOIN dbo.TableRows trOld WITH (NOLOCK) ON trOld.TableRowID = ntr.OldTableRowID
		INNER JOIN dbo.TableRows trNew WITH (NOLOCK) ON trNew.TableRowID = ntr.NewTableRowID
	END
	ELSE
	BEGIN
		/* Use the new value */
		UPDATE dbo.TableRows 
		SET DetailFieldPageID = @NewDetailFieldPageID
		FROM @NewTableRows ntr
		INNER JOIN dbo.TableRows trNew WITH (NOLOCK) ON trNew.TableRowID = ntr.NewTableRowID
	END
	
	SELECT @NewTRID=NewTableRowID FROM @NewTableRows
	
	RETURN @NewTRID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CopyTableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CopyTableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CopyTableRow] TO [sp_executeall]
GO
