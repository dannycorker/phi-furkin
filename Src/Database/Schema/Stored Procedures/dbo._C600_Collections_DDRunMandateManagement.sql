SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 22-Oct-2014
-- Description:	Collections DD mandate requests
-- Mods
-- 2016-09-27 DCM Set AccountMandate.Reference
-- 2016-11-21 DCM change mandate status to 3 (ready) from 1 (pending)
-- 2017-08-23 CPS updated call to fn_C00_CalculateActualCollectionDate
-- 2017-08-30 CPS use fn_C600_GetDatabaseSpecificConfigValue to get the correct file path by database for local
-- 2017-09-19 JEL use fn_C600_GetDatabaseSpecificConfigValue to get the correct file path by database for sftp
-- 2018-03-13 ACE Updated for new blackbox billing
-- 2018-03-24 JEL Added Client Account config
-- 2018-04-10 JEL Changed file name generation 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_DDRunMandateManagement]
(
	@XmlData XML	
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FileAndFolderEntryToUse INT -- TableRowID from Fileand Folder Names table

	DECLARE 
			@AccountID INT,
			@Cert VARCHAR(2000),
			@ClientID INT = dbo.fnGetPrimaryClientID(), /*2018-03-15 ACE Set the clientid*/
			@EffectiveDate DATE,
			@EPSTable dbo.tvpGenericPaymentData,
			@EventToAdd INT,
			@FileFormatID INT,
			@Filename VARCHAR(250),
			@LeadEventID INT, 
			@LocalFolder VARCHAR(500),
			@MatterID INT, 
			@PayRef VARCHAR(50),
			@PW VARCHAR(100),
			@Seq INT,
			@ServerName VARCHAR(500),
			@SFTPFolder VARCHAR(500),
			@TransactionCode VARCHAR(10),
			@Username VARCHAR(100),
			@WhoCreated INT = dbo.fn_C600_GetAqAutomationUser(),
			@TableRowID INT,
			@ClientAccountID INT 
			
	EXEC dbo.LogXML__AddEntry @ClientID, 0, '_C600_Collections_DDRunMandateManagement_LnG', @XmlData, NULL

	INSERT INTO @EPSTable (AccountName,AccountNumber,SortCode,Amount,BacsTransactionCode,Reference)
	SELECT
    bacs.value('(AccountHolderName)[1]', 'varchar(100)'),
    bacs.value('(AccountNumber)[1]', 'varchar(100)'),
    bacs.value('(SortCode)[1]', 'varchar(100)'),
    bacs.value('(Amount)[1]', 'varchar(100)'),
    bacs.value('(TransactionCode)[1]', 'varchar(100)'),
    bacs.value('(Reference)[1]', 'varchar(100)') 
	FROM @XMLData.nodes('/row') AS Tbl(bacs)
	
	/*Pick up the client Account ID for this run, will be uniform for each record per run*/  
	SELECT TOP 1 @ClientAccountID =  bacs.value('(ClientAccountID)[1]','varchar(100)')
	FROM @XMLData.nodes('/row') AS Tbl(bacs)
	
	
	-- Pick up the sftp location and details
	SELECT	     @SFTPFolder=dbo.fn_C600_GetDatabaseSpecificConfigValue('SFTPdb') /*JEL 2017-09-19*/--,@SFTPFolder=CASE WHEN ut.ValueInt=5144 THEN tf.DetailValue ELSE lf.DetailValue END
				,@LocalFolder = dbo.fn_C600_GetDatabaseSpecificConfigValue('SharedResourcesPath') /*CPS 2017-08-30 (with James' approval!)*/

			,@ServerName	=CASE WHEN ut.ValueInt=5144 THEN tserv.DetailValue	ELSE serv.DetailValue	END
			,@Username		=CASE WHEN ut.ValueInt=5144 THEN 'Aquarium_support' ELSE un.DetailValue		END
			,@PW			=CASE WHEN ut.ValueInt=5144 THEN 'Password4231'		ELSE pw.DetailValue		END
			,@Cert			=CASE WHEN ut.ValueInt=5144 THEN tcrt.DetailValue	ELSE crt.DetailValue	END
	FROM ClientDetailValues tun WITH (NOLOCK) 
	INNER JOIN ClientDetailValues tpw WITH (NOLOCK) ON tun.ClientID=tpw.ClientID AND tpw.DetailFieldID=177170 
	INNER JOIN ClientDetailValues tcrt WITH (NOLOCK) ON tun.ClientID=tcrt.ClientID AND tcrt.DetailFieldID=177171 
	INNER JOIN ClientDetailValues tserv WITH (NOLOCK) ON tun.ClientID=tserv.ClientID AND tserv.DetailFieldID=177173 

	INNER JOIN ClientDetailValues un WITH (NOLOCK) ON tun.ClientID=un.ClientID AND un.DetailFieldID=177115 
	INNER JOIN ClientDetailValues pw WITH (NOLOCK) ON tun.ClientID=pw.ClientID AND pw.DetailFieldID=177116 
	INNER JOIN ClientDetailValues crt WITH (NOLOCK) ON tun.ClientID=crt.ClientID AND crt.DetailFieldID=177121 
	INNER JOIN ClientDetailValues serv WITH (NOLOCK) ON tun.ClientID=serv.ClientID AND serv.DetailFieldID=177172 
		
	INNER JOIN ClientDetailValues ut WITH (NOLOCK) ON tun.ClientID=ut.ClientID AND ut.DetailFieldID=177168 
	WHERE tun.ClientID = @ClientID
	AND tun.DetailFieldID=177169 
	
	SELECT @Filename = c.OutGoingMandateFileName FROM ClientAccount c WITH ( NOLOCK ) 
	WHERE c.ClientAccountID = @ClientAccountID
		
	/*Replace out the date fields*/ 
	SELECT	 @Filename=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Filename
				,'|DD|',RIGHT('0' + CAST(DATEPART(DD,dbo.fn_GetDate_Local()) AS VARCHAR),2))
				,'|MM|',RIGHT('0' + CAST(DATEPART(MM,dbo.fn_GetDate_Local()) AS VARCHAR),2))
				,'|YYYY|',RIGHT('0' + CAST(DATEPART(YYYY,dbo.fn_GetDate_Local()) AS VARCHAR),4))
				,'|HH|',RIGHT('0' + CAST(DATEPART(HH,dbo.fn_GetDate_Local()) AS VARCHAR),2))
				,'|NN|',RIGHT('0' + CAST(DATEPART(MINUTE,dbo.fn_GetDate_Local()) AS VARCHAR),2))
				,'|SS|',RIGHT('0' + CAST(DATEPART(SS,dbo.fn_GetDate_Local()) AS VARCHAR),2))
		
	SELECT @Filename AS [ExportFileName],
			@LocalFolder AS [ExportFilePath],
			0  AS [OutputHeaderRow],
			@ServerName AS [SFTPServer],
			@Username AS [SFTPUserName],
			@PW AS [SFTPPassword],
			@SFTPFolder AS [SFTPFolder],
			@Cert AS [SFTPCertificate],
			22 AS [SFTPPort]


		-- PART 2 detail

		IF EXISTS ( SELECT * FROM @EPSTable )
		BEGIN
						
			--SELECT @TableRowID = 301323 /*DD Organisation Details By Underwriter*/
			
			EXEC dbo._C600_Collections_DDCreateOutput  @EPSTable , @ClientAccountID	
						
		END
		ELSE
		BEGIN
		
			SELECT ''
		
		END
		
		-- create BACS file entry
		INSERT INTO dbo.BACSFile (ClientID, BACSFileFormatID, [FileName], ExportLocation, NumberOfRowsInFile, TotalCreditValue, TotalDebitValue, TotalValue, ExportDate, WhoCreated, WhenCreated) 
		SELECT @ClientID,@FileFormatID,@Filename,@SFTPFolder,COUNT(*),0,0,0,dbo.fn_GetDate_Local(),@WhoCreated,dbo.fn_GetDate_Local()
		FROM @EPSTable 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_DDRunMandateManagement] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_DDRunMandateManagement] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_DDRunMandateManagement] TO [sp_executeall]
GO
