SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-09-05
-- Description:	Gets the join details for an editing query
-- UPDATE: changed to order by the last table added to try and show a more sensible output 
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingTable__GetJoinDetails]

	@QueryEditingID INT,
	@TableName varchar(250) = '',
	@Action varchar(50) = 'AllTables'
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (@Action = 'AllTables')
	BEGIN
		SELECT rt.ReportTableName as JoinTable, rt.RequiresClientSecurity AS NewTableRequiresSecurity, rt.ShowAquariumValues AS ShowAquariumValues
		FROM ReportTables rt
		ORDER BY JoinTable
	END
	ELSE IF (@Action = 'JoinTables')
	BEGIN
		WITH vTableLimit AS
		(
			SELECT	'vMatterDetailValues' AS ReportTableName,
					MAX(CASE WHEN TableAlias LIKE '%vMatterDetailValues2%' THEN 1 ELSE 0 END) as LIMIT
			FROM	SqlQueryEditingTable WITH (NOLOCK) 
			WHERE	SqlQueryEditingID = @QueryEditingID
			UNION
			SELECT	'vLeadDetailValues' AS ReportTableName,
					MAX(CASE WHEN TableAlias LIKE '%vLeadDetailValues2%' THEN 1 ELSE 0 END) as LIMIT
			FROM	SqlQueryEditingTable WITH (NOLOCK) 
			WHERE	SqlQueryEditingID = @QueryEditingID
		)
		SELECT rt.ReportTableName as JoinTable, rt.RequiresClientSecurity AS NewTableRequiresSecurity, rt.ShowAquariumValues AS ShowAquariumValues
		FROM ReportTables rt
		LEFT JOIN vTableLimit vrt ON rt.ReportTableName = vrt.ReportTableName
		where rt.ReportTableName in
		(
			SELECT rtr.TableTo 
				FROM ReportTables rt 
				INNER JOIN ReportTableRelationships rtr ON rtr.TableFrom = rt.ReportTableName
				INNER JOIN SqlQueryEditingTable t ON 
					t.SqlQueryEditingID = @QueryEditingID AND t.SqlQueryTableName = rt.ReportTableName
			UNION
			SELECT rtr.TableFrom
				FROM ReportTables rt 
				INNER JOIN ReportTableRelationships rtr ON rtr.TableTo = rt.ReportTableName
				INNER JOIN SqlQueryEditingTable t ON 
					t.SqlQueryEditingID = @QueryEditingID AND t.SqlQueryTableName = rt.ReportTableName
		)
		AND (vrt.LIMIT = 0 OR vrt.LIMIT IS NULL)
		ORDER BY rt.ReportTableName
	END
	ELSE IF (@Action = 'JoinConditions')
	BEGIN
		SELECT rtr.ReportTableRelationshipID AS RTRID, 
			CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END AS TableAlias, 
			rtr.TableTo AS JoinTable, rtr.TableJoin AS TableJoin, rtr.Description AS Description, rt.RequiresClientSecurity AS NewTableRequiresSecurity, rt.ShowAquariumValues, t.SqlQueryEditingTableID AS JoinedTableID
			FROM ReportTables rt 
			INNER JOIN ReportTableRelationships rtr ON rtr.TableFrom = rt.ReportTableName
			INNER JOIN SqlQueryEditingTable t ON t.SqlQueryEditingID = @QueryEditingID AND t.SqlQueryTableName = rt.ReportTableName
			WHERE rtr.TableTo = @TableName
		UNION
		SELECT rtr.ReportTableRelationshipID AS RTRID, 
			CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END AS TableAlias,
			rtr.TableFrom AS JoinTable, rtr.TableJoin AS TableJoin, rtr.Description AS Description, rt.RequiresClientSecurity AS NewTableRequiresSecurity, rt.ShowAquariumValues, t.SqlQueryEditingTableID AS JoinedTableID
			FROM ReportTables rt 
			INNER JOIN ReportTableRelationships rtr ON rtr.TableTo = rt.ReportTableName
			INNER JOIN SqlQueryEditingTable t ON t.SqlQueryEditingID = @QueryEditingID AND t.SqlQueryTableName = rt.ReportTableName
			WHERE rtr.TableFrom = @TableName
		ORDER BY JoinedTableID DESC
	END
END








GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetJoinDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable__GetJoinDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetJoinDetails] TO [sp_executeall]
GO
