SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Script table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Script_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@ScriptName varchar (250)  = null ,

	@ScriptDescription varchar (2000)  = null ,

	@WhenCreated datetime   = null ,

	@WhoCreated int   = null ,

	@WhenModified datetime   = null ,

	@WhoModified int   = null ,

	@FinishEventTypeID int   = null ,

	@Published bit   = null ,

	@Disabled bit   = null ,

	@ScriptParentID int   = null ,

	@StartEventTypeID int   = null ,

	@ShowNotes bit   = null ,

	@NumberOfNotesToShow int   = null ,

	@IntegrateWithDialler bit   = null ,

	@ApplyFinishEventTo int   = null ,

	@Redact bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptID]
	, [ClientID]
	, [LeadTypeID]
	, [ScriptName]
	, [ScriptDescription]
	, [WhenCreated]
	, [WhoCreated]
	, [WhenModified]
	, [WhoModified]
	, [FinishEventTypeID]
	, [Published]
	, [Disabled]
	, [ScriptParentID]
	, [StartEventTypeID]
	, [ShowNotes]
	, [NumberOfNotesToShow]
	, [IntegrateWithDialler]
	, [ApplyFinishEventTo]
	, [Redact]
    FROM
	[dbo].[Script] WITH (NOLOCK) 
    WHERE 
	 ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ScriptName] = @ScriptName OR @ScriptName IS NULL)
	AND ([ScriptDescription] = @ScriptDescription OR @ScriptDescription IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([FinishEventTypeID] = @FinishEventTypeID OR @FinishEventTypeID IS NULL)
	AND ([Published] = @Published OR @Published IS NULL)
	AND ([Disabled] = @Disabled OR @Disabled IS NULL)
	AND ([ScriptParentID] = @ScriptParentID OR @ScriptParentID IS NULL)
	AND ([StartEventTypeID] = @StartEventTypeID OR @StartEventTypeID IS NULL)
	AND ([ShowNotes] = @ShowNotes OR @ShowNotes IS NULL)
	AND ([NumberOfNotesToShow] = @NumberOfNotesToShow OR @NumberOfNotesToShow IS NULL)
	AND ([IntegrateWithDialler] = @IntegrateWithDialler OR @IntegrateWithDialler IS NULL)
	AND ([ApplyFinishEventTo] = @ApplyFinishEventTo OR @ApplyFinishEventTo IS NULL)
	AND ([Redact] = @Redact OR @Redact IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptID]
	, [ClientID]
	, [LeadTypeID]
	, [ScriptName]
	, [ScriptDescription]
	, [WhenCreated]
	, [WhoCreated]
	, [WhenModified]
	, [WhoModified]
	, [FinishEventTypeID]
	, [Published]
	, [Disabled]
	, [ScriptParentID]
	, [StartEventTypeID]
	, [ShowNotes]
	, [NumberOfNotesToShow]
	, [IntegrateWithDialler]
	, [ApplyFinishEventTo]
	, [Redact]
    FROM
	[dbo].[Script] WITH (NOLOCK) 
    WHERE 
	 ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ScriptName] = @ScriptName AND @ScriptName is not null)
	OR ([ScriptDescription] = @ScriptDescription AND @ScriptDescription is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([FinishEventTypeID] = @FinishEventTypeID AND @FinishEventTypeID is not null)
	OR ([Published] = @Published AND @Published is not null)
	OR ([Disabled] = @Disabled AND @Disabled is not null)
	OR ([ScriptParentID] = @ScriptParentID AND @ScriptParentID is not null)
	OR ([StartEventTypeID] = @StartEventTypeID AND @StartEventTypeID is not null)
	OR ([ShowNotes] = @ShowNotes AND @ShowNotes is not null)
	OR ([NumberOfNotesToShow] = @NumberOfNotesToShow AND @NumberOfNotesToShow is not null)
	OR ([IntegrateWithDialler] = @IntegrateWithDialler AND @IntegrateWithDialler is not null)
	OR ([ApplyFinishEventTo] = @ApplyFinishEventTo AND @ApplyFinishEventTo is not null)
	OR ([Redact] = @Redact AND @Redact is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Find] TO [sp_executeall]
GO
