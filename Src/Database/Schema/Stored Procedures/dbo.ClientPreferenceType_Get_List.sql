SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientPreferenceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreferenceType_Get_List]

AS


				
				SELECT
					[ClientPreferenceTypeID],
					[PreferenceTypeName]
				FROM
					[dbo].[ClientPreferenceType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreferenceType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Get_List] TO [sp_executeall]
GO
