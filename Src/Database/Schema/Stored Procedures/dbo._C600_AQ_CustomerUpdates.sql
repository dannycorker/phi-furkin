SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
CREATE PROC [dbo].[_C600_AQ_CustomerUpdates] 
(
	@AffinityRLID INT,
	@AgeOfChange INT = 1 -- How many days history of changes to include
)
AS
BEGIN

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	;WITH HistoryCTE AS (
	SELECT 
		cuh.CustomerID
		,cuh.TitleID,cuh.FirstName,cuh.LastName,cuh.Address1,cuh.Address2,cuh.Town,cuh.County,cuh.PostCode
		,cuh.EmailAddress,cuh.DayTimeTelephoneNumber,cuh.HomeTelephone,cuh.MobileTelephone
		,cuh.WhenChanged
	FROM 
		CustomerHistory cuh WITH (NOLOCK)
	)

	,PolicyCTE AS (
	SELECT 
		l.CustomerID, MAX(l.LeadID) LeadID
	FROM 
		Lead l WITH (NOLOCK)
		INNER JOIN LeadDetailValues ldvPolSts WITH (NOLOCK) ON ldvPolSts.LeadID = l.LeadID AND ldvPolSts.DetailFieldID = 146197 AND ldvPolSts.ValueInt = 43002
	GROUP BY 
		l.CustomerID
	)

	SELECT 
		cu.CustomerID, l.LeadRef PolicyNumber, l.LeadID
		,t.Title,cu.FirstName,cu.LastName,cu.Address1,cu.Address2,cu.Town,cu.County,cu.PostCode,cu.EmailAddress,cu.DayTimeTelephoneNumber,cu.HomeTelephone,cu.MobileTelephone
		,cp.UserName,cu.WhenChanged
		,(CASE WHEN cu.TitleID <> h.TitleID THEN 'Title,' ELSE '' END
			+CASE WHEN cu.FirstName <> h.FirstName THEN 'FirstName,' ELSE '' END
			+CASE WHEN cu.LastName <> h.LastName THEN 'LastName,' ELSE '' END
			+CASE WHEN cu.Address1 <> h.Address1 THEN 'Address1,' ELSE '' END
			+CASE WHEN cu.Address2 <> h.Address2 THEN 'Address2,' ELSE '' END
			+CASE WHEN cu.Town <> h.Town THEN 'Town,' ELSE '' END
			+CASE WHEN cu.County <> h.County THEN 'County,' ELSE '' END
			+CASE WHEN cu.PostCode <> h.PostCode THEN 'PostCode,' ELSE '' END
			+CASE WHEN cu.EmailAddress <> h.EmailAddress THEN 'EmailAddress,' ELSE '' END
			+CASE WHEN cu.DayTimeTelephoneNumber <> h.DayTimeTelephoneNumber THEN 'DayTimeTelephoneNumber,' ELSE '' END
			+CASE WHEN cu.HomeTelephone <> h.HomeTelephone THEN 'HomeTelephone,' ELSE '' END
			+CASE WHEN cu.MobileTelephone <> h.MobileTelephone THEN 'MobileTelephone,' ELSE '' END) ChangedFields
		,ISNULL(t2.Title,'')+'|'+ISNULL(h.FirstName,'')+'|'+ISNULL(h.LastName,'')+'|'+ISNULL(h.Address1,'')+'|'+ISNULL(h.Address2,'')+'|'+ISNULL(h.Town,'')+'|'+ISNULL(h.County,'')+'|'+ISNULL(h.PostCode,'')+'|'+ISNULL(h.EmailAddress,'')+'|'+ISNULL(h.DayTimeTelephoneNumber,'')+'|'+ISNULL(h.HomeTelephone,'')+'|'+ISNULL(h.MobileTelephone,'') Previous
	FROM 
		Customers cu WITH (NOLOCK) 
		LEFT JOIN Titles t WITH (NOLOCK) ON cu.TitleID = t.TitleID
		INNER JOIN PolicyCTE p WITH (NOLOCK) ON p.CustomerID = cu.CustomerID
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = p.LeadID
		LEFT JOIN HistoryCTE h ON h.CustomerID = cu.CustomerID and h.WhenChanged < cu.WhenChanged
		LEFT JOIN Titles t2 WITH (NOLOCK) ON t2.TitleID = h.TitleID
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = cu.WhoChanged
		INNER JOIN CustomerDetailValues aff WITH (NOLOCK) ON aff.CustomerID=cu.CustomerID AND aff.DetailFieldID=170144
	WHERE 
		cu.ClientID = @ClientID
		AND cu.test = 0
		AND aff.ValueInt=@AffinityRLID
		AND DATEDIFF(DAY, cu.WhenChanged, dbo.fn_GetDate_Local()) BETWEEN 0 AND @AgeOfChange
		AND NOT EXISTS (
			SELECT 1 FROM HistoryCTE h2 
			WHERE h2.CustomerID = h.CustomerID 
			and h2.WhenChanged < cu.WhenChanged 
			and h2.WhenChanged > h.WhenChanged
			) -- only match the latest history record before the most recent change
	ORDER BY 
		cu.WhenChanged

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_AQ_CustomerUpdates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_AQ_CustomerUpdates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_AQ_CustomerUpdates] TO [sp_executeall]
GO
