SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 28/08/2016
-- Description:	Creates Account Record
-- Mods
-- 2016-11-30 DCM Take remainder up front from billing config table
-- 2016-12-20 DCM Add new PPID to historical policy table
-- 2017-06-02 CPS Populate Number of Installments
-- 2017-07-07 CPS remove adjustment of -1 from EndDate
-- 2018-03-19 JEL Pick up the affinity specific client account ID to write to the customers account record
-- 2019-05-10 PL  Adding Policy Admin Matter ID to the Collection Matter - #55342
-- 2019-11-05 GPR Override payment frequency when Payment Method = Premium Credit, create only 1 installment as opposed to 12
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-02-12 CPS for JIRA AAG-106	| Small code tidy-up wile testing PurchasedProductPaymentScheduleDetail changes
-- 2020-03-18 NG  for AAG-512    | Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- 2020-04-25 GPR | Added LocalTax to assignment of @PremiumTax along with the NationalTax value for AAG-691
-- 2020-11-18 CPS for JIRA PPET-725 | Save the PurchasedProductID back to the WrittenPremium table
-- ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BillingSystem_CreatePurchasedProductAndSchedule]
(
	@MatterID INT,
	@WhoCreated INT
)
AS
BEGIN

	PRINT Object_Name(@@ProcID) + ' @MatterID = ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + ', @WhoCreated = ' + ISNULL(CONVERT(VARCHAR,@WhoCreated),'NULL')
	--declare @matterID int = 379
	SET NOCOUNT ON;

	DECLARE  @AccountID INT
			,@CaseID INT
			,@ClientID INT
			,@ColCaseID INT
			,@CustomerID INT
			,@FirstPaymentDate DATE
			,@PurchaseDate DATE
			,@PaymentFrequencyID INT
			,@PreferredPaymentDay INT
			,@PremiumCalculationDetailID INT
			,@PremiumGross MONEY
			,@PremiumNet MONEY
			,@PremiumTax MONEY
			,@ProductCostBreakdown XML
			,@ProductDescription VARCHAR(250)
			,@ProductName VARCHAR(250)
			,@PurchasedProductID INT
			,@RemainderUpFront INT
			,@RuleSetID INT
			,@TableRowID INT
			,@ValidFrom DATE
			,@ValidTo DATE
			,@AdminFee MONEY
			,@ErrorMessage	VARCHAR(2000)
			,@ClientAccountID INT
			,@CollectionsMatterID INT
			,@WrittenPremiumID INT
			/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee (Shred xml and xave to matter fields)*/
			,@EnrollmentFee NUMERIC(18,2) = 0.00
			,@EnrollmentFeeTax NUMERIC(18,2) = 0.00
			,@EnrollmentFeeTotal NUMERIC(18,2) = 0.00
			,@NextPurchasedProductPaymentScheduleID INT
			,@CurrencyID INT

	CREATE TABLE #InsertedPayments (CustomerPaymentScheduleID INT, PaymentID INT)
	CREATE TABLE #InsertedCustomerLedger (CustomerLedgerID INT, PaymentID INT)

	DECLARE @InsertedPurchasedProductPaymentSchedule TABLE (PurchasedProductPaymentScheduleID INT, CustomerPaymentScheduleID INT)

	SELECT @CaseID=m.CaseID 
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID=@MatterID

	SELECT @ColCaseID = CaseID, @ClientID = ClientID, @CustomerID = CustomerID 
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID = dbo.fn_C600_GetCollectionsMatterID(@MatterID)
	/*!! NO SQL HERE !!*/
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No Collections Case found for MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	IF dbo.fnGetDvAsInt(176973,@ColCaseID) /*Billing System Account ID*/ is NULL
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  AccountID is NULL on Collections CaseID ' + ISNULL(CONVERT(VARCHAR,@ColCaseID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	/* JEL 2018-03-19 Pick up the affinity specific client account ID to write to the customers account record*/ 
	SELECT @ClientAccountID = rldv.ValueInt 
	FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
	INNER JOIN Matter m WITH ( NOLOCK ) on m.CustomerID = cdv.CustomerID 
	INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt
	WHERE m.MatterID = @MatterID 
	AND cdv.DetailFieldID = 170144
	AND rldv.DetailFieldID = 179949
	
	SELECT @CurrencyID = ca.CurrencyID
	FROM ClientAccount ca WITH (NOLOCK) 
	WHERE ca.ClientAccountID = @ClientAccountID

	-- Create purchased product record
	SELECT TOP 1
			@PaymentFrequencyID = CASE dbo.fnGetDvAsInt(170176,m.CaseID)
										WHEN 69943 THEN 4 /*Monthly*/
										WHEN 293321 THEN 7 /*Fortnightly*/
										ELSE 5 /*Annually*/
									END, -- Monthly (4), Annually (5)
			@PurchaseDate				= dbo.fnGetDvAsDate(177418,m.CaseID),
			@ValidFrom					= dbo.fnGetDvAsDate(170036,m.CaseID), /*Policy Start Date*/
			@ValidTo					= dbo.fnGetDvAsDate(170037,m.CaseID), /*Policy End Date*/
			@PreferredPaymentDay		= dbo.fnGetDvAsInt (170168,m.CaseID),
			@FirstPaymentDate			= NULL,
			@RemainderUpFront			= bf.RemainderUpFront,
			@ProductName				= rlpn.DetailValue,
			@ProductDescription			= petname.DetailValue + ' (Policy No: ' + polnum.DetailValue + ')' ,
			@RuleSetID					= dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(m.MatterID, dbo.fnGetSimpleDvAsDate(176925,m.MatterID)),
			@PremiumGross				= wp.AnnualPriceForRiskGross,
			@PremiumTax					= (wp.AnnualPriceForRiskNationalTax + wp.AnnualPriceForRiskLocalTax), /*GPR 2020-04-25 added LocalTax*/
			@PremiumNet					= wp.AnnualPriceForRiskNET,
			@PremiumCalculationDetailID = wp.PremiumCalculationID,
			@AccountID					= dbo.fnGetDvAsInt(176973,@ColCaseID), /*Billing System Account ID*/
			@AdminFee					= mdvAdmin.ValueMoney,
			@WrittenPremiumID			= wp.WrittenPremiumID -- 2020-11-18 CPS for JIRA PPET-725 | We need to update this WrittenPremium record with the ID of the Purchased Product we create later in this procedure.
	FROM Matter m WITH (NOLOCK)
	INNER JOIN MatterDetailValues pn WITH (NOLOCK) ON m.MatterID=pn.MatterID AND pn.DetailFieldID=170034
	INNER JOIN ResourceListDetailValues rlpn WITH (NOLOCK) ON rlpn.ResourceListID=pn.ValueInt AND rlpn.DetailFieldID=146200
	INNER JOIN WrittenPremium wp WITH (NOLOCK) on wp.MatterID = m.MatterID
	INNER JOIN LeadDetailValues petname WITH (NOLOCK) ON m.LeadID=petname.LeadID AND petname.DetailFieldID=144268
	INNER JOIN MatterDetailValues polnum WITH (NOLOCK) ON m.MatterID=polnum.MatterID AND polnum.DetailFieldID=170050 /*Actual Policy Number*/
	INNER JOIN BillingConfiguration bf WITH (NOLOCK) ON bf.ClientID=m.ClientID
	 LEFT JOIN MatterDetailValues mdvAdmin WITH ( NOLOCK ) on mdvAdmin.MatterID = m.MatterID AND mdvAdmin.DetailFieldID = 177470 /*Current Admin Fee*/
	WHERE m.MatterID=@MatterID
	ORDER BY wp.WrittenPremiumID DESC
	/*!! NO SQL HERE !!*/	

	PRINT CONVERT(VARCHAR,@@RowCount) + ' config rows identified.'

	PRINT '@MatterID = ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + ': PolicyNumber = ' + ISNULL(dbo.fnGetSimpleDv(170050,@MatterID),'NULL')
	PRINT '@AccountID = ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL') + ' @CollCaseID = ' + ISNULL(CONVERT(VARCHAR,@ColCaseID),'NULL')
	PRINT '@PremiumGross = ' + ISNULL(CONVERT(VARCHAR,@PremiumGross),'NULL')
	PRINT '@WrittenPremiumID = ' + ISNULL(CONVERT(VARCHAR,@WrittenPremiumID),'NULL')

	IF EXISTS (SELECT *
		FROM CardTransactionPolicy ctp WITH (NOLOCK)
		INNER JOIN [dbo].[CardTransaction] ct WITH (NOLOCK) ON ct.CardTransactionID = ctp.CardTransactionID
		WHERE ctp.PAMatterID = @MatterID
		AND ct.WhenCreated > dbo.fn_GetDate_Local()-30
		AND ct.ErrorCode = 'OK'
		AND (ctp.PolicyPaymentAmount = @PremiumGross OR ctp.PolicyPaymentAmount IS NULL)
	) AND @PaymentFrequencyID = 4
	BEGIN

		SELECT @PaymentFrequencyID = 1

	END

	/*GPR 2019-11-05 override Payment Frequency to create 1 record as opposed to 12 for Premium Credit payers*/
	IF dbo.fnGetDvAsInt(170115,@CaseID) = 76618 /*Payment Method = Premium Credit*/
	BEGIN
		SELECT @PaymentFrequencyID = 5 /*Not Monthly (4) so that only 1 installment is created*/
	END

	INSERT INTO dbo.PurchasedProduct (ClientID, CustomerID, AccountID, PaymentFrequencyID, NumberOfInstallments, ProductPurchasedOnDate, ValidFrom, ValidTo, PreferredPaymentDay, FirstPaymentDate, RemainderUpFront, ProductName, ProductDescription, ProductCostNet, ProductCostVAT, ProductCostGross, ProductCostCalculatedWithRuleSetID, ProductCostCalculatedOn, ProductCostBreakdown, ObjectID, ObjectTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID, PremiumCalculationDetailID, ProductAdditionalFee,ClientAccountID, CurrencyID)
	SELECT @ClientID, 
			@CustomerID, 
			@AccountID, --- must get
			@PaymentFrequencyID, 
			CASE @PaymentFrequencyID
				WHEN 4 THEN 12
				WHEN 7 THEN 26
				ELSE 1
			END, -- NumberOfInstallments (billing system will use its default for this. CPS 2017-06-02 (it appears not to be using its default, so populate this)
			@PurchaseDate,			    -- ProductPurchasedOnDate
			@ValidFrom,					-- = start date
			@ValidTo,					-- = end date - 1 
			@PreferredPaymentDay,		 
			@FirstPaymentDate,			-- use date of next regular payment calculation
			@RemainderUpFront,			-- from client config data 
			@ProductName,
			@ProductDescription,		-- ProductDescription
			@PremiumNet,				-- ProductCostNet
			@PremiumTax,				-- ProductCostVAT 
			@PremiumGross,				-- ProductCostGross
			@RuleSetID,					-- ProductCostCalculatedWithRuleSetID
			dbo.fn_GetDate_Local(),					-- ProductCostCalculatedOn
			@ProductCostBreakdown, 
			@MatterID,					-- ObjectID
			2,							-- ObjectTypeID 
			@WhoCreated,				-- WhoCreated
			dbo.fn_GetDate_Local(), 
			@WhoCreated,				-- WhoModified
			dbo.fn_GetDate_Local(),
			NULL,NULL,NULL,				-- PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID
			@PremiumCalculationDetailID
			,@AdminFee
			,@ClientAccountID
			,@CurrencyID

	SELECT @PurchasedProductID = SCOPE_IDENTITY()

	-- 2020-11-18 CPS for JIRA PPET-725 | Save the PurchasedProduct
	UPDATE wp
	SET PurchasedProductID = @PurchasedProductID
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.WrittenPremiumID = @WrittenPremiumID

	EXEC dbo._C00_SimpleValueIntoField 177074, @PurchasedProductID, @MatterID, @WhoCreated /*Purchased Policy ID*/
	-- and historical policy table	
	-- find the most recent row where PPID is blank
	SELECT @TableRowID=TableRowID 
	FROM TableDetailValues ppid WITH (NOLOCK) 
	WHERE ppid.MatterID=@MatterID 
		AND ppid.DetailFieldID=177419 /*Purchased Policy ID*/
		AND ppid.DetailValue=''
	ORDER BY TableRowID DESC
	
	EXEC dbo._C00_SimpleValueIntoField 177419, @PurchasedProductID, @TableRowID, @WhoCreated /*Purchased Policy ID*/
	EXEC dbo._C00_SimpleValueIntoField 177898, @PremiumGross, @MatterID, @WhoCreated /*Current/New Premium - After Discount*/

	/*Adding Policy Admin Matter ID to the Collection Matter - #55342*/
	SELECT @CollectionsMatterID = a.ObjectID
	FROM Account a WITH ( NOLOCK )
	WHERE a.AccountID = @AccountID

	EXEC dbo._C00_SimpleValueIntoField 180262, @MatterID, @CollectionsMatterID, @WhoCreated

	-- Create purchased product schedule
	EXEC PurchasedProduct__CreatePaymentSchedule @PurchasedProductID	

	/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
	/*Add enrollment fee*/
	SELECT @EnrollmentFee = dbo.fnGetSimpleDvAsMoney(315878, @MatterID)

	/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee (Shred xml and xave to matter fields)*/
	IF @EnrollmentFee > 0
	BEGIN

		SELECT @EnrollmentFeeTax = dbo.fnGetSimpleDvAsMoney(315880, @MatterID),
				@EnrollmentFeeTotal = dbo.fnGetSimpleDvAsMoney(315881, @MatterID)

		/*Get the next row so we have the CPS ID and dates etc*/
		SELECT TOP 1 @NextPurchasedProductPaymentScheduleID = ppps.PurchasedProductPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		ORDER BY ppps.ActualCollectionDate

		/*Add the enrollment fee*/
		INSERT INTO PurchasedProductPaymentSchedule ([ClientID], [CustomerID], [AccountID], [PurchasedProductID], 
			[ActualCollectionDate], [CoverFrom], [CoverTo], [PaymentDate], 
			[PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], 
			[WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], [PurchasedProductPaymentScheduleTypeID], 
			[PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID], [TransactionFee])
		OUTPUT inserted.PurchasedProductPaymentScheduleID, inserted.CustomerPaymentScheduleID INTO @InsertedPurchasedProductPaymentSchedule (PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID)
		SELECT [ClientID], [CustomerID], [AccountID], [PurchasedProductID], 
			[ActualCollectionDate], [CoverFrom], [CoverFrom], [PaymentDate], 
			@EnrollmentFee, @EnrollmentFeeTax, @EnrollmentFeeTotal, [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], 
			[WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], 5 /*Admin Fee*/, 
			[PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID], [TransactionFee]
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductPaymentScheduleID = @NextPurchasedProductPaymentScheduleID

		/*Link it back to itself*/
		UPDATE s
		SET PurchasedProductPaymentScheduleParentID = s.PurchasedProductPaymentScheduleID
		FROM @InsertedPurchasedProductPaymentSchedule i 
		INNER JOIN PurchasedProductPaymentSchedule s ON s.PurchasedProductPaymentScheduleID = i.PurchasedProductPaymentScheduleID

		/*Update the CPS record to include the fee*/
		UPDATE c
		SET PaymentGross = PaymentGross + @EnrollmentFeeTotal,
			PaymentNet = PaymentNet + @EnrollmentFee,
			PaymentVAT = PaymentVAT + ISNULL(@EnrollmentFeeTax, 0.00)
		FROM @InsertedPurchasedProductPaymentSchedule i
		INNER JOIN CustomerPaymentSchedule c ON c.CustomerPaymentScheduleID = i.CustomerPaymentScheduleID

		/*GPR 2021-03-10 for FURKIN-374*/
		UPDATE p
		SET p.ProductAdditionalFee = @EnrollmentFeeTotal
		FROM PurchasedProduct p WITH (NOLOCK)
		WHERE p.PurchasedProductID = @PurchasedProductID


	END
	/*Enrollment fee end*/

	EXEC Account__SetDateAndAmountOfNextPayment @AccountID	
	
	RETURN @PurchasedProductID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BillingSystem_CreatePurchasedProductAndSchedule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BillingSystem_CreatePurchasedProductAndSchedule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BillingSystem_CreatePurchasedProductAndSchedule] TO [sp_executeall]
GO
