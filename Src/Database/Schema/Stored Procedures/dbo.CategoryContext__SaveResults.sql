SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-10-2014
-- Description:	Save Categorisation Results
-- =============================================
CREATE PROCEDURE [dbo].[CategoryContext__SaveResults]
	
	@ClientID INT,
	@LeadTypeID INT,
	@LeadID INT, 
	@MatterID INT,
	@Context VARCHAR(2000),
	@Text VARCHAR(2000),
	@Sentiment VARCHAR(2000),
	@Confidence VARCHAR(2000),
	@Language VARCHAR(2000),
	@MixedSentiment VARCHAR(2000),
	@Category VARCHAR(2000),
	@Group VARCHAR(2000),
	@Keywords VARCHAR(2000),
	@MatchingWords VARCHAR(2000),
	@WordCount VARCHAR(2000)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @DF_Question INT 	
	SELECT @DF_Question = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1332 AND ClientID = @ClientID
   
	DECLARE @DF_Answer INT 	
	SELECT @DF_Answer = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1333 AND ClientID = @ClientID
   
	DECLARE @DF_Sentiment INT 	
	SELECT @DF_Sentiment = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1334 AND ClientID = @ClientID

	DECLARE @DF_Confidence INT 	
	SELECT @DF_Confidence = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1335 AND ClientID = @ClientID

	DECLARE @DF_Language INT 	
	SELECT @DF_Language = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1336 AND ClientID = @ClientID
	
	DECLARE @DF_MixedSentiment INT 	
	SELECT @DF_MixedSentiment = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1337 AND ClientID = @ClientID

	DECLARE @DF_Category INT 	
	SELECT @DF_Category = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1338 AND ClientID = @ClientID
	
	DECLARE @DF_Group INT 	
	SELECT @DF_Group = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1339 AND ClientID = @ClientID
	
	DECLARE @DF_Keywords INT 	
	SELECT @DF_Keywords = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1340 AND ClientID = @ClientID
	
	DECLARE @DF_MatchingWords INT 	
	SELECT @DF_MatchingWords = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1341 AND ClientID = @ClientID
	
	DECLARE @DF_WordCount INT 	
	SELECT @DF_WordCount = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1342 AND ClientID = @ClientID
	
	DECLARE @TableDetailFieldID INT 	
	SELECT @TableDetailFieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=1331 AND ClientID = @ClientID
	
	IF @TableDetailFieldID IS NOT NULL AND @TableDetailFieldID<>0
	BEGIN
	
		DECLARE @PageID INT
		SELECT @PageID=DetailFieldPageID FROM DetailFields WITH (NOLOCK) WHERE DetailFieldID=@TableDetailFieldID
	
		INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, MatterID, LeadID)
		VALUES (@ClientID, @TableDetailFieldID, @PageID,1,1, @MatterID, @LeadID)

		DECLARE @TableRowID INT	
		SELECT @TableRowID = SCOPE_IDENTITY()	
	
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Question, @Context, @ClientID, @LeadID, @MatterID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Answer, @Text, @ClientID, @LeadID, @MatterID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Sentiment, @Sentiment, @ClientID, @LeadID, @MatterID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Confidence, @Confidence, @ClientID, @LeadID, @MatterID)
		
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Language, @Language, @ClientID, @LeadID, @MatterID)
		
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_MixedSentiment, @MixedSentiment, @ClientID, @LeadID, @MatterID)		
		
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Category, @Category, @ClientID, @LeadID, @MatterID)				
		
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Group, @Group, @ClientID, @LeadID, @MatterID)						
		
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_Keywords, @Keywords, @ClientID, @LeadID, @MatterID)								
	
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_MatchingWords, @MatchingWords, @ClientID, @LeadID, @MatterID)								
	
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientID, LeadID, MatterID)		
		VALUES (@TableRowID, @DF_WordCount, @WordCount, @ClientID, @LeadID, @MatterID)								

	END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CategoryContext__SaveResults] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CategoryContext__SaveResults] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CategoryContext__SaveResults] TO [sp_executeall]
GO
