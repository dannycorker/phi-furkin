SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryGrouping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryGrouping_GetBySqlQueryGroupingID]
(

	@SqlQueryGroupingID int   
)
AS


				SELECT
					[SqlQueryGroupingID],
					[ClientID],
					[SqlQueryID],
					[GroupByClause],
					[HavingClause],
					[HavingColumnID],
					[HavingCriteria1],
					[HavingCriteria2],
					[TempHavingColumnID],
					[TempHavingID],
					[TempGroupingID]
				FROM
					[dbo].[SqlQueryGrouping] WITH (NOLOCK) 
				WHERE
										[SqlQueryGroupingID] = @SqlQueryGroupingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_GetBySqlQueryGroupingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryGrouping_GetBySqlQueryGroupingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_GetBySqlQueryGroupingID] TO [sp_executeall]
GO
