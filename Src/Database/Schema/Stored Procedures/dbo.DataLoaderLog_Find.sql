SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderLog table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderLog_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderLogID int   = null ,

	@ClientID int   = null ,

	@DataLoaderMapID int   = null ,

	@DataLoaderMapSectionID int   = null ,

	@DataLoaderFileID int   = null ,

	@DataLoaderFieldDefinitionID int   = null ,

	@DataLoaderObjectFieldID int   = null ,

	@DetailFieldID int   = null ,

	@ObjectName varchar (250)  = null ,

	@RowIndex int   = null ,

	@ColIndex int   = null ,

	@NodeName varchar (2000)  = null ,

	@Message varchar (2000)  = null ,

	@LogLevel int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderLogID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderMapSectionID]
	, [DataLoaderFileID]
	, [DataLoaderFieldDefinitionID]
	, [DataLoaderObjectFieldID]
	, [DetailFieldID]
	, [ObjectName]
	, [RowIndex]
	, [ColIndex]
	, [NodeName]
	, [Message]
	, [LogLevel]
    FROM
	[dbo].[DataLoaderLog] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderLogID] = @DataLoaderLogID OR @DataLoaderLogID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DataLoaderMapID] = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	AND ([DataLoaderMapSectionID] = @DataLoaderMapSectionID OR @DataLoaderMapSectionID IS NULL)
	AND ([DataLoaderFileID] = @DataLoaderFileID OR @DataLoaderFileID IS NULL)
	AND ([DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID OR @DataLoaderFieldDefinitionID IS NULL)
	AND ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID OR @DataLoaderObjectFieldID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([RowIndex] = @RowIndex OR @RowIndex IS NULL)
	AND ([ColIndex] = @ColIndex OR @ColIndex IS NULL)
	AND ([NodeName] = @NodeName OR @NodeName IS NULL)
	AND ([Message] = @Message OR @Message IS NULL)
	AND ([LogLevel] = @LogLevel OR @LogLevel IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderLogID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderMapSectionID]
	, [DataLoaderFileID]
	, [DataLoaderFieldDefinitionID]
	, [DataLoaderObjectFieldID]
	, [DetailFieldID]
	, [ObjectName]
	, [RowIndex]
	, [ColIndex]
	, [NodeName]
	, [Message]
	, [LogLevel]
    FROM
	[dbo].[DataLoaderLog] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderLogID] = @DataLoaderLogID AND @DataLoaderLogID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DataLoaderMapID] = @DataLoaderMapID AND @DataLoaderMapID is not null)
	OR ([DataLoaderMapSectionID] = @DataLoaderMapSectionID AND @DataLoaderMapSectionID is not null)
	OR ([DataLoaderFileID] = @DataLoaderFileID AND @DataLoaderFileID is not null)
	OR ([DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID AND @DataLoaderFieldDefinitionID is not null)
	OR ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID AND @DataLoaderObjectFieldID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([RowIndex] = @RowIndex AND @RowIndex is not null)
	OR ([ColIndex] = @ColIndex AND @ColIndex is not null)
	OR ([NodeName] = @NodeName AND @NodeName is not null)
	OR ([Message] = @Message AND @Message is not null)
	OR ([LogLevel] = @LogLevel AND @LogLevel is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderLog_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Find] TO [sp_executeall]
GO
