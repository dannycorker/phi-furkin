SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIClaimStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_Insert]
(

	@RPIClaimStatusID int    OUTPUT,

	@MatterID int   ,

	@ClientID int   ,

	@ApplicationID varchar (150)  ,

	@ActivityEngineGuid varchar (250)  ,

	@PhaseCacheID varchar (250)  ,

	@PhaseCacheName varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@RPIAsUser varchar (250)  ,

	@RPIUser varchar (250)  ,

	@RPIPass varchar (250)  ,

	@Notes varchar (500)  
)
AS


				
				INSERT INTO [dbo].[RPIClaimStatus]
					(
					[MatterID]
					,[ClientID]
					,[ApplicationID]
					,[ActivityEngineGuid]
					,[PhaseCacheID]
					,[PhaseCacheName]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[RPIAsUser]
					,[RPIUser]
					,[RPIPass]
					,[Notes]
					)
				VALUES
					(
					@MatterID
					,@ClientID
					,@ApplicationID
					,@ActivityEngineGuid
					,@PhaseCacheID
					,@PhaseCacheName
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@RPIAsUser
					,@RPIUser
					,@RPIPass
					,@Notes
					)
				-- Get the identity value
				SET @RPIClaimStatusID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Insert] TO [sp_executeall]
GO
