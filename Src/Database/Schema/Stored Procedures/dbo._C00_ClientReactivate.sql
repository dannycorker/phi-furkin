SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-13
-- Description:	Switch a client back on again
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ClientReactivate]
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Try to unmangle the validity code so that users can log on again */
	IF 'zzz' = (
		SELECT LEFT(cu.Validity, 3) 
		FROM dbo.ClientUsage cu WITH (NOLOCK) 
		WHERE cu.ClientID = @ClientID
	)
	BEGIN
		UPDATE dbo.ClientUsage 
		SET Validity = RIGHT(Validity, LEN(Validity) - 3) 
		WHERE ClientID = @ClientID 
		
		/* 
			Enable batch jobs that we disabled on switch-off
			Use SourceID to show which we switched off as opposed to old, disabled jobs the client no longer wants.
		*/
		UPDATE dbo.AutomatedTask
		SET Enabled = 1, SourceID = NULL
		WHERE ClientID = @ClientID
		AND Enabled = 0
		AND SourceID = TaskID
	END
	ELSE
	BEGIN
		PRINT 'The usage code does not appear to have been deactivated?'
	END
		
	/* Show the results */
	SELECT * 
	FROM dbo.ClientUsage cu WITH (NOLOCK) 
	WHERE cu.ClientID = @ClientID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ClientReactivate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ClientReactivate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ClientReactivate] TO [sp_executeall]
GO
