SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIEventTypeMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventTypeMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIEventTypeMappingID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@RPIEventTypeID int   = null ,

	@EventTypeID int   = null ,

	@IsEnabled bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIEventTypeMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [RPIEventTypeID]
	, [EventTypeID]
	, [IsEnabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[RPIEventTypeMapping] WITH (NOLOCK) 
    WHERE 
	 ([RPIEventTypeMappingID] = @RPIEventTypeMappingID OR @RPIEventTypeMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([RPIEventTypeID] = @RPIEventTypeID OR @RPIEventTypeID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIEventTypeMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [RPIEventTypeID]
	, [EventTypeID]
	, [IsEnabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[RPIEventTypeMapping] WITH (NOLOCK) 
    WHERE 
	 ([RPIEventTypeMappingID] = @RPIEventTypeMappingID AND @RPIEventTypeMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([RPIEventTypeID] = @RPIEventTypeID AND @RPIEventTypeID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventTypeMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Find] TO [sp_executeall]
GO
