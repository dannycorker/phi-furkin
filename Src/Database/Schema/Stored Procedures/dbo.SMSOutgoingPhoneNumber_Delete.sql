SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SMSOutgoingPhoneNumber table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_Delete]
(

	@SMSOutgoingPhoneNumberID int   
)
AS


				DELETE FROM [dbo].[SMSOutgoingPhoneNumber] WITH (ROWLOCK) 
				WHERE
					[SMSOutgoingPhoneNumberID] = @SMSOutgoingPhoneNumberID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Delete] TO [sp_executeall]
GO
