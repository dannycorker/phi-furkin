SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-03-09
-- Description:	Format SqlQuery or SqlQueryHistory record neatly
-- =============================================
CREATE PROCEDURE [dbo].[SSQL]  
	@SqlQueryID int,
	@HistoryVersionNumber int = null
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE	@SqlStr varchar(8000),
	@ClientID int
	
	SELECT @ClientID = sq.ClientID
	FROM dbo.SqlQuery sq WITH (NOLOCK) 
	WHERE sq.QueryID = @SqlQueryID

	IF @HistoryVersionNumber IS NULL
	BEGIN

		SELECT @SqlStr = replace(sq.QueryText,'@ClientID',@ClientID)
		FROM dbo.SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @SqlQueryID 
		
	END
	ELSE
	BEGIN
		IF @HistoryVersionNumber = 0
		BEGIN

			SELECT * 
			FROM dbo.SqlQueryHistory sq WITH (NOLOCK) 
			WHERE sq.QueryID = @SqlQueryID 
			ORDER BY sq.SqlQueryHistoryID ASC
			
		END
		ELSE
		BEGIN
		
			;
			WITH sqh AS 
			(
				SELECT sq.QueryID, sq.SqlQueryHistoryID, row_number() over (order by SqlQueryHistoryID) as rn
				FROM dbo.SqlQueryHistory sq WITH (NOLOCK) 
				WHERE sq.QueryID = @SqlQueryID 
			)
			SELECT @SqlStr = sq.QueryText 
			FROM dbo.SqlQueryHistory sq WITH (NOLOCK) 
			INNER JOIN sqh ON sqh.SqlQueryHistoryID = sq.SqlQueryHistoryID AND sqh.rn = @HistoryVersionNumber
		END
		
	END		
	
	PRINT @SqlStr
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[SSQL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SSQL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SSQL] TO [sp_executeall]
GO
