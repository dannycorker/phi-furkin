SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the NotificationGroup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroup_GetByNotificationGroupID]
(

	@NotificationGroupID int   
)
AS


				SELECT
					[NotificationGroupID],
					[ClientID],
					[NotificationGroupName],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[NotificationGroup] WITH (NOLOCK) 
				WHERE
										[NotificationGroupID] = @NotificationGroupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_GetByNotificationGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroup_GetByNotificationGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_GetByNotificationGroupID] TO [sp_executeall]
GO
