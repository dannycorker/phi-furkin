SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the UploadedFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_Update]
(

	@UploadedFileID int   ,

	@ClientID int   ,

	@FolderID int   ,

	@UploadedFileName varchar (255)  ,

	@UploadedFileTitle varchar (1000)  ,

	@UploadedFileDescription varchar (2000)  ,

	@UploadedFile varbinary (MAX)  ,

	@UploadedFileGUID uniqueidentifier   ,

	@UploadedFileSize bigint    OUTPUT,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[UploadedFile]
				SET
					[ClientID] = @ClientID
					,[FolderID] = @FolderID
					,[UploadedFileName] = @UploadedFileName
					,[UploadedFileTitle] = @UploadedFileTitle
					,[UploadedFileDescription] = @UploadedFileDescription
					,[UploadedFile] = @UploadedFile
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[UploadedFileID] = @UploadedFileID 
				
				
				-- Select computed columns into output parameters
				SELECT
 @UploadedFileSize = [UploadedFileSize]
				FROM
					[dbo].[UploadedFile] WITH (NOLOCK) 
				WHERE
[UploadedFileID] = @UploadedFileID 
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Update] TO [sp_executeall]
GO
