SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DroppedOutCustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerQuestionnaires_Get_List]

AS


				
				SELECT
					[CustomerQuestionnaireID],
					[ClientQuestionnaireID],
					[CustomerID],
					[SubmissionDate],
					[TrackingID],
					[ClientID]
				FROM
					[dbo].[DroppedOutCustomerQuestionnaires] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerQuestionnaires_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Get_List] TO [sp_executeall]
GO
