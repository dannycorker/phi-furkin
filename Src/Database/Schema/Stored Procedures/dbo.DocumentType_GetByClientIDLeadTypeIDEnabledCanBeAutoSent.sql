SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType_GetByClientIDLeadTypeIDEnabledCanBeAutoSent]
(

	@ClientID int   ,

	@LeadTypeID int   ,

	@Enabled bit   ,

	@CanBeAutoSent bit   
)
AS


				SELECT
					[DocumentTypeID],
					[ClientID],
					[LeadTypeID],
					[DocumentTypeName],
					[DocumentTypeDescription],
					[Header],
					[Template],
					[Footer],
					[CanBeAutoSent],
					[EmailSubject],
					[EmailBodyText],
					[InputFormat],
					[OutputFormat],
					[Enabled],
					[RecipientsTo],
					[RecipientsCC],
					[RecipientsBCC],
					[ReadOnlyTo],
					[ReadOnlyCC],
					[ReadOnlyBCC],
					[SendToMultipleRecipients],
					[MultipleRecipientDataSourceType],
					[MultipleRecipientDataSourceID],
					[SendToAllByDefault],
					[ExcelTemplatePath],
					[FromDetails],
					[ReadOnlyFrom],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[FolderID],
					[IsThunderheadTemplate],
					[ThunderheadUniqueTemplateID],
					[ThunderheadDocumentFormat],
					[DocumentTitleTemplate],
					[CreateNewVersionWhenSaved]
				FROM
					[dbo].[DocumentType] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [LeadTypeID] = @LeadTypeID
					AND [Enabled] = @Enabled
					AND [CanBeAutoSent] = @CanBeAutoSent
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetByClientIDLeadTypeIDEnabledCanBeAutoSent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_GetByClientIDLeadTypeIDEnabledCanBeAutoSent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetByClientIDLeadTypeIDEnabledCanBeAutoSent] TO [sp_executeall]
GO
