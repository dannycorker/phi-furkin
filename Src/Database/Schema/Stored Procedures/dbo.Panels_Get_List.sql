SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Panels table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_Get_List]

AS


				
				SELECT
					[PanelID],
					[ClientID],
					[ClientPersonnelID],
					[PanelName]
				FROM
					[dbo].[Panels] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Get_List] TO [sp_executeall]
GO
