SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 11-07-2017
-- Description:	Deletes all the document standard target 
--              records that have the give document type version id
-- =============================================
CREATE PROCEDURE [dbo].[DocumentStandardTarget__DeleteByDocumentTypeVersionID]
(
@DocumentTypeVersionID int
)
AS
	DELETE FROM [dbo].[DocumentStandardTarget] WITH (ROWLOCK)  WHERE [DocumentTypeVersionID] = @DocumentTypeVersionID
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget__DeleteByDocumentTypeVersionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
