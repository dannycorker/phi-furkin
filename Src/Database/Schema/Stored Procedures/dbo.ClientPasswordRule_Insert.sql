SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPasswordRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPasswordRule_Insert]
(

	@ClientPasswordRuleID int    OUTPUT,

	@ClientID int   ,

	@MinLength int   ,

	@MinLowerCase int   ,

	@MinUpperCase int   ,

	@MinNumber int   ,

	@MinSymbol int   ,

	@ExpiryDays int   ,

	@ResetForcesChange bit   ,

	@PasswordHistoryCount int   
)
AS


				
				INSERT INTO [dbo].[ClientPasswordRule]
					(
					[ClientID]
					,[MinLength]
					,[MinLowerCase]
					,[MinUpperCase]
					,[MinNumber]
					,[MinSymbol]
					,[ExpiryDays]
					,[ResetForcesChange]
					,[PasswordHistoryCount]
					)
				VALUES
					(
					@ClientID
					,@MinLength
					,@MinLowerCase
					,@MinUpperCase
					,@MinNumber
					,@MinSymbol
					,@ExpiryDays
					,@ResetForcesChange
					,@PasswordHistoryCount
					)
				-- Get the identity value
				SET @ClientPasswordRuleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPasswordRule_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Insert] TO [sp_executeall]
GO
