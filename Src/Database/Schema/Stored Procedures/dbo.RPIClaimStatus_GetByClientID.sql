SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIClaimStatus table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[RPIClaimStatusID],
					[MatterID],
					[ClientID],
					[ApplicationID],
					[ActivityEngineGuid],
					[PhaseCacheID],
					[PhaseCacheName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[RPIAsUser],
					[RPIUser],
					[RPIPass],
					[Notes]
				FROM
					[dbo].[RPIClaimStatus] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_GetByClientID] TO [sp_executeall]
GO
