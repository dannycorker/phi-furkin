SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-29
-- Description:	Get Mailchimp Campaign Details
-- =============================================
CREATE PROCEDURE [dbo].[MailChimp__GetCampaignResults]
	@ClientPersonnelID INT,
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT CONVERT(varchar(17), mc.SentDate, 113) AS [Sent Date], mc.Title AS [Campaign Name], mc.Status, mc.EmailSent AS [Number Of Email Sent], COUNT(mco.Opened) AS [Read], COUNT(mco.Clicked) AS [Clicked], COUNT(mco.Bounced) AS [Bounced], ISNULL(COUNT(mco.UnsubscribeReason), 0) AS [Unsubscribed], mc.LastCompletePoll
	FROM MailChimpCampaigns mc WITH (NOLOCK)
	LEFT JOIN MailChimpCampaignOutcomes mco WITH (NOLOCK) ON mc.CampaignID = mco.CampainID
	WHERE mc.ClientID = @ClientID
	GROUP BY mc.SentDate, mc.Title, mc.EmailSent, ISNULL(mc.Unsubscribed, 0), mc.Status, mc.LastCompletePoll
	ORDER BY mc.SentDate DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimp__GetCampaignResults] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MailChimp__GetCampaignResults] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimp__GetCampaignResults] TO [sp_executeall]
GO
