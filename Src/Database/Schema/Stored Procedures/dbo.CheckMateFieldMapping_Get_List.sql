SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CheckMateFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_Get_List]

AS


				
				SELECT
					[CheckMateFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[CheckMateFieldID],
					[FieldID],
					[ColumnFieldID],
					[DataLoaderObjectTypeID],
					[CheckMateFieldGroupID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[CheckMateFieldMapping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Get_List] TO [sp_executeall]
GO
