SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType__GetByClientIDEnabled]
(
	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT *
				FROM
					[dbo].[LeadType] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID AND Enabled = 1
				
				SELECT @@ROWCOUNT
				
			





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDEnabled] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType__GetByClientIDEnabled] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDEnabled] TO [sp_executeall]
GO
