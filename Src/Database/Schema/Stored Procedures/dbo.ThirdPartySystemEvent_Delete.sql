SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartySystemEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_Delete]
(

	@ThirdPartySystemEventID int   
)
AS


				DELETE FROM [dbo].[ThirdPartySystemEvent] WITH (ROWLOCK) 
				WHERE
					[ThirdPartySystemEventID] = @ThirdPartySystemEventID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Delete] TO [sp_executeall]
GO
