SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPasswordRule table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPasswordRule_GetByClientPasswordRuleID]
(

	@ClientPasswordRuleID int   
)
AS


				SELECT
					[ClientPasswordRuleID],
					[ClientID],
					[MinLength],
					[MinLowerCase],
					[MinUpperCase],
					[MinNumber],
					[MinSymbol],
					[ExpiryDays],
					[ResetForcesChange],
					[PasswordHistoryCount]
				FROM
					[dbo].[ClientPasswordRule] WITH (NOLOCK) 
				WHERE
										[ClientPasswordRuleID] = @ClientPasswordRuleID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_GetByClientPasswordRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPasswordRule_GetByClientPasswordRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_GetByClientPasswordRuleID] TO [sp_executeall]
GO
