SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-02-09
-- Description:	Select ClientPersonnel details for the User Directory page in the app.
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__GetForUserDirectory] 
	@ClientID int, 
	@ClientPersonnelID int, 
	@LastNameFilter varchar(50) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT cp.ClientPersonnelID, 
	CASE COALESCE(t.Title, '') WHEN 'Unknown' THEN '' WHEN '' THEN '' ELSE t.Title + ' ' END + cp.UserName as [Name], 
	cp.UserName, 
	cp.JobTitle, 
	COALESCE(co.BuildingName, '') as [ClientOffice]
	FROM dbo.ClientPersonnel cp 
	LEFT JOIN dbo.Titles t ON t.TitleID = cp.TitleID 
	LEFT JOIN dbo.ClientOffices co ON co.ClientOfficeID = cp.ClientOfficeID 
	WHERE cp.ClientID = @ClientID 
	AND cp.LastName LIKE @LastNameFilter + '%'
	AND (@ClientID NOT IN (120) OR cp.ClientPersonnelID = @ClientPersonnelID) 
	AND cp.AccountDisabled = 0
END



GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetForUserDirectory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetForUserDirectory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetForUserDirectory] TO [sp_executeall]
GO
