SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ObjectTypeRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_Delete]
(

	@ObjectTypeRelationshipID int   
)
AS


				DELETE FROM [dbo].[ObjectTypeRelationship] WITH (ROWLOCK) 
				WHERE
					[ObjectTypeRelationshipID] = @ObjectTypeRelationshipID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Delete] TO [sp_executeall]
GO
