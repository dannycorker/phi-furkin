SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReferenceValueSectionTypes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueSectionTypes_GetByReferenceValueSectionTypeID]
(

	@ReferenceValueSectionTypeID int   
)
AS


				SELECT
					[ReferenceValueSectionTypeID],
					[SectionTypeName]
				FROM
					[dbo].[ReferenceValueSectionTypes] WITH (NOLOCK) 
				WHERE
										[ReferenceValueSectionTypeID] = @ReferenceValueSectionTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_GetByReferenceValueSectionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueSectionTypes_GetByReferenceValueSectionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_GetByReferenceValueSectionTypeID] TO [sp_executeall]
GO
