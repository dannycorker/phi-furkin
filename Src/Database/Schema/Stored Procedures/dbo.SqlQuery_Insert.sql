SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_Insert]
(

	@QueryID int    OUTPUT,

	@ClientID int   ,

	@QueryText varchar (MAX)  ,

	@QueryTitle varchar (250)  ,

	@AutorunOnline tinyint   ,

	@OnlineLimit int   ,

	@BatchLimit int   ,

	@SqlQueryTypeID int   ,

	@FolderID int   ,

	@IsEditable bit   ,

	@IsTemplate bit   ,

	@IsDeleted bit   ,

	@WhenCreated datetime   ,

	@CreatedBy int   ,

	@OwnedBy int   ,

	@RunCount int   ,

	@LastRundate datetime   ,

	@LastRuntime int   ,

	@LastRowcount int   ,

	@MaxRuntime int   ,

	@MaxRowcount int   ,

	@AvgRuntime int   ,

	@AvgRowcount int   ,

	@Comments varchar (2000)  ,

	@WhenModified datetime   ,

	@ModifiedBy int   ,

	@LeadTypeID int   ,

	@ParentQueryID int   ,

	@IsParent bit   ,

	@SqlQueryTemplateID int   ,

	@OutputFormat varchar (20)  ,

	@ShowInCustomSearch bit   ,

	@OutputFileExtension varchar (50)  ,

	@OutputSeparatorCharmapID tinyint   ,

	@OutputEncapsulatorCharmapID tinyint   ,

	@SuppressHeaderRow bit   ,

	@LockAllTables bit   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[SqlQuery]
					(
					[ClientID]
					,[QueryText]
					,[QueryTitle]
					,[AutorunOnline]
					,[OnlineLimit]
					,[BatchLimit]
					,[SqlQueryTypeID]
					,[FolderID]
					,[IsEditable]
					,[IsTemplate]
					,[IsDeleted]
					,[WhenCreated]
					,[CreatedBy]
					,[OwnedBy]
					,[RunCount]
					,[LastRundate]
					,[LastRuntime]
					,[LastRowcount]
					,[MaxRuntime]
					,[MaxRowcount]
					,[AvgRuntime]
					,[AvgRowcount]
					,[Comments]
					,[WhenModified]
					,[ModifiedBy]
					,[LeadTypeID]
					,[ParentQueryID]
					,[IsParent]
					,[SqlQueryTemplateID]
					,[OutputFormat]
					,[ShowInCustomSearch]
					,[OutputFileExtension]
					,[OutputSeparatorCharmapID]
					,[OutputEncapsulatorCharmapID]
					,[SuppressHeaderRow]
					,[LockAllTables]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@QueryText
					,@QueryTitle
					,@AutorunOnline
					,@OnlineLimit
					,@BatchLimit
					,@SqlQueryTypeID
					,@FolderID
					,@IsEditable
					,@IsTemplate
					,@IsDeleted
					,@WhenCreated
					,@CreatedBy
					,@OwnedBy
					,@RunCount
					,@LastRundate
					,@LastRuntime
					,@LastRowcount
					,@MaxRuntime
					,@MaxRowcount
					,@AvgRuntime
					,@AvgRowcount
					,@Comments
					,@WhenModified
					,@ModifiedBy
					,@LeadTypeID
					,@ParentQueryID
					,@IsParent
					,@SqlQueryTemplateID
					,@OutputFormat
					,@ShowInCustomSearch
					,@OutputFileExtension
					,@OutputSeparatorCharmapID
					,@OutputEncapsulatorCharmapID
					,@SuppressHeaderRow
					,@LockAllTables
					,@SourceID
					)
				-- Get the identity value
				SET @QueryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Insert] TO [sp_executeall]
GO
