SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypePaymentGateway table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_GetByClientPaymentGatewayID]
(

	@ClientPaymentGatewayID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypePaymentGatewayID],
					[ClientID],
					[EventTypeID],
					[ClientPaymentGatewayID],
					[TransactionTypeID],
					[CardMaskingMethodID]
				FROM
					[dbo].[EventTypePaymentGateway] WITH (NOLOCK) 
				WHERE
					[ClientPaymentGatewayID] = @ClientPaymentGatewayID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_GetByClientPaymentGatewayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_GetByClientPaymentGatewayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_GetByClientPaymentGatewayID] TO [sp_executeall]
GO
