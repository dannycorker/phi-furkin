SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Create procedure basic template
-- =============================================

/*
*
* Author: 	Ben Crinion
* Created:	23-April-2007
* Comments:	Get all the enabled equations for a lead type.
* Modified: 06-12-2010 by Paul Richardson
* Added Client ID and Customer Equations 
*/
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields

CREATE PROCEDURE [dbo].[DetailFields__GetEquationsByLeadTypeId] 
	@LeadTypeID int,
	@ClientID int = null
AS
	SELECT
		*
	FROM
		dbo.fnDetailFieldsShared(@ClientID)
	WHERE
		([LeadTypeID] = @LeadTypeID OR [LeadTypeID] = 0)
		AND ([ClientID] = @ClientID OR @ClientID IS NULL)
		AND [QuestionTypeID] = 10
		AND [Enabled] = 1

	Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEquationsByLeadTypeId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetEquationsByLeadTypeId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEquationsByLeadTypeId] TO [sp_executeall]
GO
