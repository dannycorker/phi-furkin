SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-11
-- Description:	check to see if changeset is editable based on id and type passed in
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ChangeSets_IsEditable]
(
	@ClientID INT,
	@ItemTypeID INT,
	@ItemType VARCHAR(20)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	MAX(CASE WHEN CAST(cs.ValidFrom AS VARCHAR) IS NULL AND CAST(cs.ValidTo AS VARCHAR) IS NULL THEN 1 ELSE 0 END) IsEditable
	FROM RulesEngine_ChangeSets cs WITH (NOLOCK) 
	LEFT JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.ChangeSetID = cs.ChangeSetID
	LEFT JOIN RulesEngine_Rules r WITH (NOLOCK) ON r.RuleSetID = rs.RuleSetID 
	LEFT JOIN RulesEngine_RuleParameters p WITH (NOLOCK) ON p.RuleID = r.RuleID
	WHERE cs.ClientID = @ClientID
	AND ((@ItemType = 'ChangeSet' AND cs.ChangeSetID = @ItemTypeID) OR (@ItemType <> 'ChangeSet'))
	AND ((@ItemType = 'RuleSet' AND rs.RuleSetID = @ItemTypeID) OR (@ItemType <> 'RuleSet'))
	AND ((@ItemType = 'Rule' AND r.RuleID = @ItemTypeID) OR (@ItemType <> 'Rule'))
	AND ((@ItemType = 'Parameter' AND p.RuleParameterID = @ItemTypeID) OR (@ItemType <> 'Parameter'))
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_IsEditable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ChangeSets_IsEditable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_IsEditable] TO [sp_executeall]
GO
