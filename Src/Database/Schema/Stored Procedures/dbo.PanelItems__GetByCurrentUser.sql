SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[PanelItems__GetByCurrentUser]
(
	@CreatedBy int,
	@ClientID int
)

AS

SET		ANSI_NULLS OFF

SELECT	PanelItemCharting.PanelItemChartingID,
		PanelItemCharting.PanelItemChartingName,
		Panels.PanelName
FROM		PanelItemCharting
INNER JOIN	Panels ON PanelItemCharting.PanelID = Panels.PanelID
WHERE	PanelItemCharting.CreatedBy = @CreatedBy
AND		PanelItemCharting.ClientID = @ClientID

Select		@@ROWCOUNT

SET		ANSI_NULLS ON




GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems__GetByCurrentUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems__GetByCurrentUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems__GetByCurrentUser] TO [sp_executeall]
GO
