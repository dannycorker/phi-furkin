SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



 --=============================================
 --Author:		James Lewis
 --Create date: 2016-10-05
 --Description:	Saves a new claim to the db
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
 --=============================================
CREATE PROCEDURE [dbo].[_C600_VetEnvoy_SaveNewClaim]

(
	@ClaimID INT,
	@Debug BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE	 @Result			VARCHAR(2000) = ''
			,@ConversationID	VARCHAR(50)
			,@Xml				XML
			,@SenderID			VARCHAR(50)
			,@ClaimsLeadID		INT
			,@LeadEventID		INT
			,@ClientID			INT = dbo.fnGetPrimaryClientID()
		
	SELECT @ConversationID = v.ConversationID, @Xml = CAST(v.xml as XML), @SenderID = v.SenderID
	FROM VetEnvoy_Claims v WITH (NOLOCK) 
	where v.ClaimID = @ClaimID	

	DECLARE @AqAutomation INT = 44412,
			@Date DATE,
			@VarcharDate VARCHAR
			
	SELECT @VarcharDate = Cast(@Date as VARCHAR(18))		

	SELECT @Date = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	
	-- Might need to wrap this in a tran and should probably hand off to _C{@ClientID}_VetEnvoy_SaveNewClaim procs
	
	DECLARE @PolicyRef VARCHAR(2000)
	;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim')
	SELECT @PolicyRef = @Xml.value('(/InsuranceClaim/InfoFromPolicyHolder/PolicyDetails/PolicyNumber)[1]', 'varchar(2000)')
	
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	WHERE l.ClientID = @ClientID
	AND l.LeadRef = @PolicyRef
	--AND c.Test = 0
	
	SELECT @ClaimsLeadID = dbo.fn_C00_1272_GetClaimLeadFromPolicyLead (@LeadID)
	
	IF @Debug = 1
	BEGIN
	
		SELECT @PolicyRef AS PolicyRef, @LeadID AS LeadID, @ClaimsLeadID as ClaimsLeadID
	
	END
	
	IF @LeadID IS NULL
	BEGIN
	
		SELECT @Result += 'Unable to match policy with ref ' + @PolicyRef
	
	END	
	ELSE
	BEGIN

	
		UPDATE dbo.VetEnvoy_Claims
		SET LeadID = @ClaimsLeadID
		WHERE ConversationID  = @ConversationID
		
		DECLARE @MatterIDs TABLE
		(
			ID INT IDENTITY,
			MatterID INT
		)
	
		-- Check for existing matters
		DECLARE @MatterCount INT,
				@MatterID INT,
				@CreateMatter BIT = 0,
				@CaseID INT
				
		SELECT @MatterCount = COUNT(*) 
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE LeadID = @ClaimsLeadID
		
		IF @Debug = 1
		BEGIN
			SELECT @MatterCount AS MatterCount
		END
		
		IF @MatterCount = 1
		BEGIN
			
			SELECT @MatterID = MatterID, @CaseID = CaseID
			FROM dbo.Matter WITH (NOLOCK) 
			WHERE LeadID = @ClaimsLeadID
			
			-- Work out if we need to reuse
			DECLARE @EventCount INT
			SELECT @EventCount = COUNT(*) 
			FROM dbo.LeadEvent WITH (NOLOCK) 
			WHERE CaseID = @CaseID
			
			DECLARE @DescriptionValue VARCHAR(2000)
			SELECT @DescriptionValue = DetailValue
			FROM dbo.MatterDetailValues WITH (NOLOCK) 
			WHERE MatterID = @MatterID
			AND DetailFieldID = 144332
			
			IF @EventCount > 1 OR @DescriptionValue IS NOT NULL
			BEGIN
				SELECT @CreateMatter = 1 
			END
			
		END
		ELSE
		BEGIN
			SELECT @CreateMatter = 1
		END
		
		IF @Debug = 1
		BEGIN
			SELECT @CreateMatter AS CreateMatter
		END
		
		DECLARE @Conditions TABLE
		(
			ID INT IDENTITY,
			ConditionCode VARCHAR(2000),
			Description VARCHAR(2000),
			DateOfLoss VARCHAR(2000),
			TreatmentFrom VARCHAR(2000),
			TreatmentTo VARCHAR(2000),
			InvoiceItems XML,
			TotalExVAT VARCHAR(2000),
			VAT VARCHAR(2000),
			GrandTotal VARCHAR(2000)
		)
		
		
		DECLARE @ClaimItems TABLE
		(
			ID INT,
			Description VARCHAR(2000),
			Type VARCHAR(2000),
			InvoiceNumber VARCHAR(2000),
			Date VARCHAR(2000),
			Quantity VARCHAR(2000),
			AmountExVAT MONEY,
			VAT MONEY,
			Total MONEY
		)
		DECLARE @TableRowsInserted TABLE
		(
			SourceID INT,
			TableRowID INT
		)
		DECLARE @FieldIDs TABLE
		(
			FieldID INT
		)
		INSERT @FieldIDs (FieldID) VALUES
		(162680), -- Policy sections
		(162681), -- Description
		(162682), -- Type
		(162683), -- Invoice No
		(162684), -- Date
		(162685), -- Quantity
		(162686), -- Amount Ex VAT
		(162687), -- VAT
		(162688)  -- Total
		
		DECLARE @ClinicalHistory TABLE
		(
			ID INT IDENTITY,
			Date VARCHAR(2000),
			Time VARCHAR(2000),
			EnteredBy VARCHAR(2000),
			TextEntry VARCHAR(2000)
		)
		DECLARE @HistoryFieldIDs TABLE
		(
			FieldID INT
		)
		INSERT @HistoryFieldIDs (FieldID) VALUES
		(162666), -- Date
		(162667), -- Time
		(162668), -- Entered By
		(162669)  -- Text Entry
		
		
		
		;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim'),
		RawData AS 
		(
			SELECT	node.value('Date[1]', 'VARCHAR(2000)') AS Date,
					node.value('Time[1]', 'VARCHAR(2000)') AS Time,
					node.value('EnteredBy[1]', 'VARCHAR(2000)') AS EnteredBy,
					node.value('TextEntry[1]', 'VARCHAR(2000)') AS TextEntry
			FROM @Xml.nodes('//InfoFromVet/AnimalClinicalHistory/Entry') AS received (node)
		)
		
		INSERT INTO @ClinicalHistory (Date, Time, EnteredBy, TextEntry)
		SELECT *
		FROM RawData
		
		IF @Debug = 1
		BEGIN
		
			SELECT *
			FROM @ClinicalHistory
		
		END
		

		;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim'),
		RawData AS 
		(
			SELECT	node.value('ConditionCode[1]', 'VARCHAR(2000)') AS ConditionCode,
					node.value('DiagnosisOrSigns[1]', 'VARCHAR(2000)') AS Description,
					node.value('Started[1]', 'VARCHAR(2000)') AS DateOfLoss,
					node.value('(TreatmentDates/DateFrom)[1]', 'VARCHAR(2000)') AS TreatmentFrom,
					node.value('(TreatmentDates/DateTo)[1]', 'VARCHAR(2000)') AS TreatmentTo,
					node.query('Financial/InvoiceItems')  AS InvoiceItems,
					node.value('(Financial/TotalExVAT)[1]', 'VARCHAR(2000)') AS TotalExVAT,
					node.value('(Financial/VAT)[1]', 'VARCHAR(2000)') AS VAT,
					node.value('(Financial/TotalIncVat)[1]', 'VARCHAR(2000)') AS GrandTotal		
			FROM @Xml.nodes('//InfoFromVet/Conditions/Condition') AS received (node)
		)
		
		INSERT INTO @Conditions (ConditionCode, Description, DateOfLoss, TreatmentFrom, TreatmentTo, InvoiceItems, TotalExVAT, VAT, GrandTotal)
		SELECT *
		FROM RawData
		
		IF @Debug = 1
		BEGIN
		
			SELECT *
			FROM @Conditions
		
		END
		
		
		DECLARE @Count INT,
				@Index INT,
				@ConditionCode VARCHAR(2000),
				@Description VARCHAR(2000),
				@DateOfLoss VARCHAR(2000),
				@TreatmentFrom VARCHAR(2000),
				@TreatmentTo VARCHAR(2000),
				@InvoiceItems XML,
				@TotalExVAT VARCHAR(2000),
				@VAT VARCHAR(2000),
				@GrandTotal VARCHAR(2000)
				
		SELECT @Count =  COUNT(*), @Index = 0
		FROM @Conditions
		
		WHILE @Index < @Count
		BEGIN
			
			-- Clear out the claim items
			DELETE @ClaimItems
			DELETE @TableRowsInserted
			
			
			SELECT @Index = @Index + 1
			
			SELECT	@ConditionCode = ConditionCode,
					@Description = Description,
					@DateOfLoss = DateOfLoss,
					@TreatmentFrom = TreatmentFrom,
					@TreatmentTo = TreatmentTo,
					@InvoiceItems = InvoiceItems,
					@TotalExVAT = TotalExVAT,
					@VAT = VAT,
					@GrandTotal = GrandTotal
			FROM @Conditions
			WHERE ID = @Index
			
			IF @Debug = 0
			BEGIN
				IF @CreateMatter = 1
				BEGIN
				
					EXEC @MatterID = dbo._C00_CreateNewCaseForLead @ClaimsLeadID, @AqAutomation
					SELECT @CaseID = CaseID
					FROM dbo.Matter WITH (NOLOCK) 
					WHERE MatterID = @MatterID
					
					EXEC dbo._C00_AddProcessStart @CaseID, @AqAutomation, 150002
						
				END
				
				INSERT @MatterIDs (MatterID) VALUES (@MatterID)
				
			END
			
			SELECT @CreateMatter = 1
			
			-- Set the basic details
			IF @Debug = 0
			BEGIN
				-- Apply the process start event
				SELECT @LeadEventID = c.LatestInProcessLeadEventID FROM Cases c WITH (NOLOCK) where c.CaseID = @CaseID 					
				
				EXEC _C00_ApplyLeadEventByAutomatedEventQueue  @LeadEventID,156769,@AqAutomation,0
				 
				-- Save VE Conversation id
				EXEC dbo._C00_SimpleValueIntoField 162679, @ConversationID, @MatterID
				-- Claim papers received
				EXEC _C00_SimpleValueIntoField 144520, @Date, @MatterID
				-- Claim details entered
				EXEC _C00_SimpleValueIntoField 153017, @Date, @MatterID
				EXEC _C00_SimpleValueIntoField 144892, @DateOfLoss, @MatterID
				EXEC _C00_SimpleValueIntoField 144332, @Description, @MatterID
				EXEC _C00_SimpleValueIntoField 144483, '44356', @MatterID -- Claim type New	
				EXEC _C00_SimpleValueIntoField 144366, @TreatmentFrom, @MatterID
				EXEC _C00_SimpleValueIntoField 145674, @TreatmentTo, @MatterID
				EXEC _C00_SimpleValueIntoField 149850, @GrandTotal, @MatterID

			END
			
			;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim'),
			RawData AS 
			(
				SELECT	ROW_NUMBER() OVER(ORDER BY node) as rn,
						node.value('Description[1]', 'VARCHAR(2000)') AS Description,
						node.value('Type[1]', 'VARCHAR(2000)') AS Type,
						node.value('InvoiceNumber[1]', 'VARCHAR(2000)') AS InvoiceNumber,
						node.value('Date[1]', 'VARCHAR(2000)') AS Date,
						node.value('Quantity[1]', 'VARCHAR(2000)') AS Quantity	,
						node.value('AmountExVAT[1]', 'MONEY') AS AmountExVAT,
						node.value('VAT[1]', 'MONEY') AS VAT
				FROM @InvoiceItems.nodes('//Item') AS received (node)
			)
			
			INSERT INTO @ClaimItems (ID, Description, Type, InvoiceNumber, Date, Quantity, AmountExVAT, VAT, Total)
			SELECT rn, Description, Type, InvoiceNumber, Date, Quantity, AmountExVAT, ISNULL(VAT, AmountExVAT * 0.2), AmountExVAT + ISNULL(VAT, AmountExVAT * 0.2)
			FROM RawData
			
			IF @Debug = 1
			BEGIN
			
				SELECT *
				FROM @ClaimItems
			
			END
			ELSE
			BEGIN
			
				INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
				OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowsInserted
				SELECT @ClientID, @MatterID, 162689, 16157, ID
				FROM @ClaimItems


				
				INSERT dbo.TableDetailValues (ClientID, DetailFieldID, LeadID, MatterID, TableRowID, DetailValue, ResourceListID)
				SELECT @ClientID, f.FieldID, @ClaimsLeadID, @MatterID, t.TableRowID, 
					CASE
						WHEN f.FieldID = 162680 THEN NULL				-- Policy sections
						WHEN f.FieldID = 162681 THEN c.Description		-- Description 
						WHEN f.FieldID = 162682 THEN c.Type				-- Type
						WHEN f.FieldID = 162683 THEN c.InvoiceNumber	-- Invoice No
						WHEN f.FieldID = 162684 THEN c.Date				-- Date
						WHEN f.FieldID = 162685 THEN c.Quantity			-- Quantity
						WHEN f.FieldID = 162686 THEN CAST(ISNULL(c.AmountExVAT, 0) AS VARCHAR)	-- Amount Ex VAT
						WHEN f.FieldID = 162687 THEN CAST(ISNULL(c.VAT, 0) AS VARCHAR)			-- VAT
						WHEN f.FieldID = 162688 THEN CAST(ISNULL(c.AmountExVAT, 0) + ISNULL(c.VAT, 0) AS VARCHAR)	-- Total
					END AS Value,
					CASE
						WHEN f.FieldID = 162680 THEN '75577'
						ELSE NULL
					END AS RLID	
				FROM @ClaimItems c
				INNER JOIN @TableRowsInserted t ON c.ID = t.SourceID
				CROSS JOIN @FieldIDs f
				
				DELETE @TableRowsInserted
				
				INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
				OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowsInserted
				SELECT @ClientID, @MatterID, 162665, 16188, ID
				FROM @ClinicalHistory
								
				INSERT dbo.TableDetailValues (ClientID, DetailFieldID, LeadID, MatterID, TableRowID, DetailValue)
				SELECT @ClientID, f.FieldID, @ClaimsLeadID, @MatterID, t.TableRowID, 
					CASE
						WHEN f.FieldID = 162666 THEN c.Date
						WHEN f.FieldID = 162667 THEN c.Time
						WHEN f.FieldID = 162668 THEN c.EnteredBy
						WHEN f.FieldID = 162669 THEN c.TextEntry
					END AS Value
				FROM @ClinicalHistory c
				INNER JOIN @TableRowsInserted t ON c.ID = t.SourceID
				CROSS JOIN @HistoryFieldIDs f
							
			END
			
			-- We now insert a comma separated list of matter IDs back to the table
			DECLARE @MatterIDsConcat VARCHAR(2000) = NULL
			SELECT @MatterIDsConcat =	STUFF((SELECT ',' + CAST(MatterID AS VARCHAR(MAX))
										FROM @MatterIDs
										FOR XML PATH(''), TYPE).value('(./text())[1]', 'VARCHAR(MAX)'), 1, 1, '')
										
			UPDATE dbo.VetEnvoy_Claims
			SET MatterIDs = @MatterIDsConcat
			WHERE ConversationID = @ConversationID
			


		END
		
		
	END
	
   -- Removed this as don't have similar fields in the non client specific fields
	--UPDATE dbo.VetEnvoy_Claims
	--SET Result = @Result,
	--	Processed = dbo.fn_GetDate_Local()
	--WHERE ID = @ID
	
	SELECT @Result = @Result + ' ' + @VarcharDate
	
	EXEC _C00_LogIt  'Proc','_C600_VetEnvoy_SaveNewClaim','_C600_VetEnvoy_SaveNewClaim' ,@Result,@AqAutomation 
	
	
	SELECT @Result AS Result

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetEnvoy_SaveNewClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_VetEnvoy_SaveNewClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetEnvoy_SaveNewClaim] TO [sp_executeall]
GO
