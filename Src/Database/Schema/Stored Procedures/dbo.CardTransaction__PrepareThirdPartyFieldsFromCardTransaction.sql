SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-10-21
-- Description:	Populate payments card transaction fields for a Policy Admin or Collections matter
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__PrepareThirdPartyFieldsFromCardTransaction]
	 @CardTransactionID	INT
	,@RunAsUserID		INT
AS
BEGIN
	PRINT OBJECT_NAME(@@ProcID)

	SET NOCOUNT ON;

	DECLARE  @ClientID			INT
			,@LeadID			INT
			,@LeadTypeID		INT
			,@RowCount			INT
			,@ErrorMessage		VARCHAR(2000)
			,@Description		VARCHAR(2000)
			,@Amount			VARCHAR(2000)
			,@FirstName			VARCHAR(2000)
			,@LastName			VARCHAR(2000)
			,@Address			VARCHAR(2000)
			,@Address2			VARCHAR(2000)
			,@City				VARCHAR(2000)
			,@StateProvince		VARCHAR(2000)
			,@Country			VARCHAR(2000)
			,@ZipPostal			VARCHAR(2000)
			,@CustomerID		INT
	DECLARE	 @MattersToUpdate	TABLE ( MatterID INT, LeadTypeID INT )
			
	DECLARE  @DetailValueData	dbo.tvpDetailValueData
			,@BlackHole			dbo.tvpIntInt	-- Used to collect (and ignore) the output of DetailValues__BulkSave
			,@ThirdPartyFields	dbo.tvpInt

	SELECT	 @Description	= ct.Description
			,@Amount		= ct.Amount
			,@FirstName		= ct.FirstName
			,@LastName		= ct.LastName
			,@Address		= ct.Address
			,@Address2		= ct.Address2
			,@City			= ct.City
			,@StateProvince	= ct.StateProvince
			,@Country		= ct.Country
			,@ZipPostal		= ct.ZipPostal
			,@CustomerID	= ct.CustomerID
	FROM CardTransaction ct WITH (NOLOCK) 
	WHERE ct.CardTransactionID = @CardTransactionID
	/*!! NO SQL HERE !!*/
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No CardTransaction matched for CardTransactionID ' + ISNULL(CONVERT(VARCHAR,@CardTransactionID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	INSERT @MattersToUpdate ( MatterID, LeadTypeID )
	SELECT m.MatterID, l.LeadTypeID
	FROM dbo.CardTransaction ct WITH (NOLOCK) 
	INNER JOIN CardTransactionPolicy ctp WITH (NOLOCK) ON ctp.CardTransactionID = ct.CardTransactionID
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = ctp.PAMatterID
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.MatterID
	WHERE ct.CardTransactionID = @CardTransactionID

	/*Find the relevant ThirdPartyFields*/
	INSERT @ThirdPartyFields ( AnyID )
	SELECT pf.ThirdPartyFieldID
	FROM ThirdPartyField pf WITH ( NOLOCK )
	WHERE pf.ThirdPartyFieldID IN (4459,4460,4462,4463,4472,4473,4474,4475,4476,4477)

	/*Compile a tvp of detail value data to save.  If the incoming variable is NULL, use the existing value*/
	INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
	SELECT @ClientID, fm.DetailFieldID, 2, ISNULL(mdv.MatterDetailValueID,-1), mtu.MatterID,
		CASE fm.ThirdPartyFieldID 
			WHEN 4459 THEN ISNULL(@Description	,'')
			WHEN 4460 THEN ISNULL(@Amount		,'')
			WHEN 4462 THEN ISNULL(@FirstName	,'')
			WHEN 4463 THEN ISNULL(@LastName		,'')
			WHEN 4472 THEN ISNULL(@Address		,'')
			WHEN 4473 THEN ISNULL(@Address2		,'')
			WHEN 4474 THEN ISNULL(@City			,'')
			WHEN 4475 THEN ISNULL(@StateProvince,'')
			WHEN 4476 THEN ISNULL(@Country		,'')
			WHEN 4477 THEN ISNULL(@ZipPostal	,'')
			ELSE ''
		END 
	FROM ThirdPartyFieldMapping fm WITH ( NOLOCK )
	INNER JOIN @ThirdPartyFields tpf on tpf.AnyID = fm.ThirdPartyFieldID
	INNER JOIN @MattersToUpdate mtu on mtu.LeadTypeID = fm.LeadTypeID
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = mtu.MatterID AND mdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.LeadTypeID = @LeadTypeID		
		UNION ALL
	SELECT @ClientID, df.DetailFieldID, 2, ISNULL(mdv.MatterDetailValueID,-1), mtu.MatterID, CONVERT(VARCHAR,@CardTransactionID)
	FROM @MattersToUpdate mtu 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.LeadTypeID = mtu.LeadTypeID
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = mtu.MatterID AND mdv.DetailFieldID = df.DetailFieldID
	WHERE df.DetailFieldID = 180010 /*Transaction to Retry*/
	/*!! NO SQL HERE !!*/
	SELECT @RowCount = @@ROWCOUNT
	
	/*Save the detial values, storing history*/
	IF @RowCount > 0
	BEGIN
		PRINT CONVERT(VARCHAR,@RowCount) + ' fields updated for CustomerID ' + ISNULL(CONVERT(VARCHAR,@CustomerID),'NULL')
		INSERT @BlackHole ( ID1, ID2 ) -- Capture the output from BulkSave and do nothing with it, just to prevent this SP from returning unnecessary data
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID
	END

	RETURN @RowCount

END



GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__PrepareThirdPartyFieldsFromCardTransaction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__PrepareThirdPartyFieldsFromCardTransaction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__PrepareThirdPartyFieldsFromCardTransaction] TO [sp_executeall]
GO
