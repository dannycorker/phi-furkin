SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DetailFields table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_Update]
(

	@DetailFieldID int   ,

	@ClientID int   ,

	@LeadOrMatter tinyint   ,

	@FieldName varchar (50)  ,

	@FieldCaption varchar (100)  ,

	@QuestionTypeID int   ,

	@Required bit   ,

	@Lookup bit   ,

	@LookupListID int   ,

	@LeadTypeID int   ,

	@Enabled bit   ,

	@DetailFieldPageID int   ,

	@FieldOrder int   ,

	@MaintainHistory bit   ,

	@EquationText varchar (2000)  ,

	@MasterQuestionID int   ,

	@FieldSize int   ,

	@LinkedDetailFieldID int   ,

	@ValidationCriteriaFieldTypeID int   ,

	@ValidationCriteriaID int   ,

	@MinimumValue varchar (50)  ,

	@MaximumValue varchar (50)  ,

	@RegEx varchar (2000)  ,

	@ErrorMessage varchar (250)  ,

	@ResourceListDetailFieldPageID int   ,

	@TableDetailFieldPageID int   ,

	@DefaultFilter varchar (250)  ,

	@ColumnEquationText varchar (2000)  ,

	@Editable bit   ,

	@Hidden bit   ,

	@LastReferenceInteger int   ,

	@ReferenceValueFormatID int   ,

	@Encrypt bit   ,

	@ShowCharacters int   ,

	@NumberOfCharactersToShow int   ,

	@TableEditMode int   ,

	@DisplayInTableView bit   ,

	@ObjectTypeID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@DetailFieldStyleID int   ,

	@Hyperlink nvarchar (2000)  ,

	@IsShared bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DetailFields]
				SET
					[ClientID] = @ClientID
					,[LeadOrMatter] = @LeadOrMatter
					,[FieldName] = @FieldName
					,[FieldCaption] = @FieldCaption
					,[QuestionTypeID] = @QuestionTypeID
					,[Required] = @Required
					,[Lookup] = @Lookup
					,[LookupListID] = @LookupListID
					,[LeadTypeID] = @LeadTypeID
					,[Enabled] = @Enabled
					,[DetailFieldPageID] = @DetailFieldPageID
					,[FieldOrder] = @FieldOrder
					,[MaintainHistory] = @MaintainHistory
					,[EquationText] = @EquationText
					,[MasterQuestionID] = @MasterQuestionID
					,[FieldSize] = @FieldSize
					,[LinkedDetailFieldID] = @LinkedDetailFieldID
					,[ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID
					,[ValidationCriteriaID] = @ValidationCriteriaID
					,[MinimumValue] = @MinimumValue
					,[MaximumValue] = @MaximumValue
					,[RegEx] = @RegEx
					,[ErrorMessage] = @ErrorMessage
					,[ResourceListDetailFieldPageID] = @ResourceListDetailFieldPageID
					,[TableDetailFieldPageID] = @TableDetailFieldPageID
					,[DefaultFilter] = @DefaultFilter
					,[ColumnEquationText] = @ColumnEquationText
					,[Editable] = @Editable
					,[Hidden] = @Hidden
					,[LastReferenceInteger] = @LastReferenceInteger
					,[ReferenceValueFormatID] = @ReferenceValueFormatID
					,[Encrypt] = @Encrypt
					,[ShowCharacters] = @ShowCharacters
					,[NumberOfCharactersToShow] = @NumberOfCharactersToShow
					,[TableEditMode] = @TableEditMode
					,[DisplayInTableView] = @DisplayInTableView
					,[ObjectTypeID] = @ObjectTypeID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[DetailFieldStyleID] = @DetailFieldStyleID
					,[Hyperlink] = @Hyperlink
					,[IsShared] = @IsShared
				WHERE
[DetailFieldID] = @DetailFieldID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Update] TO [sp_executeall]
GO
