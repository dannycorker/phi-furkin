SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SearchQueryValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_Get_List]

AS


				
				SELECT
					[SearchQueryValueID],
					[ClientID],
					[SubClientID],
					[SearchQueryID],
					[SearchFieldID],
					[SearchFieldOperatorID],
					[FieldValue],
					[FieldValueID]
				FROM
					[dbo].[SearchQueryValue] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Get_List] TO [sp_executeall]
GO
