SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/11/2019
-- Description:	Inserts/Updates cancel agreement request/response
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderCancelAgreementRequestAndResponse__Insert]

	@PremiumFunderCancelAgreementRequestID INT = 0, /*Cancel Agreement Request*/
	@PremiumFunderCancelAgreementResponseID INT = 0, /*Cancel Agreement Response*/
	@PremiumFunderRequestID INT = 0, /*Quote Request, links the Quote Request/Response with the Cancel Agreement Request/Response*/
	@ClientID INT,
	@CustomerID INT = 0,
	@MatterID INT = NULL,	
	@RequestJson NVARCHAR(MAX),
	@ResponseJson NVARCHAR(MAX),
	@WhenCreated DATETIME,
    @WhoCreated INT	
AS
BEGIN	
	
	SET NOCOUNT ON;

	IF @PremiumFunderCancelAgreementRequestID > 0 -- update
	BEGIN

		UPDATE PremiumFunderCancelAgreementRequest
		SET RequestJson = @RequestJson
		WHERE PremiumFunderCancelAgreementRequestID  = @PremiumFunderCancelAgreementRequestID

	END
	ELSE -- insert
	BEGIN

		INSERT INTO PremiumFunderCancelAgreementRequest([ClientID], [CustomerID], [MatterID], [RequestJson], [PremiumFunderRequestID], [WhenCreated], [WhoCreated])
		VALUES (@ClientID, @CustomerID, NULL, @RequestJson, @PremiumFunderRequestID, @WhenCreated, @WhoCreated)

		SELECT @PremiumFunderCancelAgreementRequestID = SCOPE_IDENTITY()

	END

	IF @PremiumFunderCancelAgreementResponseID > 0 -- update
	BEGIN

		UPDATE PremiumFunderCancelAgreementResponse
		SET ResponseJson = @ResponseJson
		WHERE PremiumFunderCancelAgreementResponseID  = @PremiumFunderCancelAgreementResponseID

	END
	ELSE
	BEGIN

		INSERT INTO PremiumFunderCancelAgreementResponse ([PremiumFunderCancelAgreementRequestID], [ClientID], [CustomerID], [MatterID], [ResponseJson], [PremiumFunderRequestID], [WhenCreated], [WhoCreated])
		VALUES (@PremiumFunderCancelAgreementRequestID, @ClientID, @CustomerID, @MatterID, @ResponseJson, @PremiumFunderRequestID, @WhenCreated, @WhoCreated)

		SELECT @PremiumFunderCancelAgreementResponseID = SCOPE_IDENTITY()

	END

    SELECT @PremiumFunderCancelAgreementRequestID PremiumFunderCancelAgreementRequestID, @PremiumFunderCancelAgreementResponseID PremiumFunderCancelAgreementResponseID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderCancelAgreementRequestAndResponse__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderCancelAgreementRequestAndResponse__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderCancelAgreementRequestAndResponse__Insert] TO [sp_executeall]
GO
