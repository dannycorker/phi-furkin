SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:        Cathal Sherry
-- Create date: 2014-09-22
-- Description:   Update Set Costs override table for ISI clients.
-- TODO:          Third Party Map
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISI_UpdateSetCostsOverrides]
(
      @CustomerID INT
)

AS
BEGIN
      SET NOCOUNT ON

      /*
      Pre-populate total set costs override page 
      */
            DECLARE  @ClientID INT,
						@CaseID INT
            
            SELECT TOP 1 @ClientID = cu.ClientID, @CaseID=ca.CaseID
            FROM Customers cu WITH ( NOLOCK )
            INNER JOIN Lead l WITH (NOLOCK) ON cu.CustomerID=l.CustomerID
            INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
            WHERE cu.CustomerID = @CustomerID
            
            DECLARE  @TableDetailFieldID        INT = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1395)
						,@TableDetailFieldID1	INT = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 4204)
                        ,@ColumnDetailFieldID         INT = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1393)
                        ,@DefaultValueDetailFieldID INT = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1394)
                        ,@AmountToUseDetailFieldID    INT = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1396)
                        ,@OverrideDetailFieldID       INT = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1406)
                                                
            DECLARE     @TypeFieldMappingID INT = ( SELECT TOP 1 fm.ThirdPartyFieldMappingID
                                                     FROM ThirdPartyFieldMapping fm WITH ( NOLOCK ) 
                                                     WHERE fm.ClientID = @ClientID
                                                     AND fm.ThirdPartyFieldGroupID = 82
                                                     AND fm.DetailFieldID = @ColumnDetailFieldID )

            DECLARE  @FoodDetailFieldID INT                                   = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1405)
                        ,@FoodLookupListItemID INT                            = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Food' )
                        ,@ClothingDetailFieldID INT                           = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1378)
                        ,@ClothingLookupListItemID INT                        = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Clothing' )
                        ,@PersonalDetailFieldID INT                           = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1379)
                        ,@PersonalLookupListItemID INT                        = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Personal Care' )
                        ,@HealthDetailFieldID INT                             = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1380)
                        ,@HealthLookupListItemID INT                    = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Health' )
                        ,@CommsDetailFieldID INT                              = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1383)
                        ,@CommsLookupListItemID INT                           = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Communications' )
                        ,@SocialDetailFieldID INT                             = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1384)
                        ,@SocialLookupListItemID INT                    = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Social Inclusion & Participation' )
                        ,@EducationDetailFieldID INT                    = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1385)
                        ,@EducationLookupListItemID INT                       = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Education' )
                        ,@TravelDetailFieldID INT                             = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1392)
                        ,@TravelLookupListItemID INT                    = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Travel (Public/Transport)' )
                        ,@CarDetailFieldID INT                                = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1390) 
                        ,@CarLookupListItemID INT                             = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Car Insurance' )
                        ,@SavingsDetailFieldID INT                            = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1391)
                        ,@SavingsLookupListItemID INT                   = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Savings & Contingencies' )
                        ,@HouseholdGoodsDetailFieldID INT               = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1381)
                        ,@HouseholdGoodsLookupListItemID INT            = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Household Goods' )
                        ,@HouseholdServicesDetailFieldID INT            = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1382)
                        ,@HouseholdServicesLookupListItemID INT         = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Household Services' )
                        ,@HomeHeatingDetailFieldID INT                        = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1387)
                        ,@HomeHeatingLookupListItemID INT               = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Home Heating' )
                        ,@PersonalCostsDetailFieldID INT                = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1388)
                        ,@PersonalCostsLookupListItemID INT             = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Personal Costs' )
                        ,@HomeInsuranceDetailFieldID INT                = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1389)
                        ,@HomeInsuranceLookupListItemID INT             = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Home Insurance' )
                        ,@HouseholdElectricityDetailFieldID INT         = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 1386)
                        ,@HouseholdElectricityLookupListItemID INT      = (SELECT vk.LookupListItemID FROM ThirdPartyFieldMappingValueKey vk WITH ( NOLOCK ) WHERE vk.FieldMappingID = @TypeFieldMappingID AND vk.ThirdPartyKey = 'Household Electricity' )

            EXEC _C00_PrePopulateTableFieldFromLookupList @TableDetailFieldID, @ColumnDetailFieldID, @CustomerID   /*New Expenditure Table -> Type*/
            EXEC _C00_PrePopulateTableFieldFromLookupList @TableDetailFieldID1, @ColumnDetailFieldID, @CustomerID   /*New Expenditure Table -> Type*/

            UPDATE tr
            SET DenyDelete = 1
            FROM TableRows tr WITH ( NOLOCK )
            WHERE tr.DetailFieldID = @TableDetailFieldID /*New Expenditure Table*/
            AND tr.CustomerID = @CustomerID
            
            /*Populate all of the rows*/
            INSERT TableDetailValues ( ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue )
            SELECT tr.ClientID, tr.CustomerID, tr.TableRowID, df2.DetailFieldID, ''
            FROM TableRows tr WITH ( NOLOCK )
            INNER JOIN DetailFields df WITH ( NOLOCK ) on df.DetailFieldID = tr.DetailFieldID
            INNER JOIN DetailFields df2 WITH ( NOLOCK ) on df2.DetailFieldPageID = df.TableDetailFieldPageID
            WHERE tr.CustomerID = @CustomerID 
            AND tr.DetailFieldID IN (@TableDetailFieldID,@TableDetailFieldID1) /*New Expenditure Table*/
            and not exists ( SELECT * 
                             FROM TableDetailValues tdv WITH ( NOLOCK )
                             WHERE tdv.TableRowID = tr.TableRowID 
                             AND tdv.DetailFieldID = df2.DetailFieldID )



      --DECLARE @applicantonepercentage numeric(5,2) = 296280,
      --            @applicanttwopercentage numeric (5,2) = 296281,
      --            @applicantonetable INT = 297482,
      --            @applicanttwotable INT = 298592

            /*Pick up the correct defaults for this customer*/
            DECLARE @Defaults TABLE ( LookupListItemID INT, DetailFieldID INT, DefaultValue Varchar (2000) )
            INSERT @Defaults ( LookupListItemID, DetailFieldID )
            SELECT      li.LookupListItemID
                        ,     CASE li.LookupListItemID
                              WHEN @FoodLookupListItemID                            THEN      @FoodDetailFieldID                              --Food
                              WHEN @ClothingLookupListItemID                        THEN      @ClothingDetailFieldID                    --Clothing
                              WHEN @PersonalLookupListItemID                        THEN      @PersonalDetailFieldID                    --Personal Care
                              WHEN @HealthLookupListItemID                    THEN      @HealthDetailFieldID                      --Health
                              WHEN @HouseholdGoodsLookupListItemID            THEN      @HouseholdGoodsDetailFieldID        --Household Goods
                              WHEN @HouseholdServicesLookupListItemID   THEN      @HouseholdServicesDetailFieldID           --Household Services
                              WHEN @CommsLookupListItemID                           THEN      @CommsDetailFieldID                             --Communications
                              WHEN @SocialLookupListItemID                    THEN      @SocialDetailFieldID                      --Social Inclusion & Participation
                              WHEN @EducationLookupListItemID                       THEN      @EducationDetailFieldID                   --Education
                              WHEN @TravelLookupListItemID                    THEN      @TravelDetailFieldID                      --Travel (Public/Transport)
                              WHEN @HomeHeatingLookupListItemID               THEN      @HomeHeatingDetailFieldID                 --Home Heating
                              WHEN @PersonalCostsLookupListItemID             THEN      @PersonalCostsDetailFieldID               --Personal Costs
                              WHEN @HomeInsuranceLookupListItemID             THEN      @HomeInsuranceDetailFieldID               --Home Insurance
                              WHEN @CarLookupListItemID                             THEN      @CarDetailFieldID                         --Car Insurance
                              WHEN @SavingsLookupListItemID                   THEN      @SavingsDetailFieldID                     --Savings & Contingencies
                              WHEN @HouseholdElectricityLookupListItemID      THEN      @HouseholdElectricityDetailFieldID  --Household Electricity
                              END
            FROM LookupListItems li WITH ( NOLOCK )
            WHERE li.ClientID = @ClientID
            AND li.LookupListItemID IN ( @FoodLookupListItemID                            
                                                      ,@ClothingLookupListItemID                      
                                                      ,@PersonalLookupListItemID                      
                                                      ,@HealthLookupListItemID                        
                                                      ,@HouseholdGoodsLookupListItemID          
                                                      ,@HouseholdServicesLookupListItemID       
                                                      ,@CommsLookupListItemID                         
                                                      ,@SocialLookupListItemID                        
                                                      ,@EducationLookupListItemID                     
                                                      ,@TravelLookupListItemID                        
                                                      ,@HomeHeatingLookupListItemID             
                                                      ,@PersonalCostsLookupListItemID                 
                                                      ,@HomeInsuranceLookupListItemID                 
                                                      ,@CarLookupListItemID                           
                                                      ,@SavingsLookupListItemID                       
                                                      ,@HouseholdElectricityLookupListItemID )
      
            UPDATE @Defaults
            
            SET DefaultValue =  CAST( cdv.Amount *  ( CAST(dbo.fnGetDv (296280, @CaseID) AS MONEY) / 100.00 ) AS NUMERIC(18,2))
            FROM @Defaults d 
            CROSS APPLY dbo.fn_C00_ISI_SetCostsBreakdown (@CustomerID) cdv
            --LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.CustomerID = @CustomerID AND cdv.DetailFieldID = d.DetailFieldID
            WHERE d.DetailFieldID = CASE 
            WHEN cdv.Type = 'Food'								THEN @FoodDetailFieldID
            WHEN cdv.Type = 'Clothing'							THEN @ClothingDetailFieldID
            WHEN cdv.Type = 'Personal Care'						THEN @PersonalDetailFieldID
            WHEN cdv.Type = 'Health'							THEN @HealthDetailFieldID
            WHEN cdv.Type = 'Household Goods'					THEN @HouseholdGoodsDetailFieldID
            WHEN cdv.Type = 'Household Services'				THEN @HouseholdServicesDetailFieldID
			WHEN cdv.Type = 'Communications'					THEN @CommsDetailFieldID           
            WHEN cdv.Type = 'Social Inclusion & Participation'	THEN @SocialDetailFieldID
            WHEN cdv.Type = 'Education'							THEN @EducationDetailFieldID
            WHEN cdv.Type = 'Travel (Public/Transport)'			THEN @TravelDetailFieldID
            WHEN cdv.Type = 'Household Electricity'				THEN @HouseholdElectricityDetailFieldID
            WHEN cdv.Type = 'Home Heating'						THEN @HomeHeatingDetailFieldID
            WHEN cdv.Type = 'Personal Costs'					THEN @PersonalCostsDetailFieldID
            WHEN cdv.Type = 'Home Insurance'					THEN @HomeInsuranceDetailFieldID
            WHEN cdv.Type = 'Car Insurance'						THEN @CarDetailFieldID
            WHEN cdv.Type = 'Savings & Contingencies'			THEN @SavingsDetailFieldID
            END


            /*Update any of the rows for which the values are incorrect*/
            /*We only ever update Default Value and/or Amount to Use for a given row*/
            /*Amount to use does not change if there is an override value present*/
            --SELECT tdv_up.TableRowID, tdv_up.DetailValue, tdv_up.DetailFieldID, df.DefaultValue
            UPDATE tdv_up
            SET DetailValue = df.DefaultValue
            FROM TableRows tr WITH ( NOLOCK )
            INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID /*Type*/
            INNER JOIN TableDetailValues tdv_de WITH ( NOLOCK ) on tdv_de.TableRowID = tr.TableRowID AND tdv_de.DetailFieldID = @DefaultValueDetailFieldID
            INNER JOIN TableDetailValues tdv_tu WITH ( NOLOCK ) on tdv_tu.TableRowID = tr.TableRowID AND tdv_tu.DetailFieldID = @AmountToUseDetailFieldID
            INNER JOIN TableDetailValues tdv_or WITH ( NOLOCK ) on tdv_or.TableRowID = tr.TableRowID AND tdv_or.DetailFieldID = @OverrideDetailFieldID
            INNER JOIN TableDetailValues tdv_up WITH ( NOLOCK ) on tdv_up.TableRowID = tr.TableRowID AND tdv_up.DetailFieldID IN ( @DefaultValueDetailFieldID,@AmountToUseDetailFieldID )
            INNER JOIN @Defaults df on df.LookupListItemID = tdv.ValueInt
            WHERE tr.CustomerID = @CustomerID 
            AND
            (
                  ( tdv_up.DetailFieldID = @DefaultValueDetailFieldID AND tdv_up.DetailValue <> df.DefaultValue )
            or
                  ( tdv_up.DetailFieldID = @AmountToUseDetailFieldID AND ISNULL(tdv_or.ValueMoney,0.00) = 0.00 AND tdv_up.DetailValue <> df.DefaultValue )
            )

                                                                                                                                                                        
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_UpdateSetCostsOverrides] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISI_UpdateSetCostsOverrides] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_UpdateSetCostsOverrides] TO [sp_executeall]
GO
