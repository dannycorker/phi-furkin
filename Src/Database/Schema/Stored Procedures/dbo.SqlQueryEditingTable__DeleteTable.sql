SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-09-05
-- Description:	Delete current sql table and all 
-- referencing tables, columns and criteria
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingTable__DeleteTable]
(
	@SqlQueryEditingID int,
	@SqlQueryEditingTableID int
)
AS

	DECLARE @SqlQueryEditingTableHeirarchy TABLE
	(
		SqlQueryEditingTableID INT
	)

	-- Aquire a list of all the tables that need deleting
	;WITH SqlQueryEditingTableHeirarchy (SqlQueryEditingTableID) AS
	(

	   SELECT
			t.SqlQueryEditingTableID
	   FROM SqlQueryEditingTable t
	   WHERE t.SqlQueryEditingTableID = @SqlQueryEditingTableID
	   AND	 t.SqlQueryEditingID = @SqlQueryEditingID

	   UNION ALL

	   SELECT
			t.SqlQueryEditingTableID
	   FROM SqlQueryEditingTable t
		  INNER JOIN SqlQueryEditingTableHeirarchy th ON
			 t.JoinTableID = th.SqlQueryEditingTableID
	)

	INSERT INTO @SqlQueryEditingTableHeirarchy
	SELECT	SqlQueryEditingTableID
	FROM	SqlQueryEditingTableHeirarchy

	-- DELETE FROM SqlQueryEditingGrouping (Future Requirement)

	-- DELETE FROM SqlQueryEditingCriteria
	DELETE
	FROM	SqlQueryEditingCriteria
	WHERE	SqlQueryEditingID = @SqlQueryEditingID
	AND		SqlQueryEditingTableID IN (SELECT SqlQueryEditingTableID FROM @SqlQueryEditingTableHeirarchy)

	-- DELETE FROM SqlQueryEditingColumns
	DELETE
	FROM	SqlQueryEditingColumns
	WHERE	SqlQueryEditingID = @SqlQueryEditingID
	AND		SqlQueryEditingTableID IN (SELECT SqlQueryEditingTableID FROM @SqlQueryEditingTableHeirarchy)

	-- DELETE FROM SqlQueryEditingTable
	DELETE
	FROM	SqlQueryEditingTable
	WHERE	SqlQueryEditingID = @SqlQueryEditingID
	AND		SqlQueryEditingTableID IN (SELECT SqlQueryEditingTableID FROM @SqlQueryEditingTableHeirarchy)





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__DeleteTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable__DeleteTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__DeleteTable] TO [sp_executeall]
GO
