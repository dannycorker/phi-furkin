SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[CountLeadsByType] 

@clientid int

AS

select lt.leadtypedescription, count(*), 1 as sortorder
from lead l, leadtype lt
where l.leadtypeid = lt.leadtypeid
and l.clientid = @clientid
group by lt.leadtypedescription 

union

select 'Grand Total for all lead types: ' as leadtypedescription, count(*), 2 as sortorder
from lead l
where l.clientid = @clientid 
order by 3,1



GO
GRANT VIEW DEFINITION ON  [dbo].[CountLeadsByType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CountLeadsByType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CountLeadsByType] TO [sp_executeall]
GO
