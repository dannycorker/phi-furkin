SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AssetHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetHistory_Delete]
(

	@AssetHistoryID int   
)
AS


				DELETE FROM [dbo].[AssetHistory] WITH (ROWLOCK) 
				WHERE
					[AssetHistoryID] = @AssetHistoryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Delete] TO [sp_executeall]
GO
