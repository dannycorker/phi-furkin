SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_GetByMatterIDDetailFieldIDClientIDLeadID]
(

	@MatterID int   ,

	@DetailFieldID int   ,

	@ClientID int   ,

	@LeadID int   
)
AS


				SELECT
					[MatterDetailValueID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[OriginalDetailValueID],
					[OriginalLeadID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[MatterDetailValues] WITH (NOLOCK) 
				WHERE
										[MatterID] = @MatterID
					AND [DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
					AND [LeadID] = @LeadID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_GetByMatterIDDetailFieldIDClientIDLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_GetByMatterIDDetailFieldIDClientIDLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_GetByMatterIDDetailFieldIDClientIDLeadID] TO [sp_executeall]
GO
