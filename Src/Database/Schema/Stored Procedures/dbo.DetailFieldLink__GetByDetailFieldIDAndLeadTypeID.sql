SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink__GetByDetailFieldIDAndLeadTypeID]
(

	@DetailFieldID int,
	@LeadTypeID int
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[DetailFieldLinkID],
					[ClientID],
					[DetailFieldID],
					[MasterQuestionID],
					[LeadTypeID]
				FROM
					[dbo].[DetailFieldLink]
				WHERE
					[DetailFieldID] = @DetailFieldID
				AND
					[LeadTypeID] = @LeadTypeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink__GetByDetailFieldIDAndLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink__GetByDetailFieldIDAndLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink__GetByDetailFieldIDAndLeadTypeID] TO [sp_executeall]
GO
