SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Delete]
(

	@LeadTypePageTitleSpecialisedDetailFieldTargetID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[LeadTypePageTitleSpecialisedDetailFieldTargetID] = @LeadTypePageTitleSpecialisedDetailFieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Delete] TO [sp_executeall]
GO
