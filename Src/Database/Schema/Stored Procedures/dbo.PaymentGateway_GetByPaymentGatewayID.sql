SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PaymentGateway table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentGateway_GetByPaymentGatewayID]
(

	@PaymentGatewayID int   
)
AS


				SELECT
					[PaymentGatewayID],
					[Gateway],
					[Name],
					[Description],
					[Url]
				FROM
					[dbo].[PaymentGateway] WITH (NOLOCK) 
				WHERE
										[PaymentGatewayID] = @PaymentGatewayID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_GetByPaymentGatewayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway_GetByPaymentGatewayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_GetByPaymentGatewayID] TO [sp_executeall]
GO
