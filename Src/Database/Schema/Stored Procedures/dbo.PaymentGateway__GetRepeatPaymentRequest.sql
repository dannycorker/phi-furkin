SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-21
-- Description:	Gets the repeat payment request data
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__GetRepeatPaymentRequest]
	@ClientID int,
	@CustomerID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT tpm.ThirdPartyFieldID, cdv.DetailValue, cdv.CustomerID
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON 
		tpm.DetailFieldID = cdv.DetailFieldID AND 
		tpm.LeadTypeID = 0 AND tpm.IsEnabled = 1 AND 
		tpm.ThirdPartyFieldID IN (622,623,624,625,603,604,605,606,607,608,593,584,585,586)
	WHERE cdv.ClientID = @ClientID
	AND cdv.CustomerID = @CustomerID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetRepeatPaymentRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__GetRepeatPaymentRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetRepeatPaymentRequest] TO [sp_executeall]
GO
