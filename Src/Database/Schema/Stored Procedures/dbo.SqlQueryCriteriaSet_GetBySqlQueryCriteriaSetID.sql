SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryCriteriaSet table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSet_GetBySqlQueryCriteriaSetID]
(

	@SqlQueryCriteriaSetID int   
)
AS


				SELECT
					[SqlQueryCriteriaSetID],
					[ClientID],
					[SqlQueryID],
					[SetName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[SqlQueryCriteriaSet] WITH (NOLOCK) 
				WHERE
										[SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_GetBySqlQueryCriteriaSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSet_GetBySqlQueryCriteriaSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_GetBySqlQueryCriteriaSetID] TO [sp_executeall]
GO
