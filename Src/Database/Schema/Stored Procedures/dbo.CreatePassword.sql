SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[CreatePassword] (@password [nvarchar] (4000))
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [SqlServerProject_Regex].[UserDefinedFunctions].[CreatePassword]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreatePassword] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreatePassword] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreatePassword] TO [sp_executeall]
GO
