SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FunctionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_Get_List]

AS


				
				SELECT
					[FunctionTypeID],
					[ModuleID],
					[ParentFunctionTypeID],
					[FunctionTypeName],
					[Level],
					[IsVirtual],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[FunctionType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Get_List] TO [sp_executeall]
GO
