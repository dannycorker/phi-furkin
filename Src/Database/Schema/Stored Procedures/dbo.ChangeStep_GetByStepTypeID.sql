SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeStep table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStep_GetByStepTypeID]
(

	@StepTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChangeStepID],
					[ClientID],
					[ChangeInstanceID],
					[StepTypeID],
					[ChangeDocumentID],
					[Comment],
					[ExternalUrl],
					[ChangeStepStateID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ChangeStep] WITH (NOLOCK) 
				WHERE
					[StepTypeID] = @StepTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_GetByStepTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStep_GetByStepTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_GetByStepTypeID] TO [sp_executeall]
GO
