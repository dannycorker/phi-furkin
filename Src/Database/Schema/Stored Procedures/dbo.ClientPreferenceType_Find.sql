SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPreferenceType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreferenceType_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPreferenceTypeID int   = null ,

	@PreferenceTypeName varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPreferenceTypeID]
	, [PreferenceTypeName]
    FROM
	[dbo].[ClientPreferenceType] WITH (NOLOCK) 
    WHERE 
	 ([ClientPreferenceTypeID] = @ClientPreferenceTypeID OR @ClientPreferenceTypeID IS NULL)
	AND ([PreferenceTypeName] = @PreferenceTypeName OR @PreferenceTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPreferenceTypeID]
	, [PreferenceTypeName]
    FROM
	[dbo].[ClientPreferenceType] WITH (NOLOCK) 
    WHERE 
	 ([ClientPreferenceTypeID] = @ClientPreferenceTypeID AND @ClientPreferenceTypeID is not null)
	OR ([PreferenceTypeName] = @PreferenceTypeName AND @PreferenceTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreferenceType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Find] TO [sp_executeall]
GO
