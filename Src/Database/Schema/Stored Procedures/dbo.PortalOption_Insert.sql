SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalOption_Insert]
(

	@PortalOptionID int    OUTPUT,

	@ClientID int   ,

	@ShowHelpLink bit   ,

	@HelpLinkCaption varchar (250)  ,

	@HelpLinkURL varchar (500)  ,

	@LeadListShowAssignedTo bit   ,

	@LeadListShowAssignedToCaption varchar (50)  ,

	@LeadListShowCaseStatus bit   ,

	@LeadListShowCaseStatusCaption varchar (50)  
)
AS


				
				INSERT INTO [dbo].[PortalOption]
					(
					[ClientID]
					,[ShowHelpLink]
					,[HelpLinkCaption]
					,[HelpLinkURL]
					,[LeadListShowAssignedTo]
					,[LeadListShowAssignedToCaption]
					,[LeadListShowCaseStatus]
					,[LeadListShowCaseStatusCaption]
					)
				VALUES
					(
					@ClientID
					,@ShowHelpLink
					,@HelpLinkCaption
					,@HelpLinkURL
					,@LeadListShowAssignedTo
					,@LeadListShowAssignedToCaption
					,@LeadListShowCaseStatus
					,@LeadListShowCaseStatusCaption
					)
				-- Get the identity value
				SET @PortalOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Insert] TO [sp_executeall]
GO
