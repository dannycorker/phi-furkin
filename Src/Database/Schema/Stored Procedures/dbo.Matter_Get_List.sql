SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Matter table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_Get_List]

AS


				
				SELECT
					[MatterID],
					[ClientID],
					[MatterRef],
					[CustomerID],
					[LeadID],
					[MatterStatus],
					[RefLetter],
					[BrandNew],
					[CaseID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[Matter] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Get_List] TO [sp_executeall]
GO
