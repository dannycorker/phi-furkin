SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-08
-- Description:	Update CustomTableSQL
-- =============================================
CREATE PROCEDURE [dbo].[CustomTableSQL_Delete]
	@CustomTableSQLID INT

AS
BEGIN

	SET NOCOUNT ON;

	DELETE
	FROM CustomTableSQL
	WHERE CustomTableSQL.CustomTableSQLID = @CustomTableSQLID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQL_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL_Delete] TO [sp_executeall]
GO
