SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By:  (Aaran)
-- Purpose: Select records from the DocumentQueue table through a foreign key
-- UPDATE: Simon Brushett 2012-05-08 Added a CTE to try and force the client personnel filter to come after the others
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DocumentQueue__EnhancedGetByClientLeadTypeDocTypeClientPersonnel]
(
	@ClientID int,
	@LeadTypeID int = NULL,
	@DocumentTypeID int = NULL,
	@ClientPersonnelID int = NULL,
	@RequiresApproval bit = 0
)
WITH RECOMPILE
AS
BEGIN
	
	
	;WITH InnerSql AS 
	(
		SELECT  
		  dq.[DocumentQueueID], 
		  dq.[ClientID], 
		  dq.[LeadID], 
		  dq.[CaseID], 
		  l.[LeadTypeID], 
		  dt.[DocumentTypeID], 
		  dt.[DocumentTypeName], 
		  dq.[ParsedDocumentTitle],
		  et.[EventTypeName], 
		  cp.[UserName], 
		  dq.[WhenStored], 
		  dq.[WhenCreated], 
		  dq.[RequiresApproval],		 
		  le.WhoCreated,
		  ROW_NUMBER() OVER (PARTITION BY dq.[DocumentQueueID] ORDER BY dq.[DocumentQueueID]) AS rn  
		  FROM        DocumentQueue dq (nolock)
		  INNER JOIN  LeadEvent le (nolock) on le.DocumentQueueID = dq.DocumentQueueId
		  INNER JOIN  Lead l (nolock) on dq.LeadID = l.LeadID
		  LEFT JOIN   DocumentType dt (nolock) on dq.DocumentTypeID = dt.DocumentTypeID
		  INNER JOIN  EventType et (nolock) on le.EventTypeID = et.EventTypeID
		  INNER JOIN  ClientPersonnel cp (nolock) on dq.WhoCreated = cp.ClientPersonnelID 
		  LEFT JOIN   DocumentZip dz (nolock) on dz.[DocumentQueueID] = dq.[DocumentQueueID] 
		  WHERE dq.ClientID = @ClientID 
		  AND dq.[WhenCreated] IS NULL
		  AND dz.[DocumentQueueID] IS NULL 
		  --AND (@ClientID <> 57 OR (@ClientID = 57 AND dt.DocumentTypeID <> 14258))
		  AND (@LeadTypeID = 0 OR l.LeadTypeID = @LeadTypeID)
		  AND (@DocumentTypeID IS NULL OR dt.DocumentTypeID = @DocumentTypeID)
		  AND dq.[RequiresApproval] = @RequiresApproval
	)
	
	SELECT DISTINCT i.* 
	FROM InnerSql i 
	WHERE (@ClientPersonnelID IS NULL OR i.WhoCreated = @ClientPersonnelID)
	AND i.rn = 1
	
	
	
	/*SELECT DISTINCT 
	dq.[DocumentQueueID], 
	dq.[ClientID], 
	dq.[LeadID], 
	dq.[CaseID], 
	l.[LeadTypeID], 
	dt.[DocumentTypeID], 
	dt.[DocumentTypeName], 
	et.[EventTypeName], 
	cp.[UserName], 
	dq.[WhenStored], 
	dq.[WhenCreated], 
	dq.[RequiresApproval]
	FROM		DocumentQueue dq (nolock)
	INNER JOIN  LeadEvent le (nolock) on le.DocumentQueueID = dq.DocumentQueueId
	INNER JOIN	Lead l (nolock) on dq.LeadID = l.LeadID
	LEFT JOIN	DocumentType dt (nolock) on dq.DocumentTypeID = dt.DocumentTypeID
	INNER JOIN	EventType et (nolock) on le.EventTypeID = et.EventTypeID
	INNER JOIN	ClientPersonnel cp (nolock) on dq.WhoCreated = cp.ClientPersonnelID 
	LEFT JOIN   DocumentZip dz (nolock) on dz.[DocumentQueueID] = dq.[DocumentQueueID] 
	WHERE dq.ClientID = @ClientID 
	AND dq.[WhenCreated] IS NULL
	AND dz.[DocumentQueueID] IS NULL 
	--AND (@ClientID <> 57 OR (@ClientID = 57 AND dt.DocumentTypeID <> 14258))
	AND (@LeadTypeID = 0 OR l.LeadTypeID = @LeadTypeID)
	AND (@DocumentTypeID IS NULL OR dt.DocumentTypeID = @DocumentTypeID)
	--AND (@ClientPersonnelID IS NULL OR le.WhoCreated = @ClientPersonnelID)
	AND dq.[RequiresApproval] = @RequiresApproval
	
	ORDER BY dq.[DocumentQueueID] asc



	SELECT		dq.[DocumentQueueID]
				,dq.[ClientID]
				,dq.[LeadID]
				,dq.[CaseID]
				,l.[LeadTypeID]
				,dt.[DocumentTypeID]
				,dt.[DocumentTypeName]
				,et.[EventTypeName]
				,cp.[UserName]
				,dq.[WhenStored]
				,dq.[WhenCreated]
				,dq.[RequiresApproval]
	FROM		LeadEvent le
	LEFT JOIN	DocumentQueue dq on le.DocumentQueueID = dq.DocumentQueueId
	LEFT JOIN	Lead l		 on dq.LeadID = l.LeadID
	LEFT JOIN	DocumentType dt on dq.DocumentTypeID = dt.DocumentTypeID
	LEFT JOIN	EventType et on le.EventTypeID = et.EventTypeID
	LEFT JOIN	ClientPersonnel cp on dq.WhoCreated = cp.ClientPersonnelID
	WHERE dq.ClientID = @ClientID
	AND dq.[WhenCreated] is null
	AND NOT EXISTS (
		SELECT * 
		FROM DocumentZip dz 
		WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
	)
	ORDER BY dq.[DocumentQueueID] asc
*/
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue__EnhancedGetByClientLeadTypeDocTypeClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue__EnhancedGetByClientLeadTypeDocTypeClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue__EnhancedGetByClientLeadTypeDocTypeClientPersonnel] TO [sp_executeall]
GO
