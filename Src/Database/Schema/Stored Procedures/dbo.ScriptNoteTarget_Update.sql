SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptNoteTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_Update]
(

	@ScriptNoteTargetID int   ,

	@ScriptID int   ,

	@ClientID int   ,

	@NoteTypeID int   ,

	@PriorityID int   ,

	@ApplyToAllCases bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptNoteTarget]
				SET
					[ScriptID] = @ScriptID
					,[ClientID] = @ClientID
					,[NoteTypeID] = @NoteTypeID
					,[PriorityID] = @PriorityID
					,[ApplyToAllCases] = @ApplyToAllCases
				WHERE
[ScriptNoteTargetID] = @ScriptNoteTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Update] TO [sp_executeall]
GO
