SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-21
-- Description:	Gets the payment request data
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__GetPaymentRequest]
	@ClientID INT,
	@CustomerID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT tpm.ThirdPartyFieldID, cdv.DetailValue, cdv.CustomerID
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON 
		tpm.DetailFieldID = cdv.DetailFieldID AND 
		tpm.LeadTypeID = 0 AND tpm.IsEnabled = 1 AND 
		tpm.ThirdPartyFieldID IN (593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614)
	WHERE cdv.ClientID = @ClientID
		AND cdv.CustomerID = @CustomerID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetPaymentRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__GetPaymentRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetPaymentRequest] TO [sp_executeall]
GO
