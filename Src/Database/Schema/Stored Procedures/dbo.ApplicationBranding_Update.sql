SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ApplicationBranding table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ApplicationBranding_Update]
(

	@ApplicationBrandingID int   ,

	@ClientID int   ,

	@FileName varchar (50)  ,

	@VerticalSize int   ,

	@HorizontalSize int   ,

	@LeadManagerAlias varchar (50)  ,

	@LeadAlias varchar (50)  ,

	@LeadDetailsTabImageFileName varchar (255)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ApplicationBranding]
				SET
					[ClientID] = @ClientID
					,[FileName] = @FileName
					,[VerticalSize] = @VerticalSize
					,[HorizontalSize] = @HorizontalSize
					,[LeadManagerAlias] = @LeadManagerAlias
					,[LeadAlias] = @LeadAlias
					,[LeadDetailsTabImageFileName] = @LeadDetailsTabImageFileName
				WHERE
[ApplicationBrandingID] = @ApplicationBrandingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ApplicationBranding_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Update] TO [sp_executeall]
GO
