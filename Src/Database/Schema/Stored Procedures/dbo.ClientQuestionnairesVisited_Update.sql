SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientQuestionnairesVisited table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisited_Update]
(

	@ClientQuestionnairesVisitedID int   ,

	@ClientQuestionnaireID int   ,

	@VisitedDate datetime   ,

	@IPAddress varchar (50)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientQuestionnairesVisited]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[VisitedDate] = @VisitedDate
					,[IPAddress] = @IPAddress
					,[ClientID] = @ClientID
				WHERE
[ClientQuestionnairesVisitedID] = @ClientQuestionnairesVisitedID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisited_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_Update] TO [sp_executeall]
GO
