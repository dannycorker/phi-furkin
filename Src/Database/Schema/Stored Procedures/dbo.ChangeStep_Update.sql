SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChangeStep table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStep_Update]
(

	@ChangeStepID int   ,

	@ClientID int   ,

	@ChangeInstanceID int   ,

	@StepTypeID int   ,

	@ChangeDocumentID int   ,

	@Comment varchar (255)  ,

	@ExternalUrl varchar (255)  ,

	@ChangeStepStateID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChangeStep]
				SET
					[ClientID] = @ClientID
					,[ChangeInstanceID] = @ChangeInstanceID
					,[StepTypeID] = @StepTypeID
					,[ChangeDocumentID] = @ChangeDocumentID
					,[Comment] = @Comment
					,[ExternalUrl] = @ExternalUrl
					,[ChangeStepStateID] = @ChangeStepStateID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[ChangeStepID] = @ChangeStepID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStep_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Update] TO [sp_executeall]
GO
