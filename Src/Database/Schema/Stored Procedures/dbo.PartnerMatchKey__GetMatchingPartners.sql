SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Get Partners with matching keys
--				ACE 2013-01-30 Used with clause
-- =============================================
CREATE PROCEDURE [dbo].[PartnerMatchKey__GetMatchingPartners]

	@ClientID INT,	
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250)

AS
BEGIN

	WITH pmk as (
		SELECT *, CASE WHEN p.FirstName = @FirstName THEN 1 ELSE 0 END as [FNameMatch],
				CASE WHEN p.LastName = @LastName THEN 1 ELSE 0 END as [LNameMatch],
				CASE WHEN p.Address1 = @Address1 THEN 1 ELSE 0 END as [A1Match],
				CASE WHEN p.Postcode = @PostCode THEN 1 ELSE 0 END as [PostcodeMatch],
				CASE WHEN p.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [DobMatch],
				CASE WHEN p.FirstName = @FirstName THEN 1 ELSE 0 END +
				CASE WHEN p.LastName = @LastName THEN 1 ELSE 0 END +
				CASE WHEN p.Address1 = @Address1 THEN 1 ELSE 0 END +
				CASE WHEN p.Postcode = @PostCode THEN 1 ELSE 0 END +
				CASE WHEN p.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [TotalMatch]
		FROM PartnerMatchKey p WITH (NOLOCK)
		WHERE p.ClientID = @ClientID
		AND (
			p.FirstName = @FirstName
			OR
			p.LastName = @LastName
			OR 
			p.Address1 = @Address1
			OR
			p.Postcode = @PostCode
			--OR
			--p.DateOfBirth = @DateOfBirth
		)
	)

	SELECT 
		p.ClientID,
		p.PartnerID,
		p.FirstName, 
		p.LastName, 
		CASE p.UseCustomerAddress WHEN 1 THEN c.Address1 ELSE p.Address1 END Address1,
		CASE p.UseCustomerAddress WHEN 1 THEN c.PostCode ELSE p.Postcode END Postcode,
		p.Fullname,
		p.DateOfBirth,
		p.EmailAddress,
		p.HomeTelephone,
		p.MobileTelephone,
		p.Occupation,
		p.DateOfBirth,
		pmk.Firstname FirstnameKey, 
		pmk.Lastname LastnameKey, 
		pmk.Address1 Address1Key, 
		pmk.Postcode PostcodeKey,
		pmk.DateOfBirth DateOfBirth
	FROM pmk WITH (NOLOCK)
	INNER JOIN dbo.Partner p WITH (NOLOCK) on p.PartnerID = pmk.PartnerID
	INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = p.CustomerID AND c.Test = 0
	WHERE pmk.TotalMatch > 3

END




GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__GetMatchingPartners] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PartnerMatchKey__GetMatchingPartners] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__GetMatchingPartners] TO [sp_executeall]
GO
