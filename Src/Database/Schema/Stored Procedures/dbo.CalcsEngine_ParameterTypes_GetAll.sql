SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Returns all parameter types for the calculation
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_ParameterTypes_GetAll]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.CalcsEngine_ParameterTypes WITH (NOLOCK) 
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_ParameterTypes_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_ParameterTypes_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_ParameterTypes_GetAll] TO [sp_executeall]
GO
