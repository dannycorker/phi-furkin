SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetYellowPageAreaCode] @PostCode nvarchar(10)
AS
SELECT     YellowPagesAreaCodeID, YellowPagesAreaCode, PostCode
FROM         dbo.YellowPagesAreaCodes
WHERE     (@PostCode LIKE REPLACE(PostCode, ' ', '') + '%')



GO
GRANT VIEW DEFINITION ON  [dbo].[GetYellowPageAreaCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetYellowPageAreaCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetYellowPageAreaCode] TO [sp_executeall]
GO
