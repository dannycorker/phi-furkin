SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the GroupAndUserEditingControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupAndUserEditingControl_Insert]
(

	@EditingID int    OUTPUT,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@WhoIsEditing int   ,

	@EditStartedAt datetime   
)
AS


				
				INSERT INTO [dbo].[GroupAndUserEditingControl]
					(
					[ClientPersonnelAdminGroupID]
					,[ClientPersonnelID]
					,[WhoIsEditing]
					,[EditStartedAt]
					)
				VALUES
					(
					@ClientPersonnelAdminGroupID
					,@ClientPersonnelID
					,@WhoIsEditing
					,@EditStartedAt
					)
				-- Get the identity value
				SET @EditingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupAndUserEditingControl_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Insert] TO [sp_executeall]
GO
