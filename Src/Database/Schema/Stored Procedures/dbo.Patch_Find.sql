SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Patch table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Patch_Find]
(

	@SearchUsingOR bit   = null ,

	@PatchID int   = null ,

	@PatchDate datetime   = null ,

	@PatchVersion varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PatchID]
	, [PatchDate]
	, [PatchVersion]
    FROM
	[dbo].[Patch] WITH (NOLOCK) 
    WHERE 
	 ([PatchID] = @PatchID OR @PatchID IS NULL)
	AND ([PatchDate] = @PatchDate OR @PatchDate IS NULL)
	AND ([PatchVersion] = @PatchVersion OR @PatchVersion IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PatchID]
	, [PatchDate]
	, [PatchVersion]
    FROM
	[dbo].[Patch] WITH (NOLOCK) 
    WHERE 
	 ([PatchID] = @PatchID AND @PatchID is not null)
	OR ([PatchDate] = @PatchDate AND @PatchDate is not null)
	OR ([PatchVersion] = @PatchVersion AND @PatchVersion is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Patch_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Find] TO [sp_executeall]
GO
