SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIClientPersonnelCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientPersonnelCredential_Update]
(

	@RPIClientPersonnelCredentialID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@UserName varchar (250)  ,

	@Password varchar (50)  ,

	@AsUser varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIClientPersonnelCredential]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[UserName] = @UserName
					,[Password] = @Password
					,[AsUser] = @AsUser
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[RPIClientPersonnelCredentialID] = @RPIClientPersonnelCredentialID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientPersonnelCredential_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Update] TO [sp_executeall]
GO
