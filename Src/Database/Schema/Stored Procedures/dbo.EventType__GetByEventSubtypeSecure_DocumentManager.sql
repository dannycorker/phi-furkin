SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2007-10-31
-- Description:	Gets all EventTypes that user has access to, by ClientID
-- SB  2014-07-10 Updated to use view which includes shared event types
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetByEventSubtypeSecure_DocumentManager]
(
	@LeadTypeID int,
	@EventSubtypeID int,
	@UserID int
)
AS
	SET ANSI_NULLS OFF
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID
	
	IF @LeadTypeID = -1
	BEGIN
		SET @LeadTypeID = null
	END

	

	SELECT
		[EventTypeID]
		,et.[ClientID]
		,[EventTypeName]  
		,('(' + lt.LeadTypeName + ') ' + et.[EventTypeDescription]) EventTypeDescription
		,et.[Enabled]
		,et.[UnitsOfEffort]
		,et.[FollowupTimeUnitsID]
		,et.[FollowupQuantity]
		,et.[AvailableManually]
		,et.[StatusAfterEvent]
		,et.[AquariumEventAfterEvent]
		,et.[EventSubtypeID]
		,et.[DocumentTypeID]
		,et.[LeadTypeID]
		,et.[AllowCustomTimeUnits]
		,et.[InProcess]
		,et.[KeyEvent]
		,et.[UseEventCosts]
		,et.[UseEventUOEs]
		,et.[UseEventDisbursements]
		,et.[UseEventComments]
		,et.[SignatureRequired]
		,et.[SignatureOverride]
		,et.[VisioX]
		,et.[VisioY]
		,et.[AquariumEventSubtypeID]
		,et.[WhoCreated]
		,et.[WhenCreated]
		,et.[WhoModified]
		,et.[WhenModified]		
		,et.[FollowupWorkingDaysOnly]
		,et.[CalculateTableRows]
		,et.SmsGatewayID
		,et.IsShared						
	FROM
		dbo.fnEventTypeShared(@ClientID) et 
		INNER JOIN fnEventTypeSecure(@UserID, null) f ON et.EventTypeID = f.objectid
		INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = et.LeadTypeID
	WHERE
		[EventSubTypeID] = @EventSubtypeID
	AND (et.[LeadTypeID] = @LeadTypeID OR @LeadTypeID is null)
	AND et.[Enabled] = 1 
	ORDER BY lt.LeadTypeName, EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON













GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByEventSubtypeSecure_DocumentManager] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByEventSubtypeSecure_DocumentManager] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByEventSubtypeSecure_DocumentManager] TO [sp_executeall]
GO
