SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author: Mark Law
-- Create date: 2009-08-06
-- Description:	Copy Detail Fields after Case Transfer
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CaseTransferDetailValues]
(
@LeadEventID int,
@Direction varchar(10)
)
	
AS
BEGIN

	SET NOCOUNT ON

Declare	@myERROR int,
	@LeadID int,
	@EventTypeID int,
	@MatterID int,
	@CustomerID int,
	@ClientID int,
	@CaseID int

	SELECT
	@LeadID = Leadevent.LeadID,
	@EventTypeID = LeadEvent.EventTypeID,
	@CustomerID = l.CustomerID,
	@ClientID = l.ClientID,
	@CaseID = LeadEvent.CaseID
	FROM LeadEvent
	Inner Join Cases c ON c.CaseID = LeadEvent.CaseID
	Inner Join Lead l on l.leadid = c.leadid
	WHERE leadevent.leadeventid = @LeadEventID 
	
	/* Create all detail fields ACE 2009-08-06 */
	If @Direction = 'To'
	Begin

		Insert Into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		Select ctm.NewClientID, ctm.NewLeadID, ctm.NewMatterID, ctfm.DestinationDetailFieldID, ''
		From CaseTransferMapping ctm 
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'To' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 2
		Where ctm.newcaseid = @CaseID
		and NOT EXISTS (Select * From MatterDetailValues Where MatterID = ctm.newMatterID and DetailFieldID = ctfm.DestinationDetailFieldID)

		Insert Into LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		Select ctm.NewClientID, ctm.NewLeadID, ctfm.DestinationDetailFieldID, ''
		From CaseTransferMapping ctm
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'To' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 1
		Where ctm.NewCaseID = @CaseID
		and NOT EXISTS (Select * From LeadDetailValues Where LeadID = ctm.NewLeadID and DetailFieldID = ctfm.DestinationDetailFieldID)

		Update ldv_to 
		Set ldv_to.DetailValue = ldv_from.DetailValue
		From LeadDetailValues ldv_to
		Inner Join CaseTransferMapping ctm on ctm.NewLeadID = ldv_to.LeadID and ctm.NewCaseID = @CaseID
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.DestinationDetailFieldID = ldv_to.detailfieldid and ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'To' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 1 and df.questiontypeid <> 4
		Inner Join LeadDetailValues ldv_from on ldv_from.leadid = ctm.LeadID and ldv_from.detailfieldid = ctfm.OriginatingDetailFieldID
		Where ldv_from.DetailValue <> ''

		/* Do Lookup Lists */
		Update ldv_to 
		Set ldv_to.DetailValue = luli_to.LookupListItemID
		From LeadDetailValues ldv_to
		Inner Join CaseTransferMapping ctm on ctm.NewLeadID = ldv_to.LeadID and ctm.NewCaseID = @CaseID
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.DestinationDetailFieldID = ldv_to.detailfieldid and ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'To' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 1 and df.questiontypeid = 4
		Inner Join LeadDetailValues ldv_from on ldv_from.leadid = ctm.LeadID and ldv_from.detailfieldid = ctfm.OriginatingDetailFieldID
		Inner Join DetailFields df_from on df_from.detailfieldid = ldv_from.detailfieldid and df_from.questiontypeid = 4
		Inner Join LookupListItems luli_from on luli_from.LookupListItemID = ldv_from.ValueInt
		Inner Join LookupListItems luli_to on luli_to.LookupListID = df.LookupListID and luli_to.itemvalue = luli_from.itemvalue
		Where ldv_from.DetailValue <> ''

		Update mdv_to 
		Set mdv_to.DetailValue = mdv_from.DetailValue
		From MatterDetailValues mdv_to
		Inner Join CaseTransferMapping ctm on ctm.NewMatterID = mdv_to.MatterID and ctm.NewCaseID = @CaseID
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.DestinationDetailFieldID = mdv_to.detailfieldid and ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'To' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 2 and df.questiontypeid <> 4
		Inner Join MatterDetailValues mdv_from on mdv_from.MatterID = ctm.MatterID and mdv_from.detailfieldid = ctfm.OriginatingDetailFieldID
		Where mdv_from.DetailValue <> ''

		/* Do Lookup Lists */
		Update mdv_to 
		Set mdv_to.DetailValue = luli_to.LookupListItemID
		From MatterDetailValues mdv_to
		Inner Join CaseTransferMapping ctm on ctm.NewMatterID = mdv_to.MatterID and ctm.NewCaseID = @CaseID
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.DestinationDetailFieldID = mdv_to.detailfieldid and ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'To' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 2 and df.questiontypeid = 4
		Inner Join MatterDetailValues mdv_from on mdv_from.MatterID = ctm.MatterID and mdv_from.detailfieldid = ctfm.OriginatingDetailFieldID
		Inner Join DetailFields df_from on df_from.detailfieldid = mdv_from.detailfieldid and df_from.questiontypeid = 4
		Inner Join LookupListItems luli_from on luli_from.LookupListItemID = mdv_from.ValueInt
		Inner Join LookupListItems luli_to on luli_to.LookupListID = df.LookupListID and luli_to.itemvalue = luli_from.itemvalue
		Where mdv_from.DetailValue <> ''

	End

	If @Direction = 'From'
	Begin

		Insert Into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		Select ctm.ClientID, ctm.LeadID, ctm.MatterID, ctfm.OriginatingDetailFieldID, ''
		From CaseTransferMapping ctm 
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'From' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 2
		Where ctm.caseid = @CaseID
		and NOT EXISTS (Select * From MatterDetailValues Where MatterID = ctm.newMatterID and DetailFieldID = ctfm.DestinationDetailFieldID)

		Insert Into LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		Select ctm.ClientID, ctm.LeadID, ctfm.DestinationDetailFieldID, ''
		From CaseTransferMapping ctm
		Inner Join _CaseTransferDetailFieldMap ctfm on ctfm.ClientRelationshipID = ctm.ClientRelationshipID and (ctfm.Direction = 'From' Or ctfm.Direction = 'Both')
		Inner Join DetailFields df on df.DetailFieldID = ctfm.DestinationDetailFieldID and df.leadormatter = 1
		Where ctm.CaseID = @CaseID
		and NOT EXISTS (Select * From LeadDetailValues Where LeadID = ctm.NewLeadID and DetailFieldID = ctfm.DestinationDetailFieldID)

	End

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransferDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CaseTransferDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransferDetailValues] TO [sp_executeall]
GO
