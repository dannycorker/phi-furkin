SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By:	Chris Townsend
-- Date:		13th May 2008
-- Purpose:		Get a name/value collection of valid sql queries (ie: queries that have 
				columns from which to build series) for use in a drop down list
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart__GetSqlQueryNameValueCollectionByClientID]
(
	@ClientID int   
)
AS

				SET ANSI_NULLS OFF
				
				SELECT DISTINCT(QueryID), QueryTitle
				FROM SqlQuery sq
				INNER JOIN SqlQueryColumns sqc ON sq.queryid = sqc.sqlqueryid
				WHERE sq.ClientID = @ClientID
				AND sq.FolderID > -1 -- not in recycle bin
				ORDER BY QueryTitle

				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			







GO
GRANT VIEW DEFINITION ON  [dbo].[Chart__GetSqlQueryNameValueCollectionByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart__GetSqlQueryNameValueCollectionByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart__GetSqlQueryNameValueCollectionByClientID] TO [sp_executeall]
GO
