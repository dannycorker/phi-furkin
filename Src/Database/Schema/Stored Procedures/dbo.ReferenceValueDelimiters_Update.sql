SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReferenceValueDelimiters table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDelimiters_Update]
(

	@ReferenceValueDelimiterID int   ,

	@Delimiter char (1)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReferenceValueDelimiters]
				SET
					[Delimiter] = @Delimiter
				WHERE
[ReferenceValueDelimiterID] = @ReferenceValueDelimiterID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDelimiters_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Update] TO [sp_executeall]
GO
