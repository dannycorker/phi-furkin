SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DroppedOutCustomers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_Update]
(

	@CustomerID int   ,

	@ClientID int   ,

	@TitleID int   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@EmailAddress varchar (255)  ,

	@DayTimeTelephoneNumber varchar (50)  ,

	@HomeTelephone varchar (50)  ,

	@MobileTelephone varchar (50)  ,

	@CompanyTelephone varchar (50)  ,

	@WorksTelephone varchar (50)  ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (200)  ,

	@PostCode varchar (10)  ,

	@HasDownloaded bit   ,

	@DownloadedOn datetime   ,

	@AquariumStatusID int   ,

	@ClientStatusID int   ,

	@Test bit   ,

	@CompanyName varchar (100)  ,

	@Occupation varchar (100)  ,

	@Employer varchar (100)  ,

	@ReminderEmailSentDate datetime   ,

	@ReminderSmsSentDate datetime   ,

	@CountryID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DroppedOutCustomers]
				SET
					[ClientID] = @ClientID
					,[TitleID] = @TitleID
					,[FirstName] = @FirstName
					,[MiddleName] = @MiddleName
					,[LastName] = @LastName
					,[EmailAddress] = @EmailAddress
					,[DayTimeTelephoneNumber] = @DayTimeTelephoneNumber
					,[HomeTelephone] = @HomeTelephone
					,[MobileTelephone] = @MobileTelephone
					,[CompanyTelephone] = @CompanyTelephone
					,[WorksTelephone] = @WorksTelephone
					,[Address1] = @Address1
					,[Address2] = @Address2
					,[Town] = @Town
					,[County] = @County
					,[PostCode] = @PostCode
					,[HasDownloaded] = @HasDownloaded
					,[DownloadedOn] = @DownloadedOn
					,[AquariumStatusID] = @AquariumStatusID
					,[ClientStatusID] = @ClientStatusID
					,[Test] = @Test
					,[CompanyName] = @CompanyName
					,[Occupation] = @Occupation
					,[Employer] = @Employer
					,[ReminderEmailSentDate] = @ReminderEmailSentDate
					,[ReminderSmsSentDate] = @ReminderSmsSentDate
					,[CountryID] = @CountryID
				WHERE
[CustomerID] = @CustomerID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Update] TO [sp_executeall]
GO
