SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-07-20
-- Description:	Import file from _ImportFile
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ImportLeadDoc] 
@EventTypeID int,
@LeadID int,
@CaseID int,
@MatterID int,
@WhoUploaded int,
@FileName varchar(200),
@Path varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ClientID int,
			@LeadDocumentID int,
			@LeadEventID int,
			@DocumentBlob varbinary(max),
			@SelectStatement varchar(max),
			@UploadDateTime datetime
			
	Declare @Doc TABLE (DocumentBlob varbinary(max))

	IF @LeadID IS NULL and @CaseID IS NULL and @MatterID IS NOT NULL
	BEGIN
	
		Select @CaseID = CaseID, @LeadID = LeadID, @ClientID = ClientID
		From Matter m WITH (NOLOCK)
		Where m.MatterID = @MatterID
	
	END

	Select @SelectStatement = 'Select BulkColumn 
	FROM OPENROWSET(Bulk N''' + @Path + '\' + @FileName + ''', SINGLE_BLOB) AS BLOB'
	
	Insert Into @Doc
	Exec (@SelectStatement)

	Select @DocumentBlob = DocumentBlob
	From @Doc
	
	If DATALENGTH(@DocumentBlob) > 0
	BEGIN
		/*
		Insert Into LeadDocument (ClientID,LeadID,DocumentTypeID,LeadDocumentTitle,UploadDateTime,WhoUploaded,DocumentBLOB,FileName,EmailBLOB,DocumentFormat,EmailFrom,EmailTo,CcList,BccList,ElectronicSignatureDocumentKey,Encoding,ContentFormat,ZipFormat)
		Select @ClientID,@LeadID,NULL,'Imported File',dbo.fn_GetDate_Local(),@WhoUploaded,@DocumentBlob,@FileName,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL

		Select @LeadDocumentID = SCOPE_IDENTITY()
		*/
		SELECT @UploadDateTime = dbo.fn_GetDate_Local()
			
		EXEC @LeadDocumentID = dbo._C00_CreateLeadDocument @ClientID = @ClientID, @LeadID = @LeadID, @DocumentBLOB = @DocumentBlob, @LeadDocumentTitle = 'Imported File', @WhoUploaded = @WhoUploaded, @FileName = @FileName, @UploadDateTime = @UploadDateTime 

		INSERT INTO [dbo].[LeadEvent]([ClientID],[LeadID],[WhenCreated],[WhoCreated],[Cost],[Comments],[EventTypeID],[NoteTypeID],[FollowupDateTime],[WhenFollowedUp],[AquariumEventType],[NextEventID],[CaseID],[LeadDocumentID],[NotePriority],[DocumentQueueID],[EventDeleted],[WhoDeleted],[DeletionComments],[ContactID],[BaseCost],[DisbursementCost],[DisbursementDescription],[ChargeOutRate],[UnitsOfEffort],[CostEnteredManually],[IsOnHold],[HoldLeadEventID])
		Select @ClientID,@LeadID,dbo.fn_GetDate_Local(),@WhoUploaded,0.00,'',@EventTypeID,NULL,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local(),NULL,NULL,@CaseID,@LeadDocumentID,NULL,NULL,0,NULL,NULL,NULL,0.00,NULL,NULL,0.00,0,0,NULL,NULL

		Select @LeadEventID = SCOPE_IDENTITY()

		IF @LeadDocumentID IS NOT NULL
		BEGIN
	
				Print '@ECHO Added Document ' + @FileName + ' To Lead ' + Convert(Varchar(20), @LeadID) + ' Case ' + Convert(Varchar(20), @CaseID)  + ' Matter ' + Convert(Varchar(20), @MatterID)  + ' LeadDocumentID ' + Convert(Varchar(20), @LeadDocumentID)  + ' LeadEventID ' + Convert(Varchar(20), @LeadEventID) + '
			mkdir ' + @Path + '\C_' + convert(varchar,@ClientID) + '\' + convert(varchar(10),dbo.fn_GetDate_Local(),120) + ' 
			@Echo ' + @FileName + ',' + Convert(Varchar(20), @LeadID) + ',' +  Convert(Varchar(20), @CaseID) + ',' + Convert(Varchar(20), @MatterID) + ',' + Convert(Varchar(20), @LeadDocumentID) + ',' + Convert(Varchar(20), @LeadEventID) + '>>importlog.csv
			move ' + @Path + '\' + @FileName + ' ' + @Path + '\C_' + convert(varchar,@ClientID) + '\' + convert(varchar(10),dbo.fn_GetDate_Local(),120)
		
		END
		ELSE
		BEGIN
			
				Print '@ECHO Failed to add ' + @FileName + ' To Lead ' + Convert(Varchar(20), @LeadID) + ' Case ' + Convert(Varchar(20), @CaseID)  + ' Matter ' + Convert(Varchar(20), @MatterID)  + ' LeadDocumentID ' + Convert(Varchar(20), @LeadDocumentID)  + ' LeadEventID ' + Convert(Varchar(20), @LeadEventID)
		
		END

	END
	ELSE
	BEGIN
	
		Print 'Could Not add document ' + @FileName + ' To Lead ' + Convert(Varchar(20), @LeadID) + ' Case ' + Convert(Varchar(20), @CaseID) + ' as the File Size is 0KB.'
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ImportLeadDoc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ImportLeadDoc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ImportLeadDoc] TO [sp_executeall]
GO
