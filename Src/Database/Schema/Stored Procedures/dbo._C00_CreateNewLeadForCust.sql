SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new Lead, Case and Matter for a Customer 
-- 2010-04-19 JWG Added WhoCreated
-- 2010-09-21 JWG Added fnRefLetterFromCaseNum for leads with more than 26 matters
-- 2011-02-16 ACE Added @CreateLeadRegardless flag and removed needless IF Exixts clause (replaced with ELSE)
-- 2014-02-11 JWG Added index hint for the Lead table (described below)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateNewLeadForCust]
(
	@CustomerID INT,
	@LeadTypeID INT,
	@ClientID INT,
	@WhoCreated INT,
	@CreateLeadRegardless INT = 0,
	@ReturnNewMatterID INT = 0,
	@AddProcessStart INT = 0
)

AS
BEGIN
	SET NOCOUNT ON;

	IF @CustomerID IS NULL RETURN

	DECLARE	@myERROR INT,
			@NewMatterID INT,
			@NewLeadID INT,
			@NewCaseID INT,
			@CaseNum INT,
			@ReturnValue INT 

	/*
		JWG 2014-02-11 Sql Server has decided the best query plan is to fetch all leads for this lead type, 
		then search for the customer, taking 30 minutes or more to import a file of new customers, 
		because 750000 records are found for lead type 1309 each time (and that number is rising all the time).
		
		The index hint INDEX(IX_Lead_Customer) forces it to use the more appropriate index on CustomerID, which finds approx 1 row instantly.
	*/
	IF NOT EXISTS (SELECT * FROM Lead WITH (NOLOCK, INDEX(IX_Lead_Customer)) WHERE CustomerID = @CustomerID AND LeadTypeID = @LeadTypeID) OR @CreateLeadRegardless = 1
	BEGIN

		INSERT INTO [dbo].[Lead]([ClientID],[LeadRef],[CustomerID],[LeadTypeID],[AquariumStatusID],[ClientStatusID],[BrandNew],[Assigned],[AssignedTo],[AssignedBy],[AssignedDate],[RecalculateEquations],[Password],[Salt],[WhenCreated])
		VALUES(@ClientID, '',@CustomerID,@LeadTypeID,2,NULL,0,0,NULL,NULL,NULL,0,NULL,NULL,dbo.fn_GetDate_Local())

		SELECT @NewLeadID = SCOPE_IDENTITY()

		SELECT @CaseNum = 1

	END
	ELSE
	BEGIN

		SELECT @NewLeadID = LeadID
		FROM Lead WITH (NOLOCK) 
		WHERE CustomerID = @CustomerID 
		AND LeadTypeID = @LeadTypeID
		ORDER BY LeadID DESC

		SELECT @CaseNum = COUNT(*) + 1
		FROM Cases WITH (NOLOCK) 
		WHERE LeadID = @NewLeadID

	END

	INSERT INTO [dbo].[Cases]([LeadID],[ClientID],[CaseNum],[CaseRef],[ClientStatusID],[AquariumStatusID],[DefaultContactID], WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES(@NewLeadID,@ClientID,@CaseNum,'Case ' + CONVERT(VARCHAR(10), @CaseNum),NULL,2,NULL, @WhoCreated, dbo.fn_GetDate_Local(), @WhoCreated, dbo.fn_GetDate_Local())

	SELECT @NewCaseID = SCOPE_IDENTITY()

	INSERT INTO [dbo].[Matter]([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID])
	VALUES(@ClientID, NULL, @CustomerID, @NewLeadID, 1, dbo.fnRefLetterFromCaseNum(@CaseNum), 0, @NewCaseID)

	SELECT @NewMatterID = SCOPE_IDENTITY()

	IF @AddProcessStart = 1
	BEGIN
	
		EXEC dbo._C00_AddProcessStart @NewCaseID, @WhoCreated
	
	END

	IF @ReturnNewMatterID = 1
	BEGIN
		
		SELECT @ReturnValue = @NewMatterID
	
	END
	ELSE
	BEGIN
	
		SELECT @ReturnValue = @NewLeadID
	
	END

	RETURN @ReturnValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewLeadForCust] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateNewLeadForCust] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewLeadForCust] TO [sp_executeall]
GO
