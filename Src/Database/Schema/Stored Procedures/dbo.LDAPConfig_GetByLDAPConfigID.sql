SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LDAPConfig table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LDAPConfig_GetByLDAPConfigID]
(

	@LDAPConfigID int   
)
AS


				SELECT
					[LDAPConfigID],
					[ClientID],
					[QueryString],
					[QueryAccountName],
					[QueryAccountPassword],
					[Enabled],
					[Domain]
				FROM
					[dbo].[LDAPConfig] WITH (NOLOCK) 
				WHERE
										[LDAPConfigID] = @LDAPConfigID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_GetByLDAPConfigID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_GetByLDAPConfigID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_GetByLDAPConfigID] TO [sp_executeall]
GO
