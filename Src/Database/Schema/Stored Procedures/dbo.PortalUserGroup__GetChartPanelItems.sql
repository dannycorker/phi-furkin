SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 2010-07-13
-- Description:	Gets a list of the panel items available to a particular PortalUserGroup
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserGroup__GetChartPanelItems]
(
	@PortalUserGroupID int,
	@ClientID int	
)
	
AS

SET ANSI_NULLS OFF
				
SELECT p.PanelID, p.PanelName, pic.*, 1 AS [Accessible]
FROM dbo.PanelItemCharting pic WITH (NOLOCK)
INNER JOIN dbo.Panels p WITH (NOLOCK) ON pic.PanelID = p.PanelID
INNER JOIN dbo.PortalUserGroupPanelItemChartingAccess pugpica WITH (NOLOCK) ON pugpica.PanelItemChartingID = pic.PanelItemChartingID
	AND pugpica.PortalUserGroupID = @PortalUserGroupID
	AND pugpica.HasAccess = 1
WHERE pic.ClientID = @ClientID

ORDER BY p.PanelID

SELECT @@ROWCOUNT
SET ANSI_NULLS ON




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetChartPanelItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup__GetChartPanelItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetChartPanelItems] TO [sp_executeall]
GO
