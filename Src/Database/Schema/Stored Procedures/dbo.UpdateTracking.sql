SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.UpdateTracking    Script Date: 08/09/2006 12:22:42 ******/

CREATE PROCEDURE [dbo].[UpdateTracking]
@TrackingID int,
@TrackingName nvarchar(100),
@TrackingDescription nvarchar(255),
@StartDate datetime,
@EndDate datetime,
@ClientQuestionnaireID int,
@ClientID int

as

Update Tracking
SET  TrackingName = @TrackingName,
TrackingDescription = @TrackingDescription,
StartDate = @StartDate,
EndDate = @EndDate,
ClientQuestionnaireID = @ClientQuestionnaireID,
ClientID = @ClientID
where TrackingID = @TrackingID




GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateTracking] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateTracking] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateTracking] TO [sp_executeall]
GO
