SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Create Linked Leads and Complete Setup
-- 2020-02-12 CPS for JIRA AAG-106	| Assume "Yes" for @UsesPolicyAdminProcess, otherwise this ends up as NULL
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-05-15 ALM for AAG-607	| Added a call to _C600_Mediate_QueueForVision
-- 2020-07-09 ALM Added insert into OneVision
-- 2020-09-21 ALM Added additional insert into OneVision for Wellness
-- 2020-10-07 ALM Added additional insert into OneVision for ExamFee 
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_155196]
	@LeadEventID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)

	DECLARE	/* Specific to procedure */
			 @VarcharDate					VARCHAR(10)
			,@ValueInt						INT
			,@EventComments					VARCHAR(2000)
			,@UsesPolicyAdminProcess		BIT = 1 -- 2020-02-12 CPS for JIRA AAG-106 | Assume "Yes", otherwise this ends up as NULL
			,@ClaimLeadID					INT
			,@ClaimMatterID					INT
			,@ClaimCaseID					INT
			,@RunTime						DATETIME
			,@ColLeadID						INT
			,@ColMatterID					INT
			,@NewCollectionsMatter			INT = 0
			,@ColCaseID						INT
			,@NewLeadEventID				INT
			,@AccountID						INT
			,@NextEventTypeID				INT
			,@RatingDate					DATE
			,@IsNewColCase					INT = 1
			,@PayRef						VARCHAR(100)
			,@RequestType					VARCHAR(8) = 'Customer'
			,@HttpMethod					VARCHAR(4) = 'POST'
			,@OneVisionID					INT
			,@LatestEvent					INT
			,@PetNumber						INT
			,@PetDOB						DATE
			,@AgeInMonths					INT
			,@PetAge						VARCHAR(25)
			,@Policyinceptiondate			DATE
			,@CustomerInVision				INT
			,@VisionCustomerID				INT
			,@VisionAddressID				INT
			,@PurchasedProductID			INT
			,@WrittenPremiumID				INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	/* Complete initial set-up including spawning linked leads */
	IF @EventTypeID IN (155196, -- Create linked leads and complete set-up
						158768, /* 2018-03-09 Mark Beaumont - Create linked leads and complete set up for One Month Free (OMF) which has no collections lead */
						155399) -- Create PA fields for match attempt 

	BEGIN
		-- 2019-10-21 CPS for JIRA LPC-35 | Bypass CalculatePremiumForPeriod if we already have a calculation in place

		/*GPR 2018-05-18*/
		-- 2020-02-12 CPS for JIRA AAG-106 | Bypassed @RenewalDate field in favour of nested functions
		SELECT @VarcharDate = CONVERT(VARCHAR,DATEADD(DAY, +1, dbo.fnGetSimpleDvAsDate(170037,@MatterID) /*Policy End Date*/),121)
		EXEC dbo._C00_SimpleValueIntoField 175307, @VarcharDate, @MatterID, @AqAutomation	-- renewal date	
	
		EXEC dbo._C00_CreateDetailFields '170144,170034,175279', @LeadID

		SELECT @ValueInt = /*Aggressive Tendencies*/
			CASE WHEN EXISTS	(	
						SELECT *  
						FROM TableRows tr WITH (NOLOCK)   
						INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 177126 /*Existing Condition*/  
						INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID AND tdv2.DetailFieldID = 177306 /*Customer Response*/  
						WHERE tr.DetailFieldID = 177128 /*Existing Conditions*/  
						AND dbo.fnGetSimpleDvAsInt(177492,tdv.ResourceListID) = 1 /*Yes Blocks Third Party Liability Cover*/
						AND tdv2.ValueInt = 5144 /*Yes*/  
						AND tr.LeadID = @LeadID
						)
			THEN 5144 /*Yes*/ 
			ELSE 5145 /*No*/ 
			END

		IF @ValueInt = 5144 /*Yes*/
		BEGIN
			EXEC _C600_CreatePolicySectionExclusionRow @MatterID, 148149 /*TPL*/, 74574 /*Aggressive Tendencies*/, @LeadEventID, @WhoCreated
			SELECT @EventComments += 'Third Party Liability cover excluded.' + CHAR(13)+CHAR(10)
		END

		EXEC dbo._C00_SimpleValueIntoField 177452, @ValueInt, @LeadID, @WhoCreated /*Has your pet ever shown aggressive tendencies?*/
		EXEC dbo._C00_SimpleValueIntoField 177469, 74565, @MatterID, @WhoCreated /*Distributor Profile Type = A*/

		-- if customer level affinity not set, set it
		IF dbo.fnGetDv(170144,@CaseID) = ''
		BEGIN
		
			UPDATE caf
			SET DetailValue=rlaf.ResourceListID
			FROM MatterDetailValues cp WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues rlcp WITH (NOLOCK) ON cp.ValueInt=rlcp.ResourceListID AND rlcp.DetailFieldID=175269
			INNER JOIN ResourceListDetailValues rlaf WITH (NOLOCK) ON rlcp.ValueInt = rlaf.ValueInt AND rlaf.DetailFieldID=170127
			INNER JOIN Matter m WITH (NOLOCK) ON cp.MatterID=m.MatterID
			INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
			INNER JOIN CustomerDetailValues caf WITH (NOLOCK) ON l.CustomerID=caf.CustomerID AND caf.DetailFieldID=170144
			WHERE cp.DetailFieldID=170034 AND cp.MatterID=@MatterID	
		
		END

		-- does this affinity use Policy Admin?
		IF dbo.fnGetRLDvAsInt(170144,175277,@CaseID) = 5145 
			SELECT @UsesPolicyAdminProcess = 0

		-- set breed for page title
		EXEC _C600_SAS @WhoCreated, @CustomerID, @LeadID

		-- set current PH and Pet details DFs
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 1, 1
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 2, 1
		
		-- check communication prefs/set defaults if none set
	--	EXEC [dbo].[_C600_CheckCommunicationPreferences] @CustomerID  /*Removed by GPR 2019-11-01*/
		EXEC dbo._C00_SimpleValueIntoField 175360, '', @CustomerID
		
		IF @EventTypeID<>155399  -- not if we are only attempting a customer match
		BEGIN
		
			-- Create a claims lead if one doesn't already exist for this lead
			SELECT @ClaimLeadID=ToLeadID 
			FROM LeadTypeRelationship WITH (NOLOCK) 
			WHERE FromLeadID=@LeadID 
			AND FromLeadTypeID=1492 
			AND ToLeadTypeID=1490

			IF @ClaimLeadID IS NULL
			BEGIN
				/*JEL Added param to apply process start to ensure the claims can be picked up by workflows*/ 
				EXEC @ClaimMatterID = _C00_CreateNewLeadForCust  @CustomerID,1490,@ClientID,@WhoCreated,1,1,1

				SELECT @ClaimLeadID=m.LeadID, @ClaimCaseID=m.CaseID 
				FROM Matter m WITH (NOLOCK) 
				WHERE m.MatterID = @ClaimMatterID
				
				-- add the start event
				SELECT @RunTime=DATEADD(SECOND,10,dbo.fn_GetDate_Local())
				EXEC AutomatedTask__RunNow 19841, 0, @RunTime, 0
				
				-- link new claim lead to policy admin lead
				INSERT INTO dbo.LeadTypeRelationship (FromLeadTypeID, ToLeadTypeID, FromLeadID, ToLeadID, FromMatterID, ToMatterID, ClientID) 
				VALUES (1492, 1490, @LeadID, @ClaimLeadID, @MatterID, @ClaimMatterID, @ClientID)
				
				SELECT @ValueInt = SCOPE_IDENTITY()
				EXEC _C00_LogIt 'Info', '_C600_LeadTypeRelationships', '_C600_SAE', @ValueInt, 58540 /*Cathal Sherry*/

				-- We need to update the first auto created case on the claims lead to say Claim 1 rather than case 1		
				DECLARE @CaseRef VARCHAR(500)
				SELECT @CaseRef = CaseDisplayName + ' 1'
				FROM dbo.LeadType WITH (NOLOCK) 
				WHERE LeadTypeID = 1490
				AND ClientID = @ClientID

				UPDATE dbo.Cases
				SET CaseRef = ISNULL(@CaseRef, 'Case 1')
				WHERE CaseID = @ClaimCaseID
				AND ClientID = @ClientID

			END
			ELSE
			BEGIN

				-- if this is Policy admin case 1 check that LeadTypeRelationship has matter links in it. Add them if not
				IF EXISTS ( SELECT * FROM Cases ca WITH (NOLOCK) 
							INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromLeadID=ca.LeadID 
								AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1490
								AND ltr.FromMatterID IS NULL 
							WHERE ca.CaseID=@CaseID AND ca.CaseNum=1 )
				BEGIN
							
					SELECT TOP 1 @ClaimMatterID=m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.LeadID=@ClaimLeadID ORDER BY m.MatterID
					
					UPDATE ltr
					SET FromMatterID=@MatterID,ToMatterID=@ClaimMatterID
					FROM Cases ca WITH (NOLOCK) 
					INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromLeadID=ca.LeadID 
						AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1490
						AND ltr.FromMatterID IS NULL 
					WHERE ca.CaseID=@CaseID AND ca.CaseNum=1
			
				END
				
			END	
			
			/* 2018-03-09 Mark Beaumont - Prevent collection lead from being set up for One Month Free (OMF) */
			IF @EventTypeID <> 158768
			AND 76488 <> ( 
				SELECT c.WhoCreated 
				FROM Cases c WITH (NOLOCK) 
				WHERE c.CaseID = @CaseID 
			) /*Case is not created by Claims API User*/
			BEGIN
				
			/*JEL make an Account for every Polciy as collections for multi should not be grouped*/ 
				---- Create a collections lead if one doesn't already exist for this customer or new case if this lead/matter has new account details
				SELECT @ColLeadID = LeadID 
				FROM Lead WITH (NOLOCK) 
				WHERE CustomerID = @CustomerID 
				AND LeadTypeID = 1493
				
				IF @ColLeadID IS NULL
				BEGIN

					SELECT @NewCollectionsMatter = 1
					
					EXEC @ColMatterID = _C00_CreateNewLeadForCust @CustomerID,1493,@ClientID,@WhoCreated,1,1,0

					SELECT @ColLeadID=m.LeadID, @ColCaseID=m.CaseID 
					FROM Matter m WITH (NOLOCK) 
					WHERE m.MatterID=@ColMatterID
					
					---- link new collections lead to policy admin lead
					INSERT INTO dbo.LeadTypeRelationship (FromLeadTypeID, ToLeadTypeID, FromLeadID, ToLeadID, FromMatterID, ToMatterID, ClientID) 
					VALUES (1492, 1493, @LeadID, @ColLeadID, @MatterID, @ColMatterID, @ClientID)
					
					--SELECT @ValueInt = SCOPE_IDENTITY()
					--EXEC _C00_LogIt 'Info', '_C600_LeadTypeRelationships', '_C600_SAE', @ValueInt, 58540 /*Cathal Sherry*/
				
				END
				------ this must be a new pet for an existing customer
				ELSE  
				BEGIN
				

					-- if a multi-pet case from the web, find the collections case already set up for it
					IF dbo.fnGetDvAsInt(175280,@CaseID)=72330 /*Resolved - QandB MATCH*/ 
					   AND dbo.fnGetSimpleDvAsInt(170114,@MatterID) <> 69930 /* Only merge collections for a Card Payer*/ 
					BEGIN
						
						/*This is a multi pet lead, so find the card transaction that paid for this policy. We can use that to get to the correct col matterID*/ 
						SELECT @ColMatterID = a.ObjectID FROM CardTransactionPolicy p WITH (NOLOCK) 
						INNER JOIN CardTransactionPolicy pp WITH (NOLOCK) on pp.CardTransactionID = p.CardTransactionID 
						INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = pp.PAmatterID AND mdv.DetailFieldID = 177074
						INNER JOIN PurchasedProduct pur WITH (NOLOCK) on pur.PurchasedProductID = mdv.ValueInt
						INNER JOIN Account a WITH (NOLOCK) on a.AccountID = pur.AccountID
						AND p.PAMatterID = @MatterID 
						AND pp.PAMatterID <> @MatterID 					
						
						EXEC dbo._C00_SimpleValueIntoField 175279, @ColMatterID, @MatterID, @AqAutomation						
										
					END		

					IF @ColMatterID IS NULL
					BEGIN
				
						SELECT @NewCollectionsMatter = 1

						EXEC @ColMatterID = _C00_CreateNewCaseForLead @ColLeadID, @WhoCreated, 1
						SELECT @ColCaseID = m.CaseID
						FROM Matter m WITH (NOLOCK) 
						WHERE m.MatterID = @ColMatterID
					END

					-- link collections matter to policy admin matter
					INSERT INTO dbo.LeadTypeRelationship (FromLeadTypeID, ToLeadTypeID, FromLeadID, ToLeadID, FromMatterID, ToMatterID, ClientID) VALUES  
					(1492, 1493, @LeadID, @ColLeadID, @MatterID, @ColMatterID, @ClientID) /*GPR 2018-07-04 added FromLeadID, ToLeadID, adjusted 2018-07-05 (GPR)*/
					
					SELECT @ValueInt = SCOPE_IDENTITY()
					EXEC _C00_LogIt 'Info', '_C600_LeadTypeRelationships', '_C600_SAE', @ValueInt, 58540 /*Cathal Sherry*/
											
				END
				IF @ColCaseID IS NOT NULL
				BEGIN
					
					-- add process start
					EXEC @NewLeadEventID=_C00_AddProcessStart @ColCaseID, @WhoCreated

					-- copy payment details over to the new matter
					INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
					SELECT @ClientID,
							@ColLeadID,
							@ColMatterID, 
							CASE WHEN polmdv.DetailFieldID = 170103 THEN 175460 --Payment Reference			
								 WHEN polmdv.DetailFieldID = 170104 THEN 170188 --Account Name
								 WHEN polmdv.DetailFieldID = 170105 THEN 170189 --Account Sort Code
								 WHEN polmdv.DetailFieldID = 170106 THEN 170190 --Account Number
								 WHEN polmdv.DetailFieldID = 170114 THEN 170115 --Payment Method
								 WHEN polmdv.DetailFieldID = 170170 THEN 170187 --Bank Name
								 WHEN polmdv.DetailFieldID = 170171 THEN 170191 --Customer Reference Number
								 WHEN polmdv.DetailFieldID = 170172 THEN 170192 --Credit Card Holder Name
								 WHEN polmdv.DetailFieldID = 170173 THEN 170193 --Card Identifier
								 WHEN polmdv.DetailFieldID = 170174 THEN 170194 --Credit Card Expire Date
								 WHEN polmdv.DetailFieldID = 170175 THEN 170185 --Date of first payment
								 WHEN polmdv.DetailFieldID = 175449 THEN 175407 --Override date of first or next payment
								 WHEN polmdv.DetailFieldID = 175450 THEN 175409 -- Explicit consent
								 WHEN polmdv.DetailFieldID = 179904 THEN 179907 -- Authority Required For account
								 WHEN polmdv.DetailFieldID = 179905 THEN 179908 -- Policy Holder is Not Account Holder
								 END,
							polmdv.DetailValue						   
					FROM MatterDetailValues polmdv WITH (NOLOCK) 
					WHERE polmdv.DetailFieldID IN (170114,170175,170104,170103,170170,170105,170106,170171,170172,170173,170174,175449,175450,179904,179905)
					AND polmdv.MatterID = @MatterID
											
					-- add payment type & interval event (to set the collections off down the correct sub-process or process not required)
					EXEC dbo._C00_CreateDetailFields '170114,170176', @LeadID
					IF dbo.fnGetRLDvAsInt(170144,175278,@CaseID) = 5144
					BEGIN
						--SELECT @NextEventTypeID= aq 1,3,150127 -- prepare account for collections  -- BEU ******
						SELECT @NextEventTypeID=
							CASE meth.ValueInt WHEN 69930 THEN 150180 -- DD Collections
														  ELSE 150182 -- CC Collections
														  END
						FROM MatterDetailValues meth WITH (NOLOCK) 
						INNER JOIN MatterDetailValues intv WITH (NOLOCK) ON meth.MatterID=intv.MatterID AND intv.DetailFieldID=170176
						WHERE meth.MatterID=@MatterID AND meth.DetailFieldID=170114	
					END	
					ELSE
					BEGIN
					
						SELECT @NextEventTypeID=155198	-- Collections process not required
						
					END
										
					-- clear payment details these from their temporary home in the policy admin lead 
					UPDATE src 
					SET DetailValue='' 
					FROM MatterDetailValues src WITH (NOLOCK) 
					INNER JOIN DetailFields srcdf WITH (NOLOCK) ON src.DetailFieldID=srcdf.DetailFieldID AND srcdf.DetailFieldPageID=19020
					INNER JOIN DetailFields snkdf WITH (NOLOCK) ON srcdf.FieldName=snkdf.FieldName AND snkdf.DetailFieldPageID IN (19018,19057)
					WHERE src.MatterID=@MatterID AND src.DetailFieldID NOT IN (170114,170176,170168) -- retain payment method, interval and day
				
					SELECT @AccountID = NULLIF(dbo.fnGetDvAsInt(176973,@ColCaseID),0)
				
					-- create account in new billing system				
					IF @AccountID is NULL
					BEGIN
						EXEC @AccountID=_C600_BillingSystem_CreateAccountRecord @MatterID, @AqAutomation 
					END
	PRINT '@AccountID = ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL')
					-- add collections event
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, @ColCaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, @NextEventTypeID, @AqAutomation, 1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.CaseID = c.CaseID AND le.LeadEventID=@NewLeadEventID
				
				END
				ELSE
				BEGIN
				
					SELECT @IsNewColCase=0, @ColCaseID=CaseID 
					FROM Matter WITH (NOLOCK) WHERE MatterID=@ColMatterID
				
				END
				
				/*JEL 2018-05-24 If we have the card transaction for this customer, update it with the account we have just created*/ 
				IF EXISTS (SELECT * FROM CardTransaction ct WITH (NOLOCK) 
						   /*JEL 2019-01-09 removed cardtransactionpolicy join as we do not create this for worldpay records*/ 
						   --  INNER JOIN CardTransactionPolicy cp WITH (NOLOCK) on cp.CardTransactionID = ct.CardTransactionID
						   WHERE ct.CustomerID = @CustomerID 
						   AND ISNULL(ct.AccountID,0) =0) 
						  -- AND cp.PAMatterID = @MAtterID) 
				BEGIN 
				
					UPDATE ct 
					SET AccountID = @AccountID  
					FROM CardTransaction ct 
					INNER JOIN CardTransactionPolicy cp on cp.CardTransactionID = ct.CardTransactionID
					WHERE ct.CustomerID = @CustomerID 
					AND ISNULL(ct.AccountID,0) = 0  
					AND cp.PAMatterID = @MatterID
				
				END 		   
				
	PRINT '@UsesPolicyAdminProcess = ' + ISNULL(CONVERT(VARCHAR,@UsesPolicyAdminProcess),'NULL')
				IF @UsesPolicyAdminProcess = 1
				BEGIN
							
					-- 2019-10-21 CPS for JIRA LPC-35 | Bypass CalculatePremiumForPeriod if we already have a calculation in place
					-- 2020-02-05 CPS for JIRA AAG-91	| Replace WrittenPremium references with PurchasedProductPaymentScheduleDetail
					IF NOT EXISTS ( 
						SELECT * 
						FROM dbo.WrittenPremium wp WITH (NOLOCK) 
						WHERE wp.MatterID = @MatterID 
					)
					BEGIN

						-- add first row to premium adjustment history now that the collections link has been made
						SELECT @RatingDate=dbo.fnGetDvAsDate (170036,@CaseID) -- Start Date
						EXEC @WrittenPremiumID = dbo._C00_1273_Policy_CalculatePremiumForPeriod @MatterID, @Leadeventid, @RatingDate, 8 

					END
					ELSE
					BEGIN

						SELECT @EventComments += 'Rules Engine Calculation Complete' + CHAR(13)+CHAR(10)

					END		
					
					-- update collections documentation fields
					EXEC _C600_Collections_RefreshDocumentationFields @ColMatterID
	print '@colmatterid = ' + ISNULL(CONVERT(VARCHAR,@colmatterid),'NULL')
					-- Create purchased product record

					EXEC @PurchasedProductID = _C600_BillingSystem_CreatePurchasedProductAndSchedule @MatterID, @AqAutomation

					/*ALM 2020-11-13*/
					UPDATE wp
					SET wp.PurchasedProductID = @PurchasedProductID
					FROM WrittenPremium wp WITH (NOLOCK)
					WHERE wp.WrittenPremiumID = @WrittenPremiumID

					-- Account type - DD
					IF dbo.fnGetDvAsInt(170114,@CaseID)=69930
					BEGIN

						-- set mandate reference field
						SELECT @PayRef=dbo.fn_C600_GetMandateReference(@MatterID)
						EXEC dbo._C00_SimpleValueIntoField 175460, @PayRef, @ColMatterID, @AqAutomation

					END

					-- reset current account details DF
					EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 3, 1

				END
			END		/* 2018-03-09 Mark Beaumont - END OF Prevent collection lead from being set up for One Month Free (OMF) */	
		END	
					
		SET @ValueInt = NULL
		EXEC @ValueInt = _C600_SetDocumentFields @MatterID , @WhoCreated, 'NewBusiness' /*CS 2017-05-24.  Didn't realise that PrepDocumentFields existed, sorry!*/
		EXEC _C600_PrepDocumentFields @MatterID , @WhoCreated			
		
		SELECT @EventComments += ISNULL(CONVERT(VARCHAR,@ValueInt),'NULL') + ' document fields set.'
		
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		
		/*GPR 2020-10-05 removed*/
		--insert a matter detail value for field 180257 and set it to '1'
		--insert into MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		--values (@ClientID, @leadid, @matterid, 180257,'true')
		
		IF '' <> ( SELECT cu.CustomerRef FROM CUstomers cu WITH (NOLOCK) WHERE cu.CustomerID = @CustomerID )
		BEGIN
			SELECT	 @HttpMethod  = 'PUT'
		END

		/*NG 2020-09-16 Add Pet Age at Inception*/
		/* Generate Pet Age In Years */

		SELECT 
			@Policyinceptiondate = dbo.fnGetSimpleDvAsDate(170035, @MatterID) /*Grab policy inception date*/
			,@PetDOB = dbo.fnGetSimpleDvAsDate(144274, @LeadID)
			,@AgeInMonths = DATEDIFF(MONTH,@PetDOB, DATEADD(DAY, 1-DATEPART(DAY,@PetDOB), CONVERT(DATE,@Policyinceptiondate))) /*Handles whether the pet is one month younger than calculated when taking days into account*/

		SELECT @PetAge = CONVERT(VARCHAR, CONVERT(DECIMAL (10,0), ROUND((@AgeInMonths / 12),0,1))) + ' years ' /*set pet age in years*/

		/*Write Pet Age to Field */

		EXEC dbo._C00_SimpleValueIntoField 314262,@PetAge,@LeadID,@WhoCreated /*pet age at inception*/

		SELECT TOP 1 @LatestEvent = EventTypeID, @WhoCreated = WhoCreated
		FROM LeadEvent WITH (NOLOCK) 
		WHERE CaseID = @CaseID
		AND EventTypeID IN (150055)
		ORDER BY WhenCreated DESC 

		SELECT @CustomerInVision = 1 
		FROM OneVision ov WITH (NOLOCK)
		WHERE ov.CustomerID = @CustomerID 
		AND ov.VisionCustomerID IS NOT NULL 

		IF @CustomerInVision = 1 
		BEGIN 

			SELECT TOP 1 @VisionCustomerID = ov.VisionCustomerID, @VisionAddressID = ov.VisionAddressID
			FROM OneVision ov WITH (NOLOCK) 
			WHERE ov.CustomerID = @CustomerID

			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			VALUES (@VisionCustomerID, NULL, NULL, NULL, @CustomerID, @LeadID, @MatterID, NULL, @VisionAddressID, 'Policy') 

			IF dbo.fnGetSimpleDvAsInt(314235, @MatterID) > 0 
			BEGIN 
			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			VALUES (@VisionCustomerID, NULL, NULL, NULL, @CustomerID, @LeadID, @MatterID, NULL, @VisionAddressID, 'Wellness') 
			END

			IF dbo.fnGetSimpleDvAsInt(313931, @LeadID) = 5144
			BEGIN 
			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			VALUES (@VisionCustomerID, NULL, NULL, NULL, @CustomerID, @LeadID, @MatterID, NULL, @VisionAddressID, 'ExamFee') 
			END

		END 
		ELSE
		BEGIN
			/*Insert into OneVision table*/
			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			VALUES (NULL, NULL, NULL, NULL, @CustomerID, @LeadID, @MatterID, NULL, NULL, 'Policy') 

			SELECT @OneVisionID = SCOPE_IDENTITY()

			IF dbo.fnGetSimpleDvAsInt(314235, @MatterID) > 0 
			BEGIN
			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			VALUES (NULL, NULL, NULL, NULL, @CustomerID, @LeadID, @MatterID, NULL, NULL, 'Wellness') 
			END

			IF dbo.fnGetSimpleDvAsInt(313931, @LeadID) = 5144
			BEGIN 
			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			VALUES (@VisionCustomerID, NULL, NULL, NULL, @CustomerID, @LeadID, @MatterID, NULL, @VisionAddressID, 'ExamFee') 
			END

			SELECT @PetNumber = mdv.ValueInt 
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			WHERE mdv.DetailFieldID = 175453
			AND mdv.MatterID = @MatterID

			IF @PetNumber <= 2
			BEGIN
				EXEC _C600_Mediate_QueueForVision @RequestType, @HttpMethod, @LeadEventID, @OneVisionID
			END
		END
				
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155196] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_155196] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155196] TO [sp_executeall]
GO
