SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeAccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAccessRule_Insert]
(

	@EventTypeAccessRuleID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@AccessRuleID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[EventTypeAccessRule]
					(
					[ClientID]
					,[EventTypeID]
					,[AccessRuleID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@AccessRuleID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @EventTypeAccessRuleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRule_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_Insert] TO [sp_executeall]
GO
