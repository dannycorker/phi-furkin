SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryCriteriaSet table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSet_Insert]
(

	@SqlQueryCriteriaSetID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SetName varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[SqlQueryCriteriaSet]
					(
					[ClientID]
					,[SqlQueryID]
					,[SetName]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@SetName
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@SourceID
					)
				-- Get the identity value
				SET @SqlQueryCriteriaSetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSet_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Insert] TO [sp_executeall]
GO
