SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderFileAbort table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_Insert]
(

	@DataLoaderFileAbortID int    OUTPUT,

	@ClientID int   ,

	@DateTimeFileAborted datetime   ,

	@ReasonForAbort varchar (1000)  ,

	@DataLoaderFileID int   ,

	@FileAbortedOn datetime   ,

	@LastRowImported int   
)
AS


				
				INSERT INTO [dbo].[DataLoaderFileAbort]
					(
					[ClientID]
					,[DateTimeFileAborted]
					,[ReasonForAbort]
					,[DataLoaderFileID]
					,[FileAbortedOn]
					,[LastRowImported]
					)
				VALUES
					(
					@ClientID
					,@DateTimeFileAborted
					,@ReasonForAbort
					,@DataLoaderFileID
					,@FileAbortedOn
					,@LastRowImported
					)
				-- Get the identity value
				SET @DataLoaderFileAbortID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Insert] TO [sp_executeall]
GO
