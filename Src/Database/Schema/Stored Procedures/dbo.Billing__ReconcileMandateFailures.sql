SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-03-13
-- Description:	Reconcile mandate failures
-- MODS
-- JEL 2018-03-22 Fixed add event functionality 
-- JEL 2018-03-24 Added BACsReturnProcessingID to updates
-- JEL 2018-03-24 Added CaseID and EventToAdd to updates 
-- ACE 2019-07-15 Handle E differently as we might just get an update of name..
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[Billing__ReconcileMandateFailures]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CountryID INT

	/*GPR 2020-03-02 for AAG-202*/
	SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
	WHERE cl.ClientID = @ClientID
	
	IF @CountryID = 14 /*Australia*/
	BEGIN

		RETURN
	
	END
	ELSE
	BEGIN



	/*
		1. Update the BACSMandateFailures table set the account ID and account mandate id based on bacs return processing 
			not matching the same state.

		2. Update the bacs mandate table with the corrected new status.
		3. Apply events to the required cases
	*/

	/*
		NOTES:
		1. Import Status. 1 = New, 2 = Processed OK, 3 = Failed to Reconcile
	*/

	DECLARE @BACSMandateFailureID INT,
			@TestID INT = 0,
			@FileType VARCHAR(50),
			@AccountNumber VARCHAR(20),
			@SortCode VARCHAR(10),
			@NewAccountNumber VARCHAR(20),
			@NewSortCode VARCHAR(10),
			@FailureReason VARCHAR(200),
			@FailureCode VARCHAR(20),
			@Reference VARCHAR(200),
			@AccountID INT,
			@AccountMandateID INT,
			@CustomerID INT,
			@ObjectID INT,
			@NewAccountID INT,
			@NewAccountMandateID INT,
			@BACSReturnProcessingID INT,
			@EventToApply INT,
			@CaseID INT,
			@NewMandateStatusID INT,
			@AddEventAs INT,
			@BACSProcessedDate VARCHAR(50),
			@RunasUserID INT,
			@NewAccountRequired BIT = 0,
			@NewName VARCHAR(200),
			@FriendlyName VARCHAR(200),
			@ClientAccountID INT,
			@EventTypeID INT,
			@LeadEventID INT,
			@LeadID INT, 
			@DateForEvent DATE = dbo.fn_GetDate_Local(),
			@FileName VARCHAR(200),
			@CommentsToApply VARCHAR(1500)
			
	SELECT @RunAsUserID	= dbo.fn_C00_GetAutomationUser(@ClientID)

	DECLARE @UpdatedPaymentScheduleRecords TABLE (CustomerPaymentScheduleID INT)

	BEGIN TRY
		BEGIN TRAN

		SELECT TOP 1 @BACSMandateFailureID = bmf.BACSMandateFailureID,
			@FileType = bmf.FileType,
			@AccountNumber = bmf.AccountNumber,
			@SortCode = bmf.SortCode,
			@NewAccountNumber = bmf.NewAccountNumber,
			@NewSortCode = bmf.NewSortCode,
			@FailureCode = bmf.FailureCode,
			@Reference = bmf.Reference,
			@NewName = bmf.NewName,
			@FileName = bmf.FileName
		FROM BACSMandateFailures bmf
		WHERE bmf.ImportStatusID = 1
		AND bmf.ClientID = @ClientID
		ORDER BY bmf.BACSMandateFailureID ASC

		WHILE @BACSMandateFailureID > @TestID
		BEGIN

			SELECT @AccountID = NULL,
					@AccountMandateID = NULL,
					@ObjectID = NULL,
					@BACSReturnProcessingID = NULL,
					@NewMandateStatusID = NULL,
					@EventToApply = NULL,
					@AddEventAs = NULL

			SELECT TOP 1 @BACSReturnProcessingID = brp.BACSReturnProcessingID,
						@NewMandateStatusID = brp.MandateStatusID,
						@EventToApply = brp.EventToApply,
						@AddEventAs = ISNULL(brp.AddEventAs,@RunAsUserID),
						@NewAccountRequired = ISNULL(brp.NewAccountRequired, 0)
			FROM BACSReturnProcessing brp WITH (NOLOCK)
			WHERE brp.ReasonCodeType = @FileType /*AUDDIS/ADDACS ETC*/
			AND brp.FailureCode = @FailureCode
			AND brp.ClientID IN (0,@ClientID)
			ORDER BY brp.ClientID DESC /*If i have one for my client use it or use client 0's*/
			
			IF @FileType = 'AWACS'
			BEGIN 
			
				SELECT TOP 1 @AccountID = a.AccountID, 
							@ObjectID = a.ObjectID,
							@CustomerID = a.CustomerID,
							@FriendlyName = a.FriendlyName,
							@ClientAccountID = a.ClientAccountID
				FROM Account a WITH (NOLOCK) 
				WHERE a.Sortcode = @SortCode
				AND a.AccountNumber = @AccountNumber
				AND a.Reference = @Reference 
				AND a.ClientID = @ClientID
				ORDER BY a.AccountID DESC

			END 
			ELSE 
			BEGIN 
			
				SELECT TOP 1 @AccountID = a.AccountID, 
							@AccountMandateID = am.AccountMandateID, 
							@ObjectID = a.ObjectID,
							@CustomerID = a.CustomerID,
							@FriendlyName = a.FriendlyName,
							@ClientAccountID = a.ClientAccountID
				FROM AccountMandate am WITH (NOLOCK)
				INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = am.AccountID
				WHERE a.Sortcode = @SortCode
				AND a.AccountNumber = @AccountNumber
				AND am.MandateStatusID <> CASE WHEN @FileType IN ('AUDDIS', 'ADDACS') AND @FailureCode IN ('3','C', 'E')
					THEN 4 ELSE @NewMandateStatusID 
					END
					/*If we need to reinstate one then we can*/
				AND a.Reference = @Reference 
				AND a.ClientID = @ClientID
				ORDER BY am.AccountMandateID DESC
			
			END 

			IF @AccountID > 0
			BEGIN

				/*We have a mandate we can update lets do it!*/
				UPDATE bmf
				SET AccountID = @AccountID, AccountMandateID = @AccountMandateID
				FROM BACSMandateFailures bmf
				WHERE bmf.BACSMandateFailureID = @BACSMandateFailureID

				IF @FailureCode = 'E' AND ISNULL(@NewSortCode, '') = '' AND ISNULL(@NewAccountNumber, '') = ''
				BEGIN

					/*
						ACE - If the status code is "E" (change of details) and the account itself hasnt changed it's a name only change.
						Do not create a new account, do not re-enable any mandates, update the existing account with the new name and stop there.
						@NewMandateStatusID being -1 will cause it to not fall into the 1,3 or 4 traps.
					*/
					SELECT @NewAccountRequired = 0, @NewMandateStatusID = -1

					UPDATE a
					SET AccountHolderName = @NewName, Comments = ISNULL(Comments, '') + ' Updated Name', WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunasUserID
					FROM Account a 
					WHERE a.AccountID = @AccountID

				END

				IF @NewMandateStatusID IN (1,3)
				BEGIN

					IF @NewAccountRequired = 1
					BEGIN

						/*Ok, first thing to do here would be to set the current account to inactive and do the same for any mandates for that account*/
						UPDATE a
						SET Active = 0,
							WhenModified = dbo.fn_GetDate_Local(), 
							WhoModified = @RunasUserID, 
							Comments = 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. From file ' + ISNULL(@FileName,'NULL')
						FROM Account a
						WHERE a.AccountID = @AccountID

						UPDATE am
						SET MandateStatusID = 5, 
							WhenModified = dbo.fn_GetDate_Local(), 
							WhoModified = @RunasUserID, 
							Comments = 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. From file ' + ISNULL(@FileName,'NULL')
						FROM AccountMandate am 
						WHERE am.AccountID = @AccountID
						AND am.MandateStatusID IN (1,2,3)

						INSERT INTO Account ([ClientID], [CustomerID], [AccountHolderName], [FriendlyName], [AccountNumber], [Sortcode], [Reference], [CardToken], [MaskedCardNumber], [ExpiryMonth], [ExpiryYear], [ExpiryDate], [AccountTypeID], [Active], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified], [NextPaymentDate], [NextPaymentTotal], [CustomerPaymentScheduleID], [Address1], [Address2], [Town], [County], [Postcode], [CountryID], [ObjectID], [ObjectTypeID], [CardType], [FirstName], [LastName], [Bankname], [AccountSubTypeID], [SourceID], [ClientAccountID], Comments)
						VALUES (@ClientID, @CustomerID, @NewName, @FriendlyName, @NewAccountNumber, @NewSortCode, @Reference, NULL, NULL, NULL, NULL, null, 1, 1, @RunasUserID, dbo.fn_GetDate_Local(), @RunasUserID, dbo.fn_GetDate_Local(), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @ObjectID, 2, NULL, NULL, NULL, NULL, NULL, @AccountID, @ClientAccountID, 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. Status 3 from file ' + ISNULL(@FileName,'NULL'))

						SELECT @NewAccountID = SCOPE_IDENTITY()

						IF @NewMandateStatusID = 3
						BEGIN

							/*2018-06-28 ACE (Ta for the heads up JL) - FirstAcceptablePaymentDate Set to today as payments can be requested immediately..*/
							INSERT INTO AccountMandate (ClientID, CustomerID, AccountID, MandateStatusID, DateRequested, FirstAcceptablePaymentDate, DirectDebitInstructionID, Reference, FailureCode, FailureDate, WhoCreated, WhenCreated, Comments)
							VALUES (@ClientID, @CustomerID, @NewAccountID, @NewMandateStatusID, @BACSProcessedDate, dbo.fn_GetDate_Local(), NULL, @Reference, NULL, NULL, @RunasUserID, dbo.fn_GetDate_Local(), 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. Status 3 from file ' + ISNULL(@FileName,'NULL'))

						END

						UPDATE cps
						SET AccountID = @NewAccountID, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunasUserID, ActualCollectionDate = CASE WHEN ActualCollectionDate < CONVERT(DATE,dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),10)) THEN CONVERT(DATE,dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(), 10)) ELSE ActualCollectionDate END
						OUTPUT inserted.CustomerPaymentScheduleID INTO @UpdatedPaymentScheduleRecords (CustomerPaymentScheduleID)
						FROM CustomerPaymentSchedule cps 
						WHERE cps.AccountID = @AccountID
						AND cps.PaymentStatusID = 1 /*Only do new!*/
						AND cps.CustomerID = @CustomerID

						UPDATE ppps
						SET AccountID = @NewAccountID, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunasUserID, ActualCollectionDate = CASE WHEN ActualCollectionDate < CONVERT(DATE,dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),10)) THEN CONVERT(DATE,dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(), 10)) ELSE ActualCollectionDate END
						FROM PurchasedProductPaymentSchedule ppps 
						INNER JOIN @UpdatedPaymentScheduleRecords c ON c.CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
						WHERE ppps.AccountID = @AccountID
						AND ppps.PaymentStatusID = 1 /*Only do new!*/
						AND ppps.CustomerID = @CustomerID

						UPDATE pp
						SET AccountID = @NewAccountID, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunasUserID
						FROM PurchasedProduct pp
						WHERE pp.CustomerID = @CustomerID
						AND pp.AccountID = @AccountID
						AND EXISTS (
							SELECT *
							FROM PurchasedProductPaymentSchedule ppps
							WHERE ppps.PurchasedProductID = pp.PurchasedProductID
							AND ppps.AccountID = @NewAccountID
						)

						/*The mandate requests should now take care of the rest.*/
						EXEC Account__SetDateAndAmountOfNextPayment @NewAccountID

					END
					ELSE
					BEGIN

						/*There can only be one mandate that is active!*/
						IF NOT EXISTS (
							SELECT *
							FROM AccountMandate am WITH (NOLOCK)
							WHERE am.AccountID = @AccountID
							AND am.MandateStatusID = 3
						)
						BEGIN

								INSERT INTO AccountMandate (ClientID, CustomerID, AccountID, MandateStatusID, DateRequested, FirstAcceptablePaymentDate, DirectDebitInstructionID, Reference, FailureCode, FailureDate, WhoCreated, WhenCreated, Comments)
								VALUES (@ClientID, @CustomerID, @AccountID, @NewMandateStatusID, @BACSProcessedDate, NULL, NULL, @Reference, NULL, NULL, @RunasUserID, dbo.fn_GetDate_Local(), 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. Status 3 from file ' + ISNULL(@FileName,'NULL'))

								/*
								SELECT @NewAccountID = SCOPE_IDENTITY()
								*/

								/*The mandate requests should now take care of the rest.*/
								EXEC Account__SetDateAndAmountOfNextPayment @AccountID

						END

						/*Make sure the account is now active*/
						UPDATE a
						SET Active = 1, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunasUserID
						FROM Account a
						WHERE a.AccountID = @AccountID

					END

				END

				IF @NewMandateStatusID = 4
				BEGIN

					/*
						Cancel the existing account mandate and set the account to failed. 
						If we are going to re-lodge we will re-activate the account at that point or we run the risk of going in circles...
					*/
					UPDATE am
					SET MandateStatusID = @NewMandateStatusID, 
						WhenModified = dbo.fn_GetDate_Local(), 
						WhoModified = @RunasUserID, 
						Comments = 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. Status 4 from file ' + ISNULL(@FileName,'NULL')
					FROM AccountMandate am 
					WHERE am.AccountMandateID = @AccountMandateID

					UPDATE a
					SET Active = 0, 
						WhenModified = dbo.fn_GetDate_Local(), 
						WhoModified = @RunasUserID, 
						Comments = 'Came in from an ' + ISNULL(@FileType, 'NULL') + ' file type. Status 4 from file ' + ISNULL(@FileName,'NULL')
					FROM Account a
					WHERE a.AccountID = @AccountID

				END

				EXEC Account__SetDateAndAmountOfNextPayment @AccountID

				/*Finally Add any events... Make sure we havent added the same one in the last hour...*/
				SELECT TOP 1 @LeadEventID = c.LatestNonNoteLeadEventID, @EventTypeID = le.EventTypeID, @LeadID = m.LeadID, @CaseID = c.CaseID
				FROM Matter m WITH (NOLOCK)
				INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = m.CaseID
				INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestNonNoteLeadEventID
				WHERE m.MatterID = @ObjectID

				IF NOT EXISTS (
					SELECT *
					FROM LeadEvent le WITH (NOLOCK)
					WHERE le.CaseID = @CaseID 
					AND le.EventTypeID = @EventToApply
					AND le.WhenCreated > DATEADD(HH, -1, dbo.fn_GetDate_Local())
				)
				BEGIN
					PRINT 'Ac'
					PRINT @AccountID 
					PRINT 'LE'
					PRINT @LeadEventID 
					PRINT 'ca'
					PRINT @CaseID 
					PRINT 'Who'
					PRINT @RunAsUserID
					
					SELECT @CommentsToApply = 'File Type: ' + ISNULL(@FileType, '') + '</br>AcNo: ' + ISNULL(RIGHT(@AccountNumber, 4), '') + '</br>New Ac No ' + ISNULL(CASE WHEN LEN(@NewAccountNumber)> 4 THEN RIGHT(@NewAccountNumber, 4) ELSE @NewAccountNumber END, '') + '</br>Failure Code ' + ISNULL(@FailureCode, '') + '</br>Reference ' + ISNULL(@Reference, '') + '</br>New Name ' + ISNULL(@NewName, '') + '</br>File Name ' + ISNULL(@FileName, '')

					EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueueFull @ClientID, @LeadID, @CaseID, @LeadEventID, @EventTypeID, @EventToApply, -1, @DateForEvent, @RunAsUserID, @CommentsToApply

					IF @FailureCode = '1'
					BEGIN
					
						EXEC _C00_AddANote @CaseID,866,@CommentsToApply,1,@RunAsUserID,0,1,@DateForEvent 
					
					END
					ELSE
					BEGIN 
					
						EXEC _C00_AddANote @CaseID,865,@CommentsToApply,1,@RunAsUserID,0,1,@DateForEvent 
					
					END 


				END
				/*JEL Added BACsReturnProcessingID to update*/ 
				UPDATE bmf
				SET ImportStatusID = 2, ImportedDate = dbo.fn_GetDate_Local(), Comments = 'Located Account ' + CONVERT(VARCHAR, @AccountID) + ', Mandate ' + CONVERT(VARCHAR, @AccountMandateID), BACsReturnProcessingID = @BACSReturnProcessingID,
				EventToApply = @EventToApply, CaseID = @CaseID				
				FROM BACSMandateFailures bmf
				WHERE bmf.BACSMandateFailureID = @BACSMandateFailureID

			END
			ELSE
			BEGIN

				UPDATE bmf
				SET ImportStatusID = 3, ImportedDate = dbo.fn_GetDate_Local(), Comments = 'Could not locate mandate/account'
				FROM BACSMandateFailures bmf
				WHERE bmf.BACSMandateFailureID = @BACSMandateFailureID

			END

			SELECT @TestID = @BACSMandateFailureID

			SELECT TOP 1 @BACSMandateFailureID = bmf.BACSMandateFailureID,
				@FileType = bmf.FileType,
				@AccountNumber = bmf.AccountNumber,
				@SortCode = bmf.SortCode,
				@NewAccountNumber = bmf.NewAccountNumber,
				@NewSortCode = bmf.NewSortCode,
				@FailureCode = bmf.FailureCode,
				@Reference = bmf.Reference,
				@NewName = bmf.NewName
			FROM BACSMandateFailures bmf
			WHERE bmf.ImportStatusID = 1
			AND bmf.BACSMandateFailureID > @BACSMandateFailureID
			AND bmf.ClientID = @ClientID
			ORDER BY bmf.BACSMandateFailureID ASC

		END

		COMMIT

	END TRY
	BEGIN CATCH
		
		ROLLBACK

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		
	END CATCH

	EXEC dbo._C00_LogIt 'Info', '_C321_ImportARUDD', 'Proc Complete', '', @RunasUserID

	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__ReconcileMandateFailures] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__ReconcileMandateFailures] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__ReconcileMandateFailures] TO [sp_executeall]
GO
