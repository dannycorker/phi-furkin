SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryCriteriaSetMember table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryID]
(

	@SqlQueryID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryCriteriaSetMemberID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryCriteriaSetID],
					[SqlQueryColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryCriteriaSetMember] WITH (NOLOCK) 
				WHERE
					[SqlQueryID] = @SqlQueryID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryID] TO [sp_executeall]
GO
