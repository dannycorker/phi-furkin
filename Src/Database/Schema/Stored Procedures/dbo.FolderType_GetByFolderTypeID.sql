SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FolderType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FolderType_GetByFolderTypeID]
(

	@FolderTypeID int   
)
AS


				SELECT
					[FolderTypeID],
					[FolderTypeName],
					[FolderTypeDescription]
				FROM
					[dbo].[FolderType] WITH (NOLOCK) 
				WHERE
										[FolderTypeID] = @FolderTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_GetByFolderTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FolderType_GetByFolderTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_GetByFolderTypeID] TO [sp_executeall]
GO
