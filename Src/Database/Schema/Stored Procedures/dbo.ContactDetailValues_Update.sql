SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ContactDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ContactDetailValues_Update]
(

	@ContactDetailValueID int   ,

	@ClientID int   ,

	@ContactID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt nchar (10)  ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ContactDetailValues]
				SET
					[ClientID] = @ClientID
					,[ContactID] = @ContactID
					,[DetailFieldID] = @DetailFieldID
					,[DetailValue] = @DetailValue
					,[ErrorMsg] = @ErrorMsg
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[SourceID] = @SourceID
				WHERE
[ContactDetailValueID] = @ContactDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ContactDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ContactDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ContactDetailValues_Update] TO [sp_executeall]
GO
