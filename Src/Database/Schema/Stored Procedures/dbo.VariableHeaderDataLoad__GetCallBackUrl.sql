SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-05-2015
-- Description:	Gets the call back url for the variable header data load
-- =============================================
CREATE PROCEDURE [dbo].[VariableHeaderDataLoad__GetCallBackUrl]

	@ClientID INT

AS
BEGIN
	
	
	SET NOCOUNT ON;
	
	DECLARE @DetailFieldID INT
	SELECT @DetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4203 -- call back url field
    
    SELECT DetailValue CallbackUrl FROM ClientDetailValues WITH (NOLOCK) 
    WHERE ClientID=@ClientID AND DetailFieldID=@DetailFieldID
    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__GetCallBackUrl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad__GetCallBackUrl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__GetCallBackUrl] TO [sp_executeall]
GO
