SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the TableRows table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_Update]
(

	@TableRowID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@MatterID int   ,

	@DetailFieldID int   ,

	@DetailFieldPageID int   ,

	@DenyEdit bit   ,

	@DenyDelete bit   ,

	@CustomerID int   ,

	@CaseID int   ,

	@ClientPersonnelID int   ,

	@ContactID int   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[TableRows]
				SET
					[ClientID] = @ClientID
					,[LeadID] = @LeadID
					,[MatterID] = @MatterID
					,[DetailFieldID] = @DetailFieldID
					,[DetailFieldPageID] = @DetailFieldPageID
					,[DenyEdit] = @DenyEdit
					,[DenyDelete] = @DenyDelete
					,[CustomerID] = @CustomerID
					,[CaseID] = @CaseID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[ContactID] = @ContactID
					,[SourceID] = @SourceID
				WHERE
[TableRowID] = @TableRowID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Update] TO [sp_executeall]
GO
