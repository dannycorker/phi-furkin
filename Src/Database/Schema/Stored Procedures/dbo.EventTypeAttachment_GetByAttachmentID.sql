SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAttachment table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_GetByAttachmentID]
(

	@AttachmentID int   
)
AS


				SELECT
					[AttachmentID],
					[EventTypeID],
					[AttachmentEventTypeID],
					[All],
					[AttachmentEventGroupID]
				FROM
					[dbo].[EventTypeAttachment] WITH (NOLOCK) 
				WHERE
										[AttachmentID] = @AttachmentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_GetByAttachmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_GetByAttachmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_GetByAttachmentID] TO [sp_executeall]
GO
