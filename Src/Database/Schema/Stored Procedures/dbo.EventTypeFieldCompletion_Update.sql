SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeFieldCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_Update]
(

	@EventTypeFieldCompletionID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@DetailFieldID int   ,

	@InsertOptionID int   ,

	@DeleteOptionID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeFieldCompletion]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[DetailFieldID] = @DetailFieldID
					,[InsertOptionID] = @InsertOptionID
					,[DeleteOptionID] = @DeleteOptionID
				WHERE
[EventTypeFieldCompletionID] = @EventTypeFieldCompletionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Update] TO [sp_executeall]
GO
