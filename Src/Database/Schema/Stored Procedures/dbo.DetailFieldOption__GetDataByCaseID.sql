SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-10
-- Description:	DetailFieldOption__GetDataByLeadID
-- =============================================
Create PROCEDURE [dbo].[DetailFieldOption__GetDataByCaseID]
@ClientID int,
@CaseID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, c.CaseID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 11
	INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = @CaseID
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID and l.LeadTypeID = df.LeadTypeID
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM CaseDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CaseID = c.CaseID
		AND cdv.DetailFieldID = dfo.DetailFieldID
	)

	SELECT dfo.DetailFieldID, df.FieldCaption, cdv.DetailValue, dfo.FieldOrder, dfo.Mandatory, ca.CaseID, ca.LeadID, df.LeadOrMatter
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 11
	INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = @CaseID
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = ca.LeadID  and l.LeadTypeID = df.LeadTypeID
	LEFT JOIN CaseDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = df.DetailFieldID and cdv.CaseID = ca.CaseID
	WHERE dfo.ClientID = @ClientID
	ORDER BY dfo.FieldOrder

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldOption__GetDataByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByCaseID] TO [sp_executeall]
GO
