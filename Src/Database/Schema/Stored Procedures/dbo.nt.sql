SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-08-07
-- Description:	List all NoteTypes for a client
-- =============================================
CREATE PROCEDURE [dbo].[nt] 
	@ClientID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.NoteType nt WITH (NOLOCK) 
	WHERE nt.ClientID = @ClientID
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[nt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[nt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[nt] TO [sp_executeall]
GO
