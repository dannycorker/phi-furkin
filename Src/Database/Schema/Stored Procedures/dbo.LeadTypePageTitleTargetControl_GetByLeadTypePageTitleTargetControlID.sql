SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitleTargetControl table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleTargetControl_GetByLeadTypePageTitleTargetControlID]
(

	@LeadTypePageTitleTargetControlID int   
)
AS


				SELECT
					[LeadTypePageTitleTargetControlID],
					[ClientID],
					[LeadTypeID],
					[LastParsed]
				FROM
					[dbo].[LeadTypePageTitleTargetControl] WITH (NOLOCK) 
				WHERE
										[LeadTypePageTitleTargetControlID] = @LeadTypePageTitleTargetControlID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_GetByLeadTypePageTitleTargetControlID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleTargetControl_GetByLeadTypePageTitleTargetControlID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_GetByLeadTypePageTitleTargetControlID] TO [sp_executeall]
GO
