SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_GetByLeadID]
(

	@LeadID int   
)
AS


				SELECT
					[MatterDetailValueID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[OriginalDetailValueID],
					[OriginalLeadID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[MatterDetailValues] WITH (NOLOCK) 
				WHERE
										[LeadID] = @LeadID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_GetByLeadID] TO [sp_executeall]
GO
