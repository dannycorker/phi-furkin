SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Send Confirmation of Purchase
-- NG  2020-09-07 Populate State Variance RL
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_156594]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@EventComments					VARCHAR(2000)
			,@OutputString					VARCHAR(2000) = ''
			,@LeadIDvarchar					VARCHAR(1000)
			,@LeadRefs						VARCHAR(1000)
			,@CountryID						INT

		DECLARE @TempLeadIds TABLE (ID int)


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


		IF @EventTypeID = 156594 /*Send Confirmation of Purchase*/
		BEGIN
		
		
		EXEC dbo._C00_CreateDetailFields '170144,170034,175279,314016', @LeadID
		
		-- if customer level affinity not set, set it
		IF dbo.fnGetDv(170144,@CaseID) = ''
		BEGIN
		
			UPDATE caf
			SET DetailValue=rlaf.ResourceListID
			FROM MatterDetailValues cp WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues rlcp WITH (NOLOCK) ON cp.ValueInt=rlcp.ResourceListID AND rlcp.DetailFieldID=175269
			INNER JOIN ResourceListDetailValues rlaf WITH (NOLOCK) ON rlcp.ValueInt = rlaf.ValueInt AND rlaf.DetailFieldID=170127
			INNER JOIN Matter m WITH (NOLOCK) ON cp.MatterID=m.MatterID
			INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
			INNER JOIN CustomerDetailValues caf WITH (NOLOCK) ON l.CustomerID=caf.CustomerID AND caf.DetailFieldID=170144
			WHERE cp.DetailFieldID=170034 AND cp.MatterID=@MatterID	
		
		END

		-- if customer level state variance RL not set, set it. NG 2020-09-07. Updated for Canada/US NG 2021-04-27
		IF (dbo.fnGetDv(314016,@CaseID) = '' OR dbo.fnGetDv(314016,@CaseID) = 0)

		SELECT @CountryID = cl.CountryID
		FROM Clients cl WITH (NOLOCK) WHERE cl.ClientID = @ClientID

		BEGIN
			IF @CountryID = 233
			BEGIN
		
				UPDATE csv
				SET DetailValue = rldv.ResourceListID
				FROM Customers cu WITH (NOLOCK) 
				INNER JOIN StateByZip sbz WITH (NOLOCK) on cu.PostCode = sbz.Zip
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.DetailFieldID = 313958 AND rldv.ValueInt = sbz.StateID
				INNER JOIN CustomerDetailValues csv WITH (NOLOCK) ON cu.CustomerID = csv.CustomerID AND csv.DetailFieldID = 314016
				WHERE cu.customerID = @CustomerID
		
			END
			ELSE
			IF @CountryID = 39
			BEGIN
		
				UPDATE csv
				SET DetailValue = rldv.ResourceListID
				FROM Customers cu WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.DetailFieldID = 313957 AND rldv.DetailValue = cu.County
				INNER JOIN CustomerDetailValues csv WITH (NOLOCK) ON cu.CustomerID = csv.CustomerID AND csv.DetailFieldID = 314016
				WHERE cu.customerID = @CustomerID

			END

		EXEC dbo._C00_SimpleValueIntoField 175360, '', @CustomerID

		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		END
		END
		
		IF @EventTypeID in (156594) /*Send confirmation of purchase*/
		BEGIN
			/*
			BUGFIX
			CPS 2017-06-05
			Quote and Buy is creating garbage LeadTypeRelationship links and dev can't work out why.  Delete them.
			*/		
			DELETE ltr
			FROM LeadTypeRelationship ltr WITH ( NOLOCK ) 
			INNER JOIN Lead l WITH ( NOLOCK ) on l.LeadID = ltr.FromLeadID
			WHERE ltr.ToLeadID = @LeadID
			AND l.CustomerID <> @CustomerID
			/*!! NO SQL HERE !!*/
			SELECT @EventComments += CONVERT(VARCHAR,@@RowCount) + ' LeadTypeRelationships repaired.' + CHAR(13)+CHAR(10)
		
			EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1

			--clear out the list of pet names and policy numbers at customer level as we only want to send the policy confirmation email to the list of policies we gather below
			EXEC _C00_SimpleValueIntoField 180177,'',@CustomerID,@WhoCreated
		
			--cast the current leadid as a string
			SELECT @LeadIDvarchar = CAST(@leadid AS VARCHAR)
			SELECT @LeadIDvarchar = '%' + @LEADIDVARCHAR + '%'

			--find all the leadID's within the transaction and store;
			SELECT @LeadRefs = qs.MatterIDs FROM  _C600_QuoteSessions qs WITH ( NOLOCK )
			WHERE qs.LeadIDs LIKE @LeadIDvarchar

			--now fetch the lead for each leadref and insert into a temp table
			INSERT INTO @TempLeadIds
			SELECT Leadid FROM Lead WITH (NOLOCK) where LeadRef in 
			(SELECT * from dbo.fnTableOfValuesFromCSV(@LeadRefs)) --This select splits the comma sperated list into multiple table rows  

			--now get petref + petname + </br>
			SELECT @OutputString += ldvPetName.DetailValue + ' : ' + l.LeadRef + CHAR(13)+CHAR(10)
			FROM Lead l WITH ( NOLOCK ) 
			INNER JOIN dbo.LeadDetailValues ldvPetName WITH ( NOLOCK ) on ldvPetName.LeadID = l.LeadID AND ldvPetName.DetailFieldID = 144268   /*Pet Name DF ID*/
			WHERE l.LeadTypeID = 1492 /*Policy Admin*/
			and l.LeadID in (SELECT * FROM @TempLeadIds)

			--stamp it into a field on customer level ready for referencing in the policy confirmation email
			EXEC _C00_SimpleValueIntoField 180177,@OutputString,@CustomerID,@WhoCreated
		END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_156594] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_156594] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_156594] TO [sp_executeall]
GO
