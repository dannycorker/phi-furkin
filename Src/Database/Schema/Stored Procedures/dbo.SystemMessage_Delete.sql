SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SystemMessage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessage_Delete]
(

	@SystemMessageID int   
)
AS


				DELETE FROM [dbo].[SystemMessage] WITH (ROWLOCK) 
				WHERE
					[SystemMessageID] = @SystemMessageID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessage_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Delete] TO [sp_executeall]
GO
