SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-01-25
-- Description:	Add an expense to an irish insolvency customer
-- =============================================
CREATE PROCEDURE [dbo].[_C00_IISSA_XML_AddExpense]
	 @CustomerID		INT
	,@ExpenseType		VARCHAR(2000)
	,@OtherDetails		VARCHAR(2000)
	,@Amount			VARCHAR(2000)
	,@Comment			VARCHAR(2000)
	,@PrimaryApplicant	INT = 1
	,@IsGeneralExpense	BIT	= 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @TableRowID				INT
			,@ClientID					INT
			,@LeadTypeID				INT = 0
			,@ResoucesLeadTypeID		INT
			,@ThirdPartyFieldGroupID	INT = 85
			,@TableDetailFieldID		INT 
			,@TableDetailFieldPageID	INT
			,@PrimaryApplicantLuli		INT
			,@TableThirdPartyFieldID	INT

	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH ( NOLOCK )
	WHERE cu.CustomerID = @CustomerID
	
	IF @ExpenseType = 'SocialInclusionAndParticipation'
	BEGIN
		SELECT @ExpenseType = 'Social Inclusion & Participation'
	END
	ELSE
	IF @ExpenseType = 'Transport'
	BEGIN
		SELECT @ExpenseType = 'Travel (Public/Transport)'
	END
	ELSE
	IF @ExpenseType = 'SavingsAndContingencies'
	BEGIN
		SELECT @ExpenseType = 'Savings & Contingencies'
	END
	
	SELECT @TableThirdPartyFieldID = CASE	@IsGeneralExpense 
									WHEN 1 THEN 
												CASE WHEN @PrimaryApplicant = 1 THEN 2859 ELSE 1395 END 
									ELSE 2376 END 

	SELECT	 @TableDetailFieldID = df.DetailFieldID, @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM	DetailFields df WITH ( NOLOCK )
	WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, @TableThirdPartyFieldID )  
	
	SELECT @ResoucesLeadTypeID = lt.LeadTypeID
	FROM LeadType lt WITH ( NOLOCK ) 
	WHERE lt.ClientID = @ClientID 
	AND lt.LeadTypeName like '%Resource%'

	IF @TableDetailFieldID is NULL or @TableDetailFieldPageID is NULL
	/*or @IsGeneralExpense = 1 /*CS Temporary 2015-03-03*/*/
	BEGIN
		RETURN
	END

	DECLARE @ColumnFieldMapping TABLE ( DetailFieldID INT, DetailValue VARCHAR(2000) )

	/*Find the appropriate column fields to populate*/
	INSERT @ColumnFieldMapping ( DetailFieldID, DetailValue )
	SELECT 
		df.DetailFieldID
		,
		CASE 
			WHEN fm.ThirdPartyFieldID IN (3366,4067)	THEN CONVERT(VARCHAR,ISNULL(li.LookupListItemID,0))
			WHEN fm.ThirdPartyFieldID IN (3367,4069)	THEN @Amount
			WHEN fm.ThirdPartyFieldID IN (3368,4070)	THEN @Comment
			WHEN fm.ThirdPartyFieldID IN (4076)	THEN CONVERT(VARCHAR,ISNULL(li.LookupListItemID,0))
		END
	FROM DetailFields df WITH ( NOLOCK ) 
	INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = df.DetailFieldID AND fm.ThirdPartyFieldGroupID = 85
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on ( fm.ThirdPartyFieldID IN (3366,4067)
														AND li.LookupListID = df.LookupListID 
														AND (li.ItemValue = @ExpenseType OR REPLACE(li.ItemValue, ' ', '') = @ExpenseType) )
													or
													( fm.ThirdPartyFieldID = 4076
														AND li.LookupListID = df.LookupListID 
														AND li.ItemValue = CASE @PrimaryApplicant WHEN 1 THEN 'Applicant' WHEN 2 THEN 'Partner' ELSE 'Joint' END  )
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID

	IF @IsGeneralExpense = 1
	BEGIN

		/*2015-04-27 ACE Added the below.*/
		SELECT @TableRowID = tdv.TableRowID
		FROM TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN @ColumnFieldMapping c ON c.DetailFieldID = tdv.DetailFieldID and tdv.DetailValue = c.DetailValue 
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID
		WHERE tdv.CustomerID = @CustomerID
		AND tdv.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 4067)
		AND tr.DetailFieldID = @TableDetailFieldID
		
		UPDATE TableDetailValues
		SET DetailValue = @Amount
		WHERE TableRowID = @TableRowID
		AND DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 82, 4069)
	
		--SELECT *, @TableRowID
		--FROM @ColumnFieldMapping
	
	END
	ELSE
	BEGIN

		/*Create a table row in the relevant field*/
		INSERT TableRows ( ClientID, CustomerID, DetailFieldID, DetailFieldPageID )
		VALUES ( @ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID )
		
		SELECT @TableRowID = SCOPE_IDENTITY()

		/*Populate the values for which we have a mapping*/
		INSERT TableDetailValues ( ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue )
		SELECT @ClientID, @CustomerID, @TableRowID, cfm.DetailFieldID, cfm.DetailValue
		FROM @ColumnFieldMapping cfm 
		WHERE cfm.DetailFieldID > 0
		AND cfm.DetailValue > ''
	
	END

	/*Complete the table row*/
	--EXEC _C00_CompleteTableRow @TableRowID, NULL, 0, 0

	/*Reutrn the table RowID*/
	RETURN @TableRowID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IISSA_XML_AddExpense] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_IISSA_XML_AddExpense] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IISSA_XML_AddExpense] TO [sp_executeall]
GO
