SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-11-13
-- Description:	List all event groups for a client, or all clients
-- =============================================
CREATE PROC [dbo].[eg] 
	@ClientID int = null 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT	cl.ClientID,
			cl.CompanyName,
			eg.EventGroupID,
			eg.EventGroupName,
			eg.EventGroupDescription,
			COUNT(em.EventGroupMemberID) [Events],
			eg.WhoCreated,
			cp.UserName,
			eg.WhenCreated,
			eg.WhoModified,
			eg.WhenModified,
			eg.ChangeNotes,
			eg.SourceID,
			eg.LeadTypeID,
			lt.LeadTypeName
	FROM EventGroup eg WITH (NOLOCK) 
	LEFT JOIN dbo.EventGroupMember em WITH (NOLOCK) on em.EventGroupID = eg.EventGroupID
	INNER JOIN Clients cl WITH (NOLOCK) on cl.ClientID = eg.ClientID
	LEFT JOIN LeadType lt WITH (NOLOCK) on lt.LeadTypeID = eg.LeadTypeID
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = eg.WhoCreated
	WHERE eg.ClientID = @ClientID or @ClientID is null
	GROUP BY cl.ClientID,
			cl.CompanyName,
			eg.EventGroupID,
			eg.EventGroupName,
			eg.EventGroupDescription,
			eg.WhoCreated,
			cp.UserName,
			eg.WhenCreated,
			eg.WhoModified,
			eg.WhenModified,
			eg.ChangeNotes,
			eg.SourceID,
			eg.LeadTypeID,
			lt.LeadTypeName
	ORDER BY cl.ClientID, eg.EventGroupID
END





GO
GRANT VIEW DEFINITION ON  [dbo].[eg] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[eg] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[eg] TO [sp_executeall]
GO
