SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-24
-- Description:	Delete all completed AutomatedEventQueue records for this scheduler
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__SetAllEventsComplete]
	@SchedulerID int, 
	@ServerName varchar(50) 
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Delete all completed AutomatedEventQueue records for this scheduler
	*/
	DELETE dbo.AutomatedEventQueue 
	WHERE SchedulerID = @SchedulerID 
	AND ServerName = @ServerName
	AND LockDateTime IS NOT NULL
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__SetAllEventsComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__SetAllEventsComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__SetAllEventsComplete] TO [sp_executeall]
GO
