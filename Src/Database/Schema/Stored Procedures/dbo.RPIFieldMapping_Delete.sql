SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_Delete]
(

	@RPIFieldMappingID int   
)
AS


				DELETE FROM [dbo].[RPIFieldMapping] WITH (ROWLOCK) 
				WHERE
					[RPIFieldMappingID] = @RPIFieldMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Delete] TO [sp_executeall]
GO
