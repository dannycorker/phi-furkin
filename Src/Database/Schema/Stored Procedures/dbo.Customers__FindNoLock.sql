SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Customers table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers__FindNoLock]
(

	@SearchUsingOR bit   = null ,

	@CustomerID int   = null ,

	@ClientID int   = null ,

	@TitleID int   = null ,

	@IsBusiness bit   = null ,

	@FirstName varchar (100)  = null ,

	@MiddleName varchar (100)  = null ,

	@LastName varchar (100)  = null ,

	@EmailAddress varchar (255)  = null ,

	@DayTimeTelephoneNumber varchar (50)  = null ,

	@DayTimeTelephoneNumberVerifiedAndValid bit   = null ,

	@HomeTelephone varchar (50)  = null ,

	@HomeTelephoneVerifiedAndValid bit   = null ,

	@MobileTelephone varchar (50)  = null ,

	@MobileTelephoneVerifiedAndValid bit   = null ,

	@CompanyTelephone varchar (50)  = null ,

	@CompanyTelephoneVerifiedAndValid bit   = null ,

	@WorksTelephone varchar (50)  = null ,

	@WorksTelephoneVerifiedAndValid bit   = null ,

	@Address1 varchar (200)  = null ,

	@Address2 varchar (200)  = null ,

	@Town varchar (200)  = null ,

	@County varchar (200)  = null ,

	@PostCode varchar (50)  = null ,

	@Website varchar (200)  = null ,

	@HasDownloaded bit   = null ,

	@DownloadedOn datetime   = null ,

	@AquariumStatusID int   = null ,

	@ClientStatusID int   = null ,

	@Test bit   = null ,

	@CompanyName varchar (100)  = null ,

	@Occupation varchar (100)  = null ,

	@Employer varchar (100)  = null ,

	@Fullname varchar (201)  = null ,

	@PhoneNumbersVerifiedOn datetime   = null ,

	@DoNotEmail bit   = null ,

	@DoNotSellToThirdParty bit   = null ,

	@AgreedToTermsAndConditions bit   = null ,

	@DateOfBirth datetime   = null ,

	@DefaultContactID int   = null ,

	@DefaultOfficeID int   = null ,

	@AddressVerified bit   = null ,

	@CountryID int   = null ,

	@SubClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT *
    FROM 
	[dbo].[Customers] with (nolock)
    WHERE 
	 ([CustomerID] = @CustomerID OR @CustomerID is null)
	AND ([ClientID] = @ClientID OR @ClientID is null)
	AND ([TitleID] = @TitleID OR @TitleID is null)
	AND ([IsBusiness] = @IsBusiness OR @IsBusiness is null)
	AND ([FirstName] = @FirstName OR @FirstName is null)
	AND ([MiddleName] = @MiddleName OR @MiddleName is null)
	AND ([LastName] = @LastName OR @LastName is null)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress is null)
	AND ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber OR @DayTimeTelephoneNumber is null)
	AND ([DayTimeTelephoneNumberVerifiedAndValid] = @DayTimeTelephoneNumberVerifiedAndValid OR @DayTimeTelephoneNumberVerifiedAndValid is null)
	AND ([HomeTelephone] = @HomeTelephone OR @HomeTelephone is null)
	AND ([HomeTelephoneVerifiedAndValid] = @HomeTelephoneVerifiedAndValid OR @HomeTelephoneVerifiedAndValid is null)
	AND ([MobileTelephone] = @MobileTelephone OR @MobileTelephone is null)
	AND ([MobileTelephoneVerifiedAndValid] = @MobileTelephoneVerifiedAndValid OR @MobileTelephoneVerifiedAndValid is null)
	AND ([CompanyTelephone] = @CompanyTelephone OR @CompanyTelephone is null)
	AND ([CompanyTelephoneVerifiedAndValid] = @CompanyTelephoneVerifiedAndValid OR @CompanyTelephoneVerifiedAndValid is null)
	AND ([WorksTelephone] = @WorksTelephone OR @WorksTelephone is null)
	AND ([WorksTelephoneVerifiedAndValid] = @WorksTelephoneVerifiedAndValid OR @WorksTelephoneVerifiedAndValid is null)
	AND ([Address1] = @Address1 OR @Address1 is null)
	AND ([Address2] = @Address2 OR @Address2 is null)
	AND ([Town] = @Town OR @Town is null)
	AND ([County] = @County OR @County is null)
	AND ([PostCode] = @PostCode OR @PostCode is null)
	AND ([Website] = @Website OR @Website is null)
	AND ([HasDownloaded] = @HasDownloaded OR @HasDownloaded is null)
	AND ([DownloadedOn] = @DownloadedOn OR @DownloadedOn is null)
	AND ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID is null)
	AND ([ClientStatusID] = @ClientStatusID OR @ClientStatusID is null)
	AND ([Test] = @Test OR @Test is null)
	AND ([CompanyName] = @CompanyName OR @CompanyName is null)
	AND ([Occupation] = @Occupation OR @Occupation is null)
	AND ([Employer] = @Employer OR @Employer is null)
	AND ([Fullname] = @Fullname OR @Fullname is null)
	AND ([PhoneNumbersVerifiedOn] = @PhoneNumbersVerifiedOn OR @PhoneNumbersVerifiedOn is null)
	AND ([DoNotEmail] = @DoNotEmail OR @DoNotEmail is null)
	AND ([DoNotSellToThirdParty] = @DoNotSellToThirdParty OR @DoNotSellToThirdParty is null)
	AND ([AgreedToTermsAndConditions] = @AgreedToTermsAndConditions OR @AgreedToTermsAndConditions is null)
	AND ([DateOfBirth] = @DateOfBirth OR @DateOfBirth is null)
	AND ([DefaultContactID] = @DefaultContactID OR @DefaultContactID is null)
	AND ([DefaultOfficeID] = @DefaultOfficeID OR @DefaultOfficeID is null)
	AND ([AddressVerified] = @AddressVerified OR @AddressVerified IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT *
    FROM 
	[dbo].[Customers] with (nolock)
    WHERE  
	 ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([IsBusiness] = @IsBusiness AND @IsBusiness is not null)
	OR ([FirstName] = @FirstName AND @FirstName is not null)
	OR ([MiddleName] = @MiddleName AND @MiddleName is not null)
	OR ([LastName] = @LastName AND @LastName is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber AND @DayTimeTelephoneNumber is not null)
	OR ([DayTimeTelephoneNumberVerifiedAndValid] = @DayTimeTelephoneNumberVerifiedAndValid AND @DayTimeTelephoneNumberVerifiedAndValid is not null)
	OR ([HomeTelephone] = @HomeTelephone AND @HomeTelephone is not null)
	OR ([HomeTelephoneVerifiedAndValid] = @HomeTelephoneVerifiedAndValid AND @HomeTelephoneVerifiedAndValid is not null)
	OR ([MobileTelephone] = @MobileTelephone AND @MobileTelephone is not null)
	OR ([MobileTelephoneVerifiedAndValid] = @MobileTelephoneVerifiedAndValid AND @MobileTelephoneVerifiedAndValid is not null)
	OR ([CompanyTelephone] = @CompanyTelephone AND @CompanyTelephone is not null)
	OR ([CompanyTelephoneVerifiedAndValid] = @CompanyTelephoneVerifiedAndValid AND @CompanyTelephoneVerifiedAndValid is not null)
	OR ([WorksTelephone] = @WorksTelephone AND @WorksTelephone is not null)
	OR ([WorksTelephoneVerifiedAndValid] = @WorksTelephoneVerifiedAndValid AND @WorksTelephoneVerifiedAndValid is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	OR ([Website] = @Website AND @Website is not null)
	OR ([HasDownloaded] = @HasDownloaded AND @HasDownloaded is not null)
	OR ([DownloadedOn] = @DownloadedOn AND @DownloadedOn is not null)
	OR ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([ClientStatusID] = @ClientStatusID AND @ClientStatusID is not null)
	OR ([Test] = @Test AND @Test is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([Occupation] = @Occupation AND @Occupation is not null)
	OR ([Employer] = @Employer AND @Employer is not null)
	OR ([Fullname] = @Fullname AND @Fullname is not null)
	OR ([PhoneNumbersVerifiedOn] = @PhoneNumbersVerifiedOn AND @PhoneNumbersVerifiedOn is not null)
	OR ([DoNotEmail] = @DoNotEmail AND @DoNotEmail is not null)
	OR ([DoNotSellToThirdParty] = @DoNotSellToThirdParty AND @DoNotSellToThirdParty is not null)
	OR ([AgreedToTermsAndConditions] = @AgreedToTermsAndConditions AND @AgreedToTermsAndConditions is not null)
	OR ([DateOfBirth] = @DateOfBirth AND @DateOfBirth is not null)
	OR ([DefaultContactID] = @DefaultContactID AND @DefaultContactID is not null)
	OR ([DefaultOfficeID] = @DefaultOfficeID AND @DefaultOfficeID is not null)
	OR ([AddressVerified] = @AddressVerified AND @AddressVerified is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	Select @@ROWCOUNT			
  END
				





GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindNoLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindNoLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindNoLock] TO [sp_executeall]
GO
