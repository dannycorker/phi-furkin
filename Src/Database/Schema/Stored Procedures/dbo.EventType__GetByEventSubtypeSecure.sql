SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2007-10-31
-- Description:	Gets all EventTypes that user has access to, by ClientID
-- SB  2014-07-10 Updated to use view which includes shared event types
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetByEventSubtypeSecure]
(
	@LeadTypeID int,
	@EventSubtypeID int,
	@UserID int
)
AS
	SET ANSI_NULLS OFF
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID
	
	IF @LeadTypeID = -1
	BEGIN
		SET @LeadTypeID = null
	END

	SELECT et.*		
	FROM
		dbo.fnEventTypeShared(@ClientID) et 
		INNER JOIN fnEventTypeSecure(@UserID, null) f ON et.EventTypeID = f.objectid
	WHERE
		[EventSubTypeID] = @EventSubtypeID
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID is null)
	AND et.[Enabled] = 1 
	ORDER BY EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON












GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByEventSubtypeSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByEventSubtypeSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByEventSubtypeSecure] TO [sp_executeall]
GO
