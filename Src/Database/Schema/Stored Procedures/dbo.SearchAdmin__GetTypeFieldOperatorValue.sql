SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
	Author : Ian Slack
	Dated : 2012-03-19
	Does a lookup of search type, field, operator and values
*/
CREATE PROCEDURE [dbo].[SearchAdmin__GetTypeFieldOperatorValue]
(
	@ClientID INT,
	@SubClientID INT,
	@ClientPersonnelAdminGroupID INT,
	@SearchTypeID INT,
	@SearchFieldID INT,
	@SearchFieldOperatorID INT 
)
AS

	SET ANSI_NULLS OFF

	SELECT	st.SearchTypeID,
			st.SearchTypeName,
			sf.SearchFieldID,
			sf.SearchFieldName,
			sfo.SearchFieldOperatorID,
			so.SearchOperatorName,
			ISNULL(lli.LookupListItemID,0) FieldValueID,
			ISNULL(lli.ItemValue,'') FieldValue
	FROM	SearchAdmin sa WITH (NOLOCK)
	LEFT JOIN SearchType st on sa.SearchTypeID = st.SearchTypeID
	LEFT JOIN SearchField sf on st.SearchTypeID = sf.SearchTypeID
	LEFT JOIN SearchFieldOperator sfo on sfo.SearchFieldID = sf.SearchFieldID
	INNER JOIN SearchOperator so on sfo.SearchOperatorID = so.SearchOperatorID
	LEFT JOIN DetailFields df on sf.DetailFieldID = df.DetailFieldID and df.QuestionTypeID in (4)
	LEFT JOIN LookupListItems lli on df.LookupListID = lli.LookupListID
	where sa.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID

	and (st.SearchTypeID = @SearchTypeID OR @SearchTypeID = 0)
	and (sf.SearchFieldID = @SearchFieldID OR @SearchFieldID = 0)
	and (sfo.SearchFieldOperatorID = @SearchFieldOperatorID OR @SearchFieldOperatorID = 0)
	order by st.SearchTypeName, sf.SearchFieldName
			
	SELECT @@ROWCOUNT

	SET ANSI_NULLS ON



GO
GRANT VIEW DEFINITION ON  [dbo].[SearchAdmin__GetTypeFieldOperatorValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchAdmin__GetTypeFieldOperatorValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchAdmin__GetTypeFieldOperatorValue] TO [sp_executeall]
GO
