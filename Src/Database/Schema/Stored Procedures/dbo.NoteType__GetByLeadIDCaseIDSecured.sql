SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 25-07-2010

-- Created By:  Slacky
-- Purpose: Show the most recent EventCount notes for a lead/case in order of priority and date
--			for client personnel and minium access rights
-- Modified	2014-07-14	SB	Changed to use the new view for lead type sharing
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType__GetByLeadIDCaseIDSecured]
(
	@LeadID int,
	@CaseID int,
	@ClientPersonnelID int,
	@MinimumRightsRequired int,
	@NoteCount int = 50
)
AS

	SET ROWCOUNT @NoteCount
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	SELECT
		le.LeadEventID,
		le.LeadID,
		le.Comments,
		convert(char(11),le.WhenCreated,103) + LEFT(convert(char(5),LE.WhenCreated,108),5) AS 'When',
		cp.UserName,
		le.NotePriority,
		isnull(nt.AlertColour, '') as [AlertColour],
		isnull(nt.NormalColour, '') as [NormalColour]
	FROM
		LeadEvent le WITH (NOLOCK) 
		LEFT OUTER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID
		LEFT OUTER JOIN dbo.fnNoteTypeShared(@ClientID) nt ON le.NoteTypeID = nt.NoteTypeID
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON le.WhoCreated = cp.ClientPersonnelID 
		INNER JOIN dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) snt ON snt.ObjectID = nt.NoteTypeID
	WHERE 
		le.LeadID = @LeadID 
	AND le.CaseID = @CaseID 
	AND	(le.EventDeleted = 0 OR le.EventDeleted = 0)
	AND et.EventTypeID is null
	ORDER BY IsNull(le.NotePriority,0) desc, le.WhenCreated desc, le.LeadEventID desc



GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType__GetByLeadIDCaseIDSecured] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType__GetByLeadIDCaseIDSecured] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType__GetByLeadIDCaseIDSecured] TO [sp_executeall]
GO
