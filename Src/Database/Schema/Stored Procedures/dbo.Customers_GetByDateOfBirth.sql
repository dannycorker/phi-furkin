SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-01-2012
-- Description:	Gets the customers whose date of births are within the given date range
-- =============================================
CREATE PROCEDURE [dbo].[Customers_GetByDateOfBirth]

	@ClientID INT,
	@SubClientID INT = NULL,
	@FromDate DATETIME,
	@ToDate DATETIME

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT CustomerID, Fullname, Convert(varchar, YEAR(@FromDate)) + '-' + right('0' + Convert(varchar,Month(DateOfBirth)),2) + '-' + Convert(varchar, Day(DateOfBirth)) DateOfBirth FROM Customers WITH (NOLOCK) 
	WHERE 
		ClientID=@ClientID AND 
		(SubClientID=@SubClientID) AND 
		Test=0 AND
		((MONTH(DateOfBirth) >= MONTH(@FromDate)) OR
		(MONTH(DateOfBirth) <= MONTH(@ToDate)))
	ORDER BY DateOfBirth
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByDateOfBirth] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_GetByDateOfBirth] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByDateOfBirth] TO [sp_executeall]
GO
