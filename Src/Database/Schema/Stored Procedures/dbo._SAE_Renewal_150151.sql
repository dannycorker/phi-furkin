SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Auto renew policy
-- 2020-06-18 GPR | Update procedure to use WrittenPremium table in place of PremiumDetailHistory
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Renewal_150151]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PurchasedProductID			INT
			,@AccountID						INT
			,@CardTransactionID				INT
			,@TableRowID					INT
			,@EndDate						DATE
			,@Now							DATETIME = dbo.fn_GetDate_Local()
			,@PolicyStartDate				DATE
			,@RenewalTableRowID				INT
			,@ValueDate						DATE
			,@ValueMoney					MONEY
			,@OldPurchasedProductID			INT
			,@ValueInt						INT
			,@InceptionDate					DATE
			,@StartDate						DATE
			,@SchemeID						INT
			,@VolExcess						MONEY = 0
			,@OptCover						VARCHAR(MAX)
			,@NewTableRowID					INT
			,@VarcharDate					VARCHAR(10)
			,@RenewalDate					DATE
			,@ColCaseID						INT
			,@DetailValue					VARCHAR(2000)
			,@ColMatterID					INT
			,@VarcharDateTime				VARCHAR(19)
			,@PremiumGross					DECIMAL (18,2)
			,@OldPremiumNet					NUMERIC(18,2)
			,@OldPremiumTax					NUMERIC(18,2)
			,@OldPaymentGross				NUMERIC(18,2)
			,@ActualCollectionDate			DATE
			,@AccountObjectID				INT
			,@ToMatterID					INT
			,@AccountTypeID					INT
			,@OneVisionID					INT /*2020-08-03 ALM added*/

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

			
/* Auto renew policy */
	IF @EventTypeID IN (150151)
	BEGIN
		
		/*GPR 2019-03-04 C600 #55470 - Get the value held within 'ChangeSetID To EvaluateBy RenewalWindow', push this into 'ChangeSetID To EvaluateBy', then set 'ChangeSetID To EvaluateBy RenewalWindow' to blank.*/
		SELECT @ValueInt = dbo.fnGetSimpleDvAsInt(180234, @MatterID)
		EXEC _C00_SimpleValueIntoField 180232,@ValueInt, @MatterID
		EXEC _C00_SimpleValueIntoField 180234,'', @MatterID
		
		-- add new row to policy history
		SELECT @InceptionDate=dbo.fnGetDvAsDate(170035,@CaseID)
		SELECT @StartDate=dbo.fnGetDvAsDate(170037,@CaseID)
		SELECT @StartDate = DATEADD(DAY,1,@StartDate) -- Add one to end date
		SELECT @EndDate=DATEADD(DAY,-1,DATEADD(YEAR,1,@StartDate)) /*AHOD 2018-11-13 Added to fix date issue in Year 2+*/
			
		SELECT TOP 1 @SchemeID = tdvScheme.ResourceListID
					, @VolExcess = ISNULL(tdvVolExcess.ValueMoney, 0)
					, @OptCover=ISNULL(tdvOptCover.DetailValue,'')
					,@TableRowID = r.TableRowID 
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
		INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 145666
		LEFT JOIN dbo.TableDetailValues tdvVolExcess WITH (NOLOCK) ON r.TableRowID = tdvVolExcess.TableRowID AND tdvVolExcess.DetailFieldID = 145667
		LEFT JOIN dbo.TableDetailValues tdvOptCover WITH (NOLOCK) ON r.TableRowID = tdvOptCover.TableRowID AND tdvOptCover.DetailFieldID = 175737
		WHERE tdvStatus.ValueInt=43002
			AND r.MatterID=@MatterID
		ORDER BY r.TableRowID DESC


		/*Update the last Row to be not live*/ 
		--EXEC _C00_SimpleValueIntoField 145666, 74536 , @TableRowID , @WhoCreated

		/*GPR 2020-06-18 Update the WrittenPremium table to set the old active row as no longer active*/
		DECLARE @WrittenPremiumID INT

		--SELECT TOP (1)
		--@WrittenPremiumID = wp.WrittenPremiumID
		--FROM WrittenPremium wp WITH (NOLOCK)
		--WHERE wp.MatterID = @MatterID
		--AND wp.Active = 1
		--ORDER BY wp.WrittenPremiumID DESC	

		UPDATE WrittenPremium
		SET Active = 0
		WHERE MatterID = @MatterID
		AND Active = 1

		SELECT @TableRowID = NULL	

		/*GPR 2020-09-17 for PPET-530*/
		DECLARE @Excess MONEY, @CoPay MONEY
		SELECT @Excess = dbo.fnGetSimpleDvAsMoney(313932,@LeadID)
		SELECT @CoPay = dbo.fnGetSimpleDvAsMoney(313930,@LeadID)

		EXEC _C00_1273_Policy_CreatePolicyHistory @MatterID, @SchemeID, @InceptionDate, @StartDate, @EndDate, @StartDate, @VolExcess, @OptCover, @Excess, @CoPay

		SELECT @NewTableRowID=ISNULL(dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow (@CaseID, @StartDate),0)
		
		-- update start, end & terms date
		EXEC dbo._C00_SimpleValueIntoField 170036, @StartDate, @MatterID, @WhoCreated		
		EXEC dbo._C00_SimpleValueIntoField 176925, @StartDate, @MatterID, @WhoCreated		
		EXEC dbo._C00_SimpleValueIntoField 170037, @EndDate, @MatterID, @WhoCreated	

		-- update current/previous/renewal premium fields for printing
		
		-- copy current to previous
		EXEC _C600_CopyFieldToField 175337,175340,@MatterID
		EXEC _C600_CopyFieldToField 175338,175341,@MatterID
		EXEC _C600_CopyFieldToField 175339,175342,@MatterID
		
		-- copy renewal to current
		EXEC _C600_CopyFieldToField 175443,175337,@MatterID
		EXEC _C600_CopyFieldToField 175444,175338,@MatterID
		EXEC _C600_CopyFieldToField 175445,175339,@MatterID

		-- clear renewal
		EXEC dbo._C00_SimpleValueIntoField 175443, '', @MatterID		
		EXEC dbo._C00_SimpleValueIntoField 175444, '', @MatterID		
		EXEC dbo._C00_SimpleValueIntoField 175445, '', @MatterID		
		--EXEC dbo._C00_SimpleValueIntoField 175307, '', @MatterID
		
		--update renewal /*GPR 2019-01-17*/
		SELECT @VarcharDate = dbo.fnGetSimpleDvAsDate(170037,@MatterID)		
		SELECT @RenewalDate = DATEADD(DAY, +1, @VarcharDate)
		EXEC dbo._C00_SimpleValueIntoField 175307, @RenewalDate, @MatterID, @AqAutomation	-- renewal date	
				
		-- if this is an annual policy allow next collection date to be incremented
		SELECT @ColCaseID=CaseID, @ColMatterID=MatterID 
		FROM Matter WITH (NOLOCK) 
		WHERE MatterID=dbo.fn_C600_GetCollectionsMatterID(@MatterID)
		
		-- Convert the re-rate for renewal from status 'other' to status 'live' and 
		-- downgrade the existing 'live' to 'ex-live'
		
		/*GPR 2020-06-18 commented out*/
		--SELECT TOP 1 @TableRowID=tr.TableRowID -- live
		--FROM TableRows tr WITH (NOLOCK) 
		--INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=175722
		--WHERE tr.DetailFieldID=175336 
		--AND tr.MatterID=@MatterID
		--AND stat.ValueInt IN (72326,76514)/*GPR added MigLive to list)*/
		--ORDER BY tr.TableRowID DESC
		
		--SELECT @DetailValue=CONVERT(VARCHAR(10),DATEADD(DAY,-1,@StartDate),120)
		--EXEC dbo._C00_SimpleValueIntoField 175722, 72327, @TableRowID	-- status=exlive	
		--EXEC dbo._C00_SimpleValueIntoField 175347, @DetailValue, @TableRowID	-- ToDate(=startdate less one day)	

		/*GPR 2020-06-18 set the latest renewal row to Active*/
		SELECT TOP (1)
		@WrittenPremiumID = wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 3 /*Renewal*/
		ORDER BY wp.WrittenPremiumID DESC	

		UPDATE WrittenPremium
		SET Active = 1
		WHERE WrittenPremiumID = @WrittenPremiumID
		AND MatterID = @MatterID
		AND Active = 0

		--SELECT @TableRowID=NULL
		--SELECT @TableRowID=tr.TableRowID -- re-rate renewal
		--FROM TableRows tr WITH (NOLOCK) 
		--INNER JOIN TableDetailValues evnt WITH (NOLOCK) ON tr.TableRowID=evnt.TableRowID AND evnt.DetailFieldID=175348
		--INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=175722
		--WHERE tr.DetailFieldID=175336 
		--AND tr.MatterID=@MatterID
		--AND stat.ValueInt=72329 -- other
		--AND evnt.DetailValue LIKE '%renewal%'
		--ORDER BY tr.TableRowID DESC
		
		--SELECT @DetailValue=CONVERT(VARCHAR(10),@StartDate,120)
		--EXEC dbo._C00_SimpleValueIntoField 175722, 72326, @TableRowID	-- status=live	
		--EXEC dbo._C00_SimpleValueIntoField 175346, @StartDate, @TableRowID	-- FromDate(=startdate)	
		----EXEC dbo._C00_SimpleValueIntoField 175709, '', @TableRowID	-- PCID	
		--SELECT @VarcharDateTime=CONVERT(VARCHAR(19),dbo.fn_GetDate_Local(),121)
		--EXEC dbo._C00_SimpleValueIntoField 175400, @VarcharDateTime, @TableRowID, @WhoCreated	-- adjustment application date
		--EXEC dbo._C00_SimpleValueIntoField 175377, @NewTableRowID, @TableRowID, @WhoCreated	-- policy history tablerowid

		/*JEL 2019-06-06 Relax the filter so if the batch applies late it still locates the correctPP*/
		SELECT TOP 1 @PurchasedProductID = pp.PurchasedProductID, @PremiumGross = pp.ProductCostGross
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.ObjectID = @MatterID
		AND pp.ValidFrom <= @StartDate
		AND pp.ValidTo > @Now
		Order by pp.ValidFrom DESC

		EXEC dbo._C00_SimpleValueIntoField 177074, @PurchasedProductID, @MatterID, @WhoCreated /*Purchased Policy ID*/
		-- and historical policy table	
		-- find the most recent row where PPID is blank

		SELECT @TableRowID=TableRowID 
		FROM TableDetailValues ppid WITH (NOLOCK) 
		WHERE ppid.MatterID=@MatterID 
			AND ppid.DetailFieldID=177419 /*Purchased Policy ID*/
			AND ppid.DetailValue=''
		ORDER BY TableRowID DESC
	
		EXEC dbo._C00_SimpleValueIntoField 177419, @PurchasedProductID, @TableRowID, @WhoCreated /*Purchased Policy ID*/
		EXEC dbo._C00_SimpleValueIntoField 177898, @PremiumGross, @MatterID, @WhoCreated /*Current/New Premium - After Discount*/

		/*Check for case 3 (payment made for full amount)*/
		SELECT @OldPremiumNet = pp.ProductCostNet, @OldPremiumTax = pp.ProductCostVAT, @OldPaymentGross = pp.ProductCostGross, @OldPurchasedProductID = pp.PurchasedProductID, @AccountID = pp.AccountID
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.ObjectID = @MatterID
		AND pp.ValidFrom >= @ActualCollectionDate

		/*If this is a migrated policy, the account will have been imported as inactive,
		  Locate these accounts and set them to active*/ 

		  UPDATE a 
		  SET Active = 1 
		  FROM Account a 
		  INNER JOIN PurchasedProduct p WITH (NOLOCK) on a.AccountID = p.AccountID 
		  WHERE p.PurchasedProductID = @PurchasedProductID 
		  AND EXISTS (
			/*ACE 2018-06-15 - Only set up mandates where we have positive payments, if we only have a refund we dont need a mandate*/
			SELECT *
			FROM CustomerPaymentSchedule cps WITH (NOLOCK)
			WHERE cps.AccountID = a.AccountID
			AND cps.PaymentGross > 0.00
			AND cps.PaymentStatusID IN (1,5,2)
		)
		/*PL 2019-08-08 Update the Lead Type Relationship. #58552*/
		SELECT @AccountObjectID = a.ObjectID, @ToMatterID = ltr.ToMatterID, @AccountTypeID = a.AccountTypeID
		FROM PurchasedProduct pp WITH (NOLOCK)
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = pp.AccountID
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON pp.ObjectID = ltr.FromMatterID AND ltr.ToLeadTypeID = 1493
		WHERE pp.ObjectID = @MatterID
		AND dbo.fn_GetDate_Local() < pp.ValidTo

		IF @AccountObjectID <> @ToMatterID AND @AccountTypeID = 1
		BEGIN

			UPDATE ltr
			SET ltr.ToMatterID = @AccountObjectID
			FROM LeadTypeRelationship ltr
			WHERE ltr.FromMatterID = @MatterID AND ltr.ToLeadTypeID = 1493

		END

		/*Remove the 'Renewal AccountID' value so this can be recalculated next renewal*/ 
		EXEC _C00_SimpleValueIntoField 180219,0,@MatterID,@WhoCreated
	
		IF dbo.fnGetSimpleDvAsInt(314235, @MatterID) <> 0
		BEGIN
			/*GPR 2020-09-20 Don't forget the Wellness History!*/
			EXEC [_C600_PA_Policy_UpdateWellnessHistory] @MatterID, @LeadEventID /*GPR 2020-09-17*/
		END

	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_150151] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Renewal_150151] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_150151] TO [sp_executeall]
GO
