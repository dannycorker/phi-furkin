SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*--------------------------------------------- 
Author: Robin Hall 2014-07-23                 
Clone process from Cxxx/C0 process to Cyyy/C0 
 --------------------------------------------*/

CREATE PROC [dbo].[_C00_Admin_CloneSharingClient] 
(
		 @SourceClientID INT = 327
		,@ProcessStartEventTypeID INT = 119427
		,@TargetClientID INT = 384
		,@TargetLeadTypeID INT = 1490
		,@TargetDefaultFolder INT = 6313
		,@AutomationUser INT
)
AS

BEGIN


	DECLARE @InProcessEvents TABLE (ID INT IDENTITY, EventTypeID INT, ClientID INT, Processed BIT)
	DECLARE 
		@EventTypeId INT

	SELECT @AutomationUser=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@TargetClientID,53,'CFG|AqAutomationCPID',0)

	/* Build list of all events in the Pet Claim process by cascasing down from process start event */

	BEGIN

		SET NOCOUNT ON
		
		-- Start with "New Claim" process start event
		INSERT @InProcessEvents (EventTypeID, ClientID, Processed)
		SELECT @ProcessStartEventTypeID, @SourceClientID, 0

		-- Loop through adding to list until all events have been processed for next events
		WHILE (SELECT COUNT(*) FROM @InProcessEvents WHERE Processed = 0) > 0
		BEGIN
			
			SELECT TOP 1 @EventTypeId = e.EventTypeID
			FROM @InProcessEvents e WHERE e.Processed = 0
			
			INSERT @InProcessEvents (EventTypeID, ClientID, [Processed])
			SELECT ec.NextEventTypeID, etNext.ClientID, 0
			FROM EventChoice ec WITH (NOLOCK)
			INNER JOIN dbo.EventType etNext WITH (NOLOCK) ON etNext.EventTypeID = ec.NextEventTypeID
			WHERE ec.EventTypeID = @eventtypeid
			AND NOT EXISTS (SELECT 1 FROM @InProcessEvents WHERE EventTypeID = ec.NextEventTypeID)
			
			UPDATE @InProcessEvents SET Processed = 1 WHERE EventTypeID = @eventtypeid

		END
		
		SET NOCOUNT OFF
		
	END


	/* Copy any Document Types referenced by in-process unshared events */

	BEGIN

		INSERT INTO [DocumentType]
				   ([ClientID]
				   ,[LeadTypeID]
				   ,[DocumentTypeName]
				   ,[DocumentTypeDescription]
				   ,[Header]
				   ,[Template]
				   ,[Footer]
				   ,[CanBeAutoSent]
				   ,[EmailSubject]
				   ,[EmailBodyText]
				   ,[InputFormat]
				   ,[OutputFormat]
				   ,[Enabled]
				   ,[RecipientsTo]
				   ,[RecipientsCC]
				   ,[RecipientsBCC]
				   ,[ReadOnlyTo]
				   ,[ReadOnlyCC]
				   ,[ReadOnlyBCC]
				   ,[SendToMultipleRecipients]
				   ,[MultipleRecipientDataSourceType]
				   ,[MultipleRecipientDataSourceID]
				   ,[SendToAllByDefault]
				   ,[ExcelTemplatePath]
				   ,[FromDetails]
				   ,[ReadOnlyFrom]
				   ,[SourceID]
				   ,[WhoCreated]
				   ,[WhenCreated]
				   ,[WhoModified]
				   ,[WhenModified]
				   ,[FolderID]
				   ,[IsThunderheadTemplate]
				   ,[ThunderheadUniqueTemplateID]
				   ,[ThunderheadDocumentFormat])
		SELECT 
			   @TargetClientID
			  ,@TargetLeadTypeID
			  ,[DocumentTypeName]
			  ,[DocumentTypeDescription]
			  ,[Header]
			  ,[Template]
			  ,[Footer]
			  ,[CanBeAutoSent]
			  ,[EmailSubject]
			  ,[EmailBodyText]
			  ,[InputFormat]
			  ,[OutputFormat]
			  ,dt.[Enabled]
			  ,[RecipientsTo]
			  ,[RecipientsCC]
			  ,[RecipientsBCC]
			  ,[ReadOnlyTo]
			  ,[ReadOnlyCC]
			  ,[ReadOnlyBCC]
			  ,[SendToMultipleRecipients]
			  ,[MultipleRecipientDataSourceType]
			  ,[MultipleRecipientDataSourceID]
			  ,[SendToAllByDefault]
			  ,[ExcelTemplatePath]
			  ,[FromDetails]
			  ,[ReadOnlyFrom]
			  ,dt.DocumentTypeID -- Map to original
			  ,dt.[WhoCreated]
			  ,dt.[WhenCreated]
			  ,dt.[WhoModified]
			  ,dt.[WhenModified]
			  ,@TargetDefaultFolder -- Unfiled
			  ,[IsThunderheadTemplate]
			  ,[ThunderheadUniqueTemplateID]
			  ,[ThunderheadDocumentFormat]
		FROM 
			@InProcessEvents e
			INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = e.EventTypeID
			INNER JOIN dbo.[DocumentType] dt WITH (NOLOCK) on dt.DocumentTypeID = et.DocumentTypeID
		WHERE 
			e.ClientID = @SourceClientID 

		-- Put all docs in default folder
		INSERT DocumentTypeFolderLink (DocumentTypeID, FolderID)
		SELECT DocumentTypeID, @TargetDefaultFolder
		FROM DocumentType
		WHERE ClientID = @TargetClientID

	END


	/* Insert all unshared in-process events with change of Client and LeadType */

	BEGIN

		INSERT INTO [EventType]
				   ([ClientID]
				   ,[EventTypeName]
				   ,[EventTypeDescription]
				   ,[Enabled]
				   ,[UnitsOfEffort]
				   ,[FollowupTimeUnitsID]
				   ,[FollowupQuantity]
				   ,[AvailableManually]
				   ,[StatusAfterEvent]
				   ,[AquariumEventAfterEvent]
				   ,[EventSubtypeID]
				   ,[DocumentTypeID]
				   ,[LeadTypeID]
				   ,[AllowCustomTimeUnits]
				   ,[InProcess]
				   ,[KeyEvent]
				   ,[UseEventCosts]
				   ,[UseEventUOEs]
				   ,[UseEventDisbursements]
				   ,[UseEventComments]
				   ,[SignatureRequired]
				   ,[SignatureOverride]
				   ,[VisioX]
				   ,[VisioY]
				   ,[AquariumEventSubtypeID]
				   ,[WhoCreated]
				   ,[WhenCreated]
				   ,[WhoModified]
				   ,[WhenModified]
				   ,[FollowupWorkingDaysOnly]
				   ,[CalculateTableRows]
				   ,[SourceID])
		SELECT 
			  @TargetClientID AS ClientID-- New client
			  ,[EventTypeName]
			  ,[EventTypeDescription]
			  ,et.[Enabled]
			  ,[UnitsOfEffort]
			  ,[FollowupTimeUnitsID]
			  ,[FollowupQuantity]
			  ,[AvailableManually]
			  ,[StatusAfterEvent]
			  ,[AquariumEventAfterEvent]
			  ,[EventSubtypeID]
			  ,dt.DocumentTypeID -- New DocumentType
			  ,@TargetLeadTypeID AS LeadTypeID -- New client target LeadType
			  ,[AllowCustomTimeUnits]
			  ,[InProcess]
			  ,[KeyEvent]
			  ,[UseEventCosts]
			  ,[UseEventUOEs]
			  ,[UseEventDisbursements]
			  ,[UseEventComments]
			  ,[SignatureRequired]
			  ,[SignatureOverride]
			  ,[VisioX]
			  ,[VisioY]
			  ,[AquariumEventSubtypeID]
			  ,et.[WhoCreated]
			  ,et.[WhenCreated]
			  ,et.[WhoModified]
			  ,et.[WhenModified]
			  ,[FollowupWorkingDaysOnly]
			  ,[CalculateTableRows]
			  ,et.EventTypeID -- Map back to source EventTypeID
		FROM 
			@InProcessEvents e
			INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = e.EventTypeID
			LEFT JOIN DocumentType dt WITH (NOLOCK) on dt.SourceID = et.DocumentTypeID
		WHERE 
			e.ClientID = @SourceClientID -- ONLY unshared events

	END

	/* Copy all in-process eventchoice connections */

	BEGIN

		INSERT INTO [EventChoice]
				   ([EventTypeID]
				   ,[Description]
				   ,[NextEventTypeID]
				   ,[ClientID]
				   ,[LeadTypeID]
				   ,[ThreadNumber]
				   ,[EscalationEvent]
				   ,[Field]
				   ,[LogicalOperator]
				   ,[Value1]
				   ,[Value2]
				   ,[SqlClauseForInclusion]
				   ,[Weighting]
				   ,[ValueTypeID])
		SELECT CASE WHEN e1.ClientID = 0 THEN ec.EventTypeID ELSE et1.EventTypeID END AS EventTypeID
			  ,[Description]
			  ,CASE WHEN e2.ClientID = 0 THEN ec.NextEventTypeID ELSE et2.EventTypeID END AS NextEventTypeID
			  ,@TargetClientID AS ClientID
			  ,@TargetLeadTypeID AS LeadTypeID
			  ,[ThreadNumber]
			  ,[EscalationEvent]
			  ,[Field]
			  ,[LogicalOperator]
			  ,[Value1]
			  ,[Value2]
			  ,[SqlClauseForInclusion]
			  ,[Weighting]
			  ,[ValueTypeID]
		FROM 
			[EventChoice] ec WITH (NOLOCK)
			INNER JOIN @InProcessEvents e1 on e1.EventTypeID = ec.EventTypeID
			INNER JOIN @InProcessEvents e2 on e2.EventTypeID = ec.NextEventTypeID
			LEFT JOIN dbo.EventType et1 WITH (NOLOCK) on et1.SourceID = ec.EventTypeID and et1.ClientID = @TargetClientID
			LEFT JOIN dbo.EventType et2 WITH (NOLOCK) on et2.SourceID = ec.NextEventTypeID and et2.ClientID = @TargetClientID

	END

	/* Copy Helper fields, Mandatory fields, Automated Events, LinkedFields, Event Groups and Event Group Members */

	BEGIN

		-- Helper fields
		INSERT EventTypeHelperField (ClientID, EventTypeID, LeadTypeID, DetailFieldID)
		SELECT 
			eTo.ClientID, eTo.EventTypeID, eTo.LeadTypeID, x.DetailFieldID
		FROM 
			EventTypeHelperField x WITH (NOLOCK)
			INNER JOIN dbo.EventType eFrom WITH (NOLOCK) ON eFrom.EventTypeID = x.EventTypeID AND eFrom.ClientID = @SourceClientID
			INNER JOIN dbo.EventType eTo WITH (NOLOCK) ON eTo.SourceID = x.EventTypeID AND eTo.ClientID = @TargetClientID

		-- Mandatory fields
		INSERT EventTypeMandatoryField (ClientID, EventTypeID, LeadTypeID, DetailFieldID)
		SELECT 
			eTo.ClientID, eTo.EventTypeID, eTo.LeadTypeID, x.DetailFieldID
		FROM 
			EventTypeMandatoryField x WITH (NOLOCK)
			INNER JOIN dbo.EventType eFrom WITH (NOLOCK) ON eFrom.EventTypeID = x.EventTypeID AND eFrom.ClientID = @SourceClientID
			INNER JOIN dbo.EventType eTo WITH (NOLOCK) ON eTo.SourceID = x.EventTypeID AND eTo.ClientID = @TargetClientID

		-- Automated events
		INSERT EventTypeAutomatedEvent (ClientID, EventTypeID, AutomatedEventTypeID, DelaySeconds, InternalPriority, RunAsUserID, ThreadToFollowUp)
		SELECT 
			eTo.ClientID, eTo.EventTypeID
			, CASE WHEN eNextFrom.ClientID = 0 THEN x.AutomatedEventTypeID ELSE eNextTo.EventTypeID END
			, x.DelaySeconds, x.InternalPriority, @AutomationUser, x.ThreadToFollowUp
		FROM 
			EventTypeAutomatedEvent x WITH (NOLOCK)
			INNER JOIN dbo.EventType eFrom WITH (NOLOCK) ON eFrom.EventTypeID = x.EventTypeID AND eFrom.ClientID = @SourceClientID
			INNER JOIN dbo.EventType eTo WITH (NOLOCK) ON eTo.SourceID = x.EventTypeID AND eTo.ClientID = @TargetClientID
			LEFT JOIN dbo.EventType eNextFrom WITH (NOLOCK) ON eNextFrom.EventTypeID = x.AutomatedEventTypeID
			LEFT JOIN EventType eNextTo WITH (NOLOCK) ON eNextTo.SourceID = x.AutomatedEventTypeID

		-- Linked fields
		INSERT EventTypefieldcompletion (ClientID, EventTypeID, DetailFieldID, DeleteOptionID, InsertOptionID)
		SELECT 
			eTo.ClientID, eTo.EventTypeID, x.DetailFieldID, x.DeleteOptionID, x.InsertOptionID
		FROM 
			EventTypeFieldCompletion x WITH (NOLOCK)
			INNER JOIN dbo.EventType eFrom WITH (NOLOCK) ON eFrom.EventTypeID = x.EventTypeID AND eFrom.ClientID = @SourceClientID
			INNER JOIN dbo.EventType eTo WITH (NOLOCK) ON eTo.SourceID = x.EventTypeID AND eTo.ClientID = @TargetClientID

		--Event groups
		INSERT EventGroup (ClientID, EventGroupName, EventGroupDescription, LeadTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		SELECT 
			@TargetClientID, g.EventGroupName, g.EventGroupDescription, @TargetLeadTypeID, g.EventGroupID, @AutomationUser, dbo.fn_GetDate_Local(), @AutomationUser, dbo.fn_GetDate_Local()
		FROM 
			EventGroup g

		--Event group members
		INSERT 
			EventGroupMember (ClientID, EventGroupID, EventTypeID, EventGroupMemberDescription, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		SELECT 
			@TargetClientID, g.EventGroupID, ISNULL(eto.EventTypeID, eFrom.EventTypeID), m.EventGroupMemberDescription, m.EventGroupMemberID, @AutomationUser, dbo.fn_GetDate_Local(), @AutomationUser, dbo.fn_GetDate_Local()
			FROM EventGroupMember m WITH (NOLOCK)
			INNER JOIN dbo.EventGroup g WITH (NOLOCK) ON g.SourceID = m.EventGroupID
			INNER JOIN dbo.EventType eFrom WITH (NOLOCK) ON eFrom.EventTypeID = m.EventTypeID
			LEFT JOIN EventType eTo WITH (NOLOCK) ON eTo.SourceID = m.EventTypeID AND eTo.ClientID = @TargetClientID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Admin_CloneSharingClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Admin_CloneSharingClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Admin_CloneSharingClient] TO [sp_executeall]
GO
