SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ActiveSessions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession__DeleteAll]
AS

	DELETE FROM [dbo].[ActiveSession]
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession__DeleteAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession__DeleteAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession__DeleteAll] TO [sp_executeall]
GO
