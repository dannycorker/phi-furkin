SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date:	2019-11-26
-- Description:	Updates a request in the Liberty Mediate API queue.
--
-- 2020-01-07 MAB Added header comment and modified update to Exception column so that any existing value is not overwritten
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Mediate_Update]
(
    @AppKey         [NVARCHAR](50),
    @Queue          [NVARCHAR](50),
    @Flow           [NVARCHAR](50),
    @Action         [NVARCHAR](50),
    @Method         [NVARCHAR](50),
	@ObjectID		[NVARCHAR](MAX),
    @RequestBody    [NVARCHAR](MAX),
    @RequestType    [NVARCHAR](50),
    @ResponseBody   [NVARCHAR](MAX),
    @ResponseType   [NVARCHAR](50),
    @Exception      [NVARCHAR](2000)='',
    @ExceptionCount INT=0,
    @QueueID        [bigint]=0,
	@ParentObjectID	[NVARCHAR](MAX) = NULL
) AS
BEGIN
 
	SET NOCOUNT ON

	DECLARE  @V2ObjectID	VARCHAR(200)
			,@LogEntry		VARCHAR(2000)

	--BEGIN TRY
	--	SELECT @ResponseXML = CAST(@ResponseBody AS XML)
	--END TRY
	--BEGIN CATCH
	--	SELECT @ResponseXML = NULL
	--END CATCH

	IF @RequestType like '%Vision%' AND @ResponseBody IS NOT NULL AND dbo.fnIsInt(@ObjectID) = 1
	BEGIN
		SELECT @V2ObjectID = @ResponseBody

		UPDATE cu
		SET  cu.CustomerRef = @V2ObjectID
			,cu.WhenChanged = dbo.fn_GetDate_Local()
			,cu.ChangeSource = OBJECT_NAME(@@ProCID) + ' @QueueID = ' + ISNULL(CONVERT(VARCHAR,@QueueID),'NULL')
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @ObjectID

		SELECT @LogEntry = CONVERT(VARCHAR,@@ROWCOUNT) + ' customers updated for ObjectID ' + ISNULL(CONVERT(VARCHAR,@ObjectID),'NULL') + ' | ' + ISNULL(CONVERT(VARCHAR(MAX),@ResponseBody),'NULL')
	END
	ELSE
	BEGIN
		SELECT @LogEntry = 'No customers matched for Request Type "' + ISNULL(@RequestType,'NULL') + '" | ObjectID ' + ISNULL(CONVERT(VARCHAR,@ObjectID),'NULL') + ' | ' + ISNULL(CONVERT(VARCHAR(MAX),@ResponseBody),'NULL')
	END

	EXEC dbo._C00_LogIt 'Info','_C00_Mediate_Update',@QueueID,@LogEntry,0

    UPDATE  [dbo].[_C00_Mediate_Queue]
    SET     [Action] = @Action,
			[ModifyDate] = dbo.fn_GetDate_Local(),
			[ResponseBody] = @ResponseBody,
            [Exception] =	CASE WHEN 
								ISNULL(@Exception, '') > '' 
							THEN 
								CASE WHEN
									ISNULL([Exception], '') > ''
								THEN
									[Exception] + ' | ' + @Exception
								ELSE
									@Exception
								END
							ELSE
								[Exception]
							END,
            [ExceptionCount] += ISNULL(CASE WHEN @Exception > '' THEN 1 END, 0)
    OUTPUT inserted.*
    WHERE [QueueID] = @QueueID
    AND ExceptionCount < 5
    AND [Queue] = @Queue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Mediate_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Update] TO [sp_executeall]
GO
