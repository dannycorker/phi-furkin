SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuillCustomerMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCustomerMapping_Delete]
(

	@QuillCustomerMappingID int   
)
AS


				DELETE FROM [dbo].[QuillCustomerMapping] WITH (ROWLOCK) 
				WHERE
					[QuillCustomerMappingID] = @QuillCustomerMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCustomerMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Delete] TO [sp_executeall]
GO
