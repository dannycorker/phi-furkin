SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientQuestionnairesVisitedButNotComplete table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedButNotComplete_GetByClientQuestionnairesVisitedButNotCompleteID]
(

	@ClientQuestionnairesVisitedButNotCompleteID int   
)
AS


				SELECT
					[ClientQuestionnairesVisitedButNotCompleteID],
					[ClientQuestionnaireID],
					[VisitedDate],
					[IPAddress],
					[ClientID]
				FROM
					[dbo].[ClientQuestionnairesVisitedButNotComplete] WITH (NOLOCK) 
				WHERE
										[ClientQuestionnairesVisitedButNotCompleteID] = @ClientQuestionnairesVisitedButNotCompleteID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_GetByClientQuestionnairesVisitedButNotCompleteID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_GetByClientQuestionnairesVisitedButNotCompleteID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_GetByClientQuestionnairesVisitedButNotCompleteID] TO [sp_executeall]
GO
