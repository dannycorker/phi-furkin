SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReminderTimeshift table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReminderTimeshift_Delete]
(

	@ReminderTimeshiftID int   
)
AS


				DELETE FROM [dbo].[ReminderTimeshift] WITH (ROWLOCK) 
				WHERE
					[ReminderTimeshiftID] = @ReminderTimeshiftID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReminderTimeshift_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Delete] TO [sp_executeall]
GO
