SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-03
-- Description:	Creates policy history row
-- Mods
--  2015-10-19 DCM Added optional coverages
--  2016-12-14 DCM Added policy entitlements and limits
--  2016-12-20 DCM Added Purchased Policy ID
--  2020-08-05 ALM Added @Excess and @CoPay PPET-7
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_CreatePolicyHistory] 
(
	@MatterID INT, 
	@ProductID INT, 
	@InceptionDate DATE,
	@StartDate DATE,
	@EndDate DATE,
	@TermsDate DATE,
	@VolExcess MONEY = 0,
	@PassedOptionalCoverages VARCHAR(2000) = NULL,
	@Excess MONEY = 0,
	@CoPay MONEY = 0
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	DECLARE @PolicyHistoryFields TABLE (FieldID INT)
	INSERT @PolicyHistoryFields(FieldID) VALUES
	(145665), -- Scheme
	(145662), -- Inception
	(145663), -- Start
	(145664), -- End
	(170092), -- Terms Date
	(145666), -- Status
	(145667), -- Vol Excess
	(175737), -- Opt Cover List
	(175738), -- Opt Cover Text
	(177419), -- Purchased Policy ID
	(177414), -- TPL
	(177415), -- Co Pay
	(177416), -- Excess
	(177417), -- Cover
	(180216), -- Is Migrated term
	(314011),  -- Exam Fee
	(314267) -- Wellness Level

	DECLARE @WellnessLevel VARCHAR(20)
	DECLARE @ExamFee VARCHAR(3)
	DECLARE @LeadID INT

	/*GPR 2020-09-18 added to resolve NULL @ExamFee issue for Conor R.*/
	IF @LeadID IS NULL
	BEGIN
		SELECT @LeadID = m.LeadID
		FROM Matter m WITH (NOLOCK)
		WHERE m.MatterID = @MatterID
	END

	SET @ExamFee = ISNULL(dbo.fnGetSimpleDvLuli(313931, @LeadID),'No')
	SET @WellnessLevel = ISNULL(dbo.fnGetSimpleRLDv(314235, 314005, @MatterID, 0),'')

	DECLARE @TableRowID INT
	
	IF @TermsDate IS NULL
	BEGIN
		SELECT @TermsDate = @InceptionDate
	END
	
	DECLARE @PassedOptionalCoveragesText VARCHAR(2000)
	
	SELECT @PassedOptionalCoveragesText = COALESCE(@PassedOptionalCoveragesText + ', ', '') + lli.ItemValue
	FROM LookupListItems lli WITH (NOLOCK)
	INNER JOIN fnTableOfIDsFromCSV(@PassedOptionalCoverages) l ON lli.LookupListItemID=l.AnyID
	ORDER BY lli.ItemValue
	
	SELECT @PassedOptionalCoveragesText = dbo.fn_C00_ReplaceLastCommaWithAnd(@PassedOptionalCoveragesText)
	
	-- entitlements and limits
	-- Cover
	DECLARE @SchemeMatterID INT, @Cover MONEY, @TPL MONEY
	SELECT @SchemeMatterID=dbo.fn_C00_1273_GetCurrentScheme(@ProductID,@StartDate)
	SELECT @Cover=limit.ValueMoney 
	FROM dbo.TableRows pl WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues pols WITH (NOLOCK) on pols.TableRowID = pl.TableRowID and pols.DetailFieldID =  144357 and pols.ResourceListID in (145552,143035)
	INNER JOIN dbo.TableDetailValues limit WITH (NOLOCK) on limit.TableRowID = pl.TableRowID and limit.DetailFieldID =  144358
	WHERE pl.MatterID = @SchemeMatterID 
		AND pl.DetailFieldID = 145692
		
	-- TPL
	SELECT @TPL=limit.ValueMoney 
	FROM dbo.TableRows pl WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues pols WITH (NOLOCK) on pols.TableRowID = pl.TableRowID and pols.DetailFieldID =  144357 /*Policy Section*/ and pols.ResourceListID in (148149) /*Third Party Liability*/
	INNER JOIN dbo.TableDetailValues limit WITH (NOLOCK) on limit.TableRowID = pl.TableRowID and limit.DetailFieldID =  144358 /*Sum Insured*/
	WHERE pl.MatterID = @SchemeMatterID 
		AND pl.DetailFieldID = 145692 /*Policy Limits*/
	
	/*ALM commented out for PPET-7 as CoPay and Excess now passed in
	-- CoPay
	SELECT @CoPay=dbo.fn_C600_GetPolicyCoInsurance (@MatterID, @StartDate)

	-- Excess
	SELECT @Excess=dbo.fn_C600_GetPolicyCompulsoryExcess (@MatterID, 74284, @StartDate, 0, @StartDate, @InceptionDate)
	*/
	
	INSERT dbo.TableRows(ClientID, MatterID, DetailFieldID, DetailFieldPageID)
	VALUES(@ClientID, @MatterID, 170033, 19010)
	
	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT dbo.TableDetailValues (TableRowID, MatterID, ClientID, DetailFieldID, DetailValue, ResourceListID)
	SELECT @TableRowID, @MatterID, @ClientID, f.FieldID,
		CASE f.FieldID
			WHEN 145665 THEN NULL	
			WHEN 145662 THEN CONVERT(VARCHAR, @InceptionDate, 120)				
			WHEN 145663 THEN CONVERT(VARCHAR, @StartDate, 120)
			WHEN 145664 THEN CONVERT(VARCHAR, @EndDate, 120)
			WHEN 170092 THEN CONVERT(VARCHAR, @TermsDate, 120)
			WHEN 145666 THEN '43002'
			WHEN 145667 THEN CAST(@VolExcess AS VARCHAR(2000))
			WHEN 175737 THEN ISNULL(@PassedOptionalCoverages,'')
			WHEN 175738 THEN ISNULL(@PassedOptionalCoveragesText,'')
			WHEN 177419 THEN ''
			WHEN 177414 THEN CONVERT(VARCHAR, @TPL)
			WHEN 177415 THEN CONVERT(VARCHAR, @CoPay)
			WHEN 177416 THEN CONVERT(VARCHAR, @Excess)
			WHEN 177417 THEN CONVERT(VARCHAR, @Cover)
			WHEN 180216 THEN 'false'
			WHEN 314011 THEN @ExamFee
			WHEN 314267 THEN @WellnessLevel
		END AS Value,
		CASE f.FieldID
			WHEN 145665 THEN CAST(@ProductID AS VARCHAR)	
			ELSE NULL
		END	
	FROM @PolicyHistoryFields f 
	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CreatePolicyHistory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CreatePolicyHistory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CreatePolicyHistory] TO [sp_executeall]
GO
