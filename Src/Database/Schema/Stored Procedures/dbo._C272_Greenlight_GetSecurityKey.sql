SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-05-13
-- Description:	Retrieve Client's security key for greenlight
-- =============================================
CREATE PROCEDURE [dbo].[_C272_Greenlight_GetSecurityKey]
	 @ClientID			INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadID INT
			,@ClientPersonnelID	INT = 26692 /*Greenlight System*/
	
	/*
	Every Greenlight integration has a Client 272 Lead.
	That lead has LeadDetailValues for each of the mapped elements
	First, find the lead for this ClientID.
	Next, update the security key
	*/
	SELECT @LeadID = l.LeadID 
	FROM Lead l WITH (NOLOCK) 
	INNER JOIN LeadDetailValues ldv WITH (NOLOCK) on ldv.LeadID = l.LeadID and ldv.DetailFieldID = 207284 /*Aquarium ClientID*/
	WHERE l.LeadTypeID = 1387 /*Integration*/
	AND ldv.ValueInt = @ClientID

	SELECT ldv.DetailValue [SecurityKey]
	FROM LeadDetailValues ldv WITH (NOLOCK) 
	WHERE ldv.LeadID = @LeadID
	AND ldv.DetailFieldID = 216290 /*Security Key*/
END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_GetSecurityKey] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C272_Greenlight_GetSecurityKey] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_GetSecurityKey] TO [sp_executeall]
GO
