SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-10-25
-- Description:	EventTypeAutomatedWorkflow Insert
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedWorkflow__Insert]
@ClientID int, 
@EventTypeID int, 
@WorkflowGroupID int, 
@RunAsUserID int = NULL, 
@FollowUp bit, 
@EventTypeToAdd int, 
@Priority tinyint, 
@Important int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.EventTypeAutomatedWorkflow (ClientID, EventTypeID, WorkflowGroupID, RunAsUserID, FollowUp, EventTypeToAdd, Priority, Important)
	VALUES (@ClientID, @EventTypeID, @WorkflowGroupID, @RunAsUserID, @FollowUp, @EventTypeToAdd, @Priority, @Important)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflow__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedWorkflow__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflow__Insert] TO [sp_executeall]
GO
