SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_Insert]
(

	@SqlQueryTemplateID int    OUTPUT,

	@ClientID int   ,

	@Name varchar (150)  ,

	@Description varchar (500)  ,

	@TemplatePath varchar (255)  ,

	@DataWorksheetNumber int   ,

	@DataStartCell varchar (10)  ,

	@TemplateFormat varchar (20)  ,

	@FolderID int   ,

	@CreatedBy int   ,

	@CreatedOn datetime   ,

	@ModifiedBy int   ,

	@ModifiedOn datetime   ,

	@Enabled bit   
)
AS


				
				INSERT INTO [dbo].[SqlQueryTemplate]
					(
					[ClientID]
					,[Name]
					,[Description]
					,[TemplatePath]
					,[DataWorksheetNumber]
					,[DataStartCell]
					,[TemplateFormat]
					,[FolderID]
					,[CreatedBy]
					,[CreatedOn]
					,[ModifiedBy]
					,[ModifiedOn]
					,[Enabled]
					)
				VALUES
					(
					@ClientID
					,@Name
					,@Description
					,@TemplatePath
					,@DataWorksheetNumber
					,@DataStartCell
					,@TemplateFormat
					,@FolderID
					,@CreatedBy
					,@CreatedOn
					,@ModifiedBy
					,@ModifiedOn
					,@Enabled
					)
				-- Get the identity value
				SET @SqlQueryTemplateID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Insert] TO [sp_executeall]
GO
