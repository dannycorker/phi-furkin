SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuillCaseMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_Delete]
(

	@QuillCaseMappingID int   
)
AS


				DELETE FROM [dbo].[QuillCaseMapping] WITH (ROWLOCK) 
				WHERE
					[QuillCaseMappingID] = @QuillCaseMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Delete] TO [sp_executeall]
GO
