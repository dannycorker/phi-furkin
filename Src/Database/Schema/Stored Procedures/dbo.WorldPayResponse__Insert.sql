SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 27/07/2017
-- Description:	Insert into WorldPayResponse
-- =============================================
CREATE PROCEDURE [dbo].[WorldPayResponse__Insert]

	@WorldPayRequestID INT,
	@ClientID INT,
	@CustomerID INT,
	@MatterID INT,
	@ResponseXml VARCHAR(MAX),
	@WhenCreated DATETIME,
	@WhoCreated INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @XML xml = CONVERT (XML, @ResponseXml,2) 
	
	INSERT INTO [WorldPayResponse] ([WorldPayRequestID],[ClientID],[CustomerID],[MatterID],[ResponseXml],[WhenCreated],[WhoCreated])
    VALUES (@WorldPayRequestID, @ClientID, @CustomerID, @MatterID, @ResponseXml, @WhenCreated, @WhoCreated)

	SELECT SCOPE_IDENTITY() AS WorldPayResponseID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayResponse__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorldPayResponse__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayResponse__Insert] TO [sp_executeall]
GO
