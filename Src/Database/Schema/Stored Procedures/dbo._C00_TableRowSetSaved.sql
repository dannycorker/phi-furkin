SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-11-25
-- Description:	SAS TableRow Set Who and When saved
-- =============================================
CREATE PROCEDURE [dbo].[_C00_TableRowSetSaved]
@TableRowID int,
@ClientPersonnelID int,
@WhoSavedFieldID int,
@WhenSavedFieldID int,
@ClinetPersonnelIDFieldID int
AS
BEGIN

	DECLARE @UserName varchar(2000)
	
	SELECT @UserName = cp.UserName
	FROM ClientPersonnel cp WITH (NOLOCK)
	WHERE cp.ClientPersonnelID = @ClientPersonnelID

	UPDATE TableDetailValues
	SET DetailValue = CASE DetailFieldID 
		WHEN @WhoSavedFieldID THEN @UserName 
		WHEN @ClinetPersonnelIDFieldID THEN CONVERT(varchar,@ClientPersonnelID)
		ELSE CONVERT(varchar(10),dbo.fn_GetDate_Local(),120) 
		END
	FROM TableDetailValues
	WHERE DetailFieldID IN (@WhoSavedFieldID,@WhenSavedFieldID,@ClinetPersonnelIDFieldID)
	AND TableRowID = @TableRowID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_TableRowSetSaved] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_TableRowSetSaved] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_TableRowSetSaved] TO [sp_executeall]
GO
