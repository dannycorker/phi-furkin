SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AquariumApplication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumApplication_Delete]
(

	@AquariumApplicationID int   
)
AS


				DELETE FROM [dbo].[AquariumApplication] WITH (ROWLOCK) 
				WHERE
					[AquariumApplicationID] = @AquariumApplicationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumApplication_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Delete] TO [sp_executeall]
GO
