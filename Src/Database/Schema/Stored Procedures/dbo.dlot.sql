SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-28
-- Description:	DataLoaderObjectType helper
-- =============================================
CREATE PROCEDURE [dbo].[dlot]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * 
	FROM dbo.DataLoaderObjectType dlot WITH (NOLOCK) 
	ORDER BY dlot.DataLoaderObjectTypeID 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[dlot] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dlot] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dlot] TO [sp_executeall]
GO
