SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelAccess table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_GetByClientPersonnelAdminGroupIDClientIDLeadIDCaseID]
(

	@ClientPersonnelAdminGroupID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@CaseID int   
)
AS


				SELECT
					[ClientPersonnelAccessID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[LeadID],
					[CaseID],
					[AccessRuleID],
					[SourceLeadEventID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[AccessLevel]
				FROM
					[dbo].[ClientPersonnelAccess] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					AND [ClientID] = @ClientID
					AND [LeadID] = @LeadID
					AND [CaseID] = @CaseID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_GetByClientPersonnelAdminGroupIDClientIDLeadIDCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_GetByClientPersonnelAdminGroupIDClientIDLeadIDCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_GetByClientPersonnelAdminGroupIDClientIDLeadIDCaseID] TO [sp_executeall]
GO
