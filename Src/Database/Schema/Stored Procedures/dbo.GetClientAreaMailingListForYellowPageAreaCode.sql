SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetClientAreaMailingListForYellowPageAreaCode] @ClientQuestionnaireID int, @YellowPagesAreaCode nvarchar(10)
AS

Select ClientAreaMailingLists.Name, Email, PostCode, OutcomeID, YellowPagesAreaCode, ClientID, ClientQuestionnaireID, ClientAreaMailingListID, OnHold
From ClientAreaMailingLists
Where ( (ClientQuestionnaireID = @ClientQuestionnaireID) and (YellowPagesAreaCode = @YellowPagesAreaCode) )



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingListForYellowPageAreaCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientAreaMailingListForYellowPageAreaCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingListForYellowPageAreaCode] TO [sp_executeall]
GO
