SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-09-05
-- Description:	Saves join types for sql query editing tables
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingTables__SaveTableJoinTypes]
(
	@SqlQueryEditingID INT,
	@TableJoinTypes dbo.tvpIDValue READONLY
)
AS

	UPDATE t
	SET t.JoinType = d.AnyValue
	FROM dbo.SqlQueryEditingTable t
	INNER JOIN @TableJoinTypes d ON t.SqlQueryEditingTableID = d.AnyID
	WHERE t.SqlQueryEditingID = @SqlQueryEditingID




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTables__SaveTableJoinTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTables__SaveTableJoinTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTables__SaveTableJoinTypes] TO [sp_executeall]
GO
