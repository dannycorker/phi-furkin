SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CaseTransferStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferStatus_Update]
(

	@CaseTransferStatusID int   ,

	@Status varchar (60)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CaseTransferStatus]
				SET
					[Status] = @Status
				WHERE
[CaseTransferStatusID] = @CaseTransferStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Update] TO [sp_executeall]
GO
