SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_GetByThirdPartyFieldID]
(

	@ThirdPartyFieldID int   
)
AS


				SELECT
					[ThirdPartyFieldID],
					[ThirdPartySystemId],
					[FieldName],
					[FieldDescription],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyField] WITH (NOLOCK) 
				WHERE
										[ThirdPartyFieldID] = @ThirdPartyFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_GetByThirdPartyFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_GetByThirdPartyFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_GetByThirdPartyFieldID] TO [sp_executeall]
GO
