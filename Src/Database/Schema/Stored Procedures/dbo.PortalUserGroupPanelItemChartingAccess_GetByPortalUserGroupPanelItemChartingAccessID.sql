SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserGroupPanelItemChartingAccess table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPortalUserGroupPanelItemChartingAccessID]
(

	@PortalUserGroupPanelItemChartingAccessID int   
)
AS


				SELECT
					[PortalUserGroupPanelItemChartingAccessID],
					[ClientID],
					[PortalUserGroupID],
					[PanelItemChartingID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupPanelItemChartingAccess] WITH (NOLOCK) 
				WHERE
										[PortalUserGroupPanelItemChartingAccessID] = @PortalUserGroupPanelItemChartingAccessID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPortalUserGroupPanelItemChartingAccessID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPortalUserGroupPanelItemChartingAccessID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_GetByPortalUserGroupPanelItemChartingAccessID] TO [sp_executeall]
GO
