SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-03-2012
-- Description:	Inserts a lead for IC customer portal - work around for the IC batch job applying a process start
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertLeadForCustomerPortal]
	@LeadTypeID int,
	@CustomerID int,
	@ClientID int
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO Lead 
	(ClientID, LeadTypeID, CustomerID, AquariumStatusID, WhenCreated, Assigned, RecalculateEquations) 
	values (@ClientID, @LeadTypeID, @CustomerID, 2, dbo.fn_GetDate_Local() ,0, 0)
		
	Select SCOPE_IDENTITY() LeadID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertLeadForCustomerPortal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertLeadForCustomerPortal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertLeadForCustomerPortal] TO [sp_executeall]
GO
