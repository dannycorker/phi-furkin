SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-02-18
-- Description:	Get all DocumentZipFtpControl records for this client and client zero.
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipFtpControl__GetByClientIDInOrder] 
	@ClientID int,
	@DocumentTypeID int -- Can be NULL, eg when you are just processing the queue from the app.
AS
BEGIN
	
	SET NOCOUNT ON;
	
	-- Special processing only takes place if a batch job auto-inserted
	-- these records for a particular document type.
	-- However, regular processing (where controlRespondsToDocumentTypeID is 0)
	-- takes place under all circumstances unless a special control record exists
	-- with "Suppress all after me" set to true.
	SELECT con.[DocumentZipFtpControlID],
	con.[ClientID],
	con.[JobName],
	con.[JobNotes],
	con.[FiringOrder],
	con.[SuppressAllLaterJobs],
	con.[IsConfigFileRequired],
	con.[ConfigFileName],
	con.[IsBatFileRequired],
	con.[BatFileName],
	con.[Enabled],
	con.[MaxWaitTimeMS],
	con.[SpecificDocumentTypesOnly]
	FROM dbo.DocumentZipFtpControl con 
	WHERE con.ClientID IN (0, @ClientID) 
	AND con.Enabled = 1 
	AND ((con.SpecificDocumentTypesOnly = 0) OR EXISTS(SELECT * FROM dbo.DocumentZipFtpControlLink link WHERE link.ClientID = con.ClientID AND link.DocumentZipFtpControlID = con.DocumentZipFtpControlID AND link.Enabled = 1 AND link.DocumentTypeID = @DocumentTypeID))
	ORDER BY con.FiringOrder, con.ClientID 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFtpControl__GetByClientIDInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipFtpControl__GetByClientIDInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFtpControl__GetByClientIDInOrder] TO [sp_executeall]
GO
