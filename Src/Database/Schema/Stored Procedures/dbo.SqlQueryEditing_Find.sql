SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditing table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingID int   = null ,

	@ClientID int   = null ,

	@SqlQueryID int   = null ,

	@WhoIsEditing int   = null ,

	@EditStartedAt datetime   = null ,

	@QueryText varchar (MAX)  = null ,

	@QueryTitle varchar (250)  = null ,

	@LeadTypeID int   = null ,

	@FolderID int   = null ,

	@ParentQueryID int   = null ,

	@LockAllTables bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingID]
	, [ClientID]
	, [SqlQueryID]
	, [WhoIsEditing]
	, [EditStartedAt]
	, [QueryText]
	, [QueryTitle]
	, [LeadTypeID]
	, [FolderID]
	, [ParentQueryID]
	, [LockAllTables]
    FROM
	[dbo].[SqlQueryEditing] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryID] = @SqlQueryID OR @SqlQueryID IS NULL)
	AND ([WhoIsEditing] = @WhoIsEditing OR @WhoIsEditing IS NULL)
	AND ([EditStartedAt] = @EditStartedAt OR @EditStartedAt IS NULL)
	AND ([QueryText] = @QueryText OR @QueryText IS NULL)
	AND ([QueryTitle] = @QueryTitle OR @QueryTitle IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([FolderID] = @FolderID OR @FolderID IS NULL)
	AND ([ParentQueryID] = @ParentQueryID OR @ParentQueryID IS NULL)
	AND ([LockAllTables] = @LockAllTables OR @LockAllTables IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingID]
	, [ClientID]
	, [SqlQueryID]
	, [WhoIsEditing]
	, [EditStartedAt]
	, [QueryText]
	, [QueryTitle]
	, [LeadTypeID]
	, [FolderID]
	, [ParentQueryID]
	, [LockAllTables]
    FROM
	[dbo].[SqlQueryEditing] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryID] = @SqlQueryID AND @SqlQueryID is not null)
	OR ([WhoIsEditing] = @WhoIsEditing AND @WhoIsEditing is not null)
	OR ([EditStartedAt] = @EditStartedAt AND @EditStartedAt is not null)
	OR ([QueryText] = @QueryText AND @QueryText is not null)
	OR ([QueryTitle] = @QueryTitle AND @QueryTitle is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([FolderID] = @FolderID AND @FolderID is not null)
	OR ([ParentQueryID] = @ParentQueryID AND @ParentQueryID is not null)
	OR ([LockAllTables] = @LockAllTables AND @LockAllTables is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Find] TO [sp_executeall]
GO
