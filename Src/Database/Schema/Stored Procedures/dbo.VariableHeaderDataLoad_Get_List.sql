SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the VariableHeaderDataLoad table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[VariableHeaderDataLoad_Get_List]

AS


				
				SELECT
					[VariableHeaderDataLoadID],
					[ClientID],
					[ListID],
					[ImportID],
					[TokenKey],
					[CallbackMethod],
					[ActionToPerform],
					[DeleteListRecords],
					[EmailFailureReport],
					[EmailAddress],
					[FileName],
					[WhoUploaded],
					[DateTimeOfUpload],
					[Processed],
					[Notes],
					[DateTimeStart],
					[DateTimeEnd],
					[LeadTypeID]
				FROM
					[dbo].[VariableHeaderDataLoad] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Get_List] TO [sp_executeall]
GO
