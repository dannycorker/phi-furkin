SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the MessageRecipient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_Insert]
(

	@MessageRecipientID int    OUTPUT,

	@MessageID int   ,

	@ClientPersonnelID int   ,

	@PortalUserID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[MessageRecipient]
					(
					[MessageID]
					,[ClientPersonnelID]
					,[PortalUserID]
					,[ClientID]
					)
				VALUES
					(
					@MessageID
					,@ClientPersonnelID
					,@PortalUserID
					,@ClientID
					)
				-- Get the identity value
				SET @MessageRecipientID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Insert] TO [sp_executeall]
GO
