SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryEditingColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_Get_List]

AS


				
				SELECT
					[SqlQueryEditingColumnID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[CompleteOutputText],
					[ColumnAlias],
					[ShowColumn],
					[DisplayOrder],
					[SortOrder],
					[SortType],
					[IsAggregate],
					[ColumnDataType],
					[ManipulatedDataType],
					[ColumnNaturalName],
					[RealSqlQueryColumnID],
					[ComplexValue]
				FROM
					[dbo].[SqlQueryEditingColumns] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Get_List] TO [sp_executeall]
GO
