SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailFields table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_Delete]
(

	@DetailFieldID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[DetailFields] WITH (ROWLOCK) 
				WHERE
					[DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Delete] TO [sp_executeall]
GO
