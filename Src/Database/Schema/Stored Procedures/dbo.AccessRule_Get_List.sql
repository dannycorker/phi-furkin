SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_Get_List]

AS


				
				SELECT
					[AccessRuleID],
					[ClientID],
					[AccessRuleName],
					[AccessRuleDescription],
					[AccessRuleEnabled],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[PortalUserID],
					[DataLoaderObjectTypeID],
					[LeadTypeID],
					[DetailFieldSubTypeID],
					[DetailFieldPageID],
					[DetailFieldID],
					[ClientQuestionnaireID],
					[ValueDecoder],
					[ValueDecoderColumnFieldID],
					[ValueToCompare],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[AccessLevel]
				FROM
					[dbo].[AccessRule] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Get_List] TO [sp_executeall]
GO
