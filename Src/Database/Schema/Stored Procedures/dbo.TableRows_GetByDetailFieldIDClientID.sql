SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TableRows table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_GetByDetailFieldIDClientID]
(

	@DetailFieldID int   ,

	@ClientID int   
)
AS


				SELECT
					[TableRowID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailFieldPageID],
					[DenyEdit],
					[DenyDelete],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID],
					[SourceID]
				FROM
					[dbo].[TableRows] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_GetByDetailFieldIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_GetByDetailFieldIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_GetByDetailFieldIDClientID] TO [sp_executeall]
GO
