SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Date Created: 25-07-2011

-- Created By:  Slacky
-- Purpose: Show the most recent EventCount events for a lead in order of priority and date
-- securing notes to client personnel access rights
-- JWG 2014-05-21 #24161 Show comments as tooltip for all events, not just notes.
-- SB  2014-08-26 Changed to select top x as the ROWCOUNT was limiting the rows returned from the function
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[GetRecentEventsInOrderSecured]
(

	@LeadID INT   ,
	@CaseID INT = NULL,
	@EventCount INT = 5,
	@ShowDeleted BIT = 0,
	@ShowNotes BIT = 1,
	@ClientPersonnelID INT,
	@MinimumRightsRequired INT
)
AS
BEGIN

	--SET ROWCOUNT @EventCount

	SELECT TOP (@EventCount)
		le.LeadEventID AS 'EventID',
		le.LeadID AS 'LID',
		le.Cost AS 'Cost',
		le.Comments AS 'Comments',
		/*JWG 2014-05-21 #24161 Show comments as tooltip for all events, not just notes.*/
		/*CASE WHEN et.EventTypeID IS NULL THEN CAST(le.Comments AS VARBINARY(MAX)) ELSE NULL END AS 'Note',*/
		CASE WHEN et.EventTypeID IS NULL OR ISNULL(clop.EventCommentTooltipOff, 0) = 0 THEN CAST(le.Comments AS VARBINARY(MAX)) ELSE NULL END AS 'Note',
		ISNULL ( nt.NoteTypeName, et.EventTypeName) AS 'EventType',
		/* AMG: 2010-04-15 Replaced to include date and time
			convert(char,le.WhenCreated,103) AS 'When',
		*/
		CONVERT(CHAR(11),le.WhenCreated,103) + LEFT(CONVERT(CHAR(5),LE.WhenCreated,108),5) AS 'When',
		cp.UserName AS 'Who',
		le.EventDeleted,

		-- No changes allowed after this comment!
		-- Do not change the column order of these 3 Note fields!
		le.NotePriority AS [NotePriority],
		ISNULL(nt.AlertColour, '') AS [AlertColour],
		ISNULL(nt.NormalColour, '') AS [NormalColour]
		-- No more columns allowed after the Note fields!
	FROM
		LeadEvent le WITH (NOLOCK)  
		LEFT OUTER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID
		LEFT OUTER JOIN NoteType nt WITH (NOLOCK) ON le.NoteTypeID = nt.NoteTypeID
		--LEFT OUTER JOIN LeadDocument d WITH (NOLOCK) ON le.LeadDocumentId = d.LeadDocumentID
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON le.WhoCreated = cp.ClientPersonnelID
		LEFT JOIN ClientOption clop WITH (NOLOCK) ON clop.ClientID = cp.ClientID
		LEFT JOIN dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) snt ON snt.ObjectID = le.NoteTypeID
	WHERE 
		le.LeadID = @LeadID AND
		(le.CaseID = @CaseID OR @CaseID IS NULL) AND
		CASE
			WHEN @ShowDeleted = 0 AND (le.EventDeleted = 0 OR le.EventDeleted IS NULL) THEN 1
			WHEN @ShowDeleted = 1 THEN 1
		END = 1
		AND ((@ShowNotes = 1) OR (@ShowNotes = 0 AND le.NoteTypeID IS NULL))
		AND	
			CASE
				WHEN et.EventTypeID IS NULL AND snt.ObjectID IS NULL THEN 0
				ELSE 1 
			END = 1
			
	ORDER BY ISNULL(le.NotePriority,0) DESC, le.WhenCreated DESC, le.LeadEventID DESC
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetRecentEventsInOrderSecured] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetRecentEventsInOrderSecured] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetRecentEventsInOrderSecured] TO [sp_executeall]
GO
