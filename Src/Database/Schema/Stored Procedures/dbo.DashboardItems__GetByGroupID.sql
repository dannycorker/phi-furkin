SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-06
-- Description:	Get Group Items
-- =============================================
CREATE PROCEDURE [dbo].[DashboardItems__GetByGroupID] 
	@ClientID INT,
	@DashboardGroupID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT d.DashboardItemID, 
		d.DashboardGroupID, 
		d.ShowAs, 
		d.Title, 
		d.QueryID, 
		d.TransposeRequired, 
		d.ChartType, 
		d.Enabled, 
		d.WhenCreated, 
		d.WhoCreated, 
		d.WhenChanged, 
		d.WhoChanged,
		sq.QueryTitle
	FROM DashboardItems d WITH (NOLOCK)
	INNER JOIN SqlQuery sq WITH (NOLOCK) ON sq.QueryID = d.QueryID
	WHERE d.DashboardGroupID = @DashboardGroupID
	
	SELECT *
	FROM DashboardGroup dg WITH (NOLOCK) 
	WHERE dg.DashboardGroupID = @DashboardGroupID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItems__GetByGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItems__GetByGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItems__GetByGroupID] TO [sp_executeall]
GO
