SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the IncomingPostEventValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEventValue_Get_List]

AS


				
				SELECT
					[IncomingPostEventValueID],
					[IncomingPostEventID],
					[PostKey],
					[PostValue],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID]
				FROM
					[dbo].[IncomingPostEventValue] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEventValue_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Get_List] TO [sp_executeall]
GO
