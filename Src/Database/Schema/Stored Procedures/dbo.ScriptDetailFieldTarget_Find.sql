SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptDetailFieldTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptDetailFieldTargetID int   = null ,

	@ScriptID int   = null ,

	@ClientID int   = null ,

	@Target varchar (250)  = null ,

	@DetailFieldID int   = null ,

	@ColumnFieldID int   = null ,

	@Format varchar (250)  = null ,

	@SectionID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptDetailFieldTargetID]
	, [ScriptID]
	, [ClientID]
	, [Target]
	, [DetailFieldID]
	, [ColumnFieldID]
	, [Format]
	, [SectionID]
    FROM
	[dbo].[ScriptDetailFieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptDetailFieldTargetID] = @ScriptDetailFieldTargetID OR @ScriptDetailFieldTargetID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ColumnFieldID] = @ColumnFieldID OR @ColumnFieldID IS NULL)
	AND ([Format] = @Format OR @Format IS NULL)
	AND ([SectionID] = @SectionID OR @SectionID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptDetailFieldTargetID]
	, [ScriptID]
	, [ClientID]
	, [Target]
	, [DetailFieldID]
	, [ColumnFieldID]
	, [Format]
	, [SectionID]
    FROM
	[dbo].[ScriptDetailFieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptDetailFieldTargetID] = @ScriptDetailFieldTargetID AND @ScriptDetailFieldTargetID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ColumnFieldID] = @ColumnFieldID AND @ColumnFieldID is not null)
	OR ([Format] = @Format AND @Format is not null)
	OR ([SectionID] = @SectionID AND @SectionID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Find] TO [sp_executeall]
GO
