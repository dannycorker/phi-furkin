SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[ELMAH_GetErrorXml]
(
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

    SET NOCOUNT ON

    SELECT 
        [AllXml]
    FROM 
        [ELMAH_Error] WITH (NOLOCK) 
    WHERE
        [ErrorId] = @ErrorId
    AND
        [Application] = @Application





GO
GRANT VIEW DEFINITION ON  [dbo].[ELMAH_GetErrorXml] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ELMAH_GetErrorXml] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ELMAH_GetErrorXml] TO [sp_executeall]
GO
