SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-09-29
-- Description:	List all AutomatedTaskResults for a task
-- =============================================
CREATE PROCEDURE [dbo].[atrd] 
	@AgeInDays int = 1, 
	@ClientID int = NULL, 
	@TaskID int = NULL, 
	@MaxRows int = 500 
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ClientID IS NULL
	BEGIN
		SELECT TOP (@MaxRows) atr.* 
		FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
		WHERE atr.RunDate > dbo.fn_GetDate_Local() - @AgeInDays
		ORDER BY atr.AutomatedTaskResultID DESC 
	END
	ELSE
	BEGIN
		IF @TaskID IS NULL
		BEGIN
			SELECT TOP (@MaxRows) atr.* 
			FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
			WHERE atr.RunDate > dbo.fn_GetDate_Local() - @AgeInDays
			AND atr.ClientID = @ClientID 
			ORDER BY atr.AutomatedTaskResultID DESC 
		END
		ELSE
		BEGIN
			SELECT TOP (@MaxRows) atr.* 
			FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
			WHERE atr.RunDate > dbo.fn_GetDate_Local() - @AgeInDays
			AND atr.ClientID = @ClientID 
			AND atr.TaskID = @TaskID 
			ORDER BY atr.AutomatedTaskResultID DESC 
		END
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[atrd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[atrd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[atrd] TO [sp_executeall]
GO
