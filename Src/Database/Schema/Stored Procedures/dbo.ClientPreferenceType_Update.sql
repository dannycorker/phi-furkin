SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPreferenceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreferenceType_Update]
(

	@ClientPreferenceTypeID int   ,

	@PreferenceTypeName varchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPreferenceType]
				SET
					[PreferenceTypeName] = @PreferenceTypeName
				WHERE
[ClientPreferenceTypeID] = @ClientPreferenceTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreferenceType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Update] TO [sp_executeall]
GO
