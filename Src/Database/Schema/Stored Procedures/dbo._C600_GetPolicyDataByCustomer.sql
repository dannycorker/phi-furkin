SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-07-20
-- Description:	Policies – Get by Customer ID, return MatterIDs, Policy Numbers and Pet Name 
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetPolicyDataByCustomer] 
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT l.CustomerID[CustomerID], m.MatterID[Policy Admin MatterID], l.LeadRef[Policy Number], ldv.DetailValue[Pet Name]
	FROM Lead l WITH (NOLOCK)
	JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
	JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID
	WHERE l.CustomerID = @CustomerID
	AND l.LeadTypeID = 1492 /*Policy Admin*/
	AND ldv.DetailFieldID = 144268 /*Pet Name*/
	AND l.ClientID = @ClientID
	AND m.ClientID = @ClientID
	AND ldv.ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPolicyDataByCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetPolicyDataByCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPolicyDataByCustomer] TO [sp_executeall]
GO
