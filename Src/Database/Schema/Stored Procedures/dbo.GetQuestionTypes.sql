SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetQuestionTypes    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[GetQuestionTypes]
AS
select QuestionTypeID, QuestionTypes.Name, QuestionTypes.Description, QuestionTypes.EcatcherField
from QuestionTypes






GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionTypes] TO [sp_executeall]
GO
