SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-07-11
-- Description:	Test substitution of @TaskID by the scheduler
-- =============================================
CREATE PROCEDURE [dbo].[_JG_Demo_Test]
	@UserID int = null,
	@TaskID int = null
AS
BEGIN
	SET NOCOUNT ON;

	EXEC dbo._C00_LogIt 'Debug', 'dbo._JG_Demo_Test', 'EXEC', @TaskID, @UserID
	
	SELECT @TaskID as [Outcome]
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_JG_Demo_Test] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_JG_Demo_Test] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_JG_Demo_Test] TO [sp_executeall]
GO
