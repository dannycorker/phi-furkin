SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-10
-- Description:	Save Dashboard Group
-- =============================================
CREATE PROCEDURE [dbo].[DashboardGroup__SaveGroup]
	@DashboardGroupID INT = NULL,
	@ClientID INT,
	@GroupName VARCHAR(100),
	@ClientPersonnelAdminGroupID INT = NULL,
	@ClientPersonnelID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	IF @DashboardGroupID > 0
	BEGIN
	
		UPDATE DashboardGroup
		SET GroupName = @GroupName, ClientID = @ClientID, ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @ClientPersonnelID
		WHERE DashboardGroupID = @DashboardGroupID
		
	END
	ELSE
	BEGIN
	
		INSERT INTO DashboardGroup (GroupName, ClientID, ClientPersonnelAdminGroupID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@GroupName, @ClientID, @ClientPersonnelAdminGroupID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardGroup__SaveGroup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardGroup__SaveGroup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardGroup__SaveGroup] TO [sp_executeall]
GO
