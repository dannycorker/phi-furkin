SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientDetailValues_Update]
(

	@ClientDetailValueID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientDetailValues]
				SET
					[ClientID] = @ClientID
					,[DetailFieldID] = @DetailFieldID
					,[DetailValue] = @DetailValue
					,[ErrorMsg] = @ErrorMsg
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[SourceID] = @SourceID
				WHERE
[ClientDetailValueID] = @ClientDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_Update] TO [sp_executeall]
GO
