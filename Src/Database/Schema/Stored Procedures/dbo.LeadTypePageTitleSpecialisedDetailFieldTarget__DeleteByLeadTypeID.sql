SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


create PROCEDURE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget__DeleteByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget__DeleteByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget__DeleteByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget__DeleteByLeadTypeID] TO [sp_executeall]
GO
