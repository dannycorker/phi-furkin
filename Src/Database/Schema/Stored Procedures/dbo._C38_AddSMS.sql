SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_AddSMS]
(
@LeadEventID int
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@ClientID int,
	@myERROR int,
	@LeadID int,
	@EventTypeID int

	Select
	@ClientID = ldv.DetailValue, @EventTypeID = LeadEvent.EventTypeID
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	Inner Join LeadDetailValues ldv on ldv.LeadID = leadevent.leadid and ldv.DetailFieldID = 6453
	where leadevent.leadeventid = @LeadEventID 

Begin Tran

If @EventTypeID = 13770
Begin

	/* Add SMS Functionality */
	Update Clients
	Set AllowSMS = 1
	Where ClientID = @ClientID

	Select @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

END

If @EventTypeID = 13771
Begin

	/* Remove SMS Functionality */
	Update Clients
	Set AllowSMS = 0
	Where ClientID = @ClientID

	Select @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

END

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	GOTO END_NOW

HANDLE_ERROR:
    ROLLBACK TRAN

END_NOW:


END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_AddSMS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_AddSMS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_AddSMS] TO [sp_executeall]
GO
