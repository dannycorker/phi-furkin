SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-14
-- Description:	Get the ClientID for the given object
-- =============================================
Create PROCEDURE [dbo].[_C00_CheckObjectAndDetailFieldSubtype]
	@DetailFieldSubtypeID INT,
	@ObjectID INT
AS
BEGIN

	SET NOCOUNT ON;

	IF @DetailFieldSubtypeID = 1 -- Lead
	BEGIN
	
		SELECT ClientID
		FROM Lead l WITH (NOLOCK)
		WHERE l.LeadID = @ObjectID
	
	END

	IF @DetailFieldSubtypeID = 2 -- Matter
	BEGIN
	
		SELECT ClientID
		FROM Matter m WITH (NOLOCK)
		WHERE m.MatterID = @ObjectID
	
	END

	IF @DetailFieldSubtypeID = 6 -- Table
	BEGIN
	
		SELECT ClientID
		FROM TableRows t WITH (NOLOCK)
		WHERE t.TableRowID = @ObjectID
	
	END

	IF @DetailFieldSubtypeID = 10 -- Customer
	BEGIN
	
		SELECT ClientID
		FROM Customers c WITH (NOLOCK)
		WHERE c.CustomerID = @ObjectID
	
	END

	IF @DetailFieldSubtypeID = 11 -- Case
	BEGIN
	
		SELECT ClientID
		FROM Cases c WITH (NOLOCK)
		WHERE c.CaseID = @ObjectID
	
	END

	IF @DetailFieldSubtypeID = 12 -- Client
	BEGIN
	
		SELECT @ObjectID AS [ClientID]
	
	END

	IF @DetailFieldSubtypeID = 13 -- ClientPersonnel
	BEGIN
	
		SELECT ClientID 
		FROM ClientPersonnel cp WITH (NOLOCK)
		WHERE cp.ClientPersonnelID = @ObjectID
	
	END

	IF @DetailFieldSubtypeID = 14 -- Contact
	BEGIN
	
		SELECT ClientID 
		FROM Contact c WITH (NOLOCK)
		WHERE c.ContactID = @ObjectID
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CheckObjectAndDetailFieldSubtype] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CheckObjectAndDetailFieldSubtype] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CheckObjectAndDetailFieldSubtype] TO [sp_executeall]
GO
