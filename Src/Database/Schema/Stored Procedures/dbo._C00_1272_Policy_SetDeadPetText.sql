SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-11-27
-- Description:	Gets the status of the policy
-- Used by:		SAE and SAS
-- Mods
-- 2015-01-28 changed to using _C384_AddANote
-- 2015-01-28 added _C384_DeleteANote
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_SetDeadPetText] 
(
	@LeadID INT,
	@CaseID INT,
	@ClientPersonnelID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @MatterID INT
	SELECT @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	DECLARE @DateOfDeath DATE,
			@NoteText VARCHAR(2000)
			
	-- Add a dead pet note if we are saving a date of loss
	SELECT @DateOfDeath = ValueDate
	FROM dbo.LeadDetailValues WITH (NOLOCK) 
	WHERE LeadID = @PolicyLeadID
	AND DetailFieldID = 144271
	
	--SELECT @DateOfDeath
	
	IF @DateOfDeath IS NOT NULL
	BEGIN	
	
		IF NOT EXISTS (SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadID = @LeadID AND le.NoteTypeID = 801 AND le.NotePriority = 1 AND le.EventDeleted = 0)
		BEGIN
		
			SELECT @NoteText = 'Dead or lost: ' + ISNULL(CONVERT(VARCHAR,@DateOfDeath,103), '')
		
			--EXEC dbo._C00_AddANote @CaseID, 801, @NoteText, 1, @ClientPersonnelID, 1
			EXEC _C600_AddANote @CaseID, 801, @NoteText, 1, @ClientPersonnelID
			
			SELECT @NoteText, @CaseID
		
		END	 
		
	END
	ELSE
	BEGIN
	
		EXEC _C600_DeleteANote @CaseID, 801
	
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_SetDeadPetText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_SetDeadPetText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_SetDeadPetText] TO [sp_executeall]
GO
