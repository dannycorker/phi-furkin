SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Module table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Module_Find]
(

	@SearchUsingOR bit   = null ,

	@ModuleID int   = null ,

	@ModuleName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ModuleID]
	, [ModuleName]
    FROM
	[dbo].[Module] WITH (NOLOCK) 
    WHERE 
	 ([ModuleID] = @ModuleID OR @ModuleID IS NULL)
	AND ([ModuleName] = @ModuleName OR @ModuleName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ModuleID]
	, [ModuleName]
    FROM
	[dbo].[Module] WITH (NOLOCK) 
    WHERE 
	 ([ModuleID] = @ModuleID AND @ModuleID is not null)
	OR ([ModuleName] = @ModuleName AND @ModuleName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Module_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Find] TO [sp_executeall]
GO
