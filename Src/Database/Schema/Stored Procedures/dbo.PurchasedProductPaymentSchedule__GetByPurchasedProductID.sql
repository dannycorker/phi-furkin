SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-06-2016
-- Description:	Gets a product payment schedule
-- JEL 2017-10-11 Changed ordering to use PurchasedProductPaymentScheduleID after payment date rather than typeID 
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentSchedule__GetByPurchasedProductID]
	@PurchasedProductID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	SELECT 
	
	schedule.PurchasedProductPaymentScheduleID, schedule.ClientID	, schedule.CustomerID, schedule.AccountID, schedule.PurchasedProductID, schedule.ActualCollectionDate, schedule.CoverFrom, schedule.CoverTo, schedule.PaymentDate, schedule.PaymentNet, schedule.PaymentVAT, schedule.PaymentGross, schedule.PaymentStatusID, schedule.CustomerLedgerID, schedule.ReconciledDate, schedule.CustomerPaymentScheduleID, schedule.WhoCreated	, schedule.WhenCreated, schedule.PurchasedProductPaymentScheduleParentID, schedule.PurchasedProductPaymentScheduleTypeID, schedule.PaymentGroupedIntoID, 
	ps.* 
	
	FROM PurchasedProductPaymentSchedule schedule WITH ( NOLOCK )
	LEFT JOIN dbo.PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = schedule.PaymentStatusID
	WHERE schedule.PurchasedProductID=@PurchasedProductID AND schedule.ClientID=@ClientID
	ORDER BY schedule.PaymentDate ASC, schedule.PurchasedProductPaymentScheduleID ASC
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetByPurchasedProductID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentSchedule__GetByPurchasedProductID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetByPurchasedProductID] TO [sp_executeall]
GO
