SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetNewLeadsByClientQuestionnaireID] 
	@clientQuestionnaireID VARCHAR(50), 
	@clientID VARCHAR(50), 
	@sortBy VARCHAR(1000) 
AS
BEGIN

	DECLARE @allsql VARCHAR(8000)
	DECLARE @where VARCHAR (500)
	SET @where = ''

	IF(@clientQuestionnaireID IS NOT NULL)
	BEGIN
		SET @where = 'CustomerQuestionnaires.ClientQuestionnaireID = ' + @clientQuestionnaireID + ' AND'
	END

	SET @allsql = 'SELECT Customers.CustomerID, Customers.Fullname, Customers.EmailAddress, 
	Customers.DayTimeTelephoneNumber, Customers.Town, Customers.County, 
	Customers.Postcode, CustomerQuestionnaires.SubmissionDate, 
	CustomerQuestionnaires.CustomerQuestionnaireID, CustomerQuestionnaires.ClientQuestionnaireID, 
	ClientQuestionnaires.QuestionnaireTitle, Customers.AquariumStatusID, Lead.LeadID
	FROM dbo.Customers WITH (NOLOCK) 
	INNER JOIN dbo.CustomerQuestionnaires WITH (NOLOCK) ON Customers.CustomerID = CustomerQuestionnaires.CustomerID 
	INNER JOIN dbo.ClientQuestionnaires WITH (NOLOCK) ON CustomerQuestionnaires.ClientQuestionnaireID = ClientQuestionnaires.ClientQuestionnaireID 
	LEFT OUTER JOIN dbo.Lead WITH (NOLOCK) ON Lead.CustomerID = Customers.CustomerID
	WHERE '+ @where + ' (Customers.AquariumStatusID = 1) AND (Customers.ClientID = ' + @clientID + ') ORDER BY ' + @sortBy

	PRINT @allsql
	EXEC (@allsql)


END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetNewLeadsByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetNewLeadsByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetNewLeadsByClientQuestionnaireID] TO [sp_executeall]
GO
