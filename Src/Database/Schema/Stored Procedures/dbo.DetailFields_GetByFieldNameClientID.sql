SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFields table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_GetByFieldNameClientID]
(

	@FieldName varchar (50)  ,

	@ClientID int   
)
AS


				SELECT
					[DetailFieldID],
					[ClientID],
					[LeadOrMatter],
					[FieldName],
					[FieldCaption],
					[QuestionTypeID],
					[Required],
					[Lookup],
					[LookupListID],
					[LeadTypeID],
					[Enabled],
					[DetailFieldPageID],
					[FieldOrder],
					[MaintainHistory],
					[EquationText],
					[MasterQuestionID],
					[FieldSize],
					[LinkedDetailFieldID],
					[ValidationCriteriaFieldTypeID],
					[ValidationCriteriaID],
					[MinimumValue],
					[MaximumValue],
					[RegEx],
					[ErrorMessage],
					[ResourceListDetailFieldPageID],
					[TableDetailFieldPageID],
					[DefaultFilter],
					[ColumnEquationText],
					[Editable],
					[Hidden],
					[LastReferenceInteger],
					[ReferenceValueFormatID],
					[Encrypt],
					[ShowCharacters],
					[NumberOfCharactersToShow],
					[TableEditMode],
					[DisplayInTableView],
					[ObjectTypeID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[DetailFieldStyleID],
					[Hyperlink],
					[IsShared]
				FROM
					dbo.fnDetailFieldsShared(@ClientID)
				WHERE
										[FieldName] = @FieldName
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_GetByFieldNameClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_GetByFieldNameClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_GetByFieldNameClientID] TO [sp_executeall]
GO
