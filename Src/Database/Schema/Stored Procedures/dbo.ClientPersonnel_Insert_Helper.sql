SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2011-12-08
-- Description:	Allows a quick insert into the client personnel table correctly setting the context which this table enforces
-- JWG 2017-03-17 #42393 Cannot have these distributed transactions from other DB clusters
-- =============================================


CREATE PROCEDURE [dbo].[ClientPersonnel_Insert_Helper]
(
	@ClientID INT,
	@FirstName VARCHAR (100),
	@LastName VARCHAR (100),
	@EmailAddress VARCHAR (255),
	@Password VARCHAR (65) = NULL,
	@Salt VARCHAR (50) = NULL,
	@ClientOfficeID INT = NULL,
	@TitleID INT = NULL,
	@MiddleName VARCHAR (100) = NULL,
	@JobTitle VARCHAR (100) = NULL,
	@ClientPersonnelAdminGroupID INT = NULL,
	@MobileTelephone VARCHAR (50) = NULL,
	@HomeTelephone VARCHAR (50) = NULL,
	@OfficeTelephone VARCHAR (50) = NULL,
	@OfficeTelephoneExtension VARCHAR (50) = NULL,
	@ChargeOutRate MONEY = 0,
	@AttemptedLogins INT = 0,
	@AccountDisabled BIT = 0,
	@ManagerID INT = NULL,
	@LanguageID INT = 1,
	@SubClientID INT = NULL,
	@ForcePasswordChangeOn DATETIME = NULL,
	@ThirdPartySystemId INT = 0,
	@CustomerID INT = NULL,
	@IsAquarium BIT = 0
)
AS

BEGIN
		
	-- If we don't have an outer tranasction e.g. from the lead event 
	/*DECLARE @TranCount INT = @@TRANCOUNT
	IF @TranCount = 0
	BEGIN
		BEGIN TRAN
	END*/
	
	BEGIN TRY
			
		-- Get a new key from the master DB
		DECLARE @MasterClientPersonnelID INT
		EXEC @MasterClientPersonnelID = AquariusMaster.dbo.AQ_ClientPersonnel_Insert @ClientID, @EmailAddress, @ThirdPartySystemId
		
						
		IF @MasterClientPersonnelID > 0 
		BEGIN

			-- Set context info to allow access to table
			DECLARE @ContextInfo VARBINARY(100) = CAST('ClientPersonnel' AS VARBINARY)
			SET CONTEXT_INFO @ContextInfo
			
			SET IDENTITY_INSERT [dbo].[ClientPersonnel] ON
						
			INSERT INTO [dbo].[ClientPersonnel] ([ClientPersonnelID], [ClientID], [ClientOfficeID], [TitleID], [FirstName], [MiddleName], [LastName], [JobTitle], [Password], [ClientPersonnelAdminGroupID], 
												[MobileTelephone], [HomeTelephone], [OfficeTelephone], [OfficeTelephoneExtension], [EmailAddress], [ChargeOutRate], [Salt], [AttemptedLogins], 
												[AccountDisabled], [ManagerID], [LanguageID], [SubClientID], [ForcePasswordChangeOn], [ThirdPartySystemId],[CustomerID], IsAquarium)
			VALUES (@MasterClientPersonnelID, @ClientID, @ClientOfficeID, @TitleID, @FirstName, @MiddleName, @LastName, @JobTitle, @Password, @ClientPersonnelAdminGroupID, @MobileTelephone, 
					@HomeTelephone, @OfficeTelephone, @OfficeTelephoneExtension, @EmailAddress, @ChargeOutRate, @Salt, @AttemptedLogins, @AccountDisabled, @ManagerID, @LanguageID, 
					@SubClientID, @ForcePasswordChangeOn, @ThirdPartySystemId, @CustomerID, @IsAquarium)
			
			-- If we don't have an outer tran we can commit here
			/*IF @TranCount = 0
			BEGIN
				COMMIT
			END*/
			
			-- Clear the things we set above
			SET IDENTITY_INSERT [dbo].[ClientPersonnel] OFF
			SET CONTEXT_INFO 0x0
			
		END 
		
		-- Get the identity value
		RETURN @MasterClientPersonnelID		
		
	END TRY			
	BEGIN CATCH    
	    
	    -- If we have any tran open then we need to rollback
	    /*IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK
		END*/
		
		-- Clear the things we set above
		SET IDENTITY_INSERT [dbo].[ClientPersonnel] OFF
		SET CONTEXT_INFO 0x0
		
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
		DECLARE @ErrorState INT = ERROR_STATE()

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
        
    END CATCH		
			
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Insert_Helper] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Insert_Helper] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Insert_Helper] TO [sp_executeall]
GO
