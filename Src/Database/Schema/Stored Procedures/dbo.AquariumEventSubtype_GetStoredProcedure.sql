SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[AquariumEventSubtype_GetStoredProcedure]
(
	@AquariumEventSubtypeID int   
)
AS
	SET ANSI_NULLS ON
	
	SELECT
		[StoredProcedure]
	FROM
		[dbo].[AquariumEventSubtype] WITH (NOLOCK) 
	WHERE
		[AquariumEventSubtypeID] = @AquariumEventSubtypeID
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_GetStoredProcedure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_GetStoredProcedure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_GetStoredProcedure] TO [sp_executeall]
GO
