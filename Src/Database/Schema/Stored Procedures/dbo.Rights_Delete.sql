SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Rights table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Rights_Delete]
(

	@RightID int   
)
AS


				DELETE FROM [dbo].[Rights] WITH (ROWLOCK) 
				WHERE
					[RightID] = @RightID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Rights_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Delete] TO [sp_executeall]
GO
