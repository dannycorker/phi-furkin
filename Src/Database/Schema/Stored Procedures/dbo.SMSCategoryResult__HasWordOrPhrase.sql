SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-08-01
-- Description:	Checks the SMS Response Categorisation Results table for the given word or phrase
-- =============================================
CREATE PROCEDURE [dbo].[SMSCategoryResult__HasWordOrPhrase]
	@TableRowID INT,
	@WordOrPhrase VARCHAR(250)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadTypeID INT
	DECLARE @LeadID INT
	DECLARE @MatterID INT
	DECLARE @ClientID INT
	
	SELECT @MatterID=MatterID FROM TableRows WITH (NOLOCK) WHERE TableRowID=@TableRowID
	SELECT @LeadID=LeadID, @ClientID=ClientID FROM Matter WITH (NOLOCK)  WHERE MatterID=@MatterID
	SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

	DECLARE @WordOrPhraseColumnDetailFieldID INT	
	SELECT @WordOrPhraseColumnDetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=901 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID

	DECLARE @Count INT
	IF @WordOrPhraseColumnDetailFieldID IS NOT NULL AND @WordOrPhraseColumnDetailFieldID<>0
	BEGIN
	
		SELECT @Count=COUNT(TableRowID) FROM TableDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID=@WordOrPhraseColumnDetailFieldID AND 
		TableRowID=@TableRowID AND DetailValue LIKE '%' + @WordOrPhrase + '%'
	
	END

	SELECT @Count HasWordOrPhrase
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__HasWordOrPhrase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCategoryResult__HasWordOrPhrase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__HasWordOrPhrase] TO [sp_executeall]
GO
