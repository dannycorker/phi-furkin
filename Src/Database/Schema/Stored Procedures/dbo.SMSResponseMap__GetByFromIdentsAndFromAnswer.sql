SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-04-2013
-- Description:	Gets a list of Responses by there from idents and the from answer
-- =============================================
CREATE PROCEDURE [dbo].[SMSResponseMap__GetByFromIdentsAndFromAnswer]
(
	@SMSQuestionnaireID INT,
	@SMSQuestionGroupID INT,
	@SMSQuestionID INT,
	@FromAnswer VARCHAR(25)
)
AS


				SET ANSI_NULLS ON
				
				SELECT *
					
				FROM
					[dbo].[SMSResponseMap] WITH (NOLOCK) 
				WHERE
					[SMSQuestionnaireID] = @SMSQuestionnaireID AND
					[FromSMSQuestionGroupID] = @SMSQuestionGroupID AND
					[FromSMSQuestionID] = @SMSQuestionID AND
					[FromAnswer] = @FromAnswer
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByFromIdentsAndFromAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap__GetByFromIdentsAndFromAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByFromIdentsAndFromAnswer] TO [sp_executeall]
GO
