SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Functions table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_GetByModuleID]
(

	@ModuleID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FunctionID],
					[FunctionName],
					[FunctionDescription],
					[FunctionTypeID],
					[ModuleID],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[Functions] WITH (NOLOCK) 
				WHERE
					[ModuleID] = @ModuleID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_GetByModuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_GetByModuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_GetByModuleID] TO [sp_executeall]
GO
