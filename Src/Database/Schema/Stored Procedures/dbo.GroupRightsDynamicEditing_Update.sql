SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the GroupRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamicEditing_Update]
(

	@GroupRightsDynamicEditingID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[GroupRightsDynamicEditing]
				SET
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[FunctionTypeID] = @FunctionTypeID
					,[LeadTypeID] = @LeadTypeID
					,[ObjectID] = @ObjectID
					,[RightID] = @RightID
				WHERE
[GroupRightsDynamicEditingID] = @GroupRightsDynamicEditingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamicEditing_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Update] TO [sp_executeall]
GO
