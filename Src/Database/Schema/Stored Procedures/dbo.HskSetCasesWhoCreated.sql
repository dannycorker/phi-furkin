SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-04-22
-- Description:	Set Cases.WhoCreated where it is unknown (nightly housekeeping)
-- =============================================
CREATE PROCEDURE [dbo].[HskSetCasesWhoCreated] 
AS
BEGIN
	SET NOCOUNT ON;

	/* Use first event on Case if any exist. */
	/* (The semi-colon is required for the WITH...AS syntax) */
	;
	WITH FirstEvent AS 
	(
		SELECT le.CaseID, min(le.LeadEventID) as [FirstLeadEventID] 
		FROM dbo.Cases ca WITH (NOLOCK) 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.CaseID = ca.CaseID 
		WHERE ca.WhoCreated IS NULL
		GROUP BY le.CaseID 
	)
	UPDATE dbo.Cases 
	SET WhoCreated = le.WhoCreated, 
	WhoModified = le.WhoCreated 
	FROM dbo.Cases ca 
	INNER JOIN FirstEvent f on f.CaseID = ca.CaseID 
	INNER JOIN dbo.LeadEvent le on le.LeadEventID = f.FirstLeadEventID
	
	/* Now set everything else to the 'Aquarium Automation' user for each client. */
	UPDATE dbo.Cases 
	SET WhoCreated = cp.ClientPersonnelID, 
	WhoModified = cp.ClientPersonnelID 
	FROM dbo.Cases ca 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientID = ca.ClientID AND cp.UserName = 'Aquarium Automation' 
	WHERE ca.WhoCreated IS NULL 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[HskSetCasesWhoCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HskSetCasesWhoCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HskSetCasesWhoCreated] TO [sp_executeall]
GO
