SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-01-13
-- Description:	Apply a leadevent with VARBINARY version of an XML request
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateLetterInEventWithXmlAttachment]
	@CaseID INT,
	@EventTypeID INT,
	@WhoCreated INT,
	@Comments VARCHAR (250),
	@XMLContent XML,
	@DocumentTitle VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Now DATETIME = dbo.fn_GetDate_Local(),
			@LeadEventTable dbo.tvpleadevent,
			@ClientID INT 	,
			@LeadDocumentID INT,
			@LeadID INT,
			@Output VARBINARY (MAX)
	
	IF EXISTS (SELECT * FROM EventType et WITH (NOLOCK) where et.EventTypeID = @EventTypeID and et.InProcess = 0) 
	BEGIN
		
		SELECT @LeadID = c.leadID, @ClientID = c.ClientID, @DocumentTitle = @DocumentTitle + '.txt' FROM Cases c WITH (NOLOCK) where c.CaseID = @CaseID
	
		/*First convert the XML content to a format we can understand as a leaddocument*/	
		select @Output = CAST(@XMLContent as  VARBINARY(MAX)) 
		
		/*Cracking, add it as a lead document*/
		EXEC @LeadDocumentID =  _C00_CreateLeadDocument   @ClientID,@LeadID,@Output,@DocumentTitle,NULL,@Now,@WhoCreated,@DocumentTitle,NULL,'TXT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		
		/*Now add the event and assign the above lead document to it*/ 
		INSERT INTO @LeadEventTable (ClientID,LeadID,WhenCreated,WhoCreated,Comments,EventTypeID,CaseID,LeadDocumentID,NotePriority,EventDeleted)
		VALUES (@ClientID,@LeadID,@Now,@WhoCreated,@Comments,@EventTypeID,@CaseID,@LeadDocumentID,0,0) 
	
		EXEC _C00_ApplyLeadEvent @LeadEventTable,-1 
	
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateLetterInEventWithXmlAttachment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateLetterInEventWithXmlAttachment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateLetterInEventWithXmlAttachment] TO [sp_executeall]
GO
