SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-12-02
-- Description:	Helpful details about a LeadEvent
-- =============================================
CREATE PROCEDURE [dbo].[LeadEvent__Describe] 
	@LeadEventID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ThisEventTypeID int

	/*-- Headline information
	SELECT 'Headline Info' as [Headline Info], et.EventTypeID, et.EventTypeName, et.Enabled, et.AvailableManually, et.InProcess, et.KeyEvent, et.EventSubtypeID 
	FROM EventType et
	WHERE et.EventTypeID = @EventTypeID */

	-- Show all values
	SELECT 'Lead Event' as [Lead Event Info], le.* 
	FROM dbo.LeadEvent le (nolock)
	WHERE le.LeadEventID = @LeadEventID  

	SELECT @ThisEventTypeID = EventTypeID 
	FROM dbo.LeadEvent (nolock) 
	WHERE LeadEventID = @LeadEventID 
	
	IF EXISTS ( SELECT * 
	            FROM dbo.EventTypeSql sae WITH (NOLOCK) 
	            WHERE sae.EventTypeID = @ThisEventTypeID )
	BEGIN
		SELECT REPLACE(sae.PostUpdateSql,'@LeadEventID', convert(varchar,@LeadEventID)) [Rerun SAE]
		FROM dbo.EventTypeSql sae WITH (NOLOCK) 
		WHERE sae.EventTypeID = @ThisEventTypeID
	END

	EXEC dbo.EventType__Describe @ThisEventTypeID 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__Describe] TO [sp_executehelper]
GO
