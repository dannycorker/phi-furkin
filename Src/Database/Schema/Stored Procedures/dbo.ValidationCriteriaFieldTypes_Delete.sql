SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ValidationCriteriaFieldTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriteriaFieldTypes_Delete]
(

	@ValidationCriteriaFieldTypeID int   
)
AS


				DELETE FROM [dbo].[ValidationCriteriaFieldTypes] WITH (ROWLOCK) 
				WHERE
					[ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriteriaFieldTypes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_Delete] TO [sp_executeall]
GO
