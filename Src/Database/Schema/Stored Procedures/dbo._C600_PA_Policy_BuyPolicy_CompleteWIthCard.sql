SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-04
-- Description:	Complete purchase by card
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthCard]
	@MatterID INT,
	@CardTransactionID INT,
	@CreditCardToken VARCHAR(2000),
	@CreditCardName VARCHAR(2000),
	@CreditCardMasked VARCHAR(2000),
	@CreditCardExpire DATE,
	@ErrorCode VARCHAR(100),
	@ErrorMessage VARCHAR(500),
	@TransactionID VARCHAR(50),
	@AuthCode VARCHAR(250),
	@StatusMsg VARCHAR(250)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CardExpireVC VARCHAR(10) = ISNULL(CONVERT(VARCHAR(2000), @CreditCardExpire, 120), '')
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	DECLARE @AutomationUserID INT = dbo.fn_C00_GetAutomationUser(@ClientID)

	/*Upate the ct record with +ve or -ve result..*/
	UPDATE CardTransaction
	SET ErrorCode = @ErrorCode,
		ErrorMessage = @ErrorMessage,
		TransactionID = @TransactionID,
		AuthCode = @AuthCode,
		StatusMsg = @StatusMsg
	WHERE CardTransactionID=@CardTransactionID
	
	EXEC dbo._C00_SimpleValueIntoField 170174, @CardExpireVC, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170171, @CreditCardToken, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170172, @CreditCardName, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170173, @CreditCardMasked, @MatterID, @AutomationUserID
	/*Set pay method to card*/
	EXEC dbo._C00_SimpleValueIntoField 170114, '69931', @MatterID, @AutomationUserID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthCard] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthCard] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthCard] TO [sp_executeall]
GO
