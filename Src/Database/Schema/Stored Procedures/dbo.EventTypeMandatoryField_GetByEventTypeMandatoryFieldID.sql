SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeMandatoryField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField_GetByEventTypeMandatoryFieldID]
(

	@EventTypeMandatoryFieldID int   
)
AS


				SELECT
					[EventTypeMandatoryFieldID],
					[EventTypeID],
					[DetailFieldID],
					[LeadTypeID],
					[ClientID],
					[ForceEditableOverride]
				FROM
					[dbo].[EventTypeMandatoryField] WITH (NOLOCK) 
				WHERE
										[EventTypeMandatoryFieldID] = @EventTypeMandatoryFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_GetByEventTypeMandatoryFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField_GetByEventTypeMandatoryFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_GetByEventTypeMandatoryFieldID] TO [sp_executeall]
GO
