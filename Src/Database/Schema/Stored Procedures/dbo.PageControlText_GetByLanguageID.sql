SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PageControlText table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_GetByLanguageID]
(

	@LanguageID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PageControlTextID],
					[ClientID],
					[LanguageID],
					[PageName],
					[ControlName],
					[ControlText],
					[TooltipText],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[PageControlText] WITH (NOLOCK) 
				WHERE
					[LanguageID] = @LanguageID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_GetByLanguageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_GetByLanguageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_GetByLanguageID] TO [sp_executeall]
GO
