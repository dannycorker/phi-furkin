SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FAS_FeesAndCharges table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_FeesAndCharges_Insert]
(

	@FasFeesAndChargesID int    OUTPUT,

	@FasCheckerDataID int   ,

	@Description varchar (512)  ,

	@Amount decimal (18, 0)  ,

	@WhenPaid varchar (50)  
)
AS


				
				INSERT INTO [dbo].[FAS_FeesAndCharges]
					(
					[FasCheckerDataID]
					,[Description]
					,[Amount]
					,[WhenPaid]
					)
				VALUES
					(
					@FasCheckerDataID
					,@Description
					,@Amount
					,@WhenPaid
					)
				-- Get the identity value
				SET @FasFeesAndChargesID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_FeesAndCharges_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Insert] TO [sp_executeall]
GO
