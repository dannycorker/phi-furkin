SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-11-01
-- Description:	Get Detail Field ID From Field and LeadType
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping__GetByClientLeadTypeAndThirdPartyFieldID] 
	@ClientID INT, 
	@LeadTypeID INT,
	@ThirdPartyFieldID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	AND t.ThirdPartyFieldID = @ThirdPartyFieldID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetByClientLeadTypeAndThirdPartyFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping__GetByClientLeadTypeAndThirdPartyFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetByClientLeadTypeAndThirdPartyFieldID] TO [sp_executeall]
GO
