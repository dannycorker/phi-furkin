SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudicationType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeName]
(

	@AutoAdjudicationTypeName varchar (50)  
)
AS


				SELECT
					[AutoAdjudicationTypeID],
					[AutoAdjudicationTypeName]
				FROM
					[dbo].[AutoAdjudicationType] WITH (NOLOCK) 
				WHERE
										[AutoAdjudicationTypeName] = @AutoAdjudicationTypeName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeName] TO [sp_executeall]
GO
