SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 15/04/2016
-- Description:	Gets a member of the callback queue
-- =============================================
CREATE PROCEDURE [dbo].[CallbackQueue__GetOldestMember]
	@ClientID INT
AS
BEGIN	
	
	SET NOCOUNT ON;

	SELECT TOP 1 * FROM CallBackQueue WITH (NOLOCK)
	WHERE ClientID=@ClientID 
	ORDER BY DateCreated DESC	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[CallbackQueue__GetOldestMember] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CallbackQueue__GetOldestMember] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CallbackQueue__GetOldestMember] TO [sp_executeall]
GO
