SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteOutcomeCriteriaForOutcome    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[DeleteOutcomeCriteriaForOutcome] @OutcomeID int
AS
Delete From OutcomeCriterias
Where OutcomeID = @OutcomeID





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeCriteriaForOutcome] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteOutcomeCriteriaForOutcome] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeCriteriaForOutcome] TO [sp_executeall]
GO
