SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingTemplate table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyFieldGroupID]
(

	@ThirdPartyFieldGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyMappingTemplateID],
					[ThirdPartyFieldGroupID],
					[ThirdPartySystemID],
					[Name],
					[Description],
					[XMLItemTemplate],
					[XMLContainerName],
					[XMLItemName],
					[XMLItemKey]
				FROM
					[dbo].[ThirdPartyMappingTemplate] WITH (NOLOCK) 
				WHERE
					[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyFieldGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyFieldGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyFieldGroupID] TO [sp_executeall]
GO
