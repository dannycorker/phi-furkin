SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AquariumEventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_Delete]
(

	@AquariumEventSubtypeID int   
)
AS


				DELETE FROM [dbo].[AquariumEventSubtype] WITH (ROWLOCK) 
				WHERE
					[AquariumEventSubtypeID] = @AquariumEventSubtypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Delete] TO [sp_executeall]
GO
