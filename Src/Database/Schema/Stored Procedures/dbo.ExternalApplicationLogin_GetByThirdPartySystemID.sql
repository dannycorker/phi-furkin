SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ExternalApplicationLogin table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_GetByThirdPartySystemID]
(

	@ThirdPartySystemID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ExternalApplicationLoginID],
					[ThirdPartySystemID],
					[ClientID],
					[SubClientID],
					[UserName],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[ClientPersonnelAdminGroupID],
					[AttemptedLogins],
					[AccountDisabled],
					[CustomerID]
				FROM
					[dbo].[ExternalApplicationLogin] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemID] = @ThirdPartySystemID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_GetByThirdPartySystemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_GetByThirdPartySystemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_GetByThirdPartySystemID] TO [sp_executeall]
GO
