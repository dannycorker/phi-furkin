SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_GetByMasterQuestionID]
(

	@MasterQuestionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailFieldLinkID],
					[ClientID],
					[DetailFieldID],
					[MasterQuestionID],
					[LeadTypeID]
				FROM
					[dbo].[DetailFieldLink] WITH (NOLOCK) 
				WHERE
					[MasterQuestionID] = @MasterQuestionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_GetByMasterQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_GetByMasterQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_GetByMasterQuestionID] TO [sp_executeall]
GO
