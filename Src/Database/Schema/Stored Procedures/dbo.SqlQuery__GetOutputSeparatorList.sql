SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-27
-- Description:	List valid separator characters for SqlQuery output
-- =============================================
CREATE PROCEDURE [dbo].[SqlQuery__GetOutputSeparatorList] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	c.AsciiValue, 
	CASE c.AsciiValue 
		WHEN 0 THEN 'None' 
		WHEN 124 THEN 'Pipe' 
		ELSE c.CharDescription 
	END AS CharDescription 
	FROM dbo.CharMap c WITH (NOLOCK) 
	WHERE c.AsciiValue IN (
		0,   /* NULL */
		9,   /* Tab */
		44,  /* Comma */
		124  /* Pipe (Vertical bar) */
	)
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetOutputSeparatorList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__GetOutputSeparatorList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetOutputSeparatorList] TO [sp_executeall]
GO
