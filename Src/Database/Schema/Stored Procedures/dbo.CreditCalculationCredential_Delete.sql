SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CreditCalculationCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_Delete]
(

	@CreditCalculationCredentialID int   
)
AS


				DELETE FROM [dbo].[CreditCalculationCredential] WITH (ROWLOCK) 
				WHERE
					[CreditCalculationCredentialID] = @CreditCalculationCredentialID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Delete] TO [sp_executeall]
GO
