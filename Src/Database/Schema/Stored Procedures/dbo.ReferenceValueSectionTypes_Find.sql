SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReferenceValueSectionTypes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueSectionTypes_Find]
(

	@SearchUsingOR bit   = null ,

	@ReferenceValueSectionTypeID int   = null ,

	@SectionTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReferenceValueSectionTypeID]
	, [SectionTypeName]
    FROM
	[dbo].[ReferenceValueSectionTypes] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueSectionTypeID] = @ReferenceValueSectionTypeID OR @ReferenceValueSectionTypeID IS NULL)
	AND ([SectionTypeName] = @SectionTypeName OR @SectionTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReferenceValueSectionTypeID]
	, [SectionTypeName]
    FROM
	[dbo].[ReferenceValueSectionTypes] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueSectionTypeID] = @ReferenceValueSectionTypeID AND @ReferenceValueSectionTypeID is not null)
	OR ([SectionTypeName] = @SectionTypeName AND @SectionTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueSectionTypes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Find] TO [sp_executeall]
GO
