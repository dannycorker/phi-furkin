SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-06-12
-- Description:	Gets the authorise request data
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__GetAuthoriseRequest]
	@ClientID int,
	@CustomerID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT tpm.ThirdPartyFieldID, cdv.DetailValue, cdv.CustomerID
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON 
		tpm.DetailFieldID = cdv.DetailFieldID AND 
		tpm.LeadTypeID = 0 AND tpm.IsEnabled = 1 AND 
		tpm.ThirdPartyFieldID IN (593,584,585, 645,646,647,648)
	WHERE cdv.ClientID = @ClientID
	AND cdv.CustomerID = @CustomerID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetAuthoriseRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__GetAuthoriseRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetAuthoriseRequest] TO [sp_executeall]
GO
