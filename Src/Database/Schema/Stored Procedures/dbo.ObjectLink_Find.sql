SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ObjectLink table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_Find]
(

	@SearchUsingOR bit   = null ,

	@ObjectLinkID int   = null ,

	@FromObjectID int   = null ,

	@ToObjectID int   = null ,

	@ObjectTypeRelationshipID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ObjectLinkID]
	, [FromObjectID]
	, [ToObjectID]
	, [ObjectTypeRelationshipID]
    FROM
	[dbo].[ObjectLink] WITH (NOLOCK) 
    WHERE 
	 ([ObjectLinkID] = @ObjectLinkID OR @ObjectLinkID IS NULL)
	AND ([FromObjectID] = @FromObjectID OR @FromObjectID IS NULL)
	AND ([ToObjectID] = @ToObjectID OR @ToObjectID IS NULL)
	AND ([ObjectTypeRelationshipID] = @ObjectTypeRelationshipID OR @ObjectTypeRelationshipID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ObjectLinkID]
	, [FromObjectID]
	, [ToObjectID]
	, [ObjectTypeRelationshipID]
    FROM
	[dbo].[ObjectLink] WITH (NOLOCK) 
    WHERE 
	 ([ObjectLinkID] = @ObjectLinkID AND @ObjectLinkID is not null)
	OR ([FromObjectID] = @FromObjectID AND @FromObjectID is not null)
	OR ([ToObjectID] = @ToObjectID AND @ToObjectID is not null)
	OR ([ObjectTypeRelationshipID] = @ObjectTypeRelationshipID AND @ObjectTypeRelationshipID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Find] TO [sp_executeall]
GO
