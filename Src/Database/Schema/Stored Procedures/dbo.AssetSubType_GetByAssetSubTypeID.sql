SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AssetSubType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetSubType_GetByAssetSubTypeID]
(

	@AssetSubTypeID int   
)
AS


				SELECT
					[AssetSubTypeID],
					[AssetSubTypeName],
					[Note],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[AssetSubType] WITH (NOLOCK) 
				WHERE
										[AssetSubTypeID] = @AssetSubTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_GetByAssetSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetSubType_GetByAssetSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_GetByAssetSubTypeID] TO [sp_executeall]
GO
