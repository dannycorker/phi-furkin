SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetOutcomeCriteriaFromID    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[GetOutcomeCriteriaFromID] @OutcomeCriteriaID int
AS

SELECT     OutcomeCriterias.OutcomeID, QuestionPossibleAnswers.AnswerText, OutcomeCriterias.OutcomeCriteriaID, 
                      OutcomeCriterias.QuestionPossibleAnswerID, QuestionPossibleAnswers.MasterQuestionID
FROM         QuestionPossibleAnswers INNER JOIN
                      OutcomeCriterias ON QuestionPossibleAnswers.QuestionPossibleAnswerID = .OutcomeCriterias.QuestionPossibleAnswerID
WHERE     (OutcomeCriterias.OutcomeCriteriaID = @OutcomeCriteriaID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCriteriaFromID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeCriteriaFromID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCriteriaFromID] TO [sp_executeall]
GO
