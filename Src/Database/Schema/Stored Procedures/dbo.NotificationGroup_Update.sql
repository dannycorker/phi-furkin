SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the NotificationGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroup_Update]
(

	@NotificationGroupID int   ,

	@ClientID int   ,

	@NotificationGroupName varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[NotificationGroup]
				SET
					[ClientID] = @ClientID
					,[NotificationGroupName] = @NotificationGroupName
					,[Enabled] = @Enabled
					,[Deleted] = @Deleted
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[NotificationGroupID] = @NotificationGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_Update] TO [sp_executeall]
GO
