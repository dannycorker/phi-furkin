SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











/*
----------------------------------------------------------------------------------------------------

-- Created By:  Jim Green
-- Purpose: Select Lead Type records, as long as the user has access to them
-- but this time, only return lead types they have full rights to.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType__GetByClientIDSecureEditOnly]
(

	@ClientID int,
	@UserID int    
)
AS


				SET ANSI_NULLS OFF
				
				SELECT lt.*
									
				FROM
					[dbo].[LeadType] lt INNER JOIN fnLeadTypeSecure(@UserID) f ON lt.LeadTypeID = f.LeadTypeID
				WHERE
					[ClientID] = @ClientID
				AND f.rightid > 1
				ORDER BY 
					[LeadTypeName]
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			














GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDSecureEditOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType__GetByClientIDSecureEditOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDSecureEditOnly] TO [sp_executeall]
GO
