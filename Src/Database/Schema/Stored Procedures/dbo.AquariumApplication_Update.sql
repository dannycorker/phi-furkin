SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AquariumApplication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumApplication_Update]
(

	@AquariumApplicationID int   ,

	@ApplicationName varchar (50)  ,

	@ApplicationDescription varchar (250)  ,

	@CurrentVersion varchar (50)  ,

	@MinimumVersion varchar (50)  ,

	@CurrentVersionValidFrom datetime   ,

	@CurrentVersionValidTo datetime   ,

	@CurrentVersionDeployedOn datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AquariumApplication]
				SET
					[ApplicationName] = @ApplicationName
					,[ApplicationDescription] = @ApplicationDescription
					,[CurrentVersion] = @CurrentVersion
					,[MinimumVersion] = @MinimumVersion
					,[CurrentVersionValidFrom] = @CurrentVersionValidFrom
					,[CurrentVersionValidTo] = @CurrentVersionValidTo
					,[CurrentVersionDeployedOn] = @CurrentVersionDeployedOn
				WHERE
[AquariumApplicationID] = @AquariumApplicationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumApplication_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Update] TO [sp_executeall]
GO
