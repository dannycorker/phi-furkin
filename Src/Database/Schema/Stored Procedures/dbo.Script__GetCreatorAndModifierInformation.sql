SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 19-03-2015
-- Description:	Get Script and creator/modifier information
-- =============================================
CREATE PROCEDURE [dbo].[Script__GetCreatorAndModifierInformation]

	@ScriptID INT

AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT s.*, 
		 cpCreator.FirstName CreatorFirstName, cpCreator.MiddleName CreatorMiddleName, cpCreator.LastName CreatorLastName,
		 cpModifier.FirstName ModifierFirstName, cpModifier.MiddleName ModifierMiddleName, cpModifier.LastName ModifierLastName
	FROM Script s WITH (NOLOCK) 
	LEFT JOIN ClientPersonnel cpCreator WITH (NOLOCK) on cpCreator.ClientPersonnelID=s.WhoCreated
	LEFT JOIN ClientPersonnel cpModifier WITH (NOLOCK) on cpModifier.ClientPersonnelID=s.WhoModified
	WHERE s.ScriptID=@ScriptID
    
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetCreatorAndModifierInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetCreatorAndModifierInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetCreatorAndModifierInformation] TO [sp_executeall]
GO
