SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- ALTER date: 2011-10-24
-- Description:	List all AutomatedTasks in a waiting state.
-- JWG 2013-02-28 Only show tasks for clients who are still in the current database.
-- =============================================
CREATE PROCEDURE [dbo].[atw] 
	@QueueID int = NULL, 
	@ClientID int = NULL,
	@ShowAll bit = 0
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ServerName sysname, @DatabaseName sysname

	SELECT @ServerName = CAST(SERVERPROPERTY('Servername') AS sysname), @DatabaseName = DB_NAME()

	SELECT @ServerName AS [Current Server], @DatabaseName AS [Current Database]
	
	/* Count waiting tasks by QueueID and status */
	;WITH Tasks AS 
	(
		SELECT ati.QueueID, 
		CASE 
			WHEN at.AlreadyRunning = 1 THEN 'Already Running' 
			WHEN LockDateTime IS NOT NULL THEN 'Locked to a Scheduler' 
			ELSE 'Waiting' 
		END as [Status],
		count(*) as [Total] 
		FROM dbo.AutomatedTask at WITH (NOLOCK) 
		INNER JOIN [AquariusMaster].dbo.ClientDatabase cd WITH (NOLOCK) ON cd.ClientID = at.ClientID AND cd.ServerName = @ServerName AND cd.DatabaseName = @DatabaseName
		LEFT JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID 
		WHERE at.Enabled = 1
		AND at.NextRunDateTime < dbo.fn_GetDate_Local()
		AND at.ClientID not in (224)
		GROUP BY ati.QueueID,
		CASE 
			WHEN at.AlreadyRunning = 1 THEN 'Already Running' 
			WHEN LockDateTime IS NOT NULL THEN 'Locked to a Scheduler' 
			ELSE 'Waiting' 
		END 
	)
	SELECT t.QueueID, t.Status, t.Total
	FROM Tasks t 
	UNION ALL
	SELECT 0, 'All Tasks', SUM(t.Total) 
	FROM Tasks t 
	ORDER BY t.QueueID, t.Status 
	
	/*
		Show any tasks that are running right now.
		This includes any that claim to be running but aren't any
		longer if the scheduler crashed or we shut it down.
	*/
	SELECT 
	CASE 
		WHEN at.AlreadyRunning = 1 THEN 'Already Running' 
		WHEN LockDateTime IS NOT NULL THEN 'Locked to a Scheduler' 
		ELSE 'Waiting' 
	END as [Wait Status], ati.*, at.* 
	FROM dbo.AutomatedTask at WITH (NOLOCK) 
	LEFT JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID 
	WHERE at.Enabled = 1
	AND at.NextRunDateTime < dbo.fn_GetDate_Local()
	AND at.ClientID not in (224)
	AND (@QueueID IS NULL OR ati.QueueID IS NULL OR ati.QueueID = @QueueID) 
	AND (@ClientID IS NULL OR ati.ClientID IS NULL OR ati.ClientID = @ClientID) 
	ORDER BY [Wait Status], at.NextRunDateTime 
	
	/* Show all details if required */
	IF @ShowAll = 1
	BEGIN
		SELECT ati.QueueID, at.NextRunDateTime, 
		CASE 
			WHEN at.AlreadyRunning = 1 THEN 'Already Running' 
			WHEN LockDateTime IS NOT NULL THEN 'Locked to a Scheduler' 
			ELSE 'Waiting' 
		END as [Status], ati.*, at.*
		FROM dbo.AutomatedTask at WITH (NOLOCK) 
		INNER JOIN [AquariusMaster].dbo.ClientDatabase cd WITH (NOLOCK) ON cd.ClientID = at.ClientID AND cd.ServerName = @ServerName AND cd.DatabaseName = @DatabaseName
		LEFT JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID 
		AND at.ClientID not in (224)
		WHERE at.Enabled = 1
		AND at.NextRunDateTime < dbo.fn_GetDate_Local() 
		ORDER BY at.NextRunDateTime
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[atw] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[atw] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[atw] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[atw] TO [sp_executehelper]
GO
