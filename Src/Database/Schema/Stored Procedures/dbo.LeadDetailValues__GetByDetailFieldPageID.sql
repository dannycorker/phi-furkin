SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[LeadDetailValues__GetByDetailFieldPageID]
(

	@PageID int, 
	@ClientID int
	
)

AS


				SELECT v.*
				FROM
					[dbo].[LeadDetailValues] v
					INNER JOIN dbo.DetailFields f ON v.DetailFieldID = f.DetailFieldID
				WHERE
					f.DetailFieldPageID = @PageID
					AND v.[ClientID] = @ClientID





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__GetByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues__GetByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__GetByDetailFieldPageID] TO [sp_executeall]
GO
