SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the GBAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GBAddress_Delete]
(

	@GBAddressID int   
)
AS


				DELETE FROM [dbo].[GBAddress] WITH (ROWLOCK) 
				WHERE
					[GBAddressID] = @GBAddressID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GBAddress_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Delete] TO [sp_executeall]
GO
