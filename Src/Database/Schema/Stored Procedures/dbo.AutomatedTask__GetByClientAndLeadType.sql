SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-03-29
-- Description:	Get Automated Tasks by (inferred) LeadTypeID
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__GetByClientAndLeadType] 
	@ClientID int, 
	@ShowAll bit = 1,
	@LeadTypeID int = null 
AS
BEGIN
	SET NOCOUNT ON;

	IF @ShowAll = 1
	BEGIN
		SELECT at.*,
		sq.QueryTitle
		FROM dbo.AutomatedTask at WITH (NOLOCK) 
		INNER JOIN dbo.AutomatedTaskParam atp_r WITH (NOLOCK) ON atp_r.TaskID = at.TaskID AND atp_r.ParamName = 'SQL_QUERY' 
		INNER JOIN dbo.SqlQuery sq WITH (NOLOCK) ON sq.QueryID = CAST(atp_r.ParamValue as int) 
		WHERE at.ClientID = @ClientID 
		ORDER BY at.Taskname, at.TaskID 
	END
	ELSE
	BEGIN
		SELECT at.*,
		sq.QueryTitle
		FROM dbo.AutomatedTask at WITH (NOLOCK) 
		INNER JOIN dbo.AutomatedTaskParam atp_r WITH (NOLOCK) ON atp_r.TaskID = at.TaskID AND atp_r.ParamName = 'SQL_QUERY' 
		INNER JOIN dbo.SqlQuery sq WITH (NOLOCK) ON sq.QueryID = CAST(atp_r.ParamValue as int) 
		LEFT JOIN dbo.AutomatedTaskParam atp_e WITH (NOLOCK) ON atp_e.TaskID = at.TaskID AND atp_e.ParamName = 'LEADEVENT_TO_ADD' 
		LEFT JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = CAST(atp_e.ParamValue as int) 
		WHERE at.ClientID = @ClientID 
		AND (
			/* Option 2: Only show batch jobs with this lead type */
			(@LeadTypeID > 0 AND (@LeadTypeID = sq.LeadTypeID OR @LeadTypeID = et.LeadTypeID))
			OR
			/* Option 3: Only show batch jobs that have no lead type */
			(@LeadTypeID IS NULL AND sq.LeadTypeID IS NULL AND et.LeadTypeID IS NULL)
		) 
		ORDER BY at.Taskname, at.TaskID 
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__GetByClientAndLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__GetByClientAndLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__GetByClientAndLeadType] TO [sp_executeall]
GO
