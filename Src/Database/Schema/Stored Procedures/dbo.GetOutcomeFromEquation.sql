SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetOutcomeFromEquation] @EquationID int

AS

SELECT     dbo.Outcomes.OutcomeID, dbo.Outcomes.ClientQuestionnaireID, dbo.Outcomes.OutcomeName, dbo.Outcomes.OutcomeDescription, 
                      dbo.Outcomes.SmsWhenFound, dbo.Outcomes.mobileNumber, dbo.Outcomes.EmailCustomer, dbo.Outcomes.CustomersEmail, dbo.Outcomes.CustomersEmailSubject,
                      dbo.Outcomes.ThankYouPage, dbo.Outcomes.EmailFromAddress, dbo.Outcomes.SmsToCustomer, dbo.OutcomeEquations.OutcomeEquationID, 
                      dbo.OutcomeEquations.OutcomeID AS Expr1, dbo.OutcomeEquations.EquationID, dbo.Outcomes.ClientID, dbo.Outcomes.EmailClient, dbo.Outcomes.ClientEmail
FROM         dbo.Outcomes INNER JOIN
                      dbo.OutcomeEquations ON dbo.Outcomes.OutcomeID = dbo.OutcomeEquations.OutcomeID
WHERE     (dbo.OutcomeEquations.EquationID = @EquationID)




GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeFromEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeFromEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeFromEquation] TO [sp_executeall]
GO
