SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Dave Morgan
-- Create date: 27/10/2016
-- Description:	Inserts a new product into a change set by copying the master template
-- Mods:
-- =============================================
CREATE PROCEDURE [dbo].[_C600_RulesEngine_AddAProduct]
(
	@LeadEventID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	-------------------------------------- configuration -------------------------------------------
	
	DECLARE @TemplateChangeSetID INT = 1
			
	DECLARE @MasterRuleSetList TABLE ( MasterRuleSetID INT, MasterRuleSetName VARCHAR(200), Done INT )
	
	INSERT INTO @MasterRuleSetList VALUES
	(110,'lk_Age (at Inception) Risk Level',0),
	(102,'lk_Age (at Term) Risk Level',0),
	(122,'lk_Breed enabled',0),
	(87,'lk_Breed Risk Level',0),
	(101,'lk_Campaign Risk Level',0),
	(137,'lk_Commission percent',0),
	(125,'lk_DiscountRate Loyalty',0),
	(124,'lk_DiscountRate Multipet',0),
	(104,'lk_DiscountRate One Month Free',0),
	(86,'lk_Geozone',0),
	(108,'lk_Loss Ratio Band',0),
	(93,'lk_Maxmin_Base Cap',0),
	(94,'lk_Maxmin_Base Collar',0),
	(130,'lk_Premium Band at Current Evaluation',0),
	(129,'lk_Premium Band at Last Period Exit',0),
	(128,'lk_Previous Claimer',0),
	(105,'lk_Price Testing',0),
	(131,'lk_Renewal Moderator Cap',0),
	(127,'lk_Renewal Moderator Collar',0),
	(103,'lk_SI Risk Level',0),
	(99,'lk_Specified Breeds',0),
	(118,'Master Breed List',0)
	
	-------------------------------------- end of configuration ------------------------------------
	
	DECLARE 
			@CaseID INT,
			@CLientID INT,
			@DeleteRuleSetID INT,
			@ErrorMessage VARCHAR(1000),
			@InsertIntoChangeSetID INT,
			@MasterRuleSetID INT, 
			@MasterRuleSetName VARCHAR(200),
			@MatterID INT,
			@NewChangeSetID INT,
			@NewName VARCHAR(100),
			@NewPrefix VARCHAR(20),
			@ParentChangeSetID INT,
			@WhoCreated INT 



	SELECT TOP (1) 
	@ClientID = l.ClientID, 
	@CaseID = le.CaseID, 
	@WhoCreated = le.WhoCreated,
	@MatterID = m.MatterID
	FROM dbo.LeadEvent le
	INNER JOIN dbo.Cases ca ON ca.CaseID = le.CaseID
	INNER JOIN dbo.Lead l ON l.LeadID = ca.LeadID
	INNER JOIN dbo.Matter m ON m.CaseID = ca.CaseID	
	INNER JOIN dbo.Customers cust WITH (NOLOCK) ON l.CustomerID = cust.CustomerID 
	WHERE le.LeadEventID = @LeadEventID 
	
	-- check that insert into is a valid and unique tag name RAISERROR if no
	SELECT @InsertIntoChangeSetID=dbo.fnGetDv (177301,@CaseID),	-- change set tag of change set copy to be inserted into
			@NewPrefix=dbo.fnGetDv (177302,@CaseID) + '_'	-- new product prefix

	-- copy the template change set
	EXEC dbo.RulesEngine_ChangeSets_Create @ClientID, @TemplateChangeSetID, @WhoCreated
	
	SELECT TOP 1 @NewChangeSetID=ChangeSetID 
	FROM dbo.RulesEngine_ChangeSets 
	ORDER BY ChangeSetID DESC

	-- delete the master rule sets
	WHILE EXISTS ( SELECT * FROM @MasterRuleSetList WHERE Done=0 )
	BEGIN
	
		SELECT @MasterRuleSetID=MasterRuleSetID, @DeleteRuleSetID=rs.RuleSetID
		FROM @MasterRuleSetList mrs 
		INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON mrs.MasterRuleSetName=rs.Name
		WHERE mrs.Done=0
		AND rs.ChangeSetID=@NewChangeSetID
		
		EXEC dbo.RulesEngine_RuleSets_Delete @DeleteRuleSetID, 1
		
		UPDATE @MasterRuleSetList SET Done=1 WHERE MasterRuleSetID=@MasterRuleSetID
	
	END
	
	-- rename the remaining rulesets and set the parent to NULL i.e. make this new template copy the master for the product it is rating
	UPDATE rs 
	SET Name=@NewPrefix + Name,
		ParentRuleSetID=NULL
	--select rs.RuleSetID,@NewPrefix + Name	
	FROM RulesEngine_RuleSets rs
	WHERE ChangeSetID=@NewChangeSetID
	
	-- rename the parameters of type ruleset
	UPDATE rp 
	SET Name=
	--SELECT rp.RuleParameterID,rp.name,
	REPLACE(rp.Name,'Rule Set: ','Rule Set: ' + @NewPrefix) 
	FROM RulesEngine_RuleParameters rp 
	INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON rp.RuleID=r.RuleID
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON r.RuleSetID=rs.RuleSetID
	WHERE rs.ChangeSetID=@NewChangeSetID
	AND rp.Name LIKE 'Rule Set:%'
	AND NOT EXISTS ( SELECT * FROM @MasterRuleSetList m WHERE rp.Name LIKE 'Rule Set: ' + m.MasterRuleSetName) 
	
	-- switch the ruleset id of the parameters using the master rulesets
	UPDATE rp 
	SET Value=
	--SELECT rp.RuleParameterID,rp.name,rp.Value,
	m.MasterRuleSetID 
	FROM RulesEngine_RuleParameters rp 
	INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON rp.RuleID=r.RuleID
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON r.RuleSetID=rs.RuleSetID
	INNER JOIN @MasterRuleSetList m ON rp.Name = 'Rule Set: ' + m.MasterRuleSetName
	WHERE rs.ChangeSetID=@NewChangeSetID
	AND rp.Name LIKE 'Rule Set:%'
	
	-- all done now insert the new product into the specified change set i.e. switch new change set ID to the insert into change set ID
	UPDATE rs
	SET ChangeSetID=@InsertIntoChangeSetID
	--select ChangeSetID,@InsertIntoChangeSetID
	FROM RulesEngine_RuleSets rs
	WHERE ChangeSetID=@NewChangeSetID
	
	-- and tidy up
	-- delete the new change set 
	EXEC dbo.RulesEngine_ChangeSets_Delete @NewChangeSetID, @ClientID, @WhoCreated
	
	-- clear temp fields
	EXEC dbo._C00_SimpleValueIntoField 177301, '', @MatterID, @WhoCreated  -- insert into change set id
	EXEC dbo._C00_SimpleValueIntoField 177302, '', @MatterID, @WhoCreated  -- new prefix
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_RulesEngine_AddAProduct] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_RulesEngine_AddAProduct] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_RulesEngine_AddAProduct] TO [sp_executeall]
GO
