SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Dashboard table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_GetByDashboardID]
(

	@DashboardID int   
)
AS


				SELECT
					[DashboardID],
					[DashboardName],
					[DashboardDescription],
					[OwnerID],
					[DashboardLayoutID],
					[ClientID]
				FROM
					[dbo].[Dashboard] WITH (NOLOCK) 
				WHERE
										[DashboardID] = @DashboardID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_GetByDashboardID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_GetByDashboardID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_GetByDashboardID] TO [sp_executeall]
GO
