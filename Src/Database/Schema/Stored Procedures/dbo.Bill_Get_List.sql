SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Bill table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill_Get_List]

AS


				
				SELECT
					[BillID],
					[ClientID],
					[Reference],
					[Description],
					[CreatedDate],
					[DiaryAppointmentID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[BillStatusID]
				FROM
					[dbo].[Bill] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Get_List] TO [sp_executeall]
GO
