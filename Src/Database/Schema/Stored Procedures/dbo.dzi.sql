SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-06
-- Description:	Helpful details about DocumentZipping
-- =============================================
CREATE PROCEDURE [dbo].[dzi] 
	@ClientID int = null,
	@DocumentZipInformationID int = null,
	@StatusID int = null,
	@Repair bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @DocumentZipInformationID > 0 AND @Repair = 1
	BEGIN
	
		/* Set all child records back to "Pending" */
		UPDATE dbo.DocumentZip  
		SET StatusID = 35 
		WHERE DocumentZipInformationID = @DocumentZipInformationID
		AND StatusID <> 35 

		/* Set header record back to "Pending" */
		UPDATE dbo.DocumentZipInformation 
		SET StatusID = 35 
		WHERE DocumentZipInformationID = @DocumentZipInformationID
		
	END
	
	ELSE
	
	BEGIN
	
		/* Show all values */
		SELECT 'All Files' as [All Fields], dzi.*  
		FROM dbo.DocumentZipInformation dzi (nolock) 
		WHERE (@ClientID IS NULL OR dzi.ClientID = @ClientID)
		AND (@DocumentZipInformationID IS NULL OR dzi.DocumentZipInformationID = @DocumentZipInformationID)
		AND (@StatusID IS NULL OR dzi.StatusID = @StatusID)
		ORDER BY dzi.DocumentZipInformationID DESC
		
	END
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[dzi] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dzi] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dzi] TO [sp_executeall]
GO
