SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2007-02-28
-- Description:	This is now a general housekeeping job
-- 2011-07-01 JWG Use HskLeadAndThreadDuplicateRemovals instead of __LeadAndThreadDuplicateRemoval now.
-- 2012-05-29 JWG AquariumIndexHousekeeping has been moved to its own job step, so that it can run even if this fails.
-- 2014-09-09 SB  Allowed LeadStatus to be client 0 for shared lead types
-- 2015-05-18 PR  Remove Script Locks(Line 458) and removed duplicate _SystemApplicationLog delete
-- 2017-02-02 DCM Client specific cleanup of XML logging
-- =============================================
CREATE PROCEDURE [dbo].[AquariumHousekeeping] AS 
BEGIN
	
	/* Give this script the highest priority there is, where the scale is -10 (lowest) to 10 (highest) */
	SET DEADLOCK_PRIORITY 10;
	
	SET NOCOUNT ON;
	
	DECLARE @LastRowCount INT, 
	@Debug BIT = 1, 
	@AdminUserID INT = 2795,
	@LogMessage VARCHAR(1024),
	@EscalationUserID INT = NULL 

	EXEC dbo._C00_LogIt 'Info', 'AquariumHousekeeping', 'Start', 'Start of housekeeping', @AdminUserID 

	DECLARE @EventsToFollowUp TABLE (
		LeadEventID INT,
		ClientID INT,
		LeadID INT,
		CaseID INT,
		WhoCreatedID INT,
		EscalationEventTypeID INT,
		EscEventFollowupTimeUnits INT, 
		EscEventFollowupTimeQuantity INT
	)

	-- OUTPUT...INTO only works with @TABLE, so even though we will only
	-- be getting one new ID at a time, we need another @table to hold this one value at a time
	DECLARE @NextEvent TABLE (
		NextLeadEventID INT
	)

	DECLARE @OneLeadEventIDAtATime INT, @Counter INT 

	INSERT INTO @EventsToFollowUp (LeadEventID, ClientID, LeadID, CaseID, WhoCreatedID, EscalationEventTypeID, EscEventFollowupTimeUnits, EscEventFollowupTimeQuantity)
	SELECT le.LeadEventID, le.ClientID, le.LeadID, le.CaseID, le.WhoCreated, ec.NextEventTypeID, esc.FollowupTimeUnitsID, esc.FollowupQuantity
	FROM LeadEvent le 
	INNER JOIN EventType et		ON le.EventTypeID = et.EventTypeID
	INNER JOIN Lead l			ON le.LeadID = l.LeadID 
	INNER JOIN EventChoice ec	ON ec.EventTypeID = et.EventTypeID
	INNER JOIN EventType esc	ON ec.NextEventTypeID = esc.EventTypeID
	WHERE l.AquariumStatusID = 2
	AND le.FollowupDateTime IS NOT NULL 
	AND le.WhenFollowedUp IS NULL 
	AND dbo.fn_GetDate_Local() > le.FollowupDateTime 
	AND ec.EscalationEvent = 1
	AND le.EventDeleted = 0
	AND NOT EXISTS (
		SELECT * FROM LeadEventThreadCompletion letc
		WHERE letc.CaseID = le.CaseID 
		AND letc.FromLeadEventID = le.LeadEventID
		AND letc.FromEventTypeID = le.EventTypeID
		AND letc.ThreadNumberRequired = ec.ThreadNumber
		AND letc.ToLeadEventID IS NOT NULL
	)

	SELECT @Counter = COUNT(*) FROM @EventsToFollowUp
	SELECT @LogMessage = CONVERT(VARCHAR, @Counter) + ' to followup up'
	EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Escalation: EventsToFollowUp', @LogMessage, @AdminUserID 

	WHILE @Counter > 0
	BEGIN

		SET ROWCOUNT 1

		SELECT @OneLeadEventIDAtATime = LeadEventID FROM @EventsToFollowUp

		SET ROWCOUNT 0

		BEGIN TRY
		
			INSERT INTO LeadEvent(ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, 
			FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, EventDeleted, WhoDeleted) 
			OUTPUT inserted.LeadEventID INTO @NextEvent(NextLeadEventID)
			SELECT e.ClientID, e.LeadID, dbo.fn_GetDate_Local(), e.WhoCreatedID, 0, 'Auto-escalated by AquariumNet Batch Scheduler', e.EscalationEventTypeID, 
			dbo.fnAddTimeUnitsToDate(dbo.fn_GetDate_Local(), e.EscEventFollowupTimeUnits, e.EscEventFollowupTimeQuantity), NULL, NULL, NULL, e.CaseID, 0, NULL  
			FROM @EventsToFollowUp e
			WHERE e.LeadEventID = @OneLeadEventIDAtATime

			UPDATE LeadEvent
			SET WhenFollowedUp = dbo.fn_GetDate_Local(), NextEventID = n.NextLeadEventID 
			FROM @NextEvent n
			WHERE LeadEvent.LeadEventID = @OneLeadEventIDAtATime
			
		END TRY
		BEGIN CATCH
		
			SELECT @LogMessage = 'Failed to followup up LeadEventID ' + CAST(@OneLeadEventIDAtATime AS VARCHAR) + ' with EventType ' + CAST(e.EscalationEventTypeID AS VARCHAR) 
			FROM @EventsToFollowUp e
			WHERE e.LeadEventID = @OneLeadEventIDAtATime
			
			EXEC dbo._C00_LogIt 'Error', 'AquariumHousekeeping', 'Escalation: EventsToFollowUp', @LogMessage, @AdminUserID 
			
		END CATCH
		
		DELETE @NextEvent
		
		DELETE FROM @EventsToFollowUp WHERE LeadEventID = @OneLeadEventIDAtATime
		
		SELECT @Counter = @Counter - 1

	END


	/* 
		Now set all events back to the right Lead where the CaseID
		is correct, but the LeadID has come from SESSION and the user
		had two windows open, changing the variable.
		JWG 2011-10-13 Commented this out as it has not found any for the past 31 days. It takes about 3 minutes to run against 25million LeadEvent records.
	*/
	/*
	UPDATE dbo.LeadEvent 
	SET LeadID = ca.LeadID 
	FROM dbo.LeadEvent le 
	INNER JOIN dbo.Cases ca ON ca.CaseID = le.CaseID
	WHERE ca.LeadID <> le.LeadID
	
	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'LeadEvent LeadID Fix', @LogMessage, @AdminUserID 
	END*/

	/* Put tasks in the right queues where they have been switched between workflow and regular */
	UPDATE dbo.AutomatedTaskInfo 
	SET QueueID = 3 
	FROM dbo.AutomatedTask AT WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID 
	WHERE at.WorkflowTask = 1 
	AND ati.QueueID < 3

	UPDATE dbo.AutomatedTaskInfo 
	SET QueueID = 1 
	FROM dbo.AutomatedTask AT WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID 
	WHERE at.WorkflowTask = 0 
	AND ati.QueueID = 3

	/* Put ordinary tasks in the right queue by size */
	UPDATE dbo.AutomatedTaskInfo 
	SET QueueID = 2 
	WHERE QueueID = 1 
	AND MaxRuntime > 45 
	AND AvgRuntime > 45 
	AND LastRuntime > 45
	
	/* No way back just yet. Need more stats - write times & rowcounts to AutomatedTaskResult each time in future? */
	/*UPDATE dbo.AutomatedTaskInfo 
	SET QueueID = 1 
	WHERE QueueID = 2 
	AND LastRuntime < 30*/
	
	/* Remove invalid StatusAfterEvent values */
	IF EXISTS(
		SELECT * FROM dbo.EventType et1 WITH (NOLOCK) 
		WHERE et1.StatusAfterEvent > 0 
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.LeadStatus ls WITH (NOLOCK) 
			WHERE ls.StatusID = et1.StatusAfterEvent 
		)
	)
	BEGIN
		UPDATE dbo.EventType
		SET StatusAfterEvent = NULL
		WHERE StatusAfterEvent > 0 
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.LeadStatus ls WITH (NOLOCK) 
			WHERE ls.StatusID = StatusAfterEvent 
		)
	END
	
	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'EventType StatusAfterEvent Fix', @LogMessage, @AdminUserID 
	END

	-- Now run other tasks that are independent of the transaction
	-- Now set all Cases to the right status, as some web services etc
	-- do not always manage to do this for some reason.
	/*update dbo.cases 
	set clientstatusid = et2.statusafterevent
	from dbo.cases c 
	inner join dbo.leadevent le2 on c.leadid = le2.leadid
	inner join dbo.eventtype et2 on le2.eventtypeid = et2.eventtypeid
	where le2.leadeventid = 
	(
		select max(le1.leadeventid) 
		from leadevent le1, eventtype et1 
		where le1.eventtypeid = et1.eventtypeid 
		and et1.statusafterevent > 0 
		and le1.caseid = c.caseid 
		and le1.eventdeleted = 0
	)
	and isnull(c.clientstatusid, '') <> et2.statusafterevent*/
	;
	WITH maxevents AS (
		SELECT le1.CaseID, le1.LeadEventID, et1.StatusAfterEvent, ROW_NUMBER() OVER(PARTITION BY le1.CaseID ORDER BY le1.WhenCreated DESC, le1.LeadEventID DESC) AS rn
		FROM LeadEvent le1 WITH (NOLOCK)
		INNER JOIN EventType et1 WITH (NOLOCK) ON le1.EventTypeID = et1.EventTypeID 
		INNER JOIN LeadStatus ls WITH (NOLOCK) ON ls.StatusID = et1.StatusAfterEvent AND ls.ClientID IN (0, et1.ClientID)
		WHERE le1.EventDeleted = 0 
		AND et1.statusafterevent > 0 
		AND le1.EventDeleted = 0
	)
	UPDATE dbo.Cases 
	SET ClientStatusID = maxevents.StatusAfterEvent 
	FROM dbo.cases ca WITH (NOLOCK) 
	INNER JOIN maxevents ON maxevents.CaseID = ca.CaseID AND maxevents.rn = 1
	WHERE (ca.clientstatusid IS NULL OR ca.clientstatusid <> maxevents.statusafterevent)
	
	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Case Status Fix', @LogMessage, @AdminUserID 
	END


	-- Remove old LeadViewHistory information to keep
	-- the dashboard page running sweetly
	-- ignore client 38 as there are very few leads
	DELETE dbo.LeadViewHistory 
	WHERE WhenViewed < DATEADD(dd, -14, dbo.fn_GetDate_Local())
	AND ClientID NOT IN (38)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'LVH Delete Old', @LogMessage, @AdminUserID 
	END

	DELETE dbo.LeadViewHistoryArchive
	WHERE WhenViewed < DATEADD(mm, -2, dbo.fn_GetDate_Local())
	AND ClientID NOT IN (38)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'LVH Delete Old', @LogMessage, @AdminUserID 
	END


	/* Remove old login history details */
	DELETE dbo.LoginHistory 
	WHERE LoginDateTime < DATEADD(dd, -31, dbo.fn_GetDate_Local()) 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'LH Delete Old', @LogMessage, @AdminUserID 
	END

	
	-- Also delete LeadViewHistory information daily
	-- for very-high-volume users.
	DELETE leadviewhistory 
	FROM  (
		SELECT clientpersonnelid, COUNT(*) AS lvh_count
		FROM leadviewhistory 
		GROUP BY clientpersonnelid 
		HAVING COUNT(*) > 50
	) AS innercp
	WHERE leadviewhistory.whenviewed < DATEADD(dd, -1, dbo.fn_GetDate_Local())
	AND leadviewhistory.clientpersonnelid = innercp.clientpersonnelid

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'LVH Delete Many', @LogMessage, @AdminUserID 
	END


	-- Set address2 to blank where it contains a single space.
	-- Still finds the odd one now and again.
	UPDATE dbo.Customers 
	SET address2 = '' 
	WHERE address2 LIKE ' '

	SELECT @LastRowCount = @@ROWCOUNT

	/* JWG 2011-07-15 No longer required. Triggers prevent these at source now.
	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Customers Address2 Fix', @LogMessage, @AdminUserID 
	END


	-- Remove duplicate MDV and LDV
	EXEC dbo._C00_RemoveDuplicateDetailValues @WhatToRemove = 'ALL'
	
	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = 'Done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Remove duplicate MDV and LDV', @LogMessage, @AdminUserID 
	END*/


	/* ACE 2012-05-10 - Commented this section of HSK out as it was removing 
		records where the dropdown had been set to Please Select after something was there.
		This is causing confusion where the last thing the field was set to is one thing and 
		it's current state to indicate another.

	-- Delete zero lookuplistitemids from DetailValueHistory
	DELETE FROM detailvaluehistory 
	WHERE detailvaluehistoryid IN (
		SELECT dvh.detailvaluehistoryid 
		FROM detailvaluehistory dvh 
		INNER JOIN detailfields df ON df.detailfieldid = dvh.detailfieldid 
		WHERE df.lookuplistid > 0 
		AND df.enabled = 1 
		AND df.maintainhistory = 1 
		AND dvh.fieldvalue = '0' 
	)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'DVH Lookuplist Fix', @LogMessage, @AdminUserID 
	END
	*/

	-- Delete duplicate dynamic group/user rights records
	-- These are probably coming from the EventType editing screen...
	DELETE g1
	FROM GroupRightsDynamic g1 
	INNER JOIN GroupRightsDynamic g2 
		ON g1.ClientPersonnelAdminGroupID = g2.ClientPersonnelAdminGroupID 
		AND g1.FunctionTypeID = g2.FunctionTypeID 
		AND g1.LeadTypeID = g2.LeadTypeID 
		AND g1.ObjectID = g2.ObjectID 
		AND g1.GroupRightsDynamicID > g2.GroupRightsDynamicID

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'GRD Fix', @LogMessage, @AdminUserID 
	END
	

	DELETE u1
	FROM UserRightsDynamic u1 
	INNER JOIN UserRightsDynamic u2 
		ON u1.ClientPersonnelID = u2.ClientPersonnelID 
		AND u1.FunctionTypeID = u2.FunctionTypeID 
		AND u1.LeadTypeID = u2.LeadTypeID 
		AND u1.ObjectID = u2.ObjectID 
		AND u1.UserRightsDynamicID > u2.UserRightsDynamicID

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'URD Fix', @LogMessage, @AdminUserID 
	END
	
	
	-- Update HasDescendants for groups/users where there were no descendants when the group was set up,
	-- but descendants have since been created (eg encrypted fields)
	UPDATE dbo.GroupFunctionControl 
	SET HasDescendants = 1
    FROM dbo.GroupFunctionControl gfc  
	INNER JOIN dbo.GroupRightsDynamic grd ON grd.ClientPersonnelAdminGroupID = gfc.ClientPersonnelAdminGroupID 
	WHERE gfc.HasDescendants = 0 
	AND grd.FunctionTypeID = gfc.FunctionTypeID 
	AND grd.LeadTypeID = gfc.LeadTypeID 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'GFC HasDescendants Fix', @LogMessage, @AdminUserID 
	END
	
	UPDATE dbo.UserFunctionControl 
	SET HasDescendants = 1
    FROM dbo.UserFunctionControl ufc  
	INNER JOIN dbo.UserRightsDynamic urd ON urd.ClientPersonnelID = ufc.ClientPersonnelID 
	WHERE ufc.HasDescendants = 0 
	AND urd.FunctionTypeID = ufc.FunctionTypeID 
	AND urd.LeadTypeID = ufc.LeadTypeID 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'UFC HasDescendants Fix', @LogMessage, @AdminUserID 
	END

	
	/* Tidy Automated Task Result records */
	DELETE dbo.AutomatedTaskResult 
	WHERE RunDate < dbo.fn_GetDate_Local() - 31 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'ATR Delete', @LogMessage, @AdminUserID 
	END

	/* Tidy _SystemApplicationLog Log records */
	DELETE dbo._SystemApplicationLog 
	WHERE TimeGenerated < dbo.fn_GetDate_Local() - 31 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', '_SystemApplicationLog Delete', @LogMessage, @AdminUserID 
	END
	
	/* Releasing all Script Locks */
	DELETE dbo.ScriptLock
	
	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'ScriptLock Delete', @LogMessage, @AdminUserID 
	END

	/* Tidy up old log entries */
	DELETE dbo.Logs 
	WHERE LogDateTime < dbo.fn_GetDate_Local() - 31 

	SELECT @LastRowCount = @@ROWCOUNT

	/*Remove Old Perflog entries*/
	DELETE dbo.Perflog 
	WHERE Start < dbo.fn_GetDate_Local() - 93 

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Logs Delete', @LogMessage, @AdminUserID 
	END


	/* Doc Zip/Info housekeeping */
	DELETE dbo.DocumentZipInformation 
	FROM dbo.DocumentZipInformation dzi 
	WHERE dzi.StatusID = 31 /* Success */
	AND EXISTS (
		SELECT * 
		FROM dbo.DocumentZip dz (NOLOCK) 
		INNER JOIN dbo.DocumentQueue dq (NOLOCK) ON dq.DocumentQueueID = dz.DocumentQueueID 
		WHERE dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		AND dz.StatusID = 31 /* Success */ 
		AND dq.WhenCreated < dbo.fn_GetDate_Local() - 31
	)
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.DocumentZip dz (NOLOCK) 
		WHERE dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		AND dz.StatusID <> 31 /* Success */ 
	)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'DocumentZipInformation Delete', @LogMessage, @AdminUserID 
	END


	-- Doc Queue housekeeping
	-- 1) When a Case got split while documents were queued up,
	--    blank out the second copy.
	UPDATE dbo.LeadEvent 
	SET DocumentQueueID = NULL 
	FROM dbo.LeadEvent le
	INNER JOIN dbo.DocumentQueue dq (NOLOCK) ON dq.DocumentQueueID = le.DocumentQueueID
	WHERE dq.WhenCreated < dbo.fn_GetDate_Local() - 30
	-- 2) Now remove any old queue records that are no longer required.
	DELETE dbo.DocumentQueue
	FROM dbo.DocumentQueue dq 
	WHERE dq.WhenCreated < dbo.fn_GetDate_Local() - 31
	AND NOT EXISTS (
		SELECT *
		FROM dbo.DocumentZip dz 
		WHERE dz.DocumentQueueID = dq.DocumentQueueID 
	)
	AND NOT EXISTS (
		SELECT *
		FROM dbo.LeadEvent le  
		WHERE le.DocumentQueueID = dq.DocumentQueueID 
	)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'DocumentQueue Delete', @LogMessage, @AdminUserID 
	END


	/* DataLoaderFile housekeeping */
	DELETE dbo.DataLoaderFile 
	WHERE [TargetFileLocation] IN ('Passed', 'Archive') 
	AND ([DataLoadedDateTime] IS NULL OR [DataLoadedDateTime] < dbo.fn_GetDate_Local() - 31) 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'DataLoaderFile Delete', @LogMessage, @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.CancelAllRightsForEditing', 'Calling now', @AdminUserID 
	END
	EXEC dbo.CancelAllRightsForEditing
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.CancelAllRightsForEditing', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.CancelAllSqlQueryForEditing', 'Calling now', @AdminUserID 
	END
	EXEC dbo.CancelAllSqlQueryForEditing
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.CancelAllSqlQueryForEditing', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.HskLeadAndThreadDuplicateRemovals', 'Calling now', @AdminUserID 
	END
	EXEC dbo.HskLeadAndThreadDuplicateRemovals
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.HskLeadAndThreadDuplicateRemovals', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.SetEventTypeInProcess', 'Calling now', @AdminUserID 
	END
	EXEC dbo.SetEventTypeInProcess
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.SetEventTypeInProcess', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Delete Pending Activations', 'Running now', @AdminUserID 
	END
	/* Delete any account activation links that have not been completed in time. */
	DELETE dbo.PendingAccount 

	SELECT @LastRowCount = @@ROWCOUNT
	
	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Delete Pending Activations', @LogMessage, @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo._C38_CreateMissingClientRelationshipsInTDV', 'Calling now', @AdminUserID 
	END
	EXEC dbo._C38_CreateMissingClientRelationshipsInTDV 
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo._C38_CreateMissingClientRelationshipsInTDV', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.[CreateMissingRights]', 'Calling now', @AdminUserID 
	END
	EXEC [dbo].[CreateMissingRights] 
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.[CreateMissingRights]', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.[DeleteDuplicateRights]', 'Calling now', @AdminUserID 
	END
	EXEC [dbo].[DeleteDuplicateRights] 
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.[DeleteDuplicateRights]', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.[DeleteWorkTables]', 'Calling now', @AdminUserID 
	END
	EXEC [dbo].[DeleteWorkTables] 
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.[DeleteWorkTables]', 'Complete', @AdminUserID 
	END

	-- Temp BF questionnaire 3rd party webservice/import problem. Set clientid correctly where 0.
	UPDATE c 
	SET aquariumstatusid = l.aquariumstatusid 
	FROM customers c 
	INNER JOIN lead l ON l.customerid = c.customerid 
	WHERE c.aquariumstatusid = 1 
	AND c.clientid <> 0

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'BF questionnaire 3rd party webservice/import problem Fix', @LogMessage, @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo._HskDeleteLeadType', 'Calling now', @AdminUserID 
	END
	EXEC [dbo].[HskDeleteLeadType]	
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo._HskDeleteLeadType', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo._C00_UpdateMobiles', 'Calling now', @AdminUserID 
	END
	EXEC [dbo]._C00_UpdateMobiles	
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo._C00_UpdateMobiles', 'Complete', @AdminUserID 
	END

	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.HskSetCasesWhoCreated', 'Calling now', @AdminUserID 
	END
	EXEC [dbo].HskSetCasesWhoCreated	
	IF @Debug = 1
	BEGIN
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'dbo.HskSetCasesWhoCreated', 'Complete', @AdminUserID 
	END


	/* Weekly Tasks */
	/* SUNDAY = 1 */
	IF DATEPART(dw, dbo.fn_GetDate_Local()) = 1 
	BEGIN TRY
		IF @Debug = 1
		BEGIN
			EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Weekly Recompiles', 'Starting', @AdminUserID 
		END
		EXEC dbo.RecompileAllSprocs 0,'AquariumHousekeeping';
		EXEC dbo.RecompileAllFunctions;
		IF @Debug = 1
		BEGIN
			EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'Weekly Recompiles', 'Complete', @AdminUserID 
		END
	END TRY
	BEGIN CATCH
		SELECT @LogMessage = ERROR_MESSAGE()
		EXEC dbo._C00_LogIt 'Error', 'AquariumHousekeeping', 'Weekly Recompiles', @LogMessage, @AdminUserID 
	END CATCH
		
	/* JWG 2011-03-25 Keep doing this until the app is up to date */
	UPDATE dbo.PortalUserCase 
	SET ClientID = pu.ClientID
	FROM dbo.PortalUserCase puc 
	INNER JOIN dbo.PortalUser pu ON pu.PortalUserID = puc.PortalUserID
	WHERE puc.ClientID IS NULL

	/* JWG 2011-12-20 Keep the XML log tidy */
	DELETE dbo.LogXML 
	WHERE LogDateTime < dbo.fn_GetDate_Local() - 31
	
	/* DCM 2017-02-02 Client specific XML logs cleanup*/
	EXEC _C00_XMLHousekeeping
	
	/* JWG 2012-02-03 SMS out must always come from HTML.  If the app sees RTF it tries to zip the DocumentBLOB output, which is NULL now, so crashes! */
	UPDATE dbo.DocumentType
	SET InputFormat = 'HTML'
	WHERE OutputFormat = 'SMS'
	AND InputFormat = 'RTF' 
	
	/* LB/JWG 2012-06-19 Clear out temporary start pages that will be invalid when WordTemp is cleared out */
	UPDATE ClientPersonnelOptions 
	SET OptionValue = '' 
	WHERE ClientPersonnelOptionTypeID = 7 
	AND OptionValue LIKE '%WordTemp%' 
	
	/* Fix LatestCaseEvents where broken */
	DECLARE @Exec NVARCHAR(MAX),
			@Row INT
	
	DECLARE @IDTable TABLE (ID INT IDENTITY(1,1), StoredProc NVARCHAR(MAX))

	;WITH cte as
	(
		SELECT c.CaseID,c.LeadID,MAX(le.LeadEventID) as LeadEventID,c.LatestLeadEventID
		FROM Cases c WITH (NOLOCK) 
		INNER JOIN LeadEvent le WITH (NOLOCK) on le.CaseID = c.CaseID
		WHERE le.EventDeleted = 0
		GROUP BY c.CaseID,c.LatestLeadEventID,c.LeadID
		HAVING MAX(le.LeadEventID) <> c.LatestLeadEventID
	)
	INSERT INTO @IDTable (StoredProc)
	Select 'EXEC SetLatestCaseEvents '+ CAST(CaseID AS VARCHAR)
	from cte

	WHILE EXISTS (SELECT * FROM @IDTable)
	BEGIN

		SELECT TOP 1 @Row = ID, @Exec = StoredProc 
		FROM @IDTable
		ORDER BY ID ASC

		EXEC(@EXEC) 

		DELETE FROM @IDTable 
		WHERE (ID = @Row)
		
	END
	
	EXEC dbo.RemoteAuthentication_Housekeeping 
	
	/* All done */
	EXEC dbo._C00_LogIt 'Info', 'AquariumHousekeeping', 'End', 'Housekeeping complete', @AdminUserID 

	/* One-off special code */
	/*IF dbo.fn_GetDate_Local() BETWEEN '2013-01-04 00:00:01' AND '2013-01-04 01:00:00'
	BEGIN
	
		EXEC dbo._C00_LogIt 'Info', 'AquariumHousekeeping', 'End', 'Housekeeping special code beginning', @AdminUserID 
		;
		

		EXEC dbo._C00_LogIt 'Info', 'AquariumHousekeeping', 'End', 'Housekeeping special code ending', @AdminUserID 
		;
		
	END*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumHousekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumHousekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumHousekeeping] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[AquariumHousekeeping] TO [sp_executehelper]
GO
