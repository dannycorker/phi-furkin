SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetUnSecuredSiteMapChildren    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetUnSecuredSiteMapChildren] @ParentID int

AS

SELECT     SiteMapID, Url, Title, Description, ParentID, Secured
FROM         dbo.SiteMap
WHERE     (ParentID = @ParentID) AND ( (Secured = 0) OR
                      (Secured IS NULL) )




GO
GRANT VIEW DEFINITION ON  [dbo].[GetUnSecuredSiteMapChildren] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUnSecuredSiteMapChildren] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUnSecuredSiteMapChildren] TO [sp_executeall]
GO
