SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeID int   = null ,

	@ClientID int   = null ,

	@EventTypeName varchar (50)  = null ,

	@EventTypeDescription varchar (250)  = null ,

	@Enabled bit   = null ,

	@UnitsOfEffort int   = null ,

	@FollowupTimeUnitsID int   = null ,

	@FollowupQuantity int   = null ,

	@AvailableManually bit   = null ,

	@StatusAfterEvent int   = null ,

	@AquariumEventAfterEvent int   = null ,

	@EventSubtypeID int   = null ,

	@DocumentTypeID int   = null ,

	@LeadTypeID int   = null ,

	@AllowCustomTimeUnits bit   = null ,

	@InProcess bit   = null ,

	@KeyEvent bit   = null ,

	@UseEventCosts bit   = null ,

	@UseEventUOEs bit   = null ,

	@UseEventDisbursements bit   = null ,

	@UseEventComments bit   = null ,

	@SignatureRequired bit   = null ,

	@SignatureOverride bit   = null ,

	@VisioX decimal (18, 8)  = null ,

	@VisioY decimal (18, 8)  = null ,

	@AquariumEventSubtypeID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@FollowupWorkingDaysOnly bit   = null ,

	@CalculateTableRows bit   = null ,

	@SourceID int   = null ,

	@SmsGatewayID int   = null ,

	@IsShared bit   = null ,

	@SocialFeedID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeID]
	, [ClientID]
	, [EventTypeName]
	, [EventTypeDescription]
	, [Enabled]
	, [UnitsOfEffort]
	, [FollowupTimeUnitsID]
	, [FollowupQuantity]
	, [AvailableManually]
	, [StatusAfterEvent]
	, [AquariumEventAfterEvent]
	, [EventSubtypeID]
	, [DocumentTypeID]
	, [LeadTypeID]
	, [AllowCustomTimeUnits]
	, [InProcess]
	, [KeyEvent]
	, [UseEventCosts]
	, [UseEventUOEs]
	, [UseEventDisbursements]
	, [UseEventComments]
	, [SignatureRequired]
	, [SignatureOverride]
	, [VisioX]
	, [VisioY]
	, [AquariumEventSubtypeID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [FollowupWorkingDaysOnly]
	, [CalculateTableRows]
	, [SourceID]
	, [SmsGatewayID]
	, [IsShared]
	, [SocialFeedID]
    FROM
	dbo.fnEventTypeShared(@ClientID)
    WHERE 
	 ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeName] = @EventTypeName OR @EventTypeName IS NULL)
	AND ([EventTypeDescription] = @EventTypeDescription OR @EventTypeDescription IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([UnitsOfEffort] = @UnitsOfEffort OR @UnitsOfEffort IS NULL)
	AND ([FollowupTimeUnitsID] = @FollowupTimeUnitsID OR @FollowupTimeUnitsID IS NULL)
	AND ([FollowupQuantity] = @FollowupQuantity OR @FollowupQuantity IS NULL)
	AND ([AvailableManually] = @AvailableManually OR @AvailableManually IS NULL)
	AND ([StatusAfterEvent] = @StatusAfterEvent OR @StatusAfterEvent IS NULL)
	AND ([AquariumEventAfterEvent] = @AquariumEventAfterEvent OR @AquariumEventAfterEvent IS NULL)
	AND ([EventSubtypeID] = @EventSubtypeID OR @EventSubtypeID IS NULL)
	AND ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([AllowCustomTimeUnits] = @AllowCustomTimeUnits OR @AllowCustomTimeUnits IS NULL)
	AND ([InProcess] = @InProcess OR @InProcess IS NULL)
	AND ([KeyEvent] = @KeyEvent OR @KeyEvent IS NULL)
	AND ([UseEventCosts] = @UseEventCosts OR @UseEventCosts IS NULL)
	AND ([UseEventUOEs] = @UseEventUOEs OR @UseEventUOEs IS NULL)
	AND ([UseEventDisbursements] = @UseEventDisbursements OR @UseEventDisbursements IS NULL)
	AND ([UseEventComments] = @UseEventComments OR @UseEventComments IS NULL)
	AND ([SignatureRequired] = @SignatureRequired OR @SignatureRequired IS NULL)
	AND ([SignatureOverride] = @SignatureOverride OR @SignatureOverride IS NULL)
	AND ([VisioX] = @VisioX OR @VisioX IS NULL)
	AND ([VisioY] = @VisioY OR @VisioY IS NULL)
	AND ([AquariumEventSubtypeID] = @AquariumEventSubtypeID OR @AquariumEventSubtypeID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly OR @FollowupWorkingDaysOnly IS NULL)
	AND ([CalculateTableRows] = @CalculateTableRows OR @CalculateTableRows IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([SmsGatewayID] = @SmsGatewayID OR @SmsGatewayID IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
	AND ([SocialFeedID] = @SocialFeedID OR @SocialFeedID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeID]
	, [ClientID]
	, [EventTypeName]
	, [EventTypeDescription]
	, [Enabled]
	, [UnitsOfEffort]
	, [FollowupTimeUnitsID]
	, [FollowupQuantity]
	, [AvailableManually]
	, [StatusAfterEvent]
	, [AquariumEventAfterEvent]
	, [EventSubtypeID]
	, [DocumentTypeID]
	, [LeadTypeID]
	, [AllowCustomTimeUnits]
	, [InProcess]
	, [KeyEvent]
	, [UseEventCosts]
	, [UseEventUOEs]
	, [UseEventDisbursements]
	, [UseEventComments]
	, [SignatureRequired]
	, [SignatureOverride]
	, [VisioX]
	, [VisioY]
	, [AquariumEventSubtypeID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [FollowupWorkingDaysOnly]
	, [CalculateTableRows]
	, [SourceID]
	, [SmsGatewayID]
	, [IsShared]
	, [SocialFeedID]
    FROM
	dbo.fnEventTypeShared(@ClientID) 
    WHERE 
	 ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeName] = @EventTypeName AND @EventTypeName is not null)
	OR ([EventTypeDescription] = @EventTypeDescription AND @EventTypeDescription is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([UnitsOfEffort] = @UnitsOfEffort AND @UnitsOfEffort is not null)
	OR ([FollowupTimeUnitsID] = @FollowupTimeUnitsID AND @FollowupTimeUnitsID is not null)
	OR ([FollowupQuantity] = @FollowupQuantity AND @FollowupQuantity is not null)
	OR ([AvailableManually] = @AvailableManually AND @AvailableManually is not null)
	OR ([StatusAfterEvent] = @StatusAfterEvent AND @StatusAfterEvent is not null)
	OR ([AquariumEventAfterEvent] = @AquariumEventAfterEvent AND @AquariumEventAfterEvent is not null)
	OR ([EventSubtypeID] = @EventSubtypeID AND @EventSubtypeID is not null)
	OR ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([AllowCustomTimeUnits] = @AllowCustomTimeUnits AND @AllowCustomTimeUnits is not null)
	OR ([InProcess] = @InProcess AND @InProcess is not null)
	OR ([KeyEvent] = @KeyEvent AND @KeyEvent is not null)
	OR ([UseEventCosts] = @UseEventCosts AND @UseEventCosts is not null)
	OR ([UseEventUOEs] = @UseEventUOEs AND @UseEventUOEs is not null)
	OR ([UseEventDisbursements] = @UseEventDisbursements AND @UseEventDisbursements is not null)
	OR ([UseEventComments] = @UseEventComments AND @UseEventComments is not null)
	OR ([SignatureRequired] = @SignatureRequired AND @SignatureRequired is not null)
	OR ([SignatureOverride] = @SignatureOverride AND @SignatureOverride is not null)
	OR ([VisioX] = @VisioX AND @VisioX is not null)
	OR ([VisioY] = @VisioY AND @VisioY is not null)
	OR ([AquariumEventSubtypeID] = @AquariumEventSubtypeID AND @AquariumEventSubtypeID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly AND @FollowupWorkingDaysOnly is not null)
	OR ([CalculateTableRows] = @CalculateTableRows AND @CalculateTableRows is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([SmsGatewayID] = @SmsGatewayID AND @SmsGatewayID is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	OR ([SocialFeedID] = @SocialFeedID AND @SocialFeedID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Find] TO [sp_executeall]
GO
