SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[ThirdPartyFieldID],
					[DetailFieldID],
					[ColumnFieldID],
					[ThirdPartyFieldGroupID],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_GetByClientID] TO [sp_executeall]
GO
