SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UserFunctionControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_Insert]
(

	@UserFunctionControlID int    OUTPUT,

	@ClientPersonnelID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants int   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[UserFunctionControl]
					(
					[ClientPersonnelID]
					,[ModuleID]
					,[FunctionTypeID]
					,[HasDescendants]
					,[RightID]
					,[LeadTypeID]
					)
				VALUES
					(
					@ClientPersonnelID
					,@ModuleID
					,@FunctionTypeID
					,@HasDescendants
					,@RightID
					,@LeadTypeID
					)
				-- Get the identity value
				SET @UserFunctionControlID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Insert] TO [sp_executeall]
GO
