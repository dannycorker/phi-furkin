SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the UserRightsDynamicEditing table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamicEditing_Find]
(

	@SearchUsingOR bit   = null ,

	@UserRightsDynamicEditingID int   = null ,

	@ClientPersonnelID int   = null ,

	@FunctionTypeID int   = null ,

	@LeadTypeID int   = null ,

	@ObjectID int   = null ,

	@RightID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [UserRightsDynamicEditingID]
	, [ClientPersonnelID]
	, [FunctionTypeID]
	, [LeadTypeID]
	, [ObjectID]
	, [RightID]
    FROM
	[dbo].[UserRightsDynamicEditing] WITH (NOLOCK) 
    WHERE 
	 ([UserRightsDynamicEditingID] = @UserRightsDynamicEditingID OR @UserRightsDynamicEditingID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([FunctionTypeID] = @FunctionTypeID OR @FunctionTypeID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ObjectID] = @ObjectID OR @ObjectID IS NULL)
	AND ([RightID] = @RightID OR @RightID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [UserRightsDynamicEditingID]
	, [ClientPersonnelID]
	, [FunctionTypeID]
	, [LeadTypeID]
	, [ObjectID]
	, [RightID]
    FROM
	[dbo].[UserRightsDynamicEditing] WITH (NOLOCK) 
    WHERE 
	 ([UserRightsDynamicEditingID] = @UserRightsDynamicEditingID AND @UserRightsDynamicEditingID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([FunctionTypeID] = @FunctionTypeID AND @FunctionTypeID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ObjectID] = @ObjectID AND @ObjectID is not null)
	OR ([RightID] = @RightID AND @RightID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamicEditing_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Find] TO [sp_executeall]
GO
