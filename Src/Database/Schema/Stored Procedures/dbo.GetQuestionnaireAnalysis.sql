SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetQuestionnaireAnalysis] @ClientID int 

AS

SELECT     dbo.ClientQuestionnaires.ClientID, dbo.ClientQuestionnaires.ClientQuestionnaireID, dbo.ClientQuestionnaires.QuestionnaireTitle, 
                      COUNT(dbo.CustomerQuestionnaires.CustomerID) AS LeadCount
FROM         dbo.CustomerQuestionnaires INNER JOIN
                      dbo.ClientQuestionnaires ON dbo.CustomerQuestionnaires.ClientQuestionnaireID = dbo.ClientQuestionnaires.ClientQuestionnaireID
GROUP BY dbo.ClientQuestionnaires.ClientID, dbo.ClientQuestionnaires.ClientQuestionnaireID, dbo.ClientQuestionnaires.QuestionnaireTitle
HAVING      (dbo.ClientQuestionnaires.ClientID = @ClientID)
ORDER BY ClientQuestionnaires.ClientQuestionnaireID DESC




GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireAnalysis] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionnaireAnalysis] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireAnalysis] TO [sp_executeall]
GO
