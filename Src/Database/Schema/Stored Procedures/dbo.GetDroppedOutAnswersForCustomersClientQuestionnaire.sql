SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetDroppedOutAnswersForCustomersClientQuestionnaire] @CustomerID int, @ClientQuestionnaireID int

as
SELECT     dbo.DroppedOutCustomerQuestionnaires.CustomerQuestionnaireID, dbo.DroppedOutCustomerQuestionnaires.ClientQuestionnaireID, 
                      dbo.DroppedOutCustomerQuestionnaires.CustomerID, dbo.DroppedOutCustomerQuestionnaires.SubmissionDate, dbo.DroppedOutCustomerAnswers.MasterQuestionID, 
                      dbo.DroppedOutCustomerAnswers.Answer, dbo.DroppedOutCustomerAnswers.QuestionPossibleAnswerID, dbo.DroppedOutCustomerAnswers.CustomerAnswerID, dbo.DroppedOutCustomerQuestionnaires.ClientID 
FROM         dbo.DroppedOutCustomerQuestionnaires INNER JOIN
                      dbo.DroppedOutCustomerAnswers ON dbo.DroppedOutCustomerQuestionnaires.CustomerQuestionnaireID = dbo.DroppedOutCustomerAnswers.CustomerQuestionnaireID
WHERE     (dbo.DroppedOutCustomerQuestionnaires.CustomerID = @CustomerID) AND (dbo.DroppedOutCustomerQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutAnswersForCustomersClientQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDroppedOutAnswersForCustomersClientQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutAnswersForCustomersClientQuestionnaire] TO [sp_executeall]
GO
