SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to rollback events
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LeadEventRollBack]
(
	@LeadEventID nvarchar(100)
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare @myERROR int,
			@FromLeadEventID int


	Select @FromLeadEventID = letc.FromLeadEventID
	From LeadEventThreadCompletion letc
	Where ToLeadEventID = @LeadEventID

	Update LeadEvent
	Set EventDeleted = 1, WhoDeleted = 502, DeletionComments = 'Deleted as per Ed Shropshire'
	Where LeadEventID = @LeadEventID

	Update LeadEvent
	Set FollowUpDateTime = NULL, NextEventID = NULL
	Where LeadEventID = @FromLeadEventID

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadEventRollBack] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LeadEventRollBack] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadEventRollBack] TO [sp_executeall]
GO
