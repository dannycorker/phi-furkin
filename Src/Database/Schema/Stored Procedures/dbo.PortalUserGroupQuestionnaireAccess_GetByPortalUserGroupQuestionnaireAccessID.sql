SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserGroupQuestionnaireAccess table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupQuestionnaireAccessID]
(

	@PortalUserGroupQuestionnaireAccessID int   
)
AS


				SELECT
					[PortalUserGroupQuestionnaireAccessID],
					[ClientID],
					[PortalUserGroupID],
					[ClientQuestionnaireID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupQuestionnaireAccess] WITH (NOLOCK) 
				WHERE
										[PortalUserGroupQuestionnaireAccessID] = @PortalUserGroupQuestionnaireAccessID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupQuestionnaireAccessID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupQuestionnaireAccessID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupQuestionnaireAccessID] TO [sp_executeall]
GO
