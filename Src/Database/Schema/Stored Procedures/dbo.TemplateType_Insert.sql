SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the TemplateType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TemplateType_Insert]
(

	@TemplateTypeID int    OUTPUT,

	@TemplateTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[TemplateType]
					(
					[TemplateTypeName]
					)
				VALUES
					(
					@TemplateTypeName
					)
				-- Get the identity value
				SET @TemplateTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TemplateType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Insert] TO [sp_executeall]
GO
