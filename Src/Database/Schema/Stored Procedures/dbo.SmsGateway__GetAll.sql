SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-05-2014
-- Description:	Gets a list of sms gateways
-- Modified BY PR - Ed requested that esendex should be the only gate way available 03-06-2014
-- =============================================
CREATE PROCEDURE [dbo].[SmsGateway__GetAll]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM SmsGateway WITH (NOLOCK) 

END



GO
GRANT VIEW DEFINITION ON  [dbo].[SmsGateway__GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SmsGateway__GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SmsGateway__GetAll] TO [sp_executeall]
GO
