SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-11-12
-- Description:	Gets the pet information from the collections side
-- Mods:
--	2016-11-02 DCM Updated for new billing system
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1493_LeadInfo_GetPetInfo] 
(
	@LeadID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @CustomerID INT,
			@ClientID INT
			
	SELECT	@CustomerID = CustomerID,
			@ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	DECLARE @PolicyLeadTypeID INT,
			@ClaimLeadTypeID INT
			
	SELECT @PolicyLeadTypeID = s.SharedTo
	FROM dbo.LeadTypeShare s WITH (NOLOCK) 
	WHERE s.SharedFrom = 1273
	AND s.ClientID = @ClientID
	
	SELECT @ClaimLeadTypeID = s.SharedTo
	FROM dbo.LeadTypeShare s WITH (NOLOCK) 
	WHERE s.SharedFrom = 1272
	AND s.ClientID = @ClientID	
	
	--SELECT @CustomerID AS CustomerID, lPol.LeadID AS PolicyLeadID, lClaim.LeadID AS ClaimLeadID, ldvName.DetailValue AS Name
	--FROM dbo.Lead lPol WITH (NOLOCK) 
	--INNER JOIN dbo.LeadTypeRelationship r WITH (NOLOCK) ON lPol.LeadID = r.FromLeadID
	--INNER JOIN dbo.Lead lClaim WITH (NOLOCK) ON r.ToLeadID = lClaim.LeadID
	--INNER JOIN dbo.LeadDetailValues ldvName WITH (NOLOCK) ON lPol.LeadID = ldvName.LeadID AND ldvName.DetailFieldID = 144268
	--WHERE lPol.CustomerID = @CustomerID
	--AND r.FromLeadTypeID = @PolicyLeadTypeID
	--AND r.ToLeadTypeID = @ClaimLeadTypeID
	--AND lClaim.CustomerID = @CustomerID
	
	SELECT DISTINCT @CustomerID AS CustomerID, pam.LeadID AS PolicyLeadID, ltr2.ToLeadID AS ClaimLeadID, ldvName.DetailValue + ' (BS Acc. No. ' + accid.DetailValue + ')' AS Name 
	FROM Lead lcol WITH (NOLOCK)
	INNER JOIN Matter mcol WITH (NOLOCK) ON mcol.LeadID=lcol.LeadID
	INNER JOIN MatterDetailValues accid WITH (NOLOCK) ON mcol.MatterID=accid.MatterID AND accid.DetailFieldID=176973
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON mcol.MatterID=ltr.ToMatterID
	INNER JOIN Matter pam WITH (NOLOCK) ON ltr.FromMatterID = pam.MatterID AND ltr.FromLeadTypeID=@PolicyLeadTypeID
	INNER JOIN LeadDetailValues ldvName WITH (NOLOCK) ON pam.LeadID = ldvName.LeadID AND ldvName.DetailFieldID = 144268
	INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON ltr.FromMatterID=ltr2.FromMatterID AND ltr2.ToLeadTypeID=@ClaimLeadTypeID
	WHERE lcol.LeadID=@LeadID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1493_LeadInfo_GetPetInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1493_LeadInfo_GetPetInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1493_LeadInfo_GetPetInfo] TO [sp_executeall]
GO
