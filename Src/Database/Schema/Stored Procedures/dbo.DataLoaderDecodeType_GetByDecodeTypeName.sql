SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderDecodeType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDecodeType_GetByDecodeTypeName]
(

	@DecodeTypeName varchar (250)  
)
AS


				SELECT
					[DataLoaderDecodeTypeID],
					[DecodeTypeName],
					[DecodeTypeDescription]
				FROM
					[dbo].[DataLoaderDecodeType] WITH (NOLOCK) 
				WHERE
										[DecodeTypeName] = @DecodeTypeName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_GetByDecodeTypeName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDecodeType_GetByDecodeTypeName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_GetByDecodeTypeName] TO [sp_executeall]
GO
