SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-06
-- Description:	Flag which EventTypes are in process and which are not
--              NB we could check for events that are only linked to an escalation event,
--              and then declare these to be out-of-process. But everyone seems happy for
--              them to remain in the "Jump To" list, which normal users can't see, rather
--              than the OOP list, which everyone can see.
-- JWG 2010-05-13 Why does the AND EXISTS update take 10mins+ to run?! Rewrite using CTEs instead.
-- JWG 2013-10-10 #23301 This is instant on test DBs but takes ages on live under load, so rewrite to select the 
--              events to update first (NOLOCK) and then just update those 2 on average. FYI the AND EXISTS above
--              was slow because of the OR within the clause AND (ec.EventTypeID = et.EventTypeID OR ec.NextEventTypeID = et.EventTypeID)
--              This works fine in a NOT EXISTS clause but MSS cannot handle it at all well within EXISTS.
-- =============================================
CREATE PROCEDURE [dbo].[SetEventTypeInProcess] 
	
	@LeadTypeID int = null

AS
BEGIN
	
	SET NOCOUNT ON;
	
	/*
		A tiny work table to hold the EventTypeID(s) to update and the new value for the InProcess flag
	*/
	DECLARE @ETU TABLE (
		EventTypeID INT, 
		SetInProcToThis BIT
	)
	
	
	/*
		Populate the work table with any InProcess events that now have no EventChoice connections
	*/
	INSERT INTO @ETU (EventTypeID, SetInProcToThis) 
	SELECT et.EventTypeID, 0 
	FROM dbo.EventType et WITH (NOLOCK) 
	WHERE (@LeadTypeID IS NULL OR et.LeadTypeID = @LeadTypeID)
	AND InProcess = 1 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.EventChoice ec WITH (NOLOCK) 
		WHERE (ec.EventTypeID = et.EventTypeID OR ec.NextEventTypeID = et.EventTypeID) 
	)
	
	
	/*
		Populate the work table with any OOP events that now do have at least one EventChoice connection
	*/
	INSERT INTO @ETU (EventTypeID, SetInProcToThis) 
	SELECT et.EventTypeID, 1 
	FROM dbo.EventType et WITH (NOLOCK) 
	WHERE (@LeadTypeID IS NULL OR et.LeadTypeID = @LeadTypeID)
	AND et.InProcess = 0 
	AND 
		(
		EXISTS (
			SELECT * 
			FROM dbo.EventChoice ec WITH (NOLOCK)
			WHERE (ec.EventTypeID = et.EventTypeID)
		)
		OR 
		EXISTS (
			SELECT * 
			FROM dbo.EventChoice ec WITH (NOLOCK)
			WHERE (ec.NextEventTypeID = et.EventTypeID)
		)
	)
	
	/*
		If there are any updates to do, issue the update statement from the work table
	*/
	IF EXISTS (SELECT * FROM @ETU)
	BEGIN

		UPDATE dbo.EventType 
		SET InProcess = etu.SetInProcToThis 
		FROM dbo.EventType et 
		INNER JOIN @ETU etu ON etu.EventTypeID = et.EventTypeID 

	END

	/* Old code commented out JWG 2013-10-10 #23301 */
	/*
	IF @LeadTypeID IS NULL
	BEGIN
		UPDATE dbo.EventType 
		SET InProcess = 0 
		WHERE (@LeadTypeID IS NULL OR LeadTypeID = @LeadTypeID)
		AND InProcess = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.EventChoice ec
			WHERE (ec.EventTypeID = EventType.EventTypeID OR ec.NextEventTypeID = EventType.EventTypeID)
		)
		
		;
		WITH ec1 AS (
			SELECT ec.EventTypeID, ROW_NUMBER() OVER(PARTITION BY ec.EventTypeID ORDER BY NextEventTypeID) as rn 
			FROM dbo.EventChoice ec WITH (NOLOCK) 
			WHERE (@LeadTypeID IS NULL OR ec.LeadTypeID = @LeadTypeID)
		)
		UPDATE dbo.EventType
		SET InProcess = 1 
		FROM dbo.EventType et WITH (NOLOCK) 
		INNER JOIN ec1 ON ec1.EventTypeID = et.EventTypeID AND rn = 1 
		WHERE et.InProcess = 0 

		;
		WITH ec2 AS (
			SELECT ec.NextEventTypeID, ROW_NUMBER() OVER(PARTITION BY ec.NextEventTypeID ORDER BY ec.EventTypeID) as rn 
			FROM dbo.EventChoice ec WITH (NOLOCK) 
			WHERE (@LeadTypeID IS NULL OR ec.LeadTypeID = @LeadTypeID)
		)
		UPDATE dbo.EventType
		SET InProcess = 1 
		FROM dbo.EventType et WITH (NOLOCK) 
		INNER JOIN ec2 ON ec2.NextEventTypeID = et.EventTypeID AND rn = 1 
		WHERE et.InProcess = 0 
		
	END
	
	ELSE
	BEGIN
	
		UPDATE dbo.EventType 
		SET InProcess = 0 
		WHERE (LeadTypeID = @LeadTypeID)
		AND InProcess = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.EventChoice ec
			WHERE (ec.LeadTypeID = @LeadTypeID)
			AND (ec.EventTypeID = EventType.EventTypeID OR ec.NextEventTypeID = EventType.EventTypeID)
		)
		
		;
		WITH ec1 AS (
			SELECT ec.EventTypeID, ROW_NUMBER() OVER(PARTITION BY ec.EventTypeID ORDER BY NextEventTypeID) as rn 
			FROM dbo.EventChoice ec WITH (NOLOCK) 
			WHERE (ec.LeadTypeID = @LeadTypeID)
		)
		UPDATE dbo.EventType
		SET InProcess = 1 
		FROM dbo.EventType et WITH (NOLOCK) 
		INNER JOIN ec1 ON ec1.EventTypeID = et.EventTypeID AND rn = 1 
		WHERE et.InProcess = 0 
		AND (et.LeadTypeID = @LeadTypeID)

		;
		WITH ec2 AS (
			SELECT ec.NextEventTypeID, ROW_NUMBER() OVER(PARTITION BY ec.NextEventTypeID ORDER BY ec.EventTypeID) as rn 
			FROM dbo.EventChoice ec WITH (NOLOCK) 
			WHERE (ec.LeadTypeID = @LeadTypeID)
		)
		UPDATE dbo.EventType
		SET InProcess = 1 
		FROM dbo.EventType et WITH (NOLOCK) 
		INNER JOIN ec2 ON ec2.NextEventTypeID = et.EventTypeID AND rn = 1 
		WHERE et.InProcess = 0 
		AND (et.LeadTypeID = @LeadTypeID)
		
	END
	*/
	
	/*UPDATE EventType 
	SET InProcess = 1 
	WHERE (@LeadTypeID IS NULL OR LeadTypeID = @LeadTypeID)
	AND InProcess = 0 
	AND EXISTS (
		SELECT * 
		FROM EventChoice ec
		WHERE (@LeadTypeID IS NULL OR ec.LeadTypeID = @LeadTypeID)
		AND (ec.EventTypeID = EventType.EventTypeID OR ec.NextEventTypeID = EventType.EventTypeID)
		/*AND (ec.EscalationEvent = 0)*/
	)
	*/
END



GO
GRANT VIEW DEFINITION ON  [dbo].[SetEventTypeInProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SetEventTypeInProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SetEventTypeInProcess] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[SetEventTypeInProcess] TO [sp_executehelper]
GO
