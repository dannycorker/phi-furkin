SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReminderTimeshift table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReminderTimeshift_Update]
(

	@ReminderTimeshiftID int   ,

	@ReminderTimeshiftName varchar (50)  ,

	@TimeUnitsID int   ,

	@TimeUnitsQuantity int   ,

	@DisplayOrder int   ,

	@TimeUnitsQuantityInMinutes int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReminderTimeshift]
				SET
					[ReminderTimeshiftName] = @ReminderTimeshiftName
					,[TimeUnitsID] = @TimeUnitsID
					,[TimeUnitsQuantity] = @TimeUnitsQuantity
					,[DisplayOrder] = @DisplayOrder
					,[TimeUnitsQuantityInMinutes] = @TimeUnitsQuantityInMinutes
				WHERE
[ReminderTimeshiftID] = @ReminderTimeshiftID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReminderTimeshift_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Update] TO [sp_executeall]
GO
