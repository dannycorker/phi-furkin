SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-02-01
-- Description:	Reconcile DDIC file
-- JEL 2019-04-18 changed dates for reconciliation to bring this in line with ARUDD processing, exact day it too restrictive
-- =============================================
CREATE PROCEDURE [dbo].[Billing__ReconcileDDIC]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TestID INT = 0,
			@DDICImportID INT = 0,
			@PaymentAmount NUMERIC(18,2),
			@BACSProcessedDate DATE,
			@FailureCode VARCHAR(200),
			@Reference VARCHAR(200),
			@AccountID INT,
			@PaymentID INT,
			@FileName VARCHAR(2000),
			@LogMessage VARCHAR(1024) = '',
			@RunAsUserID INT

	SELECT @RunAsUserID	= dbo.fn_C00_GetAutomationUser(@ClientID)

	BEGIN TRY
		BEGIN TRAN
	
		SELECT TOP 1 @DDICImportID = i.DDICImportID,
					@PaymentAmount = i.PaymentAmount,
					@BACSProcessedDate = i.BACSProcessedDate,
					@FailureCode = i.FailureCode,
					@Reference = i.Reference
		FROM BACSDDICImport i
		WHERE i.ImportStatusID = 1
		ORDER BY i.DDICImportID

		WHILE @DDICImportID > @TestID
		BEGIN

			SELECT @PaymentID = NULL, @AccountID = NULL

			SELECT @PaymentID = p.PaymentID, @AccountID = cps.AccountID
			FROM Account a WITH (NOLOCK) 
			INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.AccountID = a.AccountID 
			INNER JOIN Payment p ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
			WHERE a.ClientID = @ClientID
			AND a.Reference = @Reference /*Same ref*/
			AND p.PaymentGross = @PaymentAmount /*Same amount*/

			--AND p.PaymentDateTime = @BACSProcessedDate /*Same date, might have to relax this a little.*/
			/*JEL changed dates to bring this in line with ARUDD processing, exact day it too restrictive*/ 
			AND (@BACSProcessedDate BETWEEN DATEADD(DD,-1, cps.PaymentDate) AND DATEADD(DD,1, cps.ActualCollectionDate))
			AND cps.PaymentStatusID IN (6,5,2) /*Processed, retried, paid*/

			IF @PaymentID > 0
			BEGIN

				UPDATE i
				SET PaymentID = @PaymentID, AccountID = @AccountID
				FROM BACSDDICImport i
				WHERE i.DDICImportID = @DDICImportID

				/*Update the failed row with a reason and set the comments to DDIC*/
				UPDATE P
				SET FailureCode = @FailureCode, WhenModified = dbo.fn_GetDate_Local(), Comments = 'DDIC', WhoModified = @RunAsUserID
				FROM Payment p 
				WHERE p.PaymentID = @PaymentID

				/*Now we have set the minimum we can call the post payment procedure to do the required updates/reversals.*/
				EXEC dbo.Payment__PostPaymentProcedure @PaymentID, @ClientID

				/*Set the import table as processed*/
				UPDATE b
				SET ImportedDate = dbo.fn_GetDate_Local(), Comments = 'Processed', ImportStatusID = 2
				FROM BACSDDICImport b
				WHERE b.DDICImportID = @DDICImportID

			END
			ELSE
			BEGIN
			
				/*Set the import table to failed...*/
				UPDATE b
				SET ImportedDate = dbo.fn_GetDate_Local(), Comments = 'Failed to Process', ImportStatusID = 3
				FROM BACSDDICImport b
				WHERE b.DDICImportID = @DDICImportID

			END
			
			SELECT @TestID = @DDICImportID
			
			/*Grab the next row to process and continue...*/
			SELECT TOP 1 @DDICImportID = i.DDICImportID,
						@PaymentAmount = i.PaymentAmount,
						@BACSProcessedDate = i.BACSProcessedDate,
						@FailureCode = i.FailureCode,
						@Reference = i.Reference
			FROM BACSDDICImport i
			WHERE i.ImportStatusID = 1
			ORDER BY i.DDICImportID

		END

		COMMIT

	END TRY
	BEGIN CATCH
		
		ROLLBACK

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		
	END CATCH

	EXEC dbo._C00_LogIt 'Info', '_C321_ImportARUDD', 'Proc Complete', '', @RunAsUserID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__ReconcileDDIC] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__ReconcileDDIC] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__ReconcileDDIC] TO [sp_executeall]
GO
