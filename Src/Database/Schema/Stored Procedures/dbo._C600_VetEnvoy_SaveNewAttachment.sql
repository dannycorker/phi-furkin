SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-10-06
-- Description:	Saves a new attachment to the db
-- 2016-05-23 ACE Copied fron 224
-- =============================================
CREATE PROCEDURE [dbo].[_C600_VetEnvoy_SaveNewAttachment]
(
	@ID INT
)
AS
BEGIN
	SET NOCOUNT ON;

	
	DECLARE @AqAutomation INT = 44412,
			@EventType INT = 156767,
			@AquariumClientID INT = 600 
	
	DECLARE @Bytes VARBINARY(MAX),
			@ConversationID UNIQUEIDENTIFIER,
			@ContentType VARCHAR(2000),
			@ParentConversationID UNIQUEIDENTIFIER,
			@FileName VARCHAR(2000)
			
	SELECT @Bytes = Bytes, @ConversationID = ConversationID, @ContentType = ContentType
	FROM dbo.VetEnvoy_Attachments WITH (NOLOCK) 
	WHERE AttachmentID = @ID
	
	DECLARE @ContentTypes TABLE
	(
		Match VARCHAR(2000),
		FileName VARCHAR(2000)
	)
	INSERT @ContentTypes (Match, FileName)VALUES
	('%word%', 'Attachment.doc'),
	('%excel%', 'Attachment.xls'),
	('%powerpoint%', 'Attachment.ppt'),
	('%pdf%', 'Attachment.pdf'),
	('%jpeg%', 'Attachment.jpg'),
	('%gif%', 'Attachment.gif'),
	('%png%', 'Attachment.png'),
	('%tiff%', 'Attachment.tiff'),
	('%bmp%', 'Attachment.bmp')
	
	SELECT @FileName = FileName 
	FROM @ContentTypes
	WHERE @ContentType LIKE Match 
	
	SELECT @FileName = ISNULL(@FileName, 'Attachment.file')
	
	
	SELECT @ParentConversationID = ParentConversationID
	FROM dbo.VetEnvoy_Conversations WITH (NOLOCK) 
	WHERE ConversationID = @ConversationID
	
	DECLARE @MatterIDString VARCHAR(2000)
	SELECT @MatterIDString = MatterIDs
	FROM dbo.VetEnvoy_Claims WITH (NOLOCK) 
	WHERE ConversationID = @ParentConversationID
	
	DECLARE @MatterIDs TABLE 
	(
		ID INT IDENTITY, 
		MatterID INT,
		CaseID INT,
		LeadID INT
	)
	INSERT @MatterIDs (MatterID)
	SELECT AnyID
	FROM dbo.fnTableOfIDsFromCSV(@MatterIDString)
	
	UPDATE i
	SET CaseID = m.CaseID,
		LeadID = m.LeadID
	FROM @MatterIDs i
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON i.MatterID = m.MatterID
	
	DECLARE @Now DATETIME
	SELECT @Now = dbo.fn_GetDate_Local()
	
	DECLARE @Count INT,
			@Index INT = 0
			
	SELECT @Count = COUNT(*) 
	FROM @MatterIDs
	
	DECLARE @BlackHole TABLE (ID INT, LeadEventID INT)
	DECLARE @Event dbo.tvpLeadEvent
	
	WHILE @Index < @Count
	BEGIN
	
		SELECT @Index += 1
		
		DECLARE @CaseID INT = NULL,
				@LeadID INT = NULL,
				@LeadEventID INT = NULL
				
		SELECT	@CaseID = CaseID,
				@LeadID = LeadID
		FROM @MatterIDs
		WHERE ID = @Index 
				
		DECLARE @LeadDocumentID INT = NULL
		-- TODO - get attachment extension from content type
		EXEC @LeadDocumentID = dbo._C00_CreateLeadDocument @ClientID = @AquariumClientID, @LeadID = @LeadID, @DocumentBLOB = @Bytes, @LeadDocumentTitle = 'VetEnvoy Attachment', @WhoUploaded = @AqAutomation, @FileName = @FileName, @UploadDateTime = @Now   
		
		DELETE @Event
		DELETE @BlackHole
		
		INSERT @Event (ClientID, LeadID, CaseID, EventTypeID, LeadDocumentID, WhenCreated, WhoCreated, EventDeleted)
		VALUES (@AquariumClientID, @LeadID, @CaseID, @EventType, @LeadDocumentID, @Now, @AqAutomation, 0)
		  
		INSERT @BlackHole		
		EXEC dbo._C00_ApplyLeadEvent @Event
		
		SELECT @LeadEventID = LeadEventID
		FROM @BlackHole
		
		UPDATE dbo.VetEnvoy_Attachments
		SET LeadEventID = @LeadEventID
		WHERE AttachmentID = @ID
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetEnvoy_SaveNewAttachment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_VetEnvoy_SaveNewAttachment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetEnvoy_SaveNewAttachment] TO [sp_executeall]
GO
