SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[SqlQueryEditingTable__GetJoinTables]
	@QueryEditingID INT = -1
AS
BEGIN
	SET NOCOUNT ON;

	IF @QueryEditingID <> -1
	BEGIN
		WITH SqlTables AS
		(
			SELECT	SqlQueryTableName
			FROM	SqlQueryEditingTable WITH (NOLOCK) 
			WHERE	SqlQueryEditingID = @QueryEditingID
		)
		SELECT rtr.TableTo AS JoinTable
			FROM ReportTables rt 
			INNER JOIN ReportTableRelationships rtr ON rtr.TableFrom = rt.ReportTableName
			INNER JOIN SqlTables ON rt.ReportTableName = SqlQueryTableName
		UNION
		SELECT rtr.TableFrom AS JoinTable
			FROM ReportTables rt 
			INNER JOIN ReportTableRelationships rtr ON rtr.TableTo = rt.ReportTableName
			INNER JOIN SqlTables ON rt.ReportTableName = SqlQueryTableName
		ORDER BY JoinTable
	END
	ELSE
	BEGIN
		SELECT rtr.TableTo AS JoinTable
			FROM ReportTables rt 
			INNER JOIN ReportTableRelationships rtr ON rtr.TableFrom = rt.ReportTableName
		UNION
		SELECT rtr.TableFrom AS JoinTable
			FROM ReportTables rt 
			INNER JOIN ReportTableRelationships rtr ON rtr.TableTo = rt.ReportTableName
		ORDER BY JoinTable
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetJoinTables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable__GetJoinTables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetJoinTables] TO [sp_executeall]
GO
