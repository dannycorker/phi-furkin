SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 27/03/2013
-- Description:	Gets a list of SMSOutgoingPhoneNumber including the 
-- LeadTypeName
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumberList__GetBySMSQuestionnaireID]
	
	@SMSQuestionnaireID INT

AS
BEGIN

	SELECT lt.LeadTypeID, lt.LeadTypeName, smsOP.* FROM SMSOutgoingPhoneNumber smsOP WITH (NOLOCK) 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = smsOP.LeadTypeID
	WHERE smsOP.SMSQuestionnaireID=@SMSQuestionnaireID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumberList__GetBySMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumberList__GetBySMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumberList__GetBySMSQuestionnaireID] TO [sp_executeall]
GO
