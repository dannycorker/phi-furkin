SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-11-15
-- Description:	Wraps fnGetSimpleDv for calling from custom controls
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetSimpleDv]
(
	@DetailFieldID INT,
	@ObjectID INT,
	@ClientPersonnelID INT
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @DetailValue VARCHAR(2000)

	SELECT @DetailValue = dbo.fnGetSimpleDv(@DetailFieldID, @ObjectID)
	
	SELECT @DetailValue AS DetailValue

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetSimpleDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetSimpleDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetSimpleDv] TO [sp_executeall]
GO
