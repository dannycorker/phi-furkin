SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlFunction table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunction_GetBySqlFunctionID]
(

	@SqlFunctionID int   
)
AS


				SELECT
					[SqlFunctionID],
					[SqlFunctionName],
					[FunctionAlias],
					[FunctionDescription],
					[RestrictedToDataTypeID],
					[WhenCreated],
					[WhenModified],
					[IsPreferred],
					[Example],
					[ExampleResult],
					[ResultDataTypeID],
					[ExpressionMask],
					[IsAggregate]
				FROM
					[dbo].[SqlFunction] WITH (NOLOCK) 
				WHERE
										[SqlFunctionID] = @SqlFunctionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_GetBySqlFunctionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunction_GetBySqlFunctionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_GetBySqlFunctionID] TO [sp_executeall]
GO
