SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Check if a DetailValue is unique for a given MatterDetailField
-- UPDATED SB - Now includes subclient ID
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDetailValues__CheckUnique]
(
	@DetailFieldID int,
	@DetailValue varchar(2000),
	@SubClientID INT = null
)
AS

BEGIN

				SET NOCOUNT ON
				IF NOT EXISTS (
					SELECT ldv.[LeadDetailValueID]
						FROM
							[dbo].[LeadDetailValues] ldv WITH (NOLOCK)
							INNER JOIN dbo.Lead l WITH (NOLOCK) ON ldv.LeadID = l.LeadID
							INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID  
						WHERE
							ldv.[DetailFieldID] = @DetailFieldID
							AND ldv.[DetailValue] = @DetailValue
							AND c.SubClientID = @SubClientID
							AND (c.Test IS NULL OR c.Test = 0)
					)
					SELECT 1
				ELSE
					SELECT 0
				END





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__CheckUnique] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues__CheckUnique] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__CheckUnique] TO [sp_executeall]
GO
