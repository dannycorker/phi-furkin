SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EquationTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_GetByEquationTargetID]
(

	@EquationTargetID int   
)
AS


				SELECT
					[EquationTargetID],
					[ClientID],
					[EquationDetailFieldID],
					[DetailFieldID],
					[Target],
					[ObjectName],
					[PropertyName],
					[SqlFunctionID],
					[IsEquation],
					[IsColumnSum],
					[ColumnFieldID],
					[DetailFieldSubTypeID]
				FROM
					[dbo].[EquationTarget] WITH (NOLOCK) 
				WHERE
										[EquationTargetID] = @EquationTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_GetByEquationTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_GetByEquationTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_GetByEquationTargetID] TO [sp_executeall]
GO
