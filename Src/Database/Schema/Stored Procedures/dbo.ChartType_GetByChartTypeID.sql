SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartType_GetByChartTypeID]
(

	@ChartTypeID int   
)
AS


				SELECT
					[ChartTypeID],
					[ChartTypeName]
				FROM
					[dbo].[ChartType] WITH (NOLOCK) 
				WHERE
										[ChartTypeID] = @ChartTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_GetByChartTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartType_GetByChartTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_GetByChartTypeID] TO [sp_executeall]
GO
