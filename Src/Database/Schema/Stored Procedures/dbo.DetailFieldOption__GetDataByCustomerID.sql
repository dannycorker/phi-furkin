SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-12-24
-- Description:	DetailFieldOption__GetDataByCustomerID
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldOption__GetDataByCustomerID]
@ClientID int,
@CustomerID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @LeadIDs TABLE (LeadID int)
	DECLARE @CaseIDs TABLE (CaseID int)
	DECLARE @MatterIDs TABLE (MatterID int)
	
	DECLARE @LeadID int, 
			@CaseID int,
			@MatterID int,
			@TestID int = 0

	SELECT dfo.DetailFieldID, df.FieldCaption, cdv.DetailValue, dfo.FieldOrder, dfo.Mandatory, cdv.CustomerID, df.LeadOrMatter
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 10
	LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = df.DetailFieldID and cdv.CustomerID = @CustomerID
	WHERE dfo.ClientID = @ClientID
	ORDER BY dfo.FieldOrder

	INSERT INTO @LeadIDs (LeadID)
	SELECT LeadID 
	FROM Lead l WITH (NOLOCK)
	WHERE l.CustomerID = @CustomerID

	SELECT TOP 1 @LeadID = l.LeadID
	FROM @LeadIDs l
	ORDER BY l.LeadID

	WHILE @LeadID > @TestID
	BEGIN
	
		SELECT dfo.DetailFieldID, df.FieldCaption, ldv.DetailValue, dfo.FieldOrder, dfo.Mandatory, l.LeadID, l.CustomerID, df.LeadOrMatter
		FROM DetailFieldOption dfo WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 1
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = @LeadID AND l.LeadTypeID = df.LeadTypeID
		LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = df.DetailFieldID and ldv.LeadID = l.LeadID
		WHERE dfo.ClientID = @ClientID
		ORDER BY dfo.FieldOrder

		SELECT @TestID = @LeadID

		SELECT TOP 1 @LeadID = l.LeadID
		FROM @LeadIDs l
		WHERE l.LeadID > @LeadID
		ORDER BY l.LeadID

	END

	INSERT INTO @CaseIDs (CaseID)
	SELECT CaseID 
	FROM Cases c WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID
	WHERE l.CustomerID = @CustomerID
	
	SELECT TOP 1 @CaseID = c.CaseID, @TestID = 0
	FROM @CaseIDs c
	ORDER BY c.CaseID
	
	WHILE @CaseID > @TestID
	BEGIN
	
		SELECT dfo.DetailFieldID, df.FieldCaption, cdv.DetailValue, dfo.FieldOrder, dfo.Mandatory, ca.CaseID, ca.LeadID, df.LeadOrMatter
		FROM DetailFieldOption dfo WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 11
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = @CaseID
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = ca.LeadID  and l.LeadTypeID = df.LeadTypeID
		LEFT JOIN CaseDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = df.DetailFieldID and cdv.CaseID = ca.CaseID
		WHERE dfo.ClientID = @ClientID
		ORDER BY dfo.FieldOrder

		SELECT @TestID = @CaseID

		SELECT TOP 1 @CaseID = c.CaseID
		FROM @CaseIDs c
		WHERE c.CaseID > @CaseID
		ORDER BY c.CaseID

	END

	INSERT INTO @MatterIDs (MatterID)
	SELECT MatterID 
	FROM Matter m WITH (NOLOCK)
	WHERE m.CustomerID = @CustomerID

	SELECT TOP 1 @MatterID = m.MatterID, @TestID = 0
	FROM @MatterIDs m 
	ORDER BY m.MatterID

	WHILE @MatterID > @TestID
	BEGIN

		SELECT dfo.DetailFieldID, df.FieldCaption, mdv.DetailValue, dfo.FieldOrder, dfo.Mandatory, m.LeadID, m.MatterID, m.CaseID, df.LeadOrMatter
		FROM DetailFieldOption dfo WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 2
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = @MatterID
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID  and l.LeadTypeID = df.LeadTypeID
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = df.DetailFieldID and mdv.MatterID = m.MatterID
		WHERE dfo.ClientID = @ClientID
		ORDER BY dfo.FieldOrder

		SELECT @TestID = @MatterID

		SELECT TOP 1 @MatterID = m.MatterID 
		FROM @MatterIDs m 
		WHERE m.MatterID > @MatterID
		ORDER BY m.MatterID
	
	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldOption__GetDataByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByCustomerID] TO [sp_executeall]
GO
