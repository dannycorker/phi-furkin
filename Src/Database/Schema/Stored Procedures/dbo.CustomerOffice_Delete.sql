SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomerOffice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_Delete]
(

	@OfficeID int   
)
AS


				DELETE FROM [dbo].[CustomerOffice] WITH (ROWLOCK) 
				WHERE
					[OfficeID] = @OfficeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Delete] TO [sp_executeall]
GO
