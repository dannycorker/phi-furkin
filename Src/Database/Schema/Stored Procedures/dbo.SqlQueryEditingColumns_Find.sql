SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditingColumns table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingColumnID int   = null ,

	@ClientID int   = null ,

	@SqlQueryEditingID int   = null ,

	@SqlQueryEditingTableID int   = null ,

	@CompleteOutputText varchar (500)  = null ,

	@ColumnAlias varchar (50)  = null ,

	@ShowColumn bit   = null ,

	@DisplayOrder int   = null ,

	@SortOrder int   = null ,

	@SortType varchar (10)  = null ,

	@IsAggregate bit   = null ,

	@ColumnDataType varchar (50)  = null ,

	@ManipulatedDataType varchar (50)  = null ,

	@ColumnNaturalName varchar (50)  = null ,

	@RealSqlQueryColumnID int   = null ,

	@ComplexValue nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingColumnID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryEditingTableID]
	, [CompleteOutputText]
	, [ColumnAlias]
	, [ShowColumn]
	, [DisplayOrder]
	, [SortOrder]
	, [SortType]
	, [IsAggregate]
	, [ColumnDataType]
	, [ManipulatedDataType]
	, [ColumnNaturalName]
	, [RealSqlQueryColumnID]
	, [ComplexValue]
    FROM
	[dbo].[SqlQueryEditingColumns] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingColumnID] = @SqlQueryEditingColumnID OR @SqlQueryEditingColumnID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([SqlQueryEditingTableID] = @SqlQueryEditingTableID OR @SqlQueryEditingTableID IS NULL)
	AND ([CompleteOutputText] = @CompleteOutputText OR @CompleteOutputText IS NULL)
	AND ([ColumnAlias] = @ColumnAlias OR @ColumnAlias IS NULL)
	AND ([ShowColumn] = @ShowColumn OR @ShowColumn IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([SortOrder] = @SortOrder OR @SortOrder IS NULL)
	AND ([SortType] = @SortType OR @SortType IS NULL)
	AND ([IsAggregate] = @IsAggregate OR @IsAggregate IS NULL)
	AND ([ColumnDataType] = @ColumnDataType OR @ColumnDataType IS NULL)
	AND ([ManipulatedDataType] = @ManipulatedDataType OR @ManipulatedDataType IS NULL)
	AND ([ColumnNaturalName] = @ColumnNaturalName OR @ColumnNaturalName IS NULL)
	AND ([RealSqlQueryColumnID] = @RealSqlQueryColumnID OR @RealSqlQueryColumnID IS NULL)
	AND ([ComplexValue] = @ComplexValue OR @ComplexValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingColumnID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryEditingTableID]
	, [CompleteOutputText]
	, [ColumnAlias]
	, [ShowColumn]
	, [DisplayOrder]
	, [SortOrder]
	, [SortType]
	, [IsAggregate]
	, [ColumnDataType]
	, [ManipulatedDataType]
	, [ColumnNaturalName]
	, [RealSqlQueryColumnID]
	, [ComplexValue]
    FROM
	[dbo].[SqlQueryEditingColumns] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingColumnID] = @SqlQueryEditingColumnID AND @SqlQueryEditingColumnID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([SqlQueryEditingTableID] = @SqlQueryEditingTableID AND @SqlQueryEditingTableID is not null)
	OR ([CompleteOutputText] = @CompleteOutputText AND @CompleteOutputText is not null)
	OR ([ColumnAlias] = @ColumnAlias AND @ColumnAlias is not null)
	OR ([ShowColumn] = @ShowColumn AND @ShowColumn is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([SortOrder] = @SortOrder AND @SortOrder is not null)
	OR ([SortType] = @SortType AND @SortType is not null)
	OR ([IsAggregate] = @IsAggregate AND @IsAggregate is not null)
	OR ([ColumnDataType] = @ColumnDataType AND @ColumnDataType is not null)
	OR ([ManipulatedDataType] = @ManipulatedDataType AND @ManipulatedDataType is not null)
	OR ([ColumnNaturalName] = @ColumnNaturalName AND @ColumnNaturalName is not null)
	OR ([RealSqlQueryColumnID] = @RealSqlQueryColumnID AND @RealSqlQueryColumnID is not null)
	OR ([ComplexValue] = @ComplexValue AND @ComplexValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Find] TO [sp_executeall]
GO
