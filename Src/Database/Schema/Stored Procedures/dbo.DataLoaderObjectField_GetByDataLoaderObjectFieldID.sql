SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderObjectField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_GetByDataLoaderObjectFieldID]
(

	@DataLoaderObjectFieldID int   
)
AS


				SELECT
					[DataLoaderObjectFieldID],
					[DataLoaderObjectTypeID],
					[FieldName],
					[DataTypeID],
					[IsMandatory],
					[IsNormalisedField],
					[IsABitField],
					[IsClientOwned],
					[IsUniqueTableID],
					[IsWriteOnce],
					[HelperText]
				FROM
					[dbo].[DataLoaderObjectField] WITH (NOLOCK) 
				WHERE
										[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_GetByDataLoaderObjectFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_GetByDataLoaderObjectFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_GetByDataLoaderObjectFieldID] TO [sp_executeall]
GO
