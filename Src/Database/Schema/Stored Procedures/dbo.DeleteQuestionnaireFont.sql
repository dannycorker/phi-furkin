SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteQuestionnaireFont    Script Date: 08/09/2006 12:22:37 ******/
CREATE PROCEDURE [dbo].[DeleteQuestionnaireFont] @QuestionnaireFontID int

AS

Delete From QuestionnaireFonts
Where QuestionnaireFontID = @QuestionnaireFontID




GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteQuestionnaireFont] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteQuestionnaireFont] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteQuestionnaireFont] TO [sp_executeall]
GO
