SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-11-25
-- Description:	Find stuff in the schema
-- =============================================
CREATE PROCEDURE [dbo].[Find]
	@StringToFind VARCHAR(2000)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT OBJECT_NAME(object_id), m.* 
	FROM sys.all_sql_modules m WITH (NOLOCK) 
	WHERE m.definition LIKE '%' + @StringToFind + '%'
	ORDER BY 1

	SELECT *
	FROM SqlQuery s WITH (NOLOCK)
	WHERE s.QueryText LIKE '%' + @StringToFind + '%'
	ORDER BY s.QueryID DESC

END
GO
