SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ======================================================================
-- Author:		Alex Elger
-- Create date: 2013-03-05
-- Description:	Calls SQL After save for various levels in bulk.
-- ======================================================================
CREATE PROCEDURE [dbo].[SqlAfterSave_Bulk]
(
	@DetailValues tvpDetailValueData READONLY,
	@ClientID INT,
	@ClientPersonnelID int
)

AS
BEGIN
		
	-- Used to link the ids passed in to the new keys generated from the insert
	DECLARE @DetailValueIDs TABLE
	(
		PassedID INT,
		DetailValueID INT
	)	
	
	DECLARE @NumberOfDetailFieldNotBelongingToClient INT,
			@CustomerID int,
			@LeadID int,
			@CaseID int,
			@MatterID int,
			@ClientPersonnelIDForDV int,
			@ContactIDForDV int,
			@TestID int = 0
	
	SELECT
		@NumberOfDetailFieldNotBelongingToClient = COUNT(*)
	FROM
		@DetailValues DV
		INNER JOIN DetailFields DF
		ON DV.DetailFieldID = DF.DetailFieldID
		AND DF.ClientID != @ClientID
	
	IF (@NumberOfDetailFieldNotBelongingToClient >= 1)
	BEGIN
		-- Return back the expected structure, but with no rows.
		SELECT PassedID, DetailValueID
		FROM @DetailValueIDs
		WHERE 1=0
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 1)
	BEGIN
		
		SELECT TOP 1 @LeadID = t.ObjectID 
		FROM @DetailValues t
		WHERE t.DetailFieldSubType = 1 
		ORDER BY t.ObjectID
		
		WHILE @LeadID > @TestID 
		BEGIN

			SELECT @CustomerID = l.CustomerID
			FROM Lead l WITH (NOLOCK)
			WHERE l.LeadID = @LeadID

			/*Execute SAS for Lead*/
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID, @CustomerID, @LeadID

			SELECT @TestID = @LeadID
		
			SELECT TOP 1 @LeadID = t.ObjectID 
			FROM @DetailValues t
			WHERE t.DetailFieldSubType = 1 
			AND t.ObjectID > @LeadID
			ORDER BY t.ObjectID
		
		END
		
		SELECT @TestID = 0

	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 2)
	BEGIN
		
		SELECT TOP 1 @MatterID = t.ObjectID 
		FROM @DetailValues t
		WHERE t.DetailFieldSubType = 2
		ORDER BY t.ObjectID
		
		WHILE @MatterID > @TestID 
		BEGIN

			SELECT @CustomerID = m.CustomerID, @LeadID = m.LeadID, @CaseID = m.CaseID
			FROM Matter m WITH (NOLOCK)
			WHERE m.MatterID = @MatterID

			/*Execute SAS for Lead*/
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID, @CustomerID, @LeadID, @CaseID, @MatterID

			SELECT @TestID = @MatterID
		
			SELECT TOP 1 @MatterID = t.ObjectID 
			FROM @DetailValues t
			WHERE t.DetailFieldSubType = 2 
			AND t.ObjectID > @MatterID
			ORDER BY t.ObjectID
		
		END
		
		SELECT @TestID = 0
		
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 10)
	BEGIN
		
		SELECT TOP 1 @CustomerID = t.ObjectID 
		FROM @DetailValues t
		WHERE t.DetailFieldSubType = 10
		ORDER BY t.ObjectID
		
		WHILE @CustomerID > @TestID 
		BEGIN

			/*Execute SAS for Lead*/
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID, @CustomerID

			SELECT @TestID = @CustomerID
		
			SELECT TOP 1 @CustomerID = t.ObjectID 
			FROM @DetailValues t
			WHERE t.DetailFieldSubType = 10 
			AND t.ObjectID > @CustomerID
			ORDER BY t.ObjectID
		
		END
		
		SELECT @TestID = 0
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 11)
	BEGIN
		
		SELECT TOP 1 @CaseID = t.ObjectID 
		FROM @DetailValues t
		WHERE t.DetailFieldSubType = 11
		ORDER BY t.ObjectID
		
		WHILE @CaseID > @TestID 
		BEGIN

			SELECT @CustomerID = l.CustomerID, @LeadID = c.LeadID
			FROM Cases c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID
			WHERE c.CaseID = @CaseID

			/*Execute SAS for Lead*/
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID, @CustomerID, @LeadID, @CaseID

			SELECT @TestID = @MatterID
		
			SELECT TOP 1 @CaseID = t.ObjectID 
			FROM @DetailValues t
			WHERE t.DetailFieldSubType = 11 
			AND t.ObjectID > @CaseID
			ORDER BY t.ObjectID
		
		END
		
		SELECT @TestID = 0
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 12)
	BEGIN
		
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 13)
	BEGIN
		
		SELECT TOP 1 @ClientPersonnelIDForDV = t.ObjectID 
		FROM @DetailValues t
		WHERE t.DetailFieldSubType = 13
		ORDER BY t.ObjectID
		
		WHILE @CaseID > @TestID 
		BEGIN

			/*Execute SAS for Lead*/
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID, NULL, NULL, NULL, NULL, NULL, NULL, @ClientPersonnelIDForDV

			SELECT @TestID = @MatterID
		
			SELECT TOP 1 @ClientPersonnelIDForDV = t.ObjectID 
			FROM @DetailValues t
			WHERE t.DetailFieldSubType = 13 
			AND t.ObjectID > @ClientPersonnelIDForDV
			ORDER BY t.ObjectID
		
		END
		
		SELECT @TestID = 0
		
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 14)
	BEGIN
		
		SELECT TOP 1 @ContactIDForDV = t.ObjectID 
		FROM @DetailValues t
		WHERE t.DetailFieldSubType = 14
		ORDER BY t.ObjectID
		
		WHILE @CaseID > @TestID 
		BEGIN

			/*Execute SAS for Lead*/
			EXEC dbo._C00_SAS @ClientID, @ClientPersonnelID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @ContactIDForDV

			SELECT @TestID = @MatterID
		
			SELECT TOP 1 @ContactIDForDV = t.ObjectID 
			FROM @DetailValues t
			WHERE t.DetailFieldSubType = 14 
			AND t.ObjectID > @ContactIDForDV
			ORDER BY t.ObjectID
		
		END
		
		SELECT @TestID = 0
		
	END	
	
	SELECT PassedID, DetailValueID
	FROM @DetailValueIDs
	
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[SqlAfterSave_Bulk] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlAfterSave_Bulk] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlAfterSave_Bulk] TO [sp_executeall]
GO
