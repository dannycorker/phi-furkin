SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetClientAreaMailingListForPostCode] @ClientQuestionnaireID int, @PostCode nvarchar(10) 

AS

SELECT     ClientAreaMailingListID, Name, Email, PostCode, ClientID, ClientQuestionnaireID, OnHold
FROM         dbo.ClientAreaMailingLists
WHERE     (@PostCode LIKE PostCode + '%') AND (ClientQuestionnaireID = @ClientQuestionnaireID) AND (OnHold <> 1)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingListForPostCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientAreaMailingListForPostCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingListForPostCode] TO [sp_executeall]
GO
