SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudication table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_GetByAutoAdjudicationFieldTypeID]
(

	@AutoAdjudicationFieldTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AutoAdjudicationID],
					[EventChoiceID],
					[AutoAdjudicationTypeID],
					[ObjectID],
					[AutoAdjudicationFieldTypeID],
					[Comparison],
					[Value1],
					[Value2],
					[AppliesTo],
					[Count],
					[ValueTypeID]
				FROM
					[dbo].[AutoAdjudication] WITH (NOLOCK) 
				WHERE
					[AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_GetByAutoAdjudicationFieldTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_GetByAutoAdjudicationFieldTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_GetByAutoAdjudicationFieldTypeID] TO [sp_executeall]
GO
