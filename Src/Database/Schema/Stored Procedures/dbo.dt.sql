SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-22
-- Description:	List some DocumentType details by client and lead type
-- =============================================
CREATE PROCEDURE [dbo].[dt] 
	@ClientID int, 
	@LeadTypeID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	/* Document Type basics */
	SELECT  
	[DocumentTypeID],
	[ClientID],
	[LeadTypeID],
	[DocumentTypeName],
	[DocumentTypeDescription],
	[CanBeAutoSent],
	[EmailSubject],
	[InputFormat],
	[OutputFormat],
	[Enabled],
	[RecipientsTo],
	[RecipientsCC],
	[RecipientsBCC],
	[ReadOnlyTo],
	[ReadOnlyCC],
	[ReadOnlyBCC],
	[SendToMultipleRecipients],
	[MultipleRecipientDataSourceType],
	[MultipleRecipientDataSourceID],
	[SendToAllByDefault]
	FROM dbo.DocumentType dt (nolock) 
	WHERE (dt.ClientID = @ClientID) 
	AND (dt.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
	ORDER BY dt.DocumentTypeName, dt.LeadTypeID   

	/* Event Type links to docs */
	SELECT et.*, 
	dt.[DocumentTypeID],
	dt.[DocumentTypeName],
	dt.[DocumentTypeDescription],
	dt.[CanBeAutoSent],
	dt.[EmailSubject],
	dt.[InputFormat],
	dt.[OutputFormat],
	dt.[Enabled]
	FROM dbo.DocumentType dt (nolock) 
	LEFT JOIN dbo.EventType et (nolock) on et.DocumentTypeID = dt.DocumentTypeID 
	WHERE (dt.ClientID = @ClientID) 
	AND (dt.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
	ORDER BY et.EventTypeName, et.LeadTypeID   
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[dt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dt] TO [sp_executeall]
GO
