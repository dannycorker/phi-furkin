SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientPreference table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreference_Get_List]

AS


				
				SELECT
					[ClientPreferenceID],
					[ClientID],
					[ClientPreferenceTypeID],
					[PreferenceValue]
				FROM
					[dbo].[ClientPreference] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreference_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Get_List] TO [sp_executeall]
GO
