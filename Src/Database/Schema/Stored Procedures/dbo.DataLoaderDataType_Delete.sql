SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderDataType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDataType_Delete]
(

	@DataLoaderDataTypeID int   
)
AS


				DELETE FROM [dbo].[DataLoaderDataType] WITH (ROWLOCK) 
				WHERE
					[DataLoaderDataTypeID] = @DataLoaderDataTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDataType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Delete] TO [sp_executeall]
GO
