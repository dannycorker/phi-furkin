SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-09
-- Description:	Quick check of recent AutomatedTasks to see what is going on
-- =============================================
CREATE PROCEDURE [dbo].[atc]
	@hours tinyint = 1, 
	@future bit = 0, 
	@minrecordcount int = 0 
AS
BEGIN
	SET NOCOUNT ON;

	-- Show params and options
	SELECT 'Options : Hours = ' + convert(varchar, @hours) + ' : ' + 'Show future = ' + convert(varchar, @future) + ' : ' + 'Min Record Count = ' + convert(varchar, @minrecordcount) as [Options Specified] 

	SELECT atp.* FROM dbo.AutomatedTaskParam atp where atp.ParamName = 'ALREADY_RUNNING' and ParamValue = '1'

	SELECT ati.* FROM dbo.AutomatedTaskInfo ati where ati.LockDateTime IS NOT NULL

	-- Show tasks that ran in the last (n) hours
	SELECT at.*, atr.* 
	FROM dbo.AutomatedTaskResult atr 
	INNER JOIN dbo.AutomatedTask at ON at.TaskID = atr.TaskID 
	WHERE atr.RunDate > dateadd(hh, -(@hours), dbo.fn_GetDate_Local()) 
	AND (atr.RecordCount >= @minrecordcount OR atr.Complete <> 1)
	ORDER BY atr.RunDate 
	
	-- Show tasks that will run in the next (n) hours
	IF @future = 1 
	BEGIN
		SELECT at.* 
		FROM dbo.AutomatedTask at 
		WHERE at.Enabled = 1 
		AND at.NextRunDateTime < dateadd(hh, @hours, dbo.fn_GetDate_Local()) 
		ORDER BY at.NextRunDateTime 
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[atc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[atc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[atc] TO [sp_executeall]
GO
