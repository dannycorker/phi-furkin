SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyMappingUserCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential_Insert]
(

	@ThirdPartyMappingUserCredentialID int    OUTPUT,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@UserID int   ,

	@UserName varchar (255)  ,

	@Password varchar (65)  ,

	@Salt varchar (50)  ,

	@Domain varchar (50)  ,

	@AttemptedLogins int   ,

	@AccountDisabled bit   ,

	@ThirdPartyClientID varchar (50)  ,

	@LDAPConfigID int   ,

	@ImpersonateThirdPartySystemID int   
)
AS


				
				INSERT INTO [dbo].[ThirdPartyMappingUserCredential]
					(
					[ThirdPartySystemID]
					,[ClientID]
					,[UserID]
					,[UserName]
					,[Password]
					,[Salt]
					,[Domain]
					,[AttemptedLogins]
					,[AccountDisabled]
					,[ThirdPartyClientID]
					,[LDAPConfigID]
					,[ImpersonateThirdPartySystemID]
					)
				VALUES
					(
					@ThirdPartySystemID
					,@ClientID
					,@UserID
					,@UserName
					,@Password
					,@Salt
					,@Domain
					,@AttemptedLogins
					,@AccountDisabled
					,@ThirdPartyClientID
					,@LDAPConfigID
					,@ImpersonateThirdPartySystemID
					)
				-- Get the identity value
				SET @ThirdPartyMappingUserCredentialID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Insert] TO [sp_executeall]
GO
