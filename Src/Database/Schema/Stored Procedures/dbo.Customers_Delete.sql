SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Customers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_Delete]
(

	@CustomerID int   
)
AS


				DELETE FROM [dbo].[Customers] WITH (ROWLOCK) 
				WHERE
					[CustomerID] = @CustomerID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Delete] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Delete] TO [sp_executeall]
GO
