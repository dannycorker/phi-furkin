SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTask table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTask_GetByRepeatTimeUnitsID]
(

	@RepeatTimeUnitsID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[TaskID],
					[ClientID],
					[Taskname],
					[Description],
					[Enabled],
					[RunAtHour],
					[RunAtMinute],
					[RepeatTimeUnitsID],
					[RepeatTimeQuantity],
					[NextRunDateTime],
					[WorkflowTask],
					[AlreadyRunning],
					[AutomatedTaskGroupID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[MaximumAllowableErrors],
					[EventSubTypeThresholding]
				FROM
					[dbo].[AutomatedTask] WITH (NOLOCK) 
				WHERE
					[RepeatTimeUnitsID] = @RepeatTimeUnitsID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_GetByRepeatTimeUnitsID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_GetByRepeatTimeUnitsID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_GetByRepeatTimeUnitsID] TO [sp_executeall]
GO
