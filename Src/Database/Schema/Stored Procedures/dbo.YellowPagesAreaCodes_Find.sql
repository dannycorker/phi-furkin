SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the YellowPagesAreaCodes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[YellowPagesAreaCodes_Find]
(

	@SearchUsingOR bit   = null ,

	@YellowPagesAreaCodeID int   = null ,

	@YellowPagesAreaCode varchar (10)  = null ,

	@PostCode varchar (10)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [YellowPagesAreaCodeID]
	, [YellowPagesAreaCode]
	, [PostCode]
    FROM
	[dbo].[YellowPagesAreaCodes] WITH (NOLOCK) 
    WHERE 
	 ([YellowPagesAreaCodeID] = @YellowPagesAreaCodeID OR @YellowPagesAreaCodeID IS NULL)
	AND ([YellowPagesAreaCode] = @YellowPagesAreaCode OR @YellowPagesAreaCode IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [YellowPagesAreaCodeID]
	, [YellowPagesAreaCode]
	, [PostCode]
    FROM
	[dbo].[YellowPagesAreaCodes] WITH (NOLOCK) 
    WHERE 
	 ([YellowPagesAreaCodeID] = @YellowPagesAreaCodeID AND @YellowPagesAreaCodeID is not null)
	OR ([YellowPagesAreaCode] = @YellowPagesAreaCode AND @YellowPagesAreaCode is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[YellowPagesAreaCodes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Find] TO [sp_executeall]
GO
