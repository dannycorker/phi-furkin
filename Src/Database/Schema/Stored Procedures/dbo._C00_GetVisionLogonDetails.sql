SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-03-29
-- Description:	Get logon details for Vision
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetVisionLogonDetails]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	IF DB_NAME() IN ('Aquarius607Dev', 'Aquarius607QA')
	BEGIN

		SELECT 'trupanion-uat.aqvision.pet' AS [VisionUrl],
				'azure_client' AS [VisionClientID],
				'azure_secret' AS [VisionSecret]

	END

END
GO
GRANT EXECUTE ON  [dbo].[_C00_GetVisionLogonDetails] TO [sp_executeall]
GO
