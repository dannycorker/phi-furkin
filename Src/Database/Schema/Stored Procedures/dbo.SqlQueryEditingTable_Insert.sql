SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditingTable table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingTable_Insert]
(

	@SqlQueryEditingTableID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryTableName varchar (50)  ,

	@TableAlias varchar (50)  ,

	@TableDisplayOrder int   ,

	@JoinType varchar (25)  ,

	@JoinText varchar (500)  ,

	@JoinTableID int   ,

	@JoinRTRID int   ,

	@RealSqlQueryTableID int   
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditingTable]
					(
					[ClientID]
					,[SqlQueryEditingID]
					,[SqlQueryTableName]
					,[TableAlias]
					,[TableDisplayOrder]
					,[JoinType]
					,[JoinText]
					,[JoinTableID]
					,[JoinRTRID]
					,[RealSqlQueryTableID]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryEditingID
					,@SqlQueryTableName
					,@TableAlias
					,@TableDisplayOrder
					,@JoinType
					,@JoinText
					,@JoinTableID
					,@JoinRTRID
					,@RealSqlQueryTableID
					)
				-- Get the identity value
				SET @SqlQueryEditingTableID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Insert] TO [sp_executeall]
GO
