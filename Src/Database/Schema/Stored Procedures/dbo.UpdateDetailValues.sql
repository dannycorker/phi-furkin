SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 31-Jan-2010
-- Description:	Updates the given detail values
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDetailValues]

	@tvpDetailValue tvpDetailValue READONLY

AS
BEGIN

	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 1)
	BEGIN
		UPDATE LeadDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.LeadDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND dv.LeadID = t.AnyID AND t.DetailFieldSubTypeID = 1 
	END

	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 2)
	BEGIN
		UPDATE MatterDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.MatterDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND dv.MatterID = t.AnyID AND t.DetailFieldSubTypeID = 2 
	END
	
	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 10)
	BEGIN
		UPDATE CustomerDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.CustomerDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND dv.CustomerID = t.AnyID AND t.DetailFieldSubTypeID = 10 
	END

	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 11)
	BEGIN
		UPDATE CaseDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.CaseDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND dv.CaseID = t.AnyID AND t.DetailFieldSubTypeID = 11 
	END

	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 12)
	BEGIN
		UPDATE ClientDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.ClientDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND t.DetailFieldSubTypeID = 12
	END

	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 13)
	BEGIN
		UPDATE ClientPersonnelDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.ClientPersonnelDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND dv.ClientPersonnelID = t.AnyID and t.DetailFieldSubTypeID = 13
	END
	
	IF EXISTS(SELECT * FROM @tvpDetailValue WHERE DetailFieldSubTypeID = 14)
	BEGIN
		UPDATE ContactDetailValues 
		SET DetailValue = t.DetailValue 
		FROM dbo.ContactDetailValues dv 
		INNER JOIN @tvpDetailValue t on dv.DetailFieldID = t.DetailFieldID 
		WHERE dv.ClientID = t.ClientID AND dv.ContactID = t.AnyID and t.DetailFieldSubTypeID = 14
	END	

	SET NOCOUNT ON;
    
END




GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDetailValues] TO [sp_executeall]
GO
