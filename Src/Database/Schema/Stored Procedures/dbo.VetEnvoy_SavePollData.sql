SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-23
-- Description:	Saves the poll data from VetEnvoy for processing
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_SavePollData]
(
	@ClientID INT,
	@AccountID INT,
	@Xml XML
)
AS
BEGIN
	SET NOCOUNT ON;
	
	
	-- Get all the raw data
	DECLARE @Conversations TABLE
	(
		ID INT IDENTITY,
		Service VARCHAR(50),
		Status INT,
		ConversationID UNIQUEIDENTIFIER,
		ParentConversationID UNIQUEIDENTIFIER,
		ConversationPK INT
	)
	INSERT @Conversations (Service, Status, ConversationID, ParentConversationID)
	SELECT	T.N.value('@t', 'VARCHAR(50)') AS Service,
			T.N.value('@s', 'INT') AS Status,
			T.N.value('@id', 'UNIQUEIDENTIFIER') AS ConversationID,
			T.N.value('@pid', 'UNIQUEIDENTIFIER') AS ParentConversationID
	FROM @Xml.nodes('//c') AS T(N)
	WHERE T.N.value('@s', 'INT') IN (13000, 23000, 33000)
	
	-- See if this conversation already exists
	UPDATE c
	SET c.ConversationPK = e.ConversationPK
	FROM @Conversations c
	INNER JOIN dbo.VetEnvoy_Conversations e WITH (NOLOCK) ON c.ConversationID = e.ConversationID
	
	-- Create the ones that don't
	DECLARE @NewConversations TABLE
	(
		ConversationID VARCHAR(50),
		ConversationPK INT
	)
	INSERT INTO dbo.VetEnvoy_Conversations (ClientID, AccountID, ConversationID, ParentConversationID, Service)
	OUTPUT inserted.ConversationID, inserted.ConversationPK INTO @NewConversations
	SELECT @ClientID, @AccountID, ConversationID, ParentConversationID, Service
	FROM @Conversations
	WHERE ConversationPK IS NULL
	
	UPDATE c
	SET c.ConversationPK = e.ConversationPK
	FROM @Conversations c
	INNER JOIN @NewConversations e ON c.ConversationID = e.ConversationID
	
	-- Now insert the history row
	INSERT INTO dbo.VetEnvoy_ConversationHistory(ClientID, ConversationPK, ConversationID, Received, Service, Status)
	SELECT @ClientID, c.ConversationPK, c.ConversationID, dbo.fn_GetDate_Local(), c.Service, c.Status
	FROM @Conversations c
	WHERE NOT EXISTS 
	(
		SELECT *
		FROM dbo.VetEnvoy_ConversationHistory h WITH (NOLOCK) 
		WHERE h.ConversationID = c.ConversationID
		AND h.Status = c.Status
	)
	
	-- Save the updated poll ref
	DECLARE @PollRef BIGINT
	SELECT @PollRef = @Xml.value('/node()[1]/@ref', 'BIGINT')
		
	UPDATE dbo.VetEnvoy_Accounts 
	SET Pollref = @PollRef
	WHERE AccountID = @AccountID
	
	SELECT COUNT(*) AS Count
	FROM @Conversations

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SavePollData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_SavePollData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SavePollData] TO [sp_executeall]
GO
