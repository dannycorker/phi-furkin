SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUserGroupPanelItemChartingAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_Update]
(

	@PortalUserGroupPanelItemChartingAccessID int   ,

	@ClientID int   ,

	@PortalUserGroupID int   ,

	@PanelItemChartingID int   ,

	@HasAccess bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalUserGroupPanelItemChartingAccess]
				SET
					[ClientID] = @ClientID
					,[PortalUserGroupID] = @PortalUserGroupID
					,[PanelItemChartingID] = @PanelItemChartingID
					,[HasAccess] = @HasAccess
				WHERE
[PortalUserGroupPanelItemChartingAccessID] = @PortalUserGroupPanelItemChartingAccessID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Update] TO [sp_executeall]
GO
