SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the GroupFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing_Update]
(

	@GroupFunctionControlEditingID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants bit   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[GroupFunctionControlEditing]
				SET
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ModuleID] = @ModuleID
					,[FunctionTypeID] = @FunctionTypeID
					,[HasDescendants] = @HasDescendants
					,[RightID] = @RightID
					,[LeadTypeID] = @LeadTypeID
				WHERE
[GroupFunctionControlEditingID] = @GroupFunctionControlEditingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Update] TO [sp_executeall]
GO
