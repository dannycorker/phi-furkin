SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-11-24
-- Description:	SQL After Note Create processing
-- Mods:
-- =============================================
CREATE PROCEDURE [dbo].[_C321_SAN]
	@LeadEventID int

AS
BEGIN
	SET NOCOUNT ON;

--declare @LeadEventID INT = 894
	/*
	CS 2016-09-30
	This only exists for cross-compatibilitiy with C384 changes that were made on dev
	*/


	RETURN

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C321_SAN] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C321_SAN] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C321_SAN] TO [sp_executeall]
GO
