SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-10-24
-- Description:	Prepare BankVal Request
-- AquariumEventSubType that handled
-- 31	BankVal Validate a sort code and account number bankValPlus2(AccountNumber, Sortcode, UserID, PIN, Format)
-- 32	BankVal Lookup a sortcode getBranchDetails2(Sortcode, UserID, PIN, Format)
-- 33	BankVal Get the IBAN and SWIFT Code for a UK bank account deriveIbanBic(Sortcode, AccountNumber, UserID, PIN, Format)
-- 34	BankVal Validate Building Society Roll Number refValidate(Sortcode, AccountNumber, BuildingSocietyRollNumber, UserID, PIN, Format)
-- 35	BankVal Lookup SWIFT bank details getBankDetails2(SwiftBIC, UserID, PIN, Format)
-- 36	BankVal Validate an IBAN ibanValidate(IBAN, UserID, PIN, Format)
-- 37	BankVal Lookup ABA bank details getAbaDetails(ABA, UserID, PIN, Format)
-- 38	BankVal Get addresses in a postcode addressList(Postcode, UserID, PIN, Format)
-- 39	BankVal Validate an address addressCheck(Postcode, Premise, UserID, PIN, Format)
-- Mods
-- dcm 04/11/2014 Added client 327 code for subtype 31
-- JWG 2016-03-03 #37098 Charge for BankVal lookups, using the ClientBillingDetail suite
-- dcm 2016-11-01 Added policy admin fields IDs for 384
-- UAH/GPR 2018-08-03 Added line 129, if new account number and sort blank, select current sort and account no if the event that is being executed is 'issue refund BACS'
-- =============================================
CREATE PROCEDURE [dbo].[BankVal__PrepareRequest]
	@EventTypeID INT,
	@AquariumEventSubtypeID INT,
	@ClientID INT,
	@LeadID INT,
	@CaseID INT,
	@ClientPersonnelID INT,
	@HandleInDatabase BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	/* May want to extract this to Aq table. May be/become client specific */
	DECLARE @UserID VARCHAR(100) = 'Aqua001',
			@PIN VARCHAR(100) = '56653' 

	DECLARE 
			@AccName VARCHAR(500),
			@AccNo VARCHAR(30),
			@AccNoDFID INT,
			@ErrorMessage VARCHAR(1000)='',
			@LeadTypeID INT,
			@LogEntry VARCHAR(2000),
			@MatterID INT,
			@SortCode VARCHAR(30),
			@SortCodeDFID INT,
			@WarningMessage VARCHAR(1000)='',
			@ServiceMethod VARCHAR(100)=''

	-- DO BILLING BASED ON @ClientPersonnelID
	-- May need to validate if allowance has been reached
					
	--SELECT @LogEntry='Called with EventTypID:' + 	convert(varchar(30),@EventTypeID) + ', AquariumEventSubtypeID:' + CONVERT(varchar(30),@AquariumEventSubtypeID) + ', ClientID:' 
	--	+ CONVERT(VARCHAR(30),@ClientID) + ', LeadID:' + CONVERT(VARCHAR(30),@LeadID) + ', CaseID:' + CONVERT(VARCHAR(30),@CaseID)
	--EXEC _C00_LogIt 'Info', 'BankVal__PrepareRequest', 'Collecting Test Data', @LogEntry, 2372 

	---- temp fix
	--SELECT @ClientID=ClientID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID						
	
	SELECT @ServiceMethod = 'bankvalplus2'
			
	IF @HandleInDatabase = 1
	BEGIN
	
		IF @AquariumEventSubtypeID = 37
		BEGIN
			
			SELECT @ServiceMethod = 'bankvalplus2'
			
			IF @ClientID BETWEEN 600 AND 699 -- 2020-01-13 CPS for JIRA LPC-356 | 60X will all be t-pet clients
			BEGIN
			
				SELECT @LeadTypeID=l.LeadTypeID	FROM Lead l WITH (NOLOCK) WHERE LeadID=@LeadID
				IF @LeadTypeID = 1490 -- Pet Claim
				BEGIN

					SELECT TOP 1 @MatterID=MatterID FROM Matter WITH (NOLOCK) WHERE CaseID=@CaseID
				
					-- customer requests to use existing bank details (on collections case)
					IF 5144 = ( SELECT dbo.fnGetDvAsInt(175297,@CaseID) )
					BEGIN
					
						SELECT TOP 1 @AccNo=accno.DetailValue,
								@SortCode=accsc.DetailValue,
								@AccName=accn.DetailValue  
						FROM LeadTypeRelationship ltrclaim WITH (NOLOCK) 
						INNER JOIN LeadTypeRelationship ltrpacol WITH (NOLOCK) ON ltrpacol.FromLeadID=ltrclaim.FromLeadID AND ltrpacol.ToLeadTypeID=1493
						INNER JOIN LeadTypeRelationship ltrcol WITH (NOLOCK) ON ltrpacol.FromMatterID=ltrcol.FromMatterID and ltrcol.ToLeadTypeID=1493
						INNER JOIN MatterDetailValues accno WITH (NOLOCK) ON accno.MatterID=ltrcol.ToMatterID and accno.DetailFieldID=170190
						INNER JOIN MatterDetailValues accsc WITH (NOLOCK) ON accno.MatterID=accsc.MatterID and accsc.DetailFieldID=170189
						INNER JOIN MatterDetailValues accn WITH (NOLOCK) ON accno.MatterID=accn.MatterID and accn.DetailFieldID=170188
						WHERE ltrclaim.ToLeadID=@LeadID AND ltrclaim.FromLeadTypeID=1492
						
						EXEC dbo._C00_SimpleValueIntoField 170264, @AccName, @MatterID
						EXEC dbo._C00_SimpleValueIntoField 170265, @AccNo, @MatterID
						EXEC dbo._C00_SimpleValueIntoField 170266, @SortCode, @MatterID

					END

					EXEC dbo._C00_SimpleValueIntoField 175297, '', @MatterID

					SELECT @AccNoDFID=170265, @SortCodeDFID=170266
				
				END
				ELSE IF @LeadTypeID = 1493 -- Collections
				BEGIN

					IF @EventTypeID= 155272
					BEGIN
						SELECT @AccNoDFID=175382, @SortCodeDFID=175363
					END
					ELSE
					BEGIN
						SELECT @AccNoDFID=170190, @SortCodeDFID=170189
					END
				
				END
				ELSE IF @LeadTypeID = 1492 -- Policy Admin
				BEGIN
--====================================================================================
					IF @EventTypeID = 158840 --issue refund by BACS
					BEGIN 
						SELECT @AccNoDFID=180068, @SortCodeDFID=180069 --Use the stored account no/sortcode based on PA lead
					END
					ELSE
					BEGIN 
						SELECT @AccNoDFID=170252, @SortCodeDFID=170251
					END

--====================================================================================	
				END
				ELSE
				BEGIN 
	
					SELECT @ErrorMessage = 'Detail Field IDs not set up for this lead type. Raise ticket with Aquarium',
							@UserID='',
							@PIN='',
							@AccNo='',
							@SortCode=''
				
				END
				
				IF @ErrorMessage = ''
				BEGIN

					SELECT @AccNo = dbo.fnGetDv(@AccNoDFID,@CaseID)
					SELECT @SortCode =  dbo.fnGetDv(@SortCodeDFID,@CaseID)
			
					/* #37098 - Charge the appropriate client for the lookup (calculates the charge automatically) */
					EXEC dbo.ClientBillingDetail__Insert @ClientID = @ClientID, @SourceAquariumOptionID = 85, @ModuleID = 1, @WhenCreated = NULL, @WhoCreated = @ClientPersonnelID, @ChargeInPence = 2, @Notes = 'BankVal__PrepareRequest proc' 
					
				END
				
			END

			SELECT 
				@UserID UserID, 
				@PIN PIN,
				@ServiceMethod ServiceMethod,
				@AccNo AccountNumber, 
				@SortCode Sortcode,
				'' BuildingSocietyRollNumber, 
				'' SwiftBIC, 
				'' IBAN, 
				'' ABA, 
				'' Postcode, 
				'' Premise, 
				'xml' Format, 
				@ErrorMessage Error, 
				@WarningMessage Warning		
					
		END
		ELSE
		BEGIN
			SELECT 
			'' UserID, 
			'' PIN,
			@ServiceMethod ServiceMethod,
			'' AccountNumber, 
			'' Sortcode, 
			'' BuildingSocietyRollNumber, 
			'' SwiftBIC, 
			'' IBAN, 
			'' ABA, 
			'' Postcode, 
			'' Premise, 
			'xml' Format, 
			'Prepare request has no available' Error, 
			'Prepare request has no available' Warning
		END
		
	END
	--ELSE
	--BEGIN
	--	SELECT 
	--		@UserID UserID, 
	--		@PIN PIN,
	--		@ServiceMethod ServiceMethod,
	--		'81186078' AccountNumber, 
	--		'401808' Sortcode, 
	--		'' BuildingSocietyRollNumber, 
	--		'' SwiftBIC, 
	--		'' IBAN, 
	--		'' ABA, 
	--		'' Postcode, 
	--		'' Premise, 
	--		'xml' Format, 
	--		'' Error, 
	--		'' Warning
	--END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[BankVal__PrepareRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankVal__PrepareRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankVal__PrepareRequest] TO [sp_executeall]
GO
