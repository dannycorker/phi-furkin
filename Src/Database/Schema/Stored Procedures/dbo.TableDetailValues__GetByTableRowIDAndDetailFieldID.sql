SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID]
(

	@TableRowID int,
	@ClientID int,
	@DetailFieldID int
)
AS


				SELECT
					[TableDetailValueID],
					[TableRowID],
					[ResourceListID],
					[DetailFieldID],
					[DetailValue],
					[LeadID],
					[MatterID],
					[ClientID],
					[EncryptedValue],
					[ErrorMsg],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID]
				FROM
					[dbo].[TableDetailValues] WITH (NOLOCK) 
				WHERE
					[TableRowID] = @TableRowID and 
					[ClientID] = @ClientID and					
					[DetailFieldID] = @DetailFieldID

				UNION
				
				SELECT
					tdv.[TableDetailValueID],
					tdv.[TableRowID],
					tdv.[ResourceListID],
					tdv.[DetailFieldID],
					tdv.[DetailValue],
					tdv.[LeadID],
					tdv.[MatterID],
					tdv.[ClientID],
					tdv.[EncryptedValue],
					tdv.[ErrorMsg],
					tdv.[ValueInt],
					tdv.[ValueMoney],
					tdv.[ValueDate],
					tdv.[ValueDateTime],
					tdv.[CustomerID],
					tdv.[CaseID],
					tdv.[ClientPersonnelID],
					tdv.[ContactID]
				FROM
					[dbo].[TableDetailValues] tdv WITH (NOLOCK) 
				INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = tdv.ResourceListID and rldv.DetailFieldID = @DetailFieldID
				WHERE
					tdv.[TableRowID] = @TableRowID and 
					tdv.[ClientID] = @ClientID 
					
					
			Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID] TO [sp_executeall]
GO
