SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-14
-- Description:	Sets the amount due at next payment date
-- Mods
-- 2015-03-16 Update multi-policy summation
-- 2015-05-05 Update next payment section
-- 2015-07-15 Update for due date
-- 2017-05-24 CPS added DISTINCT to @PolicyMatters insert
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SetNextPaymentAmount] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50022237
	
	DECLARE @CaseID INT,
			@Gross MONEY,
			@LastPremiumDate DATE,
			@NextPaymentDate DATE,
			@NextPremiumDFID INT,
			@PolicyCaseID INT,
			@PolicyMatterID INT,
			@SubTotal MONEY
			
	DECLARE @PolicyMatters TABLE ( MatterID INT, Total MONEY, Done INT)
	
	-- make sure we are dealing with a collections matterID
	IF NOT EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @MatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
		
	END

	-- get next payment date
	SELECT @CaseID=CaseID FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID
	SELECT @NextPaymentDate=dbo.fnGetDvAsDate (170185, @CaseID)
	
	-- get related policy matters
	INSERT INTO @PolicyMatters
	SELECT DISTINCT ltr.FromMatterID, 0, 0 
	FROM LeadTypeRelationship ltr WITH (NOLOCK) 
	WHERE ltr.ToMatterID=@MatterID AND ltr.FromLeadTypeID=1492
--SELECT * FROM @PolicyMatters
	-- for each:
	WHILE EXISTS ( SELECT * FROM @PolicyMatters WHERE Done=0 )
	BEGIN
	
		SELECT TOP 1 @PolicyMatterID=MatterID FROM @PolicyMatters WHERE Done=0 
		
		-- sum existing outstanding payments
		SELECT @SubTotal=SUM(ISNULL(am.ValueMoney,0)) 
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues crid WITH (NOLOCK) ON tr.TableRowID=crid.TableRowID AND crid.DetailFieldID=170073
		INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078
		INNER JOIN TableDetailValues am WITH (NOLOCK) ON tr.TableRowID=am.TableRowID AND am.DetailFieldID=170072
		INNER JOIN TableDetailValues DueDate WITH (NOLOCK) ON tr.TableRowID=DueDate.TableRowID AND DueDate.DetailFieldID=175597
		WHERE tr.DetailFieldID=170088 AND tr.MatterID=@PolicyMatterID
		AND crid.DetailValue=''
		AND stat.ValueInt=72154 -- ready
		AND DueDate.ValueDate<=@NextPaymentDate

--SELECT @SubTotal
		UPDATE @PolicyMatters SET Total=ISNULL(@SubTotal,0),Done=1 WHERE MatterID=@PolicyMatterID
	
	END
--select @Gross	
	-- get grand total
	SELECT @Gross=SUM(Total) FROM @PolicyMatters
--select @Gross	
	-- update field
	EXEC dbo._C00_SimpleValueIntoField 175439, @Gross, @MatterID	
	
	--RETURN @Gross
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetNextPaymentAmount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SetNextPaymentAmount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetNextPaymentAmount] TO [sp_executeall]
GO
