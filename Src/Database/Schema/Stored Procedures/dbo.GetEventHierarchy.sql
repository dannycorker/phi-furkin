SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetEventHierarchy] (@LeadTypeID int, @StartFromID int = 0) AS

SET NOCOUNT ON

DECLARE @level int, @EventTypeID int, @EventSubtypeID int, @eventchoicedesc varchar(250), @eventtypedesc varchar(250), @outputline varchar(2000)

-- Create a temp table for this event_choice_stack
CREATE TABLE #stack (s_eventtypeid int, s_eventsubtypeid int,  s_eventchoicedesc varchar(250), s_eventtypedesc varchar(250), s_level int)

-- Create a temp table for avoiding infinite loops
CREATE TABLE #choices (c_eventtypeid int, c_nexteventtypeid int)

-- Get the Process Start event type for this lead type, unless a specific starting event type was passed in
IF @StartFromID > 0
BEGIN
	SET @EventTypeID = @StartFromID
END
ELSE
BEGIN
	SELECT @EventTypeID = EventTypeID
	FROM EventType
	WHERE LeadTypeID = @LeadTypeID 
	AND EventSubtypeID = 10
END

SELECT @eventtypedesc = EventTypeName, @EventSubtypeID = EventSubtypeID
FROM EventType 
WHERE EventTypeID = @EventTypeID

INSERT INTO #stack VALUES (@EventTypeID, @EventSubtypeID, 'Starting Event', @eventtypedesc, 1)

SELECT @level = 1

WHILE @level > 0
BEGIN
	IF EXISTS (SELECT * FROM #stack WHERE s_level = @level)
	BEGIN
		SELECT @EventTypeID = s_eventtypeid, @EventSubtypeID = s_eventsubtypeid,  @eventchoicedesc = s_eventchoicedesc, @eventtypedesc = s_eventtypedesc
		FROM #stack
		WHERE s_level = @level

		SELECT @outputline = space((@level - 1)*2) + convert(varchar,@EventTypeID) + '  ' + @eventchoicedesc + ' (' + convert(varchar,@EventSubtypeID) + ')   '  --+ @eventtypedesc

		PRINT @outputline

		DELETE FROM #stack
		WHERE s_level = @level
		AND s_eventtypeid = @EventTypeID

		INSERT #stack
		SELECT EventChoice.NextEventTypeID, EventType.EventSubtypeID, EventType.EventTypeName /*EventChoice.[Description]*/, EventType.EventTypeName, @level + 1
		FROM EventChoice
		INNER JOIN EventType ON EventType.EventTypeID = EventChoice.NextEventTypeID
		WHERE EventChoice.EventTypeID = @EventTypeID
		AND EventChoice.EscalationEvent = 0
		AND NOT EXISTS (
					SELECT * 
					FROM #choices 
					WHERE EventChoice.EventTypeID = c_eventtypeid
					AND EventChoice.NextEventTypeID = c_nexteventtypeid
				    )

		INSERT #choices
		SELECT EventChoice.EventTypeID, EventChoice.NextEventTypeID
		FROM EventChoice
		WHERE EventChoice.EventTypeID = @EventTypeID
		AND EventChoice.EscalationEvent = 0
		AND NOT EXISTS (
					SELECT * 
					FROM #choices 
					WHERE EventChoice.EventTypeID = c_eventtypeid
					AND EventChoice.NextEventTypeID = c_nexteventtypeid
				    )


		IF @@ROWCOUNT > 0
		BEGIN
			SELECT @level = @level + 1
		END
	END
	ELSE
	BEGIN
		SELECT @level = @level - 1
	END

END -- WHILE

DROP TABLE #stack

DROP TABLE #choices




GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventHierarchy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventHierarchy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventHierarchy] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[GetEventHierarchy] TO [sp_executehelper]
GO
