SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-02-12
-- Description:	Get a query and run it for a customer/lead
--				CPS 2017-07-05 added TOP 10 limit
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LeadInfoCustomSql]
	@LeadID INT,
	@ClientPersonnelID INT,
	@CaseID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CustomerID INT,
			@LeadTypeID INT,
			@ClientPersonnelAdminGroupID INT,
			@SQLQuery VARCHAR(MAX) = '',
			@ClientID INT = dbo.fnGetPrimaryClientID()
	
	SELECT @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID, @ClientID = l.ClientID
	FROM Lead l WITH (NOLOCK)
	WHERE l.LeadID = @LeadID
	
	SELECT @ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID
	
	SELECT TOP 10 @SQLQuery = @SQLQuery + ' ' + sq.QueryText
	FROM LeadInfoCustomSQL c WITH (NOLOCK)
	INNER JOIN SqlQuery sq WITH (NOLOCK) ON sq.QueryID = c.SQLQueryID
	WHERE c.LeadTypeID = @LeadTypeID
	AND c.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	ORDER BY c.ViewOrder

	IF @@ROWCOUNT = 0
	BEGIN
	
		/*
			ACE 2015-03-03
			As there are no rows we will try again but with a null Client Personnel Admin Group ID
		*/
		SELECT TOP (10) @SQLQuery = @SQLQuery + ' ' + '/* QueryID ' + CONVERT(VARCHAR,sq.QueryID) + ' */' + sq.QueryText
		FROM LeadInfoCustomSQL c WITH (NOLOCK)
		INNER JOIN SqlQuery sq WITH (NOLOCK) ON sq.QueryID = c.SQLQueryID
		WHERE c.LeadTypeID = @LeadTypeID
		AND c.ClientPersonnelAdminGroupID IS NULL
		ORDER BY c.ViewOrder
	
		IF @@ROWCOUNT = 0
		BEGIN
		
			DECLARE @Table TABLE (MyInt INT)
			SELECT *
			FROM @Table

		END
	
	END
	
	SELECT @SQLQuery = REPLACE(@SQLQuery, '@CustomerID', CONVERT(VARCHAR,@CustomerID))
	SELECT @SQLQuery = REPLACE(@SQLQuery, '@LeadID', CONVERT(VARCHAR,@LeadID))
	SELECT @SQLQuery = REPLACE(@SQLQuery, '@ClientPersonnelID', CONVERT(VARCHAR,@ClientPersonnelID))
	SELECT @SQLQuery = REPLACE(@SQLQuery, '@ClientID',CONVERT(VARCHAR,@ClientID))
	SELECT @SQLQuery = REPLACE(@SQLQuery, '@CaseID',CONVERT(VARCHAR,ISNULL(@CaseID,'')))
		
	EXEC (@SQLQuery)
	
	PRINT @SqlQuery

END







GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadInfoCustomSql] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LeadInfoCustomSql] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadInfoCustomSql] TO [sp_executeall]
GO
