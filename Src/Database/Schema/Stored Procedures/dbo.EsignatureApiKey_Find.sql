SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EsignatureApiKey table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureApiKey_Find]
(

	@SearchUsingOR bit   = null ,

	@EsignatureApiKeyID int   = null ,

	@ClientID int   = null ,

	@ApiKey varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EsignatureApiKeyID]
	, [ClientID]
	, [ApiKey]
    FROM
	[dbo].[EsignatureApiKey] WITH (NOLOCK) 
    WHERE 
	 ([EsignatureApiKeyID] = @EsignatureApiKeyID OR @EsignatureApiKeyID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ApiKey] = @ApiKey OR @ApiKey IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EsignatureApiKeyID]
	, [ClientID]
	, [ApiKey]
    FROM
	[dbo].[EsignatureApiKey] WITH (NOLOCK) 
    WHERE 
	 ([EsignatureApiKeyID] = @EsignatureApiKeyID AND @EsignatureApiKeyID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ApiKey] = @ApiKey AND @ApiKey is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureApiKey_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Find] TO [sp_executeall]
GO
