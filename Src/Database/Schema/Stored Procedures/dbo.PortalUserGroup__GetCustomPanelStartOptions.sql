SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-12-07
-- Description:	Gets details of custom Portal startup requirements
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserGroup__GetCustomPanelStartOptions]
(
	@PortalUserGroupID int,
	@PortalUserID int,
	@ClientID int	
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	/* If ShowGroupMessage is on, show the message, even if it is entirely blank. */				
	SELECT 'MESSAGE' AS [StartOption], IsNull(pug.MessageTitle, '') as [MessageTitle], REPLACE(IsNull(pug.MessageBody, ''), CHAR(13) + CHAR(10), '<BR />') as [MessageBody] 
	FROM dbo.PortalUserGroup pug WITH (NOLOCK) 
	WHERE pug.PortalUserGroupID = @PortalUserGroupID 
	AND pug.ClientID = @ClientID 
	AND pug.ShowGroupMessage = 1
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetCustomPanelStartOptions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup__GetCustomPanelStartOptions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetCustomPanelStartOptions] TO [sp_executeall]
GO
