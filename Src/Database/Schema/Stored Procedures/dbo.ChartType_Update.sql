SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChartType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartType_Update]
(

	@ChartTypeID int   ,

	@OriginalChartTypeID int   ,

	@ChartTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChartType]
				SET
					[ChartTypeID] = @ChartTypeID
					,[ChartTypeName] = @ChartTypeName
				WHERE
[ChartTypeID] = @OriginalChartTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Update] TO [sp_executeall]
GO
