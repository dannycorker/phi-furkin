SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-01-11
-- Modified: PR - Fixed matter comparison and made it only return rows for the given equation column field
-- Description:	Get a list of all the table rows that
--              need to be recalculated when a lead/matter is saved
-- MODIFIED:	2015-02-04	ACE	Updated to use customer tables #30916
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetTableRowsForCalulating] 
@equationColumnFieldID INT, 
@customerID INT, 
@leadID INT, 
@caseID INT, 
@matterID INT, 
@clientID INT, 
@clientPersonnelID INT, 
@contactID INT, 
@leadTypeID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @pdID INT
	DECLARE @pageDetailFieldID INT
	
	SELECT @pdID = df.DetailFieldPageID 
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE DetailFieldID = @equationColumnFieldID

	--SELECT @pageDetailFieldID = df.DetailFieldID
	--FROM dbo.DetailFields df WITH (NOLOCK) 
	--WHERE TableDetailFieldPageID = @pdID

	SELECT tr.*
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = @equationColumnFieldID
	INNER JOIN dbo.DetailFields df2 WITH (NOLOCK) ON tr.DetailFieldID = df2.DetailFieldID and df2.TableDetailFieldPageID = @pdID 
	WHERE (tr.CustomerID = @customerID AND @customerID > 0) 
	OR (tr.LeadID = @leadID AND @leadID > 0 AND tr.MatterID = 0)
	OR (tr.CaseID = @caseID AND @caseID > 0) 
	OR (tr.MatterID = @matterID AND @matterID > 0)
	ORDER BY tr.TableRowID 

	SELECT @@ROWCOUNT

END



GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetTableRowsForCalulating] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetTableRowsForCalulating] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetTableRowsForCalulating] TO [sp_executeall]
GO
