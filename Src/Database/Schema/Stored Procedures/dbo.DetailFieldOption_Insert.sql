SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-06-24
-- Description:	Created in absence of DAL proc
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldOption_Insert]
	@ClientID INT,
	@DetailFieldID INT,
	@Mandatory BIT,
	@FieldOrder INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [DetailFieldOption]
           ([ClientID],
           [DetailFieldID],
           [Mandatory],
           [FieldOrder])
     VALUES
           (@ClientID,
           @DetailFieldID,
           @Mandatory,
           @FieldOrder)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption_Insert] TO [sp_executeall]
GO
