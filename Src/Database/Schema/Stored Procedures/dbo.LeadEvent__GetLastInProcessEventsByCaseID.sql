SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Ben Crinion
-- Purpose: Return  the InProcess events for a case that have not been followed up or deleted.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent__GetLastInProcessEventsByCaseID]
(

	@CaseID int   
)
AS


	SELECT  le.*         
	FROM    LeadEvent le 
	INNER JOIN EventType et ON le.EventTypeID = et.EventTypeID
	WHERE   le.whenfollowedup IS NULL
		AND le.EventDeleted = 0
		AND et.InProcess    = 1
		AND et.Enabled      = 1
		AND le.CaseID = @CaseID


	Select @@ROWCOUNT
					
			



GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetLastInProcessEventsByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__GetLastInProcessEventsByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetLastInProcessEventsByCaseID] TO [sp_executeall]
GO
