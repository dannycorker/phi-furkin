SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset Type By SubType
-- =============================================
CREATE PROCEDURE [dbo].[AssetType__GetSubType] 
	@AssetSubTypeID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		at.[AssetTypeID],
		at.[AssetSubTypeID],
		at.[AssetTypeName],
		at.[Note],
		at.[Enabled],
		at.[Deleted],
		at.[WhoCreated],
		at.[WhenCreated],
		at.[WhoModified],
		at.[WhenModified]
	FROM 
		[dbo].[AssetType] at WITH (NOLOCK)
	WHERE
		at.AssetSubTypeID = @AssetSubTypeID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType__GetSubType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType__GetSubType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType__GetSubType] TO [sp_executeall]
GO
