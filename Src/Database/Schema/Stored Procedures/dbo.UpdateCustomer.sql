SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[UpdateCustomer]
@CustomerID int,
@ClientID int,
@TitleID int,
@FirstName nvarchar(100),
@MiddleName nvarchar(100),
@LastName nvarchar(100),
@EmailAddress nvarchar(255),
@DayTimeTelephoneNumber nvarchar(50),
@DayTimeTelephoneNumberVerifiedAndValid bit,
@HomeTelephone nvarchar(50),
@HomeTelephoneVerifiedAndValid bit,
@MobileTelephone nvarchar(50),
@MobileTelephoneVerifiedAndValid bit,
@Address1 nvarchar(200),
@Address2 nvarchar(200), 
@Town nvarchar(200),
@County nvarchar(200),
@PostCode nvarchar(10),
@Test bit,
@CompanyName nvarchar(100),
@CompanyTelephone nvarchar(50),
@CompanyTelephoneVerifiedAndValid bit,
@WorksTelephone nvarchar(50),
@WorksTelephoneVerifiedAndValid bit,
@Occupation nvarchar(100),
@Employer nvarchar(100),
@DoNotEmail bit,
@DoNotSellToThirdParty bit,
@AgreedToTermsAndConditions bit,
@PhoneNumbersVerifiedOn datetime,
@DateOfBirth dateTime,
@IsBusiness bit,
@DefaultContactID int


as

Update Customers

Set ClientID = @ClientID,
TitleID = @TitleID,
FirstName = @FirstName, 
MiddleName = @MiddleName, 
LastName = @LastName, 
EmailAddress = @EmailAddress, 
DayTimeTelephoneNumber = @DayTimeTelephoneNumber, 
DayTimeTelephoneNumberVerifiedAndValid = @DayTimeTelephoneNumberVerifiedAndValid,
HomeTelephone = @HomeTelephone, 
HomeTelephoneVerifiedAndValid = @HomeTelephoneVerifiedAndValid, 
MobileTelephone = @MobileTelephone, 
MobileTelephoneVerifiedAndValid = @MobileTelephoneVerifiedAndValid,
Address1 = @Address1, 
Address2 = @Address2, 
Town = @Town, 
County = @County, 
PostCode = @PostCode, 
Test = @Test, 
CompanyName = @CompanyName, 
CompanyTelephone = @CompanyTelephone, 
CompanyTelephoneVerifiedAndValid = @CompanyTelephoneVerifiedAndValid,
WorksTelephone = @WorksTelephone, 
WorksTelephoneVerifiedAndValid = @WorksTelephoneVerifiedAndValid, 
Occupation = @Occupation, 
Employer = @Employer,
DoNotEmail = @DoNotEmail,
DoNotSellToThirdParty=@DoNotSellToThirdParty,
AgreedToTermsAndConditions=~AgreedToTermsAndConditions,
PhoneNumbersVerifiedOn = @PhoneNumbersVerifiedOn,
DateOfBirth = @DateOfBirth,
IsBusiness = @IsBusiness,
DefaultContactID = @DefaultContactID

Where CustomerID = @CustomerID






GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateCustomer] TO [sp_executeall]
GO
