SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- ALTER date: 24-04-2013
-- Description:	Checks to see if the Outgoing number has been used on any other client
-- =============================================
CREATE PROCEDURE [dbo].[SMSSurveyClientDatabase_Check]
	@OutgoingPhoneNumber VARCHAR(250)
AS
BEGIN

	EXEC AquariusMaster.dbo.AQ_SMSSurveyClientDatabase_Check @OutgoingPhoneNumber

END


GO
GRANT VIEW DEFINITION ON  [dbo].[SMSSurveyClientDatabase_Check] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSSurveyClientDatabase_Check] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSSurveyClientDatabase_Check] TO [sp_executeall]
GO
