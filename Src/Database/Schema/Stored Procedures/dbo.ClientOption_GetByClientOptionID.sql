SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOption_GetByClientOptionID]
(

	@ClientOptionID int   
)
AS


				SELECT
					[ClientOptionID],
					[ClientID],
					[ZenDeskOff],
					[ZenDeskURLPrefix],
					[ZenDeskText],
					[ZenDeskTitle],
					[iDiaryOff],
					[WorkflowOff],
					[UserDirectoryOff],
					[eCatcherOff],
					[LeadAssignmentOff],
					[UserMessagesOff],
					[SystemMessagesOff],
					[UserPortalOff],
					[ReportSearchOff],
					[NormalSearchOff],
					[UseXero],
					[UseUltra],
					[UseCaseSummary],
					[UseMoreProminentReminders],
					[UseSMSSurvey],
					[UseNewAddLead],
					[DiallerInsertMethod],
					[DiallerPrimaryKey],
					[UseCHARMS],
					[TextMessageService],
					[UseThunderhead],
					[UseMemorableWordVerification],
					[ApplyInactivityTimeout],
					[InactivityTimeoutInSeconds],
					[InactivityRedirectUrl],
					[UseSentimentAnalysis],
					[ShowProcessInfoButton],
					[LatestRecordsFirst],
					[UseSecureMessage],
					[EventCommentTooltipOff],
					[EnableScripting],
					[EnableBilling],
					[UseDocumentTypeVersioning],
					[EnableSMSSTOP],
					[UseEngageMail],
					[PreventForcedLogout]
				FROM
					[dbo].[ClientOption] WITH (NOLOCK) 
				WHERE
										[ClientOptionID] = @ClientOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_GetByClientOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOption_GetByClientOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_GetByClientOptionID] TO [sp_executeall]
GO
