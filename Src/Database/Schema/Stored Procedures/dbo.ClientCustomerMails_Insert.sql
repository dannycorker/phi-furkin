SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientCustomerMails table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientCustomerMails_Insert]
(

	@ClientCustomerMailID int    OUTPUT,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@ClientAreaMailingListID int   ,

	@DateSent datetime   
)
AS


				
				INSERT INTO [dbo].[ClientCustomerMails]
					(
					[ClientID]
					,[ClientQuestionnaireID]
					,[CustomerID]
					,[ClientAreaMailingListID]
					,[DateSent]
					)
				VALUES
					(
					@ClientID
					,@ClientQuestionnaireID
					,@CustomerID
					,@ClientAreaMailingListID
					,@DateSent
					)
				-- Get the identity value
				SET @ClientCustomerMailID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientCustomerMails_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Insert] TO [sp_executeall]
GO
