SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditingCriteria table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingCriteriaID int   = null ,

	@ClientID int   = null ,

	@SqlQueryEditingID int   = null ,

	@SqlQueryEditingTableID int   = null ,

	@SqlQueryEditingColumnID int   = null ,

	@CriteriaText varchar (1000)  = null ,

	@Criteria1 varchar (250)  = null ,

	@Criteria2 varchar (250)  = null ,

	@CriteriaName varchar (250)  = null ,

	@SubQueryID int   = null ,

	@SubQueryLinkType varchar (50)  = null ,

	@ParamValue varchar (250)  = null ,

	@IsSecurityClause bit   = null ,

	@CriteriaSubstitutions varchar (2000)  = null ,

	@IsParameterizable bit   = null ,

	@Comparison varchar (50)  = null ,

	@IsJoinClause bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingCriteriaID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryEditingTableID]
	, [SqlQueryEditingColumnID]
	, [CriteriaText]
	, [Criteria1]
	, [Criteria2]
	, [CriteriaName]
	, [SubQueryID]
	, [SubQueryLinkType]
	, [ParamValue]
	, [IsSecurityClause]
	, [CriteriaSubstitutions]
	, [IsParameterizable]
	, [Comparison]
	, [IsJoinClause]
    FROM
	[dbo].[SqlQueryEditingCriteria] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingCriteriaID] = @SqlQueryEditingCriteriaID OR @SqlQueryEditingCriteriaID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([SqlQueryEditingTableID] = @SqlQueryEditingTableID OR @SqlQueryEditingTableID IS NULL)
	AND ([SqlQueryEditingColumnID] = @SqlQueryEditingColumnID OR @SqlQueryEditingColumnID IS NULL)
	AND ([CriteriaText] = @CriteriaText OR @CriteriaText IS NULL)
	AND ([Criteria1] = @Criteria1 OR @Criteria1 IS NULL)
	AND ([Criteria2] = @Criteria2 OR @Criteria2 IS NULL)
	AND ([CriteriaName] = @CriteriaName OR @CriteriaName IS NULL)
	AND ([SubQueryID] = @SubQueryID OR @SubQueryID IS NULL)
	AND ([SubQueryLinkType] = @SubQueryLinkType OR @SubQueryLinkType IS NULL)
	AND ([ParamValue] = @ParamValue OR @ParamValue IS NULL)
	AND ([IsSecurityClause] = @IsSecurityClause OR @IsSecurityClause IS NULL)
	AND ([CriteriaSubstitutions] = @CriteriaSubstitutions OR @CriteriaSubstitutions IS NULL)
	AND ([IsParameterizable] = @IsParameterizable OR @IsParameterizable IS NULL)
	AND ([Comparison] = @Comparison OR @Comparison IS NULL)
	AND ([IsJoinClause] = @IsJoinClause OR @IsJoinClause IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingCriteriaID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryEditingTableID]
	, [SqlQueryEditingColumnID]
	, [CriteriaText]
	, [Criteria1]
	, [Criteria2]
	, [CriteriaName]
	, [SubQueryID]
	, [SubQueryLinkType]
	, [ParamValue]
	, [IsSecurityClause]
	, [CriteriaSubstitutions]
	, [IsParameterizable]
	, [Comparison]
	, [IsJoinClause]
    FROM
	[dbo].[SqlQueryEditingCriteria] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingCriteriaID] = @SqlQueryEditingCriteriaID AND @SqlQueryEditingCriteriaID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([SqlQueryEditingTableID] = @SqlQueryEditingTableID AND @SqlQueryEditingTableID is not null)
	OR ([SqlQueryEditingColumnID] = @SqlQueryEditingColumnID AND @SqlQueryEditingColumnID is not null)
	OR ([CriteriaText] = @CriteriaText AND @CriteriaText is not null)
	OR ([Criteria1] = @Criteria1 AND @Criteria1 is not null)
	OR ([Criteria2] = @Criteria2 AND @Criteria2 is not null)
	OR ([CriteriaName] = @CriteriaName AND @CriteriaName is not null)
	OR ([SubQueryID] = @SubQueryID AND @SubQueryID is not null)
	OR ([SubQueryLinkType] = @SubQueryLinkType AND @SubQueryLinkType is not null)
	OR ([ParamValue] = @ParamValue AND @ParamValue is not null)
	OR ([IsSecurityClause] = @IsSecurityClause AND @IsSecurityClause is not null)
	OR ([CriteriaSubstitutions] = @CriteriaSubstitutions AND @CriteriaSubstitutions is not null)
	OR ([IsParameterizable] = @IsParameterizable AND @IsParameterizable is not null)
	OR ([Comparison] = @Comparison AND @Comparison is not null)
	OR ([IsJoinClause] = @IsJoinClause AND @IsJoinClause is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Find] TO [sp_executeall]
GO
