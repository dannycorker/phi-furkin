SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-24
-- Description:	Saves the action date back to the update rows
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_UpdateRowsActioned]
(
	@HistoryRows dbo.tvpInt READONLY
)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE dbo.VetEnvoy_ConversationHistory
	SET Actioned = dbo.fn_GetDate_Local()
	WHERE HistoryID IN
	(
		SELECT AnyID 
		FROM @HistoryRows
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_UpdateRowsActioned] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_UpdateRowsActioned] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_UpdateRowsActioned] TO [sp_executeall]
GO
