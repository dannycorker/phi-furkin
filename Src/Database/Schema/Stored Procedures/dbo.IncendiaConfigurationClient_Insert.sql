SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the IncendiaConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_Insert]
(

	@IncendiaConfigurationClientID int    OUTPUT,

	@AquariumClientID int   ,

	@Username varchar (50)  ,

	@Password varchar (50)  ,

	@URI varchar (150)  
)
AS


				
				INSERT INTO [dbo].[IncendiaConfigurationClient]
					(
					[AquariumClientID]
					,[Username]
					,[Password]
					,[URI]
					)
				VALUES
					(
					@AquariumClientID
					,@Username
					,@Password
					,@URI
					)
				-- Get the identity value
				SET @IncendiaConfigurationClientID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Insert] TO [sp_executeall]
GO
