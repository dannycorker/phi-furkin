SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientQuestionnaireLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaireLayout_Insert]
(

	@ClientQuestionnaireLayoutID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@PageNumber int   ,

	@LayoutCss varchar (MAX)  ,

	@LayoutCssFileName varchar (512)  ,

	@QuestionColumns int   
)
AS


				
				INSERT INTO [dbo].[ClientQuestionnaireLayout]
					(
					[ClientQuestionnaireID]
					,[ClientID]
					,[PageNumber]
					,[LayoutCss]
					,[LayoutCssFileName]
					,[QuestionColumns]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@ClientID
					,@PageNumber
					,@LayoutCss
					,@LayoutCssFileName
					,@QuestionColumns
					)
				-- Get the identity value
				SET @ClientQuestionnaireLayoutID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaireLayout_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Insert] TO [sp_executeall]
GO
