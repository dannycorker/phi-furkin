SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-06-15
-- Description:	Runs the claim calculations on a group of matters		
-- =============================================
CREATE PROCEDURE [dbo].[LeadEvent__GetNotesByLeadEventIDs] 
(
	@LeadEventIDs dbo.tvpInt READONLY
)

AS
BEGIN
	
	SELECT nt.NoteTypeName AS FileName, nt.NoteTypeDescription AS LeadDocumentTitle, le.WhenCreated AS UploadDateTime, le.Comments, le.CaseID
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN @LeadEventIDs id ON le.LeadEventID = id.AnyID
	INNER JOIN dbo.NoteType nt WITH (NOLOCK) ON nt.NoteTypeID = le.NoteTypeID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetNotesByLeadEventIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__GetNotesByLeadEventIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetNotesByLeadEventIDs] TO [sp_executeall]
GO
