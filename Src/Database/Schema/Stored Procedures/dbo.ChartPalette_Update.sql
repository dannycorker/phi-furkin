SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChartPalette table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartPalette_Update]
(

	@ChartPaletteID int   ,

	@OriginalChartPaletteID int   ,

	@ChartPaletteName varchar (50)  ,

	@ChartPalettePreviewImageUrl varchar (255)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChartPalette]
				SET
					[ChartPaletteID] = @ChartPaletteID
					,[ChartPaletteName] = @ChartPaletteName
					,[ChartPalettePreviewImageUrl] = @ChartPalettePreviewImageUrl
				WHERE
[ChartPaletteID] = @OriginalChartPaletteID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartPalette_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Update] TO [sp_executeall]
GO
