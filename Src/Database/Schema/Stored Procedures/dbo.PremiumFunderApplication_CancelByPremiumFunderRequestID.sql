SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2019-12-17
-- Description:	identifies AQ Matters of non-Cancelled on AQ PremiumFunderApplicationPolicies. Identifies ApplicationPolicy by a passed in PremiumFunderRequestID.
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplication_CancelByPremiumFunderRequestID]
	@PremiumFunderRequestID INT
AS
BEGIN
		
	SET NOCOUNT ON;


	DECLARE @LeadEventID INT
			,@AutomatedEventTypeID INT = 158974 /*Cancelled by Premium Funder*/
			,@MatterID INT
			,@CancellationDate DATE
			,@AqAutomation INT = 58552

	DECLARE @LeadEventIDs TABLE (LeadEventID INT, MatterID INT)
				
	INSERT INTO @LeadEventIDs (LeadEventID, MatterID)
	SELECT cases.latestnonnoteleadeventid, petpolicy.PAMatterID FROM PremiumFunderApplicationPolicy petpolicy WITH (NOLOCK)
	INNER JOIN Matter matter WITH (NOLOCK) ON matter.MatterID = petpolicy.PAMatterID
	INNER JOIN Cases cases WITH (NOLOCK) ON cases.CaseID = matter.CaseID
	WHERE petpolicy.PremiumFunderRequestID = @PremiumFunderRequestID and petpolicy.PremiumFunderRequestID NOT IN 
		(
			SELECT cancelAgreement.PremiumFunderRequestID 
			FROM PremiumFunderCancelAgreementResponse cancelAgreement WITH (NOLOCK) 
			WHERE cancelAgreement.MatterID = petpolicy.PAMatterID
		)

	/*GPR 2019-12-18 handle Loan agreements where there are morew than 1 Pet Policy for this agreement*/
	WHILE (SELECT COUNT(*) FROM @LeadEventIDs) > 0
	BEGIN

		SELECT @LeadEventID = (SELECT TOP(1) LeadEventID FROM @LeadEventIDs ORDER BY 1 DESC)
		SELECT @MatterID = (SELECT TOP(1) MatterID FROM @LeadEventIDs ORDER BY 1 DESC)

		EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID, @AutomatedEventTypeID, @AqAutomation, 1

		DELETE FROM @LeadEventIDs WHERE LeadEventID = @LeadEventID
		
		EXEC [dbo].[_C00_LogIt] 'Info', 'PremiumFunderApplication_CancelByPremiumFunderRequestID', 'Policy Cancelled by Premium Funder. Policy MatterID:', @MatterID, 58552

	END;
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplication_CancelByPremiumFunderRequestID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplication_CancelByPremiumFunderRequestID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplication_CancelByPremiumFunderRequestID] TO [sp_executeall]
GO
