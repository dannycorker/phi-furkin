SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-17
-- Description:	AI Dashboard - Document zips
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard_DocumentZips]
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SummaryOnly = 1 
	BEGIN
	
		SELECT 
		CASE 
			/* DZI claims to be complete but is not */
			WHEN EXISTS (
				SELECT * 
				FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
				INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
				WHERE dzi.StatusID = 31 
				AND dz.StatusID <> 31
			) THEN 'Red' 
			
			/* DZI has a bad status */
			WHEN EXISTS (
				SELECT * 
				FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
				WHERE dzi.StatusID IN (32, 33, 34)
			) THEN 'Amber' 
			
			/* Been hanging around for more than an hour */
			WHEN EXISTS (
				SELECT * 
				FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
				INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
				WHERE dzi.StatusID <> 31 
				AND dzi.WhenStored < DATEADD(hh, -1, dbo.fn_GetDate_Local())
			) THEN 'Amber' 
			
			ELSE 'Green' 
		END AS [Status] 
		
	END
	ELSE
	BEGIN
		
		/* Document zips */	
		;WITH ZipsOfInterest AS 
		(
			SELECT dzi.DocumentZipInformationID 
			FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
			WHERE 
			(
				(dzi.StatusID <> 31) /* Not complete */
				OR
				(dzi.WhenStored > DATEADD(hh, -1, dbo.fn_GetDate_Local())) /* Very recent */
				OR
				(dzi.StatusID = 31 /* Allegedly complete but with flaws */
					AND EXISTS	(
									SELECT * 
									FROM dbo.DocumentZip dz WITH (NOLOCK) 
									WHERE dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
									AND dz.StatusID <> 31
								)
				)
			)
		),
		ZipsByStatus AS 
		(
			SELECT dz.DocumentZipInformationID, dz.StatusID, COUNT(*) as CountByStatus 
			FROM ZipsOfInterest zoi  
			INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = zoi.DocumentZipInformationID 
			GROUP BY dz.DocumentZipInformationID, dz.StatusID
		)
		SELECT dzi.DocumentZipInformationID AS [DZID],
		CASE 
			WHEN COALESCE(zbs32.CountByStatus, 0) > 0 THEN 'Red' 
			WHEN COALESCE(zbs33.CountByStatus, 0) > 0 THEN 'Red' 
			WHEN COALESCE(zbs34.CountByStatus, 0) > 0 THEN 'Red' 
			WHEN dzi.StatusID = 31 AND COALESCE(zbs35.CountByStatus, 0) > 0 THEN 'Red' 
			WHEN dzi.StatusID <> 31 AND (dzi.WhenStored IS NULL OR dzi.WhenStored < DATEADD(hh, -1, dbo.fn_GetDate_Local())) THEN 'Amber' 
			ELSE 'Green' 
		END AS [DziStatus],
		dzi.ClientID AS [CID],
		dzi.DocumentsPerZip AS [DPZ],
		dzi.ZipPrefix,
		dzi.StatusID,
		ao.AquariumOptionName,
		cp.UserName AS [WhoStored],
		dzi.WhenStored,
		zbs31.CountByStatus AS [Success],
		zbs35.CountByStatus AS [Waiting],
		zbs37.CountByStatus AS [Processing],
		zbs34.CountByStatus AS [Failed],
		zbs32.CountByStatus AS [Fatal],
		zbs33.CountByStatus AS [Other],
		CASE	WHEN zbs32.CountByStatus is not null THEN 'exec DocumentZipInformation__SplitOutFailedZips ' + CONVERT(varchar,dzi.DocumentZipInformationID) 
				WHEN (dzi.StatusID in(32,34) or zbs35.CountByStatus is not null)  THEN 'exec DocumentZip__KillAndCure ' + CONVERT(varchar,dzi.DocumentZipInformationID) 
				WHEN COALESCE(zbs31.CountByStatus,zbs35.CountByStatus,zbs37.CountByStatus,zbs34.CountByStatus,zbs32.CountByStatus,zbs33.CountByStatus) is null THEN 'exec DocumentZipInformation__Annihilate ' + CONVERT(varchar,dzi.DocumentZipInformationID) 
				ELSE '' END AS [Proc to Run] /*Case statement Added by CS - no point K&C if there are none waiting*/ 
		FROM ZipsOfInterest zoi WITH (NOLOCK) 
		INNER JOIN dbo.DocumentZipInformation dzi WITH (NOLOCK) ON dzi.DocumentZipInformationID = zoi.DocumentZipInformationID 
		INNER JOIN dbo.AquariumOption ao WITH (NOLOCK) ON ao.AquariumOptionID = dzi.StatusID 
		LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = dzi.WhoStored 
		LEFT JOIN ZipsByStatus zbs31 ON zbs31.DocumentZipInformationID = dzi.DocumentZipInformationID AND zbs31.StatusID = 31 
		LEFT JOIN ZipsByStatus zbs32 ON zbs32.DocumentZipInformationID = dzi.DocumentZipInformationID AND zbs32.StatusID = 32 
		LEFT JOIN ZipsByStatus zbs33 ON zbs33.DocumentZipInformationID = dzi.DocumentZipInformationID AND zbs33.StatusID IN (33, 36, 38)
		LEFT JOIN ZipsByStatus zbs34 ON zbs34.DocumentZipInformationID = dzi.DocumentZipInformationID AND zbs34.StatusID = 34 
		LEFT JOIN ZipsByStatus zbs35 ON zbs35.DocumentZipInformationID = dzi.DocumentZipInformationID AND zbs35.StatusID = 35 
		LEFT JOIN ZipsByStatus zbs37 ON zbs37.DocumentZipInformationID = dzi.DocumentZipInformationID AND zbs37.StatusID = 37 
		ORDER BY dzi.StatusID, dzi.WhenStored
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_DocumentZips] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard_DocumentZips] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_DocumentZips] TO [sp_executeall]
GO
