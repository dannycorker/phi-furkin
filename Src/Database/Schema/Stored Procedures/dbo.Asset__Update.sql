SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-02
-- Description:	Update Asset from Asset table
-- =============================================
CREATE PROCEDURE [dbo].[Asset__Update] 
	@ClientPersonnelID INT = NULL,
	@AssetID INT = NULL,
	@ClientID INT = NULL,
	@AssetTypeID INT = NULL,
	@AssetSubTypeID INT = NULL,
	@LocationID INT = NULL,
	@AssetName VARCHAR(255) = NULL,
	@Note VARCHAR(255) = NULL,
	@SecureNote VARCHAR(255) = NULL,
	@SecureUserName VARCHAR(255) = NULL,
	@SecurePassword VARCHAR(255) = NULL,
	@SecureOther VARCHAR(255) = NULL,
	@Version VARCHAR(255) = NULL,
	@ValidFrom DATETIME = NULL,
	@ValidTo DATETIME = NULL,
	@Enabled BIT = NULL,
	@Deleted BIT = NULL,
	@ReminderTimeUnitID INT = NULL,
	@ReminderTimeUnitQuantity INT = NULL,
	@ReminderNotificationGroupID INT = NULL
	-- @WhoCreated = No change
	-- @WhenCreated = No change
	-- @WhoModified = ClientPersonnelID,
	-- @WhenModified = Current DATETIME
	
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE a
	SET
		[ClientID] = @ClientID, 
		[AssetTypeID] = @AssetTypeID, 
		[AssetSubTypeID] = @AssetSubTypeID, 
		[LocationID] = @LocationID, 
		[AssetName] = @AssetName, 
		[Note] = @Note,
		[SecureNote] = @SecureNote, 
		[SecureUserName] = @SecureUserName, 
		[SecurePassword] = @SecurePassword,
		[SecureOther] = @SecureOther, 
		[Version] = @Version, 
		[ValidFrom] = @ValidFrom, 
		[ValidTo] = @ValidTo,
		[Enabled] = @Enabled, 
		[Deleted] = @Deleted,
		[ReminderTimeUnitID] = @ReminderTimeUnitID,
		[ReminderTimeUnitQuantity] = @ReminderTimeUnitQuantity,
		[ReminderNotificationGroupID] = @ReminderNotificationGroupID,
		[WhoModified] = @ClientPersonnelID,
		[WhenModified] = dbo.fn_GetDate_Local()
	FROM [dbo].[Asset] AS a
	INNER JOIN [ClientPersonnel] AS cp
		ON cp.[ClientID] = a.ClientID
	WHERE
		[AssetID] = @AssetID
		AND @ClientPersonnelID = cp.ClientPersonnelID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset__Update] TO [sp_executeall]
GO
