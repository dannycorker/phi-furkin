SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-10-15
-- Description:	Create default table rows according to a lookup list.  One row per item in the row.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PrePopulateTableFieldFromLookupList]
(
	@TableDetailFieldID INT,
	@LookupListColumnDetailFieldID INT,
	@ObjectID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE	@ClientID INT,
			@DetailFieldSubTypeID INT,
			@ClientPersonnelID INT,
			@CustomerID INT,
			@LeadID INT,
			@CaseID INT,
			@MatterID INT,
			@ErrorMessage VARCHAR(2000),
			@RowCount	INT = 0,
			@TableDetailFieldPageID INT
			
	SELECT	@ClientID				= df.ClientID,
			@DetailFieldSubtypeID	= df.LeadOrMatter,
			@TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableDetailFieldID

	IF @DetailFieldSubTypeID = 13 /*ClientPersonnel*/
	BEGIN
		SELECT	 @ClientPersonnelID = cp.ClientPersonnelID
				,@CustomerID		= null
				,@LeadID			= null
				,@CaseID			= null
				,@MatterID			= null
		FROM ClientPersonnel cp WITH (NOLOCK) 
		WHERE cp.ClientID = @ClientID
		and cp.ClientPersonnelID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 10 /*Customer*/
	BEGIN
		SELECT	 @ClientPersonnelID = null
				,@CustomerID		= cu.CustomerID
				,@LeadID			= null
				,@CaseID			= null
				,@MatterID			= null
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.ClientID = @ClientID
		and cu.CustomerID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 1 /*Lead*/
	BEGIN
		SELECT	 @ClientPersonnelID = null
				,@CustomerID		= null
				,@LeadID			= l.LeadID
				,@CaseID			= null
				,@MatterID			= null
		FROM Lead l WITH (NOLOCK) 
		WHERE l.ClientID = @ClientID
		and l.LeadID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 11 /*Case*/
	BEGIN
		SELECT	 @ClientPersonnelID = null
				,@CustomerID		= null
				,@LeadID			= null
				,@CaseID			= ca.CaseID
				,@MatterID			= null
		FROM Cases ca WITH (NOLOCK) 
		WHERE ca.ClientID = @ClientID
		and ca.CaseID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 2 /*Matter*/
	BEGIN
		SELECT	 @ClientPersonnelID = null
				,@CustomerID		= null
				,@LeadID			= m.LeadID /*Matter-level table rows include both lead and matter IDs*/
				,@CaseID			= null
				,@MatterID			= m.MatterID
		FROM Matter m WITH (NOLOCK) 
		WHERE m.ClientID = @ClientID
		and m.MatterID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 12 /*Client*/
	BEGIN
		SELECT	 @ClientPersonnelID = NULL
				,@CustomerID		= NULL
				,@LeadID			= NULL
				,@CaseID			= NULL
				,@MatterID			= NULL
	END
	ELSE 
	BEGIN
		SELECT @ErrorMessage = 'Error creating table rows.  No detail field matched for DetailFieldID ' + ISNULL(convert(varchar,@TableDetailFieldID),'null') + ', ClientID ' + ISNULL(convert(varchar,@ClientID),'null')
	END

	/*Check that the object(s) that we have mapped are accurate*/	
	IF COALESCE ( @ClientPersonnelID, @CustomerID, @LeadID, @CaseID, @MatterID, 0 ) = 0 /*If they are all null, the object ID must be wrong*/
	BEGIN
		SELECT @ErrorMessage = 'Error creating table rows.  ObjectID ' + ISNULL(convert(varchar,@ObjectID),'null') + ' is invalid for TableField of subtype ' + ISNULL(convert(varchar,@DetailFieldSubTypeID),'null')+ ', ClientID ' + ISNULL(convert(varchar,@ClientID),'null')
	END

	/*Check that the lookup list detail field is actually a column in this table*/
	IF NOT EXISTS 
	(
	SELECT *
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN DetailFields df_col WITH (NOLOCK) on df_col.DetailFieldPageID = df.TableDetailFieldPageID 
	WHERE df.DetailFieldID = @TableDetailFieldID
	and df_col.QuestionTypeID = 4
	and df_col.DetailFieldID = @LookupListColumnDetailFieldID
	)
	BEGIN
		SELECT @ErrorMessage = 'Error creating table rows.  DetailFieldID ' + ISNULL(convert(varchar,@LookupListColumnDetailFieldID),'null') + ' is not a LookupList column in Table Field ' + ISNULL(convert(varchar,@TableDetailFieldID),'null')
	END
	
	/*Create the tablerows where they don't already exist*/
	DECLARE @LookupListTableRowIDs TABLE (LookupListItemID INT, TableRowID INT)

	INSERT @LookupListTableRowIDs (LookupListItemID, TableRowID)
	SELECT li.LookupListItemID, 0
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN LookupListItems li WITH (NOLOCK) on li.LookupListID = df.LookupListID
	WHERE df.DetailFieldID = @LookupListColumnDetailFieldID

	/*Create all the rows*/
	INSERT TableRows ( ClientID, CustomerID, LeadID, CaseID, MatterID, ClientPersonnelID, DetailFieldID, SourceID )
	SELECT @ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientPersonnelID, @TableDetailFieldID, lr.LookupListItemID
	FROM @LookupListTableRowIDs lr 
	INNER JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = lr.LookupListItemID
	WHERE not exists(	SELECT * 
						FROM TableRows tr WITH (NOLOCK) 
						INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @LookupListColumnDetailFieldID
						WHERE tr.DetailFieldID = @TableDetailFieldID 
						and tdv.ValueInt = lr.LookupListItemID
						and	
						(
							( @DetailFieldSubTypeID = 10 AND tr.CustomerID		  = @CustomerID )
							or
							( @DetailFieldSubTypeID = 1  AND tr.LeadID			  = @LeadID )
							or
							( @DetailFieldSubTypeID = 2  AND tr.MatterID		  = @MatterID )
							or
							( @DetailFieldSubTypeID = 12 AND tr.ClientID		  = @ClientID )
							or
							( @DetailFieldSubTypeID = 11 AND tr.CaseID			  = @CaseID )
							or
							( @DetailFieldSubTypeID = 13 AND tr.ClientPersonnelID = @ClientPersonnelID )
						)
					)
	ORDER BY li.SortOrder, li.ItemValue
	SELECT @RowCount = @@ROWCOUNT
	
	/*Populate the table detail values*/
	INSERT TableDetailValues ( ClientID, CustomerID, LeadID, CaseID, MatterID, ClientPersonnelID, TableRowID, DetailFieldID, DetailValue )
	SELECT	tr.ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientPersonnelID, tr.TableRowID, df.DetailFieldID, CASE WHEN df.DetailFieldID = @LookupListColumnDetailFieldID THEN CONVERT(VARCHAR,tr.SourceID) ELSE '' END
	FROM TableRows tr WITH (NOLOCK)
	CROSS JOIN DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID
	and tr.DetailFieldID = @TableDetailFieldID
	and not exists ( SELECT * 
	                 FROM TableDetailValues tdv WITH (NOLOCK) 
	                 WHERE tdv.TableRowID = tr.TableRowID 
	                 and tdv.DetailFieldID = df.DetailFieldID )
	and	
	(
		( @DetailFieldSubTypeID = 10 AND tr.CustomerID		  = @CustomerID )
		or
		( @DetailFieldSubTypeID = 1  AND tr.LeadID			  = @LeadID )
		or
		( @DetailFieldSubTypeID = 2  AND tr.MatterID		  = @MatterID )
		or
		( @DetailFieldSubTypeID = 12 AND tr.ClientID		  = @ClientID )
		or
		( @DetailFieldSubTypeID = 11 AND tr.CaseID			  = @CaseID )
		or
		( @DetailFieldSubTypeID = 13 AND tr.ClientPersonnelID = @ClientPersonnelID )
	)	
	
	/*Return the number of rows created*/	
	RETURN @RowCount
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PrePopulateTableFieldFromLookupList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PrePopulateTableFieldFromLookupList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PrePopulateTableFieldFromLookupList] TO [sp_executeall]
GO
