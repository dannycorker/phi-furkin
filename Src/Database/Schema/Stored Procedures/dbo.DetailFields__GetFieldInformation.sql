SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 19-01-2010
-- Description:	Gets the detail field page caption, 
-- field caption, detail field id and its field alias if one exists
-- the information is ordered by page caption, field caption
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetFieldInformation]
	@detailFieldSubTypeID int, 
	@leadTypeID int, 
	@clientID int
AS
BEGIN
	
	SELECT dfp.PageCaption, df.FieldCaption, df.DetailFieldID, dfa.DetailFieldAlias, df.LeadOrMatter, df.LeadTypeID, df.QuestionTypeID
	FROM dbo.fnDetailFieldsShared(@ClientID) df
	INNER JOIN DetailFieldPages dfp WITH (NOLOCK) on dfp.DetailFieldPageID = df.DetailFieldPageID 
	LEFT OUTER JOIN DetailFieldAlias dfa WITH (NOLOCK) on dfa.DetailFieldID = df.DetailFieldID 
	WHERE df.ClientID = @clientID AND 
	      df.Enabled = 1 AND 
	      df.LeadOrMatter = @detailFieldSubTypeID AND
	      df.LeadTypeID = @LeadTypeID
	ORDER BY  dfp.PageCaption, df.FieldCaption ASC 

END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetFieldInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetFieldInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetFieldInformation] TO [sp_executeall]
GO
