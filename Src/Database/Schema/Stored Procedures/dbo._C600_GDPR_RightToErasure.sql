SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2019-09-12
-- Description:	C600 GDPR Right to Erasure / 'the right to be forgotten'. Triggered by the Approve Obfuscation event. 
-- 2019-10-09 CR #60175 - Defect raised to stop the Account no and Sort code from being obfuscated as there is a requirement to hold this information
-- 2019-10-23 GPR added update to obfuscate 'Change Address - Address Picke' (CustomerDetailValues - Internal) for #60432 / Defect #160
-- 2019-11-12 GPR updated for #60720 / Defect #163
-- 2019-12-06 LB Fixed ambigious column issue #61079
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GDPR_RightToErasure]
	@CustomerID INT,
	@LeadEventID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	/* STATIC OBFUSCATION VALUES
		
		The obfuscation procedure will replace the field values detailed within CR00029 Right to Erasure v0)1.docx with the following values:
		
		-----------------------------------------
		|	DATA TYPE	|	REPLACEMENT VALUE	|
		-----------------------------------------
		|	Date		|	NULL				|		
		-----------------------------------------
		|	Money		|	NULL				|		
		-----------------------------------------
		|	Text		|	XXXXX				|		
		-----------------------------------------
		|	Decimal		|	NULL				|		
		-----------------------------------------
		|	Int			|	NULL				|		
		-----------------------------------------
		|	XML			|	<deleted></deleted>	|		
		-----------------------------------------
	
	*/
	
	DECLARE		@ClientID				INT = dbo.fnGetPrimaryClientID() -- ClientID for System
			,	@ObfuscationComplete	BIT
			,	@Date					DATE = '1900-01-01'
			,	@Money					MONEY = NULL
			,	@Text					VARCHAR(10) = 'XXXXX'
			,	@Decimal				DECIMAL(18,2) = NULL
			,	@Int					INT = NULL
			,	@Comments				VARCHAR(20) = 'Obfuscated for GDPR'
			,	@CaseID					INT
			,	@AccountNumber			VARCHAR(8)
			,	@SortCode				VARCHAR(6)
			,	@DBName					VARCHAR(50) = 'Aquarius603Dev'
			,	@ErrorMessage			VARCHAR(1000)

	/*ClientID CHECK*/
	IF @ClientID NOT BETWEEN 600 AND 700
	BEGIN
		SELECT @ErrorMessage = '<font color="red"></br></br>FAILURE: This script cannot be executed against on your Client. Please contact Support.</br></font><font color="#edf4fa">'

		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN -1
	END

	/*DBNAME CHECK*/
	IF DB_NAME() <> @DBName
	BEGIN
		SELECT @ErrorMessage = '<font color="red"></br></br>FAILURE: This script cannot be executed against your Database. Please contact Support.</br></font><font color="#edf4fa">'

		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN -1
	END
	
	/*Gather all part IDs for this Customer*/
	DECLARE @IDs TABLE (LeadID INT, LeadTypeID INT, CaseID INT, MatterID INT)

	INSERT INTO @IDs (LeadID, LeadTypeID, CaseID, MatterID)
	SELECT Lead.LeadID, Lead.LeadTypeID, Cases.CaseID, Matter.MatterID
	FROM Lead Lead WITH (NOLOCK)
	INNER JOIN Cases Cases WITH (NOLOCK) ON Lead.LeadID = Cases.LeadID
	INNER JOIN Matter Matter WITH (NOLOCK) ON Matter.CaseID = Cases.CaseID
	WHERE Lead.CustomerID = @CustomerID
	
	/*CUSTOMER*/
		UPDATE	Customers 
			SET	TitleID = 0, /*Unknown - GPR 2019-09-25*/
				FirstName = @Text,
				LastName = @Text, 
				Address1 = @Text, 
				Address2 = @Text,
				Town = @Text,
				County = @Text,
				PostCode = @Text,
				Comments = @Comments,
				EmailAddress = @Text,
				DayTimeTelephoneNumber = @Int,
				CompanyTelephone = @Int,
				HomeTelephone = @Int,
				MobileTelephone = @Int, 
				WorksTelephone = @Int, 
				DateOfBirth = @Date
		WHERE Customers.CustomerID = @CustomerID
		AND Customers.ClientID = @ClientID

		/*CUSTOMER HISTORY*/
		UPDATE	CustomerHistory 
			SET		TitleID = 0, /*Unknown - GPR 2019-09-25*/
					FirstName = @Text,
					LastName = @Text, 
					Address1 = @Text, 
					Address2 = @Text,
					Town = @Text,
					County = @Text,
					PostCode = @Text,
					Comments = @Comments,
					EmailAddress = @Text,
					DayTimeTelephoneNumber = @Int,
					CompanyTelephone = @Int,
					HomeTelephone = @Int,
					MobileTelephone = @Int, 
					WorksTelephone = @Int, 
					DateOfBirth = @Date
			WHERE CustomerHistory.CustomerID = @CustomerID
			AND CustomerHistory.ClientID = @ClientID

/*Data Classification_Aquarium 1*/
	
		/*Claim - MatterDetailValues - Payments*/
		
			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (157808 /*Pay TP First Name*/, 157809 /*Pay TP Surname*/, 157810 /*Pay TP Address 1*/, 157811 /*Pay TP Address 2*/, 157812 /*Pay TP Address 3*/, 157813 /*Pay TP Address 4*/, 157814 /*Pay TP Address 5*/, 157815 /*Pay TP Postcode*/, 157816 /*Pay TP Country*/, 157873 /*Pay TP Title*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (157808 /*Pay TP First Name*/, 157809 /*Pay TP Surname*/, 157810 /*Pay TP Address 1*/, 157811 /*Pay TP Address 2*/, 157812 /*Pay TP Address 3*/, 157813 /*Pay TP Address 4*/, 157814 /*Pay TP Address 5*/, 157815 /*Pay TP Postcode*/, 157816 /*Pay TP Country*/, 157873 /*Pay TP Title*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Policy Admin - LeadDetailValues - Pet Details*/

			UPDATE LeadDetailValues
				SET DetailValue = @Text WHERE DetailFieldID = 170030 --Microchip number
			AND EXISTS (SELECT id.LeadID FROM @IDs id WHERE id.LeadID = LeadDetailValues.LeadID)
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID = 170030 --Microchip number
			AND EXISTS (SELECT id.LeadID FROM @IDs id WHERE id.LeadID = DetailValueHistory.LeadID)
			AND ClientID = @ClientID

		/*Policy Admin - MatterDetailValues - Billing*/
			
			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (170104 /*Account Name Billing*//*, 170105 /*Account Sort Code*/, 170106 /*Account Number*/*/, 170170 /*Bank Name*/, 170172 /*Credit Card Holder Name*/)  --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (170104 /*Account Name Billing*//*, 170105 /*Account Sort Code*/, 170106 /*Account Number*/*/, 170170 /*Bank Name*/, 170172 /*Credit Card Holder Name*/)  --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Collections - MatterDetailValues - DD Collections*/
			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (170188 /*Account Name*//*, 170189 /*Account Sort Code*/, 170190 /*Account Number*/*/)   --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (170188 /*Account Name*//*, 170189 /*Account Sort Code*/, 170190 /*Account Number*/*/)   --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Collections - MatterDetailValues - CP Collections*/
			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (170193 /*Card Identifier*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (170193 /*Card Identifier*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

			UPDATE MatterDetailValues
				SET DetailValue = @Date WHERE DetailFieldID IN (170194 /*Credit Card Expire Date*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Date WHERE DetailFieldID IN (170194 /*Credit Card Expire Date*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Claim - MatterDetailValues - Payments*/

			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (/*177495 /*Sort Code*/, */ 175296 /*Existing Bank Details*/)   --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (/*177495 /*Sort Code*/,*/ 175296 /*Existing Bank Details*/)     --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Collections - MatterDetailValues - Billing system helper fields*/

			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (177223 /*CardT_ReferenceNumber*/, 177224 /*CardT_Description*/,	177229 /*CardT_CardName*/, 177230 /*CardT_Number*/,177231 /*CardT_ExpiryMonth*/,177232 /*CardT_ExpiryYear*/, 177233 /*CardT_Code*/, 177234 /*CardT_AuthCode*/, 177235 /*CardT_AvsCode*/, 177243 /*CardT_BankAccountType*/, 177244 /*CardT_BankCode*/, 177245 /*CardT_BankName*/,	177247 /*CardT_GatewayCustomerID*/,	177248 /*CardT_TransactionID*/,	177249 /*CardT_TransactionTypeID*/, 177250 /*CardT_Email*/,	177251 /*CardT_Fax*/,	177252 /*CardT_Phone*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (177223 /*CardT_ReferenceNumber*/, 177224 /*CardT_Description*/,	177229 /*CardT_CardName*/, 177230 /*CardT_Number*/,177231 /*CardT_ExpiryMonth*/,177232 /*CardT_ExpiryYear*/, 177233 /*CardT_Code*/, 177234 /*CardT_AuthCode*/, 177235 /*CardT_AvsCode*/, 177243 /*CardT_BankAccountType*/, 177244 /*CardT_BankCode*/, 177245 /*CardT_BankName*/,	177247 /*CardT_GatewayCustomerID*/,	177248 /*CardT_TransactionID*/,	177249 /*CardT_TransactionTypeID*/, 177250 /*CardT_Email*/,	177251 /*CardT_Fax*/,	177252 /*CardT_Phone*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID
				
		/*Collections - MatterDetailValues - Billing*/

			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID = 177393 /*Credit Card Type*/
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID = 177393 /*Credit Card Type*/
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Collections - MatterDetailValues - Process Control*/

			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (177446 /*New Account Name*//*, 175363 /*New Sort Code*/, 175382 /*New Account Number*/*/)    --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (177446 /*New Account Name*//*, 175363 /*New Sort Code*/, 175382 /*New Account Number*/*/)     --2019-10-09 CR #60175
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

		/*Claim - MatterDetailValues - Third Party Information*/

			UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (179761 /*Enter Claimant Name*/, 179762 /*Enter Claimant Address*/, 179763 /*Enter Claimant Contact Number*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

			UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (179761 /*Enter Claimant Name*/, 179762 /*Enter Claimant Address*/, 179763 /*Enter Claimant Contact Number*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)	
			AND ClientID = @ClientID

/*Data Classification_Aquarium 2: Correspondence Notes*/

			UPDATE LeadEvent SET Comments = @Comments WHERE ISNULL(Comments, '') <> ''
			AND LeadEvent.CaseID IN (SELECT CaseID FROM @IDs)

		/*Other correspondence: EmailIn/Out, LetterIn/Out*/
			
			/*GPR 2019-09-17 replaced direct deletes with replace for Email documents to avoid 'Your request was malformed' error when previewing email in the EventHistory*/
			DECLARE @DocumentLeadEvent TABLE (LeadDocumentID INT, SubTypeID INT)
			INSERT INTO @DocumentLeadEvent (LeadDocumentID, SubTypeID)
			SELECT LeadDocumentID, et.EventSubTypeID FROM LeadEvent WITH (NOLOCK)
			INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = LeadEvent.EventTypeID
			WHERE LeadDocumentID IS NOT NULL
			AND LeadEvent.CaseID IN (SELECT CaseID FROM @IDs)

			UPDATE LeadEvent
			SET LeadDocumentID = @Int
			WHERE LeadEvent.LeadDocumentID IN (SELECT LeadDocumentID FROM @DocumentLeadEvent WHERE SubTypeID NOT IN (5,6) /*Not EmailIn/Out*/)

			DELETE FROM LeadDocument
			WHERE LeadDocument.LeadDocumentID IN (SELECT LeadDocumentID FROM @DocumentLeadEvent WHERE SubTypeID NOT IN (5,6) /*Not EmailIn/Out*/)

			DELETE FROM LeadDocumentFS
			WHERE LeadDocumentFS.LeadDocumentID IN (SELECT LeadDocumentID FROM @DocumentLeadEvent WHERE SubTypeID NOT IN (5,6) /*Not EmailIn/Out*/)

			UPDATE LeadDocumentFS
			SET EmailBlob = 0x47445052205B4445562054455354494E475D /*GDPR Default*/, EmailTo = NULL, EmailFrom = NULL /*GPR 2019-10-09 C600 Defect #157*/
            WHERE LeadDocumentFS.LeadDocumentID IN (SELECT LeadDocumentID FROM @DocumentLeadEvent WHERE SubTypeID IN (5,6) /*EmailIn/Out*/) 

			 UPDATE LeadDocument
            SET EmailTo = NULL, EmailFrom = NULL /*GPR 2019-10-09 C600 Defect #157*/
            WHERE LeadDocument.LeadDocumentID IN (SELECT LeadDocumentID FROM @DocumentLeadEvent WHERE SubTypeID IN (5,6) /*EmailIn/Out*/) 


/*Data Classification_Aquarium 3*/

		/*Data Classification_Aquarium 3: DPA Information (Authorities): Name, Password*/
	
			DECLARE @Authorities TABLE (TableRowID INT)
			INSERT INTO @Authorities (TableRowID)
			SELECT TableRowID FROM TableRows WITH (NOLOCK)
			WHERE DetailFieldID = 178263 /*Authorities*/
			AND TableRows.CustomerID = @CustomerID

			UPDATE TableDetailValues
				SET DetailValue = @Text
			WHERE DetailFieldID IN (178259 /*Name*/, 178286 /*Password*/)
			AND EXISTS (SELECT authorities.TableRowID FROM @Authorities authorities WHERE authorities.TableRowID = TableDetailValues.TableRowID)

			UPDATE TableDetailValuesHistory
				SET DetailValue = @Text
			WHERE DetailFieldID IN (178259 /*Name*/, 178286 /*Password*/)
			AND EXISTS (SELECT authorities.TableRowID FROM @Authorities authorities WHERE authorities.TableRowID = TableDetailValuesHistory.TableRowID)

		/*Data Classification_Aquarium 3: DPA Checklist: Name of the person accessing account, Policy Holder Name, Additional Contact Name, Address Line 1, Policy Holder Date of Birth, Password Used*/

			UPDATE CustomerDetailValues
				SET	DetailValue = @Text
			WHERE DetailFieldID = 178266 -- Name of the person accessing account
			AND CustomerDetailValues.CustomerID = @CustomerID

			UPDATE CustomerDetailValues
				SET DetailValue = @Text
			WHERE DetailFieldID IN (179076 /*Policy Holder Name*/, 178268 /*Additional Contact Name*/, 178267 /*Address Line 1*/, 178407 /*Policy Holder Date of Birth*/, 178408 /*Password Used*/)
			AND CustomerDetailValues.CustomerID = @CustomerID

			UPDATE DetailValueHistory
				SET FieldValue = @Text
			WHERE DetailFieldID IN (179076 /*Policy Holder Name*/, 178268 /*Additional Contact Name*/, 178267 /*Address Line 1*/, 178407 /*Policy Holder Date of Birth*/, 178408 /*Password Used*/)
			AND DetailValueHistory.CustomerID = @CustomerID

		/*Data Classification_Aquarium 3: DPA Audit History: Name on Call, Policy Holder Name, Additional Contact Name, Address Line 1, Policy Holder Date of Birth, Password*/

			DECLARE @DPAAutit TABLE (TableRowID INT)
			INSERT INTO @DPAAutit (TableRowID)
			SELECT TableRowID FROM TableRows WITH (NOLOCK)
			WHERE DetailFieldID = 178258 /*DPA Audit*/
			AND TableRows.CustomerID = @CustomerID

			UPDATE TableDetailValues
				SET DetailValue = @Text
			WHERE DetailFieldID IN (178252 /*Name on Call*/, 179078 /*Policy Holder Name*/, 178254 /*Additional Contact Name*/, 178272 /*Address Line 1*/, 179079 /*Password*/)
			AND EXISTS (SELECT dpaaudit.TableRowID FROM @DPAAutit dpaaudit WHERE dpaaudit.TableRowID = TableDetailValues.TableRowID)

			UPDATE TableDetailValues
				SET DetailValue = @Date
			WHERE DetailFieldID = 179077 -- Policy Holder Date of Birth
			AND EXISTS (SELECT dpaaudit.TableRowID FROM @DPAAutit dpaaudit WHERE dpaaudit.TableRowID = TableDetailValues.TableRowID)
	
			UPDATE TableDetailValuesHistory
				SET DetailValue = @Text
			WHERE DetailFieldID IN (178252 /*Name on Call*/, 179078 /*Policy Holder Name*/, 178254 /*Additional Contact Name*/, 178272 /*Address Line 1*/, 179079 /*Password*/)
			AND EXISTS (SELECT dpaaudit.TableRowID FROM @DPAAutit dpaaudit WHERE dpaaudit.TableRowID = TableDetailValuesHistory.TableRowID)

			UPDATE TableDetailValuesHistory
				SET DetailValue = @Date
			WHERE DetailFieldID = 179077 -- Policy Holder Date of Birth
			AND EXISTS (SELECT dpaaudit.TableRowID FROM @DPAAutit dpaaudit WHERE dpaaudit.TableRowID = TableDetailValuesHistory.TableRowID)

			DELETE FROM @DPAAutit;

		/*Data Classification_Aquarium 3: DDA Customer (DDA Selection): Customer's Medical Condition*/
	
			DECLARE @DPASelection TABLE (TableRowID INT)
			INSERT INTO @DPASelection (TableRowID)
			SELECT TableRowID FROM TableRows WITH (NOLOCK)
			WHERE DetailFieldID = 177670 /*DPA Selection*/
			AND TableRows.CustomerID = @CustomerID -- GPR 2019-10-24 removed hardcoded CustomerID
	
			UPDATE TableDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (179918 /*Deaf Blind or Other*/, 179919 /*Additional Information*/)
			AND EXISTS (SELECT dpaselection.TableRowID FROM @DPASelection dpaselection WHERE dpaselection.TableRowID = TableDetailValues.TableRowID)
		
			UPDATE TableDetailValues
				SET DetailValue = @Date WHERE DetailFieldID = 179920 -- Date to be reviewed
			AND EXISTS (SELECT dpaselection.TableRowID FROM @DPASelection dpaselection WHERE dpaselection.TableRowID = TableDetailValues.TableRowID)

			UPDATE TableDetailValuesHistory
				SET DetailValue = @Text WHERE DetailFieldID IN (179918 /*Deaf Blind or Other*/, 179919 /*Additional Information*/)
			AND EXISTS (SELECT dpaselection.TableRowID FROM @DPASelection dpaselection WHERE dpaselection.TableRowID = TableDetailValuesHistory.TableRowID)
		
			UPDATE TableDetailValuesHistory
				SET DetailValue = @Date WHERE DetailFieldID = 179920 -- Date to be reviewed
			AND EXISTS (SELECT dpaselection.TableRowID FROM @DPASelection dpaselection WHERE dpaselection.TableRowID = TableDetailValuesHistory.TableRowID)

			DELETE FROM @DPASelection;

		/*
			ADDITIONAL CUSTOMER DETAIL VALUES
			- Letter Address And Salutation - DF: 170110, 170111, 170112, 170113
			- Current Payment Details - DF: 175331
			- Current Policy Holder Details - DF: 175332
		*/

			UPDATE CustomerDetailValues
				SET	DetailValue = @Text
			WHERE DetailFieldID IN (170110, 170111, 170112, 170113, 175331,175332)
			AND CustomerDetailValues.CustomerID = @CustomerID

			UPDATE DetailValueHistory
				SET	FieldValue = @Text
			WHERE DetailFieldID IN (170110, 170111, 170112, 170113, 175331,175332)
			AND DetailValueHistory.CustomerID = @CustomerID


		/*Update Account Information*/
		UPDATE Account SET	FriendlyName = @Text, AccountHolderName = @Text
						/*AccountNumber = @Int,
						SortCode = @Int*/
				WHERE Account.CustomerID = @CustomerID

		/*GPR 2019-09-19 AccountHistory is not currently in use in the C600 implementation, added in to future-proof.*/
		UPDATE AccountHistory SET	FriendlyName = @Text, AccountHolderName = @Text
						/*AccountNumber = @Int,
						SortCode = @Int*/
				WHERE AccountHistory.CustomerID = @CustomerID

		/* GPR 2019-09-19 QuoteSessions / SavedQuote records for this Customer*/
			DECLARE @QuoteSessions TABLE (QuoteSessionID INT)
			INSERT INTO @QuoteSessions (QuoteSessionID)
			SELECT QuoteSessionID FROM _C600_QuoteSessions WITH (NOLOCK)
			WHERE CustomerID = @CustomerID

			UPDATE _C600_QuoteSessions
			SET	FirstName = @Text,
				LastName = @Text,
				Email = @Text,
				PostCode = @Text,
				BuyXML = '<deleted></deleted>'
			WHERE CustomerID = @CustomerID

			UPDATE _C600_SavedQuote
			SET EmailAddress = @Text,
				SecretQuestionValue = @Text,
				SavedQuoteXML = '<deleted></deleted>',
				PetQuoteDefinitionXML = '<deleted></deleted>'
			WHERE EXISTS (SELECT quotesessions.QuoteSessionID FROM @QuoteSessions quotesessions WHERE quotesessions.QuoteSessionID = _C600_SavedQuote.QuoteSessionID)
	
			UPDATE _C600_QuoteValues
			SET	FirstName = @Text,
				LastName = @Text,
				Email = @Text,
				PostCode = @Text,
				QuoteXML = '<deleted></deleted>',
				PetXML = '<deleted></deleted>',
				ResponseXML = '<deleted></deleted>'
			WHERE EXISTS (SELECT quotesessions.QuoteSessionID FROM @QuoteSessions quotesessions WHERE quotesessions.QuoteSessionID = _C600_QuoteValues.QuoteSessionID) /*#61079*/


	/*2019-10-23 GPR for #60432 / Defect #160 - 'Change Address - Address Picker'*/
	UPDATE CustomerDetailValues
	SET	DetailValue = @Text
	WHERE DetailFieldID IN (175478)
	AND CustomerDetailValues.CustomerID = @CustomerID


	/*2019-10-24 GPR for #60454 / Defect #161 - Claim Payments Page*/
	UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (157808 /*AccountNameToPay*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

	UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (157808 /*AccountNameToPay*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)
			AND FieldValue IS NOT NULL		
			AND ClientID = @ClientID

	/*2019-11-12 GPR for #60720 / Defect #163 - Claim Payments Page*/
	UPDATE MatterDetailValues
				SET DetailValue = @Text WHERE DetailFieldID IN (170264 /*AccountNameToPay*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = MatterDetailValues.MatterID)
			AND DetailValue IS NOT NULL		
			AND ClientID = @ClientID

	UPDATE DetailValueHistory
				SET FieldValue = @Text WHERE DetailFieldID IN (170264 /*AccountNameToPay*/)
			AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = DetailValueHistory.MatterID)
			AND FieldValue IS NOT NULL		
			AND ClientID = @ClientID

	
/*2019-10-24 GPR for #60454 / Defect #161 - Claim Payments Page - Payee*/
	DECLARE @ClaimPayments TABLE (TableRowID INT)
	INSERT INTO @ClaimPayments (TableRowID)
	SELECT TableRowID FROM TableRows WITH (NOLOCK)
	WHERE DetailFieldID = 154485 /*Payment Data*/
	AND EXISTS (SELECT id.MatterID FROM @IDs id WHERE id.MatterID = TableRows.MatterID)
	AND ClientID = @ClientID

	UPDATE TableDetailValues
				SET DetailValue = @Text
				WHERE DetailFieldID IN (154489 /*Title*/, 154487 /*FirstName*/, 154488 /*LastName*/, 154490 /*Organisation*/, 154491 /*Address1*/, 154492 /*Address2*/, 154493 /*Address3*/, 154494 /*Address4*/, 154495 /*Address5*/, 154496 /*PostCode*/)
				AND EXISTS (SELECT claimpayment.TableRowID FROM @ClaimPayments claimpayment WHERE claimpayment.TableRowID = TableDetailValues.TableRowID)

	UPDATE TableDetailValuesHistory
				SET DetailValue = @Text
				WHERE DetailFieldID IN (154489 /*Title*/, 154487 /*FirstName*/, 154488 /*LastName*/, 154490 /*Organisation*/, 154491 /*Address1*/, 154492 /*Address2*/, 154493 /*Address3*/, 154494 /*Address4*/, 154495 /*Address5*/, 154496 /*PostCode*/)
				AND EXISTS (SELECT claimpayment.TableRowID FROM @ClaimPayments claimpayment WHERE claimpayment.TableRowID = TableDetailValuesHistory.TableRowID)
	
	/*2019-10-24 GPR for #60454 / Defect #161 - Claim Payments Page - PolicyHolder*/
	UPDATE TableDetailValues
				SET DetailValue = @Text
				WHERE DetailFieldID IN (154501 /*FirstName*/, 154502 /*LastName*/, 154503 /*Organisation*/, 154504 /*Address1*/, 154505 /*Address2*/, 154506 /*Address3*/, 154507 /*Address4*/, 154508 /*Address5*/, 154509 /*PostCode*/)
				AND EXISTS (SELECT claimpayment.TableRowID FROM @ClaimPayments claimpayment WHERE claimpayment.TableRowID = TableDetailValues.TableRowID)

	UPDATE TableDetailValuesHistory
				SET DetailValue = @Text
				WHERE DetailFieldID IN (154501 /*FirstName*/, 154502 /*LastName*/, 154503 /*Organisation*/, 154504 /*Address1*/, 154505 /*Address2*/, 154506 /*Address3*/, 154507 /*Address4*/, 154508 /*Address5*/, 154509 /*PostCode*/)
				AND EXISTS (SELECT claimpayment.TableRowID FROM @ClaimPayments claimpayment WHERE claimpayment.TableRowID = TableDetailValuesHistory.TableRowID)




	/*Obfuscation Complete*/
	SET @ObfuscationComplete = 1

	IF (@ObfuscationComplete = 1)
	BEGIN
		
		/*GPR 2019-09-17 Apply the completed email to all PolicyAdmin Leads for this Customer, not just the one the process was started on/.*/
		WHILE EXISTS (SELECT CaseID FROM @IDs WHERE LeadTypeID = 1492 /*Policy Admin*/)
		BEGIN
		
			SELECT TOP(1) @CaseID = CaseID FROM @IDs WHERE LeadTypeID = 1492 /*Policy Admin*/

			SELECT @LeadEventID = LatestNonNoteLeadEventID FROM Cases Cases WITH (NOLOCK) WHERE Cases.CaseID = @CaseID
			/*Obfuscation Reporting - 'GDPR Obfuscation Complete'*/
			EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID, 158927 /*GDPR Obfuscation Complete*/, 58552 /*AQ Automation*/, 0
		
			DELETE TOP(1) FROM @IDs 
			WHERE CaseID = @CaseID
			AND LeadTypeID = 1492

		END

		EXEC [dbo].[_C00_LogIt] 'Info', 'GDPR', '_C600_GDPR_RightToErasure Complete',@CustomerID, @ClientPersonnelID

	END
	ELSE /*Obfuscation Incomplete - Rollback*/
	BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		/*Obfuscation Error Handling - Process Errors*/
		EXEC [dbo].[_C00_LogIt] 'Info', 'GDPR', '_C600_GDPR_RightToErasure Failed',@CustomerID, @ClientPersonnelID
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GDPR_RightToErasure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GDPR_RightToErasure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GDPR_RightToErasure] TO [sp_executeall]
GO
