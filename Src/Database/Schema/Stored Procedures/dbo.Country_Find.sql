SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Country table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_Find]
(

	@SearchUsingOR bit   = null ,

	@CountryID int   = null ,

	@CountryName varchar (255)  = null ,

	@CountryNamePlain varchar (255)  = null ,

	@Alpha2Code char (2)  = null ,

	@Alpha3Code char (3)  = null ,

	@NumericCode smallint   = null ,

	@ISO3166Dash2Code char (13)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null ,

	@UsePostcode bit   = null ,

	@PostcodeRegex varchar (500)  = null ,

	@DiallingCode varchar (4)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CountryID]
	, [CountryName]
	, [CountryNamePlain]
	, [Alpha2Code]
	, [Alpha3Code]
	, [NumericCode]
	, [ISO3166Dash2Code]
	, [WhenCreated]
	, [WhenModified]
	, [UsePostcode]
	, [PostcodeRegex]
	, [DiallingCode]
    FROM
	[dbo].[Country] WITH (NOLOCK) 
    WHERE 
	 ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([CountryName] = @CountryName OR @CountryName IS NULL)
	AND ([CountryNamePlain] = @CountryNamePlain OR @CountryNamePlain IS NULL)
	AND ([Alpha2Code] = @Alpha2Code OR @Alpha2Code IS NULL)
	AND ([Alpha3Code] = @Alpha3Code OR @Alpha3Code IS NULL)
	AND ([NumericCode] = @NumericCode OR @NumericCode IS NULL)
	AND ([ISO3166Dash2Code] = @ISO3166Dash2Code OR @ISO3166Dash2Code IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([UsePostcode] = @UsePostcode OR @UsePostcode IS NULL)
	AND ([PostcodeRegex] = @PostcodeRegex OR @PostcodeRegex IS NULL)
	AND ([DiallingCode] = @DiallingCode OR @DiallingCode IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CountryID]
	, [CountryName]
	, [CountryNamePlain]
	, [Alpha2Code]
	, [Alpha3Code]
	, [NumericCode]
	, [ISO3166Dash2Code]
	, [WhenCreated]
	, [WhenModified]
	, [UsePostcode]
	, [PostcodeRegex]
	, [DiallingCode]
    FROM
	[dbo].[Country] WITH (NOLOCK) 
    WHERE 
	 ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([CountryName] = @CountryName AND @CountryName is not null)
	OR ([CountryNamePlain] = @CountryNamePlain AND @CountryNamePlain is not null)
	OR ([Alpha2Code] = @Alpha2Code AND @Alpha2Code is not null)
	OR ([Alpha3Code] = @Alpha3Code AND @Alpha3Code is not null)
	OR ([NumericCode] = @NumericCode AND @NumericCode is not null)
	OR ([ISO3166Dash2Code] = @ISO3166Dash2Code AND @ISO3166Dash2Code is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([UsePostcode] = @UsePostcode AND @UsePostcode is not null)
	OR ([PostcodeRegex] = @PostcodeRegex AND @PostcodeRegex is not null)
	OR ([DiallingCode] = @DiallingCode AND @DiallingCode is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Find] TO [sp_executeall]
GO
