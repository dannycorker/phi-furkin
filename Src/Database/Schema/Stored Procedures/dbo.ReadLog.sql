SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-09-20
-- Description:	Read a Log entry from a LogID
--				CS 2015-12-16	Show surrounding logs to help diagnosis
-- =============================================
CREATE PROCEDURE [dbo].[ReadLog]
	@LogID int

AS
BEGIN
	SET NOCOUNT ON;

	Declare @LogEntry varchar(max)
	
	SELECT @LogEntry = l.LogEntry
	FROM Logs l WITH (NOLOCK) 
	WHERE l.LogID = @LogID
	
	SELECT cp.ClientPersonnelID,cp.UserName,l.LogDateTime,l.TypeOfLogEntry,l.ClassName,l.MethodName,l.LogEntry
	FROM Logs l WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = l.ClientPersonnelID
	WHERE l.LogID = @LogID
	
	/*CS 2015-12-16*/
	SELECT 'SurroundingLogs' [SurroundingLogs], l.LogID - @LogID [N], cp.ClientPersonnelID,cp.UserName,l.LogDateTime,l.TypeOfLogEntry,l.ClassName,l.MethodName,l.LogEntry
	FROM Logs l WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = l.ClientPersonnelID
	WHERE l.LogID BETWEEN @LogID - 5 AND @LogID + 5
	
	EXEC dbo.SplitString @LogEntry, 'text', ' at'

END



GO
GRANT VIEW DEFINITION ON  [dbo].[ReadLog] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReadLog] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReadLog] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[ReadLog] TO [sp_executehelper]
GO
