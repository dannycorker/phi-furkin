SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-30
-- Description:	Inserts or updates a rule parameter
-- Updates:		2014-15-05	SB	Removed the code clearing @Parent out as this is now relevant for other types e.g. table fields
--				2016-07-12	ID Back fill parameter values based on presets
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleParameters_InsertUpdateParameter]
(
	@RuleParameterID INT,
	@Name VARCHAR(200),
	@ParameterTypeID INT,
	@Value VARCHAR(50),
	@DataTypeID INT,
	@RuleID INT,
	@ClientID INT,
	@UserID INT,
	@Parent VARCHAR(50),
	@RuleParameterPreSetID INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	
	SELECT 
		@Name = Name,
		@ParameterTypeID = ParameterTypeID,
		@Value = Value,
		@DataTypeID = DataTypeID,
		@Parent = Parent
	FROM dbo.RulesEngine_RuleParameters_PreSet WITH (NOLOCK)
	WHERE RuleParameterPreSetID = @RuleParameterPreSetID

	IF @RuleParameterID > 0
	BEGIN
	
		UPDATE dbo.RulesEngine_RuleParameters
		SET Name = @Name,
			ParameterTypeID = @ParameterTypeID,
			Value = @Value,
			DataTypeID = @DataTypeID,
			Parent = @Parent,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID,
			RuleParameterPreSetID = @RuleParameterPreSetID
		WHERE RuleParameterID = @RuleParameterID
	
	END
	ELSE
	BEGIN
		
		INSERT INTO dbo.RulesEngine_RuleParameters (Name, RuleID, ParameterTypeID, Value, DataTypeID, Parent, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, RuleParameterPreSetID)
		VALUES (@Name, @RuleID, @ParameterTypeID, @Value, @DataTypeID, @Parent, @ClientID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @RuleParameterPreSetID)
		
		SELECT @RuleParameterID = SCOPE_IDENTITY()
	
	END

	EXEC dbo.RulesEngine_RuleParameters_GetParametersByRuleID @ClientID, @RuleID, @RuleParameterID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_InsertUpdateParameter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleParameters_InsertUpdateParameter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_InsertUpdateParameter] TO [sp_executeall]
GO
