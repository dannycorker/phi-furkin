SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyMappingTemplate table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingTemplate_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyMappingTemplateID int   = null ,

	@ThirdPartyFieldGroupID int   = null ,

	@ThirdPartySystemID int   = null ,

	@Name varchar (50)  = null ,

	@Description varchar (255)  = null ,

	@XMLItemTemplate varchar (MAX)  = null ,

	@XMLContainerName varchar (MAX)  = null ,

	@XMLItemName varchar (MAX)  = null ,

	@XMLItemKey varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyMappingTemplateID]
	, [ThirdPartyFieldGroupID]
	, [ThirdPartySystemID]
	, [Name]
	, [Description]
	, [XMLItemTemplate]
	, [XMLContainerName]
	, [XMLItemName]
	, [XMLItemKey]
    FROM
	[dbo].[ThirdPartyMappingTemplate] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingTemplateID] = @ThirdPartyMappingTemplateID OR @ThirdPartyMappingTemplateID IS NULL)
	AND ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID OR @ThirdPartyFieldGroupID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([XMLItemTemplate] = @XMLItemTemplate OR @XMLItemTemplate IS NULL)
	AND ([XMLContainerName] = @XMLContainerName OR @XMLContainerName IS NULL)
	AND ([XMLItemName] = @XMLItemName OR @XMLItemName IS NULL)
	AND ([XMLItemKey] = @XMLItemKey OR @XMLItemKey IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyMappingTemplateID]
	, [ThirdPartyFieldGroupID]
	, [ThirdPartySystemID]
	, [Name]
	, [Description]
	, [XMLItemTemplate]
	, [XMLContainerName]
	, [XMLItemName]
	, [XMLItemKey]
    FROM
	[dbo].[ThirdPartyMappingTemplate] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingTemplateID] = @ThirdPartyMappingTemplateID AND @ThirdPartyMappingTemplateID is not null)
	OR ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID AND @ThirdPartyFieldGroupID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([XMLItemTemplate] = @XMLItemTemplate AND @XMLItemTemplate is not null)
	OR ([XMLContainerName] = @XMLContainerName AND @XMLContainerName is not null)
	OR ([XMLItemName] = @XMLItemName AND @XMLItemName is not null)
	OR ([XMLItemKey] = @XMLItemKey AND @XMLItemKey is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingTemplate_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Find] TO [sp_executeall]
GO
