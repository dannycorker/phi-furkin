SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Gets a record(s) from DocumentZipInformation table where the ClientID = @ClientID
-- Date: 13/02/2009
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentZipInformation__GetByClientID]
(

	@ClientID int   
)
AS
BEGIN
	
	SELECT
		dzi.[DocumentZipInformationID],
		dzi.[ClientID],
		dzi.[ServerMapPath],
		dzi.[SecurePath],
		dzi.[HostPath],
		dzi.[WordTempDirectory],
		dzi.[DocumentsPerZip],
		dzi.[ZipPrefix],
		dzi.[StatusID],
		IsNull(dzi.[TriggeredByAutomatedTaskID], 0) AS [TriggeredByAutomatedTaskID],
		IsNull(dzi.[TriggeredByDocumentTypeID], 0) AS [TriggeredByDocumentTypeID],
		dzi.[WhoStored], 
		dzi.[WhenStored], 
		dzi.[WhenCreated], 
		dzi.[EmailOnSuccess], 
		dzi.[EmailOnError], 
		cp_stored.UserName as [WhoStoredUserName] 
	FROM
		[dbo].[DocumentZipInformation] dzi WITH (NOLOCK) 
		LEFT JOIN dbo.ClientPersonnel cp_stored WITH (NOLOCK) ON cp_stored.ClientPersonnelID = dzi.WhoStored 
	
	WHERE
		dzi.[ClientID] = @ClientID
	
	--Select @@ROWCOUNT
	SET ANSI_NULLS ON

END




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetByClientID] TO [sp_executeall]
GO
