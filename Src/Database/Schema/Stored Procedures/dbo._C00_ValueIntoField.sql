SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-20
-- Description:	Insert Fields finto MDV or LDV fields
-- JWG 2013-01-25 Allow user to pass in MatterID instead of LeadEventID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ValueIntoField]
(
	@LeadEventID INT,
	@DetailFieldID INT,
	@DetailValue VARCHAR(2000),
	@MatterID INT = NULL, 
	@UserID INT = NULL
)
	
AS
BEGIN

	SET NOCOUNT ON

	DECLARE	@LeadID INT,
	@LeadTypeID INT,
	@ClientID INT,
	@CaseID INT,
	@LeadOrMatter INT,
	@ClientPersonnelID INT,
	@MaintainHistory INT,
	@CustomerID INT
	
	IF @LeadEventID IS NULL AND @MatterID > 0
	BEGIN
		SELECT
		@LeadID = m.LeadID,
		@LeadTypeID = l.LeadTypeID,
		@ClientID = m.ClientID,
		@CaseID = m.CaseID,
		@ClientPersonnelID = @UserID,
		@CustomerID = m.CustomerID
		FROM dbo.Matter m WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID 
		AND m.MatterID = @MatterID
	END
	ELSE
	BEGIN
		SELECT
		@LeadID = l.LeadID,
		@LeadTypeID = l.LeadTypeID,
		@ClientID = l.ClientID,
		@CaseID = le.CaseID,
		@ClientPersonnelID = le.WhoCreated,
		@CustomerID = l.CustomerID
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID
		WHERE le.LeadEventID = @LeadEventID 
	END
	
	SELECT 
	@LeadOrMatter = LeadOrMatter, @MaintainHistory = MaintainHistory
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	/* New way of creating detail fields*/
	EXEC dbo._C00_CreateDetailFields @DetailFieldID,@LeadID

	IF @LeadOrMatter = 1
	BEGIN

		/* Insert Comments into Lead Feild*/
		UPDATE LeadDetailValues 
		SET DetailValue = ISNULL(@DetailValue,'')
		WHERE LeadID = @LeadID
		AND DetailFieldID = @DetailFieldID

		IF @MaintainHistory = 1
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, @DetailFieldID, @LeadOrMatter, @LeadID, NULL, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID

		END
	END

	IF @LeadOrMatter = 2
	BEGIN

		/* Insert Comments into Matter Feild*/
		UPDATE MatterDetailValues 
		SET DetailValue = ISNULL(@DetailValue,'')
		FROM MatterDetailValues
		INNER JOIN Matter ON Matter.MatterID = MatterDetailValues.MatterID AND Matter.CaseID = @CaseID
		WHERE MatterDetailValues.DetailFieldID = @DetailFieldID

		IF @MaintainHistory = 1
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, @DetailFieldID, @LeadOrMatter, @LeadID, MatterID, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM Matter
			WHERE Matter.CaseID = @CaseID

		END

	END

	IF @LeadOrMatter = 10
	BEGIN

		/* Insert Comments into Lead Feild*/
		UPDATE CustomerDetailValues
		SET DetailValue = ISNULL(@DetailValue,'')
		WHERE CustomerID = @CustomerID
		AND DetailFieldID = @DetailFieldID

		IF @MaintainHistory = 1
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, CustomerID, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, @DetailFieldID, @LeadOrMatter, @CustomerID, NULL, NULL, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID

		END
	END

	IF @LeadOrMatter = 11
	BEGIN

		/* Insert Comments into Case*/
		UPDATE CaseDetailValues
		SET DetailValue = ISNULL(@DetailValue,'')
		WHERE CaseID = @CaseID
		AND DetailFieldID = @DetailFieldID

		IF @MaintainHistory = 1
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, CaseID, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, @DetailFieldID, @LeadOrMatter, @CaseID, NULL, NULL, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID

		END
	END
	


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ValueIntoField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ValueIntoField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ValueIntoField] TO [sp_executeall]
GO
