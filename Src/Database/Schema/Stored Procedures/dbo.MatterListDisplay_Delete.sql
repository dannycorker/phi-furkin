SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the MatterListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_Delete]
(

	@MatterListDisplayID int   
)
AS


				DELETE FROM [dbo].[MatterListDisplay] WITH (ROWLOCK) 
				WHERE
					[MatterListDisplayID] = @MatterListDisplayID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Delete] TO [sp_executeall]
GO
