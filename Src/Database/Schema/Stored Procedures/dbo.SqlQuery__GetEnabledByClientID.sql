SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery__GetEnabledByClientID]
(
	@ClientID int   
)
AS

				SET ANSI_NULLS OFF
				
				SELECT *
					
				FROM
					[dbo].[SqlQuery] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
					AND [FolderID] not in(-1,4510) /*CS 2011-08-14 Hide some enbled reports from the portal - req of ES*/
					AND QueryID NOT IN (6513, 8004,5098) /* JWG 2010-02-23 Don't let LA2Plus pod users run these! */ /*CS added 5098 2011-08-14 req of Ed*/
 				ORDER BY 
					[QueryTitle] asc

				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetEnabledByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__GetEnabledByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetEnabledByClientID] TO [sp_executeall]
GO
