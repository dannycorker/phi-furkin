SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSetting table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSetting_GetByChartID]
(

	@ChartID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChartSettingID],
					[ChartID],
					[SettingName],
					[SettingValue],
					[ClientID]
				FROM
					[dbo].[ChartSetting] WITH (NOLOCK) 
				WHERE
					[ChartID] = @ChartID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_GetByChartID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSetting_GetByChartID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_GetByChartID] TO [sp_executeall]
GO
