SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddCustomerOutcome] 
@CustomerID int,
@OutcomeID int,
@ClientQuestionnaireID int,
@ClientID int

as

insert into CustomerOutcomes (CustomerID, OutcomeID,ClientQuestionnaireID, ClientID)
values (@CustomerID, @OutcomeID, @ClientQuestionnaireID, @ClientID )



GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerOutcome] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddCustomerOutcome] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerOutcome] TO [sp_executeall]
GO
