SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-08-20
-- Description:	Gets the third party mapping information for the RPI credential fields for the given client
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping__GetRPICredentialMap]
(
	@ClientID int,
	@LeadTypeID int   
)
AS

	SET ANSI_NULLS ON
	
	SELECT tpfm.ThirdPartyFieldMappingID, tpfm.ClientID, tpfm.LeadTypeID, tpfm.ThirdPartyFieldID,
	tpfm.DetailFieldID, df.FieldName, tpfm.ColumnFieldID, dfCol.FieldName ColumnFieldName, tpfm.ThirdPartyFieldGroupID, tpfm.IsEnabled, tpf.FieldName tpFieldName
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tpfm.DetailFieldID
	INNER JOIN dbo.ThirdPartyField tpf WITH (NOLOCK) ON tpf.ThirdPartyFieldID = tpfm.ThirdPartyFieldID
	LEFT OUTER JOIN dbo.DetailFields dfCol WITH (NOLOCK) ON dfCol.DetailFieldID = tpfm.ColumnFieldID
	WHERE tpfm.ThirdPartyFieldID in (322,323,324) AND tpfm.ClientID=@ClientID and tpfm.LeadTypeID=@LeadTypeID
		
				
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetRPICredentialMap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping__GetRPICredentialMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetRPICredentialMap] TO [sp_executeall]
GO
