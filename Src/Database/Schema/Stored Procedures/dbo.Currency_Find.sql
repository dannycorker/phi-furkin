SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Currency table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Currency_Find]
(

	@SearchUsingOR bit   = null ,

	@CurrencyID int   = null ,

	@CurrencyCode char (3)  = null ,

	@CurrencyName varchar (100)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null ,

	@MajorNamePlural varchar (50)  = null ,

	@MajorNameSingular varchar (50)  = null ,

	@MinorNamePlural varchar (50)  = null ,

	@MinorNameSingular varchar (50)  = null ,

	@CurrencySymbol nvarchar (2)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CurrencyID]
	, [CurrencyCode]
	, [CurrencyName]
	, [WhenCreated]
	, [WhenModified]
	, [MajorNamePlural]
	, [MajorNameSingular]
	, [MinorNamePlural]
	, [MinorNameSingular]
	, [CurrencySymbol]
    FROM
	[dbo].[Currency] WITH (NOLOCK) 
    WHERE 
	 ([CurrencyID] = @CurrencyID OR @CurrencyID IS NULL)
	AND ([CurrencyCode] = @CurrencyCode OR @CurrencyCode IS NULL)
	AND ([CurrencyName] = @CurrencyName OR @CurrencyName IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([MajorNamePlural] = @MajorNamePlural OR @MajorNamePlural IS NULL)
	AND ([MajorNameSingular] = @MajorNameSingular OR @MajorNameSingular IS NULL)
	AND ([MinorNamePlural] = @MinorNamePlural OR @MinorNamePlural IS NULL)
	AND ([MinorNameSingular] = @MinorNameSingular OR @MinorNameSingular IS NULL)
	AND ([CurrencySymbol] = @CurrencySymbol OR @CurrencySymbol IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CurrencyID]
	, [CurrencyCode]
	, [CurrencyName]
	, [WhenCreated]
	, [WhenModified]
	, [MajorNamePlural]
	, [MajorNameSingular]
	, [MinorNamePlural]
	, [MinorNameSingular]
	, [CurrencySymbol]
    FROM
	[dbo].[Currency] WITH (NOLOCK) 
    WHERE 
	 ([CurrencyID] = @CurrencyID AND @CurrencyID is not null)
	OR ([CurrencyCode] = @CurrencyCode AND @CurrencyCode is not null)
	OR ([CurrencyName] = @CurrencyName AND @CurrencyName is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([MajorNamePlural] = @MajorNamePlural AND @MajorNamePlural is not null)
	OR ([MajorNameSingular] = @MajorNameSingular AND @MajorNameSingular is not null)
	OR ([MinorNamePlural] = @MinorNamePlural AND @MinorNamePlural is not null)
	OR ([MinorNameSingular] = @MinorNameSingular AND @MinorNameSingular is not null)
	OR ([CurrencySymbol] = @CurrencySymbol AND @CurrencySymbol is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Currency_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Find] TO [sp_executeall]
GO
