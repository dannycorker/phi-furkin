SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alistair Jones
-- Create date: 2007-08-02
-- Description:	Select record detail from the EventTypeMandatoryField table through a foreign key
-- JWG 2015-02-11 #30930 New flag ForceEditableOverride, used in the MandatoryFieldEditor to override the Editable setting of DetailFields.
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeHelperField__GetDetailByEventTypeID]
(
	@EventTypeID INT   
)
AS
BEGIN
			
	SELECT h.EventTypeHelperFieldID,
	h.EventTypeID,
	df.FieldName,
	h.DetailFieldID,
	h.LeadTypeID,
	h.ClientID, 
	h.ForceEditableOverride
	FROM dbo.EventTypeHelperField h WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON h.DetailFieldID = df.DetailFieldID 
	WHERE h.EventTypeID = @EventTypeID 
	ORDER BY df.FieldName 
	
	SELECT @@ROWCOUNT
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField__GetDetailByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField__GetDetailByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField__GetDetailByEventTypeID] TO [sp_executeall]
GO
