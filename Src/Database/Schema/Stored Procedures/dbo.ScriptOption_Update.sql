SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_Update]
(

	@ScriptOptionID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@ScriptEditorTheme varchar (250)  ,

	@ShowLineNumbers bit   ,

	@CollapseTopPanel bit   ,

	@CollapseLeftPanel bit   ,

	@CollapseRightPanel bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptOption]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[ScriptEditorTheme] = @ScriptEditorTheme
					,[ShowLineNumbers] = @ShowLineNumbers
					,[CollapseTopPanel] = @CollapseTopPanel
					,[CollapseLeftPanel] = @CollapseLeftPanel
					,[CollapseRightPanel] = @CollapseRightPanel
				WHERE
[ScriptOptionID] = @ScriptOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Update] TO [sp_executeall]
GO
