SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[UpdateOutcomeEquation]

@OutcomeEquationID int,
@OutcomeID int,
@EquationID int,
@ClientID int = null

as

Update OutcomeEquations
Set OutcomeID = @OutcomeID, EquationID = @EquationID
Where OutcomeEquationID = @OutcomeEquationID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcomeEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateOutcomeEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcomeEquation] TO [sp_executeall]
GO
