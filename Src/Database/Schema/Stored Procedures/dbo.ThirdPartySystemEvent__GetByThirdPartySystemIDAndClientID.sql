SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartySystemEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemIDAndClientID]
(
	@ThirdPartySystemID int,
	@ClientID int
)
AS


				SELECT *
				FROM
					[dbo].[ThirdPartySystemEvent]
				WHERE
					[ThirdPartySystemID] = @ThirdPartySystemID AND
					[ClientID] = @ClientID
					
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemIDAndClientID] TO [sp_executeall]
GO
