SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jan Wilson
-- Create date: 2014-05-28
-- Description:	Returns all data types for the rule sets
-- Parameters:  @ClientID
--				@ParameterTypeID
--              1 = Detail Fields
--              2 = Customer Fields
--              3 = Rule Sets
--              4 = Functions
--              5 = Resource Lists
-- Modified:	2014-11-12	SB	Changed to use fnDetailFieldsShared
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_GetListByParameterTypeID]
(
	@ClientID			INT,
	@ParameterTypeID	INT
)
AS
BEGIN
	DECLARE @DummyCustomerId INT

	IF (@ParameterTypeID = 1) -- Detail Fields
	BEGIN
		SELECT
			CAST(DF.DetailFieldID AS VARCHAR(MAX)) ID,
			DF.FieldName Name
		FROM
			dbo.fnDetailFieldsShared(@ClientID) DF
		WHERE
			DF.ClientID = @ClientID
			AND DF.Enabled = 1
		ORDER BY
			Name
		RETURN
	END
	
	IF (@ParameterTypeID = 2) -- Customer Fields
	BEGIN
		SET @DummyCustomerId = (SELECT TOP 1 CustomerID FROM Customers WHERE ClientID = @ClientID)
		SELECT 
			RawColumnName ID,
			RawColumnName Name
		FROM
			dbo.fnUnpivotCustomers(@DummyCustomerID)
		UNION ALL
		SELECT 'CalculationDate' ID, 'CalculationDate' Name
		ORDER BY Name
		RETURN
	END
	IF (@ParameterTypeID = 3) -- Rule Sets
	BEGIN
		SELECT
			CAST(RS.RuleSetID  AS VARCHAR(MAX)) ID,
			RS.Name Name
		FROM
			RulesEngine_RuleSets RS
		WHERE
			RS.ClientID = @ClientID
		ORDER BY RS.Name
		RETURN
	END
	
	-- @ParameterTypeID = 4 (no list for this as text is provided only)
	
	IF (@ParameterTypeID = 5) -- Resource Lists
	BEGIN
		SELECT
			CAST(df.DetailFieldID AS VARCHAR(MAX)) ParentID,
			df.FieldCaption ParentCaption,
			CAST(df_fields.DetailFieldID AS VARCHAR(MAX)) ChildID,
			df_fields.FieldCaption ChildCaption
		FROM 
			dbo.fnDetailFieldsShared(@ClientID) df
			INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df_fields
			ON df_fields.DetailFieldPageID = df.ResourceListDetailFieldPageID
		WHERE
			df_fields.ClientID = @ClientID
		AND df.LeadOrMatter NOT IN (4, 6, 8)
		ORDER BY
			df.FieldCaption, df_fields.FieldOrder			
		RETURN
	END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_GetListByParameterTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_GetListByParameterTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_GetListByParameterTypeID] TO [sp_executeall]
GO
