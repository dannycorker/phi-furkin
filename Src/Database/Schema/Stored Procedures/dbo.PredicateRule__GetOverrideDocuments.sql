SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Predicate Get Override Documents, similar to the one passed in
-- 2015-12-07 AH, JWG #33841 Only include enabled document templates
-- =============================================
CREATE PROCEDURE [dbo].[PredicateRule__GetOverrideDocuments] 
	@ClientID INT,
	@DocumentTypeID INT,
	@LeadTypeID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	IF (@DocumentTypeID > 0)
	BEGIN
		SELECT	dtLike.DocumentTypeID, dtLike.DocumentTypeName, dtLike.OutputFormat
		FROM	DocumentType dtLike WITH (NOLOCK)
		INNER JOIN DocumentType dt WITH (NOLOCK) ON 
			dtLike.ClientID = dt.ClientID AND 
			dtLike.OutputFormat = dt.OutputFormat AND
			(dtLike.LeadTypeID IS NULL OR dtLike.LeadTypeID = dt.LeadTypeID) AND 
			dtLike.Enabled = 1
		WHERE	dt.ClientID = @ClientID
		AND		(dt.DocumentTypeID = @DocumentTypeID)
		
		UNION ALL
		
		SELECT -1 DocumentTypeID, '[No Document]' DocumentTypeName, 'MS_WORD_2003' OutputFormat
		ORDER BY DocumentTypeName
	END
	ELSE
	BEGIN
		SELECT	dt.DocumentTypeID, dt.DocumentTypeName, dt.OutputFormat
		FROM	DocumentType dt WITH (NOLOCK)
		WHERE	dt.ClientID = @ClientID
		AND		(dt.LeadTypeID IS NULL OR dt.LeadTypeID = @LeadTypeID)
		AND 	dt.Enabled = 1 /* #33841 */
		
		UNION ALL
		
		SELECT -1 DocumentTypeID, '[No Document]' DocumentTypeName, 'MS_WORD_2003' OutputFormat
		ORDER BY DocumentTypeName
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRule__GetOverrideDocuments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PredicateRule__GetOverrideDocuments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRule__GetOverrideDocuments] TO [sp_executeall]
GO
