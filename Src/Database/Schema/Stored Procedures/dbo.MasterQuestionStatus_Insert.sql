SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the MasterQuestionStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestionStatus_Insert]
(

	@MasterQuestionStatusID int   ,

	@MasterQuestionStatusName varchar (50)  ,

	@MasterQuestonStatusDescription varchar (250)  
)
AS


				
				INSERT INTO [dbo].[MasterQuestionStatus]
					(
					[MasterQuestionStatusID]
					,[MasterQuestionStatusName]
					,[MasterQuestonStatusDescription]
					)
				VALUES
					(
					@MasterQuestionStatusID
					,@MasterQuestionStatusName
					,@MasterQuestonStatusDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestionStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Insert] TO [sp_executeall]
GO
