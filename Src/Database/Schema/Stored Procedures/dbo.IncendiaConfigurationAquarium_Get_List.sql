SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the IncendiaConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationAquarium_Get_List]

AS


				
				SELECT
					[IncendiaConfigurationAquariumID],
					[AccountID]
				FROM
					[dbo].[IncendiaConfigurationAquarium] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationAquarium_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Get_List] TO [sp_executeall]
GO
