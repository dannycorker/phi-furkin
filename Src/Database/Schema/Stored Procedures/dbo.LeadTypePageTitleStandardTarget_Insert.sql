SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadTypePageTitleStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_Insert]
(

	@LeadTypePageTitleStandardTargetID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@TemplateTypeID int   ,

	@Notes varchar (250)  
)
AS


				
				INSERT INTO [dbo].[LeadTypePageTitleStandardTarget]
					(
					[ClientID]
					,[LeadTypeID]
					,[Target]
					,[ObjectName]
					,[PropertyName]
					,[TemplateTypeID]
					,[Notes]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@Target
					,@ObjectName
					,@PropertyName
					,@TemplateTypeID
					,@Notes
					)
				-- Get the identity value
				SET @LeadTypePageTitleStandardTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Insert] TO [sp_executeall]
GO
