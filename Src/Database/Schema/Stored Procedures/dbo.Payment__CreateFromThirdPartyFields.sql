SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-07-2016
-- Description:	Creates a payment record from the third party fields,
--				also creates a customer ledger entry and attempts to 
--				reconcile the customer payment schedule
-- Mods:
--	2016-08-16	DCM Also over-ride reference if customer payment schedule id is set
--  2016-08-25  DCM Protected third-party money fields against a value of ''
--  2016-08-28  DCM Don't default @DateReceived
--  2016-08-31  PR	Added EXEC Account__SetDateAndAmountOfNextPayment @AccountID
--  2016-10-19  DCM Make customer ledger when created full date & time
--  2016-12-07  DCM remove block on negative payments (we need to make refunds from the payments table)
--  2017-02-13  JEL Changed Transaction Date on Customer ledger record to be GetDate in order to reflect when transmision file created rather than payment date
-- =============================================
CREATE PROCEDURE [dbo].[Payment__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT
AS
BEGIN


	SET NOCOUNT ON;

	-- Gets the customer, lead, matter identities and the leadtype
	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @WhoCreated INT
	SELECT @LeadID=cs.LeadID FROM Cases cs WITH (NOLOCK) WHERE cs.CaseID = @CaseID
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@LeadID
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.CaseID=@CaseID
	
	SELECT @WhoCreated = le.WhoCreated FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID=@LeadEventID
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	--4369 PaymentDateTime
	DECLARE @PaymentDateTime_DV VARCHAR(2000), @IsDateTime_PaymentDateTime BIT, @PaymentDateTime DATETIME
	SELECT @PaymentDateTime_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4369)		
	SELECT @IsDateTime_PaymentDateTime = dbo.fnIsDateTime(@PaymentDateTime_DV)		
	IF(@IsDateTime_PaymentDateTime=1)
	BEGIN
		SELECT @PaymentDateTime = CAST(@PaymentDateTime_DV AS DATETIME)
	END
	IF @PaymentDateTime_DV is NULL or @PaymentDateTime_DV = '' 
	BEGIN
		SELECT @PaymentDateTime = dbo.fn_GetDate_Local() 
	END
					
	--4370 PaymentTypeID
	DECLARE @PaymentTypeID_DV VARCHAR(2000), @IsInt_PaymentTypeID BIT, @PaymentTypeID INT, @ItemID_PaymentTypeID INT, @PaymentTypeID_ItemValue VARCHAR(500)
	SELECT @PaymentTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4370)	
	SELECT @IsInt_PaymentTypeID = dbo.fnIsInt(@PaymentTypeID_DV)
	IF(@IsInt_PaymentTypeID=1)
	BEGIN
		SELECT @ItemID_PaymentTypeID = CAST(@PaymentTypeID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual payment type
		SELECT @PaymentTypeID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_PaymentTypeID
		SELECT @PaymentTypeID = pt.PaymentTypeID FROM  PaymentTypes pt WITH (NOLOCK) WHERE pt.PaymentTypeName = @PaymentTypeID_ItemValue
	END			
	
	--4371 DateReceived
	DECLARE @DateReceived_DV VARCHAR(2000), @IsDateTime_DateReceived BIT, @DateReceived DATETIME
	SELECT @DateReceived_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4371)		
	SELECT @IsDateTime_DateReceived = dbo.fnIsDateTime(@DateReceived_DV)
	IF(@IsDateTime_DateReceived=1)
	BEGIN
		SELECT @DateReceived = CAST(@DateReceived_DV AS DATETIME)
	END	
	
	--4372 PaymentDescription
	DECLARE @PaymentDescription_DV VARCHAR(2000)
	SELECT @PaymentDescription_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4372)		
	
	--4373 PaymentReference
	DECLARE @PaymentReference_DV VARCHAR(2000)
	SELECT @PaymentReference_DV = ISNULL(dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4373)		,'')
	
	DECLARE @LogEntry VARCHAR(MAX),@MethodName VARCHAR(200)	
	--4374 PaymentNet
	DECLARE @PaymentNet_DV VARCHAR(2000), @PaymentNet NUMERIC(18,2)
	SELECT @PaymentNet_DV = CASE WHEN dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4374) = ''
								THEN '0'
								ELSE
									dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4374)
								END	
		
	BEGIN TRY  
		SELECT @PaymentNet = CAST(@PaymentNet_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Payment Net'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'Payment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 
	
	--4375 PaymentVAT
	DECLARE @PaymentVAT_DV VARCHAR(2000), @PaymentVAT NUMERIC(18,2)
	SELECT @PaymentVAT_DV = CASE WHEN dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4375) = ''
								THEN '0'
								ELSE
									dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4375)
								END	
		
	BEGIN TRY  
		SELECT @PaymentVAT = CAST(@PaymentVAT_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Payment VAT'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'Payment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 	
	
	--4376 PaymentGross
	DECLARE @PaymentGross_DV VARCHAR(2000), @PaymentGross NUMERIC(18,2)
	SELECT @PaymentGross_DV = CASE WHEN dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4376) = ''		
								THEN '0'
								ELSE
									dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4376)
								END	
		
	BEGIN TRY  
		SELECT @PaymentGross = CAST(@PaymentGross_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Payment Gross'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'Payment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 
	
	--4377 PaymentCurrency
	DECLARE @PaymentCurrency_DV VARCHAR(2000), @IsInt_PaymentCurrency BIT, @PaymentCurrency INT
	SELECT @PaymentCurrency_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4377)
	SELECT @IsInt_PaymentCurrency = dbo.fnIsInt(@PaymentCurrency_DV)
	IF(@IsInt_PaymentCurrency=1)
	BEGIN
		SELECT @PaymentCurrency = CAST(@PaymentCurrency_DV AS INT)
	END
	
	--4378 RelatedOrderRef
	DECLARE @RelatedOrderRef_DV VARCHAR(2000)
	SELECT @RelatedOrderRef_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4378)	
	
	--4379 RelatedRequestDescription
	DECLARE @RelatedRequestDescription_DV VARCHAR(2000)
	SELECT @RelatedRequestDescription_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4379)		
	
	--4380 PayeeFullName
	DECLARE @PayeeFullName_DV VARCHAR(2000)
	SELECT @PayeeFullName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4380)		

	--4381 MaskedAccountNumber
	DECLARE @MaskedAccountNumber_DV VARCHAR(2000)
	SELECT @MaskedAccountNumber_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4381)		
		
	--4382 AccountTypeDescription
	DECLARE @AccountTypeDescription_DV VARCHAR(2000)
	SELECT @AccountTypeDescription_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4382)		
		
	--4383 PaymentStatusID
	DECLARE @PaymentStatusID_DV VARCHAR(2000), @IsInt_PaymentStatusID BIT, @PaymentStatusID INT, @ItemID_PaymentStatusID INT, @PaymentStatusID_ItemValue VARCHAR(500)
	SELECT @PaymentStatusID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4383)	
	SELECT @IsInt_PaymentStatusID = dbo.fnIsInt(@PaymentStatusID_DV)		
	IF(@IsInt_PaymentStatusID=1)
	BEGIN
		SELECT @ItemID_PaymentStatusID = CAST(@PaymentStatusID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual payment status type
		SELECT @PaymentStatusID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_PaymentStatusID
		SELECT @PaymentStatusID = ps.PaymentStatusID FROM PaymentStatus ps WITH (NOLOCK) WHERE ps.PaymentStatusName = @PaymentStatusID_ItemValue
	END		
	
	--4384 DateReconciled
	DECLARE @DateReconciled_DV VARCHAR(2000), @IsDateTime_DateReconciled BIT, @DateReconciled DATETIME
	SELECT @DateReconciled_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4384)		
	SELECT @IsDateTime_DateReconciled = dbo.fnIsDateTime(@DateReconciled_DV)		
	IF(@IsDateTime_DateReconciled=1)
	BEGIN
		SELECT @DateReconciled = CAST(@DateReconciled_DV AS DATETIME)
	END	
	
	--4385 ReconciliationOutcome
	DECLARE @ReconciliationOutcome_DV VARCHAR(2000)
	SELECT @ReconciliationOutcome_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4385)		
		
	--4386 ObjectID
	DECLARE @ObjectID_DV VARCHAR(2000), @IsInt_ObjectID BIT, @ObjectID INT
	SELECT @ObjectID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4386)
	SELECT @IsInt_ObjectID = dbo.fnIsInt(@ObjectID_DV)
	IF(@IsInt_ObjectID=1)
	BEGIN
		SELECT @ObjectID = CAST(@ObjectID_DV AS INT)
	END	
	
	--4387 ObjectTypeID
	DECLARE @ObjectTypeID_DV VARCHAR(2000), @IsInt_ObjectTypeID BIT, @ObjectTypeID INT
	SELECT @ObjectTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4387)
	SELECT @IsInt_ObjectTypeID = dbo.fnIsInt(@ObjectTypeID_DV)
	IF(@IsInt_ObjectTypeID=1)
	BEGIN
		SELECT @ObjectTypeID = CAST(@ObjectTypeID_DV AS INT)
	END	
	
	--4388 Comments
	DECLARE @Comments_DV VARCHAR(2000)
	SELECT @Comments_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4388)		

	--4389 PaymentFileName
	DECLARE @PaymentFileName_DV VARCHAR(2000)
	SELECT @PaymentFileName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4389)		
	
	--4390 FailureCode
	DECLARE @FailureCode_DV VARCHAR(2000)
	SELECT @FailureCode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4390)		
	
	--4391 FailureReason
	DECLARE @FailureReason_DV VARCHAR(2000)
	SELECT @FailureReason_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4391)		
	
	--4392 WhoCreated	-- setup from leadevent at top of proc
	--4393 WhenCreated	-- setup from leadevent at top of proc
	
	--4394 WhoModified -- since its a new one simply set who and when modified to created
	DECLARE @WhoModified INT
	SELECT @WhoModified = @WhoCreated
	--4395 WhenModified
	DECLARE @WhenModified DATETIME
	SELECT @WhenModified = @WhenCreated
	

	--4396 CustomerPaymentScheduleID
	DECLARE @AccountID INT
	DECLARE @CustomerPaymentScheduleID_DV VARCHAR(2000), @IsInt_CustomerPaymentScheduleID BIT, @CustomerPaymentScheduleID INT
	SELECT @CustomerPaymentScheduleID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4396)
	SELECT @IsInt_CustomerPaymentScheduleID = dbo.fnIsInt(@CustomerPaymentScheduleID_DV)
	IF(@IsInt_CustomerPaymentScheduleID=1)
	BEGIN
		SELECT @CustomerPaymentScheduleID = CAST(@CustomerPaymentScheduleID_DV AS INT)
		-- if there is a valid customer payment schedule id then overide the following values:-
		--PaymentDate,PaymentNet,PaymentVAT,PaymentGross,Reference,Description
		IF(@CustomerPaymentScheduleID>0)
		BEGIN
			SELECT @PaymentNet=cps.PaymentNet, @PaymentVAT=cps.PaymentVAT, @PaymentGross=cps.PaymentGross, @PaymentDateTime=cps.PaymentDate,
					@PaymentReference_DV=ISNULL(a.Reference,@PaymentReference_DV), @AccountID=a.AccountID
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID
			WHERE cps.CustomerPaymentScheduleID=@CustomerPaymentScheduleID
		END
	END

	EXEC _C00_LogIt 'Info', 'Payment__CreateFromThirdPartyFields', '@CustomerPaymentScheduleID',@CustomerPaymentScheduleID,58540

	-- 2019-10-07 CPS for JIRA LPC-28  | Return dbo.fn_GetDate_Local() if we don't have a date and time
	-- 2019-12-03 CPS for JIRA LPC-174 | removed temporary change now we have correct worldpay credentials
	--IF @PaymentDateTime is NULL
	--BEGIN
	--	SELECT	 @PaymentDateTime = dbo.fn_GetDate_Local()
	--			,@PaymentTypeID	  = 7
	--			,@PaymentReference_DV = 'Default Reference'
	--			,@PaymentGross = 101
	--END

	DECLARE @PaymentID INT, @CustomerLedgerID INT		
	--Inserts the thirdpartymapped fields into the outgoing payment table	
	--IF ((@PaymentGross IS NOT NULL) AND (@PaymentGross>0)) -- Checks payment is not null and is greater than 0
	IF (@PaymentGross IS NOT NULL) -- Checks payment is not null
	BEGIN
	
		-- only set date received if passed in in third party field (to allowed for delayed receipt of BACS payments)
		--DECLARE @IsDate BIT
		--SELECT @IsDate = dbo.fnIsInt(@DateReceived)
		--IF(@IsDate=0)
		--BEGIN
		--	SELECT @DateReceived = @WhenCreated
		--END

		--Create payment record

		INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, CustomerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@ClientID, @CustomerID, @PaymentDateTime, @PaymentTypeID, @DateReceived, @PaymentDescription_DV, @PaymentReference_DV, @PaymentNet, @PaymentVAT, @PaymentGross, @PaymentCurrency, @RelatedOrderRef_DV, @RelatedRequestDescription_DV, @PayeeFullName_DV, @MaskedAccountNumber_DV, @AccountTypeDescription_DV, @PaymentStatusID, @DateReconciled, @ReconciliationOutcome_DV, @ObjectID, @ObjectTypeID, @Comments_DV, @PaymentFileName_DV, @FailureCode_DV, @FailureReason_DV, @CustomerPaymentScheduleID,@WhoCreated, @WhenCreated, @WhoModified, @WhenModified)

		SELECT @PaymentID = SCOPE_IDENTITY()		

		-- Insert CustomerLedger Entry		
		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		VALUES (@ClientID, @CustomerID, NULL, @FailureCode_DV, @FailureReason_DV, dbo.fn_GetDate_Local(), @PaymentReference_DV, @PaymentDescription_DV, @PaymentNet,@PaymentVAT, @PaymentGross, @LeadEventID, @ObjectID, @ObjectTypeID, @PaymentID, NULL, @WhoCreated, dbo.fn_GetDate_Local())		
		
		SELECT @CustomerLedgerID = SCOPE_IDENTITY()	

		--Reconcile payment against CustomerPaymentSchedule update PaymentStatusID and ReconciledDate
		IF(@CustomerPaymentScheduleID>0)
		BEGIN
			UPDATE CustomerPaymentSchedule
			SET CustomerLedgerID=@CustomerLedgerID, ReconciledDate = @WhenCreated, PaymentStatusID=2
			WHERE CustomerPaymentScheduleID=@CustomerPaymentScheduleID
			
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentStatusID=cps.PaymentStatusID, ReconciledDate = cps.ReconciledDate, CustomerLedgerID = cps.CustomerLedgerID
			FROM CustomerPaymentSchedule cps
			INNER JOIN dbo.PurchasedProductPaymentSchedule pps on cps.CustomerPaymentScheduleID = pps.CustomerPaymentScheduleID
			WHERE cps.CustomerLedgerID=@CustomerLedgerID AND 
				  cps.ReconciledDate = @WhenCreated AND 
				  cps.PaymentStatusID=2
				  
			IF @AccountID<=0
			BEGIN
				SELECT @AccountID=AccountID FROM CustomerPaymentSchedule WHERE CustomerPaymentScheduleID=@CustomerPaymentScheduleID
			END
		
			EXEC Account__SetDateAndAmountOfNextPayment @AccountID
			
		END
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Payment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
