SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-08
-- Description:	Loop through all new imports attempting reconcile
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BACS_PostImportBulkReconcile] 
(
	@ImportFileID VARCHAR(100)
)

AS
BEGIN

	SET NOCOUNT ON;

--declare
--	@ImportFileID INT = 00015310,
--	@ClientPersonnelID INT

	
	DECLARE	
			@ClientPersonnelID INT = dbo.fn_C600_GetAqAutomationUser(),
			@TableRowID INT
	
	
	DECLARE @IntInt dbo.tvpIntInt
	
	INSERT @IntInt (ID1, ID2)
	SELECT tr.TableRowID, 0
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID
	WHERE tdv.DetailFieldID = 170206 /* Import FileID */
	and tr.DetailFieldID	= 170221 /* New Imports */
	AND tdv.DetailValue = @ImportFileID

	WHILE EXISTS ( SELECT * FROM @IntInt id WHERE id.ID2 = 0 )
	BEGIN
		SELECT TOP(1) @TableRowID = id.ID1 FROM @IntInt id WHERE id.ID2 = 0

		EXEC _C600_BACS_ReconcileImport @TableRowID, 1, @ClientPersonnelID
		
		UPDATE id SET ID2 = 1 FROM @IntInt id WHERE id.ID1 = @TableRowID
	END
				
END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_PostImportBulkReconcile] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BACS_PostImportBulkReconcile] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_PostImportBulkReconcile] TO [sp_executeall]
GO
