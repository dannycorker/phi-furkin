SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SearchQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_Insert]
(

	@SearchQueryID int    OUTPUT,

	@ClientID int   ,

	@SubClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@SearchQueryName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[SearchQuery]
					(
					[ClientID]
					,[SubClientID]
					,[ClientPersonnelAdminGroupID]
					,[ClientPersonnelID]
					,[SearchQueryName]
					)
				VALUES
					(
					@ClientID
					,@SubClientID
					,@ClientPersonnelAdminGroupID
					,@ClientPersonnelID
					,@SearchQueryName
					)
				-- Get the identity value
				SET @SearchQueryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Insert] TO [sp_executeall]
GO
