SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientQuestionnairesVisitedAndCompleted table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientQuestionnairesVisitedAndCompletedID],
					[ClientQuestionnaireID],
					[VisitedDate],
					[IPAddress],
					[ClientID]
				FROM
					[dbo].[ClientQuestionnairesVisitedAndCompleted] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientID] TO [sp_executeall]
GO
