SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FunctionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_Delete]
(

	@FunctionTypeID int   
)
AS


				DELETE FROM [dbo].[FunctionType] WITH (ROWLOCK) 
				WHERE
					[FunctionTypeID] = @FunctionTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Delete] TO [sp_executeall]
GO
