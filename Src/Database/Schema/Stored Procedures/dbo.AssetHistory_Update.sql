SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AssetHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetHistory_Update]
(

	@AssetHistoryID int   ,

	@AssetID int   ,

	@ClientID int   ,

	@AssetTypeID int   ,

	@AssetSubTypeID int   ,

	@LocationID int   ,

	@AssetName varchar (255)  ,

	@Note varchar (255)  ,

	@SecureNote varchar (255)  ,

	@SecureUserName varchar (255)  ,

	@SecurePassword varchar (255)  ,

	@SecureOther varchar (255)  ,

	@Version varchar (255)  ,

	@ValidFrom datetime   ,

	@ValidTo datetime   ,

	@ReminderDue datetime   ,

	@ReminderTimeUnitID int   ,

	@ReminderTimeUnitQuantity int   ,

	@ReminderNotificationGroupID int   ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AssetHistory]
				SET
					[AssetID] = @AssetID
					,[ClientID] = @ClientID
					,[AssetTypeID] = @AssetTypeID
					,[AssetSubTypeID] = @AssetSubTypeID
					,[LocationID] = @LocationID
					,[AssetName] = @AssetName
					,[Note] = @Note
					,[SecureNote] = @SecureNote
					,[SecureUserName] = @SecureUserName
					,[SecurePassword] = @SecurePassword
					,[SecureOther] = @SecureOther
					,[Version] = @Version
					,[ValidFrom] = @ValidFrom
					,[ValidTo] = @ValidTo
					,[ReminderDue] = @ReminderDue
					,[ReminderTimeUnitID] = @ReminderTimeUnitID
					,[ReminderTimeUnitQuantity] = @ReminderTimeUnitQuantity
					,[ReminderNotificationGroupID] = @ReminderNotificationGroupID
					,[Enabled] = @Enabled
					,[Deleted] = @Deleted
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[AssetHistoryID] = @AssetHistoryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Update] TO [sp_executeall]
GO
