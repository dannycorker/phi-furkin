SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-05-13
-- Description: Forces a conversation to be updated to a returned status
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_ForceStatusUpdate]
(
	@ClientID INT,
	@AccountID INT,
	@ConversationID UNIQUEIDENTIFIER,
	@HistoryID INT,
	@Status INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	-- First close off the action as done
	UPDATE dbo.VetEnvoy_ConversationHistory
	SET Actioned = dbo.fn_GetDate_Local()
	WHERE HistoryID = @HistoryID
	
	-- Now save the new status already actioned for completeness
	DECLARE @ConversationPK INT,
			@Service VARCHAR(50)
			
	SELECT	@ConversationPK = ConversationPK,
			@Service = Service
	FROM dbo.VetEnvoy_Conversations WITH (NOLOCK) 
	WHERE ConversationID = @ConversationID
	
	INSERT INTO dbo.VetEnvoy_ConversationHistory (ClientID, ConversationPK, ConversationID, Received, Service, Status, Actioned)
	VALUES (@ClientID, @ConversationPK, @ConversationID, dbo.fn_GetDate_Local(), @Service, @Status, dbo.fn_GetDate_Local())


END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_ForceStatusUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_ForceStatusUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_ForceStatusUpdate] TO [sp_executeall]
GO
