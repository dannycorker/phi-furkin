SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditing table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_GetByWhoIsEditing]
(

	@WhoIsEditing int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingID],
					[ClientID],
					[SqlQueryID],
					[WhoIsEditing],
					[EditStartedAt],
					[QueryText],
					[QueryTitle],
					[LeadTypeID],
					[FolderID],
					[ParentQueryID],
					[LockAllTables]
				FROM
					[dbo].[SqlQueryEditing] WITH (NOLOCK) 
				WHERE
					[WhoIsEditing] = @WhoIsEditing
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_GetByWhoIsEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_GetByWhoIsEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_GetByWhoIsEditing] TO [sp_executeall]
GO
