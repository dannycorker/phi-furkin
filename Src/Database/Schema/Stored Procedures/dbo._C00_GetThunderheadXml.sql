SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 21-11-2013
-- Description:	Gets a pivoted customer as xml
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetThunderheadXml]

	@CaseID INT

AS
BEGIN
	
	DECLARE @CustomerID INT
	DECLARE @LeadID INT
	
	SELECT @LeadID=LeadID FROM Cases WITH (NOLOCK) WHERE CaseID=@CaseID
	SELECT @CustomerID=CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID
	
	SELECT * FROM Customers WITH (NOLOCK)  
	WHERE CustomerID = @CustomerID 
	FOR XML PATH('Customers')
	
	SELECT * FROM Lead WITH (NOLOCK)  
	WHERE LeadID = @LeadID
	FOR XML PATH('Lead')
	
	SELECT * FROM Cases WITH (NOLOCK)  
	WHERE CaseID = @CaseID
	FOR XML PATH('Cases')
	
	SELECT TOP 1 * FROM Matter WITH (NOLOCK)  
	WHERE CaseID = @CaseID
	ORDER BY MatterID DESC
	FOR XML PATH('Matter')
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetThunderheadXml] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetThunderheadXml] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetThunderheadXml] TO [sp_executeall]
GO
