SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

Created By:		Chris Townsend
				Aquarium Software (https://www.aquarium-software.com)
Creation Date:	10-02-2010
Purpose:		Gets the LeadType-level default events for a specified lead type
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventSubtypeDefault__GetByLeadTypeIDAndClientPersonnelID]
(

	@LeadTypeID int,
	@ClientPersonnelID int 
	  
)
AS
				SET ANSI_NULLS OFF
				
				SELECT
					[EventSubtypeDefaultID],
					[ClientID],
					[EventSubtypeID],
					[EventTypeID],
					[LeadTypeID],
					[ClientPersonnelID]
				FROM
					[dbo].[EventSubtypeDefault]
				WHERE
					[LeadTypeID] = @LeadTypeID
					AND [ClientPersonnelID] = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault__GetByLeadTypeIDAndClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault__GetByLeadTypeIDAndClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault__GetByLeadTypeIDAndClientPersonnelID] TO [sp_executeall]
GO
