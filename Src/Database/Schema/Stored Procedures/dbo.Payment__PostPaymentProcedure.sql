SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-10-07
-- Description:	Payment Post Payment reconciliation
-- 2017-02-21 ACE Updated to replace double quotes from the error code
-- 2018-03-13 ACE Adapted from 321 for 
-- 2018-03-21 JEL Fixed Event Application 
-- 2018-03-21 JEL Amended payment status to use correct paid status
-- 2019-04-18 JEL corrected No Instruction retry date logic on CPS
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- 2020-07-16 JEL Added logic to split action based on AccountUseID so claim errors can be included 
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[Payment__PostPaymentProcedure]
	@PaymentID INT,
	@ClientID INT
AS 
BEGIN

	SET NOCOUNT ON;

	DECLARE  @PaymentStatus			INT
			,@BACSRetryType			VARCHAR(255)
			,@RetryWorkingDays		INT
			,@NextPaymentDate		DATE
			,@OriginalPaymentStatus INT
			,@CaseID				INT
			,@LeadID				INT
			,@NoteText				VARCHAR(2000) = ''
			,@SystemNoteTypeID		INT = 919
			,@VarcharDate			VARCHAR(10) = ''
			,@MatterID				INT
			,@AccountID				INT
			,@FailureReason			VARCHAR(100)
			,@LeadEventID			INT
			,@EventTypeID			INT
			,@DateForEvent			DATE = dbo.fn_GetDate_Local()
			,@PolicyStatus			INT
			,@EventLeadTypeID		INT
			,@RunAsUserID			INT
			,@EventToAdd			INT
			,@CustomerLedgerID		INT
			,@CommentsToApply		VARCHAR(1500)
			,@ClientAccountID		INT
			,@ClientAccountUseID	INT
			,@CountryID				INT

	SELECT @RunAsUserID	= dbo.fn_C00_GetAutomationUser(@ClientID)

	DECLARE @EventToApply TABLE (EventTypeID INT, AddEventAs INT)
	
	DECLARE @CustomerPaymentScheduleTransform TABLE (NewCustomerPaymentScheduleID INT, OldCustomerPaymentScheduleID INT)
	
	/*GPR 2020-03-02 for AAG-202*/
	SELECT @CountryID = cl.CountryID 
	FROM Clients cl WITH (NOLOCK)
	WHERE cl.ClientID = @ClientID

	/*Pick up the client account ID from the purchasedproduct*/ 
	SELECT @ClientAccountID = cps.ClientAccountID  
	FROM Payment p WITH ( NOLOCK ) 
	INNER JOIN CustomerPaymentSchedule cps WITH ( NOLOCK ) on cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
	WHERE p.PaymentID = @PaymentID 
	
	UPDATE p
	SET FailureCode = REPLACE(FailureCode, '"', '')
	FROM Payment p 
	WHERE p.PaymentID = @PaymentID
	
	IF (
		SELECT ISNULL(p.FailureCode , '')
		FROM Payment p WITH (NOLOCK) 
		WHERE p.PaymentID = @PaymentID
	) <> ''
	BEGIN
		PRINT 'Processing ' + ISNULL(CONVERT(VARCHAR,@PaymentID),'NULL')
		/*2018-04-27 ACE Updated from 321*/
		IF EXISTS (
			SELECT *
			FROM Payment p WITH (NOLOCK) 
			INNER JOIN PurchasedProductPaymentSchedule pp WITH (NOLOCK) ON pp.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
			WHERE p.PaymentID = @PaymentID
			AND pp.PurchasedProductPaymentScheduleID <> pp.PurchasedProductPaymentScheduleParentID
		)
		BEGIN
		
			/*This payment must be a retry. Therefor we will not attempt any more payment attempts...*/
			SELECT @OriginalPaymentStatus = 5 
		
		END
		ELSE
		BEGIN

			SELECT @OriginalPaymentStatus = 2 

		END
	
		/*
		SELECT @OriginalPaymentStatus = p.PaymentStatusID
		FROM Payment p
		WHERE p.PaymentID = @PaymentID
		*/

		/*So we have a failure code. We should decode this to a description and set the status accordingly.*/
		/*
			2017-02-21 ACE - Only use the left 1 character of the bacs failure code as they are coming 
			in as 1025, B025, 6025 etc
		*/

		SELECT @ClientAccountUseID = ca.AccountUseID
		FROM ClientAccount ca WITH (NOLOCK)
		WHERE ca.ClientAccountID = @ClientaccountID
		
		/*GPR 2020-05-18 separated AUS*/
		/*JEL 2020-07-16 added detail to check accountUseID to allow this proc to function for Claim errors too*/ 
		IF @CountryID = 14
		BEGIN
		UPDATE p 
			SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
				p.FailureReason = b.FailureDescription, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
			OUTPUT b.EventToApply, @RunAsUserID INTO @EventToApply (EventTypeID, AddEventAs)
			FROM Payment p
			INNER JOIN BACSReturnProcessing b ON b.FailureCode = LEFT(p.FailureCode, 3)
			INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) on cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
			INNER JOIN Account a WITH (NOLOCK) on a.AccountID = cps.AccountID 
			WHERE p.PaymentID = @PaymentID
			AND p.PaymentStatusID IN (6,2)
			AND b.ReasonCodeType IN ('EFT')
			AND  b.AccountUseID = CASE WHEN cps.PaymentGross > 0 THEN 1 ELSE a.AccountUseID END  
			AND b.AccountUseID = @ClientAccountUseID /*Allows for claims payments and returned refunds to be individually used.*/
		END
		ELSE IF @CountryID = 233
		BEGIN
			UPDATE p 
			SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
				p.FailureReason = b.FailureDescription, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
			OUTPUT b.EventToApply, @RunAsUserID INTO @EventToApply (EventTypeID, AddEventAs)
			FROM Payment p
			INNER JOIN BACSReturnProcessing b ON b.FailureCode = p.FailureCode
			WHERE p.PaymentID = @PaymentID
			AND p.PaymentStatusID IN (6,2) /*JEL ammended 7 to 6 for paid*/
			AND (b.ReasonCodeType = p.Comments /*DDIC/ARUDD/ARUCS*/ OR (p.Comments = '' AND b.ReasonCodeType IN ('ARUDD')))
			AND b.AccountUseID = @ClientAccountUseID /*Allows for claims payments and returned refunds to be individually used.*/
		END
		ELSE
		IF @ClientAccountID IN ( 2,9 ) -- HSBC Claims Payment Accounts
		BEGIN
			PRINT 'HSBC'

			/*Collect the Lead, Case, and Matter for the first existing account matter as we don't create new matters for claim payment accounts.*/
			SELECT TOP 1	 
				 @LeadID	= m.LeadID
				,@CaseID	= m.CaseID
				,@MatterID  = m.MatterID
			FROM Lead l WITH (NOLOCK) 
			INNER JOIN Cases  c WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
			WHERE l.LeadTypeID = 1493 /*Collections*/
			AND l.CustomerID = ( SELECT p.CustomerID FROM Payment p WITH (NOLOCK) WHERE p.PaymentID = @PaymentID )
			ORDER BY CASE WHEN c.ClientStatusID IN ( 4708,4707 ) /*Active*/ THEN 0 ELSE 1 END, m.MatterID

			PRINT 'Processing for PaymentID ' + ISNULL(CONVERT(VARCHAR,@PaymentID),'NULL') + ', using account MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL')

			SELECT TOP 1 @AccountID = cps.AccountID
			FROM Payment p WITH (NOLOCK)
			INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) on cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
			WHERE p.PaymentID = @PaymentID
			ORDER BY cps.CustomerPaymentScheduleID DESC
			
			-- 2021-02-09 CPS for JIRA FURKIN-72 | [WIP] Step for HSBC processing
			UPDATE p 
			SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
				p.FailureReason = b.FailureDescription, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
				OUTPUT b.EventToApply, @RunAsUserID INTO @EventToApply (EventTypeID, AddEventAs)
			FROM Payment p WITH (NOLOCK) 
			INNER JOIN BACSReturnProcessing b WITH (NOLOCK) ON b.ReasonCodeType = 'HSBC_PSRv3' AND b.FailureCode = p.FailureCode
			WHERE p.PaymentID = @PaymentID
		END
		ELSE
		BEGIN
			UPDATE p 
			SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
				p.FailureReason = b.FailureDescription, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
			OUTPUT b.EventToApply, @RunAsUserID INTO @EventToApply (EventTypeID, AddEventAs)
			FROM Payment p
			INNER JOIN BACSReturnProcessing b ON b.FailureCode = LEFT(p.FailureCode, 1) OR b.FailureCode = CASE WHEN LEN(p.FailureCode) < 4 THEN '0' END
				WHERE p.PaymentID = @PaymentID
			AND p.PaymentStatusID IN (6,2) /*JEL ammended 7 to 6 for paid*/
			AND (b.ReasonCodeType = p.Comments /*DDIC/ARUDD/ARUCS*/ OR (p.Comments = '' AND b.ReasonCodeType IN ('ARUDD')))
			AND b.AccountUseID = @ClientAccountUseID /*Allows for claims payments and returned refunds to be individually used.*/
		END
		/*!!!!!!NO SQL HERE!!!!!!*/
		
		IF @@ROWCOUNT >0
		BEGIN
			PRINT '@@RowCount > 0'
		
			SELECT @FailureReason = p.FailureReason
			FROM Payment p
			WHERE p.PaymentID = @PaymentID

			SELECT @EventToAdd = e.EventTypeID, @RunAsUserID = e.AddEventAs
			FROM @EventToApply e 
			Print 'event'
			PRINT @EventToAdd 
	
			/*If we have a row then process it...*/
			-- insert a GL reversal record
			INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			SELECT cl.ClientID, cl.CustomerID, EffectivePaymentDate, p.FailureCode, p.FailureReason, CAST(dbo.fn_GetDate_Local() AS DATE), TransactionReference, 'Failure notification ' + p.FailureReason, -cl.TransactionNet, -cl.TransactionVAT, -cl.TransactionGross, cl.LeadEventID, cl.ObjectID, cl.ObjectTypeID, @PaymentID, OutgoingPaymentID, 32438 /*@AqAutomation*/, dbo.fn_GetDate_Local() 
			FROM CustomerLedger cl WITH (NOLOCK) 
			INNER JOIN Payment p on p.PaymentID = cl.PaymentID
			WHERE cl.PaymentID=@PaymentID
			
			SELECT @CustomerLedgerID = SCOPE_IDENTITY()

			-- reverse customerpaymentschedule, reverse payment status
			UPDATE cps  
			SET PaymentStatusID=4, ReconciledDate=dbo.fn_GetDate_Local()
			FROM CustomerPaymentSchedule cps
			INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE p.PaymentID=@PaymentID
			
			-- purchasedproductpaymentschedule, reverse payment status
			UPDATE ppps  
			SET PaymentStatusID=4, ReconciledDate=dbo.fn_GetDate_Local(), ContraCustomerLedgerID = @CustomerLedgerID
			FROM PurchasedProductPaymentSchedule ppps
			INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE p.PaymentID=@PaymentID
			
			/*
				I think that we should be creating new payment retries at this point.
				We will need to figure out what the next actual collection date is for the next payment, do we bundle 
				it into the next payment or try and take it in x working days?
				
				So, first customer payment schedule, then purchased product payment schedule. 
				A batch should then pick up the paymnet request, add the customer ledger and the payment record.
				
			*/
			SELECT @BACSRetryType = cp.PreferenceValue
			FROM ClientPreference cp WITH (NOLOCK)
			WHERE cp.ClientID = @ClientID
			AND cp.ClientPreferenceTypeID = 3
		
			
			SELECT @RetryWorkingDays = cp.PreferenceValue
			FROM ClientPreference cp WITH (NOLOCK)
			WHERE cp.ClientID = @ClientID
			AND cp.ClientPreferenceTypeID = 4 /*BACS Retry Days*/
			
			SELECT @NextPaymentDate = dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(), @RetryWorkingDays)
		
			SELECT @CaseID = m.CaseID, @MatterID = m.MatterID, @VarcharDate = CONVERT(VARCHAR,@NextPaymentDate,121), @LeadID = m.LeadID, 
				@EventLeadTypeID = l.LeadTypeID, @AccountID = cps.AccountID
			FROM Payment p WITH ( NOLOCK )
			INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
			INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID /*2018-06-26 ACE Lets use account again, yay!*/
			INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = a.ObjectID
			INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID /*We need this for the leadtypeid for tpmapped field*/
			WHERE p.PaymentID = @PaymentID
			AND p.ObjectTypeID = 2 /*Matter*/
			
			PRINT '@BACSRetryType = ' + ISNULL(CONVERT(VARCHAR,@BACSRetryType),'NULL') + ', @FailureReason = ' + ISNULL(CONVERT(VARCHAR,@FailureReason),'NULL')
					
			IF @BACSRetryType = 'Try In X Days' AND  @FailureReason IN ('Refer to Payer', 'Refer to Customer')
			BEGIN

				PRINT @OriginalPaymentStatus
			
				/*Dont retry if this is a retry...*/
				IF @OriginalPaymentStatus <> 5
				BEGIN
						
					INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, WhoCreated, WhenCreated, SourceID, ClientAccountID, RelatedObjectID, RelatedObjectTypeID)
					OUTPUT inserted.CustomerPaymentScheduleID, inserted.SourceID INTO @CustomerPaymentScheduleTransform (NewCustomerPaymentScheduleID, OldCustomerPaymentScheduleID)
					SELECT cps.ClientID, cps.CustomerID, cps.AccountID, @NextPaymentDate, @NextPaymentDate, cps.PaymentNet, cps.PaymentVAT, cps.PaymentGross, 5 /*Retry*/, NULL, NULL, cps.WhoCreated, dbo.fn_GetDate_Local(), cps.CustomerPaymentScheduleID, @ClientAccountID, cps.RelatedObjectID, cps.RelatedObjectTypeID
					FROM CustomerPaymentSchedule cps WITH (NOLOCK)
					INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
					WHERE p.PaymentID=@PaymentID

					INSERT INTO PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
					SELECT ppps.ClientID, ppps.CustomerID, ppps.AccountID, ppps.PurchasedProductID, @NextPaymentDate, ppps.CoverFrom, ppps.CoverTo, @NextPaymentDate, ppps.PaymentNet, ppps.PaymentVAT, ppps.PaymentGross, 5 /*Retry*/, NULL, NULL, cpt.NewCustomerPaymentScheduleID, ppps.WhoCreated, dbo.fn_GetDate_Local(), ppps.PurchasedProductPaymentScheduleParentID, 3, @ClientAccountID, ppps.TransactionFee
					FROM PurchasedProductPaymentSchedule ppps
					INNER JOIN @CustomerPaymentScheduleTransform cpt ON cpt.OldCustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
					INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
					WHERE p.PaymentID=@PaymentID

					INSERT OneVisionClaimPayment ( [OneVisionClaimID], [VisionPaymentID], [Gross], [AccountID], [AccountNumber], [SortCode], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified] )
					SELECT vcd.[OneVisionClaimID], vcd.[VisionPaymentID], vcd.[Gross], ac.AccountID, ac.AccountNumber, ac.SortCode, cps.NewCustomerPaymentScheduleID, @RunAsUserID, dbo.fn_GetDate_Local(), @RunAsUserID, dbo.fn_GetDate_Local()
					FROM @CustomerPaymentScheduleTransform cps 
					INNER JOIN dbo.OneVisionClaimPayment vcd WITH (NOLOCK) on vcd.CustomerPaymentScheduleID = cps.OldCustomerPaymentScheduleID
					INNER JOIN dbo.Account ac WITH (NOLOCK) on ac.AccountID = @AccountID

					PRINT CONVERT(VARCHAR,@@RowCount) + ' new VisionClaimPayments inserted for AccountID ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL')

					--EXEC _C00_SimpleValueIntoThirdPartyField 4532, @VarcharDate, @MatterID, @RunAsUserID, @EventLeadTypeID, @ClientID /*Scheduled Date of Payment Retry*/

					SELECT @NoteText = 'DD Failure Identified.  Retry scheduled for ' + ISNULL(@VarcharDate,'NULL') + ' Failure reason ' + ISNULL(@FailureReason, 'Not set')
					EXEC _C00_AddANote @CaseID, @SystemNoteTypeID, @NoteText, 0, @RunAsUserID, 0, 1 /*System*/
					
					EXEC dbo.Account__SetDateAndAmountOfNextPayment @AccountID
					
				END
				ELSE 
				BEGIN
					
					IF EXISTS (SELECT * FROM Account a with (NOLOCK) WHERE a.AccountID = @AccountID and a.AccountUseID = 1) 
					BEGIN 

						/*This is the second retry, Cancel the policy*/ 
						SELECT @EventToAdd = 150098	

						PRINT @EventToAdd

						UPDATE a
						SET ExpiryDate = dbo.fnAddWorkingDays(@NextPaymentDate,10) 
						FROM Account a
						WHERE a.AccountID = @AccountID
					END 
					ELSE 
					BEGIN 
						/*Account is claims account, so open process to allow user to assess account details*/ 
						SELECT @EventToAdd =  159004

					END 

				END

			END
			
			IF @BACSRetryType = 'Try In X Days' AND  (@FailureReason = 'No Instruction' /*OR @FailureReason = 'Instruction cancelled' 2017-04-24 ACE Removed by request of Lisa Lee*/)
			BEGIN
			
				/*Dont retry if this is a retry...*/
				IF @OriginalPaymentStatus <> 5
				BEGIN
			
					SELECT @NextPaymentDate = dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(), 10)
					SELECT @VarcharDate = CONVERT(VARCHAR,@NextPaymentDate,121)

					INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, WhoCreated, WhenCreated, SourceID, ClientAccountID, RelatedObjectID, RelatedObjectTypeID)
					OUTPUT inserted.CustomerPaymentScheduleID, inserted.SourceID INTO @CustomerPaymentScheduleTransform (NewCustomerPaymentScheduleID, OldCustomerPaymentScheduleID)
					SELECT cps.ClientID, cps.CustomerID, cps.AccountID, @NextPaymentDate, @NextPaymentDate, cps.PaymentNet, cps.PaymentVAT, cps.PaymentGross, 5 /*Retry*/, NULL, NULL, cps.WhoCreated, dbo.fn_GetDate_Local(), cps.CustomerPaymentScheduleID, @ClientAccountID, cps.RelatedObjectID, cps.RelatedObjectTypeID
					FROM CustomerPaymentSchedule cps WITH (NOLOCK)
					INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
					WHERE p.PaymentID=@PaymentID
					
					INSERT INTO PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
					SELECT ppps.ClientID, ppps.CustomerID, ppps.AccountID, ppps.PurchasedProductID, @NextPaymentDate, ppps.CoverFrom, ppps.CoverTo, @NextPaymentDate, ppps.PaymentNet, ppps.PaymentVAT, ppps.PaymentGross, 5 /*Retry*/, NULL, NULL, cpt.NewCustomerPaymentScheduleID, ppps.WhoCreated, dbo.fn_GetDate_Local(), ppps.PurchasedProductPaymentScheduleParentID, 3,@ClientAccountID, ppps.TransactionFee
					FROM PurchasedProductPaymentSchedule ppps
					INNER JOIN @CustomerPaymentScheduleTransform cpt ON cpt.OldCustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
					INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
					WHERE p.PaymentID=@PaymentID

					INSERT OneVisionClaimPayment ( [OneVisionClaimID], [VisionPaymentID], [Gross], [AccountID], [AccountNumber], [SortCode], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified] )
					SELECT vcd.[OneVisionClaimID], vcd.[VisionPaymentID], vcd.[Gross], ac.AccountID, ac.AccountNumber, ac.SortCode, cps.NewCustomerPaymentScheduleID, @RunAsUserID, GETDATE(), @RunAsUserID, GETDATE()
					FROM @CustomerPaymentScheduleTransform cps 
					INNER JOIN dbo.OneVisionClaimPayment vcd WITH (NOLOCK) on vcd.CustomerPaymentScheduleID = cps.OldCustomerPaymentScheduleID
					INNER JOIN dbo.Account ac WITH (NOLOCK) on ac.AccountID = @AccountID

					PRINT CONVERT(VARCHAR,@@RowCount) + ' new VisionClaimPayments inserted for AccountID ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL')

					--EXEC _C00_SimpleValueIntoThirdPartyField 4532, @VarcharDate, @MatterID, @RunAsUserID, @EventLeadTypeID, @ClientID /*Scheduled Date of Payment Retry*/

					SELECT @NoteText = 'DD Failure Identified.  Retry scheduled for ' + ISNULL(@VarcharDate,'NULL') + ' Failure reason ' + ISNULL(@FailureReason, 'Not set')
					EXEC _C00_AddANote @CaseID, @SystemNoteTypeID, @NoteText, 0, @RunAsUserID, 0, 1 /*System*/
					
					EXEC dbo.Account__SetDateAndAmountOfNextPayment @AccountID

					UPDATE a
					SET ExpiryDate = dbo.fnAddWorkingDays(@NextPaymentDate,10) 
					FROM Account a
					WHERE a.AccountID = @AccountID

				END

			END

			IF @FailureReason = 'No Instruction'
			BEGIN
	
				IF @CountryID <> 14 /*Australia*/
				BEGIN

					/*Set the mandate to failed so we raise another request*/
					UPDATE a
					SET MandateStatusID = 5
					FROM AccountMandate a
					WHERE a.AccountID = @AccountID
					AND a.MandateStatusID IN (2,3)
				
				END


				EXEC dbo.Account__SetDateAndAmountOfNextPayment @AccountID

			END

			PRINT '@EventToAdd = ' + ISNULL(CONVERT(VARCHAR,@EventToAdd),'NULL')

			/*Add required event*/
			IF @EventToAdd > 0 /*And make sure we havent got one in the last hour..*/
			BEGIN

				/*No Instruction. Do the mandate thing...*/
				SELECT TOP 1 @LeadEventID = c.LatestLeadEventID, @EventTypeID = le.EventTypeID
				FROM Cases c WITH (NOLOCK)
				INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestNonNoteLeadEventID
				WHERE c.CaseID = @CaseID

				IF NOT EXISTS (
					SELECT *
					FROM LeadEvent le WITH (NOLOCK)
					WHERE le.CaseID = @CaseID 
					AND le.EventTypeID = @EventToAdd
					AND le.WhenCreated > DATEADD(HH, -1, dbo.fn_GetDate_Local())
				)
				BEGIN
					
					PRINT 'LE'
					PRINT @LeadEventID
					PRINT 'Event'
					PRINT @EventToAdd
					PRINT 'USER'
					PRINT @RunAsUserID 
					PRINT 'Case'
					PRINT @CaseID 
					PRINT '@EventTypeID'
					PRINT @EventTypeID
					
					SELECT @CommentsToApply = '</br>Failure Reason ' + ISNULL(@FailureReason, '') + '</br>AccountID ' + ISNULL(CONVERT(VARCHAR,@AccountID), '') + ' PaymentID ' + ISNULL(CONVERT(VARCHAR,@PaymentID), '')
											+ '</br>' + CONVERT(VARCHAR,(SELECT COUNT(*) FROM @CustomerPaymentScheduleTransform)) + ' New CustomerPaymentScheduleIDs</br>' 

					SELECT @CommentsToApply += CONVERT(VARCHAR,cps.NewCustomerPaymentScheduleID) + ' </br>'
					FROM @CustomerPaymentScheduleTransform cps 

					--EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID,@EventToAdd,@RunAsUserID, -1
					EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueueFull @ClientID, @LeadID, @CaseID, @LeadEventID, @EventTypeID, @EventToAdd, -1, @DateForEvent, @RunAsUserID, @CommentsToApply
					
					PRINT 'Added Queue Record from LeadEventID ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL') + ', CaseID ' + ISNULL(CONVERT(VARCHAR,@CaseID),'NULL')

				END
				
			END	

			/*		
			UPDATE AutomatedTask
			SET NextRunDateTime = DATEADD(MI, 1, dbo.fn_GetDate_Local())
			WHERE TaskID IN (20184,20185,20238)
			*/

		END
/*		ELSE
		BEGIN
		
			/*Ok if its not a retry in x days then we must be combining into the next scheduled payment date?*/
			
		END
*/		
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__PostPaymentProcedure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Payment__PostPaymentProcedure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__PostPaymentProcedure] TO [sp_executeall]
GO
