SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-06
-- Description:	Import Arudd Data to Arudd Processing Table
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_ImportAuddisAddacsData]
	 @XMLRaw VARCHAR(MAX),
	 @FileName VARCHAR(100) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XML XML = @XMLRaw,
			@ClientID INT = dbo.fnGetPrimaryClientID()

	INSERT INTO dbo.BACSMandateFailures (ClientID, FileType, FileName, RowNo, AccountNumber, SortCode, Name, NewAccountNumber, NewSortCode, NewName, BACSProcessedDate, FailureReason, FailureCode, Reference, ImportStatusID, ImportedDate, Comments)
	OUTPUT inserted.*
	SELECT @ClientID AS [ClientID],
			@XML.value('(/BACSDocument/Data/MessagingAdvices/MessagingHeader/@advice-type)[1]', 'varchar(2000)') AS [FileType], /*IE ADDACS, AUDDIS ETC*/
			@FileName AS [FileName],
			ROW_NUMBER() OVER(ORDER BY (SELECT 1)) AS [RowNum],
			n.c.value('(@payer-account-number)[1]' , 'varchar(2000)') AS [PayerAccountNumber],
			REPLACE(n.c.value('(@payer-sort-code)[1]' , 'varchar(2000)'), '-', '') AS [PayerSortCode],
			n.c.value('(@payer-name)[1]' , 'varchar(2000)') AS [PayerName],
			n.c.value('(@payer-new-account-number)[1]' , 'varchar(2000)') AS [NewAccountNumber],
			REPLACE(n.c.value('(@payer-new-sort-code)[1]' , 'varchar(2000)'), '-', '') AS [NewSortCode],
			n.c.value('(@payer-name)[1]' , 'varchar(2000)') AS [NewName],
			n.c.value('(@effective-date)[1]' , 'varchar(2000)') AS [BACSProcessedDate],
			NULL AS [FailureReason],
			n.c.value('(@reason-code)[1]' , 'varchar(2000)') AS [FailureCode],
			LTRIM(RTRIM(n.c.value('(@reference)[1]' , 'varchar(2000)'))) AS [Reference],
			1 AS [ImportStatusID],
			dbo.fn_GetDate_Local() AS [ImportedDate], 
			'' AS [Comments]
	FROM @XML.nodes('//BACSDocument/Data/MessagingAdvices/MessagingAdvice') n(c)

	EXEC dbo.Billing__ReconcileMandateFailures @ClientID

	SELECT *
	FROM BACSMandateFailures b WITH (NOLOCK)
	WHERE b.FileName = @FileName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportAuddisAddacsData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_ImportAuddisAddacsData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportAuddisAddacsData] TO [sp_executeall]
GO
