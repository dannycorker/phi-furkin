SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-24
-- Description:	Cancel all editing, using whichever ID is available
-- =============================================
CREATE PROCEDURE [dbo].[CancelSqlQueryForEditing]
	@SqlQueryID int = null,
	@SqlQueryEditingID int = null
AS
BEGIN
	SET NOCOUNT ON;

	IF @SqlQueryEditingID IS NOT NULL
	BEGIN
		DELETE SqlQueryEditing WHERE SqlQueryEditingID = @SqlQueryEditingID
	END	
	ELSE
	BEGIN
		IF @SqlQueryID IS NOT NULL
		BEGIN
			DELETE SqlQueryEditing WHERE SqlQueryID = @SqlQueryID
		END	
	END
END











GO
GRANT VIEW DEFINITION ON  [dbo].[CancelSqlQueryForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CancelSqlQueryForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CancelSqlQueryForEditing] TO [sp_executeall]
GO
