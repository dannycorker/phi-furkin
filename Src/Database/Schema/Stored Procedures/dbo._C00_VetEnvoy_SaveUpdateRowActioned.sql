SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-08-19
-- Description:	Saves the action date back to the update rows
-- =============================================
CREATE PROCEDURE [dbo].[_C00_VetEnvoy_SaveUpdateRowActioned]
(
	@ClientID INT,
	@TableRowIDs dbo.tvpInt READONLY
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Date VARCHAR(2000)
	SELECT @Date = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	
	INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, ClientID)
	SELECT AnyID, 162662, @Date, @ClientID
	FROM @TableRowIDs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_SaveUpdateRowActioned] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_VetEnvoy_SaveUpdateRowActioned] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_SaveUpdateRowActioned] TO [sp_executeall]
GO
