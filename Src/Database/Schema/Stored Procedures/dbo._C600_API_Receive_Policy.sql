SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-11-12
-- Description:	Take an XML package representing a policy and create the matter and case records within this database
-- 2019-12-20 CPS for JIRA LAGCLAIM-277 | Changed from attribute- to element-centric XML for Ian
--										| Updated to recieve and process full XML package directly rather than looking up from a table
-- 2019-12-23 CPS for JIRA LAGCLAIM-282 | Added missing filter to resource list updates
-- 2020-01-07 CR updated the proc so that Yes/No responses can be handled and added as part of the Pet transfer.
-- 2020-01-09 CR Added a the delete table rows for the Historical Policy 
-- 2020-01-09 MAB Validate input data for LAGCLAIM-290
-- 2020-01-10 MAB corrected filter for resource list updates, removed redundant mapping table column DecodeLookupListID, 
--					reformatted mapping table definition and population for clarity.
-- 2020-01-24 ALM refactored to call _C00_BulkValidateAndSaveDetailValuesFromXml to eliminate duplicate code in the "receive" procedures
-- 2020-01-24 MAB additions to ALM's refactor of same date.
-- 2020-07-14 ALM updated [dbo].[fn_C600_Harvest_Policy_JSON] to [dbo].[fn_C600_Mediate_Harvest_Policy]
-- =============================================
CREATE PROCEDURE [dbo].[_C600_API_Receive_Policy]
(
	 @XmlPayload		XML
	,@HttpMethod		VARCHAR(50)
	,@ObjectID			UNIQUEIDENTIFIER
	,@ParentObjectID	UNIQUEIDENTIFIER
	,@RunAsUserID		INT
	,@JsonResponse		NVARCHAR(MAX) OUTPUT
	,@ShowNewPolicy		BIT = 0
)
AS
BEGIN

	/*
	- Collect and Read the XML
	- Make sure we have enough information to proceed including a mapped CustomerID and PetID
	- Look for existing policies and determine whether we are inserting or updating
	
	INSERT
	- Create new Case and Matter record, using existing variables
	- Collect and return the new MatterID

	UPDATE
	- Locate and update existing Matter
	*/
	DECLARE	 
		 @RefLetter				VARCHAR(3)
		,@CaseNum				INT = 1
		,@NewCustomerID			INT
		,@NewLeadID				INT
		,@NewCaseID				INT
		,@NewMatterID			INT
		,@ClientID				INT				= dbo.fnGetPrimaryClientID()
		,@DateTimeCompleted		DATETIME2		= CURRENT_TIMESTAMP
		,@ErrorMessage			VARCHAR(2000)	= ''
		,@NewLeadTypeID			INT				= 1492 /*Policy Admin*/
		,@PolicyTermsXML		XML
		,@ObjectIDVchr			VARCHAR(40)		= CONVERT(VARCHAR(40), @ObjectID)
		,@HttpMethodID			INT
		,@ErrorMessageSeparator	VARCHAR(3)		= ' | '
	
	DECLARE	@MatterFieldMapping tvpXmlToDetailFieldMap				/*XML node to Matter Detail Field Mapping*/
	DECLARE @PolicyTermsTableFieldMapping tvpXmlToDetailFieldMap	/*XML node to Policy Terms AQ Table Mapping*/

	/*
		XML to Detail Field Mapping Configuration Data
		----------------------------------------------
	*/

	/*MATTER Detail Field Mapping*/
	INSERT @MatterFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 170050, 			'reference',				'TEXT', 		NULL, 								NULL	)	/*Policy Number*/
		,( 170038, 			'status',					'TEXT', 		NULL, 								NULL	)	/*Policy Status*/
		,( 170034, 			'productName',				'TEXT', 		146200, 							NULL	)	/*Current Policy*/
		,( 170035, 			'inceptionDate',			'DATE', 		NULL, 								NULL	)	/*Policy Inception Date*/
		,( 170036, 			'startDate',				'DATE', 		NULL, 								NULL	)	/*Policy Start Date*/
		,( 170037, 			'endDate',					'DATE', 		NULL, 								NULL	)	/*Policy End Date*/
		,( 170170, 			'bankName',					'TEXT', 		NULL, 								NULL	)	/*Bank Name*/
		,( 170104, 			'payee',					'TEXT', 		NULL, 								NULL	)	/*Account Name*/
		,( 170105, 			'sortCode',					'TEXT', 		NULL, 								NULL	)	/*Account Sort Code*/
		,( 170106, 			'accountNumber',			'TEXT', 		NULL, 								NULL	)	/*Account Number*/
		,( 170114, 			'paymentMethod',			'TEXT', 		NULL, 								NULL	)	/*Payment Method*/
		,( 170176, 			'paymentInterval',			'TEXT', 		NULL, 								NULL	)	/*PPayment Interval*/
							
	/*POLICY TERMS Table Detail Field Mapping*/
	INSERT @PolicyTermsTableFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 145663,			'startDate',				'DATE',			NULL,								NULL	)	/*Start*/
		,( 145664,			'endDate',					'DATE',			NULL,								NULL	)	/*End*/
		,( 145662,			'inceptionDate',			'DATE',			NULL,								NULL	)	/*Item Not Covered*/
		,( 145665,			'productName',				'TEXT',			146200,								NULL	)	/*Item Not Covered*/
		,( 145666,			'status',					'TEXT',			NULL,								'43002'	)	/*Status (default=Live)*/

	/*
		Match Policy to its Parent AQ Objects or Create New 
		--------------------------------------------------
	*/	

	/*Pick up the Customer ID and policy admin Lead ID from GUIDs. The customer and pet will have come across in a previous transaction.*/
	SELECT	 @NewLeadID		= ldv.LeadID
			,@NewCustomerID = l.CustomerID
	FROM LeadDetailValues ldv WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = ldv.LeadID
	WHERE ldv.DetailFieldID = 313787 /*API Transfer Mapping GUID*/
	AND ldv.DetailValue = CONVERT(VARCHAR(40), @ParentObjectID)
	 
	IF @NewLeadID is NULL
	BEGIN
		SELECT @ErrorMessage += 'No mapped Policy Admin LeadID or CustomerID found for ParentObjectID ' + ISNULL(CONVERT(VARCHAR(40), @ParentObjectID),'NULL') + @ErrorMessageSeparator
	END

	IF @HttpMethod = 'PUT'	/*= SQL Update*/
	BEGIN
		/*Get the existing policy matter ID by matching the mapping GUID that was saved when the matter was created*/
		SELECT TOP 1 @NewMatterID	= mdv.MatterID
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.DetailFieldID = 313792 /*API Transfer Mapping GUID*/
		AND mdv.DetailValue = @ObjectIDVchr

		IF @NewMatterID is NULL
		BEGIN
			SELECT @ErrorMessage += 'No mapped Policy Admin MatterID found for ObjectID ' + ISNULL(CONVERT(VARCHAR(40), @ObjectID),'NULL') + @ErrorMessageSeparator
		END
	END

	/*If a validation error occurred, raise it - it will be caught in the calling procedure _C00_Mediate_Pop*/
	IF @ErrorMessage > ''
	BEGIN
		SELECT @ErrorMessage = SUBSTRING(@ErrorMessage, 1, LEN(@ErrorMessage)-(LEN(@ErrorMessageSeparator)-1)) /*Remove the final error separator*/

		RAISERROR (@ErrorMessage, 16, 1)
		
		RETURN
	END

	IF @HttpMethod = 'PUT'	/*= SQL Update*/
	BEGIN
		/*Get the existing case for the claim matter*/
		SELECT @NewCaseID = m.CaseID
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @NewMatterID

		/*Record when changes were made and who made them.*/
		UPDATE m
		SET  WhenModified	= @DateTimeCompleted
			,WhoModified	= @RunAsUserID
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @NewMatterID
	END
	ELSE	/*HTTP Method = POST = SQL Insert*/
	BEGIN
		/*Get the next case number*/
		SELECT TOP 1 @CaseNum = c.CaseNum + 1
		FROM Cases c WITH (NOLOCK) 
		WHERE c.LeadID = @NewLeadID
		ORDER BY c.CaseNum DESC
		
		/*Create a new Case*/
		INSERT INTO Cases (LeadID,ClientID,CaseNum,CaseRef,ClientStatusID,AquariumStatusID,DefaultContactID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@NewLeadID,@ClientID,@CaseNum,'Case ' + CONVERT(VARCHAR,(@CaseNum + 1)),NULL,2,NULL, @RunAsUserID, @DateTimeCompleted, @RunAsUserID, @DateTimeCompleted)

		SELECT @NewCaseID = SCOPE_IDENTITY()

		/*Get the case reference for the newly created case*/
		SELECT @RefLetter = dbo.fnRefLetterFromCaseNum(1+COUNT(*)) 
		FROM Matter WITH (NOLOCK)
		WHERE LeadID = @NewLeadID

		/*Create a new Matter*/
		INSERT INTO Matter([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID], WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@ClientID,'',@NewCustomerID,@NewLeadID,1,@RefLetter,0,@NewCaseID, @RunAsUserID, @DateTimeCompleted, @RunAsUserID, @DateTimeCompleted)

		SELECT @NewMatterID = SCOPE_IDENTITY()

		/*
			Default to the customer match equivalent of "this is a new customer", so that linked leads will be  
			created by the "Create linked leads and complete set-up" event later.
		*/
		EXEC dbo._C00_SimpleValueIntoField 175280/*Type of customer match*/, '72011'/*Match not claimed*/, @NewMatterID, @RunAsUserID 
		
		/*Save the mapping GUID in the new policy so it can be matched for future updates.*/
		EXEC dbo._C00_SimpleValueIntoField 313792/*API Transfer Mapping GUID*/, @ObjectIDVchr, @NewMatterID, @RunAsUserID
	END
	
	/*Save the last HTTP method that was applied to the claim matter*/
	SELECT @HttpMethodID = lli.LookupListItemID
	FROM LookupListItems lli WITH (NOLOCK)
	WHERE lli.LookupListID = 11071 /*HTTP Method*/
	AND lli.ItemValue = @HttpMethod

	EXEC dbo._C00_SimpleValueIntoField 313793/*API Transfer Method*/, @HttpMethodID, @NewMatterID, @RunAsUserID

	/*
		Save Data to Detail Fields
		--------------------------
	*/

	/*Validate and save the Matter detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @XmlPayload
		,@XmlToDetailFieldMap	= @MatterFieldMapping
		,@ObjectID				= @NewMatterID
		,@ObjectTypeID			= 2 /*Matter*/
		,@TableDetailFieldID	= NULL
		,@RunAsUserID			= @RunAsUserID


	/* Extract, restructure and sort the source Historical Policy table data*/
	SET @PolicyTermsXML = (
		SELECT PolicyTerm.query('./*')
		FROM @XmlPayload.nodes('(//policyTerms/PolicyTerm)') policyTerms(PolicyTerm)
		ORDER BY PolicyTerm.value('(startDate)[1]','DATE')
		FOR XML RAW ('tablerow'), ROOT ('tablerows')
	)

	/*Validate and save the Historical Policy table detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @PolicyTermsXML
		,@XmlToDetailFieldMap	= @PolicyTermsTableFieldMapping
		,@ObjectID				= @NewMatterID
		,@ObjectTypeID			= 2 /*Matter*/
		,@TableDetailFieldID	= 170033 /*Historical Policy*/
		,@RunAsUserID			= @RunAsUserID

	/*
		Return Response, Debug Display (if required) and New AQ Object ID
		-----------------------------------------------------------------
	*/

	/*Assemble response and return it*/
	SELECT @JsonResponse = ISNULL([dbo].[fn_C600_Mediate_Harvest_Policy](@NewMatterID,0), '{}')

	/*Return the whole policy object*/
	IF @ShowNewPolicy = 1
	BEGIN
		/*Matter detail fields*/
		SELECT	 'Matter DF'
				,m.CustomerID		[CustomerID]
				,m.LeadID			[LeadID]
				,m.MatterID			[MatterID]
				,mdv.DetailFieldID	[DetailFieldID]
				,mdv.DetailValue	[DetailValue]
		FROM Matter m WITH (NOLOCK) 
		CROSS APPLY @MatterFieldMapping mfm
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID 
		AND mdv.DetailFieldID = mfm.DetailFieldID
		WHERE m.MatterID = @NewMatterID
		UNION
		SELECT	 'Matter DF'
				,m.CustomerID		[CustomerID]
				,m.LeadID			[LeadID]
				,m.MatterID			[MatterID]
				,mdv.DetailFieldID	[DetailFieldID]
				,mdv.DetailValue	[DetailValue]
		FROM Matter m WITH (NOLOCK) 
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID 
		AND mdv.DetailFieldID = 175280 /*Type of customer match*/
		WHERE m.MatterID = @NewMatterID
		ORDER BY mdv.DetailFieldID

		/*Historical Policy table detail fields*/
		SELECT	 'Historical Policy table DF'
				,m.CustomerID		[MappedCustomerID]
				,m.LeadID			[MappedParentID]
				,m.MatterID			[MappedClaimID]
				,tdv.TableRowID		[TableRowID]
				,tdv.DetailFieldID	[DetailFieldID]
				,tdv.DetailValue	[DetailValue]
				,tdv.ResourceListID	[ResourceListID]
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID
			AND tr.DetailFieldID = 170033 /*Historical Policy*/
		CROSS APPLY @PolicyTermsTableFieldMapping dtfm
		LEFT JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			AND tdv.DetailFieldID = dtfm.DetailFieldID
		WHERE m.MatterID = @NewMatterID
		ORDER BY tdv.TableRowID
			,tdv.DetailFieldID
	END

	RETURN @NewMatterID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Policy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_API_Receive_Policy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Policy] TO [sp_executeall]
GO
