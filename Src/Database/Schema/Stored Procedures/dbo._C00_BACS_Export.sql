SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-11-30
-- Description:	BACS Export
--				@LeadOrMatter 1=Lead, 2=Matter, 6/8=Table
--				@ExportType 1 = HSBC BACS Standard 18
-- =============================================
CREATE PROCEDURE [dbo].[_C00_BACS_Export] 
@Debug int = 0,
@ExportType int = 1,
@PaymentLeadORMatter int = 8,
@AccountLeadOrMatter int = 2,
@SUN varchar(6) = '      ',
@PaymentDateDetailFieldID int,
@PaymentAmountDetailFieldID int,
@DateForPayment date,
@SortCodeDetailFieldID int,
@SortCodeRLFieldID int,
@AccountNumberDetailFieldID int,
@AccountNameDetailFieldID int,
@OriginatingSort char(6),
@OriginatingAcct char(8),
@OrigAccountName char(18),
@ProcessingDay varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @CurrentDate varchar(10), 
			@RecordCount varchar(10),
			@TotalValue money

	DECLARE @PTCPF TABLE (
			TableRowID int,
			Contents varchar(1000)
		)
		
	DECLARE @Payments TABLE(
			LeadID int,
			MatterID int,
			AccountName varchar(18),
			SortCode varchar(6),
			AccountNumber varchar(8),
			Amount money
			)

	Select	@CurrentDate=CONVERT(VARCHAR(6), dbo.fn_GetDate_Local()+1, 12),
			@ProcessingDay = CONVERT(Varchar(2),RIGHT(DATEPART(yy, dbo.fn_GetDate_Local()), 2)) + CONVERT(Varchar(3),DATEPART(dy, dbo.fn_GetDate_Local()+1)),
			@DateForPayment = CASE WHEN @DateForPayment IS NULL THEN CONVERT(Date, dbo.fn_GetDate_Local()+1) ELSE @DateForPayment END

	/*If Payment is coming from lead, matter, table get lead/matter/tableid*/
	IF @PaymentLeadORMatter = 1
	BEGIN

		INSERT INTO @Payments (LeadID, MatterID, Amount)
		SELECT ldv.LeadID, 0, ldv_amount.ValueMoney
		From MatterDetailValues ldv WITH (NOLOCK)
		INNER JOIN LeadDetailValues ldv_amount WITH (NOLOCK) ON ldv_amount.LeadID = ldv.LeadID and ldv_amount.DetailFieldID = @PaymentAmountDetailFieldID
		Where ldv.DetailFieldID = @PaymentDateDetailFieldID
		and ldv.ValueDate = @DateForPayment

	END
	ELSE
	IF @PaymentLeadORMatter = 2
	BEGIN

		INSERT INTO @Payments (LeadID, MatterID, Amount)
		SELECT mdv.LeadID, mdv.MatterID, mdv_amount.ValueMoney
		From MatterDetailValues mdv WITH (NOLOCK)
		INNER JOIN MatterDetailValues mdv_amount WITH (NOLOCK) ON mdv_amount.MatterID = mdv.MatterID and mdv_amount.DetailFieldID = @PaymentAmountDetailFieldID
		Where mdv.DetailFieldID = @PaymentDateDetailFieldID
		and mdv.ValueDate = @DateForPayment

	END
	ELSE IF @PaymentLeadORMatter IN (6,8)
	BEGIN
	
		INSERT INTO @Payments (LeadID, MatterID, Amount)
		SELECT tdv.LeadID, tdv.MatterID, tdv_amount.ValueMoney
		From TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN TableDetailValues tdv_amount WITH (NOLOCK) ON tdv_amount.TableRowID = tdv.TableRowID and tdv_amount.DetailFieldID = @PaymentAmountDetailFieldID
		Where tdv.DetailFieldID = @PaymentDateDetailFieldID
		and tdv.ValueDate = @DateForPayment

	END

	IF @AccountLeadOrMatter = 1
	BEGIN
	
		Update p 
		SET AccountNumber = ldv_ac_number.DetailValue, SortCode = ISNULL(rldv.DetailValue, ldv_sort.DetailValue), AccountName = Left (Upper(ldv_ac_name.DetailValue),18)
		FROM @Payments p
		INNER JOIN LeadDetailValues ldv_sort WITH (NOLOCK) ON ldv_sort.LeadID = p.LeadID and ldv_sort.DetailFieldID = @SortCodeDetailFieldID
		LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = ldv_sort.ValueInt and rldv.DetailFieldID = @SortCodeRLFieldID
		INNER JOIN LeadDetailValues ldv_ac_number WITH (NOLOCK) ON ldv_ac_number.LeadID = p.LeadID and ldv_ac_number.DetailFieldID = @AccountNumberDetailFieldID
		INNER JOIN LeadDetailValues ldv_ac_name WITH (NOLOCK) ON ldv_ac_name.LeadID = p.LeadID and ldv_ac_name.DetailFieldID = @AccountNameDetailFieldID


	END
	ELSE
	IF @AccountLeadOrMatter = 2
	BEGIN
	
		Update p 
		SET AccountNumber = LEFT(mdv_ac_number.DetailValue,8), SortCode = LEFT(ISNULL(rldv.DetailValue, mdv_sort.DetailValue),6), AccountName = Left (Upper(mdv_ac_name.DetailValue),18)
		FROM @Payments p
		INNER JOIN MatterDetailValues mdv_sort WITH (NOLOCK) ON mdv_sort.LeadID = p.LeadID and mdv_sort.DetailFieldID = @SortCodeDetailFieldID
		LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv_sort.ValueInt and rldv.DetailFieldID = @SortCodeRLFieldID
		INNER JOIN MatterDetailValues mdv_ac_number WITH (NOLOCK) ON mdv_ac_number.LeadID = p.LeadID and mdv_ac_number.DetailFieldID = @AccountNumberDetailFieldID
		INNER JOIN MatterDetailValues mdv_ac_name WITH (NOLOCK) ON mdv_ac_name.LeadID = p.LeadID and mdv_ac_name.DetailFieldID = @AccountNameDetailFieldID

	END

	IF @ExportType = 1
	BEGIN

		/* Vol Record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 1, 'VOL1' + replace(convert(varchar(8),convert(time,dbo.fn_GetDate_Local())), ':', '') + '                     HSBC      ' + @SUN + '                                1' as [DD]

		/* Header 1 record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 2, 'HDR1A' + @SUN + 'S  1      00000100010001       ' + @ProcessingDay + ' ' + @ProcessingDay + ' 000000                    ' as [DD]

		/* Header 2 record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 3, 'HDR2F0200000100                                   00                            ' as [DD]

		/* UHL Record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 4, 'UHL1 ' + @ProcessingDay + '999999    000000001 DAILY  001                                        ' as [DD]

		/* Standard Record */
		Insert into @PTCPF(TableRowID,Contents)
		Select distinct 4, replace(p.SortCode,'-','') + p.AccountNumber + '0' + '99' + @OriginatingSort + @OriginatingAcct + '    ' + RIGHT(REPLICATE('0', 11) + CAST(replace(p.Amount,'.', '') AS VARCHAR(11)), 11) + @OrigAccountName + LEFT(CAST(p.LeadID AS VARCHAR(18)) + REPLICATE(' ', 18), 18) + LEFT(CAST(p.AccountName AS VARCHAR(18)) + REPLICATE(' ', 18), 18)
		From @Payments p

		Select @RecordCount = @@ROWCOUNT

		Select @TotalValue = SUM(p.Amount)
		From @Payments p

		/* Contra Record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 5, @OriginatingSort + @OriginatingAcct + '099' +  @OriginatingSort + @OriginatingAcct + '    ' + RIGHT(REPLICATE('0', 11) + CAST(replace(@TotalValue,'.', '') AS VARCHAR(11)), 11) + @ProcessingDay + '001          CONTRA            ' + @OrigAccountName

		/* End of file 1 record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 6, 'EOF1A' + @SUN + 'S  1      00000100010001       ' + @ProcessingDay + ' ' + @ProcessingDay + ' 000000                    ' as [DD]

		/* End of file 2 record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 7, 'EOF2F0200000100                                   00                            ' as [DD]

		/* UTL1 record */
		Insert into @PTCPF(TableRowID,Contents)
		Select 8, 'UTL1' + RIGHT(REPLICATE('0', 13) + CAST(replace(@TotalValue,'.', '') AS VARCHAR(13)), 13) + RIGHT(REPLICATE('0', 13) + CAST(replace(@TotalValue,'.', '') AS VARCHAR(13)), 13) + '0000001' + RIGHT(REPLICATE('0', 7) + CAST(@RecordCount AS VARCHAR(7)), 7) + '                                    ' as [DD]

	END

	IF @RecordCount > 0 AND @Debug = 0 
	BEGIN

		Select Contents
		From @PTCPF
		Order By TableRowID

	END

	IF @Debug = 1 
	BEGIN
	
		SELECT * 
		From @Payments
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BACS_Export] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BACS_Export] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BACS_Export] TO [sp_executeall]
GO
