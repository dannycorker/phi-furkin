SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ResultCode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResultCode_Get_List]

AS


				
				SELECT
					[ResultCodeID],
					[Description]
				FROM
					[dbo].[ResultCode] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResultCode_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Get_List] TO [sp_executeall]
GO
