SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectType_Delete]
(

	@ObjectTypeID int   
)
AS


				DELETE FROM [dbo].[ObjectType] WITH (ROWLOCK) 
				WHERE
					[ObjectTypeID] = @ObjectTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Delete] TO [sp_executeall]
GO
