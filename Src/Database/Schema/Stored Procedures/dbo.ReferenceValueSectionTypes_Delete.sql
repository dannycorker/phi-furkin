SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReferenceValueSectionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueSectionTypes_Delete]
(

	@ReferenceValueSectionTypeID int   
)
AS


				DELETE FROM [dbo].[ReferenceValueSectionTypes] WITH (ROWLOCK) 
				WHERE
					[ReferenceValueSectionTypeID] = @ReferenceValueSectionTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueSectionTypes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Delete] TO [sp_executeall]
GO
