SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_DeleteDisabledFieldsAndValues]
(
@LeadEventID int
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@AquariumClientID int,
		@myERROR int

	Select @AquariumClientID = ldv.detailvalue
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	Inner join leaddetailvalues ldv on ldv.leadid = lead.leadid and ldv.detailfieldid = 6453
	where leadevent.leadeventid = @LeadEventID 

if @AquariumClientID is not null
begin

Begin Tran

	delete dvh
	from detailfields df
	inner join detailvaluehistory dvh on dvh.detailfieldid = df.detailfieldid
	where enabled = 0
	and dvh.clientid = @AquariumClientID 

	delete mdv
	from detailfields df
	inner join matterdetailvalues mdv on mdv.detailfieldid = df.detailfieldid
	where enabled = 0
	and mdv.clientid = @AquariumClientID 

	delete ldv
	from detailfields df
	inner join leaddetailvalues ldv on ldv.detailfieldid = df.detailfieldid
	where enabled = 0
	and ldv.clientid = @AquariumClientID 

	delete dfl
	from detailfields df
	inner join detailfieldlink dfl on dfl.detailfieldid = df.detailfieldid
	where enabled = 0
	and dfl.clientid = @AquariumClientID 

	delete df
	from detailfields df
	where enabled = 0
	and df.clientid = @AquariumClientID 

END

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	GOTO END_NOW

HANDLE_ERROR:
    ROLLBACK TRAN

END_NOW:


END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_DeleteDisabledFieldsAndValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_DeleteDisabledFieldsAndValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_DeleteDisabledFieldsAndValues] TO [sp_executeall]
GO
