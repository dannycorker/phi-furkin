SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.UpdateQuestionnaireColour    Script Date: 08/09/2006 12:22:42 ******/

CREATE PROCEDURE [dbo].[UpdateQuestionnaireColour]
@QuestionnaireColourID int,
@ClientQuestionnaireID int,
@Page nvarchar(50),
@Header nvarchar(50),
@Intro nvarchar(50),
@Body nvarchar(50),
@Footer nvarchar(50),
@ClientID int = null

as

Update QuestionnaireColours

Set ClientQuestionnaireID = @ClientQuestionnaireID,
Page = @Page,
Header = @Header,
Intro = @Intro,
Body = @Body,
Footer = @Footer

where QuestionnaireColourID = @QuestionnaireColourID





GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateQuestionnaireColour] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateQuestionnaireColour] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateQuestionnaireColour] TO [sp_executeall]
GO
