SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-18
-- Description:	Get the next reference value int from a detail field
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetNextRefValueInt] 
	@DetailFieldID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ValueInt int
	
	BEGIN TRAN
	
		/* 
			Increment the last used value by 1 first. 
			Do this within a transaction so that nothing else can read it
			until we have finished.
		*/
		UPDATE dbo.DetailFields 
		SET LastReferenceInteger = COALESCE(LastReferenceInteger, 0) + 1 
		WHERE DetailFieldID = @DetailFieldID 
		
		/*
			Now read the new value, commit the tran, and return the new value.
		*/
		SELECT @ValueInt = df.LastReferenceInteger 
		FROM dbo.DetailFields df 
		WHERE df.DetailFieldID = @DetailFieldID 
		
	COMMIT
	
	RETURN @ValueInt 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetNextRefValueInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetNextRefValueInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetNextRefValueInt] TO [sp_executeall]
GO
