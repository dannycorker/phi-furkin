SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-11-2012
-- Description:	Counts the number of events that are within the given event group
-- =============================================
CREATE PROCEDURE [dbo].[EventGroupMember_CountEvents]

	@EventGroupID INT

AS
BEGIN

	SELECT COUNT(*) EventCount FROM EventGroupMember WITH (NOLOCK) 
	WHERE EventGroupID=@EventGroupID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember_CountEvents] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroupMember_CountEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember_CountEvents] TO [sp_executeall]
GO
