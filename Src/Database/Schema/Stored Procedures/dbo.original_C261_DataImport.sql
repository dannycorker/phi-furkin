SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2013
-- Description:	Imports xml data into client 261 Phones4U
-- Modified: PR 20-09-2013 - Added support for second lead type 1260
-- =============================================
CREATE PROCEDURE [dbo].[original_C261_DataImport]
	
	@LeadTypeID INT,
	@XmlRaw NVARCHAR(MAX)

AS
BEGIN

	--DECLARE @ClientID INT = 2
	--DECLARE @LeadTypeID INT = 50 
	
	DECLARE @ClientID INT = 261
	
	
	DECLARE @XMLFileSequenceNo VARCHAR(2000)
	
	DECLARE @Xml XML
	SELECT @Xml = REPLACE(@XmlRaw, '<?xml version="1.0" encoding="utf-16"?>', '')

	SELECT @XMLFileSequenceNo = n.c.value('(Header/FileSequenceNo)[1]', 'varchar(2000)')
	FROM @Xml.nodes('//Main') n(c)

	DECLARE @ImportData TABLE (
		RowID INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
		StoreName VARCHAR(2000),
		StoreNumber VARCHAR(2000),
		EmployeeFirstName VARCHAR(2000),
		EmployeeLastName VARCHAR(2000),
		EmployeeNumber VARCHAR(2000),
		CustomerTitle VARCHAR(2000),
		CustomerFirstName VARCHAR(2000),
		CustomerLastName VARCHAR(2000),
		CustomerDOB VARCHAR(2000),
		CustomerPostCode VARCHAR(2000),
		CustomerTown VARCHAR(2000),
		MainMobile VARCHAR(2000),
		AlternateMobile VARCHAR(2000),
		TransactionDate VARCHAR(2000),
		TransactionTime VARCHAR(2000),
		TransactionReferenceNumber VARCHAR(2000),
		TransactionType VARCHAR(2000),
		PortedFlag VARCHAR(2000),
		Network VARCHAR(2000),
		Manufacturer VARCHAR(2000),
		Model VARCHAR(2000),
		LeadID INT,
		ERROR VARCHAR(2000)
	);

	DECLARE @DuplicateData TABLE (
		RowID INT, 
		StoreName VARCHAR(2000),
		StoreNumber VARCHAR(2000),
		EmployeeFirstName VARCHAR(2000),
		EmployeeLastName VARCHAR(2000),
		EmployeeNumber VARCHAR(2000),
		CustomerTitle VARCHAR(2000),
		CustomerFirstName VARCHAR(2000),
		CustomerLastName VARCHAR(2000),
		CustomerDOB VARCHAR(2000),
		CustomerPostCode VARCHAR(2000),
		CustomerTown VARCHAR(2000),
		MainMobile VARCHAR(2000),
		AlternateMobile VARCHAR(2000),
		TransactionDate VARCHAR(2000),
		TransactionTime VARCHAR(2000),
		TransactionReferenceNumber VARCHAR(2000),
		TransactionType VARCHAR(2000),
		PortedFlag VARCHAR(2000),
		Network VARCHAR(2000),
		Manufacturer VARCHAR(2000),
		Model VARCHAR(2000),	
		LeadID INT,	
		ERROR VARCHAR(2000)
	);

	INSERT INTO @ImportData (StoreName, StoreNumber, EmployeeFirstName,EmployeeLastName,EmployeeNumber,
							 CustomerTitle, CustomerFirstName, CustomerLastName, CustomerDOB, CustomerPostCode, CustomerTown,
							 MainMobile, AlternateMobile, TransactionDate, TransactionTime, TransactionReferenceNumber, 
							 TransactionType, PortedFlag, Network, Manufacturer, Model)	
	SELECT 
		n.c.value('(StoreDetails/StoreName)[1]', 'varchar(2000)') AS StoreName,
		n.c.value('(StoreDetails/TeamNumber)[1]', 'varchar(2000)') AS StoreNumber,
		n.c.value('(EmployeeDetails/EmployeeFirstName)[1]', 'varchar(2000)') AS EmployeeFirstName,
		n.c.value('(EmployeeDetails/EmployeeSurname)[1]', 'varchar(2000)') AS EmployeeLastName,
		n.c.value('(EmployeeDetails/EmployeeId)[1]', 'varchar(2000)') AS EmployeeNumber,
		n.c.value('(CustomerDetails/CustomerTitle)[1]', 'varchar(2000)') AS CustomerTitle,
		n.c.value('(CustomerDetails/CustomerFirstName)[1]', 'varchar(2000)') AS CustomerFirstName,
		n.c.value('(CustomerDetails/CustomerSurname)[1]', 'varchar(2000)') AS CustomerLastName,
		n.c.value('(CustomerDetails/CustomerDOB)[1]', 'varchar(2000)') AS CustomerDOB,
		n.c.value('(CustomerDetails/CustomerPostData/CustomerPostCode)[1]', 'varchar(2000)') AS CustomerPostCode,
		n.c.value('(CustomerDetails/CustomerPostData/CustomerPostTown)[1]', 'varchar(2000)') AS CustomerTown,
		n.c.value('(OrderDetails/MobileNo)[1]', 'varchar(2000)') AS MainMobile,
		n.c.value('(OrderDetails/MobileNo2)[1]', 'varchar(2000)') AS AlternateMobile,
		n.c.value('(OrderDetails/TransactionDateTime/TransactionDate)[1]', 'varchar(2000)') AS TransactionDate,
		n.c.value('(OrderDetails/TransactionDateTime/TransactionTime)[1]', 'varchar(2000)') AS TransactionTime,
		n.c.value('(OrderDetails/TransactionReferenceNumber)[1]', 'varchar(2000)') AS TransactionReferenceNumber,
		n.c.value('(OrderDetails/TransactionType)[1]', 'varchar(2000)') AS TransactionType,
		n.c.value('(OrderDetails/PortedFlag)[1]', 'varchar(2000)') AS PortedFlag,
		n.c.value('(OrderDetails/Network)[1]', 'varchar(2000)') AS Network,
		n.c.value('(OrderDetails/Manufacturer)[1]', 'varchar(2000)') AS Manufacturer,
		n.c.value('(OrderDetails/Model)[1]', 'varchar(2000)') AS Model
	FROM @Xml.nodes('//SalesDetail') n(c)

	--Create table with duplicate records - these are the ones that do not have a unique MainMobile
	INSERT INTO @DuplicateData(RowID, StoreName, StoreNumber, EmployeeFirstName,EmployeeLastName,EmployeeNumber,
							 CustomerTitle, CustomerFirstName, CustomerLastName, CustomerDOB, CustomerPostCode, CustomerTown,
							 MainMobile, AlternateMobile, TransactionDate, TransactionTime, TransactionReferenceNumber, 
							 TransactionType, PortedFlag, Network, Manufacturer, Model, LeadID, ERROR)	
    SELECT id.* FROM @ImportData id 
    WHERE EXISTS
	(
		SELECT 1
		FROM @ImportData id2
		WHERE id.RowID <> id2.RowID AND id.MainMobile = id2.MainMobile
	)

	DECLARE @RowsToProcess  INT 
	DECLARE @CurrentRow     INT
	
	SELECT @RowsToProcess = COUNT(*) FROM @ImportData
	
	DECLARE @StoreName VARCHAR(2000), 
			@StoreNumber VARCHAR(2000), 
			@EmployeeFirtsName VARCHAR(2000),
			@EmployeeLastName VARCHAR(2000),
			@EmployeeNumber VARCHAR(2000),
			@CustomerTitle VARCHAR(2000), 
			@CustomerFirstName VARCHAR(2000), 
			@CustomerLastName VARCHAR(2000), 
			@CustomerDOB VARCHAR(2000), 
			@CustomerPostCode VARCHAR(2000), 
			@CustomerTown VARCHAR(2000),
			@MainMobile VARCHAR(2000), 
			@AlternateMobile VARCHAR(2000), 
			@TransactionDate VARCHAR(2000), 
			@TransactionTime VARCHAR(2000), 
			@TransactionReferenceNumber VARCHAR(2000), 
			@TransactionType VARCHAR(2000), 
			@PortedFlag VARCHAR(2000), 
			@Network VARCHAR(2000), 
			@Manufacturer VARCHAR(2000), 
			@Model VARCHAR(2000),
			@CustomerID INT, 
			@LeadID INT, 
			@CaseID INT, 
			@MatterID INT,
			@TitleID INT

	SET @CurrentRow=0
	WHILE @CurrentRow<@RowsToProcess
	BEGIN
	
		SET @CurrentRow=@CurrentRow+1
		
		SELECT  					
			@StoreName=StoreName,
			@StoreNumber=StoreNumber,
			@EmployeeFirtsName=EmployeeFirstName,
			@EmployeeLastName=EmployeeLastName,
			@EmployeeNumber=EmployeeNumber,
			@CustomerTitle=CustomerTitle,
			@CustomerFirstName=CustomerFirstName,
			@CustomerLastName=CustomerLastName,
			@CustomerDOB=CustomerDOB,
			@CustomerPostCode=CustomerPostCode,
			@CustomerTown=CustomerTown,
			@MainMobile=
					CASE 
						WHEN MainMobile IS NULL THEN '' 
						WHEN DATALENGTH(REPLACE(MainMobile, ' ', '')) < 10 THEN ''
						WHEN MainMobile LIKE '+44%' THEN REPLACE(REPLACE(MainMobile, '+44', '0'),' ','')
						WHEN MainMobile LIKE '44%' THEN REPLACE('0' + RIGHT(MainMobile,LEN(MainMobile)-2), ' ', '')
						ELSE Replace(MainMobile, ' ', '')
					END,
			@AlternateMobile=
					CASE 
						WHEN AlternateMobile IS NULL THEN '' 
						WHEN DATALENGTH(REPLACE(AlternateMobile, ' ', '')) < 10 THEN ''
						WHEN AlternateMobile LIKE '+44%' THEN REPLACE(REPLACE(AlternateMobile, '+44', '0'),' ','')
						WHEN AlternateMobile LIKE '44%' THEN REPLACE('0' + RIGHT(AlternateMobile,LEN(AlternateMobile)-2), ' ', '')	
						ELSE Replace(AlternateMobile, ' ', '')
					END,			
			@TransactionDate=TransactionDate,
			@TransactionTime=TransactionTime,
			@TransactionReferenceNumber=TransactionReferenceNumber,
			@TransactionType=TransactionType,
			@PortedFlag=PortedFlag,
			@Network=Network,
			@Manufacturer=Manufacturer,
			@Model=Model			
		FROM @ImportData i
		WHERE RowID=@CurrentRow 
		
		IF @CurrentRow NOT IN (SELECT RowID FROM @DuplicateData)
		BEGIN

			SELECT @TitleID=TitleID FROM Titles WHERE Title = @CustomerTitle
			IF @TitleID IS NULL
			BEGIN 
				SET @TitleID=0 --unknown title
			END 

			--Remove old mobile number from customer
			UPDATE Customers
			SET MobileTelephone=''
			WHERE MobileTelephone=@MainMobile AND ClientID=@ClientID
			
			--Insert New Customer, Lead For LeadType 1245, Case and Matter
			EXEC @CustomerID = _C00_CreateNewCustomer @ClientID, @LeadTypeID, @TitleID, @CustomerFirstName, @CustomerLastName, '', 0, '','','',@CustomerTown,'',@CustomerPostCode,'',23396
			
			IF @CustomerID>0 -- customer successfully inserted
			BEGIN
			
				--Retrieve identities			
				SELECT TOP 1 @MatterID=m.MatterID, @CaseID=c.CaseID, @LeadID=l.LeadID
				FROM dbo.Lead l WITH (NOLOCK) 
				INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 			
				INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
				WHERE l.CustomerID = @CustomerID
				AND l.ClientID = @ClientID
				ORDER BY l.LeadID, c.CaseNum, m.MatterID
				
				EXEC dbo._C00_AddProcessStart @CaseID, 23396
				
				UPDATE @ImportData
				SET LeadID=@LeadID
				WHERE RowID = @CurrentRow
				
				--Inserts the matter detail values for the newly created customer for client 2
				--INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
				--VALUES 
				--		(@ClientID, @LeadID, @MatterID,156255, @XMLFileSequenceNo),							--XMLFileSequenceNo
				--		(@ClientID, @LeadID, @MatterID,156256, @TransactionDate + ' ' + @TransactionTime),  --Transaction DateTime
				--		(@ClientID, @LeadID, @MatterID,156257, @TransactionReferenceNumber), 				--Transaction Ref No
				--		(@ClientID, @LeadID, @MatterID,156258, @MainMobile),								--Main Mobile
				--		(@ClientID, @LeadID, @MatterID,156259, @AlternateMobile),							--Alternate Mobile
				--		(@ClientID, @LeadID, @MatterID,156260, @StoreNumber),								--Store Number
				--		(@ClientID, @LeadID, @MatterID,156261, @StoreName),									--Store Name		
				--		(@ClientID, @LeadID, @MatterID,156262, @EmployeeNumber),							--Employee Number
				--		(@ClientID, @LeadID, @MatterID,156263, @EmployeeFirtsName),							--Employee First Name					
				--		(@ClientID, @LeadID, @MatterID,156264, @EmployeeLastName),							--Employee Last Name
				--		(@ClientID, @LeadID, @MatterID,156265, @TransactionType),							--Transaction Type
				--		(@ClientID, @LeadID, @MatterID,156266, @PortedFlag),								--Ported Flag
				--		(@ClientID, @LeadID, @MatterID,156267, @Network),									--Network
				--		(@ClientID, @LeadID, @MatterID,156268, @Manufacturer),								--Manufacturer
				--		(@ClientID, @LeadID, @MatterID,156269, @Model),										--Model
				--		(@ClientID, @LeadID, @MatterID,156270, CONVERT(VARCHAR(2000), dbo.fn_GetDate_Local()))			--Date Imported
				
				IF(@LeadTypeID = 1245) -- Retail Customer Feedback Surveys
				BEGIN
					--Inserts the matter detail values for the newly created customer for client 261
					INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
					VALUES 
							(@ClientID, @LeadID, @MatterID,161283, @XMLFileSequenceNo),							--XMLFileSequenceNo
							(@ClientID, @LeadID, @MatterID,161256, @TransactionDate + ' ' + @TransactionTime),  --Transaction DateTime
							(@ClientID, @LeadID, @MatterID,161285, @TransactionReferenceNumber), 				--Transaction Ref No
							(@ClientID, @LeadID, @MatterID,161266, @MainMobile),								--Main Mobile
							(@ClientID, @LeadID, @MatterID,161267, @AlternateMobile),							--Alternate Mobile
							(@ClientID, @LeadID, @MatterID,161257, @StoreNumber),								--Store Number
							(@ClientID, @LeadID, @MatterID,161259, @StoreName),									--Store Name		
							(@ClientID, @LeadID, @MatterID,161260, @EmployeeNumber),							--Employee Number
							(@ClientID, @LeadID, @MatterID,161284, @EmployeeFirtsName),							--Employee First Name					
							(@ClientID, @LeadID, @MatterID,161261, @EmployeeLastName),							--Employee Last Name
							(@ClientID, @LeadID, @MatterID,161262, @TransactionType),							--Transaction Type
							(@ClientID, @LeadID, @MatterID,161286, @PortedFlag),								--Ported Flag
							(@ClientID, @LeadID, @MatterID,161263, @Network),									--Network
							(@ClientID, @LeadID, @MatterID,161264, @Manufacturer),								--Manufacturer
							(@ClientID, @LeadID, @MatterID,161265, @Model),										--Model
							(@ClientID, @LeadID, @MatterID,161281, CONVERT(VARCHAR(2000), dbo.fn_GetDate_Local()))			--Date Imported
					
					UPDATE Customers
					SET MobileTelephone=@MainMobile, DateOfBirth=@CustomerDOB
					WHERE CustomerID=@CustomerID AND ClientID=@ClientID
				END
				
				IF(@LeadTypeID = 1260) -- ECTS Customer Feedback Surveys
				BEGIN
					--Inserts the matter detail values for the newly created customer for client 261
					INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
					VALUES 
							(@ClientID, @LeadID, @MatterID,162172, @XMLFileSequenceNo),							--XMLFileSequenceNo
							(@ClientID, @LeadID, @MatterID,162160, @TransactionDate + ' ' + @TransactionTime),  --Transaction DateTime
							(@ClientID, @LeadID, @MatterID,162174, @TransactionReferenceNumber), 				--Transaction Ref No
							(@ClientID, @LeadID, @MatterID,162169, @MainMobile),								--Main Mobile
							(@ClientID, @LeadID, @MatterID,162170, @AlternateMobile),							--Alternate Mobile
							(@ClientID, @LeadID, @MatterID,162161, @StoreNumber),								--Store Number
							(@ClientID, @LeadID, @MatterID,162162, @StoreName),									--Store Name		
							(@ClientID, @LeadID, @MatterID,162163, @EmployeeNumber),							--Employee Number
							(@ClientID, @LeadID, @MatterID,162173, @EmployeeFirtsName),							--Employee First Name					
							(@ClientID, @LeadID, @MatterID,162164, @EmployeeLastName),							--Employee Last Name
							(@ClientID, @LeadID, @MatterID,162165, @TransactionType),							--Transaction Type
							(@ClientID, @LeadID, @MatterID,162175, @PortedFlag),								--Ported Flag
							(@ClientID, @LeadID, @MatterID,162166, @Network),									--Network
							(@ClientID, @LeadID, @MatterID,162167, @Manufacturer),								--Manufacturer
							(@ClientID, @LeadID, @MatterID,162168, @Model),										--Model
							(@ClientID, @LeadID, @MatterID,162171, CONVERT(VARCHAR(2000), dbo.fn_GetDate_Local()))			--Date Imported
					
					UPDATE Customers
					SET MobileTelephone=@MainMobile, DateOfBirth=@CustomerDOB
					WHERE CustomerID=@CustomerID AND ClientID=@ClientID
				END
				
			END
			ELSE -- failed to create the customer, lead, case and matter
			BEGIN
			
				UPDATE @ImportData
				SET ERROR='Failed to create customer'
				WHERE RowID=@CurrentRow				
				
			END
		END 
		ELSE -- process duplicate
		BEGIN 
		
			UPDATE @ImportData
			SET ERROR='Duplicate'
			WHERE RowID = @CurrentRow
			
		END
	END -- while
	
	SELECT * FROM @ImportData
	SELECT * FROM @DuplicateData

END
GO
GRANT VIEW DEFINITION ON  [dbo].[original_C261_DataImport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[original_C261_DataImport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[original_C261_DataImport] TO [sp_executeall]
GO
