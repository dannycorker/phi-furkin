SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-10-11
-- Description:	Adds the invoice result after posting to Sage
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_SaveResultInfo]
(
	@ClientID INT,
	@TableRowID INT,
	@Result BIT,
	@Message VARCHAR(2000)
)
AS
BEGIN
	
	/*
	
	This proc saves results back from Sage into the mapped table fields.
	IR use two seperate table definitions for their invoices and credit notes so there are duplicate values in ThirdpartyFieldMapping for the different table fields.
	This is no problem for updates as we have the table row but it makes the updates have to join through the detail field page id to make sure we are mapping to the correct table fields.	

	*/
			
	-- Sent to Sage
	IF EXISTS 
	(
	SELECT * 
	FROM dbo.TableDetailValues t WITH (NOLOCK) 
	INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
	WHERE m.ClientID = @ClientID
	AND m.ThirdPartyFieldID = 163
	AND t.TableRowID = @TableRowID
	)
	BEGIN
		UPDATE t
		SET t.DetailValue = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		FROM dbo.TableDetailValues t
		INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
		WHERE m.ClientID = @ClientID
		AND m.ThirdPartyFieldID = 163
		AND t.TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT dbo.TableDetailValues (CaseID, ClientID, ClientPersonnelID, ContactID, CustomerID, DetailFieldID, DetailValue, LeadID, MatterID, ResourceListID, TableRowID)
		SELECT t.CaseID, t.ClientID, t.ClientPersonnelID, t.ContactID, t.CustomerID, m2.DetailFieldID, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120), t.LeadID, t.MatterID, t.ResourceListID, @TableRowID
		FROM dbo.TableDetailValues t WITH (NOLOCK) 
		INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
		INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON m.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.DetailFields f2 WITH (NOLOCK) ON f.DetailFieldPageID = f2.DetailFieldPageID -- this joins to other fields in the same table definition
		INNER JOIN dbo.ThirdPartyFieldMapping m2 WITH (NOLOCK) ON f2.DetailFieldID = m2.DetailFieldID
		WHERE m.ClientID = @ClientID
		AND m2.ClientID = @ClientID
		AND m.ThirdPartyFieldID = 160 -- this is the invoice no field that we know already exists
		AND m2.ThirdPartyFieldID = 163 -- this is the sent to Sage field
		AND t.TableRowID = @TableRowID
	END
	
	-- Result
	IF EXISTS 
	(
	SELECT * 
	FROM dbo.TableDetailValues t WITH (NOLOCK) 
	INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
	WHERE m.ClientID = @ClientID
	AND m.ThirdPartyFieldID = 164
	AND t.TableRowID = @TableRowID
	)
	BEGIN
		UPDATE t
		SET t.DetailValue = CASE @Result WHEN 1 THEN 'true' ELSE 'false' END
		FROM dbo.TableDetailValues t
		INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
		WHERE m.ClientID = @ClientID
		AND m.ThirdPartyFieldID = 164
		AND t.TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT dbo.TableDetailValues (CaseID, ClientID, ClientPersonnelID, ContactID, CustomerID, DetailFieldID, DetailValue, LeadID, MatterID, ResourceListID, TableRowID)
		SELECT t.CaseID, t.ClientID, t.ClientPersonnelID, t.ContactID, t.CustomerID, m2.DetailFieldID, CASE @Result WHEN 1 THEN 'true' ELSE 'false' END, t.LeadID, t.MatterID, t.ResourceListID, @TableRowID
		FROM dbo.TableDetailValues t WITH (NOLOCK) 
		INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
		INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON m.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.DetailFields f2 WITH (NOLOCK) ON f.DetailFieldPageID = f2.DetailFieldPageID -- this joins to other fields in the same table definition
		INNER JOIN dbo.ThirdPartyFieldMapping m2 WITH (NOLOCK) ON f2.DetailFieldID = m2.DetailFieldID
		WHERE m.ClientID = @ClientID
		AND m2.ClientID = @ClientID
		AND m.ThirdPartyFieldID = 160 -- this is the invoice no field that we know already exists
		AND m2.ThirdPartyFieldID = 164 -- result field
		AND t.TableRowID = @TableRowID
	END
	
	-- Message
	IF EXISTS 
	(
	SELECT * 
	FROM dbo.TableDetailValues t WITH (NOLOCK) 
	INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
	WHERE m.ClientID = @ClientID
	AND m.ThirdPartyFieldID = 165
	AND t.TableRowID = @TableRowID
	)
	BEGIN
		UPDATE t
		SET t.DetailValue = @Message
		FROM dbo.TableDetailValues t
		INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
		WHERE m.ClientID = @ClientID
		AND m.ThirdPartyFieldID = 165
		AND t.TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT dbo.TableDetailValues (CaseID, ClientID, ClientPersonnelID, ContactID, CustomerID, DetailFieldID, DetailValue, LeadID, MatterID, ResourceListID, TableRowID)
		SELECT t.CaseID, t.ClientID, t.ClientPersonnelID, t.ContactID, t.CustomerID, m2.DetailFieldID, @Message, t.LeadID, t.MatterID, t.ResourceListID, @TableRowID
		FROM dbo.TableDetailValues t WITH (NOLOCK) 
		INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON t.DetailFieldID = m.DetailFieldID
		INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON m.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.DetailFields f2 WITH (NOLOCK) ON f.DetailFieldPageID = f2.DetailFieldPageID -- this joins to other fields in the same table definition
		INNER JOIN dbo.ThirdPartyFieldMapping m2 WITH (NOLOCK) ON f2.DetailFieldID = m2.DetailFieldID
		WHERE m.ClientID = @ClientID
		AND m2.ClientID = @ClientID
		AND m.ThirdPartyFieldID = 160 -- this is the invoice no field that we know already exists
		AND m2.ThirdPartyFieldID = 165 -- message field
		AND t.TableRowID = @TableRowID
	END
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_SaveResultInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_SaveResultInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_SaveResultInfo] TO [sp_executeall]
GO
