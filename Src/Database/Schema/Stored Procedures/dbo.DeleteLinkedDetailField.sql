SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[DeleteLinkedDetailField] @LinkedDetailFieldID int
AS
Delete From LinkedDetailFields
Where (LinkedDetailFieldID = @LinkedDetailFieldID)



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLinkedDetailField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteLinkedDetailField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLinkedDetailField] TO [sp_executeall]
GO
