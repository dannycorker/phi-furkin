SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FunctionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_Update]
(

	@FunctionTypeID int   ,

	@OriginalFunctionTypeID int   ,

	@ModuleID int   ,

	@ParentFunctionTypeID int   ,

	@FunctionTypeName varchar (50)  ,

	@Level int   ,

	@IsVirtual bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FunctionType]
				SET
					[FunctionTypeID] = @FunctionTypeID
					,[ModuleID] = @ModuleID
					,[ParentFunctionTypeID] = @ParentFunctionTypeID
					,[FunctionTypeName] = @FunctionTypeName
					,[Level] = @Level
					,[IsVirtual] = @IsVirtual
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[FunctionTypeID] = @OriginalFunctionTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Update] TO [sp_executeall]
GO
