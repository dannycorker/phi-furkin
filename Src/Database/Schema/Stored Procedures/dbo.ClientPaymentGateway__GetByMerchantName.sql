SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-04-2018
-- Description:	Gets a Client Payment Gateway by MerchantName
-- =============================================
CREATE PROCEDURE [dbo].[ClientPaymentGateway__GetByMerchantName]
	@ClientID INT,
	@MerchantName VARCHAR(250)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * FROM ClientPaymentGateway WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND MerchantName = @MerchantName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByMerchantName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPaymentGateway__GetByMerchantName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByMerchantName] TO [sp_executeall]
GO
