SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-07-24
-- Description:	POST Pet and Policy for creation and complete setup in Vision
-- 2020-07-30 ALM Added POST for PaymentAccount
-- 2020-09-08 ALM Added join and insert into MediateQueuePolicyAudit to prevent duplication
-- 2020-09-24 ALM Fix added for multipet issue
-- 2021-04-26 ALM Fix for preventing mediation errors FURKIN-548
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Mediate_PostPetAndPolicy]
AS
BEGIN

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID(), 
			@LeadEventID INT,
			@OneVisionID INT,
			@MatterID INT,
			@ExamFeeIncluded INT = 0, 
			@WellnessIncluded INT = 0 

	DECLARE @UnprocessedRecords TABLE (OneVisionID INT, MatterID INT, LeadEventID INT)

	DECLARE @EmptyTableDoNotUse TABLE (RowsProcessed INT)

	EXEC dbo._C00_LogIt 'Info', '_C600_Mediate_PostPetAndPolicy', '', 'Proc Started', 58552

	INSERT INTO @UnprocessedRecords (OneVisionID, MatterID, LeadEventID)
	SELECT ov.OneVisionID, m.MatterID, le.LeadEventID
	FROM OneVision ov WITH (NOLOCK) 
	JOIN Matter m WITH (NOLOCK) ON m.MatterID = ov.PAMatterID
	JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = m.CaseID
	JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = 
	(SELECT TOP 1 le1.LeadEventID 
	FROM LeadEvent le1 
	WHERE le1.CaseID = ca.CaseID 
	AND le1.EventTypeID = 156920 
	ORDER BY 1 DESC)
	WHERE NOT EXISTS (
		SELECT * 
		FROM MediateQueuePolicyAudit mqaudit WITH (NOLOCK) 
		WHERE mqaudit.OneVisionID = ov.OneVisionID
	)
	AND ov.VisionPolicyID IS NULL 
	AND ov.VisionPetID IS NULL 
	AND ov.VisionCustomerID IS NOT NULL 
	AND m.ClientID = @ClientID 
	AND ov.PolicyTermType = 'Policy' /*GPR 2020-09-21*/
	ORDER BY ov.OneVisionID DESC

	IF EXISTS (SELECT * FROM @UnprocessedRecords)
	BEGIN
		WHILE (SELECT COUNT(*) FROM @UnprocessedRecords) >= 1
		BEGIN

			SELECT TOP 1 @OneVisionID = u.OneVisionID, @LeadEventID = u.LeadEventID, @MatterID = u.MatterID
			FROM @UnprocessedRecords u
			ORDER BY OneVisionID DESC 

			INSERT INTO MediateQueuePolicyAudit (OneVisionID, ProcessedDateTime)
			VALUES (@OneVisionID, dbo.fn_GetDate_Local())

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Pets',
			                                      @HttpMethod = 'POST',
			                                      @LeadEventID = @LeadEventID,
												  @OneVisionID = @OneVisionID

			/*2020-10-10 ALM Prevent trying to post to the mediate queue where policy claim account doesn't exist*/
			IF EXISTS (SELECT a.AccountID FROM Account a WITH (NOLOCK) 
						JOIN Matter m WITH (NOLOCK) ON m.CustomerID = a.CustomerID 
						WHERE a.AccountUseID = 2
						AND a.Active = 1
						AND m.MatterID = @MatterID
						/*ALM 2021-04-26 FURKIN-548*/
						AND NOT EXISTS (SELECT * FROM OneVision ov WITH (NOLOCK) 
						JOIN _C00_Mediate_Queue mq WITH (NOLOCK) ON mq.OneVisionID = ov.OneVisionID
						WHERE ov.CustomerID = m.CustomerID
						AND mq.RequestType = 'VisionPaymentRequest')
						)
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'PaymentAccount',
													  @HttpMethod = 'POST',
													  @LeadEventID = @LeadEventID,
													  @OneVisionID = @OneVisionID
			END

			/*SELECT @ExamFeeIncluded = 
			CASE WHEN ldv.ValueInt = 5144 THEN 1 ELSE 0 END
			FROM @UnprocessedRecords ur 
			JOIN Matter m WITH (NOLOCK) ON m.MatterID = ur.MatterID
			JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID 
			JOIN LeadDetailValues ldv WITH (NOLOCK) on ldv.LeadID = l.LeadID
			WHERE ldv.DetailFieldID = 313931 
			AND ldv.ValueInt = 5144
			AND m.MatterID = @MatterID 

			IF @ExamFeeIncluded = 1
			BEGIN 
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'VisionExamNote',
													  @HttpMethod = 'POST',
													  @LeadEventID = @LeadEventID,
													  @OneVisionID = @OneVisionID
			END

			SELECT @WellnessIncluded = 
			CASE WHEN mdv.ValueInt > 0 THEN 1 ELSE 0 END
			FROM @UnprocessedRecords ur 
			JOIN Matter m WITH (NOLOCK) ON m.MatterID = ur.MatterID
			JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID
			WHERE mdv.DetailFieldID = 314235 
			AND m.MatterID = @MatterID

			IF @WellnessIncluded = 1
			BEGIN 
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'VisionWellnessNote',
													  @HttpMethod = 'POST',
													  @LeadEventID = @LeadEventID,
													  @OneVisionID = @OneVisionID
			END*/

			DELETE FROM @UnprocessedRecords 
			WHERE OneVisionID = @OneVisionID

		END
	END

	/*Return empty table to keep the scheduler happy*/
	SELECT *
	FROM @EmptyTableDoNotUse t 

	EXEC dbo._C00_LogIt 'Info', '_C600_Mediate_PostPetAndPolicy', '', 'Proc Ended', 58552
   
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Mediate_PostPetAndPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Mediate_PostPetAndPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Mediate_PostPetAndPolicy] TO [sp_executeall]
GO
