SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-09-29
-- Description:	Promote a document type to the next stage in the process
-- 2017-10-17 CPS update to use sp_executesql
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CR_AdHoc_DocumentType_Update]
	 @DocumentTypeID	INT
	,@QueueTableRowID	INT
AS
BEGIN

	DECLARE  @ErrorMessage		VARCHAR(2000)
			,@EventComments		VARCHAR(2000)
			,@ThisDB			VARCHAR(2000) = DB_NAME()
			,@dSQL_Document		VARCHAR(MAX)
			,@dSQL_Targets		VARCHAR(MAX)
			,@dSQL_Special		VARCHAR(MAX)
			,@dSQL_Standard		VARCHAR(MAX)
			,@TargetServerAndDb	VARCHAR(MAX)
			,@Result			dbo.tvpIntVarchar
			,@MatterID			INT
			,@Success			BIT = 0
			,@VarcharDate		VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121)
			,@TableComments		VARCHAR(2000) = ''

	DECLARE  @ClientID INT
			,@LeadTypeID INT
			,@DocumentTypeName VARCHAR(2000)
			,@DocumentTypeDescription VARCHAR(2000)
			,@Header VARCHAR(2000)
			,@Template VARCHAR(MAX)
			,@Footer VARCHAR(2000)
			,@CanBeAutoSent VARCHAR(2000)
			,@EmailSubject VARCHAR(2000)
			,@EmailBodyText VARCHAR(MAX)
			,@InputFormat VARCHAR(2000)
			,@OutputFormat VARCHAR(2000)
			,@Enabled BIT
			,@RecipientsTo VARCHAR(2000)
			,@RecipientsCC VARCHAR(2000)
			,@RecipientsBCC VARCHAR(2000)
			,@ReadOnlyTo VARCHAR(2000)
			,@ReadOnlyCC VARCHAR(2000)
			,@ReadOnlyBCC VARCHAR(2000)
			,@SendToMultipleRecipients VARCHAR(2000)
			,@MultipleRecipientDataSourceType VARCHAR(2000)
			,@MultipleRecipientDataSourceID INT
			,@SendToAllByDefault BIT
			,@ExcelTemplatePath VARCHAR(2000)
			,@FromDetails VARCHAR(2000)
			,@ReadOnlyFrom VARCHAR(2000)
			,@SourceID INT
			,@WhoCreated VARCHAR(2000)
			,@WhenCreated DATETIME
			,@WhoModified VARCHAR(2000)
			,@WhenModified DATETIME
			,@FolderID INT
			,@IsThunderheadTemplate VARCHAR(2000)
			,@ThunderheadUniqueTemplateID INT
			,@ThunderheadDocumentFormat VARCHAR(2000)
			,@DocumentTitleTemplate VARCHAR(2000)


	SELECT   @ClientID = dt.ClientID
			,@LeadTypeID = dt.LeadTypeID
			,@DocumentTypeName = dt.DocumentTypeName
			,@DocumentTypeDescription = dt.DocumentTypeDescription
			,@Header = dt.Header
			,@Template = dt.Template
			,@Footer = dt.Footer
			,@CanBeAutoSent = dt.CanBeAutoSent
			,@EmailSubject = dt.EmailSubject
			,@EmailBodyText = dt.EmailBodyText
			,@InputFormat = dt.InputFormat
			,@OutputFormat = dt.OutputFormat
			,@Enabled = dt.Enabled
			,@RecipientsTo = dt.RecipientsTo
			,@RecipientsCC = dt.RecipientsCC
			,@RecipientsBCC = dt.RecipientsBCC
			,@ReadOnlyTo = dt.ReadOnlyTo
			,@ReadOnlyCC = dt.ReadOnlyCC
			,@ReadOnlyBCC = dt.ReadOnlyBCC
			,@SendToMultipleRecipients = dt.SendToMultipleRecipients
			,@MultipleRecipientDataSourceType = dt.MultipleRecipientDataSourceType
			,@MultipleRecipientDataSourceID = dt.MultipleRecipientDataSourceID
			,@SendToAllByDefault = dt.SendToAllByDefault
			,@ExcelTemplatePath = dt.ExcelTemplatePath
			,@FromDetails = dt.FromDetails
			,@ReadOnlyFrom = dt.ReadOnlyFrom
			,@SourceID = dt.SourceID
			,@WhoCreated = dt.WhoCreated
			,@WhenCreated = dt.WhenCreated
			,@WhoModified = dt.WhoModified
			,@WhenModified = dt.WhenModified
			,@FolderID = dt.FolderID
			,@IsThunderheadTemplate = dt.IsThunderheadTemplate
			,@ThunderheadUniqueTemplateID = dt.ThunderheadUniqueTemplateID
			,@ThunderheadDocumentFormat = dt.ThunderheadDocumentFormat
			,@DocumentTitleTemplate = dt.DocumentTitleTemplate
	FROM DocumentType dt WITH ( NOLOCK ) 
	WHERE dt.DocumentTypeID = @DocumentTypeID

	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No match found for DocumentTypeID ' + ISNULL(CONVERT(VARCHAR,@DocumentTypeID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	DECLARE @DocumentDetailFieldTargets TABLE ( DocumentDetailFieldTargetID INT, ClientID INT, DocumentTypeID INT, FieldTarget VARCHAR(2000), DetailFieldID INT, TemplateTypeID INT, DetailFieldAlias VARCHAR(2000), Notes VARCHAR(2000), ExcelSheetLocation VARCHAR(2000), Format VARCHAR(2000) )
	DECLARE @DocumentSpecialisedDetailFieldTargets TABLE ( DocumentSpecialisedDetailFieldTargetID INT, ClientID INT, DocumentTypeID INT, FieldTarget VARCHAR(2000), DetailFieldID INT, ColumnField INT, TemplateTypeID INT, DetailFieldAlias VARCHAR(2000), ColumnFieldAlias VARCHAR(2000), RowField INT , Notes VARCHAR(2000), ExcelSheetLocation VARCHAR(2000), Format VARCHAR(2000) )
	DECLARE @DocumentStandardTargets TABLE ( DocumentStandardTargetID INT, ClientID INT, DocumentTypeID INT, FieldTarget VARCHAR(2000), ObjectName VARCHAR(2000), PropertyName VARCHAR(2000), TemplateTypeID INT, Notes VARCHAR(2000), IsSpecial BIT, ExcelSheetLocation VARCHAR(2000) )

	SELECT @dSQL_Targets = 
	'
	SELECT DocumentDetailFieldTargetID, ClientID, DocumentTypeID, Target, DetailFieldID, TemplateTypeID, DetailFieldAlias, Notes, ExcelSheetLocation, Format
	FROM DocumentDetailFieldTarget ft WITH ( NOLOCK ) 
	WHERE ft.DocumentTypeID = ' + CONVERT(VARCHAR(MAX),@DocumentTypeID) + '
	and not exists ( SELECT * 
	                 FROM {{ServerAndDatabase}}.dbo.DocumentDetailFieldTarget trgt WITH ( NOLOCK ) 
	                 WHERE trgt.Target = ft.Target 
	                 AND trgt.DocumentTypeID = ft.DocumentTypeID )'

	SELECT @dSQL_Special = 
	'
	SELECT DocumentSpecialisedDetailFieldTargetID, ClientID, DocumentTypeID, Target, DetailFieldID, ColumnField, TemplateTypeID, DetailFieldAlias, ColumnFieldAlias, RowField, Notes, ExcelSheetLocation, Format
	FROM DocumentSpecialisedDetailFieldTarget ft WITH ( NOLOCK ) 
	WHERE ft.DocumentTypeID = ' + CONVERT(VARCHAR(MAX),@DocumentTypeID) + '
	and not exists ( SELECT * 
	                 FROM {{ServerAndDatabase}}.dbo.DocumentSpecialisedDetailFieldTarget trgt WITH ( NOLOCK ) 
	                 WHERE trgt.Target = ft.Target 
	                 AND trgt.DocumentTypeID = ft.DocumentTypeID )'

	SELECT @dSQL_Standard = 
	'
	SELECT ClientID, DocumentTypeID, Target, ObjectName, PropertyName, TemplateTypeID, Notes, IsSpecial, ExcelSheetLocation
	FROM DocumentStandardTarget ft WITH ( NOLOCK ) 
	WHERE ft.DocumentTypeID = ' + CONVERT(VARCHAR(MAX),@DocumentTypeID) + '
	and not exists ( SELECT * 
	                 FROM {{ServerAndDatabase}}.dbo.DocumentStandardTarget trgt WITH ( NOLOCK ) 
	                 WHERE trgt.Target = ft.Target 
	                 AND trgt.DocumentTypeID = ft.DocumentTypeID )'

	SELECT @TargetServerAndDb = CASE @ThisDB
								WHEN 'Aquarius600'		THEN 'Aquarius600QA'
								END 

	SELECT @dSQL_Document	= REPLACE(@dSQL_Document,'{{ServerAndDatabase}}',@TargetServerAndDb)
	SELECT @dSQL_Targets	= REPLACE(@dSQL_Targets	,'{{ServerAndDatabase}}',@TargetServerAndDb)
	SELECT @dSQL_Standard	= REPLACE(@dSQL_Standard,'{{ServerAndDatabase}}',@TargetServerAndDb)
	SELECT @dSQL_Special	= REPLACE(@dSQL_Special	,'{{ServerAndDatabase}}',@TargetServerAndDb)

	IF not exists ( SELECT * FROM DocumentType dt WITH ( NOLOCK ) WHERE dt.DocumentTypeID = @DocumentTypeID )
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  DocumentTypeID ' + ISNULL(CONVERT(VARCHAR,@DocumentTypeID),'NULL') + ' does not exist on ' + ISNULL(@ThisDb,'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END

	IF @ThisDB = 'Aquarius600'
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  You cannot promote FROM the live database.  Log in to a test DB and try again.</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	ELSE
	IF @ThisDB = 'Aquarius600Dev'
	BEGIN
		--EXEC Aquarius600.dbo.DocumentType_Update @DocumentTypeID = @DocumentTypeID, @ClientID = @ClientID  ,@LeadTypeID = @LeadTypeID  ,@DocumentTypeName = @DocumentTypeName  ,@DocumentTypeDescription = @DocumentTypeDescription  ,@Header = @Header  ,@Template = @Template  ,@Footer = @Footer  ,@CanBeAutoSent = @CanBeAutoSent  ,@EmailSubject = @EmailSubject  ,@EmailBodyText = @EmailBodyText  ,@InputFormat = @InputFormat  ,@OutputFormat = @OutputFormat  ,@Enabled = @Enabled  ,@RecipientsTo = @RecipientsTo  ,@RecipientsCC = @RecipientsCC  ,@RecipientsBCC = @RecipientsBCC  ,@ReadOnlyTo = @ReadOnlyTo  ,@ReadOnlyCC = @ReadOnlyCC  ,@ReadOnlyBCC = @ReadOnlyBCC  ,@SendToMultipleRecipients = @SendToMultipleRecipients  ,@MultipleRecipientDataSourceType = @MultipleRecipientDataSourceType  ,@MultipleRecipientDataSourceID = @MultipleRecipientDataSourceID  ,@SendToAllByDefault = @SendToAllByDefault  ,@ExcelTemplatePath = @ExcelTemplatePath  ,@FromDetails = @FromDetails  ,@ReadOnlyFrom = @ReadOnlyFrom  ,@SourceID = @SourceID  ,@WhoCreated = @WhoCreated  ,@WhenCreated = @WhenCreated  ,@WhoModified = @WhoModified  ,@WhenModified = @WhenModified  ,@FolderID = @FolderID  ,@IsThunderheadTemplate = @IsThunderheadTemplate  ,@ThunderheadUniqueTemplateID = @ThunderheadUniqueTemplateID  ,@ThunderheadDocumentFormat = @ThunderheadDocumentFormat  ,@DocumentTitleTemplate = @DocumentTitleTemplate
		SELECT @Success = 1
	END
	ELSE 
	IF @ThisDB = 'Aquarius600'
	BEGIN
		--EXEC [878574-SQLCLUS1\CPSQL1].Aquarius600.dbo.DocumentType_Update @DocumentTypeID = @DocumentTypeID, @ClientID = @ClientID  ,@LeadTypeID = @LeadTypeID  ,@DocumentTypeName = @DocumentTypeName  ,@DocumentTypeDescription = @DocumentTypeDescription  ,@Header = @Header  ,@Template = @Template  ,@Footer = @Footer  ,@CanBeAutoSent = @CanBeAutoSent  ,@EmailSubject = @EmailSubject  ,@EmailBodyText = @EmailBodyText  ,@InputFormat = @InputFormat  ,@OutputFormat = @OutputFormat  ,@Enabled = @Enabled  ,@RecipientsTo = @RecipientsTo  ,@RecipientsCC = @RecipientsCC  ,@RecipientsBCC = @RecipientsBCC  ,@ReadOnlyTo = @ReadOnlyTo  ,@ReadOnlyCC = @ReadOnlyCC  ,@ReadOnlyBCC = @ReadOnlyBCC  ,@SendToMultipleRecipients = @SendToMultipleRecipients  ,@MultipleRecipientDataSourceType = @MultipleRecipientDataSourceType  ,@MultipleRecipientDataSourceID = @MultipleRecipientDataSourceID  ,@SendToAllByDefault = @SendToAllByDefault  ,@ExcelTemplatePath = @ExcelTemplatePath  ,@FromDetails = @FromDetails  ,@ReadOnlyFrom = @ReadOnlyFrom  ,@SourceID = @SourceID  ,@WhoCreated = @WhoCreated  ,@WhenCreated = @WhenCreated  ,@WhoModified = @WhoModified  ,@WhenModified = @WhenModified  ,@FolderID = @FolderID  ,@IsThunderheadTemplate = @IsThunderheadTemplate  ,@ThunderheadUniqueTemplateID = @ThunderheadUniqueTemplateID  ,@ThunderheadDocumentFormat = @ThunderheadDocumentFormat  ,@DocumentTitleTemplate = @DocumentTitleTemplate
		PRINT @DocumentTypeID
		PRINT @Template
		SELECT @Success = 1
	END
	ELSE 
	IF @ThisDB = 'Aquarius600'
	BEGIN
		--EXEC [878574-SQLCLUS1\CPSQL1].Aquarius600.dbo.DocumentType_Update @DocumentTypeID = @DocumentTypeID, @ClientID = @ClientID  ,@LeadTypeID = @LeadTypeID  ,@DocumentTypeName = @DocumentTypeName  ,@DocumentTypeDescription = @DocumentTypeDescription  ,@Header = @Header  ,@Template = @Template  ,@Footer = @Footer  ,@CanBeAutoSent = @CanBeAutoSent  ,@EmailSubject = @EmailSubject  ,@EmailBodyText = @EmailBodyText  ,@InputFormat = @InputFormat  ,@OutputFormat = @OutputFormat  ,@Enabled = @Enabled  ,@RecipientsTo = @RecipientsTo  ,@RecipientsCC = @RecipientsCC  ,@RecipientsBCC = @RecipientsBCC  ,@ReadOnlyTo = @ReadOnlyTo  ,@ReadOnlyCC = @ReadOnlyCC  ,@ReadOnlyBCC = @ReadOnlyBCC  ,@SendToMultipleRecipients = @SendToMultipleRecipients  ,@MultipleRecipientDataSourceType = @MultipleRecipientDataSourceType  ,@MultipleRecipientDataSourceID = @MultipleRecipientDataSourceID  ,@SendToAllByDefault = @SendToAllByDefault  ,@ExcelTemplatePath = @ExcelTemplatePath  ,@FromDetails = @FromDetails  ,@ReadOnlyFrom = @ReadOnlyFrom  ,@SourceID = @SourceID  ,@WhoCreated = @WhoCreated  ,@WhenCreated = @WhenCreated  ,@WhoModified = @WhoModified  ,@WhenModified = @WhenModified  ,@FolderID = @FolderID  ,@IsThunderheadTemplate = @IsThunderheadTemplate  ,@ThunderheadUniqueTemplateID = @ThunderheadUniqueTemplateID  ,@ThunderheadDocumentFormat = @ThunderheadDocumentFormat  ,@DocumentTitleTemplate = @DocumentTitleTemplate
		SELECT @Success = 1
	END

	IF @ThisDB <> '' AND @dSQL_Document <> '' AND @DocumentTypeID <> 0
	BEGIN
		--BEGIN TRY

			--INSERT @Result (AnyValue,AnyID)
			--EXEC (@dSQL_Document)

			INSERT @DocumentDetailFieldTargets ( DocumentDetailFieldTargetID, ClientID, DocumentTypeID, FieldTarget, DetailFieldID, TemplateTypeID, DetailFieldAlias, Notes, ExcelSheetLocation, Format )
			EXEC (@dSQL_Targets)

			INSERT @Result (AnyValue,AnyID)
			SELECT 'DocumentDetailFieldTagets', count(*) 
			FROM @DocumentDetailFieldTargets

			DECLARE  @target_id					INT
					,@target_ClientID			INT
					,@target_DocumentTypeID		INT
					,@target_Target				VARCHAR(2000)
					,@target_DetailFieldID		INT
					,@target_TemplateTypeID		INT
					,@target_DetailFieldAlias	VARCHAR(2000)
					,@target_Notes				VARCHAR(2000)
					,@target_ExcelSheetLocation	VARCHAR(2000)
					,@target_Format				VARCHAR(2000)
					,@target_ColumnField		INT
					,@target_ColumnFieldAlias	VARCHAR(2000)
					,@target_RowField			INT
					,@target_ObjectName			VARCHAR(2000)
					,@target_PropertyName		VARCHAR(2000)
					,@target_IsSpecial			BIT

			WHILE EXISTS ( SELECT * FROM @DocumentDetailFieldTargets )
			BEGIN
				SELECT TOP 1 @target_id					= t.DocumentDetailFieldTargetID
							,@target_ClientID			= t.ClientID
							,@target_DocumentTypeID		= t.DocumentTypeID
							,@target_Target				= t.FieldTarget
							,@target_DetailFieldID		= t.DetailFieldID
							,@target_TemplateTypeID		= t.TemplateTypeID
							,@target_DetailFieldAlias	= t.DetailFieldAlias
							,@target_Notes				= t.Notes
							,@target_ExcelSheetLocation	= t.ExcelSheetLocation
							,@target_Format				= t.Format
				FROM @DocumentDetailFieldTargets t 
				
				SELECT @dSQL_Targets = @TargetServerAndDb + 'dbo.DocumentDetailFieldTarget_Insert @DocumentDetailFieldTargetID, @ClientID, @DocumentTypeID, @Target, @DetailFieldID, @TemplateTypeID, @DetailFieldAlias, @Notes, @ExcelSheetLocation, @Format'
				EXEC sp_executesql @dSQL_targets, NULL, @target_ClientID, @target_DocumentTypeID, @target_Target, @target_DetailFieldID, @target_TemplateTypeID, @target_DetailFieldAlias, @target_Notes, @target_ExcelSheetLocation, @target_Format
				
				DELETE ft
				FROM @DocumentDetailFieldTargets ft 
				WHERE ft.DocumentDetailFieldTargetID = @target_id
				SELECT @dSQL_Targets = NULL
			END
			
			INSERT @DocumentSpecialisedDetailFieldTargets ( DocumentSpecialisedDetailFieldTargetID, ClientID, DocumentTypeID, FieldTarget, DetailFieldID, ColumnField, TemplateTypeID, DetailFieldAlias, ColumnFieldAlias, RowField, Notes, ExcelSheetLocation, Format )
			EXEC (@dSQL_Special)

			WHILE EXISTS ( SELECT * FROM @DocumentSpecialisedDetailFieldTargets )
			BEGIN
				SELECT TOP 1 @target_id					= t.DocumentSpecialisedDetailFieldTargetID
							,@target_ClientID			= t.ClientID
							,@target_DocumentTypeID		= t.DocumentTypeID
							,@target_Target				= t.FieldTarget
							,@target_DetailFieldID		= t.DetailFieldID
							,@target_TemplateTypeID		= t.TemplateTypeID
							,@target_DetailFieldAlias	= t.DetailFieldAlias
							,@target_Notes				= t.Notes
							,@target_ExcelSheetLocation	= t.ExcelSheetLocation
							,@target_Format				= t.Format
							,@target_ColumnField		= t.ColumnField
							,@target_ColumnFieldAlias	= t.ColumnFieldAlias
							,@target_RowField			= t.RowField
				FROM @DocumentSpecialisedDetailFieldTargets t 
				
				SELECT @dSQL_Targets = @TargetServerAndDb + 'dbo.DocumentSpecialisedDetailFieldTarget_Insert @DocumentSpecialisedDetailFieldTargetID, @ClientID, @DocumentTypeID, @Target, @DetailFieldID, @ColumnField, @TemplateTypeID, @DetailFieldAlias, @ColumnFieldAlias, @RowField, @Notes, @ExcelSheetLocation, @Format'
				EXEC sp_executesql @dSQL_targets, NULL, @target_ClientID, @target_DocumentTypeID, @target_Target, @target_DetailFieldID, @target_ColumnField, @target_TemplateTypeID, @target_DetailFieldAlias, @target_ColumnFieldAlias, @target_RowField, @target_Notes, @target_ExcelSheetLocation, @target_Format
				
				DELETE ft
				FROM @DocumentSpecialisedDetailFieldTargets ft 
				WHERE ft.DocumentSpecialisedDetailFieldTargetID = @target_id
				SELECT @dSQL_Targets = NULL
			END


			INSERT @Result (AnyValue,AnyID)
			SELECT 'DocumentSpecialisedDetailFieldTagets', count(*)
			FROM @DocumentSpecialisedDetailFieldTargets

			INSERT @DocumentStandardTargets ( DocumentStandardTargetID, ClientID, DocumentTypeID, FieldTarget, ObjectName, PropertyName, TemplateTypeID, Notes, IsSpecial, ExcelSheetLocation )
			EXEC (@dSQL_Standard)						
			
			INSERT @Result (AnyValue,AnyID)
			SELECT 'DocumentStandardTargets', count(*)
			FROM @DocumentStandardTargets

			WHILE EXISTS ( SELECT * FROM @DocumentStandardTargets )
			BEGIN
				SELECT TOP 1 @target_id					= t.DocumentStandardTargetID
							,@target_ClientID			= t.ClientID
							,@target_DocumentTypeID		= t.DocumentTypeID
							,@target_Target				= t.FieldTarget
							,@target_ObjectName			= t.ObjectName
							,@target_PropertyName		= t.PropertyName
							,@target_TemplateTypeID		= t.TemplateTypeID
							,@target_Notes				= t.Notes
							,@target_ExcelSheetLocation	= t.ExcelSheetLocation
							,@target_IsSpecial			= t.IsSpecial
				FROM @DocumentStandardTargets t 
				
				SELECT @dSQL_Targets = @TargetServerAndDb + 'dbo.DocumentStandardTarget_Insert @DocumentStandardTargetID, @ClientID, @DocumentTypeID, @Target, @ObjectName, @PropertyName, @TemplateTypeID, @Notes, @IsSpecial, @ExcelSheetLocation'
				EXEC sp_executesql @dSQL_targets, NULL, @target_ClientID, @target_DocumentTypeID, @target_Target, @target_ObjectName, @target_PropertyName, @target_TemplateTypeID, @target_Notes, @target_IsSpecial, @target_ExcelSheetLocation
				
				DELETE ft
				FROM @DocumentStandardTargets ft 
				WHERE ft.DocumentStandardTargetID = @target_id
				SELECT @dSQL_Targets = NULL
			END			
		--END TRY
		--BEGIN CATCH
			SELECT @ErrorMessage = ERROR_MESSAGE()
		--END CATCH
	END

	SELECT @EventComments += r.AnyValue + ' - ' + ISNULL(CONVERT(VARCHAR,r.AnyID),'NULL') + ' record(s).' + CHAR(13)+CHAR(10)
	FROM @Result r
	
	SELECT @EventComments += ISNULL('Error Message: ' + @ErrorMessage + CHAR(13)+CHAR(10),'')

	IF @Success = 1 AND @QueueTableRowID <> 0
	BEGIN
		SELECT @TableComments = ISNULL(dbo.fnGetSimpleDv(179404,@QueueTableRowID),'') /*Comments*/
		SELECT @TableComments += CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + ISNULL(@EventComments,'')
		EXEC dbo._C00_SimpleValueIntoField 179404, @TableComments, @QueueTableRowID /*Comments*/
		EXEC dbo._C00_SimpleValueIntoField 179408, @VarcharDate, @QueueTableRowID /*Date Released*/
		
		EXEC _C00_CompleteTableRow @QueueTableRowID, NULL, 1, 1
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CR_AdHoc_DocumentType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CR_AdHoc_DocumentType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CR_AdHoc_DocumentType_Update] TO [sp_executeall]
GO
