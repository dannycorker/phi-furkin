SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.CountMasterQuestions    Script Date: 08/09/2006 12:22:51 ******/
CREATE PROCEDURE [dbo].[CountMasterQuestions] @ClientQuestionnaireID int

 AS


SELECT     dbo.ClientQuestionnaires.ClientQuestionnaireID, COUNT(dbo.MasterQuestions.MasterQuestionID) AS QuestionCount
FROM         dbo.ClientQuestionnaires INNER JOIN
                      dbo.MasterQuestions ON dbo.ClientQuestionnaires.ClientQuestionnaireID = dbo.MasterQuestions.ClientQuestionnaireID
GROUP BY dbo.ClientQuestionnaires.ClientQuestionnaireID
HAVING      (dbo.ClientQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID)




GO
GRANT VIEW DEFINITION ON  [dbo].[CountMasterQuestions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CountMasterQuestions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CountMasterQuestions] TO [sp_executeall]
GO
