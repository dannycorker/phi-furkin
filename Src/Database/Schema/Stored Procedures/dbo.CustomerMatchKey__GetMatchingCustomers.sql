SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Get Customers with matching keys
--				ACE 2013-01-30 Used with clause
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMatchKey__GetMatchingCustomers]

	@ClientID INT,	
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250)

AS
BEGIN

	WITH cmk as (
		SELECT *, CASE WHEN c.FirstName = @FirstName THEN 1 ELSE 0 END as [FNameMatch],
				CASE WHEN c.LastName = @LastName THEN 1 ELSE 0 END as [LNameMatch],
				CASE WHEN c.Address1 = @Address1 THEN 1 ELSE 0 END as [A1Match],
				CASE WHEN c.Postcode = @PostCode THEN 1 ELSE 0 END as [PostcodeMatch],
				CASE WHEN c.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [DobMatch],
				CASE WHEN c.FirstName = @FirstName THEN 1 ELSE 0 END +
				CASE WHEN c.LastName = @LastName THEN 1 ELSE 0 END +
				CASE WHEN c.Address1 = @Address1 THEN 1 ELSE 0 END +
				CASE WHEN c.Postcode = @PostCode THEN 1 ELSE 0 END +
				CASE WHEN c.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [TotalMatch]
		FROM CustomerMatchKey c WITH (NOLOCK)
		WHERE c.ClientID = @ClientID
		AND (
			c.FirstName = @FirstName
			OR
			c.LastName = @LastName
			OR 
			c.Address1 = @Address1
			OR
			c.Postcode = @PostCode
			--OR
			--c.DateOfBirth = @DateOfBirth
		)
	)
	SELECT 	c.ClientID,
			c.CustomerID,		 
			c.FirstName, 
			c.LastName, 
			c.Address1, 
			c.PostCode,
			c.Fullname,
			c.DateOfBirth,
			cmk.Firstname FirstnameKey, 
			cmk.Lastname LastnameKey, 
			cmk.Address1 Address1Key, 
			cmk.Postcode PostcodeKey,
			cmk.DateOfBirth DateOfBirth

	FROM cmk
	INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = cmk.CustomerID AND c.Test = 0
	WHERE cmk.TotalMatch > 3

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetMatchingCustomers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMatchKey__GetMatchingCustomers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetMatchingCustomers] TO [sp_executeall]
GO
