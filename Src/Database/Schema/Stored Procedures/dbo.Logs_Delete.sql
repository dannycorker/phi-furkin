SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Logs table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_Delete]
(

	@LogID int   
)
AS


				DELETE FROM [dbo].[Logs] WITH (ROWLOCK) 
				WHERE
					[LogID] = @LogID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Delete] TO [sp_executeall]
GO
