SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 6-11-2014
-- Description:	Gets the SMS Complaint Screen title and logo for the given client
-- =============================================
CREATE PROCEDURE [dbo].[SMSComplaintConfiguration__GetByCLientID]
	@ClientID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM dbo.SMSComplaintConfiguration WITH (NOLOCK) WHERE ClientID=@ClientID
    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSComplaintConfiguration__GetByCLientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSComplaintConfiguration__GetByCLientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSComplaintConfiguration__GetByCLientID] TO [sp_executeall]
GO
