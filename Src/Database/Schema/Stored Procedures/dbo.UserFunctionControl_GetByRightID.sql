SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserFunctionControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_GetByRightID]
(

	@RightID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[UserFunctionControlID],
					[ClientPersonnelID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[UserFunctionControl] WITH (NOLOCK) 
				WHERE
					[RightID] = @RightID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_GetByRightID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_GetByRightID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_GetByRightID] TO [sp_executeall]
GO
