SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientDetailValues_Get_List]

AS


				
				SELECT
					[ClientDetailValueID],
					[ClientID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ClientDetailValues] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientDetailValues_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_Get_List] TO [sp_executeall]
GO
