SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderDecodeType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDecodeType_GetByDataLoaderDecodeTypeID]
(

	@DataLoaderDecodeTypeID int   
)
AS


				SELECT
					[DataLoaderDecodeTypeID],
					[DecodeTypeName],
					[DecodeTypeDescription]
				FROM
					[dbo].[DataLoaderDecodeType] WITH (NOLOCK) 
				WHERE
										[DataLoaderDecodeTypeID] = @DataLoaderDecodeTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_GetByDataLoaderDecodeTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDecodeType_GetByDataLoaderDecodeTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_GetByDataLoaderDecodeTypeID] TO [sp_executeall]
GO
