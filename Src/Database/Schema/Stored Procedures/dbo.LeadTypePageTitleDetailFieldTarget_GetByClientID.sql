SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitleDetailFieldTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleDetailFieldTarget_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadTypePageTitleDetailFieldTargetID],
					[ClientID],
					[LeadTypeID],
					[Target],
					[DetailFieldID],
					[TemplateTypeID],
					[DetailFieldAlias],
					[Notes]
				FROM
					[dbo].[LeadTypePageTitleDetailFieldTarget] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleDetailFieldTarget_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_GetByClientID] TO [sp_executeall]
GO
