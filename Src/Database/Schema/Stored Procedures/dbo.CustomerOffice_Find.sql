SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CustomerOffice table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_Find]
(

	@SearchUsingOR bit   = null ,

	@OfficeID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@OfficeName varchar (100)  = null ,

	@MainPhoneNumber varchar (100)  = null ,

	@FaxNumber varchar (100)  = null ,

	@Address1 varchar (100)  = null ,

	@Address2 varchar (100)  = null ,

	@Town varchar (100)  = null ,

	@County varchar (100)  = null ,

	@Postcode varchar (100)  = null ,

	@Country varchar (100)  = null ,

	@Notes varchar (255)  = null ,

	@CountryID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OfficeID]
	, [ClientID]
	, [CustomerID]
	, [OfficeName]
	, [MainPhoneNumber]
	, [FaxNumber]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [Notes]
	, [CountryID]
    FROM
	[dbo].[CustomerOffice] WITH (NOLOCK) 
    WHERE 
	 ([OfficeID] = @OfficeID OR @OfficeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([OfficeName] = @OfficeName OR @OfficeName IS NULL)
	AND ([MainPhoneNumber] = @MainPhoneNumber OR @MainPhoneNumber IS NULL)
	AND ([FaxNumber] = @FaxNumber OR @FaxNumber IS NULL)
	AND ([Address1] = @Address1 OR @Address1 IS NULL)
	AND ([Address2] = @Address2 OR @Address2 IS NULL)
	AND ([Town] = @Town OR @Town IS NULL)
	AND ([County] = @County OR @County IS NULL)
	AND ([Postcode] = @Postcode OR @Postcode IS NULL)
	AND ([Country] = @Country OR @Country IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OfficeID]
	, [ClientID]
	, [CustomerID]
	, [OfficeName]
	, [MainPhoneNumber]
	, [FaxNumber]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [Notes]
	, [CountryID]
    FROM
	[dbo].[CustomerOffice] WITH (NOLOCK) 
    WHERE 
	 ([OfficeID] = @OfficeID AND @OfficeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([OfficeName] = @OfficeName AND @OfficeName is not null)
	OR ([MainPhoneNumber] = @MainPhoneNumber AND @MainPhoneNumber is not null)
	OR ([FaxNumber] = @FaxNumber AND @FaxNumber is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([Postcode] = @Postcode AND @Postcode is not null)
	OR ([Country] = @Country AND @Country is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Find] TO [sp_executeall]
GO
