SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-02-15
-- Description:	SP For Filtering
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ShowCustomersWithData]
@ClientID int,
@SubClientID int,
@tvpIDValue tvpIDValueInt READONLY,
@ClientPersonnelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @realtablequery table (TableName varchar(100), ColumnName varchar(100), Criteria varchar(2000), Operator int)

	DECLARE @DistinctTables table (UNID int identity (1,1), TableName Varchar(100))

	DECLARE @Query varchar(max) = '', 
			@Cols varchar(max) = '', 
			@SelectStatement varchar(max) = '', 
			@WhereClause varchar(max) = '', 
			@Completestatement varchar(max),
			@FinalWhereClause varchar(max) = '', 
			@TestID int, 
			@Unid int, 
			@TableName varchar(100)

	SELECT @SelectStatement = 'SELECT CustomerID, FullName'

	IF EXISTS (SELECT * FROM DetailFields df WITH (NOLOCK) INNER JOIN @tvpIDValue t on t.AnyID = df.DetailFieldID and df.LeadOrMatter = 10)
	BEGIN 

		SELECT @Query = 'SELECT cdv.CustomerID, Customers.FullName, cdv.DetailValue, df.FieldCaption 
		FROM Customers WITH (NOLOCK)
		INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.CustomerID = Customers.CustomerID 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = cdv.DetailFieldID
		'

		SELECT @WhereClause = ' WHERE df.ClientID = ' + Convert(varchar,@ClientID) + '
		AND Customers.SubClientID = ' + Convert(varchar,@SubClientID) + '
		AND cdv.DetailFieldID IN ('

		SELECT @WhereClause = @WhereClause + CONVERT(varchar,df.DetailFieldID) + ','
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN @tvpIDValue t on t.AnyID = df.DetailFieldID 
		WHERE df.LeadOrMatter = 10
		
		SELECT @WhereClause = LEFT(@WhereClause, LEN(@WhereClause)-1) + ')'
		
		SELECT @Query = @Query + @WhereClause

	END

	/*Lead Info*/
	IF EXISTS (SELECT * FROM DetailFields df WITH (NOLOCK) INNER JOIN @tvpIDValue t on t.AnyID = df.DetailFieldID and df.LeadOrMatter = 1)
	BEGIN 

		IF @Query <> '' SELECT @Query = @Query + ' UNION ' 
		
		SELECT @Query = @Query + 
		'SELECT Customers.CustomerID, Customers.FullName, ldv.DetailValue, df.FieldCaption 
		FROM Customers WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = Customers.CustomerID
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ldv.DetailFieldID'

		SELECT @WhereClause = 
		' WHERE df.ClientID = ' + Convert(varchar,@ClientID) + '
		AND Customers.SubClientID = ' + Convert(varchar,@SubClientID) + '
		AND ldv.DetailFieldID IN ('

		SELECT @WhereClause = @WhereClause + CONVERT(varchar,df.DetailFieldID) + ','
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN @tvpIDValue t on t.AnyID = df.DetailFieldID 
		WHERE df.LeadOrMatter = 1

		SELECT @WhereClause = LEFT(@WhereClause, LEN(@WhereClause)-1) + ')'
		
		SELECT @Query = @Query + @WhereClause
		--SELECT @Query 
	END

	SELECT @Cols = COALESCE(@Cols + ',[' + FieldCaption + ']', '[' + FieldCaption + ']')
	FROM dbo.DetailFields df WITH (NOLOCK)	
	INNER JOIN @tvpIDValue t on t.AnyID = df.DetailFieldID
	WHERE df.Enabled = 1
	ORDER BY FieldOrder

	SELECT @Cols = RIGHT(@Cols, LEN(@Cols)-1)

	SELECT @SelectStatement = COALESCE(@SelectStatement + ',[' + FieldCaption + ']', '[' + FieldCaption + ']')
	FROM dbo.DetailFields df WITH (NOLOCK)	
	INNER JOIN @tvpIDValue t on t.AnyID = df.DetailFieldID
	WHERE df.Enabled = 1
	ORDER BY FieldOrder

	SELECT @FinalWhereClause = 
	' WHERE '

	SELECT @FinalWhereClause = @FinalWhereClause + 
		'pvt.[' + df.FieldCaption + ']  ' + 
	CASE t.AnyInt
		WHEN 1 THEN '= ''' + t.AnyValue + ''' AND '
		WHEN 2 THEN '<>''' + t.AnyValue + ''' AND '
		WHEN 3 THEN 'LIKE ''' + t.AnyValue + '%'' AND '
		WHEN 4 THEN 'NOT LIKE ''%' + t.AnyValue + '%'' AND '
		END 
	FROM @tvpIDValue t
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID

	--SELECT @Query, @SelectStatement, @FinalWhereClause

	select @Completestatement = @SelectStatement + 
	' FROM (' + @Query + ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'	 + @FinalWhereClause

	select @Completestatement = LEFT(@Completestatement,LEN(@Completestatement)-4)

	EXEC (@Completestatement)

	print @Completestatement

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ShowCustomersWithData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ShowCustomersWithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ShowCustomersWithData] TO [sp_executeall]
GO
