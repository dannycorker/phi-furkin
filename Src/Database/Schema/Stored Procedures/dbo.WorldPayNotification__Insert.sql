SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 27/07/2017
-- Description:	Inserts a WorldPay Notification
-- 2018-07-09 ACE - Use varchar for xml input as conversion error is happening due to single quotes..
-- =============================================
CREATE PROCEDURE [dbo].[WorldPayNotification__Insert]
	
	@CardTransactionID INT = NULL,
	@OrderCode VARCHAR(250) = NULL,
	@NotificationXml VARCHAR(MAX),
	@WhenCreated DATETIME
	
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @XML XML = CONVERT (XML, @NotificationXml,2)  

	INSERT INTO WorldPayNotification ([CardTransactionID],[OrderCode], [NotificationXml], [WhenCreated])		
	VALUES (@CardTransactionID, @OrderCode, @XML, @WhenCreated)
	
	SELECT SCOPE_IDENTITY() AS WorldPayNotificationID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayNotification__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorldPayNotification__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayNotification__Insert] TO [sp_executeall]
GO
