SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2011-03-16
-- Description:	Tell us all about a Workflow Group
-- =============================================

CREATE PROC [dbo].[WorkflowGroup__Describe] 
	@WorkflowGroupID int = null 
AS
BEGIN

	/*Group details*/
	SELECT g.ClientID, g.WorkflowGroupID, g.Name, g.Description, g.Enabled, cp.UserName as [Manager] 
	FROM dbo.WorkflowGroup g WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = g.ManagerID 
	WHERE g.WorkflowGroupID = @WorkflowGroupID 

	/*Automated tasks that populate the group*/
	SELECT at.ClientID,at.TaskID as [Populated By Task], at.Taskname, at.Description, at.Enabled,atp2.ParamValue as [Priority], at.RunAtHour, at.RunAtMinute, at.RepeatTimeQuantity as [Repeat Every], tu.TimeUnitsName as [Time Units], at.NextRunDateTime, count(et.EventTypeID) as [Trigger Events], ati.LastRowcount, ati.AvgRowcount, ati.MaxRowcount, 'exec dbo.AutomatedTask__RunNow ' + CONVERT(varchar,at.TaskID) [Rerun]
	FROM dbo.AutomatedTaskParam atp WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) on at.TaskID = atp.TaskID 
	LEFT JOIN dbo.TimeUnits tu WITH (NOLOCK) on tu.TimeUnitsID = at.RepeatTimeUnitsID 
	LEFT JOIN dbo.EventTypeAutomatedTask eta WITH (NOLOCK) on eta.AutomatedTaskID = at.TaskID
	LEFT JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = eta.EventTypeID 
	LEFT JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) on ati.TaskID = at.TaskID 
	LEFT JOIN dbo.AutomatedTaskParam atp2 WITH (NOLOCK) on atp2.TaskID = at.TaskID and atp2.ParamName = 'WORKFLOW_PRIORITY'
	WHERE at.WorkflowTask = 1
	and atp.ParamName = 'WORKFLOW_GROUP_ID'
	and atp.ParamValue = @WorkflowGroupID 
	GROUP BY at.ClientID,at.TaskID, at.Taskname, at.Description, at.Enabled, at.RunAtHour, at.RunAtMinute, at.RepeatTimeQuantity, tu.TimeUnitsName, at.NextRunDateTime, ati.LastRowcount, ati.AvgRowcount, ati.MaxRowcount,atp2.ParamValue 
	ORDER BY atp2.ParamValue asc
	
	/*Events that populate the group*/
	SELECT aw.EventTypeAutomatedWorkflowID, et.EventTypeID, et.EventTypeName, aw.FollowUp, aw.Important, aw.Priority, aw.RunAsUserID, cp.UserName
	FROM EventTypeAutomatedWorkflow aw WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = aw.EventTypeID 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = aw.RunAsUserID
	WHERE aw.WorkflowGroupID = @WorkflowGroupID
	
	/*Assigned personnel*/
	SELECT cp.ClientPersonnelID, cp.UserName, cpa.GroupName, a.Priority 
	FROM dbo.WorkflowGroupAssignment a WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = a.ClientPersonnelID 
	INNER JOIN dbo.ClientPersonnelAdminGroups cpa WITH (NOLOCK) on cpa.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID 
	WHERE a.WorkflowGroupID = @WorkflowGroupID  
	
	/*Completed*/
	SELECT count(*) as [Completed Tasks], MIN(t.CompletedOn) as [Earliest],MAX(t.CompletedOn) as [Latest]
	FROM dbo.WorkflowTaskCompleted t WITH (NOLOCK) 
	WHERE t.WorkflowGroupID = @WorkflowGroupID

	/*Tasks*/
	SELECT t.WorkflowTaskID, t.AutomatedTaskID, t.Priority, cp.UserName as [Assigned To], t.AssignedDate, t.LeadID, t.CaseID, et.EventTypeID, et.EventTypeName, t.ClientID, t.FollowUp, t.Important, t.CreationDate, t.Escalated, t.EscalatedBy, t.EscalationReason, t.EscalationDate, t.Disabled, t.DisabledDate, t.DisabledReason 
	FROM dbo.WorkflowTask t WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = t.AssignedTo 
	LEFT JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = t.EventTypeID 
	WHERE t.WorkflowGroupID = @WorkflowGroupID 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup__Describe] TO [sp_executehelper]
GO
