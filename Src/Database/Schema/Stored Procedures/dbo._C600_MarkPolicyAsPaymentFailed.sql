SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- ALTER date:	2015-01-07
-- Description:	MarkPolicyAsPaymentFailed
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MarkPolicyAsPaymentFailed]
(
	@TableRowID INT,
	@ErrorCode VARCHAR(2000)	
)
AS
BEGIN
	
	UPDATE TableDetailValues 
	SET DetailValue=69940
	WHERE DetailFieldID=170078 AND TableRowID=@TableRowID
--declare @TableRowID INT = 21837934
 
	SET NOCOUNT ON;
	
	DECLARE @AqAutomation INT,
			@DatePaid VARCHAR(10),
			@EventTypeID INT,
			@TransactionDateTime VARCHAR(16),
			@MatterID INT,
			@NewCollectionsTableRowID INT,
			@NewPremiumTableRowID INT,
			@PremiumTableRowID INT
	
	SELECT @AqAutomation=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (tr.ClientID,53,'CFG|AqAutomationCPID',0) 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.TableRowID=@TableRowID
	
	-- collections table (add failed row)
	EXEC @NewCollectionsTableRowID=_C600_CopyTableRow @TableRowID, NULL, NULL
	
	SELECT @TransactionDateTime=CONVERT(VARCHAR(16),dbo.fn_GetDate_Local(),120)
	
	EXEC _C00_SimpleValueIntoField 175467,@TransactionDateTime,@NewCollectionsTableRowID,@AqAutomation
	EXEC _C00_SimpleValueIntoField 175469,72151,@NewCollectionsTableRowID,@AqAutomation
	EXEC _C00_SimpleValueIntoField 175470,@ErrorCode,@NewCollectionsTableRowID,@AqAutomation

	-- premiums table (mark existing as failed & add a new as payment returned with a negative amount)
	-- mark existing as failed
	SELECT @PremiumTableRowID=ValueInt,
			@DatePaid=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
	FROM TableDetailValues WITH (NOLOCK) WHERE DetailFieldID=175471 AND TableRowID=@TableRowID
	
	EXEC _C00_SimpleValueIntoField 170078,69940,@PremiumTableRowID,@AqAutomation -- status
	EXEC _C00_SimpleValueIntoField 175473,@DatePaid,@PremiumTableRowID,@AqAutomation -- Date Paid
	
	-- add a new row as payment returned with a negative amount
	EXEC @NewPremiumTableRowID = _C600_CopyTableRow @PremiumTableRowID, NULL, NULL
	EXEC _C00_SimpleValueIntoField 170078,72139,@NewPremiumTableRowID,@AqAutomation -- status
	EXEC _C00_SimpleValueIntoField 170066,@DatePaid,@NewPremiumTableRowID,@AqAutomation -- Date
	EXEC _C00_SimpleValueIntoField 175473,@DatePaid,@NewPremiumTableRowID,@AqAutomation -- Date Paid
	EXEC _C00_SimpleValueIntoField 170073,@PremiumTableRowID,@NewPremiumTableRowID,@AqAutomation -- crid
	UPDATE TableDetailValues SET DetailValue=-(ValueMoney) WHERE DetailFieldID=170072 AND TableRowID=@NewPremiumTableRowID -- negative Grand Total

	-- Add Next Event on collections lead
	SELECT @MatterID=tr.MatterID, @EventTypeID=CASE le.EventTypeID
												WHEN 155370 THEN 155290
												WHEN 155302 THEN 155290
												WHEN 155371 THEN 155298
												WHEN 155372 THEN 155300
												END
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON tr.MatterID=m.MatterID
	INNER JOIN Cases ca WITH (NOLOCK) ON m.CaseID=ca.CaseID
	INNER JOIN LeadEvent le WITH (NOLOCK) ON le.CaseID=ca.CaseID and le.EventDeleted=0 AND le.WhenFollowedUp IS NULL
	WHERE tr.TableRowID=@TableRowID	
	
	IF @EventTypeID IS NOT NULL
	BEGIN
	
		EXEC _C600_AddAutomatedEvent @MatterID, @EventTypeID, 1
		
		-- if there is going to be a further retry to collect add a new pending row into the premiums table
		IF @EventTypeID IN (155290,155298)
		BEGIN
	
			EXEC @NewPremiumTableRowID = _C600_CopyTableRow @PremiumTableRowID, NULL, NULL
			EXEC _C00_SimpleValueIntoField 170078,69903,@NewPremiumTableRowID,@AqAutomation -- status
			EXEC _C00_SimpleValueIntoField 170066,@DatePaid,@NewPremiumTableRowID,@AqAutomation -- Date
			EXEC _C00_SimpleValueIntoField 175473,'',@NewPremiumTableRowID,@AqAutomation -- Date Paid
		
		END
	
	END	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MarkPolicyAsPaymentFailed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MarkPolicyAsPaymentFailed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MarkPolicyAsPaymentFailed] TO [sp_executeall]
GO
