SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditingGrouping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingGrouping_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingGroupingID int   = null ,

	@ClientID int   = null ,

	@SqlQueryEditingID int   = null ,

	@GroupByClause varchar (2000)  = null ,

	@HavingClause varchar (500)  = null ,

	@HavingColumnID int   = null ,

	@HavingCriteria1 varchar (250)  = null ,

	@HavingCriteria2 varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingGroupingID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [GroupByClause]
	, [HavingClause]
	, [HavingColumnID]
	, [HavingCriteria1]
	, [HavingCriteria2]
    FROM
	[dbo].[SqlQueryEditingGrouping] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingGroupingID] = @SqlQueryEditingGroupingID OR @SqlQueryEditingGroupingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([GroupByClause] = @GroupByClause OR @GroupByClause IS NULL)
	AND ([HavingClause] = @HavingClause OR @HavingClause IS NULL)
	AND ([HavingColumnID] = @HavingColumnID OR @HavingColumnID IS NULL)
	AND ([HavingCriteria1] = @HavingCriteria1 OR @HavingCriteria1 IS NULL)
	AND ([HavingCriteria2] = @HavingCriteria2 OR @HavingCriteria2 IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingGroupingID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [GroupByClause]
	, [HavingClause]
	, [HavingColumnID]
	, [HavingCriteria1]
	, [HavingCriteria2]
    FROM
	[dbo].[SqlQueryEditingGrouping] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingGroupingID] = @SqlQueryEditingGroupingID AND @SqlQueryEditingGroupingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([GroupByClause] = @GroupByClause AND @GroupByClause is not null)
	OR ([HavingClause] = @HavingClause AND @HavingClause is not null)
	OR ([HavingColumnID] = @HavingColumnID AND @HavingColumnID is not null)
	OR ([HavingCriteria1] = @HavingCriteria1 AND @HavingCriteria1 is not null)
	OR ([HavingCriteria2] = @HavingCriteria2 AND @HavingCriteria2 is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingGrouping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_Find] TO [sp_executeall]
GO
