SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadTypePageTitle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitle_Insert]
(

	@LeadTypeID int   ,

	@PageTitle varchar (100)  
)
AS


				
				INSERT INTO [dbo].[LeadTypePageTitle]
					(
					[LeadTypeID]
					,[PageTitle]
					)
				VALUES
					(
					@LeadTypeID
					,@PageTitle
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitle_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Insert] TO [sp_executeall]
GO
