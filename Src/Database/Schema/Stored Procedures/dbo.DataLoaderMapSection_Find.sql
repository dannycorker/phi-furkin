SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderMapSection table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderMapSectionID int   = null ,

	@ClientID int   = null ,

	@DataLoaderMapID int   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@DataLoaderObjectActionID int   = null ,

	@DataLoaderSectionLocaterTypeID int   = null ,

	@DetailFieldSubTypeID tinyint   = null ,

	@HasAquariumID bit   = null ,

	@NumberOfFooterRowsToSkip int   = null ,

	@ImportIntoLeadManager bit   = null ,

	@IsFixedLengthSection bit   = null ,

	@FixedLengthNumberOfRows int   = null ,

	@IsMandatory bit   = null ,

	@IsMultipleAllowedWithinFile bit   = null ,

	@Notes varchar (2000)  = null ,

	@TableDetailFieldID int   = null ,

	@ResourceListDetailFieldID int   = null ,

	@CreatePaymentSchedule bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderMapSectionID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderObjectTypeID]
	, [DataLoaderObjectActionID]
	, [DataLoaderSectionLocaterTypeID]
	, [DetailFieldSubTypeID]
	, [HasAquariumID]
	, [NumberOfFooterRowsToSkip]
	, [ImportIntoLeadManager]
	, [IsFixedLengthSection]
	, [FixedLengthNumberOfRows]
	, [IsMandatory]
	, [IsMultipleAllowedWithinFile]
	, [Notes]
	, [TableDetailFieldID]
	, [ResourceListDetailFieldID]
	, [CreatePaymentSchedule]
    FROM
	[dbo].[DataLoaderMapSection] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderMapSectionID] = @DataLoaderMapSectionID OR @DataLoaderMapSectionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DataLoaderMapID] = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	AND ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([DataLoaderObjectActionID] = @DataLoaderObjectActionID OR @DataLoaderObjectActionID IS NULL)
	AND ([DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID OR @DataLoaderSectionLocaterTypeID IS NULL)
	AND ([DetailFieldSubTypeID] = @DetailFieldSubTypeID OR @DetailFieldSubTypeID IS NULL)
	AND ([HasAquariumID] = @HasAquariumID OR @HasAquariumID IS NULL)
	AND ([NumberOfFooterRowsToSkip] = @NumberOfFooterRowsToSkip OR @NumberOfFooterRowsToSkip IS NULL)
	AND ([ImportIntoLeadManager] = @ImportIntoLeadManager OR @ImportIntoLeadManager IS NULL)
	AND ([IsFixedLengthSection] = @IsFixedLengthSection OR @IsFixedLengthSection IS NULL)
	AND ([FixedLengthNumberOfRows] = @FixedLengthNumberOfRows OR @FixedLengthNumberOfRows IS NULL)
	AND ([IsMandatory] = @IsMandatory OR @IsMandatory IS NULL)
	AND ([IsMultipleAllowedWithinFile] = @IsMultipleAllowedWithinFile OR @IsMultipleAllowedWithinFile IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([TableDetailFieldID] = @TableDetailFieldID OR @TableDetailFieldID IS NULL)
	AND ([ResourceListDetailFieldID] = @ResourceListDetailFieldID OR @ResourceListDetailFieldID IS NULL)
	AND ([CreatePaymentSchedule] = @CreatePaymentSchedule OR @CreatePaymentSchedule IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderMapSectionID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderObjectTypeID]
	, [DataLoaderObjectActionID]
	, [DataLoaderSectionLocaterTypeID]
	, [DetailFieldSubTypeID]
	, [HasAquariumID]
	, [NumberOfFooterRowsToSkip]
	, [ImportIntoLeadManager]
	, [IsFixedLengthSection]
	, [FixedLengthNumberOfRows]
	, [IsMandatory]
	, [IsMultipleAllowedWithinFile]
	, [Notes]
	, [TableDetailFieldID]
	, [ResourceListDetailFieldID]
	, [CreatePaymentSchedule]
    FROM
	[dbo].[DataLoaderMapSection] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderMapSectionID] = @DataLoaderMapSectionID AND @DataLoaderMapSectionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DataLoaderMapID] = @DataLoaderMapID AND @DataLoaderMapID is not null)
	OR ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([DataLoaderObjectActionID] = @DataLoaderObjectActionID AND @DataLoaderObjectActionID is not null)
	OR ([DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID AND @DataLoaderSectionLocaterTypeID is not null)
	OR ([DetailFieldSubTypeID] = @DetailFieldSubTypeID AND @DetailFieldSubTypeID is not null)
	OR ([HasAquariumID] = @HasAquariumID AND @HasAquariumID is not null)
	OR ([NumberOfFooterRowsToSkip] = @NumberOfFooterRowsToSkip AND @NumberOfFooterRowsToSkip is not null)
	OR ([ImportIntoLeadManager] = @ImportIntoLeadManager AND @ImportIntoLeadManager is not null)
	OR ([IsFixedLengthSection] = @IsFixedLengthSection AND @IsFixedLengthSection is not null)
	OR ([FixedLengthNumberOfRows] = @FixedLengthNumberOfRows AND @FixedLengthNumberOfRows is not null)
	OR ([IsMandatory] = @IsMandatory AND @IsMandatory is not null)
	OR ([IsMultipleAllowedWithinFile] = @IsMultipleAllowedWithinFile AND @IsMultipleAllowedWithinFile is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([TableDetailFieldID] = @TableDetailFieldID AND @TableDetailFieldID is not null)
	OR ([ResourceListDetailFieldID] = @ResourceListDetailFieldID AND @ResourceListDetailFieldID is not null)
	OR ([CreatePaymentSchedule] = @CreatePaymentSchedule AND @CreatePaymentSchedule is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Find] TO [sp_executeall]
GO
