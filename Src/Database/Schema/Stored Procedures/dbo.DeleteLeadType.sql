SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Deletes a record in the LeadType table including any related records
--			that cannot be included in the cascaded delete (DetailFieldAlias).
--          JWG 2009-06-03 Added a few new tables (fields, pages, linked fields) and check no leads exist.
--          JWG 2009-08-27 Don't do this during the day any more. It locks the DB for hours at a time.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DeleteLeadType]
(
	@LeadTypeID int   
)
AS
BEGIN

	SET NOCOUNT ON
	
	-- Check no leads exist first
	IF NOT EXISTS (SELECT * FROM dbo.Lead (nolock) WHERE LeadTypeID = @LeadTypeID)
	BEGIN
		
		/* 
			First clear out any DetailFieldAlias records, and any other 
			associated tables.  Some cannot be included in the cascade 
			delete for LeadType, so we must delete them manually in a sensible order.
		*/
		/*DELETE dbo.DetailFieldAlias 
		WHERE LeadTypeID = @LeadTypeID 

		DELETE dbo.DetailFieldLink 
		WHERE LeadTypeID = @LeadTypeID 

		DELETE dbo.DetailFields 
		WHERE LeadTypeID = @LeadTypeID 

		DELETE dbo.DetailFieldPages 
		WHERE LeadTypeID = @LeadTypeID 
		*/
		/*
			There are dozens of other tables that the cascades should take care of.
			These tables can all be found with this sql statement if needed:
			
			select * 
			from information_schema.columns 
			where column_name = 'LeadTypeID' 
			order by table_name, column_name 
		*/
		
		/* 
			Finally delete the LeadType Record
		*/
		/*
		DELETE dbo.LeadType 
		WHERE LeadTypeID = @LeadTypeID 
		*/
		INSERT INTO dbo.HskLeadTypeToDelete (LeadTypeID, ClientID, WhenRequested, Comments, FailedToDelete) 
		SELECT lt.LeadTypeID, lt.ClientID, dbo.fn_GetDate_Local(), 'LeadTypeCopier requested the delete', 0 
		FROM dbo.LeadType lt (nolock) 
		WHERE lt.LeadTypeID = @LeadTypeID 
		
		UPDATE dbo.LeadType 
		SET [Enabled] = 0 
		WHERE LeadTypeID = @LeadTypeID 
		
	END
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLeadType] TO [sp_executeall]
GO
