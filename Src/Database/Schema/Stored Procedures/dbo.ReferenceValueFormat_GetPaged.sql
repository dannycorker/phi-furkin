SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the ReferenceValueFormat table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueFormat_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ReferenceValueFormatID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([ReferenceValueFormatID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ReferenceValueFormatID]'
				SET @SQL = @SQL + ' FROM [dbo].[ReferenceValueFormat] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[ReferenceValueFormatID], O.[ClientID], O.[ReferenceValueName], O.[ReferenceValueDescription], O.[IntegerSectionSize], O.[IntegerStartPoint], O.[DateFormat], O.[YearFormat], O.[DelimiterID], O.[Section1TypeID], O.[Section1DetailFieldID], O.[Section2TypeID], O.[Section2DetailFieldID], O.[Section3TypeID], O.[Section3DetailFieldID], O.[Section4TypeID], O.[Section4DetailFieldID], O.[Section5TypeID], O.[Section5DetailFieldID], O.[LeftPadIntegerSection], O.[IncludeBlankSections], O.[EnforceUniqueness], O.[SourceID]
				FROM
				    [dbo].[ReferenceValueFormat] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[ReferenceValueFormatID] = PageIndex.[ReferenceValueFormatID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ReferenceValueFormat] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueFormat_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_GetPaged] TO [sp_executeall]
GO
