SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the HyperlinkSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget_Delete]
(

	@HyperlinkSpecialisedDetailFieldTargetID int   
)
AS


				DELETE FROM [dbo].[HyperlinkSpecialisedDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[HyperlinkSpecialisedDetailFieldTargetID] = @HyperlinkSpecialisedDetailFieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Delete] TO [sp_executeall]
GO
