SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-10-2012
-- Description:	Updates the attempted logins count for ThirdPartyMappingUserCredential
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential__UpdatedAttemptedLogins]

	@ThirdPartyMappingUserCredentialID INT,
	@Reset BIT = 0
	
AS
BEGIN

	DECLARE @AttemptedLogins INT	
	IF @Reset = 1
	BEGIN
		SET @AttemptedLogins = 0
	END
	ELSE
	BEGIN
		
		SELECT @AttemptedLogins=AttemptedLogins 
		FROM ThirdPartyMappingUserCredential
		WHERE ThirdPartyMappingUserCredentialID = @ThirdPartyMappingUserCredentialID
		
		IF @AttemptedLogins is null
		BEGIN
			SET @AttemptedLogins = 1
		END
		ELSE
		BEGIN
			SET @AttemptedLogins = @AttemptedLogins + 1		
		END
	END
	
	UPDATE ThirdPartyMappingUserCredential
	SET AttemptedLogins = @AttemptedLogins
	WHERE ThirdPartyMappingUserCredentialID = @ThirdPartyMappingUserCredentialID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__UpdatedAttemptedLogins] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential__UpdatedAttemptedLogins] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__UpdatedAttemptedLogins] TO [sp_executeall]
GO
