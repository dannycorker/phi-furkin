SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Chris Townsend
-- Create date: 17th August 2007
-- Description:	Gets a list of all potential fax numbers
--				that the user can send a fax to
-- =============================================
CREATE PROCEDURE [dbo].[GetSendableFaxNumbersForLead] 

@leadID int,
@caseID int,
@clientID int

AS
BEGIN

	SET NOCOUNT ON

	DECLARE @SQLStatement varchar(5000)

IF @leadID is not null -- get all LeadDetail fax number fields for the current lead
BEGIN	
	
	SELECT @SQLStatement =  
	'
	-- UNION	
	
	SELECT null As MatterID, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, ldv.DetailValue AS FaxNumber, ldv.EncryptedValue
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN LeadDetailValues ldv WITH (NOLOCK)  ON ldv.DetailFieldID = df.DetailFieldID
	WHERE df.QuestionTypeID = 18 -- Type is "fax number"
	AND ldv.LeadID = ' + CAST(@leadID AS VARCHAR(20)) + 
	'AND ldv.DetailValue <> '''' '

	IF @caseID is not null -- get all MatterDetail fax number fields for the current case
	BEGIN

	SELECT @SQLStatement = @SQLStatement + 
		
		' UNION
		
		SELECT mdv.MatterID AS MatterID, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, mdv.DetailValue AS FaxNumber, mdv.EncryptedValue
		FROM DetailFields df  WITH (NOLOCK) 
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK)  ON mdv.DetailFieldID = df.DetailFieldID
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID
		WHERE df.QuestionTypeID = 18 -- Type is "fax number"
		AND mdv.LeadID = ' + CAST(@leadID AS VARCHAR(20)) + 
		'AND m.CaseID = ' + CAST(@caseID AS VARCHAR(20)) +
		'AND mdv.DetailValue <> '''' '

	SELECT @SQLStatement = @SQLStatement +
		' UNION
		
		SELECT null As MatterID, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, rldv.DetailValue AS FaxNumber, rldv.EncryptedValue
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK)  ON rldv.DetailFieldID = df.DetailFieldID
		INNER JOIN DetailFieldPages dfp ON df.DetailFieldPageID = dfp.DetailFieldPageID
		WHERE rldv.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + 
		'AND df.QuestionTypeID = 18 -- Type is "fax number"
		AND rldv.DetailValue <>'''' '

	END
END

EXEC(@SQLStatement)

END






GO
GRANT VIEW DEFINITION ON  [dbo].[GetSendableFaxNumbersForLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSendableFaxNumbersForLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSendableFaxNumbersForLead] TO [sp_executeall]
GO
