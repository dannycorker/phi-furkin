SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 27-02-2015
-- Description:	Gets a list of standard targets by the script identity and
-- the table name and column name
-- =============================================
CREATE PROCEDURE [dbo].[ScriptStandardTarget__GetByScriptIDObjectNameAndPropertyName]
	@ScriptID INT,
	@TableName NVARCHAR(250),
	@ColumnName NVARCHAR(250)
AS
BEGIN

	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM ScriptStandardTarget WITH (NOLOCK) 
	WHERE ScriptID=@ScriptID and ObjectName=@TableName And PropertyName=@ColumnName

END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget__GetByScriptIDObjectNameAndPropertyName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget__GetByScriptIDObjectNameAndPropertyName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget__GetByScriptIDObjectNameAndPropertyName] TO [sp_executeall]
GO
