SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the WorkflowTaskCompleted table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTaskCompleted_Insert]
(

	@WorkflowTaskCompletedID int    OUTPUT,

	@ClientID int   ,

	@WorkflowTaskID int   ,

	@WorkflowGroupID int   ,

	@AutomatedTaskID int   ,

	@Priority int   ,

	@AssignedTo int   ,

	@AssignedDate datetime   ,

	@LeadID int   ,

	@CaseID int   ,

	@EventTypeID int   ,

	@FollowUp bit   ,

	@Important bit   ,

	@CreationDate datetime   ,

	@Escalated bit   ,

	@EscalatedBy int   ,

	@EscalationReason varchar (255)  ,

	@EscalationDate datetime   ,

	@Disabled bit   ,

	@CompletedBy int   ,

	@CompletedOn datetime   ,

	@CompletionDescription varchar (255)  
)
AS


				
				INSERT INTO [dbo].[WorkflowTaskCompleted]
					(
					[ClientID]
					,[WorkflowTaskID]
					,[WorkflowGroupID]
					,[AutomatedTaskID]
					,[Priority]
					,[AssignedTo]
					,[AssignedDate]
					,[LeadID]
					,[CaseID]
					,[EventTypeID]
					,[FollowUp]
					,[Important]
					,[CreationDate]
					,[Escalated]
					,[EscalatedBy]
					,[EscalationReason]
					,[EscalationDate]
					,[Disabled]
					,[CompletedBy]
					,[CompletedOn]
					,[CompletionDescription]
					)
				VALUES
					(
					@ClientID
					,@WorkflowTaskID
					,@WorkflowGroupID
					,@AutomatedTaskID
					,@Priority
					,@AssignedTo
					,@AssignedDate
					,@LeadID
					,@CaseID
					,@EventTypeID
					,@FollowUp
					,@Important
					,@CreationDate
					,@Escalated
					,@EscalatedBy
					,@EscalationReason
					,@EscalationDate
					,@Disabled
					,@CompletedBy
					,@CompletedOn
					,@CompletionDescription
					)
				-- Get the identity value
				SET @WorkflowTaskCompletedID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTaskCompleted_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_Insert] TO [sp_executeall]
GO
