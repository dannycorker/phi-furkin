SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================  
-- Author:		Jim Green 
-- Create date: 2014-04-03 
-- Description: Show useful datepart and datename info 
-- =============================================  
CREATE PROCEDURE [dbo].[dow]   
AS  
BEGIN  

	SET NOCOUNT ON;  
	
	PRINT 'The current date and time is ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120) 
	PRINT 'SELECT CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)'
	PRINT ''
	PRINT 'The month is ' + DATENAME(MONTH, dbo.fn_GetDate_Local())
	PRINT 'SELECT DATENAME(MONTH, dbo.fn_GetDate_Local())'
	PRINT ''
	PRINT 'Today is a ' + DATENAME(WEEKDAY, dbo.fn_GetDate_Local())
	PRINT 'SELECT DATENAME(WEEKDAY, dbo.fn_GetDate_Local())'
	PRINT ''
	PRINT 'The day number within this week is ' + CONVERT(VARCHAR, DATEPART(WEEKDAY, dbo.fn_GetDate_Local()))
	PRINT 'SELECT CONVERT(VARCHAR, DATEPART(WEEKDAY, dbo.fn_GetDate_Local()))'
	PRINT ''
	PRINT 'The day number within this year is ' + CONVERT(VARCHAR, DATEPART(DY, dbo.fn_GetDate_Local()))
	PRINT 'SELECT CONVERT(VARCHAR, DATEPART(DY, dbo.fn_GetDate_Local()))'
	PRINT ''
	PRINT 'The quarter is ' + CONVERT(VARCHAR, DATEPART(QUARTER, dbo.fn_GetDate_Local()))
	PRINT 'SELECT CONVERT(VARCHAR, DATEPART(QUARTER, dbo.fn_GetDate_Local()))'
	PRINT ''
	PRINT 'The week number is ' + CONVERT(VARCHAR, DATEPART(WEEK, dbo.fn_GetDate_Local())) + ' and the ISO week number is ' + CONVERT(VARCHAR, DATEPART(ISO_WEEK, dbo.fn_GetDate_Local()))
	PRINT 'SELECT CONVERT(VARCHAR, DATEPART(WEEK, dbo.fn_GetDate_Local())); SELECT CONVERT(VARCHAR, DATEPART(ISO_WEEK, dbo.fn_GetDate_Local()))'
	PRINT ''
	PRINT 'Useful dateparts are: year, quarter, month, dayofyear, day, week, weekday, hour, minute, second, millisecond, microsecond, nanosecond, TZoffset, ISO_WEEK'
	 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[dow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dow] TO [sp_executeall]
GO
