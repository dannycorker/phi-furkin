SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuillConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationClient_Get_List]

AS


				
				SELECT
					[QuillConfigurationClientID],
					[AquariumClientID],
					[QuillClientID],
					[Username],
					[Password],
					[URI]
				FROM
					[dbo].[QuillConfigurationClient] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationClient_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Get_List] TO [sp_executeall]
GO
