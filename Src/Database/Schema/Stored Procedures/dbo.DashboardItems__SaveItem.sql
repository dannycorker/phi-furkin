SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-10
-- Description:	Save Dashboard Group
-- ACE 2016-04-14 Added enabled
-- =============================================
CREATE PROCEDURE [dbo].[DashboardItems__SaveItem]
	@DashboardItemID INT,
	@DashboardGroupID INT,
	@ClientID INT,
	@ClientPersonnelID INT,
	@ShowAs VARCHAR(100),
	@Title VARCHAR(100),
	@QueryID INT,
	@TransposeRequired INT,
	@ChartType VARCHAR(10),
	@Enabled BIT
AS
BEGIN

	SET NOCOUNT ON;

	IF @DashboardItemID > 0
	BEGIN
	
		UPDATE DashboardItems 
		SET DashboardGroupID = @DashboardGroupID, 
			ShowAs = @ShowAs, 
			Title = @Title, 
			QueryID = @QueryID, 
			TransposeRequired = @TransposeRequired, 
			ChartType = @ChartType, 
			Enabled = @Enabled, 
			WhenChanged = dbo.fn_GetDate_Local(), 
			WhoChanged = @ClientPersonnelID
		WHERE DashboardItemID = @DashboardItemID

	END
	ELSE
	BEGIN
	
		INSERT INTO DashboardItems (DashboardGroupID, ShowAs, Title, QueryID, TransposeRequired, ChartType, Enabled, WhenCreated, WhoCreated, WhenChanged, WhoChanged)
		VALUES (@DashboardGroupID, @ShowAs, @Title, @QueryID, @TransposeRequired, @ChartType, @Enabled, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItems__SaveItem] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItems__SaveItem] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItems__SaveItem] TO [sp_executeall]
GO
