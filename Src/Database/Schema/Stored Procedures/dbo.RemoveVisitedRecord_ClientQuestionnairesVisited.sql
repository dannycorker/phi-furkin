SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisited] @id int
AS
BEGIN
Delete From ClientQuestionnairesVisited where ClientQuestionnairesVisitedID = @id
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisited] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisited] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisited] TO [sp_executeall]
GO
