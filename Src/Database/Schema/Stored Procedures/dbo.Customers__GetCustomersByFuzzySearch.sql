SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 04/03/08
-- Description:	Used to perform a thorough search
--				of the database	to return results
--				from Customers (Individuals &
--				Businesses) AND Contacts
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetCustomersByFuzzySearch]
	-- Add the parameters for the stored procedure here
	@SearchText varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @SearchTerm varchar(100)
SET @SearchTerm = '%' + @SearchText + '%'

--First SELECT to choose Individual Customers
SELECT TOP 5
	Customers.CustomerID,
	Customers.TitleID,
	Customers.Fullname,
	Customers.EmailAddress,
	Customers.Town,
	Customers.County,
	Customers.Postcode
FROM Customers
WHERE
-- Customers.Type = 1 AND (
	Customers.Fullname LIKE @SearchTerm
OR	Customers.EmailAddress LIKE @SearchTerm
OR	Customers.Address1 LIKE @SearchTerm
OR	Customers.Address2 LIKE @SearchTerm
OR	Customers.Town LIKE @SearchTerm
OR	Customers.County LIKE @SearchTerm
OR	Customers.Postcode LIKE @SearchTerm
OR	Customers.CompanyName LIKE @SearchTerm
OR	Customers.Employer LIKE @SearchTerm
-- )

--Second SELECT to choose Business Customers
SELECT TOP 10
	Customers.CustomerID,
	Customers.TitleID,
	Customers.Fullname,
	Customers.EmailAddress,
	Customers.Town,
	Customers.County,
	Customers.Postcode
FROM Customers
WHERE
-- Customers.Type = 2 AND (
	Customers.Fullname LIKE @SearchTerm
OR	Customers.EmailAddress LIKE @SearchTerm
OR	Customers.Address1 LIKE @SearchTerm
OR	Customers.Address2 LIKE @SearchTerm
OR	Customers.Town LIKE @SearchTerm
OR	Customers.County LIKE @SearchTerm
OR	Customers.Postcode LIKE @SearchTerm
OR	Customers.CompanyName LIKE @SearchTerm
OR	Customers.Employer LIKE @SearchTerm
-- )

--Second SELECT to choose Business Customers
SELECT TOP 50
	Customers.CustomerID,
	Customers.TitleID,
	Customers.Fullname,
	Customers.EmailAddress,
	Customers.Town,
	Customers.County,
	Customers.Postcode
FROM Customers
WHERE
-- Customers.Type = 2 AND (
	Customers.Fullname LIKE @SearchTerm
OR	Customers.EmailAddress LIKE @SearchTerm
OR	Customers.Address1 LIKE @SearchTerm
OR	Customers.Address2 LIKE @SearchTerm
OR	Customers.Town LIKE @SearchTerm
OR	Customers.County LIKE @SearchTerm
OR	Customers.Postcode LIKE @SearchTerm
OR	Customers.CompanyName LIKE @SearchTerm
OR	Customers.Employer LIKE @SearchTerm
-- )

--Third SELECT to choose Contacts
-- SELECT * FROM BusinessContacts
-- WHERE
--		BusinessContacts.Fullname LIKE @SearchTerm
-- OR	BusinessContacts.EmailAddress LIKE @SearchTerm
-- OR	BusinessContacts.Address1 LIKE @SearchTerm
-- OR	BusinessContacts.Address2 LIKE @SearchTerm
-- OR	BusinessContacts.Town LIKE @SearchTerm
-- OR	BusinessContacts.County LIKE @SearchTerm
-- OR	BusinessContacts.Postcode LIKE @SearchTerm
-- OR	BusinessContacts.CompanyName LIKE @SearchTerm
-- OR	BusinessContacts.Employer LIKE @SearchTerm


END






GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetCustomersByFuzzySearch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetCustomersByFuzzySearch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetCustomersByFuzzySearch] TO [sp_executeall]
GO
