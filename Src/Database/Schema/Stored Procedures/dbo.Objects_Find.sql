SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Objects table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Objects_Find]
(

	@SearchUsingOR bit   = null ,

	@ObjectID int   = null ,

	@ClientID int   = null ,

	@SubClientID int   = null ,

	@ObjectTypeID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@Name varchar (250)  = null ,

	@Details varchar (2000)  = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ObjectID]
	, [ClientID]
	, [SubClientID]
	, [ObjectTypeID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Name]
	, [Details]
	, [SourceID]
    FROM
	[dbo].[Objects] WITH (NOLOCK) 
    WHERE 
	 ([ObjectID] = @ObjectID OR @ObjectID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([ObjectTypeID] = @ObjectTypeID OR @ObjectTypeID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Details] = @Details OR @Details IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ObjectID]
	, [ClientID]
	, [SubClientID]
	, [ObjectTypeID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Name]
	, [Details]
	, [SourceID]
    FROM
	[dbo].[Objects] WITH (NOLOCK) 
    WHERE 
	 ([ObjectID] = @ObjectID AND @ObjectID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([ObjectTypeID] = @ObjectTypeID AND @ObjectTypeID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Details] = @Details AND @Details is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Objects_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Find] TO [sp_executeall]
GO
