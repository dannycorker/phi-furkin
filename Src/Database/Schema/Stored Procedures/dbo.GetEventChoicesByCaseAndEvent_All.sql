SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-09-21
-- Description:	For use in GetEventChoicesByCaseAndEvent proc
-- =============================================
CREATE PROCEDURE [dbo].[GetEventChoicesByCaseAndEvent_All] 
	
	@ClientID INT,
	@ThreadNumber INT,
	@FromEventTypeID INT
	
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT ec.EventChoiceID, ec.NextEventTypeID, net.EventTypeName, ec.ThreadNumber, ec.Description, net.EventSubtypeID, ec.Weighting
	FROM dbo.EventChoice ec WITH (NOLOCK) 
	INNER JOIN EventType net WITH (NOLOCK) ON net.EventTypeID = ec.NextEventTypeID 
	WHERE net.Enabled = 1
	AND net.AvailableManually = 1
	AND ec.EscalationEvent = 0
	AND ec.EventTypeID = @FromEventTypeID 
	AND ec.ThreadNumber = @ThreadNumber
	AND ec.ClientID = @ClientID
	AND net.ClientID = @ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoicesByCaseAndEvent_All] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventChoicesByCaseAndEvent_All] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoicesByCaseAndEvent_All] TO [sp_executeall]
GO
