SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-02-29
-- Description:	List all Tracking records for a client
-- =============================================
CREATE PROCEDURE [dbo].[tr] 
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;
	
	Select	t.TrackingID, 
			t.TrackingName, 
			CONVERT(varchar(10),t.StartDate,120) [StartDate], 
			CONVERT(varchar(10),t.EndDate,120)[EndDate],
			t.ClientQuestionnaireID, 
			clq.QuestionnaireTitle [Title], 
			COUNT(cq.CustomerQuestionnaireID) [Submitted], 
			'https://aquarium-software.com/QuestionnairePreview.aspx?id=' + CONVERT(varchar,t.ClientQuestionnaireID) + '&trackingID=' + CONVERT(varchar,t.TrackingID) [URL]
	From Tracking t WITH (NOLOCK)
	LEFT JOIN dbo.CustomerQuestionnaires cq WITH (NOLOCK) on cq.TrackingID = t.TrackingID  
	INNER JOIN dbo.ClientQuestionnaires clq WITH (NOLOCK) on clq.ClientQuestionnaireID = t.ClientQuestionnaireID 
	WHERE t.ClientID = @ClientID 
	GROUP BY t.TrackingID, t.TrackingName, t.StartDate, t.EndDate, t.ClientQuestionnaireID, clq.QuestionnaireTitle 
	ORDER BY t.ClientQuestionnaireID,t.TrackingID 
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[tr] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[tr] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[tr] TO [sp_executeall]
GO
