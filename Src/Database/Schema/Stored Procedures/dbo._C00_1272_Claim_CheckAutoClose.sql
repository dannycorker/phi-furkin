SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-26
-- Description:	Deducts any outstanding owed premium from the claim
-- Used by:		DF 145676
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_CheckAutoClose] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CaseStatus INT,
			@EventCount INT,
			@CaseID INT
			
	SELECT @CaseID = CaseID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
			
	SELECT @CaseStatus = c.ClientStatusID
	FROM dbo.Cases c WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	
	SELECT @EventCount = COUNT(*) 
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	AND le.EventDeleted = 0
			
	
	SELECT	CASE 
				WHEN @CaseStatus = 4547 THEN 1
				WHEN @EventCount > 0 THEN 1
				ELSE NULL
			END AS ProcessStartLeadEventID
	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_CheckAutoClose] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_CheckAutoClose] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_CheckAutoClose] TO [sp_executeall]
GO
