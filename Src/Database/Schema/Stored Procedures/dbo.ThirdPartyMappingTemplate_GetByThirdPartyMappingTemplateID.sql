SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingTemplate table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyMappingTemplateID]
(

	@ThirdPartyMappingTemplateID int   
)
AS


				SELECT
					[ThirdPartyMappingTemplateID],
					[ThirdPartyFieldGroupID],
					[ThirdPartySystemID],
					[Name],
					[Description],
					[XMLItemTemplate],
					[XMLContainerName],
					[XMLItemName],
					[XMLItemKey]
				FROM
					[dbo].[ThirdPartyMappingTemplate] WITH (NOLOCK) 
				WHERE
										[ThirdPartyMappingTemplateID] = @ThirdPartyMappingTemplateID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyMappingTemplateID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyMappingTemplateID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_GetByThirdPartyMappingTemplateID] TO [sp_executeall]
GO
