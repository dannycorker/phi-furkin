SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PostCodeLookup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PostCodeLookup_Delete]
(

	@PostCodeLookupID int   
)
AS


				DELETE FROM [dbo].[PostCodeLookup] WITH (ROWLOCK) 
				WHERE
					[PostCodeLookupID] = @PostCodeLookupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PostCodeLookup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Delete] TO [sp_executeall]
GO
