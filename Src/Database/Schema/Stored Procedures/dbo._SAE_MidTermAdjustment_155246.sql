SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-05
-- Description:	Sql After Event - Change Customer Details
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_155246]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@TermsDate						DATE
			,@AdjustmentDate				DATE
			,@Count							INT
			,@ValueInt						INT
			,@DetailValueData				dbo.tvpDetailValueData
			,@EventComments					VARCHAR(2000)
			,@NextEventID					INT
			,@IdId							dbo.tvpIntInt
			,@WorkflowTaskID				INT

			DECLARE @NumberedValues TABLE ( ID INT Identity, Value VARCHAR(2000) )


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= m.CustomerID
			,@ClientID		= m.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0)
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

/*155246 PH Update PH Details
		Different routes:
			0) + effective date before renewal
				--> refer to senior user,check against business rules, requote, then continue as normal
			
			1) + effective date after renewal
			   + no change to premium
					--> Change Details + Issue doc through preferred method
				 
			2) + effective date after renewal
			   + change to premium
			   + customer happy to proceed
					--> Change Details + Issue doc through preferred method
			
			3) + effective date after renewal
			   + change to premium
			   + customer IS NOT happy to proceed
			   + doesn't want to continue without making change
					--> Cancel Policy + Issue cancel document
			
			4) + effective date after renewal
			   + change to premium
			   + customer IS NOT happy to proceed
			   + wants to proceed without making change
			   + change IS NOT on rating factors
					--> make note, end subprocess
			
			5) + effective date after renewal
			   + change to premium
			   + customer IS NOT happy to proceed
			   + wants to proceed without making change
			   + change IS on rating factors
					--> Issue letter to advise that details must be accurate otherwise policy may be invalid
		
		Events in this sub process:
			156666 MTA - Refer to senior user
			156669 Requote 
			156667 MTA - Check Against Business Rules 
			156670 Re-rate policy (MTA PH) 
			156671 Mid term adjustment PH
			156673 MTA - PH continue without making change? 
			156674 MTA - PH is change on rating factors?
				156676 MTA - Cancelled change not on rating factor
				156675 MTA LET - Advise accurate details
					156677 MTA LET - Advise accurate details(batch)
			156618 PH Apply Change Update PH Record 
	*/

IF @EventTypeID = 155246 /*begin - Change Customer Details*/
	BEGIN
	
		/*GPR 2019-12-02 SDLPC-25*/
		IF dbo.fnGetSimpleDvAsDate(175334,@MatterID) > dbo.fnGetSimpleDvAsDate(170037,@MatterID) /*Policy End Date*/
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Date entered is greater than Policy End Date. Please correct before continuing.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END
	
	
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated /*GPR 2018-06-27*/

		/*2018-04-22 - SA - Update to raise error when adjustment date less than inception date*/
		IF dbo.fnGetSimpleDvAsDate(175334,@MatterID) < dbo.fnGetSimpleDvAsDate(170035,@MatterID) 
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Adjustment date is less than inception date. Please correct before continuing</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END
		
		/*GPR 2018-10-31 for Migrated Policy*/
		SELECT @TermsDate = dbo._C600_GetFirstNonMigratedTermsDate(@MatterID)
		IF @AdjustmentDate < @TermsDate
		BEGIN	
				SELECT @ErrorMessage = '<font color="red"></br></br>The adjustment date is in a period of cover that is not known to Aquarium (Migrated Term). Please check the Adjustment Date.</br></font><font color="#edf4fa">'

				RAISERROR( @ErrorMessage, 16, 1 )
				RETURN -1	
		END
		
		/*GPR 2018-07-24 for C600 Defect 856*/
		SELECT @Count = dbo.fn_C00_MTA_CountOtherCasesAwaitingDecision(@CaseID)

		IF @Count >= 1 /*Unfinished MTA(s) for this Policy Holder*/
		BEGIN
		
			SELECT @ErrorMessage = '<font color="red"></br></br>This Customer has an open MTA thread on another policy. Please abort or complete the MTA before continuing with this one.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)    
			RETURN -1    
			
		END		

		IF dbo.fnGetSimpleDvAsInt(170038,@MatterID) IN (43003,43834)
		BEGIN
		
			SELECT @ErrorMessage = '<BR><BR><font color="red">Error: You cannot perform an MTA on a policy that is not live </font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			
		END
		
		/*Get the affinity value of the Camp Code we have entered*/ 
		SELECT @ValueINT = CASE rldv.ValueInt WHEN 76185 THEN 76183  -- L&G
											  WHEN 76186 THEN 74539 ELSE 0 END  -- Buddies
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHere cdv.DetailFieldID = 179912
		AND rldv.DetailFieldID = 179709
		AND cdv.CustomerID = @CustomerID 

		/*If we have entered a camp code, raise an error if it's for the wrong affinity*/ 
		IF ISNULL(@ValueINT,0) <> 0 AND  EXISTS ( SELECT * 
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHERE cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 170127
		AND rldv.ValueInt <> @ValueINT
		AND cdv.CustomerID = @CustomerID) 
		BEGIN 
			
			/*GPR 2018-06-05 C600 Defect 247 - Clear out the selected RLDV and Abort the MTA*/
			EXEC dbo._C00_SimpleValueIntoField 179912, 0, @CustomerID, @AqAutomation -- Clear out the field
			--EXEC dbo._C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, 157052, @WhoCreated, 1 -- Abort MTA, this will apply naturally as there will be a no change in premimum.
			EXEC dbo._C00_SetLeadEventComments @LeadEventID, 'The selected campaign code does not belong to the affinity for this Policy. Abort MTA.', 1
			
		END 
		
		/*GPR 2018-04-09 Error Message for L&G midterm campaign code C600 Defect 246*/
		IF ISNULL(@ValueINT,0) <> 0 AND  EXISTS ( SELECT * 
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHERE cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 170127
		AND rldv.ValueInt = 76183  -- L&G
		AND cdv.CustomerID = @CustomerID) 
		BEGIN 
			
			/*GPR 2018-06-05 C600 Defect 247 - Clear out the selected RLDV and Abort the MTA*/
			EXEC dbo._C00_SimpleValueIntoField 179912, 0, @CustomerID, @AqAutomation -- Clear out the field
			--EXEC dbo._C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, 157052, @WhoCreated, 1 -- Abort MTA, this will apply naturally as there will be a no change in premimum
			EXEC dbo._C00_SetLeadEventComments @LeadEventID, 'The affinity for this Policy does not support the addition of discount codes mid-term. Abort MTA.', 1
					
		END
	
		-- copy adjustment date to the field used by Pet and endorsement MTAs
		EXEC _C600_CopyFieldToField 175334, 175442, @MatterID, @WhoCreated /*Adjustment or Cancellation Date => /*Adjustment Date*/*/
		/*GPR 2019-03-21 55721*/ --EXEC dbo._C00_SimpleValueIntoField 175334, '', @MatterID, @WhoCreated	 /*Adjustment or Cancellation Date*/
		
		/*copy adjustment date to field to use for docs*/ /*NG 2020-12-09 for JIRA SDPRU-187*/
		EXEC _C600_CopyFieldToField 175334, 314353, @MatterID, @WhoCreated /*Adjustment or Cancellation Date => MTA Date*/

		/*
		CPS 2017-07-12
		Take the address lookup value and populate the address detail fields
		*/
		IF @ClientID <> 607
		BEGIN
			INSERT @NumberedValues ( Value )
			SELECT fn.AnyValue
			/*FROM dbo.fnTableOfValuesFromCSV(REPLACE(dbo.fnGetSimpleDv(175478,@CustomerID),'_',',')) fn -- Change Address - Address Picker
			GPR 2018-05-21 spoke with Cathal and agreed function change so that multi comma addresses do not push postcode onto line 6+  -- C600 Defect 448*/
			FROM dbo.fnTableOfValues(dbo.fnGetSimpleDv(175478,@CustomerID),'_') fn
		
			/*Construct a tvp of data to populate*/
			INSERT @DetailValueData (ClientID, DetailFieldSubType, ObjectID, DetailFieldID, DetailValueID, DetailValue )
			SELECT @ClientID, 10, @CustomerID, df.DetailFieldID, ISNULL(cdv.CustomerDetailValueID,-1), ISNULL(nv.Value,'')
			FROM @NumberedValues nv 
			INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = CASE nv.ID
																			WHEN 1 THEN 175314 /*Address 1*/
																			WHEN 2 THEN 175315 /*Address 2*/
																			WHEN 3 THEN 175316 /*Town*/
																			WHEN 4 THEN 175317 /*County*/
																			WHEN 5 THEN 175318 /*Postcode*/
																		   END 
			LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.DetailFieldID = df.DetailFieldID AND cdv.CustomerDetailValueID = @CustomerID
			WHERE df.Enabled = 1
		
			/*Perform a bulk save... hide the output of the sp in the @IdId variable*/
			INSERT @IdId ( ID1, ID2 )
			EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @WhoCreated

		END

		/*GPR 2021-02-26 for FURKIN-296*/
		/*
		DECLARE @SORID INT
		DECLARE @PostCodeValue VARCHAR(10)

		SELECT @SORID = dbo.fnGetSimpleDvAsInt(315889, @MatterID)

		SELECT @PostCodeValue = sor.ResponseXML.value('(root/CanadianAddressInfoV2/PostalCode)[1]', 'VARCHAR')
		FROM _C607_ServiceObjectsResponse sor WITH (NOLOCK)
		WHERE sor.ServiceObjectsResponseID = @SORID

		EXEC _C00_SimpleValueIntoField 175318, @PostCodeValue, @CustomerID
		*/

		/*put temporary pet & customer changes in a permanent place, ready for generating a quote when the event 156667 is applied */
		EXEC _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated /*TempLeadID*/
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated /*TempMatterID*/
		--EXEC dbo._C600_TempSwapPHandPetDetails @CustomerID
	
		DELETE @IdId
	
		/*CPS 2017-08-03*/
		/*Only trigger an MTA if we have changed any premium affecting fields*/
		INSERT @IdId ( ID1, ID2 )
		SELECT cdv.DetailFieldID, cdv.CustomerDetailValueID
		FROM TableDetailValues tdv WITH ( NOLOCK ) 
		INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = 177665 /*Premium Affecting Fields*/
		INNER JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.DetailFieldID = tdv.ValueInt
		INNER JOIN CustomerDetailValues cdv2 WITH ( NOLOCK ) on cdv2.CustomerID = cdv.CustomerID AND cdv2.DetailFieldID = 170144 /*Match on Affinity*/
		LEFT JOIN TableDetailValues tdvCoi WITH ( NOLOCK ) on tdvCoi.TableRowID = tr.TableRowID AND tdvCoi.DetailFieldID = 178667 /*COI Only*/
		INNER JOIN TableDetailValues tdvAffinity WITH ( NOLOCK ) on tdvAffinity.TableRowID = tr.TableRowID AND tdvAffinity.DetailFieldID = 180001 /*COI Only*/
		WHERE cdv.CustomerID = @CustomerID
		AND tdv.DetailFieldID = 177660 /*DetailFieldID*/
		AND ( tdvCoi.ValueInt = 0 or tdvCoi.TableDetailValueID is NULL )
		AND cdv.DetailValue <> ''
		and exists ( SELECT * 
					 FROM EventTypeHelperField hf WITH ( NOLOCK ) 
					 WHERE hf.EventTypeID = @EventTypeID 
					 AND hf.DetailFieldID = tdv.ValueInt )
		AND tdvAffinity.ValueInt = cdv2.ValueInt /*GPR 2018-04-13 Added Affinity to Table, updated 2018-04-16*/
					

		SELECT @EventComments += CONVERT(VARCHAR,@@ROWCOUNT) + ' key fields updated.' + CHAR(13)+CHAR(10)

		SELECT @EventComments += CONVERT(VARCHAR,df.DetailFieldID ) + ' (' + df.FieldCaption + ')' + CHAR(13)+CHAR(10)
		FROM @IdId id
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = id.ID1

			/*SA 2018-04-17 removed this section to ensure full MTA process is triggered regardless of key fields (to ensure changes can be rolled back)*/

		/*CPS 2017-07-06 don't bother with the MTA step if you have not changed key fields*/
		IF (Select dbo.fn_C00_1273_GetCurrentSchemeFromMatter(@MatterID)) = 10276 /*CW 2018-04-30 OMF MTA*/ 
		BEGIN
			IF EXISTS ( SELECT * FROM @IdId )
			BEGIN
				EXEC _C600_UpdatePHOrPetDetails @LeadEventID = @LeadEventID, @UpdateType = 1 /*Policy Holder*/
				SELECT @NextEventID = 158811 /* OMF Mid Term Adjustment PH */
				EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @NextEventID, @WhoCreated, 0
			END
			ELSE
			BEGIN
				SELECT @EventComments += 'No MTA Required.' + CHAR(13)+CHAR(10)
				EXEC _C600_UpdatePHOrPetDetails @LeadEventID = @LeadEventID, @UpdateType = 1 /*Policy Holder*/
				SELECT @NextEventID = 150156 /*No change in premium*/
				EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @NextEventID, @WhoCreated, 0
			END
		END
		ELSE IF NOT EXISTS ( SELECT * FROM @IdId )
		BEGIN
			SELECT @EventComments += 'No MTA Required.' + CHAR(13)+CHAR(10)
			EXEC _C600_UpdatePHOrPetDetails @LeadEventID = @LeadEventID, @UpdateType = 1 /*Policy Holder*/
			SELECT @NextEventID = 150156 /*No change in premium*/
			EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @NextEventID, @WhoCreated, 0
		END
		ELSE
		BEGIN
			EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, 156669 /*.Mid Term Adjustment Policy Holder*/, @WhoCreated, 0
			SELECT @EventComments += 'Mid Term Adjustment Policy Holder triggered' + CHAR(13)+CHAR(10)		
			SELECT @NextEventID = @EventTypeID
		END
		
		/*If there are other active pets for this customer, populate the adjustment date, queue the same event, and mark their MTA as in calculating*/
		/*JEL 2018-10-04  ensure that UPP uploads are not included in an MTA attempt*/ 
		IF EXISTS ( SELECT * 
					FROM Lead l WITH ( NOLOCK )
					INNER JOIN Matter m with (NOLOCK) on m.LeadID = l.LeadID 
					INNER JOIN MatterDetailValues mdv with (NOLOCK) on mdv.MatterID = m.MatterID
					WHERE l.CustomerID = @CustomerID
					AND l.LeadTypeID = 1492 /*Policy Admin*/
					AND l.LeadID <> @LeadID
					AND mdv.DetailFieldID = 170034
					AND mdv.ValueInt <> 0)
		   AND @WhoCreated <> @AqAutomation
		BEGIN
			INSERT @IdId ( ID1, ID2 )
			SELECT l.LeadID, m.MatterID
			FROM Lead l WITH ( NOLOCK ) 
			INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = c.CaseID 
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170038 /*Policy Status*/
			WHERE mdv.ValueInt = 43002 /*Live*/
			AND l.LeadID <> @LeadID
			AND l.LeadTypeID = 1492 /*Policy Admin*/
			AND m.CustomerID = @CustomerID
			
			/*Find values to save*/
			INSERT @DetailValueData ( ClientID, DetailFieldSubType, ObjectID, DetailFieldID, DetailValueID, DetailValue )
			SELECT @ClientID, 2, id.ID2, df.DetailFieldID, ISNULL(mdv.MatterDetailValueID,-1), ISNULL(NULLIF(dbo.fnGetSimpleDv(df.DetailFieldID,@MatterID),''), CONVERT(VARCHAR,@WhenCreated,121) )
			FROM @IdId id
			INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID IN ( 175334 /*Adjustment or Cancellation Date*/
																			 ,177904 /*Customer MTA Calculating*/
																			 ,314353 /*MTA Date (do not delete value at end of process)*/)
			LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = id.ID2 AND mdv.DetailFieldID = df.DetailFieldID
			WHERE (@NextEventID = @EventTypeID OR @NextEventID = 150156) -- edited NG 2020-12-10 to pass the values to multipet leads even if there's no change in premium for SDPRU-187
			
			/*Save fields*/
			INSERT @IdId (ID1,ID2)
			EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @WhoCreated
					
			/*Queue Events*/
			INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
			SELECT m.ClientID, m.CustomerID, m.LeadID, m.CaseID, @LeadEventID, @EventTypeID, @WhenCreated, @AqAutomation, @NextEventID, @AqAutomation, -1, 5, 0, 0
			FROM @IdId id
			INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = id.ID2
			WHERE @NextEventID <> 0

			SELECT @EventComments += ISNULL(CONVERT(VARCHAR,@@ROWCOUNT),'NULL') + ' MTAs queued for other live pets.' + CHAR(13)+CHAR(10)
		END
		
		SELECT @EventComments += ' Event "' + et.EventTypeName + '" queued.' + CHAR(13)+CHAR(10)
		FROM EventType et WITH ( NOLOCK ) 
		WHERE et.EventTypeID = @NextEventID

		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

		/*UAH 07/11/2018 - As there is only 1 MTA Workflow group, I am adding SAE on all events which relate to MTA's to remove workflow tasks once these MTA events have been applied*/

		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) where w.caseid = @caseID
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, 
		w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, @WhoCreated,dbo.fn_GetDate_Local(),'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE CaseID = @CaseID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 
		AND WorkflowGroupID = 1239 
		
		/*GPR 2019-03-21 #1525*/
		EXEC dbo._C00_SimpleValueIntoField 175334, '', @MatterID, @WhoCreated	 /*Adjustment or Cancellation Date*/
	END



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_155246] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_MidTermAdjustment_155246] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_155246] TO [sp_executeall]
GO
