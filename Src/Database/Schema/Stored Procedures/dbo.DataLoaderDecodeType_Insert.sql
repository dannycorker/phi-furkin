SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderDecodeType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDecodeType_Insert]
(

	@DataLoaderDecodeTypeID int   ,

	@DecodeTypeName varchar (250)  ,

	@DecodeTypeDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderDecodeType]
					(
					[DataLoaderDecodeTypeID]
					,[DecodeTypeName]
					,[DecodeTypeDescription]
					)
				VALUES
					(
					@DataLoaderDecodeTypeID
					,@DecodeTypeName
					,@DecodeTypeDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDecodeType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Insert] TO [sp_executeall]
GO
