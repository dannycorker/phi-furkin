SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Tracking table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Tracking_Delete]
(

	@TrackingID int   
)
AS


				DELETE FROM [dbo].[Tracking] WITH (ROWLOCK) 
				WHERE
					[TrackingID] = @TrackingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Tracking_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Delete] TO [sp_executeall]
GO
