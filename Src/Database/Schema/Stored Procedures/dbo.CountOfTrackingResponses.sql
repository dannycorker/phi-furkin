SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[CountOfTrackingResponses] @TrackingID int

AS


SELECT     TrackingID, COUNT(CustomerQuestionnaireID) AS countOfTracking
FROM         dbo.CustomerQuestionnaires WITH (NOLOCK)
GROUP BY TrackingID
HAVING      (TrackingID = @TrackingID)



GO
GRANT VIEW DEFINITION ON  [dbo].[CountOfTrackingResponses] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CountOfTrackingResponses] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CountOfTrackingResponses] TO [sp_executeall]
GO
