SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-11-30
-- Description:	Shortcut to the AIDashboard procs
-- =============================================
CREATE PROCEDURE [dbo].[dash] 
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT SERVERPROPERTY('ComputerNamePhysicalNetBIOS') as [Current Server], dbo.fn_GetDate_Local() as [Current DateTime]
	
	EXEC dbo.AIDashboard_DBBackupStatus
	EXEC dbo.AIDashboard_LogShipStatus 0, 0, 1
	EXEC dbo.AIDashboard_DBFreeSpace
	EXEC dbo.AIDashboard_DiskFreeSpace
	EXEC dbo.hsk 1, 1, 1  
END




GO
GRANT VIEW DEFINITION ON  [dbo].[dash] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dash] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dash] TO [sp_executeall]
GO
