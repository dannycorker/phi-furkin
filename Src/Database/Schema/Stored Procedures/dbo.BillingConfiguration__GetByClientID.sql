SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 27-07-2016
-- Description:	Gets a billing configuration record - there should only be one per client
-- =============================================
CREATE PROCEDURE [dbo].[BillingConfiguration__GetByClientID]
	
	@ClientID INT	
	
AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT TOP 1 * FROM BillingConfiguration WITH (NOLOCK) WHERE ClientID=@ClientID   

END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillingConfiguration__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__GetByClientID] TO [sp_executeall]
GO
