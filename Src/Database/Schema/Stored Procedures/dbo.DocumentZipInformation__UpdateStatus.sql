SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Updates the statusID of the DocumentZipInformation record
-- Date: 13/02/2009
-- JWG 2010-08-26 Update WhenCreated if this is a final status (passed or failed)
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentZipInformation__UpdateStatus]
(
	@DocumentZipInformationID int ,
	@StatusID int
)
AS
BEGIN

	DECLARE @WhenCreated datetime = NULL
	
	
	/* WhenCreated if this is a final status (passed or failed) */
	IF @StatusID IN (31, 32, 34)
	BEGIN
		SET @WhenCreated = dbo.fn_GetDate_Local()
	END 
	
	/* DEBUG: JWG 2011-05-19 */
	--EXEC dbo._C00_LogIt 'Debug', 'DocumentZipInformation__UpdateStatus', @DocumentZipInformationID, @StatusID, 93 
	
	/* Modify the status and date (if applicable) */
	UPDATE [dbo].[DocumentZipInformation]
	SET [StatusID] = @StatusID, 
	WhenCreated = @WhenCreated 
	WHERE [DocumentZipInformationID] = @DocumentZipInformationID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__UpdateStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__UpdateStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__UpdateStatus] TO [sp_executeall]
GO
