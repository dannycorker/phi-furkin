SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_GetByRPIFieldID]
(

	@RPIFieldID int   
)
AS


				SELECT
					[RPIFieldID],
					[RPIFieldName],
					[RPIFieldDescription],
					[IsEnabled],
					[QuestionTypeID],
					[DetailFieldSubtypeID],
					[LookupListID],
					[FieldOrder],
					[FieldSize],
					[WhenCreated],
					[WhenModified],
					[RPIFieldGroupID]
				FROM
					[dbo].[RPIField] WITH (NOLOCK) 
				WHERE
										[RPIFieldID] = @RPIFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_GetByRPIFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_GetByRPIFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_GetByRPIFieldID] TO [sp_executeall]
GO
