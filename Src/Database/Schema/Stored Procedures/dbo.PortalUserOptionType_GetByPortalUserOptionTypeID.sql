SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserOptionType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOptionType_GetByPortalUserOptionTypeID]
(

	@PortalUserOptionTypeID int   
)
AS


				SELECT
					[PortalUserOptionTypeID],
					[OptionTypeName]
				FROM
					[dbo].[PortalUserOptionType] WITH (NOLOCK) 
				WHERE
										[PortalUserOptionTypeID] = @PortalUserOptionTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_GetByPortalUserOptionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOptionType_GetByPortalUserOptionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_GetByPortalUserOptionTypeID] TO [sp_executeall]
GO
