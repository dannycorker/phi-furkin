SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset Type List
-- =============================================
CREATE PROCEDURE [dbo].[AssetType__GetList] 
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		at.[AssetTypeID],
		at.[AssetSubTypeID],
		at.[AssetTypeName],
		at.[Note],
		at.[Enabled],
		at.[Deleted],
		at.[WhoCreated],
		at.[WhenCreated],
		at.[WhoModified],
		at.[WhenModified]
	FROM 
		[dbo].[AssetType] at WITH (NOLOCK)
	ORDER BY at.[AssetTypeID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType__GetList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType__GetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType__GetList] TO [sp_executeall]
GO
