SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the WorkflowGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_Delete]
(

	@WorkflowGroupID int   
)
AS


				DELETE FROM [dbo].[WorkflowGroup] WITH (ROWLOCK) 
				WHERE
					[WorkflowGroupID] = @WorkflowGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Delete] TO [sp_executeall]
GO
