SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 23/03/2016
-- Description:	inserts or updates a partner
-- =============================================
CREATE PROCEDURE [dbo].[Partner__Save]
	@PartnerID int,
	@CustomerID int   ,
	@ClientID int   ,
	@UseCustomerAddress bit   ,
	@TitleID int   ,
	@FirstName varchar (100)  ,
	@MiddleName varchar (100)  ,
	@LastName varchar (100)  ,
	@EmailAddress varchar (255)  ,
	@DayTimeTelephoneNumber varchar (50)  ,
	@HomeTelephone varchar (50)  ,
	@MobileTelephone varchar (50)  ,
	@Address1 varchar (200)  ,
	@Address2 varchar (200)  ,
	@Town varchar (200)  ,
	@County varchar (200)  ,
	@PostCode varchar (50)  ,
	@Occupation varchar (100)  ,
	@Employer varchar (100)  ,
	@DateOfBirth datetime   ,
	@FullName varchar (201)   OUTPUT,
	@CountryID int   
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @RC int
	
	IF @PartnerID>0 -- perform update
	BEGIN

		EXECUTE @RC = [dbo].[Partner_Update] 
		   @PartnerID
		  ,@CustomerID
		  ,@ClientID
		  ,@UseCustomerAddress
		  ,@TitleID
		  ,@FirstName
		  ,@MiddleName
		  ,@LastName
		  ,@EmailAddress
		  ,@DayTimeTelephoneNumber
		  ,@HomeTelephone
		  ,@MobileTelephone
		  ,@Address1
		  ,@Address2
		  ,@Town
		  ,@County
		  ,@PostCode
		  ,@Occupation
		  ,@Employer
		  ,@DateOfBirth
		  ,@FullName OUTPUT
		  ,@CountryID

	END
	ELSE
	BEGIN

		EXECUTE @RC = [dbo].[Partner_Insert] 
		   @PartnerID OUTPUT
		  ,@CustomerID
		  ,@ClientID
		  ,@UseCustomerAddress
		  ,@TitleID
		  ,@FirstName
		  ,@MiddleName
		  ,@LastName
		  ,@EmailAddress
		  ,@DayTimeTelephoneNumber
		  ,@HomeTelephone
		  ,@MobileTelephone
		  ,@Address1
		  ,@Address2
		  ,@Town
		  ,@County
		  ,@PostCode
		  ,@Occupation
		  ,@Employer
		  ,@DateOfBirth
		  ,@FullName OUTPUT
		  ,@CountryID

	END
	
	SELECT @PartnerID PartnerID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner__Save] TO [sp_executeall]
GO
