SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05/04/2018
-- Description:	Gets The Relay Url
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetRelayUrl]
	@CardTransactionID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	-- TODO look up the relay url from the client

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID FROM CardTransaction WITH (NOLOCK) 
	WHERE CardTransactionID=@CardTransactionID
    
	SELECT 'http://localhost:8281772/WorldPayTest' RelayUrl
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetRelayUrl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetRelayUrl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetRelayUrl] TO [sp_executeall]
GO
