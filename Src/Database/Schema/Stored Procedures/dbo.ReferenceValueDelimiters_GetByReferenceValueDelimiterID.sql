SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReferenceValueDelimiters table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDelimiters_GetByReferenceValueDelimiterID]
(

	@ReferenceValueDelimiterID int   
)
AS


				SELECT
					[ReferenceValueDelimiterID],
					[Delimiter]
				FROM
					[dbo].[ReferenceValueDelimiters] WITH (NOLOCK) 
				WHERE
										[ReferenceValueDelimiterID] = @ReferenceValueDelimiterID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_GetByReferenceValueDelimiterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDelimiters_GetByReferenceValueDelimiterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_GetByReferenceValueDelimiterID] TO [sp_executeall]
GO
