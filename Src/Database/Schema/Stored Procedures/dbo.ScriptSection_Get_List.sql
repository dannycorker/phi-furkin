SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSection_Get_List]

AS


				
				SELECT
					[ScriptSectionID],
					[ScriptID],
					[ClientID],
					[SectionTitle],
					[SectionOrder],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified],
					[SourceID],
					[NextEventTypeID]
				FROM
					[dbo].[ScriptSection] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Get_List] TO [sp_executeall]
GO
