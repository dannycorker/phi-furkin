SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-11-02
-- Description:	Selects the payment info to show on the claim
-- ROH 2014-12-10 Temp change for testing to return payment rows before they've been sent
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetSapPayments] 
(
	@MatterID INT,
	@OnlyPayments BIT = 0
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT	pvt.*
	FROM (
		SELECT r.TableRowID,	CASE 
									WHEN df.QuestionTypeID = 4 THEN ll.ItemValue
									WHEN tdv.ValueDate IS NOT NULL THEN CONVERT(VARCHAR, tdv.ValueDate, 103)
									ELSE LTRIM(RTRIM(REPLACE(tdv.DetailValue, ',', ' ')))
								END AS DetailValue, Replace(df.FieldName, ' ', '') AS FieldName
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON tdv.ValueInt = ll.LookupListItemID
		WHERE r.DetailFieldID = 154485
		AND r.MatterID = @MatterID) src
	PIVOT (MAX(DetailValue) FOR FieldName IN (	PayeeOrganisation,
												PayTo,
												DateCreated,
												PaymentApproved,
												DateSentToSAP,
												DateResponseFromSAP,
												Result,
												DocumentCode,
												PaymentStopped,
												PaymentAmount,
												PaymentType)) AS pvt
	--WHERE (@OnlyPayments = 0 OR (PaymentType = '0' AND DateSentToSAP > '' AND DateResponseFromSAP > ''))
	WHERE (@OnlyPayments = 0 OR (PaymentType = '0' AND PaymentApproved > ''))
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetSapPayments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetSapPayments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetSapPayments] TO [sp_executeall]
GO
