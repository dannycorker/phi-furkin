SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailFieldPages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_Delete]
(

	@DetailFieldPageID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[DetailFieldPages] WITH (ROWLOCK) 
				WHERE
					[DetailFieldPageID] = @DetailFieldPageID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Delete] TO [sp_executeall]
GO
