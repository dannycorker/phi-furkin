SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteClientOffice    Script Date: 08/09/2006 12:22:46 ******/

CREATE PROCEDURE [dbo].[DeleteClientOffice] @ClientOfficeID int
AS
Delete From ClientOffices
Where ClientOfficeID = @ClientOfficeID





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientOffice] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteClientOffice] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientOffice] TO [sp_executeall]
GO
