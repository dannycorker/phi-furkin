SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaires_Delete]
(

	@ClientQuestionnaireID int   
)
AS


				DELETE FROM [dbo].[ClientQuestionnaires] WITH (ROWLOCK) 
				WHERE
					[ClientQuestionnaireID] = @ClientQuestionnaireID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Delete] TO [sp_executeall]
GO
