SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[EventTypeID],
					[ClientID],
					[EventTypeName],
					[EventTypeDescription],
					[Enabled],
					[UnitsOfEffort],
					[FollowupTimeUnitsID],
					[FollowupQuantity],
					[AvailableManually],
					[StatusAfterEvent],
					[AquariumEventAfterEvent],
					[EventSubtypeID],
					[DocumentTypeID],
					[LeadTypeID],
					[AllowCustomTimeUnits],
					[InProcess],
					[KeyEvent],
					[UseEventCosts],
					[UseEventUOEs],
					[UseEventDisbursements],
					[UseEventComments],
					[SignatureRequired],
					[SignatureOverride],
					[VisioX],
					[VisioY],
					[AquariumEventSubtypeID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[FollowupWorkingDaysOnly],
					[CalculateTableRows],
					[SourceID],
					[SmsGatewayID],
					[IsShared],
					[SocialFeedID]
				FROM
					dbo.fnEventTypeShared(NULL)
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetBySourceID] TO [sp_executeall]
GO
