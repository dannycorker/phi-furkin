SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseDetailValues_GetByDetailFieldIDValueIntClientID]
(

	@DetailFieldID int   ,

	@ValueInt int   ,

	@ClientID int   
)
AS


				SELECT
					[CaseDetailValueID],
					[ClientID],
					[CaseID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[CaseDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ValueInt] = @ValueInt
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_GetByDetailFieldIDValueIntClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseDetailValues_GetByDetailFieldIDValueIntClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_GetByDetailFieldIDValueIntClientID] TO [sp_executeall]
GO
