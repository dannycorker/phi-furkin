SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Department table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department_Update]
(

	@DepartmentID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@DepartmentName varchar (100)  ,

	@DepartmentDescription varchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Department]
				SET
					[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[DepartmentName] = @DepartmentName
					,[DepartmentDescription] = @DepartmentDescription
				WHERE
[DepartmentID] = @DepartmentID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Update] TO [sp_executeall]
GO
