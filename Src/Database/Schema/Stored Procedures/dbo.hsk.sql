SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-26
-- Description:	Check the log for housekeeping activities
-- =============================================
CREATE PROCEDURE [dbo].[hsk] 
	@TodayOnly bit = 1, 
	@StartFinishOnly bit = 0, 
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FinishTime datetime
	
	IF @SummaryOnly = 1
	BEGIN
		SELECT @FinishTime = l.LogDateTime
		FROM dbo.Logs l WITH (NOLOCK) 
		WHERE l.ClassName = 'AquariumIndexHousekeeping' 
		AND l.LogEntry = 'End of index housekeeping' 
		AND l.TypeOfLogEntry = 'Info' 
		AND l.LogDateTime > dbo.fn_GetDate_Local() - 1 
	
		SELECT CASE WHEN @FinishTime IS NULL THEN 'Red' WHEN DATEPART(HOUR, @FinishTime) > 0 THEN 'Amber' ELSE 'Green' END as [Status], @FinishTime as [Housekeeping Finish Time]
	END
	ELSE
	BEGIN
		SELECT * 
		FROM dbo.Logs l WITH (NOLOCK) 
		WHERE l.ClassName LIKE 'Aquarium%Housekeeping' 
		AND (@TodayOnly = 0 OR l.LogDateTime > dbo.fn_GetDate_Local() - 1) 
		AND (@StartFinishOnly = 0 OR l.TypeOfLogEntry = 'Info') 
		ORDER BY 1 DESC
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[hsk] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[hsk] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[hsk] TO [sp_executeall]
GO
