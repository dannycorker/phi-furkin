SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Chris Townsend
-- Create date: 15/07/2008
-- Description:	Gets a user's reminder for a specific appointment
-- =============================================

CREATE PROCEDURE [dbo].[DiaryReminder__GetReminderByAppointmentIDAndClientPersonnelID] 

	@DiaryAppointmentID int,
	@ClientPersonnelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT		DiaryReminder.DiaryReminderID,
				DiaryReminder.ClientID,
				DiaryReminder.DiaryAppointmentID,
				DiaryReminder.ClientPersonnelID,
				DiaryReminder.ReminderTimeshiftID,
				DiaryReminder.ReminderDate
	FROM		DiaryReminder
	WHERE		DiaryReminder.ClientPersonnelID = @ClientPersonnelID
	AND			DiaryReminder.DiaryAppointmentID = @DiaryAppointmentID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder__GetReminderByAppointmentIDAndClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder__GetReminderByAppointmentIDAndClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder__GetReminderByAppointmentIDAndClientPersonnelID] TO [sp_executeall]
GO
