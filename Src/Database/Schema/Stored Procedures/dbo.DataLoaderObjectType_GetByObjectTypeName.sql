SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderObjectType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectType_GetByObjectTypeName]
(

	@ObjectTypeName varchar (250)  
)
AS


				SELECT
					[DataLoaderObjectTypeID],
					[ObjectTypeName],
					[ObjectTypeDescription],
					[ProcessingOrder]
				FROM
					[dbo].[DataLoaderObjectType] WITH (NOLOCK) 
				WHERE
										[ObjectTypeName] = @ObjectTypeName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_GetByObjectTypeName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectType_GetByObjectTypeName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_GetByObjectTypeName] TO [sp_executeall]
GO
