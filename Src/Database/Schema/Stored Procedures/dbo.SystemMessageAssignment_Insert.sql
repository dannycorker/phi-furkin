SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SystemMessageAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_Insert]
(

	@SystemMessageAssignmentID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@SystemMessageID int   
)
AS


				
				INSERT INTO [dbo].[SystemMessageAssignment]
					(
					[ClientID]
					,[ClientPersonnelAdminGroupID]
					,[ClientPersonnelID]
					,[SystemMessageID]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelAdminGroupID
					,@ClientPersonnelID
					,@SystemMessageID
					)
				-- Get the identity value
				SET @SystemMessageAssignmentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Insert] TO [sp_executeall]
GO
