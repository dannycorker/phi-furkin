SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowTaskCompleted table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTaskCompleted_GetByCompletedBy]
(

	@CompletedBy int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[WorkflowTaskCompletedID],
					[ClientID],
					[WorkflowTaskID],
					[WorkflowGroupID],
					[AutomatedTaskID],
					[Priority],
					[AssignedTo],
					[AssignedDate],
					[LeadID],
					[CaseID],
					[EventTypeID],
					[FollowUp],
					[Important],
					[CreationDate],
					[Escalated],
					[EscalatedBy],
					[EscalationReason],
					[EscalationDate],
					[Disabled],
					[CompletedBy],
					[CompletedOn],
					[CompletionDescription]
				FROM
					[dbo].[WorkflowTaskCompleted] WITH (NOLOCK) 
				WHERE
					[CompletedBy] = @CompletedBy
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_GetByCompletedBy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTaskCompleted_GetByCompletedBy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_GetByCompletedBy] TO [sp_executeall]
GO
