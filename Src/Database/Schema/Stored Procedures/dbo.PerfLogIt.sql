SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-14
-- Description:	Performance logging
-- =============================================
CREATE PROCEDURE [dbo].[PerfLogIt]
(
	@Origin VARCHAR(128),
	@ClientPersonnelID INT = NULL,
	@ParamList VARCHAR(MAX) = NULL,
	@PerfLogID INT = NULL
)
AS
BEGIN

	IF @PerfLogID IS NOT NULL
	BEGIN

		UPDATE Perflog
		SET Finish = CURRENT_TIMESTAMP, RunTimeSeconds = DATEDIFF(Second,Start,CURRENT_TIMESTAMP)
		WHERE PerfLogID = @PerfLogID
		
	END
	ELSE
	BEGIN

		INSERT INTO Perflog (SPID,Origin,Start,ParamList,ClientPersonnelID)
		SELECT @@SPID,@Origin,CURRENT_TIMESTAMP,@ParamList,@ClientPersonnelID
		
		RETURN SCOPE_IDENTITY()
		
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PerfLogIt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PerfLogIt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PerfLogIt] TO [sp_executeall]
GO
