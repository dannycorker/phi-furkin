SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2015
-- Description:	Gets a Social Feed by the screen name and user identity
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__GetBySocialUserIDAndScreenName]

	@ScreenName VARCHAR(250),
	@SocialUserID VARCHAR(50),
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM SocialFeed WITH (NOLOCK) 
	WHERE ScreenName=@ScreenName AND SocialUserID=@SocialUserID AND ClientID=@ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetBySocialUserIDAndScreenName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__GetBySocialUserIDAndScreenName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetBySocialUserIDAndScreenName] TO [sp_executeall]
GO
