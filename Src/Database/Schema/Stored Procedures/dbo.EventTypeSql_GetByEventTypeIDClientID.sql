SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeSql table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_GetByEventTypeIDClientID]
(

	@EventTypeID int   ,

	@ClientID int   
)
AS


				SELECT
					[EventTypeSqlID],
					[ClientID],
					[EventTypeID],
					[PostUpdateSql],
					[IsNativeSql]
				FROM
					[dbo].[EventTypeSql] WITH (NOLOCK) 
				WHERE
										[EventTypeID] = @EventTypeID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_GetByEventTypeIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_GetByEventTypeIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_GetByEventTypeIDClientID] TO [sp_executeall]
GO
