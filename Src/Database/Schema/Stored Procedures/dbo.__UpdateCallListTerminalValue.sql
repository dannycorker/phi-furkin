SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-12-10
-- Description:	Updates the terminal status of the call list record
-- =============================================
CREATE PROCEDURE [dbo].[__UpdateCallListTerminalValue]

	@CaseID INT,
	@RowID INT,
	@Terminal VARCHAR(2000)

AS
BEGIN

	DECLARE @TableRowID INT,
	@DetailFieldPageID INT,
	@DetailFieldID INT,
	@354FieldID INT, -- RowID
	@357FieldID INT, -- CaseID
	@358FieldID INT, -- Terminal
	@LeadTypeID INT,
	@LeadID INT,
	@ClientID INT,
	@CustomerID INT

	SELECT @LeadID=LeadID, @ClientID=ClientID FROM Cases WITH (NOLOCK) WHERE CaseID = @CaseID;
	SELECT @CustomerID=CustomerID, @LeadTypeID = LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID						

	SELECT @354FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 354 -- RowID
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @357FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 357 -- CaseID
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @358FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 358 -- Terminal
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID
	
	IF @RowID>0
	BEGIN
		SELECT @TableRowID = tdvCaseID.TableRowID FROM TableDetailValues tdvCaseID WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvRowID WITH (NOLOCK) ON tdvRowID.DetailFieldID=@354FieldID AND tdvRowID.ValueInt = @RowID
		WHERE tdvCaseID.DetailFieldID = @357FieldID AND tdvCaseID.ValueInt = @CaseID
	END
	ELSE
	BEGIN
		SELECT @TableRowID = tdvCaseID.TableRowID FROM TableDetailValues tdvCaseID WITH (NOLOCK) 		
		WHERE tdvCaseID.DetailFieldID = @357FieldID AND tdvCaseID.ValueInt = @CaseID	
	END	

	IF @TableRowID IS NOT NULL AND @TableRowID>0
	BEGIN
		UPDATE TableDetailValues 
		SET DetailValue=@Terminal
		WHERE TableRowID = @TableRowID AND DetailFieldID = @358FieldID
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[__UpdateCallListTerminalValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__UpdateCallListTerminalValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__UpdateCallListTerminalValue] TO [sp_executeall]
GO
