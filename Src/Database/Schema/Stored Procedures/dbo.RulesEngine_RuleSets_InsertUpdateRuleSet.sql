SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-29
-- Description:	Inserts or updates a new rule set
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSets_InsertUpdateRuleSet]
(
	@RuleSetID INT,
	@Name VARCHAR(200),
	@InputTypeID INT,
	@OutputTypeID INT,
	@ClientID INT,
	@UserID INT,
	@Description VARCHAR(MAX),
	@ExposeForUse BIT = true,
	@RuleSetTypeID INT = NULL,
	@ChangeSetID INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF @RuleSetID > 0
	BEGIN
	
		UPDATE dbo.RulesEngine_RuleSets
		SET Name = @Name,
			InputTypeID = @InputTypeID,
			OutputTypeID = @OutputTypeID,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID,
			Description = @Description,
			ExposeForUse = @ExposeForUse,
			RuleSetTypeID = @RuleSetTypeID
		WHERE RuleSetID = @RuleSetID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.RulesEngine_RuleSets (Name, InputTypeID, OutputTypeID, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, Description, ExposeForUse, RuleSetTypeID, ChangeSetID)
		VALUES (@Name, @InputTypeID, @OutputTypeID, @ClientID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @Description, @ExposeForUse, @RuleSetTypeID, @ChangeSetID)
		
		SELECT @RuleSetID = SCOPE_IDENTITY()
	
	END

	EXEC dbo.RulesEngine_RuleSets_GetRuleSetsByClientID @ClientID, @RuleSetID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_InsertUpdateRuleSet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSets_InsertUpdateRuleSet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_InsertUpdateRuleSet] TO [sp_executeall]
GO
