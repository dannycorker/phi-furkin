SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alistair Jones
-- Create date: 27th November 2007
-- Description:	Used to return Lead Type and Lead Id
-- =============================================
CREATE PROCEDURE  [dbo].[Customers__GetAllLeadTypesAndLeadIDByCustomerID]
(
	@CustomerID int, 
	@UserID int
)
AS
BEGIN
	SELECT LeadType.LeadTypeName + ' - ' + convert(varchar, Lead.LeadID) as Descr, 
	Lead.LeadID
	FROM dbo.Customers WITH (NOLOCK) 
	inner join dbo.lead WITH (NOLOCK) on lead.customerid = customers.customerid 
	inner join dbo.leadtype WITH (NOLOCK) on lead.leadtypeid = leadtype.leadtypeid
	inner join dbo.fnLeadTypeSecure(@UserID) f on lead.LeadTypeID = f.LeadTypeID 
	and f.rightid > 0
	WHERE Customers.CustomerID = @CustomerID


END



GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetAllLeadTypesAndLeadIDByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetAllLeadTypesAndLeadIDByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetAllLeadTypesAndLeadIDByCustomerID] TO [sp_executeall]
GO
