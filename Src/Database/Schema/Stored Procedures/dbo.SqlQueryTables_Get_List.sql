SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_Get_List]

AS


				
				SELECT
					[SqlQueryTableID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryTableName],
					[TableAlias],
					[TableDisplayOrder],
					[JoinType],
					[JoinText],
					[JoinTableID],
					[JoinRTRID],
					[TempTableID],
					[TempJoinTableID],
					[SourceID]
				FROM
					[dbo].[SqlQueryTables] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Get_List] TO [sp_executeall]
GO
