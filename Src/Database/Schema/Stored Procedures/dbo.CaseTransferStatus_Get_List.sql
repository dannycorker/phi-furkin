SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CaseTransferStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferStatus_Get_List]

AS


				
				SELECT
					[CaseTransferStatusID],
					[Status]
				FROM
					[dbo].[CaseTransferStatus] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferStatus_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Get_List] TO [sp_executeall]
GO
