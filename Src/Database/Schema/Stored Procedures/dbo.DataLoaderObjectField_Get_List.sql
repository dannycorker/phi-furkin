SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderObjectField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_Get_List]

AS


				
				SELECT
					[DataLoaderObjectFieldID],
					[DataLoaderObjectTypeID],
					[FieldName],
					[DataTypeID],
					[IsMandatory],
					[IsNormalisedField],
					[IsABitField],
					[IsClientOwned],
					[IsUniqueTableID],
					[IsWriteOnce],
					[HelperText]
				FROM
					[dbo].[DataLoaderObjectField] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Get_List] TO [sp_executeall]
GO
