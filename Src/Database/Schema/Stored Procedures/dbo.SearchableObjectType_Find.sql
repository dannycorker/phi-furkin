SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SearchableObjectType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchableObjectType_Find]
(

	@SearchUsingOR bit   = null ,

	@SearchableObjectTypeID int   = null ,

	@SearchableObject varchar (250)  = null ,

	@PKColumnName varchar (250)  = null ,

	@JoinTable varchar (250)  = null ,

	@JoinColumn varchar (250)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SearchableObjectTypeID]
	, [SearchableObject]
	, [PKColumnName]
	, [JoinTable]
	, [JoinColumn]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[SearchableObjectType] WITH (NOLOCK) 
    WHERE 
	 ([SearchableObjectTypeID] = @SearchableObjectTypeID OR @SearchableObjectTypeID IS NULL)
	AND ([SearchableObject] = @SearchableObject OR @SearchableObject IS NULL)
	AND ([PKColumnName] = @PKColumnName OR @PKColumnName IS NULL)
	AND ([JoinTable] = @JoinTable OR @JoinTable IS NULL)
	AND ([JoinColumn] = @JoinColumn OR @JoinColumn IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SearchableObjectTypeID]
	, [SearchableObject]
	, [PKColumnName]
	, [JoinTable]
	, [JoinColumn]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[SearchableObjectType] WITH (NOLOCK) 
    WHERE 
	 ([SearchableObjectTypeID] = @SearchableObjectTypeID AND @SearchableObjectTypeID is not null)
	OR ([SearchableObject] = @SearchableObject AND @SearchableObject is not null)
	OR ([PKColumnName] = @PKColumnName AND @PKColumnName is not null)
	OR ([JoinTable] = @JoinTable AND @JoinTable is not null)
	OR ([JoinColumn] = @JoinColumn AND @JoinColumn is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchableObjectType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Find] TO [sp_executeall]
GO
