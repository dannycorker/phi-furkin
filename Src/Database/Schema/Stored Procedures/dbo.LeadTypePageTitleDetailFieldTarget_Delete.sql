SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleDetailFieldTarget_Delete]
(

	@LeadTypePageTitleDetailFieldTargetID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[LeadTypePageTitleDetailFieldTargetID] = @LeadTypePageTitleDetailFieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Delete] TO [sp_executeall]
GO
