SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ChartSeries table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_Get_List]

AS


				
				SELECT
					[ChartSeriesID],
					[ChartSeriesTypeID],
					[ChartSeriesOrder],
					[ChartID],
					[ClientID],
					[PanelItemChartingID],
					[ColumnName]
				FROM
					[dbo].[ChartSeries] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Get_List] TO [sp_executeall]
GO
