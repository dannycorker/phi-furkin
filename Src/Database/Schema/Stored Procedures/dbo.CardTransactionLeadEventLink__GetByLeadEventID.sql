SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 28/07/2017
-- Description:	Gets a card transaction - leade event link record
-- =============================================
CREATE PROCEDURE [dbo].[CardTransactionLeadEventLink__GetByLeadEventID]
	@LeadEventID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM CardTransactionLeadEventLink WITH (NOLOCK) 
	WHERE LeadEventID = @LeadEventID    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransactionLeadEventLink__GetByLeadEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransactionLeadEventLink__GetByLeadEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransactionLeadEventLink__GetByLeadEventID] TO [sp_executeall]
GO
