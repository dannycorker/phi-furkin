SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-12-12
-- Description:	Creates and execs dynamic sql to modify the passed in XML.
-- =============================================
CREATE PROCEDURE [dbo].[XML_Modify] 
	@Xml XML OUTPUT,
	@XPath VARCHAR(1000),
	@XmlParam XML = NULL,
	@Namespace VARCHAR(1000) = NULL,
	@NamespacePrefix VARCHAR(50)= NULL
AS
BEGIN
	
	DECLARE @EncodedXPath VARCHAR(2000)
	SELECT @EncodedXPath = REPLACE(@XPath, '''', '''''')
	
	DECLARE @ReturnVal XML
	
	DECLARE @Sql NVARCHAR(MAX)
	SELECT @Sql = ''
	
	IF @Namespace IS NOT NULL AND @NamespacePrefix IS NOT NULL
	BEGIN
		SELECT @Sql += 'WITH XMLNAMESPACES (''' + @Namespace + ''' as ' + @NamespacePrefix + ') '
	END
	SELECT @Sql += N'SET @Xml.modify(''' + @EncodedXPath + N''')'  
	
	EXEC sp_executesql @Sql, N'@Xml XML OUTPUT, @XmlParam XML', @Xml OUTPUT, @XmlParam

END



GO
GRANT VIEW DEFINITION ON  [dbo].[XML_Modify] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[XML_Modify] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[XML_Modify] TO [sp_executeall]
GO
