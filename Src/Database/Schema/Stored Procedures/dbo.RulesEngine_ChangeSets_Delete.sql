SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-11
-- Description:	deletes change set detail
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ChangeSets_Delete]
(
	@ChangeSetID INT, 
	@ClientID INT,
	@UserID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @DeleteRuleSets TABLE (DeleteRowID INT IDENTITY(1,1), RuleSetID INT)
	DECLARE @RuleSetID INT = 0,
			@DeleteRowID INT = 0,
			@TotalDeleteRowID INT = 0
	
	INSERT INTO @DeleteRuleSets (RuleSetID)
	SELECT	RuleSetID 
	FROM	RulesEngine_RuleSets WITH (NOLOCK) 
	WHERE	ChangeSetID = @ChangeSetID
	AND		ClientID = @ClientID
	AND		ParentRuleSetID IS NOT NULL
	
	SELECT @TotalDeleteRowID = SCOPE_IDENTITY()

	WHILE @DeleteRowID < @TotalDeleteRowID
	BEGIN
	
		SELECT @DeleteRowID += 1
	
		SELECT @RuleSetID = RuleSetID FROM @DeleteRuleSets WHERE DeleteRowID = @DeleteRowID
		
		EXEC [dbo].[RulesEngine_RuleSets_Delete] @RuleSetID, 1

		
	END
	
	DELETE	cs
	FROM	RulesEngine_ChangeSets cs
	WHERE	cs.ChangeSetID = @ChangeSetID
	AND		NOT EXISTS (SELECT RuleSetID FROM RulesEngine_RuleSets rs WITH (NOLOCK) WHERE rs.ClientID = @ClientID AND rs.ChangeSetID = @ChangeSetID)
	AND		cs.ClientID = @ClientID
	
	DELETE
	FROM	RulesEngine_TestFile
	WHERE	ChangeSetID NOT IN (SELECT ChangeSetID FROM RulesEngine_ChangeSets WITH (NOLOCK) WHERE ClientID = @ClientID)
	AND		ClientID = @ClientID

	DELETE
	FROM	RulesEngine_TestFileItem
	WHERE	TestFileID NOT IN (SELECT TestFileID FROM RulesEngine_TestFile WITH (NOLOCK) WHERE ClientID = @ClientID)
	AND		ClientID = @ClientID

	DELETE
	FROM	RulesEngine_TestFileItemOverride
	WHERE	TestFileItemID NOT IN (SELECT TestFileItemID FROM RulesEngine_TestFileItem WITH (NOLOCK) WHERE ClientID = @ClientID)
	AND		ClientID = @ClientID
	
	DELETE
	FROM	RulesEngine_TestFileItemResult
	WHERE	TestFileItemID NOT IN (SELECT TestFileItemID FROM RulesEngine_TestFileItem WITH (NOLOCK) WHERE ClientID = @ClientID)
	AND		ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ChangeSets_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_Delete] TO [sp_executeall]
GO
