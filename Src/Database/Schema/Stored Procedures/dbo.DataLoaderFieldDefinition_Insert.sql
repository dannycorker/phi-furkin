SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderFieldDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_Insert]
(

	@DataLoaderFieldDefinitionID int    OUTPUT,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderObjectTypeID int   ,

	@DataLoaderMapSectionID int   ,

	@DataLoaderObjectFieldID int   ,

	@DetailFieldID int   ,

	@DetailFieldAlias varchar (500)  ,

	@NamedValue varchar (250)  ,

	@Keyword varchar (250)  ,

	@DataLoaderKeywordMatchTypeID int   ,

	@RowRelativeToKeyword int   ,

	@ColRelativeToKeyword int   ,

	@SectionRelativeRow int   ,

	@SectionAbsoluteCol int   ,

	@ValidationRegex varchar (250)  ,

	@Equation varchar (2000)  ,

	@IsMatchField bit   ,

	@DecodeTypeID int   ,

	@DefaultLookupItemID int   ,

	@SourceDataLoaderFieldDefinitionID int   ,

	@Notes varchar (2000)  ,

	@AllowErrors bit   
)
AS


				
				INSERT INTO [dbo].[DataLoaderFieldDefinition]
					(
					[ClientID]
					,[DataLoaderMapID]
					,[DataLoaderObjectTypeID]
					,[DataLoaderMapSectionID]
					,[DataLoaderObjectFieldID]
					,[DetailFieldID]
					,[DetailFieldAlias]
					,[NamedValue]
					,[Keyword]
					,[DataLoaderKeywordMatchTypeID]
					,[RowRelativeToKeyword]
					,[ColRelativeToKeyword]
					,[SectionRelativeRow]
					,[SectionAbsoluteCol]
					,[ValidationRegex]
					,[Equation]
					,[IsMatchField]
					,[DecodeTypeID]
					,[DefaultLookupItemID]
					,[SourceDataLoaderFieldDefinitionID]
					,[Notes]
					,[AllowErrors]
					)
				VALUES
					(
					@ClientID
					,@DataLoaderMapID
					,@DataLoaderObjectTypeID
					,@DataLoaderMapSectionID
					,@DataLoaderObjectFieldID
					,@DetailFieldID
					,@DetailFieldAlias
					,@NamedValue
					,@Keyword
					,@DataLoaderKeywordMatchTypeID
					,@RowRelativeToKeyword
					,@ColRelativeToKeyword
					,@SectionRelativeRow
					,@SectionAbsoluteCol
					,@ValidationRegex
					,@Equation
					,@IsMatchField
					,@DecodeTypeID
					,@DefaultLookupItemID
					,@SourceDataLoaderFieldDefinitionID
					,@Notes
					,@AllowErrors
					)
				-- Get the identity value
				SET @DataLoaderFieldDefinitionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Insert] TO [sp_executeall]
GO
