SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SMSResponseMap table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSResponseMap_Find]
(

	@SearchUsingOR bit   = null ,

	@SMSResponseMapID int   = null ,

	@ClientID int   = null ,

	@SMSQuestionnaireID int   = null ,

	@FromSMSQuestionGroupID int   = null ,

	@FromSMSQuestionID int   = null ,

	@FromAnswer varchar (256)  = null ,

	@ToSMSQuestionGroupID int   = null ,

	@ToSMSQuestionID int   = null ,

	@EndOfQuestionnaire bit   = null ,

	@Score int   = null ,

	@ConfidenceRangeStart int   = null ,

	@ConfidenceRangeEnd int   = null ,

	@RuleID int   = null ,

	@ProcessingOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SMSResponseMapID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [FromSMSQuestionGroupID]
	, [FromSMSQuestionID]
	, [FromAnswer]
	, [ToSMSQuestionGroupID]
	, [ToSMSQuestionID]
	, [EndOfQuestionnaire]
	, [Score]
	, [ConfidenceRangeStart]
	, [ConfidenceRangeEnd]
	, [RuleID]
	, [ProcessingOrder]
    FROM
	[dbo].[SMSResponseMap] WITH (NOLOCK) 
    WHERE 
	 ([SMSResponseMapID] = @SMSResponseMapID OR @SMSResponseMapID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SMSQuestionnaireID] = @SMSQuestionnaireID OR @SMSQuestionnaireID IS NULL)
	AND ([FromSMSQuestionGroupID] = @FromSMSQuestionGroupID OR @FromSMSQuestionGroupID IS NULL)
	AND ([FromSMSQuestionID] = @FromSMSQuestionID OR @FromSMSQuestionID IS NULL)
	AND ([FromAnswer] = @FromAnswer OR @FromAnswer IS NULL)
	AND ([ToSMSQuestionGroupID] = @ToSMSQuestionGroupID OR @ToSMSQuestionGroupID IS NULL)
	AND ([ToSMSQuestionID] = @ToSMSQuestionID OR @ToSMSQuestionID IS NULL)
	AND ([EndOfQuestionnaire] = @EndOfQuestionnaire OR @EndOfQuestionnaire IS NULL)
	AND ([Score] = @Score OR @Score IS NULL)
	AND ([ConfidenceRangeStart] = @ConfidenceRangeStart OR @ConfidenceRangeStart IS NULL)
	AND ([ConfidenceRangeEnd] = @ConfidenceRangeEnd OR @ConfidenceRangeEnd IS NULL)
	AND ([RuleID] = @RuleID OR @RuleID IS NULL)
	AND ([ProcessingOrder] = @ProcessingOrder OR @ProcessingOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SMSResponseMapID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [FromSMSQuestionGroupID]
	, [FromSMSQuestionID]
	, [FromAnswer]
	, [ToSMSQuestionGroupID]
	, [ToSMSQuestionID]
	, [EndOfQuestionnaire]
	, [Score]
	, [ConfidenceRangeStart]
	, [ConfidenceRangeEnd]
	, [RuleID]
	, [ProcessingOrder]
    FROM
	[dbo].[SMSResponseMap] WITH (NOLOCK) 
    WHERE 
	 ([SMSResponseMapID] = @SMSResponseMapID AND @SMSResponseMapID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SMSQuestionnaireID] = @SMSQuestionnaireID AND @SMSQuestionnaireID is not null)
	OR ([FromSMSQuestionGroupID] = @FromSMSQuestionGroupID AND @FromSMSQuestionGroupID is not null)
	OR ([FromSMSQuestionID] = @FromSMSQuestionID AND @FromSMSQuestionID is not null)
	OR ([FromAnswer] = @FromAnswer AND @FromAnswer is not null)
	OR ([ToSMSQuestionGroupID] = @ToSMSQuestionGroupID AND @ToSMSQuestionGroupID is not null)
	OR ([ToSMSQuestionID] = @ToSMSQuestionID AND @ToSMSQuestionID is not null)
	OR ([EndOfQuestionnaire] = @EndOfQuestionnaire AND @EndOfQuestionnaire is not null)
	OR ([Score] = @Score AND @Score is not null)
	OR ([ConfidenceRangeStart] = @ConfidenceRangeStart AND @ConfidenceRangeStart is not null)
	OR ([ConfidenceRangeEnd] = @ConfidenceRangeEnd AND @ConfidenceRangeEnd is not null)
	OR ([RuleID] = @RuleID AND @RuleID is not null)
	OR ([ProcessingOrder] = @ProcessingOrder AND @ProcessingOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Find] TO [sp_executeall]
GO
