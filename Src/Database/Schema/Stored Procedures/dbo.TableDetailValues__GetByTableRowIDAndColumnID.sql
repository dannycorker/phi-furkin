SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TableDetailValues__GetByTableRowIDAndColumnID]
	@TableRowID INT = NULL,
	@TableDetailFieldID INT = NULL,
	@ColumnDetailFieldID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SqlQuerID INT,
	@SQL VARCHAR(MAX),
	@newword varchar(100),
	@body VARCHAR(MAX),
	@Columns VARCHAR(2000),
	@ColWidth INT,
	@Loop INT = 0,
	@MyXML XML,
	@ColumnFieldName VARCHAR(2000),
	@Xpath VARCHAR(2000),
	@Output VARCHAR(2000),
	@ClientID INT

	DECLARE @Table Table (XMLValue XML)

	/*Temporary logging.  Remove at next release*/
	DECLARE @LogEntry VARCHAR(2000)
	SELECT @LogEntry = 'EXEC TableDetailValues__GetByTableRowIDAndColumnID @TableRowID = ' + ISNULL(CONVERT(VARCHAR,@TableRowID),'NULL') + ',@TableDetailFieldID = ' + ISNULL(CONVERT(VARCHAR,@TableDetailFieldID),'NULL') + ',@ColumnDetailFieldID = ' + ISNULL(CONVERT(VARCHAR,@ColumnDetailFieldID),'NULL')
	EXEC _C00_LogIt 'Info', 'TableDetailValues__GetByTableRowIDAndColumnID', 'TableDetailValues__GetByTableRowIDAndColumnID', @LogEntry, 32825 /*Cathal*/
	
	SELECT @ClientID = tr.ClientID
	FROM TableRows tr WITH ( NOLOCK ) 
	WHERE tr.TableRowID = @TableRowID
	
	IF EXISTS (SELECT *
		FROM CustomTableSQL c WITH (NOLOCK)
		WHERE c.DetailFieldID = @TableDetailFieldID)
	BEGIN
	
		SELECT @Columns = c.HeaderColumns, @SqlQuerID = c.SQLQueryID, @ClientID = c.ClientID
		FROM CustomTableSQL c WITH (NOLOCK)
		WHERE c.DetailFieldID = @TableDetailFieldID
		
		SELECT @ColumnFieldName = REPLACE(df.FieldCaption, ' ', '')
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldID = @ColumnDetailFieldID
	
		SELECT @SQL = sc.QueryText + 'AND TableRows.TableRowID = ' + CONVERT(VARCHAR,@TableRowID)
		FROM SqlQuery sc WITH (NOLOCK)
		WHERE sc.QueryID = @SqlQuerID
		
		SELECT @SQL = REPLACE(@SQL, 'AND (Matter.MatterID = @MatterID)', '')
		SELECT @SQL = REPLACE(@SQL, 'AND Matter.MatterID = @MatterID', '')
		SELECT @SQL = REPLACE(@SQL, 'AND (Lead.LeadID = @LeadID)', '')
		SELECT @SQL = REPLACE(@SQL, '@ClientID', CONVERT(VARCHAR,@ClientID))
		
		WHILE @SQL LIKE '%cell_replace%' AND @Loop < 50
		BEGIN

			SELECT TOP 1 @newword = REPLACE(CONVERT(VARCHAR(100),RTRIM(LTRIM(Item))), ' ', '')
			FROM dbo.fnSplitString (@Columns, ',') f
			WHERE f.ItemIndex = @Loop
			ORDER by f.ItemIndex

			select  @SQL = stuff(@SQL, charindex('cell_replace', @SQL), len('cell_replace'), @newword)

			SELECT @Loop = @Loop + 1

		END

		/*Add the outer select and inner select together and format as XML*/
		SELECT @SQL = 'SELECT CAST((' + @SQL + ' FOR XML RAW(''tr''), ELEMENTS
			) AS VARCHAR(MAX))'

		PRINT @SQL 
		INSERT INTO @Table
		EXEC (@SQL)
		
		SELECT @MyXML = t.XMLValue
		FROM @Table t
		
		SELECT @Xpath = '(//' + @ColumnFieldName + ')[1]'

		EXEC dbo.XML_SelectSingleNode @MyXML, @Xpath, @Output OUTPUT
		
		DECLARE @Table2 TABLE ([TableDetailValueID] [int] NOT NULL,
	[TableRowID] [int] NOT NULL,
	[ResourceListID] [int] NULL,
	[DetailFieldID] [int] NOT NULL,
	[DetailValue] [varchar](2000) NULL,
	[LeadID] [int] NULL,
	[MatterID] [int] NULL,
	[ClientID] [int] NOT NULL,
	[EncryptedValue] [varchar](3000) NULL,
	[ErrorMsg] [varchar](1000) NULL,
	[ValueInt] [int] NULL,
	[ValueMoney] [money] NULL,
	[ValueDate] [date] NULL,
	[ValueDateTime] [datetime2](0) NULL,
	[CustomerID] [int] NULL,
	[CaseID] [int] NULL,
	[ClientPersonnelID] [int] NULL,
	[ContactID] [int] NULL)

		INSERT INTO @Table2
		SELECT CONVERT(INT,1) AS TableDetailValueID, @TableRowID AS [TableRowID], CONVERT(INT,NULL) AS ResourceListID, CONVERT(INT,@ColumnDetailFieldID) AS DetailFieldID, CONVERT(VARCHAR(2000),@Output) AS DetailValue, CONVERT(INT,0) AS LeadID, CONVERT(INT,0) AS MatterID, CONVERT(INT,@ClientID) AS ClientID, CONVERT(VARCHAR(3000),'') AS EncryptedValue, CONVERT(VARCHAR(1000),'') AS ErrorMsg, CONVERT(INT,0) AS ValueInt, CONVERT(INT,0) AS ValueMoney, CONVERT(Date,NULL) AS ValueDate, CONVERT(DATETIME2(0),NULL) AS ValueDateTime, CONVERT(INT,0) AS CustomerID, CONVERT(INT,0) AS CaseID, CONVERT(INT,0) AS ClientPersonnelID, CONVERT(INT,0) AS ContactID

		SELECT *
		FROM @Table2

		--PRINT @Sql
	
	END
	ELSE
	BEGIN
	
		SELECT *
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.TableRowID = @TableRowID
		AND tdv.DetailFieldID = @ColumnDetailFieldID

	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndColumnID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByTableRowIDAndColumnID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndColumnID] TO [sp_executeall]
GO
