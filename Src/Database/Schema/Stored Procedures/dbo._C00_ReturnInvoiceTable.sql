SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-08-11
-- Description:	Return a listing of Invoices From third party fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReturnInvoiceTable]
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT,
			@InvoiceNumberFieldID INT,
			@CaseNumFieldID INT,
			@OutstandingFieldID INT,
			@InvoiceTableFieldID INT,
			@SaveValueIntoFieldID INT,
			@AmountToPayFieldID INT
	
	SELECT @ClientID = c.ClientID
	FROM Customers c WITH (NOLOCK)
	WHERE c.CustomerID = @CustomerID

	SELECT @InvoiceTableFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 0, 70, 1305),
			@InvoiceNumberFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 0, 70, 1306),
			@OutstandingFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 0, 70, 1307),
			@CaseNumFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 0, 70, 1308),
			@SaveValueIntoFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 0, 70, 1309),
			@AmountToPayFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 0, 70, 1310)


	SELECT tr.TableRowID, tdv_invno.DetailValue AS [InvoiceNumber], l.LeadID, lt.LeadTypeName, tdv_caseID.ValueInt AS [CaseID], c.CaseNum, m.MatterRef, tdv_outstanding.DetailValue AS [Outstanding], @SaveValueIntoFieldID AS [SaveToFieldID], tdv_topay.DetailValue AS [AmountToPay]
	FROM TableRows tr WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) ON tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = @InvoiceNumberFieldID
	INNER JOIN TableDetailValues tdv_caseID WITH (NOLOCK) ON tdv_caseID.TableRowID = tdv_invno.TableRowID AND tdv_caseID.DetailFieldID = @CaseNumFieldID
	INNER JOIN TableDetailValues tdv_outstanding WITH (NOLOCK) ON tdv_outstanding.TableRowID = tdv_caseID.TableRowID AND tdv_outstanding.DetailFieldID = @OutstandingFieldID
	LEFT JOIN TableDetailValues tdv_topay WITH (NOLOCK) ON tdv_topay.TableRowID = tr.TableRowID and tdv_topay.DetailFieldID = @AmountToPayFieldID
	INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = tdv_caseID.ValueInt
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
	WHERE tr.DetailFieldID = @InvoiceTableFieldID
	AND tr.CustomerID = @CustomerID
	ORDER BY lt.[LeadTypeName], l.[LeadID], c.[CaseNum]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReturnInvoiceTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReturnInvoiceTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReturnInvoiceTable] TO [sp_executeall]
GO
