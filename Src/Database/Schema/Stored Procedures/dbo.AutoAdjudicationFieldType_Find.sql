SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AutoAdjudicationFieldType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationFieldType_Find]
(

	@SearchUsingOR bit   = null ,

	@AutoAdjudicationFieldTypeID int   = null ,

	@AutoAdjudicationFieldTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AutoAdjudicationFieldTypeID]
	, [AutoAdjudicationFieldTypeName]
    FROM
	[dbo].[AutoAdjudicationFieldType] WITH (NOLOCK) 
    WHERE 
	 ([AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID OR @AutoAdjudicationFieldTypeID IS NULL)
	AND ([AutoAdjudicationFieldTypeName] = @AutoAdjudicationFieldTypeName OR @AutoAdjudicationFieldTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AutoAdjudicationFieldTypeID]
	, [AutoAdjudicationFieldTypeName]
    FROM
	[dbo].[AutoAdjudicationFieldType] WITH (NOLOCK) 
    WHERE 
	 ([AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID AND @AutoAdjudicationFieldTypeID is not null)
	OR ([AutoAdjudicationFieldTypeName] = @AutoAdjudicationFieldTypeName AND @AutoAdjudicationFieldTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationFieldType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_Find] TO [sp_executeall]
GO
