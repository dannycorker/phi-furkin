SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-17
-- Description:	Gets a useful list of Cases that the current PortalUser has access to
-- JWG 2014-02-03 #25493 Adapt this slightly for the ManageLeadAccess page in Lead Manager, aka "Specific Access", to show
--                the Rights to each Case (None, View, Full). FYI this proc is also used to populate PortalUser.ascx within CustomersLeadDetails2
-- JWG 2014-05-13 #26507 LatestRecordsFirst 
-- =============================================
CREATE PROCEDURE  [dbo].[PortalUserCase__GetLeadsAndCases]
	@PortalUserID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LatestRecordsFirst BIT = 0
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	INNER JOIN dbo.PortalUser pu WITH (NOLOCK) ON pu.ClientID = cl.ClientID
	WHERE pu.PortalUserID = @PortalUserID
	

    -- Select all PortalUserCase details for the chosen Portal user,
    -- and also get Lead, Case, Customer
	SELECT		puc.PortalUserCaseID, 
				puc.PortalUserID, 
				puc.LeadID, 
				puc.CaseID, 
				c.CustomerID, 
				c.Fullname, 
				ca.CaseNum, 
				puc.ClientID, 
				'FULL' AS [AccessLevel] /* View=Full in the Portal, and None means this record doesn't exist in the first place */
	FROM        dbo.PortalUserCase puc WITH (NOLOCK) 
	INNER JOIN	Lead l WITH (NOLOCK) ON puc.LeadID = l.LeadID 
	INNER JOIN	Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID 
	LEFT JOIN   Cases ca WITH (NOLOCK) ON ca.CaseID = puc.CaseID 
	WHERE		puc.PortalUserID = @PortalUserID 
	/*ORDER BY l.LeadID ASC, ca.CaseNum ASC*/
	ORDER BY 
		/*
			JWG 2014-05-13 #26507 LatestRecordsFirst
			The dynamic part is really the ASC/DESC rather than the column to order by, but this is the only way to do it at present.
		*/
		CASE @LatestRecordsFirst WHEN 1 THEN l.LeadID END DESC, 
		CASE @LatestRecordsFirst WHEN 0 THEN l.LeadID END ASC, 
		CASE @LatestRecordsFirst WHEN 1 THEN ca.CaseNum END DESC, 
		CASE @LatestRecordsFirst WHEN 0 THEN ca.CaseNum END ASC 
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetLeadsAndCases] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__GetLeadsAndCases] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetLeadsAndCases] TO [sp_executeall]
GO
