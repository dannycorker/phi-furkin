SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DiaryAppointment table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_Find]
(

	@SearchUsingOR bit   = null ,

	@DiaryAppointmentID int   = null ,

	@ClientID int   = null ,

	@DiaryAppointmentTitle varchar (250)  = null ,

	@DiaryAppointmentText varchar (2000)  = null ,

	@CreatedBy int   = null ,

	@DueDate datetime   = null ,

	@EndDate datetime   = null ,

	@AllDayEvent bit   = null ,

	@Completed bit   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@CustomerID int   = null ,

	@Version int   = null ,

	@ExportVersion int   = null ,

	@RecurrenceInfo varchar (MAX)  = null ,

	@DiaryAppointmentEventType int   = null ,

	@ResourceInfo varchar (MAX)  = null ,

	@TempReminderTimeshiftID int   = null ,

	@StatusID int   = null ,

	@LabelID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DiaryAppointmentID]
	, [ClientID]
	, [DiaryAppointmentTitle]
	, [DiaryAppointmentText]
	, [CreatedBy]
	, [DueDate]
	, [EndDate]
	, [AllDayEvent]
	, [Completed]
	, [LeadID]
	, [CaseID]
	, [CustomerID]
	, [Version]
	, [ExportVersion]
	, [RecurrenceInfo]
	, [DiaryAppointmentEventType]
	, [ResourceInfo]
	, [TempReminderTimeshiftID]
	, [StatusID]
	, [LabelID]
    FROM
	[dbo].[DiaryAppointment] WITH (NOLOCK) 
    WHERE 
	 ([DiaryAppointmentID] = @DiaryAppointmentID OR @DiaryAppointmentID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DiaryAppointmentTitle] = @DiaryAppointmentTitle OR @DiaryAppointmentTitle IS NULL)
	AND ([DiaryAppointmentText] = @DiaryAppointmentText OR @DiaryAppointmentText IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([DueDate] = @DueDate OR @DueDate IS NULL)
	AND ([EndDate] = @EndDate OR @EndDate IS NULL)
	AND ([AllDayEvent] = @AllDayEvent OR @AllDayEvent IS NULL)
	AND ([Completed] = @Completed OR @Completed IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([Version] = @Version OR @Version IS NULL)
	AND ([ExportVersion] = @ExportVersion OR @ExportVersion IS NULL)
	AND ([RecurrenceInfo] = @RecurrenceInfo OR @RecurrenceInfo IS NULL)
	AND ([DiaryAppointmentEventType] = @DiaryAppointmentEventType OR @DiaryAppointmentEventType IS NULL)
	AND ([ResourceInfo] = @ResourceInfo OR @ResourceInfo IS NULL)
	AND ([TempReminderTimeshiftID] = @TempReminderTimeshiftID OR @TempReminderTimeshiftID IS NULL)
	AND ([StatusID] = @StatusID OR @StatusID IS NULL)
	AND ([LabelID] = @LabelID OR @LabelID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DiaryAppointmentID]
	, [ClientID]
	, [DiaryAppointmentTitle]
	, [DiaryAppointmentText]
	, [CreatedBy]
	, [DueDate]
	, [EndDate]
	, [AllDayEvent]
	, [Completed]
	, [LeadID]
	, [CaseID]
	, [CustomerID]
	, [Version]
	, [ExportVersion]
	, [RecurrenceInfo]
	, [DiaryAppointmentEventType]
	, [ResourceInfo]
	, [TempReminderTimeshiftID]
	, [StatusID]
	, [LabelID]
    FROM
	[dbo].[DiaryAppointment] WITH (NOLOCK) 
    WHERE 
	 ([DiaryAppointmentID] = @DiaryAppointmentID AND @DiaryAppointmentID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DiaryAppointmentTitle] = @DiaryAppointmentTitle AND @DiaryAppointmentTitle is not null)
	OR ([DiaryAppointmentText] = @DiaryAppointmentText AND @DiaryAppointmentText is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([DueDate] = @DueDate AND @DueDate is not null)
	OR ([EndDate] = @EndDate AND @EndDate is not null)
	OR ([AllDayEvent] = @AllDayEvent AND @AllDayEvent is not null)
	OR ([Completed] = @Completed AND @Completed is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([Version] = @Version AND @Version is not null)
	OR ([ExportVersion] = @ExportVersion AND @ExportVersion is not null)
	OR ([RecurrenceInfo] = @RecurrenceInfo AND @RecurrenceInfo is not null)
	OR ([DiaryAppointmentEventType] = @DiaryAppointmentEventType AND @DiaryAppointmentEventType is not null)
	OR ([ResourceInfo] = @ResourceInfo AND @ResourceInfo is not null)
	OR ([TempReminderTimeshiftID] = @TempReminderTimeshiftID AND @TempReminderTimeshiftID is not null)
	OR ([StatusID] = @StatusID AND @StatusID is not null)
	OR ([LabelID] = @LabelID AND @LabelID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Find] TO [sp_executeall]
GO
