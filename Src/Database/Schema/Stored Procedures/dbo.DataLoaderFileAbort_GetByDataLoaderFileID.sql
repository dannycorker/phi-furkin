SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFileAbort table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_GetByDataLoaderFileID]
(

	@DataLoaderFileID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderFileAbortID],
					[ClientID],
					[DateTimeFileAborted],
					[ReasonForAbort],
					[DataLoaderFileID],
					[FileAbortedOn],
					[LastRowImported]
				FROM
					[dbo].[DataLoaderFileAbort] WITH (NOLOCK) 
				WHERE
					[DataLoaderFileID] = @DataLoaderFileID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_GetByDataLoaderFileID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_GetByDataLoaderFileID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_GetByDataLoaderFileID] TO [sp_executeall]
GO
