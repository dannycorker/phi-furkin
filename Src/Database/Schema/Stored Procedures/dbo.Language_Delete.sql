SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Language table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language_Delete]
(

	@LanguageID int   
)
AS


				DELETE FROM [dbo].[Language] WITH (ROWLOCK) 
				WHERE
					[LanguageID] = @LanguageID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Delete] TO [sp_executeall]
GO
