SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LDAPConfig table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LDAPConfig_Get_List]

AS


				
				SELECT
					[LDAPConfigID],
					[ClientID],
					[QueryString],
					[QueryAccountName],
					[QueryAccountPassword],
					[Enabled],
					[Domain]
				FROM
					[dbo].[LDAPConfig] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Get_List] TO [sp_executeall]
GO
