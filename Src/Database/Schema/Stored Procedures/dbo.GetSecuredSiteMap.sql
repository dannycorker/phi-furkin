SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetSecuredSiteMap    Script Date: 08/09/2006 12:22:41 ******/
CREATE PROCEDURE [dbo].[GetSecuredSiteMap] AS


SELECT     SiteMapID, Url, Title, Description, ParentID, Secured
FROM         dbo.SiteMap
WHERE     (Secured = 1)




GO
GRANT VIEW DEFINITION ON  [dbo].[GetSecuredSiteMap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSecuredSiteMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSecuredSiteMap] TO [sp_executeall]
GO
