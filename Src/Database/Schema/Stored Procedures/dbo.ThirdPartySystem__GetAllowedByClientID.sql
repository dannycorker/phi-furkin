SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-23
-- Description:	Get Third Party System(s) for Client
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartySystem__GetAllowedByClientID]
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @UseUltra BIT,
			@UseXero BIT
	
	SELECT @UseUltra = co.UseUltra, @UseXero = co.UseXero
	FROM ClientOption co WITH (NOLOCK) 
	WHERE co.ClientID = @ClientID 

	SELECT tps.ThirdPartySystemId, tps.SystemName, tps.WhenCreated, tps.WhenModified
	FROM ThirdPartySystem tps WITH (NOLOCK)
	WHERE (tps.ThirdPartySystemId = 4 AND @UseUltra = 1)
	OR (tps.ThirdPartySystemId = 5 AND @UseXero = 1)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem__GetAllowedByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem__GetAllowedByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem__GetAllowedByClientID] TO [sp_executeall]
GO
