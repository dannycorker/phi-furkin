SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-11-20
-- Description:	Show and create ClientPasswordRule records
-- =============================================
CREATE PROCEDURE [dbo].[cpr]
	@ClientID INT = NULL, 
	@AutoCreate BIT = NULL, 
	@ImmediateResetRequired BIT = 0, 
	@ExpiryDays INT = 30, 
	@MinLength INT = 1, 
	@MinLowerCase INT = 1, 
	@MinNumber INT = 1, 
	@MinSymbol INT = 1, 
	@MinUpperCase INT = 1, 
	@PasswordHistoryCount INT = 1, 
	@ResetForcesChange BIT = 1 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ExistingRuleID INT
	
	/* Show all rules by default */
	IF @ClientID IS NULL AND @AutoCreate IS NULL
	BEGIN
	
		SELECT *
		FROM dbo.ClientPasswordRule cpr WITH (NOLOCK) 
		ORDER BY cpr.ClientID
		
	END
	ELSE
	BEGIN
	
		IF @ClientID > 0
		BEGIN
		
			/* Check for an existing rule */
			SELECT @ExistingRuleID = cpr.ClientPasswordRuleID
			FROM dbo.ClientPasswordRule cpr WITH (NOLOCK) 
			WHERE cpr.ClientID = @ClientID
			
			IF @ExistingRuleID IS NULL AND @AutoCreate = 1
			BEGIN
				INSERT INTO dbo.ClientPasswordRule (ClientID, ExpiryDays, MinLength, MinLowerCase, MinNumber, MinSymbol, MinUpperCase, PasswordHistoryCount, ResetForcesChange)
				VALUES (@ClientID, @ExpiryDays, @MinLength, @MinLowerCase, @MinNumber, @MinSymbol, @MinUpperCase, @PasswordHistoryCount, @ResetForcesChange)
				
				SELECT @ExistingRuleID = SCOPE_IDENTITY()
				
				/*
					Update all ClientPersonnel records for this client.
					This can be right now, or (eg) 30 days in the future, whatever the value of @ExpiryDays is set to.
				*/
				UPDATE dbo.ClientPersonnel
				SET ForcePasswordChangeOn = DATEADD(DAY, CASE WHEN @ImmediateResetRequired = 1 THEN -1 ELSE @ExpiryDays END, dbo.fn_GetDate_Local())
				WHERE ClientID = @ClientID
			END
			
			IF @ExistingRuleID > 0
			BEGIN
				SELECT *
				FROM dbo.ClientPasswordRule cpr WITH (NOLOCK) 
				ORDER BY cpr.ClientID
			END
			
		END
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[cpr] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cpr] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cpr] TO [sp_executeall]
GO
