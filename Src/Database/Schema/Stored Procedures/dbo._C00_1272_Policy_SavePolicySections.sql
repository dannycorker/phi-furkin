SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-01-31
-- Description:	Saves the policy sections the user has selected
-- Used by:		DF 145704
-- ROH 2014-01-20 Fix for bug with historical claims where the cover had since lapsed Zen#25324
-- ROH 2014-02-03 Last fix was over-zealous. Further refined to make sure Coins cutoff is pushed out to the renewal date Zen#25517
-- SB  2014-05-01 Correcting inconsistencies over policy date handling for no-cover rows
-- SB  2014-07-01 Added breed for co-insurance lookup and also allowed to run in debug even if claim locked
-- SB  2014-12-06 Pass @ReturnMultipleYearsForExcess = 1 to the get policy cover proc for SIG as they always want 12 month and reinstatement rows to be split across policy years
-- ROH 2014-12-16 Create no-cover rows where pet is older than the policy section age limit Zen#29705
-- SB  2015-06-18 Change to allow multiple levels of age driven coinsurance ZD #33065
-- SB  2015-06-25 Fixed end dates to always be out of cover.  ZD #33065
-- SB  2015-10-22 Moved logic to clear rows for age related policy limit to the calcs so it can work for optional limits too
-- SB  2015-10-26 Appled NCI fix #33167 while I am working in this area
-- JL  2016-07-02 Applied while loop to allow Policy type (reinstatment etc) to be applied at section level. This should mean that we loop over for each section passing in the correct type. 
-- JL  2016-11-16 Forces 'Multiple years for excess' to be true across the board
-- JL  2019-03-06 As we have fixed the policy end date elsewhere, we no longer want to subtract a day from the treatment end 
-- CPS 2019-03-21 NoCoverEnd date display modification for #55546
-- GPR 2019-05-03 uncommented block removed on 6th March as TreatmentEnd is a day out for Defect #1685 C600
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_SavePolicySections]
(
	@LeadID INT,
	@MatterID INT,
	@ResourceListIDs dbo.TvpInt READONLY,
	@Debug INT = 0
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LogEntry VARCHAR(2000) = ''
	
	SELECT @LogEntry = ' @LeadID = ' + ISNULL(CONVERT(VARCHAR,@LeadID),'NULL')
					 + ',@MatterID = ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL')
					 + ',@ResourceListIDs = '
	
	SELECT @LogEntry += CONVERT(VARCHAR,tvp.AnyID) + ','
	FROM @ResourceListIDs tvp
	
	EXEC _C00_LogIt 'Info', '_C00_1272_Policy_SavePolicySections', 'Input Parameters', @LogEntry, 0
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	DECLARE @CaseID INT
	SELECT @CaseID = m.CaseID
	FROM dbo.Matter m
	WHERE m.MatterID = @MatterID

	DECLARE @CurrentPolicyMatterID INT
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)

	DECLARE @Sections TABLE (RowNumber INT,ResourceListID INT, SectionType INT, Processed BIT) 
	INSERT @Sections (RowNumber,ResourceListID,SectionType, Processed)
	SELECT ROW_NUMBER() OVER (ORDER BY r.AnyID DESC), r.AnyID, tdv1.ValueInt, 0  FROM @ResourceListIDs r
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.ResourceListID = r.AnyID AND tdv.DetailFieldID = 144357
	INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 176935
	WHERE tdv.MatterID = @CurrentPolicyMatterID
	
	-- Get the treament start and end dates for this claim
	DECLARE @TreatmentStart DATE,
			@TreatmentEnd DATE,
			@DateOfLoss DATE
	
	SELECT @TreatmentStart = ValueDate
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144366
	
	SELECT @TreatmentEnd = ValueDate
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 145674
	
	IF @TreatmentEnd < @TreatmentStart
	BEGIN
	
		RAISERROR('<BR><BR><font color="red">On the Claim Details Tab. Treatment Start Date is Later than the Treatment End date.</font>',16,1)
		
	END
	
	SELECT @DateOfLoss = ValueDate
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144892
	
	DECLARE @IDToProcess INT,
			@RLID INT,
			@SectionType INT 

	IF dbo.fn_C00_1272_Claim_AllowedTo('edit', @CaseID) = 1 OR @Debug = 1
	BEGIN
			
		WHILE EXISTS (SELECT * FROM @Sections s WHERE s.Processed = 0)
		BEGIN

			SELECT TOP 1 @IDToProcess = s.RowNumber, @RLID = s.ResourceListID, @SectionType = s.SectionType 
			FROM @Sections s 
			WHERE s.Processed = 0 
		
			-- Get the policy cover dates.  
			DECLARE @PolicyCover TABLE
			(
				PolicyType VARCHAR(50),
				StartDate DATE,
				EndDate DATE,
				InceptionDate DATE
			)
			
			DECLARE @ReturnMultipleYears BIT = NULL  
			IF @ClientID = 384
			BEGIN
				SELECT @ReturnMultipleYears = 1
			END
			
			INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
			EXEC dbo._C00_1272_Policy_GetGroupedPolicyCover   @MatterID, @LeadID, @CurrentPolicyMatterID, @SectionType, @MaxBenLimitToTreatmentDates = 1--, @ReturnMultipleYears = @ReturnMultipleYears

			-- Start loading the date splits we need
			DECLARE @DateSplits TABLE
			(
				ResourceListID INT,
				ParentPolicySection INT,
				FromDate DATETIME,
				ToDate DATETIME,
				CoIns BIT,
				NewlyInserted BIT
			)
			
			INSERT @DateSplits (ResourceListID, ParentPolicySection, FromDate, ToDate, CoIns)
			SELECT @RLID, s.Out_ResourceListID, c.StartDate, c.EndDate, 0
			FROM @PolicyCover c
			INNER JOIN dbo.fn_C00_1272_GetAllSectionRelationships() s ON @RLID = s.ResourceListID

			-- Get a list of the co-insurance limits that apply
			DECLARE @PolicyCoInsurance TABLE
			(
				ID INT IDENTITY,
				ResourceListID INT,
				SECTION VARCHAR(2000),
				SubSection VARCHAR(2000),
				PercentageExcess DECIMAL(19,4),
				DateCutOff DATE,
				CoInsLinkedTo INT,
				MinThreshold MONEY,
				ClaimPaid MONEY,
				Breed VARCHAR(2000)
			)
			INSERT @PolicyCoInsurance (ResourceListID, SECTION, SubSection, PercentageExcess, DateCutOff, CoInsLinkedTo, MinThreshold, Breed)
			EXEC dbo._C00_1272_Policy_GetGroupedPolicyCoInsurance @MatterID, @CurrentPolicyMatterID
			
			DECLARE @CoInsAnniversaryCount INT
			SELECT @CoInsAnniversaryCount = COUNT(*) 
			FROM @PolicyCoInsurance
			WHERE CoInsLinkedTo = 52783
			
			IF @CoInsAnniversaryCount > 0
			BEGIN
							
				-- Get the next renewal date
				DECLARE @Renewal DATE
				SELECT @Renewal = DATEADD(YEAR, 1, PolicyStart)
				FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
						
				-- If we have co-insurance rows linked to the policy anniversary then we need to update the Date Cut off to be the 
				-- earliest policy from date that is after the current cut off
				;WITH PolicyDates AS 
				(
					SELECT tdvFrom.ValueDate
					FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
					INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
				), CoInsPolicyDates AS
				(
					SELECT co.*, pd.ValueDate, ROW_NUMBER() OVER(PARTITION BY co.ID ORDER BY pd.ValueDate) AS rn 
					FROM @PolicyCoInsurance co
					INNER JOIN PolicyDates pd ON pd.ValueDate >= co.DateCutOff
					WHERE co.CoInsLinkedTo = 52783
				), EarliesPolicyDates AS 
				(
					SELECT * 
					FROM CoInsPolicyDates
					WHERE rn = 1
				)
				UPDATE ci
				SET ci.DateCutOff = ISNULL(pd.ValueDate, @Renewal) -- try and match with the policy history but roll back the the renewal date in the future
				--SET ci.DateCutOff = pd.ValueDate
				FROM @PolicyCoInsurance ci
				LEFT JOIN EarliesPolicyDates pd ON ci.ID = pd.ID
				--INNER JOIN EarliesPolicyDates pd ON ci.ID = pd.ID
				
			
			END
		
			
			-- If the co-insurance is linked to the date of onset and the cut off date is greater than the treatment start date then delete these rows as they will not apply		
			DELETE @PolicyCoInsurance
			WHERE CoInsLinkedTo = 45241
			--AND DateCutOff > @TreatmentStart -- Date of loss not treatment start! Zen#33167
			AND DateCutOff > @DateOfLoss

			
			
			-- For all the rest we now...
			-- 1. Update all cover periods where the from date is greater than or equal to the cut off date to flag as co-ins
			UPDATE ds
			SET CoIns = 1
			FROM @DateSplits ds
			INNER JOIN @PolicyCoInsurance co ON ds.ParentPolicySection = co.ResourceListID 
			WHERE ds.FromDate >= co.DateCutOff	
			
			-- 2. Insert a new co-ins period
			INSERT @DateSplits (ResourceListID, ParentPolicySection, FromDate, ToDate, CoIns, NewlyInserted)
			SELECT ds.ResourceListID, ds.ParentPolicySection, co.DateCutOff, ds.ToDate, 1, 1 
			FROM @DateSplits ds
			INNER JOIN @PolicyCoInsurance co ON ds.ParentPolicySection = co.ResourceListID 
			--WHERE co.DateCutOff BETWEEN ds.FromDate AND ds.ToDate
			WHERE (co.DateCutOff >= ds.FromDate AND co.DateCutOff < ds.ToDate)
			AND co.CoInsLinkedTo IN (45242, 52783) -- If linked to date of onset then co-insurance always applies to the full period so only need to split for treatment date / policy anninversary
			--AND ds.CoIns = 0 -- We need to remove this condition so that we split rows for multiple co-ins cut in dates
			
			-- 3. Split a cover period where the co-ins cut off falls between the policy cover dates
			UPDATE ds
			SET ds.ToDate = co.DateCutOff -- DATEADD(DAY, -1, co.DateCutOff) Changed this so end date is consistently out of cover
			FROM @DateSplits ds
			INNER JOIN @PolicyCoInsurance co ON ds.ParentPolicySection = co.ResourceListID 
			--WHERE co.DateCutOff BETWEEN ds.FromDate AND ds.ToDate
			WHERE (co.DateCutOff >= ds.FromDate AND co.DateCutOff < ds.ToDate)
			AND co.CoInsLinkedTo IN (45242, 52783)
			--AND ds.CoIns = 0 -- limit to non Co-Ins rows so we don't affect the new row inserted in 2. above
			AND ds.NewlyInserted IS NULL -- limit to existing rows so we don't affect the new row inserted in 2. above
			
			-- Get a table of our final data
			DECLARE @RowsToInsert TABLE
			(
				ID INT IDENTITY,
				ResourceListID INT,
				FromDate DATE,
				ToDate DATE,
				TYPE INT,
				TypeName VARCHAR(50)
			)
			
			;WITH NoCoverStart AS 
			(
				SELECT *, 
				ROW_NUMBER() OVER(PARTITION BY ResourceListID ORDER BY FromDate) AS rn 
				FROM @DateSplits
			),
			NoCoverEnd AS 
			(
				SELECT *, 
				ROW_NUMBER() OVER(PARTITION BY ResourceListID ORDER BY ToDate DESC) AS rn 
				FROM @DateSplits
			)
			INSERT @RowsToInsert (ResourceListID, FromDate, ToDate, TYPE, TypeName)
			-- No cover at the start of the treatment period... (goes from the treatment start up to the day before cover begins) not anymore...
			-- #33065 - goes from the treatment start up to the day cover begins as the to date is OUT OF COVER
			--SELECT ResourceListID, @TreatmentStart AS FromDate, DATEADD(DAY, -1, FromDate) AS ToDate, 0 AS Type, 'NoCoverStart' AS TypeName
			SELECT ResourceListID, @TreatmentStart AS FromDate, DATEADD(DAY, 1, FromDate) AS ToDate, 0 AS TYPE, 'NoCoverStart' AS TypeName
			FROM NoCoverStart
			WHERE rn = 1 
			AND @TreatmentStart < FromDate
			UNION
			-- No cover at the end of the treatment period... goes from the end of the cover (remembering that the end date of a policy is out of cover)
																-- to the end of the treatment period ... #33065 add one day
																-- ToDate AS FromDate ... this shit practically writes itself!
			SELECT ResourceListID, DATEADD(DAY,1,ToDate) AS FromDate, DATEADD(DAY, 1, @TreatmentEnd) AS ToDate, 0 AS TYPE, 'NoCoverEnd' AS TypeName
			FROM NoCoverEnd
			WHERE rn = 1 
			--AND @TreatmentEnd >= ToDate -- Swapped this back #33065... not sure why it was changed in the first place 
			/*
			CPS 2019-02-20 swapped back AGAIN for ticket #54538
			Previous change meant that the final date of cover was marked as NO COVER, which is inconsistent with Policy Admin.
			Same issue reported by C433, C327 and C600
			SB unavailable to review with and RH no longer exists...
			JEL 2019-05-07 The saga continues... because we add a day in some cases to the ToDate, we need to add even if the end date = the treatment end date... so we can take it off again later. 
			*/
			AND @TreatmentEnd > ToDate
			UNION -- All the rest
			SELECT	ResourceListID,
					/*
					#33065
					CASE WHEN @TreatmentStart BETWEEN FromDate AND ToDate THEN @TreatmentStart ELSE FromDate END AS FromDate,
					CASE WHEN @TreatmentEnd BETWEEN FromDate AND ToDate THEN @TreatmentEnd ELSE ToDate END AS ToDate,
					*/				
					CASE WHEN @TreatmentStart >= FromDate AND @TreatmentStart < ToDate THEN @TreatmentStart ELSE FromDate END AS FromDate,
					CASE WHEN @TreatmentEnd >= FromDate AND @TreatmentEnd < ToDate THEN DATEADD(DAY, 1, @TreatmentEnd) ELSE DATEADD(DAY,1,ToDate) END AS ToDate,
					CASE WHEN CoIns = 0 THEN 1 ELSE 2 END AS TYPE,
					CASE WHEN CoIns = 0 THEN 'Standard' ELSE 'CoInsurance' END AS TypeName
			FROM @DateSplits
			-- #33065... trying to keep everything consistent with end being out of cover
			--WHERE @TreatmentStart <= ToDate AND @TreatmentEnd >= FromDate
			WHERE @TreatmentStart <= ToDate AND @TreatmentEnd >= FromDate
			ORDER BY ResourceListID, FromDate
			


			
			
			/* Set to no-cover any sections for which the pet is too old - See comment from 2015-10-22
			
			UPDATE rti SET Type = 0 -- No-cover row
			FROM 
				Matter m WITH (NOLOCK)
				INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON mdvDateOfLoss.MatterID = m.MatterID AND mdvDateOfLoss.DetailFieldID = 144892
				-- Policy lead
				INNER JOIN dbo.Lead lPolicy WITH (NOLOCK) ON lPolicy.CustomerID = m.CustomerID AND lPolicy.LeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(m.MatterID)
				INNER JOIN dbo.LeadDetailValues ldvPetDOB WITH (NOLOCK) ON ldvPetDOB.LeadID = lPolicy.LeadID AND ldvPetDOB.DetailFieldID = 144274
				INNER JOIN dbo.LeadDetailValues ldvPetBreed WITH (NOLOCK) ON ldvPetBreed.LeadID = lPolicy.LeadID AND ldvPetBreed.DetailFieldID = 144272
				INNER JOIN dbo.ResourceListDetailValues rldvPetType WITH (NOLOCK) ON rldvPetType.ResourceListID = ldvPetBreed.ValueInt AND rldvPetType.DetailFieldID = 144269
				---- Policy limits
				INNER JOIN dbo.TableRows rPolLimit WITH (NOLOCK) ON rPolLimit.MatterID = @CurrentPolicyMatterID AND rPolLimit.DetailFieldID = 145692
				INNER JOIN dbo.TableDetailValues tSection WITH (NOLOCK) ON tSection.TableRowID = rPolLimit.TableRowID AND tSection.DetailFieldID = 144357
				INNER JOIN dbo.TableDetailValues tLimitType WITH (NOLOCK) ON tLimitType.TableRowID = rPolLimit.TableRowID AND tLimitType.DetailFieldID = 175388
				INNER JOIN dbo.TableDetailValues tAgeLimit WITH (NOLOCK) ON tAgeLimit.TableRowID = rPolLimit.TableRowID AND tAgeLimit.DetailFieldID = 144267
				LEFT JOIN dbo.TableDetailValues tPetType WITH (NOLOCK) ON tPetType.TableRowID = rPolLimit.TableRowID AND tPetType.DetailFieldID = 170013
				-- Rows being inserted
				INNER JOIN @RowsToInsert rti ON rti.ResourceListID = tSection.ResourceListID
			WHERE 
				m.MatterID = @Matterid
				AND tLimitType.ValueInt = 72058 -- Age limit
				AND (tPetType.ValueInt IS NULL OR tPetType.ValueInt = rldvPetType.ValueInt) -- Matching pet type
				AND DATEADD(YEAR, tAgeLimit.ValueInt, ldvPetDOB.ValueDate) <= mdvDateOfLoss.ValueDate -- Pet too old
			*/
			
			/* Finalise */
			
			DECLARE @RowCount INT, @ExistingRowCount INT
			
			-- Delete any rows where the From and ToDates are the same as this represents no cover.
			DELETE @RowsToInsert
			WHERE FromDate = ToDate
			
				
			SELECT @RowCount = COUNT(*) 
			FROM @Sections
			
			SELECT @ExistingRowCount = COUNT(*)  
			FROM dbo.TableRows WITH (NOLOCK) 
			WHERE MatterID = @MatterID
			AND DetailFieldID = 144355
			AND DetailFieldPageID = 16157
			
			PRINT 'Pre-debug'
			
			IF @Debug = 1
			BEGIN

			PRINT 'Debug route'
				
				SELECT 'PolicyCover' AS PolicyCover, * 
				FROM @PolicyCover
				
				SELECT 'DateSplits', * 
				FROM @DateSplits
				ORDER BY ResourceListID, FromDate 
				
				SELECT 'PolicyCoInsurance', * 
				FROM @PolicyCoInsurance
				
				SELECT 'RowsToInsert' , * 
				FROM @RowsToInsert
				
				SELECT @RowCount AS [ROWCOUNT], @ExistingRowCount AS ExistingRowCount
			
			END
		
			DELETE @PolicyCover
			DELETE @DateSplits
			DELETE @PolicyCoInsurance
			
			UPDATE s
			SET Processed = 1 
			FROM @Sections s 
			WHERE s.RowNumber = @IDToProcess 
		
		END
			
		
		IF @Debug = 0
		BEGIN
			
			PRINT 'Debug = 0'

			-- Now finally change the to date to be in cover so it makes sense to the user... we don't match on this in the calcs so it doesn't really matter but trying to make it look OK.
			/*JEL 2019-03-06 an earlier fix makes this redundant*/ 
			/*GPR 2019-05-03 added back in for #1685*/
			UPDATE @RowsToInsert
			SET ToDate = DATEADD(DAY, -1, ToDate)

			-- Now do the inserts themselved
			DECLARE @Inserted TABLE
			(
				ID INT,
				TableRowID INT
			)
			-- Keep track of the table rows inserted
			INSERT dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @Inserted
			SELECT @ClientID, @MatterID, 144355, 16157, ID /*Claim Details Data*/
			FROM @RowsToInsert
		
		-- And now insert the table detail values
			
			-- Section RLID
			INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, ResourceListID, LeadID, MatterID, ClientID)
			SELECT i.TableRowID, 144350, r.ResourceListID, @LeadID, @MatterID, @ClientID /*Policy Section*/
			FROM @RowsToInsert r
			INNER JOIN @Inserted i ON r.ID = i.ID 
			-- From
			INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
			SELECT i.TableRowID, 144349, r.FromDate, @LeadID, @MatterID, @ClientID /*Treat Start*/
			FROM @RowsToInsert r
			INNER JOIN @Inserted i ON r.ID = i.ID 
			-- To
			INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
			SELECT i.TableRowID, 144351, r.ToDate, @LeadID, @MatterID, @ClientID /*Treatment End*/
			FROM @RowsToInsert r
			INNER JOIN @Inserted i ON r.ID = i.ID 
			-- Row type
			INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
			SELECT	i.TableRowID, 149778, /*Claim Row Type*/
					CASE 
						WHEN r.Type = 0 THEN 50539 -- No cover
						WHEN r.Type = 1 THEN 50537 -- Standard
						ELSE 50538 -- Co-Ins
					END,
					@LeadID, @MatterID, @ClientID
			FROM @RowsToInsert r
			INNER JOIN @Inserted i ON r.ID = i.ID 
	
			SELECT @LogEntry = ' @RowCount = ' + ISNULL(CONVERT(VARCHAR,@RowCount),'NULL')
							 + ',@ExistingRowCount = ' + ISNULL(CONVERT(VARCHAR,@ExistingRowCount),'NULL')
							 
			EXEC _C00_LogIt 'Info', '_C00_1272_Policy_SavePolicySections', 'Pre-Save Parameters', @LogEntry, 0

			-- Claim amount - if there is a single row and no existing rows then we can default this amount
			IF @RowCount = 1 AND @ExistingRowCount = 0
			BEGIN

				DECLARE @ClaimAmount MONEY,
						@TableRowID INT,
						@Deductions dbo.tvpIntVarcharVarchar
					
				SELECT @ClaimAmount = ValueMoney
				FROM dbo.MatterDetailValues WITH (NOLOCK) 
				WHERE MatterID = @MatterID
				AND DetailFieldID = 149850 /*Total Claim Amount*/
				 
				SELECT @TableRowID = TableRowID
				FROM @Inserted	

				EXEC _C00_1272_Claim_SaveClaimDetailsRow  @MatterID, @TableRowID, @ClaimAmount, @Deductions
			
			END 
						
		END
		
	END

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_SavePolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_SavePolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_SavePolicySections] TO [sp_executeall]
GO
