SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-01-31
-- Description:	Gets the pivoted policy sections for the user to select
-- Used by:		DF 159267
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetCurrentPolicySections] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SchemeID INT,
			@Now DATE,
			@CurrentPolicyMatterID INT
	
	-- Look up the scheme that is defined on the lead / policy 
	DECLARE @MatterID INT
	SELECT TOP 1 @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	SELECT @SchemeID = CurrentPolicyID
	FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
	
	SELECT @Now = dbo.fn_GetDate_Local()	
	
	
	-- Match this scheme against the correct matter on the Affinity lead type
	SELECT @CurrentPolicyMatterID = mdvScheme.MatterID 
	FROM  dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 145689 AND mdvScheme.ValueInt = @SchemeID
	
	-- Removed these to just get the last matter of this type as for PDSA we were not matching a scheme when the scheme ended before today
	--INNER JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 145690 AND mdvStart.ValueDate <= @Now
	--INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 145691 AND (mdvEnd.ValueDate >= @Now OR mdvEnd.ValueDate IS NULL) 
	
	
	SELECT tdvRl.ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRl WITH (NOLOCK) ON r.TableRowID = tdvRl.TableRowID AND tdvRl.DetailFieldID = 144357
	INNER JOIN dbo.ResourceListDetailValues rlSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSection.ResourceListID AND rlSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rlSection.ValueInt = llSection.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rlSubSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSubSection.ResourceListID AND rlSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rlSubSection.ValueInt = llSubSection.LookupListItemID 
	WHERE r.MatterID = @CurrentPolicyMatterID
	ORDER BY CASE WHEN llSection.LookupListItemID = 42878 THEN 0 ELSE 1 END, llSection.ItemValue, llSubSection.ItemValue


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetCurrentPolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetCurrentPolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetCurrentPolicySections] TO [sp_executeall]
GO
