SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalOption_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalOptionID int   = null ,

	@ClientID int   = null ,

	@ShowHelpLink bit   = null ,

	@HelpLinkCaption varchar (250)  = null ,

	@HelpLinkURL varchar (500)  = null ,

	@LeadListShowAssignedTo bit   = null ,

	@LeadListShowAssignedToCaption varchar (50)  = null ,

	@LeadListShowCaseStatus bit   = null ,

	@LeadListShowCaseStatusCaption varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalOptionID]
	, [ClientID]
	, [ShowHelpLink]
	, [HelpLinkCaption]
	, [HelpLinkURL]
	, [LeadListShowAssignedTo]
	, [LeadListShowAssignedToCaption]
	, [LeadListShowCaseStatus]
	, [LeadListShowCaseStatusCaption]
    FROM
	[dbo].[PortalOption] WITH (NOLOCK) 
    WHERE 
	 ([PortalOptionID] = @PortalOptionID OR @PortalOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ShowHelpLink] = @ShowHelpLink OR @ShowHelpLink IS NULL)
	AND ([HelpLinkCaption] = @HelpLinkCaption OR @HelpLinkCaption IS NULL)
	AND ([HelpLinkURL] = @HelpLinkURL OR @HelpLinkURL IS NULL)
	AND ([LeadListShowAssignedTo] = @LeadListShowAssignedTo OR @LeadListShowAssignedTo IS NULL)
	AND ([LeadListShowAssignedToCaption] = @LeadListShowAssignedToCaption OR @LeadListShowAssignedToCaption IS NULL)
	AND ([LeadListShowCaseStatus] = @LeadListShowCaseStatus OR @LeadListShowCaseStatus IS NULL)
	AND ([LeadListShowCaseStatusCaption] = @LeadListShowCaseStatusCaption OR @LeadListShowCaseStatusCaption IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalOptionID]
	, [ClientID]
	, [ShowHelpLink]
	, [HelpLinkCaption]
	, [HelpLinkURL]
	, [LeadListShowAssignedTo]
	, [LeadListShowAssignedToCaption]
	, [LeadListShowCaseStatus]
	, [LeadListShowCaseStatusCaption]
    FROM
	[dbo].[PortalOption] WITH (NOLOCK) 
    WHERE 
	 ([PortalOptionID] = @PortalOptionID AND @PortalOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ShowHelpLink] = @ShowHelpLink AND @ShowHelpLink is not null)
	OR ([HelpLinkCaption] = @HelpLinkCaption AND @HelpLinkCaption is not null)
	OR ([HelpLinkURL] = @HelpLinkURL AND @HelpLinkURL is not null)
	OR ([LeadListShowAssignedTo] = @LeadListShowAssignedTo AND @LeadListShowAssignedTo is not null)
	OR ([LeadListShowAssignedToCaption] = @LeadListShowAssignedToCaption AND @LeadListShowAssignedToCaption is not null)
	OR ([LeadListShowCaseStatus] = @LeadListShowCaseStatus AND @LeadListShowCaseStatus is not null)
	OR ([LeadListShowCaseStatusCaption] = @LeadListShowCaseStatusCaption AND @LeadListShowCaseStatusCaption is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Find] TO [sp_executeall]
GO
