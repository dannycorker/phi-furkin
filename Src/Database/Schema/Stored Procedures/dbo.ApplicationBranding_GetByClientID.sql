SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ApplicationBranding table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ApplicationBranding_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[ApplicationBrandingID],
					[ClientID],
					[FileName],
					[VerticalSize],
					[HorizontalSize],
					[LeadManagerAlias],
					[LeadAlias],
					[LeadDetailsTabImageFileName]
				FROM
					[dbo].[ApplicationBranding] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ApplicationBranding_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_GetByClientID] TO [sp_executeall]
GO
