SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DetailFieldLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_Update]
(

	@DetailFieldLinkID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@MasterQuestionID int   ,

	@LeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DetailFieldLink]
				SET
					[ClientID] = @ClientID
					,[DetailFieldID] = @DetailFieldID
					,[MasterQuestionID] = @MasterQuestionID
					,[LeadTypeID] = @LeadTypeID
				WHERE
[DetailFieldLinkID] = @DetailFieldLinkID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Update] TO [sp_executeall]
GO
