SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Chart table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_GetByLastEditedBy]
(

	@LastEditedBy int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChartID],
					[QueryID],
					[ClientID],
					[ChartTitle],
					[ChartDescription],
					[ChartTypeID],
					[XAxisColumn],
					[CreatedBy],
					[CreatedOn],
					[LastEditedBy],
					[LastEditedOn]
				FROM
					[dbo].[Chart] WITH (NOLOCK) 
				WHERE
					[LastEditedBy] = @LastEditedBy
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_GetByLastEditedBy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_GetByLastEditedBy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_GetByLastEditedBy] TO [sp_executeall]
GO
