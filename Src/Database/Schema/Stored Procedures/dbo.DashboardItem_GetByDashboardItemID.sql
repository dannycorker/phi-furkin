SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DashboardItem table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItem_GetByDashboardItemID]
(

	@DashboardItemID int   
)
AS


				SELECT
					[DashboardItemID],
					[DashboardID],
					[DashboardItemTypeID],
					[ItemID],
					[ClientID],
					[LayoutPosition]
				FROM
					[dbo].[DashboardItem] WITH (NOLOCK) 
				WHERE
										[DashboardItemID] = @DashboardItemID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_GetByDashboardItemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItem_GetByDashboardItemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_GetByDashboardItemID] TO [sp_executeall]
GO
