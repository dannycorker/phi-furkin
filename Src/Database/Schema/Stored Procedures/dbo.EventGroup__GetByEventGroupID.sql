SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-11-2012
-- Description:	Gets an event group by its identity
-- =============================================
CREATE PROCEDURE [dbo].[EventGroup__GetByEventGroupID]

	@EventGroupID INT,
	@ClientID INT

AS
BEGIN

	SELECT * FROM EventGroup WITH (NOLOCK) 
	WHERE EventGroupID=@EventGroupID AND ClientID=@ClientID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__GetByEventGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroup__GetByEventGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__GetByEventGroupID] TO [sp_executeall]
GO
