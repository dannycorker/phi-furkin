SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudicationFieldType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeName]
(

	@AutoAdjudicationFieldTypeName varchar (50)  
)
AS


				SELECT
					[AutoAdjudicationFieldTypeID],
					[AutoAdjudicationFieldTypeName]
				FROM
					[dbo].[AutoAdjudicationFieldType] WITH (NOLOCK) 
				WHERE
										[AutoAdjudicationFieldTypeName] = @AutoAdjudicationFieldTypeName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeName] TO [sp_executeall]
GO
