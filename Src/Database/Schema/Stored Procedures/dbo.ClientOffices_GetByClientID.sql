SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOffices table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[ClientOfficeID],
					[ClientID],
					[OfficeName],
					[BuildingName],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Country],
					[PostCode],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[OfficeFax],
					[CountryID],
					[LanguageID],
					[SubClientID],
					[AdminClientPersonnelID],
					[CurrencyID]
				FROM
					[dbo].[ClientOffices] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_GetByClientID] TO [sp_executeall]
GO
