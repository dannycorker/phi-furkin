SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 28/08/2016
-- Description:	Creates Account Record
-- Mods
--  2016-10-31 DCM Protected against expiry date not being a real date
--	2019-11-14 GPR Added switch for AccountType4 Premium Funding for LPC-54
--  2020-04-02 JEL Capture Customer first and last name to account record
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BillingSystem_CreateAccountRecord]
(
	@MatterID INT,
	@WhoCreated INT,
	@UpdateAllUnpaid BIT = 1 
)
AS
BEGIN

--declare @matterID int = 379
	SET NOCOUNT ON;

	DECLARE 
			@AccountName VARCHAR(100),
			@Accounts tvpIntInt,
			@AccNumber VARCHAR(100),
			@AccSortCode VARCHAR(10),
			@AccountID INT,
			@AccountTypeID INT,
			@CardToken VARCHAR(500),
			@CaseID INT,
			@ClientID INT,
			@CustomerID INT,
			@ExpiryDate VARCHAR(10),
			@ExpiryMonth INT,
			@ExpiryYear INT,
			@FriendlyName VARCHAR(500),
			@OldAccountID INT,
			@MaskedCardNumber VARCHAR(50),
			@PurchasedProductID INT,
			@Reference VARCHAR(200),
			@WhenCreated DATETIME ,
			@ClientAccountID INT ,
			@FirstName VARCHAR(200),
			@LastName VARCHAR(200)

	SELECT @CaseID=CaseID, @ClientID=ClientID, @CustomerID=CustomerID, @MatterID=MatterID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID=dbo.fn_C600_GetCollectionsMatterID(@MatterID)
	
	/*Pick up the affinity specific client account ID to write to the customers account record*/ 
	SELECT @ClientAccountID = rldv.ValueInt 
	FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
	INNER JOIN Matter m WITH ( NOLOCK ) on m.CustomerID = cdv.CustomerID 
	INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt
	WHERE m.MatterID = @MatterID 
	AND cdv.DetailFieldID = 170144
	AND rldv.DetailFieldID = 179949
	
	SELECT @AccountTypeID = CASE dbo.fnGetDv(170115,@CaseID)
								WHEN 69930 THEN 1 -- bank
								WHEN 69931 THEN 2 -- card
								WHEN 76618 THEN 4 -- premium funding /*GPR 2019-11-14 for LPC-54*/
								WHEN 193355 THEN 2 -- if BrainTree then be card... TREE OF BRAINS 
								WHEN 193365 THEN 2 /*Apple pay*/
								WHEN 193366 THEN 2 /*PayPal*/
								END
	/*GPR 2019-11-14 added switch for AccountTypeID 4 premium funding for LPC-54*/							
	SELECT	@AccountName = CASE WHEN @AccountTypeID IN (1,4) THEN dbo.fnGetDV(170188,@CaseID) ELSE dbo.fnGetDV(170192,@CaseID) END,
			@AccNumber = CASE WHEN @AccountTypeID IN (1,4) THEN dbo.fnGetDV(170190,@CaseID) ELSE NULL END,
			@AccSortCode = CASE WHEN @AccountTypeID IN (1,4) THEN dbo.fnGetDV(170189,@CaseID) ELSE NULL END,
			@FriendlyName = dbo.fnGetDV(170110,@CaseID),  -- letter full name
			@Reference = dbo.fn_C600_GetMandateReference(@MatterID),
			@CardToken = dbo.fnGetDV(170191,@CaseID),
			@MaskedCardNumber = CASE WHEN @AccountTypeID IN (1,4) THEN NULL ELSE dbo.fnGetDV(170193,@CaseID) END,
			@ExpiryDate = CASE WHEN @AccountTypeID IN (1,4) THEN NULL ELSE dbo.fnGetDV(170194,@CaseID) END
	
	/*JEL 2020-04-02 Added customer name tracking*/ 
	SELECT @FirstName = c.FirstName, @LastName = c.LastName 
	FROM Customers c WITH (NOLOCK)
	where c.CustomerID = @CustomerID

	IF @ExpiryDate IS NOT NULL AND EXISTS (SELECT * FROM ClientPaymentGateway c with (NOLOCK) WHERE c.PaymentGatewayID IN (52,55) and c.ClientID = @ClientID)
	BEGIN
	
		-- is expiry date passed in year
		IF LEFT(@ExpiryDate,4) < '2000'
		BEGIN

			SELECT @ExpiryMonth=LEFT(@ExpiryDate,2), @ExpiryYear=RIGHT(LEFT(@ExpiryDate,4),2)

			SELECT @ExpiryDate = '20' + CAST(@ExpiryYear as VARCHAR) + '-' + CAST(@ExpiryMonth as VARCHAR) + '-' + '01'
			
		END		
		ELSE -- expiry date passed as year & month of date 
		BEGIN
		
			SELECT @ExpiryMonth=DATEPART(MONTH,@ExpiryDate), @ExpiryYear=DATEPART(YYYY,@ExpiryDate)	
			
			IF @ClientID <> 607
			BEGIN

				SELECT @ExpiryDate=NULL

			END

		END	
		
		
	
	END	
	-- does an account ID already exists? 
	-- if it does then we are changing so do backup & after creating the new account update on all uncollected payments
	SELECT @OldAccountID=dbo.fnGetDVasInt(176973,@CaseID)

	-- backup to account history
	IF @OldAccountID <> 0 
	BEGIN
		EXEC dbo.AccountHistory__Backup @OldAccountID, @WhoCreated
		UPDATE Account SET Active=0 WHERE AccountID=@OldAccountID
	END


	-- Create a new account record in the billing system
	INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference, CardToken, MaskedCardNumber, ExpiryMonth, ExpiryYear, ExpiryDate, AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified, ObjectID, ObjectTypeID, ClientAccountID, FirstName, LastName)
	SELECT @ClientID,
		   @CustomerID,
		   @AccountName,
		   @FriendlyName,
		   @AccNumber,
		   @AccSortCode,
		   @Reference,
		   @CardToken,
		   @MaskedCardNumber,
		   @ExpiryMonth,
		   @ExpiryYear,
		   @ExpiryDate,
		   @AccountTypeID,
		   1, -- Active
		   @WhoCreated,  -- who created 
		   dbo.fn_GetDate_Local(),
		   @WhoCreated, -- who modified
		   dbo.fn_GetDate_Local(),
		   @MatterID,
		   2,
		   @ClientAccountID,
		   @FirstName,
		   @LastName
					   
	SELECT @AccountID = SCOPE_IDENTITY()
	
	EXEC dbo._C00_SimpleValueIntoField 176973, @AccountID, @MatterID, @WhoCreated
	
	-- change account number on uncollected rows		
	IF @OldAccountID <> 0 
	BEGIN

		INSERT INTO @Accounts 
		SELECT pp.PurchasedProductID,0 
		FROM PurchasedProduct pp WITH (NOLOCK) 
		WHERE pp.AccountID=@OldAccountID
		
		WHILE EXISTS ( SELECT * FROM @Accounts WHERE ID2=0 )
		BEGIN
			
			
			SELECT TOP 1 @PurchasedProductID=ID1 FROM @Accounts WHERE ID2=0
		
			EXEC dbo.PurchasedProductHistory__Backup @PurchasedProductID, @WhoCreated
			EXEC dbo.PurchasedProductPaymentScheduleHistory__Backup @PurchasedProductID, @WhoCreated, 0
			
			UPDATE @Accounts SET ID2=1 WHERE ID1=@PurchasedProductID
		
		END
		
		IF ISNULL(@UpdateAllUnpaid,1) = 1 /*Only do this is we need to update all upaid to new account*/ 
		BEGIN
			UPDATE pp 
			SET AccountID=@AccountID,
			WhoModified=@WhoCreated,
			WhenModified=dbo.fn_GetDate_Local()
			FROM PurchasedProduct pp
			INNER JOIN @Accounts a ON pp.PurchasedProductID=a.ID1
			
			UPDATE pp 
			SET AccountID=@AccountID
			FROM PurchasedProductPaymentSchedule pp
			INNER JOIN @Accounts a ON pp.PurchasedProductID=a.ID1
			WHERE pp.PaymentStatusID IN (1,5)

			EXEC dbo.CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 0

			UPDATE CustomerPaymentSchedule
			SET AccountID=@AccountID
			WHERE CustomerID=@CustomerID
				AND AccountID=@OldAccountID
				AND PaymentStatusID IN (1,5)
		END
		
		EXEC dbo.Account__SetDateAndAmountOfNextPayment @AccountID
		
	END	

	RETURN @AccountID 

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BillingSystem_CreateAccountRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BillingSystem_CreateAccountRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BillingSystem_CreateAccountRecord] TO [sp_executeall]
GO
