SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-10-2012
-- Description:	Gets the ThirdPartyMappingUserCredential
-- =============================================
CREATE PROCEDURE [dbo].[Login_GetThirdPartyMappingUserCredential]
	@User VARCHAR(2000),
	@Pass VARCHAR(2000),
	@Domain VARCHAR(2000),
	@AppInfo VARCHAR(50) = NULL,
	@ThirdPartySystemID INT = NULL,
	@UserIPDetails VARCHAR(50) = NULL
AS
BEGIN

	/*
		Scenarios:

		1) User not found
		2) Password not valid
		3) Account disabled
		4) User cannot login like this (eg not from the App)

		There are some that we can't cater for here though:
		a) User can only login from restricted IP Addresses
		b) Too many failed logins
	*/
	DECLARE @ClientID INT, 
	@ClientPersonnelID INT,
	@AttemptedLogins INT,
	@AccountDisabled BIT,
	@StoredPassword VARCHAR(65), 
	@IsSuccess BIT = 1,
	@Notes VARCHAR(2000) = 'Passed to the app for verification'

	SELECT @ClientID = ClientID, 
	@ClientPersonnelID = UserID,
	@AttemptedLogins = AttemptedLogins,
	@AccountDisabled = AccountDisabled,
	@StoredPassword = [Password] 
	FROM ThirdPartyMappingUserCredential WITH (NOLOCK) 
	WHERE ThirdPartyMappingUserCredential.UserName= @User
	AND ThirdPartySystemId = @ThirdPartySystemID

	/*
		1) If all NULL at this point, then user not found
	*/
	IF @ClientPersonnelID IS NULL
	BEGIN
		SELECT @IsSuccess = 0, @Notes = 'Third Party User not found'
	END
	ELSE
	BEGIN
		/*
			2) If password doesn't match then FAIL
		*/
		IF ((@StoredPassword IS NULL) OR (@Pass IS NULL) OR (@StoredPassword <> @Pass))
		BEGIN
			SELECT @IsSuccess = 0, @Notes = 'Third Party Password Incorrect'
		END
		ELSE
		BEGIN

			/*
				3) If account disabled then FAIL
			*/
			IF @AccountDisabled = 1
			BEGIN
				SELECT @IsSuccess = 0, @Notes = 'Third Party Account Disabled'
			END

			IF @AttemptedLogins >= 3
			BEGIN
				SELECT @IsSuccess = 0, @Notes = 'Exceeded Attempted Logins'
			END
		END

	END

	/*
		Log the results
	*/
	INSERT INTO dbo.LoginHistory (
		UserName, 
		LoginDateTime, 
		IsSuccess, 
		ClientID, 
		ClientPersonnelID, 
		AppLogin, 
		AttemptedLogins, 
		AccountDisabled, 
		Notes,
		ThirdPartySystemID,
		HostName,
		UserIPDetails
	) 
	VALUES 
	( 
		@User, 
		dbo.fn_GetDate_Local(), 
		@IsSuccess, 
		@ClientID, 
		@ClientPersonnelID, 
		0, 
		@AttemptedLogins, 
		@AccountDisabled, 
		@Notes,
		@ThirdPartySystemID,			
		HOST_NAME(),
		@UserIPDetails
	)	


	IF @IsSuccess=1
	BEGIN
	
		SELECT * FROM ThirdPartyMappingUserCredential c WITH (NOLOCK) 
		WHERE c.ThirdPartySystemID = @ThirdPartySystemID AND 
			  c.UserName = @User AND 
			  c.Password = @Pass AND 
			  c.Domain = @Domain

	END 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Login_GetThirdPartyMappingUserCredential] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Login_GetThirdPartyMappingUserCredential] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Login_GetThirdPartyMappingUserCredential] TO [sp_executeall]
GO
