SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientCustomerMails table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientCustomerMails_Update]
(

	@ClientCustomerMailID int   ,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@ClientAreaMailingListID int   ,

	@DateSent datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientCustomerMails]
				SET
					[ClientID] = @ClientID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[CustomerID] = @CustomerID
					,[ClientAreaMailingListID] = @ClientAreaMailingListID
					,[DateSent] = @DateSent
				WHERE
[ClientCustomerMailID] = @ClientCustomerMailID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientCustomerMails_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Update] TO [sp_executeall]
GO
