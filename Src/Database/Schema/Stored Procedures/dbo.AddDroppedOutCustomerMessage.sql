SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddDroppedOutCustomerMessage]
@ClientID int,
@ClientQuestionnaireID int,
@EmailFromAddress nvarchar(255),
@EmailSubject nvarchar(500),
@Email text,
@SmsMessage text

as

insert into DroppedOutCustomerMessages(ClientID, ClientQuestionnaireID, EmailFromAddress, EmailSubject, Email, SmsMessage)
values (@ClientID, @ClientQuestionnaireID, @EmailFromAddress, @EmailSubject, @Email, @SmsMessage)

declare @DroppedOutCustomerMessageID int

set @DroppedOutCustomerMessageID = SCOPE_IDENTITY()

Select DroppedOutCustomerMessageID from DroppedOutCustomerMessages where DroppedOutCustomerMessageID = @DroppedOutCustomerMessageID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomerMessage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddDroppedOutCustomerMessage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomerMessage] TO [sp_executeall]
GO
