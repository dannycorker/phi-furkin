SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-07-31
-- Description:	Create Table row in correct place based on DDL value
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISI_CreateDestinationCreditor]
	@TableRowID INT,
	@CustomerID INT,
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ConfigRow1ID INT,
			@ConfigRow2ID INT,
			@NewTableRowID INT,
			@DetailFieldID INT,
			@LookupListItemID INT,
			@LookupListItemFieldID INT,
			@LogMessage VARCHAR(2000)
	
	/*
		Get the field that the table sits on so we can look up it's 
		configuration row.
	*/
	SELECT @DetailFieldID = tr.DetailFieldID 
	FROM TableRows tr WITH (NOLOCK)
	WHERE tr.TableRowID = @TableRowID
	
	SELECT @ConfigRow1ID = tdv.TableRowID
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.DetailFieldID = 220715
	AND tdv.ValueInt = @DetailFieldID
	
	IF @ConfigRow1ID > 0
	BEGIN
	
		/*Get the field ID of the dropdown that will drive this SP.*/
		SELECT @DetailFieldID = tdv.DetailFieldID
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.DetailFieldID = 220742
		AND tdv.TableRowID = @ConfigRow1ID
		
		SELECT @LookupListItemFieldID = tdv.ValueInt
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.DetailFieldID = @DetailFieldID 
		AND tdv.TableRowID = @ConfigRow1ID
		
		SELECT @LookupListItemID = tdv.ValueInt
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.DetailFieldID = @LookupListItemFieldID
		AND tdv.TableRowID = @TableRowID
		
		SELECT @ConfigRow2ID = tdv.TableRowID
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.DetailFieldID = 220680
		AND tdv.ValueInt = @LookupListItemID

		SELECT @LogMessage = 'Table Row ID: ' + CONVERT(VARCHAR,@TableRowID) + ' CustomerID: '  + CONVERT(VARCHAR,@CustomerID) + ' ClientID: '  + CONVERT(VARCHAR,@ClientID) + ' DetailFieldID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' ConfigRowID: ' + CONVERT(VARCHAR,@ConfigRow1ID) + ' ConfigRow2ID: ' + CONVERT(VARCHAR,ISNULL(@ConfigRow2ID,0)) + ' DetailFieldID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' LookupListItemID: ' + CONVERT(VARCHAR,ISNULL(@LookupListItemID,0))

		EXEC dbo._C00_LogIt 'Debug', 'ISI', '_C00_ISI_CreateDestinationCreditor', @LogMessage, 0
	
		IF @ConfigRow2ID > 0
		BEGIN
		
			/*So we have a destination table, lets go ahead and create the destination row.*/
			INSERT INTO TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, SourceID)
			VALUES (@ClientID, dbo.fnGetSimpleDvAsInt(220681, @ConfigRow2ID), dbo.fnGetSimpleDvAsInt(220682, @ConfigRow2ID), 1, 1, @CustomerID, @TableRowID)
			
			SELECT @NewTableRowID = SCOPE_IDENTITY()
			
			/*Do creditor Name if applicable*/
			IF dbo.fnGetSimpleDvAsInt(220683, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220683, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220716, @ConfigRow1ID),@TableRowID))
			
			END
			
			/*MonthlyRepaymentsDue*/
			IF dbo.fnGetSimpleDvAsInt(220706, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220706, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220719, @ConfigRow1ID),@TableRowID))
			
			END
			
			/*MonthlyRepaymentsBeingMade*/
			IF dbo.fnGetSimpleDvAsInt(220707, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220707, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220720, @ConfigRow1ID),@TableRowID))
			
			END
			
			/*RemainingTermInMonths*/
			IF dbo.fnGetSimpleDvAsInt(220708, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220708, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220721, @ConfigRow1ID),@TableRowID))
			
			END

			/*CurrentBalance*/
			IF dbo.fnGetSimpleDvAsInt(220709, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220709, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220722, @ConfigRow1ID),@TableRowID))
			
			END

			/*TotalArrears*/
			IF dbo.fnGetSimpleDvAsInt(220711, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220711, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220723, @ConfigRow1ID),@TableRowID))
			
			END

			/*IsTheDebtSecured*/
			IF dbo.fnGetSimpleDvAsInt(220712, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220712, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220724, @ConfigRow1ID),@TableRowID))
			
			END

			/*HasTheDebtBeenRestructed*/
			IF dbo.fnGetSimpleDvAsInt(220713, @ConfigRow2ID) > 0
			BEGIN
			
				INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @CustomerID, @NewTableRowID, dbo.fnGetSimpleDvAsInt(220713, @ConfigRow2ID), dbo.fnGetSimpleDv(dbo.fnGetSimpleDvAsInt(220725, @ConfigRow1ID),@TableRowID))
			
			END

			/*Now finally deny editing or deleting of the old row.*/
			UPDATE TableRows
			SET DenyDelete = 1, DenyEdit = 1
			WHERE TableRowID = @TableRowID
		
		END
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_CreateDestinationCreditor] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISI_CreateDestinationCreditor] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_CreateDestinationCreditor] TO [sp_executeall]
GO
