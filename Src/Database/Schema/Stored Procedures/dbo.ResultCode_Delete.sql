SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ResultCode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResultCode_Delete]
(

	@ResultCodeID varchar (4)  
)
AS


				DELETE FROM [dbo].[ResultCode] WITH (ROWLOCK) 
				WHERE
					[ResultCodeID] = @ResultCodeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResultCode_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Delete] TO [sp_executeall]
GO
