SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-11-18
-- Description:	Mod of the TableDetailValues__GetByTableRowIDAndLeadID proc to also look at matter ID
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__GetByTableRowIDAndClientID]
(

	@TableRowID int,
	@ClientID int
)
AS


				SELECT
					tdv.[TableDetailValueID],
					tdv.[TableRowID],
					tdv.[ResourceListID],
					tdv.[DetailFieldID],
					tdv.[DetailValue],
					tdv.[LeadID],
					tdv.[MatterID],
					tdv.[ClientID],
					tdv.[EncryptedValue],
					tdv.[ErrorMsg],
					tdv.[ValueInt],
					tdv.[ValueMoney],
					tdv.[ValueDate],
					tdv.[ValueDateTime],	
					tdv.[CustomerID],
					tdv.[CaseID],
					tdv.[ClientPersonnelID],
					tdv.[ContactID],
					df.[QuestionTypeID],
					df.[FieldCaption],
					CASE df.QuestionTypeID WHEN 4 THEN ll.ItemValue ELSE NULL END AS ItemValue
				FROM
					[dbo].[TableDetailValues] tdv WITH (NOLOCK) 	
					left outer join dbo.LookupListItems ll on ll.LookupListItemID = tdv.ValueInt
					INNER JOIN dbo.DetailFields df on df.DetailFieldID = tdv.DetailFieldID											
				WHERE
					tdv.[TableRowID] = @TableRowID and
					tdv.[ClientID] = @ClientID
					
			Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByTableRowIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndClientID] TO [sp_executeall]
GO
