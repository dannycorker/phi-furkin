SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType_Delete]
(

	@LeadTypeID int   
)
AS


				DELETE FROM [dbo].[LeadType] WITH (ROWLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Delete] TO [sp_executeall]
GO
