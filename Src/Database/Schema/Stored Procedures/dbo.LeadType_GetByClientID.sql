SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadTypeID],
					[ClientID],
					[LeadTypeName],
					[LeadTypeDescription],
					[Enabled],
					[Live],
					[MatterDisplayName],
					[LastReferenceInteger],
					[ReferenceValueFormatID],
					[IsBusiness],
					[LeadDisplayName],
					[CaseDisplayName],
					[CustomerDisplayName],
					[LeadDetailsTabImageFileName],
					[CustomerTabImageFileName],
					[UseEventCosts],
					[UseEventUOEs],
					[UseEventDisbursements],
					[UseEventComments],
					[AllowMatterPageChoices],
					[AllowLeadPageChoices],
					[IsRPIEnabled],
					[FollowupWorkingDaysOnly],
					[MatterLastReferenceInteger],
					[MatterReferenceValueFormatID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[CalculateAllMatters],
					[SmsGatewayID]
				FROM
					[dbo].[LeadType] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_GetByClientID] TO [sp_executeall]
GO
