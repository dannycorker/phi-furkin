SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2016-10-14
-- Description:	Searches existing customers for new quote and buy
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Customer_SearchExisting_Backup] 
(
	@BrandID INT,
	@Firstname VARCHAR(200),
	@Lastname VARCHAR(200),
	@Postcode VARCHAR(200),
	@Email VARCHAR(200),
	@Page INT = NULL,
	@PageSize INT = NULL
)

AS
BEGIN
	
	-- sort paging variables
	SELECT	@Page = ISNULL(@Page, 1),
			@PageSize = ISNULL(@PageSize, 10)
	
	DECLARE @First INT,
			@Last INT
			
	SELECT @First = ((@Page - 1) * @PageSize) + 1
	SELECT @Last = @Page * @PageSize
	
	-- Get the paged customer IDs first - ordered by last name, first name
	DECLARE @Customers TABLE
	(
		ID INT IDENTITY,
		CustomerID INT, 
		Total INT
	)
	
	;WITH
	--CustomerPets AS
	--(
	--	SELECT	l.CustomerID ,
	--			ROW_NUMBER() OVER(PARTITION BY l.CustomerID ORDER BY l.LeadID) as rn  
	--	FROM dbo.Lead l WITH (NOLOCK) 
	--	INNER JOIN dbo.LeadDetailValues ldvName WITH (NOLOCK) ON l.LeadID = ldvName.LeadID AND ldvName.DetailFieldID = 155956
	--	WHERE l.LeadTypeID = 1288
	--	AND (@Petname IS NULL OR @Petname = '' OR ldvName.DetailValue LIKE @Petname + '%')
	--),
	Customers AS 
	(
		SELECT	c.CustomerID, 
				ROW_NUMBER() OVER(ORDER BY c.LastName, c.FirstName, c.CustomerID) as rn,
				COUNT(c.CustomerID) OVER() AS total
		FROM dbo.Customers c WITH (NOLOCK) 
		LEFT JOIN dbo.Partner p WITH (NOLOCK) ON c.CustomerID = p.CustomerID
		INNER JOIN CustomerDetailValues b ON c.CustomerID = b.CustomerID AND b.DetailFieldID = 170144
		--INNER JOIN CustomerPets l ON c.CustomerID = l.CustomerID AND l.rn = 1
		WHERE c.Test = 0
		AND b.ValueInt = ISNULL(@BrandID, 134923) -- Default to PetProtect
		AND 
			(
			(@Firstname IS NULL OR @Firstname = '' OR c.FirstName LIKE @Firstname + '%') 
				AND (@Lastname IS NULL OR @Lastname = '' OR c.LastName LIKE @Lastname + '%')
			OR
			(@Firstname IS NULL OR @Firstname = '' OR p.FirstName LIKE @Firstname + '%') 
				AND (@Lastname IS NULL OR @Lastname = '' OR p.LastName LIKE @Lastname + '%')
			)
		AND (@Postcode IS NULL OR @Postcode = '' OR c.PostCode LIKE @Postcode + '%')
		AND (@Email IS NULL OR @Email = '' OR c.EmailAddress LIKE @Email + '%')
	)

	INSERT @Customers (CustomerID, Total)
	SELECT CustomerID, total
	FROM Customers
	WHERE rn BETWEEN @First AND @Last
	ORDER BY rn
	
	
	-- Now select out the customer data
	;WITH LeadCreated AS 
	(
		SELECT l.CustomerID, MIN(l.WhenCreated) AS WhenCreated
		FROM dbo.Lead l WITH (NOLOCK)
		GROUP BY l.CustomerID
	)
	
	SELECT	c.CustomerID, c.FirstName, c.LastName, c.Address1, c.Address2, c.Town AS TownCity, c.PostCode AS Postcode, 
			c.EmailAddress AS Email, c.HomeTelephone AS HomePhone, c.MobileTelephone AS Mobile, l.WhenCreated
	FROM dbo.Customers c WITH (NOLOCK) 
	INNER JOIN LeadCreated l ON c.CustomerID = l.CustomerID
	INNER JOIN @Customers ids ON c.CustomerID = ids.CustomerID
	ORDER BY ids.ID
	
	-- Right at the end we return the paging info
	DECLARE @TotalRows INT			
	SELECT TOP 1 @TotalRows = Total
	FROM @Customers
	
	SELECT ISNULL(@TotalRows, 0) AS TotalRows, @Page AS Page, @PageSize AS PageSize
	
	
	
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Customer_SearchExisting_Backup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Customer_SearchExisting_Backup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Customer_SearchExisting_Backup] TO [sp_executeall]
GO
