SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cronofy__GetAppointments]

AS
BEGIN

	SET NOCOUNT ON;

	SELECT d.DiaryAppointmentID, d.ClientID, CASE WHEN d.DiaryAppointmentTitle = '' THEN 'You need a title' ELSE d.DiaryAppointmentTitle END AS [DiaryAppointmentTitle], CASE WHEN d.DiaryAppointmentText = '' THEN 'You need an appointment text' ELSE d.DiaryAppointmentText END AS [DiaryAppointmentText], d.CreatedBy, CONVERT(VARCHAR,d.DueDate,127) + 'Z' AS [DueDate], CONVERT(VARCHAR,CASE WHEN d.EndDate < d.DueDate THEN d.DueDate ELSE d.EndDate END,127) + 'Z' AS [EndDate], d.AllDayEvent, d.Completed, d.LeadID, d.CaseID, d.CustomerID, d.Version, d.ExportVersion, d.RecurrenceInfo, d.DiaryAppointmentEventType, d.ResourceInfo, d.TempReminderTimeshiftID, d.StatusID, d.LabelID, c.CronofyTokenID, c.ClientID, c.ClientPersonnelID, c.AccessToken, c.TokenType, c.ExpiresIn, c.RefreshToken, c.Scope, c.DateTimeUpdated, DATEADD(SS,c.ExpiresIn-180,c.DateTimeUpdated) AS [RefreshTime]
	FROM DiaryAppointment d with (nolock)
	INNER JOIN CronofyTokens c WITH (NOLOCK) ON c.ClientPersonnelID = d.CreatedBy
	WHERE d.Completed = 0
	AND d.EndDate > dbo.fn_GetDate_Local()

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Cronofy__GetAppointments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cronofy__GetAppointments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cronofy__GetAppointments] TO [sp_executeall]
GO
