SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- UPDATES:		Simon Brushett, 2011-12-08, Changed to call into the CP_Insert_Helper proc as direct insert into CP no longer allowed
-- =============================================
CREATE PROCEDURE [dbo].[_C38_ClientCreate]
(
	@LeadEventID INT
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE	@CompanyName NVARCHAR(100),
	@TitleID INT,
	@FirstName NVARCHAR(100),
	@MiddleName NVARCHAR(100),
	@LastName NVARCHAR(100),
	@NewFirstName NVARCHAR(100),
	@NewLastName NVARCHAR(100),
	@JobTitle NVARCHAR(100),
	@Password NVARCHAR(100),
	@MobilePhone NVARCHAR(100),
	@OfficeTelephoneCP NVARCHAR(50),
	@OfficeTelephoneExtension NVARCHAR(100),
	@Salt NVARCHAR(100),
	@OfficeTelephone NVARCHAR(50),
	@Address1 NVARCHAR(200),
	@Address2 NVARCHAR(200),
	@Town NVARCHAR(200),
	@County NVARCHAR(50),
	@Country NVARCHAR(200),
	@PostCode NVARCHAR(10),
	@ChargeOutRate MONEY,
	@MobileTelephone NVARCHAR(50),
	@HomeTelephone NVARCHAR(50),
	@ClientID INT,
	@OfficeID INT,
	@myERROR INT,
	@LeadID INT,
	@CreatedBy INT,
	@FolderID INT,
	@SqlQueryID INT,
	@CustomersTableID INT,
	@LeadTableID INT,
	@CasesTableID INT,
	@LeadEventTableID INT,
	@WhenFollowedUpColID INT,
	@EventDeletedColID INT,
	@TestColID INT,
	@AquariumClientID INT,
	@CreateRLandT INT,
	@ClientPersonnelID INT

	SELECT
	@Address1 = Customers.Address1,
	@Address2 = Customers.Address2,
	@Town = Customers.Town,
	@County = Customers.County,
	@PostCode = Customers.PostCode,
	@CompanyName = Customers.CompanyName,
	@OfficeTelephone = Customers.DayTimeTelephoneNumber,
	@TitleID = ClientPersonnel.TitleID,
	@FirstName = ClientPersonnel.FirstName,
	@LastName = ClientPersonnel.LastName,
	@MiddleName = ClientPersonnel.MiddleName,
	@JobTitle = ClientPersonnel.JobTitle,
	@Password = ClientPersonnel.Password,
	@MobilePhone = ClientPersonnel.MobileTelephone,
	@HomeTelephone = ClientPersonnel.HomeTelephone,
	@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
	@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
	@ChargeOutRate = ClientPersonnel.ChargeOutRate,
	@Salt = ClientPersonnel.Salt,
	@LeadID = Lead.LeadID,
	@AquariumClientID = ldv.DetailValue,
	@CreateRLandT = ldv2.DetailValue
	FROM customers
	INNER JOIN lead ON lead.customerid = customers.customerid
	INNER JOIN leadevent ON lead.leadid = leadevent.leadid
	INNER JOIN ClientPersonnel ON ClientPersonnel.ClientPersonnelID = LeadEvent.WhoCreated
	LEFT JOIN LeadDetailValues ldv ON ldv.LeadID = leadevent.leadid AND ldv.DetailFieldID = 6453
	INNER JOIN LeadDetailValues ldv2 ON ldv2.LeadID = leadevent.leadid AND ldv2.DetailFieldID = 88623
	WHERE leadevent.leadeventid = @LeadEventID 

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID INT
	)

	INSERT @RequiredFields SELECT 6453 

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID INT, 
		MatterID INT 
	);

	INSERT INTO @LeadsAndMatters (LeadID, MatterID)
	SELECT DISTINCT m.LeadID, m.MatterID 
	FROM Matter m 
	INNER JOIN Lead l ON l.LeadID = m.LeadID AND l.LeadTypeID = 135 
	WHERE m.ClientID = 38

	IF (@AquariumClientID IS NULL OR @AquariumClientID = '')
	BEGIN

		/* Create all necessary LeadDetailValues records that don't already exist */
		INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		SELECT 38,lam.LeadID,rf.DetailFieldID,'' 
		FROM @LeadsAndMatters lam 
		CROSS JOIN @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WHERE ldv.LeadID = lam.LeadID AND ldv.DetailFieldID = rf.DetailFieldID) 
		
		INSERT INTO [dbo].[Clients] ([CompanyName],[WebAddress],[IPAddress],[DefaultEmailAddress],[ClientTypeID],[AllowSMS], UseEventComments, UseEventCosts, UseEventDisbursements, UseEventUOEs)
		VALUES (@CompanyName,'',NULL,'',1,0,0,0,0,0)

		SELECT @ClientID = SCOPE_IDENTITY()
		
		EXEC [AquariusMaster].dbo.AQ_Client_Insert @ClientID

		INSERT INTO [dbo].[ClientOffices] ([ClientID],[BuildingName],[Address1],[Address2],[Town],[County],[Country],[PostCode],[OfficeTelephone],[OfficeTelephoneExtension])
		VALUES (@ClientID,'',@Address1,@Address2,@Town,@County,'',@PostCode,@OfficeTelephone,'')

		SELECT @OfficeID = SCOPE_IDENTITY()

		DECLARE @UserName VARCHAR(200) = @FirstName + '.' + @LastName + CONVERT(VARCHAR, @ClientID) + '@aquarium-software.com'
		EXEC @CreatedBy = dbo.ClientPersonnel_Insert_Helper @ClientID, @FirstName, @LastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @ClientID, @JobTitle, 1, @MobileTelephone, @HomeTelephone, @OfficeTelephoneCP, @OfficeTelephoneExtension, @ChargeOutRate, @IsAquarium=1
		
		
		/*
			JWG 2010-04-20 
			Create all required ClientPersonnel records based on the Client Zero equivalents.
			Exclude any that already exist, eg if Aaran set up the new Client, don't add him in again.
		*/
		DECLARE @Count INT,
				@Index INT = 0
				
		DECLARE @ClientPersonnelToInsert TABLE 
		(
			ID INT IDENTITY,
			ClientID INT,
			OfficeID INT,
			TitleID INT,
			Firstname VARCHAR(100),
			Middlename VARCHAR(100),
			Lastname VARCHAR(100),
			JobTitle VARCHAR(100),
			Password VARCHAR(65),
			MobileTelephone VARCHAR(50),
			HomeTelephone VARCHAR(50),
			OfficeTelephone VARCHAR(50),
			OfficeTelephoneExtension VARCHAR(50),
			UserName VARCHAR(200),
			ChargeOutRate MONEY,
			Salt VARCHAR(50)
		)
		
		--[MobileTelephone],[HomeTelephone],[OfficeTelephone],[OfficeTelephoneExtension],[EmailAddress],[ChargeOutRate],[Salt],[AttemptedLogins],[AccountDisabled],[ManagerID])
		
		INSERT INTO @ClientPersonnelToInsert (ClientID, OfficeID, TitleID, Firstname, Lastname, Middlename, JobTitle, Password, MobileTelephone, HomeTelephone, OfficeTelephone, 
					OfficeTelephoneExtension, UserName, ChargeOutRate, Salt)
		SELECT @ClientID,@OfficeID,cp.TitleID,cp.FirstName,cp.LastName, @ClientID,cp.JobTitle,cp.Password,cp.MobileTelephone,cp.HomeTelephone,cp.OfficeTelephone,cp.OfficeTelephoneExtension,
				cp.FirstName + '.' + cp.LastName + CONVERT(VARCHAR,@ClientID) + '@aquarium-software.com',cp.ChargeOutRate,cp.Salt
		FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
		WHERE cp.ClientID = 0 
		AND cp.AccountDisabled = 0	
		AND cp.FirstName <> 'Aquarium'
		AND NOT (cp.FirstName = @FirstName AND cp.LastName = @LastName)
		
		SELECT @Count = COUNT(*)
		FROM @ClientPersonnelToInsert
		
		WHILE @Index < @Count
		BEGIN
		
			SELECT @Index += 1
			
			SELECT	@ClientID = ClientID,
					@OfficeID = OfficeID,
					@TitleID = TitleID,
					@Firstname = Firstname,
					@MiddleName = Middlename,
					@LastName = Lastname,
					@JobTitle = JobTitle,
					@Password = Password,
					@MobileTelephone = MobileTelephone,
					@HomeTelephone = HomeTelephone,
					@OfficeTelephone = OfficeTelephone,
					@OfficeTelephoneExtension = OfficeTelephoneExtension,
					@UserName = UserName,
					@ChargeOutRate = ChargeOutRate,
					@Salt = Salt
					
			FROM  @ClientPersonnelToInsert
			WHERE ID = @Index
			
			EXEC dbo.ClientPersonnel_Insert_Helper @ClientID, @FirstName, @LastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @MiddleName, @JobTitle, 1, @MobileTelephone, @HomeTelephone, @OfficeTelephone, @OfficeTelephoneExtension, @ChargeOutRate, @IsAquarium=1
		
		END

		/*If @FirstName <> 'Alex' and @LastName <> 'Elger'
		Begin

			Select 
			@TitleID = ClientPersonnel.TitleID,
			@NewFirstName = ClientPersonnel.FirstName,
			@NewLastName = ClientPersonnel.LastName,
			@JobTitle = ClientPersonnel.JobTitle,
			@Password = ClientPersonnel.Password,
			@MobilePhone = ClientPersonnel.MobileTelephone,
			@HomeTelephone = ClientPersonnel.HomeTelephone,
			@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
			@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
			@ChargeOutRate = ClientPersonnel.ChargeOutRate,
			@Salt = ClientPersonnel.Salt
			From ClientPersonnel 
			Where ClientPersonnel.ClientPersonnelID = 315

			INSERT INTO [dbo].[ClientPersonnel]([ClientID],[ClientOfficeID],[TitleID],[FirstName],[MiddleName],[LastName],[JobTitle],[Password],[ClientPersonnelAdminGroupID],[MobileTelephone],[HomeTelephone],[OfficeTelephone],[OfficeTelephoneExtension],[EmailAddress],[ChargeOutRate],[Salt],[AttemptedLogins],[AccountDisabled],[ManagerID])
			VALUES(@ClientID,@OfficeID,@TitleID,@NewFirstName,@ClientID,@NewLastName,@JobTitle,@Password,1,@MobileTelephone,@HomeTelephone,@OfficeTelephoneCP,@OfficeTelephoneExtension,@NewFirstName + '.' + @NewLastName + convert(varchar,@ClientID) + '@aquarium-software.com',@ChargeOutRate,@Salt,0,0,null)

			Select @CreatedBy = SCOPE_IDENTITY()

		END

		If @FirstName <> 'Jim' and @LastName <> 'Green'
		Begin

			Select 
			@TitleID = ClientPersonnel.TitleID,
			@NewFirstName = ClientPersonnel.FirstName,
			@NewLastName = ClientPersonnel.LastName,
			@JobTitle = ClientPersonnel.JobTitle,
			@Password = ClientPersonnel.Password,
			@MobilePhone = ClientPersonnel.MobileTelephone,
			@HomeTelephone = ClientPersonnel.HomeTelephone,
			@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
			@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
			@ChargeOutRate = ClientPersonnel.ChargeOutRate,
			@Salt = ClientPersonnel.Salt
			From ClientPersonnel 
			Where ClientPersonnel.ClientPersonnelID = 850

			INSERT INTO [dbo].[ClientPersonnel]([ClientID],[ClientOfficeID],[TitleID],[FirstName],[MiddleName],[LastName],[JobTitle],[Password],[ClientPersonnelAdminGroupID],[MobileTelephone],[HomeTelephone],[OfficeTelephone],[OfficeTelephoneExtension],[EmailAddress],[ChargeOutRate],[Salt],[AttemptedLogins],[AccountDisabled],[ManagerID])
			VALUES(@ClientID,@OfficeID,@TitleID,@NewFirstName,@ClientID,@NewLastName,@JobTitle,@Password,1,@MobileTelephone,@HomeTelephone,@OfficeTelephoneCP,@OfficeTelephoneExtension,@NewFirstName + '.' + @NewLastName + convert(varchar,@ClientID) + '@aquarium-software.com',@ChargeOutRate,@Salt,0,0,null)

		END

		If @FirstName <> 'Chris' and @LastName <> 'Townsend'
		Begin

			Select 
			@TitleID = ClientPersonnel.TitleID,
			@NewFirstName = ClientPersonnel.FirstName,
			@NewLastName = ClientPersonnel.LastName,
			@JobTitle = ClientPersonnel.JobTitle,
			@Password = ClientPersonnel.Password,
			@MobilePhone = ClientPersonnel.MobileTelephone,
			@HomeTelephone = ClientPersonnel.HomeTelephone,
			@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
			@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
			@ChargeOutRate = ClientPersonnel.ChargeOutRate,
			@Salt = ClientPersonnel.Salt
			From ClientPersonnel 
			Where ClientPersonnel.ClientPersonnelID = 2268

			INSERT INTO [dbo].[ClientPersonnel]([ClientID],[ClientOfficeID],[TitleID],[FirstName],[MiddleName],[LastName],[JobTitle],[Password],[ClientPersonnelAdminGroupID],[MobileTelephone],[HomeTelephone],[OfficeTelephone],[OfficeTelephoneExtension],[EmailAddress],[ChargeOutRate],[Salt],[AttemptedLogins],[AccountDisabled],[ManagerID])
			VALUES(@ClientID,@OfficeID,@TitleID,@NewFirstName,@ClientID,@NewLastName,@JobTitle,@Password,1,@MobileTelephone,@HomeTelephone,@OfficeTelephoneCP,@OfficeTelephoneExtension,@NewFirstName + '.' + @NewLastName + convert(varchar,@ClientID) + '@aquarium-software.com',@ChargeOutRate,@Salt,0,0,null)

		END

		If @FirstName <> 'Aaran' and @LastName <> 'GraveStock'
		Begin

			Select 
			@TitleID = ClientPersonnel.TitleID,
			@NewFirstName = ClientPersonnel.FirstName,
			@NewLastName = ClientPersonnel.LastName,
			@JobTitle = ClientPersonnel.JobTitle,
			@Password = ClientPersonnel.Password,
			@MobilePhone = ClientPersonnel.MobileTelephone,
			@HomeTelephone = ClientPersonnel.HomeTelephone,
			@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
			@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
			@ChargeOutRate = ClientPersonnel.ChargeOutRate,
			@Salt = ClientPersonnel.Salt
			From ClientPersonnel 
			Where ClientPersonnel.ClientPersonnelID = 1057

			INSERT INTO [dbo].[ClientPersonnel]([ClientID],[ClientOfficeID],[TitleID],[FirstName],[MiddleName],[LastName],[JobTitle],[Password],[ClientPersonnelAdminGroupID],[MobileTelephone],[HomeTelephone],[OfficeTelephone],[OfficeTelephoneExtension],[EmailAddress],[ChargeOutRate],[Salt],[AttemptedLogins],[AccountDisabled],[ManagerID])
			VALUES(@ClientID,@OfficeID,@TitleID,@NewFirstName,@ClientID,@NewLastName,@JobTitle,@Password,1,@MobileTelephone,@HomeTelephone,@OfficeTelephoneCP,@OfficeTelephoneExtension,@NewFirstName + '.' + @NewLastName + convert(varchar,@ClientID) + '@aquarium-software.com',@ChargeOutRate,@Salt,0,0,null)

		END

		If @FirstName <> 'Paul' and @LastName <> 'Richardson'
		Begin

			Select 
			@TitleID = ClientPersonnel.TitleID,
			@NewFirstName = ClientPersonnel.FirstName,
			@NewLastName = ClientPersonnel.LastName,
			@JobTitle = ClientPersonnel.JobTitle,
			@Password = ClientPersonnel.Password,
			@MobilePhone = ClientPersonnel.MobileTelephone,
			@HomeTelephone = ClientPersonnel.HomeTelephone,
			@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
			@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
			@ChargeOutRate = ClientPersonnel.ChargeOutRate,
			@Salt = ClientPersonnel.Salt
			From ClientPersonnel 
			Where ClientPersonnel.ClientPersonnelID = 1967

			INSERT INTO [dbo].[ClientPersonnel]([ClientID],[ClientOfficeID],[TitleID],[FirstName],[MiddleName],[LastName],[JobTitle],[Password],[ClientPersonnelAdminGroupID],[MobileTelephone],[HomeTelephone],[OfficeTelephone],[OfficeTelephoneExtension],[EmailAddress],[ChargeOutRate],[Salt],[AttemptedLogins],[AccountDisabled],[ManagerID])
			VALUES(@ClientID,@OfficeID,@TitleID,@NewFirstName,@ClientID,@NewLastName,@JobTitle,@Password,1,@MobileTelephone,@HomeTelephone,@OfficeTelephoneCP,@OfficeTelephoneExtension,@NewFirstName + '.' + @NewLastName + convert(varchar,@ClientID) + '@aquarium-software.com',@ChargeOutRate,@Salt,0,0,null)

		END*/

		SELECT 
		@TitleID = 0,
		@NewFirstName = 'Web',
		@NewLastName = 'Service',
		@JobTitle = 'Web Service User (App Login Excluded)',
		@Password = 'mash',
		@MobilePhone = '',
		@HomeTelephone = '',
		@OfficeTelephoneCP = '',
		@OfficeTelephoneExtension = '',
		@ChargeOutRate = 0.00,
		@Salt = 'mash'
		
		SELECT @UserName = @NewFirstName + '.' + @NewLastName + CONVERT(VARCHAR,@ClientID) + '@aquarium-software.com'
		EXEC @ClientPersonnelID = dbo.ClientPersonnel_Insert_Helper @ClientID, @NewFirstName, @NewLastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @ClientID, @JobTitle, 1, @MobileTelephone, @HomeTelephone, @OfficeTelephoneCP, @OfficeTelephoneExtension, @ChargeOutRate, @IsAquarium=1
		
		INSERT INTO __LoginExclusions (ClientID, ClientPersonnelID, Description)
		SELECT @ClientID, @ClientPersonnelID, 'Web Service User'

		SELECT 
		@TitleID = 0,
		@NewFirstName = 'Aquarium',
		@NewLastName = 'Automation',
		@JobTitle = 'Aquarium Automation User (App Login Excluded)',
		@Password = 'mash',
		@MobilePhone = '',
		@HomeTelephone = '',
		@OfficeTelephoneCP = '',
		@OfficeTelephoneExtension = '',
		@ChargeOutRate = 0.00,
		@Salt = 'mash'
		
		SELECT @UserName = @NewFirstName + '.' + @NewLastName + CONVERT(VARCHAR,@ClientID) + '@aquarium-software.com'
		EXEC @ClientPersonnelID = dbo.ClientPersonnel_Insert_Helper @ClientID, @NewFirstName, @NewLastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @ClientID, @JobTitle, 1, @MobileTelephone, @HomeTelephone, @OfficeTelephoneCP, @OfficeTelephoneExtension, @ChargeOutRate, @IsAquarium=1
		
		INSERT INTO __LoginExclusions (ClientID, ClientPersonnelID, Description)
		SELECT @ClientID, @ClientPersonnelID, 'Aquarium Automation User'

		UPDATE LeadDetailValues
		SET DetailValue = @ClientID
		WHERE DetailfieldID = 6453
		AND LeadID = @LeadID

		UPDATE Lead
		SET LeadRef = @ClientID
		WHERE LeadID = @LeadID

		UPDATE Customers
		SET LastName = @ClientID
		FROM Customers
		INNER JOIN Lead ON Lead.CustomerID = Customers.CustomerID
		WHERE Lead.LeadID = @LeadID

		IF @CreateRLandT = 5144
		BEGIN

			/*2014-07-21 ACE Disabled the Enabled and live for Resources and Tables*/
			INSERT INTO LeadType (ClientID, LeadTypeName, LeadTypeDescription, Enabled, Live, MatterDisplayName, LastReferenceInteger, ReferenceValueFormatID, IsBusiness, LeadDisplayName, CaseDisplayName, CustomerDisplayName, LeadDetailsTabImageFileName, CustomerTabImageFileName, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments)
			SELECT @ClientID,'Resources and Tables','Resources and Tables',0,0,'',NULL,NULL,0,'','','',NULL,NULL,0,0,0,0

		END

		/* Create Automation Folder */
		INSERT INTO Folder(ClientID, FolderTypeID, FolderParentID, FolderName, FolderDescription, WhenCreated, WhoCreated)
		SELECT @ClientID, 1, NULL, 'Automation Reports', 'Automation Reports', dbo.fn_GetDate_Local(), @CreatedBy

		SELECT @FolderID = SCOPE_IDENTITY()

		/* Create Base Report */
		INSERT INTO SqlQuery (ClientID,QueryText,QueryTitle,AutorunOnline,OnlineLimit,BatchLimit,SqlQueryTypeID,FolderID,IsEditable,IsTemplate,IsDeleted,WhenCreated,CreatedBy,OwnedBy,RunCount,LastRundate,LastRuntime,LastRowcount,MaxRuntime,MaxRowcount,AvgRuntime,AvgRowcount,Comments,WhenModified,ModifiedBy,LeadTypeID,ParentQueryID,IsParent)
		SELECT @ClientID,'SELECT Cases.CaseID, Customers.CustomerID, Lead.LeadID, LeadEvent.EventTypeID, LeadEvent.WhenFollowedUp, LeadEvent.EventDeleted, Customers.Test, LeadEvent.LeadEventID  
		FROM Customers    
		INNER JOIN Lead  ON Lead.CustomerID = Customers.CustomerID  
		INNER JOIN Cases  ON Cases.LeadID = Lead.LeadID  
		INNER JOIN LeadEvent  ON LeadEvent.CaseID = Cases.CaseID  
		WHERE Customers.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '   
		AND Lead.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '   
		AND Cases.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '   
		AND LeadEvent.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '   
		AND (Customers.Test IS NULL  OR Customers.Test = 0)   
		AND (LeadEvent.EventDeleted = 0)   
		AND (LeadEvent.WhenFollowedUp IS NULL )','_Base',0,0,0,3,@FolderID,1,0,0,dbo.fn_GetDate_Local(),@CreatedBy,@CreatedBy,0,NULL,0,0,0,0,0,0,'',dbo.fn_GetDate_Local(),@CreatedBy,NULL,NULL,0

		SELECT @SqlQueryID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
		SELECT @ClientID,@SqlQueryID,'Customers','',0,'','',NULL,NULL,NULL,NULL

		SELECT @CustomersTableID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
		SELECT @ClientID,@SqlQueryID,'Lead','',1,'INNER JOIN','Lead.CustomerID = Customers.CustomerID',NULL,30,NULL,NULL

		SELECT @LeadTableID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
		SELECT @ClientID,@SqlQueryID,'Cases','',2,'INNER JOIN','Cases.LeadID = Lead.LeadID',NULL,41,NULL,NULL

		SELECT @CasesTableID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
		SELECT @ClientID,@SqlQueryID,'LeadEvent','',3,'INNER JOIN','LeadEvent.CaseID = Cases.CaseID',NULL,63,NULL,NULL

		SELECT @LeadEventTableID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.CustomerID','',1,2,NULL,NULL,NULL,NULL,0,'Number','Number','CustomerID'

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@LeadTableID,'Lead.LeadID','',1,3,NULL,NULL,NULL,NULL,0,'Number','Number','LeadID'

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@CasesTableID,'Cases.CaseID','',1,1,NULL,NULL,NULL,NULL,0,'Number','Number','CaseID'

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.EventTypeID','',1,4,NULL,NULL,NULL,NULL,0,'Number','Number','EventTypeID'

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.WhenFollowedUp',NULL,1,5,NULL,NULL,NULL,NULL,0,'DateTime','DateTime','WhenFollowedUp'

		SELECT @WhenFollowedUpColID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.EventDeleted',NULL,1,6,NULL,NULL,NULL,NULL,0,'Number','Number','EventDeleted'

		SELECT @EventDeletedColID = SCOPE_IDENTITY()

		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.Test',NULL,1,7,NULL,NULL,NULL,NULL,0,'Number','Number','Test'

		SELECT @TestColID = SCOPE_IDENTITY()

		/* 2010-05-06 Added by ACE */
		INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.LeadEventID','',1,8,NULL,NULL,NULL,NULL,0,'Number','Number','LeadEventID'

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@CustomersTableID,NULL,'Customers.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Customers Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@LeadTableID,NULL,'Lead.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Lead Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@CasesTableID,NULL,'Cases.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Cases Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,NULL,'LeadEvent.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','LeadEvent Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@CustomersTableID,@TestColID,'(Customers.Test IS NULL  OR Customers.Test = 0)',0,'','',NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,NULL

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,@EventDeletedColID,'(LeadEvent.EventDeleted = 0)',0,'','',NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,NULL

		INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		SELECT @ClientID,@SqlQueryID,@LeadEventTableID,@WhenFollowedUpColID,'(LeadEvent.WhenFollowedUp IS NULL )','','','',NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,NULL

		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 1
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 2
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 3
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 4
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 5
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 0, 6
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 1, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 2, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 3, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 4, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 5, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 6, 0
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 1, 1
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 2, 2
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 3, 3
		--exec _C00_CaseExampleReport @CreatedBy, @FolderID, 4, 4

		INSERT INTO __BatchErrorExclusion (ClientID, FinishDate, Reason)
		SELECT @ClientID, DATEADD(dd,15,dbo.fn_GetDate_Local()), 'New Client Created'
		
		INSERT INTO ClientOption (ClientID, ZenDeskOff, ZenDeskURLPrefix, ZenDeskText, ZenDeskTitle, iDiaryOff, WorkflowOff, UserDirectoryOff, eCatcherOff, LeadAssignmentOff, UserMessagesOff, SystemMessagesOff, UserPortalOff, ReportSearchOff, NormalSearchOff, UseXero, UseUltra, UseCaseSummary)
		VALUES (@ClientID, 0, 'aquarium', 'How may we help you? Please fill in details below, and we will get back to you as soon as possible.', 'Aquarium Software Limited', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_ClientCreate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_ClientCreate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_ClientCreate] TO [sp_executeall]
GO
