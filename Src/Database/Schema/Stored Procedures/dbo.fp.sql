SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-01
-- Description:	Describe what is going on with the field parsing of a document
-- 2019-10-15 CPS for JIRA LPC-76 | Added @DocumentTypeVersionID support
-- =============================================
CREATE PROCEDURE [dbo].[fp] 
	 @DocumentTypeID		INT = NULL
	,@LeadTypeID			INT = NULL
	,@DocumentTypeVersionID	INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientPersonnelID int = 1, 
	@LeadID int = 2, 
	@CaseID int = 3 
	
	IF @DocumentTypeID > 0
	BEGIN
	
		SELECT * 
		FROM dbo.DocumentStandardTarget d WITH (NOLOCK) 
		WHERE d.DocumentTypeID = @DocumentTypeID
		AND (d.DocumentTypeVersionID = @DocumentTypeVersionID OR @DocumentTypeVersionID is NULL)

		SELECT * 
		FROM dbo.DocumentDetailFieldTarget d WITH (NOLOCK) 
		WHERE d.DocumentTypeID = @DocumentTypeID
		AND (d.DocumentTypeVersionID = @DocumentTypeVersionID OR @DocumentTypeVersionID is NULL)

		SELECT * 
		FROM dbo.DocumentMRTarget d WITH (NOLOCK) 
		WHERE d.DocumentTypeID = @DocumentTypeID
		AND (d.DocumentTypeVersionID = @DocumentTypeVersionID OR @DocumentTypeVersionID is NULL)

		SELECT * 
		FROM dbo.DocumentSpecialisedDetailFieldTarget d WITH (NOLOCK) 
		WHERE d.DocumentTypeID = @DocumentTypeID
		AND (d.DocumentTypeVersionID = @DocumentTypeVersionID OR @DocumentTypeVersionID is NULL)
		
		;WITH InnerSql AS 
		(
			SELECT MAX(ld.LeadDocumentID) as LatestLeadDocumentID 
			FROM dbo.LeadDocument ld WITH (NOLOCK) 
			WHERE ld.DocumentTypeID = @DocumentTypeID 
		)
		SELECT @ClientPersonnelID = le.WhoCreated, @LeadID = le.LeadID, @CaseID = le.CaseID 
		FROM InnerSql i 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.LeadDocumentID = i.LatestLeadDocumentID 
		
		SELECT 'EXEC dbo.FieldParser_ParseTemplate @ClientPersonnelID = ' + CAST(@ClientPersonnelID as varchar) + ', @LeadID = ' + CAST(@LeadID as varchar) + ', @CaseID = ' + CAST(@CaseID as varchar) + ', @DocumentTypeID = ' + CAST(@DocumentTypeID as varchar) as [Helper Proc]
		
		SELECT cp.* 
		FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
		INNER JOIN dbo.DocumentType d WITH (NOLOCK) on d.ClientID = cp.ClientID 
		WHERE d.DocumentTypeID = @DocumentTypeID 
		ORDER BY cp.UserName 
		
	END
	
	IF @LeadTypeID > 0
	BEGIN
	
		SELECT * 
		FROM dbo.LeadTypePageTitleStandardTarget d WITH (NOLOCK) 
		WHERE d.LeadTypeID = @LeadTypeID

		SELECT * 
		FROM dbo.LeadTypePageTitleDetailFieldTarget d WITH (NOLOCK) 
		WHERE d.LeadTypeID = @LeadTypeID

		SELECT * 
		FROM dbo.LeadTypePageTitleSpecialisedDetailFieldTarget d WITH (NOLOCK) 
		WHERE d.LeadTypeID = @LeadTypeID
		
		SELECT TOP 1 @ClientPersonnelID = le.WhoCreated, @LeadID = le.LeadID, @CaseID = le.CaseID 
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.LeadID = l.LeadID 
		WHERE l.LeadTypeID = @LeadTypeID 
		ORDER BY le.LeadEventID DESC
		
		SELECT 'EXEC dbo.FieldParser_ParseTemplate @ClientPersonnelID = ' + CAST(@ClientPersonnelID as varchar) + ', @LeadID = ' + CAST(@LeadID as varchar) + ', @CaseID = ' + CAST(@CaseID as varchar) + ', @PageTitleLeadTypeID = ' + CAST(@LeadTypeID as varchar) as [Helper Proc]
		
		SELECT cp.* 
		FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
		INNER JOIN dbo.LeadType d WITH (NOLOCK) on d.ClientID = cp.ClientID 
		WHERE d.LeadTypeID = @LeadTypeID
		ORDER BY cp.UserName 
		
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[fp] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fp] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fp] TO [sp_executeall]
GO
