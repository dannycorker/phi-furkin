SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Ben Crinion
-- Purpose: Select the CaseID from a LeadEvent. This is specifically for the BatchScheduler.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent__GetCaseIDByLeadEventID]
(

	@LeadEventID int   
)
AS


				SELECT
					[CaseID]
				FROM
					[dbo].[LeadEvent]
				WHERE
					[LeadEventID] = @LeadEventID
			Select @@ROWCOUNT
					
			




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetCaseIDByLeadEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__GetCaseIDByLeadEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetCaseIDByLeadEventID] TO [sp_executeall]
GO
