SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ExternalApplicationLogin table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_Get_List]

AS


				
				SELECT
					[ExternalApplicationLoginID],
					[ThirdPartySystemID],
					[ClientID],
					[SubClientID],
					[UserName],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[ClientPersonnelAdminGroupID],
					[AttemptedLogins],
					[AccountDisabled],
					[CustomerID]
				FROM
					[dbo].[ExternalApplicationLogin] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Get_List] TO [sp_executeall]
GO
