SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-05-13
-- Description:	Gets a list of VetEnvoy claims that could not be imported
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_GetClaimsNotImported]
(
	@ClientID INT,
	@Type VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim')
	SELECT	c.ClaimID, a.Name, c.Received, c.PolicyRef, 
			c.Xml.value('(//PolicyholderName)[1]', 'VARCHAR(2000)') AS Policyholder,
			c.Xml.value('(//AnimalDetails/Name)[1]', 'VARCHAR(2000)') AS PetName,
			c.Xml.value('(//AnimalDetails/Breed)[1]', 'VARCHAR(2000)') AS Breed
	FROM dbo.VetEnvoy_Claims c WITH (NOLOCK) 
	INNER JOIN dbo.VetEnvoy_Accounts a WITH (NOLOCK) ON c.AccountID = a.AccountID
	WHERE (c.LeadID IS NULL OR c.MatterIDs IS NULL)
	AND a.ClientID = @ClientID
	AND a.AccountType = @Type
	AND a.Enabled = 1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_GetClaimsNotImported] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_GetClaimsNotImported] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_GetClaimsNotImported] TO [sp_executeall]
GO
