SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-24
-- Description:	Delete a completed AutomatedEventQueue record when the event has been applied
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__SetEventError]
	@AutomatedEventQueueID int,
	@ErrorMessage varchar(2000)
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Clear the lock details from AutomatedEventQueue records for a particular scheduler that has crashed.
	*/
	UPDATE dbo.AutomatedEventQueue 
	SET ErrorMessage = @ErrorMessage, 
	ErrorDateTime = dbo.fn_GetDate_Local(), 
	ErrorCount += 1,
	SchedulerID = NULL,
	ServerName = NULL,
	LockDateTime = NULL
	WHERE AutomatedEventQueueID = @AutomatedEventQueueID 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__SetEventError] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__SetEventError] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__SetEventError] TO [sp_executeall]
GO
