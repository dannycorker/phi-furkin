SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Dave Morgan
-- Create date: 16/08/2016
-- Description:	Picks the first payment available and queues it. If there are more it adds the queue event again
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_QueuePremiumPayment]
(
	@LeadEventID INT,
	@MatterID INT,
	@IsPayment BIT
)
AS
BEGIN

--declare @matterID int = 1306,
--		@LeadEventID INT = 19276,
--		@IsPayment INT = 1

	SET NOCOUNT ON;
	

	DECLARE @Payments TABLE ( PaymentScheduleID INT, MatterID INT, PaymentMethod VARCHAR(30), Done INT )

	DECLARE 
			@AccountID INT,
			@ClientID INT,
			@DetailValue VARCHAR(200), 
			@LeadTypeID INT,
			@ObjectID INT,
			@PaymentScheduleID INT,
			@PaymentTypeID VARCHAR(30),
			@SettlementDate DATE

	
	SELECT @LeadTypeID=l.LeadTypeID, @ClientID=l.ClientID 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=m.LeadID
	WHERE MatterID=@MatterID
	
	SELECT @SettlementDate=dbo.fnAddWorkingDays ( dbo.fnGetNextWorkingDate (CONVERT(DATE,dbo.fn_GetDate_Local()),0) ,cdv.ValueInt + 1 ) 
	FROM ClientDetailValues cdv WITH (NOLOCK) WHERE cdv.DetailFieldID=170226 AND cdv.ClientID=@ClientID	
	
	INSERT INTO @Payments ( PaymentScheduleID, MatterID, PaymentMethod, Done )
	SELECT cps.CustomerPaymentScheduleID, m.MatterID, paymeth.DetailValue, 0
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
	INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID
	LEFT JOIN MatterDetailValues suspended WITH (NOLOCK) ON m.MatterID=suspended.MatterID AND suspended.DetailFieldID=175275
	INNER JOIN MatterDetailValues paymeth WITH (NOLOCK) ON m.MatterID=paymeth.MatterID AND paymeth.DetailFieldID=170115
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToMatterID=m.MatterID AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1493
	INNER JOIN Matter pam WITH (NOLOCK) ON ltr.FromMatterID=pam.MatterID
	INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON pam.MatterID=pstat.MatterID AND pstat.DetailFieldID=170038
	INNER JOIN Cases pac WITH (NOLOCK) ON pam.CaseID=pac.CaseID
	INNER JOIN MatterDetailValues aid WITH (NOLOCK) ON m.MatterID=aid.MatterID AND aid.DetailFieldID=176973
	INNER JOIN Account a WITH (NOLOCK) ON aid.ValueInt=a.AccountID
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON a.AccountID=cps.AccountID
	WHERE  c.Test=0 AND c.ClientID=@ClientID
	---- and not cancelled
	--AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=pac.CaseID AND le.EventTypeID=150145 AND le.WhenFollowedUp IS NULL AND le.EventDeleted=0 )
	-- and we are doing collections for this customer
	AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=ca.CaseID AND le.EventDeleted=0 AND le.EventTypeID=155198 )
	-- and collections are not on hold
	AND ( suspended.ValueInt <> 5144 OR suspended.ValueInt IS NULL )
	-- and policy status is live
	--AND pstat.ValueInt=43002
	-- and payment schedule entry has not already been queued in the GL
	AND cps.CustomerLedgerID IS NULL
	-- and payment date is before or the same as settlement day
	AND cps.ActualCollectionDate <= @SettlementDate
	-- and just for this matter
	AND m.MatterID = @MatterID
	-- and payment value is not zero
	AND ( (cps.PaymentGross>0 AND @IsPayment=1) OR
		  (cps.PaymentGross<0 AND @IsPayment=0) )
		  
	SELECT TOP 1 @MatterID=p.MatterID, 
				 @PaymentScheduleID=p.PaymentScheduleID,
				 @PaymentTypeID=p.PaymentMethod
	FROM @Payments p 
	WHERE p.Done=0
	ORDER BY p.PaymentScheduleID
	
	DECLARE @PaymentTypes TABLE ( PaymentType INT, PaymentDescription VARCHAR(500), RNO INT )
	INSERT INTO @PaymentTypes
	SELECT ppps.PurchasedProductPaymentScheduleTypeID,
		   pname.DetailValue + ' - ' +
		   CASE WHEN pppst.PurchasedProductPaymentScheduleTypeName IS NULL THEN 'Scheduled' 
				ELSE pppst.PurchasedProductPaymentScheduleTypeName END,
		   ROW_NUMBER() OVER(PARTITION BY ppps.PurchasedProductPaymentScheduleTypeID, pname.DetailValue ORDER BY ppps.CustomerPaymentScheduleID ASC) as [RNO] 
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	LEFT JOIN PurchasedProductPaymentScheduleType pppst WITH (NOLOCK) ON ppps.PurchasedProductPaymentScheduleTypeID=pppst.PurchasedProductPaymentScheduleTypeID
	INNER JOIN MatterDetailValues ppid WITH (NOLOCK) ON ppps.PurchasedProductID=ppid.ValueInt AND ppid.DetailFieldID=177074
	INNER JOIN Matter m WITH (NOLOCK) ON ppid.MatterID=m.MatterID
	INNER JOIN LeadDetailValues pname WITH (NOLOCK) ON m.LeadID=pname.LeadID AND pname.DetailFieldID=144268
	WHERE ppps.CustomerPaymentScheduleID=@PaymentScheduleID
	
	SELECT @DetailValue=NULL
	SELECT @DetailValue = COALESCE(@DetailValue + ', ', '') + p.PaymentDescription + ' (' + CAST(p.RNO AS VARCHAR) + ' item)'  
	FROM @PaymentTypes p
	WHERE NOT EXISTS ( SELECT * FROM @PaymentTypes p2 WHERE p.RNO < p2.RNO AND p.PaymentDescription=p2.PaymentDescription )
	ORDER BY PaymentDescription

	UPDATE LeadEvent Set Comments = Comments 
										+ CASE WHEN Comments='' THEN '' ELSE ' ' END 
										+ 'Collecting CustomerPaymentScheduleID: ' 
										+  CAST(@PaymentScheduleID AS VARCHAR)
	WHERE LeadEventID=@LeadEventID

	-- clear helper or update fields
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4370)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PaymentTypeID, @MatterID  -- payment type ID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4373)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- payment ref
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4372)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @DetailValue, @MatterID  -- payment descript
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4374)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- payment net
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4375)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- payment tax
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4376)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- payment gross
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4396)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PaymentScheduleID, @MatterID  -- payment sched ID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4386)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @MatterID, @MatterID  -- object ID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4387)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, 2, @MatterID  -- object type ID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4383)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, 69940, @MatterID  -- payment status ID

	UPDATE @Payments SET Done=1 WHERE PaymentScheduleID=@PaymentScheduleID
	
	SELECT @AccountID=AccountID 
	FROM CustomerPaymentSchedule WITH (NOLOCK) 
	WHERE CustomerPaymentScheduleID=@PaymentScheduleID
	
	EXEC Account__SetDateAndAmountOfNextPayment @AccountID	
	
	-- add event again to do next payment
	IF EXISTS ( SELECT * FROM @Payments WHERE Done=0 )
	BEGIN
	
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount,EarliestRunDateTime)
		SELECT TOP 1 le.ClientID, m.CustomerID, le.LeadID, le.CaseID, LeadEventID, EventTypeID, dbo.fn_GetDate_Local(), le.WhoCreated, EventTypeID, le.WhoCreated, -1, 5, 0, 5, DATEADD(SECOND,15,dbo.fn_GetDate_Local())
		FROM LeadEvent le WITH (NOLOCK) 
		INNER JOIN Matter m WITH (NOLOCK) ON le.CaseID=m.CaseID
		WHERE LeadEventID=@LeadEventID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueuePremiumPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_QueuePremiumPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueuePremiumPayment] TO [sp_executeall]
GO
