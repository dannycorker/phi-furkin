SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_Insert]
(

	@FieldTargetID int    OUTPUT,

	@Target varchar (250)  ,

	@ObjectName varchar (50)  ,

	@PropertyName varchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[FieldTarget]
					(
					[Target]
					,[ObjectName]
					,[PropertyName]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@Target
					,@ObjectName
					,@PropertyName
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @FieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Insert] TO [sp_executeall]
GO
