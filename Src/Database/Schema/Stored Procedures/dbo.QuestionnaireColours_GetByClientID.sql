SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireColours table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[QuestionnaireColourID],
					[ClientQuestionnaireID],
					[Page],
					[Header],
					[Intro],
					[Body],
					[Footer],
					[ClientID]
				FROM
					[dbo].[QuestionnaireColours] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_GetByClientID] TO [sp_executeall]
GO
