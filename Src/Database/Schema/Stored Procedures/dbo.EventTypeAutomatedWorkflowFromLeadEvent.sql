SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-08-10
-- Description:	Run batch jobs based on events 
-- 2018-02-08 CPS change case statement so if e.Followup = 1 then it uses the current event type.
-- 2018-05-30 CPS allow this to return the WorkflowTaskID that it has created.  Should only ever be one.
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedWorkflowFromLeadEvent] 
	@LeadEventID int,
	@EventTypeID int 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @WorkflowTaskID INT

	/*Batch 5235 is a client 0 batch (required for the workflow task table)*/
	INSERT INTO WorkflowTask (WorkflowGroupID, AutomatedTaskID, Priority, AssignedTo, AssignedDate, LeadID, CaseID, EventTypeID, ClientID, FollowUp, Important, CreationDate, Escalated, EscalatedBy, EscalationReason, EscalationDate, Disabled, DisabledReason, DisabledDate)
	SELECT WorkflowGroupID, 5235, e.Priority, NULL, NULL, le.LeadID, le.CaseID, CASE e.FollowUp WHEN 1 THEN @EventTypeID ELSE e.EventTypeToAdd END, e.ClientID, e.FollowUp, e.Important, dbo.fn_GetDate_Local(), 0, NULL, NULL, NULL, 0, NULL, NULL
	FROM EventTypeAutomatedWorkflow e WITH ( NOLOCK )
	INNER JOIN LeadEvent le WITH ( NOLOCK ) ON le.LeadEventID = @LeadEventID and le.EventTypeID = e.EventTypeID and le.ClientID = e.ClientID
	WHERE e.EventTypeID = @EventTypeID

	SELECT @WorkflowTaskID = SCOPE_IDENTITY()
	RETURN @WorkflowTaskID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflowFromLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedWorkflowFromLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflowFromLeadEvent] TO [sp_executeall]
GO
