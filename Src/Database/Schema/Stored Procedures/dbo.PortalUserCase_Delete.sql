SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalUserCase table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserCase_Delete]
(

	@PortalUserCaseID int   
)
AS


				DELETE FROM [dbo].[PortalUserCase] WITH (ROWLOCK) 
				WHERE
					[PortalUserCaseID] = @PortalUserCaseID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Delete] TO [sp_executeall]
GO
