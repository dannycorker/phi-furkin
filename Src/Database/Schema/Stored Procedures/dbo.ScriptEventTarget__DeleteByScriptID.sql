SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 12-11-2014
-- Description:	Deletes the event targets via the script id
-- =============================================
CREATE PROCEDURE [dbo].[ScriptEventTarget__DeleteByScriptID]
	@ClientID INT, 
	@ScriptID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DELETE FROM ScriptEventTarget
	WHERE ScriptID=@ScriptID AND ClientID=@ClientID
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget__DeleteByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget__DeleteByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget__DeleteByScriptID] TO [sp_executeall]
GO
