SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-03-13
-- Description:	Reconcile payment failures
-- 2018-06-07 ACE Set failure file type based off the BacsPaymentFailures table
-- 2020-05-13 NG  Comment proc
-- 2020-05-17 GPR Added logging to assist with testing in DEV
-- 2021-02-09 CPS for JIRA FURKIN-71 | Added a section for Canada, a missing ELSE, and NOLOCKs on each of the Payment joins
-- =============================================
CREATE PROCEDURE [dbo].[Billing__ReconcilePaymentFailures]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SET NOCOUNT ON;

	DECLARE @TestID INT = 0,
			@PaymentFailureID INT = 0,
			@AccountNumber VARCHAR(20),
			@SortCode VARCHAR(10),
			@PaymentAmount NUMERIC(18,2),
			@BACSProcessedDate DATE,
			@FailureCode VARCHAR(200),
			@Reference VARCHAR(200),
			@AccountID INT,
			@PaymentID INT,
			@FileName VARCHAR(2000),
			@LogMessage VARCHAR(1024) = '',
			@RunAsUserID INT,
			@FailureFileType VARCHAR(50),
			@CountryID INT, 
			@ProjectedCancellationDate DATE, 
			@MatterID INT 

	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID
	SELECT @RunAsUserID	= dbo.fn_C00_GetAutomationUser(@ClientID)

	EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'CountryID', @CountryID, @RunAsUserID
	
	BEGIN TRY
		BEGIN TRAN
	
		/*BACS Failures have been inserted into the BACSPaymentFailures table from an imported file
		Find the first one which has not been actioned (ImportstatusID = 1*/
		SELECT TOP 1 @PaymentFailureID = i.PaymentFailureID,
					@AccountNumber = i.AccountNumber,
					@SortCode = i.SortCode,
					@PaymentAmount = i.PaymentAmount,
					@BACSProcessedDate = i.BACSProcessedDate,
					@FailureCode = i.FailureCode,
					@Reference = i.Reference,
					@FailureFileType = i.FailureFileType
		FROM BACSPaymentFailures i WITH (NOLOCK)
		WHERE i.ImportStatusID = 1
		ORDER BY i.PaymentFailureID

		EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'FailureCode', @FailureCode, @RunAsUserID
		EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'BacsProcessedDate', @BACSProcessedDate, @RunAsUserID
		EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'Reference', @Reference, @RunAsUserID
	

		WHILE @PaymentFailureID > @TestID
		BEGIN

			SELECT @PaymentID = NULL, @AccountID = NULL

			IF @CountryID IN (14)
			BEGIN

				EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', '@BACSProcessedDate', @BACSProcessedDate, @RunAsUserID

				/*match the failures to their payment requests*/
				SELECT TOP 1 @PaymentID = p.PaymentID, @AccountID = cps.AccountID
				FROM Account a WITH (NOLOCK) 
				INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.AccountID = a.AccountID 
				INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
				WHERE a.ClientID = @ClientID
				AND a.Reference = @Reference /*Same ref*/
				AND p.PaymentGross = @PaymentAmount /*Same amount*/
				/*GPR 2020-05-17 - need to check the rules for AUS with JEL, 5 days in AUS*/
				AND (
						/*Reconcile on any date between sending the file 1 day before payment date and five days after expected collection date*/
						(@BACSProcessedDate BETWEEN DATEADD(DD,-1, cps.PaymentDate) AND DATEADD(DD,5, cps.ActualCollectionDate))
					OR
						(@BACSProcessedDate IS NULL AND @FailureFileType IN ('1','2','3','4'))
					)
				/*Same date, might have to relax this a little.*/
				AND cps.PaymentStatusID IN (6,5,2) /*Processed, retried, paid*/
				ORDER BY cps.CustomerPaymentScheduleID DESC

				EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'PaymentID', @PaymentID, @RunAsUserID
				EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'AccountID', @AccountID, @RunAsUserID
	
			END
			ELSE IF @CountryID IN (233) /*GPR 2020-09-11*/
			BEGIN
				/*match the failures to their payment requests*/
				SELECT TOP 1 @PaymentID = p.PaymentID, @AccountID = cps.AccountID
				FROM Account a WITH (NOLOCK) 
				INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.AccountID = a.AccountID 
				INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
				WHERE a.ClientID = @ClientID
				AND a.CardToken = @Reference /*C605 pass the Token as the Reference*/
				AND p.PaymentGross = @PaymentAmount /*Same amount*/
				AND (
						/*Reconcile on any date between sending the file -1 day and one day after expected collection date*/
						(@BACSProcessedDate BETWEEN DATEADD(DD,-1, cps.PaymentDate) AND DATEADD(DD,3, cps.ActualCollectionDate))
					OR
						(@BACSProcessedDate IS NULL AND @FailureFileType = 'ARUCS')
					)
				/*Same date, might have to relax this a little.*/
				AND cps.PaymentStatusID IN (6,5,2) /*Processed, retried, paid*/
				ORDER BY cps.CustomerPaymentScheduleID DESC
			END
			ELSE IF @CountryID IN (39) /*Canada*/
			BEGIN
				-- 2021-02-09 CPS for JIRA FURKIN-71 | HSBC give us loads of useful information back so we already have the PaymentID.  Read from the BACSPaymentFailures table followed by CPS to maintain consistency with other countries in this proc
				SELECT @PaymentID = bpf.PaymentID
				FROM BACSPaymentFailures bpf WITH (NOLOCK) 
				WHERE bpf.PaymentFailureID = @PaymentFailureID

				SELECT @AccountID = cps.AccountID
				FROM Payment p WITH (NOLOCK) 
				INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) on cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
				WHERE p.PaymentID = @PaymentID

				/*ALM/GPR 2021-03-11 FURKIN-247*/
				/*Calculate Projected Cancellation Date and store against the Policy*/
                IF NULLIF(@Reference, '') IS NOT NULL 
				BEGIN 
					SELECT @MatterID = a.ObjectID FROM Account a
					WHERE a.Reference = @Reference
					AND a.Active = 1
					AND a.ClientID = 607

					SELECT @MatterID = [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections](@MatterID)

					SELECT @ProjectedCancellationDate = ISNULL(dbo.fn_C607_CalculatedProjectedCancellationDate(@MatterID), dbo.fn_GetDate_Local())
					EXEC _C00_SimpleValueIntoField 315274, @ProjectedCancellationDate, @MatterID, 58552
				END
			END
			ELSE -- 2021-02-09 CPS for JIRA FURKIN-71 | This ELSE was missing so anything that happened above was being overridden if the next (unconditional) section got any results.
			BEGIN
				/*match the failures to their payment requests*/
				SELECT TOP 1 @PaymentID = p.PaymentID, @AccountID = cps.AccountID
				FROM Account a WITH (NOLOCK) 
				INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.AccountID = a.AccountID 
				INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
				WHERE a.ClientID = @ClientID
				AND a.Reference = @Reference /*Same ref*/
				AND p.PaymentGross = @PaymentAmount /*Same amount*/
				AND (
						/*Reconcile on any date between sending the file -1 day and one day after expected collection date*/
						(@BACSProcessedDate BETWEEN DATEADD(DD,-1, cps.PaymentDate) AND DATEADD(DD,1, cps.ActualCollectionDate))
					OR
						(@BACSProcessedDate IS NULL AND @FailureFileType = 'ARUCS')
					)
				/*Same date, might have to relax this a little.*/
				AND cps.PaymentStatusID IN (6,5,2) /*Processed, retried, paid*/
				ORDER BY cps.CustomerPaymentScheduleID DESC
			END

			EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'PaymentID', @PaymentID, @RunAsUserID

			IF @PaymentID > 0
			BEGIN

				/*Update the failure row with the matching payment*/
				UPDATE i
				SET PaymentID = @PaymentID, AccountID = @AccountID
				FROM BACSPaymentFailures i
				WHERE i.PaymentFailureID = @PaymentFailureID

				/*Update the failed payment row with a failure reason and set the comments to the type of failure, eg DDIC/ARUDD/ARUCS*/
				UPDATE P
				SET FailureCode = @FailureCode, WhenModified = dbo.fn_GetDate_Local(), Comments = @FailureFileType, WhoModified = @RunAsUserID
				FROM Payment p 
				WHERE p.PaymentID = @PaymentID

				/*Call the post payment procedure to do the required updates/reversals to the payment*/
				EXEC dbo.Payment__PostPaymentProcedure @PaymentID, @ClientID

				/*Set the payment failure row as processed*/
				UPDATE b
				SET ImportedDate = dbo.fn_GetDate_Local(), Comments = 'Processed', ImportStatusID = 2
				FROM BACSPaymentFailures b
				WHERE b.PaymentFailureID = @PaymentFailureID

			END
			ELSE
			BEGIN

				/*Set the payment failure row to failed to process*/
				UPDATE b
				SET ImportedDate = dbo.fn_GetDate_Local(), Comments = 'Failed to Process', ImportStatusID = 3
				FROM BACSPaymentFailures b
				WHERE b.PaymentFailureID = @PaymentFailureID

			END
			
			SELECT @TestID = @PaymentFailureID
			
			/*Grab the next row to process and continue*/
			SELECT TOP 1 @PaymentFailureID = i.PaymentFailureID,
						@AccountNumber = i.AccountNumber,
						@SortCode = i.SortCode,
						@PaymentAmount = i.PaymentAmount,
						@BACSProcessedDate = i.BACSProcessedDate,
						@FailureCode = i.FailureCode,
						@Reference = i.Reference
			FROM BACSPaymentFailures i WITH (NOLOCK)
			WHERE i.ImportStatusID = 1
			ORDER BY i.PaymentFailureID

		END

		COMMIT

	END TRY
	BEGIN CATCH
		
		ROLLBACK

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		
	END CATCH

	EXEC dbo._C00_LogIt 'Info', 'Billing__ReconcilePaymentFailures', 'Proc Complete', '', @RunAsUserID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__ReconcilePaymentFailures] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__ReconcilePaymentFailures] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__ReconcilePaymentFailures] TO [sp_executeall]
GO
