SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFileFormat table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileFormat_GetByDataLoaderFileFormatID]
(

	@DataLoaderFileFormatID int   
)
AS


				SELECT
					[DataLoaderFileFormatID],
					[FileFormatName],
					[FileFormatDescription]
				FROM
					[dbo].[DataLoaderFileFormat] WITH (NOLOCK) 
				WHERE
										[DataLoaderFileFormatID] = @DataLoaderFileFormatID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_GetByDataLoaderFileFormatID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileFormat_GetByDataLoaderFileFormatID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_GetByDataLoaderFileFormatID] TO [sp_executeall]
GO
