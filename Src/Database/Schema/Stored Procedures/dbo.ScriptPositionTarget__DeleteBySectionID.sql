SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-03-2015
-- Description:	Deletes all the script position targets for the given section
-- =============================================
CREATE PROCEDURE [dbo].[ScriptPositionTarget__DeleteBySectionID]
(
	@SectionID INT   
)
AS
	DELETE FROM [dbo].[ScriptPositionTarget] WITH (ROWLOCK) 
	WHERE [SectionID] = @SectionID
			


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget__DeleteBySectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget__DeleteBySectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget__DeleteBySectionID] TO [sp_executeall]
GO
