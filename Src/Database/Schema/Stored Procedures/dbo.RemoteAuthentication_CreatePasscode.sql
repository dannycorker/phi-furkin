SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- ALTER date: 2015-09-23
-- Description:	Request remote authentication for a user.  
-- Validate the caller's credentials and ALTER a GUID as a logon token for the emailaddress who will try to logon later.
-- JWG 2015-11-12 #35428 Add Ultra credentials too.  Move to a new set of tables on AquariusMaster.
-- JWG 2015-11-13 #35369 Add Dakiver demo client 383 credentials too.
-- JWG 2015-12-18 #35977 New Dakiver demo client 389 credentials added.
-- =============================================
CREATE PROCEDURE [dbo].[RemoteAuthentication_CreatePasscode] 
	@Key VARCHAR(50),
	@EmailAddress VARCHAR(255),
	@IPAddress VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Key tells us who is calling this proc via the web method in Scripting 
		EmailAddress is the user they want to be able to log on to scripting
		IPAddress is the IP Address of the person/app making the request
	*/
	
	DECLARE @IsValid BIT = 0, 
	@ValidUntil DATETIME2(0) = NULL, 
	@ClientID INT = NULL, 
	@ClientPersonnelID INT = NULL, 
	@AttemptedLogins INT = NULL, 
	@AccountDisabled BIT = NULL, 
	@LogonToken VARCHAR(50) = '', 
	@ThirdPartySystemID INT = 0, 
	@Notes VARCHAR(500) = ''
		
	/* This is the Daviker key */
	/* #35428 Add more credentials */
	/*IF @Key = '7262AA9B-A645-4E3F-9FB9-39D206C3BB3D'*/
	IF @Key IN ('7262AA9B-A645-4E3F-9FB9-39D206C3BB3D', '50BB5FC0-49FB-4046-84A0-600AFA25FBDC')
	BEGIN
		
		/*
			Is this a valid (Daviker) IP Address?
			Daviker range is '81.128.180.1/28' where the /28 means:
			"from left to right, 28 out of 32 bits that make up 255.255.255.255 are locked down to the Network part of the address
			 which leaves just the final four bits 1111 (=15) available for the Host part to play with, ie 1 to 15.  0 and 255 are never valid for the Host part"
			It seemed easier to spell them all out here
			'192.168.6.1', '192.168.6.2' are our load balancers at Rackspace
		*/
		/* #35428, #35977 Add more credentials */
		/*IF @IPAddress IN ('92.27.117.1', '127.0.0.1', '::1', '81.128.180.1', '81.128.180.2', '81.128.180.3', '81.128.180.4', '81.128.180.5', '81.128.180.6', '81.128.180.7', '81.128.180.8', '81.128.180.9', '81.128.180.10', '81.128.180.11', '81.128.180.12', '81.128.180.13', '81.128.180.14', '81.128.180.15' )*/
		IF @IPAddress IN ('127.0.0.1', '192.168.6.1', '192.168.6.2', '::1', /* #35977 1Buzz 13.69. */ '13.69.158.181', '13.69.247.191', '81.128.180.1', '81.128.180.2', '81.128.180.3', '81.128.180.4', '81.128.180.5', '81.128.180.6', '81.128.180.7', '81.128.180.8', '81.128.180.9', '81.128.180.10', '81.128.180.11', '81.128.180.12', '81.128.180.13', '81.128.180.14', '81.128.180.15', '81.139.211.76', '92.27.117.1'	
		,'40.113.19.88'	/*dialler1.maxcontact.com*/,'137.135.189.33','178.22.142.146','40.85.95.189' /*#36860*/,'193.238.70.107','193.238.70.108','37.205.61.34','13.69.252.118', /*#37357*/'13.79.161.67',/*#37409*/ '13.79.156.39',/*#37659*/'40.85.95.189','40.113.90.58', '52.169.179.152' /*#38194*/,'40.69.38.53','104.45.82.90' /*38390*/, '13.79.167.153' /*38478*/)
		BEGIN
			SELECT @ClientID = cp.ClientID, 
			@ClientPersonnelID = cp.ClientPersonnelID, 
			@AttemptedLogins = cp.AttemptedLogins, 
			@AccountDisabled = cp.AccountDisabled 
			FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
			WHERE cp.EmailAddress = @EmailAddress 
			AND cp.ThirdPartySystemId = 0
						
			/* This is a valid Daviker client */
			/* #35428, #35977 Add more credentials */
			/*IF @ClientID IN (364, 380, 2, 4, 0) */
			IF @ClientID IN (0, 2, 4, 364, 379, 380, 383, 389,392,391,393,394,395,396,401) 
			BEGIN
				
				IF @AccountDisabled = 1
				BEGIN
					SELECT @Notes = 'INVALID: Account disabled'
				END
				ELSE
				BEGIN
					IF @AttemptedLogins > 2
					BEGIN
						SELECT @Notes = 'INVALID: Attempted logins > 2'
					END
					ELSE
					BEGIN
						/* Hooray - all valid */
						SELECT @IsValid = 1, 
						@ValidUntil = DATEADD(HOUR, 3, dbo.fn_GetDate_Local()), 
						@LogonToken = CAST(NEWID() AS VARCHAR(50)), 
						@ThirdPartySystemID = 42
					END
				END
				
			END
			ELSE
			BEGIN
				IF @ClientID IS NULL
				BEGIN
					SELECT @Notes = 'INVALID: User account not found'
				END
				ELSE
				BEGIN
					SELECT @Notes = 'INVALID: Operation not allowed for this client'
				END
			END
		END
		ELSE
		BEGIN
			SELECT @Notes = 'INVALID: IP Address not allowed for this key: ' + ISNULL(@Key, 'NULL')
		END
	END
	ELSE
	BEGIN
		SELECT @Notes = 'INVALID: Key is not valid: ' + ISNULL(@Key, 'NULL')
	END
	
	IF @IsValid = 1
	BEGIN
		INSERT INTO dbo.RemoteAuth(AuthKey, RequestedUsername, ClientPersonnelID, ClientID, WhenRequested, IPAddress, IsValid, ValidUntil, WhenFirstUsed, WhenLastUsed, UsageCount)
		VALUES (@LogonToken, @EmailAddress, @ClientPersonnelID, @ClientID, dbo.fn_GetDate_Local(), @IPAddress, @IsValid, @ValidUntil, NULL, NULL, 0)
	END
	ELSE
	BEGIN
		INSERT INTO dbo.RemoteAuthHistory(AuthKey, RequestedUsername, ClientPersonnelID, ClientID, WhenRequested, IPAddress, IsValid, ValidUntil, WhenFirstUsed, WhenLastUsed, UsageCount, WhenRemoved, Notes)
		VALUES (@LogonToken, @EmailAddress, @ClientPersonnelID, @ClientID, dbo.fn_GetDate_Local(), @IPAddress, @IsValid, @ValidUntil, NULL, NULL, 0, NULL, @Notes)
	END
	
	/* Select the GUID back to the app */
	SELECT @LogonToken AS Passcode
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoteAuthentication_CreatePasscode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoteAuthentication_CreatePasscode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoteAuthentication_CreatePasscode] TO [sp_executeall]
GO
