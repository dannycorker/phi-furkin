SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeAutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAutomatedTask_Get_List]

AS


				
				SELECT
					[EventTypeAutomatedTaskID],
					[ClientID],
					[EventTypeID],
					[AutomatedTaskID],
					[TimeDelayInSeconds],
					[AddTimeDelayIfAboutToRun]
				FROM
					[dbo].[EventTypeAutomatedTask] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Get_List] TO [sp_executeall]
GO
