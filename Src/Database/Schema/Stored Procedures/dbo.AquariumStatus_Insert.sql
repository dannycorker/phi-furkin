SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AquariumStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumStatus_Insert]
(

	@AquariumStatusID int   ,

	@AquariumStatusName varchar (50)  ,

	@AquariumStatusDescription varchar (250)  ,

	@Usable bit   
)
AS


				
				INSERT INTO [dbo].[AquariumStatus]
					(
					[AquariumStatusID]
					,[AquariumStatusName]
					,[AquariumStatusDescription]
					,[Usable]
					)
				VALUES
					(
					@AquariumStatusID
					,@AquariumStatusName
					,@AquariumStatusDescription
					,@Usable
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Insert] TO [sp_executeall]
GO
