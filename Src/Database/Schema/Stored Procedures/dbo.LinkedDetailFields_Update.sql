SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LinkedDetailFields table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LinkedDetailFields_Update]
(

	@LinkedDetailFieldID int   ,

	@LeadTypeIDTo int   ,

	@LeadTypeIDFrom int   ,

	@DetailFieldIDTo int   ,

	@DetailFieldIDFrom int   ,

	@DisplayOnPageID int   ,

	@FieldOrder int   ,

	@Enabled bit   ,

	@History bit   ,

	@LeadOrMatter int   ,

	@FieldName varchar (100)  ,

	@LeadLinkedTo int   ,

	@LeadLinkedFrom int   ,

	@IncludeLinkedField bit   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LinkedDetailFields]
				SET
					[LeadTypeIDTo] = @LeadTypeIDTo
					,[LeadTypeIDFrom] = @LeadTypeIDFrom
					,[DetailFieldIDTo] = @DetailFieldIDTo
					,[DetailFieldIDFrom] = @DetailFieldIDFrom
					,[DisplayOnPageID] = @DisplayOnPageID
					,[FieldOrder] = @FieldOrder
					,[Enabled] = @Enabled
					,[History] = @History
					,[LeadOrMatter] = @LeadOrMatter
					,[FieldName] = @FieldName
					,[LeadLinkedTo] = @LeadLinkedTo
					,[LeadLinkedFrom] = @LeadLinkedFrom
					,[IncludeLinkedField] = @IncludeLinkedField
					,[ClientID] = @ClientID
				WHERE
[LinkedDetailFieldID] = @LinkedDetailFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LinkedDetailFields_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Update] TO [sp_executeall]
GO
