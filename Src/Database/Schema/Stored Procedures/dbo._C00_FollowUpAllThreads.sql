SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Robin Hall
-- Create date: 2013-09-17
-- Description:	Follow up any dangling threads 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FollowUpAllThreads]
(
@CaseID INT
,@LeadEventID INT
)
AS
BEGIN

	DECLARE @WhenFollowedUp DATETIME
		,@EventTypeID INT
		,@ClientID INT

	-- Get time and eventtype from LeadEvent	
	SELECT @EventTypeID = le.EventTypeID, @WhenFollowedUp = le.WhenCreated, @ClientID = le.ClientID
	FROM LeadEvent le 
	WHERE le.LeadEventID = @LeadEventID
	
	IF (@ClientID IS NOT NULL AND @CaseID IS NOT NULL) -- Just don't do crazy stuff
	BEGIN
		
		-- Follow up threads
		UPDATE LeadEventThreadCompletion
		SET ToEventTypeID = @EventTypeID, ToLeadEventID = @LeadEventID
		WHERE CaseID = @CaseID
		AND ToLeadEventID IS NULL 
		AND ClientID = @ClientID
		
		-- Follow up events
		UPDATE LeadEvent
		SET WhenFollowedUp = @WhenFollowedUp, NextEventID = @LeadEventID
		WHERE CaseID = @CaseID
		AND WhenFollowedUp IS NULL
		AND ClientID = @ClientID
	
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FollowUpAllThreads] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FollowUpAllThreads] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FollowUpAllThreads] TO [sp_executeall]
GO
