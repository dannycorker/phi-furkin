SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 03/04/2018
-- Description:	Creates Account Record
-- Mods
-- 2018-03-04 ACE Made client inspecific
-- 2018-04-06 JEL Added Account Type to cater for Cheques and made Accountnum and SortCode nullable 
-- 2020-11-09 ALM SDPRU-123 Updated length of @AccSortCode and @AccNumber
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreateClaimsAccountRecord]
(
	@ClientID INT,
	@ObjectID INT,
	@ObjectTypeID INT,
	@AccNumber VARCHAR(12) = NULL,
	@AccSortCode VARCHAR(9) = NULL,
	@AccountName VARCHAR(100),
	@ClientAccountID INT = NULL,
	@CustomerID INT,
	@WhoCreated INT,
	@AccountTypeID INT,
	@BankName VARCHAR(250) = '',
	@InstitutionNumber VARCHAR(50)
)
AS
BEGIN

--declare @matterID int = 379
	SET NOCOUNT ON;

	DECLARE @AccountID INT

	-- Create a new account record in the billing system
	INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference,AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified, ObjectID, ObjectTypeID, ClientAccountID, AccountUseID, BankName, InstitutionNumber)
	VALUES (
				@ClientID,
				@CustomerID,
				@AccountName,
				'Claim Payment Account',
				@AccNumber,
				@AccSortCode,
				'CL' + CAST(@ObjectID as VARCHAR),
				@AccountTypeID,-- Bscs
				1, -- Active
				@WhoCreated,  -- who created 
				dbo.fn_GetDate_Local(),
				@WhoCreated, -- who modified
				dbo.fn_GetDate_Local(),
				@ObjectID,
				@ObjectTypeID,
				@ClientAccountID,
				2, -- Account UseID for Claims Account
				@BankName,
				@InstitutionNumber /*GPR 2021-04-29 for SDFURPHI-32*/
		   ) 
					   
	SELECT @AccountID = SCOPE_IDENTITY()
	
	SELECT @AccountID 

	RETURN @AccountID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreateClaimsAccountRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreateClaimsAccountRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreateClaimsAccountRecord] TO [sp_executeall]
GO
