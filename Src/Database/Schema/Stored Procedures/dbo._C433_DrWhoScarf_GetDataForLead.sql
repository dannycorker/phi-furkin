SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-10-18
-- Description:	Get a dataset to use for Dr Who scarf display
-- 2017-10-24 JEL changed to use @CaseID
-- 2018-03-19 GPR added £ symbol to limit amount
-- 2019-05-28 ACE - Remove redundant lead table and nolocked the others that were missing.
-- =============================================
CREATE PROCEDURE [dbo].[_C433_DrWhoScarf_GetDataForLead]
	 @CaseID			INT
	,@ClientPersonnelID	INT = NULL
AS
BEGIN

	/***************************************/
	/*                                     */
	/*                                     */
	/*                                     */
	/*       DUMMY DATA ONLY FOR DEV       */
	/*                                     */
	/*                                     */
	/*                                     */
	/***************************************/


	--DECLARE @PrimarySection	TABLE ( PrimarySectionID INT, Label VARCHAR(2000), LeftMargin VARCHAR(2000), RightMargin VARCHAR(2000) )
	--INSERT @PrimarySection ( PrimarySectionID, Label, LeftMargin, RightMargin )
	--VALUES	 ( 1, 'Vet Fees','£1500.00 per year','£1000.00 remaining' )
	--		,( 2, 'Vet Fees - Accident','£500.00 per year','£450.00 remaining' )
	--		,( 3, 'Condition - Gout','£300.00 per year','£150.00 remaining' )
	--		,( 4, 'Condition - Limp','£300.00 per year','£250.00 remaining' )
	--		,( 5, 'Condition - Epillepsy','£300.00 per year','£0.00 remaining' )
			
	--DECLARE @SecondarySection TABLE ( SecondarySectionID INT, PrimarySectionID INT, Label VARCHAR(2000), LeftMargin VARCHAR(2000), RightMargin VARCHAR(2000), MatterID INT, Claimed MONEY, Paid MONEY, Excess MONEY, Limit MONEY, Deductions MONEY, Coinsurance MONEY, NoCover MONEY, Other MONEY, BarColourHex VARCHAR(50), HoverOverMessage VARCHAR(2000), Percentage DECIMAL(18,2), PaidOn DATE )
	--INSERT @SecondarySection ( SecondarySectionID, PrimarySectionID, Label, LeftMargin, RightMargin, MatterID, Claimed, Paid, Excess, Limit, Deductions, Coinsurance, NoCover, Other, BarColourHex, HoverOverMessage, Percentage, PaidOn )
	--VALUES 	 ( 1 , 1, 'Claim 1 - Gout'		,'£150.00','',1111,150.00,150.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#f27b97', 'Gout claim for £150.00'	,		10.00	,'2017-01-01')
	--		,( 2 , 3, 'Claim 1 - Gout'		,'£150.00','',1111,150.00,150.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#f27b97', 'Gout claim for £150.00'	, 		50.00	,'2017-01-01')
	--		,( 3 , 1, 'Claim 2 - Limp'		, '£30.00','',2222, 30.00, 30.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#fc0505', 'Limp claim for £30.00'	, 		2.00	,'2017-02-10')
	--		,( 4 , 2, 'Claim 2 - Limp'		, '£30.00','',2222, 30.00, 30.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#fc0505', 'Limp claim for £30.00'	, 		6.00	,'2017-02-10')
	--		,( 5 , 4, 'Claim 2 - Limp'		, '£30.00','Some narrative here',2222, 30.00, 30.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#fc0505', 'Limp claim for £30.00'	, 		10.00	,'2017-02-10')
	--		,( 6 , 1, 'Claim 3 - Epillepsy'	,'£300.00','',3333,500.00,300.00,-50.00,-100.00,-50.00, 0.00, 0.00, 0.00,'#f27b97', 'Epillepsy claim for £300.00'	, 	20.00	,'2017-04-13')
	--		,( 7 , 5, 'Claim 3 - Epillepsy'	,'£300.00','',3333,500.00,300.00,-50.00,-100.00,-50.00, 0.00, 0.00, 0.00,'#f27b97', 'Epillepsy claim for £300.00'	, 	100.00	,'2017-04-13')
	--		,( 8 , 1, 'Claim 4 - Limp'		, '£20.00','',4444, 20.00, 20.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#fc0505', 'Limp claim for £20.00'	, 		1.33	,'2017-08-21')
	--		,( 9 , 2, 'Claim 4 - Limp'		, '£20.00','',4444, 20.00, 20.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#fc0505', 'Limp claim for £20.00'	, 		4.00	,'2017-08-21')
	--		,( 10, 4, 'Claim 4 - Limp'		, '£20.00','',4444, 20.00, 20.00, 00.00,  00.00, 00.00, 0.00, 0.00, 0.00,'#fc0505', 'Limp claim for £20.00'	, 		6.67	,'2017-08-21')


	
	--/***************************************/
	--/*                                     */
	--/*                                     */
	--/*                                     */
	--/*           CORRECT APPROACH          */
	--/*                                     */
	--/*                                     */
	--/*                                     */
	--/***************************************/

	--/*
	--Find the individual claims and their policy sections first.  These will inform the parent sections.
	--You will need one row per claim, per high-level section that it appear in.
	--Ignore the Hex Code, the control assign colours randomly.
	--*/
	
	--/*
	--Find all of the sections that are required:
	--- all PolicySections and SubSections from Table Column 144350, Resource Columns 146189/146190
	--- all distinct ailments, if the scheme imposes a condition limit
	
	--Populate LeftMargin with the annual price (or number of days, for things like overseas cover)
	--Populate LeftMargin with the remaining value (or number of days, for things like overseas cover)
	--*/
	
	--/*
	--Work out the percentage value for each sub-section.   This is the amount of the bar that will be coloured to represent this claim.
	--*/

--EXEC _C00_LogIt 'Info', '_C600_DrWhoScarf_GetDataForLead', '_C600_DrWhoScarf_GetDataForLead', @CaseID, '58552' -- GPR 2018-01-29 to check to see if Proc is called.

	-- Get the list of claim rows from all related matters
	DECLARE @ClaimRows TABLE   
	(
		ID INT,
		CustomerID INT,
		LeadID INT,
		CaseID INT,
		MatterID INT,
		ParentID INT,
		TableRowID INT,
		ResourceListID INT,
		ParentResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		ConditionID INT,
		Condition VARCHAR(200),
		DateOfLoss DATE,
		TreatmentStart DATE,
		TreatmentEnd DATE,
		Claim MONEY,
		UserDeductions MONEY,
		Settle MONEY,
		Total MONEY,
		Paid DATE,
		OutsidePolicyCover MONEY,
		Excess MONEY,
		VoluntaryExcess MONEY,
		ExcessRebate MONEY,
		CoInsurance MONEY,
		Limit MONEY,
		TotalSettle MONEY,
		LimitRLID INT,
		AfterCoInsCutIn BIT,
		ClaimRowType VARCHAR(500)

	)
	
	DECLARE  @MatterID	INT 
			,@LeadID	INT
			,@ClientID	INT
			,@RowCount	INT
			,@ID        INT
			,@ValueMoney DECIMAL (18,2)
			
	SELECT	@MatterID = MatterID, 
			@LeadID = LeadID,
			@ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID

	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@LeadID)

	DECLARE @CurrentPolicyMatterID INT ,
			@PolicyType INT 

	/*FInd the current scheme matter*/ 
	SELECT @CurrentPolicyMatterID = schmdv.MatterID  , @PolicyType = rdvType.ValueInt 
	FROM LeadDetailValues ldv WITH ( NOLOCK ) 
	INNER JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = ldv.ValueInt AND rdv.ClientID = @ClientID  AND rdv.DetailFieldID = 144269 /*Pet Type (Species)*/
	INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) on rdvType.ResourceListID = rdv.ValueInt and rdvType.DetailFieldID = 144319 and rdvType.ClientID = @ClientID 
	INNER JOIN MatterDetailValues polmdv with (NOLOCK) on polmdv.LeadID = ldv.LeadID and polmdv.DetailFieldID = 170034 /*Policy scheme RL*/  and polmdv.ClientID = @ClientID 
	INNER JOIN MatterDetailValues schmdv with (NOLOCK) on schmdv.DetailFieldID = 145689 and schmdv.ValueINT = polmdv.valueINT /*Scheme RL*/  and schmdv.ClientID = @ClientID 
	INNER JOIN MatterDetailValues sp with (NOLOCK) on sp.MatterID = schmdv.MatterID and sp.DetailFieldID = 177283 and sp.ValueINT = rdv.ValueINT /*SPecides*/ and sp.ClientID = @ClientID 
	WHERE ldv.leadID = @PolicyLeadID 
	AND ldv.ClientID = @ClientID
	AND ldv.DetailFieldID = 144272 /*Pet Type*/	
	
	/*First get all the claims rows we may care about*/ 
	;WITH ClaimRowData AS 
	(
	SELECT	ROW_NUMBER() OVER(ORDER BY CASE WHEN m.CaseID = @CaseID THEN 1 ELSE 0 END, ISNULL(tdvPaid.ValueDateTime, dbo.fn_GetDate_Local()), tdvStart.ValueDate, tdvSettle.ValueMoney DESC, r.TableRowID) AS ID, 
			tdvSettle.CustomerID, tdvSettle.LeadID, tdvSettle.CaseID, 
			tdvSettle.MatterID, m.ParentID, r.TableRowID, tdvRLID.ResourceListID, ISNULL(s.Out_ResourceListID, tdvRLID.ResourceListID) AS ParentResourceListID, mdvCondition.ValueINT as [ConditionID], rldv.DetailValue as[Condition],
			ISNULL(mdvDateOfLoss.ValueDate, tdvStart.ValueDate) AS DateOfLoss, tdvStart.ValueDate AS TreatmentStart, tdvEnd.ValueDate AS TreatmentEnd,  
			tdvClaim.ValueMoney AS Claim, tdvUserDeduct.ValueMoney AS UserDeductions, ISNULL(tdvSettle.ValueMoney, tdvClaim.ValueMoney) AS Settle, ISNULL(tdvTotal.ValueMoney,0) AS Total, 
			tdvPaid.ValueDate AS Paid, tdvOutside.ValueMoney AS OutsidePolicyCover, tdvExcess.ValueMoney AS Excess,0  AS VoluntaryExcess, 
			0 AS ExcessRebate, tdvCoInsurance.ValueMoney AS CoInsurance, tdvLimit.ValueMoney AS LIMIT, llSection.ItemValue AS SECTION, 
			llSubSection.ItemValue AS SubSection,
			CASE WHEN tdvClaimRowType.ValueInt = 50538 THEN 1 ELSE 0 END AS AfterCoInsCutIn, tdvClaimRowType.ValueInt AS ClaimRowType

	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m ON r.MatterID = m.MatterID -- get related condition claims or all if a reinstatement policy
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON m.CaseID = c.CaseID 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON tdvRLID.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON tdvRLID.ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID
	LEFT JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID AND s.ResourceListID != s.Out_ResourceListID
	INNER JOIN dbo.TableDetailValues tdvClaim WITH (NOLOCK) ON r.TableRowID = tdvClaim.TableRowID AND tdvClaim.DetailFieldID = 144353
	INNER JOIN dbo.TableDetailValues tdvUserDeduct WITH (NOLOCK) ON r.TableRowID = tdvUserDeduct.TableRowID AND tdvUserDeduct.DetailFieldID = 146179
	INNER JOIN dbo.TableDetailValues tdvSettle WITH (NOLOCK) ON r.TableRowID = tdvSettle.TableRowID AND tdvSettle.DetailFieldID = 145678
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 144349 
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 144351
	LEFT JOIN dbo.TableDetailValues tdvClaimRowType WITH (NOLOCK) ON r.TableRowID = tdvClaimRowType.TableRowID AND tdvClaimRowType.DetailFieldID = 149778 /*Claim Row Type*/
	LEFT JOIN dbo.TableDetailValues tdvPaid WITH (NOLOCK) ON r.TableRowID = tdvPaid.TableRowID AND tdvPaid.DetailFieldID = 144362 -- 144354 we now use approved and not paid
	LEFT JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	LEFT JOIN dbo.TableDetailValues tdvOutside WITH (NOLOCK) ON r.TableRowID = tdvOutside.TableRowID AND tdvOutside.DetailFieldID = 147001
	LEFT JOIN dbo.TableDetailValues tdvExcess WITH (NOLOCK) ON r.TableRowID = tdvExcess.TableRowID AND tdvExcess.DetailFieldID = 146406
	LEFT JOIN dbo.TableDetailValues tdvCoInsurance WITH (NOLOCK) ON r.TableRowID = tdvCoInsurance.TableRowID AND tdvCoInsurance.DetailFieldID = 146407
	LEFT JOIN dbo.TableDetailValues tdvLimit WITH (NOLOCK) ON r.TableRowID = tdvLimit.TableRowID AND tdvLimit.DetailFieldID = 146408
	LEFT JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892
	INNER JOIN MatterDetailValues mdvCondition WITH ( NOLOCK ) on mdvCondition.MatterID = m.MatterID and mdvCondition.DetailFieldID = 144504
	INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = mdvCondition.ValueInt and rldv.DetailFieldID = 144340
	WHERE r.DetailFieldID = 144355
	AND r.DetailFieldPageID = 16157
	AND r.ClientID = @ClientID
	AND (c.ClientStatusID IS NULL OR c.ClientStatusID NOT IN (3819)) -- Exclude rejected claims
	AND (tdvPaid.ValueDate IS NOT NULL OR c.CaseID = @CaseID) -- All paid rows and the rows from this claim
	)
	
	
	INSERT @ClaimRows (ID, CustomerID, LeadID, CaseID, MatterID, ParentID, TableRowID, ResourceListID, ParentResourceListID,ConditionID, Condition, DateOfLoss, TreatmentStart, TreatmentEnd, Claim, UserDeductions, 
						Settle, Total, Paid, OutsidePolicyCover, Excess, VoluntaryExcess, ExcessRebate, CoInsurance, Limit, Section, SubSection, AfterCoInsCutIn, ClaimRowType)
	SELECT * 
	FROM ClaimRowData
	
	--SELECT * 
	--FROM @ClaimRows
	
	/*Now pick out the details of the Matter we are on*/ 
	DECLARE @DateOfLoss DATE,
			@TreatmentStart DATE,
			@TreatmentEnd DATE ,
			@Condition INT
	
	SELECT @DateOfLoss = c.DateOfLoss, @Condition = c.ConditionID FROM @ClaimRows c 
	WHERE c.MatterID = @MatterID 

	SELECT @TreatmentStart = trstart.ValueDate, @TreatmentEnd = trend.ValueDate 
	FROM Matter m with (NOLOCK) 
	INNER JOIN MatterDetailValues trstart WITH (NOLOCK) on trstart.MatterID = m.MatterID and trstart.DetailFieldID = 144366
	INNER JOIN MatterDetailValues trend WITH (NOLOCK) on trend.MatterID = m.MatterID and trend.DetailFieldID = 145674
	WHERE m.MatterID = @MatterID
	
	/*We can have multiple sections per matter so add these to a table*/ 		
	DECLARE @Sections TABLE (ParentName VARCHAR(200),Parent INT, SubName VARCHAR(200) ,Sub INT, Done INT) 
	INSERT INTO @Sections
	SELECT c.Section, c.ParentResourceListID, c.SubSection, c.ResourceListID, 0
	FROM @ClaimRows c 
	WHERE c.MatterID = @MatterID 
	
	/*Now we need to cut out any claims that arent in cover*/ 
	
	DECLARE @PolicyCover TABLE
		(
			ID INT IDENTITY,
			PolicyType VARCHAR(50),
			StartDate DATE,
			EndDate DATE,
			InceptionDate DATE,
			SectionName VARCHAR(50),
			SectionRLID INT
			,SubSectionRLID	INT
		)


	DECLARE @TempPolicyCover TABLE 
		(	
		ID INT IDENTITY,
		PolicyType VARCHAR(50),
		StartDate DATE,
		EndDate DATE,
		InceptionDate DATE 
		)
		
	--SELECT * FROM @Sections
	
	PRINT 'Starting While'

	WHILE EXISTS (SELECT * FROM @Sections s where s.Done = 0) 
	BEGIN 
		
		PRINT 'Declare Stuff'
		
		DECLARE @SectionName VARCHAR(200),
				@SectionRLID INT,
				@SubSectionRLID INT 
				
		PRINT 'Get Sect'

		SELECT @SectionName = c.ParentName, @SectionRLID = c.Parent, @SubSectionRLID = c.Sub
		FROM @Sections c 
		WHERE Done = 0		
	
		PRINT 'Temp Insert'

		INSERT INTO @TempPolicyCover(PolicyType, StartDate, EndDate, InceptionDate)
		EXEC dbo._C00_1272_Policy_GetGroupedPolicyCover @MatterID, @LeadID, @CurrentPolicyMatterID, @PolicyType, @TreatmentStart, @TreatmentEnd
		
		PRINT 'Pol Cover Insert'

		INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate,SectionName,SectionRLID,SubSectionRLID)
		SELECT t.PolicyType,t.StartDate,t.EndDate,t.InceptionDate,@SectionName, @SectionRLID, @SubSectionRLID  FROM @TempPolicyCover t

		PRINT 'Start Update'

		Update @Sections 
		SET Done = 1 
		WHERE ParentName =@SectionName
		AND Parent = @SectionRLID
		AND Sub = @SubSectionRLID

		PRINT 'Update Complete'

	END 

	PRINT 'While Complete'
	
	/*If the claim doesnt touch the same policy periods as the current claim we don't care*/
	DELETE c 
	FROM @ClaimRows c  
	WHERE NOT EXISTS (SELECT * FROM @PolicyCover p 
	WHERE c.TreatmentStart BETWEEN p.StartDate and p.EndDate 
	AND c.TreatmentEnd BETWEEN p.StartDate and p.EndDate
	AND p.SectionName = c.Section)
	
	--SELECT * FROM @ClaimRows 

	PRINT 'Delete c'
	
	/*If the claim doesn't share a condition or parent section of the claim we are on then we should remove this*/ 
	DELETE c 
	FROM @ClaimRows c
	WHERE c.ConditionID <> @Condition
	AND NOT EXISTS (SELECT * FROM @Sections s where s.Parent = c.ParentResourceListID )
	
	--SELECT * FROM @PolicyCover 
	
	--SELECT * FROM @ClaimRows 

	PRINT 'Dec Pol Tab'

	DECLARE @PolicyLimits TABLE
	(
		ID INT IDENTITY,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		SumInsured MONEY,
		Balance MONEY,
		StartDate DATE,
		EndDate DATE,
		AllowedCount INT,
		--AllowedRemaining INT, See comment from 2015-10-22
		LimitTypeID INT,
		LimitType VARCHAR(2000)
	)
	INSERT @PolicyLimits (ResourceListID, Section, SubSection, SumInsured, AllowedCount, LimitTypeID, LimitType)
	EXEC dbo._C00_1272_Policy_GetGroupedPolicyLimits   @MatterID, @CurrentPolicyMatterID ,0
	
	
	--SELECT * 
	--FROM @PolicyLimits
	
	--/*Add the section limits for financial limits*/ 
	DECLARE @PrimarySection	TABLE ( PrimarySectionID INT IDENTITY(1,1), Label VARCHAR(2000), LeftMargin VARCHAR(2000), RightMargin VARCHAR(2000), LimitType VARCHAR(20) )
	INSERT @PrimarySection (  Label, LeftMargin, LimitType )
	SELECT  ISNULL(NULLIF(p.SubSection,'-'), p.Section), p.SumInsured, 'Financial'
	FROM @PolicyLimits p
	WHERE p.LimitTypeID = 0 
	
	/*Now for non-financial*/ 
	INSERT @PrimarySection (  Label, LeftMargin, LimitType )
	SELECT  ISNULL(NULLIF(p.SubSection,'-'),p.Section), p.AllowedCount, 'Time'
	FROM @PolicyLimits p
	WHERE p.LimitTypeID <> 0
		
	
	--SELECT * from @PrimarySection
	
	/*Add the claim detail for the section limits*/ 
	
	DECLARE @SecondarySection TABLE ( SecondarySectionID INT, PrimarySectionID INT, Label VARCHAR(2000), LeftMargin VARCHAR(2000), RightMargin VARCHAR(2000), MatterID INT, Claimed MONEY, Paid MONEY, Excess MONEY, Limit MONEY, Deductions MONEY, Coinsurance MONEY, NoCover MONEY, Other MONEY, BarColourHex VARCHAR(50), HoverOverMessage VARCHAR(2000), Percentage DECIMAL(18,2), PaidOn DATE )
	INSERT @SecondarySection ( SecondarySectionID, PrimarySectionID, Label, LeftMargin, PaidOn, MatterID, Claimed, Paid, Excess, Limit, Deductions, Coinsurance, NoCover, Other, BarColourHex, HoverOverMessage )
	SELECT ID, p.PrimarySectionID, c.Condition, '£' + CAST(c.Total as VARCHAR(20)),c.Paid,c.MatterID,c.Claim,c.Total,c.Excess,c.Limit,c.UserDeductions,c.CoInsurance,c.OutsidePolicyCover,0.00,'%' ,''   
	FROM @ClaimRows c
	INNER JOIN @PrimarySection p  on p.Label = c.SubSection
	WHERE p.LimitType = 'Financial'

	INSERT @SecondarySection ( SecondarySectionID, PrimarySectionID, Label, LeftMargin, PaidOn, MatterID, Claimed, Paid, Excess, Limit, Deductions, Coinsurance, NoCover, Other, BarColourHex, HoverOverMessage )
	SELECT ID, p.PrimarySectionID, c.Condition, '£' + CAST(c.Total as VARCHAR(20)),c.Paid,c.MatterID,c.Claim,c.Total,c.Excess,c.Limit,c.UserDeductions,c.CoInsurance,c.OutsidePolicyCover,0.00,'%' ,''   
	FROM @ClaimRows c
	INNER JOIN @PrimarySection p  on p.Label = c.Section
	WHERE p.LimitType = 'Financial'
	--Select * FROM @SecondarySection
	
	/*Condition limits*/
		
	DECLARE @Conditions TABLE (ConditionID INT, ParentRL INT, Condition VARCHAR(200)) 
	INSERT INTO @Conditions (ConditionID,ParentRL,Condition)
	SELECT DISTINCT c.ConditionID, c.ParentResourceListID, c.Condition FROM @ClaimRows c 

	INSERT @PrimarySection (  Label, LeftMargin, LimitType)
	SELECT  'Condition - ' + p.Condition, '£' + tdvLimit.ValueMoney, 'Condition'
	FROM TableRows tr WITH ( NOLOCK )
	INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 176984 /*Policy Sections*/
	INNER JOIN ResourceListDetailValues rdvSection WITH ( NOLOCK ) on rdvSection.ResourceListID = tdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
	INNER JOIN LookupListItems liSection WITH ( NOLOCK ) on liSection.LookupListItemID = rdvSection.ValueInt
	INNER JOIN TableDetailValues tdvLimit WITH ( NOLOCK ) on tdvLimit.TableRowID = tr.TableRowID AND tdvLimit.DetailFieldID = 176985 /*Limit*/
	INNER JOIN @Conditions p on p.ParentRL = rdvSection.ResourceListID
	WHERE tr.MatterID = @CurrentPolicyMatterID
	AND tr.DetailFieldID = 176986 /*Condition Limits*/
	
		
	--DECLARE @SecondarySection TABLE ( SecondarySectionID INT, PrimarySectionID INT, Label VARCHAR(2000), LeftMargin VARCHAR(2000), RightMargin VARCHAR(2000), MatterID INT, Claimed MONEY, Paid MONEY, Excess MONEY, Limit MONEY, Deductions MONEY, Coinsurance MONEY, NoCover MONEY, Other MONEY, BarColourHex VARCHAR(50), HoverOverMessage VARCHAR(2000), Percentage DECIMAL(18,2), PaidOn DATE )
	INSERT @SecondarySection ( SecondarySectionID, PrimarySectionID, Label, LeftMargin, PaidOn, MatterID, Claimed, Paid, Excess, Limit, Deductions, Coinsurance, NoCover, Other, BarColourHex, HoverOverMessage )
	SELECT ID, p.PrimarySectionID, c.Condition, '£' + CAST(c.Total as VARCHAR(20)),c.Paid,c.MatterID,c.Claim,c.Total,c.Excess, c.Limit,c.UserDeductions,c.CoInsurance,c.OutsidePolicyCover,0.00,'%' ,''   
	FROM @ClaimRows c
	INNER JOIN @PrimarySection p  on p.Label = 'Condition - ' + c.Condition
	WHERE p.LimitType = 'Condition'

	INSERT @SecondarySection ( SecondarySectionID, PrimarySectionID, Label, LeftMargin, PaidOn, MatterID, Claimed, Paid, Excess, Limit, Deductions, Coinsurance, NoCover, Other, BarColourHex, HoverOverMessage )
	SELECT ID, p.PrimarySectionID, c.Condition, DATEDIFF(DAY,c.TreatmentStart,c.TreatmentEnd) ,c.Paid,c.MatterID,c.Claim,c.Total,c.Excess, c.Limit,c.UserDeductions,c.CoInsurance,c.OutsidePolicyCover,0.00,'%' ,''   
	FROM @ClaimRows c
	INNER JOIN @PrimarySection p  on p.Label = c.SubSection
	WHERE p.LimitType = 'Time'


	WHILE EXISTS (SELECT * FROM @PrimarySection p where p.RightMargin IS NULL and p.LimitType IN ('Financial','Condition')) 
	BEGIN
		
		SELECT TOP 1 @ID = p.PrimarySectionID FROM @PrimarySection p 
		WHERE p.RightMargin IS NULL 
		
		SELECT @ValueMoney = SUM(s.Paid) 
		FROM @SecondarySection s 
		WHERE s.PrimarySectionID = @ID
		
		SELECT @ValueMoney = p.LeftMargin - @ValueMoney
		FROM @PrimarySection p 
		Where p.PrimarySectionID = @ID
		
		UPDATE p
		SET RightMargin = '£' + CAST(ISNULL(@ValueMoney,0.00) as VARCHAR(20)) + ' remaining' /*CPS 2017-11-02 added ISNULL()*/
 		FROM @PrimarySection p 
		WHERE p.PrimarySectionID = @ID 
		
		PRINT 'Limit'

		DECLARE @Limit MONEY
		SELECT @Limit = p.LeftMargin 
		FROM @PrimarySection p
		where p.PrimarySectionID = @ID
		
		/* GPR 2018-03-18 for C600 DefectID: 142 - Add £ symbol to limit amount*/
		UPDATE p
		SET LeftMargin = '£' + CAST(@Limit as VARCHAR(20))
		FROM @PrimarySection p 
		WHERE p.PrimarySectionID = @ID 
		
	
	END 
	
	/*2018-06-15 ACE Dond do this where there is no remaining balance as divide by zero error..*/
	UPDATE @SecondarySection
	SET Percentage = s.Paid/NULLIF(p.LeftMargin, '£0.00') * 100
	FROM @SecondarySection s
	INNER JOIN @PrimarySection p on p.PrimarySectionID = s.PrimarySectionID 
	WHERE p.LimitType IN ('Financial','Condition')
	AND p.RightMargin <> '£0.00 remaining'

	/* 2018-10-18 JEL yet we do still need a value for zero balance...*/ 
	UPDATE @SecondarySection
	SET Percentage = 100
	FROM @SecondarySection s
	INNER JOIN @PrimarySection p on p.PrimarySectionID = s.PrimarySectionID 
	WHERE p.LimitType IN ('Financial','Condition')
	AND p.RightMargin = '£0.00 remaining'

	SELECT @ValueMoney = NULL
	SELECT @ValueMoney = SUM(CAST(s.LeftMargin as INT)) 
	FROM @SecondarySection s 
	INNER JOIN @PrimarySection p on p.PrimarySectionID = s.PrimarySectionID
	WHERE p.LimitType = 'Time'
	
	
	UPDATE p
	SET RightMargin = CAST(CAST(p.LeftMargin as INT) - CAST(@ValueMoney as INT) as VARCHAR) + ' Days remaining'
	FROM @SecondarySection s
	INNER JOIN @PrimarySection p on p.PrimarySectionID = s.PrimarySectionID 
	WHERE p.LimitType = 'Time'
	
	UPDATE @SecondarySection
	SET Percentage = CAST(s.LeftMargin as DECIMAL(18,2))/NULLIF(CAST(p.LeftMargin as DECIMAL(18,2)), 0.00) * 100
	FROM @SecondarySection s
	INNER JOIN @PrimarySection p on p.PrimarySectionID = s.PrimarySectionID 
	WHERE p.LimitType = 'Time'
	
	SELECT PrimarySectionID, Label, LeftMargin, RightMargin
	FROM @PrimarySection ps 
	ORDER BY ps.PrimarySectionID



	SELECT SecondarySectionID, PrimarySectionID, Label, LeftMargin, RightMargin, MatterID, Claimed, Paid, Excess, Limit, Deductions, Coinsurance, NoCover, Other, BarColourHex, HoverOverMessage, Percentage, PaidOn
	FROM @SecondarySection ss
	ORDER BY ss.PrimarySectionID, ss.SecondarySectionID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C433_DrWhoScarf_GetDataForLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C433_DrWhoScarf_GetDataForLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C433_DrWhoScarf_GetDataForLead] TO [sp_executeall]
GO
