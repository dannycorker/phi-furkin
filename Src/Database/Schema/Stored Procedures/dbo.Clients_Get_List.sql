SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Clients table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_Get_List]

AS


				
				SELECT
					[ClientID],
					[CompanyName],
					[WebAddress],
					[IPAddress],
					[DefaultEmailAddress],
					[ClientTypeID],
					[AllowSMS],
					[SecurityCode],
					[LeadsBelongToOffices],
					[UseEventCosts],
					[UseEventUOEs],
					[UseEventDisbursements],
					[UseEventComments],
					[VerifyAddress],
					[UseTapi],
					[UseRPI],
					[UseGBAddress],
					[UsePinpoint],
					[UseSage],
					[UseSAS],
					[UseCreditCalculation],
					[FollowupWorkingDaysOnly],
					[AddLeadByQuestionnaire],
					[UseIncendia],
					[LanguageID],
					[CountryID],
					[UseMobileInterface],
					[UseGBValidation],
					[CurrencyID],
					[AllowSmsCommandProcessing]
				FROM
					[dbo].[Clients] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Get_List] TO [sp_executeall]
GO
