SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 
-- Description:	
-- Modified By: PR on 14-04-2016 added detail field id for AQ 4
-- Note: ClientPersonnelID appears not to be used - requires talking to ACE to find out why...
-- =============================================
CREATE PROCEDURE [dbo].[QuickSearchOptions__GetByClientPersonnelAdminGroupID]
	@ClientPersonnelAdminGroupID INT,
	@CLientPersonnelID INT,
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	/*
		SP Used to populate the QuickSearchOptions table
		Standard DDL options in the app are (Caption/Code):
		"Last name", "ln"
		"Business name", "bn"
		helper.PostcodeText, "pc"
		"Phone number", "pn"
		customerCaption + " ID", "cu"
		caseCaption + " ID", "aid"
		matterCaption + " ID", "mid"
		globalLeadAlias + " Ref", "lr"
		matterCaption + " Ref", "mr"
		"Email address", "ea"
		"LeadID", "lid"
	*/

	SELECT q.QuickSearchOptionID, q.ClientID, q.DropDownCaption, q.DropDownCode + ISNULL('_' + CONVERT(VARCHAR,q.DetailFieldID), '') AS [DropDownCode], q.SortOrder, q.DetailFieldID
	FROM QuickSearchOptions q WITH (NOLOCK)
	WHERE q.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	AND q.ClientID = @ClientID
	ORDER BY q.SortOrder

END

GO
GRANT VIEW DEFINITION ON  [dbo].[QuickSearchOptions__GetByClientPersonnelAdminGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuickSearchOptions__GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuickSearchOptions__GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
