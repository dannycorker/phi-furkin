SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2013-09-02
-- Description:	Saves the policy sections the user has selected
-- Used by:		DF 162693
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_VetEnvoy_SavePolicySections] 
(
	@LeadID INT,
	@MatterID INT,
	@ResourceListIDs dbo.TvpIntInt READONLY,
	@Debug INT = 0
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	UPDATE tdv
	SET ResourceListID = rl.ID2
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID AND tdv.DetailFieldID = 162680
	INNER JOIN @ResourceListIDs rl ON r.TableRowID = rl.ID1
	
	DECLARE @DistinctIDs dbo.TvpInt
	INSERT @DistinctIDs (AnyID)
	SELECT DISTINCT rl.ID2
	FROM @ResourceListIDs rl
	
	EXEC dbo._C00_1272_Policy_SavePolicySections @LeadID, @MatterID, @DistinctIDs, @Debug
	
	DECLARE @SummedRows TABLE
	(
		ParentRowID INT,
		FromDate DATE,
		ToDate DATE, 
		PolicySection INT,
		DetailRowID INT,
		Total MONEY
	)
	
	INSERT INTO @SummedRows (ParentRowID, FromDate, ToDate, PolicySection, DetailRowID, Total)
	SELECT r.TableRowID, tdvFrom.ValueDate, tdvTo.ValueDate, tdvSection.ResourceListID, rChild.TableRowID, tdvTotal.ValueMoney
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 144349
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 144351
	INNER JOIN dbo.TableDetailValues tdvSection WITH (NOLOCK) ON r.TableRowID = tdvSection.TableRowID AND tdvSection.DetailFieldID = 144350
	INNER JOIN @ResourceListIDs rl ON tdvSection.ResourceListID = rl.ID2
	INNER JOIN dbo.TableRows rChild WITH (NOLOCK) ON rl.ID1 = rChild.TableRowID
	INNER JOIN dbo.TableDetailValues tdvDate WITH (NOLOCK) ON rChild.TableRowID = tdvDate.TableRowID AND tdvDate.DetailFieldID = 162684
															AND tdvDate.ValueDate BETWEEN tdvFrom.ValueDate AND tdvTo.ValueDate
	INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON rChild.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 162688
	WHERE r.MatterID = @MatterID
	
	IF @Debug = 1
	BEGIN
	
		SELECT * 
		FROM @SummedRows
		
	END
	ELSE
	BEGIN
	
		-- Group by parent row ID and insert to summed total into claimed amount and settle
		DECLARE @ParentFields TABLE (FieldID INT)
		INSERT @ParentFields (FieldID) VALUES (144353), (145678), (146179)
		
		;WITH InnerSql AS 
		(
			SELECT ParentRowID, SUM(Total) AS Total
			FROM @SummedRows
			GROUP BY ParentRowID
		)

		INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, LeadID, MatterID, ClientID, DetailValue)
		SELECT	i.ParentRowID, f.FieldID, @LeadID, @MatterID, @ClientID, 
				CASE f.FieldID
					WHEN 146179 THEN '0.00'
					ELSE CAST(i.Total AS VARCHAR)
				END
		FROM InnerSql i
		CROSS JOIN @ParentFields f
		
		
		-- Insert the parent table row ID into the detail rows 	
		INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, LeadID, MatterID, ClientID, DetailValue)
		SELECT	s.DetailRowID, 162692, @LeadID, @MatterID, @ClientID, s.ParentRowID
		FROM @SummedRows s	
		
	END
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_VetEnvoy_SavePolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_VetEnvoy_SavePolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_VetEnvoy_SavePolicySections] TO [sp_executeall]
GO
