SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-28
-- Description:	Get all Function Types, and merge in
-- all Lead Types for this Client as well
-- JWG 2011-06-07 Include Client Zero lead types for ClientDetailValues, CustomerDetailValues etc
-- =============================================
CREATE PROCEDURE [dbo].[GetFunctionTypesAndLeadTypesByClient]
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	/*
		Get all fixed FunctionTypes for the tree, whose integer values will be unique.
		Exclude known troublemakers whose IDs could clash (a DetailFieldPageID could easily be the same as an EventTypeID): 
			Lead Types by Office (11)
			Pages by Lead Type   (12)
			Events by Lead Type  (13)
			Individual Lead Type (23)
			Encrypted Fields	 (25)
		
		Exclude Charms FunctionTypes by default:
			Charms				(27)
			Asset Management	(28)
			Asset Groups		(29)
			Asset				(30)
	*/
	SELECT ft.FunctionTypeID as [TreeID], ft.FunctionTypeID, ft.ModuleID, ft.ParentFunctionTypeID as [ParentTreeID], ft.FunctionTypeName, ft.[Level], -1 as [LeadTypeID] 
	FROM dbo.FunctionType ft WITH (NOLOCK) 
	WHERE ft.FunctionTypeID NOT IN (11, 12, 13, 23, 25, 27, 28, 29, 30)

	UNION
	
	/* If the ClientID has its column in [ClientOption], UseCHARMS set to true, then include Charms and relevent FunctionTypes */
	SELECT ft.FunctionTypeID as [TreeID], ft.FunctionTypeID, ft.ModuleID, ft.ParentFunctionTypeID as [ParentTreeID], ft.FunctionTypeName, ft.[Level], -1 as [LeadTypeID] 
	FROM [dbo].[FunctionType] ft WITH (NOLOCK)
	INNER JOIN [dbo].[ClientOption] co WITH (NOLOCK)
	ON @ClientID = co.[ClientID]
	WHERE ft.[FunctionTypeID] IN (27, 28, 29, 30)
	AND co.[UseCHARMS] = 1
	
	UNION

	/* Add a dynamic level of Lead Types for this client within "Lead Types" branch (7) */
	-- 2000...
	SELECT (2000000000 + lt.LeadTypeID) as [TreeID], 23 as [FunctionTypeID], 1 as [ModuleID], 7 as [ParentTreeID], lt.LeadTypeName as [FunctionTypeName], 4 as [Level], lt.LeadTypeID as [LeadTypeID]
	FROM dbo.LeadType lt WITH (NOLOCK) 
	/*WHERE LeadType.ClientID = @ClientID*/ 
	WHERE lt.ClientID IN (@ClientID, 0)

	UNION

	/* Add nested dynamic levels for Pages (12) and Events (13) and Encrypted Fields (25) within each dynamic Lead Type branch */
	-- 2012... and 2013... and 2025...
	SELECT (2000000000 + (1000000 * ft.FunctionTypeID) + lt.LeadTypeID) as [TreeID], ft.FunctionTypeID as [FunctionTypeID], 1 as [ModuleID], (2000000000 + lt.LeadTypeID) as [ParentTreeID], ft.FunctionTypeName as [FunctionTypeName], 5 as [Level], lt.LeadTypeID as [LeadTypeID]
	FROM dbo.FunctionType ft WITH (NOLOCK) 
	CROSS JOIN dbo.LeadType lt WITH (NOLOCK) 
	WHERE ft.FunctionTypeID IN (12, 13, 25)
	/*AND LeadType.ClientID = @ClientID*/
	AND lt.ClientID IN (@ClientID, 0)

	UNION

	/* Add nested dynamic levels for Client Offices within Offices by Lead Types */
	SELECT (2014000000 + co.ClientOfficeID) as [TreeID], 11 as [FunctionTypeID], 1 as [ModuleID], 8 as [ParentTreeID], co.BuildingName /*COLLATE SQL_Latin1_General_CP1_CI_AS*/ as [FunctionTypeName], 5 as [Level], -1 as [LeadTypeID]
	FROM dbo.ClientOffices co WITH (NOLOCK) 
	WHERE co.ClientID = @ClientID

	/*ORDER BY 3, 6, 4, 5, 2*/
	ORDER BY [ModuleID], [Level], [ParentTreeID], [FunctionTypeName], [FunctionTypeID]
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetFunctionTypesAndLeadTypesByClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetFunctionTypesAndLeadTypesByClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetFunctionTypesAndLeadTypesByClient] TO [sp_executeall]
GO
