SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-09-10
-- Description:	Admin dashboard account TaskFailures
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminDashboardTaskFailures]
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT at.TaskID, at.Taskname, atr.RunDate, LEFT(atr.Description, 150) AS [Error]
	FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) ON at.TaskID = atr.TaskID
	WHERE atr.RunDate > (dbo.fn_GetDate_Local() - 1) 
	--AND atr.RecordCount = -1 AMG removed as incorrect and added line below.
	AND atr.Complete = 0
	AND (at.NextRunDateTime IS NULL OR at.NextRunDateTime > dbo.fn_GetDate_Local())
	AND atr.ClientID = @ClientID 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.AutomatedTaskResult atr2 WITH (NOLOCK) 
		WHERE atr2.TaskID = atr.TaskID 
		AND atr2.AutomatedTaskResultID > atr.AutomatedTaskResultID 
		AND atr2.RecordCount > -1 
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardTaskFailures] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAdminDashboardTaskFailures] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardTaskFailures] TO [sp_executeall]
GO
