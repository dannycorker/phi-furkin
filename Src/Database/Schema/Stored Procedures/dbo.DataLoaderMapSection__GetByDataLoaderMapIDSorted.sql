SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderMapSection table through a foreign key
-- JWG 2012-01-26 Join to DataLoaderObjectType so that we can specify a sort order in which the objects will be processed (Customer, Lead, Case etc)
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection__GetByDataLoaderMapIDSorted]
(
	@DataLoaderMapID int   
)
AS
BEGIN
	
	SELECT
		d.*
	FROM
		[dbo].[DataLoaderMapSection] d WITH (NOLOCK) 
		INNER JOIN dbo.DataLoaderObjectType dlot WITH (NOLOCK) ON dlot.DataLoaderObjectTypeID = d.DataLoaderObjectTypeID
	WHERE
		d.[DataLoaderMapID] = @DataLoaderMapID
	ORDER BY dlot.ProcessingOrder
	
	SELECT @@ROWCOUNT

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection__GetByDataLoaderMapIDSorted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection__GetByDataLoaderMapIDSorted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection__GetByDataLoaderMapIDSorted] TO [sp_executeall]
GO
