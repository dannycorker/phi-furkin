SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DocumentTypeFolderLink table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink_Find]
(

	@SearchUsingOR bit   = null ,

	@DocumentTypeID int   = null ,

	@FolderID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DocumentTypeID]
	, [FolderID]
    FROM
	[dbo].[DocumentTypeFolderLink] WITH (NOLOCK) 
    WHERE 
	 ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([FolderID] = @FolderID OR @FolderID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DocumentTypeID]
	, [FolderID]
    FROM
	[dbo].[DocumentTypeFolderLink] WITH (NOLOCK) 
    WHERE 
	 ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([FolderID] = @FolderID AND @FolderID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Find] TO [sp_executeall]
GO
