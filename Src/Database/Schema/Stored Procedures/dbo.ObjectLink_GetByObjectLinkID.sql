SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectLink table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_GetByObjectLinkID]
(

	@ObjectLinkID int   
)
AS


				SELECT
					[ObjectLinkID],
					[FromObjectID],
					[ToObjectID],
					[ObjectTypeRelationshipID]
				FROM
					[dbo].[ObjectLink] WITH (NOLOCK) 
				WHERE
										[ObjectLinkID] = @ObjectLinkID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_GetByObjectLinkID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_GetByObjectLinkID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_GetByObjectLinkID] TO [sp_executeall]
GO
