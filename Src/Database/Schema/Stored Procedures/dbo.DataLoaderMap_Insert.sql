SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap_Insert]
(

	@DataLoaderMapID int    OUTPUT,

	@ClientID int   ,

	@MapName varchar (250)  ,

	@MapDescription varchar (2000)  ,

	@WhenCreated datetime   ,

	@CreatedBy int   ,

	@LastUpdated datetime   ,

	@UpdatedBy int   ,

	@EnabledForUse bit   ,

	@Deleted bit   ,

	@LeadTypeID int   ,

	@UseDefaultLeadForCustomer bit   ,

	@UseDefaultCaseForLead bit   ,

	@UseDefaultMatterForCase bit   
)
AS


				
				INSERT INTO [dbo].[DataLoaderMap]
					(
					[ClientID]
					,[MapName]
					,[MapDescription]
					,[WhenCreated]
					,[CreatedBy]
					,[LastUpdated]
					,[UpdatedBy]
					,[EnabledForUse]
					,[Deleted]
					,[LeadTypeID]
					,[UseDefaultLeadForCustomer]
					,[UseDefaultCaseForLead]
					,[UseDefaultMatterForCase]
					)
				VALUES
					(
					@ClientID
					,@MapName
					,@MapDescription
					,@WhenCreated
					,@CreatedBy
					,@LastUpdated
					,@UpdatedBy
					,@EnabledForUse
					,@Deleted
					,@LeadTypeID
					,@UseDefaultLeadForCustomer
					,@UseDefaultCaseForLead
					,@UseDefaultMatterForCase
					)
				-- Get the identity value
				SET @DataLoaderMapID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Insert] TO [sp_executeall]
GO
