SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 20/11/2013
-- Description:	Gets the Columns for the given table together with there data type
--				Used to dynamically create thunderhead xsd
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetTableColumnNames]

	@TableName VarChar(300)	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT COLUMN_NAME,DATA_TYPE
	FROM   information_schema.columns
	WHERE  table_name = @TableName
	ORDER  BY ordinal_position
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetTableColumnNames] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetTableColumnNames] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetTableColumnNames] TO [sp_executeall]
GO
