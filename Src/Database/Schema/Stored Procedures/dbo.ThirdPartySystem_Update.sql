SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartySystem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystem_Update]
(

	@ThirdPartySystemId int   ,

	@SystemName varchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartySystem]
				SET
					[SystemName] = @SystemName
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ThirdPartySystemId] = @ThirdPartySystemId 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Update] TO [sp_executeall]
GO
