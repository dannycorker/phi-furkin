SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateDroppedOutCustomerSmsSentDate]

@CustomerID int,
@ReminderSmsSentDate datetime

as

Update DroppedOutCustomers
SET ReminderSmsSentDate=@ReminderSmsSentDate
where CustomerID = @CustomerID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDroppedOutCustomerSmsSentDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateDroppedOutCustomerSmsSentDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDroppedOutCustomerSmsSentDate] TO [sp_executeall]
GO
