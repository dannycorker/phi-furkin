SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteria table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingTableID]
(

	@SqlQueryEditingTableID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SqlQueryEditingCriteriaID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[SqlQueryEditingColumnID],
					[CriteriaText],
					[Criteria1],
					[Criteria2],
					[CriteriaName],
					[SubQueryID],
					[SubQueryLinkType],
					[ParamValue],
					[IsSecurityClause]
				FROM
					[dbo].[SqlQueryEditingCriteria]
				WHERE
					[SqlQueryEditingTableID] = @SqlQueryEditingTableID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingTableID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingTableID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingTableID] TO [sp_executeall]
GO
