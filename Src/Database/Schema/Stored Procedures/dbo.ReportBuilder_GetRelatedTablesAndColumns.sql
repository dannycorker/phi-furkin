SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 15th July 2007
-- Description:	Gets all related tables and their columns for the provided SelectedTables list
-- JWG 2010-03-05 Prevent multiple joins to expensive views
-- =============================================
CREATE PROCEDURE [dbo].[ReportBuilder_GetRelatedTablesAndColumns]
	@SelectedTables varchar(4000) = null,
	@GetSelectedTableColumns bit = 0
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQLStatement varchar(5000)

	DECLARE @TableExclusions varchar(500), 
			@ColumnExclusions varchar(500),
			@DataTypeExclusions varchar(500),
			@OrderBy1 varchar(500)

	SET @TableExclusions = ' '
	SET @ColumnExclusions = ' AND isc.column_name NOT IN (''Password'',''Salt'',''SourceID'',''EncryptedValue'') '
	SET @DataTypeExclusions = ' AND isc.data_type NOT IN (''binary'',''image'',''ntext'',''sql_variant'',''sysname'',''text'',''timestamp'',''uniqueidentifier'',''varbinary'',''xml'') '
	SET @OrderBy1 = ' ORDER BY [NewOrExisting], rt.reporttablename, isc.column_name'

	IF @SelectedTables is null
	
	BEGIN
		SELECT @SQLStatement = 
		'SELECT rt.reporttablename AS TableName, rt.displayorder AS DisplayOrder, isc.column_name AS ColumnName, isc.data_type AS DataType, ''NEW'' as [NewOrExisting]
		FROM INFORMATION_SCHEMA.COLUMNS isc, reporttables rt
		WHERE isc.table_name = rt.reporttablename'
		+ @TableExclusions + @ColumnExclusions + @DataTypeExclusions + @OrderBy1
	END
	ELSE
	BEGIN
	
		/*
			JWG 2010-03-05
			Save users from themselves by limiting the number of joins
			to vMatterDetailValues and vLeadDetailValues (3 each max)
		*/
		IF @SelectedTables LIKE '%vMatterDetailValues2%'
		BEGIN
			SET @TableExclusions += 'AND rt.reporttablename <> ''vMatterDetailValues'' '
		END
		IF @SelectedTables LIKE '%vLeadDetailValues2%'
		BEGIN
			SET @TableExclusions += 'AND rt.reporttablename <> ''vLeadDetailValues'' '
		END
		
		/* Build a select statement based on the provided @SelectedTables list */
		SELECT @SQLStatement = 
		'SELECT rt.reporttablename AS TableName, rt.displayorder AS DisplayOrder, isc.column_name AS ColumnName, isc.data_type AS DataType, ''NEW'' as [NewOrExisting]
		FROM INFORMATION_SCHEMA.COLUMNS isc, reporttables rt, reporttablerelationships rtr
		WHERE rt.reporttablename = rtr.tablefrom 
		AND rtr.tableto IN (' + @SelectedTables + ') 
		AND isc.table_name = rt.reporttablename'
		+ @TableExclusions + @ColumnExclusions + @DataTypeExclusions 
		+ '
		UNION 
		SELECT rt.reporttablename AS TableName, rt.displayorder AS DisplayOrder, isc.column_name AS ColumnName, isc.data_type AS DataType, ''NEW'' as [NewOrExisting]
		FROM INFORMATION_SCHEMA.COLUMNS isc, reporttables rt, reporttablerelationships rtr
		WHERE rt.reporttablename = rtr.tableto 
		AND rtr.tablefrom IN (' + @SelectedTables + ') 
		AND isc.table_name = rt.reporttablename'
		+ @TableExclusions + @ColumnExclusions + @DataTypeExclusions 
		
		IF @GetSelectedTableColumns = 1
		BEGIN
			SELECT @SQLStatement = @SQLStatement + 
			' 
			UNION 
			SELECT rt.reporttablename AS TableName, rt.displayorder AS DisplayOrder, isc.column_name AS ColumnName, isc.data_type AS DataType, ''EXISTING'' as [NewOrExisting]
			FROM INFORMATION_SCHEMA.COLUMNS isc, reporttables rt, reporttablerelationships rtr
			WHERE rt.reporttablename IN (' + @SelectedTables + ')
			AND isc.table_name = rt.reporttablename'
			+ @TableExclusions + @ColumnExclusions + @DataTypeExclusions + @OrderBy1
		END

	END

	/* Debug info */
    /* PRINT @SQLStatement */

    /* Execute the SQL statement */
    EXEC(@SQLStatement)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetRelatedTablesAndColumns] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportBuilder_GetRelatedTablesAndColumns] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetRelatedTablesAndColumns] TO [sp_executeall]
GO
