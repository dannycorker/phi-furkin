SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2010-04-20
-- Description:	List all Matter fields for a Matter or Lead
-- =============================================
CREATE PROCEDURE [dbo].[m] 
	@MatterID int = NULL,
	@LeadID int = NULL
	WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;

	/* Grab a sample lead if only LeadType is specified */
	IF @MatterID IS NULL AND @LeadID IS NULL
	BEGIN
		SELECT 'Please enter MatterID or LeadID' as [Info]
	END
	ELSE
	BEGIN
		SELECT c.Fullname, m.*, l.*, lt.* 
		FROM dbo.Matter m WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.LeadID  
		INNER JOIN dbo.LeadType lt WITH (NOLOCK) on lt.LeadTypeID = l.LeadTypeID 
		INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = l.CustomerID 
		WHERE (m.MatterID = @MatterID OR @MatterID IS NULL)
		AND (m.LeadID = @LeadID OR @LeadID IS NULL)
		
		SELECT 'Other Matters for this Case' [Other Matters for this Case], m2.*
		FROM dbo.Matter m WITH (NOLOCK) 
		INNER JOIN dbo.Matter m2 WITH (NOLOCK) on m2.CaseID = m.CaseID 
		WHERE m.MatterID = @MatterID 
		and m.MatterID <> m2.MatterID
	END

END








GO
GRANT VIEW DEFINITION ON  [dbo].[m] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[m] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[m] TO [sp_executeall]
GO
