SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChangeInstance table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeInstance_Update]
(

	@ChangeInstanceID int   ,

	@ClientID int   ,

	@ChangeDefinitionID int   ,

	@ChangeTypeID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Deleted bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChangeInstance]
				SET
					[ClientID] = @ClientID
					,[ChangeDefinitionID] = @ChangeDefinitionID
					,[ChangeTypeID] = @ChangeTypeID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[Deleted] = @Deleted
				WHERE
[ChangeInstanceID] = @ChangeInstanceID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeInstance_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Update] TO [sp_executeall]
GO
