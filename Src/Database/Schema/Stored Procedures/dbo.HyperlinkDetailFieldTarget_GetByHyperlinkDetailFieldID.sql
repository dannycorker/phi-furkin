SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the HyperlinkDetailFieldTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldID]
(

	@HyperlinkDetailFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[HyperlinkDetailFieldTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[DetailFieldID],
					[TemplateTypeID],
					[DetailFieldAlias],
					[Notes]
				FROM
					[dbo].[HyperlinkDetailFieldTarget] WITH (NOLOCK) 
				WHERE
					[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldID] TO [sp_executeall]
GO
