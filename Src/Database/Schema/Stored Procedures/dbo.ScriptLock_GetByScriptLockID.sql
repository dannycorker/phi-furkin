SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptLock table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_GetByScriptLockID]
(

	@ScriptLockID int   
)
AS


				SELECT
					[ScriptLockID],
					[ScriptID],
					[ClientID],
					[WhoIsEditing],
					[EditStartedAt]
				FROM
					[dbo].[ScriptLock] WITH (NOLOCK) 
				WHERE
										[ScriptLockID] = @ScriptLockID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_GetByScriptLockID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_GetByScriptLockID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_GetByScriptLockID] TO [sp_executeall]
GO
