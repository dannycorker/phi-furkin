SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptInputType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptInputType_Get_List]

AS


				
				SELECT
					[ScriptInputTypeID],
					[InputType],
					[Template]
				FROM
					[dbo].[ScriptInputType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptInputType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Get_List] TO [sp_executeall]
GO
