SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Contact table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact_Get_List]

AS


				
				SELECT
					[ContactID],
					[ClientID],
					[CustomerID],
					[TitleID],
					[Firstname],
					[Middlename],
					[Lastname],
					[Fullname],
					[EmailAddressWork],
					[EmailAddressOther],
					[DirectDial],
					[MobilePhoneWork],
					[MobilePhoneOther],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Postcode],
					[Country],
					[OfficeID],
					[DepartmentID],
					[JobTitle],
					[Notes],
					[CountryID],
					[LanguageID],
					[WhenModified],
					[WhoModified],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Contact] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Get_List] TO [sp_executeall]
GO
