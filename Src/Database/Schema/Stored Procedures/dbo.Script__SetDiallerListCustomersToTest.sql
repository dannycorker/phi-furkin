SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-05-2015
-- Description:	Mark customers as test for a given List ID
-- ListID is a third party mapped field 4201 and is stored at the customer level
-- =============================================
CREATE PROCEDURE [dbo].[Script__SetDiallerListCustomersToTest]
	@ClientID INT,
	@ListID VARCHAR(2000)
AS
BEGIN	
	
	SET NOCOUNT ON;
	
	DECLARE @ListID_DetailFieldID INT
	
	SELECT @ListID_DetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4201 AND LeadTypeID=0 -- Customer level field

    IF @ListID_DetailFieldID>0
    BEGIN
    
		UPDATE Customers
		SET Test=1
		WHERE CustomerID IN 
			(SELECT CustomerID FROM CustomerDetailValues WITH (NOLOCK) 
				WHERE DetailFieldID = @ListID_DetailFieldID AND 
					  ClientID = @ClientID AND 
					  DetailValue = @ListID AND
					  Test = 0)
    
    END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Script__SetDiallerListCustomersToTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__SetDiallerListCustomersToTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__SetDiallerListCustomersToTest] TO [sp_executeall]
GO
