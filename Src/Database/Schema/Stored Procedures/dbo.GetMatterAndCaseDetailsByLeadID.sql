SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 10 May 2007

-- Created By: Jim
-- Purpose: Select records from the Matter, Cases and LeadStatus tables by LeadID
-- 2007-08-05 JWG ANSI Joins
-- 2007-12-04 JWG Return user-defined columns as well (if this user has rights to see fields from those pages).
-- 2010-09-30 JWG Include Resource List columns.
-- 2011-08-05 SB  Include Case-level security
-- 2014-05-13 #26507 LatestRecordsFirst 
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetMatterAndCaseDetailsByLeadID]
(

	@LeadID int, 
	@UserID int = null, 
	@CaseID int = null 
)
AS
BEGIN

	DECLARE @SqlStatement varchar(8000), 
	@CaseClause varchar(100) = '', 
	@BitFields varchar(1000) = '',
	@FieldHeader varchar(102), 
	@LookupListID int, 
	@FieldList varchar(2000) = '', 
	@TableList varchar(2000) = '', 
	@Field1 int, 
	@Field2 int, 
	@Field3 int, 
	@Field4 int, 
	@ColumnField1 int, 
	@ColumnField2 int, 
	@ColumnField3 int, 
	@ColumnField4 int, 
	@Prefix1 varchar(5) = 'mdv1', 
	@Prefix2 varchar(5) = 'mdv2', 
	@Prefix3 varchar(5) = 'mdv3', 
	@Prefix4 varchar(5) = 'mdv4',
	@ShowMatterRef bit, 
	@ShowMatterStatus bit, 
	@ShowMatterID bit,
	@LeadTypeID int, 
	@FieldTypeID int, 
	@LUTableClause varchar(100) = '', 
	@RLTableClause varchar(150) = '', 
	@LatestRecordsFirst BIT = 0, 
	@FinalSortOrder varchar(100) = ' ORDER BY a.CaseNum, m.RefLetter '
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.ClientID = cl.ClientID
	WHERE l.LeadID = @LeadID 


	SET NOCOUNT ON


	IF @CaseID > 0  
	BEGIN
		SET @CaseClause = ' AND m.CaseID = ' + convert(varchar, @CaseID)
	END

	SELECT @Field1 = Field1, @ColumnField1 = mld.Field1ColumnDetailFieldID, 
	@Field2 = Field2, @ColumnField2 = mld.Field2ColumnDetailFieldID, 
	@Field3 = Field3, @ColumnField3 = mld.Field3ColumnDetailFieldID, 
	@Field4 = Field4, @ColumnField4 = mld.Field4ColumnDetailFieldID, 
	@ShowMatterRef = mld.MatterRef, @ShowMatterStatus = mld.MatterStatus, @ShowMatterID = mld.MatterID, 
	@LeadTypeID = l.LeadTypeID 
	FROM dbo.Lead l WITH (NOLOCK) 
	LEFT JOIN dbo.MatterListDisplay mld WITH (NOLOCK) ON mld.LeadTypeID = l.LeadTypeID 
	WHERE l.LeadID = @LeadID 

	IF @ShowMatterRef IS NOT NULL
	BEGIN
		SELECT @BitFields = ',' + convert(varchar, @ShowMatterRef) + ' as ShowMatterRef,' + convert(varchar, @ShowMatterStatus) + ' as ShowMatterStatus,' + convert(varchar, @ShowMatterID) + ' as ShowMatterID'
	END

	/* Just-in-time declaration and population of @AllowedPages should speed this proc up a lot */
	IF @Field1 IS NOT NULL OR @Field2 IS NOT NULL OR @Field3 IS NOT NULL OR @Field4 IS NOT NULL
	BEGIN

		DECLARE @AllowedPages TABLE (ObjectID int, RightID int)

		/* We are only going to show this user fields from pages that they can view. */
		INSERT INTO @AllowedPages (ObjectID, RightID) 
		SELECT f.ObjectID, f.RightID 
		FROM fnDetailFieldPagesSecure(@UserID, @LeadTypeID) f

		IF @Field1 IS NOT NULL
		BEGIN
			/* Use ColumnField (ie ResourceList column) if present, else just the regular field */
			SELECT @FieldHeader = IsNull('[' + COALESCE(df_rlc.FieldCaption, df.FieldCaption) + ']',''), @LookupListID = COALESCE(df_rlc.LookupListID, df.LookupListID), @FieldTypeID = COALESCE(df_rlc.QuestionTypeID, df.QuestionTypeID)   
			FROM dbo.DetailFields df WITH (NOLOCK) 
			LEFT JOIN dbo.DetailFields df_rlc WITH (NOLOCK) ON df_rlc.DetailFieldID = @ColumnField1 
			INNER JOIN @AllowedPages ap ON df.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE df.DetailFieldID = @Field1 
			
			IF @@ROWCOUNT = 1
			BEGIN
				
				/* Table Join: MDV Standard */
				SELECT @TableList = ' LEFT JOIN dbo.MatterDetailValues mdv1 (nolock) on mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = ' + convert(varchar, @Field1)
				
				IF @ColumnField1 > 0
				BEGIN
					SELECT @Prefix1 = 'rldv1'
					/* Table Join: RLDV Standard */
					SELECT @RLTableClause = ' LEFT JOIN dbo.ResourceListDetailValues rldv1 (nolock) on rldv1.ResourceListID = mdv1.ValueInt AND rldv1.DetailFieldID = ' + convert(varchar, @ColumnField1)
				END
				
				/* Not a resource column */
				IF @LookupListID IS NULL
				BEGIN
					/* Not a lookup list */
					IF @FieldTypeID = 5
					BEGIN
						/* Date DD/MM/YYYY that needs converting */
						SELECT @FieldList = ',CASE IsNull(' + @Prefix1 + '.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(' + @Prefix1 + '.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(' + @Prefix1 + '.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(' + @Prefix1 + '.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						/* Regular field to display */
						SELECT @FieldList = ',IsNull(' + @Prefix1 + '.DetailValue,'''') as ' + @FieldHeader
					END
				END
				ELSE
				BEGIN
					/* Yes it is a lookup list */
					SELECT @FieldList = ',IsNull(luli1.ItemValue,'''') as ' + @FieldHeader
					/* Table Join: MDV LULI*/
					SELECT @LUTableClause = ' LEFT JOIN dbo.LookupListItems luli1 (nolock) ON luli1.LookupListItemID = ' + @Prefix1 + '.ValueInt '
				END
				
				/* Add in any extra joins in the right order */
				SELECT @TableList += @RLTableClause + @LUTableClause
				/* Clear them out for future use */
				SELECT @RLTableClause = '', @LUTableClause = ''
			END
		END



		IF @Field2 IS NOT NULL
		BEGIN
			/* Use ColumnField (ie ResourceList column) if present, else just the regular field */
			SELECT @FieldHeader = IsNull('[' + COALESCE(df_rlc.FieldCaption, df.FieldCaption) + ']',''), @LookupListID = COALESCE(df_rlc.LookupListID, df.LookupListID), @FieldTypeID = COALESCE(df_rlc.QuestionTypeID, df.QuestionTypeID)   
			FROM dbo.DetailFields df WITH (NOLOCK) 
			LEFT JOIN dbo.DetailFields df_rlc WITH (NOLOCK) ON df_rlc.DetailFieldID = @ColumnField2 
			INNER JOIN @AllowedPages ap ON df.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE df.DetailFieldID = @Field2 
			
			IF @@ROWCOUNT = 1
			BEGIN
				
				/* Table Join: MDV Standard */
				SELECT @TableList += ' LEFT JOIN dbo.MatterDetailValues mdv2 (nolock) on mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = ' + convert(varchar, @Field2)
				
				IF @ColumnField2 > 0
				BEGIN
					SELECT @Prefix2 = 'rldv2'
					/* Table Join: RLDV Standard */
					SELECT @RLTableClause = ' LEFT JOIN dbo.ResourceListDetailValues rldv2 (nolock) on rldv2.ResourceListID = mdv2.ValueInt AND rldv2.DetailFieldID = ' + convert(varchar, @ColumnField2)
				END
				
				/* Not a resource column */
				IF @LookupListID IS NULL
				BEGIN
					/* Not a lookup list */
					IF @FieldTypeID = 5
					BEGIN
						/* Date DD/MM/YYYY that needs converting */
			/* Use ColumnField (ie ResourceList column) if present, else just the regular field */
						SELECT @FieldList += ',CASE IsNull(' + @Prefix2 + '.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(' + @Prefix2 + '.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(' + @Prefix2 + '.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(' + @Prefix2 + '.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						/* Regular field to display */
						SELECT @FieldList += ',IsNull(' + @Prefix2 + '.DetailValue,'''') as ' + @FieldHeader
					END
				END
				ELSE
				BEGIN
					/* Yes it is a lookup list */
					SELECT @FieldList += ',IsNull(luli2.ItemValue,'''') as ' + @FieldHeader
					/* Table Join: MDV LULI*/
					SELECT @LUTableClause = ' LEFT JOIN dbo.LookupListItems luli2 (nolock) ON luli2.LookupListItemID = ' + @Prefix2 + '.ValueInt '
				END
				
				/* Add in any extra joins in the right order */
				SELECT @TableList += @RLTableClause + @LUTableClause
				/* Clear them out for future use */
				SELECT @RLTableClause = '', @LUTableClause = ''
			END
		END
		
		IF @Field3 IS NOT NULL
		BEGIN
			/* Use ColumnField (ie ResourceList column) if present, else just the regular field */
			SELECT @FieldHeader = IsNull('[' + COALESCE(df_rlc.FieldCaption, df.FieldCaption) + ']',''), @LookupListID = COALESCE(df_rlc.LookupListID, df.LookupListID), @FieldTypeID = COALESCE(df_rlc.QuestionTypeID, df.QuestionTypeID)   
			FROM dbo.DetailFields df WITH (NOLOCK) 
			LEFT JOIN dbo.DetailFields df_rlc WITH (NOLOCK) ON df_rlc.DetailFieldID = @ColumnField3 
			INNER JOIN @AllowedPages ap ON df.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE df.DetailFieldID = @Field3 
			
			IF @@ROWCOUNT = 1
			BEGIN
				
				/* Table Join: MDV Standard */
				SELECT @TableList += ' LEFT JOIN dbo.MatterDetailValues mdv3 (nolock) on mdv3.MatterID = m.MatterID AND mdv3.DetailFieldID = ' + convert(varchar, @Field3)
				
				IF @ColumnField3 > 0
				BEGIN
					SELECT @Prefix3 = 'rldv3'
					/* Table Join: RLDV Standard */
					SELECT @RLTableClause = ' LEFT JOIN dbo.ResourceListDetailValues rldv3 (nolock) on rldv3.ResourceListID = mdv3.ValueInt AND rldv3.DetailFieldID = ' + convert(varchar, @ColumnField3)
				END
				
				/* Not a resource column */
				IF @LookupListID IS NULL
				BEGIN
					/* Not a lookup list */
					IF @FieldTypeID = 5
					BEGIN
						/* Date DD/MM/YYYY that needs converting */
						SELECT @FieldList += ',CASE IsNull(' + @Prefix3 + '.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(' + @Prefix3 + '.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(' + @Prefix3 + '.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(' + @Prefix3 + '.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						/* Regular field to display */
						SELECT @FieldList += ',IsNull(' + @Prefix3 + '.DetailValue,'''') as ' + @FieldHeader
					END
				END
				ELSE
				BEGIN
					/* Yes it is a lookup list */
					SELECT @FieldList += ',IsNull(luli3.ItemValue,'''') as ' + @FieldHeader
					/* Table Join: MDV LULI*/
					SELECT @LUTableClause = ' LEFT JOIN dbo.LookupListItems luli3 (nolock) ON luli3.LookupListItemID = ' + @Prefix3 + '.ValueInt '
				END
				
				/* Add in any extra joins in the right order */
				SELECT @TableList += @RLTableClause + @LUTableClause
				/* Clear them out for future use */
				SELECT @RLTableClause = '', @LUTableClause = ''
			END
		END
		
		IF @Field4 IS NOT NULL
		BEGIN
			SELECT @FieldHeader = IsNull('[' + COALESCE(df_rlc.FieldCaption, df.FieldCaption) + ']',''), @LookupListID = COALESCE(df_rlc.LookupListID, df.LookupListID), @FieldTypeID = COALESCE(df_rlc.QuestionTypeID, df.QuestionTypeID)   
			FROM dbo.DetailFields df WITH (NOLOCK) 
			LEFT JOIN dbo.DetailFields df_rlc WITH (NOLOCK) ON df_rlc.DetailFieldID = @ColumnField4 
			INNER JOIN @AllowedPages ap ON df.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE df.DetailFieldID = @Field4 
			
			IF @@ROWCOUNT = 1
			BEGIN
				
				/* Table Join: MDV Standard */
				SELECT @TableList += ' LEFT JOIN dbo.MatterDetailValues mdv4 (nolock) on mdv4.MatterID = m.MatterID AND mdv4.DetailFieldID = ' + convert(varchar, @Field4)
				
				IF @ColumnField4 > 0
				BEGIN
					SELECT @Prefix4 = 'rldv4'
					/* Table Join: RLDV Standard */
					SELECT @RLTableClause = ' LEFT JOIN dbo.ResourceListDetailValues rldv4 (nolock) on rldv4.ResourceListID = mdv4.ValueInt AND rldv4.DetailFieldID = ' + convert(varchar, @ColumnField4)
				END
				
				/* Not a resource column */
				IF @LookupListID IS NULL
				BEGIN
					/* Not a lookup list */
					IF @FieldTypeID = 5
					BEGIN
						/* Date DD/MM/YYYY that needs converting */
						SELECT @FieldList += ',CASE IsNull(' + @Prefix4 + '.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(' + @Prefix4 + '.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(' + @Prefix4 + '.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(' + @Prefix4 + '.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						/* Regular field to display */
						SELECT @FieldList += ',IsNull(' + @Prefix4 + '.DetailValue,'''') as ' + @FieldHeader
					END
				END
				ELSE
				BEGIN
					/* Yes it is a lookup list */
					SELECT @FieldList += ',IsNull(luli4.ItemValue,'''') as ' + @FieldHeader
					/* Table Join: MDV LULI*/
					SELECT @LUTableClause = ' LEFT JOIN dbo.LookupListItems luli4 (nolock) ON luli4.LookupListItemID = ' + @Prefix4 + '.ValueInt '
				END
				
				/* Add in any extra joins in the right order */
				SELECT @TableList += @RLTableClause + @LUTableClause
				/* Clear them out for future use */
				SELECT @RLTableClause = '', @LUTableClause = ''
			END
		END
			
	END
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	IF @LatestRecordsFirst = 1
	BEGIN
		SELECT @FinalSortOrder = ' ORDER BY a.CaseNum DESC, m.RefLetter DESC'
	END
	
	SELECT @SqlStatement = 'SELECT m.MatterID,m.ClientID,m.MatterRef AS [Matter Ref],m.CustomerID,m.LeadID,m.MatterStatus,
	m.RefLetter AS [Ref Letter],m.BrandNew,m.CaseID,a.CaseNum,a.CaseRef,IsNull(ls.StatusName, '''') as CaseStatusName ' + @BitFields + @FieldList + ' FROM dbo.Lead l (nolock) INNER JOIN dbo.Matter m (nolock) ON l.LeadID = m.LeadID
	INNER JOIN dbo.Cases a (nolock) ON m.CaseID = a.CaseID 
	INNER JOIN dbo.fnGetUserLeadCaseAccess(' + CAST(@UserID AS VARCHAR) + ', ' + CAST(@LeadID AS VARCHAR) + ') ca ON a.CaseID = ca.CaseID
	LEFT JOIN dbo.LeadStatus ls (nolock) ON a.ClientStatusID = ls.StatusID ' + @TableList + ' WHERE m.LeadID = ' + convert(varchar,@LeadID) + @CaseClause + @FinalSortOrder /*' ORDER BY a.CaseNum, m.RefLetter '*/

	/*PRINT @FieldList*/

	/*PRINT @SqlStatement*/

	EXEC(@SqlStatement)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetMatterAndCaseDetailsByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMatterAndCaseDetailsByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMatterAndCaseDetailsByLeadID] TO [sp_executeall]
GO
