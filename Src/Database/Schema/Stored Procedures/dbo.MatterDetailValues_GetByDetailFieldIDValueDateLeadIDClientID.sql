SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_GetByDetailFieldIDValueDateLeadIDClientID]
(

	@DetailFieldID int   ,

	@ValueDate date   ,

	@LeadID int   ,

	@ClientID int   
)
AS


				SELECT
					[MatterDetailValueID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[OriginalDetailValueID],
					[OriginalLeadID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[MatterDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ValueDate] = @ValueDate
					AND [LeadID] = @LeadID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_GetByDetailFieldIDValueDateLeadIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_GetByDetailFieldIDValueDateLeadIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_GetByDetailFieldIDValueDateLeadIDClientID] TO [sp_executeall]
GO
