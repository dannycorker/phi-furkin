SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-01-14
-- Description:	Return the primary ClientID for this database via stored procedure to support code changes
-- 2020-01-14 CPS for JIRA LPC-358 | Procedure creation
-- =============================================
 
CREATE PROCEDURE [dbo].[GetPrimaryClientID]
AS
BEGIN

	DECLARE @ClientID INT

	SELECT @ClientID = dbo.fnGetPrimaryClientID()

	SELECT @ClientID [ClientID]
	RETURN @ClientID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetPrimaryClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetPrimaryClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetPrimaryClientID] TO [sp_executeall]
GO
