SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the VariableHeaderDataLoad table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[VariableHeaderDataLoad_Insert]
(

	@VariableHeaderDataLoadID int    OUTPUT,

	@ClientID int   ,

	@ListID int   ,

	@ImportID int   ,

	@TokenKey varchar (2000)  ,

	@CallbackMethod varchar (2000)  ,

	@ActionToPerform int   ,

	@DeleteListRecords bit   ,

	@EmailFailureReport bit   ,

	@EmailAddress varchar (2000)  ,

	@FileName varchar (2000)  ,

	@WhoUploaded int   ,

	@DateTimeOfUpload datetime   ,

	@Processed bit   ,

	@Notes varchar (2000)  ,

	@DateTimeStart datetime   ,

	@DateTimeEnd datetime   ,

	@LeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[VariableHeaderDataLoad]
					(
					[ClientID]
					,[ListID]
					,[ImportID]
					,[TokenKey]
					,[CallbackMethod]
					,[ActionToPerform]
					,[DeleteListRecords]
					,[EmailFailureReport]
					,[EmailAddress]
					,[FileName]
					,[WhoUploaded]
					,[DateTimeOfUpload]
					,[Processed]
					,[Notes]
					,[DateTimeStart]
					,[DateTimeEnd]
					,[LeadTypeID]
					)
				VALUES
					(
					@ClientID
					,@ListID
					,@ImportID
					,@TokenKey
					,@CallbackMethod
					,@ActionToPerform
					,@DeleteListRecords
					,@EmailFailureReport
					,@EmailAddress
					,@FileName
					,@WhoUploaded
					,@DateTimeOfUpload
					,@Processed
					,@Notes
					,@DateTimeStart
					,@DateTimeEnd
					,@LeadTypeID
					)
				-- Get the identity value
				SET @VariableHeaderDataLoadID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Insert] TO [sp_executeall]
GO
