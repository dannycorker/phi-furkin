SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientTypes_Insert]
(

	@ClientTypeID int    OUTPUT,

	@ClientTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ClientTypes]
					(
					[ClientTypeName]
					)
				VALUES
					(
					@ClientTypeName
					)
				-- Get the identity value
				SET @ClientTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientTypes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Insert] TO [sp_executeall]
GO
