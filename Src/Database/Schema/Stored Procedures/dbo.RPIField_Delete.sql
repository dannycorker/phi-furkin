SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_Delete]
(

	@RPIFieldID int   
)
AS


				DELETE FROM [dbo].[RPIField] WITH (ROWLOCK) 
				WHERE
					[RPIFieldID] = @RPIFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Delete] TO [sp_executeall]
GO
