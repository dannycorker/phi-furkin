SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-26
-- Description:	Keeps related claim data in sync
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_KeepRelatedClaimsInSync] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	-- Keep the date of loss, ailment, description of loss, body part and sap ref in sync
	DECLARE @Ailment VARCHAR(2000),
			@DateOfOnset VARCHAR(2000),
			@DescriptionOfLoss VARCHAR(2000),
			@BodyPart VARCHAR(2000)
			
	SELECT @Ailment = DetailValue 
	FROM dbo.MatterDetailValues WITH (NOLOCK)
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144504
	
	SELECT @DateOfOnset = DetailValue 
	FROM dbo.MatterDetailValues WITH (NOLOCK)
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144892
	
	SELECT @DescriptionOfLoss = DetailValue 
	FROM dbo.MatterDetailValues WITH (NOLOCK)
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144332
	
	SELECT @BodyPart = DetailValue
	FROM dbo.MatterDetailValues WITH (NOLOCK)
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144333
	
	UPDATE mdv
	SET mdv.DetailValue = @Ailment
	FROM dbo.MatterDetailValues mdv
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) c ON mdv.MatterID = c.MatterID 
	WHERE mdv.DetailFieldID = 144504
	AND ClientID = @ClientID	
	AND @Ailment > ''
	
	UPDATE mdv
	SET mdv.DetailValue = @DateOfOnset
	FROM dbo.MatterDetailValues mdv
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) c ON mdv.MatterID = c.MatterID 
	WHERE mdv.DetailFieldID = 144892
	AND ClientID = @ClientID	
	AND @DateOfOnset > ''
	
	UPDATE mdv
	SET mdv.DetailValue = @DescriptionOfLoss
	FROM dbo.MatterDetailValues mdv
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) c ON mdv.MatterID = c.MatterID 
	WHERE mdv.DetailFieldID = 144332
	AND ClientID = @ClientID	
	AND LTRIM(RTRIM(mdv.DetailValue)) = ''
	and @DescriptionOfLoss > ''	
	
	UPDATE mdv
	SET mdv.DetailValue = @BodyPart
	FROM dbo.MatterDetailValues mdv
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) c ON mdv.MatterID = c.MatterID 
	WHERE mdv.DetailFieldID = 144333
	AND ClientID = @ClientID	
	AND LTRIM(RTRIM(mdv.DetailValue)) = '' 	
	AND @BodyPart > ''
	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_KeepRelatedClaimsInSync] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_KeepRelatedClaimsInSync] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_KeepRelatedClaimsInSync] TO [sp_executeall]
GO
