SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Tracking table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Tracking_Insert]
(

	@TrackingID int    OUTPUT,

	@TrackingName varchar (100)  ,

	@TrackingDescription varchar (255)  ,

	@StartDate datetime   ,

	@EndDate datetime   ,

	@ClientQuestionnaireID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[Tracking]
					(
					[TrackingName]
					,[TrackingDescription]
					,[StartDate]
					,[EndDate]
					,[ClientQuestionnaireID]
					,[ClientID]
					)
				VALUES
					(
					@TrackingName
					,@TrackingDescription
					,@StartDate
					,@EndDate
					,@ClientQuestionnaireID
					,@ClientID
					)
				-- Get the identity value
				SET @TrackingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Tracking_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Insert] TO [sp_executeall]
GO
