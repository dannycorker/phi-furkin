SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIClientCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientCredential_Insert]
(

	@RPIClientCredentialID int    OUTPUT,

	@ClientID int   ,

	@UserName varchar (250)  ,

	@Password varchar (50)  ,

	@AsUser varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[RPIClientCredential]
					(
					[ClientID]
					,[UserName]
					,[Password]
					,[AsUser]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@UserName
					,@Password
					,@AsUser
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @RPIClientCredentialID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientCredential_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Insert] TO [sp_executeall]
GO
