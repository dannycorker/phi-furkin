SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-07-26
-- Description: Deletes a full rule all the way down through the child tables... be careful!
-- Mods:
-- 2014-11-26 DCM update RuleOrder for all rules in the set with a higher RuleOrder
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Rules_Delete]
(
	@RuleID INT
)


AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @RuleSetID INT,
			@DeletedRuleOrder INT
			
	SELECT @RuleSetID=RuleSetID, @DeletedRuleOrder=RuleOrder 
	FROM RulesEngine_Rules WITH (NOLOCK) 
	WHERE RuleID=@RuleID
	
	DELETE c
	FROM dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) ON c.RuleOutputID = o.RuleOutputID
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = o.RuleID
	WHERE r.RuleID = @RuleID

	DELETE o 
	FROM dbo.RulesEngine_RuleOutputs o
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = o.RuleID
	WHERE r.RuleID = @RuleID
	
	DELETE o 
	FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON o.RuleParameterID = p.RuleParameterID
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = p.RuleID
	WHERE r.RuleID = @RuleID
	
	DELETE p 
	FROM dbo.RulesEngine_RuleParameters p
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = p.RuleID
	WHERE r.RuleID = @RuleID
	
	DELETE dbo.RulesEngine_Rules
	WHERE RuleID = @RuleID
	
	UPDATE RulesEngine_Rules
	SET RuleOrder=RuleOrder-1
	WHERE RuleSetID=@RuleSetID AND RuleOrder > @DeletedRuleOrder
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Rules_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_Delete] TO [sp_executeall]
GO
