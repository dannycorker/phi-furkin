SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[GetCustomerPhoneNumberVerification]
AS

Select PhoneNumberVerificationID, CustomerID, ClientQuestionnaireID 
From PhoneNumberVerifications WITH (NOLOCK) 


GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerPhoneNumberVerification] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerPhoneNumberVerification] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerPhoneNumberVerification] TO [sp_executeall]
GO
