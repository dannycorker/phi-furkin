SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptTag table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptTag_Insert]
(

	@ScriptTagID int    OUTPUT,

	@Tag varchar (50)  ,

	@Template varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ScriptTag]
					(
					[Tag]
					,[Template]
					)
				VALUES
					(
					@Tag
					,@Template
					)
				-- Get the identity value
				SET @ScriptTagID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptTag_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptTag_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptTag_Insert] TO [sp_executeall]
GO
