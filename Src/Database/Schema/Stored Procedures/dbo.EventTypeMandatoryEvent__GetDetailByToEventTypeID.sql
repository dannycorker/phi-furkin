SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01 February 2008

-- Created By: Aaran Gravestock
-- Purpose: Select record detail from the EventTypeMandatoryEvent table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent__GetDetailByToEventTypeID]
(
	@EventTypeID int,
	@ClientID int
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					EventTypeMandatoryEvent.[EventTypeMandatoryEventID],
					EventTypeMandatoryEvent.[ToEventTypeID],
					EventType.[EventTypeName],
					EventTypeMandatoryEvent.[RequiredEventID],
					EventTypeMandatoryEvent.[LeadTypeID],
					EventTypeMandatoryEvent.[ClientID]
				FROM
					dbo.[EventTypeMandatoryEvent]
					inner join dbo.[EventType] on dbo.[EventTypeMandatoryEvent].RequiredEventID = dbo.[EventType].EventTypeID
				WHERE
					EventTypeMandatoryEvent.[ToEventTypeID] = @EventTypeID
					AND EventTypeMandatoryEvent.[ClientID] = @ClientID
				ORDER BY
					EventType.[EventTypeName]
					
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			








GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent__GetDetailByToEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent__GetDetailByToEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent__GetDetailByToEventTypeID] TO [sp_executeall]
GO
