SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeMandatoryEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_Delete]
(

	@EventTypeMandatoryEventID int   
)
AS


				DELETE FROM [dbo].[EventTypeMandatoryEvent] WITH (ROWLOCK) 
				WHERE
					[EventTypeMandatoryEventID] = @EventTypeMandatoryEventID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Delete] TO [sp_executeall]
GO
