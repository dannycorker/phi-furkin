SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadEventThreadCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_Update]
(

	@LeadEventThreadCompletionID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@FromLeadEventID int   ,

	@FromEventTypeID int   ,

	@ThreadNumberRequired int   ,

	@ToLeadEventID int   ,

	@ToEventTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadEventThreadCompletion]
				SET
					[ClientID] = @ClientID
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[FromLeadEventID] = @FromLeadEventID
					,[FromEventTypeID] = @FromEventTypeID
					,[ThreadNumberRequired] = @ThreadNumberRequired
					,[ToLeadEventID] = @ToLeadEventID
					,[ToEventTypeID] = @ToEventTypeID
				WHERE
[LeadEventThreadCompletionID] = @LeadEventThreadCompletionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Update] TO [sp_executeall]
GO
