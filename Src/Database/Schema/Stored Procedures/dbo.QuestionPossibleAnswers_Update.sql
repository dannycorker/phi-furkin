SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuestionPossibleAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_Update]
(

	@QuestionPossibleAnswerID int   ,

	@ClientQuestionnaireID int   ,

	@MasterQuestionID int   ,

	@AnswerText varchar (2000)  ,

	@Branch int   ,

	@LinkedQuestionnaireQuestionPossibleAnswerID int   ,

	@ClientID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuestionPossibleAnswers]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[MasterQuestionID] = @MasterQuestionID
					,[AnswerText] = @AnswerText
					,[Branch] = @Branch
					,[LinkedQuestionnaireQuestionPossibleAnswerID] = @LinkedQuestionnaireQuestionPossibleAnswerID
					,[ClientID] = @ClientID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[QuestionPossibleAnswerID] = @QuestionPossibleAnswerID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Update] TO [sp_executeall]
GO
