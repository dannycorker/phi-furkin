SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-03-09
-- Description:	Request Mandates for Accounts with no Mandate
-- 2018-03-23 ACE Updated to cancel mandates once an account has expired.
-- 2018-03-24 JEL Fixed output tables to work with output proc
-- 2019-09-23 NSD Remove Ampersand from Account Reference and Name as to not create issue with file
-- 2019-10-14 CPS for Environment Setup | Changed @RequestDate to DateTime because it gets stored in DateTime fields.  Added WhoModified, WhenModified.
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[Billing__RequestAccountMandates]
	@ClientID INT,
	@ClientAccountID INT,
	@RunAsUserID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CountryID INT

	/*GPR 2020-03-02 for AAG-202*/
	SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
	WHERE cl.ClientID = @ClientID
	
	IF @CountryID = 14 /*Australia*/
	BEGIN

		RETURN
	
	END
	ELSE
	BEGIN


	DECLARE @RequestDate DATETIME = dbo.fn_GetDate_Local(),
			@EventToAdd INT,
			@CancelEventToAdd INT

	CREATE TABLE #MandateRequests (AccountID INT, AccountMandateID INT, TransactionCode VARCHAR(100))

	CREATE TABLE #MandatesToCancel (AccountID INT)

	BEGIN TRY
		BEGIN TRAN

		SELECT @EventToAdd = EventToApply
		FROM BillingEventConfig bce WITH (NOLOCK)
		WHERE bce.ClientID = @ClientID
		AND bce.ClientAccountID = @ClientAccountID
		AND bce.FileGenerationType = 2
	
		SELECT @CancelEventToAdd = EventToApply
		FROM BillingEventConfig bce WITH (NOLOCK)
		WHERE bce.ClientID = @ClientID
		AND bce.ClientAccountID = @ClientAccountID
		AND bce.FileGenerationType = 4

		/*Remove Ampersand from Reference and AccountHolderName */
		UPDATE a
		SET Reference = REPLACE(Reference, '&', ''), AccountHolderName  = REPLACE(AccountHolderName, '&', '')
		FROM Account a WITH (NOLOCK)
		WHERE (a.Reference LIKE '%&%' or a.AccountHolderName like '%&%')
	
	
		/*
			1. Get all live bank type accounts without live mandates
			2. Add the mandate request to the mandate table for each account
			3. Add an event to each collections matters based on the account object ID.
		*/
		INSERT INTO AccountMandate ([ClientID], [CustomerID], [AccountID], [MandateStatusID], [DateRequested], [FirstAcceptablePaymentDate], [DirectDebitInstructionID], [Reference], [FailureCode], [FailureDate], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified])
		OUTPUT inserted.AccountID, inserted.AccountMandateID, '0N' AS [TransactionCode] INTO #MandateRequests
		SELECT a.ClientID, a.CustomerID, a.AccountID, 2, @RequestDate, dbo.fn_C00_CalculateActualCollectionDate(a.AccountID, 1, dbo.fn_GetDate_Local(), 4, 0.00), NULL, a.Reference, NULL, NULL, @RunAsUserID, @RequestDate, @RunAsUserID, @RequestDate
		FROM Account a WITH (NOLOCK)
		INNER JOIN Customers c with (NOLOCK) on c.CustomerID = a.CustomerID 
		WHERE a.Active = 1
		AND c.Test = 0 
		AND a.ClientID = @ClientID
		AND a.ClientAccountID = @ClientAccountID
		AND a.AccountTypeID = 1
		AND LEN(a.SortCode) = 6
		AND a.AccountUseID = 1 
		/*JEL do we need to consider account numbers greater or less than 8*/ AND LEN(a.AccountNumber) = 8
		AND NOT EXISTS (
			SELECT *
			FROM AccountMandate am WITH (NOLOCK)
			WHERE am.AccountID = a.AccountID
			AND am.MandateStatusID IN (2,3) /*Requested, Accepted*/
		)
		AND EXISTS (
			/*ACE 2018-06-15 - Only set up mandates where we have positive payments, if we only have a refund we dont need a mandate*/
			SELECT *
			FROM CustomerPaymentSchedule cps WITH (NOLOCK)
			WHERE cps.AccountID = a.AccountID
			AND cps.PaymentGross > 0.00
		)

		INSERT INTO #MandatesToCancel (AccountID)
		SELECT a.AccountID
		FROM Account a WITH (NOLOCK)
		WHERE a.Active = 1
		AND a.ExpiryDate <= dbo.fn_GetDate_Local()
		AND a.ClientAccountID = @ClientAccountID
		AND EXISTS (
			SELECT *
			FROM AccountMandate am WITH (NOLOCK)
			WHERE am.AccountID = a.AccountID
			AND am.MandateStatusID IN (2,3) /*Requested, Accepted*/
		)

		/*Set the mandates to cancelled!*/
		UPDATE am
		SET MandateStatusID = 5, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunAsUserID
		FROM #MandatesToCancel m
		INNER JOIN AccountMandate am ON am.AccountID = m.AccountID
		WHERE am.MandateStatusID IN (2,3)

		/*Disable the account*/
		UPDATE a
		SET Active = 0, WhenModified = dbo.fn_GetDate_Local(), WhoModified = @RunAsUserID
		FROM #MandatesToCancel m
		INNER JOIN Account a ON a.AccountID = m.AccountID
		WHERE a.Active = 1

		/*Finally add events to the event history*/
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount,EarliestRunDateTime)
		SELECT a.ClientID, a.CustomerID, m.LeadID, m.CaseID, ca.LatestNonNoteLeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @RunAsUserID, @EventToAdd, @RunAsUserID, -1, 5, 0, 5, DATEADD(SECOND,15,dbo.fn_GetDate_Local())
		FROM #MandateRequests amr
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = amr.AccountID
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = a.ObjectID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = m.CaseID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = ca.LatestNonNoteLeadEventID

		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount,EarliestRunDateTime)
		SELECT a.ClientID, a.CustomerID, m.LeadID, m.CaseID, ca.LatestNonNoteLeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @RunAsUserID, @CancelEventToAdd, @RunAsUserID, -1, 5, 0, 5, DATEADD(SECOND,15,dbo.fn_GetDate_Local())
		FROM #MandatesToCancel amr
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = amr.AccountID
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = a.ObjectID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = m.CaseID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = ca.LatestNonNoteLeadEventID

		/*
		UPDATE b
		SET TransactionCode = '0C'
		FROM #MandateRequests b
		WHERE EXISTS (
			SELECT *
			FROM AccountMandate am WITH (NOLOCK)
			WHERE am.AccountID = b.AccountID
			AND am.AccountMandateID < b.AccountMandateID
		)
		*/

		SELECT m.MatterID, amr.TransactionCode, a.AccountID, a.AccountHolderName, a.AccountNumber, a.Sortcode as [SortCode], a.Reference, a.ClientAccountID
		FROM #MandateRequests amr
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = amr.AccountID
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = a.ObjectID

		UNION

		SELECT m.MatterID, '0C', a.AccountID, a.AccountHolderName, a.AccountNumber, a.Sortcode as [SortCode], a.Reference, a.ClientAccountID
		FROM #MandatesToCancel amr
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = amr.AccountID
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = a.ObjectID

		COMMIT

	END TRY
	BEGIN CATCH
		
		ROLLBACK

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		
	END CATCH

	DROP TABLE #MandateRequests
	DROP TABLE #MandatesToCancel

	/*JEL changed Client ID*/ 
	/*ACE Changed to correct proc name..*/
	EXEC dbo._C00_LogIt 'Info', 'Billing__RequestAccountMandates', 'Proc Complete', '', @RunasUserID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__RequestAccountMandates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__RequestAccountMandates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__RequestAccountMandates] TO [sp_executeall]
GO
