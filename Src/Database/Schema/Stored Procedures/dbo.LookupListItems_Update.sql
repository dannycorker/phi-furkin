SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LookupListItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_Update]
(

	@LookupListItemID int   ,

	@LookupListID int   ,

	@ItemValue varchar (500)  ,

	@ClientID int   ,

	@Enabled bit   ,

	@SortOrder int   ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   ,

	@IsShared bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LookupListItems]
				SET
					[LookupListID] = @LookupListID
					,[ItemValue] = @ItemValue
					,[ClientID] = @ClientID
					,[Enabled] = @Enabled
					,[SortOrder] = @SortOrder
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[SourceID] = @SourceID
					,[IsShared] = @IsShared
				WHERE
[LookupListItemID] = @LookupListItemID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Update] TO [sp_executeall]
GO
