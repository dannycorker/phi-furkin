SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Created By: Jan Wilson
-- Date      : 2013-02-26
-- Purpose   : To retrieve all the users that a document can be escalated to
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[_C00_GetClientPersonnelForClientID]
(
	@ClientID int   
)
AS

				SET ANSI_NULLS ON
				
				SELECT
					CP.[ClientPersonnelID],
					CP.[ClientID],
					CP.[ClientOfficeID],
					CP.[TitleID],
					CP.[FirstName],
					CP.[MiddleName],
					CP.[LastName],
					CP.[JobTitle],
					CP.[Password],
					CP.[ClientPersonnelAdminGroupID],
					CP.[MobileTelephone],
					CP.[HomeTelephone],
					CP.[OfficeTelephone],
					CP.[OfficeTelephoneExtension],
					CP.[EmailAddress],
					CP.[ChargeOutRate],
					CP.[UserName],
					CP.[Salt],
					CP.[AttemptedLogins],
					CP.[AccountDisabled],
					CP.[ManagerID],
					CP.[Initials],
					CP.[LanguageID],
					CP.[SubClientID],
					CP.[ForcePasswordChangeOn],
					CP.[ThirdPartySystemId],
					CP.[CustomerID],
					CPA.GroupName
				FROM
					[dbo].[ClientPersonnel] CP WITH (NOLOCK) 
					INNER JOIN [dbo].[ClientPersonnelAdminGroups] CPA WITH (NOLOCK)
					ON CP.ClientPersonnelAdminGroupID = CPA.ClientPersonnelAdminGroupID
					
				WHERE
					CP.[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetClientPersonnelForClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetClientPersonnelForClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetClientPersonnelForClientID] TO [sp_executeall]
GO
