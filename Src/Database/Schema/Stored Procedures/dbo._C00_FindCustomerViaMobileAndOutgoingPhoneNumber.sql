SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-07-2012
-- Description:	Finds a lead based upon the given mobile number
--
-- Modified By PR 17/06/2013 Checked Client ID Against Outgoing Phone Number
-- Modified By PR 24/06/2013 Checks for the SMS Survey Start Event to ascertain the matter 
--						     where the results will be written.
-- Modified By PR 30/12/2013 Now also returns the AllowSMSCommandProcessing flag and the SubClientID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FindCustomerViaMobileAndOutgoingPhoneNumber]

	@MobilePhoneNumber VARCHAR(MAX),
	@OutgoingPhoneNumber VARCHAR(MAX)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @LeadTypeID INT, @ClientID INT, @CaseID INT, @MatterID INT, @LeadID INT
	
	SELECT TOP 1 @LeadTypeID=LeadTypeID, @ClientID=ClientID FROM SMSOutgoingPhoneNumber WITH (NOLOCK) 
	WHERE OutgoingPhoneNumber = @OutgoingPhoneNumber
	
	IF @ClientID IS NULL OR @ClientID=0
	BEGIN
		SELECT TOP 1 @ClientID=ClientID FROM SMSCommandGroup WITH (NOLOCK) 
		WHERE SMSIncomingPhoneNumber = @OutgoingPhoneNumber
	END
	
	IF @LeadTypeID IS NULL OR @LeadTypeID=0
	BEGIN
		SELECT top 1 @LeadTypeID = l.LeadTypeID 
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) on l.CustomerID = c.CustomerID
		WHERE c.MobileTelephone = @MobilePhoneNumber AND c.Test = 0 AND c.ClientID = @ClientID
		order by l.LeadID desc
	END
	
	SELECT TOP 1 @LeadID = l.leadID, @CaseID=CaseID FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND et.EventSubtypeID=24 AND et.LeadTypeID = @LeadTypeID
	INNER JOIN Customers c WITH (NOLOCK) On c.MobileTelephone = @MobilePhoneNumber AND c.Test = 0 AND c.ClientID = @ClientID
	INNER JOIN Lead l WITH (NOLOCK) ON le.LeadID = l.LeadID AND l.LeadTypeID=@LeadTypeID AND l.CustomerID=c.CustomerID
	WHERE le.ClientID=@ClientID ORDER BY le.WhenCreated DESC

	IF @CaseID IS NOT NULL AND @CaseID>0
	BEGIN
		
		SELECT TOP 1 MobileTelephone, c.CustomerID, @LeadID LeadID, @CaseID CaseID, c.ClientID, m.MatterID, @LeadTypeID LeadTypeID, c.AllowSmsCommandProcessing, c.SubClientID
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.LeadID=l.LeadID and ca.CaseID=@CaseID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID		
	
	END
	ELSE -- No SMS Survey Start Event Found - so use the default case and matter
	BEGIN
	
		SELECT TOP 1 MobileTelephone, c.CustomerID, l.LeadID, cs.CaseID, c.ClientID, m.MatterID, l.LeadTypeID, c.AllowSmsCommandProcessing, c.SubClientID
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID AND l.LeadTypeID = @LeadTypeID
		INNER JOIN dbo.Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID AND m.CaseID = cs.CaseID
		WHERE c.MobileTelephone = @MobilePhoneNumber		 
			  AND c.Test = 0 AND c.ClientID = @ClientID
		ORDER BY c.CustomerID DESC, l.LeadID DESC, cs.CaseID DESC, m.MatterID DESC
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerViaMobileAndOutgoingPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindCustomerViaMobileAndOutgoingPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerViaMobileAndOutgoingPhoneNumber] TO [sp_executeall]
GO
