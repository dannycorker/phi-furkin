SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChangeStepState table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStepState_Find]
(

	@SearchUsingOR bit   = null ,

	@ChangeStepStateID int   = null ,

	@StateDescription varchar (255)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChangeStepStateID]
	, [StateDescription]
    FROM
	[dbo].[ChangeStepState] WITH (NOLOCK) 
    WHERE 
	 ([ChangeStepStateID] = @ChangeStepStateID OR @ChangeStepStateID IS NULL)
	AND ([StateDescription] = @StateDescription OR @StateDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChangeStepStateID]
	, [StateDescription]
    FROM
	[dbo].[ChangeStepState] WITH (NOLOCK) 
    WHERE 
	 ([ChangeStepStateID] = @ChangeStepStateID AND @ChangeStepStateID is not null)
	OR ([StateDescription] = @StateDescription AND @StateDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStepState_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Find] TO [sp_executeall]
GO
