SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DetailFieldLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink__GetByDetailFieldIDForList]
(
	@ClientID int,
	@DetailFieldID int
)
AS
	
	SET ANSI_NULLS OFF

	SELECT		dfl.[DetailFieldLinkID],
				dfl.[ClientID],
				dfl.[DetailFieldID],
				cq.[QuestionnaireTitle], 
				mq.[QuestionText],
				qt.[Description] AS QuestionType
	FROM		[DetailFieldLink] dfl
	INNER JOIN	MasterQuestions mq ON dfl.[MasterQuestionID] = mq.[MasterQuestionID]
	INNER JOIN	ClientQuestionnaires cq ON mq.ClientQuestionnaireID = cq.ClientQuestionnaireID
	INNER JOIN	QuestionTypes qt ON mq.[QuestionTypeID] = qt.[QuestionTypeID]
	WHERE		(dfl.ClientID = @ClientID)
	AND			(dfl.DetailFieldID = @DetailFieldID)
	ORDER BY	cq.QuestionnaireTitle,
				mq.QuestionText
	
	Select @@ROWCOUNT
	SET ANSI_NULLS ON






GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink__GetByDetailFieldIDForList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink__GetByDetailFieldIDForList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink__GetByDetailFieldIDForList] TO [sp_executeall]
GO
