SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the VariableHeaderDataLoad table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[VariableHeaderDataLoad_Update]
(

	@VariableHeaderDataLoadID int   ,

	@ClientID int   ,

	@ListID int   ,

	@ImportID int   ,

	@TokenKey varchar (2000)  ,

	@CallbackMethod varchar (2000)  ,

	@ActionToPerform int   ,

	@DeleteListRecords bit   ,

	@EmailFailureReport bit   ,

	@EmailAddress varchar (2000)  ,

	@FileName varchar (2000)  ,

	@WhoUploaded int   ,

	@DateTimeOfUpload datetime   ,

	@Processed bit   ,

	@Notes varchar (2000)  ,

	@DateTimeStart datetime   ,

	@DateTimeEnd datetime   ,

	@LeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[VariableHeaderDataLoad]
				SET
					[ClientID] = @ClientID
					,[ListID] = @ListID
					,[ImportID] = @ImportID
					,[TokenKey] = @TokenKey
					,[CallbackMethod] = @CallbackMethod
					,[ActionToPerform] = @ActionToPerform
					,[DeleteListRecords] = @DeleteListRecords
					,[EmailFailureReport] = @EmailFailureReport
					,[EmailAddress] = @EmailAddress
					,[FileName] = @FileName
					,[WhoUploaded] = @WhoUploaded
					,[DateTimeOfUpload] = @DateTimeOfUpload
					,[Processed] = @Processed
					,[Notes] = @Notes
					,[DateTimeStart] = @DateTimeStart
					,[DateTimeEnd] = @DateTimeEnd
					,[LeadTypeID] = @LeadTypeID
				WHERE
[VariableHeaderDataLoadID] = @VariableHeaderDataLoadID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Update] TO [sp_executeall]
GO
