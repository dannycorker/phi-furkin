SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the WorkflowGroupAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroupAssignment_Get_List]

AS


				
				SELECT
					[WorkflowGroupAssignmentID],
					[WorkflowGroupID],
					[ClientPersonnelID],
					[Priority],
					[ClientID]
				FROM
					[dbo].[WorkflowGroupAssignment] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroupAssignment_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Get_List] TO [sp_executeall]
GO
