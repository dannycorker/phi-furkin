SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChangeStepState table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStepState_Delete]
(

	@ChangeStepStateID int   
)
AS


				DELETE FROM [dbo].[ChangeStepState] WITH (ROWLOCK) 
				WHERE
					[ChangeStepStateID] = @ChangeStepStateID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStepState_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Delete] TO [sp_executeall]
GO
