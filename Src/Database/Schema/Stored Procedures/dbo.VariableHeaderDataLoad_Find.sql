SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the VariableHeaderDataLoad table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[VariableHeaderDataLoad_Find]
(

	@SearchUsingOR bit   = null ,

	@VariableHeaderDataLoadID int   = null ,

	@ClientID int   = null ,

	@ListID int   = null ,

	@ImportID int   = null ,

	@TokenKey varchar (2000)  = null ,

	@CallbackMethod varchar (2000)  = null ,

	@ActionToPerform int   = null ,

	@DeleteListRecords bit   = null ,

	@EmailFailureReport bit   = null ,

	@EmailAddress varchar (2000)  = null ,

	@FileName varchar (2000)  = null ,

	@WhoUploaded int   = null ,

	@DateTimeOfUpload datetime   = null ,

	@Processed bit   = null ,

	@Notes varchar (2000)  = null ,

	@DateTimeStart datetime   = null ,

	@DateTimeEnd datetime   = null ,

	@LeadTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [VariableHeaderDataLoadID]
	, [ClientID]
	, [ListID]
	, [ImportID]
	, [TokenKey]
	, [CallbackMethod]
	, [ActionToPerform]
	, [DeleteListRecords]
	, [EmailFailureReport]
	, [EmailAddress]
	, [FileName]
	, [WhoUploaded]
	, [DateTimeOfUpload]
	, [Processed]
	, [Notes]
	, [DateTimeStart]
	, [DateTimeEnd]
	, [LeadTypeID]
    FROM
	[dbo].[VariableHeaderDataLoad] WITH (NOLOCK) 
    WHERE 
	 ([VariableHeaderDataLoadID] = @VariableHeaderDataLoadID OR @VariableHeaderDataLoadID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ListID] = @ListID OR @ListID IS NULL)
	AND ([ImportID] = @ImportID OR @ImportID IS NULL)
	AND ([TokenKey] = @TokenKey OR @TokenKey IS NULL)
	AND ([CallbackMethod] = @CallbackMethod OR @CallbackMethod IS NULL)
	AND ([ActionToPerform] = @ActionToPerform OR @ActionToPerform IS NULL)
	AND ([DeleteListRecords] = @DeleteListRecords OR @DeleteListRecords IS NULL)
	AND ([EmailFailureReport] = @EmailFailureReport OR @EmailFailureReport IS NULL)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress IS NULL)
	AND ([FileName] = @FileName OR @FileName IS NULL)
	AND ([WhoUploaded] = @WhoUploaded OR @WhoUploaded IS NULL)
	AND ([DateTimeOfUpload] = @DateTimeOfUpload OR @DateTimeOfUpload IS NULL)
	AND ([Processed] = @Processed OR @Processed IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([DateTimeStart] = @DateTimeStart OR @DateTimeStart IS NULL)
	AND ([DateTimeEnd] = @DateTimeEnd OR @DateTimeEnd IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [VariableHeaderDataLoadID]
	, [ClientID]
	, [ListID]
	, [ImportID]
	, [TokenKey]
	, [CallbackMethod]
	, [ActionToPerform]
	, [DeleteListRecords]
	, [EmailFailureReport]
	, [EmailAddress]
	, [FileName]
	, [WhoUploaded]
	, [DateTimeOfUpload]
	, [Processed]
	, [Notes]
	, [DateTimeStart]
	, [DateTimeEnd]
	, [LeadTypeID]
    FROM
	[dbo].[VariableHeaderDataLoad] WITH (NOLOCK) 
    WHERE 
	 ([VariableHeaderDataLoadID] = @VariableHeaderDataLoadID AND @VariableHeaderDataLoadID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ListID] = @ListID AND @ListID is not null)
	OR ([ImportID] = @ImportID AND @ImportID is not null)
	OR ([TokenKey] = @TokenKey AND @TokenKey is not null)
	OR ([CallbackMethod] = @CallbackMethod AND @CallbackMethod is not null)
	OR ([ActionToPerform] = @ActionToPerform AND @ActionToPerform is not null)
	OR ([DeleteListRecords] = @DeleteListRecords AND @DeleteListRecords is not null)
	OR ([EmailFailureReport] = @EmailFailureReport AND @EmailFailureReport is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([FileName] = @FileName AND @FileName is not null)
	OR ([WhoUploaded] = @WhoUploaded AND @WhoUploaded is not null)
	OR ([DateTimeOfUpload] = @DateTimeOfUpload AND @DateTimeOfUpload is not null)
	OR ([Processed] = @Processed AND @Processed is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([DateTimeStart] = @DateTimeStart AND @DateTimeStart is not null)
	OR ([DateTimeEnd] = @DateTimeEnd AND @DateTimeEnd is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_Find] TO [sp_executeall]
GO
