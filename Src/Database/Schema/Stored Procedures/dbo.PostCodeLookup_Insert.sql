SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PostCodeLookup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PostCodeLookup_Insert]
(

	@PostCodeLookupID int    OUTPUT,

	@PostCode varchar (10)  
)
AS


				
				INSERT INTO [dbo].[PostCodeLookup]
					(
					[PostCode]
					)
				VALUES
					(
					@PostCode
					)
				-- Get the identity value
				SET @PostCodeLookupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PostCodeLookup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Insert] TO [sp_executeall]
GO
