SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetPossibleAnswersForMasterQuestion    Script Date: 08/09/2006 12:22:37 ******/
/****** Modified by Paul Richardson 23/10/2013 - Added QuestionPossibleAnswerSortOrder		******/
CREATE PROCEDURE [dbo].[GetPossibleAnswersForMasterQuestion] 
(
	@MasterQuestionID INT
)
AS

	DECLARE @QuestionPossibleAnswerSortOrder INT
 	
 	SELECT 
 		@QuestionPossibleAnswerSortOrder=QuestionPossibleAnswerSortOrder 
 	FROM MasterQuestions WITH (NOLOCK) 
 	WHERE MasterQuestionID=@MasterQuestionID

	IF (@QuestionPossibleAnswerSortOrder IS NULL OR @QuestionPossibleAnswerSortOrder=0)
	BEGIN
		SELECT 
			QuestionPossibleAnswers.QuestionPossibleAnswerID, 
			QuestionPossibleAnswers.MasterQuestionID, 
			QuestionPossibleAnswers.AnswerText, 
			QuestionPossibleAnswers.Branch, 
			QuestionPossibleAnswers.ClientID 
		FROM QuestionPossibleAnswers WITH (NOLOCK) 
		WHERE QuestionPossibleAnswers.MasterQuestionID = @MasterQuestionID
	END
	ELSE IF (@QuestionPossibleAnswerSortOrder=21) -- ascending
	BEGIN
		SELECT 
			QuestionPossibleAnswers.QuestionPossibleAnswerID, 
			QuestionPossibleAnswers.MasterQuestionID, 
			QuestionPossibleAnswers.AnswerText, 
			QuestionPossibleAnswers.Branch, 
			QuestionPossibleAnswers.ClientID 
		FROM QuestionPossibleAnswers WITH (NOLOCK) 
		WHERE QuestionPossibleAnswers.MasterQuestionID = @MasterQuestionID
		ORDER BY QuestionPossibleAnswers.AnswerText ASC
	END
	ELSE IF (@QuestionPossibleAnswerSortOrder=22) -- descending
	BEGIN
		SELECT 
			QuestionPossibleAnswers.QuestionPossibleAnswerID, 
			QuestionPossibleAnswers.MasterQuestionID, 
			QuestionPossibleAnswers.AnswerText, 
			QuestionPossibleAnswers.Branch, 
			QuestionPossibleAnswers.ClientID 
		FROM QuestionPossibleAnswers WITH (NOLOCK) 
		WHERE QuestionPossibleAnswers.MasterQuestionID = @MasterQuestionID
		ORDER BY QuestionPossibleAnswers.AnswerText DESC
	END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetPossibleAnswersForMasterQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetPossibleAnswersForMasterQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetPossibleAnswersForMasterQuestion] TO [sp_executeall]
GO
