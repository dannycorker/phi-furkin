SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*
----------------------------------------------------------------------------------------------------

-- Created By:  ()
-- Purpose: Select records from the DocumentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType__GetRTFDocumentByDocumentTypeID]
(

	@DocumentTypeID int   
)
AS


				SELECT *
				FROM
					[dbo].[DocumentType]
				WHERE
					[DocumentTypeID] = @DocumentTypeID 
					AND [InputFormat] = 'RTF'

			Select @@ROWCOUNT
					
			









GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetRTFDocumentByDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetRTFDocumentByDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetRTFDocumentByDocumentTypeID] TO [sp_executeall]
GO
