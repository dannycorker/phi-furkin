SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypePaymentGateway table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_GetByEventTypePaymentGatewayID]
(

	@EventTypePaymentGatewayID int   
)
AS


				SELECT
					[EventTypePaymentGatewayID],
					[ClientID],
					[EventTypeID],
					[ClientPaymentGatewayID],
					[TransactionTypeID],
					[CardMaskingMethodID]
				FROM
					[dbo].[EventTypePaymentGateway] WITH (NOLOCK) 
				WHERE
										[EventTypePaymentGatewayID] = @EventTypePaymentGatewayID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_GetByEventTypePaymentGatewayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_GetByEventTypePaymentGatewayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_GetByEventTypePaymentGatewayID] TO [sp_executeall]
GO
