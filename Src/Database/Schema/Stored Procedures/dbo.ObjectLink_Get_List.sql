SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ObjectLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_Get_List]

AS


				
				SELECT
					[ObjectLinkID],
					[FromObjectID],
					[ToObjectID],
					[ObjectTypeRelationshipID]
				FROM
					[dbo].[ObjectLink] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Get_List] TO [sp_executeall]
GO
