SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 2008-11-03
-- Description:	Gets all matter resource list info ready for pivoting
-- =============================================

CREATE PROCEDURE [dbo].[GetResourceListMatterValuesForMultiRecipientList]

	@LeadID int,
	@DetailFieldID int
AS
BEGIN

	SELECT     

		mdv.MatterDetailValueID,
		mdv.ClientID, 
		mdv.LeadID, 
		mdv.MatterID, 
		mdv.DetailFieldID, 
		mdv.ErrorMsg, 
		mdv.OriginalDetailValueID, 
		mdv.OriginalLeadID, 
		mdv.EncryptedValue, 

		rdv.ResourceListDetailValueID, 
		rdv.ResourceListID, 
		rdv.LeadOrMatter, 
		rdv.DetailValue,

		df.FieldName, 
		df.FieldCaption, 
		df.QuestionTypeID, 
		df.Required, 
		df.Lookup, 
		df.LookupListID, 
		df.LeadTypeID, 
		df.Enabled, 
		df.DetailFieldPageID, 
		df.FieldOrder, 
		df.ResourceListDetailFieldPageID, 
		df.TableDetailFieldPageID, 
		df.Hidden, 
		df.Encrypt

		FROM         

		MatterDetailValues mdv WITH (NOLOCK)

		INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) ON rdv.ResourceListID = mdv.DetailValue 
		INNER JOIN DetailFields df WITH (NOLOCK) ON rdv.DetailFieldID = df.DetailFieldID

		WHERE		mdv.DetailFieldID = @DetailFieldID
					AND mdv.LeadID = @LeadID
					AND df.Enabled = 1
					

		ORDER BY rdv.ResourceListID, df.FieldOrder

END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListMatterValuesForMultiRecipientList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetResourceListMatterValuesForMultiRecipientList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListMatterValuesForMultiRecipientList] TO [sp_executeall]
GO
