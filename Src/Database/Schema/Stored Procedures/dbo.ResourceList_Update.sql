SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ResourceList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceList_Update]
(

	@ResourceListID int   ,

	@ClientID int   ,

	@DetailFieldPageID int   ,

	@ResourceListHelperCode varchar (100)  ,

	@ResourceListHelperName varchar (250)  ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ResourceList]
				SET
					[ClientID] = @ClientID
					,[DetailFieldPageID] = @DetailFieldPageID
					,[ResourceListHelperCode] = @ResourceListHelperCode
					,[ResourceListHelperName] = @ResourceListHelperName
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[ResourceListID] = @ResourceListID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Update] TO [sp_executeall]
GO
