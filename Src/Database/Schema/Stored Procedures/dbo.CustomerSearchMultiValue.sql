SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 2010-10-06
-- Created By:   Jim Green
-- Description:  Gets records from the Customers, Lead and Matter tables passing page index and page count params.
--               Also allows users to search for LDV/MDV/CDV/ADV as well as any properties of Customer, Case, Lead, Matter etc
--               Groups of criteria can now be "OR"ed together by giving them the same OrGroupID within tvpObjectSearch
-- Updates:		 2011-03-01 Simon Brushett - added in customer ref to basic details. NB	had to name this CustomerReference to avoid a conflict with BM
--				 2011-04-06 Simon Brushett - Fixed search for standard objects like customer where the or group id is null
--				 2011-04-19 Simon Brushett - Fixed single quotes in search terms
--				 2012-02-08 Jim Green - Fixed real date searchs (eg Customers.DateOfBirth) to use '=' instead of 'LIKE'
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[CustomerSearchMultiValue] 
(
	@UserID int, 
	@tvpObjectSearch tvpObjectSearch READONLY,
	@ClientID int, 
	@SubClientID int = null,
	@SortCol varchar (250) = 'Customers.[CustomerID]',
	@SortOrder varchar (10) = 'ASC',
	@PageIndex int = 0,
	@PageSize int = 100,
	@ShowBusiness bit = 1, 
	@ShowIndividual bit = 1, 
	@ShowContact bit = 0, 
	@ShowTests bit = 0,
	@ShowCustomerOnly tinyint = 0, 
	@LeadsBelongToOffice bit = 0,
	@ClientOfficeID int = NULL, 
	@CanViewUnassignedLeads bit = 1, 
	@CanViewLeadsAssignedToOthers bit = 1,
	@Debug bit = 0
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PageLowerBound int,
	@PageUpperBound int,
	@WhereClause varchar (2000) = 'Customers.[ClientID] = ' + cast(@ClientID AS varchar),
	@LoopSpecialCol varchar(2000)
	
	DECLARE @CustomFields TABLE (
		UniqueRowID int,
		IsFixedAquariumObject bit, /* eg Customers.LastName as opposed to MatterDetailValues */
		DataloaderObjectTypeID int,
		DataloaderObjectFieldID int NULL,
		SearchValue varchar(2000),
		OrGroupID int NULL,
		RowNo int,
		SourceTable varchar(300),
		SourceColumn varchar(250) NULL, 
		NeedsQuotes bit, 
		DetailFieldID int,
		ColumnDetailFieldID int NULL,
		PKColumnName varchar(300) NULL, 
		JoinTable varchar(300) NULL,
		JoinColumn varchar(300) NULL,
		JoinType varchar(300) NULL,
		TableAlias varchar(300),
		QuestionTypeID int NULL,
		OutputHeader varchar(250) NULL,
		DataLoaderDataTypeID int NULL
	)

	IF @SubClientID > 0
	BEGIN
		SELECT @WhereClause += ' AND Customers.[SubClientID] = ' + cast(@SubClientID AS varchar) 
	END
	
	/* Load those tvpObjectSearch records that represent DetailFields into an internal work table */
	;WITH InnerSql AS 
	(
		SELECT t.AnyID, t.SearchableObjectTypeID, t.ColumnDetailFieldID, t.SearchValue, t.OutputHeader, t.OrGroupID, ROW_NUMBER() OVER(ORDER BY t.AnyID) as rn
		FROM @tvpObjectSearch t 
		WHERE t.SearchableObjectTypeID IN (5, 6, 7) /* LDV/MDV */
	)
	INSERT INTO @CustomFields (UniqueRowID, IsFixedAquariumObject, DetailFieldID, ColumnDetailFieldID, DataloaderObjectTypeID, SearchValue, OrGroupID, RowNo, SourceTable, SourceColumn, PKColumnName, JoinTable, JoinColumn, JoinType, TableAlias, QuestionTypeID, OutputHeader) 
	SELECT rn, 0, i.AnyID, i.ColumnDetailFieldID, i.SearchableObjectTypeID, i.SearchValue, i.OrGroupID, i.rn, s.SearchableObject, 'DetailValue', s.PKColumnName, s.JoinTable, s.JoinColumn, CASE WHEN i.SearchValue = '%' THEN 'LEFT' ELSE 'INNER' END , 'V' + cast(i.rn AS varchar), COALESCE(rldf.QuestionTypeID, df.QuestionTypeID), i.OutputHeader 
	FROM InnerSql i 
	INNER JOIN dbo.SearchableObjectType s WITH (NOLOCK) ON s.SearchableObjectTypeID = i.SearchableObjectTypeID 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = i.AnyID 
	LEFT JOIN dbo.DetailFields rldf WITH (NOLOCK) ON rldf.DetailFieldID = i.ColumnDetailFieldID 

	/* Load the other tvpObjectSearch records (eg Customers.Lastname) into a separate table */
	;WITH InnerSql AS 
	(
		SELECT t.AnyID, t.SearchableObjectTypeID, t.SearchValue, t.OrGroupID, ROW_NUMBER() OVER(ORDER BY t.AnyID) as rn
		FROM @tvpObjectSearch t 
		WHERE t.SearchableObjectTypeID IN (1, 2, 3, 4) /* Customers/Lead/Cases/Matter */
	)
	INSERT INTO @CustomFields (UniqueRowID, IsFixedAquariumObject, DetailFieldID, DataloaderObjectTypeID, DataloaderObjectFieldID, SearchValue, OrGroupID, RowNo, SourceTable, SourceColumn, NeedsQuotes, DataLoaderDataTypeID) 
	SELECT -(rn), 1, i.AnyID, -(rn), i.SearchableObjectTypeID, i.SearchValue, i.OrGroupID, i.rn, s.SearchableObject, dlof.FieldName, CASE WHEN dlof.DataTypeID BETWEEN 12 AND 16 THEN 1 ELSE 0 END, dlof.DataTypeID /* datetime, char etc */
	FROM InnerSql i 
	INNER JOIN dbo.SearchableObjectType s WITH (NOLOCK) ON s.SearchableObjectTypeID = i.SearchableObjectTypeID 
	INNER JOIN dbo.DataLoaderObjectField dlof WITH (NOLOCK) ON dlof.DataLoaderObjectTypeID = i.SearchableObjectTypeID AND dlof.DataLoaderObjectFieldID = i.AnyID AND i.SearchableObjectTypeID IN (1, 2, 3, 4) /* Customers/Lead/Cases/Matter */

	/* Set the page bounds */
	SELECT @PageLowerBound = (@PageSize * @PageIndex) + 1
	SELECT @PageUpperBound = (@PageIndex + 1) * @PageSize
	
	IF @Debug = 1
	BEGIN
		SELECT * FROM @CustomFields
	END
	
	
	DECLARE @SelectPart1 varchar(max), 
	@SelectPart1Contacts varchar(max), 
	@SelectPart2Contacts varchar(max), 
	@SelectWhere varchar(max), 
	@SelectWhereContacts varchar(max), 
	@SelectPart2 varchar(max), 
	@SelectFull varchar(max), 
	@SelectFullContacts varchar(max), 
	@SpecialCols varchar(max), 
	@SpecialJoins varchar(max), 
	@SortColContacts varchar (250),
	@SortColContactsLen tinyint,
	@SortColContactsDotPos tinyint,
	@LeadBelongsToOfficeJoin varchar(250),
	@LeadBelongsToOfficeWhere varchar(250),
	@CustomersTestOrNot varchar(100),
	@NoDuplicatesClause varchar(200) = '', 
	@NoDuplicatesContactsClause varchar(200) = '' 
	
	
	/* Option to show just 1 Matter per Case / Case per Lead / Lead per Customer */
	SELECT @NoDuplicatesClause = CASE @ShowCustomerOnly 
							WHEN 1 THEN 'ROW_NUMBER() OVER(PARTITION BY Customers.[CustomerID] ORDER BY Matter.MatterID)' 
							WHEN 2 THEN 'ROW_NUMBER() OVER(PARTITION BY Lead.[LeadID] ORDER BY Matter.MatterID)' 
							WHEN 3 THEN 'ROW_NUMBER() OVER(PARTITION BY Cases.[CaseID] ORDER BY Matter.MatterID)' 
							ELSE '1' 
							END

	SELECT @NoDuplicatesContactsClause = CASE @ShowCustomerOnly 
							WHEN 1 THEN 'ROW_NUMBER() OVER(PARTITION BY [CustomerID] ORDER BY [MatterID])' 
							WHEN 2 THEN 'ROW_NUMBER() OVER(PARTITION BY [LeadID] ORDER BY [MatterID])' 
							WHEN 3 THEN 'ROW_NUMBER() OVER(PARTITION BY [CaseID] ORDER BY [MatterID])' 
							ELSE '1' 
							END 
	
	
	SELECT @SpecialCols = '', 
	@SpecialJoins = '', 
	@SelectWhere = ' WHERE Customers.[AquariumStatusID] IN (2,4,5) ',
	@SelectWhereContacts = ' WHERE Customers.[AquariumStatusID] IN (2,4,5) '
	
	
	IF LEN(@WhereClause) > 0
	BEGIN
		/* Now add the original "where" criteria to the original base "where" settings */
		SET @SelectWhere = @SelectWhere + ' AND ' + @WhereClause

		/* 
			Set up the Contacts where clause a bit differently, replacing all subsequent 
			references to "Customers" with "Contact".
		*/
		SET @SelectWhereContacts = @SelectWhereContacts + ' AND ' + replace(replace(replace(@WhereClause,'Customers.','Contact.'), 'Contact.CompanyName', 'Customers.CompanyName'), 'EmailAddress', 'EmailAddressWork')

		SET @SelectWhereContacts = replace(@SelectWhereContacts,'DayTimeTelephoneNumber','DirectDial')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'HomeTelephone','DirectDial')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'MobileTelephone','DirectDial')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'CompanyTelephone','MobilePhoneWork')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'WorksTelephone','MobilePhoneOther')

	END
	
	
	/* If Client has LeadsBelongToOffice switched on, only return the rows which belong to the current User's ClientOfficeID */
	IF @LeadsBelongToOffice = 1
	BEGIN
	
		/* 
			JWG 2010-07-01 New security option to restrict users from seeing unassigned leads. 
			Some clients want offices to see their own leads AND any new ones,
			but others want new leads to be invisible until an office has been selected, which
			happens when the lead is assigned to a user, who works at one office by definition. 
		*/
		SELECT @LeadBelongsToOfficeJoin = '	LEFT JOIN dbo.ClientOfficeLead COL (NOLOCK) on COL.LeadID = Lead.LeadID ',--AND COL.ClientOfficeID = ' + Convert(varchar(50), @ClientOfficeID) + ' ',
		@SpecialJoins = @SpecialJoins + @LeadBelongsToOfficeJoin,
		@LeadBelongsToOfficeWhere = ' AND (COL.ClientOfficeID = ' + Convert(varchar(11), @ClientOfficeID) + CASE @CanViewUnassignedLeads WHEN 1 THEN ' OR Lead.Assigned = 0) ' ELSE ')' END ,
		@SelectWhere = @SelectWhere + @LeadBelongsToOfficeWhere

		IF @Debug = 1
		BEGIN
			PRINT @SelectWhere
		END
	END

	/* 
		JWG 2010-07-01 New security option to restrict users from seeing unassigned leads. 
		This overlaps somewhat with the office security clause above, but if anyone tries to use 
		this without having "LeadsBelongToOffice" switched on, they will still expect it to work.
	*/
	IF @CanViewUnassignedLeads = 0
	BEGIN
		SELECT @SelectWhere += ' AND Lead.Assigned = 1 '
		
		SELECT @SelectWhereContacts += ' AND Lead.Assigned = 1 '
	END
	
	/* 
		JWG 2010-07-01 New security option to restrict users from seeing leads assigned to others, 
		regardless of whether or not the other user works within the same office.
	*/	
	IF @CanViewLeadsAssignedToOthers = 0
	BEGIN
		SELECT @SelectWhere += ' AND Lead.AssignedTo = ' + convert(varchar(11), @UserID)
		
		SELECT @SelectWhereContacts += ' AND Lead.AssignedTo = ' + convert(varchar(11), @UserID)
	END
	
	SELECT @SelectPart2 = '
	),
	Counts AS (
		SELECT count(*) as [TotalRows] FROM Matches
		WHERE [nodupsrowno] = 1 
	),
	RowsToReturn AS (
		SELECT Matches.*, row_number() over (order by Matches.[rowno]) as [rownotoreturn] 
		FROM Matches 
		WHERE [nodupsrowno] = 1 
	)
	SELECT Counts.TotalRows, RowsToReturn.*  
	FROM Counts 
	CROSS JOIN RowsToReturn 
	WHERE RowsToReturn.rownotoreturn BETWEEN ' + convert(varchar, @PageLowerBound) + ' AND ' + convert(varchar, @PageUpperBound) + ' 
	AND RowsToReturn.[nodupsrowno] = 1 
	ORDER BY RowsToReturn.rownotoreturn '
	
	
	/* JWG 2009-07-09 New option to let people see their test records */
	IF @ShowTests = 1
	BEGIN
		SET @CustomersTestOrNot = ''
	END
	ELSE
	BEGIN
		SET @CustomersTestOrNot = ' AND (Customers.[Test] = 0 OR Customers.[Test] IS NULL)'
	END

	/* Start the main select statement */
	SELECT @SelectPart1 = 'WITH Matches AS (
	SELECT Customers.CustomerID, Customers.CustomerRef AS CustomerReference, Customers.ClientID, Customers.TitleID, Customers.FirstName, Customers.MiddleName, Customers.LastName, Customers.Fullname as [Name], 
	Matter.LeadID as [ID], Customers.EmailAddress as [Email Address], Customers.DayTimeTelephoneNumber, Customers.HomeTelephone, Customers.MobileTelephone, Customers.Address1,
	Customers.Address2, Customers.Town, Customers.County, Customers.PostCode as [Post Code], Customers.HasDownloaded, Customers.DownloadedOn, Customers.Test,
	Customers.Occupation, Customers.Employer, ClientPersonnel.UserName as [Assigned To], Lead.LeadID, IsNull(Lead.LeadRef,'''') as [LeadRef],
	LeadType.LeadTypeName as [Lead Type], Cases.CaseID, Cases.CaseNum as [Case], Matter.MatterID, Customers.IsBusiness, IsNull(Titles.Title,'''') as [Title],
	IsNull(LeadStatus.StatusName,'''') as [Status], AquariumStatus.AquariumStatusName, Matter.RefLetter as [Ref], convert(char(10), Cases.WhenCreated, 103) as [WhenCreated],  
	'
	
	
	/* 
		JWG 2010-11-17
		Merge in special fields if the user has specified any 
		The final query will end up with extra joins and clauses something like this:
		
		LEFT JOIN dbo.MatterDetailValues V1 (NOLOCK) on V1.MatterID = Matter.MatterID AND V1.DetailFieldID = 117771 
		LEFT JOIN dbo.ResourceListDetailValues rldvV1 WITH (NOLOCK) ON rldvV1.ResourceListID = V1.ValueInt AND rldvV1.DetailFieldID = 117761
		LEFT JOIN dbo.LeadDetailValues V2 (NOLOCK) on V2.LeadID = Lead.LeadID AND V2.DetailFieldID = 117895  
		WHERE Customers.[AquariumStatusID] IN (2,4,5)  
		AND Customers.[ClientID] = 183 
		AND Customers.[SubClientID] = 1 
		AND ( (rldvV1.DetailValue LIKE 'Ch%') )  
		AND ( (V2.DetailValue IS NULL OR V2.DetailValue LIKE '%') OR (etc etc etc) ) 
		
		
	*/

	/* Detail Fields */
	DECLARE @UniqueRowID int,  
	@FieldID int, 
	@ColumnFieldID int, 
	@QuestionTypeID int, 
	@OrGroupID int, 
	@LastOrGroupID int = -1, 
	@SourceTable varchar(300),
	@SourceColumn varchar(300),
	@JoinTable varchar(300),
	@JoinColumn varchar(300),
	@JoinType varchar(300),
	@TableAlias varchar(300), 
	@SearchValue varchar(2000),
	@OutputHeader varchar(250),
	@SpecialColumn varchar(1000),
	@SpecialJoin varchar(2000),
	@BracketsAreOpen bit = 0,
	@IsFixedAquariumObject bit,
	@NeedsQuotes bit,
	@DataLoaderDataTypeID int
	
	/* Custom search fields now */
	WHILE EXISTS (SELECT * FROM @CustomFields)
	BEGIN
		SELECT TOP (1) 
		@UniqueRowID = f.UniqueRowID,
		@IsFixedAquariumObject = f.IsFixedAquariumObject,
		@FieldID = f.DetailFieldID, 
		@ColumnFieldID = f.ColumnDetailFieldID, 
		@QuestionTypeID = f.QuestionTypeID,
		@OrGroupID = f.OrGroupID, 
		@SourceTable = f.SourceTable,
		@SourceColumn = f.SourceColumn,
		@JoinTable = f.JoinTable,
		@JoinColumn = f.JoinColumn,
		@JoinType = f.JoinType,
		@TableAlias = f.TableAlias, 
		@OutputHeader = f.OutputHeader, 
		@SearchValue = QUOTENAME(f.SearchValue, ''''), 
		@NeedsQuotes = f.NeedsQuotes, 
		@DataLoaderDataTypeID = f.DataLoaderDataTypeID 
		FROM @CustomFields f 
		ORDER BY f.OrGroupID, f.DetailFieldID 

		IF @Debug = 1
		BEGIN		
			SELECT 
			@IsFixedAquariumObject,
			@FieldID, 
			@ColumnFieldID, 
			@QuestionTypeID,
			@OrGroupID, 
			@SourceTable,
			@SourceColumn,
			@JoinTable,
			@JoinColumn,
			@JoinType,
			@TableAlias, 
			@OutputHeader, 
			@SearchValue
		END
						
		IF @IsFixedAquariumObject = 1
		BEGIN
		 
			SELECT TOP (1) 
			@LoopSpecialCol = @SourceTable + '.' + @SourceColumn,  
			--@SpecialColumn = CASE @NeedsQuotes WHEN 1 THEN ' LIKE ' ELSE ' = ' END + @SearchValue -- + CASE @NeedsQuotes WHEN 1 THEN '''' ELSE '' END
			@SpecialColumn = CASE WHEN @DataLoaderDataTypeID = 12 OR @NeedsQuotes = 0 THEN ' = ' ELSE ' LIKE ' END + @SearchValue 
			
			IF @OrGroupID IS NOT NULL
			BEGIN
				IF @OrGroupID = @LastOrGroupID
				BEGIN
					SELECT @SelectWhere += ' OR (' + CASE WHEN @SearchValue = '%' THEN @LoopSpecialCol + ' IS NULL OR ' ELSE '' END + @LoopSpecialCol + @SpecialColumn + ') '
				END
				ELSE
				BEGIN
					IF @BracketsAreOpen = 1
					BEGIN 
						SELECT @SelectWhere += ') ', 
						@BracketsAreOpen = 0
					END
					
					SELECT @LastOrGroupID = @OrGroupID,
					@SelectWhere += ' 
					AND ( (' + CASE WHEN @SearchValue = '%' THEN @LoopSpecialCol + ' IS NULL OR ' ELSE '' END + @LoopSpecialCol + @SpecialColumn + ') ',
					@BracketsAreOpen = 1
				END
			END
			ELSE
			BEGIN
			
				-- We now handle where the or group id is null and just 'and' these conditions together.			
				SELECT @SelectWhere += ' 
					AND (' + CASE WHEN @SearchValue = '%' THEN @LoopSpecialCol + ' IS NULL OR ' ELSE '' END + @LoopSpecialCol + @SpecialColumn + ') '
			END
			
			/* Avoid listing the same column out more than once */
			IF CHARINDEX(@LoopSpecialCol, @SelectPart1, 1) = 0
			BEGIN
				SELECT @SpecialCols += @LoopSpecialCol + ', ' 
			END
		END
		ELSE
		BEGIN
			/* Column to display/test */
			SELECT @SpecialColumn = CASE 
										WHEN @QuestionTypeID IN (2, 4) THEN 'luli' + @TableAlias + '.ItemValue'
										WHEN @ColumnFieldID IS NOT NULL THEN 'rldv' + @TableAlias + '.DetailValue'
										ELSE @TableAlias + '.DetailValue'
									END
			
			SELECT
			@SpecialCols += 'IsNull(' + @SpecialColumn + ', '''') as [' + COALESCE(@OutputHeader, (@TableAlias + @SourceColumn)) +'], ' ,
			@SpecialJoins += '
			' + @JoinType + ' JOIN dbo.' + @SourceTable + ' ' + @TableAlias + ' (NOLOCK) on ' + @TableAlias + '.' + @JoinColumn + ' = ' + @JoinTable + '.' + @JoinColumn + ' AND ' + @TableAlias + '.DetailFieldID = ' + convert(varchar, @FieldID) + ' ' 
						+ CASE WHEN @ColumnFieldID IS NOT NULL THEN '
						' + @JoinType + ' JOIN dbo.ResourceListDetailValues rldv' + @TableAlias + ' WITH (NOLOCK) ON rldv' + @TableAlias + '.ResourceListID = ' + @TableAlias + '.ValueInt AND rldv' + @TableAlias + '.DetailFieldID = ' + cast(@ColumnFieldID AS varchar) ELSE '' END
						+ CASE 
							WHEN @QuestionTypeID IN (2, 4) 
							THEN '
							' + @JoinType + ' JOIN dbo.LookupListItems luli' + @TableAlias + ' WITH (NOLOCK) ON luli' + @TableAlias + '.LookupListItemID = ' + CASE WHEN @ColumnFieldID IS NOT NULL THEN 'rldv' ELSE '' END + @TableAlias + '.ValueInt ' 
							ELSE '' 
						END 
						+ CASE WHEN @OrGroupID IS NULL THEN ' AND ' + @SpecialColumn + ' LIKE ' + @SearchValue + ' ' ELSE '' END

			IF @OrGroupID IS NOT NULL
			BEGIN
				IF @OrGroupID = @LastOrGroupID
				BEGIN
					SELECT @SelectWhere += ' OR (' + CASE WHEN @SearchValue = '%' THEN @SpecialColumn + ' IS NULL OR ' ELSE '' END + @SpecialColumn + ' LIKE ' + @SearchValue + ' ' + ') '
				END
				ELSE
				BEGIN
					IF @BracketsAreOpen = 1
					BEGIN 
						SELECT @SelectWhere += ') ', 
						@BracketsAreOpen = 0
					END
					
					SELECT @LastOrGroupID = @OrGroupID,
					@SelectWhere += ' 
					AND ( (' + CASE WHEN @SearchValue = '%' THEN @SpecialColumn + ' IS NULL OR ' ELSE '' END + @SpecialColumn + ' LIKE ' + @SearchValue + ') ',
					@BracketsAreOpen = 1
				END
			END
			
		END
		
		DELETE TOP (1) @CustomFields WHERE UniqueRowID = @UniqueRowID 
		
	END
	
	IF @BracketsAreOpen = 1
	BEGIN 
		SELECT @SelectWhere += ') ', 
		@BracketsAreOpen = 0
	END
	
	
	IF @Debug = 1
	BEGIN
		PRINT '@SelectPart1'
		PRINT @SelectPart1
		PRINT '@SpecialCols'
		PRINT @SpecialCols
		PRINT '@SortCol'
		PRINT @SortCol
		PRINT '@SortOrder'
		PRINT @SortOrder
		PRINT '@NoDuplicatesClause'
		PRINT @NoDuplicatesClause
		PRINT '@CustomersTestOrNot'
		PRINT @CustomersTestOrNot
		PRINT '@UserID'
		PRINT @UserID
		PRINT '@SpecialJoins'
		PRINT @SpecialJoins
		PRINT '@SelectWhere'
		PRINT @SelectWhere
		PRINT '@SelectPart2'
		PRINT @SelectPart2
	END
	

	
	/* Complete the main select statement with all special columns and joins included */
	IF @ShowBusiness = 1 OR @ShowIndividual = 1
	BEGIN
		
		SELECT @SelectPart1 += @SpecialCols + '
		row_number() over (order by ' + @SortCol + ' ' + @SortOrder + ') as [rowno], ' + @NoDuplicatesClause + ' AS [nodupsrowno] ' + 
		'FROM dbo.[Lead] (NOLOCK) 
		INNER JOIN dbo.[Customers] (NOLOCK) on Lead.[CustomerID] = Customers.[CustomerID] ' + @CustomersTestOrNot + CASE WHEN @ShowBusiness = 0 AND @ShowIndividual = 1 THEN ' AND Customers.[IsBusiness] = 0 ' WHEN @ShowBusiness = 1 AND @ShowIndividual = 0 THEN ' AND Customers.[IsBusiness] = 1 ' ELSE '' END + 
		'INNER JOIN dbo.[LeadType] (NOLOCK) on LeadType.[LeadTypeID] = Lead.[LeadTypeID] 
		INNER JOIN dbo.fnLeadTypeSecure(' + convert(varchar, @UserID) + ') f ON LeadType.LeadTypeID = f.LeadTypeID
		INNER JOIN dbo.[Cases] (NOLOCK) on Cases.[LeadID] = Lead.[LeadID] 
		INNER JOIN dbo.[Matter] (NOLOCK) on Matter.[CaseID] = Cases.[CaseID] 
		INNER JOIN dbo.[AquariumStatus] (NOLOCK) on AquariumStatus.[AquariumStatusID] = Customers.[AquariumStatusID] 
		LEFT OUTER JOIN dbo.[Titles] (NOLOCK) on Titles.TitleID = Customers.[TitleID] 
		LEFT OUTER JOIN dbo.[LeadStatus] (NOLOCK) on LeadStatus.StatusID = Cases.[ClientStatusID] 
		LEFT OUTER JOIN dbo.[ClientPersonnel] (NOLOCK) on ClientPersonnel.ClientPersonnelID = Lead.[AssignedTo] '

		SELECT @SelectFull = @SelectPart1 + @SpecialJoins + @SelectWhere + @SelectPart2
		
		IF @Debug = 1
		BEGIN
			PRINT @SelectFull
		END
		ELSE
		BEGIN
			EXEC(@SelectFull)	
		END
		
	END
	ELSE
	BEGIN
		SELECT 0 as [TotalRows] 
	END
	
	
	IF @ShowContact = 1
	BEGIN
	
		SELECT @SortColContacts = replace(@SortCol, 'EmailAddress', 'EmailAddressWork')
		SELECT @SortColContacts = replace(@SortColContacts, 'UserName', 'Fullname')
		
		SELECT @SortColContactsLen = len(@SortColContacts), @SortColContactsDotPos = charindex('.', @SortColContacts)
		
		IF @SortColContactsDotPos > 0
		BEGIN
			SELECT @SortColContacts = substring(@SortColContacts, @SortColContactsDotPos + 1, @SortColContactsLen)
		END
		
		SELECT @SelectPart1Contacts = 'WITH RawData AS (
		SELECT DISTINCT Customers.[CustomerID], Customers.[ClientID], Contact.[ContactID], Contact.[TitleID], Contact.[FirstName], Contact.[MiddleName], Contact.[LastName], Contact.[Fullname], 
		Contact.[EmailAddressWork], Contact.[EmailAddressOther], Contact.[DirectDial], Contact.[MobilePhoneWork], Contact.[MobilePhoneOther], 
		Contact.[Address1], Contact.[Address2], Contact.[Town], Contact.[County], Contact.[PostCode], Contact.[Country], 
		Lead.[LeadID], IsNull(Lead.LeadRef,'''') as [LeadRef], 
		LeadType.LeadTypeName as [LeadTypeName], Customers.[IsBusiness], Customers.[CompanyName], Contact.[Notes], Contact.[OfficeID], Contact.[DepartmentID],  
		IsNull(Titles.Title,'''') as [Title], IsNull(DepartmentName,'''') as [DepartmentName], IsNull(OfficeName,'''') as [OfficeName], Cases.[CaseID], Matter.[MatterID], convert(char(10), Cases.[WhenCreated], 103) as [WhenCreated], Customers.Test 
		FROM dbo.[Lead] (NOLOCK) 
		INNER JOIN dbo.[Customers] (NOLOCK) on Customers.[CustomerID] = Lead.[CustomerID] ' + @CustomersTestOrNot + '
		INNER JOIN dbo.[Contact] (NOLOCK) on Contact.CustomerID = Customers.[CustomerID] 
		INNER JOIN dbo.[LeadType] (NOLOCK) on LeadType.[LeadTypeID] = Lead.[LeadTypeID] 
		INNER JOIN dbo.[Cases] (NOLOCK) on Cases.[LeadID] = Lead.[LeadID]
		INNER JOIN dbo.[Matter] (NOLOCK) on Matter.[CaseID] = Cases.[CaseID] 
		INNER JOIN fnLeadTypeSecure(' + convert(varchar, @UserID) + ') f ON LeadType.LeadTypeID = f.LeadTypeID 
		LEFT OUTER JOIN dbo.[Titles] (NOLOCK) on Titles.TitleID = Customers.[TitleID] 
		LEFT OUTER JOIN dbo.[Department] (NOLOCK) on Department.DepartmentID = Contact.[DepartmentID] 
		LEFT OUTER JOIN dbo.[CustomerOffice] (NOLOCK) on CustomerOffice.OfficeID = Contact.[OfficeID] 
		'
		
		SELECT @SelectPart2Contacts = '),
		Matches AS (
		SELECT [CustomerID], [ClientID], [ContactID], [TitleID], [FirstName], [MiddleName], [LastName], [Fullname], 
		[EmailAddressWork], [EmailAddressOther], [DirectDial], [MobilePhoneWork], [MobilePhoneOther], 
		[Address1], [Address2], [Town], [County], [PostCode], [Country], 
		[LeadID], [LeadRef], 
		[LeadTypeName], [IsBusiness], [CompanyName] as ''Company Name'', [Notes], [OfficeID], [DepartmentID],  
		[Title], [DepartmentName], [OfficeName], 
		row_number() over (order by ' + @SortColContacts + ' ' + @SortOrder + ') as [rowno], ' + @NoDuplicatesContactsClause + ' AS [nodupsrowno] ' + ' 
		FROM RawData
		'

		/* If Client has LeadsBelongToOffice switched on, only return the rows which belong to the current User's ClientOfficeID */
		IF @LeadsBelongToOffice = 1
		BEGIN
			SELECT @SelectPart1Contacts = @SelectPart1Contacts + @LeadBelongsToOfficeJoin,
				@SelectWhereContacts = @SelectWhereContacts + @LeadBelongsToOfficeWhere 
		END

		SELECT @SelectFullContacts = @SelectPart1Contacts + @SelectWhereContacts + @SelectPart2Contacts + @SelectPart2
		
		IF @Debug = 1
		BEGIN
			PRINT @SelectFullContacts
		END
		ELSE
		BEGIN
			EXEC(@SelectFullContacts)
		END

	END
	ELSE
	BEGIN
	
		SELECT 0 as [TotalRows] 
		
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerSearchMultiValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerSearchMultiValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerSearchMultiValue] TO [sp_executeall]
GO
