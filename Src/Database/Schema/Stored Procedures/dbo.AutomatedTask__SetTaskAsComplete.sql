SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-18
-- Description:	Update stats for a task that has just finished.
--              Called by the scheduler.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__SetTaskAsComplete] 
	@TaskID int, 
	@LastRundateStart datetime,
	@LastRundateEnd datetime,
	@LastRowcount int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @RunCount int,
	@MaxRuntime int, -- "seconds" is the time unit for all runtime values
	@MaxRowcount int,
	@AvgRuntime int,
	@AvgRowcount int, 
	@TotalRuntime decimal(18,4),
	@TotalRowcount decimal(18,4), 
	@LastRuntime int

	IF @LastRowcount > -1
	BEGIN
		
		-- Calculate runtime in seconds from the last start/end times
		SELECT @LastRuntime = datediff(ss, @LastRundateStart, @LastRundateEnd)
		IF @LastRuntime = 0
		BEGIN 
			SELECT @LastRuntime = 1
		END

		SELECT @RunCount = RunCount + 1,
		@MaxRuntime = CASE WHEN MaxRuntime > @LastRuntime THEN MaxRuntime ELSE @LastRuntime END,
		@MaxRowcount = CASE WHEN MaxRowcount > @LastRowcount THEN MaxRowcount ELSE @LastRowcount END,
		@TotalRuntime = (AvgRuntime * RunCount) + @LastRuntime,
		@TotalRowcount = (AvgRowcount * RunCount) + @LastRowcount
		FROM dbo.AutomatedTaskInfo ati (nolock) 
		WHERE ati.TaskID = @TaskID 

		SELECT @AvgRuntime = ceiling(@TotalRuntime / @RunCount),
		@AvgRowcount = ceiling(@TotalRowcount / @RunCount)

		UPDATE dbo.AutomatedTaskInfo 
		SET RunCount = @RunCount,
		LastRundate = @LastRundateStart,
		LastRuntime = @LastRuntime,
		LastRowcount = @LastRowcount,
		MaxRuntime = @MaxRuntime,
		MaxRowcount = @MaxRowcount,
		AvgRuntime = @AvgRuntime,
		AvgRowcount = @AvgRowcount, 
		LockDateTime = NULL             -- Free this task up, ready for the next time it needs to be run.
		WHERE TaskID = @TaskID 

	END
	ELSE
	BEGIN
	
		/*
			If the task errored for any reason, 
			don't do all that stats work. Just clear the lock.
		*/
		UPDATE dbo.AutomatedTaskInfo 
		SET LockDateTime = NULL         -- Free this task up, ready for the next time it needs to be run.
		WHERE TaskID = @TaskID 

	END
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__SetTaskAsComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__SetTaskAsComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__SetTaskAsComplete] TO [sp_executeall]
GO
