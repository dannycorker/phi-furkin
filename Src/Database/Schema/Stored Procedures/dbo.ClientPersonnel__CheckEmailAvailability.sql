SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Check if a User already exists with the given email address
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel__CheckEmailAvailability]
(
	@Email varchar(255),
	@ThirdPartySystemId int
)
AS

BEGIN

	SET NOCOUNT ON
		
	DECLARE @Result INT
	EXEC @Result = AquariusMaster.dbo.AQ_ClientPersonnel__CheckEmailAvailability @Email, @ThirdPartySystemId
	RETURN @Result


END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__CheckEmailAvailability] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__CheckEmailAvailability] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__CheckEmailAvailability] TO [sp_executeall]
GO
