SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-14
-- Description:	Creates a standard set of override parameters for quotes
-- Mods
-- 2105-01-26 DCM Added pet number
-- 2015-10-19 DCM Added Marketing Code, IsMultipet, and Optional Coverages  
-- 2016-11-24 DCM added IPT
-- 2017-05-29 CPS added DistributionProfile
-- 2018-01-23 CPS pass in age type (shelf-life) for ticket #47677
-- 2018-02-15 GPR changed DetailFieldID for @MarketingCode to look at the ResourceListField (177363)
-- 2018-02-20 CPS changed ID for PetNumber from 1 to 2 to match RulesEngine_RuleParameters_PreSet
-- 2018-02-28 GPR added fn_C00_1273_RulesEngine_PetCount
-- 2018-07-19 GPR added SourceLookupListItemID for Affiliates build
-- 2018-09-17 GPR added @ShortCodeID to fix Affinity issue
-- 2018-11-27 GPR added attempted fix for #1239
-- 2020-08-28 SB  Handle optional coverages with RE config
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_GetOverrides] 
(
	@Postcode VARCHAR(50),
	@County VARCHAR(200),
	@SpeciesID INT, 
	@Breed VARCHAR(2000),
	@GenderID INT, 
	@BirthDate DATE, 
	@IsNeutered BIT,
	@PetValue MONEY,
	@VolExcess MONEY,
	@PetNumber INT,
	@MarketingCode VARCHAR(200),
	@IsMultipet INT,
	@OptionalCoverages VARCHAR(2000),
	@SignUpMethod INT,
	@LastPeriodExitPremium MONEY,
	@LossRationPercentage MONEY,
	@NumberPreviousClaims INT,
	@PaymentFrequency VARCHAR(20),
	@PolicyYearNumber INT,
	@MicrochipNumber VARCHAR(500),
	@ItemName VARCHAR(500),
	@Brand VARCHAR(500),
	@ProductName VARCHAR(500),
	@IPT MONEY,
	@SourceLookupListItemID	INT = 0
)
 
AS
BEGIN

	SET NOCOUNT ON;
	PRINT OBJECT_NAME(@@ProcID) + ' @Postcode = ''' + ISNULL(@Postcode,'NULL') + ''''
	
	DECLARE  @Overrides						dbo.tvpIntVarcharVarchar
			,@BreedAgeTypeLookupListItemID	INT
			,@BreedListToUse				INT
			,@PetCount						INT
			,@ShortCodeID					INT
			,@IsAggregator					INT
	
	DECLARE @IsNeuteredVarchar VARCHAR(2000) = ''
	IF @IsNeutered = 1
	BEGIN
		SELECT @IsNeuteredVarchar = '5144'
	END
	ELSE IF @IsNeutered = 0
	BEGIN
		SELECT @IsNeuteredVarchar = '5145'
	END
	
	/*GPR 2018-11-30*/
	IF @IsAggregator = 1
	BEGIN
		SELECT @SignUpMethod = 74564
	END	
	
	/*CPS 2018-01-23 handle shelf life for Gavin. #47677*/
	SELECT @BreedListToUse = rdvBreedList.ValueInt, @ShortCodeID = rdvShortCode.ValueInt
	FROM ResourceListDetailValues rdvBrand WITH ( NOLOCK )
	INNER JOIN ResourceListDetailValues rdvBreedList WITH ( NOLOCK ) on rdvBreedList.ResourceListID = rdvBrand.ResourceListID AND rdvBreedList.DetailFieldID = 175592 /*Breed List To Use*/
	INNER JOIN ResourceListDetailValues rdvShortCode WITH ( NOLOCK ) ON rdvBrand.ResourceListID = rdvShortCode.ResourceListID AND rdvShortCode.DetailFieldID = 170127 /*GPR 2018-09-17 Affinity*/
	WHERE rdvBrand.DetailFieldID = 176965 /*Affinity Brand*/
	AND rdvBrand.DetailValue = @Brand
	
	SELECT @BreedAgeTypeLookupListItemID = rdvAgeType.ValueInt
	FROM ResourceListDetailValues rdvType WITH ( NOLOCK )
	INNER JOIN ResourceListDetailValues rdvBreed WITH ( NOLOCK ) on rdvBreed.ResourceListID = rdvType.ResourceListID AND rdvBreed.DetailFieldID = 144270 /*Pet Breed*/
	INNER JOIN ResourceListDetailValues rdvAgeType WITH ( NOLOCK ) on rdvAgeType.ResourceListID = rdvBreed.ResourceListID AND rdvAgeType.DetailFieldID = 179682 /*Short Medium Long Life*/
	INNER JOIN ResourceListDetailValues rdvList WITH ( NOLOCK ) on rdvList.ResourceListID = rdvBreed.ResourceListID AND rdvList.DetailFieldID = 170010 /*Breed List*/
	WHERE rdvType.DetailFieldID = 144269 /*Pet Type*/
	AND rdvBreed.DetailValue = @Breed
	AND rdvType.ValueInt = @SpeciesID
	AND rdvList.ValueInt = @BreedListToUse	
	
	INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
	(2, 'Postcode', @Postcode),
	(2, 'County', @County),
	(5, '144269', CAST(@SpeciesID AS VARCHAR)),
	(5, '144270', @Breed),
	(1, '144275', CAST(@GenderID AS VARCHAR)),
	(1, '144274', CONVERT(VARCHAR, @BirthDate, 120)),
	(1, '152783', @IsNeuteredVarchar),
	(1, '144339', CAST(@PetValue AS VARCHAR)),
	(6, '145667', CAST(@VolExcess AS VARCHAR)),
	(2, 'PetNumber', CAST(@PetNumber AS VARCHAR)),
	(5, '177363', @MarketingCode),
	(2, 'Multipet', CAST(@IsMultipet AS VARCHAR)),
	(2, 'Brand', @Brand),
	(4,'fn_C00_1273_RulesEngine_PostCodeSector',LEFT(@PostCode,CHARINDEX(' ',@PostCode)+1)),
	(1,'176964',CAST(@SignUpMethod AS VARCHAR)),
	(1,'170030',@MicrochipNumber),
	(1,'144339',CAST(@PetValue AS VARCHAR)),
	(2,'ItemName',@ItemName),
	(4,'fn_C00_1273_RulesEngine_PremiumAtLastPeriodExit',CAST(@LastPeriodExitPremium AS VARCHAR)),
	(4,'fn_C00_1273_RulesEngine_LossRatioPercentage',CAST(@LossRationPercentage AS VARCHAR)),
	(4,'fn_C00_1273_RulesEngine_GetNumberOfClaims',CAST(@NumberPreviousClaims AS VARCHAR)),
	(4,'fn_C00_1273_RulesEngine_PaymentFrequency',@PaymentFrequency),
	(2,'PolicyYearNumber',CAST(@PolicyYearNumber AS VARCHAR)),
	(2,'ProductName',@ProductName),
	(4,'fn_C00_1273_RulesEngine_GetIPT',CAST(@IPT AS VARCHAR)),
	(1,'177469','74565'), /*CPS 2017-05-29*/
	--(2,'NewBusiness','1'), /*CPS 2017-08-10   - need to remove this when we build renewals, and ensure that it's passed by GetQuoteValues*/ /*GPR 2018-10-18*/
	(5,'179682', CAST(@BreedAgeTypeLookupListItemID as VARCHAR)),--, /*CPS 2018-01-23 pass in age type (shelf-life) for ticket #47677*/
	(4,'fn_C00_1273_RulesEngine_PetCount',CAST(@PetCount AS VARCHAR)), /*GPR 2018-02-28 for use in MultiPet MTA*/
	(5,'180105',CAST(@SourceLookupListItemID as VARCHAR)), /*GPR 2018-07-19 Source*/
	(5,'170127',CAST(@ShortCodeID as VARCHAR))
	
	/*GPR 2018-11-27 #1239 C600 - Set the signupmethod to be Aggregator if the source is*/
	IF @SourceLookupListItemID IN (76469 /*Confused.com*/,76470 /*Compare The Market*/,76467 /*MoneySuperMarket*/,76468 /*GoCompare*/)
	BEGIN
		UPDATE @Overrides
		SET AnyValue2 = '74564' /*Aggregator*/
		WHERE AnyValue1 = '176964' /*Sign Up Method*/
	END	

	SELECT * 
	FROM @Overrides
 
	PRINT 'Rowcount ' + ISNULL(CONVERT(VARCHAR,@@RowCount),'NULL')
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetOverrides] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_GetOverrides] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetOverrides] TO [sp_executeall]
GO
