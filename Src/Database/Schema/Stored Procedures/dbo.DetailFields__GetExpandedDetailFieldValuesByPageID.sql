SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-08-25
-- Description:	Used in the SDK this PROC gets matter or lead detail values by page.  It expands lookup lists, and resource lists nested 
-- in tables so the SDK can return a nicely formatted collection of objects
-- ##########
-- UPDATE - The original logic in this proc is now in [DetailFields__GetExpandedDetailFieldValuesByFieldIDs]
-- This proc now selects the field ids on the page and calls the new proc
-- UPDATE - Now hanldes customer id
-- UPDATE - Now limits to fields of the right type
-- 2013-09-11 JWG Added Client values
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetExpandedDetailFieldValuesByPageID] 
	@ClientID INT,
	@LeadID INT = 0,
	@MatterID INT = 0,
	@PageID INT,
	@CustomerID INT = 0
AS
BEGIN
	
	DECLARE @FieldIDs tvpInt 
	
	/*EXEC [dbo].[DetailFields__GetExpandedDetailFieldValuesByFieldIDs] @ClientID, @LeadID, @MatterID, @FieldIDs, @CustomerID*/
	IF @MatterID > 0 
	BEGIN
	
		INSERT @FieldIDs (AnyID)
		SELECT DetailFieldID 	
		FROM dbo.fnDetailFieldsShared(@ClientID) 
		WHERE DetailFieldPageID = @PageID 
		AND LeadOrMatter = 2
	
		EXEC [dbo].[DetailFields__GetExpandedMatterValuesByFieldIDs] @ClientID, @MatterID, @FieldIDs
	END
	ELSE
	BEGIN
		IF @LeadID > 0 
		BEGIN
			
			INSERT @FieldIDs (AnyID)
			SELECT DetailFieldID 	
			FROM dbo.fnDetailFieldsShared(@ClientID)
			WHERE DetailFieldPageID = @PageID 
			AND LeadOrMatter = 1
			
			EXEC [dbo].[DetailFields__GetExpandedLeadValuesByFieldIDs] @ClientID, @LeadID, @FieldIDs
		END
		ELSE
		BEGIN
			IF @CustomerID > 0 
			BEGIN
				
				INSERT @FieldIDs (AnyID)
				SELECT DetailFieldID 	
				FROM dbo.fnDetailFieldsShared(@ClientID) 
				WHERE DetailFieldPageID = @PageID 
				AND LeadOrMatter = 10
				
				EXEC [dbo].[DetailFields__GetExpandedCustomerValuesByFieldIDs] @ClientID, @CustomerID, @FieldIDs 
			END
			ELSE
			BEGIN
				/* JWG Added Client values for ticket #22523 */
				INSERT @FieldIDs (AnyID)
				SELECT DetailFieldID 	
				FROM dbo.fnDetailFieldsShared(@ClientID)
				WHERE DetailFieldPageID = @PageID 
				AND LeadOrMatter = 10
				
				EXEC [dbo].[DetailFields__GetExpandedClientValuesByFieldIDs] @ClientID, @FieldIDs 
			END
		END
	END	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedDetailFieldValuesByPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetExpandedDetailFieldValuesByPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedDetailFieldValuesByPageID] TO [sp_executeall]
GO
