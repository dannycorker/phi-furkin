SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-09-09
-- Description:	Tell us all about a Lead Type
-- =============================================

CREATE PROC [dbo].[LeadType__Describe] 
	 @LeadTypeID		INT
	,@IncludeCaseData	BIT = 0
AS
BEGIN

	DECLARE @ClientID INT

	SELECT @ClientID = lt.ClientID
	FROM LeadType lt WITH ( NOLOCK ) 
	WHERE lt.LeadTypeID = @LeadTypeID
	
	SELECT * 
	FROM LeadType lt WITH ( NOLOCK ) 
	WHERE lt.LeadTypeID = @LeadTypeID

	SELECT count(*) [Count], 'EventTypes' [Object Type]
	FROM dbo.EventType et WITH ( NOLOCK ) 
	WHERE et.LeadTypeID = @LeadTypeID
	AND et.Enabled = 1

	UNION SELECT COUNT(*), 'SAE'
	FROM dbo.EventType et WITH ( NOLOCK ) 
	INNER JOIN EventTypeSql sae WITH ( NOLOCK ) on sae.EventTypeID = et.EventTypeID
	WHERE et.LeadTypeID = @LeadTypeID
	and et.Enabled = 1

	UNION SELECT COUNT(*) , 'Documents '
	FROM dbo.EventType et WITH ( NOLOCK ) 
	WHERE et.LeadTypeID = @LeadTypeID
	and et.Enabled = 1
	AND et.DocumentTypeID is not NULL

	UNION SELECT COUNT(*) , 'DetailFields'
	FROM DetailFields df WITH ( NOLOCK ) 
	WHERE df.LeadTypeID = @LeadTypeID

	UNION SELECT count(*) , 'Resource Lists'
	FROM ResourceList rl WITH ( NOLOCK ) 
	WHERE rl.ClientID = @ClientID
	and exists ( SELECT * 
				 FROM ResourceListDetailValues rdv WITH ( NOLOCK ) 
				 INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = rdv.DetailFieldID
				 INNER JOIN DetailFields df2 WITH ( NOLOCK ) on df2.ResourceListDetailFieldPageID = df.DetailFieldPageID
				 WHERE rdv.ResourceListID = rl.ResourceListID
				 AND df2.LeadTypeID = @LeadTypeID )

	UNION SELECT COUNT(*) , 'Equations'
	FROM DetailFields df WITH ( NOLOCK ) 
	WHERE df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID = 10

	UNION SELECT COUNT(*) , 'Workflow Groups'
	FROM WorkflowGroup wg WITH ( NOLOCK ) 
	WHERE wg.ClientID = @ClientID
	AND wg.Enabled = 1
	and exists ( SELECT * 
				 FROM WorkflowTask wt WITH ( NOLOCK ) 
				 INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = wt.LeadID 
				 WHERE wt.WorkflowGroupID = wg.WorkflowGroupID
				 and l.LeadTypeID = @LeadTypeID )

	UNION SELECT COUNT(*) , 'Questionnaires'
	FROM LeadTypeLink ltl WITH ( NOLOCK ) 
	INNER JOIN Outcomes o WITH ( NOLOCK ) on o.OutcomeID = ltl.OutcomeID
	INNER JOIN ClientQuestionnaires cq WITH ( NOLOCK ) on cq.ClientQuestionnaireID = o.ClientQuestionnaireID
	WHERE ltl.LeadTypeID = @LeadTypeID

	UNION SELECT COUNT(*) , 'Reports'
	FROM SqlQuery sq WITH ( NOLOCK ) 
	WHERE sq.LeadTypeID = @LeadTypeID
	AND sq.LastRundate > dbo.fn_GetDate_Local()-60

	UNION SELECT count(*), 'Leads'
	FROM dbo.Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	WHERE cu.Test = 0
	AND l.LeadTypeID = @LeadTypeID
	AND @IncludeCaseData = 1

	UNION SELECT count(*), 'LeadDetailValues'
	FROM dbo.Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN LeadDetailValues ldv WITH ( NOLOCK ) on ldv.LeadID = l.LeadID
	WHERE cu.Test = 0
	AND l.LeadTypeID = @LeadTypeID
	AND @IncludeCaseData = 1

	UNION SELECT count(*), 'Matters'
	FROM dbo.Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
	WHERE cu.Test = 0
	AND l.LeadTypeID = @LeadTypeID
	AND @IncludeCaseData = 1

	UNION SELECT count(*), 'MatterDetailValues'
	FROM dbo.Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID
	WHERE cu.Test = 0
	AND l.LeadTypeID = @LeadTypeID
	AND @IncludeCaseData = 1

	UNION SELECT count(*), 'LeadEvents'
	FROM dbo.Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
	INNER JOIN LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.CaseID = c.CaseID
	WHERE cu.Test = 0
	AND l.LeadTypeID = @LeadTypeID
	AND @IncludeCaseData = 1

	UNION SELECT count(*), 'LeadDocuments'
	FROM dbo.Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN LeadDocument ld WITH (NOLOCK) on ld.LeadID = l.LeadID
	WHERE cu.Test = 0
	AND l.LeadTypeID = @LeadTypeID
	AND @IncludeCaseData = 1

	UNION SELECT
	( 
		SELECT count(*)
		FROM dbo.Lead l WITH ( NOLOCK ) 
		INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
		INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
		INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = m.MatterID
		WHERE cu.Test = 0
		AND l.LeadTypeID = @LeadTypeID
		AND @IncludeCaseData = 1 
		)
		+
		( 
		SELECT count(*)
		FROM dbo.Lead l WITH ( NOLOCK ) 
		INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.LeadID = l.LeadID and tr.MatterID is NULL
		WHERE cu.Test = 0
		AND l.LeadTypeID = @LeadTypeID
		AND @IncludeCaseData = 1 
	), 'TableRows'
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__Describe] TO [sp_executeall]
GO
