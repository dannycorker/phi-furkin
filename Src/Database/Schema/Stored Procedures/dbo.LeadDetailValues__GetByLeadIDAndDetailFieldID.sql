SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[LeadDetailValues__GetByLeadIDAndDetailFieldID]
(

	@LeadID int, 
	@DetailFieldID int,
	@ClientID int
	
)

AS


				SELECT
					[LeadDetailValueID],
					[ClientID],
					[LeadID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[OriginalDetailValueID],
					[OriginalLeadID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[LeadDetailValues] WITH (NOLOCK) 
				WHERE
					[DetailFieldID] = @DetailFieldID
					AND [LeadID] = @LeadID
					AND [ClientID] = @ClientID
					
			Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__GetByLeadIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues__GetByLeadIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__GetByLeadIDAndDetailFieldID] TO [sp_executeall]
GO
