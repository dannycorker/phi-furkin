SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIFieldID int   = null ,

	@RPIFieldName varchar (50)  = null ,

	@RPIFieldDescription varchar (500)  = null ,

	@IsEnabled bit   = null ,

	@QuestionTypeID int   = null ,

	@DetailFieldSubtypeID tinyint   = null ,

	@LookupListID int   = null ,

	@FieldOrder int   = null ,

	@FieldSize int   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null ,

	@RPIFieldGroupID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIFieldID]
	, [RPIFieldName]
	, [RPIFieldDescription]
	, [IsEnabled]
	, [QuestionTypeID]
	, [DetailFieldSubtypeID]
	, [LookupListID]
	, [FieldOrder]
	, [FieldSize]
	, [WhenCreated]
	, [WhenModified]
	, [RPIFieldGroupID]
    FROM
	[dbo].[RPIField] WITH (NOLOCK) 
    WHERE 
	 ([RPIFieldID] = @RPIFieldID OR @RPIFieldID IS NULL)
	AND ([RPIFieldName] = @RPIFieldName OR @RPIFieldName IS NULL)
	AND ([RPIFieldDescription] = @RPIFieldDescription OR @RPIFieldDescription IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([QuestionTypeID] = @QuestionTypeID OR @QuestionTypeID IS NULL)
	AND ([DetailFieldSubtypeID] = @DetailFieldSubtypeID OR @DetailFieldSubtypeID IS NULL)
	AND ([LookupListID] = @LookupListID OR @LookupListID IS NULL)
	AND ([FieldOrder] = @FieldOrder OR @FieldOrder IS NULL)
	AND ([FieldSize] = @FieldSize OR @FieldSize IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([RPIFieldGroupID] = @RPIFieldGroupID OR @RPIFieldGroupID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIFieldID]
	, [RPIFieldName]
	, [RPIFieldDescription]
	, [IsEnabled]
	, [QuestionTypeID]
	, [DetailFieldSubtypeID]
	, [LookupListID]
	, [FieldOrder]
	, [FieldSize]
	, [WhenCreated]
	, [WhenModified]
	, [RPIFieldGroupID]
    FROM
	[dbo].[RPIField] WITH (NOLOCK) 
    WHERE 
	 ([RPIFieldID] = @RPIFieldID AND @RPIFieldID is not null)
	OR ([RPIFieldName] = @RPIFieldName AND @RPIFieldName is not null)
	OR ([RPIFieldDescription] = @RPIFieldDescription AND @RPIFieldDescription is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([QuestionTypeID] = @QuestionTypeID AND @QuestionTypeID is not null)
	OR ([DetailFieldSubtypeID] = @DetailFieldSubtypeID AND @DetailFieldSubtypeID is not null)
	OR ([LookupListID] = @LookupListID AND @LookupListID is not null)
	OR ([FieldOrder] = @FieldOrder AND @FieldOrder is not null)
	OR ([FieldSize] = @FieldSize AND @FieldSize is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([RPIFieldGroupID] = @RPIFieldGroupID AND @RPIFieldGroupID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Find] TO [sp_executeall]
GO
