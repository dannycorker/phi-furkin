SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskResult table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_GetByTaskID]
(

	@TaskID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AutomatedTaskResultID],
					[TaskID],
					[ClientID],
					[RunDate],
					[Description],
					[RecordCount],
					[Complete]
				FROM
					[dbo].[AutomatedTaskResult] WITH (NOLOCK) 
				WHERE
					[TaskID] = @TaskID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_GetByTaskID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_GetByTaskID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_GetByTaskID] TO [sp_executeall]
GO
