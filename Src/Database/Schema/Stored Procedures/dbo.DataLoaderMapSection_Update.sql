SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderMapSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_Update]
(

	@DataLoaderMapSectionID int   ,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderObjectTypeID int   ,

	@DataLoaderObjectActionID int   ,

	@DataLoaderSectionLocaterTypeID int   ,

	@DetailFieldSubTypeID tinyint   ,

	@HasAquariumID bit   ,

	@NumberOfFooterRowsToSkip int   ,

	@ImportIntoLeadManager bit   ,

	@IsFixedLengthSection bit   ,

	@FixedLengthNumberOfRows int   ,

	@IsMandatory bit   ,

	@IsMultipleAllowedWithinFile bit   ,

	@Notes varchar (2000)  ,

	@TableDetailFieldID int   ,

	@ResourceListDetailFieldID int   ,

	@CreatePaymentSchedule bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderMapSection]
				SET
					[ClientID] = @ClientID
					,[DataLoaderMapID] = @DataLoaderMapID
					,[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[DataLoaderObjectActionID] = @DataLoaderObjectActionID
					,[DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID
					,[DetailFieldSubTypeID] = @DetailFieldSubTypeID
					,[HasAquariumID] = @HasAquariumID
					,[NumberOfFooterRowsToSkip] = @NumberOfFooterRowsToSkip
					,[ImportIntoLeadManager] = @ImportIntoLeadManager
					,[IsFixedLengthSection] = @IsFixedLengthSection
					,[FixedLengthNumberOfRows] = @FixedLengthNumberOfRows
					,[IsMandatory] = @IsMandatory
					,[IsMultipleAllowedWithinFile] = @IsMultipleAllowedWithinFile
					,[Notes] = @Notes
					,[TableDetailFieldID] = @TableDetailFieldID
					,[ResourceListDetailFieldID] = @ResourceListDetailFieldID
					,[CreatePaymentSchedule] = @CreatePaymentSchedule
				WHERE
[DataLoaderMapSectionID] = @DataLoaderMapSectionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Update] TO [sp_executeall]
GO
