SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupAndUserEditingControl table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupAndUserEditingControl_GetByEditingID]
(

	@EditingID int   
)
AS


				SELECT
					[EditingID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[WhoIsEditing],
					[EditStartedAt]
				FROM
					[dbo].[GroupAndUserEditingControl] WITH (NOLOCK) 
				WHERE
										[EditingID] = @EditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_GetByEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupAndUserEditingControl_GetByEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_GetByEditingID] TO [sp_executeall]
GO
