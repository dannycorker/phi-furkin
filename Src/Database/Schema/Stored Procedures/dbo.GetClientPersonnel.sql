SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetClientPersonnel    Script Date: 08/09/2006 12:22:49 ******/

CREATE PROCEDURE [dbo].[GetClientPersonnel] @ClientID int
AS

SELECT 

ClientPersonnelID,
ClientID,
ClientOfficeID,
TitleID,
FirstName,
MiddleName,
LastName,
JobTitle,
ClientPersonnel.Password,
MobileTelephone,
HomeTelephone,
OfficeTelephone,
OfficeTelephoneExtension,
EmailAddress,
Salt,
AttemptedLogins,
AccountDisabled

FROM dbo.ClientPersonnel WHERE ClientID = @ClientID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonnel] TO [sp_executeall]
GO
