SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderSectionLocaterType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterType_GetByDataLoaderSectionLocaterTypeID]
(

	@DataLoaderSectionLocaterTypeID int   
)
AS


				SELECT
					[DataLoaderSectionLocaterTypeID],
					[LocaterTypeName],
					[LocaterTypeDescription]
				FROM
					[dbo].[DataLoaderSectionLocaterType] WITH (NOLOCK) 
				WHERE
										[DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_GetByDataLoaderSectionLocaterTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterType_GetByDataLoaderSectionLocaterTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_GetByDataLoaderSectionLocaterTypeID] TO [sp_executeall]
GO
