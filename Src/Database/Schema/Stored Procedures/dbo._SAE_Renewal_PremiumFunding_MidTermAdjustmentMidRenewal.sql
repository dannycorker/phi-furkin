SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2019-11-12
-- Description:	Sql After Event - Mid-Renewal MTA for Policies paid by Premium Funding, adapted from the SAE previously used for UPP MTA in C600
-- 2020-01-09 GPR Added Raiseerror to prevent the event from being applied by Card Payers
-- 2020-01-13 CPS for JIRA LPC-356	| Replace hard-coded ClientID with function
-- 2020-01-17 GPR updated to create new PDH TableRow for this record
-- 2020-01-17 GPR updated so not to write a row to the PDH table at all, we only need to update the risk detail and re-rate for the new term for PCL payer
-- 2020-01-20 GPR Added contra row for previous not taken up Renewal record
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Renewal_PremiumFunding_MidTermAdjustmentMidRenewal]
	@LeadEventID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @NewPaymentScheduleDetailID INT,
			@MatterID INT,
			@PremiumGross DECIMAL (18,2),
			@PostCode VARCHAR(10),
			@PurchasedProductID INT,
			@CustomerID INT,
			@IPT INT,
			@LeadID INT,
			@Now DATE,
			@PremiumNet DECIMAL (18,2),
			@PremiumTax DECIMAL (18,2),
			@Whocreated INT,
			@Whencreated DATE,
			@IdId dbo.tvpIntInt,
			@DetailValueData dbo.tvpDetailValueData,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@EventTypeID INT,
			@CaseID INT,
			@AqAutomation INT,
			@LeadTypeID INT,
			@RenewalPaymentScheduleDetailID INT,
			@VFVT VARCHAR(10)

		DECLARE @NumberedValues TABLE ( ID INT Identity, Value VARCHAR(2000) )

		SELECT TOP (1) 
		 @LeadID		= le.LeadID
		,@EventTypeID	= le.EventTypeID
		,@CustomerID	= l.CustomerID
		,@ClientID		= l.ClientID
		,@CaseID		= le.CaseID
		,@WhoCreated	= le.WhoCreated
		,@MatterID		= m.MatterID
		,@WhenCreated	= le.WhenCreated
		,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
		,@LeadTypeID	= l.LeadTypeID
		FROM dbo.LeadEvent le WITH (NOLOCK)
		INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
		INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
		WHERE le.LeadEventID = @LeadEventID		

		DECLARE @ErrorMessage VARCHAR(2000)
		/*GPR 2020-01-09 - MTA Mid Renewal PCL*/
		IF dbo.fnGetSimpleDvAsInt(170114,@MatterID) <> 76618 /*Not Premium Funding, probably Credit Card*/
		BEGIN				
			/*Raise Error*/
			SELECT @ErrorMessage = '<font color="red"></br></br>This Event can only be used for Policies that pay by Premium Funding.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )

		END

		/*Contra the last renewal row off*/
		SELECT TOP 1 @RenewalPaymentScheduleDetailID = wp.WrittenPremiumID
					,@VFVT							 = CONVERT(VARCHAR(10),wp.ValidFrom,121)
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 2 /*Renewal*/
		ORDER BY wp.WrittenPremiumID DESC

		UPDATE wp
		SET ValidTo = @VfVt
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.WrittenPremiumID = @RenewalPaymentScheduleDetailID

		INSERT INTO WrittenPremium ( [ClientID], [CustomerID], [LeadID], [MatterID], [PurchasedProductID], [RatingDateTime], [PremiumCalculationID], ValidFrom, ValidTo, [AdjustmentTypeID], [AdjustmentRequestDate], [AnnualPriceForRiskGross], [AnnualPriceForRiskNET], [AnnualPriceForRiskNationalTax], [AnnualAdminFee], [AnnualDiscountCommissionSacrifice], [AnnualDiscountPremiumAffecting], [AdjustmentValueGross], [AdjustmentValueNET], [AdjustmentValueNationalTax], [AdjustmentAdminFee], [AdjustmentDiscountCommissionSacrifice], [AdjustmentDiscountCommissionPremium], [AdjustmentBrokerCommission], [AdjustmentUnderwriterCommission], [AdjustmentSourcecommission], [CancellationReason], [AdditionalDetailXML], [Comments], [WhoCreated], [WhenCreated], [WhoChanged], [WhenChanged], [SourceID] )
		SELECT
				 wp.ClientID
				,wp.CustomerID
				,wp.LeadID
				,wp.MatterID
				,wp.PurchasedProductID
				,dbo.fn_GetDate_Local()
				,NULL
				,wp.ValidFrom
				,wp.ValidTo
				,wp.AdjustmentTypeID
				,dbo.fn_GetDate_Local()
				,-1 * wp.AnnualPriceForRiskGross
				,-1 * wp.AnnualPriceForRiskNET
				,-1 * wp.AnnualPriceForRiskNationalTax
				,-1 * wp.AnnualAdminFee
				,-1 * wp.AnnualDiscountCommissionSacrifice
				,-1 * wp.AnnualDiscountPremiumAffecting
				,-1 * wp.AdjustmentValueGross
				,-1 * wp.AdjustmentValueNET
				,-1 * wp.AdjustmentValueNationalTax
				,-1 * wp.AdjustmentAdminFee
				,-1 * wp.AdjustmentDiscountCommissionSacrifice
				,-1 * wp.AdjustmentDiscountCommissionPremium
				,-1 * wp.AdjustmentBrokerCommission
				,-1 * wp.AdjustmentUnderwriterCommission
				,-1 * wp.AdjustmentSourcecommission
				,NULL
				,NULL
				,NULL
				,@WhoCreated
				,@WhenCreated
				,@WhoCreated
				,@WhenCreated
				,@LeadEventID
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.WrittenPremiumID = @RenewalPaymentScheduleDetailID

		SELECT @NewPaymentScheduleDetailID = SCOPE_IDENTITY()

		/*Add the lead and matter to the temp fields so we know what we're dealing with*/ 
		EXEC _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated
		
		/*Take the address lookup value and populate the address detail fields*/
		INSERT @NumberedValues ( Value )
		SELECT fn.AnyValue
		FROM dbo.fnTableOfValues(dbo.fnGetSimpleDv(175478,@CustomerID),'_') fn
		
		/*Construct a tvp of data to populate*/
		INSERT @DetailValueData (ClientID, DetailFieldSubType, ObjectID, DetailFieldID, DetailValueID, DetailValue )
		SELECT @ClientID, 10, @CustomerID, df.DetailFieldID, ISNULL(cdv.CustomerDetailValueID,-1), ISNULL(nv.Value,'')
		FROM @NumberedValues nv 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = CASE nv.ID
																		WHEN 1 THEN 175314 /*Address 1*/
																		WHEN 2 THEN 175315 /*Address 2*/
																		WHEN 3 THEN 175316 /*Town*/
																		WHEN 4 THEN 175317 /*County*/
																		WHEN 5 THEN 175318 /*Postcode*/
																	   END 
		LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.DetailFieldID = df.DetailFieldID AND cdv.CustomerDetailValueID = @CustomerID
		WHERE df.Enabled = 1
		
		/*Perform a bulk save... hide the output of the sp in the @IdId variable*/
		INSERT @IdId ( ID1, ID2 )
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @WhoCreated
		
		/*Put temporary pet & customer changes in a permanent place, ready for generating a quote when the event 156667 is applied */
		EXEC _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated /*TempLeadID*/
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated /*TempMatterID*/
	
		DELETE @IdId

		EXEC _C600_TempSwapPHandPetDetails @CustomerID

		UPDATE CustomerDetailValues 
		SET DetailValue = ''
		WHERE DetailFieldID IN (179199,175586,179912,175326,175325,175352,175329,178406,178405,175314,175315,175478,175317,175332,175311,175313,175318,175310,175316)
		AND CustomerID = @CustomerID

		UPDATE MatterDetailValues 
		SET DetailValue = ''
		WHERE DetailFieldID IN (175442,177683,180214,175334) 
		AND MatterID = @MatterID 

		/*Morph into the Applied event which has an EventChoice to Re-initialise Renewal Rating Post MTA*/
		EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 158938, 0
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_PremiumFunding_MidTermAdjustmentMidRenewal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Renewal_PremiumFunding_MidTermAdjustmentMidRenewal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_PremiumFunding_MidTermAdjustmentMidRenewal] TO [sp_executeall]
GO
