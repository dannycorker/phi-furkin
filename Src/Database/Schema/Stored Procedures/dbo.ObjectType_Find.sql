SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ObjectType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectType_Find]
(

	@SearchUsingOR bit   = null ,

	@ObjectTypeID int   = null ,

	@ClientID int   = null ,

	@SubClientID int   = null ,

	@ObjectTypeName varchar (50)  = null ,

	@ObjectTypeDescription varchar (255)  = null ,

	@Enabled bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ObjectTypeID]
	, [ClientID]
	, [SubClientID]
	, [ObjectTypeName]
	, [ObjectTypeDescription]
	, [Enabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ObjectType] WITH (NOLOCK) 
    WHERE 
	 ([ObjectTypeID] = @ObjectTypeID OR @ObjectTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([ObjectTypeName] = @ObjectTypeName OR @ObjectTypeName IS NULL)
	AND ([ObjectTypeDescription] = @ObjectTypeDescription OR @ObjectTypeDescription IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ObjectTypeID]
	, [ClientID]
	, [SubClientID]
	, [ObjectTypeName]
	, [ObjectTypeDescription]
	, [Enabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ObjectType] WITH (NOLOCK) 
    WHERE 
	 ([ObjectTypeID] = @ObjectTypeID AND @ObjectTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([ObjectTypeName] = @ObjectTypeName AND @ObjectTypeName is not null)
	OR ([ObjectTypeDescription] = @ObjectTypeDescription AND @ObjectTypeDescription is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Find] TO [sp_executeall]
GO
