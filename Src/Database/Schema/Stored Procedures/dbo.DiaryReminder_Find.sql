SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DiaryReminder table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryReminder_Find]
(

	@SearchUsingOR bit   = null ,

	@DiaryReminderID int   = null ,

	@ClientID int   = null ,

	@DiaryAppointmentID int   = null ,

	@ClientPersonnelID int   = null ,

	@ReminderTimeshiftID int   = null ,

	@ReminderDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DiaryReminderID]
	, [ClientID]
	, [DiaryAppointmentID]
	, [ClientPersonnelID]
	, [ReminderTimeshiftID]
	, [ReminderDate]
    FROM
	[dbo].[DiaryReminder] WITH (NOLOCK) 
    WHERE 
	 ([DiaryReminderID] = @DiaryReminderID OR @DiaryReminderID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DiaryAppointmentID] = @DiaryAppointmentID OR @DiaryAppointmentID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ReminderTimeshiftID] = @ReminderTimeshiftID OR @ReminderTimeshiftID IS NULL)
	AND ([ReminderDate] = @ReminderDate OR @ReminderDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DiaryReminderID]
	, [ClientID]
	, [DiaryAppointmentID]
	, [ClientPersonnelID]
	, [ReminderTimeshiftID]
	, [ReminderDate]
    FROM
	[dbo].[DiaryReminder] WITH (NOLOCK) 
    WHERE 
	 ([DiaryReminderID] = @DiaryReminderID AND @DiaryReminderID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DiaryAppointmentID] = @DiaryAppointmentID AND @DiaryAppointmentID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ReminderTimeshiftID] = @ReminderTimeshiftID AND @ReminderTimeshiftID is not null)
	OR ([ReminderDate] = @ReminderDate AND @ReminderDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Find] TO [sp_executeall]
GO
