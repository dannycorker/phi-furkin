SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalOption_Update]
(

	@PortalOptionID int   ,

	@ClientID int   ,

	@ShowHelpLink bit   ,

	@HelpLinkCaption varchar (250)  ,

	@HelpLinkURL varchar (500)  ,

	@LeadListShowAssignedTo bit   ,

	@LeadListShowAssignedToCaption varchar (50)  ,

	@LeadListShowCaseStatus bit   ,

	@LeadListShowCaseStatusCaption varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalOption]
				SET
					[ClientID] = @ClientID
					,[ShowHelpLink] = @ShowHelpLink
					,[HelpLinkCaption] = @HelpLinkCaption
					,[HelpLinkURL] = @HelpLinkURL
					,[LeadListShowAssignedTo] = @LeadListShowAssignedTo
					,[LeadListShowAssignedToCaption] = @LeadListShowAssignedToCaption
					,[LeadListShowCaseStatus] = @LeadListShowCaseStatus
					,[LeadListShowCaseStatusCaption] = @LeadListShowCaseStatusCaption
				WHERE
[PortalOptionID] = @PortalOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Update] TO [sp_executeall]
GO
