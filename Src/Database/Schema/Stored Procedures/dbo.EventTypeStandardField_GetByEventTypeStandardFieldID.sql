SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeStandardField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_GetByEventTypeStandardFieldID]
(

	@EventTypeStandardFieldID int   
)
AS


				SELECT
					[EventTypeStandardFieldID],
					[EventTypeID],
					[DataLoaderObjectFieldID],
					[ClientID],
					[Mandatory]
				FROM
					[dbo].[EventTypeStandardField] WITH (NOLOCK) 
				WHERE
										[EventTypeStandardFieldID] = @EventTypeStandardFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_GetByEventTypeStandardFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_GetByEventTypeStandardFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_GetByEventTypeStandardFieldID] TO [sp_executeall]
GO
