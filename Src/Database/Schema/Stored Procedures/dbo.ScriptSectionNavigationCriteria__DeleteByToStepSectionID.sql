SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 26/06/2015
-- Description:	Deletes the navigation criteria based upon the to step section ID
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria__DeleteByToStepSectionID]
	@ClientID INT,
	@ToStepSectionID INT
AS
BEGIN
	
	
	SET NOCOUNT ON;

	DELETE FROM ScriptSectionNavigationCriteria
	WHERE ClientID=@ClientID AND ToStepSectionID = @ToStepSectionID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__DeleteByToStepSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria__DeleteByToStepSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__DeleteByToStepSectionID] TO [sp_executeall]
GO
