SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EsignatureStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@EsignatureStatusID int   = null ,

	@EsignatureStatusDescription varchar (50)  = null ,

	@EsignatureStatus varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EsignatureStatusID]
	, [EsignatureStatusDescription]
	, [EsignatureStatus]
    FROM
	[dbo].[EsignatureStatus] WITH (NOLOCK) 
    WHERE 
	 ([EsignatureStatusID] = @EsignatureStatusID OR @EsignatureStatusID IS NULL)
	AND ([EsignatureStatusDescription] = @EsignatureStatusDescription OR @EsignatureStatusDescription IS NULL)
	AND ([EsignatureStatus] = @EsignatureStatus OR @EsignatureStatus IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EsignatureStatusID]
	, [EsignatureStatusDescription]
	, [EsignatureStatus]
    FROM
	[dbo].[EsignatureStatus] WITH (NOLOCK) 
    WHERE 
	 ([EsignatureStatusID] = @EsignatureStatusID AND @EsignatureStatusID is not null)
	OR ([EsignatureStatusDescription] = @EsignatureStatusDescription AND @EsignatureStatusDescription is not null)
	OR ([EsignatureStatus] = @EsignatureStatus AND @EsignatureStatus is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Find] TO [sp_executeall]
GO
