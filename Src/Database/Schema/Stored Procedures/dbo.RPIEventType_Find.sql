SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIEventType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventType_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIEventTypeID int   = null ,

	@RPIEventTypeName varchar (50)  = null ,

	@RPIEventTypeDescription varchar (500)  = null ,

	@IsAquariumTriggered bit   = null ,

	@IsEnabled bit   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIEventTypeID]
	, [RPIEventTypeName]
	, [RPIEventTypeDescription]
	, [IsAquariumTriggered]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[RPIEventType] WITH (NOLOCK) 
    WHERE 
	 ([RPIEventTypeID] = @RPIEventTypeID OR @RPIEventTypeID IS NULL)
	AND ([RPIEventTypeName] = @RPIEventTypeName OR @RPIEventTypeName IS NULL)
	AND ([RPIEventTypeDescription] = @RPIEventTypeDescription OR @RPIEventTypeDescription IS NULL)
	AND ([IsAquariumTriggered] = @IsAquariumTriggered OR @IsAquariumTriggered IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIEventTypeID]
	, [RPIEventTypeName]
	, [RPIEventTypeDescription]
	, [IsAquariumTriggered]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[RPIEventType] WITH (NOLOCK) 
    WHERE 
	 ([RPIEventTypeID] = @RPIEventTypeID AND @RPIEventTypeID is not null)
	OR ([RPIEventTypeName] = @RPIEventTypeName AND @RPIEventTypeName is not null)
	OR ([RPIEventTypeDescription] = @RPIEventTypeDescription AND @RPIEventTypeDescription is not null)
	OR ([IsAquariumTriggered] = @IsAquariumTriggered AND @IsAquariumTriggered is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Find] TO [sp_executeall]
GO
