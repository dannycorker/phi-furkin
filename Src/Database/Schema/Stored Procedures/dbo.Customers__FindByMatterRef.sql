SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 14/04/2016
-- Description:	Find customer via matter ref
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByMatterRef]
	@ClientID INT ,
	@Reference VARCHAR(100)
AS
BEGIN

	SELECT c.* FROM Customers c WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON m.CustomerID = c.CustomerID AND m.MatterRef LIKE '%' + @Reference + '%'
	WHERE C.ClientID=@ClientID
	ORDER BY Fullname ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByMatterRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByMatterRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByMatterRef] TO [sp_executeall]
GO
