SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CaseTransferStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@CaseTransferStatusID int   = null ,

	@Status varchar (60)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CaseTransferStatusID]
	, [Status]
    FROM
	[dbo].[CaseTransferStatus] WITH (NOLOCK) 
    WHERE 
	 ([CaseTransferStatusID] = @CaseTransferStatusID OR @CaseTransferStatusID IS NULL)
	AND ([Status] = @Status OR @Status IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CaseTransferStatusID]
	, [Status]
    FROM
	[dbo].[CaseTransferStatus] WITH (NOLOCK) 
    WHERE 
	 ([CaseTransferStatusID] = @CaseTransferStatusID AND @CaseTransferStatusID is not null)
	OR ([Status] = @Status AND @Status is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Find] TO [sp_executeall]
GO
