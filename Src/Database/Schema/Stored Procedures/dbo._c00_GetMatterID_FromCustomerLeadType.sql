SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 12-01-2012
-- Description:	Gets the matter id from the given customer and lead type
-- =============================================
CREATE PROCEDURE [dbo].[_c00_GetMatterID_FromCustomerLeadType]
	@CustomerID INT,
	@LeadTypeID INT
AS
BEGIN
		
	SET NOCOUNT ON;
    
	SELECT m.MatterID, l.LeadID, l.CustomerID, m.CaseID, l.LeadTypeID FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN Lead l ON l.LeadID = m.LeadID AND l.LeadTypeID=@LeadTypeID 
	WHERE m.CustomerID = @CustomerID
	ORDER BY m.matterid DESC
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_c00_GetMatterID_FromCustomerLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_c00_GetMatterID_FromCustomerLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_c00_GetMatterID_FromCustomerLeadType] TO [sp_executeall]
GO
