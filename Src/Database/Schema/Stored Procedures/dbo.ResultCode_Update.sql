SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ResultCode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResultCode_Update]
(

	@ResultCodeID varchar (4)  ,

	@OriginalResultCodeID varchar (4)  ,

	@Description varchar (200)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ResultCode]
				SET
					[ResultCodeID] = @ResultCodeID
					,[Description] = @Description
				WHERE
[ResultCodeID] = @OriginalResultCodeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResultCode_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Update] TO [sp_executeall]
GO
