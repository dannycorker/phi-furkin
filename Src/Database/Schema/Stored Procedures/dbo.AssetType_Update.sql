SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AssetType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_Update]
(

	@AssetTypeID int   ,

	@AssetSubTypeID int   ,

	@AssetTypeName varchar (255)  ,

	@Note varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AssetType]
				SET
					[AssetSubTypeID] = @AssetSubTypeID
					,[AssetTypeName] = @AssetTypeName
					,[Note] = @Note
					,[Enabled] = @Enabled
					,[Deleted] = @Deleted
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[AssetTypeID] = @AssetTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Update] TO [sp_executeall]
GO
