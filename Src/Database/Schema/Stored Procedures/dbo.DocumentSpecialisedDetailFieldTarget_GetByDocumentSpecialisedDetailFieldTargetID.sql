SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentSpecialisedDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDocumentSpecialisedDetailFieldTargetID]
(

	@DocumentSpecialisedDetailFieldTargetID int   
)
AS


				SELECT
					[DocumentSpecialisedDetailFieldTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[RowField],
					[Notes],
					[ExcelSheetLocation],
					[Format],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentSpecialisedDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[DocumentSpecialisedDetailFieldTargetID] = @DocumentSpecialisedDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDocumentSpecialisedDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDocumentSpecialisedDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDocumentSpecialisedDetailFieldTargetID] TO [sp_executeall]
GO
