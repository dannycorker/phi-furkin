SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldPages table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_GetByDetailFieldPageIDClientID]
(

	@DetailFieldPageID int   ,

	@ClientID int   
)
AS


				SELECT
					[DetailFieldPageID],
					[ClientID],
					[LeadOrMatter],
					[LeadTypeID],
					[PageName],
					[PageCaption],
					[PageOrder],
					[Enabled],
					[ResourceList],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[IsShared]
				FROM
					dbo.fnDetailFieldPagesShared(@ClientID)
				WHERE
										[DetailFieldPageID] = @DetailFieldPageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_GetByDetailFieldPageIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_GetByDetailFieldPageIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_GetByDetailFieldPageIDClientID] TO [sp_executeall]
GO
