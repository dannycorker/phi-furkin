SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIClientPersonnelCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientPersonnelCredential_Insert]
(

	@RPIClientPersonnelCredentialID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@UserName varchar (250)  ,

	@Password varchar (50)  ,

	@AsUser varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[RPIClientPersonnelCredential]
					(
					[ClientID]
					,[ClientPersonnelID]
					,[UserName]
					,[Password]
					,[AsUser]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelID
					,@UserName
					,@Password
					,@AsUser
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @RPIClientPersonnelCredentialID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientPersonnelCredential_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Insert] TO [sp_executeall]
GO
