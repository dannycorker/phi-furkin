SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderMapSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_Delete]
(

	@DataLoaderMapSectionID int   
)
AS


				DELETE FROM [dbo].[DataLoaderMapSection] WITH (ROWLOCK) 
				WHERE
					[DataLoaderMapSectionID] = @DataLoaderMapSectionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Delete] TO [sp_executeall]
GO
