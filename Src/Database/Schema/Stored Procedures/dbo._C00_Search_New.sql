SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- ALTER date: 2015-04-28
-- Description:	Search (Simple CRM)
-- 2015-12-14 ACE Updated to use "store" table fields for C361
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Search_New]
	@ClientID INT,
	@ClientPersonnelID INT,
	@Assignee INT = 0,
	@Name VARCHAR(200) = '',
	@Town VARCHAR(50) = '',
	@County VARCHAR(100) = '',
	@CountryName VARCHAR(50) = '',
	@SearchValues dbo.tvpIntVarchar READONLY,
	@LeadTypes dbo.tvpIntVarchar READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @LeadTypeColours dbo.tvpIntVarchar
	
	DECLARE @AllowedLeadTypes TABLE (LeadTypeID INT)
	
	IF EXISTS (SELECT * FROM @LeadTypes)
	BEGIN
	
		INSERT INTO @AllowedLeadTypes (LeadTypeID)
		SELECT f.LeadTypeID
		FROM dbo.fnLeadTypeSecure(@ClientPersonnelID) f 
		INNER JOIN @LeadTypes l ON l.AnyID = f.LeadTypeID
		WHERE f.RightID > 1
	
	END
	ELSE
	BEGIN
	
		INSERT INTO @AllowedLeadTypes (LeadTypeID)
		SELECT f.LeadTypeID
		FROM dbo.fnLeadTypeSecure(@ClientPersonnelID) f 
		WHERE f.RightID > 1
	
	END
	
	INSERT INTO @LeadTypeColours (AnyID, AnyValue)
	VALUES (1662, 'Blue'),
			(1660, 'Green'),
			(1661, 'Green'),
			(1663, 'Orange'),
			(1659, 'Blue')
	
	SELECT *
	INTO #pcodes
	FROM AquariusMaster.dbo.PostCodeLookup with (nolock)

	;
	WITH CustomerFields AS (
		SELECT df.DetailFieldID, s.AnyValue AS [DetailValue]
		FROM @SearchValues s 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = s.AnyID AND df.LeadOrMatter = 10
	),
	LeadFields AS (
		SELECT df.DetailFieldID, s.AnyValue AS [DetailValue]
		FROM @SearchValues s 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = s.AnyID AND df.LeadOrMatter = 1
	),
	CustomerDetails AS (
		SELECT c.Fullname, Address1, c.Address2, c.County, c.PostCode, c.CustomerID, l.LeadID, l.LeadTypeID, l.AssignedTo, dbo.fnUserLeadAccess(@ClientPersonnelID, l.LeadID) AS [AccessLevel]
		From Customers c WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID AND (l.AssignedTo = @Assignee OR @Assignee = 0) 
		INNER JOIN @AllowedLeadTypes alt ON alt.LeadTypeID = l.LeadTypeID
		WHERE c.ClientID = @ClientID
		AND c.Test = 0
		AND (c.Fullname LIKE @Name + '%' OR @Name = '')
		AND (c.Town LIKE @Town + '%' OR @Town = '')
		AND (c.County LIKE @County + '%' OR @County = '')
		AND ((SELECT COUNT (*) FROM LeadFields) = (SELECT COUNT(*) FROM LeadFields lf INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID AND ldv.DetailFieldID = lf.DetailFieldID AND ldv.DetailValue = lf.DetailValue))
		AND ((SELECT COUNT (*) FROM CustomerFields) = (SELECT COUNT(*) FROM CustomerFields cf INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.CustomerID = c.CustomerID AND cdv.DetailFieldID = cf.DetailFieldID AND cdv.DetailValue = cf.DetailValue))
	),
	Stores AS 
	(
		SELECT cd.Fullname, tdv_adl1.DetailValue AS [Address1], tdv_adl2.DetailValue AS [Address2], tdv_county.DetailValue AS [County], tdv_postcode.DetailValue AS [PostCode], cd.CustomerID, cd.LeadID, cd.LeadTypeID, cd.AssignedTo, dbo.fnUserLeadAccess(@ClientPersonnelID, cd.LeadID) AS [AccessLevel]
		FROM CustomerDetails cd
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.CustomerID = cd.CustomerID AND tr.DetailFieldID = 302923
		LEFT JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tr.TableRowID AND tdv_adl1.DetailFieldID = 302918
		LEFT JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tr.TableRowID AND tdv_adl2.DetailFieldID = 302919
		LEFT JOIN TableDetailValues tdv_county WITH (NOLOCK) ON tdv_county.TableRowID = tr.TableRowID AND tdv_county.DetailFieldID = 302921
		LEFT JOIN TableDetailValues tdv_postcode WITH (NOLOCK) ON tdv_postcode.TableRowID = tr.TableRowID AND tdv_postcode.DetailFieldID = 302922
	),
	UnionedResults AS 
	(
		SELECT *
		FROM Stores

		UNION

		SELECT *
		FROM CustomerDetails
		
	),
	Geocode AS 
	(
		SELECT	c.*, p.PostCode AS Match, ISNULL(cdv_lat.DetailValue,p.Latitude) AS [Latitude], ISNULL(cdv_long.DetailValue,p.Longitude) AS [Longitude],
				ROW_NUMBER() OVER(PARTITION BY c.LeadID,c.PostCode ORDER BY LEN(p.PostCode) DESC) as rn 
		FROM UnionedResults c
		LEFT JOIN CustomerDetailValues cdv_lat WITH (NOLOCK) ON cdv_lat.CustomerID = c.CustomerID AND cdv_lat.DetailFieldID = 304975
		LEFT JOIN CustomerDetailValues cdv_long WITH (NOLOCK) ON cdv_long.CustomerID = c.CustomerID AND cdv_long.DetailFieldID = 304976
		LEFT JOIN #pcodes p WITH (NOLOCK) ON REPLACE(c.Postcode, ' ', '') LIKE p.PostCode + '%'
		AND c.AccessLevel > 0
	)

	SELECT g.*, ISNULL(cp.UserName,'') AS [Assignee], ISNULL(lc.AnyValue, 'Red') AS [PinColour], lt.LeadTypeName AS [Type Of Entity]
	FROM Geocode g
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = g.LeadTypeID
	LEFT JOIN @LeadTypeColours lc ON lc.AnyID = g.LeadTypeID
	LEFT JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = g.AssignedTo
	WHERE (rn = 1 OR Match IS NULL)
	--ORDER BY g.CustomerID, g.LeadID
	
	DROP TABLE #pcodes
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Search_New] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Search_New] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Search_New] TO [sp_executeall]
GO
