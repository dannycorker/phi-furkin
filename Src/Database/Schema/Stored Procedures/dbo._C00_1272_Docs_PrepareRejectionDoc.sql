SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-06-21
-- Description:	Prepares formatted rejection text for the rejection doc
-- Used by:		SAE
-- Modified:    2013-04-08 Robin Hall		Missing vet info change request (finally put live 2013-10-07)
-- Modified:    2014-09-08 Simon Brushett	Changed to use policy lead function
-- ROH 2014-12-03 Added new {} placeholders and reorganised code
-- DCM 2016-02-09 Add generic replacement function for merge codes to augment original hard-coded codes. 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Docs_PrepareRejectionDoc] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	-- Clear out the dead pet text
	EXEC dbo._C00_SimpleValueIntoField 156628, '', @MatterID
	
	DECLARE @RejectionType INT,
			@PhText VARCHAR(2000) = '',
			@VetText VARCHAR(2000) = ''
	
	DECLARE @ExclusionReason VARCHAR(2000)
			,@Settlement VARCHAR(2000)
			,@TreatmentStart VARCHAR(50)
			,@DateOfLoss VARCHAR(50)
			
	SELECT 
		@ExclusionReason = ISNULL(mdvExclReas.DetailValue, '')
		,@RejectionType = ISNULL(mdvRejType.ValueInt, 0)
		,@TreatmentStart = ISNULL(CONVERT(VARCHAR, mdvTreatStart.ValueDate, 106), '*unknown date*')
		,@DateOfLoss = ISNULL(CONVERT(VARCHAR, mdvDateOfLoss.ValueDate, 106), '*unknown date*')
	FROM Matter m WITH (NOLOCK)
	LEFT JOIN dbo.MatterDetailValues mdvExclReas WITH (NOLOCK) ON mdvExclReas.MatterID = m.MatterID AND mdvExclReas.DetailFieldID = 152692
	LEFT JOIN dbo.MatterDetailValues mdvRejType WITH (NOLOCK) ON mdvRejType.MatterID = m.MatterID AND mdvRejType.DetailFieldID = 146184
	LEFT JOIN dbo.MatterDetailValues mdvTreatStart WITH (NOLOCK) ON mdvTreatStart.MatterID = m.MatterID AND mdvTreatStart.DetailFieldID = 144366
	LEFT JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON mdvDateOfLoss.MatterID = m.MatterID AND mdvDateOfLoss.DetailFieldID = 144892
	WHERE m.MatterID = @MatterID

	SELECT 
		@Settlement = '£' + CONVERT(VARCHAR, ISNULL(SUM(COALESCE(tdvTotal.ValueMoney, tdvSettle.ValueMoney, 0)), 0))
	FROM 
		dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvSettle WITH (NOLOCK) ON r.TableRowID = tdvSettle.TableRowID AND tdvSettle.DetailFieldID = 145678
		INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	WHERE 
		r.MatterID = @MatterID
	
	SELECT	
		@PhText = rdvPhText.DetailValue
		,@VetText = rdvVetText.DetailValue
	FROM 
		dbo.ResourceList rl WITH (NOLOCK) 
		INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON rl.ResourceListID = rdvType.ResourceListID AND rdvType.DetailFieldID = 150454
		INNER JOIN dbo.ResourceListDetailValues rdvPhText WITH (NOLOCK) ON rl.ResourceListID = rdvPhText.ResourceListID AND rdvPhText.DetailFieldID = 150455
		INNER JOIN dbo.ResourceListDetailValues rdvVetText WITH (NOLOCK) ON rl.ResourceListID = rdvVetText.ResourceListID AND rdvVetText.DetailFieldID = 150456
	WHERE rdvType.ValueInt = @RejectionType

	-- Placeholder replacements
	SELECT @PhText = REPLACE(@PhText, '{ExclusionReason}', @ExclusionReason)
	SELECT @PhText = REPLACE(@PhText, '{SettlementAmount}', @Settlement)
	SELECT @PhText = REPLACE(@PhText, '{TreatmentStart}', @TreatmentStart)
	SELECT @PhText = REPLACE(@PhText, '{DateOfLoss}', @DateOfLoss)

	SELECT @VetText = REPLACE(@VetText, '{ExclusionReason}', @ExclusionReason)
	SELECT @VetText = REPLACE(@VetText, '{SettlementAmount}', @Settlement)
	SELECT @VetText = REPLACE(@VetText, '{TreatmentStart}', @TreatmentStart)
	SELECT @VetText = REPLACE(@VetText, '{DateOfLoss}', @DateOfLoss)

	-- ** Change Request for Missing info rejections
	IF @RejectionType = 53440 -- Missing vet info
	BEGIN
		
		DECLARE @VetName VARCHAR(1000)
		DECLARE @InfoRequestDate DATE
		DECLARE @MissingItemText VARCHAR(2000) = ''
		DECLARE @MissingItems TABLE (ID INT IDENTITY, DetailFieldID INT, ItemText VARCHAR(1000))
		DECLARE @Index INT
		
		SELECT @VetName = rldvVet.DetailValue
		FROM dbo.LeadDetailValues ldvCurVet WITH (NOLOCK)
		INNER JOIN dbo.ResourceListDetailValues rldvVet WITH (NOLOCK) ON rldvVet.ResourceListID = ldvCurVet.ValueInt AND rldvVet.DetailFieldID = 144473 
		WHERE ldvCurVet.LeadID = @PolicyLeadID
		AND ldvCurVet.DetailFieldID = 146215
		
		SELECT @InfoRequestDate = mdvInfoDate.ValueDate
		FROM Matter m WITH (NOLOCK)
		INNER JOIN dbo.MatterDetailValues mdvInfoDate WITH (NOLOCK) on mdvInfoDate.MatterID = m.MatterID and mdvInfoDate.DetailFieldID = 144522
		WHERE m.MatterID = @MatterID
		
		INSERT @MissingItems (DetailFieldID, ItemText)
		SELECT mdvMissing.DetailFieldID,
		CASE mdvMissing.DetailFieldID
			WHEN 150417 THEN '- A full clinical history from your usual vet, even if your pet has been referred to a different vet.'
			WHEN 150418 THEN '- A fully itemised invoice as the official document to show the cost of your pet''s treatment, drugs and procedures.'
			WHEN 150419 THEN '- The treating vet''s signature on your claim form to certify the treatment you have claimed for.'
			WHEN 150420 THEN '- The practice stamp on your claim form to show which Veterinary Practice employs the vet who signed the form.'
		END
		FROM MatterDetailValues mdvMissing WITH (NOLOCK)
		WHERE mdvMissing.MatterID = @MatterID and mdvMissing.DetailFieldID IN (150417,150418,150419,150420) --Missing vet info items
		AND mdvMissing.ValueInt = 63964 --Missing
		ORDER BY mdvMissing.DetailFieldID DESC

		SELECT @Index = COUNT(*) from @MissingItems
		WHILE @Index > 0
		BEGIN
			SELECT @MissingItemText += CHAR(13)+CHAR(10)+ItemText
			FROM @MissingItems WHERE ID = @Index
			SET @Index -= 1
		END

		SELECT @PhText = REPLACE(@PhText,'{Vet}',ISNULL(@VetName,'*unknown vet*'))
		SELECT @PhText = REPLACE(@PhText,'{InfoRequestDate}',ISNULL(CONVERT(VARCHAR,@InfoRequestDate,103),'*unknown date*'))
		SELECT @PhText = REPLACE(@PhText,'{MissingItems}',ISNULL(@MissingItemText,'*unknown items*'))
		
	END	

	-- do any remaining generic aliases substitution
	SELECT @PhText=dbo.fn_C600_ReplaceAliasesInBraces ( @PhText, @MatterID, 'Matter' )
	SELECT @VetText=dbo.fn_C600_ReplaceAliasesInBraces ( @VetText, @MatterID, 'Matter' )
		
	EXEC dbo._C00_SimpleValueIntoField 152875, @PhText, @MatterID
	EXEC dbo._C00_SimpleValueIntoField 152876, @VetText, @MatterID
	
	-- Check for dead pet
	DECLARE @DateOfDeath DATE
	SELECT @DateOfDeath = ValueDate
	FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
	WHERE ldv.LeadID = @PolicyLeadID
	AND ldv.DetailFieldID = 144271

	
	IF @DateOfDeath IS NOT NULL
	BEGIN
	
		DECLARE @PetName VARCHAR(2000)
		SELECT @PetName = DetailValue
		FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
		WHERE ldv.LeadID = @PolicyLeadID
		AND ldv.DetailFieldID = 144268
	
		DECLARE @DeadPetText VARCHAR(2000)
		SELECT @DeadPetText = REPLACE(rdvText.DetailValue, '{PetName}', ISNULL(@PetName, 'your animal'))
		FROM dbo.ResourceList r WITH (NOLOCK) 
		INNER JOIN dbo.ResourceListDetailValues rdvCode WITH (NOLOCK) ON r.ResourceListID = rdvCode.ResourceListID AND rdvCode.DetailFieldID = 155936
		INNER JOIN dbo.ResourceListDetailValues rdvText WITH (NOLOCK) ON r.ResourceListID = rdvText.ResourceListID AND rdvText.DetailFieldID = 155937
		WHERE r.ClientID = @ClientID
		AND rdvCode.DetailValue = 'DEADPET'

		-- do any remaining generic aliases substitution
		SELECT @DeadPetText=dbo.fn_C600_ReplaceAliasesInBraces ( @DeadPetText, @MatterID, 'Matter' )
				
		EXEC dbo._C00_SimpleValueIntoField 156628, @DeadPetText, @MatterID
		
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_PrepareRejectionDoc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Docs_PrepareRejectionDoc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_PrepareRejectionDoc] TO [sp_executeall]
GO
