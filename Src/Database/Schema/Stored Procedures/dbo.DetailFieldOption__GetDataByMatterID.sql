SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-10
-- Description:	DetailFieldOption__GetDataByLeadID
-- =============================================
Create PROCEDURE [dbo].[DetailFieldOption__GetDataByMatterID]
@ClientID int,
@MatterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, m.LeadID, m.MatterID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 2
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = @MatterID
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID and l.LeadTypeID = df.LeadTypeID
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = m.MatterID
		AND mdv.DetailFieldID = dfo.DetailFieldID
	)
	

	SELECT dfo.DetailFieldID, df.FieldCaption, mdv.DetailValue, dfo.FieldOrder, dfo.Mandatory, m.LeadID, m.MatterID, m.CaseID, df.LeadOrMatter
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 2
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = @MatterID
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID  and l.LeadTypeID = df.LeadTypeID
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = df.DetailFieldID and mdv.MatterID = m.MatterID
	WHERE dfo.ClientID = @ClientID
	ORDER BY dfo.FieldOrder

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByMatterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldOption__GetDataByMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByMatterID] TO [sp_executeall]
GO
