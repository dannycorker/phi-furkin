SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-07-14
-- Description:	Updates object identifiers for new lead type (see below for details)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LeadTypeCopy_SwitchObjectIdentifiers] 
	@NewLeadTypeID int,
	@NewRTLeadTypeID int = -1,
	@NewClientID int,
	@OldClientID int,
	@ObjectName VARCHAR(500),
	@SQLFragment VARCHAR(MAX),
	@AddtionalMap VARCHAR(MAX),
	@Inspect INT = 1,
	@DisplayMap INT = 0
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


/* 
	We take the SQL snippet provided and replace the existing object identifiers with the object identifiers for the new lead type.
	
	The SQL snippet can be provided as an actual snippet in the calling argument @SQLFragment or it can be a whole procedure
	in which case the procedure name is provided in the calling argument @ObjectName.
	
	An object idenitifier map is created using the SourceID column in the relevant table. The object identifiers currently included
	in the map are,	in order of precedence:
		Detail Fields (DF), 
		Event Types (ET), 
		Document Types (DT), 
		Lookup List Items (LI),
		Lookup List (LL) 
		Resource List ID (RL), 
		Detail Field Pages (DFP)
		Lead Type (LT)
		WorkFlow Groups (WG) 
		Event Groups (EG) and 
		Client (CL)
		
	The map can be added to by passing in a comma separated list of additional identifiers listed in triplets: OldID, NewID, Type e.g.
	'123,456,QID,789,999,QID,etc'.
	
	We can be run in Inspect mode (@Inspect=1), in which case the output is annotated with all of the changes made and not made, or in live
	mode (@Inspect=0), in which case the output is only annotated where an object identitfier is duplicated in the old or new identifiers
	in the map.
	
	Annotations are as follows:
	
		- an identifier has not been changed. Prefixed with ||
		- an identifier has been changed. Prefixed with >> + the type of identifier changed to (DF, ET, etc).
		  If the identifier that has been changed is duplicated in the map 'do' is included after the >> and
		  if the new identifier is duplicated in the map 'dn' is included after the >>
		  
	Where there is duplication in the new identifiers in the map, the identifier with the highest precedence is used e.g.
	if a new DF and a new ET have the same identifier, the DF identifier is used for the replacement.
	
	The Map table that is created on-the-fly includes a duplicate column so it can be useful to display the table (@DisplayMap = 1)

 */
	SET NOCOUNT ON;
	
	DECLARE @IsNumberSearch INT = 0,
		 	@SQL VARCHAR(MAX),
		 	@Break int,
			@Pos int = 2,
			@Add1 varchar(10),
			@Add2 varchar(10),
			@Add3 varchar(10)
					
	DECLARE @Tokens TABLE (Seq INT IDENTITY(1,1), 
							Token VARCHAR(MAX),
							Done INT )	
						
	DECLARE @Map TABLE (Seq INT IDENTITY(1,1), 
						OldID VARCHAR(30), 
						NewID VARCHAR(30), 
						Type VARCHAR(20), 
						Dup VARCHAR(10))

	-- Create Map
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dfnew.SourceID,dfnew.DetailFieldID,'DF','' FROM DetailFields dfnew WITH (NOLOCK) WHERE (dfnew.LeadTypeID in (@NewLeadTypeID,@NewRTLeadTypeID) or (LeadOrMatter in (10,12,13) and ClientID=@NewClientID) ) and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT etnew.SourceID,etnew.EventTypeID,'ET','' FROM EventType etnew WITH (NOLOCK) WHERE etnew.LeadTypeID in (@NewLeadTypeID,@NewRTLeadTypeID) and SourceID is not null and SourceID <> 0 
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.DocumentTypeID,'DT','' FROM DocumentType dtnew WITH (NOLOCK) WHERE dtnew.LeadTypeID in (@NewLeadTypeID,@NewRTLeadTypeID) and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.LookupListItemID,'LI','' FROM LookupListItems dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.LookupListID,'LL','' FROM LookupList dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.ResourceListID,'RL','' FROM ResourceList dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.DetailFieldPageID,'DFP','' FROM Detailfieldpages dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.LeadTypeID,'LT','' FROM LeadType dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT wg.SourceID,wg.WorkflowGroupID,'WG','' FROM WorkflowGroup wg WITH (NOLOCK) WHERE wg.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT eg.SourceID,eg.EventGroupID,'EG','' FROM EventGroup eg WITH (NOLOCK) WHERE eg.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup) VALUES											
	(@OldClientID,@NewClientID,'CL','')

	-- add any specials to the map
	WHILE @AddtionalMap <> ''
	BEGIN
	
		SELECT @Add1='',@Add2='',@Add2=''
		
		SELECT @Add1=LEFT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN LEN(@AddtionalMap) ELSE CHARINDEX(',',@AddtionalMap)-1 END), @AddtionalMap=RIGHT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN 0 ELSE LEN(@AddtionalMap) - CHARINDEX(',',@AddtionalMap) END)
		SELECT @Add2=LEFT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN LEN(@AddtionalMap) ELSE CHARINDEX(',',@AddtionalMap)-1 END), @AddtionalMap=RIGHT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN 0 ELSE LEN(@AddtionalMap) - CHARINDEX(',',@AddtionalMap) END)
		SELECT @Add3=LEFT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN LEN(@AddtionalMap) ELSE CHARINDEX(',',@AddtionalMap)-1 END), @AddtionalMap=RIGHT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN 0 ELSE LEN(@AddtionalMap) - CHARINDEX(',',@AddtionalMap) END)

		IF @Add1<>'' AND @Add2<>'' AND @Add3<>''
			INSERT INTO @Map (OldID,NEWID,TYPE,Dup) VALUES
			(@Add1,@Add2,@Add3,'')
	
	END
	
	-- look for duplicates
	UPDATE m1
	SET m1.Dup='do'
	FROM @Map m1 
	INNER JOIN @Map m2 ON m1.OldID=m2.OldID AND m1.Seq <> m2.Seq
	UPDATE m1
	SET m1.Dup=m1.Dup+'dn'
	FROM @Map m1 
	INNER JOIN @Map m2 ON m1.NewID=m2.NewID AND m1.Seq <> m2.Seq
	
	IF @DisplayMap = 1
		SELECT * FROM @Map
		
	-- Get SQL for conversion
	IF @ObjectName IS NOT NULL
	BEGIN
		SELECT @SQL=m.definition 
		FROM sys.all_sql_modules m WITH (NOLOCK) 
		WHERE OBJECT_NAME(object_id) = @ObjectName
	END
	ELSE
	BEGIN
	
		SELECT @SQL=@SQLFragment
	
	END
	
	IF @SQL IS NULL
	BEGIN
	
		PRINT 'No SQL to convert'
		
	END
	ELSE
	BEGIN
		
		-- split SQL into numeric and non-numeric tokens
		SELECT @SQL = ' ' + @SQL

		WHILE LEN(@sql) > 0
		BEGIN

			IF @IsNumberSearch = 0
			BEGIN
			
				INSERT INTO @Tokens (Token,Done)
				SELECT LEFT(@SQL,CASE WHEN PATINDEX('%[0-9]%' ,@SQL) = 0 THEN LEN(@SQL) ELSE PATINDEX('%[0-9]%' ,@SQL)-1 END), 0
				SELECT @SQL=CASE WHEN PATINDEX('%[0-9]%' ,@SQL) = 0 THEN '' ELSE RIGHT(@SQL,LEN(@SQL)-PATINDEX('%[0-9]%',@SQL)+1) END
			
				SELECT @IsNumberSearch = 1
			END
			ELSE
			BEGIN
			
				INSERT INTO @Tokens (Token,Done)
				SELECT LEFT(@SQL,CASE WHEN PATINDEX('%[^0-9]%' ,@SQL) = 0 THEN LEN(@SQL) ELSE PATINDEX('%[^0-9]%' ,@SQL)-1 END), 0
				SELECT @SQL=CASE WHEN PATINDEX('%[^0-9]%' ,@SQL) = 0 THEN '' ELSE RIGHT(@SQL,LEN(@SQL)-PATINDEX('%[^0-9]%',@SQL)+1) END
			
				SELECT @IsNumberSearch = 0
			END

		END 

		-- update numeric tokens where there is a match
		UPDATE t
		SET Token=CASE WHEN (@Inspect=1 OR m.Dup<>'') THEN '>>'+m.Dup+m.TYPE+m.NewID ELSE m.NewID END
		FROM @Tokens t
		INNER JOIN @Map m ON t.Token=m.OldID
		WHERE m.Dup=''
		OR NOT EXISTS ( SELECT * FROM @Map subm WHERE (subm.OldID=m.OldID AND subm.Seq < m.Seq AND m.Dup='od') OR
													(subm.NewID=m.NewID AND subm.Seq < m.Seq AND m.Dup='nd') )

		IF @Inspect = 1
		BEGIN

			UPDATE @Tokens
			SET Token = '||' + Token
			WHERE Token NOT LIKE '>>%' AND Token LIKE '[0-9]%'

			IF EXISTS ( SELECT * FROM @Tokens WHERE Token LIKE '||[0-9]%' )
				SELECT 'SQL contains unchanged identifiers'
			ELSE
				SELECT 'No identifiers are unchanged'
			
			IF EXISTS ( SELECT * FROM @Tokens WHERE Token LIKE '>>d' )
				SELECT 'SQL contains duplicate identifiers'
			ELSE
				SELECT 'No identifiers are duplicated'

		END
			
		-- reconstruct SQL from the tokens
		SELECT @SQL=''
		SELECT @SQL = @SQL + Token FROM @Tokens	ORDER BY Seq

		-- print the results (line by line because there is a limit of 8000 characters per print statement)
		WHILE @Pos < LEN(@SQL) 
		BEGIN

			select @Break=CASE WHEN CHARINDEX(CHAR(13)+CHAR(10),@SQL,@Pos) = 0 THEN LEN(@SQL)+1 ELSE CHARINDEX(CHAR(13)+CHAR(10),@SQL,@Pos) END
			--select @Pos,@Break,@Break-@Pos
			-- Do your printing...
			PRINT SUBSTRING(@SQL,@pos,@Break-@Pos)
			SET @Pos = @Break + 2
		END	

	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadTypeCopy_SwitchObjectIdentifiers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LeadTypeCopy_SwitchObjectIdentifiers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadTypeCopy_SwitchObjectIdentifiers] TO [sp_executeall]
GO
