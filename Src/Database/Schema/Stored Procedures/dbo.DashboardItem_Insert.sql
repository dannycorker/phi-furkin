SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DashboardItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItem_Insert]
(

	@DashboardItemID int    OUTPUT,

	@DashboardID int   ,

	@DashboardItemTypeID int   ,

	@ItemID int   ,

	@ClientID int   ,

	@LayoutPosition int   
)
AS


				
				INSERT INTO [dbo].[DashboardItem]
					(
					[DashboardID]
					,[DashboardItemTypeID]
					,[ItemID]
					,[ClientID]
					,[LayoutPosition]
					)
				VALUES
					(
					@DashboardID
					,@DashboardItemTypeID
					,@ItemID
					,@ClientID
					,@LayoutPosition
					)
				-- Get the identity value
				SET @DashboardItemID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItem_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Insert] TO [sp_executeall]
GO
