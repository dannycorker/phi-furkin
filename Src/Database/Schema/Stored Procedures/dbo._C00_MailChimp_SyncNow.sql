SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-11-20
-- Description:	force Mailchimp sync update.
-- the sync is hardcoded to run only once a day, even if you manually rerun through the scriptrunner.- Alex
-- You can get around this by updating the sentdate in the table mailchimpcampaignoutcomes to be older than a day.
-- Updates:		
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MailChimp_SyncNow]
(
@ClientID int = 253
,@ScheduleID INT = NULL
)
	
AS
BEGIN

	SET NOCOUNT ON	
		
IF @ScheduleID is null /*For LSG ClientID:253*/
BEGIN
	SELECT @ScheduleID = 287
END

UPDATE MailChimpCampaigns
SET LastCompletePoll = cast(REPLACE(LastCompletePoll,LastCompletePoll,dateadd(day,-1,LastCompletePoll)) as datetime)
WHERE ClientID = @ClientID
and DATEDIFF(day,LastCompletePoll,dbo.fn_GetDate_Local())=0

/*Call Sync ScriptID 80*/
EXEC dbo.AutomatedScript__RunNow 80,@ScheduleID

SELECT TOP 1000 *,cast(REPLACE(LastCompletePoll,LastCompletePoll,dateadd(day,-1,LastCompletePoll)) as datetime)
FROM  MailChimpCampaigns WITH (NOLOCK) 
WHERE ClientID = 253
and DATEDIFF(day,LastCompletePoll,dbo.fn_GetDate_Local())=0

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MailChimp_SyncNow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MailChimp_SyncNow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MailChimp_SyncNow] TO [sp_executeall]
GO
