SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[DeleteLinkedDetailFields] @LeadTypeIDTo int, @LeadTypeIDFrom int
AS
Delete From LinkedDetailFields
Where (LeadTypeIDTo = @LeadTypeIDTo AND LeadTypeIDFrom = @LeadTypeIDFrom)



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLinkedDetailFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteLinkedDetailFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLinkedDetailFields] TO [sp_executeall]
GO
