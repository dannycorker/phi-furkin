SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PanelItemTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemTypes_Delete]
(

	@PanelItemTypeID int   
)
AS


				DELETE FROM [dbo].[PanelItemTypes] WITH (ROWLOCK) 
				WHERE
					[PanelItemTypeID] = @PanelItemTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemTypes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Delete] TO [sp_executeall]
GO
