SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SearchQueryValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SearchQueryValueID],
					[ClientID],
					[SubClientID],
					[SearchQueryID],
					[SearchFieldID],
					[SearchFieldOperatorID],
					[FieldValue],
					[FieldValueID]
				FROM
					[dbo].[SearchQueryValue] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_GetByClientID] TO [sp_executeall]
GO
