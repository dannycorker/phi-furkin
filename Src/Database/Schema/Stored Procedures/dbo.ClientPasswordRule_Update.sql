SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPasswordRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPasswordRule_Update]
(

	@ClientPasswordRuleID int   ,

	@ClientID int   ,

	@MinLength int   ,

	@MinLowerCase int   ,

	@MinUpperCase int   ,

	@MinNumber int   ,

	@MinSymbol int   ,

	@ExpiryDays int   ,

	@ResetForcesChange bit   ,

	@PasswordHistoryCount int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPasswordRule]
				SET
					[ClientID] = @ClientID
					,[MinLength] = @MinLength
					,[MinLowerCase] = @MinLowerCase
					,[MinUpperCase] = @MinUpperCase
					,[MinNumber] = @MinNumber
					,[MinSymbol] = @MinSymbol
					,[ExpiryDays] = @ExpiryDays
					,[ResetForcesChange] = @ResetForcesChange
					,[PasswordHistoryCount] = @PasswordHistoryCount
				WHERE
[ClientPasswordRuleID] = @ClientPasswordRuleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPasswordRule_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Update] TO [sp_executeall]
GO
