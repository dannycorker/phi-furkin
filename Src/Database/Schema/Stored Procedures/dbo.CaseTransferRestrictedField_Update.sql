SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CaseTransferRestrictedField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_Update]
(

	@CaseTransferRestrictedFieldID int   ,

	@ClientRelationshipID int   ,

	@DetailFieldID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CaseTransferRestrictedField]
				SET
					[ClientRelationshipID] = @ClientRelationshipID
					,[DetailFieldID] = @DetailFieldID
				WHERE
[CaseTransferRestrictedFieldID] = @CaseTransferRestrictedFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Update] TO [sp_executeall]
GO
