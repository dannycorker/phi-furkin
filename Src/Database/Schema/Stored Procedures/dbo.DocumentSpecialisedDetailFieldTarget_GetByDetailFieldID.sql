SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentSpecialisedDetailFieldTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDetailFieldID]
(

	@DetailFieldID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[DocumentSpecialisedDetailFieldTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[ColumnField]
				FROM
					[dbo].[DocumentSpecialisedDetailFieldTarget]
				WHERE
					[DetailFieldID] = @DetailFieldID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_GetByDetailFieldID] TO [sp_executeall]
GO
