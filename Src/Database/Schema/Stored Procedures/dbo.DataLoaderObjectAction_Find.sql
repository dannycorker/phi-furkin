SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderObjectAction table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectAction_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderObjectActionID int   = null ,

	@DataLoaderObjectActionName varchar (250)  = null ,

	@DataLoaderObjectActionDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderObjectActionID]
	, [DataLoaderObjectActionName]
	, [DataLoaderObjectActionDescription]
    FROM
	[dbo].[DataLoaderObjectAction] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderObjectActionID] = @DataLoaderObjectActionID OR @DataLoaderObjectActionID IS NULL)
	AND ([DataLoaderObjectActionName] = @DataLoaderObjectActionName OR @DataLoaderObjectActionName IS NULL)
	AND ([DataLoaderObjectActionDescription] = @DataLoaderObjectActionDescription OR @DataLoaderObjectActionDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderObjectActionID]
	, [DataLoaderObjectActionName]
	, [DataLoaderObjectActionDescription]
    FROM
	[dbo].[DataLoaderObjectAction] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderObjectActionID] = @DataLoaderObjectActionID AND @DataLoaderObjectActionID is not null)
	OR ([DataLoaderObjectActionName] = @DataLoaderObjectActionName AND @DataLoaderObjectActionName is not null)
	OR ([DataLoaderObjectActionDescription] = @DataLoaderObjectActionDescription AND @DataLoaderObjectActionDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectAction_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Find] TO [sp_executeall]
GO
