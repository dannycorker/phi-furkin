SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_DeleteDisabledEventTypes]
(
@LeadEventID int
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@ClientID int,
		@myERROR int

	Select @ClientID = ldv.detailvalue
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	Inner join leaddetailvalues ldv on ldv.leadid = lead.leadid and ldv.detailfieldid = 6453
	where leadevent.leadeventid = @LeadEventID 

if @ClientID is not null
begin

Begin Tran

	delete le
	from leadeventthreadcompletion le
	inner join eventtype et on et.eventtypeid = le.FromEventTypeID
	where et.enabled = 0
	and le.clientid = @clientid 

	delete ets
	from EventTypeSql ets
	inner join eventtype et on et.eventtypeid = ets.EventTypeID
	where et.enabled = 0
	and ets.clientid = @clientid 

	delete le
	from leadeventthreadcompletion le
	inner join eventtype et on et.eventtypeid = le.ToEventTypeID
	where et.enabled = 0
	and le.clientid = @clientid 

	delete le
	from leadevent le
	inner join eventtype et on et.eventtypeid = le.eventtypeid
	where et.enabled = 0
	and le.clientid = @clientid 

	delete mf
	from EventTypeMandatoryField mf
	inner join eventtype et on et.eventtypeid = mf.eventtypeid
	where et.enabled = 0
	and mf.clientid = @clientid 

	delete et
	from eventtype et
	where enabled = 0
	and et.clientid = @clientid 

	Select @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

END

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	GOTO END_NOW

HANDLE_ERROR:
    ROLLBACK TRAN

END_NOW:


END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_DeleteDisabledEventTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_DeleteDisabledEventTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_DeleteDisabledEventTypes] TO [sp_executeall]
GO
