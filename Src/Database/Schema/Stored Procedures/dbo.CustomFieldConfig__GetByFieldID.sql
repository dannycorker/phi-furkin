SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2019-07-09
-- Description:	Get config for custom fields
-- =============================================
CREATE PROCEDURE [dbo].[CustomFieldConfig__GetByFieldID] 
	@ClientID INT,
	@DetailFieldID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM CustomFieldConfig c WITH (NOLOCK)
	WHERE c.DetailFieldID = @DetailFieldID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomFieldConfig__GetByFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomFieldConfig__GetByFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomFieldConfig__GetByFieldID] TO [sp_executeall]
GO
