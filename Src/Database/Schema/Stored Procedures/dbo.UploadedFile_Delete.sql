SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UploadedFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_Delete]
(

	@UploadedFileID int   
)
AS


				DELETE FROM [dbo].[UploadedFile] WITH (ROWLOCK) 
				WHERE
					[UploadedFileID] = @UploadedFileID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Delete] TO [sp_executeall]
GO
