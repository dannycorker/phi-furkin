SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the GroupFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing_Delete]
(

	@GroupFunctionControlEditingID int   
)
AS


				DELETE FROM [dbo].[GroupFunctionControlEditing] WITH (ROWLOCK) 
				WHERE
					[GroupFunctionControlEditingID] = @GroupFunctionControlEditingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Delete] TO [sp_executeall]
GO
