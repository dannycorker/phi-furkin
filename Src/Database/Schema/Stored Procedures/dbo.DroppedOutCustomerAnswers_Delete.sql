SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DroppedOutCustomerAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerAnswers_Delete]
(

	@CustomerAnswerID int   
)
AS


				DELETE FROM [dbo].[DroppedOutCustomerAnswers] WITH (ROWLOCK) 
				WHERE
					[CustomerAnswerID] = @CustomerAnswerID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerAnswers_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_Delete] TO [sp_executeall]
GO
