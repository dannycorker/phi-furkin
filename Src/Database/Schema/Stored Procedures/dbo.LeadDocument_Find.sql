SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadDocument table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadDocumentID int   = null ,

	@ClientID int   = null ,

	@LeadID int   = null ,

	@DocumentTypeID int   = null ,

	@LeadDocumentTitle varchar (1000)  = null ,

	@UploadDateTime datetime   = null ,

	@WhoUploaded int   = null ,

	@DocumentBLOB image   = null ,

	@FileName varchar (255)  = null ,

	@EmailBLOB image   = null ,

	@DocumentFormat varchar (24)  = null ,

	@EmailFrom varchar (512)  = null ,

	@EmailTo varchar (MAX)  = null ,

	@CcList varchar (MAX)  = null ,

	@BccList varchar (MAX)  = null ,

	@ElectronicSignatureDocumentKey varchar (50)  = null ,

	@Encoding varchar (50)  = null ,

	@ContentFormat varchar (50)  = null ,

	@ZipFormat varchar (50)  = null 
)
AS
BEGIN

	/* This should never be used! */
	RETURN -1			
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Find] TO [sp_executeall]
GO
