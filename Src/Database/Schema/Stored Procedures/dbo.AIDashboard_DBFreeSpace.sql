SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- ALTER date: 2011-05-17
-- Description:	AI Dashboard - DB Free Space
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard_DBFreeSpace]
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	/*BEGIN TRY
		EXECUTE AS LOGIN = 'AquariumNet'
		
		EXEC AquariusMaster.dbo.AIDashboard_DBFreeSpace 
		
		REVERT
	END TRY
	BEGIN CATCH
		SELECT 'Please ask a dbo for IMPERSONATE permission on the AquariumNet login' as [Fail]
	END CATCH*/
	
	;WITH RawDBProperties AS 
	(
		SELECT 
		sf.fileid AS [FileID], 
		sf.name AS [FileName], 
		sf.filename AS [PhysicalFilePath], 
		sf.groupid AS [FileGroupID], 
		sfg.groupname AS [FileGroupName], 
		sf.size AS [FileSize], 
		FILEPROPERTY(sf.name,'SpaceUsed') AS [PageCount], 
		CONVERT(DECIMAL(12,2), ROUND(FILEPROPERTY(sf.name,'SpaceUsed')/128.0,2)) AS [SpaceUsed], 
		sf.size-FILEPROPERTY(sf.name,'SpaceUsed') AS [FreeSpace], 
		CASE FILEPROPERTY(sf.name,'IsLogFile') WHEN 1 THEN 'Log' ELSE 'Data' END AS [FileType], 
		CASE FILEPROPERTY(sf.name,'IsPrimaryFile ') WHEN 1 THEN 'Yes' ELSE '' END AS [PRIMARY], 
		sf.maxsize as [MaxFileSize],
		sf.growth as [RawGrowth],
		CASE WHEN sf.growth < 128 THEN sf.growth ELSE 0 END AS [GrowthInPercent], 
		CASE WHEN sf.growth > 127 THEN (sf.growth/128) ELSE 0 END AS [GrowthInMB], /* This 128 does not refer to pages. 128=1MB, 256=2MB etc! */
		CASE 
			WHEN sf.growth < 128 THEN ((sf.size * sf.growth) /128.0 /100.0) /* This 128 does mean pages! */
			ELSE (sf.growth / 128.0 /* This 128 does not refer to pages. 128=1MB, 256=2MB etc! */ ) END as [FileGrowth]
		FROM dbo.sysfiles sf WITH (NOLOCK) 
		LEFT JOIN dbo.sysfilegroups sfg WITH (NOLOCK) ON sfg.groupid = sf.groupid 
	)
	,GBDBProperties AS 
	(
		SELECT 
		[FileID], 
		CONVERT(DECIMAL(12,2), ROUND([PageCount]/128.0/1024.0,2)) AS [SpaceUsedGB], 
		CONVERT(DECIMAL(12,2), ROUND([FileSize]/128.0/1024.0,2)) AS [FileSizeGB], 
		CONVERT(DECIMAL(12,2), (ROUND([FileSize]/128.0/1024.0,2)-ROUND([PageCount]/128.0/1024.0,2))) AS [FreeSpaceGB], 
		CONVERT(DECIMAL(12,2), ROUND([MaxFileSize]/128.0/1024.0,2)) as [MaxFileSizeGB],
		ROUND([FileGrowth]/1024.0,3) as [FileGrowthGB]
		FROM RawDBProperties
	)
	, DBProperties AS 
	(
		SELECT 
		r.[FileID], 
		CAST((CONVERT(DECIMAL(12,3),[FileGrowthGB])) AS VARCHAR) + CASE 
			WHEN r.[RawGrowth] > 127 THEN ''
			ELSE + ' (' + CAST([GrowthInPercent] AS VARCHAR) + '%)' 
		END as [FileGrowthGB]
		FROM RawDBProperties r 
		INNER JOIN GBDBProperties g ON g.FileID = r.FileID
	)
	SELECT dbp.FileID, 
	CASE r.FileType 
		WHEN 'Data' THEN 
			/* Lack of free space is only an issue if growth is denied or impossible */
			CASE WHEN r.[FileGrowth] = 0 OR (r.[MaxFileSize] > -1 AND ((r.[FileGrowth] + r.[SpaceUsed]) > r.[MaxFileSize])) THEN
				CASE  
					WHEN g.FreeSpaceGB < 1 THEN 'Red' 
					WHEN g.FreeSpaceGB < 2 THEN 'Amber' 
					ELSE 'Green' 
				END 
			ELSE 
				'Green' 
			END 
		ELSE
			CASE  
				WHEN g.FileSizeGB > 95 THEN 'Amber'
				ELSE 'Green' 
			END 
	END as [Status], 
	r.[FileName],
	r.PhysicalFilePath,
	g.FileSizeGB,
	g.SpaceUsedGB,
	g.FreeSpaceGB,
	CASE r.FileType 
		WHEN 'Data' THEN  
			/* Lack of free space is only an issue if growth is denied or impossible */
			CASE WHEN r.[FileGrowth] = 0 OR (r.[MaxFileSize] > -1 AND ((r.[FileGrowth] + r.[SpaceUsed]) > r.[MaxFileSize])) THEN '1'
				ELSE '' 
			END 
		ELSE '> 95' 
	END as [Warning At], /* Free space for Data vs Used space for Log*/ 
	CAST(r.[GrowthInMB] AS VARCHAR) + ' / ' + CAST(r.[GrowthInPercent] AS VARCHAR) AS [Growth Rate MB / %],
	CAST(r.[FileGrowth] AS numeric(18, 2)) AS GrowthMB,
	dbp.FileGrowthGB AS GrowthGB,
	r.FileType,
	r.[PageCount]
	FROM RawDBProperties r 
	INNER JOIN GBDBProperties g ON g.FileID = r.FileID
	INNER JOIN DBProperties dbp ON dbp.FileID = r.FileID
	ORDER BY [FileType], [FileID]

END





GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_DBFreeSpace] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard_DBFreeSpace] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_DBFreeSpace] TO [sp_executeall]
GO
