SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the StyleRule table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[StyleRule_Find]
(

	@SearchUsingOR bit   = null ,

	@StyleRuleID int   = null ,

	@RuleClass varchar (100)  = null ,

	@RuleDescription varchar (255)  = null ,

	@RuleValue varchar (100)  = null ,

	@AquariumOnly bit   = null ,

	@IncludeLinkText bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [StyleRuleID]
	, [RuleClass]
	, [RuleDescription]
	, [RuleValue]
	, [AquariumOnly]
	, [IncludeLinkText]
    FROM
	[dbo].[StyleRule] WITH (NOLOCK) 
    WHERE 
	 ([StyleRuleID] = @StyleRuleID OR @StyleRuleID IS NULL)
	AND ([RuleClass] = @RuleClass OR @RuleClass IS NULL)
	AND ([RuleDescription] = @RuleDescription OR @RuleDescription IS NULL)
	AND ([RuleValue] = @RuleValue OR @RuleValue IS NULL)
	AND ([AquariumOnly] = @AquariumOnly OR @AquariumOnly IS NULL)
	AND ([IncludeLinkText] = @IncludeLinkText OR @IncludeLinkText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [StyleRuleID]
	, [RuleClass]
	, [RuleDescription]
	, [RuleValue]
	, [AquariumOnly]
	, [IncludeLinkText]
    FROM
	[dbo].[StyleRule] WITH (NOLOCK) 
    WHERE 
	 ([StyleRuleID] = @StyleRuleID AND @StyleRuleID is not null)
	OR ([RuleClass] = @RuleClass AND @RuleClass is not null)
	OR ([RuleDescription] = @RuleDescription AND @RuleDescription is not null)
	OR ([RuleValue] = @RuleValue AND @RuleValue is not null)
	OR ([AquariumOnly] = @AquariumOnly AND @AquariumOnly is not null)
	OR ([IncludeLinkText] = @IncludeLinkText AND @IncludeLinkText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StyleRule_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Find] TO [sp_executeall]
GO
