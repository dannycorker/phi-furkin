SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-01
-- Description:	Duplicate check for the table, column and ID passed in
-- =============================================
CREATE PROCEDURE [dbo].[DuplicateChecks] 
	@TableName varchar(250), 
	@ColumnValue varchar(2000),
	@ClientID int,
	@IDToIgnore int = null 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ExistingCount int

	SET @ExistingCount = -1

	/*
		There are 2 scenarios to check for:
		1) When inserting a new record (@IDToIgnore IS NULL), make sure that no record already has the
		   value we are about to insert;  
		2) When updating an existing record, make sure that no other record is using the value.
	*/

	IF @TableName = 'LookupList'
    BEGIN
		SELECT @ExistingCount = COUNT(*) 
		FROM LookupList 
		WHERE ClientID = @ClientID 
		AND LookupListName = @ColumnValue 
		AND (@IDToIgnore IS NULL OR LookupListID <> @IDToIgnore) 
	END

	SELECT @ExistingCount as [ExistingCount]

END





GO
GRANT VIEW DEFINITION ON  [dbo].[DuplicateChecks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DuplicateChecks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DuplicateChecks] TO [sp_executeall]
GO
