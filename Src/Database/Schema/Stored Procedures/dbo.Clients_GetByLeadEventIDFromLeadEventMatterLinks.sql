SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records through a junction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_GetByLeadEventIDFromLeadEventMatterLinks]
(

	@LeadEventID int   
)
AS


SELECT dbo.[Clients].[ClientID]
       ,dbo.[Clients].[CompanyName]
       ,dbo.[Clients].[WebAddress]
       ,dbo.[Clients].[IPAddress]
       ,dbo.[Clients].[DefaultEmailAddress]
       ,dbo.[Clients].[ClientTypeID]
       ,dbo.[Clients].[AllowSMS]
       ,dbo.[Clients].[SecurityCode]
       ,dbo.[Clients].[LeadsBelongToOffices]
       ,dbo.[Clients].[UseEventCosts]
       ,dbo.[Clients].[UseEventUOEs]
       ,dbo.[Clients].[UseEventDisbursements]
       ,dbo.[Clients].[UseEventComments]
       ,dbo.[Clients].[VerifyAddress]
       ,dbo.[Clients].[UseTapi]
       ,dbo.[Clients].[UseRPI]
       ,dbo.[Clients].[UseGBAddress]
       ,dbo.[Clients].[UsePinpoint]
       ,dbo.[Clients].[UseSage]
       ,dbo.[Clients].[UseSAS]
       ,dbo.[Clients].[UseCreditCalculation]
       ,dbo.[Clients].[FollowupWorkingDaysOnly]
       ,dbo.[Clients].[AddLeadByQuestionnaire]
       ,dbo.[Clients].[UseIncendia]
       ,dbo.[Clients].[LanguageID]
       ,dbo.[Clients].[CountryID]
       ,dbo.[Clients].[UseMobileInterface]
       ,dbo.[Clients].[UseGBValidation]
  FROM dbo.[Clients] WITH (NOLOCK)
 WHERE EXISTS (SELECT 1
                 FROM dbo.[LeadEventMatterLinks] WITH (NOLOCK)
                WHERE dbo.[LeadEventMatterLinks].[LeadEventID] = @LeadEventID
                  )
				SELECT @@ROWCOUNT			
				




GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_GetByLeadEventIDFromLeadEventMatterLinks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_GetByLeadEventIDFromLeadEventMatterLinks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_GetByLeadEventIDFromLeadEventMatterLinks] TO [sp_executeall]
GO
