SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypePaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_Get_List]

AS


				
				SELECT
					[EventTypePaymentGatewayID],
					[ClientID],
					[EventTypeID],
					[ClientPaymentGatewayID],
					[TransactionTypeID],
					[CardMaskingMethodID]
				FROM
					[dbo].[EventTypePaymentGateway] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Get_List] TO [sp_executeall]
GO
