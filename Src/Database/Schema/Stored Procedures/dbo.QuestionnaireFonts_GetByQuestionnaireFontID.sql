SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireFonts table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_GetByQuestionnaireFontID]
(

	@QuestionnaireFontID int   
)
AS


				SELECT
					[QuestionnaireFontID],
					[ClientQuestionnaireID],
					[PartNameID],
					[FontFamily],
					[FontSize],
					[FontColour],
					[FontWeight],
					[FontAlignment],
					[ClientID]
				FROM
					[dbo].[QuestionnaireFonts] WITH (NOLOCK) 
				WHERE
										[QuestionnaireFontID] = @QuestionnaireFontID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_GetByQuestionnaireFontID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_GetByQuestionnaireFontID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_GetByQuestionnaireFontID] TO [sp_executeall]
GO
