SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-31
-- Description:	Determine whether or not a client has any interest in Businesses.
-- =============================================
CREATE PROCEDURE [dbo].[GetBusinessUsage] 
	@ClientID int,
	@LeadTypeID int = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @IsBusiness int = 0
	
	SELECT @IsBusiness = 1 
	WHERE EXISTS (
		SELECT * 
		FROM dbo.LeadType lt WITH (NOLOCK) 
		WHERE lt.ClientID = @ClientID 
		AND lt.IsBusiness = 1 
		AND (lt.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL)
	)

	SELECT @IsBusiness
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetBusinessUsage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetBusinessUsage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetBusinessUsage] TO [sp_executeall]
GO
