SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionPossibleAnswers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SELECT
					[QuestionPossibleAnswerID],
					[ClientQuestionnaireID],
					[MasterQuestionID],
					[AnswerText],
					[Branch],
					[LinkedQuestionnaireQuestionPossibleAnswerID],
					[ClientID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[QuestionPossibleAnswers] WITH (NOLOCK) 
				WHERE
										[ClientQuestionnaireID] = @ClientQuestionnaireID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_GetByClientQuestionnaireID] TO [sp_executeall]
GO
