SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/**
*
* Date Created: 20 April 2007
* Author:	Ben Crinion
* Purpose: 	Flag to tell the system that the equations need recalculating for this leadtype or this specific lead.
*
**/

CREATE PROCEDURE [dbo].[Equations__SetLeadTypeEquationsToDirty] 
	@LeadTypeID 	int,
	@LeadID		int = NULL
AS
BEGIN

	IF @LeadID IS NULL
	BEGIN

		-- Entire LeadType needs updating
		UPDATE Lead 
		SET RecalculateEquations=1 
		WHERE LeadTypeID = @LeadTypeID

	END
	ELSE
	BEGIN

		-- Only 1 individual Lead needs updating
		UPDATE Lead 
		SET RecalculateEquations=1 
		WHERE LeadID = @LeadID

	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__SetLeadTypeEquationsToDirty] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations__SetLeadTypeEquationsToDirty] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__SetLeadTypeEquationsToDirty] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[Equations__SetLeadTypeEquationsToDirty] TO [sp_executehelper]
GO
