SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-09-20
-- Description:	Create a stub test customer
-- 2020-01-13 CPS for JIRA LPC-356	| Replace hard-coded ClientID with function
-- 2020-02-12 CPS for JIRA AAG-106	| Updated to match TPET and return the URL of your new customer along with the dataset
--									| This is by no means exhaustive and should NOT be used for formal testing
-- 2020-02-24 GPR					| Replaced default product and breed with 604 Product and a Breed that exists in the 604 list
-- 2020-03-16 NG					| Added variables for pet 2, amended breed RLID, made birth dates more recent, comms method email,
--									  underwriting questions, postcode to AUS, bank details to AUS match, reordered variables
-- 2021-01-26 CPS for JIRA FURKIN-136 | Included Pet1 and Pet2 Sex and DoB options and enrolment fee calculations
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Testing_CreateTestCustomer]
(
 	 @MultiPet					BIT				= 0
	,@Monthly					BIT				= 1
	,@FirstName					VARCHAR(2000)	= 'Cathal'
	,@LastName					VARCHAR(2000)	= 'AutoGeneratedTest'
	,@EmailAddress				VARCHAR(2000)	= 'cathal.sherry@aquarium-software.com'
	,@PetName					VARCHAR(2000)	= 'Dog Test'
	,@PetName2					VARCHAR(2000)	= 'Cat Test'
	,@BreedResourceListID		INT				= 2002638 /*Cardigan Welsh Corgi*/
	,@BreedResourceListID2		INT				= 2002468 /*Domestic Shorthair*/
	,@ContactMethodId			INT				= 60789 /*Email*/
	,@AffinityResourceListID	INT				= 2000184 /*Furkin*/
	,@ProductResourceListID		INT				= 2002315 /*Medium ARPU*/
	,@Pet1Sex					CHAR			= 'M'
	,@Pet2Sex					CHAR			= 'F'
	,@Pet1DoB					DATE			= '2018-03-16'
	,@Pet2Dob					DATE			= '2019-03-19'
	,@State						VARCHAR(5)		= 'ON'
	,@CoPay						DECIMAL(18,2)	= 20.00
	,@Deductible				DECIMAL(18,2)	= 300.00
	,@CustomerNotes				VARCHAR(2000)	= ''
	,@WhoCreated				INT				= 58552 /*Aquarium Automation*/
)
AS
BEGIN
PRINT OBJECT_NAME(@@ProcID)

	IF SYSTEM_USER <> 'CathalSherry' AND @EmailAddress = 'cathal.sherry@aquarium-software.com'
	BEGIN
		RAISERROR( 'Stop it.  Use your own email address!', 16, 1 )
		RETURN
	END

	DECLARE  @RequestVarchar			VARCHAR(MAX)
			,@RequestXML				XML
			,@ClientID					INT = dbo.fnGetPrimaryClientID()
			,@ProvinceLookupListItemID	INT
			,@PostCode					VARCHAR(2000)

	SELECT TOP 1 @WhoCreated = cp.ClientPersonnelID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.IsAquarium = 1
	AND cp.ClientID = @ClientID
	AND REPLACE(cp.UserName,' ','') = SYSTEM_USER
	AND @WhoCreated = 58552 /*Aquarium Automation*/
	ORDER BY cp.ClientPersonnelID

	SELECT TOP 1 @PostCode = pc.PostalCode
	FROM ProvinceByPostalCode pc WITH (NOLOCK) 
	INNER JOIN dbo.UnitedStates us WITH (NOLOCK) on us.StateID = pc.StateID
	WHERE us.StateCode = @State
	ORDER BY NEWID()
	
	DECLARE	 @XmlResponse		TABLE ( XmlResponse XML )

	DECLARE @EnrollmentFeeTable TABLE 
	(
		PetID INT,
		FeeTableRowID INT,
		EnrollmentFee NUMERIC(18,2),
		TaxTableRowID INT,
		Tax NUMERIC(18,2),
		Total NUMERIC(18,2)
	)

	SELECT @ProvinceLookupListItemID = CASE @State 
			WHEN 'AB' THEN 61739
			WHEN 'BC' THEN 61740
			WHEN 'MB' THEN 61741
			WHEN 'NB' THEN 61742
			WHEN 'NL' THEN 61743
			WHEN 'NS' THEN 61744
			WHEN 'NT' THEN 61745
			WHEN 'NU' THEN 61746
			WHEN 'ON' THEN 61747
			WHEN 'PE' THEN 61748
			WHEN 'QC' THEN 61749
			WHEN 'SK' THEN 61750
			WHEN 'YT' THEN 61751
		END

	/*2021-01-21 - Enrollment fee*/
	INSERT INTO @EnrollmentFeeTable (PetID, FeeTableRowID, EnrollmentFee, TaxTableRowID, Tax, Total)
	SELECT 1, FeeTableRowID, EnrollmentFee, TaxTableRowID, Tax, Total
	FROM dbo.fn_C600_GetEnrollmentFee(@AffinityResourceListID, 0, @ProvinceLookupListItemID, 1, dbo.fn_GetDate_Local())
		UNION 
	SELECT 2, FeeTableRowID, EnrollmentFee, TaxTableRowID, Tax, Total
	FROM dbo.fn_C600_GetEnrollmentFee(@AffinityResourceListID, 0, @ProvinceLookupListItemID, 2, dbo.fn_GetDate_Local())
	WHERE @MultiPet = 1

	SELECT @RequestVarchar = 
	'<BuyPolicyRequest>
	  <ClientId>' + CONVERT(VARCHAR,@ClientID) + '</ClientId>
	  <ClientPersonnelId>' + ISNULL(CONVERT(VARCHAR,@WhoCreated),'58552') + '</ClientPersonnelId>
	  <Password />
	  <BrandingId>' + CONVERT(VARCHAR,@AffinityResourceListID) + '</BrandingId>
	  <QuoteId>0</QuoteId>
	  <SessionId>0</SessionId>
	  <TransactionRef />
	  <CustomerInfo>
		<CustomerId>0</CustomerId>
		<TitleId>1</TitleId>
		<FirstName>' + @FirstName + '</FirstName>
		<LastName>' + @LastName + '</LastName>
		<Email>' + @EmailAddress + '</Email>
		<Address1>Test</Address1>
		<TownCity>Test</TownCity>
		<County>' + @State + '</County>
		<Postcode>' + ISNULL(@PostCode,'')  + '</Postcode>
		<SecondaryTitleId>0</SecondaryTitleId>
		<SecondaryFirstName />
		<SecondaryLastName />
		<SecondaryEmail />
		<SecondaryHomePhone />
		<SecondaryMobilePhone />
		<ExistingPolicyHolder>false</ExistingPolicyHolder>
		<DoNotSms>true</DoNotSms>
		<DoNotEmail>true</DoNotEmail>
		<DoNotPhone>false</DoNotPhone>
		<DoNotPost>false</DoNotPost>
		<DateOfBirth>2000-01-01T00:00:00</DateOfBirth>
		<ContactMethodId>' + ISNULL(CONVERT(VARCHAR,@ContactMethodId,0),'NULL')  + '</ContactMethodId>
		<MarketingPreferenceId>5144</MarketingPreferenceId>
	  </CustomerInfo>
	  <HouseholdInfo>
		<NumberOfPeople>1</NumberOfPeople>
		<MaritalStatusID>6294</MaritalStatusID>
		<NumberOfPets>1</NumberOfPets>
	  </HouseholdInfo>
	  <PaymentInfo>
		<PaymentDay>1</PaymentDay>
		<PaymentMethodTypeId>69930</PaymentMethodTypeId>
		<PaymentIntervalId>' + CASE WHEN @Monthly = 1 THEN '1' ELSE '12' END  + '</PaymentIntervalId>
		<SortCode>012192</SortCode>
		<AccountNumber>12345112</AccountNumber>
		<BankName>Test Bank Name</BankName>
		<BankAccountName>test Acc name</BankAccountName>
		<RenewalMethodId>74339</RenewalMethodId>
	  </PaymentInfo>
	  <PetQuotes>
		<PetQuote>
		  <PetInfo>
			<PetRef>40bebf67-52b2-486b-a160-dfa3f06f880d</PetRef>
			<PetName>' + @PetName + '</PetName>
			<SpeciesId>42989</SpeciesId>
			<BreedId>' + CONVERT(VARCHAR,@BreedResourceListID) + '</BreedId>
			<Gender>' + CAST(@Pet1Sex as VARCHAR(MAX)) + '</Gender>
			<BirthDate>' + CONVERT(VARCHAR,@Pet1DoB,121) + 'T00:00:00</BirthDate>
			<IsNeutered>true</IsNeutered>
			<HasMicrochip>true</HasMicrochip>
			<PurchasePrice>100</PurchasePrice>
			<VaccinationsUpToDate>false</VaccinationsUpToDate>
			<HasExistingConditions>false</HasExistingConditions>
			<PetColorId>73617</PetColorId>
			<CoInsuranceNextYear>false</CoInsuranceNextYear>
		  </PetInfo>
		  <VetID>140966</VetID>
		  <PolicyValues>
			<PolicyValue>
			  <ProductId>' + CONVERT(VARCHAR,@ProductResourceListID) + '</ProductId>
			  <PolicyLimit>12000</PolicyLimit>
			  <Excess>' + ISNULL(CONVERT(VARCHAR,@Deductible),'0.00') + '</Excess>
			  <VoluntaryExcess>0</VoluntaryExcess>
			  <CoInsurance>' + ISNULL(CONVERT(VARCHAR,@CoPay),'0.00') + '</CoInsurance>
			</PolicyValue>
			<PolicyValueEnrollmentFees>
				<EnrollmentFee>
					<FeeTableRowID>' + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.FeeTableRowID)	FROM @EnrollmentFeeTable t WHERE t.PetID = 1 ),'') + '</FeeTableRowID>
					<EnrollmentFee>' + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.EnrollmentFee)	FROM @EnrollmentFeeTable t WHERE t.PetID = 1 ),'') + '</EnrollmentFee>
					<TaxTableRowID>' + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.TaxTableRowID)	FROM @EnrollmentFeeTable t WHERE t.PetID = 1 ),'') + '</TaxTableRowID>
					<Tax>'			 + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.Tax)				FROM @EnrollmentFeeTable t WHERE t.PetID = 1 ),'') + '</Tax>
					<Total>'		 + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.Total)			FROM @EnrollmentFeeTable t WHERE t.PetID = 1 ),'') + '</Total>
				</EnrollmentFee>
			</PolicyValueEnrollmentFees>
		  </PolicyValues>
		  <StartDate>' + CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121) + 'T00:00:00</StartDate>
		  <UnderwritingList>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>147277</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>154602</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>2000187</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>147279</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>149241</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
		  </UnderwritingList>
		</PetQuote>'
		+ CASE WHEN @MultiPet = 1 THEN
		'<PetQuote>
		  <PetInfo>
			<PetRef>39c6c3ae-218c-46fd-a20d-c98506b195e1</PetRef>
			<PetName>' + @PetName2 + '</PetName>
			<SpeciesId>42990</SpeciesId>
			<BreedId>' + CONVERT(VARCHAR,@BreedResourceListID2) + '</BreedId>
			<Gender>' + CAST(@Pet2Sex as VARCHAR(MAX)) + '</Gender>
			<BirthDate>' + CONVERT(VARCHAR,@Pet2DoB,121) + 'T00:00:00</BirthDate>
			<IsNeutered>true</IsNeutered>
			<HasMicrochip>true</HasMicrochip>
			<PurchasePrice>0</PurchasePrice>
			<VaccinationsUpToDate>false</VaccinationsUpToDate>
			<HasExistingConditions>false</HasExistingConditions>
			<PetColorId>73619</PetColorId>
			<CoInsuranceNextYear>false</CoInsuranceNextYear>
		  </PetInfo>
		  <VetID>141917</VetID>
		  <PolicyValues>
			<PolicyValue>
			  <ProductId>' + CONVERT(VARCHAR,@ProductResourceListID) + '</ProductId>
			  <PolicyLimit>12000</PolicyLimit>
			  <Excess>' + ISNULL(CONVERT(VARCHAR,@Deductible),'0.00') + '</Excess>
			  <VoluntaryExcess>0</VoluntaryExcess>
			  <CoInsurance>' + ISNULL(CONVERT(VARCHAR,@CoPay),'0.00') + '</CoInsurance>
			</PolicyValue>
			<PolicyValueEnrollmentFees>
				<EnrollmentFee>
					<FeeTableRowID>' + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.FeeTableRowID)	FROM @EnrollmentFeeTable t WHERE t.PetID = 2 ),'') + '</FeeTableRowID>
					<EnrollmentFee>' + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.EnrollmentFee)	FROM @EnrollmentFeeTable t WHERE t.PetID = 2 ),'') + '</EnrollmentFee>
					<TaxTableRowID>' + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.TaxTableRowID)	FROM @EnrollmentFeeTable t WHERE t.PetID = 2 ),'') + '</TaxTableRowID>
					<Tax>'			 + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.Tax)				FROM @EnrollmentFeeTable t WHERE t.PetID = 2 ),'') + '</Tax>
					<Total>'		 + ISNULL(( SELECT TOP 1 CONVERT(VARCHAR,t.Total)			FROM @EnrollmentFeeTable t WHERE t.PetID = 2 ),'') + '</Total>
				</EnrollmentFee>
			</PolicyValueEnrollmentFees>
		  </PolicyValues>
		  <StartDate>' + CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121) + '</StartDate>
		  <UnderwritingList>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>147277</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>154602</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>2000187</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>147279</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>5145</AnswerId>
			  <QuestionId>149241</QuestionId>
			  <AnswerText />
			</UnderwritingAnswerItemType>
		  </UnderwritingList>
		</PetQuote>' ELSE '' END
		+
	  '</PetQuotes>
	  <OtherPetsInsured>0</OtherPetsInsured>
	  <DiscountCodes />
	  <KeyValues />
	</BuyPolicyRequest>'
	
	SELECT @RequestXML = CAST(@RequestVarchar as XML)

	IF @RequestXML is not NULL
	BEGIN

		INSERT @XmlResponse ( XmlResponse )
		EXEC _C600_PA_Policy_BuyPolicy @RequestVarchar, 0

		SELECT TOP 1 x.XmlResponse, dbo.fn_C00_GetUrl_CustomerLeadDetailsByObjectID(x.XmlResponse.value('(//MatterID)[1]','INT'),2) [URL], @RequestXML [@RequestXML]
		FROM @XmlResponse x

		DECLARE	 @CustomerID				INT 
				,@CaseID					INT
				,@Comments					VARCHAR(2000) = OBJECT_NAME(@@ProcID)
				,@ProcessStartLeadEventID	INT

		SELECT	 @CustomerID = m.CustomerID
				,@CaseID	 = m.CaseID
				,@ProcessStartLeadEventID = c.ProcessStartLeadEventID
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID
		WHERE m.MatterID = ( SELECT TOP 1 x.XmlResponse.value('(//MatterID)[1]','INT') FROM @XmlResponse x )

		UPDATE cu
		SET  Comments = @CustomerNotes
			,WhenChanged = dbo.fn_GetDate_Local()
			,ChangeSource = OBJECT_NAME(@@ProcID)
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID
	
		IF @CaseID is not NULL
		BEGIN
			EXEC _C00_CreateLetterInEventWithXmlAttachment @CaseID, 159008 /*begin - Upload General Document*/, @WhoCreated, @Comments, @RequestXML, 'Test_Customer_XML'
		END

		-- stop invalidly following up the process start event
		DELETE letc
		FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK) 
		WHERE letc.FromLeadEventID = @ProcessStartLeadEventID
		AND letc.ToEventTypeID = 159008 /*begin - Upload General Document*/

		UPDATE le
		SET  WhenFollowedUp = NULL
			,NextEventID = NULL
		FROM LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @ProcessStartLeadEventID
	END

END







GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Testing_CreateTestCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Testing_CreateTestCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Testing_CreateTestCustomer] TO [sp_executeall]
GO
