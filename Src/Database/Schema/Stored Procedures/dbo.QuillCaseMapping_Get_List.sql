SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuillCaseMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_Get_List]

AS


				
				SELECT
					[QuillCaseMappingID],
					[ClientID],
					[CaseID],
					[QuillCaseCode]
				FROM
					[dbo].[QuillCaseMapping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Get_List] TO [sp_executeall]
GO
