SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CaseTransferMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_Insert]
(

	@CaseTransferMappingID int    OUTPUT,

	@ClientRelationshipID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@MatterID int   ,

	@NewClientID int   ,

	@NewCustomerID int   ,

	@NewLeadID int   ,

	@NewCaseID int   ,

	@NewMatterID int   ,

	@CaseTransferStatusID int   ,

	@NewLeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[CaseTransferMapping]
					(
					[ClientRelationshipID]
					,[ClientID]
					,[CustomerID]
					,[LeadID]
					,[CaseID]
					,[MatterID]
					,[NewClientID]
					,[NewCustomerID]
					,[NewLeadID]
					,[NewCaseID]
					,[NewMatterID]
					,[CaseTransferStatusID]
					,[NewLeadTypeID]
					)
				VALUES
					(
					@ClientRelationshipID
					,@ClientID
					,@CustomerID
					,@LeadID
					,@CaseID
					,@MatterID
					,@NewClientID
					,@NewCustomerID
					,@NewLeadID
					,@NewCaseID
					,@NewMatterID
					,@CaseTransferStatusID
					,@NewLeadTypeID
					)
				-- Get the identity value
				SET @CaseTransferMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Insert] TO [sp_executeall]
GO
