SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PanelItemCharting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_Insert]
(

	@PanelItemChartingID int    OUTPUT,

	@ClientID int   ,

	@PanelItemChartingName varchar (250)  ,

	@PanelID int   ,

	@CreatedBy int   ,

	@ChartID int   
)
AS


				
				INSERT INTO [dbo].[PanelItemCharting]
					(
					[ClientID]
					,[PanelItemChartingName]
					,[PanelID]
					,[CreatedBy]
					,[ChartID]
					)
				VALUES
					(
					@ClientID
					,@PanelItemChartingName
					,@PanelID
					,@CreatedBy
					,@ChartID
					)
				-- Get the identity value
				SET @PanelItemChartingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Insert] TO [sp_executeall]
GO
