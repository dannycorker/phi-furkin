SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Update a QuestionPossibleAnswer from eCatcher
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[UpdateQuestionPossibleAnswer]
	@QuestionPossibleAnswerID  INT,
	@ClientQuestionnaireID INT,
	@MasterQuestionID INT,
	@AnswerText TEXT,
	@Branch INT,
	@ClientID INT, 
	@WhoEdited INT
AS
BEGIN

	UPDATE QuestionPossibleAnswers
	SET ClientQuestionnaireID = @ClientQuestionnaireID,
	MasterQuestionID = @MasterQuestionID,
	AnswerText = @AnswerText,
	Branch = @Branch, 
	WhoModified = @WhoEdited, 
	WhenModified = dbo.fn_GetDate_Local() 
	WHERE QuestionPossibleAnswerID = @QuestionPossibleAnswerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateQuestionPossibleAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateQuestionPossibleAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateQuestionPossibleAnswer] TO [sp_executeall]
GO
