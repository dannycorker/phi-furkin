SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_GetByWhenFollowedUpEventDeleted]
(

	@WhenFollowedUp datetime   ,

	@EventDeleted bit   
)
AS


				SELECT
					[LeadEventID],
					[ClientID],
					[LeadID],
					[WhenCreated],
					[WhoCreated],
					[Cost],
					[Comments],
					[EventTypeID],
					[NoteTypeID],
					[FollowupDateTime],
					[WhenFollowedUp],
					[AquariumEventType],
					[NextEventID],
					[CaseID],
					[LeadDocumentID],
					[NotePriority],
					[DocumentQueueID],
					[EventDeleted],
					[WhoDeleted],
					[DeletionComments],
					[ContactID],
					[BaseCost],
					[DisbursementCost],
					[DisbursementDescription],
					[ChargeOutRate],
					[UnitsOfEffort],
					[CostEnteredManually],
					[IsOnHold],
					[HoldLeadEventID]
				FROM
					[dbo].[LeadEvent] WITH (NOLOCK) 
				WHERE
										[WhenFollowedUp] = @WhenFollowedUp
					AND [EventDeleted] = @EventDeleted
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_GetByWhenFollowedUpEventDeleted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_GetByWhenFollowedUpEventDeleted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_GetByWhenFollowedUpEventDeleted] TO [sp_executeall]
GO
