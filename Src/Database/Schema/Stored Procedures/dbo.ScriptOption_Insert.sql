SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_Insert]
(

	@ScriptOptionID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@ScriptEditorTheme varchar (250)  ,

	@ShowLineNumbers bit   ,

	@CollapseTopPanel bit   ,

	@CollapseLeftPanel bit   ,

	@CollapseRightPanel bit   
)
AS


				
				INSERT INTO [dbo].[ScriptOption]
					(
					[ClientID]
					,[ClientPersonnelID]
					,[ScriptEditorTheme]
					,[ShowLineNumbers]
					,[CollapseTopPanel]
					,[CollapseLeftPanel]
					,[CollapseRightPanel]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelID
					,@ScriptEditorTheme
					,@ShowLineNumbers
					,@CollapseTopPanel
					,@CollapseLeftPanel
					,@CollapseRightPanel
					)
				-- Get the identity value
				SET @ScriptOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Insert] TO [sp_executeall]
GO
