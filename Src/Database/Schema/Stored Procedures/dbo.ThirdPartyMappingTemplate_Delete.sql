SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyMappingTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingTemplate_Delete]
(

	@ThirdPartyMappingTemplateID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyMappingTemplate] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyMappingTemplateID] = @ThirdPartyMappingTemplateID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingTemplate_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Delete] TO [sp_executeall]
GO
