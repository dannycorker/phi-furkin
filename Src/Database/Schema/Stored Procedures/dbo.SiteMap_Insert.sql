SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SiteMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SiteMap_Insert]
(

	@SiteMapID int    OUTPUT,

	@Url varchar (255)  ,

	@Title varchar (255)  ,

	@Description varchar (255)  ,

	@ParentID int   ,

	@Secured bit   
)
AS


				
				INSERT INTO [dbo].[SiteMap]
					(
					[Url]
					,[Title]
					,[Description]
					,[ParentID]
					,[Secured]
					)
				VALUES
					(
					@Url
					,@Title
					,@Description
					,@ParentID
					,@Secured
					)
				-- Get the identity value
				SET @SiteMapID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SiteMap_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Insert] TO [sp_executeall]
GO
