SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AquariumEventSubtype table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_Find]
(

	@SearchUsingOR bit   = null ,

	@AquariumEventSubtypeID int   = null ,

	@AquariumEventTypeID int   = null ,

	@AquariumEventSubtypeName varchar (100)  = null ,

	@AquariumEventSubtypeDescription varchar (250)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null ,

	@ClientID int   = null ,

	@StoredProcedure varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AquariumEventSubtypeID]
	, [AquariumEventTypeID]
	, [AquariumEventSubtypeName]
	, [AquariumEventSubtypeDescription]
	, [WhenCreated]
	, [WhenModified]
	, [ClientID]
	, [StoredProcedure]
    FROM
	[dbo].[AquariumEventSubtype] WITH (NOLOCK) 
    WHERE 
	 ([AquariumEventSubtypeID] = @AquariumEventSubtypeID OR @AquariumEventSubtypeID IS NULL)
	AND ([AquariumEventTypeID] = @AquariumEventTypeID OR @AquariumEventTypeID IS NULL)
	AND ([AquariumEventSubtypeName] = @AquariumEventSubtypeName OR @AquariumEventSubtypeName IS NULL)
	AND ([AquariumEventSubtypeDescription] = @AquariumEventSubtypeDescription OR @AquariumEventSubtypeDescription IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([StoredProcedure] = @StoredProcedure OR @StoredProcedure IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AquariumEventSubtypeID]
	, [AquariumEventTypeID]
	, [AquariumEventSubtypeName]
	, [AquariumEventSubtypeDescription]
	, [WhenCreated]
	, [WhenModified]
	, [ClientID]
	, [StoredProcedure]
    FROM
	[dbo].[AquariumEventSubtype] WITH (NOLOCK) 
    WHERE 
	 ([AquariumEventSubtypeID] = @AquariumEventSubtypeID AND @AquariumEventSubtypeID is not null)
	OR ([AquariumEventTypeID] = @AquariumEventTypeID AND @AquariumEventTypeID is not null)
	OR ([AquariumEventSubtypeName] = @AquariumEventSubtypeName AND @AquariumEventSubtypeName is not null)
	OR ([AquariumEventSubtypeDescription] = @AquariumEventSubtypeDescription AND @AquariumEventSubtypeDescription is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([StoredProcedure] = @StoredProcedure AND @StoredProcedure is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Find] TO [sp_executeall]
GO
