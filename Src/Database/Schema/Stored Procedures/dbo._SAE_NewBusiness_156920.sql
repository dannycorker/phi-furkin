SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM SOFTWARE
-- Create date: 2020-02-03
-- Description:	Sql After Event - Apply Available Discounts - NB / Premium change following MTA - inform customer
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_156920]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			,@OneVisionID					INT
			,@VisionCustomerID				INT
			/* Specific to procedure */

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

		/*GPR 2018-07-31 Add Commission Row to Table on Matter at NB or MTA - Used later for Commission Clawback if any*/
		IF @EventTypeID IN (156920 /*Apply Available Discounts - NB*/, 156862 /*Premium change following MTA - inform customer*/)
		BEGIN
			EXEC dbo._C600_PA_Policy_SaveCommissionData @MatterID, @LeadEventID
		END

		--SELECT @VisionCustomerID = ISNULL(mq.ResponseBody, ''), @OneVisionID = mq.OneVisionID FROM _C00_Mediate_Queue mq WITH (NOLOCK) WHERE mq.ObjectID = @CustomerID

		--IF @VisionCustomerID <> ''
		--BEGIN 

		--UPDATE Customers 
		--SET CustomerRef = @VisionCustomerID
		--WHERE CustomerID = @CustomerID

		--END

		IF @EventTypeID = 156920 /*Apply Available Discounts*/
			AND '' <> ( SELECT cu.CustomerRef FROM Customers cu WITH (NOLOCK) WHERE cu.CustomerID = @CustomerID )
		BEGIN
			
			SELECT @OneVisionID = (SELECT TOP 1 OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID ORDER BY OneVisionID ASC)

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Pets',
			                                      @HttpMethod = 'POST',
			                                      @LeadEventID = @LeadEventID,
												  @OneVisionID = @OneVisionID
		END



		/*GPR 2020-11-25 for SDPRU-163 - fix from SDAAG-171*/
		IF @EventTypeID = 156862 /*Premium change following MTA - inform customer*/
		BEGIN
			
			/*ALM/GPR 2020-11-26 for SDPRU-162*/
			----DECLARE @PurchasedProductID_DV VARCHAR(2000), @PurchasedProductID INT

			----SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)
			----SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
			
			
			/*GPR 2020-12-02*/
			/*---------*/
					DECLARE @PurchasedProductID INT
					DECLARE @PurchasedProductPaymentScheduleID INT
					DECLARE @WrittenPremiumID INT
					DECLARE @AccountID INT
					DECLARE @Now DATETIME
				

					SELECT @PurchasedProductID = p.PurchasedProductID, @AccountID = AccountID
					FROM PurchasedProduct p with (NOLOCK) 
					WHERE p.ObjectID = @MatterID
			
					DECLARE @ValidFrom DATE
					DECLARE @ValidTo DATE
					
				
						/*FROM SDAAG-84- Now we've rebuilt the schedule, calculate the splitmonth remainder*/
						DECLARE @SplitMonthDeltaGross MONEY
						DECLARE @SplitMonthDeltaNet MONEY
						DECLARE @SplitMonthDeltaTax MONEY
						DECLARE @TotalPurchasedProductPaymentScheduleGross MONEY
						DECLARE @TotalPurchasedProductPaymentScheduleNet MONEY
						DECLARE @TotalPurchasedProductPaymentScheduleTax MONEY
						DECLARE @WrittenPremiumLatestGross MONEY
						DECLARE @WrittenPremiumLatestNet MONEY
						DECLARE @WrittenPremiumLatestTax MONEY

						/*Get Latest MTA Schedule Item by PurchasedProductID*/
						SELECT @PurchasedProductPaymentScheduleID = (SELECT PurchasedProductPaymentScheduleID FROM PurchasedProductPaymentSchedule WITH (NOLOCK)
																	WHERE PurchasedProductID = @PurchasedProductID
																	AND PurchasedProductPaymentScheduleTypeID = 6 /*MTA Adjustment*/
																	AND PaymentStatusID = 1 /*New*/)

						/*Sum of Schedule Gross, Net, Tax*/
						SELECT @TotalPurchasedProductPaymentScheduleGross = SUM(ppps.PaymentNet) /*GPR 2020-12-02 changed to Net from Gross to remove TransactionFee*/, @TotalPurchasedProductPaymentScheduleNet = SUM(ppps.PaymentNet), @TotalPurchasedProductPaymentScheduleTax = SUM(ppps.PaymentVAT)
																			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
																			WHERE ppps.PaymentStatusID NOT IN (3)
																			AND ppps.PurchasedProductID = @PurchasedProductID
																			AND ppps.PurchasedProductPaymentScheduleTypeID = 1 /*GPR 2021-02-11 for FURKIN-210*/


						/*Fetch Product Validity Dates*/
						SELECT @ValidFrom = pp.ValidFrom, @ValidTo = pp.ValidTo
						FROM PurchasedProduct pp WITH (NOLOCK)
						WHERE pp.PurchasedProductID = @PurchasedProductID

						EXEC _C00_logit  'GPR', 'MidTermAdjustment','PurchasedProductID',@PurchasedProductID, 58550

						/*Fetch latest WrittenPremium for PurchasedProduct*/
						SELECT @WrittenPremiumID = (SELECT TOP(1) WrittenPremiumID FROM WrittenPremium wp WITH (NOLOCK)
														WHERE wp.MatterID = @MatterID
														AND wp.ValidFrom BETWEEN @ValidFrom AND @ValidTo /*for this Product*/
														ORDER BY 1 DESC)

						/*Sum of WrittenPremium AdjustmentValue Gross, Net, Tax*/
						SELECT @WrittenPremiumLatestGross =	SUM(wp.AdjustmentValueGross),
						@WrittenPremiumLatestNet = SUM(wp.AdjustmentValueNET),
						@WrittenPremiumLatestTax = ISNULL(SUM(AdjustmentValueLocalTax),0.00) + ISNULL(SUM(AdjustmentValueNationalTax),0.00)
																			FROM WrittenPremium wp WITH (NOLOCK)
																			WHERE wp.MatterID = @MatterID
																			AND wp.ValidFrom BETWEEN @ValidFrom AND @ValidTo /*for this Product*/

						/*Identify Difference (should be less than $1)*/									
						SELECT @SplitMonthDeltaGross = (@TotalPurchasedProductPaymentScheduleGross - @WrittenPremiumLatestGross)
						SELECT @SplitMonthDeltaNet = (@TotalPurchasedProductPaymentScheduleNet - @WrittenPremiumLatestNet)
						SELECT @SplitMonthDeltaTax = (@TotalPurchasedProductPaymentScheduleTax - @WrittenPremiumLatestTax)

						EXEC _C00_logit  'GPR', 'MidTermAdjustment','WrittenPremiumLatestGross',@WrittenPremiumLatestGross, 58550


						/*Update PaymentSchedule*/
						UPDATE ppps
						SET PaymentGross = PaymentGross - @SplitMonthDeltaGross,
						 PaymentNet = PaymentNet - @SplitMonthDeltaNet,
						  PaymentVAT = PaymentVAT - @SplitMonthDeltaTax
						FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
						WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

						UPDATE PurchasedProductPaymentSchedule
						SET PaymentGross = PaymentNet + ISNULL(PaymentVAT,0.00)
						FROM PurchasedProductPaymentSchedule
						WHERE PurchasedProductID=@PurchasedProductID
						AND PurchasedProductPaymentScheduleTypeID = 6
						AND PaymentStatusID = 1

						EXEC _C00_logit  'GPR', 'MidTermAdjustment','SplitMonthDeltaGross',@SplitMonthDeltaGross, 58550

						SELECT @Now = dbo.fn_GetDate_Local()

						EXEC _C00_logit  'GPR', 'MidTermAdjustment','Now',@Now,58550
			/*---------*/
			

			SELECT @PurchasedProductID = p.PurchasedProductID
			FROM PurchasedProduct p with (NOLOCK) 
			WHERE p.ObjectID = @MatterID

			EXEC _C00_logit  'GPR', 'MidTermAdjustment SAE','PurchasedProductID',@PurchasedProductID, 58550

			DECLARE @TransactionFee MONEY, @Tax MONEY, @Gross MONEY, @Net MONEY
			
			SELECT @TransactionFee = SUM(ppps.TransactionFee), @Gross = SUM(ppps.PaymentGross) - SUM(ppps.TransactionFee), @Net = SUM(ppps.PaymentNet), @Tax = SUM(ppps.PaymentVAT)
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
			WHERE ppps.PurchasedProductID = @PurchasedProductID
			AND ppps.PaymentStatusID IN (6,1,2) /*GPR 2020-09-25 Added for SDPRU-54*/
			AND ppps.PurchasedProductPaymentScheduleTypeID <> 5 /*GPR 2021-02-11 exclude AdminFee for FURKIN-210*/
			EXEC _C00_logit  'GPR', 'MidTermAdjustment SAE','TransactionFee',@TransactionFee, 58550
						
			UPDATE PurchasedProduct
			SET ProductAdditionalFee = @TransactionFee, ProductCostGross = @Gross, ProductCostNet = @Net, ProductCostVAT = @Tax
			WHERE PurchasedProductID = @PurchasedProductID

			--/*ALM/GPR 2020-11-30 for SDPRU-171*/
			--UPDATE pp
			--SET ProductCostGross = ProductCostGross - ProductAdditionalFee
			--FROM PurchasedProduct pp WITH (NOLOCK)
			--WHERE pp.PurchasedProductID = @PurchasedProductID
			

			EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @Now, @WhoCreated


			EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'MTA' 


		END



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_156920] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_156920] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_156920] TO [sp_executeall]
GO
