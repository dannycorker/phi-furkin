SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- CREATED:		2012-02-21
-- Description:	SLACKY-PERF, reduce calling the run calc proc twice 
--				and saves the last cached calculation
--				IS: 2019-07-03 SLACKY-PERF - breakup the calculation and the save into two parts, 1-CALC&CACHE 2-SAVE
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs_Save]
(
	@CaseID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE  @MatterID	INT
			,@LeadID	INT
			,@ClientID	INT
			
	SELECT	@MatterID = MatterID, 
			@LeadID = LeadID,
			@ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	
	CREATE TABLE #ClaimRows 
	(
		ID INT,
		CustomerID INT,
		LeadID INT,
		CaseID INT,
		MatterID INT,
		ParentID INT,
		TableRowID INT,
		ResourceListID INT,
		ParentResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		DateOfLoss DATE,
		TreatmentStart DATE,
		TreatmentEnd DATE,
		Claim MONEY,
		UserDeductions MONEY,
		Settle MONEY,
		Total MONEY,
		Paid DATE,
		OutsidePolicyCover MONEY,
		Excess MONEY,
		VoluntaryExcess MONEY,
		ExcessRebate MONEY,
		CoInsurance MONEY,
		Limit MONEY,
		TotalSettle MONEY,
		LimitRLID INT,
		AfterCoInsCutIn BIT,
		ClaimRowType VARCHAR(500)
	)

	DECLARE @PolicyLimits TABLE
	(
		ID INT IDENTITY,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		SumInsured MONEY,
		Balance MONEY,
		StartDate DATE,
		EndDate DATE,
		AllowedCount INT,
		LimitTypeID INT,
		LimitType VARCHAR(2000)
	)

	DECLARE @XML_CALC_XML XML = NULL

	SELECT	TOP 1
			@XML_CALC_XML = CAST(Payload AS XML)
	FROM	AsyncQueue (nolock)
	WHERE	QueueTypeID = @CaseID
	AND		[Status] = 0
	ORDER BY AsyncQueueID DESC

	INSERT INTO #ClaimRows
	SELECT	
			t.c.value('ID[1]','INT'),
			t.c.value('CustomerID[1]','INT'),
			t.c.value('LeadID[1]','INT'),
			t.c.value('CaseID[1]','INT'),
			t.c.value('MatterID[1]','INT'),
			t.c.value('ParentID[1]','INT'),
			t.c.value('TableRowID[1]','INT'),
			t.c.value('ResourceListID[1]','INT'),
			t.c.value('ParentResourceListID[1]','INT'),
			t.c.value('Section[1]','VARCHAR(2000)'),
			t.c.value('SubSection[1]','VARCHAR(2000)'),
			t.c.value('DateOfLoss[1]','DATE'),
			t.c.value('TreatmentStart[1]','DATE'),
			t.c.value('TreatmentEnd[1]','DATE'),
			t.c.value('Claim[1]','MONEY'),
			t.c.value('UserDeductions[1]','MONEY'),
			t.c.value('Settle[1]','MONEY'),
			t.c.value('Total[1]','MONEY'),
			t.c.value('Paid[1]','DATE'),
			t.c.value('OutsidePolicyCover[1]','MONEY'),
			t.c.value('Excess[1]','MONEY'),
			t.c.value('VoluntaryExcess[1]','MONEY'),
			t.c.value('ExcessRebate[1]','MONEY'),
			t.c.value('CoInsurance[1]','MONEY'),
			t.c.value('Limit[1]','MONEY'),
			t.c.value('TotalSettle[1]','MONEY'),
			t.c.value('LimitRLID[1]','INT'),
			t.c.value('AfterCoInsCutIn[1]','BIT'),
			t.c.value('ClaimRowType[1]','VARCHAR(500)')
	FROM	@XML_CALC_XML.nodes('//ClaimCalc/ClaimRows/cr') t(c)

	INSERT INTO @PolicyLimits
	SELECT	
			t.c.value('ResourceListID[1]','INT'),
			t.c.value('Section[1]',' VARCHAR(2000)'),
			t.c.value('SubSection[1]','VARCHAR(2000)'),
			t.c.value('SumInsured[1]','MONEY'),
			t.c.value('Balance[1]','MONEY'),
			t.c.value('StartDate[1]','DATE'),
			t.c.value('EndDate[1]','DATE'),
			t.c.value('AllowedCoun[1]','INT'),
			t.c.value('LimitTypeID[1]','INT'),
			t.c.value('LimitType[1]',' VARCHAR(2000)')
	FROM	@XML_CALC_XML.nodes('//ClaimCalc/PolicyLimits/pl') t(c)
	ORDER BY t.c.value('ID[1]','INT') ASC

	-- Need to insert new table rows where the table row ID is 0
	DECLARE @TableRowIDs TABLE
	(
		ID INT,
		TableRowID INT
	)
	INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
	OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowIDs
	SELECT @ClientID, MatterID, 144355, 16157, ID
	FROM #ClaimRows
	WHERE TableRowID = 0
		
	UPDATE c
	SET c.TableRowID = r.TableRowID
	FROM #ClaimRows c
	INNER JOIN @TableRowIDs r ON c.ID = r.ID
	WHERE c.TableRowID = 0
		
		
		
	-- And now the table detail values
	DECLARE @Fields TABLE
	(
		FieldID INT
	)
	INSERT @Fields(FieldID) VALUES
	(146406), 
	(146407), 
	(146408), 
	(144352),
	(144353), 
	(145678), 
	(148399),
	(147434),
	(158802),
	(146179),
	(179568),
	(179564)

	-- Update values that exists -- only not paid rows	
	UPDATE tdv
	SET DetailValue =
		CASE 
			WHEN tdv.DetailFieldID = 146406 THEN CAST(c.Excess AS VARCHAR)
			WHEN tdv.DetailFieldID = 146407 THEN CAST(c.CoInsurance AS VARCHAR)
			WHEN tdv.DetailFieldID = 146408 THEN CAST(c.Limit AS VARCHAR)
			WHEN tdv.DetailFieldID = 144352 THEN CAST(c.Total AS VARCHAR)
			WHEN tdv.DetailFieldID = 144353 THEN CAST(c.Claim AS VARCHAR)
			WHEN tdv.DetailFieldID = 145678 THEN CAST(c.Settle AS VARCHAR)
			WHEN tdv.DetailFieldID = 148399 THEN CAST(c.LimitRLID AS VARCHAR)
			WHEN tdv.DetailFieldID = 147434 THEN CAST(c.ExcessRebate AS VARCHAR)
			WHEN tdv.DetailFieldID = 158802 THEN CAST(c.VoluntaryExcess AS VARCHAR)
			WHEN tdv.DetailFieldID = 146179 THEN CAST(c.UserDeductions AS VARCHAR)
			WHEN tdv.DetailFieldID = 179568 THEN CAST(pl.Balance as VARCHAR) /*Remaining Limit*/
			WHEN tdv.DetailFieldID = 179564 THEN CAST(c.CoInsurance as VARCHAR) /*Coinsurance Percentage*/
		END
	FROM dbo.TableDetailValues tdv (nolock)
	INNER JOIN #ClaimRows c ON tdv.TableRowID = c.TableRowID AND tdv.MatterID = c.MatterID
	LEFT JOIN @PolicyLimits pl on pl.ResourceListID = c.ResourceListID
	WHERE tdv.ClientID = @ClientID
	AND tdv.DetailFieldID IN (SELECT * FROM @Fields)
	AND (c.Paid IS NULL OR c.MatterID = @MatterID) -- Update non paid rows or if recalculating current matter
		
	-- And insert those that don't
	INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
	SELECT DISTINCT @ClientID, f.FieldID, c.CustomerID, c.LeadID, c.CaseID, c.MatterID, c.TableRowID, 
		CASE
			WHEN f.FieldID = 146406 THEN CAST(c.Excess AS VARCHAR)
			WHEN f.FieldID = 146407 THEN CAST(c.CoInsurance AS VARCHAR)
			WHEN f.FieldID = 146408 THEN CAST(c.Limit AS VARCHAR)
			WHEN f.FieldID = 144352 THEN CAST(c.Total AS VARCHAR)
			WHEN f.FieldID = 144353 THEN CAST(c.Claim AS VARCHAR)
			WHEN f.FieldID = 145678 THEN CAST(c.Settle AS VARCHAR)
			WHEN f.FieldID = 148399 THEN CAST(c.LimitRLID AS VARCHAR)
			WHEN f.FieldID = 147434 THEN CAST(c.ExcessRebate AS VARCHAR)
			WHEN f.FieldID = 158802 THEN CAST(c.VoluntaryExcess AS VARCHAR)
			WHEN f.FieldID = 146179 THEN CAST(c.UserDeductions AS VARCHAR)
			WHEN f.FieldID = 179568 THEN CAST(pl.Balance as VARCHAR) /*Remaining Limit*/
			WHEN f.FieldID = 179564 THEN CAST(c.CoInsurance as VARCHAR) /*Coinsurance Percentage*/
		END AS VALUE	
	FROM #ClaimRows c
	CROSS JOIN @Fields f 
	LEFT JOIN @PolicyLimits pl on pl.ResourceListID = c.ResourceListID
	WHERE NOT EXISTS (
		SELECT *
		FROM dbo.TableDetailValues tdv (nolock)
		WHERE tdv.TableRowID = c.TableRowID AND tdv.MatterID = c.MatterID AND tdv.DetailFieldID = f.FieldID
	)
	AND (c.Paid IS NULL OR c.MatterID = @MatterID) -- insert non paid rows or if recalculating current matter
		
	-- Finally insert any resource list values if they don't exist - for split rows
	INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, ResourceListID)
	SELECT @ClientID, 144350, c.CustomerID, c.LeadID, c.CaseID, c.MatterID, c.TableRowID, c.ResourceListID
	FROM #ClaimRows c
	WHERE NOT EXISTS (
		SELECT *
		FROM dbo.TableDetailValues tdv (nolock)
		WHERE tdv.TableRowID = c.TableRowID AND tdv.MatterID = c.MatterID AND tdv.DetailFieldID = 144350
	)
	AND (c.Paid IS NULL OR c.MatterID = @MatterID) -- insert non paid rows or if recalculating current matter

	UPDATE	AsyncQueue
	SET		[Status] = 1
	WHERE	QueueTypeID = @CaseID
	AND		[Status] = 0

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs_Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs_Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs_Save] TO [sp_executeall]
GO
