SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2007-12-19
-- Description:	Just a wrapper for function fnCanLeadEventBeDeleted()
-- =============================================
CREATE PROCEDURE [dbo].[CanLeadEventBeDeleted] 
	-- Add the parameters for the stored procedure here
	@LeadEventID int, 
	@UserID int 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dbo.fnCanLeadEventBeDeleted(@LeadEventID, @UserID)
END



GO
GRANT VIEW DEFINITION ON  [dbo].[CanLeadEventBeDeleted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CanLeadEventBeDeleted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CanLeadEventBeDeleted] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[CanLeadEventBeDeleted] TO [sp_executehelper]
GO
