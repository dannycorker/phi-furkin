SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetClientQuestionnaires    Script Date: 08/09/2006 12:22:46 ******/

CREATE PROCEDURE [dbo].[GetClientQuestionnaires] @ClientID int

AS

Select ClientQuestionnaireID, ClientID, QuestionnaireTitle, QuestionnaireDescription, QuestionsPerPage, QuestionnaireLogo, QuestionnaireLogoFileName, QuestionnaireIntroductionText, HideIntro, QuestionnaireFooterText, HideFooter, Published, LinkedQuestionnaireClientQuestionnaireID, QuestionnaireFooterIframe, QuestionnaireHeaderIframe,  QuestionnaireFooterInternal, QuestionnaireHeaderInternal, QuestionnaireFooterIframeHeight, QuestionnaireHeaderIframeHeight, QuestionnaireFooterIframeWidth, QuestionnaireHeaderIframeWidth, DefaultEmailAddress, MailingListType, FrameMode, LayoutCss, LayoutCssFileName, ImportDirectlyIntoLeadManager, RunAsClientPersonnelID, RememberAnswers
From ClientQuestionnaires
Where ClientID = @ClientID




GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientQuestionnaires] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientQuestionnaires] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientQuestionnaires] TO [sp_executeall]
GO
