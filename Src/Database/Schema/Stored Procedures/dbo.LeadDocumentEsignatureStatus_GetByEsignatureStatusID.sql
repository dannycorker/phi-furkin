SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDocumentEsignatureStatus table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_GetByEsignatureStatusID]
(

	@EsignatureStatusID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadDocumentEsignatureStatusID],
					[ClientID],
					[LeadDocumentID],
					[ElectronicSignatureDocumentKey],
					[DocumentName],
					[SentTo],
					[DateSent],
					[EsignatureStatusID],
					[StatusCheckedDate]
				FROM
					[dbo].[LeadDocumentEsignatureStatus] WITH (NOLOCK) 
				WHERE
					[EsignatureStatusID] = @EsignatureStatusID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_GetByEsignatureStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_GetByEsignatureStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_GetByEsignatureStatusID] TO [sp_executeall]
GO
