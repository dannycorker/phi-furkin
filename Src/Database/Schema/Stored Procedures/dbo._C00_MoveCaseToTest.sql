SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-04-18
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MoveCaseToTest]
@CaseIDToMove int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @AquariumClientID int = 20,
		@NewCustomerID int,
		@NewLeadID int,
		@OldLeadID int

			INSERT INTO [dbo].[Customers]([ClientID],[TitleID],[IsBusiness],[FirstName],[LastName],[DayTimeTelephoneNumberVerifiedAndValid],[HomeTelephoneVerifiedAndValid],[MobileTelephoneVerifiedAndValid],[CompanyTelephone],[CompanyTelephoneVerifiedAndValid], [WorksTelephoneVerifiedAndValid], [Website],[HasDownloaded],[DownloadedOn],[AquariumStatusID],[ClientStatusID],[Test],[PhoneNumbersVerifiedOn],[DoNotEmail],[DoNotSellToThirdParty],[AgreedToTermsAndConditions],[DateOfBirth],[DefaultContactID],[DefaultOfficeID])
			VALUES (@AquariumClientID,0,0,@CaseIDToMove,@CaseIDToMove,NULL, NULL, NULL, NULL, NULL, NUll, NULL, NULL, NULL, 2, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL)

			Select @NewCustomerID = SCOPE_IDENTITY()

			INSERT INTO [dbo].[Lead]([ClientID],[LeadRef],[CustomerID],[LeadTypeID],[AquariumStatusID],[ClientStatusID],[BrandNew],[Assigned],[AssignedTo],[AssignedBy],[AssignedDate],[RecalculateEquations],[Password],[Salt],[WhenCreated])
			VALUES(@AquariumClientID, '',@NewCustomerID,20,2,NULL,0,0,NULL,NULL,NULL,0,NUll,NULL,dbo.fn_GetDate_Local())

			Select @NewLeadID = SCOPE_IDENTITY()

			Select @OldLeadID = LeadID
			From Cases
			Where CaseID = @CaseIDToMove

			Update Cases
			set LeadID = @NewLeadID, CaseNum = 1, CaseRef = 'Case 1'
			Where CaseID = @CaseIDToMove

			Update Matter
			set LeadID = @NewLeadID, CustomerID = @NewCustomerID
			Where CaseID = @CaseIDToMove

			Update MatterDetailValues
			Set MatterDetailValues.LeadID = @NewLeadID
			From MatterDetailValues
			Inner Join Matter on Matter.MatterID = MatterDetailValues.MatterID and Matter.CaseID = @CaseIDToMove

			Update TableRows
			Set TableRows.LeadID = @NewLeadID
			From TableRows
			Inner Join Matter on Matter.MatterID = TableRows.MatterID and Matter.CaseID = @CaseIDToMove

			Update TableDetailValues
			Set TableDetailValues.LeadID = @NewLeadID
			From TableDetailValues
			Inner Join Matter on Matter.MatterID = TableDetailValues.MatterID and Matter.CaseID = @CaseIDToMove

			Update LeadEvent
			Set LeadID = @NewLeadID
			Where CaseID = @CaseIDToMove

			Update LeadDocument
			set LeadID = @NewLeadID
			From LeadDocument
			Inner Join LeadEvent on LeadEvent.LeadDocumentID = LeadDocument.LeadDocumentID and LeadEvent.CaseID = @CaseIDToMove

			UPDATE dbo.LeadDocumentFS
			SET LeadID = @NewLeadID
			FROM dbo.LeadDocumentFS
			INNER JOIN dbo.LeadEvent ON LeadEvent.LeadDocumentID = LeadDocumentFS.LeadDocumentID AND LeadEvent.CaseID = @CaseIDToMove

			Update LeadEventThreadCompletion
			Set LeadID = @NewLeadID
			Where CaseID = @CaseIDToMove

			Update CaseTransferMapping
			Set LeadID = @NewLeadID, CustomerID = @NewCustomerID
			Where CaseID = @CaseIDToMove

			Update DocumentQueue
			Set LeadID = @NewLeadID, CustomerID = @NewCustomerID
			Where CaseID = @CaseIDToMove

			Update DetailValueHistory
			Set DetailValueHistory.LeadID = @NewLeadID
			From DetailValueHistory
			Inner Join Matter on Matter.MatterID = DetailValueHistory.MatterID and Matter.CaseID = @CaseIDToMove

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MoveCaseToTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MoveCaseToTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MoveCaseToTest] TO [sp_executeall]
GO
