SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LookupListItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_Delete]
(

	@LookupListItemID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[LookupListItems] WITH (ROWLOCK) 
				WHERE
					[LookupListItemID] = @LookupListItemID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Delete] TO [sp_executeall]
GO
