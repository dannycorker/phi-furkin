SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPreferenceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreferenceType_Delete]
(

	@ClientPreferenceTypeID int   
)
AS


				DELETE FROM [dbo].[ClientPreferenceType] WITH (ROWLOCK) 
				WHERE
					[ClientPreferenceTypeID] = @ClientPreferenceTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreferenceType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Delete] TO [sp_executeall]
GO
