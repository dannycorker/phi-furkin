SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the IncendiaConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationAquarium_Update]
(

	@IncendiaConfigurationAquariumID int   ,

	@AccountID varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[IncendiaConfigurationAquarium]
				SET
					[AccountID] = @AccountID
				WHERE
[IncendiaConfigurationAquariumID] = @IncendiaConfigurationAquariumID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationAquarium_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Update] TO [sp_executeall]
GO
