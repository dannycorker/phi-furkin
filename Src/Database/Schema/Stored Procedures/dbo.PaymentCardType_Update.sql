SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PaymentCardType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentCardType_Update]
(

	@PaymentCardTypeID int   ,

	@PaymentCardType varchar (50)  ,

	@DisplayOrder int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PaymentCardType]
				SET
					[PaymentCardType] = @PaymentCardType
					,[DisplayOrder] = @DisplayOrder
				WHERE
[PaymentCardTypeID] = @PaymentCardTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentCardType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Update] TO [sp_executeall]
GO
