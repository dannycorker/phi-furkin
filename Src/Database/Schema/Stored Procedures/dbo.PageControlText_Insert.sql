SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PageControlText table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_Insert]
(

	@PageControlTextID int    OUTPUT,

	@ClientID int   ,

	@LanguageID int   ,

	@PageName varchar (250)  ,

	@ControlName varchar (250)  ,

	@ControlText varchar (MAX)  ,

	@TooltipText varchar (MAX)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[PageControlText]
					(
					[ClientID]
					,[LanguageID]
					,[PageName]
					,[ControlName]
					,[ControlText]
					,[TooltipText]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@LanguageID
					,@PageName
					,@ControlName
					,@ControlText
					,@TooltipText
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @PageControlTextID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Insert] TO [sp_executeall]
GO
