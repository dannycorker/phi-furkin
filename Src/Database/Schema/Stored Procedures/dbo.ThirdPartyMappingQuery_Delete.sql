SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyMappingQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_Delete]
(

	@ThirdPartyMappingQueryID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyMappingQuery] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyMappingQueryID] = @ThirdPartyMappingQueryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Delete] TO [sp_executeall]
GO
