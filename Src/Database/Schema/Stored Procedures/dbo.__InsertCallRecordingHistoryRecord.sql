SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-10-01
-- Description:	Inserts a record into the call recording table
-- =============================================
CREATE PROCEDURE [dbo].[__InsertCallRecordingHistoryRecord]
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@AgentFirstName VARCHAR(2000),
	@AgentLastName VARCHAR(2000),
	@RecordingStoppedAt VARCHAR(2000),
	@RecordingStartedAt  VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @LeadTypeID INT,
			@CallRecordingHistoryTableFieldID INT,
			@CallHistoryPageID INT,
			@AgentFirstNameFieldID INT,
			@AgentLastNameFieldID INT,
			@RecordingStoppedAtFieldID INT,
			@RecordingStartedAtFieldID INT,
			@TableRowID INT
	
	SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID	
	
	SELECT @AgentFirstNameFieldID = t.ColumnFieldID, @CallRecordingHistoryTableFieldID = t.DetailFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 346	AND t.ClientID = @ClientID	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @AgentLastNameFieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 347	AND t.ClientID = @ClientID	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @RecordingStoppedAtFieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 348	AND t.ClientID = @ClientID	AND t.LeadTypeID = @LeadTypeID

	SELECT @RecordingStartedAtFieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 349	AND t.ClientID = @ClientID	AND t.LeadTypeID = @LeadTypeID

	SELECT @CallHistoryPageID = DetailFieldPageID 
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID=@CallRecordingHistoryTableFieldID

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
	VALUES (@ClientID, @CustomerID, @CallRecordingHistoryTableFieldID, @CallHistoryPageID, 1, 1)
	
	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	VALUES  (@ClientID, @CustomerID, @TableRowID, @AgentFirstNameFieldID, @AgentFirstName),
			(@ClientID, @CustomerID, @TableRowID, @AgentLastNameFieldID, @AgentLastName),
			(@ClientID, @CustomerID, @TableRowID, @RecordingStoppedAtFieldID, @RecordingStoppedAt),
			(@ClientID, @CustomerID, @TableRowID, @RecordingStartedAtFieldID, @RecordingStartedAt)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallRecordingHistoryRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__InsertCallRecordingHistoryRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallRecordingHistoryRecord] TO [sp_executeall]
GO
