SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetSecuredSiteMapChildren    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetSecuredSiteMapChildren] @ParentID int

AS

SELECT     SiteMapID, Url, Title, Description, ParentID, Secured
FROM         dbo.SiteMap
WHERE     (ParentID = @ParentID) AND (Secured = 1)




GO
GRANT VIEW DEFINITION ON  [dbo].[GetSecuredSiteMapChildren] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSecuredSiteMapChildren] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSecuredSiteMapChildren] TO [sp_executeall]
GO
