SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date:	2019-11-26
-- Description:	Pushes a request onto the Liberty Mediate API queue.
--
-- 2020-01-03 MAB added header comments + validation
-- 2020-01-08 MAB modified to only validate parameters if called from an event
-- 2020-04-02 CPS for JIRA AAG-607 | allow supression of output so we don't create unnecessary feedback from SAE
-- 2020-07-09 ALM Added @OneVisionID to _C00_Mediate_Queue insert
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Mediate_Push]
(
    @AppKey         [NVARCHAR](50),
    @Queue          [NVARCHAR](50),
    @Flow           [NVARCHAR](50),
    @Action         [NVARCHAR](50),
    @Method         [NVARCHAR](50),
	@ObjectID		[NVARCHAR](100),
    @RequestBody    [NVARCHAR](MAX),
    @RequestType    [NVARCHAR](50),
    @ResponseBody   [NVARCHAR](MAX),
    @ResponseType   [NVARCHAR](50),
    @Exception      [NVARCHAR](2000)='',
    @ExceptionCount INT=0,
    @QueueID        [bigint]=0 OUTPUT,
	@ParentObjectID	[NVARCHAR](100) = NULL,
	@SuppressOutput	[BIT] = 0,
	@OneVisionID	INT
) AS
BEGIN
 
	SET NOCOUNT ON

	DECLARE
		@ErrorMessage VARCHAR(2000) = '<br><br><font color="red">_C00_Mediate_Push:<br>'
		,@ErrorMessageSeparator VARCHAR(4) = '<br>'
		,@ErrorMessageSuffix VARCHAR(7) = '</font>'
		,@InvalidInputData BIT = 0

	IF ISNULL(@Queue, N'') NOT IN (N'In', N'Out')
	BEGIN
		SELECT @ErrorMessage += 'An invalid Queue parameter value of "' + ISNULL(@Queue, 'NULL') + '" was passed in. Valid values are In or Out' + @ErrorMessageSuffix

		RAISERROR (@ErrorMessage, 16, 1)
	END

	/*2020-01-08 MAB only validate other input parameters if called from an event*/
	IF @Queue = 'Out'
	BEGIN

		/*2020-01-03 MAB validate parameters*/
		IF ISNULL(@Flow, N'') NOT IN (N'Queue', N'Direct')
		BEGIN
			SELECT @ErrorMessage += 'An invalid Flow parameter value of "' + ISNULL(@Flow, 'NULL') + '" was passed in. Valid values are Queue or Direct' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF ISNULL(@Action, N'') <> N'Push'
		BEGIN
			SELECT @ErrorMessage += 'An invalid Action parameter value of "' + ISNULL(@Action, 'NULL') + '" was passed in. Valid value is Push' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF ISNULL(@Method, N'') NOT IN (N'POST', N'PUT')
		BEGIN
			SELECT @ErrorMessage += 'An invalid or unsupported @Method parameter value of "' + ISNULL(@Method, 'NULL') + '" was passed in. Valid values are POST or PUT' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF @ObjectID IS NULL
		BEGIN
			SELECT @ErrorMessage += 'A NULL ObjectID parameter value was passed in. Value must be a GUID' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF ISNULL(@RequestType, N'') NOT IN (N'CustomerRequest', N'PetRequest', N'PolicyRequest', N'ClaimRequest') AND @RequestType NOT like 'Vision%'
		BEGIN
			SELECT @ErrorMessage += 'An invalid or unsupported RequestType parameter value of "' + ISNULL(@RequestType, 'NULL') + '" was passed in. Valid values are CustomerRequest, PetRequest, PolicyRequest or ClaimRequest' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF @ParentObjectID IS NULL AND @Method = N'POST' AND @RequestType <> N'CustomerRequest' AND @RequestType NOT like 'Vision%'
		BEGIN
			SELECT @ErrorMessage += 'A NULL ParentObjectID parameter value was passed in. Value must be a GUID' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF ISNULL(@RequestBody, N'') = N'' OR @RequestBody = N'{}'
		BEGIN
			SELECT @ErrorMessage += 'A NULL or empty RequestBody parameter value was passed in. Value must not be empty' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF ISNULL(@ResponseType, N'') NOT IN (N'CustomerResponse', N'PetResponse', N'PolicyResponse', N'ClaimResponse', N'SimpleValueResponse') AND @ResponseType NOT like 'Vision%'
		BEGIN
			SELECT @ErrorMessage += 'An invalid or unsupported ResponseType parameter value of "' + ISNULL(@ResponseType, 'NULL') + '" was passed in. Valid values are CustomerResponse, PetResponse, PolicyResponse or ClaimResponse' + @ErrorMessageSeparator
				,@InvalidInputData = 1
		END

		IF @InvalidInputData = 1
		BEGIN
			/*Remove the final pipe character and add the closing tag*/
			SELECT @ErrorMessage = SUBSTRING(@ErrorMessage, 1, LEN(@ErrorMessage)-3) + @ErrorMessageSuffix

			RAISERROR (@ErrorMessage, 16, 1)

			RETURN
		END

	END

	IF @SuppressOutput = 1
	BEGIN
	    INSERT INTO [dbo].[_C00_Mediate_Queue]([AppKey],[Queue],[Flow],[Action],[Method],[ObjectID],[ParentObjectID],[RequestBody],[RequestType],[ResponseBody],[ResponseType],[Exception],[ExceptionCount],[OneVisionID])
		VALUES (@AppKey,@Queue,@Flow,@Action,@Method,@ObjectID,@ParentObjectID,@RequestBody,@RequestType,@ResponseBody,@ResponseType,@Exception,@ExceptionCount,@OneVisionID)
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[_C00_Mediate_Queue]([AppKey],[Queue],[Flow],[Action],[Method],[ObjectID],[ParentObjectID],[RequestBody],[RequestType],[ResponseBody],[ResponseType],[Exception],[ExceptionCount],[OneVisionID])
		OUTPUT inserted.*
		VALUES (@AppKey,@Queue,@Flow,@Action,@Method,@ObjectID,@ParentObjectID,@RequestBody,@RequestType,@ResponseBody,@ResponseType,@Exception,@ExceptionCount,@OneVisionID)
	END

	SELECT @QueueID = SCOPE_IDENTITY()
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Push] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Mediate_Push] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Push] TO [sp_executeall]
GO
