SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderDataType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDataType_GetByDataTypeName]
(

	@DataTypeName varchar (250)  
)
AS


				SELECT
					[DataLoaderDataTypeID],
					[DataTypeName],
					[DataTypeDescription]
				FROM
					[dbo].[DataLoaderDataType] WITH (NOLOCK) 
				WHERE
										[DataTypeName] = @DataTypeName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_GetByDataTypeName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDataType_GetByDataTypeName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_GetByDataTypeName] TO [sp_executeall]
GO
