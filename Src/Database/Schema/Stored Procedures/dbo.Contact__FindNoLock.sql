SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Contact table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact__FindNoLock]
(

	@SearchUsingOR bit   = null ,

	@ContactID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@TitleID int   = null ,

	@Firstname varchar (100)  = null ,

	@Middlename varchar (100)  = null ,

	@Lastname varchar (100)  = null ,

	@Fullname varchar (201)  = null ,

	@EmailAddressWork varchar (100)  = null ,

	@EmailAddressOther varchar (100)  = null ,

	@DirectDial varchar (100)  = null ,

	@MobilePhoneWork varchar (100)  = null ,

	@MobilePhoneOther varchar (100)  = null ,

	@Address1 varchar (100)  = null ,

	@Address2 varchar (100)  = null ,

	@Town varchar (100)  = null ,

	@County varchar (100)  = null ,

	@Postcode varchar (100)  = null ,

	@Country varchar (100)  = null ,

	@OfficeID int   = null ,

	@DepartmentID int   = null ,

	@JobTitle varchar (100)  = null ,

	@Notes varchar (255)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ContactID]
	, [ClientID]
	, [CustomerID]
	, [TitleID]
	, [Firstname]
	, [Middlename]
	, [Lastname]
	, [Fullname]
	, [EmailAddressWork]
	, [EmailAddressOther]
	, [DirectDial]
	, [MobilePhoneWork]
	, [MobilePhoneOther]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [OfficeID]
	, [DepartmentID]
	, [JobTitle]
	, [Notes]
    FROM
	[dbo].[Contact] with (nolock)
    WHERE 
	 ([ContactID] = @ContactID OR @ContactID is null)
	AND ([ClientID] = @ClientID OR @ClientID is null)
	AND ([CustomerID] = @CustomerID OR @CustomerID is null)
	AND ([TitleID] = @TitleID OR @TitleID is null)
	AND ([Firstname] = @Firstname OR @Firstname is null)
	AND ([Middlename] = @Middlename OR @Middlename is null)
	AND ([Lastname] = @Lastname OR @Lastname is null)
	AND ([Fullname] = @Fullname OR @Fullname is null)
	AND ([EmailAddressWork] = @EmailAddressWork OR @EmailAddressWork is null)
	AND ([EmailAddressOther] = @EmailAddressOther OR @EmailAddressOther is null)
	AND ([DirectDial] = @DirectDial OR @DirectDial is null)
	AND ([MobilePhoneWork] = @MobilePhoneWork OR @MobilePhoneWork is null)
	AND ([MobilePhoneOther] = @MobilePhoneOther OR @MobilePhoneOther is null)
	AND ([Address1] = @Address1 OR @Address1 is null)
	AND ([Address2] = @Address2 OR @Address2 is null)
	AND ([Town] = @Town OR @Town is null)
	AND ([County] = @County OR @County is null)
	AND ([Postcode] = @Postcode OR @Postcode is null)
	AND ([Country] = @Country OR @Country is null)
	AND ([OfficeID] = @OfficeID OR @OfficeID is null)
	AND ([DepartmentID] = @DepartmentID OR @DepartmentID is null)
	AND ([JobTitle] = @JobTitle OR @JobTitle is null)
	AND ([Notes] = @Notes OR @Notes is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ContactID]
	, [ClientID]
	, [CustomerID]
	, [TitleID]
	, [Firstname]
	, [Middlename]
	, [Lastname]
	, [Fullname]
	, [EmailAddressWork]
	, [EmailAddressOther]
	, [DirectDial]
	, [MobilePhoneWork]
	, [MobilePhoneOther]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [OfficeID]
	, [DepartmentID]
	, [JobTitle]
	, [Notes]
    FROM
	[dbo].[Contact] with (nolock)
    WHERE 
	 ([ContactID] = @ContactID AND @ContactID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([Firstname] = @Firstname AND @Firstname is not null)
	OR ([Middlename] = @Middlename AND @Middlename is not null)
	OR ([Lastname] = @Lastname AND @Lastname is not null)
	OR ([Fullname] = @Fullname AND @Fullname is not null)
	OR ([EmailAddressWork] = @EmailAddressWork AND @EmailAddressWork is not null)
	OR ([EmailAddressOther] = @EmailAddressOther AND @EmailAddressOther is not null)
	OR ([DirectDial] = @DirectDial AND @DirectDial is not null)
	OR ([MobilePhoneWork] = @MobilePhoneWork AND @MobilePhoneWork is not null)
	OR ([MobilePhoneOther] = @MobilePhoneOther AND @MobilePhoneOther is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([Postcode] = @Postcode AND @Postcode is not null)
	OR ([Country] = @Country AND @Country is not null)
	OR ([OfficeID] = @OfficeID AND @OfficeID is not null)
	OR ([DepartmentID] = @DepartmentID AND @DepartmentID is not null)
	OR ([JobTitle] = @JobTitle AND @JobTitle is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	Select @@ROWCOUNT			
  END
				





GO
GRANT VIEW DEFINITION ON  [dbo].[Contact__FindNoLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact__FindNoLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact__FindNoLock] TO [sp_executeall]
GO
