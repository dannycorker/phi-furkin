SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SMSQuestionGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_Insert]
(

	@SMSQuestionGroupID int    OUTPUT,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@GroupName varchar (250)  ,

	@SMSQuestionOrderID int   ,

	@GroupOrder int   
)
AS


				
				INSERT INTO [dbo].[SMSQuestionGroup]
					(
					[ClientID]
					,[SMSQuestionnaireID]
					,[GroupName]
					,[SMSQuestionOrderID]
					,[GroupOrder]
					)
				VALUES
					(
					@ClientID
					,@SMSQuestionnaireID
					,@GroupName
					,@SMSQuestionOrderID
					,@GroupOrder
					)
				-- Get the identity value
				SET @SMSQuestionGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Insert] TO [sp_executeall]
GO
