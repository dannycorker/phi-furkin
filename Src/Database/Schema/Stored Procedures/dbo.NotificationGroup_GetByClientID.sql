SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the NotificationGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroup_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[NotificationGroupID],
					[ClientID],
					[NotificationGroupName],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[NotificationGroup] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroup_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_GetByClientID] TO [sp_executeall]
GO
