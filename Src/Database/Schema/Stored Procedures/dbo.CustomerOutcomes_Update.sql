SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CustomerOutcomes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOutcomes_Update]
(

	@CustomerOutcomeID int   ,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@OutcomeID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CustomerOutcomes]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[CustomerID] = @CustomerID
					,[OutcomeID] = @OutcomeID
					,[ClientID] = @ClientID
				WHERE
[CustomerOutcomeID] = @CustomerOutcomeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOutcomes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Update] TO [sp_executeall]
GO
