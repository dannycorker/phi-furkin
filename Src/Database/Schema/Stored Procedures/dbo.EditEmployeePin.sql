SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Angel Petrov
-- Create date: 2014-24-07	
-- Description:	Gets the Guest Name and Check In Status of the specific guest on the 
-- =============================================
Create PROCEDURE [dbo].[EditEmployeePin]
@Pin int,
@ClientPersonnelID int
AS
BEGIN
SET NOCOUNT ON;
UPDATE dbo.TrackerEmployee
SET Pin = @Pin
WHERE ClientPErsonnelID = @ClientPersonnelID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[EditEmployeePin] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EditEmployeePin] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EditEmployeePin] TO [sp_executeall]
GO
