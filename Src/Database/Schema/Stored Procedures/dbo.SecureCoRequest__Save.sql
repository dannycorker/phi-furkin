SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 23/03/2020
-- Description:	Saves the secure co request and response information
-- =============================================
CREATE PROCEDURE [dbo].[SecureCoRequest__Save]
	@RequestID INT = NULL,
	@ResponseID INT = NULL,
	@ClientID INT,
	@RequestJson nVARCHAR(MAX),
	@ResponseJson nVARCHAR(MAX),
	@RequestType VARCHAR(20),
	@MetaData nVARCHAR(MAX),
	@WhoCreated INT 
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @SecureCoRequestID INT
	DECLARE @SecureCoResponseID INT

	IF @RequestID IS NOT NULL AND @RequestID > 0
	BEGIN

		UPDATE SecureCoRequest
		SET MetaData = @MetaData, RequestJson = @RequestJson, WhenCreated = dbo.fn_GetDate_Local()
		WHERE SecureCoRequestID = @RequestID

		SET @SecureCoRequestID = @RequestID
	END
	ELSE
	BEGIN

		INSERT INTO SecureCoRequest (ClientID, MetaData, RequestJson, RequestType, WhenCreated, WhoCreated)
		VALUES (@ClientID, @MetaData, @RequestJson, @RequestType, dbo.fn_GetDate_Local(), @WhoCreated)

		SELECT @SecureCoRequestID = SCOPE_IDENTITY()

	END

	IF @ResponseID IS NOT NULL AND @ResponseID > 0
	BEGIN
		
		UPDATE SecureCoResponse
		SET MetaData = @MetaData, ResponseJson = @ResponseJson, WhenCreated = dbo.fn_GetDate_Local()
		WHERE SecureCoResponseID = @ResponseID

		SET @SecureCoResponseID = @ResponseID

	END
	ELSE
	BEGIN

		INSERT INTO SecureCoResponse (ClientID, SecureCoRequestID, MetaData, ResponseJson, ResponseType, WhenCreated, WhoCreated)
		VALUES (@ClientID, @SecureCoRequestID, @MetaData, @ResponseJson, @RequestType, dbo.fn_GetDate_Local(), @WhoCreated)

		SELECT @SecureCoResponseID = SCOPE_IDENTITY()

	END

	SELECT @SecureCoRequestID SecureCoRequestID, @SecureCoResponseID SecureCoResponseID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SecureCoRequest__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SecureCoRequest__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SecureCoRequest__Save] TO [sp_executeall]
GO
