SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserCase table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserCase_GetByPortalUserIDLeadIDCaseIDClientID]
(

	@PortalUserID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@ClientID int   
)
AS


				SELECT
					[PortalUserCaseID],
					[PortalUserID],
					[LeadID],
					[CaseID],
					[ClientID]
				FROM
					[dbo].[PortalUserCase] WITH (NOLOCK) 
				WHERE
										[PortalUserID] = @PortalUserID
					AND [LeadID] = @LeadID
					AND [CaseID] = @CaseID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_GetByPortalUserIDLeadIDCaseIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_GetByPortalUserIDLeadIDCaseIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_GetByPortalUserIDLeadIDCaseIDClientID] TO [sp_executeall]
GO
