SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2013-01-17
-- Description:	Resource List Helper
-- =============================================
CREATE PROCEDURE [dbo].[rl]
	@ClientID INT = 0,
	@FriendlyName VARCHAR(50) = '',
	@DetailFieldPageID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Show resource lists for this client, optionally matching on friendly name */
	IF @DetailFieldPageID > 0
	BEGIN
		SELECT 'Specific Page' AS [Matching Resource Lists], dfp.* 
		FROM dbo.DetailFieldPages dfp WITH (NOLOCK) 
		WHERE dfp.ClientID = @ClientID 
		AND dfp.ResourceList = 1 
		AND dfp.DetailFieldPageID = @DetailFieldPageID
		
		IF @@ROWCOUNT = 1
		BEGIN
			
			/* Show the fields in the resource list */
			SELECT df.FieldName AS [RL Fields], df.* 
			FROM dbo.DetailFields df WITH (NOLOCK) 
			WHERE df.DetailFieldPageID = @DetailFieldPageID 
			ORDER BY df.FieldOrder
			
			/* Show where the resource list is used in the app */
			SELECT df.FieldName AS [RL Used Here], dfp.PageName AS [Page], dfs.DetailFieldSubTypeName AS [Lead Or Matter ;-)], df.* 
			FROM dbo.DetailFields df WITH (NOLOCK) 
			INNER JOIN dbo.DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
			INNER JOIN dbo.DetailFieldSubType dfs WITH (NOLOCK) ON dfs.DetailFieldSubTypeID = df.LeadOrMatter 
			WHERE df.ResourceListDetailFieldPageID = @DetailFieldPageID 
			
			/* Show the values in the resource list */
			SELECT rl.ResourceListID, rl.ResourceListHelperName, rl.ResourceListHelperCode, rldv.DetailFieldID, rldv.DetailValue, df.FieldName, df.FieldOrder, df.Enabled
			FROM dbo.DetailFields df WITH (NOLOCK) 
			INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID 
			INNER JOIN dbo.ResourceList rl WITH (NOLOCK) ON rl.ResourceListID = rldv.ResourceListID
			WHERE df.DetailFieldPageID = @DetailFieldPageID 
			ORDER BY rldv.ResourceListID, df.FieldOrder 			
		END
		ELSE
		BEGIN
			SELECT 'No resource list page was found for that ClientID and DetailFieldPageID'
		END
		
	END
	ELSE
	BEGIN
		SELECT 'Matching Pages' AS [Matching Resource Lists], dfp.* 
		FROM dbo.DetailFieldPages dfp WITH (NOLOCK) 
		WHERE dfp.ClientID = @ClientID 
		AND dfp.ResourceList = 1 
		AND dfp.PageName LIKE '%' + @FriendlyName + '%'
		ORDER BY dfp.PageName 
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[rl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[rl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[rl] TO [sp_executeall]
GO
