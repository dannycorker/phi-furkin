SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis 
-- Create date: 2018-06-19
-- Description:	Create a refund to be automatically requested tonight, Or a refund that has already been paid 
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[Billing_Support_CreateRefundOrCollection]
(
	@AccountID INT,
	@PurchasedProductID INT,
	@PaymentGross DECIMAL (18,2),
	@CoverFrom DATE,
	@CoverTo DATE, 
	@AlreadyPaid BIT = 0,
	@PPPSToRefund INT = NULL,
	@Notes VARCHAR(MAX),
	@TransactionFee NUMERIC(18,2)
)
AS
BEGIN

	DECLARE @CustomerID INT,
			@Now DATE = dbo.fn_GetDate_Local(), 
			@PaymentNet DECIMAL (18,2),
			@PaymentIPT DECIMAL (18,2), 
			@PostCode VARCHAR(20),
			@ClientID INT,
			@ActualCollectionDate DATE,
			@PaymentStatusID INT, 
			@CustomerPaymentScheduleID INT,
			@PurchasedProductPaymentScheduleID INT,
			@ClientAccountID INT,
			@CollectionsMatterID INT,
			@PaymentTypeID INT,
			@PaymentID INT,
			@CardTransactionID INT,
			@AccountRef VARCHAR (20) 
			
	SELECT @CustomerID = a.CustomerID,
		   @PostCode = c.PostCode, 
		   @ClientID = c.ClientID,
		   @ActualCollectionDate = CASE a.AccountTypeID WHEN 1 THEN dbo.fnAddWorkingDays(@Now,3) ELSE @Now END,
		   @PaymentTypeID = CASE a.AccountTypeID WHEN 1 THEN 1 ELSE 7 END,
		   @CollectionsMatterID = a.ObjectID,
		   @AccountRef = a.Reference
	FROM Account a with (NOLOCK)
	INNER JOIN Customers c with (NOLOCK) on c.CustomerID = a.CustomerID 
	WHERE a.AccountID = @AccountID 

	SELECT @ClientAccountID = p.ClientAccountID 
	FROM PurchasedProduct p with (NOLOCK)
	WHERE p.PurchasedProductID = @PurchasedProductID

	SELECT @PaymentStatusID = CASE @AlreadyPaid WHEN 0 THEN 1 ELSE 6 END 

	/*Work out the IPT and the NET from the Gross*/ 
	SELECT @PaymentNet = @PaymentGross/i.GrossMultiplier,
		   @PaymentIPT = @PaymentGross - @PaymentGross/i.GrossMultiplier 	
    FROM IPT i WITH (NOLOCK)
	WHERE CountryID = 232 /*UK*/  
	AND 
		(ISNULL(i.[From],'1999-01-01') <= @Now 
		AND ISNULL(i.[To], '2099-12-31') >= @Now
		AND i.Region IS NULL 
		AND LEFT(@PostCode,3) NOT IN ('GY1','GY2','GY3','GY4','GY5','GY6','GY7','GY8','GY9','GY10','IM1','IM2','IM3','IM4','IM5','IM6','IM7','IM8','IM9','JE2','JE3') ) 
	or 
		( i.Region like '%' + LEFT(@PostCode,3) + '%' )

	/*Before we do anything make sure the Account and the PurchasedProduct belong to the same Customer*/ 
	IF EXISTS (SELECT * FROM PurchasedProduct p with (NOLOCK) 
			   WHERE p.CustomerID = @CustomerID
			   AND p.PurchasedProductID = @PurchasedProductID)
	BEGIN 

		INSERT INTO CustomerPaymentSchedule (ClientID,CustomerID,AccountID,	ActualCollectionDate,PaymentDate,PaymentGross, PaymentNet, PaymentVAT , PaymentStatusID,CustomerLedgerID,ReconciledDate,WhoCreated,WhenCreated,SourceID,RelatedObjectID,RelatedObjectTypeID,ClientAccountID,WhoModified,WhenModified)
		VALUES (@ClientID, @CustomerID, @AccountID,@ActualCollectionDate,@Now,@PaymentGross,@PaymentNet,@PaymentIPT, @PaymentStatusID,NULL,NULL, 58552, @Now, @PPPSToRefund, @CollectionsMatterID, 2, @ClientAccountID, 58552, @Now )
	
		SELECT @CustomerPaymentScheduleID = SCOPE_IDENTITY() 

		INSERT INTO PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentGross, PaymentNet, PaymentVAT, PaymentStatusID, CustomerLedgerID, CustomerPaymentScheduleID, WhoCreated, WhenCreated, ClientAccountID, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, TransactionFee)  
		VALUES (@ClientID, @CustomerID, @AccountID, @PurchasedProductID, @ActualCollectionDate, @CoverFrom, @CoverTo, @Now, @PaymentGross, @PaymentNet, @PaymentIPT, @PaymentStatusID, NULL, @CustomerPaymentScheduleID,58552,@Now,@ClientAccountID,@PPPSToRefund,1, @TransactionFee)

		SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY() 

		/*Ensure we obey the ParentID rules, If this payment is spawned from a previous payment then the parent ID should be set to that payment, otherwise set this to itself*/ 
		IF EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK)
				   WHERE p.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
				   AND p.PurchasedProductPaymentScheduleParentID IS NULL)
		BEGIN 
		
			Update PurchasedProductPaymentSchedule 
			SET PurchasedProductPaymentScheduleParentID = @PurchasedProductPaymentScheduleID
			WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		END

		IF @AlreadyPaid = 1 
		BEGIN 
			
			IF @PaymentTypeID = 1 
			BEGIN 

				INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription,PaymentReference,PaymentGross, PaymentNet, PaymentVAT, PaymentStatusID, customerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
				Values (@ClientID,@CustomerID,@Now, @PaymentTypeID,@Now, @Notes, @AccountRef, @PaymentGross, @PaymentNet, @PaymentIPT, @PaymentStatusID, @CustomerPaymentScheduleID,58552,@Now, 58552,@Now)

				SELECT @PaymentID = SCOPE_IDENTITY()

				INSERT INTO CustomerLedger (ClientID,	CustomerID,	EffectivePaymentDate,TransactionDate,	TransactionReference,	TransactionDescription, TransactionGross,TransactionNet,TransactionVAT, ObjectID,	ObjectTypeID,	PaymentID,	WhoCreated,	WhenCreated)
				VALUES (@ClientID,@CustomerID, @Now, @Now, @AccountRef,@Notes, @PaymentGross, @PaymentNet,@PaymentIPT,@CollectionsMatterID,2,	@PaymentID,	58552,@Now)

			END
			ELSE
			BEGIN 

				INSERT INTO CardTransaction (ClientID, CustomerID, ObjectID, ObjectTypeID, CustomerPaymentScheduleID, ClientPaymentGatewayID, PaymentTypeID, Description, Amount,WhoCreated, WhenCreated, AccountID, ErrorCode,ErrorMessage, StatusMsg)
				VALUES (@ClientID,@CustomerID,@CollectionsMatterID,2,@CustomerPaymentScheduleID,NULL,2,@Notes,@PaymentGross,58552,@Now,@AccountID,'CAPTURED','CAPTURED','CAPTURED' )
				
				SELECT @CardTransactionID = SCOPE_IDENTITY()

				Update CardTransaction 
				SET OrderID = @CardTransactionID
				WHERE CardTransactionID = @CardTransactionID

				INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription,PaymentReference,PaymentGross, PaymentNet, PaymentVAT, PaymentStatusID, customerPaymentScheduleID, WhoCreated,CardTransactionID,WhenCreated,WhoModified,WhenModified)
				Values (@ClientID,@CustomerID,@Now, @PaymentTypeID,@Now, @Notes, @AccountRef, @PaymentGross, @PaymentNet, @PaymentIPT, @PaymentStatusID, @CustomerPaymentScheduleID,58552,@CardTransactionID,@Now, 58552,@Now)

				SELECT @PaymentID = SCOPE_IDENTITY()

				INSERT INTO CustomerLedger (ClientID,	CustomerID,	EffectivePaymentDate,TransactionDate,	TransactionReference,	TransactionDescription, TransactionGross,TransactionNet,TransactionVAT, ObjectID,	ObjectTypeID,	PaymentID,	WhoCreated,	WhenCreated)
				VALUES (@ClientID,@CustomerID, @Now, @Now, @AccountRef,@Notes, @PaymentGross, @PaymentNet,@PaymentIPT,@CollectionsMatterID,2,	@PaymentID,	58552,@Now)

			END
		END 

	END

END 
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_CreateRefundOrCollection] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing_Support_CreateRefundOrCollection] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_CreateRefundOrCollection] TO [sp_executeall]
GO
