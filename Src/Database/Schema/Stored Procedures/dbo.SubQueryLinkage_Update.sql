SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SubQueryLinkage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_Update]
(

	@SubQueryLinkageID int   ,

	@ClientID int   ,

	@SubQueryID int   ,

	@LinksToTableName varchar (50)  ,

	@LinksToColumnName varchar (50)  ,

	@LinkageDataType varchar (50)  ,

	@ParamTableName varchar (50)  ,

	@ParamColumnName varchar (50)  ,

	@ParamColumnDataType varchar (50)  ,

	@ParamColumnHelperSql varchar (2000)  ,

	@LeadTypeFilterSql varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SubQueryLinkage]
				SET
					[ClientID] = @ClientID
					,[SubQueryID] = @SubQueryID
					,[LinksToTableName] = @LinksToTableName
					,[LinksToColumnName] = @LinksToColumnName
					,[LinkageDataType] = @LinkageDataType
					,[ParamTableName] = @ParamTableName
					,[ParamColumnName] = @ParamColumnName
					,[ParamColumnDataType] = @ParamColumnDataType
					,[ParamColumnHelperSql] = @ParamColumnHelperSql
					,[LeadTypeFilterSql] = @LeadTypeFilterSql
				WHERE
[SubQueryLinkageID] = @SubQueryLinkageID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Update] TO [sp_executeall]
GO
