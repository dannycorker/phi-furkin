SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailFieldStyle table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldStyle_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailFieldStyleID int   = null ,

	@ClientID int   = null ,

	@StyleName nvarchar (50)  = null ,

	@BackgroundColor nvarchar (50)  = null ,

	@ForegroundColor nvarchar (50)  = null ,

	@FontSize nvarchar (50)  = null ,

	@FontWeight nvarchar (50)  = null ,

	@Align nvarchar (50)  = null ,

	@Padding nvarchar (50)  = null ,

	@PaddingHorizontal nvarchar (5)  = null ,

	@TextDecoration nvarchar (50)  = null ,

	@FontStyle nvarchar (50)  = null ,

	@PaddingVertical nvarchar (5)  = null ,

	@Enabled bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailFieldStyleID]
	, [ClientID]
	, [StyleName]
	, [BackgroundColor]
	, [ForegroundColor]
	, [FontSize]
	, [FontWeight]
	, [Align]
	, [Padding]
	, [PaddingHorizontal]
	, [TextDecoration]
	, [FontStyle]
	, [PaddingVertical]
	, [Enabled]
    FROM
	[dbo].[DetailFieldStyle] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldStyleID] = @DetailFieldStyleID OR @DetailFieldStyleID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([StyleName] = @StyleName OR @StyleName IS NULL)
	AND ([BackgroundColor] = @BackgroundColor OR @BackgroundColor IS NULL)
	AND ([ForegroundColor] = @ForegroundColor OR @ForegroundColor IS NULL)
	AND ([FontSize] = @FontSize OR @FontSize IS NULL)
	AND ([FontWeight] = @FontWeight OR @FontWeight IS NULL)
	AND ([Align] = @Align OR @Align IS NULL)
	AND ([Padding] = @Padding OR @Padding IS NULL)
	AND ([PaddingHorizontal] = @PaddingHorizontal OR @PaddingHorizontal IS NULL)
	AND ([TextDecoration] = @TextDecoration OR @TextDecoration IS NULL)
	AND ([FontStyle] = @FontStyle OR @FontStyle IS NULL)
	AND ([PaddingVertical] = @PaddingVertical OR @PaddingVertical IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailFieldStyleID]
	, [ClientID]
	, [StyleName]
	, [BackgroundColor]
	, [ForegroundColor]
	, [FontSize]
	, [FontWeight]
	, [Align]
	, [Padding]
	, [PaddingHorizontal]
	, [TextDecoration]
	, [FontStyle]
	, [PaddingVertical]
	, [Enabled]
    FROM
	[dbo].[DetailFieldStyle] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldStyleID] = @DetailFieldStyleID AND @DetailFieldStyleID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([StyleName] = @StyleName AND @StyleName is not null)
	OR ([BackgroundColor] = @BackgroundColor AND @BackgroundColor is not null)
	OR ([ForegroundColor] = @ForegroundColor AND @ForegroundColor is not null)
	OR ([FontSize] = @FontSize AND @FontSize is not null)
	OR ([FontWeight] = @FontWeight AND @FontWeight is not null)
	OR ([Align] = @Align AND @Align is not null)
	OR ([Padding] = @Padding AND @Padding is not null)
	OR ([PaddingHorizontal] = @PaddingHorizontal AND @PaddingHorizontal is not null)
	OR ([TextDecoration] = @TextDecoration AND @TextDecoration is not null)
	OR ([FontStyle] = @FontStyle AND @FontStyle is not null)
	OR ([PaddingVertical] = @PaddingVertical AND @PaddingVertical is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldStyle_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Find] TO [sp_executeall]
GO
