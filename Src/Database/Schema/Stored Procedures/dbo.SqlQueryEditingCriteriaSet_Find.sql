SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditingCriteriaSet table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSet_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingCriteriaSetID int   = null ,

	@ClientID int   = null ,

	@SqlQueryEditingID int   = null ,

	@SetName varchar (250)  = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingCriteriaSetID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SetName]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[SqlQueryEditingCriteriaSet] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID OR @SqlQueryEditingCriteriaSetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([SetName] = @SetName OR @SetName IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingCriteriaSetID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SetName]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[SqlQueryEditingCriteriaSet] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID AND @SqlQueryEditingCriteriaSetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([SetName] = @SetName AND @SetName is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSet_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Find] TO [sp_executeall]
GO
