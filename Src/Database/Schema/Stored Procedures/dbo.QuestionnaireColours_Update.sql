SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuestionnaireColours table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_Update]
(

	@QuestionnaireColourID int   ,

	@ClientQuestionnaireID int   ,

	@Page varchar (50)  ,

	@Header varchar (50)  ,

	@Intro varchar (50)  ,

	@Body varchar (50)  ,

	@Footer varchar (50)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuestionnaireColours]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[Page] = @Page
					,[Header] = @Header
					,[Intro] = @Intro
					,[Body] = @Body
					,[Footer] = @Footer
					,[ClientID] = @ClientID
				WHERE
[QuestionnaireColourID] = @QuestionnaireColourID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Update] TO [sp_executeall]
GO
