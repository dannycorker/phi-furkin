SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudicationFieldType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeID]
(

	@AutoAdjudicationFieldTypeID int   
)
AS


				SELECT
					[AutoAdjudicationFieldTypeID],
					[AutoAdjudicationFieldTypeName]
				FROM
					[dbo].[AutoAdjudicationFieldType] WITH (NOLOCK) 
				WHERE
										[AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_GetByAutoAdjudicationFieldTypeID] TO [sp_executeall]
GO
