SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-06-12
-- Description:	Writes the authorise response to the authorise table
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteAuthoriseResponse]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT,
	@AuthoriseResponseStatus VARCHAR(2000),
	@AuthoriseResponseStatusDetail VARCHAR(2000),
	@AuthoriseResponseVPSTxId VARCHAR(2000),
	@AuthoriseResponseSecurityKey VARCHAR(2000),
	@AuthoriseResponseTxAuthNo VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @TransactionDate DATETIME
	SET @TransactionDate = CURRENT_TIMESTAMP

	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 637 THEN @AuthoriseResponseStatus
																	WHEN 638 THEN @AuthoriseResponseStatusDetail
																	WHEN 639 THEN @AuthoriseResponseVPSTxId
																	WHEN 640 THEN @AuthoriseResponseTxAuthNo
																	WHEN 642 THEN @AuthoriseResponseSecurityKey
																	WHEN 644 THEN CONVERT(varchar, @TransactionDate,120)
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (637,638,639,640,642,644)
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteAuthoriseResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteAuthoriseResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteAuthoriseResponse] TO [sp_executeall]
GO
