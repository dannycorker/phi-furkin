SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderDecodeType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDecodeType_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderDecodeTypeID int   = null ,

	@DecodeTypeName varchar (250)  = null ,

	@DecodeTypeDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderDecodeTypeID]
	, [DecodeTypeName]
	, [DecodeTypeDescription]
    FROM
	[dbo].[DataLoaderDecodeType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderDecodeTypeID] = @DataLoaderDecodeTypeID OR @DataLoaderDecodeTypeID IS NULL)
	AND ([DecodeTypeName] = @DecodeTypeName OR @DecodeTypeName IS NULL)
	AND ([DecodeTypeDescription] = @DecodeTypeDescription OR @DecodeTypeDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderDecodeTypeID]
	, [DecodeTypeName]
	, [DecodeTypeDescription]
    FROM
	[dbo].[DataLoaderDecodeType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderDecodeTypeID] = @DataLoaderDecodeTypeID AND @DataLoaderDecodeTypeID is not null)
	OR ([DecodeTypeName] = @DecodeTypeName AND @DecodeTypeName is not null)
	OR ([DecodeTypeDescription] = @DecodeTypeDescription AND @DecodeTypeDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDecodeType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Find] TO [sp_executeall]
GO
