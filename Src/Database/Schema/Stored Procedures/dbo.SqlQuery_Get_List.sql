SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_Get_List]

AS


				
				SELECT
					[QueryID],
					[ClientID],
					[QueryText],
					[QueryTitle],
					[AutorunOnline],
					[OnlineLimit],
					[BatchLimit],
					[SqlQueryTypeID],
					[FolderID],
					[IsEditable],
					[IsTemplate],
					[IsDeleted],
					[WhenCreated],
					[CreatedBy],
					[OwnedBy],
					[RunCount],
					[LastRundate],
					[LastRuntime],
					[LastRowcount],
					[MaxRuntime],
					[MaxRowcount],
					[AvgRuntime],
					[AvgRowcount],
					[Comments],
					[WhenModified],
					[ModifiedBy],
					[LeadTypeID],
					[ParentQueryID],
					[IsParent],
					[SqlQueryTemplateID],
					[OutputFormat],
					[ShowInCustomSearch],
					[OutputFileExtension],
					[OutputSeparatorCharmapID],
					[OutputEncapsulatorCharmapID],
					[SuppressHeaderRow],
					[LockAllTables],
					[SourceID]
				FROM
					[dbo].[SqlQuery] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Get_List] TO [sp_executeall]
GO
