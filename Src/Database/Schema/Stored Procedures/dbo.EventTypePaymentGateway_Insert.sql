SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypePaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_Insert]
(

	@EventTypePaymentGatewayID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@ClientPaymentGatewayID int   ,

	@TransactionTypeID int   ,

	@CardMaskingMethodID int   
)
AS


				
				INSERT INTO [dbo].[EventTypePaymentGateway]
					(
					[ClientID]
					,[EventTypeID]
					,[ClientPaymentGatewayID]
					,[TransactionTypeID]
					,[CardMaskingMethodID]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@ClientPaymentGatewayID
					,@TransactionTypeID
					,@CardMaskingMethodID
					)
				-- Get the identity value
				SET @EventTypePaymentGatewayID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Insert] TO [sp_executeall]
GO
