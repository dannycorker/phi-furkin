SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-29
-- Description:	Looks up letter text and adds it to the specified field
--              Hack of _C00_SimpleValueIntoField
-- Mods:
-- 2016-02-09 DCM Add alias replacement
-- =============================================
CREATE PROCEDURE [dbo].[_C600_UpdateLetterTextField] 
(
	@DetailFieldID INT,
	@LookUpText VARCHAR(2000),
	@ObjectID INT,
	@ClientPersonnelID INT = NULL
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @DetailValue VARCHAR(2000),
			@ObjectType VARCHAR(30),
			@ObjectTypeID INT
			
	
	SELECT @DetailValue=pt.DetailValue, 
		@ClientPersonnelID=CASE WHEN @ClientPersonnelID IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (pc.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @ClientPersonnelID END
	FROM ResourceListDetailValues pc WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues pt WITH (NOLOCK) ON pc.ResourceListID=pt.ResourceListID AND pt.DetailFieldID=155937
	WHERE pc.DetailFieldID=155936 AND pc.DetailValue=@LookUpText
	
	-- do generic alias substitution
	SELECT @ObjectTypeID=df.LeadOrMatter FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID=@DetailFieldID
	IF @ObjectTypeID IN (1,2,11) -- only lead, case or matter
	BEGIN
		SELECT @ObjectType = CASE @ObjectTypeID 
								WHEN 1 THEN 'Lead'
								WHEN 2 THEN 'Matter'
								WHEN 11 THEN 'Case'
								END
		SELECT @DetailValue=dbo.fn_C600_ReplaceAliasesInBraces ( @DetailValue, @ObjectID, @ObjectType )
	END

	EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @DetailValue, @ObjectID, @ClientPersonnelID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_UpdateLetterTextField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_UpdateLetterTextField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_UpdateLetterTextField] TO [sp_executeall]
GO
