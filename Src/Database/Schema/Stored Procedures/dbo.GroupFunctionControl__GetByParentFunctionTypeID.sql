SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*
----------------------------------------------------------------------------------------------------

-- Created By: Jim Green
-- Purpose: Select records from the GroupFunctionControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControl__GetByParentFunctionTypeID]
(
	@ClientPersonnelAdminGroupID int,
	@ParentFunctionTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					*
				FROM
					[dbo].[GroupFunctionControl]
				WHERE
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
				AND [FunctionTypeID] IN (SELECT FunctionTypeID FROM FunctionType WHERE ParentFunctionTypeID = @ParentFunctionTypeID)
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			









GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl__GetByParentFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControl__GetByParentFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl__GetByParentFunctionTypeID] TO [sp_executeall]
GO
