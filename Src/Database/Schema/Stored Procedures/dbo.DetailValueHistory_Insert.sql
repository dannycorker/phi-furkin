SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailValueHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_Insert]
(

	@DetailValueHistoryID int    OUTPUT,

	@ClientID int   ,

	@DetailFieldID int   ,

	@LeadOrMatter tinyint   ,

	@LeadID int   ,

	@MatterID int   ,

	@FieldValue varchar (2000)  ,

	@WhenSaved datetime   ,

	@ClientPersonnelID int   ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@CustomerID int   ,

	@CaseID int   ,

	@ContactID int   ,

	@ClientPersonnelDetailValueID int   
)
AS


				
				INSERT INTO [dbo].[DetailValueHistory]
					(
					[ClientID]
					,[DetailFieldID]
					,[LeadOrMatter]
					,[LeadID]
					,[MatterID]
					,[FieldValue]
					,[WhenSaved]
					,[ClientPersonnelID]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[CustomerID]
					,[CaseID]
					,[ContactID]
					,[ClientPersonnelDetailValueID]
					)
				VALUES
					(
					@ClientID
					,@DetailFieldID
					,@LeadOrMatter
					,@LeadID
					,@MatterID
					,@FieldValue
					,@WhenSaved
					,@ClientPersonnelID
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@CustomerID
					,@CaseID
					,@ContactID
					,@ClientPersonnelDetailValueID
					)
				-- Get the identity value
				SET @DetailValueHistoryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Insert] TO [sp_executeall]
GO
