SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-03-24
-- Description:	Check for missing user rights and reapply them
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReplaceMissingUserRights] 
AS
BEGIN
	SET NOCOUNT ON;

	-- Declare a table variable to hold the list of 
	-- user rights that we know ought to be there.
	DECLARE @SpecialRights TABLE (cpid int, ftid int, ltid int, oid int, rid int)

	-- Populate the table var with the list of required values
	INSERT @SpecialRights (cpid, ftid, ltid, oid, rid) 
	SELECT 55, 12, 46, 537, 4 -- Leah Johnson, Bank Charges, Page "Credit/Debit Card Payment"
	UNION
	SELECT 55, 12, 122, 589, 4 -- Leah Johnson, Credit Cards, Page "Credit/Debit Card Payment"
	UNION
	SELECT 55, 10, NULL, 22, 4 -- Leah Johnson, Assign Leads
	UNION
	SELECT 55, 25, 46, 4318, 2 -- Leah Johnson, Bank Charges, Encrypted Field "Card Number"
	UNION
	SELECT 55, 25, 46, 4323, 2 -- Leah Johnson, Bank Charges, Encrypted Field "Cardholders Name"
	UNION
	SELECT 92, 10, NULL, 65, 4 -- Chris Flanagan, Jump to any event
	UNION
	SELECT 92, 10, NULL, 84, 4 -- Chris Flanagan, Modify Event History
	UNION
	SELECT 103, 10, NULL, 85, 4 -- Nicola Ankers, Document Queue Menu
	UNION
	SELECT 103, 15, NULL, 25, 0 -- Nicola Ankers, iReporting Options
	UNION
	SELECT 134, 10, NULL, 84, 4 -- Shaun Roberts, Modify Event History
	UNION
	SELECT 135, 10, NULL, 65, 4 -- Jane Hall, Jump to any event
	UNION
	SELECT 136, 10, NULL, 84, 4 -- Rebecca Field, Modify Event History
	UNION
	SELECT 373, 10, NULL, 65, 4 -- Sarah Wilson, Jump to any event
	UNION
	SELECT 460, 10, NULL, 85, 4 -- Nicola Boyd, Document Queue Menu 
	UNION
	SELECT 586, 10, NULL, 65, 4 -- James Bowyer, Jump to any event
	UNION
	SELECT 825, 10, NULL, 84, 4 -- Maarten Van Veen, Modify Event History
	UNION
	SELECT 1079, 10, NULL, 65, 4 -- Adam Brady, Jump to any event
	
	-- Select a list of missing values so that the scheduler
	-- can send it on to someone who cares (eg me) so we can
	-- try to track down what is deleting these rights.
	SELECT * FROM @SpecialRights s
	LEFT JOIN dbo.UserRightsDynamic u ON u.ClientPersonnelID = s.cpid 
		AND u.functiontypeid = s.ftid
		AND IsNull(u.leadtypeid, -1) = IsNull(s.ltid, -1) 
		AND u.objectid = s.oid
	WHERE u.UserRightsDynamicID IS NULL

	-- Restore all the missing values!
	IF @@ROWCOUNT > 0
	BEGIN	
		INSERT UserRightsDynamic (clientpersonnelid, functiontypeid, leadtypeid, objectid, rightid)
		SELECT cpid, ftid, ltid, oid, rid 
		FROM @SpecialRights s
		LEFT JOIN dbo.UserRightsDynamic u ON u.ClientPersonnelID = s.cpid 
			AND u.functiontypeid = s.ftid
			AND IsNull(u.leadtypeid, -1) = IsNull(s.ltid, -1) 
			AND u.objectid = s.oid
		WHERE u.UserRightsDynamicID IS NULL
	END
	
	/*
		select cp.clientpersonnelid, cp.username 
		from dbo.clientpersonnel cp 
		where cp.clientid = 3 
		order by 2, 1 
		
		aq 1, 8, nnn 
		
		select * from dbo.functions

	*/
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReplaceMissingUserRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReplaceMissingUserRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReplaceMissingUserRights] TO [sp_executeall]
GO
