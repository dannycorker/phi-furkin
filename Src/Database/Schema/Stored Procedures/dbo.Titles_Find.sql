SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Titles table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Titles_Find]
(

	@SearchUsingOR bit   = null ,

	@TitleID int   = null ,

	@Title varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TitleID]
	, [Title]
    FROM
	[dbo].[Titles] WITH (NOLOCK) 
    WHERE 
	 ([TitleID] = @TitleID OR @TitleID IS NULL)
	AND ([Title] = @Title OR @Title IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TitleID]
	, [Title]
    FROM
	[dbo].[Titles] WITH (NOLOCK) 
    WHERE 
	 ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([Title] = @Title AND @Title is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Titles_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Find] TO [sp_executeall]
GO
