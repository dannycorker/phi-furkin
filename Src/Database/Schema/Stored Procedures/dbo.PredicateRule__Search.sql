SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Predicate Rule Search
-- =============================================
CREATE PROCEDURE [dbo].[PredicateRule__Search] 
	@LeadTypeID INT, 
	@PageSize INT, 
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT	TOP(@PageSize) pr.*, df.FieldName, et.EventTypeName
	FROM	PredicateRule pr WITH (NOLOCK)
	LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = pr.FieldID AND df.ClientID = @ClientID
	LEFT JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = pr.EventTypeID AND et.LeadTypeID = @LeadTypeID AND et.ClientID = @ClientID
	WHERE	pr.ClientID = @ClientID
	AND		pr.LeadTypeID = @LeadTypeID
	AND		pr.RuleTypeID = 1
	UNION ALL
	SELECT	TOP(@PageSize) pr.*, df.FieldName, et.EventTypeName
	FROM	PredicateRule pr WITH (NOLOCK)
	LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = pr.FieldID AND df.LeadTypeID = @LeadTypeID AND df.ClientID = @ClientID
	LEFT JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = pr.EventTypeID AND et.LeadTypeID = @LeadTypeID AND et.ClientID = @ClientID
	WHERE	pr.ClientID = @ClientID
	AND		pr.LeadTypeID = @LeadTypeID
	AND		pr.RuleTypeID = 2
	UNION ALL
	SELECT	TOP(@PageSize) pr.*, '' FieldName, '' EventTypeName
	FROM	PredicateRule pr WITH (NOLOCK)
	WHERE	pr.ClientID = @ClientID
	AND		pr.LeadTypeID = @LeadTypeID
	AND		pr.RuleTypeID = 3
	UNION ALL
	SELECT	-1, '[Always True]', 3, 0, 0, 0, 0, '','', 0, 0, 0, '(SELECT 1)', @LeadTypeID, @ClientID, '', ''
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRule__Search] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PredicateRule__Search] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRule__Search] TO [sp_executeall]
GO
