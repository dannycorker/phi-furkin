SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-08-2015
-- Description:	Gets a list of chat by Matter ID
-- =============================================
CREATE PROCEDURE [dbo].[Chat__GetByObjectID]
	@ClientID INT,
	@ObjectID INT,
	@ObjectTypeID TINYINT
AS
BEGIN
	
	
	SET NOCOUNT ON;

	SELECT * FROM Chat WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ObjectID=@ObjectID AND ObjectTypeID=@ObjectTypeID
    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__GetByObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chat__GetByObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__GetByObjectID] TO [sp_executeall]
GO
