SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aquarium Software
-- Create date: 2020-01-29
-- Description:	Sql After Event - Saved Quote
-- EventTypeID: 157681
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_SavedQuote_157681]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			/* Specific to procedure */
			,@SavedQuoteID					INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


		IF @EventTypeID IN (157681)
		BEGIN

		/*If the 'Saved Quote Email' event occurs on the saved quote leadtype 1505, delete everything from the 'Saved Quote Multipet Pricing Info' table and set the 'ready to send' field back to '0' Defect #998 and #996 - UAH 11/09/2018 */

		DELETE FROM TableRows  WHERE DetailFieldID = 180164
		AND LeadID = @LeadID

		--set 'ready to send' back to 0
		EXEC dbo._C00_SimpleValueIntoField 180182, 0, @LeadID

		--get the savedquote ID from the Lead ID
		SELECT @SavedQuoteID = SavedQuoteID  FROM _C600_SavedQuote sq WITH (NOLOCK) 
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) on ldv.ValueInt = sq.SavedQuoteID and ldv.DetailFieldID = 178411 /*SavedQuoteID*/
		WHERE ldv.leadid = @LeadID

		--set the 'emailqueued' flag back to '0' after the email event has applied and the email has sent
		UPDATE _C600_SavedQuote
		SET EmailQueued = 0 
		Where SavedQuoteID = @SavedQuoteID

		END

		PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_SavedQuote_157681] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_SavedQuote_157681] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_SavedQuote_157681] TO [sp_executeall]
GO
