SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChartSetting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSetting_Delete]
(

	@ChartSettingID int   
)
AS


				DELETE FROM [dbo].[ChartSetting] WITH (ROWLOCK) 
				WHERE
					[ChartSettingID] = @ChartSettingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSetting_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Delete] TO [sp_executeall]
GO
