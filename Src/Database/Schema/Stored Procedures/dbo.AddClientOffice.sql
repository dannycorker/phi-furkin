SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.AddClientOffice    Script Date: 08/09/2006 12:22:46 ******/

CREATE PROCEDURE [dbo].[AddClientOffice] 
@ClientID int,
@BuildingName nvarchar(50), 
@Address1 nvarchar(200),
@Address2 nvarchar(200), 
@Town nvarchar(200),
@County nvarchar(50),
@Country nvarchar(200),
@PostCode nvarchar(10),
@OfficeTelephone nvarchar(50),
@OfficeExt nvarchar(10)

as

insert into ClientOffices (ClientID, BuildingName, Address1, Address2, Town, County, Country, PostCode, OfficeTelephone, OfficeTelephoneExtension)
values (@ClientID, @BuildingName, @address1,@Address2, @Town, @County, @Country, @PostCode, @OfficeTelephone, @OfficeExt)





GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientOffice] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientOffice] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientOffice] TO [sp_executeall]
GO
