SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptNoteTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_Delete]
(

	@ScriptNoteTargetID int   
)
AS


				DELETE FROM [dbo].[ScriptNoteTarget] WITH (ROWLOCK) 
				WHERE
					[ScriptNoteTargetID] = @ScriptNoteTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Delete] TO [sp_executeall]
GO
