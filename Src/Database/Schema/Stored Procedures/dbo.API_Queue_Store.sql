SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_Queue_Store
-- =============================================
CREATE PROCEDURE [dbo].[API_Queue_Store]
	@AppKey VARCHAR(50),
	@ObjectKey VARCHAR(50),
	@IdentID UNIQUEIDENTIFIER,
	@XmlData XML,
	@ByteSize INT
AS
BEGIN
	DECLARE @QIDs TABLE (QID UNIQUEIDENTIFIER)
	
	INSERT INTO API_Queue (AppKey, ObjectKey, IdentID, QueueXml, QueueByteSize)
	OUTPUT inserted.QueueID INTO @QIDs
	VALUES (@AppKey, @ObjectKey, @IdentID, @XmlData, @ByteSize)
	
	SELECT	QID
	FROM	@QIDs
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Queue_Store] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Queue_Store] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Queue_Store] TO [sp_executeall]
GO
