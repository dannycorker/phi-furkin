SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the OutcomeCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_Update]
(

	@OutcomeCriteriaID int   ,

	@OutcomeID int   ,

	@QuestionPossibleAnswerID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[OutcomeCriterias]
				SET
					[OutcomeID] = @OutcomeID
					,[QuestionPossibleAnswerID] = @QuestionPossibleAnswerID
					,[ClientID] = @ClientID
				WHERE
[OutcomeCriteriaID] = @OutcomeCriteriaID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Update] TO [sp_executeall]
GO
