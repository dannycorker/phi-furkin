SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-10-30
-- Description:	List all SqlQuery records for a client
-- =============================================
CREATE PROCEDURE [dbo].[sq] 
	@ClientID INT,
	@LeadTypeID INT = NULL,
	@SortOrder TINYINT = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 		
	[QueryID],
	[ClientID],
	[LeadTypeID],
	[QueryTitle],
	[OnlineLimit],
	[BatchLimit],
	[IsEditable],
	[CreatedBy],
	[ModifiedBy],
	[WhenModified],
	[RunCount],
	[OutputFormat],
	[ShowInCustomSearch],
	[FolderID]
	FROM dbo.SqlQuery sq (NOLOCK) 
	WHERE sq.ClientID = @ClientID 
	AND (@LeadTypeID IS NULL OR @LeadTypeID = sq.LeadTypeID) 
	ORDER BY CASE WHEN @SortOrder = 1 THEN sq.QueryTitle ELSE CAST(sq.QueryID AS VARCHAR) END 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[sq] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[sq] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[sq] TO [sp_executeall]
GO
