SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOption_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientOptionID int   = null ,

	@ClientID int   = null ,

	@ZenDeskOff bit   = null ,

	@ZenDeskURLPrefix varchar (50)  = null ,

	@ZenDeskText varchar (200)  = null ,

	@ZenDeskTitle varchar (50)  = null ,

	@IDiaryOff bit   = null ,

	@WorkflowOff bit   = null ,

	@UserDirectoryOff bit   = null ,

	@ECatcherOff bit   = null ,

	@LeadAssignmentOff bit   = null ,

	@UserMessagesOff bit   = null ,

	@SystemMessagesOff bit   = null ,

	@UserPortalOff bit   = null ,

	@ReportSearchOff bit   = null ,

	@NormalSearchOff bit   = null ,

	@UseXero bit   = null ,

	@UseUltra bit   = null ,

	@UseCaseSummary bit   = null ,

	@UseMoreProminentReminders bit   = null ,

	@UseSMSSurvey bit   = null ,

	@UseNewAddLead bit   = null ,

	@DiallerInsertMethod varchar (50)  = null ,

	@DiallerPrimaryKey varchar (50)  = null ,

	@UseCHARMS bit   = null ,

	@TextMessageService int   = null ,

	@UseThunderhead bit   = null ,

	@UseMemorableWordVerification bit   = null ,

	@ApplyInactivityTimeout bit   = null ,

	@InactivityTimeoutInSeconds int   = null ,

	@InactivityRedirectUrl varchar (50)  = null ,

	@UseSentimentAnalysis bit   = null ,

	@ShowProcessInfoButton bit   = null ,

	@LatestRecordsFirst bit   = null ,

	@UseSecureMessage bit   = null ,

	@EventCommentTooltipOff bit   = null ,

	@EnableScripting bit   = null ,

	@EnableBilling bit   = null ,

	@UseDocumentTypeVersioning bit   = null ,

	@EnableSMSSTOP bit   = null ,

	@UseEngageMail bit   = null ,

	@PreventForcedLogout bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientOptionID]
	, [ClientID]
	, [ZenDeskOff]
	, [ZenDeskURLPrefix]
	, [ZenDeskText]
	, [ZenDeskTitle]
	, [iDiaryOff]
	, [WorkflowOff]
	, [UserDirectoryOff]
	, [eCatcherOff]
	, [LeadAssignmentOff]
	, [UserMessagesOff]
	, [SystemMessagesOff]
	, [UserPortalOff]
	, [ReportSearchOff]
	, [NormalSearchOff]
	, [UseXero]
	, [UseUltra]
	, [UseCaseSummary]
	, [UseMoreProminentReminders]
	, [UseSMSSurvey]
	, [UseNewAddLead]
	, [DiallerInsertMethod]
	, [DiallerPrimaryKey]
	, [UseCHARMS]
	, [TextMessageService]
	, [UseThunderhead]
	, [UseMemorableWordVerification]
	, [ApplyInactivityTimeout]
	, [InactivityTimeoutInSeconds]
	, [InactivityRedirectUrl]
	, [UseSentimentAnalysis]
	, [ShowProcessInfoButton]
	, [LatestRecordsFirst]
	, [UseSecureMessage]
	, [EventCommentTooltipOff]
	, [EnableScripting]
	, [EnableBilling]
	, [UseDocumentTypeVersioning]
	, [EnableSMSSTOP]
	, [UseEngageMail]
	, [PreventForcedLogout]
    FROM
	[dbo].[ClientOption] WITH (NOLOCK) 
    WHERE 
	 ([ClientOptionID] = @ClientOptionID OR @ClientOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ZenDeskOff] = @ZenDeskOff OR @ZenDeskOff IS NULL)
	AND ([ZenDeskURLPrefix] = @ZenDeskURLPrefix OR @ZenDeskURLPrefix IS NULL)
	AND ([ZenDeskText] = @ZenDeskText OR @ZenDeskText IS NULL)
	AND ([ZenDeskTitle] = @ZenDeskTitle OR @ZenDeskTitle IS NULL)
	AND ([iDiaryOff] = @IDiaryOff OR @IDiaryOff IS NULL)
	AND ([WorkflowOff] = @WorkflowOff OR @WorkflowOff IS NULL)
	AND ([UserDirectoryOff] = @UserDirectoryOff OR @UserDirectoryOff IS NULL)
	AND ([eCatcherOff] = @ECatcherOff OR @ECatcherOff IS NULL)
	AND ([LeadAssignmentOff] = @LeadAssignmentOff OR @LeadAssignmentOff IS NULL)
	AND ([UserMessagesOff] = @UserMessagesOff OR @UserMessagesOff IS NULL)
	AND ([SystemMessagesOff] = @SystemMessagesOff OR @SystemMessagesOff IS NULL)
	AND ([UserPortalOff] = @UserPortalOff OR @UserPortalOff IS NULL)
	AND ([ReportSearchOff] = @ReportSearchOff OR @ReportSearchOff IS NULL)
	AND ([NormalSearchOff] = @NormalSearchOff OR @NormalSearchOff IS NULL)
	AND ([UseXero] = @UseXero OR @UseXero IS NULL)
	AND ([UseUltra] = @UseUltra OR @UseUltra IS NULL)
	AND ([UseCaseSummary] = @UseCaseSummary OR @UseCaseSummary IS NULL)
	AND ([UseMoreProminentReminders] = @UseMoreProminentReminders OR @UseMoreProminentReminders IS NULL)
	AND ([UseSMSSurvey] = @UseSMSSurvey OR @UseSMSSurvey IS NULL)
	AND ([UseNewAddLead] = @UseNewAddLead OR @UseNewAddLead IS NULL)
	AND ([DiallerInsertMethod] = @DiallerInsertMethod OR @DiallerInsertMethod IS NULL)
	AND ([DiallerPrimaryKey] = @DiallerPrimaryKey OR @DiallerPrimaryKey IS NULL)
	AND ([UseCHARMS] = @UseCHARMS OR @UseCHARMS IS NULL)
	AND ([TextMessageService] = @TextMessageService OR @TextMessageService IS NULL)
	AND ([UseThunderhead] = @UseThunderhead OR @UseThunderhead IS NULL)
	AND ([UseMemorableWordVerification] = @UseMemorableWordVerification OR @UseMemorableWordVerification IS NULL)
	AND ([ApplyInactivityTimeout] = @ApplyInactivityTimeout OR @ApplyInactivityTimeout IS NULL)
	AND ([InactivityTimeoutInSeconds] = @InactivityTimeoutInSeconds OR @InactivityTimeoutInSeconds IS NULL)
	AND ([InactivityRedirectUrl] = @InactivityRedirectUrl OR @InactivityRedirectUrl IS NULL)
	AND ([UseSentimentAnalysis] = @UseSentimentAnalysis OR @UseSentimentAnalysis IS NULL)
	AND ([ShowProcessInfoButton] = @ShowProcessInfoButton OR @ShowProcessInfoButton IS NULL)
	AND ([LatestRecordsFirst] = @LatestRecordsFirst OR @LatestRecordsFirst IS NULL)
	AND ([UseSecureMessage] = @UseSecureMessage OR @UseSecureMessage IS NULL)
	AND ([EventCommentTooltipOff] = @EventCommentTooltipOff OR @EventCommentTooltipOff IS NULL)
	AND ([EnableScripting] = @EnableScripting OR @EnableScripting IS NULL)
	AND ([EnableBilling] = @EnableBilling OR @EnableBilling IS NULL)
	AND ([UseDocumentTypeVersioning] = @UseDocumentTypeVersioning OR @UseDocumentTypeVersioning IS NULL)
	AND ([EnableSMSSTOP] = @EnableSMSSTOP OR @EnableSMSSTOP IS NULL)
	AND ([UseEngageMail] = @UseEngageMail OR @UseEngageMail IS NULL)
	AND ([PreventForcedLogout] = @PreventForcedLogout OR @PreventForcedLogout IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientOptionID]
	, [ClientID]
	, [ZenDeskOff]
	, [ZenDeskURLPrefix]
	, [ZenDeskText]
	, [ZenDeskTitle]
	, [iDiaryOff]
	, [WorkflowOff]
	, [UserDirectoryOff]
	, [eCatcherOff]
	, [LeadAssignmentOff]
	, [UserMessagesOff]
	, [SystemMessagesOff]
	, [UserPortalOff]
	, [ReportSearchOff]
	, [NormalSearchOff]
	, [UseXero]
	, [UseUltra]
	, [UseCaseSummary]
	, [UseMoreProminentReminders]
	, [UseSMSSurvey]
	, [UseNewAddLead]
	, [DiallerInsertMethod]
	, [DiallerPrimaryKey]
	, [UseCHARMS]
	, [TextMessageService]
	, [UseThunderhead]
	, [UseMemorableWordVerification]
	, [ApplyInactivityTimeout]
	, [InactivityTimeoutInSeconds]
	, [InactivityRedirectUrl]
	, [UseSentimentAnalysis]
	, [ShowProcessInfoButton]
	, [LatestRecordsFirst]
	, [UseSecureMessage]
	, [EventCommentTooltipOff]
	, [EnableScripting]
	, [EnableBilling]
	, [UseDocumentTypeVersioning]
	, [EnableSMSSTOP]
	, [UseEngageMail]
	, [PreventForcedLogout]
    FROM
	[dbo].[ClientOption] WITH (NOLOCK) 
    WHERE 
	 ([ClientOptionID] = @ClientOptionID AND @ClientOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ZenDeskOff] = @ZenDeskOff AND @ZenDeskOff is not null)
	OR ([ZenDeskURLPrefix] = @ZenDeskURLPrefix AND @ZenDeskURLPrefix is not null)
	OR ([ZenDeskText] = @ZenDeskText AND @ZenDeskText is not null)
	OR ([ZenDeskTitle] = @ZenDeskTitle AND @ZenDeskTitle is not null)
	OR ([iDiaryOff] = @IDiaryOff AND @IDiaryOff is not null)
	OR ([WorkflowOff] = @WorkflowOff AND @WorkflowOff is not null)
	OR ([UserDirectoryOff] = @UserDirectoryOff AND @UserDirectoryOff is not null)
	OR ([eCatcherOff] = @ECatcherOff AND @ECatcherOff is not null)
	OR ([LeadAssignmentOff] = @LeadAssignmentOff AND @LeadAssignmentOff is not null)
	OR ([UserMessagesOff] = @UserMessagesOff AND @UserMessagesOff is not null)
	OR ([SystemMessagesOff] = @SystemMessagesOff AND @SystemMessagesOff is not null)
	OR ([UserPortalOff] = @UserPortalOff AND @UserPortalOff is not null)
	OR ([ReportSearchOff] = @ReportSearchOff AND @ReportSearchOff is not null)
	OR ([NormalSearchOff] = @NormalSearchOff AND @NormalSearchOff is not null)
	OR ([UseXero] = @UseXero AND @UseXero is not null)
	OR ([UseUltra] = @UseUltra AND @UseUltra is not null)
	OR ([UseCaseSummary] = @UseCaseSummary AND @UseCaseSummary is not null)
	OR ([UseMoreProminentReminders] = @UseMoreProminentReminders AND @UseMoreProminentReminders is not null)
	OR ([UseSMSSurvey] = @UseSMSSurvey AND @UseSMSSurvey is not null)
	OR ([UseNewAddLead] = @UseNewAddLead AND @UseNewAddLead is not null)
	OR ([DiallerInsertMethod] = @DiallerInsertMethod AND @DiallerInsertMethod is not null)
	OR ([DiallerPrimaryKey] = @DiallerPrimaryKey AND @DiallerPrimaryKey is not null)
	OR ([UseCHARMS] = @UseCHARMS AND @UseCHARMS is not null)
	OR ([TextMessageService] = @TextMessageService AND @TextMessageService is not null)
	OR ([UseThunderhead] = @UseThunderhead AND @UseThunderhead is not null)
	OR ([UseMemorableWordVerification] = @UseMemorableWordVerification AND @UseMemorableWordVerification is not null)
	OR ([ApplyInactivityTimeout] = @ApplyInactivityTimeout AND @ApplyInactivityTimeout is not null)
	OR ([InactivityTimeoutInSeconds] = @InactivityTimeoutInSeconds AND @InactivityTimeoutInSeconds is not null)
	OR ([InactivityRedirectUrl] = @InactivityRedirectUrl AND @InactivityRedirectUrl is not null)
	OR ([UseSentimentAnalysis] = @UseSentimentAnalysis AND @UseSentimentAnalysis is not null)
	OR ([ShowProcessInfoButton] = @ShowProcessInfoButton AND @ShowProcessInfoButton is not null)
	OR ([LatestRecordsFirst] = @LatestRecordsFirst AND @LatestRecordsFirst is not null)
	OR ([UseSecureMessage] = @UseSecureMessage AND @UseSecureMessage is not null)
	OR ([EventCommentTooltipOff] = @EventCommentTooltipOff AND @EventCommentTooltipOff is not null)
	OR ([EnableScripting] = @EnableScripting AND @EnableScripting is not null)
	OR ([EnableBilling] = @EnableBilling AND @EnableBilling is not null)
	OR ([UseDocumentTypeVersioning] = @UseDocumentTypeVersioning AND @UseDocumentTypeVersioning is not null)
	OR ([EnableSMSSTOP] = @EnableSMSSTOP AND @EnableSMSSTOP is not null)
	OR ([UseEngageMail] = @UseEngageMail AND @UseEngageMail is not null)
	OR ([PreventForcedLogout] = @PreventForcedLogout AND @PreventForcedLogout is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_Find] TO [sp_executeall]
GO
