SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ResourceListDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_Insert]
(

	@ResourceListDetailValueID int    OUTPUT,

	@ResourceListID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@LeadOrMatter int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   
)
AS


				
				INSERT INTO [dbo].[ResourceListDetailValues]
					(
					[ResourceListID]
					,[ClientID]
					,[DetailFieldID]
					,[LeadOrMatter]
					,[DetailValue]
					,[ErrorMsg]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					)
				VALUES
					(
					@ResourceListID
					,@ClientID
					,@DetailFieldID
					,@LeadOrMatter
					,@DetailValue
					,@ErrorMsg
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					)
				-- Get the identity value
				SET @ResourceListDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Insert] TO [sp_executeall]
GO
