SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-08-01
-- Description:	Attempts to find what categorys the response falls into
-- =============================================
CREATE PROCEDURE [dbo].[CategoriseResponse]

	@ClientID INT,
	@Sentence VARCHAR(MAX)

AS
BEGIN

	SET NOCOUNT ON;

		DECLARE @Words TABLE 
		(
			WordOrder INT IDENTITY(1, 1), 
			WordText VARCHAR(50),
			IsSentenceBreak BIT  /* True for full stop etc, false for space */
		)
		
		DECLARE @CursorPos INT = 1, 
			@NextBreakPos INT = 0, 
			@IsSentenceBreak BIT = 0,
			@SpecialPhraseStart INT = 0, 
			@SpecialPhrase VARCHAR(100),
			@SpecialPhraseMax INT = 0, 
			@SentenceLength INT = 0,
			@CurrentWordLength INT = 0,
			@CurrentWord VARCHAR(50), 
			@Test INT = 0, 
			@ProfanityScore INT = 0,
			@WordNumber INT = 0, 
			@WordCount INT = 0, 
			@WordOrder INT = 0,
			@CurrentIsBad BIT, 
			@CurrentIsGood BIT, 
			@CurrentIsReverse BIT, 
			@CurrentScoreMultiplier DECIMAL(5, 2), 
			@CurrentScore DECIMAL(3, 1), 
			@Phrase VARCHAR(MAX) = '', 
			@PhraseReverse BIT = 0, 
			@PhraseScore DECIMAL (18, 2) = 0, 
			@PhraseScoreMultiplier DECIMAL (18, 2) = 0, 
			@OverallScore DECIMAL (18, 2) = 0, 
			@OverallPositiveCount INT = 0,
			@OverallPositiveScore DECIMAL (18, 2) = 0,
			@OverallNegativeCount INT = 0,
			@OverallNegativeScore DECIMAL (18, 2) = 0		
		
		/* Expand n't into NOT first of all */ 
		SELECT @Sentence = REPLACE(@Sentence, 'n''t', ' NOT') 
			/* Note the sentence length */ 
		SELECT @SentenceLength = LEN(@Sentence)
		
		SELECT @SpecialPhraseMax = MAX(w.CategoryKeywordID) 
		FROM dbo.CategoryKeyword w 
		WHERE w.Keyword LIKE '% %' AND ClientID=@ClientID

		
		WHILE @SpecialPhraseStart < @SpecialPhraseMax
		BEGIN
			/* Get next special phrase in order */
			SELECT TOP (1) @SpecialPhraseStart = w.CategoryKeywordID, 
			@SpecialPhrase = w.Keyword 
			FROM dbo.CategoryKeyword w 
			WHERE w.Keyword LIKE '% %'  
			AND w.CategoryKeywordID > @SpecialPhraseStart AND w.ClientID=@ClientID
			ORDER BY w.CategoryKeywordID
			
			/* Replace spaces with underscores within the special phrase */
			/* "in no way" becomes "in_no_way" to keep it all together */
			SELECT @Sentence = REPLACE(@Sentence, @SpecialPhrase, REPLACE(@SpecialPhrase, ' ', '_'))
			
		END
		
		WHILE @CursorPos > 0 AND @Test < 100
		BEGIN
			
			SELECT TOP (1) 
			@NextBreakPos = f.pos, 
			@IsSentenceBreak = f.isBreak 
			FROM dbo.fnNextWordChangeIsBreak (@Sentence, @CursorPos) f 
			
			--SELECT @nextbreakpos, @IsSentenceBreak
			
			IF @@ROWCOUNT = 0
			BEGIN
				SELECT @NextBreakPos = 0, @IsSentenceBreak = 0
			END			
			
			IF @NextBreakPos > 0
			BEGIN
				/* Extract this word or phrase */
				SELECT @CurrentWordLength = @NextBreakPos - @CursorPos
				
				/* Ignore garbage strings 50 chars or more */
				IF @CurrentWordLength < 51
				BEGIN
					SELECT @CurrentWord = SUBSTRING(@Sentence, @CursorPos, @CurrentWordLength)
				END
				ELSE
				BEGIN
					SELECT @CurrentWord = '(too long)'
				END
				
				/* Loop round again */
				SELECT @CursorPos = @NextBreakPos + 1

			END
			ELSE
			BEGIN
				/* Take everything up to the end of the sentence */
				SELECT @CurrentWordLength = (@SentenceLength - @CursorPos) + 1
				
				/* Ignore garbage strings 50 chars or more */
				IF @CurrentWordLength < 51
				BEGIN
					IF @CurrentWordLength < 1
					BEGIN
						SELECT @CurrentWord = ''
					END
					ELSE
					BEGIN
						SELECT @CurrentWord = SUBSTRING(@Sentence, @CursorPos, @CurrentWordLength)
					END
				END
				ELSE
				BEGIN
					SELECT @CurrentWord = '(too long)'
				END
				
				/* Definitely end of sentence */
				SET @IsSentenceBreak = 1
				
				/* Finish the loop */
				SELECT @CursorPos = -1
			END
			
			/* Turn phrases back into their constituent words where applicable */
			SELECT @CurrentWord = REPLACE(@currentword, '_', ' ')
			
			/* Store the word itself and the presence of a full stop etc if found */
			INSERT @Words (WordText, IsSentenceBreak) 
			VALUES (@CurrentWord, @IsSentenceBreak)				
			
			SELECT @test += 1
		END		

		SELECT cc.CategoryContextID, cc.Context, cc.Category1, cc.Category2, cc.Category3,
		cg.CategoryGroupID,	cg.GroupName, ck.CategoryKeywordID, ck.Keyword 
		FROM CategoryKeyword ck WITH (NOLOCK) 		
		INNER JOIN CategoryGroup cg WITH (NOLOCK) ON cg.CategoryGroupID = ck.CategoryGroupID		
		INNER JOIN CategoryContext  cc WITH (NOLOCK) ON cc.CategoryContextID = cg.CategoryContextID
		INNER JOIN @Words mw ON mw.WordText = ck.Keyword		
		WHERE ck.ClientID=@ClientID
		ORDER BY cc.CategoryContextID			
			
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[CategoriseResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CategoriseResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CategoriseResponse] TO [sp_executeall]
GO
