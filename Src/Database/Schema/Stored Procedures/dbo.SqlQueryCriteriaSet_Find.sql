SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryCriteriaSet table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSet_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryCriteriaSetID int   = null ,

	@ClientID int   = null ,

	@SqlQueryID int   = null ,

	@SetName varchar (250)  = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryCriteriaSetID]
	, [ClientID]
	, [SqlQueryID]
	, [SetName]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
    FROM
	[dbo].[SqlQueryCriteriaSet] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID OR @SqlQueryCriteriaSetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryID] = @SqlQueryID OR @SqlQueryID IS NULL)
	AND ([SetName] = @SetName OR @SetName IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryCriteriaSetID]
	, [ClientID]
	, [SqlQueryID]
	, [SetName]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
    FROM
	[dbo].[SqlQueryCriteriaSet] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID AND @SqlQueryCriteriaSetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryID] = @SqlQueryID AND @SqlQueryID is not null)
	OR ([SetName] = @SetName AND @SetName is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSet_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Find] TO [sp_executeall]
GO
