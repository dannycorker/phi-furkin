SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 17 July 2007

-- Created By:  (Paul Richardson)
-- Purpose: Select folders by client id and folder type id
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[Folder__GetByFolderTypeIDAndClientID]
(
	@FolderTypeID INT,
	@ClientID INT 	
)
AS


				SELECT *
				FROM
					[dbo].[Folder]
				WHERE
					[FolderTypeID] = @FolderTypeID AND
					[ClientID] = @ClientID ORDER BY FolderName ASC
				SELECT @@ROWCOUNT


GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__GetByFolderTypeIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder__GetByFolderTypeIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__GetByFolderTypeIDAndClientID] TO [sp_executeall]
GO
