SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2020-04-02
-- Description:	Card Transaction update with response for SecureCo / Team Members: James Lewis (JEL), Paul Richardson (PR), Gavin Reynolds (GPR)
-- 2020-04-20 GPR for AAG-679	| Added call to apply EventType
-- 2020-02-10 ACE Adapted from Billing_Braintree_UpdateWithResponse
-- 2021-02-17 ACE - SDPRU-239 
-- 2021-04-13 ACE - Added normal days retry date
-- =============================================
CREATE PROCEDURE [dbo].[Billing_Braintree_UpdateWithResponse]
	 @CardTransactionID			INT
	,@ErrorCode					VARCHAR (100)
	,@ErrorMessage				VARCHAR (500)
	,@TransactionID				VARCHAR (50)
	,@AuthCode					VARCHAR (250)
	,@StatusMsg					VARCHAR (250)

AS
BEGIN
  
    DECLARE	@JournalType				VARCHAR (100) 
			,@MaskedCardNumber			VARCHAR (200)
			,@ExpiryDate				DATE
			,@ExpiryMonth				VARCHAR (2)
			,@ExpiryYear				VARCHAR (4)
			,@Amount					DECIMAL (18,2)
			,@AccountID							INT
			,@PaymentID							INT
			,@CustomerPaymentScheduleID			INT
			,@CustomerLedgerID					INT
			,@RunAsUserID						INT = dbo.fn_C600_GetAqAutomationUser()
			,@EventToAdd						INT
			,@ClientID							INT = dbo.fnGetPrimaryClientID()
			,@CaseID							INT
			,@LeadEventID						INT
			,@EventTypeID						INT
			,@LeadID							INT
			,@DateForEvent						DATETIME = dbo.fn_GetDate_Local()
			,@CustomerID						INT
			,@MatterID							INT
			,@PolicyMatterID					INT
			,@Description						VARCHAR(200)
			,@LogEntry							VARCHAR (MAX)
			,@AmountNet							DECIMAL(18,2)
			,@AmountVat							DECIMAL (18,2)
			,@PurchasedProductID				INT
			,@PurchasedProductPaymentScheduleID INT 
			,@EventComments						VARCHAR(2000) = ''
			,@LogID								INT
			,@RowCount							INT
			,@LeadTypeID						INT = 1493 /*Collections*/
			,@ContraCustomerLedgerID			INT
			,@OriginalPaymentStatus             INT
			,@NextPaymentDate		            DATE
			,@ClientAccountID                   INT
			,@VarcharDate			            VARCHAR(10) = ''
			,@SystemNoteTypeID		            INT = 919
			,@NoteText				            VARCHAR(2000) = ''
			,@CCRetryType			            VARCHAR(255)
			,@RetryWorkingDays		            INT
			,@RetryDays							INT
			,@Retrypayment						BIT
			,@PAMatterID						INT
			,@ProjectedCancellationDate			DATE

	DECLARE @CustomerPaymentScheduleTransform TABLE (NewCustomerPaymentScheduleID INT, OldCustomerPaymentScheduleID INT)

	DECLARE @FailedPPPS TABLE (PPPSID INT, PurchasedProductID INT)

	SELECT @LogEntry = CAST( ISNULL(@CardTransactionID,'ull')	as VARCHAR) + ' CT '  +
					  CAST( ISNULL(@ErrorCode,'ull')		as VARCHAR) + ' ec '  +		
					  CAST( ISNULL(@ErrorMessage,'ull')			as VARCHAR) + ' em '  +
					  CAST( ISNULL(@TransactionID,'ull')		as VARCHAR) + ' ti '  +	
					  CAST( ISNULL(@AuthCode,'ull')				as VARCHAR) + ' ac '  +
					  CAST( ISNULL(@StatusMsg,'ull')			as VARCHAR) + ' sm ' 

	EXEC _C00_LogIt    'Info', 'Billing_Braintree_UpdateWithResponse','Braintree',  @LogEntry, 58550

	/*Pick up other values from Card Transaction*/ 
	SELECT @CustomerID = c.CustomerID, 
			@Amount = c.Amount, 
			@PaymentID = p.PaymentID, 
			@CustomerLedgerID = cl.CustomerLedgerID , 
			@CustomerPaymentScheduleID = c.CustomerPaymentScheduleID, 
			@MatterID = c.ObjectID , 
			@PurchasedProductID = c.ParentID, 
			@AccountID = c.AccountID /*GPR 2020-03-04 - remove  use passed in StatusMsg*/-- ,@StatusMsg = c.StatusMsg
	FROM CardTransaction c WITH (NOLOCK)
	INNER JOIN Payment p with (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
	INNER JOIN CustomerLedger cl with (NOLOCK) on cl.PaymentID = p.PaymentID 
	WHERE c.CardTransactionID = @CardTransactionID 
	AND c.ObjectID IS NOT NULL	

	/*Update the relevant schedule records if it worked*/ 
	IF @StatusMsg IN ('1000') OR (@StatusMsg = '1002' AND @AuthCode = 'Processed')
	BEGIN 

		UPDATE PurchasedProductPaymentSchedule 
		Set PaymentStatusID = 6,CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
		From PurchasedProductPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE CustomerPaymentSchedule  
		Set PaymentStatusID = 6, CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
		From CustomerPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		SELECT @Description += LEFT(p.ProductDescription,CHARINDEX('(',p.ProductDescription,1)-1) + ' ' + pt.PurchasedProductPaymentScheduleTypeName + ' '
		FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
		INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.PurchasedProductID = pp.PurchasedProductID 
		INNER JOIN PurchasedProductPaymentScheduleType pt WITH (NOLOCK) on pt.PurchasedProductPaymentScheduleTypeID = pp.PurchasedProductPaymentScheduleTypeID
		WHERE pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
									
		UPDATE CustomerLedger 
		SET TransactionDescription = @Description, TransactionDate = dbo.fn_GetDate_Local()
		WHERE CustomerLedgerID = @CustomerLedgerID 

		UPDATE p 
		SET p.PaymentStatusID = 6,
				p.FailureReason = @ErrorMessage, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
		FROM Payment p 
		WHERE p.PaymentID = @PaymentID

		Update CardTransaction 
		SET StatusMsg = @StatusMsg, TransactionID = @TransactionID, AuthCode = @AuthCode
		Where CardTransactionID = @CardTransactionID 

	END 

	/*Pick up the client account ID from the purchasedproduct*/
	SELECT @ClientAccountID = cps.ClientAccountID  
	FROM Payment p WITH (NOLOCK) 
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) on cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
	WHERE p.PaymentID = @PaymentID

	/* Set Original Payment Status */
	IF EXISTS (
		SELECT *
		FROM Payment p WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule pp WITH (NOLOCK) ON pp.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
		WHERE p.PaymentID = @PaymentID
		AND pp.PurchasedProductPaymentScheduleID <> pp.PurchasedProductPaymentScheduleParentID
	)
	BEGIN

		/*This payment must be a retry. Therefore we will not attempt any more payment attempts...*/
		SELECT @OriginalPaymentStatus = 5 	
			
	END
	ELSE
	BEGIN

		SELECT @OriginalPaymentStatus = 2 

	END

	/*Update the relevant schedule records if it failed*/ 
	IF EXISTS (
		SELECT * 
		FROM CardReturnProcessing crt WITH (NOLOCK)
		WHERE crt.FailureCode = @StatusMsg 
		AND @StatusMsg <> 0
	) 
	BEGIN

		SELECT @EventToAdd = c.EventToApply, 
				@RunAsUserID = c.AddEventAs, 
				@ErrorMessage = c.FailureDescription,
				@Retrypayment = c.RetryPayment
		FROM CardReturnProcessing c WITH (NOLOCK)
		INNER JOIN EventType et WITH (NOLOCK) on et.EventTypeID = c.EventToApply
		WHERE c.FailureCode IN ('-1', @ErrorCode) /*-1 is a fallback event...*/
		AND c.ClientID IN (0, @ClientID)
		--AND c.ReasonCodeType = @ClientPaymentGatewayID /*GPR 2020-04-02 removed as not required*/
		AND et.LeadTypeID = @LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
		ORDER BY c.ClientID DESC, c.FailureCode DESC

		Update CardTransaction 
		SET StatusMsg = @StatusMsg, TransactionID = @TransactionID, ErrorMessage = @ErrorMessage 
		Where CardTransactionID = @CardTransactionID 
				
		/*So not ok, this means we have a failure. Time to do the fail thing...*/
		UPDATE p 
		SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
				p.FailureReason = @ErrorMessage, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
		FROM Payment p 
		WHERE p.PaymentID = @PaymentID

		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		SELECT p.ClientID, p.CustomerID, PaymentDateTime, p.FailureCode, p.FailureReason, CAST(dbo.fn_GetDate_Local() AS DATE), p.CardTransactionID, 'Failure notification ' + p.FailureReason, -p.PaymentNet, -p.PaymentVAT, -p.PaymentGross, NULL, NULL, NULL, @PaymentID, NULL, 58552 /*@AqAutomation*/, dbo.fn_GetDate_Local() 
		FROM Payment p WITH (NOLOCK) 
		WHERE p.PaymentID=@PaymentID
	            
		SELECT @ContraCustomerLedgerID = SCOPE_IDENTITY()

		-- Reverse customerpaymentschedule, reverse payment status
		UPDATE cps  
		SET PaymentStatusID = 4, ReconciledDate = dbo.fn_GetDate_Local(), CustomerLedgerID = @CustomerLedgerID 
		FROM CustomerPaymentSchedule cps WITH (NOLOCK)
		INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		-- Purchasedproductpaymentschedule, reverse payment status
		UPDATE ppps  
		SET PaymentStatusID = 4, ReconciledDate = dbo.fn_GetDate_Local(), ContraCustomerLedgerID = @CustomerLedgerID, CustomerLedgerID = @CustomerLedgerID 
			OUTPUT inserted.PurchasedProductPaymentScheduleID, inserted.PurchasedProductID
			INTO @FailedPPPS (PPPSID, PurchasedProductID)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
		WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		SELECT @CCRetryType = cp.PreferenceValue 
		FROM ClientPreference cp WITH (NOLOCK)
		WHERE cp.ClientID = @ClientID
		AND cp.ClientPreferenceTypeID = 3 --try in x days

		SELECT @RetryWorkingDays = cp.PreferenceValue
		FROM ClientPreference cp WITH (NOLOCK)
		WHERE cp.ClientID = @ClientID
		AND cp.ClientPreferenceTypeID = 4 --3 days

		SELECT @RetryDays = cp.PreferenceValue
		FROM ClientPreference cp WITH (NOLOCK)
		WHERE cp.ClientID = @ClientID
		AND cp.ClientPreferenceTypeID = 8 --8 days

		IF @RetryWorkingDays > 0
		BEGIN

			SELECT @NextPaymentDate = dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(), @RetryWorkingDays)

		END
	
		IF @RetryDays > 0
		BEGIN

			SELECT @NextPaymentDate = DATEADD(DD, @RetryDays, dbo.fn_GetDate_Local())

		END
	
		SELECT @CaseID = m.CaseID, 
				@MatterID = m.MatterID, 
				@LeadID = m.LeadID, 
				@AccountID = cps.AccountID, 
				@VarcharDate = CONVERT(VARCHAR,@NextPaymentDate,121)
		FROM Payment p WITH (NOLOCK)
		INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID
		INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = a.ObjectID
		WHERE p.PaymentID = @PaymentID
		AND p.ObjectTypeID = 2 /*Matter*/

		IF @CCRetryType = 'Try In X Days' AND @Retrypayment = 1
		BEGIN

			PRINT @OriginalPaymentStatus
		
			/*Dont retry if this is a retry...*/
			IF @OriginalPaymentStatus <> 5
			BEGIN
					
				INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, WhoCreated, WhenCreated, SourceID, ClientAccountID, RelatedObjectID, RelatedObjectTypeID)
				OUTPUT inserted.CustomerPaymentScheduleID, inserted.SourceID INTO @CustomerPaymentScheduleTransform (NewCustomerPaymentScheduleID, OldCustomerPaymentScheduleID)
				SELECT cps.ClientID, cps.CustomerID, cps.AccountID, @NextPaymentDate, @NextPaymentDate, cps.PaymentNet, cps.PaymentVAT, cps.PaymentGross, 5 /*Retry*/, NULL, NULL, cps.WhoCreated, dbo.fn_GetDate_Local(), cps.CustomerPaymentScheduleID, @ClientAccountID, cps.RelatedObjectID, cps.RelatedObjectTypeID
				FROM CustomerPaymentSchedule cps WITH (NOLOCK)
				INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
				WHERE p.PaymentID = @PaymentID
				
				INSERT INTO PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
				SELECT ppps.ClientID, ppps.CustomerID, ppps.AccountID, ppps.PurchasedProductID, @NextPaymentDate, ppps.CoverFrom, ppps.CoverTo, @NextPaymentDate, ppps.PaymentNet, ppps.PaymentVAT, ppps.PaymentGross, 5 /*Retry*/, NULL, NULL, cpt.NewCustomerPaymentScheduleID, ppps.WhoCreated, dbo.fn_GetDate_Local(), ppps.PurchasedProductPaymentScheduleParentID, 3, @ClientAccountID, ppps.TransactionFee
				FROM PurchasedProductPaymentSchedule ppps
				INNER JOIN @CustomerPaymentScheduleTransform cpt ON cpt.OldCustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
				INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
				WHERE p.PaymentID = @PaymentID

				SELECT @NoteText = 'CC Failure Identified.  Retry scheduled for ' + ISNULL(@VarcharDate,'NULL') + ' Failure reason ' + ISNULL(@ErrorMessage, 'Not set')
				EXEC _C00_AddANote @CaseID, @SystemNoteTypeID, @NoteText, 0, @RunAsUserID, 0, 1 /*System*/
				
				EXEC dbo.Account__SetDateAndAmountOfNextPayment @AccountID
			
			END

		END

		/*2021-04-07 ALM/GPR for FURKIN-247 & FURKIN-248 Projected Cancellation Date*/
		IF EXISTS (
			SELECT * 
			FROM CardReturnProcessing crt WITH (NOLOCK)
			WHERE crt.FailureCode = @StatusMsg 
			AND @StatusMsg <> 0
		) 
		BEGIN
			SELECT @PAMatterID = [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections](@MatterID)
			SELECT @ProjectedCancellationDate = ISNULL(dbo.fn_C607_CalculatedProjectedCancellationDate(@PAMatterID), dbo.fn_GetDate_Local())
			EXEC _C00_SimpleValueIntoField 315274, @ProjectedCancellationDate, @PAMatterID, 58552
		END

		/*GPR 2020-04-20 for AAG-679*/
		/*
		SELECT @LeadEventID = c.LatestLeadEventID 
		FROM Lead l WITH (NOLOCK)
		INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
		WHERE l.LeadTypeID = @LeadTypeID
		AND l.CustomerID = @CustomerID
		*/

		/*GPR 2021-04-09 GetMatterID from CardTransaction for Conor*/
		/*SELECT @MatterID = c.ObjectID
		FROM CardTransaction c WITH (NOLOCK)
		WHERE c.CardTransactionID = @CardTransactionID  */

		/*2021-02-17 ACE - SDPRU-239 Dont get a random leadevent for this customer!*/
		SELECT @LeadEventID = c.LatestNonNoteLeadEventID
		FROM Matter m WITH (NOLOCK)
		INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = m.CaseID
		WHERE m.MatterID = @MatterID

		/*GPR 2020-04-22*/
		EXEC _C00_LogIt 'Info', 'Billing_Braintree_UpdateWithResponse EventToAdd', 'Braintree', @EventToAdd,  58550
		EXEC _C00_LogIt 'Info', 'Billing_Braintree_UpdateWithResponse LeadEventID','Braintree', @LeadEventID, 58550

		/*GPR 2020-04-21 added NULL check*/
		IF @LeadEventID IS NOT NULL 
		AND @EventToAdd IS NOT NULL 
		BEGIN

			EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID, @EventToAdd, 58552 /*AQ Automation*/, -1

		END

	END

END
GO
GRANT EXECUTE ON  [dbo].[Billing_Braintree_UpdateWithResponse] TO [sp_executeall]
GO
