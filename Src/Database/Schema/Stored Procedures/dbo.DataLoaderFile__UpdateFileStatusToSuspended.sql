SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 17th July 2009
-- Description:	Updates the file status to suspended for all the scheduled files in the inbox for the 
--				given map and clientid
-- =============================================
CREATE PROCEDURE [dbo].[DataLoaderFile__UpdateFileStatusToSuspended] 
@MapID int, @ClientID int
AS
BEGIN

	UPDATE
		[dbo].[DataLoaderFile]
	SET
		[FileStatusID] = 8
	WHERE TargetFileLocation = 'Inbox' and FileStatusID=2 and DataLoaderMapID=@MapID and ClientID=@ClientID


END




GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile__UpdateFileStatusToSuspended] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile__UpdateFileStatusToSuspended] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile__UpdateFileStatusToSuspended] TO [sp_executeall]
GO
