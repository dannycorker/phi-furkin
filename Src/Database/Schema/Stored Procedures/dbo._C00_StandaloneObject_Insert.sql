SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-09-16
-- Description:	Insert into standaloneobjects table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_StandaloneObject_Insert]
 @StandaloneObjectType VARCHAR(100)
, @StandaloneObjectName VARCHAR(100)
, @StandaloneObjectDescription VARCHAR(500)
, @WhenCreated datetime
, @WhenModified datetime

AS
BEGIN

	SET NOCOUNT ON;

INSERT INTO [dbo].[StandaloneObject]
           ([StandaloneObjectType]
           ,[StandaloneObjectName]
           ,[StandaloneObjectDescription]
           ,[WhenCreated]
           ,[WhenModified])
     VALUES
           (@StandaloneObjectType
           ,@StandaloneObjectName
           ,@StandaloneObjectDescription
           ,@WhenCreated
           ,@WhenModified)
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_StandaloneObject_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_StandaloneObject_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_StandaloneObject_Insert] TO [sp_executeall]
GO
