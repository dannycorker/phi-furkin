SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 14/04/2016
-- Description:	Find customer via lead ref 
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByLeadRef]
	@ClientID INT ,
	@Reference VARCHAR(100)
AS
BEGIN

	SELECT c.* FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID AND l.LeadRef LIKE '%' + @Reference + '%'
	WHERE C.ClientID=@ClientID	
	ORDER BY Fullname ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByLeadRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByLeadRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByLeadRef] TO [sp_executeall]
GO
