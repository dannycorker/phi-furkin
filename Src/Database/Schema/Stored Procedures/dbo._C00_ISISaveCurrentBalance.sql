SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-018
-- Description:	Save balance to amount field and amount to hidden one
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISISaveCurrentBalance]
	@TableRowID INT,
	@ClientPersonnelID INT,
	@Value VARCHAR(2000)
AS
BEGIN

	SET NOCOUNT ON;

	/*
		1. Get the current balance from the third party field 851
		2. Save that value to third party field 1317
		3. Save the value that is passed in to field 851
	*/
	
	DECLARE @OldValue VARCHAR(2000),
			@HiddenFieldID INT,
			@BalanceFieldID INT,
			@ClientID INT
	
	SELECT @ClientID = tr.ClientID
	FROM TableRows tr WITH (NOLOCK)
	WHERE tr.TableRowID = @TableRowID
	
	SELECT @HiddenFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = tpfm.DetailFieldID AND tdv.TableRowID = @TableRowID
	WHERE tpfm.ClientID = @ClientID
	AND tpfm.ThirdPartyFieldID = 1317

	SELECT top 1 @BalanceFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = tpfm.DetailFieldID AND tdv.TableRowID = @TableRowID
	WHERE tpfm.ClientID = @ClientID
	AND tpfm.ThirdPartyFieldID = 851
	
	SELECT @OldValue = dbo.fnGetSimpleDv(@BalanceFieldID, @TableRowID)
	
	EXEC dbo._C00_SimpleValueIntoField @HiddenFieldID, @OldValue, @TableRowID, @ClientPersonnelID

	EXEC dbo._C00_SimpleValueIntoField @BalanceFieldID, @Value, @TableRowID, @ClientPersonnelID
	
	SELECT @OldValue, @HiddenFieldID, @BalanceFieldID, @Value, @TableRowID, @ClientPersonnelID, @ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISISaveCurrentBalance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISISaveCurrentBalance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISISaveCurrentBalance] TO [sp_executeall]
GO
