SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SMSResponseMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSResponseMap_Insert]
(

	@SMSResponseMapID int    OUTPUT,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@FromSMSQuestionGroupID int   ,

	@FromSMSQuestionID int   ,

	@FromAnswer varchar (256)  ,

	@ToSMSQuestionGroupID int   ,

	@ToSMSQuestionID int   ,

	@EndOfQuestionnaire bit   ,

	@Score int   ,

	@ConfidenceRangeStart int   ,

	@ConfidenceRangeEnd int   ,

	@RuleID int   ,

	@ProcessingOrder int   
)
AS


				
				INSERT INTO [dbo].[SMSResponseMap]
					(
					[ClientID]
					,[SMSQuestionnaireID]
					,[FromSMSQuestionGroupID]
					,[FromSMSQuestionID]
					,[FromAnswer]
					,[ToSMSQuestionGroupID]
					,[ToSMSQuestionID]
					,[EndOfQuestionnaire]
					,[Score]
					,[ConfidenceRangeStart]
					,[ConfidenceRangeEnd]
					,[RuleID]
					,[ProcessingOrder]
					)
				VALUES
					(
					@ClientID
					,@SMSQuestionnaireID
					,@FromSMSQuestionGroupID
					,@FromSMSQuestionID
					,@FromAnswer
					,@ToSMSQuestionGroupID
					,@ToSMSQuestionID
					,@EndOfQuestionnaire
					,@Score
					,@ConfidenceRangeStart
					,@ConfidenceRangeEnd
					,@RuleID
					,@ProcessingOrder
					)
				-- Get the identity value
				SET @SMSResponseMapID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Insert] TO [sp_executeall]
GO
