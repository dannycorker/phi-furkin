SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the MatterDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_Insert]
(

	@MatterDetailValueID int    OUTPUT,

	@ClientID int   ,

	@LeadID int   ,

	@MatterID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@OriginalDetailValueID int   ,

	@OriginalLeadID int   ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[MatterDetailValues]
					(
					[ClientID]
					,[LeadID]
					,[MatterID]
					,[DetailFieldID]
					,[DetailValue]
					,[ErrorMsg]
					,[OriginalDetailValueID]
					,[OriginalLeadID]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@LeadID
					,@MatterID
					,@DetailFieldID
					,@DetailValue
					,@ErrorMsg
					,@OriginalDetailValueID
					,@OriginalLeadID
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@SourceID
					)
				-- Get the identity value
				SET @MatterDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Insert] TO [sp_executeall]
GO
