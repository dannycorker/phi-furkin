SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-02-2013
-- Description:	Deletes a PartnerMatchKey
-- =============================================
CREATE PROCEDURE [dbo].[PartnerMatchKey__Delete]
	
	@ClientID INT,
	@PartnerID INT

AS
BEGIN

	DELETE FROM PartnerMatchKey
	WHERE ClientID=@ClientID AND PartnerID = @PartnerID
		
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PartnerMatchKey__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__Delete] TO [sp_executeall]
GO
