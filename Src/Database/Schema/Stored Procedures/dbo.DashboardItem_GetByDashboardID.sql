SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DashboardItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItem_GetByDashboardID]
(

	@DashboardID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DashboardItemID],
					[DashboardID],
					[DashboardItemTypeID],
					[ItemID],
					[ClientID],
					[LayoutPosition]
				FROM
					[dbo].[DashboardItem] WITH (NOLOCK) 
				WHERE
					[DashboardID] = @DashboardID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_GetByDashboardID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItem_GetByDashboardID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_GetByDashboardID] TO [sp_executeall]
GO
