SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-09-25
-- Description:	SP To run report and output results
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RunReport]
@QueryID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SqlQuery varchar(max),
			@ClientID INT
	
	SELECT @SqlQuery = sq.QueryText, @ClientID = sq.ClientID
	FROM SqlQuery sq WITH (NOLOCK)
	WHERE sq.QueryID = @QueryID

	SELECT @SqlQuery = REPLACE(@SqlQuery, '@ClientID', @ClientID)
	SELECT @SqlQuery = REPLACE(@SqlQuery, '@Today', CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121) )

	EXEC (@SqlQuery)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RunReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RunReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RunReport] TO [sp_executeall]
GO
