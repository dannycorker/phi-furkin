SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailFieldSubType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldSubType_Delete]
(

	@DetailFieldSubTypeID tinyint   
)
AS


				DELETE FROM [dbo].[DetailFieldSubType] WITH (ROWLOCK) 
				WHERE
					[DetailFieldSubTypeID] = @DetailFieldSubTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldSubType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Delete] TO [sp_executeall]
GO
