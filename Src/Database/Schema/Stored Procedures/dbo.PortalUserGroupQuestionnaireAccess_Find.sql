SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUserGroupQuestionnaireAccess table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserGroupQuestionnaireAccessID int   = null ,

	@ClientID int   = null ,

	@PortalUserGroupID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@HasAccess bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserGroupQuestionnaireAccessID]
	, [ClientID]
	, [PortalUserGroupID]
	, [ClientQuestionnaireID]
	, [HasAccess]
    FROM
	[dbo].[PortalUserGroupQuestionnaireAccess] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserGroupQuestionnaireAccessID] = @PortalUserGroupQuestionnaireAccessID OR @PortalUserGroupQuestionnaireAccessID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PortalUserGroupID] = @PortalUserGroupID OR @PortalUserGroupID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([HasAccess] = @HasAccess OR @HasAccess IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserGroupQuestionnaireAccessID]
	, [ClientID]
	, [PortalUserGroupID]
	, [ClientQuestionnaireID]
	, [HasAccess]
    FROM
	[dbo].[PortalUserGroupQuestionnaireAccess] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserGroupQuestionnaireAccessID] = @PortalUserGroupQuestionnaireAccessID AND @PortalUserGroupQuestionnaireAccessID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PortalUserGroupID] = @PortalUserGroupID AND @PortalUserGroupID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([HasAccess] = @HasAccess AND @HasAccess is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Find] TO [sp_executeall]
GO
