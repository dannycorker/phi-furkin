SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PageControlText table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_GetByPageControlTextID]
(

	@PageControlTextID int   
)
AS


				SELECT
					[PageControlTextID],
					[ClientID],
					[LanguageID],
					[PageName],
					[ControlName],
					[ControlText],
					[TooltipText],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[PageControlText] WITH (NOLOCK) 
				WHERE
										[PageControlTextID] = @PageControlTextID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_GetByPageControlTextID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_GetByPageControlTextID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_GetByPageControlTextID] TO [sp_executeall]
GO
