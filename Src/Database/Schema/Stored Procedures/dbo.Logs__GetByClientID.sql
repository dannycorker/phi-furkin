SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2009/10/27
-- Description:	Gets a list of the most recent 100 log entries for the specified ClientID
--              2009-11-24 Jim Green Disabled as we are not ready to release this feature yet!
-- =============================================
CREATE PROCEDURE [dbo].[Logs__GetByClientID] 
	@ClientID int,
	@DateFrom DateTime,
	@DateTo DateTime,
	@ClientPersonnelID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 100 lg.LogID
      ,cp.ClientID
      ,cp.UserName
      ,lg.[LogDateTime]
      ,lg.[TypeOfLogEntry]
      ,lg.[ClassName]
      ,lg.[MethodName]
      ,lg.[LogEntry]
      ,lg.[ClientPersonnelID]
  FROM [dbo].[Logs] lg (NOLOCK)
  INNER JOIN ClientPersonnel cp ON cp.ClientPersonnelID = lg.ClientPersonnelID
  WHERE cp.ClientID = @ClientID
  AND lg.TypeOfLogEntry = 'Error'
  AND lg.LogDateTime BETWEEN @DateFrom AND @DateTo
  AND (lg.ClientPersonnelID = @ClientPersonnelID OR lg.ClientPersonnelID IS NULL) 
  AND 1=2 /* Our internal messages are not ready for public readership yet. They ALL need to be vetted first. */
  
  UNION
  
  SELECT 0
      ,@ClientID
      ,''
      ,dbo.fn_GetDate_Local()
      ,''
      ,''
      ,''
      ,'This feature has not been implemented yet.'
      ,NULL
  ORDER BY 3 DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs__GetByClientID] TO [sp_executeall]
GO
