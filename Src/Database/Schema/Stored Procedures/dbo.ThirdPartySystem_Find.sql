SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartySystem table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystem_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartySystemId int   = null ,

	@SystemName varchar (50)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartySystemId]
	, [SystemName]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartySystem] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartySystemId] = @ThirdPartySystemId OR @ThirdPartySystemId IS NULL)
	AND ([SystemName] = @SystemName OR @SystemName IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartySystemId]
	, [SystemName]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartySystem] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartySystemId] = @ThirdPartySystemId AND @ThirdPartySystemId is not null)
	OR ([SystemName] = @SystemName AND @SystemName is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Find] TO [sp_executeall]
GO
