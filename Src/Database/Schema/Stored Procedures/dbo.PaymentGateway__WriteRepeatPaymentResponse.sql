SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-21
-- Description:	Writes the repeat payment response to the repeat payment table
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteRepeatPaymentResponse]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT,
	@RepeatPaymentResponseStatus VARCHAR(2000),
	@RepeatPaymentResponseStatusDetail VARCHAR(2000),
	@RepeatPaymentResponseVPSTxId VARCHAR(2000),
	@RepeatPaymentResponseSecurityKey VARCHAR(2000),
	@RepeatPaymentResponseTxAuthNo VARCHAR(2000),
	@RepeatPaymentResponseCV2Status  VARCHAR(2000),
	@RepeatPaymentResponseAddressResult VARCHAR(2000),
	@RepeatPaymentResponsePostCodeResult VARCHAR(2000),
	@RepeatPaymentResponseCV2Result VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @TransactionDate DATETIME
	SET @TransactionDate = CURRENT_TIMESTAMP

	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 548 THEN @RepeatPaymentResponseStatus
																	WHEN 549 THEN @RepeatPaymentResponseStatusDetail
																	WHEN 550 THEN @RepeatPaymentResponseVPSTxId
																	WHEN 551 THEN @RepeatPaymentResponseSecurityKey
																	WHEN 552 THEN @RepeatPaymentResponseTxAuthNo
																	WHEN 553 THEN @RepeatPaymentResponseCV2Status
																	WHEN 554 THEN @RepeatPaymentResponseAddressResult
																	WHEN 555 THEN @RepeatPaymentResponsePostCodeResult
																	WHEN 556 THEN @RepeatPaymentResponseCV2Result
																	WHEN 627 THEN CONVERT(varchar, @TransactionDate,120)
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (548,549,550,551,552,553,554,555,556, 627)
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRepeatPaymentResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteRepeatPaymentResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRepeatPaymentResponse] TO [sp_executeall]
GO
