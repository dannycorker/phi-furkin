SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnel table through an index
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[ClientPersonnel_GetByEmailAddressThirdPartySystemId]
(
      @EmailAddress varchar (255),
      @ThirdPartySystemId int   
)
AS
BEGIN
    DECLARE @OrigTPSID INT = @ThirdPartySystemId

    IF @ThirdPartySystemId NOT IN (0, 11, 37)
    BEGIN
        SELECT @ThirdPartySystemId = 0
    END

    SELECT
        [ClientPersonnelID],
        [ClientID],
        [ClientOfficeID],
        [TitleID],
        [FirstName],
        [MiddleName],
        [LastName],
        [JobTitle],
        [Password],
        [ClientPersonnelAdminGroupID],
        [MobileTelephone],
        [HomeTelephone],
        [OfficeTelephone],
        [OfficeTelephoneExtension],
        [EmailAddress],
        [ChargeOutRate],
        [UserName],
        [Salt],
        [AttemptedLogins],
        [AccountDisabled],
        [ManagerID],
        [Initials],
        [LanguageID],
        [SubClientID],
        [ForcePasswordChangeOn],
        @OrigTPSID AS [ThirdPartySystemId],
        [CustomerID],
		[IsAquarium],
		[AllowSmsCommandProcessing], 
		[MemorableWord], 
		[MemorableWordSalt], 
		[MemorableWordAttempts], 
		[PendingActivation]
    FROM
        [dbo].[ClientPersonnel] WITH (NOLOCK)
    WHERE
        [EmailAddress] = @EmailAddress
        AND [ThirdPartySystemId] = @ThirdPartySystemId
		
    SELECT @@ROWCOUNT
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByEmailAddressThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_GetByEmailAddressThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByEmailAddressThirdPartySystemId] TO [sp_executeall]
GO
