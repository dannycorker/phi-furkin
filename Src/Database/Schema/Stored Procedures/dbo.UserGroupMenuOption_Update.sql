SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the UserGroupMenuOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuOption_Update]
(

	@UserGroupMenuOptionID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientID int   ,

	@MasterPageName varchar (20)  ,

	@PanelItemAction varchar (10)  ,

	@PanelItemName varchar (50)  ,

	@PanelItemCaption varchar (50)  ,

	@PanelItemIcon varchar (50)  ,

	@PanelItemURL varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Comments varchar (250)  ,

	@ItemOrder int   ,

	@ShowInFancyBox bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[UserGroupMenuOption]
				SET
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ClientID] = @ClientID
					,[MasterPageName] = @MasterPageName
					,[PanelItemAction] = @PanelItemAction
					,[PanelItemName] = @PanelItemName
					,[PanelItemCaption] = @PanelItemCaption
					,[PanelItemIcon] = @PanelItemIcon
					,[PanelItemURL] = @PanelItemURL
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[Comments] = @Comments
					,[ItemOrder] = @ItemOrder
					,[ShowInFancyBox] = @ShowInFancyBox
				WHERE
[UserGroupMenuOptionID] = @UserGroupMenuOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Update] TO [sp_executeall]
GO
