SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryEditingCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_Get_List]

AS


				
				SELECT
					[SqlQueryEditingCriteriaSetMemberID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingCriteriaSetID],
					[SqlQueryEditingColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryEditingCriteriaSetMember] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Get_List] TO [sp_executeall]
GO
