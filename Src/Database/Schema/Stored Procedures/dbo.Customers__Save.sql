SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 22/03/2016
-- Description:	Inserts a new customer or updates an existing customer if the customer id is greater than 0
-- =============================================
CREATE PROCEDURE [dbo].[Customers__Save]
	@CustomerID int = null,
	@ClientID int   ,
	@TitleID int   ,
	@IsBusiness bit   ,
	@FirstName varchar (100)  ,
	@MiddleName varchar (100)  ,
	@LastName varchar (100)  ,
	@EmailAddress varchar (255)  ,
	@DayTimeTelephoneNumber varchar (50)  ,
	@DayTimeTelephoneNumberVerifiedAndValid bit   ,
	@HomeTelephone varchar (50)  ,
	@HomeTelephoneVerifiedAndValid bit   ,
	@MobileTelephone varchar (50)  ,
	@MobileTelephoneVerifiedAndValid bit   ,
	@CompanyTelephone varchar (50)  ,
	@CompanyTelephoneVerifiedAndValid bit   ,
	@WorksTelephone varchar (50)  ,
	@WorksTelephoneVerifiedAndValid bit   ,
	@Address1 varchar (200)  ,
	@Address2 varchar (200)  ,
	@Town varchar (200)  ,
	@County varchar (200)  ,
	@PostCode varchar (50)  ,
	@Website varchar (200)  ,
	@HasDownloaded bit   ,
	@DownloadedOn datetime   ,
	@AquariumStatusID int   ,
	@ClientStatusID int   ,
	@Test bit   ,
	@CompanyName varchar (100)  ,
	@Occupation varchar (100)  ,
	@Employer varchar (100)  ,
	@Fullname varchar (201)   OUTPUT,
	@PhoneNumbersVerifiedOn datetime   ,
	@DoNotEmail bit   ,
	@DoNotSellToThirdParty bit   ,
	@AgreedToTermsAndConditions bit   ,
	@DateOfBirth datetime   ,
	@DefaultContactID int   ,
	@DefaultOfficeID int   ,
	@AddressVerified bit   ,
	@CountryID int   ,
	@SubClientID int   ,
	@CustomerRef varchar (100)  ,
	@WhoChanged int   ,
	@WhenChanged datetime   ,
	@ChangeSource varchar (200)  ,
	@EmailAddressVerifiedAndValid bit = NULL   ,
	@EmailAddressVerifiedOn datetime = NULL   ,
	@Comments varchar (MAX) = NULL  ,
	@AllowSmsCommandProcessing bit = NULL,   
	@LanguageID int = NULL  
AS
BEGIN
		
	SET NOCOUNT ON;
	DECLARE @RC int

	IF @CustomerID>0 -- perform update
	BEGIN
		
		EXECUTE @RC = [dbo].[Customers_Update] 
		   @CustomerID
		  ,@ClientID
		  ,@TitleID
		  ,@IsBusiness
		  ,@FirstName
		  ,@MiddleName
		  ,@LastName
		  ,@EmailAddress
		  ,@DayTimeTelephoneNumber
		  ,@DayTimeTelephoneNumberVerifiedAndValid
		  ,@HomeTelephone
		  ,@HomeTelephoneVerifiedAndValid
		  ,@MobileTelephone
		  ,@MobileTelephoneVerifiedAndValid
		  ,@CompanyTelephone
		  ,@CompanyTelephoneVerifiedAndValid
		  ,@WorksTelephone
		  ,@WorksTelephoneVerifiedAndValid
		  ,@Address1
		  ,@Address2
		  ,@Town
		  ,@County
		  ,@PostCode
		  ,@Website
		  ,@HasDownloaded
		  ,@DownloadedOn
		  ,@AquariumStatusID
		  ,@ClientStatusID
		  ,@Test
		  ,@CompanyName
		  ,@Occupation
		  ,@Employer
		  ,@Fullname OUTPUT
		  ,@PhoneNumbersVerifiedOn
		  ,@DoNotEmail
		  ,@DoNotSellToThirdParty
		  ,@AgreedToTermsAndConditions
		  ,@DateOfBirth
		  ,@DefaultContactID
		  ,@DefaultOfficeID
		  ,@AddressVerified
		  ,@CountryID
		  ,@SubClientID
		  ,@CustomerRef
		  ,@WhoChanged
		  ,@WhenChanged
		  ,@ChangeSource
		  ,@EmailAddressVerifiedAndValid
		  ,@EmailAddressVerifiedOn
		  ,@Comments
		  ,@AllowSmsCommandProcessing
		  ,@LanguageID

	END
	ELSE -- no customer ID so perform insert
	BEGIN

		EXECUTE @RC = [dbo].[Customers_Insert] 
			   @CustomerID OUTPUT
			  ,@ClientID
			  ,@TitleID
			  ,@IsBusiness
			  ,@FirstName
			  ,@MiddleName
			  ,@LastName
			  ,@EmailAddress
			  ,@DayTimeTelephoneNumber
			  ,@DayTimeTelephoneNumberVerifiedAndValid
			  ,@HomeTelephone
			  ,@HomeTelephoneVerifiedAndValid
			  ,@MobileTelephone
			  ,@MobileTelephoneVerifiedAndValid
			  ,@CompanyTelephone
			  ,@CompanyTelephoneVerifiedAndValid
			  ,@WorksTelephone
			  ,@WorksTelephoneVerifiedAndValid
			  ,@Address1
			  ,@Address2
			  ,@Town
			  ,@County
			  ,@PostCode
			  ,@Website
			  ,@HasDownloaded
			  ,@DownloadedOn
			  ,@AquariumStatusID
			  ,@ClientStatusID
			  ,@Test
			  ,@CompanyName
			  ,@Occupation
			  ,@Employer
			  ,@Fullname OUTPUT
			  ,@PhoneNumbersVerifiedOn
			  ,@DoNotEmail
			  ,@DoNotSellToThirdParty
			  ,@AgreedToTermsAndConditions
			  ,@DateOfBirth
			  ,@DefaultContactID
			  ,@DefaultOfficeID
			  ,@AddressVerified
			  ,@CountryID
			  ,@SubClientID
			  ,@CustomerRef
			  ,@WhoChanged
			  ,@WhenChanged
			  ,@ChangeSource
			  ,@EmailAddressVerifiedAndValid
			  ,@EmailAddressVerifiedOn
			  ,@Comments
			  ,@AllowSmsCommandProcessing
			  ,@LanguageID
				
	END    
	
	SELECT @CustomerID CustomerID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__Save] TO [sp_executeall]
GO
