SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_Update]
(

	@ThirdPartyFieldMappingID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ThirdPartyFieldID int   ,

	@DetailFieldID int   ,

	@ColumnFieldID int   ,

	@ThirdPartyFieldGroupID int   ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyFieldMapping]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[ThirdPartyFieldID] = @ThirdPartyFieldID
					,[DetailFieldID] = @DetailFieldID
					,[ColumnFieldID] = @ColumnFieldID
					,[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
					,[IsEnabled] = @IsEnabled
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ThirdPartyFieldMappingID] = @ThirdPartyFieldMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Update] TO [sp_executeall]
GO
