SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-09
-- Description:	Gets all the claim details rows from all linked matters
-- Used by:		DF 145673, DF 147723, Popup called from DF 147723
-- Updates:		SB	2012-03-02	Allow to just return the passed in matter so same proc can be used on the claim details page
--				SB	2012-03-05	Allow to just return a single table row ID so same proc can be used on the claim details page edit popup
--				JL  2016-06-13  Add Subsection to condition for clarity of claim breakdown
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetGroupedClaimDetails] 
(
	@MatterID INT,
	@ForceJustThisMatter BIT = 0,
	@ForceSingleTableRowID INT = 0
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OutPut Table (MatterID INT,ParentID INT ,TableRowID INT ,Section VARCHAR(100), SubSection VARCHAR(100), TreatmentStart VARCHAR(100) , TreatmentEnd VARCHAR(100), 
												Claim VARCHAR(100), UserDeductions VARCHAR(100), Settle VARCHAR(100), Total VARCHAR(100), Approved VARCHAR(100), Paid VARCHAR(100), Payee VARCHAR(100), Excess VARCHAR(100), VoluntaryExcess VARCHAR(100),
												CoInsurance VARCHAR(100), Limit VARCHAR(100), OutsidePolicyCover VARCHAR(100), ExcessRebate VARCHAR(100), PayTo VARCHAR(100), OwedPremium VARCHAR(100), ClaimRowType VARCHAR(100), 
												SAPPaymentResult VARCHAR(100), SAPDocumentCode VARCHAR(100), ConditionID VARCHAR(100), Ailment1 VARCHAR(500), Ailment2 VARCHAR(500),ClaimRowTypeID VARCHAR(50), Reinstatment VARCHAR(100))
	
	INSERT INTO @OutPut (MatterID ,ParentID  ,TableRowID  ,Section , SubSection , TreatmentStart  , TreatmentEnd , 
												Claim , UserDeductions , Settle , Total , Approved , Paid , Payee , Excess , VoluntaryExcess ,
												CoInsurance , Limit , OutsidePolicyCover , ExcessRebate , PayTo , OwedPremium , ClaimRowType , 
												SAPPaymentResult , SAPDocumentCode , ConditionID, Ailment1 , Ailment2 ,ClaimRowTypeID , Reinstatment)
	
	SELECT	Pvt.MatterID, ParentID, TableRowID ,Pvt.Section + ' ' +	 CONVERT(VARCHAR,CAST(pvt.TreatmentStart as DATE),103) 
	 --CONVERT(VARCHAR,DATEPART(YEAR,Pvt.TreatmentStart),103)
	 as [Section], CASE Pvt.SubSection WHEN '-'  THEN Pvt.SubSection  ELSE Pvt.SubSection  + ' ' + CONVERT(VARCHAR,CAST(Pvt.TreatmentStart as DATE),103) 
	  END  as [SubSection], Pvt.TreatmentStart, Pvt.TreatmentEnd,Pvt.Claim, Pvt.UserDeductions, Pvt.Settle, Pvt.Total, Pvt.Approved, Pvt.Paid, Pvt.Payee, Pvt.Excess, Pvt.VoluntaryExcess,	Pvt.CoInsurance, Pvt.Limit, Pvt.OutsidePolicyCover, Pvt.ExcessRebate, Pvt.PayTo, Pvt.OwedPremium, Pvt.ClaimRowType, 
												Pvt.SAPPaymentResult, Pvt.SAPDocumentCode , mdv.ValueInt AS ConditionID, rdvAilment1.DetailValue + ' - ' + pvt.SubSection AS Ailment1, rdvAilment2.DetailValue AS Ailment2, llTpye.LookupListItemID AS ClaimRowTypeID,''
	FROM (
		SELECT r.MatterID, m.ParentID, r.TableRowID, 
		CASE 
			WHEN ISNULL(rlDf.DetailFieldID, df.DetailFieldID) IN (144349, 144351, 144362, 144354) THEN CONVERT(VARCHAR, tdv.ValueDate, 120)
			ELSE COALESCE(ll.ItemValue, ll2.ItemValue, tdv.DetailValue)
		END AS DetailValue,
		CASE ISNULL(rlDf.DetailFieldID, df.DetailFieldID)
			WHEN 146189 THEN 'Section'
			WHEN 146190 THEN 'SubSection'
			WHEN 144349 THEN 'TreatmentStart'
			WHEN 144351 THEN 'TreatmentEnd'
			WHEN 144353 THEN 'Claim'
			WHEN 146179 THEN 'UserDeductions'
			WHEN 145678 THEN 'Settle'
			WHEN 144352 THEN 'Total'
			WHEN 144362 THEN 'Approved'
			WHEN 144354 THEN 'Paid'
			WHEN 145679 THEN 'Payee'
			WHEN 146406 THEN 'Excess'
			WHEN 146407 THEN 'CoInsurance'
			WHEN 146408 THEN 'Limit'
			WHEN 147001 THEN 'OutsidePolicyCover'
			WHEN 147434 THEN 'ExcessRebate'
			WHEN 147602 THEN 'PayTo'
			WHEN 147605 THEN 'OwedPremium'
			WHEN 149778 THEN 'ClaimRowType'
			WHEN 158537 THEN 'SAPPaymentResult'
			WHEN 158538 THEN 'SAPDocumentCode'
			WHEN 158802 THEN 'VoluntaryExcess'
		END AS FieldCaption
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m ON r.MatterID = m.MatterID 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON m.CaseID = ca.CaseID
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON tdv.ResourceListID = rdv.ResourceListID AND rdv.DetailFieldID IN (146189, 146190)
		LEFT JOIN dbo.DetailFields rlDf WITH (NOLOCK) ON rdv.DetailFieldID = rlDf.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdv.ValueInt = ll.LookupListItemID AND rlDf.LookupListID = ll.LookupListID AND rlDf.QuestionTypeID IN (2, 4)
		LEFT JOIN dbo.LookupListItems ll2 WITH (NOLOCK) ON tdv.ValueInt = ll2.LookupListItemID AND df.LookupListID = ll2.LookupListID AND df.QuestionTypeID IN (2, 4)
		WHERE r.DetailFieldID = 144355
		AND r.DetailFieldPageID = 16157
		AND (@ForceJustThisMatter != 0 OR @ForceSingleTableRowID != 0 OR ca.ClientStatusID IS NULL OR ca.ClientStatusID NOT IN (3819)) -- Exclude rejected claims when we are not returning a single matter or row
		AND (@ForceJustThisMatter = 0 OR m.MatterID = @MatterID)
		AND (@ForceSingleTableRowID = 0 OR r.TableRowID = @ForceSingleTableRowID)) src
	PIVOT (MAX(DetailValue) FOR FieldCaption IN (Section, SubSection, TreatmentStart, TreatmentEnd, 
												Claim, UserDeductions, Settle, Total, Approved, Paid, Payee, Excess, VoluntaryExcess,
												CoInsurance, Limit, OutsidePolicyCover, ExcessRebate, PayTo, OwedPremium, ClaimRowType, 
												SAPPaymentResult, SAPDocumentCode)) AS pvt
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON pvt.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144504 /*Ailment*/
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment1 WITH (NOLOCK) ON rdvAilment1.ResourceListID = mdv.ValueInt AND rdvAilment1.DetailFieldID = 144340
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment2 WITH (NOLOCK) ON rdvAilment2.ResourceListID = mdv.ValueInt AND rdvAilment2.DetailFieldID = 144341
	INNER JOIN dbo.LookupListItems llTpye WITH (NOLOCK) ON pvt.ClaimRowType = llTpye.ItemValue AND llTpye.LookupListID = 3924
	ORDER BY ISNULL(Paid, dbo.fn_GetDate_Local()) DESC, ISNULL(Approved, dbo.fn_GetDate_Local()) DESC, TreatmentStart DESC, Pvt.MatterID DESC, TableRowID DESC
	
	DECLARE @PolicyMatterID INT = dbo.fn_C00_1272_GetPolicyFromClaim (@MatterID)
	
	Update o 
	Set Section = l.ItemValue, SubSection = ll.ItemValue 
	FROM @OutPut o
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = o.TableRowID and tdv.DetailFieldID = 144350
	INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.ResourceListID = tdv.ResourceListID and tdv1.DetailFieldID = 144357 
	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = tdv1.ResourceListID and rldv.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems l WITH (NOLOCK) on l.LookupListItemID = rldv.ValueInt 
	INNER JOIN dbo.ResourceListDetailValues rldv1 WITH (NOLOCK) on rldv1.ResourceListID = rldv.ResourceListID and rldv1.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) on ll.LookupListItemID = rldv1.ValueInt 
	INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tdv1.TableRowID and tdv2.DetailFieldID = 176935
	where tdv1.MatterID = @PolicyMatterID 
	and tdv2.ValueInt = 42932 /*Maximum Benefit*/
	
	SELECT *
	FROM @OutPut

	
	DECLARE @LeadID INT
	SELECT  @LeadID = m.LeadID
	FROM dbo.Matter m WITH (NOLOCK) 
	WHERE m.MatterID = @MatterID
	
	DECLARE @ParentID INT,
			@TotalClaim MONEY,
			@TotalClaimed MONEY
			
	SELECT @ParentID = ISNULL(ParentID, ClaimID)
	FROM dbo.fn_C00_1272_GetClaimRelationships(@LeadID)
	WHERE ClaimID = @MatterID

	SELECT @TotalClaim = ValueMoney
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID 
	AND DetailFieldID = 149850
	
	SELECT @TotalClaimed = SUM(ValueMoney)
	FROM dbo.TableDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144353
	
	SELECT	ISNULL(@ParentID, @MatterID) AS ParentID, 
			ISNULL(@TotalClaim, 0) AS TotalClaimAmount,
			ISNULL(@TotalClaimed, 0) AS TotalClaimed

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetGroupedClaimDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetGroupedClaimDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetGroupedClaimDetails] TO [sp_executeall]
GO
