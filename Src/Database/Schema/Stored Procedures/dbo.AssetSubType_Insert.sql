SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AssetSubType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetSubType_Insert]
(

	@AssetSubTypeID int    OUTPUT,

	@AssetSubTypeName varchar (255)  ,

	@Note varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[AssetSubType]
					(
					[AssetSubTypeName]
					,[Note]
					,[Enabled]
					,[Deleted]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@AssetSubTypeName
					,@Note
					,@Enabled
					,@Deleted
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @AssetSubTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetSubType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Insert] TO [sp_executeall]
GO
