SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CheckMateField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateField_Get_List]

AS


				
				SELECT
					[CheckMateFieldID],
					[CheckMateFieldName],
					[CheckMateFieldDescription],
					[QuestionTypeID],
					[LookupListID],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[CheckMateField] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateField_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Get_List] TO [sp_executeall]
GO
