SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-01-24
-- Description:	Returns basic Lead, case and matter information for a customer for the SDK
-- =============================================
CREATE PROCEDURE [dbo].[LeadsCasesMatter__GetByCustomerID]
(
	@CustomerID INT
)

AS
BEGIN

	SELECT l.LeadID, l.LeadTypeID
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.CustomerID = @CustomerID

	SELECT l.LeadID, c.CaseID
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
	WHERE l.CustomerID = @CustomerID

	SELECT c.CaseID, m.MatterID
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
	WHERE l.CustomerID = @CustomerID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[LeadsCasesMatter__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadsCasesMatter__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadsCasesMatter__GetByCustomerID] TO [sp_executeall]
GO
