SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.UpdateClientQuestionnaireDiagram    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[UpdateClientQuestionnaireDiagram]
@ClientQuestionnaireID int,
@QuestionnaireFlowDiagram text

as

Update ClientQuestionnaires
SET QuestionnaireFlowDiagram = @QuestionnaireFlowDiagram
where ClientQuestionnaireID = @ClientQuestionnaireID




GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireDiagram] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientQuestionnaireDiagram] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireDiagram] TO [sp_executeall]
GO
