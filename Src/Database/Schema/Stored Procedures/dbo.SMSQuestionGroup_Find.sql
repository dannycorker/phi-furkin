SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SMSQuestionGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@SMSQuestionGroupID int   = null ,

	@ClientID int   = null ,

	@SMSQuestionnaireID int   = null ,

	@GroupName varchar (250)  = null ,

	@SMSQuestionOrderID int   = null ,

	@GroupOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SMSQuestionGroupID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [GroupName]
	, [SMSQuestionOrderID]
	, [GroupOrder]
    FROM
	[dbo].[SMSQuestionGroup] WITH (NOLOCK) 
    WHERE 
	 ([SMSQuestionGroupID] = @SMSQuestionGroupID OR @SMSQuestionGroupID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SMSQuestionnaireID] = @SMSQuestionnaireID OR @SMSQuestionnaireID IS NULL)
	AND ([GroupName] = @GroupName OR @GroupName IS NULL)
	AND ([SMSQuestionOrderID] = @SMSQuestionOrderID OR @SMSQuestionOrderID IS NULL)
	AND ([GroupOrder] = @GroupOrder OR @GroupOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SMSQuestionGroupID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [GroupName]
	, [SMSQuestionOrderID]
	, [GroupOrder]
    FROM
	[dbo].[SMSQuestionGroup] WITH (NOLOCK) 
    WHERE 
	 ([SMSQuestionGroupID] = @SMSQuestionGroupID AND @SMSQuestionGroupID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SMSQuestionnaireID] = @SMSQuestionnaireID AND @SMSQuestionnaireID is not null)
	OR ([GroupName] = @GroupName AND @GroupName is not null)
	OR ([SMSQuestionOrderID] = @SMSQuestionOrderID AND @SMSQuestionOrderID is not null)
	OR ([GroupOrder] = @GroupOrder AND @GroupOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Find] TO [sp_executeall]
GO
