SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-06-12
-- Description:	Get a customer and affiliate
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByLeadIDAndAffiliate]
(
	@LeadID int
)
AS
BEGIN

	SELECT TOP (1) c.*, l.LeadTypeID, luli.ItemValue as [Affiliate]
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l on c.CustomerID = l.CustomerID
	INNER JOIN Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = ca.CaseID
	INNER JOIN ThirdPartyFieldMapping tpf WITH (NOLOCK) ON tpf.LeadTypeID = l.LeadTypeID and tpf.ThirdPartyFieldID = 187
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = tpf.DetailFieldID
	LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = mdv.ValueInt
	WHERE l.Leadid = @LeadID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByLeadIDAndAffiliate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByLeadIDAndAffiliate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByLeadIDAndAffiliate] TO [sp_executeall]
GO
