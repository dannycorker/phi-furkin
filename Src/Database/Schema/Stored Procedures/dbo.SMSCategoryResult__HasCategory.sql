SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-08-01
-- Description:	Checks the SMS Response Categorisation Results table for the given category
-- =============================================
CREATE PROCEDURE [dbo].[SMSCategoryResult__HasCategory]
	@MatterID INT,
	@Category VARCHAR(250)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadTypeID INT
	DECLARE @LeadID INT
	DECLARE @ClientID INT
	SELECT @LeadID=LeadID, @ClientID=ClientID FROM Matter WITH (NOLOCK)  WHERE MatterID=@MatterID
	SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

	DECLARE @CategoryColumnDetailFieldID INT	
	SELECT @CategoryColumnDetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=900 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID

	IF @CategoryColumnDetailFieldID IS NOT NULL AND @CategoryColumnDetailFieldID<>0
	BEGIN
		SELECT TableRowID FROM TableDetailValues WITH (NOLOCK) 
		WHERE MatterID=@MatterID AND DetailValue=@Category AND DetailFieldID=@CategoryColumnDetailFieldID
	END
	ELSE
	BEGIN 
		SELECT 0 TableRowID 		
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__HasCategory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCategoryResult__HasCategory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__HasCategory] TO [sp_executeall]
GO
