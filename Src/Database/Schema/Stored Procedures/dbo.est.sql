SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-11-05
-- Description:	List all QuestionType records
-- =============================================
CREATE PROCEDURE [dbo].[est] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT est.* 
	FROM dbo.EventSubtype est (nolock) 
	ORDER BY est.EventSubtypeID ASC 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[est] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[est] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[est] TO [sp_executeall]
GO
