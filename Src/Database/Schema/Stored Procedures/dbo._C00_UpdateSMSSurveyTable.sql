SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-04-2013
-- Description:	Updates the SMS Survey Table
--
-- Modified By PR on 23/01/2014 - Added the following SMS Survey Result columns:-
--									Language, Sentiment Polarity, Sentiment Confidence, Mixed Sentiment, Rules Engine Score
-- Modified By TD on 24/04/2014 - Pushed the reply into the customers fullname detail field(209597).
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateSMSSurveyTable]

	@TableRowID INT,
	@LeadTypeID INT,
	@MatterID INT,
	@MessageText VARCHAR(2000),
	@SMSResponseEventTypeID INT,
	@Score INT = NULL,
	@UnrecognisedResponse VARCHAR(2000) = NULL,
	@Language VARCHAR(2000) = NULL,
	@SentimentPolarity VARCHAR(2000) = NULL,
	@SentimentConfidence VARCHAR(2000) = NULL,
	@MixedSentiment VARCHAR(2000) = NULL,
	@RulesEngineScore VARCHAR(2000) = NULL,
	@NPS INT = NULL

	
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @DF_SMS_Message_Reply_From_Customer INT,
	@DF_Date_SMS_Reply_Received INT,		
	@DF_SMS_Response_EventTypeID INT,
	@DF_SMS_Response_Question_ID INT,
	@DF_Unrecognised_Response INT,
	@DF_Score INT,
	@DF_Language INT,
	@DF_SentimentPolarity INT,
	@DF_SentimentConfidence INT,
	@DF_MixedSentiment INT,
	@DF_RulesEngineScore INT,
	@DF_NpsCategory INT
	
	SELECT @DF_SMS_Message_Reply_From_Customer = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=376
	
	SELECT @DF_Date_SMS_Reply_Received = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=377
	
	SELECT @DF_SMS_Response_EventTypeID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=378
	
	SELECT @DF_SMS_Response_Question_ID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=379	

	SELECT @DF_Unrecognised_Response = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=384

	SELECT @DF_Score = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=385

	SELECT @DF_Language = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=820
	
	SELECT @DF_SentimentPolarity = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=821

	SELECT @DF_SentimentConfidence = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=822
	
	SELECT @DF_MixedSentiment = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=823

	SELECT @DF_RulesEngineScore = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=824	

	SELECT @DF_NpsCategory = ColumnFieldID
	FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=865
	
	DECLARE @SMSQuestionID INT
	SELECT @SMSQuestionID = qet.SMSQuestionID
	FROM SMSQuestionEventType qet WITH (NOLOCK) 
	WHERE qet.EventTypeID = @SMSResponseEventTypeID
	
	UPDATE TableDetailValues
	SET DetailValue = @MessageText 
	WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_SMS_Message_Reply_From_Customer
	
	UPDATE TableDetailValues
	SET DetailValue = CONVERT(VARCHAR(20), dbo.fn_GetDate_Local(),120) 
	WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_Date_SMS_Reply_Received
	
	UPDATE TableDetailValues
	SET DetailValue = CONVERT(VARCHAR(2000), @SMSResponseEventTypeID) 
	WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_SMS_Response_EventTypeID

	UPDATE TableDetailValues
	SET DetailValue = CONVERT(VARCHAR(2000), @SMSQuestionID) 
	WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_SMS_Response_Question_ID
	
	IF @UnrecognisedResponse IS NULL
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = CONVERT(VARCHAR(2000), 'false') 
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_Unrecognised_Response
		
	END
	ELSE 
	BEGIN

		UPDATE TableDetailValues
		SET DetailValue = @UnrecognisedResponse
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_Unrecognised_Response	
		
	END
	
	DECLARE @Score_Info VARCHAR(2000)
	IF @Score IS NULL
	BEGIN
		SET @Score_Info = ''
	END
	ELSE
	BEGIN
		SET @Score_Info =  CONVERT(VARCHAR(2000), @Score)
	END
	
	UPDATE TableDetailValues
	SET DetailValue = @Score_Info
	WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_Score


	IF @Language IS NOT NULL
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = @Language
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_Language	
		
	END
	
	IF @SentimentPolarity IS NOT NULL
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = @SentimentPolarity
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_SentimentPolarity	
		
	END	

	IF @SentimentConfidence IS NOT NULL
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = @SentimentConfidence
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_SentimentConfidence
		
	END	

	IF @MixedSentiment IS NOT NULL
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = @MixedSentiment
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_MixedSentiment
		
	END	
	
	IF @RulesEngineScore IS NOT NULL
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = @RulesEngineScore
		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_RulesEngineScore
		
	END		

	IF @NPS IS NOT NULL
	BEGIN

		UPDATE TableDetailValues
		SET DetailValue = @NPS

		WHERE TableRowID = @TableRowID AND MatterID=@MatterID AND DetailFieldID=@DF_NpsCategory

	END		

	
	--Added By Thomas 24-04-2014 
	--Takes the reply from the customer and places it in the Customers Fullname detail field 209597
	--For ClientID 268 UKATA
	IF @LeadTypeID=1417 AND @SMSResponseEventTypeID=138919
	BEGIN
		
		DECLARE @FirstName VARCHAR(MAX)		
		SELECT @FirstName=FirstName FROM fnNamesFromName(@MessageText)	
		
		EXEC _C00_SimpleValueIntoField 209597, @MessageText, @MatterID, 24794
		EXEC _C00_SimpleValueIntoField 209599, @FirstName, @MatterID, 24794
	
	END
	
	IF @LeadTypeID=1417 AND @SMSResponseEventTypeID = 138920
	BEGIN
	  
		EXEC _C00_SimpleValueIntoField 209600, @MessageText, @MatterID, 24794
	   
	END
		
	IF @LeadTypeID=1499 AND @SMSResponseEventTypeID = 140938 
	BEGIN

		EXEC _C00_SimpleValueIntoField 221089, @MessageText, @MatterID, 24794

	END
	
	IF @LeadTypeID = 1544 AND ISNULL(dbo.fnGetSimpleDv(243826,@MatterID),'') = '' /*ICM Conference SMS Out Event For Question 637*/
	BEGIN
		SELECT @MessageText = LTRIM(RTRIM(REPLACE(@MessageText,'ICM','')))
		EXEC _C00_SimpleValueIntoField 243826, @MessageText, @MatterID, 33393 /*Customer Name*/
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateSMSSurveyTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateSMSSurveyTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateSMSSurveyTable] TO [sp_executeall]
GO
