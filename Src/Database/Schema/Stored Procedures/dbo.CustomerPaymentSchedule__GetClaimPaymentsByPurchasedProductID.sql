SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-05-07
-- Description:	Gets Claim Payments for a PurchasedProduct
-- 2020-08-06	GPR joined to ClaimData and limited to payments for valid purchased product
-- 2020-09-10	ALM added fnGetPrimaryClientID()
-- 2020-09-22	ALM/GPR added ClaimID as LegacyRef to return in Control
-- =============================================
CREATE PROCEDURE [dbo].[CustomerPaymentSchedule__GetClaimPaymentsByPurchasedProductID]
	@ClientID INT,
	@PurchasedProductID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT @ClientID = dbo.fnGetPrimaryClientID()

	/*GPR/ALM/CR 2020-09-20*/
	SELECT cps.CustomerPaymentScheduleID, cps.PaymentDate, cps.ActualCollectionDate, cps.PaymentGross, ps.PaymentStatusName, visionclaim.VisionClaimID AS [LegacyRef] /*GPR/ALM 2020-09-22 return ClaimID*/
	FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
    INNER JOIN PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = cps.PaymentStatusID
    INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID AND AccountUseID = 2
    INNER JOIN OneVisionClaimPayment claimdata WITH (NOLOCK) ON claimdata.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	INNER JOIN OneVisionClaim visionclaim WITH (NOLOCK) ON visionclaim.OneVisionClaimID = claimdata.OneVisionClaimID
    INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = @PurchasedProductID
    WHERE cps.ClientID = @ClientID
	AND visionclaim.PAMatterID = pp.ObjectID
    AND cps.ActualCollectionDate BETWEEN pp.ValidFrom AND pp.ValidTo /*GPR 2020-08-06*/

	--SELECT cps.*, ps.PaymentStatusName
	--FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	--INNER JOIN PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = cps.PaymentStatusID
	--INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID AND AccountUseID = 2
	--INNER JOIN _C600_ClaimData claimdata WITH (NOLOCK) ON claimdata.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	--INNER JOIN Matter PAMAtterID WITH (NOLOCK) ON PAMAtterID.LeadID = claimdata.PolicyID
	--INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = @PurchasedProductID
	--WHERE cps.ClientID = @ClientID
	--AND PAMAtterID.MatterID = pp.ObjectID
	--AND cps.ActualCollectionDate BETWEEN pp.ValidFrom AND pp.ValidTo /*GPR 2020-08-06*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__GetClaimPaymentsByPurchasedProductID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerPaymentSchedule__GetClaimPaymentsByPurchasedProductID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__GetClaimPaymentsByPurchasedProductID] TO [sp_executeall]
GO
