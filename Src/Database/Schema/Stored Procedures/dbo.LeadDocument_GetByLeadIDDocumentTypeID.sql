SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDocument table through an index
-- JWG 2012-07-12 Use vLeadDocumentList to cater for archived documents.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_GetByLeadIDDocumentTypeID]
(
	@LeadID INT   ,
	@DocumentTypeID INT   
)
AS
BEGIN

	SELECT
		[LeadDocumentID],
		[ClientID],
		[LeadID],
		[DocumentTypeID],
		[LeadDocumentTitle],
		[UploadDateTime],
		[WhoUploaded],
		[DocumentBLOB],
		[FileName],
		[EmailBLOB],
		[DocumentFormat],
		[EmailFrom],
		[EmailTo],
		[CcList],
		[BccList],
		[ElectronicSignatureDocumentKey],
		[Encoding],
		[ContentFormat],
		[ZipFormat],
		[DocumentBlobSize], 
		[EmailBlobSize],
		[DocumentDatabaseID],
		[WhenArchived],
		[DocumentTypeVersionID]
	FROM
		dbo.vLeadDocumentList WITH (NOLOCK) 
	WHERE
		[LeadID] = @LeadID
		AND [DocumentTypeID] = @DocumentTypeID
	
	SELECT @@ROWCOUNT
					
END			
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadIDDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_GetByLeadIDDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadIDDocumentTypeID] TO [sp_executeall]
GO
