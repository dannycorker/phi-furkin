SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-24
-- Description:	Copy all editing records back to the real Sql Query tables
-- JWG 2012-08-14 Added new columns Comparison & IsJoinClause
-- JW  2013-05-08 Added new column ComplexValue - used to support CASE function
-- =============================================
CREATE PROCEDURE [dbo].[CommitSqlQueryForEditing]
	@SqlQueryID int = 0,
	@SqlQueryEditingID int,
	@NewSqlQueryID int
AS
BEGIN
	SET NOCOUNT ON;

	/*
		JWG 2010-09-10 
		Prevent double-saves from wiping out all the editing tables.
		Only replace the real tables with the editing tables the first time.
	*/
	IF EXISTS (SELECT * FROM SqlQueryEditing WHERE SqlQueryEditingID = @SqlQueryEditingID)
	BEGIN
	
		/* Delete existing "real" data to make way for the new records */
		IF @SqlQueryID > 0
		BEGIN
			DELETE SqlQueryGrouping WHERE SqlQueryID = @SqlQueryID
			DELETE SqlQueryCriteria WHERE SqlQueryID = @SqlQueryID
			DELETE SqlQueryColumns WHERE SqlQueryID = @SqlQueryID
			DELETE SqlQueryTables WHERE SqlQueryID = @SqlQueryID
		END
		
		-- If a NEW or COPY query has taken place then a new SqlQuery record 
		-- will have been created within ReportBuilder.aspx by now.  We need
		-- to tell the Editing table about this new ID
		IF @NewSqlQueryID IS NOT NULL
		BEGIN
			IF @NewSqlQueryID <> @SqlQueryID
			BEGIN
				UPDATE SqlQueryEditing 
				SET SqlQueryID = @NewSqlQueryID
				WHERE SqlQueryEditingID = @SqlQueryEditingID
				--AND SqlQueryID IS NULL

				SET @SqlQueryID = @NewSqlQueryID
			END
		END

		-- Create the new real Tables
		INSERT INTO SqlQueryTables
		(ClientID, SqlQueryID, SqlQueryTableName, TableAlias, TableDisplayOrder,
		JoinType, JoinText, JoinTableID, JoinRTRID, TempTableID, TempJoinTableID)
		SELECT
		sqet.ClientID, @SqlQueryID, sqet.SqlQueryTableName, sqet.TableAlias, sqet.TableDisplayOrder,
		sqet.JoinType, sqet.JoinText, NULL, sqet.JoinRTRID, sqet.SqlQueryEditingTableID, sqet.JoinTableID
		FROM  SqlQueryEditingTable sqet
		INNER JOIN SqlQueryEditing sqe ON sqe.SqlQueryEditingID = sqet.SqlQueryEditingID
		WHERE sqet.SqlQueryEditingID = @SqlQueryEditingID
		ORDER BY SqlQueryEditingTableID
	 
		-- No need for this section.  Join IDs are never used
		/*
		-- Set JoinTableID values from temp to real numbers
		UPDATE SqlQueryTables
		SET JoinTableID = sqt_join.RealID
		FROM SqlQueryTables sqt 
		INNER JOIN (
			SELECT SqlQueryTableID as [RealID], TempTableID as [TempID], TempJoinTableID as [TempJoinID]
			FROM SqlQueryTables  
			WHERE SqlQueryID = 190
		) as sqt_join
		ON sqt.TempTableID = sqt_join.TempJoinID
		WHERE sqt.SqlQueryID = 190*/
		
		
		-- Create the new real Columns
		INSERT INTO SqlQueryColumns
		(ClientID, SqlQueryID, SqlQueryTableID, CompleteOutputText, ColumnAlias,
		ShowColumn, DisplayOrder, SortOrder, SortType, TempColumnID,  
		IsAggregate, ColumnDataType, ManipulatedDataType, ColumnNaturalName, ComplexValue)
		SELECT
		sqec.ClientID, @SqlQueryID, sqt.SqlQueryTableID, sqec.CompleteOutputText, sqec.ColumnAlias, 
		sqec.ShowColumn, sqec.DisplayOrder, sqec.SortOrder, sqec.SortType, sqec.SqlQueryEditingColumnID, 
		sqec.IsAggregate, sqec.ColumnDataType, sqec.ManipulatedDataType, sqec.ColumnNaturalName, sqec.ComplexValue
		FROM SqlQueryEditingColumns sqec
		INNER JOIN SqlQueryEditing sqe ON sqe.SqlQueryEditingID = sqec.SqlQueryEditingID
		LEFT JOIN SqlQueryTables sqt ON sqt.SqlQueryID = sqe.SqlQueryID AND sqt.TempTableID = sqec.SqlQueryEditingTableID
		WHERE sqec.SqlQueryEditingID = @SqlQueryEditingID 
		ORDER BY SqlQueryEditingTableID, SqlQueryEditingColumnID

		-- Create the new real Criteria
		INSERT INTO SqlQueryCriteria
		(ClientID, SqlQueryID, SqlQueryTableID, SqlQueryColumnID,
		CriteriaText, Criteria1, Criteria2, CriteriaName, SubQueryID, SubQueryLinkType,
		ParamValue, IsSecurityClause, CriteriaSubstitutions, IsParameterizable, Comparison, IsJoinClause)
		SELECT
		sqec.ClientID, @SqlQueryID, sqt.SqlQueryTableID, sqc.SqlQueryColumnID,
		sqec.CriteriaText, sqec.Criteria1, sqec.Criteria2, sqec.CriteriaName, sqec.SubQueryID, sqec.SubQueryLinkType,
		sqec.ParamValue, sqec.IsSecurityClause, sqec.CriteriaSubstitutions, sqec.IsParameterizable, sqec.Comparison, sqec.IsJoinClause
		FROM SqlQueryEditingCriteria sqec
		INNER JOIN SqlQueryEditing sqe ON sqe.SqlQueryEditingID = sqec.SqlQueryEditingID
		LEFT JOIN SqlQueryTables sqt ON sqt.SqlQueryID = sqe.SqlQueryID AND sqt.TempTableID = sqec.SqlQueryEditingTableID 
		LEFT JOIN SqlQueryColumns sqc ON sqc.SqlQueryID = sqe.SqlQueryID AND sqc.TempColumnID = sqec.SqlQueryEditingColumnID 
		WHERE sqec.SqlQueryEditingID = @SqlQueryEditingID 
		ORDER BY SqlQueryEditingTableID, SqlQueryEditingColumnID

		-- Create the new real Group By /Having clause
		INSERT INTO SqlQueryGrouping
		(ClientID, SqlQueryID, GroupByClause, HavingClause,
		HavingColumnID, HavingCriteria1, HavingCriteria2)
		SELECT
		sqeg.ClientID, @SqlQueryID, sqeg.GroupByClause, sqeg.HavingClause,
		sqc.SqlQueryColumnID, sqeg.HavingCriteria1, sqeg.HavingCriteria2
		FROM SqlQueryEditingGrouping sqeg
		INNER JOIN SqlQueryEditing sqe ON sqe.SqlQueryEditingID = sqeg.SqlQueryEditingID 
		INNER JOIN SqlQueryColumns sqc ON sqc.SqlQueryID = sqe.SqlQueryID AND sqc.TempColumnID = sqeg.HavingColumnID 
		WHERE sqeg.SqlQueryEditingID = @SqlQueryEditingID 

		-- Clear out any temp values to avoid confusion later on
		UPDATE SqlQueryTables 
		SET TempTableID = NULL,
		TempJoinTableID = NULL 
		WHERE SqlQueryID = @SqlQueryID 

		UPDATE SqlQueryColumns 
		SET TempTableID = NULL,
		TempColumnID = NULL
		WHERE SqlQueryID = @SqlQueryID 

		UPDATE SqlQueryCriteria 
		SET TempTableID = NULL,
		TempColumnID = NULL,
		TempCriteriaID = NULL
		WHERE SqlQueryID = @SqlQueryID 

		UPDATE SqlQueryGrouping 
		SET TempGroupingID = NULL,
		TempHavingColumnID = NULL,
		TempHavingID = NULL
		WHERE SqlQueryID = @SqlQueryID 

		DELETE SqlQueryEditing WHERE SqlQueryEditingID = @SqlQueryEditingID
	
	END
	
END















GO
GRANT VIEW DEFINITION ON  [dbo].[CommitSqlQueryForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CommitSqlQueryForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CommitSqlQueryForEditing] TO [sp_executeall]
GO
