SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget_Update]
(

	@DocumentDetailFieldTargetID int   ,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@Notes varchar (250)  ,

	@ExcelSheetLocation varchar (50)  ,

	@Format varchar (2000)  ,

	@DocumentTypeVersionID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentDetailFieldTarget]
				SET
					[ClientID] = @ClientID
					,[DocumentTypeID] = @DocumentTypeID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[Notes] = @Notes
					,[ExcelSheetLocation] = @ExcelSheetLocation
					,[Format] = @Format
					,[DocumentTypeVersionID] = @DocumentTypeVersionID
				WHERE
[DocumentDetailFieldTargetID] = @DocumentDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Update] TO [sp_executeall]
GO
