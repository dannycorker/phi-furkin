SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-05-19
-- Description:	Full delete data.
--				Customer/Lead/Case/Matter
-- =============================================
CREATE PROCEDURE [dbo].[FullDeleteDataForDPA]

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @LastRowCount INT,
			@LogXmlEntry VARCHAR(MAX)
	
	DECLARE @TableOfIDs TABLE (ArchivedObjectID INT)
	
	DECLARE @LogOutput TABLE (UNID INT IDENTITY (1,1), LogArea VARCHAR(50), LogResult INT, LogDateTime DATETIME)

	INSERT INTO @TableOfIDs (ArchivedObjectID)
	SELECT a.ArchivedObjectID
	FROM ArchivedObjects a
	WHERE a.DateForDeletion < dbo.fn_GetDate_Local()
	AND a.Status = 1

	/*
		Here we will perminantly remove any data that has gone past the deletion date in Archived objects.
	*/
	DELETE IncomingPostEvent 
	FROM IncomingPostEvent 
	INNER JOIN ArchivedObjects a ON (IncomingPostEvent.LeadID = ISNULL(a.NewLeadID, a.LeadID) OR IncomingPostEvent.CaseID = ISNULL(a.NewCaseID, a.CaseID) OR IncomingPostEvent.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID) OR a.MatterID = IncomingPostEvent.MatterID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE IncomingPostEvent.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('IncomingPostEvent', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE IncomingPostEventValue 
	FROM IncomingPostEventValue 
	INNER JOIN ArchivedObjects a ON (IncomingPostEventValue.LeadID = ISNULL(a.NewLeadID, a.LeadID) OR IncomingPostEventValue.CaseID = ISNULL(a.NewCaseID, a.CaseID) OR IncomingPostEventValue.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID) OR a.MatterID = IncomingPostEventValue.MatterID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE IncomingPostEventValue.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('IncomingPostEventValue', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerQuestionnaires 
	FROM CustomerQuestionnaires
	INNER JOIN ArchivedObjects a ON (ISNULL(a.NewCustomerID,a.CustomerID) = CustomerQuestionnaires.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerQuestionnaires.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerQuestionnaires', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE WorkflowTask
	FROM WorkflowTask
	INNER JOIN ArchivedObjects a ON (ISNULL(a.NewLeadID,a.LeadID) = WorkflowTask.LeadID or ISNULL(a.NewCaseID,a.CaseID) = WorkflowTask.CaseID) 
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE WorkflowTask.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('WorkflowTask', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE Department
	FROM Department
	INNER JOIN ArchivedObjects a ON ISNULL(a.NewCustomerID, a.CustomerID) = Department.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Department.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Department', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DroppedOutCustomers
	FROM DroppedOutCustomers
	INNER JOIN ArchivedObjects a ON ISNULL(a.NewCustomerID, a.CustomerID) = DroppedOutCustomers.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DroppedOutCustomers.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DroppedOutCustomers', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE Bill
	FROM Bill
	INNER JOIN ArchivedObjects a ON (Bill.CustomerID = ISNULL(a.NewCustomerID,a.CustomerID) OR ISNULL(a.NewLeadID,a.LeadID) = Bill.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = Bill.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Bill.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Bill', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE WorkflowTaskCompleted
	FROM WorkflowTaskCompleted
	INNER JOIN ArchivedObjects a ON (ISNULL(a.NewLeadID,a.LeadID) = WorkflowTaskCompleted.LeadID OR WorkflowTaskCompleted.CaseID = ISNULL(a.NewCaseID,a.CaseID)) 
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE WorkflowTaskCompleted.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('WorkflowTaskCompleted', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValuesHistory
	FROM TableDetailValuesHistory
	INNER JOIN ArchivedObjects a ON (ISNULL(a.NewCustomerID,a.CustomerID) = TableDetailValuesHistory.CustomerID) 
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValuesHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValuesHistory (Customer)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValuesHistory
	FROM TableDetailValuesHistory
	INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.LeadID = ISNULL(a.NewLeadID,a.LeadID) AND (TableDetailValuesHistory.MatterID IS NULL OR TableDetailValuesHistory.MatterID = 0))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValuesHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValuesHistory (Lead)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValuesHistory
	FROM TableDetailValuesHistory
	INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.CaseID = ISNULL(a.NewCaseID,a.CaseID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValuesHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValuesHistory (Case)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValuesHistory
	FROM TableDetailValuesHistory
	INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.MatterID = a.MatterID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValuesHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValuesHistory (Matter)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValues
	FROM TableDetailValues 
	INNER JOIN ArchivedObjects a ON (TableDetailValues.CustomerID = ISNULL(a.NewCustomerID,a.CustomerID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValues.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValues (Customer)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValues
	FROM TableDetailValues
	INNER JOIN ArchivedObjects a ON (TableDetailValues.LeadID = ISNULL(a.NewLeadID,a.LeadID) AND (TableDetailValues.MatterID IS NULL OR TableDetailValues.MatterID = 0))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValues.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValues (Lead)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValues
	FROM TableDetailValues
	INNER JOIN ArchivedObjects a ON (TableDetailValues.CaseID = ISNULL(a.NewCaseID,a.CaseID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValues.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValues (Case)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableDetailValues
	FROM TableDetailValues
	INNER JOIN ArchivedObjects a ON (TableDetailValues.MatterID = a.MatterID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableDetailValues.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableDetailValues (Matter)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableRows
	FROM TableRows 
	INNER JOIN ArchivedObjects a ON (ISNULL(a.NewCustomerID,a.CustomerID) = TableRows.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableRows.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableRows (Customer)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableRows
	FROM TableRows 
	INNER JOIN ArchivedObjects a ON ISNULL(a.NewLeadID,a.LeadID) = TableRows.LeadID AND (TableRows.MatterID = 0 OR TableRows.MatterID IS NULL)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableRows.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableRows (Lead)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableRows
	FROM TableRows 
	INNER JOIN ArchivedObjects a ON ISNULL(a.NewCaseID,a.CaseID) = TableRows.CaseID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableRows.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableRows (Case)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE TableRows
	FROM TableRows 
	INNER JOIN ArchivedObjects a ON a.MatterID = TableRows.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE TableRows.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('TableRows (Matter)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE RPIClaimStatus
	FROM RPIClaimStatus
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON RPIClaimStatus.MatterID = a.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE RPIClaimStatus.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('RPIClaimStatus', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE QuillCustomerMapping
	FROM QuillCustomerMapping
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON QuillCustomerMapping.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE QuillCustomerMapping.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('QuillCustomerMapping', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE QuillCaseMapping
	FROM QuillCaseMapping
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON QuillCaseMapping.CaseID = ISNULL(a.NewCaseID,a.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE QuillCaseMapping.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('QuillCaseMapping', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE OutcomeDelayedEmails
	FROM OutcomeDelayedEmails 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON OutcomeDelayedEmails.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE OutcomeDelayedEmails.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('OutcomeDelayedEmails', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE PartnerMatchKey
	FROM PartnerMatchKey 
	INNER JOIN Partner p ON p.PartnerID = PartnerMatchKey.PartnerID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON p.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE p.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('PartnerMatchKey', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE Partner
	FROM Partner 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON Partner.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Partner.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Partner', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE po
	FROM PortalUserOption po 
	INNER JOIN PortalUser ON PortalUser.PortalUserID = po.PortalUserID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON PortalUser.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID 
	WHERE PortalUser.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('PortalUserOption', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	-- Set context info to allow access to table
    DECLARE @ContextInfo VARBINARY(100) = CAST('DPADelete' AS VARBINARY)
    SET CONTEXT_INFO @ContextInfo

	DELETE PortalUser
	FROM PortalUser
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON PortalUser.CustomerID = ISNULL(a.NewCustomerID, a.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID 
	WHERE PortalUser.ClientID = 20
	
	SET CONTEXT_INFO 0x0

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('PortalUser', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE MatterPageChoice 
	FROM MatterPageChoice
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = MatterPageChoice.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE MatterPageChoice.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('MatterPageChoice', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	/*DELETE Matterdetailvalues.*/
	DELETE mdv
	FROM MatterDetailValues mdv
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = mdv.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE mdv.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('MatterDetailValues', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadViewHistoryArchive
	FROM LeadViewHistoryArchive
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = LeadViewHistoryArchive.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadViewHistoryArchive.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadViewHistoryArchive', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadViewHistory
	FROM LeadViewHistory
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = LeadViewHistory.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadViewHistory.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadViewHistory', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadDetailValues
	FROM LeadDetailValues
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = LeadDetailValues.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadDetailValues.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadDetailValues', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadAssignment 
	FROM LeadAssignment
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = LeadAssignment.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadAssignment.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadAssignment', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE HskLeadEventToDelete
	FROM HskLeadEventToDelete
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = HskLeadEventToDelete.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = HskLeadEventToDelete.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE HskLeadEventToDelete.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('HskLeadEventToDelete', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE FileExportEvent
	FROM FileExportEvent
	INNER JOIN LeadEvent le ON le.LeadEventID = FileExportEvent.LeadEventID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = le.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = le.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE FileExportEvent.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('FileExportEvent', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadDocumentFS
	FROM LeadDocumentFS
	INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentFS.LeadDocumentID
	INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = le.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = le.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadDocumentFS.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadDocumentFS', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadDocumentEsignatureStatus 
	FROM LeadDocumentEsignatureStatus 
	INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentEsignatureStatus.LeadDocumentID
	INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = le.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = le.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadDocumentEsignatureStatus.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadDocumentEsignatureStatus', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadEventThreadCompletion
	FROM LeadEventThreadCompletion
	INNER JOIN LeadEvent le ON le.LeadEventID = LeadEventThreadCompletion.FromLeadEventID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = le.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = le.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadEventThreadCompletion.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadEventThreadCompletion', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE LeadEvent 
	FROM LeadEvent 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = LeadEvent.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = LeadEvent.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadEvent.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('LeadEvent', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DroppedOutCustomerAnswers
	FROM DroppedOutCustomerAnswers
	INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = DroppedOutCustomerAnswers.CustomerQuestionnaireID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewCustomerID,a.CustomerID) = cq.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DroppedOutCustomerAnswers.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DroppedOutCustomerAnswers', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DroppedOutCustomerQuestionnaires
	FROM DroppedOutCustomerQuestionnaires
	INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = DroppedOutCustomerQuestionnaires.CustomerQuestionnaireID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewCustomerID,a.CustomerID) = cq.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DroppedOutCustomerQuestionnaires.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DroppedOutCustomerQuestionnaires', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DocumentBatchDetail
	FROM DocumentBatchDetail
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DocumentBatchDetail.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DocumentBatchDetail.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DocumentBatchDetail', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DiaryReminder
	FROM DiaryReminder
	INNER JOIN DiaryAppointment d on d.DiaryAppointmentID = DiaryReminder.DiaryAppointmentID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = d.LeadID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DiaryReminder.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DiaryReminder', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DiaryAppointment 
	FROM DiaryAppointment 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewLeadID,a.LeadID) = DiaryAppointment.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = DiaryAppointment.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DiaryAppointment.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DiaryAppointment', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailValueHistory
	FROM DetailValueHistory
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.CustomerID,a.CustomerID) = DetailValueHistory.CustomerID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DetailValueHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailValueHistory (Customer)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailValueHistory
	FROM DetailValueHistory
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = DetailValueHistory.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DetailValueHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailValueHistory (Lead)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailValueHistory
	FROM DetailValueHistory
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCaseID,a.CaseID) = DetailValueHistory.CaseID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DetailValueHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailValueHistory (Case)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailValueHistory
	FROM DetailValueHistory
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailValueHistory.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE DetailValueHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailValueHistory (Matter)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailFieldLock 
	FROM DetailFieldLock
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = DetailFieldLock.LeadOrMatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadOrMatter = 10
	AND DetailFieldLock.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailFieldLock (Customer)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailFieldLock 
	FROM DetailFieldLock
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = DetailFieldLock.LeadOrMatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadOrMatter = 1
	AND DetailFieldLock.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailFieldLock (Lead)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailFieldLock 
	FROM DetailFieldLock
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCaseID,a.CaseID) = DetailFieldLock.LeadOrMatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadOrMatter = 11
	AND DetailFieldLock.ClientID = 20
		
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailFieldLock (Case)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE DetailFieldLock 
	FROM DetailFieldLock
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailFieldLock.LeadOrMatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE LeadOrMatter = 2
	AND DetailFieldLock.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('DetailFieldLock (Matter)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerOutcomes
	FROM CustomerOutcomes 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = CustomerOutcomes.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerOutcomes.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerOutcomes', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	UPDATE Customers
	SET DefaultOfficeID = NULL, DefaultContactID = NULL
	FROM Customers
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = Customers.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Customers.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerOffice (Remove From Customer)', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerOffice
	FROM CustomerOffice
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = CustomerOffice.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerOffice.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerOffice', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerMatchKey
	FROM CustomerMatchKey
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = CustomerMatchKey.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerMatchKey.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerMatchKey', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerHistory
	FROM CustomerHistory
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = CustomerHistory.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerHistory.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerHistory', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerDetailValues
	FROM CustomerDetailValues 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = CustomerDetailValues.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerDetailValues.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerDetailValues', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE CustomerAnswers
	FROM CustomerAnswers
	INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = CustomerAnswers.CustomerQuestionnaireID
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = cq.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE CustomerAnswers.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CustomerAnswers', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE ContactDetailValues
	FROM ContactDetailValues
	INNER JOIN Contact c on c.ContactID = ContactDetailValues.ContactID 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = c.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE ContactDetailValues.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('ContactDetailValues', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE c
	FROM Contact c 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = c.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE c.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Contact', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE col
	FROM ClientOfficeLead col
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = col.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE col.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('ClientOfficeLead', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE cpf
	FROM ClientPersonnelFavourite cpf
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewLeadID,a.LeadID) = cpf.LeadID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE cpf.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('ClientPersonnelFavourite', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE ClientCustomerMails
	FROM ClientCustomerMails
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID,a.CustomerID) = ClientCustomerMails.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE ClientCustomerMails.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('ClientCustomerMails', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE ctm
	FROM CaseTransferMapping ctm
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (ISNULL(a.NewCustomerID,a.CustomerID) = ctm.CustomerID OR ctm.LeadID = ISNULL(a.NewLeadID,a.LeadID) OR ctm.CaseID = ISNULL(a.NewCaseID,a.CaseID) OR ctm.MatterID = a.MatterID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE ctm.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CaseTransferMapping', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE cdv
	FROM CaseDetailValues cdv WITH (NOLOCK)
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCaseID,a.CaseID) = cdv.CaseID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE cdv.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('CaseDetailValues', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE aeq
	FROM AutomatedEventQueue aeq
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (aeq.CustomerID = ISNULL(a.NewCustomerID,a.CustomerID) OR aeq.LeadID = ISNULL(a.NewLeadID,a.LeadID) OR aeq.CaseID = ISNULL(a.NewCaseID,a.CaseID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE aeq.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('AutomatedEventQueue', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE puc
	FROM PortalUserCase puc
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (puc.LeadID = ISNULL(a.NewLeadID,a.LeadID) OR puc.CaseID = ISNULL(a.NewCaseID, a.CaseID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE puc.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('PortalUserCase', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE cpa
	FROM ClientPersonnelAccess cpa
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (cpa.LeadID = ISNULL(a.NewLeadID,a.LeadID) OR cpa.CaseID = ISNULL(a.NewCaseID, a.CaseID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE cpa.ClientID = 20
	
	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('PortalUserCase', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE Matter
	FROM Matter
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = Matter.MatterID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Matter.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Matter', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE Cases
	FROM Cases 
	INNER JOIN ArchivedObjects a ON (ISNULL(a.NewLeadID,a.LeadID) = Cases.LeadID OR ISNULL(a.NewCaseID,a.CaseID) = Cases.CaseID)
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Cases.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Cases', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	DELETE l
	FROM Lead l 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (l.LeadID = ISNULL(a.NewLeadID,a.LeadID))
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE l.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Lead', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL
	
	UPDATE ClientPersonnel
	SET CustomerID = NULL
	FROM ClientPersonnel 
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = ClientPersonnel.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID

	-- Set context info to allow access to table
    SELECT @ContextInfo = CAST('DPADelete' AS VARBINARY)
    SET CONTEXT_INFO @ContextInfo

	DELETE Customers
	FROM Customers
	INNER JOIN ArchivedObjects a WITH (NOLOCK) ON ISNULL(a.NewCustomerID, a.CustomerID) = Customers.CustomerID
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE Customers.ClientID = 20

	SELECT @LastRowCount = @@ROWCOUNT
	
	SET CONTEXT_INFO 0x0
	
	INSERT INTO @LogOutput (LogArea, LogResult, LogDateTime)
	VALUES ('Customers', @LastRowCount, CURRENT_TIMESTAMP)
	
	SELECT @LastRowCount = NULL

	UPDATE a
	SET DateDeleted = dbo.fn_GetDate_Local(), Status = 3
	FROM ArchivedObjects a 
	INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID
	WHERE a.DateForDeletion < dbo.fn_GetDate_Local()
	
	SELECT *
	FROM @LogOutput

	SELECT @LogXmlEntry = 
	(
		SELECT 	CONVERT(VARCHAR,dbo.fn_GetDate_Local(),120) [DateTime],
				SUSER_NAME() AS [UsersName], 
					CAST ( ( SELECT * FROM @LogOutput FOR XML RAW ('CaseList') ) as XML )
		FOR XML RAW ('Values')
	)

	SELECT @LogXmlEntry = @LogXmlEntry + 
	(
		SELECT 	CONVERT(VARCHAR,dbo.fn_GetDate_Local(),120) [DateTime],
				SUSER_NAME() AS [UsersName], 
					CAST ( ( SELECT a.* FROM ArchivedObjects a INNER JOIN @TableOfIDs t ON t.ArchivedObjectID = a.ArchivedObjectID FOR XML RAW ('CaseList') ) as XML )
		FOR XML RAW ('Values')
	)

	INSERT INTO LogXML(ClientID, LogDateTime, ContextID, ContextVarchar, LogXMLEntry)
	VALUES (20, dbo.fn_GetDate_Local(), 0, 'DPADelete', CAST(@LogXmlEntry AS XML))
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[FullDeleteDataForDPA] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FullDeleteDataForDPA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FullDeleteDataForDPA] TO [sp_executeall]
GO
