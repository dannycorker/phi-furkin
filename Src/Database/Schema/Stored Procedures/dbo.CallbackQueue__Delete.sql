SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/04/2016
-- Description:	Deletes a member of the callback queue
-- =============================================
CREATE PROCEDURE [dbo].[CallbackQueue__Delete]
	@CallbackQueueID INT,
	@ClientID INT 	
AS
BEGIN
		
	SET NOCOUNT ON;
	
	DELETE FROM CallbackQueue
	WHERE CallbackQueueID=@CallbackQueueID AND ClientID=@ClientID    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[CallbackQueue__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CallbackQueue__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CallbackQueue__Delete] TO [sp_executeall]
GO
