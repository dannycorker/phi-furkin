SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeInstance table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeInstance_GetByChangeInstanceID]
(

	@ChangeInstanceID int   
)
AS


				SELECT
					[ChangeInstanceID],
					[ClientID],
					[ChangeDefinitionID],
					[ChangeTypeID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ChangeInstance] WITH (NOLOCK) 
				WHERE
										[ChangeInstanceID] = @ChangeInstanceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_GetByChangeInstanceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeInstance_GetByChangeInstanceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_GetByChangeInstanceID] TO [sp_executeall]
GO
