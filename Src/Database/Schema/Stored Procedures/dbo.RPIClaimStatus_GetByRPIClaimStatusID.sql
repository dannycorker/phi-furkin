SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIClaimStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_GetByRPIClaimStatusID]
(

	@RPIClaimStatusID int   
)
AS


				SELECT
					[RPIClaimStatusID],
					[MatterID],
					[ClientID],
					[ApplicationID],
					[ActivityEngineGuid],
					[PhaseCacheID],
					[PhaseCacheName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[RPIAsUser],
					[RPIUser],
					[RPIPass],
					[Notes]
				FROM
					[dbo].[RPIClaimStatus] WITH (NOLOCK) 
				WHERE
										[RPIClaimStatusID] = @RPIClaimStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_GetByRPIClaimStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_GetByRPIClaimStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_GetByRPIClaimStatusID] TO [sp_executeall]
GO
