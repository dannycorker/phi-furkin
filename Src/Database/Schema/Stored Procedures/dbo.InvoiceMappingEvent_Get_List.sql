SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the InvoiceMappingEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_Get_List]

AS


				
				SELECT
					[InvoiceMappingEventID],
					[ClientID],
					[LeadTypeID],
					[EventTypeID],
					[Type]
				FROM
					[dbo].[InvoiceMappingEvent] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Get_List] TO [sp_executeall]
GO
