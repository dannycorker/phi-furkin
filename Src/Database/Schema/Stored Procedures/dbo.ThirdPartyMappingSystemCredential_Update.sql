SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyMappingSystemCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential_Update]
(

	@ThirdPartyMappingSystemCredentialID int   ,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@ConsumerKey varchar (500)  ,

	@ConsumerSecret varchar (500)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyMappingSystemCredential]
				SET
					[ThirdPartySystemID] = @ThirdPartySystemID
					,[ClientID] = @ClientID
					,[ConsumerKey] = @ConsumerKey
					,[ConsumerSecret] = @ConsumerSecret
				WHERE
[ThirdPartyMappingSystemCredentialID] = @ThirdPartyMappingSystemCredentialID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Update] TO [sp_executeall]
GO
