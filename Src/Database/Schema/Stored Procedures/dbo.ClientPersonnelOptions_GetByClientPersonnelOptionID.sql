SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelOptions table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_GetByClientPersonnelOptionID]
(

	@ClientPersonnelOptionID int   
)
AS


				SELECT
					[ClientPersonnelOptionID],
					[ClientPersonnelID],
					[ClientPersonnelOptionTypeID],
					[OptionValue],
					[ClientID]
				FROM
					[dbo].[ClientPersonnelOptions] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelOptionID] = @ClientPersonnelOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_GetByClientPersonnelOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_GetByClientPersonnelOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_GetByClientPersonnelOptionID] TO [sp_executeall]
GO
