SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-03-09
-- Description:	Get Events for custom control
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetThirdPartyEventForCustomControl]
	@LeadID INT,
	@EventName VARCHAR(20)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT e.EventTypeID
	FROM Lead l WITH (NOLOCK)
	INNER JOIN ThirdPartySystemEvent e WITH (NOLOCK) ON e.LeadTypeID = l.LeadTypeID AND e.MessageName = @EventName
	WHERE l.LeadID = @LeadID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetThirdPartyEventForCustomControl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetThirdPartyEventForCustomControl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetThirdPartyEventForCustomControl] TO [sp_executeall]
GO
