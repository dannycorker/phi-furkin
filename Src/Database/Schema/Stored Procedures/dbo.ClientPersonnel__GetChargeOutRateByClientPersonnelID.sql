SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnel table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel__GetChargeOutRateByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


		SELECT top 1
			[ChargeOutRate]
		FROM
			[dbo].[ClientPersonnel]
		WHERE
			[ClientPersonnelID] = @ClientPersonnelID

		Select @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetChargeOutRateByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetChargeOutRateByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetChargeOutRateByClientPersonnelID] TO [sp_executeall]
GO
