SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-06-26
-- Description:	Insert into OncIncRequest and OncIncResponse from the IQB
-- =============================================
CREATE PROCEDURE [dbo].[OneIncRequestAndResponse__Insert]
	@OneIncRequestID INT = 0,
	@ClientID INT,
	@CustomerID INT = 0,
	@MatterID INT = NULL,
	@Title INT = NULL,
	@Firstname VARCHAR(100) = NULL,
	@Lastname VARCHAR(100) = NULL,
	@PostCode VARCHAR(10) = NULL,
	@Email VARCHAR(250) = NULL,
	@RequestJson NVARCHAR(MAX),
	@ResponseJson NVARCHAR(MAX),
	@WhenCreated DATETIME,
    @WhoCreated INT

AS
BEGIN

	SET NOCOUNT ON;


	--IF ISNULL(@CustomerID,0) = 0 
	--BEGIN
	--	INSERT dbo.Customers (ClientID, TitleID, FirstName, LastName, PostCode, EmailAddress, IsBusiness)	
	--	VALUES (@ClientID, @Title, @Firstname, @Lastname, ISNULL(@PostCode, ''), ISNULL(@Email,''), 0)				
	--	SELECT @CustomerID = SCOPE_IDENTITY()
	--END

	IF @OneIncRequestID  = 0
	BEGIN	

		INSERT INTO OneIncRequest ([ClientID],[CustomerID],[MatterID],[RequestJson], [WhenCreated],[WhoCreated])
		VALUES (@ClientID, @CustomerID, ISNULL(@MatterID,0), @RequestJson, @WhenCreated, @WhoCreated)
     
		SELECT @OneIncRequestID = SCOPE_IDENTITY()

		INSERT INTO [OneIncResponse] ([OneIncRequestID],[ClientID],[CustomerID],[MatterID],[ResponseJson],[WhenCreated],[WhoCreated])
		VALUES (@OneIncRequestID, @ClientID, @CustomerID, ISNULL(@MatterID,0), @ResponseJson, @WhenCreated, @WhoCreated)

	END
    ELSE
	BEGIN

		UPDATE request
		SET RequestJson = @RequestJson, WhenCreated = @WhenCreated, WhoCreated = @WhoCreated
		FROM OneIncRequest request WITH (NOLOCK)
		WHERE request.OneIncRequestID = @OneIncRequestID

		UPDATE response
		SET ResponseJson = @ResponseJson, WhenCreated = @WhenCreated, WhoCreated = @WhoCreated
		FROM OneIncResponse response WITH (NOLOCK)
		WHERE response.OneIncRequestID = @OneIncRequestID

	END

	SELECT @OneIncRequestID AS OneIncRequestID, @CustomerID AS CustomerID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[OneIncRequestAndResponse__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OneIncRequestAndResponse__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OneIncRequestAndResponse__Insert] TO [sp_executeall]
GO
