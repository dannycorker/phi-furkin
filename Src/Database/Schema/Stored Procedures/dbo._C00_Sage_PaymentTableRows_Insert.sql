SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-06-27
-- Description:	Inserts a payment row for the given customer 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_PaymentTableRows_Insert]
(	
	@ClientID INT,
	@LeadID INT,
	@InvoiceNumber varchar(max),	
	@DatePaid varchar(max),	
	@Amount varchar(max)
)
AS
BEGIN
	DECLARE @CustomerID INT,
			@PaymentTableDetailFieldID INT,
			@PaymentInvoiceNumberDetailFieldID INT,
			@PaymentDatePaidDetailFieldID INT,
			@PaymentAmountDetailFieldID INT,
			@PaymentPageID INT,
			@NewTableRowID INT
		
	SELECT @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID AND ClientID=@ClientID
	
	SELECT @PaymentTableDetailFieldID=tpfm.DetailFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=206
	
	SELECT @PaymentPageID=DetailFieldPageID FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @PaymentTableDetailFieldID

	SELECT @PaymentInvoiceNumberDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=207
	
	SELECT @PaymentDatePaidDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=208

	SELECT @PaymentAmountDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=209
			
	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
	VALUES (@ClientID, @CustomerID, @PaymentTableDetailFieldID, @PaymentPageID, 1, 1)
		
	SELECT @NewTableRowID = SCOPE_IDENTITY()
			
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	VALUES  (@ClientID, @CustomerID, @NewTableRowID, @PaymentInvoiceNumberDetailFieldID, @InvoiceNumber),
			(@ClientID, @CustomerID, @NewTableRowID, @PaymentDatePaidDetailFieldID, @DatePaid),
			(@ClientID, @CustomerID, @NewTableRowID, @PaymentAmountDetailFieldID, @Amount)
			
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_PaymentTableRows_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_PaymentTableRows_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_PaymentTableRows_Insert] TO [sp_executeall]
GO
