SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetCustomerFieldDefinitions]

@ClientQuestionnaireID int

AS

Select CustomerInformationFieldDefinitionID, ClientQuestionnaireID, FieldAtBeginningOfQuestionnaire, OrdinalPosition, FieldName, FieldLabel, Mandatory, VerifyPhoneNumber, ClientID 
From CustomerInformationFieldDefinitions
Where ClientQuestionnaireID = @ClientQuestionnaireID





GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerFieldDefinitions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerFieldDefinitions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerFieldDefinitions] TO [sp_executeall]
GO
