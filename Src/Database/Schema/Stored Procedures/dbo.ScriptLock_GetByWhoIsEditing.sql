SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptLock table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_GetByWhoIsEditing]
(

	@WhoIsEditing int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptLockID],
					[ScriptID],
					[ClientID],
					[WhoIsEditing],
					[EditStartedAt]
				FROM
					[dbo].[ScriptLock] WITH (NOLOCK) 
				WHERE
					[WhoIsEditing] = @WhoIsEditing
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_GetByWhoIsEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_GetByWhoIsEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_GetByWhoIsEditing] TO [sp_executeall]
GO
