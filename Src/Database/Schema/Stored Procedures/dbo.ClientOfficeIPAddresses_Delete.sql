SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientOfficeIPAddresses table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_Delete]
(

	@ClientOfficeIpAddressID int   
)
AS


				DELETE FROM [dbo].[ClientOfficeIPAddresses] WITH (ROWLOCK) 
				WHERE
					[ClientOfficeIpAddressID] = @ClientOfficeIpAddressID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Delete] TO [sp_executeall]
GO
