SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-04-30
-- Description:	Security Test
-- =============================================
CREATE PROCEDURE [dbo].[SecurityTest]
	@Message varchar(500) = ''
AS
BEGIN
	SET NOCOUNT ON;
	/* Write the optional message to the logs table */
	EXEC dbo._C00_LogIt 'Unauth', 'SecurityTest', 'SecurityTest', @Message, 0
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SecurityTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SecurityTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SecurityTest] TO [sp_executeall]
GO
