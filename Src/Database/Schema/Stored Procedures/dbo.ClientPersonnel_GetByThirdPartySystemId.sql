SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnel table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_GetByThirdPartySystemId]
(

	@ThirdPartySystemId int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPersonnelID],
					[ClientID],
					[ClientOfficeID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[JobTitle],
					[Password],
					[ClientPersonnelAdminGroupID],
					[MobileTelephone],
					[HomeTelephone],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[EmailAddress],
					[ChargeOutRate],
					[UserName],
					[Salt],
					[AttemptedLogins],
					[AccountDisabled],
					[ManagerID],
					[Initials],
					[LanguageID],
					[SubClientID],
					[ForcePasswordChangeOn],
					[ThirdPartySystemId],
					[CustomerID],
					[IsAquarium],
					[AllowSmsCommandProcessing],
					[MemorableWord],
					[MemorableWordSalt],
					[MemorableWordAttempts],
					[PendingActivation]
				FROM
					[dbo].[ClientPersonnel] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemId] = @ThirdPartySystemId
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_GetByThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByThirdPartySystemId] TO [sp_executeall]
GO
