SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUser table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser_GetByUsername]
(

	@Username varchar (50)  
)
AS


				SELECT
					[PortalUserID],
					[ClientID],
					[CustomerID],
					[Username],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[Enabled],
					[AttemptedLogins],
					[PortalUserGroupID]
				FROM
					[dbo].[PortalUser] WITH (NOLOCK) 
				WHERE
										[Username] = @Username
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByUsername] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_GetByUsername] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByUsername] TO [sp_executeall]
GO
