SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SiteMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SiteMap_Update]
(

	@SiteMapID int   ,

	@Url varchar (255)  ,

	@Title varchar (255)  ,

	@Description varchar (255)  ,

	@ParentID int   ,

	@Secured bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SiteMap]
				SET
					[Url] = @Url
					,[Title] = @Title
					,[Description] = @Description
					,[ParentID] = @ParentID
					,[Secured] = @Secured
				WHERE
[SiteMapID] = @SiteMapID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SiteMap_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Update] TO [sp_executeall]
GO
