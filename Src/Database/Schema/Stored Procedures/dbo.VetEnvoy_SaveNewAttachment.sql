SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-24
-- Description:	Saves a new attachment to the db
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_SaveNewAttachment]
(
	@ClientID INT,
	@AccountID INT,
	@ConversationID UNIQUEIDENTIFIER,
	@Bytes VARBINARY(MAX),
	@SenderID UNIQUEIDENTIFIER,
	@HistoryID INT,
	@ContentType VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ID INT
	
	IF NOT EXISTS (SELECT * FROM VetEnvoy_Attachments WHERE ConversationID = @ConversationID)
	BEGIN
	
		INSERT dbo.VetEnvoy_Attachments(ClientID, AccountID, ConversationID, Bytes, Received, SenderID, ContentType)
		VALUES (@ClientID, @AccountID, @ConversationID, @Bytes, dbo.fn_GetDate_Local(), @SenderID, @ContentType) 
		
		SELECT @ID = SCOPE_IDENTITY()
		
	END
	ELSE
	BEGIN
	
		SELECT @ID = AttachmentID
		FROM dbo.VetEnvoy_Attachments WITH (NOLOCK) 
		WHERE ConversationID = @ConversationID
	
	END	
	
	DECLARE @Sql NVARCHAR(2000)
	SELECT @Sql = 'EXEC dbo._C' + CAST(@ClientID AS VARCHAR(50)) + '_VetEnvoy_SaveNewAttachment ' + CAST(@ID AS VARCHAR(50))
	EXEC sp_executesql @Sql
	
	UPDATE dbo.VetEnvoy_ConversationHistory
	SET Actioned = dbo.fn_GetDate_Local()
	WHERE HistoryID = @HistoryID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SaveNewAttachment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_SaveNewAttachment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SaveNewAttachment] TO [sp_executeall]
GO
