SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptStandardTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptStandardTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptStandardTargetID int   = null ,

	@ScriptID int   = null ,

	@ClientID int   = null ,

	@Target varchar (250)  = null ,

	@ObjectName varchar (250)  = null ,

	@PropertyName varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptStandardTargetID]
	, [ScriptID]
	, [ClientID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
    FROM
	[dbo].[ScriptStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptStandardTargetID] = @ScriptStandardTargetID OR @ScriptStandardTargetID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptStandardTargetID]
	, [ScriptID]
	, [ClientID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
    FROM
	[dbo].[ScriptStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptStandardTargetID] = @ScriptStandardTargetID AND @ScriptStandardTargetID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Find] TO [sp_executeall]
GO
