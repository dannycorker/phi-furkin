SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-14
-- Description:	Performance logging generation proc
-- =============================================
CREATE PROCEDURE [dbo].[Perflog_CreateLogging]
(
	@ProcName VARCHAR(128)
)
AS
BEGIN

	DECLARE @ParamList VARCHAR(MAX) = '',
			@StartPoint VARCHAR(MAX) = 'BEGIN',
			@Increment INT = 5,
			@EndPoint VARCHAR(MAX) = 'DNE',
			@EndIncrement INT = 3,
			@TOP VARCHAR(MAX),
			@BOTTOM VARCHAR(MAX),
			@RUN NVARCHAR(MAX),
			@DEFINITION NVARCHAR(MAX),
			@TYPE VARCHAR(2)
	
	/*Find param information used in processing later*/
	SELECT @TYPE = obj.type ,@DEFINITION = mo.definition,@ProcName = SPECIFIC_NAME,@Paramlist += ''''+PARAMETER_NAME + ' = '' + ' +  'ISNULL(CAST('+PARAMETER_NAME+' as VARCHAR(MAX)),''NULL'') + '', '' + '
	FROM INFORMATION_SCHEMA.PARAMETERS p 
	INNER JOIN sys.all_sql_modules mo WITH (NOLOCK) ON OBJECT_NAME(mo.object_id) = p.SPECIFIC_NAME
	INNER JOIN sys.objects obj WITH (NOLOCK) on obj.object_id = mo.object_id
	WHERE p.SPECIFIC_NAME = @ProcName   
	AND p.PARAMETER_MODE = 'IN'
	ORDER BY p.ORDINAL_POSITION
	
	IF CHARINDEX('PerfLog',@DEFINITION,0) > 0
	BEGIN
		
		PRINT 'This proc does not currently support updating procs that already contain perf logging'
		RETURN
		
	END
	ELSE
	BEGIN
		
		IF CHARINDEX('SET NOCOUNT ON',@DEFINITION,0) > 0
		BEGIN
			SET @StartPoint = 'SET NOCOUNT ON'
			SET @Increment = LEN(@StartPoint)
		END
		
		IF CHARINDEX('RETURN',@DEFINITION,0) > 0 AND @TYPE = 'FN'
		BEGIN
			SET @EndPoint = 'NRUTER'
			SET @EndIncrement = LEN(@EndPoint)
		END
		
		IF CHARINDEX('LeadEventID = @LeadEventID',@DEFINITION,0) > 0 AND @ProcName like '%_SAE'
		BEGIN
			SET @StartPoint = 'LeadEventID = @LeadEventID'
			SET @Increment = LEN(@StartPoint)
		END
				
		/*Build up perflog sql based on the params of the proc that has been passed in*/
		SELECT @TOP =  CHAR(13) + CHAR(13) + '/*PERFLOGTOPBEGIN*/' + CHAR(13) +
		+ CHAR(13) + CHAR(9) + 'DECLARE '+ CHAR(9) +' @PerfLogEntry VARCHAR(MAX),'+ CHAR(13) + CHAR(9) + CHAR(9) + CHAR(9) + CHAR(9) +' @PerfLogID INT '+ CHAR(13) + CHAR(13) + CHAR(9) +'SELECT @PerfLogEntry = '+ LEFT(@ParamList,LEN(@ParamList)-8)
		+ CHAR(13) + CHAR(9) + 'EXEC @PerfLogID = PerfLogIt '''+@ProcName+''', @ClientPersonnelID, @PerfLogEntry'
		+ CHAR(13)+ CHAR(13) +
		'/*PERFLOGTOPEND*/'
		+ CHAR(13) 

		SELECT @BOTTOM =  CHAR(13) + '/*PERFLOGBOTTOMBEGIN*/' + CHAR(13) + 
		+ CHAR(13) + CHAR(9) + 'EXEC PerfLogIt '''+@ProcName+''', @ClientPersonnelID, NULL, @PerfLogID'
		+ CHAR(13) + CHAR(13) + 
		'/*PERFLOGBOTTOMEND*/'
		+ CHAR(13) 
		
		/*Some older procs used @WhoCreated instead of @ClientPersonnelID*/
		IF CHARINDEX('@WhoCreated',@DEFINITION,0) > 0
		BEGIN
		
			SELECT	@TOP = REPLACE(@TOP,'@ClientPersonnelID','@WhoCreated'),
					@BOTTOM = REPLACE(@BOTTOM,'@ClientPersonnelID','@WhoCreated')
		
		END
		ELSE
		BEGIN		
			/*If this proc has no ClientPersonnelID or WhoCreated passed into it, compensate*/
			IF CHARINDEX('@ClientPersonnelID',@DEFINITION,0) = 0
			BEGIN

				SELECT @TOP = REPLACE(@TOP,'/*PERFLOGTOPBEGIN*/','/*PERFLOGTOPBEGIN*/'+ CHAR(13) + CHAR(13) + CHAR(9) + +'DECLARE @ClientPersonnelID INT = 0')

			END
		END
				
		/*Insert the pre-built perflog SQL at the top and bottom of the proc*/
		SELECT @RUN = 
		
		STUFF(STUFF(@DEFINITION,CHARINDEX(@StartPoint,@DEFINITION,0)+@Increment,0,@TOP),LEN(STUFF(@DEFINITION,CHARINDEX(@StartPoint,@DEFINITION,0)+@Increment,0,@TOP)) - CHARINDEX(@EndPoint,REVERSE(STUFF(@DEFINITION,CHARINDEX(@StartPoint,@DEFINITION,0)+@Increment,0,@TOP)),0)-@EndIncrement,0,@BOTTOM)
				
		SET @RUN = CASE @TYPE WHEN 'FN' THEN NULL ELSE REPLACE(@RUN,'CREATE PROCEDURE','ALTER PROCEDURE') END
		
		EXEC (@RUN)
		
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[Perflog_CreateLogging] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Perflog_CreateLogging] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Perflog_CreateLogging] TO [sp_executeall]
GO
