SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-01-20
-- Description:	Show process from a particular point
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ShowProcess] 
@EventTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	/*Set up a table for insrerting the events into*/
	declare @Events Table (UNID INT identity(1,1),LoopNumber INT, FromEventName VARCHAR(200), FromEventTypeID INT, ToEventName VARCHAR(200), ToEventTypeID INT, ToEventSubtypeID INT, ParentID INT, AquariumEventSubTypeID INT)

	/*
		Set up variables ready for use later on,
		TestID is used for incrementing the count during the 
		loop that inserts one event following another.
		LoopNumber is used for determining what level IE loop through the 
		branched process is.
		ClientID is well, ClientID
	*/
	declare @TestID int, 
			@LoopNumber INT = 1, 
			@ClientID INT 

	SELECT @ClientID = et.ClientID
	FROM EventType et WITH (NOLOCK)
	WHERE et.EventTypeID = @EventTypeID

	/*
		Select the first event in the list. From here we will get it's event choices
		that arent escalation endpoints. This will just confuse the user at this point.
	*/
	insert into @Events (LoopNumber, FromEventName, FromEventTypeID, ToEventName, ToEventTypeID, ToEventSubtypeID, AquariumEventSubTypeID )
	SELECT @LoopNumber, et.EventTypeName, et.EventTypeID, et2.EventTypeName, et2.EventTypeID, et2.EventSubtypeID, et.AquariumEventSubtypeID
	From EventType et with (nolock)
	inner join EventChoice ec with (nolock) on ec.EventTypeID = et.EventTypeID and ec.EscalationEvent = 0
	inner join EventType et2 with (nolock) on et2.EventTypeID = ec.NextEventTypeID
	where et.EventTypeID IN (@EventTypeID)

	select @TestID = @@ROWCOUNT

	/*Start the while loop off*/
	while @TestID > 0
	BEGIN

		/*
			Set the loop number to be 2+ as loop one has been used by the insert 
			outside of the while loop
		*/
		SELECT @LoopNumber = @LoopNumber + 1

		/*
			Insert the next set, excluding any that already exist. We dont want any 
			full recursion going on..
		*/
		insert into @Events (LoopNumber, FromEventName, FromEventTypeID, ToEventName, ToEventTypeID, ToEventSubtypeID, ParentID, AquariumEventSubTypeID)
		SELECT @LoopNumber, et.ToEventName, et.ToEventTypeID, et2.EventTypeName, et2.EventTypeID, et2.EventSubtypeID, et.UNID, et.AquariumEventSubTypeID
		From @Events et 
		inner join EventChoice ec with (nolock) on ec.EventTypeID = et.ToEventTypeID and ec.EscalationEvent = 0
		inner join EventType et2 with (nolock) on et2.EventTypeID = ec.NextEventTypeID
		where NOT EXISTS (
			SELECT *
			FROM @Events e 
			WHERE e.FromEventTypeID = et.ToEventTypeID
			AND e.ToEventTypeID = et2.EventTypeID
		)

		select @TestID = @@ROWCOUNT
		/*Set the testid to be the rowcount. If none are inserted then we are done*/

	END

	/*Now Null out the ending events*/
	----Update e
	----SET e.ParentID = NULL
	----FROM @Events e
	----WHERE NOT EXISTS (
	----	SELECT *
	----	FROM @Events e2 
	----	WHERE e2.ParentID = e.UNID
	----)

	/*
		Now onto the cte's these will be used in the main select to get out the
		attachments and any helper or mandatory fields. These will also be shown to the 
		user in a list of things.
	*/
	;
	WITH Attachments AS (
		SELECT eta.EventTypeID, eta.AttachmentEventTypeID, et.EventTypeName
		FROM EventTypeAttachment eta WITH (NOLOCK) 
		INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = eta.AttachmentEventTypeID
		WHERE et.ClientID = @ClientID
	),
	HelperMandatoryFields AS (
		SELECT eth.EventTypeID, eth.DetailFieldID
		FROM EventTypeHelperField eth WITH (NOLOCK)
		WHERE eth.ClientID = @ClientID

		UNION
		
		SELECT eth.EventTypeID, eth.DetailFieldID
		FROM EventTypeMandatoryField eth WITH (NOLOCK)
		WHERE eth.ClientID = @ClientID
	)

	/*
		Finally the main select. This will get data out of the temp table and 
		join it to the cte's using for xml to get the data out in a single string
		Other areas are case statments where we can output some helpful info about 
		what the event is and what it does.
	*/
	SELECT ROW_NUMBER() OVER (partition by (select 1) order by e.loopnumber) as [TestID], 
		'' as [Date Test Performed],
		e.LoopNumber, 
		e.ParentID [Goes From], 
		CASE WHEN NOT EXISTS (SELECT * FROM @Events e2 where e2.ParentID = e.UNID) THEN NULL ELSE e.UNID END as [Goes To], 
		'Event ' + e.FromEventName + ' Goes To ' + e.ToEventName as [User Test Description], 
			'Apply the event "' + e.ToEventName + '" (EventTypeID ' + CONVERT(VARCHAR(100),e.ToEventTypeID) + ')' + 
				'. This is a ' + LOWER(est.EventSubtypeName) + ' event' +
				CASE est.EventSubtypeID 
					WHEN 3 THEN ' This is a letter in event, it will allow you to upload 1 attachment Once you have clicked go/next you will be presented with a screen that will allow you to browse to the file select it and then upload it giving it a title. If it needs to be an email in event please make a note in the Phase 1 test Notes.'
					WHEN 4 THEN ' This is a letter out event. If it needs to be a email out event please make a note inthe Phase 1 test comments field.'
					WHEN 5 THEN ' As this is an email in event it will ask you to up load an attachment by browsing to the file, selecting it and uploading it. This will then most likely be used further along the process.'
					WHEN 6 THEN ' As this is an email out event. It will present a screen pre-filled in with from, to, subject (including reference number) and email body. Under certain ciscumstances the will also have a document under the document tab. We need to check that it does to the correct recipient(s), and contain the necessary data/attachments.' + 
						ISNULL(' The following attachment(s) will be included ' + STUFF((
							SELECT ' and ' + a.EventTypeName
							FROM Attachments a
							WHERE a.EventTypeID = e.ToEventTypeID
						FOR XML PATH(''))
						,1,5,''),' If any attachments are required please make a note in Phase 1 Comments.')
					WHEN 20 THEN ' As this is a case summary event we need to check that it contains the necessary data/documents.' + 
						ISNULL(' The following attachment(s) will be included ' + STUFF((
							SELECT ' and ' + a.EventTypeName
							FROM Attachments a
							WHERE a.EventTypeID = e.ToEventTypeID
						FOR XML PATH(''))
						,1,5,''),' If any attachments are required please make a note in Phase 1 Comments.')
					WHEN 11 THEN ' A process stopped event is when the case has been completed.'
					ELSE ''
					END + 
			'. Comments are ' + CASE et.UseEventComments WHEN 1 THEN 'enabled, as they are enabled you will need to complete the process of adding the eent by clicking on Finish.' ELSE 'disabled.' END + 
			ISNULL(' The following field(s) will be asked for ' + STUFF((
				SELECT ' and ' + df.FieldName
				FROM HelperMandatoryFields h
				INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = h.DetailFieldID
				WHERE h.EventTypeID = e.ToEventTypeID
			FOR XML PATH(''))
			,1,5,'') + ' as such you will be presented with the fields to fill in and will then have to click Save at the bottom of the screen. Any fields under the heading "Mandatory Fields" you have to complete, any under the heading Helper fields are optional.','') + 
			ISNULL('. Followup time is ' + CONVERT(VARCHAR,et.FollowupQuantity) + ' ' + t.TimeUnitsName + '.','') 
		AS [Technical Desription],
			'The event "' + e.ToEventName + '" will be applied to the case history.' +
				ISNULL(' The following field(s) will be asked for ' + STUFF((
				SELECT ' and ' + df.FieldName
				FROM HelperMandatoryFields h
				INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = h.DetailFieldID
				WHERE h.EventTypeID = e.ToEventTypeID
			FOR XML PATH(''))
			,1,5,''),'')
		AS [Expected Outcome],
		'' AS [Actual Outcome],
		'' AS [Phase 1 Test Notes],
		e.AquariumEventSubTypeID
	FROM @Events e
	INNER JOIN EventSubtype est with (nolock) on est.EventSubtypeID = e.ToEventSubtypeID
	INNER JOIN EventType et with (nolock) on et.EventTypeID = e.ToEventTypeID
	LEFT JOIN TimeUnits t WITH (NOLOCK) ON t.TimeUnitsID = et.FollowupTimeUnitsID
	ORDER by e.UNID, e.ParentID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ShowProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ShowProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ShowProcess] TO [sp_executeall]
GO
