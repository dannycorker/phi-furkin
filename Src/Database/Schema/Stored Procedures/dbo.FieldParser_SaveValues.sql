SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2014-10-31
-- Description:	New for #28942 Call Centre Scripting. Save detail values back to the database.
-- Modified By ACE & PR on 19/10/2015 Added Uniqueness to table row mapping
-- Modified By PR on 14/03/2016 Scripting now only sends the field data that has been changed
-- so detail value history can be saved
-- =============================================
CREATE PROCEDURE [dbo].[FieldParser_SaveValues] 
	@ClientPersonnelID INT, 
	@ClientID INT, 
	@CustomerID INT, 
	@LeadID INT, 
	@CaseID INT, 
	@MatterID INT, 
	@LanguageID INT = 1, 
	@ScriptID INT,
	@FieldsAndValues tvpIntVarchar READONLY,	/* DetailFieldID, DetailValue */ 
	@TableRowsToDelete tvpInt READONLY,			/* TableRowID */
	@TableRowsToUpsert tvpVarchar10 READONLY	/* TableRowID, PageDetailFieldID, ColumnDetailFieldID, DetailValue */
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TargetDetailFieldSubtypeID INT, 
	@ContactID INT,
	@PageID INT
	
	/* For storing all the @FieldsAndValues and then encoding any lookup lists */
	DECLARE @DetailFieldTargets TABLE (
		DetailFieldID INT NOT NULL,
		ClientID INT NOT NULL,
		DetailFieldSubtypeID INT NOT NULL,
		LookupListID INT NULL, 
		RawValue VARCHAR(2000) NULL,
		LookupListItemID INT NULL, 
		DetailValue VARCHAR(2000) NULL, 
		ErrorMessage VARCHAR(250) NULL,
		MaintainHistory BIT
	)
	
	/* Create new TableRows for inserts (indicated by TableRowID < 0) */
	DECLARE @TableRowMapping TABLE (
		TempID INT, 
		PageDetailFieldID INT, 
		TableRowID INT NULL 
	)
	
	
	/*
		Put all targets into one table first, joining to DetailFields so we only have to look them up once.
		RawValue is the string from the caller.
		DetailValue is what we will actually store in the database, so for LookupLists we need to lookup the int equivalent and store that instead.
		Any records where DetailValue is NULL get passed back to the caller so that the problems can be reported. This includes attempts to use
		fields that do not belong to the client.
	*/
	;WITH InnerSql AS 
	(
		/*
			In case the app passes the same field and value pair multiple times, just choose the first value for each field 
			the clause PARTITION BY DetailFieldID ORDER BY (SELECT 1) takes the natural order passed in by the calling app,
			so if the app passes in different values for the same field, we take the first one in the list for each field. 
		*/
		SELECT DISTINCT df.DetailFieldID, df.ClientID, df.LeadOrMatter, df.LookupListID, s.AnyValue as RawValue, df.MaintainHistory,
		ROW_NUMBER() OVER(PARTITION BY DetailFieldID ORDER BY (SELECT 1)) as rn 
		FROM @FieldsAndValues s  
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON s.AnyID = df.DetailFieldID 
	)
	INSERT @DetailFieldTargets (DetailFieldID, ClientID, DetailFieldSubtypeID, LookupListID, RawValue, MaintainHistory, DetailValue) 
	SELECT i.DetailFieldID, i.ClientID, i.LeadOrMatter, i.LookupListID, i.RawValue, i.MaintainHistory,
	CASE WHEN i.ClientID IN (0, @ClientID) THEN				/* If the field is not for this client, or client zero, then NULL it out */
		CASE WHEN i.LookupListID IS NULL THEN i.RawValue	/* If the field is a lookuplist then leave the value NULL as we will look it up below. */
		ELSE NULL END 
	ELSE NULL END AS DetailValue
	FROM InnerSql i 
	WHERE i.rn = 1 
	
	
	/*
		Encode any text values into the appropriate LookupListItemID, so "Yes" becomes "5144" 
		The LookupListID comes from the DetailField record, so client zero lists are included automatically
		Also if the answer is a foreign language translation, try a reverse lookup on the Translation table as well.
	*/
	UPDATE @DetailFieldTargets 
	SET DetailValue = CAST(luli.LookupListItemID AS VARCHAR)
	FROM @DetailFieldTargets d 
	INNER JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = d.LookupListID AND luli.ItemValue = d.RawValue 
	LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID AND t.Translation = d.RawValue 
	WHERE luli.ClientID IN (0, @ClientID)
	AND (t.ID > 0 OR (t.ID IS NULL AND luli.ItemValue = d.RawValue)) /* Either the translation record must match, or in its absence the luli record must match */
	
	
	
	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*                                         Insert / Update Detail Values                                           */
	/*                                                                                                                 */
	/*******************************************************************************************************************/
	
	IF @ScriptID IS NOT NULL
	BEGIN
		
		/* LeadDetailValues */
		SET @TargetDetailFieldSubtypeID = 1
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
		
			/* Update existing values */	
			UPDATE dbo.LeadDetailValues
			SET DetailValue = d.DetailValue
			FROM dbo.LeadDetailValues dv 
			INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
			WHERE dv.LeadID = @LeadID
			AND d.DetailValue IS NOT NULL
			
			/* Insert any brand new values */
			INSERT INTO LeadDetailValues(LeadID, ClientID, DetailFieldID, DetailValue) 
			SELECT @LeadID, @ClientID, d.DetailFieldID, d.DetailValue 
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.LeadDetailValues dv WITH (NOLOCK) 
				WHERE dv.DetailFieldID = d.DetailFieldID 
				AND dv.LeadID = @LeadID
			)
			
			/* Saves the lead level detail value history */
			INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, @LeadID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND d.MaintainHistory = 1
			
		END
		
		
		/* MatterDetailValues */
		SET @TargetDetailFieldSubtypeID = 2
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
		
			/* Update existing values */	
			UPDATE dbo.MatterDetailValues
			SET DetailValue = d.DetailValue
			FROM dbo.MatterDetailValues dv 
			INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
			WHERE dv.MatterID = @MatterID
			AND d.DetailValue IS NOT NULL
			
			/* Insert any brand new values */
			INSERT INTO dbo.MatterDetailValues(MatterID, LeadID, ClientID, DetailFieldID, DetailValue) 
			SELECT @MatterID, @LeadID, @ClientID, d.DetailFieldID, d.DetailValue 
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.MatterDetailValues dv WITH (NOLOCK) 
				WHERE dv.DetailFieldID = d.DetailFieldID 
				AND dv.MatterID = @MatterID
			)
			
			/* Saves the matter level detail value history */
			INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, @MatterID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND d.MaintainHistory = 1

		END

		
		/* CustomerDetailValues */
		SET @TargetDetailFieldSubtypeID = 10
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
		
			/* Update existing values */	
			UPDATE dbo.CustomerDetailValues
			SET DetailValue = d.DetailValue
			FROM dbo.CustomerDetailValues dv 
			INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
			WHERE dv.CustomerID = @CustomerID
			AND d.DetailValue IS NOT NULL
			
			/* Insert any brand new values */
			INSERT INTO dbo.CustomerDetailValues(CustomerID, ClientID, DetailFieldID, DetailValue) 
			SELECT @CustomerID, @ClientID, d.DetailFieldID, d.DetailValue 
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.CustomerDetailValues dv WITH (NOLOCK) 
				WHERE dv.DetailFieldID = d.DetailFieldID 
				AND dv.CustomerID = @CustomerID
			)
			
			/* Saves the customer level detail value history */
			INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, CustomerID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, @CustomerID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND d.MaintainHistory = 1
			
		END


		/* CaseDetailValues */
		SET @TargetDetailFieldSubtypeID = 11
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
		
			/* Update existing values */	
			UPDATE dbo.CaseDetailValues
			SET DetailValue = d.DetailValue
			FROM dbo.CaseDetailValues dv 
			INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
			WHERE dv.CaseID = @CaseID
			AND d.DetailValue IS NOT NULL
			
			/* Insert any brand new values */
			INSERT INTO dbo.CaseDetailValues(CaseID, ClientID, DetailFieldID, DetailValue) 
			SELECT @CaseID, @ClientID, d.DetailFieldID, d.DetailValue 
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.CaseDetailValues dv WITH (NOLOCK) 
				WHERE dv.DetailFieldID = d.DetailFieldID 
				AND dv.CaseID = @CaseID
			)
			
			/* Saves the case level detail value history */
			INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, CaseID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, @CaseID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND d.MaintainHistory = 1
			
		END


		/* ClientDetailValues */
		SET @TargetDetailFieldSubtypeID = 12
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
		
			/* Update existing values */	
			UPDATE dbo.ClientDetailValues
			SET DetailValue = d.DetailValue
			FROM dbo.ClientDetailValues dv 
			INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
			WHERE dv.ClientID = @ClientID
			AND d.DetailValue IS NOT NULL
			
			/* Insert any brand new values */
			INSERT INTO dbo.ClientDetailValues(ClientID, DetailFieldID, DetailValue) 
			SELECT @ClientID, d.DetailFieldID, d.DetailValue 
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.ClientDetailValues dv WITH (NOLOCK) 
				WHERE dv.DetailFieldID = d.DetailFieldID 
				AND dv.ClientID = @ClientID
			)
			
			/* Saves the client level detail value history */
			INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND d.MaintainHistory = 1
			
		END
		

		/* ClientPersonnelDetailValues */
		SET @TargetDetailFieldSubtypeID = 13
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
		
			/* Update existing values */	
			UPDATE dbo.ClientPersonnelDetailValues
			SET DetailValue = d.DetailValue
			FROM dbo.ClientPersonnelDetailValues dv 
			INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
			WHERE dv.ClientPersonnelID = @ClientPersonnelID
			AND d.DetailValue IS NOT NULL
			
			/* Insert any brand new values */
			INSERT INTO dbo.ClientPersonnelDetailValues(ClientPersonnelID, ClientID, DetailFieldID, DetailValue) 
			SELECT @ClientPersonnelID, @ClientID, d.DetailFieldID, d.DetailValue 
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.ClientPersonnelDetailValues dv WITH (NOLOCK) 
				WHERE dv.DetailFieldID = d.DetailFieldID 
				AND dv.ClientPersonnelID = @ClientPersonnelID
			)
			
			/* Saves the client personnel level detail value history */
			INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, ClientPersonnelDetailValueID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, @ClientPersonnelID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
			FROM @DetailFieldTargets d 
			WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND d.DetailValue IS NOT NULL
			AND d.MaintainHistory = 1
			
		END


		/* ContactDetailValues */
		SET @TargetDetailFieldSubtypeID = 14
		
		IF EXISTS(SELECT * FROM @DetailFieldTargets d WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID AND d.DetailValue IS NOT NULL)
		BEGIN
			
			SELECT TOP (1) 
			@ContactID = c.DefaultContactID 
			FROM dbo.Customers c WITH (NOLOCK) 
			WHERE c.CustomerID = @CustomerID 
			
			IF @ContactID IS NOT NULL
			BEGIN
				/* Update existing values */	
				UPDATE dbo.ContactDetailValues
				SET DetailValue = d.DetailValue
				FROM dbo.ContactDetailValues dv 
				INNER JOIN @DetailFieldTargets d ON d.DetailFieldID = dv.DetailFieldID 
				WHERE dv.ContactID = @ContactID
				AND d.DetailValue IS NOT NULL
				
				/* Insert any brand new values */
				INSERT INTO dbo.ContactDetailValues(ContactID, ClientID, DetailFieldID, DetailValue) 
				SELECT @ContactID, @ClientID, d.DetailFieldID, d.DetailValue 
				FROM @DetailFieldTargets d 
				WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
				AND d.DetailValue IS NOT NULL
				AND NOT EXISTS (
					SELECT * 
					FROM dbo.ContactDetailValues dv WITH (NOLOCK) 
					WHERE dv.DetailFieldID = d.DetailFieldID 
					AND dv.ContactID = @ContactID
				)
				
				/* Saves the contactID level detail value history */
				INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, ContactID, FieldValue, WhenSaved, ClientPersonnelID)
				SELECT @ClientID, d.DetailFieldID, @TargetDetailFieldSubtypeID, @ContactID, d.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID
				FROM @DetailFieldTargets d 
				WHERE d.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
				AND d.DetailValue IS NOT NULL
				AND d.MaintainHistory = 1
				
			END
		END
		
	END /* ScriptID IS NOT NULL */
	
	
	/*
		We have saved all the valid details now.
		
		If any values were still NULL after the lookups at the start of the proc, return them to the caller.
		There should never be any, but this will prevent any failures from happening silently in the background. 
	*/
	/* Field does not belong to this client (or the shared client zero) */
	UPDATE @DetailFieldTargets 
	SET ErrorMessage = 'Field not found' 
	WHERE ClientID NOT IN (0, @ClientID) 
	
	/* No value was passed in for this field */
	UPDATE @DetailFieldTargets 
	SET ErrorMessage = 'No value passed in' 
	WHERE RawValue IS NULL
	AND ErrorMessage IS NULL
	
	/* The lookup list item was not found (maybe someone edited the list after this script was created) */
	UPDATE @DetailFieldTargets 
	SET ErrorMessage = 'Lookup Item not found' 
	WHERE DetailValue IS NULL 
	AND LookupListID IS NOT NULL
	AND ErrorMessage IS NULL
	
	/* Delete any unwanted TableRows */
	DELETE dbo.TableRows 
	FROM dbo.TableRows tr 
	INNER JOIN @TableRowsToDelete trtd ON trtd.AnyID = tr.TableRowID 
	WHERE tr.ClientID = @ClientID 
	
	
	
	/* Populate the mapping table with what we have got so far.  Negative ID means please make a new TableRowID. */
	INSERT INTO @TableRowMapping (TempID, PageDetailFieldID) 
	SELECT DISTINCT trtu.ID, CAST(trtu.Val1 AS INT)
	FROM @TableRowsToUpsert trtu 
	WHERE trtu.ID < 0 
	
	/* If TempID > 0 then this is an existing TableRowID, so just set TableRowID = TempID right now */
	UPDATE @TableRowMapping 
	SET TableRowID = TempID 
	WHERE TempID > 0
	
	/* If there are any mapping records without a TableRowID, make one now and map the new ID to the TempID */
	WHILE EXISTS(SELECT * FROM @TableRowMapping WHERE TableRowID IS NULL)
	BEGIN
		DECLARE @TempID INT, @NewTableRowID INT
		
		SELECT TOP (1) @TempID = TempID, @PageID = t.PageDetailFieldID
		FROM @TableRowMapping t
		WHERE TableRowID IS NULL 
		ORDER BY TempID DESC
		
		/* Create the new rows first, using DetailFieldPageID to hold the old RowID temporarily */
		INSERT INTO dbo.TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID)
		SELECT DISTINCT @ClientID, /* Always set ClientID even if LeadOrMatter is not 12 */ 
		CASE WHEN df.LeadOrMatter = 1 THEN @LeadID ELSE NULL END, 
		CASE WHEN df.LeadOrMatter = 2 THEN @MatterID ELSE NULL END, 
		df.DetailFieldID, 
		df.DetailFieldPageID, 
		0 AS DenyEdit, 
		0 AS DenyDelete, 
		CASE WHEN df.LeadOrMatter = 10 THEN @CustomerID ELSE NULL END,  
		CASE WHEN df.LeadOrMatter = 11 THEN @CaseID ELSE NULL END,  
		CASE WHEN df.LeadOrMatter = 13 THEN @ClientPersonnelID ELSE NULL END,  
		CASE WHEN df.LeadOrMatter = 14 THEN @ContactID ELSE NULL END  
		FROM @TableRowsToUpsert trtu 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = CAST(trtu.Val1 AS INT) /* This will tell us that "Jim's Invoices" belongs at the Lead level, for example */
		WHERE trtu.ID = @TempID
		AND trtu.Val1 = @PageID
		
		SELECT @NewTableRowID = SCOPE_IDENTITY()
		
		UPDATE @TableRowMapping SET TableRowID = @NewTableRowID 
		WHERE TempID = @TempID 
		AND PageDetailFieldID = @PageID
		
		EXEC dbo._C00_LogIt 'Debug', 'FieldParser_SaveValues', 'Map rows', @TempID, @NewTableRowID
	END
	
	/* Update any existing TDV */
	/* @TableRowsToUpsert tvpVarchar10 READONLY holds this data: TableRowID, PageDetailFieldID, ColumnDetailFieldID, DetailValue */
	UPDATE dbo.TableDetailValues 
	SET DetailValue = CASE WHEN t.ID > 0 THEN CAST(t.ID AS VARCHAR) WHEN luli.LookupListID > 0 THEN CAST(luli.LookupListItemID AS VARCHAR) ELSE tr.Val3 END 
	FROM dbo.TableDetailValues tdv 
	INNER JOIN @TableRowsToUpsert tr ON tr.ID = tdv.TableRowID AND CAST(tr.Val2 AS INT) = tdv.DetailFieldID 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID 
	LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID AND luli.ItemValue = tr.Val3 
	LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID AND t.Translation = tr.Val3 
	WHERE tdv.ClientID = @ClientID 


	/* Insert values for new rows */
	/* @TableRowsToUpsert tvpVarchar10 READONLY holds this data: TableRowID, PageDetailFieldID, ColumnDetailFieldID, DetailValue */
	INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, CustomerID, CaseID, ClientPersonnelID, ContactID)
	SELECT trm.TableRowID, 
	dfColumn.DetailFieldID, 
	CASE WHEN t.ID > 0 THEN CAST(t.ID AS VARCHAR) WHEN luli.LookupListID > 0 THEN CAST(luli.LookupListItemID AS VARCHAR) ELSE trtu.Val3 END, 
	CASE WHEN dfPage.LeadOrMatter = 1 THEN @LeadID ELSE NULL END, 
	CASE WHEN dfPage.LeadOrMatter = 2 THEN @MatterID ELSE NULL END, 
	@ClientID, /* Always set ClientID even if LeadOrMatter is not 12 */ 
	CASE WHEN dfPage.LeadOrMatter = 10 THEN @CustomerID ELSE NULL END,  
	CASE WHEN dfPage.LeadOrMatter = 11 THEN @CaseID ELSE NULL END,  
	CASE WHEN dfPage.LeadOrMatter = 13 THEN @ClientPersonnelID ELSE NULL END,  
	CASE WHEN dfPage.LeadOrMatter = 14 THEN @ContactID ELSE NULL END  
	FROM @TableRowsToUpsert trtu 
	INNER JOIN @TableRowMapping trm ON trm.TempID = trtu.ID 
	INNER JOIN dbo.DetailFields dfPage WITH (NOLOCK) ON dfPage.DetailFieldID = CAST(trtu.Val1 AS INT) /* This will tell us that "Jim's Invoices" belongs at the Lead level, for example */
	INNER JOIN dbo.DetailFields dfColumn WITH (NOLOCK) ON dfColumn.DetailFieldID = CAST(trtu.Val2 AS INT) /* This will tell us that "Type of Pet" uses a lookuplist, for example */
	LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = dfColumn.LookupListID AND luli.ItemValue = trtu.Val3 
	LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID AND t.Translation = trtu.Val3 
	WHERE NOT EXISTS (
		SELECT * 
		FROM dbo.TableDetailValues tdvExisting WITH (NOLOCK) 
		WHERE tdvExisting.TableRowID = trm.TableRowID 
		AND tdvExisting.DetailFieldID = dfColumn.DetailFieldID 
	)
	
	
	/* No need to send field details back to the caller */
	/*SELECT d.DetailFieldID, 
	d.LookupListID, 
	d.RawValue, 
	df.FieldName,
	lu.LookupListName,
	d.ErrorMessage 
	FROM @DetailFieldTargets d 
	LEFT JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = d.DetailFieldID AND df.ClientID = @ClientID 
	LEFT JOIN dbo.LookupList lu WITH (NOLOCK) ON lu.LookupListID = df.LookupListID AND lu.ClientID = @ClientID 
	WHERE d.DetailValue IS NULL */
	
	/*
		Send Table details back to the caller.
		When the app adds a new row to a table, it counts the number of rows so far, adds 1 and puts a minus sign in front.
		Multiple tables might be doing this at once, which is why PageDetailFieldID is used to tell them apart.
		So this table might look like:
		
		TempID	PageDetailFieldID	TableRowID
		1234566	157510,				1234566		-- Existing row, no change. Don't even send this one back.
		-2,		157510,				1234567		-- New TableRowID for field 157510 "Paul's atomic elements"
		-3,		157510,				1234568		-- ditto
		-4,		157510,				1234569		-- ditto
		-16,	157599,				1234570		-- This is a new row for a different table "Jim's invoices"
		
		Now reverse the sort order to help the app re-key these records
		-4,		157510,				1234569		
		-3,		157510,				1234568		
		-2,		157510,				1234567		
		-16,	157599,				1234570		
		
	*/
	SELECT * 
	FROM @TableRowMapping trm 
	WHERE trm.TempID < 0 
	ORDER BY trm.PageDetailFieldID ASC, trm.TableRowID DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_SaveValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldParser_SaveValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_SaveValues] TO [sp_executeall]
GO
