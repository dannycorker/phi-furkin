SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-27
-- Description:	Updates an appt and reminder to completed
-- =============================================

CREATE PROCEDURE [dbo].[_C00_iDiary_CompleteAppt] 
(
	@DiaryAppointmentID INT,
	@ClientPersonnelID INT
)

AS
BEGIN

	DECLARE @LogMessage VARCHAR(2000) = 'Diary appointment id ' + Convert(VARCHAR, @DiaryAppointmentID)
	
	EXEC dbo._C00_LogIt 'iDiary', 'CustomPage', '[dbo].[_C00_iDiary_CompleteAppt]', @LogMessage, @ClientPersonnelID

	/*Set the appointment to be complete and dismiss the reminder*/
	UPDATE d
	SET Completed = 1
	FROM DiaryAppointment d
	WHERE d.DiaryAppointmentID = @DiaryAppointmentID
	
	UPDATE DiaryReminder
	SET ReminderDate = NULL
	FROM DiaryReminder d
	WHERE d.DiaryAppointmentID = @DiaryAppointmentID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_iDiary_CompleteAppt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_iDiary_CompleteAppt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_iDiary_CompleteAppt] TO [sp_executeall]
GO
