SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UserRightsDynamic table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamic_Delete]
(

	@UserRightsDynamicID int   
)
AS


				DELETE FROM [dbo].[UserRightsDynamic] WITH (ROWLOCK) 
				WHERE
					[UserRightsDynamicID] = @UserRightsDynamicID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamic_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_Delete] TO [sp_executeall]
GO
