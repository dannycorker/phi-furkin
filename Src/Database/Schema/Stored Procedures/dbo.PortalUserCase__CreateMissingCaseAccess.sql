SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-05-07
-- Description:	Create PortalUserCase records for Cases who have a specific DetailValue
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserCase__CreateMissingCaseAccess]
	@ClientID int,
	@DetailFieldID int = NULL,
	@DetailValue varchar(200),
	@PortalUserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO PortalUserCase (PortalUserID, LeadID, CaseID) 
	SELECT @PortalUserID, c.CaseID, c.LeadID FROM Cases c WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID AND c.ClientID = @ClientID
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID
	WHERE mdv.DetailValue = @DetailValue
	AND mdv.DetailFieldID = @DetailFieldID
	AND NOT EXISTS (
		SELECT * FROM PortalUserCase puc WITH (NOLOCK)
		WHERE puc.PortalUserID = @PortalUserID
		AND (puc.LeadID = c.LeadID)
		AND ((puc.CaseID = c.CaseID) OR (puc.CaseID IS NULL))
	) 
END




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__CreateMissingCaseAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__CreateMissingCaseAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__CreateMissingCaseAccess] TO [sp_executeall]
GO
