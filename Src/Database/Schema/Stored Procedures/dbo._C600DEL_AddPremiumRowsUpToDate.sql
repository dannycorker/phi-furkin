SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-20
-- Description:	Adds all outstanding premium payment table rows for premiums due up to the date supplied
--				Used to add rows forward in certain MTA situations and to get up-to-date for
--              reinstatement after suspension
-- Mods
--	2015-07-23 DCM removed delete existing ready rows
--  2015-09-15 DCM added PremiumCalculationID
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600DEL_AddPremiumRowsUpToDate] 
(
	@MatterID INT,
	@PaymentDate DATE,
	@UsePreviousPremium INT = 0
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50021840 ,
--	@UsePreviousPremium INT = 1
	
	DECLARE @AqUserID INT,
			@CaseID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@CollectionsCaseID INT,
			@CollectionsMatterID INT,
			@FromDate DATE,
			@IsAnnual INT,
			@PaymentTypeID INT,
			@Premium MONEY,
			@PremiumCalculationID INT 
				
	DECLARE @Data TABLE
	(
		FromDate DATE,
		Type INT
	)
	
	SELECT @AqUserID=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@ClientID,53,'CFG|AqAutomationCPID',0)
		
	-- find appropriate collections matter for the matter passed in
	IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @CollectionsMatterID=@MatterID
		
	END
	ELSE
	BEGIN
	
		SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
		
	END
	SELECT @CollectionsCaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@CollectionsMatterID
	SELECT @CaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@MatterID
	SELECT @IsAnnual=CASE WHEN dbo.fnGetDvAsInt(170145,@CollectionsCaseID)=69944 THEN 1 ELSE 0 END
	IF @IsAnnual=1 -- do nothing for annual cases
	BEGIN
	
		RETURN
	
	END	
		
	-- find start of next premium payment period
	SELECT @PaymentTypeID = 69897 -- Default to a monthly payment but this might be reset to a first payment in the function if this failed
	
	INSERT @Data (FromDate, Type)
	SELECT FromDate, Type
	FROM dbo.fn_C600_GetNextPremiumFromDate(@MatterID, @PaymentTypeID)
	
	SELECT @FromDate = FromDate, @PaymentTypeID = Type
	FROM @Data
	
	-- add rows until the regular premium payment row is in place to cover the next payment due date
	-- OR the next premium payment row would be for next year's premium (renewal processing will deal with these)
	WHILE @FromDate <= @PaymentDate
	BEGIN

		IF @FromDate >= dbo.fnGetDvAsDate (170037, @MatterID) 
			BREAK
			
		SELECT @Premium=CASE WHEN @UsePreviousPremium=0 THEN
							CASE WHEN @PaymentTypeID=69896 THEN dbo.fnGetDvAsMoney(175338,@CaseID) ELSE dbo.fnGetDvAsMoney(175339,@CaseID) END
						ELSE
							CASE WHEN @PaymentTypeID=69896 THEN dbo.fnGetDvAsMoney(175341,@CaseID) ELSE dbo.fnGetDvAsMoney(175342,@CaseID) END
						END
			
		SELECT @PremiumCalculationID=CASE WHEN @UsePreviousPremium=0 THEN dbo.fnGetDvAsMoney(175711,@CaseID) ELSE dbo.fnGetDvAsMoney(175712,@CaseID) END
						
		-- add row to premiums table
		EXEC _C00_1273_Policy_AddPremiumRow @MatterID, @Premium, 0,	@Premium, 0, @Premium,	NULL, @PaymentTypeID, @FromDate, @PremiumCalculationID=@PremiumCalculationID
		
		-- update latest premium value
		EXEC dbo._C00_SimpleValueIntoField 170086, @Premium, @MatterID, @AqUserID
				
		-- get start of next premium payment period 	
		DELETE FROM @Data
		SELECT @PaymentTypeID = 69897	
		INSERT @Data (FromDate, Type)
		SELECT FromDate, Type
		FROM dbo.fn_C600_GetNextPremiumFromDate(@MatterID, @PaymentTypeID)
		
		SELECT @FromDate = FromDate, @PaymentTypeID = Type
		FROM @Data

	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600DEL_AddPremiumRowsUpToDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600DEL_AddPremiumRowsUpToDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600DEL_AddPremiumRowsUpToDate] TO [sp_executeall]
GO
