SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By:	Chris Townsend
-- Date:		15th May 2008
-- Purpose:		Gets chart details for display on the Manage Charts page
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetChartDetailsListByClientIDAndUserID]
(
	@ClientID int,
	@UserID int = 0  
)
AS

				SET ANSI_NULLS OFF
				
				SELECT	c.ChartID, 
						c.ChartTitle, 
						ct.ChartTypeName,
						c.CreatedBy,
						cp1.UserName AS CreatedByName,
						c.CreatedOn,
						cp2.UserName AS LastEditedByName,
						c.LastEditedOn
				FROM Chart c

				INNER JOIN ChartType ct ON ct.ChartTypeID = c.ChartTypeID
				LEFT JOIN ClientPersonnel cp1 ON c.CreatedBy = cp1.ClientPersonnelID
				LEFT JOIN ClientPersonnel cp2 ON c.LastEditedBy = cp2.ClientPersonnelID

				WHERE c.ClientID = @ClientID
				AND (@UserID = 0 OR c.CreatedBy = @UserID) 
					
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			







GO
GRANT VIEW DEFINITION ON  [dbo].[GetChartDetailsListByClientIDAndUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetChartDetailsListByClientIDAndUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetChartDetailsListByClientIDAndUserID] TO [sp_executeall]
GO
