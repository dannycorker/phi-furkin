SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptStandardTarget_Update]
(

	@ScriptStandardTargetID int   ,

	@ScriptID int   ,

	@ClientID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptStandardTarget]
				SET
					[ScriptID] = @ScriptID
					,[ClientID] = @ClientID
					,[Target] = @Target
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
				WHERE
[ScriptStandardTargetID] = @ScriptStandardTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Update] TO [sp_executeall]
GO
