SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-07-31
-- Description:	Find all fields that match the search pattern and all values to fill them for this Customer/Lead/Case/Matter/Contact
-- =============================================
CREATE PROCEDURE [dbo].[DetailValues__SearchAllByFieldAndCustomer] 
	@CustomerID INT, 
	@FieldName VARCHAR(50), 
	@ExactMatch BIT = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ExactMatch = 0
	BEGIN
		SELECT @FieldName = '%' + @FieldName + '%'
	END	
	
	/* Customer fields */
	SELECT 
		df.DetailFieldID, 
		df.DetailFieldPageID,
		df.FieldCaption,
		df.LeadOrMatter,
		df.QuestionTypeID,
		dv.CustomerDetailValueID [DetailValueID],
		dv.DetailValue
	FROM dbo.DetailFields AS df WITH (NOLOCK) 
	LEFT JOIN dbo.CustomerDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = df.DetailFieldID AND dv.CustomerID = @CustomerID 
	WHERE df.LeadOrMatter = 10 
	AND df.FieldCaption LIKE @FieldName 
	
	UNION
	
	/* Lead fields */
	SELECT 
		df.DetailFieldID, 
		df.DetailFieldPageID,
		df.FieldCaption,
		df.LeadOrMatter,
		df.QuestionTypeID,
		dv.LeadDetailValueID [DetailValueID],
		dv.DetailValue
	FROM dbo.DetailFields AS df WITH (NOLOCK) 
	CROSS JOIN dbo.Lead l WITH (NOLOCK) 
	LEFT JOIN dbo.LeadDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = df.DetailFieldID AND dv.LeadID = l.LeadID 
	WHERE l.CustomerID = @CustomerID 
	AND df.LeadOrMatter = 1 
	AND df.FieldCaption LIKE @FieldName 
	
	UNION
	
	/* Case fields */
	SELECT 
		df.DetailFieldID, 
		df.DetailFieldPageID,
		df.FieldCaption,
		df.LeadOrMatter,
		df.QuestionTypeID,
		dv.CaseDetailValueID [DetailValueID],
		dv.DetailValue
	FROM dbo.DetailFields AS df WITH (NOLOCK) 
	CROSS JOIN dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID 
	LEFT JOIN dbo.CaseDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = df.DetailFieldID AND dv.CaseID = ca.CaseID 
	WHERE l.CustomerID = @CustomerID 
	AND df.LeadOrMatter = 11 
	AND df.FieldCaption LIKE @FieldName 
	
	UNION
	
	/* Matter fields */
	SELECT 
		df.DetailFieldID, 
		df.DetailFieldPageID,
		df.FieldCaption,
		df.LeadOrMatter,
		df.QuestionTypeID,
		dv.MatterDetailValueID [DetailValueID],
		dv.DetailValue
	FROM dbo.DetailFields AS df WITH (NOLOCK) 
	CROSS JOIN dbo.Matter m WITH (NOLOCK) 
	LEFT JOIN dbo.MatterDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = df.DetailFieldID AND dv.MatterID = m.MatterID 
	WHERE m.CustomerID = @CustomerID 
	AND df.LeadOrMatter = 2 
	AND df.FieldCaption LIKE @FieldName 
	
	UNION
	
	/* Contact fields */
	SELECT 
		df.DetailFieldID, 
		df.DetailFieldPageID,
		df.FieldCaption,
		df.LeadOrMatter,
		df.QuestionTypeID,
		dv.ContactDetailValueID [DetailValueID],
		dv.DetailValue
	FROM dbo.DetailFields AS df WITH (NOLOCK) 
	CROSS JOIN dbo.Contact co WITH (NOLOCK) 
	LEFT JOIN dbo.ContactDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = df.DetailFieldID AND dv.ContactID = co.ContactID 
	WHERE co.CustomerID = @CustomerID 
	AND df.LeadOrMatter = 14 
	AND df.FieldCaption LIKE @FieldName 
	
	/* Ignoring Client fields */
	/* Ignoring ClientPersonnel fields */
	/* Ignoring Object fields */
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues__SearchAllByFieldAndCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValues__SearchAllByFieldAndCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues__SearchAllByFieldAndCustomer] TO [sp_executeall]
GO
