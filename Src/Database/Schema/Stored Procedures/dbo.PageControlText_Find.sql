SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PageControlText table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_Find]
(

	@SearchUsingOR bit   = null ,

	@PageControlTextID int   = null ,

	@ClientID int   = null ,

	@LanguageID int   = null ,

	@PageName varchar (250)  = null ,

	@ControlName varchar (250)  = null ,

	@ControlText varchar (MAX)  = null ,

	@TooltipText varchar (MAX)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PageControlTextID]
	, [ClientID]
	, [LanguageID]
	, [PageName]
	, [ControlName]
	, [ControlText]
	, [TooltipText]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[PageControlText] WITH (NOLOCK) 
    WHERE 
	 ([PageControlTextID] = @PageControlTextID OR @PageControlTextID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([PageName] = @PageName OR @PageName IS NULL)
	AND ([ControlName] = @ControlName OR @ControlName IS NULL)
	AND ([ControlText] = @ControlText OR @ControlText IS NULL)
	AND ([TooltipText] = @TooltipText OR @TooltipText IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PageControlTextID]
	, [ClientID]
	, [LanguageID]
	, [PageName]
	, [ControlName]
	, [ControlText]
	, [TooltipText]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[PageControlText] WITH (NOLOCK) 
    WHERE 
	 ([PageControlTextID] = @PageControlTextID AND @PageControlTextID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([PageName] = @PageName AND @PageName is not null)
	OR ([ControlName] = @ControlName AND @ControlName is not null)
	OR ([ControlText] = @ControlText AND @ControlText is not null)
	OR ([TooltipText] = @TooltipText AND @TooltipText is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Find] TO [sp_executeall]
GO
