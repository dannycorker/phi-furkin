SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskParam table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_GetByAutomatedTaskParamID]
(

	@AutomatedTaskParamID int   
)
AS


				SELECT
					[AutomatedTaskParamID],
					[TaskID],
					[ClientID],
					[ParamName],
					[ParamValue]
				FROM
					[dbo].[AutomatedTaskParam] WITH (NOLOCK) 
				WHERE
										[AutomatedTaskParamID] = @AutomatedTaskParamID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByAutomatedTaskParamID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_GetByAutomatedTaskParamID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByAutomatedTaskParamID] TO [sp_executeall]
GO
