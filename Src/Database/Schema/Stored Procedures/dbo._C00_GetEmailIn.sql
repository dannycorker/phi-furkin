SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 11/05/2016
-- Description:	Gets a list of emails by event type id for a given lead
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetEmailIn]
	@LeadID INT,
	@EventTypeID INT,
	@NumberOfRecordsToReturn INT = 1
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TOP (@NumberOfRecordsToReturn)
		   le.LeadEventID, 
		   ld.LeadDocumentID, 
		   le.WhenCreated, 
		   ld.EmailFrom, 
		   ld.EmailTo, 
		   ld.CcList, 
		   ld.BccList, 
		   ld.EmailBlobSize, 
		   ld.EmailBLOB,
		   ld.DocumentBLOB,
		   ld.LeadDocumentTitle
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.vLeadDocumentList ld WITH (NOLOCK) on ld.LeadDocumentID = le.LeadDocumentID
	WHERE le.LeadID=@LeadID AND le.EventTypeID=@EventTypeID
	ORDER BY le.WhenCreated DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetEmailIn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetEmailIn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetEmailIn] TO [sp_executeall]
GO
