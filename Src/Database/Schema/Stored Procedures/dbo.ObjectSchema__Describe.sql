SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-06-15
-- Description:	Gets information about a clients object schema 
-- =============================================
CREATE PROCEDURE [dbo].[ObjectSchema__Describe]
(	
	@ClientID INT,
	@SubClientID INT = NULL	
)	
AS
BEGIN
	
	-- Object Types
	SELECT ObjectTypeID, ObjectTypeName 
	FROM dbo.ObjectType WITH (NOLOCK) 
	WHERE ClientID = @ClientID
	AND (SubClientID IS NULL OR @SubClientID IS NULL OR SubClientID = @SubClientID)
	AND Enabled = 1
	ORDER BY ObjectTypeID
	
	-- Object Links
	SELECT	r.FromObjectTypeID, f.ObjectTypeName AS FromObjectTypeName, r.ToObjectTypeID, t.ObjectTypeName AS ToObjectTypeName, 
			r.RelationshipTypeID , o.AquariumOptionName AS RelationshipTypeName, r.RelationshipName, r.ObjectTypeRelationshipID
	FROM dbo.ObjectTypeRelationship r WITH (NOLOCK) 
	INNER JOIN dbo.ObjectType f WITH (NOLOCK) ON r.FromObjectTypeID = f.ObjectTypeID 
	INNER JOIN dbo.ObjectType t WITH (NOLOCK) ON r.ToObjectTypeID = t.ObjectTypeID 
	INNER JOIN dbo.AquariumOption o WITH (NOLOCK) ON r.RelationshipTypeID = o.AquariumOptionID 
	WHERE f.ClientID = @ClientID
	AND t.ClientID = @ClientID
	AND (f.SubClientID IS NULL OR @SubClientID IS NULL OR f.SubClientID = @SubClientID)
	AND (t.SubClientID IS NULL OR @SubClientID IS NULL OR t.SubClientID = @SubClientID)
	AND f.Enabled = 1
	AND t.Enabled = 1
	ORDER BY r.FromObjectTypeID
	
	-- Lookup Lists
	;WITH InnerSql AS 
	(
		SELECT	l.LookupListID, l.LookupListName, li.LookupListItemID, li.ItemValue, li.SortOrder, l.SortOptionID,
				ROW_NUMBER() OVER(PARTITION BY l.LookupListID, l.LookupListName, li.LookupListItemID, li.ItemValue, li.SortOrder, l.SortOptionID ORDER BY l.LookupListID) as rn 
		FROM dbo.DetailFields f WITH (NOLOCK)
		INNER JOIN dbo.ObjectType o WITH (NOLOCK) ON f.ObjectTypeID = o.ObjectTypeID
		INNER JOIN dbo.LookupList l WITH (NOLOCK) ON f.LookupListID = l.LookupListID
		INNER JOIN dbo.LookupListItems li WITH (NOLOCK) ON l.LookupListID = li.LookupListID  
		WHERE o.ClientID = @ClientID
		AND (o.SubClientID IS NULL OR @SubClientID IS NULL OR o.SubClientID = @SubClientID)
		AND o.Enabled = 1
		AND f.Enabled = 1
	)
	SELECT *
	FROM InnerSql i 
	WHERE i.rn = 1 
	ORDER BY
	CASE WHEN SortOptionID = 21 THEN ItemValue END,
	CASE WHEN SortOptionID = 22 THEN ItemValue END DESC,
	CASE WHEN SortOptionID = 23 THEN SortOrder END

	
	-- Detail fields
	SELECT	o.ObjectTypeID, o.ObjectTypeName, f.DetailFieldID, f.FieldName, f.FieldCaption, f.QuestionTypeID, f.Required, f.LookupListID, f.ErrorMessage, 
			f.Editable, f.Hidden
	FROM dbo.DetailFields f WITH (NOLOCK)
	INNER JOIN dbo.ObjectType o WITH (NOLOCK) ON f.ObjectTypeID = o.ObjectTypeID
	WHERE o.ClientID = @ClientID
	AND (o.SubClientID IS NULL OR @SubClientID IS NULL OR o.SubClientID = @SubClientID)
	AND o.Enabled = 1
	AND f.Enabled = 1
	AND f.QuestionTypeID != 22
	ORDER BY f.DetailFieldID
	
	-- Custom Procs
	DECLARE @ClientIDString VARCHAR(3) = CAST(@ClientID AS VARCHAR)

	IF LEN(@ClientIDString) < 2
	BEGIN
		SELECT @ClientIDString = '0' + @ClientIDString
	END

	SELECT	r.ROUTINE_NAME AS ProcName, 
			SUBSTRING(REPLACE(r.ROUTINE_NAME, '_C' + @ClientIDString + '_Objects_', ''), 1, CHARINDEX('_', REPLACE(r.ROUTINE_NAME, '_C' + @ClientIDString + '_Objects_', '')) - 1) AS ObjectTypeID,
			SUBSTRING(REPLACE(r.ROUTINE_NAME, '_C' + @ClientIDString + '_Objects_', ''), CHARINDEX('_', REPLACE(r.ROUTINE_NAME, '_C' + @ClientIDString + '_Objects_', '')) + 1, LEN(REPLACE(r.ROUTINE_NAME, '_C' + @ClientIDString + '_Objects_', ''))) AS MethodName
	FROM INFORMATION_SCHEMA.ROUTINES r
	WHERE r.ROUTINE_TYPE = 'PROCEDURE' 
	AND r.ROUTINE_NAME LIKE '_C' + @ClientIDString + '_Objects_%'
	ORDER BY r.ROUTINE_NAME


	SELECT r.ROUTINE_NAME AS ProcName, SUBSTRING(p.PARAMETER_NAME, 2, LEN(p.PARAMETER_NAME)) AS ParamName, p.DATA_TYPE AS DataType
	FROM INFORMATION_SCHEMA.ROUTINES r 
	INNER JOIN INFORMATION_SCHEMA.PARAMETERS p ON p.SPECIFIC_NAME = r.SPECIFIC_NAME 
	WHERE r.ROUTINE_TYPE = 'PROCEDURE' 
	AND r.ROUTINE_NAME LIKE '_C' + @ClientIDString + '_Objects_%'
	ORDER BY r.ROUTINE_NAME
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectSchema__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectSchema__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectSchema__Describe] TO [sp_executeall]
GO
