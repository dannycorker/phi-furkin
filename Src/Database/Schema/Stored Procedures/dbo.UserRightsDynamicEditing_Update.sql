SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the UserRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamicEditing_Update]
(

	@UserRightsDynamicEditingID int   ,

	@ClientPersonnelID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[UserRightsDynamicEditing]
				SET
					[ClientPersonnelID] = @ClientPersonnelID
					,[FunctionTypeID] = @FunctionTypeID
					,[LeadTypeID] = @LeadTypeID
					,[ObjectID] = @ObjectID
					,[RightID] = @RightID
				WHERE
[UserRightsDynamicEditingID] = @UserRightsDynamicEditingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamicEditing_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Update] TO [sp_executeall]
GO
