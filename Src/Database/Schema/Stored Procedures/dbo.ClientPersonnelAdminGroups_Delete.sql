SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnelAdminGroups table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAdminGroups_Delete]
(

	@ClientPersonnelAdminGroupID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnelAdminGroups] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAdminGroups_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_Delete] TO [sp_executeall]
GO
