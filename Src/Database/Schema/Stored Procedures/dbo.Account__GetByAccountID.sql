SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-06-2016
-- Description:	Gets an account
-- =============================================
CREATE PROCEDURE [dbo].[Account__GetByAccountID]
	@AccountID INT,
	@ClientID INT,
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM Account account WITH (NOLOCK)
	LEFT JOIN dbo.AccountType at WITH (NOLOCK) on at.AccountTypeID = account.AccountTypeID
	WHERE account.AccountID = @AccountID AND account.ClientID=@ClientID AND account.CustomerID=@CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetByAccountID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__GetByAccountID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetByAccountID] TO [sp_executeall]
GO
