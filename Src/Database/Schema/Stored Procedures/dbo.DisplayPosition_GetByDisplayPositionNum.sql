SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DisplayPosition table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DisplayPosition_GetByDisplayPositionNum]
(

	@DisplayPositionNum smallint   
)
AS


				SELECT
					[DisplayPositionNum],
					[DisplayPositionName]
				FROM
					[dbo].[DisplayPosition] WITH (NOLOCK) 
				WHERE
										[DisplayPositionNum] = @DisplayPositionNum
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_GetByDisplayPositionNum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DisplayPosition_GetByDisplayPositionNum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_GetByDisplayPositionNum] TO [sp_executeall]
GO
