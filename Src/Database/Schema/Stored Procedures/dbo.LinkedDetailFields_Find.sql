SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LinkedDetailFields table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LinkedDetailFields_Find]
(

	@SearchUsingOR bit   = null ,

	@LinkedDetailFieldID int   = null ,

	@LeadTypeIDTo int   = null ,

	@LeadTypeIDFrom int   = null ,

	@DetailFieldIDTo int   = null ,

	@DetailFieldIDFrom int   = null ,

	@DisplayOnPageID int   = null ,

	@FieldOrder int   = null ,

	@Enabled bit   = null ,

	@History bit   = null ,

	@LeadOrMatter int   = null ,

	@FieldName varchar (100)  = null ,

	@LeadLinkedTo int   = null ,

	@LeadLinkedFrom int   = null ,

	@IncludeLinkedField bit   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LinkedDetailFieldID]
	, [LeadTypeIDTo]
	, [LeadTypeIDFrom]
	, [DetailFieldIDTo]
	, [DetailFieldIDFrom]
	, [DisplayOnPageID]
	, [FieldOrder]
	, [Enabled]
	, [History]
	, [LeadOrMatter]
	, [FieldName]
	, [LeadLinkedTo]
	, [LeadLinkedFrom]
	, [IncludeLinkedField]
	, [ClientID]
    FROM
	[dbo].[LinkedDetailFields] WITH (NOLOCK) 
    WHERE 
	 ([LinkedDetailFieldID] = @LinkedDetailFieldID OR @LinkedDetailFieldID IS NULL)
	AND ([LeadTypeIDTo] = @LeadTypeIDTo OR @LeadTypeIDTo IS NULL)
	AND ([LeadTypeIDFrom] = @LeadTypeIDFrom OR @LeadTypeIDFrom IS NULL)
	AND ([DetailFieldIDTo] = @DetailFieldIDTo OR @DetailFieldIDTo IS NULL)
	AND ([DetailFieldIDFrom] = @DetailFieldIDFrom OR @DetailFieldIDFrom IS NULL)
	AND ([DisplayOnPageID] = @DisplayOnPageID OR @DisplayOnPageID IS NULL)
	AND ([FieldOrder] = @FieldOrder OR @FieldOrder IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([History] = @History OR @History IS NULL)
	AND ([LeadOrMatter] = @LeadOrMatter OR @LeadOrMatter IS NULL)
	AND ([FieldName] = @FieldName OR @FieldName IS NULL)
	AND ([LeadLinkedTo] = @LeadLinkedTo OR @LeadLinkedTo IS NULL)
	AND ([LeadLinkedFrom] = @LeadLinkedFrom OR @LeadLinkedFrom IS NULL)
	AND ([IncludeLinkedField] = @IncludeLinkedField OR @IncludeLinkedField IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LinkedDetailFieldID]
	, [LeadTypeIDTo]
	, [LeadTypeIDFrom]
	, [DetailFieldIDTo]
	, [DetailFieldIDFrom]
	, [DisplayOnPageID]
	, [FieldOrder]
	, [Enabled]
	, [History]
	, [LeadOrMatter]
	, [FieldName]
	, [LeadLinkedTo]
	, [LeadLinkedFrom]
	, [IncludeLinkedField]
	, [ClientID]
    FROM
	[dbo].[LinkedDetailFields] WITH (NOLOCK) 
    WHERE 
	 ([LinkedDetailFieldID] = @LinkedDetailFieldID AND @LinkedDetailFieldID is not null)
	OR ([LeadTypeIDTo] = @LeadTypeIDTo AND @LeadTypeIDTo is not null)
	OR ([LeadTypeIDFrom] = @LeadTypeIDFrom AND @LeadTypeIDFrom is not null)
	OR ([DetailFieldIDTo] = @DetailFieldIDTo AND @DetailFieldIDTo is not null)
	OR ([DetailFieldIDFrom] = @DetailFieldIDFrom AND @DetailFieldIDFrom is not null)
	OR ([DisplayOnPageID] = @DisplayOnPageID AND @DisplayOnPageID is not null)
	OR ([FieldOrder] = @FieldOrder AND @FieldOrder is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([History] = @History AND @History is not null)
	OR ([LeadOrMatter] = @LeadOrMatter AND @LeadOrMatter is not null)
	OR ([FieldName] = @FieldName AND @FieldName is not null)
	OR ([LeadLinkedTo] = @LeadLinkedTo AND @LeadLinkedTo is not null)
	OR ([LeadLinkedFrom] = @LeadLinkedFrom AND @LeadLinkedFrom is not null)
	OR ([IncludeLinkedField] = @IncludeLinkedField AND @IncludeLinkedField is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LinkedDetailFields_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Find] TO [sp_executeall]
GO
