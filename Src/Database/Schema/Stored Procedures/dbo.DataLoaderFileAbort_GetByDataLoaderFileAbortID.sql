SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFileAbort table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_GetByDataLoaderFileAbortID]
(

	@DataLoaderFileAbortID int   
)
AS


				SELECT
					[DataLoaderFileAbortID],
					[ClientID],
					[DateTimeFileAborted],
					[ReasonForAbort],
					[DataLoaderFileID],
					[FileAbortedOn],
					[LastRowImported]
				FROM
					[dbo].[DataLoaderFileAbort] WITH (NOLOCK) 
				WHERE
										[DataLoaderFileAbortID] = @DataLoaderFileAbortID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_GetByDataLoaderFileAbortID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_GetByDataLoaderFileAbortID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_GetByDataLoaderFileAbortID] TO [sp_executeall]
GO
