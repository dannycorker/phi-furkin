SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_Delete]
(

	@ThirdPartyFieldID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyField] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyFieldID] = @ThirdPartyFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Delete] TO [sp_executeall]
GO
