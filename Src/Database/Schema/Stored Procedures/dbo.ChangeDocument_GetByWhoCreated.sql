SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeDocument table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_GetByWhoCreated]
(

	@WhoCreated int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChangeDocumentID],
					[DocumentData],
					[DocumentExtension],
					[WhenCreated],
					[WhoCreated]
				FROM
					[dbo].[ChangeDocument] WITH (NOLOCK) 
				WHERE
					[WhoCreated] = @WhoCreated
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_GetByWhoCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_GetByWhoCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_GetByWhoCreated] TO [sp_executeall]
GO
