SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the DiaryAppointment table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [DiaryAppointmentID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([DiaryAppointmentID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [DiaryAppointmentID]'
				SET @SQL = @SQL + ' FROM [dbo].[DiaryAppointment] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[DiaryAppointmentID], O.[ClientID], O.[DiaryAppointmentTitle], O.[DiaryAppointmentText], O.[CreatedBy], O.[DueDate], O.[EndDate], O.[AllDayEvent], O.[Completed], O.[LeadID], O.[CaseID], O.[CustomerID], O.[Version], O.[ExportVersion], O.[RecurrenceInfo], O.[DiaryAppointmentEventType], O.[ResourceInfo], O.[TempReminderTimeshiftID], O.[StatusID], O.[LabelID]
				FROM
				    [dbo].[DiaryAppointment] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[DiaryAppointmentID] = PageIndex.[DiaryAppointmentID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[DiaryAppointment] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_GetPaged] TO [sp_executeall]
GO
