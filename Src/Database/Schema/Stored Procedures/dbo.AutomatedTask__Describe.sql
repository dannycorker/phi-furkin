SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2008-05-29
-- Description:	Describe all attributes of an automated task
--				CPS 2013-09-10 - Added fn_C00_GetUrlByDatabase() to control URLs
--				CPS 2018-10-09 - Added BACS File Format
-- 2019-10-16 CPS for JIRA LPC-76 | Brought over from Aquarius433Dev to include BacsFileFormat work
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__Describe]
	@TaskID INT
AS
BEGIN

	DECLARE @FolderID INT, 
	@SqlQueryID INT

	SELECT at.*, tu.TimeUnitsName AS [Repeat Frequency]
	FROM dbo.AutomatedTask AT WITH (NOLOCK) 
	LEFT JOIN TimeUnits tu WITH (NOLOCK) ON tu.TimeUnitsID = at.RepeatTimeUnitsID
	WHERE at.TaskID = @TaskID

	SELECT atp.*, CASE atp.ParamName	WHEN 'SQL_QUERY' THEN dbo.fn_C00_GetUrlByDatabase() + 'iReporting2/ReportBuilder.aspx?qid=' + convert(varchar,sq.QueryID) + '&fid=' + convert(varchar,sq.FolderID) + '&a=Edit'
										WHEN 'LEADEVENT_TO_ADD' THEN CASE atp.ParamValue WHEN '0' THEN '' ELSE dbo.fn_C00_GetUrlByDatabase() + 'EventTypeEdit.aspx?MenuID=pnl5f&EventTypeID=' + atp.ParamValue END 
										WHEN 'RUN_AS_USERID' THEN 'aq 1,8,' + atp.ParamValue
										WHEN 'WORKFLOW_GROUP_ID' THEN 'aq 1,12' + atp.ParamValue
										ELSE ''
										END [Useful SQL - Editing URL, Aq Describe procs etc]
	FROM dbo.AutomatedTaskParam atp WITH (NOLOCK) 
	LEFT JOIN dbo.SqlQuery sq WITH (NOLOCK) on atp.ParamName = 'SQL_QUERY' and atp.ParamValue = convert(varchar,sq.QueryID)
	WHERE TaskID = @TaskID
	ORDER BY atp.AutomatedTaskParamID

	SELECT dbo.fn_C00_GetUrlByDatabase() + 'BatchJobEdit.aspx?TaskID=' + convert(varchar,@TaskID) [Edit URL]

	IF EXISTS ( SELECT *
	            FROM AutomatedTaskParam atp WITH ( NOLOCK ) 
				WHERE atp.TaskID = @TaskID
				AND atp.ParamName = 'BACS_FILE_FORMAT_ID'
				AND atp.ParamValue NOT IN ( '', '0' )
			  )
	BEGIN
		SELECT 'BACS File Format' [BACS File Format], bff.*
	    FROM AutomatedTaskParam atp WITH ( NOLOCK ) 
		INNER JOIN dbo.BacsFileFormat bff WITH ( NOLOCK ) on CONVERT(VARCHAR,bff.BacsFileFormatID) = atp.ParamValue
		WHERE atp.TaskID = @TaskID
		AND atp.ParamName = 'BACS_FILE_FORMAT_ID'
		AND atp.ParamValue NOT IN ( '', '0' )
	END

	SELECT * FROM dbo.AutomatedTaskInfo WITH (NOLOCK) WHERE TaskID = @TaskID

	SELECT @SqlQueryID = CONVERT(INT, a.ParamValue) 
	FROM dbo.AutomatedTaskParam a WITH (NOLOCK) 
	WHERE a.TaskID = @TaskID 
	AND a.ParamName = 'SQL_QUERY' 
	
	SELECT 'SqlQuery Info' AS [SQL Query TO Run], * 
	FROM dbo.SqlQuery s WITH (NOLOCK) 
	WHERE s.QueryID = @SqlQueryID

	SELECT 'Event Info' AS [EVENT TO ADD], e.*, d.DocumentTypeName, f.FolderName 
	FROM dbo.AutomatedTaskParam a WITH (NOLOCK) 
	INNER JOIN dbo.EventType e WITH (NOLOCK) ON a.ParamName = 'LEADEVENT_TO_ADD' AND CONVERT(INT, a.ParamValue) = e.EventTypeID 
	LEFT JOIN dbo.DocumentType d WITH (NOLOCK) ON d.DocumentTypeID = e.DocumentTypeID 
	LEFT JOIN dbo.DocumentTypeFolderLink df WITH (NOLOCK) ON df.DocumentTypeID = d.DocumentTypeID 
	LEFT JOIN dbo.Folder f WITH (NOLOCK) ON f.FolderID = df.FolderID 
	WHERE TaskID = @TaskID

	SELECT 'User Info' AS [Run AS USER], * 
	FROM dbo.AutomatedTaskParam a WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel c WITH (NOLOCK) ON a.ParamName = 'RUN_AS_USERID' AND CONVERT(INT, a.ParamValue) = c.ClientPersonnelID
	WHERE TaskID = @TaskID

	SELECT TOP 1 'Last Result' AS [LAST RESULT], * 
	FROM dbo.AutomatedTaskResult a WITH (NOLOCK) 
	WHERE TaskID = @TaskID
	ORDER BY AutomatedTaskResultID DESC

	SELECT 'Triggered By Event' AS [Triggered BY EVENT], etat.* , et.EventTypeName
	FROM dbo.EventTypeAutomatedTask etat WITH (NOLOCK) 
	INNER JOIN EventType et WITH ( NOLOCK ) on et.EventTypeID = etat.EventTypeID
	WHERE etat.AutomatedTaskID = @TaskID

	SELECT @FolderID = FolderID
	FROM dbo.SqlQuery s WITH (NOLOCK) 
	WHERE s.QueryID = @SqlQueryID 

	EXEC dbo.folder__describe @FolderID
	
	EXEC dbo.SplitString @SqlQueryID 
	
	EXEC dbo.atr @TaskID 
	
	SELECT top(50) * 
	FROM dbo.AutomatedTaskHistory ath WITH (NOLOCK) 
	WHERE ath.TaskID = @TaskID
	ORDER BY ath.TaskHistoryID desc
	
	/* Automated Task Command Control section */
	IF EXISTS(SELECT * FROM dbo.AutomatedTaskCommandControl ac WITH (NOLOCK) WHERE ac.TaskID = @TaskID)
	BEGIN
		SELECT * 
		FROM dbo.AutomatedTaskCommandControl ac WITH (NOLOCK) 
		WHERE ac.TaskID = @TaskID

		SELECT cl.*
		FROM dbo.AutomatedTaskCommandControl ac WITH (NOLOCK) 
		INNER JOIN dbo.AutomatedTaskCommandLine cl WITH (NOLOCK) ON cl.AutomatedTaskCommandControlID = ac.AutomatedTaskCommandControlID
		WHERE ac.TaskID = @TaskID
		ORDER BY cl.AutomatedTaskCommandControlID, cl.LineType, cl.LineOrder
	END
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__Describe] TO [sp_executehelper]
GO
