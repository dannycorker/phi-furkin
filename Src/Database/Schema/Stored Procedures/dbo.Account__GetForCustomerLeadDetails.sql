SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-03-12
-- Description:	Gets the required accounts for a given customer id
-- =============================================
CREATE PROCEDURE [dbo].[Account__GetForCustomerLeadDetails]	
	@CustomerID INT,
	@LeadID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT * 
	FROM Account a WITH (NOLOCK) 
	WHERE a.ClientID=@ClientID 
	AND a.CustomerID=@CustomerID 
	AND a.Active=1
	AND a.AccountUseID = 1 /*Premium*/
	ORDER BY a.WhenCreated DESC
	
END
GO
GRANT EXECUTE ON  [dbo].[Account__GetForCustomerLeadDetails] TO [sp_executeall]
GO
