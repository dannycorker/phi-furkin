SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-07-27
-- Description:	Copy event choices from one event to another
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyEventChoices]
@FromEventTypeID int,
@ToEventTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO EventChoice (EventTypeID, Description, NextEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting)
	SELECT @ToEventTypeID, Description, NextEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting
	FROM EventChoice ec WITH (NOLOCK)
	WHERE ec.EventTypeID = @FromEventTypeID

	INSERT INTO EventChoice (EventTypeID, Description, NextEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting)
	SELECT EventTypeID, Description, @ToEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting
	FROM EventChoice ec WITH (NOLOCK)
	WHERE ec.NextEventTypeID = @FromEventTypeID


END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyEventChoices] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyEventChoices] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyEventChoices] TO [sp_executeall]
GO
