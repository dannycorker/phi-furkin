SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2011
-- Description:	Gets the service names so that Ultra can recognise the incoming call
-- =============================================
CREATE PROCEDURE [dbo].[__GetServiceNamesForUltra]
	@ClientID INT
AS
BEGIN

-- Looks up the third party field mapping for the service name field and the service phone number field
-- These fields will be columns in a services basic table

DECLARE @ServiceNameDetailFieldID INT
DECLARE @ServiceNumberDetailFieldID INT

SELECT @ServiceNameDetailFieldID = ColumnFieldID 
FROM dbo.ThirdPartyFieldMapping WITH (NOLOCK) WHERE ThirdPartyFieldID=99 AND ClientID=@ClientID

SELECT @ServiceNumberDetailFieldID = ColumnFieldID
FROM dbo.ThirdPartyFieldMapping WITH (NOLOCK) WHERE ThirdPartyFieldID=100 AND ClientID=@ClientID

-- Retrieves the service name and number table data 

SELECT tdv.DetailValue AS ServiceName, tdv2.DetailValue AS PhoneNumber 
FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
INNER JOIN TableDetailValues tdv2 WITH(NOLOCK) ON tdv.TableRowID = tdv2.TableRowID AND tdv2.DetailFieldID=@ServiceNumberDetailFieldID
WHERE tdv.ClientID=@ClientID AND tdv.DetailFieldID=@ServiceNameDetailFieldID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__GetServiceNamesForUltra] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetServiceNamesForUltra] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetServiceNamesForUltra] TO [sp_executeall]
GO
