SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-04-2013
-- Description:	Gets the SMSQuestionEventType for the given eventype and leadtype
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestionEventType__GetByEventTypeIDAndLeadTypeID] 

	@EventTypeID INT,
	@LeadTypeID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT qet.*,dt.DocumentTypeID, lt.LeadTypeID, lt.LeadTypeName 
	FROM SMSQuestionEventType qet WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = qet.EventTypeID
	INNER JOIN dbo.DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID=et.DocumentTypeID
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = et.LeadTypeID
	WHERE lt.LeadTypeID=@LeadTypeID AND qet.EventTypeID=@EventTypeID


END



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionEventType__GetByEventTypeIDAndLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionEventType__GetByEventTypeIDAndLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionEventType__GetByEventTypeIDAndLeadTypeID] TO [sp_executeall]
GO
