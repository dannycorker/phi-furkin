SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireParts table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireParts_GetByPartNameID]
(

	@PartNameID int   
)
AS


				SELECT
					[PartNameID],
					[PartName]
				FROM
					[dbo].[QuestionnaireParts] WITH (NOLOCK) 
				WHERE
										[PartNameID] = @PartNameID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_GetByPartNameID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireParts_GetByPartNameID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_GetByPartNameID] TO [sp_executeall]
GO
