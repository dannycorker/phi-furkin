SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeHelperField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeHelperField_Insert]
(

	@EventTypeHelperFieldID int    OUTPUT,

	@EventTypeID int   ,

	@DetailFieldID int   ,

	@LeadTypeID int   ,

	@ClientID int   ,

	@ForceEditableOverride bit   
)
AS


				
				INSERT INTO [dbo].[EventTypeHelperField]
					(
					[EventTypeID]
					,[DetailFieldID]
					,[LeadTypeID]
					,[ClientID]
					,[ForceEditableOverride]
					)
				VALUES
					(
					@EventTypeID
					,@DetailFieldID
					,@LeadTypeID
					,@ClientID
					,@ForceEditableOverride
					)
				-- Get the identity value
				SET @EventTypeHelperFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Insert] TO [sp_executeall]
GO
