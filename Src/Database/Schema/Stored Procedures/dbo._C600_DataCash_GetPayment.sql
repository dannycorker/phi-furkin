SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-02-06
-- Description:	Gets the DataCash payment row
-- =============================================
CREATE PROCEDURE [dbo].[_C600_DataCash_GetPayment]
(
	@MatterID INT
)
AS
BEGIN

	DECLARE @Amount MONEY,
			@TableRowID INT

	-- Add CP Collections Row
	EXEC @TableRowID=_C600_Collections_AddCPCollectionsRow @MatterID, 0
	
	-- Get amount
	SELECT @Amount=ValueMoney 
	FROM TableDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID=175468 AND TableRowID=@TableRowID

	SELECT @TableRowID AS TableRowID, @Amount AS Amount	

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DataCash_GetPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_DataCash_GetPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DataCash_GetPayment] TO [sp_executeall]
GO
