SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Validate BuyPolicy Request
-- Modified   : 28/11/2014
-- By         : Jan Wilson
-- Comments   : Remove requirement for home phone number  
-- MODS:  
--	2015-05-14 DCM Ticket #31733 Entire contents moved to new proc to facilitate renaming in prelive
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_BuyPolicy_Validate]
(
	@XmlRequest XML,
	@XmlResponse XML OUTPUT,
	@Result INT OUTPUT
)
AS
BEGIN

	EXEC _C600_PA_Policy_BuyPolicy_Validate @XmlRequest, @XmlResponse OUTPUT, @Result OUTPUT
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_BuyPolicy_Validate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_BuyPolicy_Validate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_BuyPolicy_Validate] TO [sp_executeall]
GO
