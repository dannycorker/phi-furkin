SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[Lead_AssignInBatchByLeadType]

@NumRows int, --How many leads to assign
@ClientID int, --ClientID to use
@AssignTo int, --User to assign leads to
@AssignBy int, --User who is assigning these leads
@AssignDate varchar(100), --Date these leads are being assigned
@LeadTypeID int --only for this LeadType

AS

DECLARE @AffectedRowCount int

--Create a temporary table to hold the results
CREATE TABLE #TempAssign (LeadID int NOT NULL)
--Restrict the number of leads to Assign
SET ROWCOUNT @NumRows
--Insert the relevant set of records into the temp table
INSERT INTO #TempAssign (LeadID)
	SELECT		Lead.LeadID
	FROM		Lead
	WHERE		(ClientID = @ClientID)
	AND			(AquariumStatusID = 2)
	AND			(Assigned = 0 OR Assigned IS NULL)
	AND			(LeadTypeID = @LeadTypeID)
	ORDER BY	LeadID DESC

UPDATE Lead
SET
Lead.Assigned = 1,
Lead.AssignedTo = @AssignTo,
Lead.AssignedBy = @AssignBy,
Lead.AssignedDate = CONVERT(datetime, @AssignDate, 103)
FROM #TempAssign
WHERE Lead.LeadID = #TempAssign.LeadID


SELECT @AffectedRowCount = @@ROWCOUNT

--Finally drop the temp table
DROP TABLE #TempAssign

SELECT @AffectedRowCount

--SET ROWCOUNT 0



GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_AssignInBatchByLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_AssignInBatchByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_AssignInBatchByLeadType] TO [sp_executeall]
GO
