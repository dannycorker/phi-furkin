SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeMandatoryEvent table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeMandatoryEventID],
					[ClientID],
					[LeadTypeID],
					[ToEventTypeID],
					[RequiredEventID]
				FROM
					[dbo].[EventTypeMandatoryEvent] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_GetByClientID] TO [sp_executeall]
GO
