SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailValueHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_Delete]
(

	@DetailValueHistoryID int   
)
AS


				DELETE FROM [dbo].[DetailValueHistory] WITH (ROWLOCK) 
				WHERE
					[DetailValueHistoryID] = @DetailValueHistoryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Delete] TO [sp_executeall]
GO
