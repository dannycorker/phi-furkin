SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-02-02
-- Description:	Handle response from create customer
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BrainTree_HandleXMLPaymentMethodCreateRequest]
	@MatterID INT,
	@ClientPersonnelID INT,
	@XMLPayload VARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @BrainTreePaymentMethodCreateID INT

	INSERT INTO dbo.BrainTreePaymentMethodCreate ([MatterID], [RequestXML], [ResponseXML], [WhoCreated], [WhenCreated])
	VALUES (@MatterID, @XMLPayload, NULL, @ClientPersonnelID, dbo.fn_GetDate_Local())

	SELECT @BrainTreePaymentMethodCreateID = SCOPE_IDENTITY()

	SELECT @BrainTreePaymentMethodCreateID AS [BrainTreePaymentMethodCreateID]

END
GO
GRANT EXECUTE ON  [dbo].[_C600_BrainTree_HandleXMLPaymentMethodCreateRequest] TO [sp_executeall]
GO
