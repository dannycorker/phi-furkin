SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-06
-- Description:	Merge Aquarium resource lists with those for the specific ClientID passed in
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldPages__GetResourceLists]
(
	@ClientID int, 
	@ShowDisabled bit = 0  -- Only show Enabled lists by default
)
AS
BEGIN

	SELECT dfp.*
	FROM
		dbo.fnDetailFieldPagesShared(@ClientID) dfp
	WHERE
		([ClientID] = @ClientID OR [ClientID] = 0)
	AND ([ResourceList] = 1)
	AND ([Enabled] = 1 OR @ShowDisabled = 1)

	ORDER BY 
		ClientID, [PageName] 
				

	SELECT @@ROWCOUNT

END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetResourceLists] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetResourceLists] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetResourceLists] TO [sp_executeall]
GO
