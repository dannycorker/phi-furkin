SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-16
-- Description:	Searches the ailment resource list for matches
-- Used by:		DF 146353
-- Modified:	ROH	2014-07-04 Include diagnosis / presenting complaint indication
--				SB	2014-11-24 For 384 show the friendly name
--				ROH 2015-01-07 Search the friendly name too Zen#30388 
--				ROH 2015-01-20 Variable record set size Zen#30388
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Ailment_SearchAilmentResourceList] 
(
	@Search VARCHAR(2000),
	@ClientID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
		@top INT
		,@dSQL NVARCHAR(MAX)
	
	SELECT @top = CASE WHEN LEN(@search) < 3 THEN 50 ELSE 200 END
	
	SELECT TOP (@top) 
		rl.ResourceListID
		, rdvAilment1.DetailValue + CASE rLabel.ValueInt WHEN 69718 THEN ' (D)' WHEN 69719 THEN ' (PC)' ELSE '' END AS Ailment1
		,	CASE 
				WHEN @ClientID IN (384) THEN rdvFriendly.DetailValue
				ELSE rdvAilment2.DetailValue 
			END AS Ailment2
	FROM dbo.ResourceList rl WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvAilment1 WITH (NOLOCK) ON rl.ResourceListID = rdvAilment1.ResourceListID AND rdvAilment1.DetailFieldID = 144340
	INNER JOIN dbo.DetailFields dfAilment1 WITH (NOLOCK) ON rdvAilment1.DetailFieldID = dfAilment1.DetailFieldID 
	INNER JOIN dbo.ResourceListDetailValues rdvAilment2 WITH (NOLOCK) ON rl.ResourceListID = rdvAilment2.ResourceListID AND rdvAilment2.DetailFieldID = 144341
	LEFT JOIN dbo.ResourceListDetailValues rLabel WITH (NOLOCK) on rLabel.ResourceListID = rl.ResourceListID and rLabel.DetailFieldID = 170014
	LEFT JOIN dbo.ResourceListDetailValues rdvFriendly WITH (NOLOCK) ON rl.ResourceListID = rdvFriendly.ResourceListID AND rdvFriendly.DetailFieldID = 170230
	WHERE dfAilment1.DetailFieldPageID = 16167
	AND (
		rdvAilment1.DetailValue LIKE '%' + @Search + '%' 
		--OR rdvAilment2.DetailValue LIKE '%' + @Search + '%'
		OR rdvFriendly.DetailValue LIKE '%' + @Search + '%'
		)
	AND rl.ClientID = @ClientID
	ORDER BY rdvAilment1.DetailValue
	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Ailment_SearchAilmentResourceList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Ailment_SearchAilmentResourceList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Ailment_SearchAilmentResourceList] TO [sp_executeall]
GO
