SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailValueHistory table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailValueHistoryID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[LeadID],
					[MatterID],
					[FieldValue],
					[WhenSaved],
					[ClientPersonnelID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ContactID],
					[ClientPersonnelDetailValueID]
				FROM
					[dbo].[DetailValueHistory] WITH (NOLOCK) 
				WHERE
					[CustomerID] = @CustomerID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_GetByCustomerID] TO [sp_executeall]
GO
