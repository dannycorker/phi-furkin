SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferRestrictedField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField__GetGridByClientRelationshipID]
(
	@ClientRelationshipID	int
)
AS

	SELECT f.DetailFieldID, cl.ClientID, f.FieldName 
	FROM DetailFields f
	INNER JOIN 	CaseTransferRestrictedField c on f.DetailFieldID = c.DetailFieldID 
	INNER JOIN 	Clients cl on f.ClientID = cl.ClientID
	WHERE 
		c.ClientRelationshipID	= @ClientRelationshipID 


	Select @@ROWCOUNT
					
			







GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField__GetGridByClientRelationshipID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField__GetGridByClientRelationshipID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField__GetGridByClientRelationshipID] TO [sp_executeall]
GO
