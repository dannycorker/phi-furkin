SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-03-26
-- Description:	Get working days around the current date
-- =============================================
CREATE PROCEDURE [dbo].[wd]
	@FutureDays int = 14, 
	@PastDays int = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT wd.CharDate, DATENAME(WEEKDAY, wd.CharDate), m.Month3Text, wd.Day, 
	CASE 
		WHEN wd.IsBankHoliday = 1 THEN 'BANK HOLIDAY' 
		WHEN wd.IsWeekDay = 0 THEN 'WEEKEND' 
		ELSE 'Work'
	END AS [TypeOfDay]
	FROM dbo.WorkingDays wd WITH (NOLOCK) 
	INNER JOIN dbo.Months m WITH (NOLOCK) ON m.MonthID = wd.Month 
	WHERE wd.Date BETWEEN (dbo.fnDateOnly(dbo.fn_GetDate_Local() - @PastDays)) AND (dbo.fnDateOnly(dbo.fn_GetDate_Local()) + @FutureDays)
	ORDER BY wd.Date ASC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[wd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[wd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[wd] TO [sp_executeall]
GO
