SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 24/11/2016
-- Description:	Gets a card transaction
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__GetByCardTransactionID]
	@CardTransactionID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	EXEC _C00_LogIt 'Info','CardTransaction__GetByCardTransactionID','CardTransactionID = ',@CardTransactionID,0
	
	SELECT * FROM CardTransaction WITH (NOLOCK) WHERE CardTransactionID=@CardTransactionID
    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__GetByCardTransactionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__GetByCardTransactionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__GetByCardTransactionID] TO [sp_executeall]
GO
