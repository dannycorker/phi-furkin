SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the IncendiaConfigurationAquarium table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationAquarium_Find]
(

	@SearchUsingOR bit   = null ,

	@IncendiaConfigurationAquariumID int   = null ,

	@AccountID varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [IncendiaConfigurationAquariumID]
	, [AccountID]
    FROM
	[dbo].[IncendiaConfigurationAquarium] WITH (NOLOCK) 
    WHERE 
	 ([IncendiaConfigurationAquariumID] = @IncendiaConfigurationAquariumID OR @IncendiaConfigurationAquariumID IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [IncendiaConfigurationAquariumID]
	, [AccountID]
    FROM
	[dbo].[IncendiaConfigurationAquarium] WITH (NOLOCK) 
    WHERE 
	 ([IncendiaConfigurationAquariumID] = @IncendiaConfigurationAquariumID AND @IncendiaConfigurationAquariumID is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationAquarium_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Find] TO [sp_executeall]
GO
