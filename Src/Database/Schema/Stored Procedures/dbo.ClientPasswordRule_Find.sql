SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPasswordRule table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPasswordRule_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPasswordRuleID int   = null ,

	@ClientID int   = null ,

	@MinLength int   = null ,

	@MinLowerCase int   = null ,

	@MinUpperCase int   = null ,

	@MinNumber int   = null ,

	@MinSymbol int   = null ,

	@ExpiryDays int   = null ,

	@ResetForcesChange bit   = null ,

	@PasswordHistoryCount int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPasswordRuleID]
	, [ClientID]
	, [MinLength]
	, [MinLowerCase]
	, [MinUpperCase]
	, [MinNumber]
	, [MinSymbol]
	, [ExpiryDays]
	, [ResetForcesChange]
	, [PasswordHistoryCount]
    FROM
	[dbo].[ClientPasswordRule] WITH (NOLOCK) 
    WHERE 
	 ([ClientPasswordRuleID] = @ClientPasswordRuleID OR @ClientPasswordRuleID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([MinLength] = @MinLength OR @MinLength IS NULL)
	AND ([MinLowerCase] = @MinLowerCase OR @MinLowerCase IS NULL)
	AND ([MinUpperCase] = @MinUpperCase OR @MinUpperCase IS NULL)
	AND ([MinNumber] = @MinNumber OR @MinNumber IS NULL)
	AND ([MinSymbol] = @MinSymbol OR @MinSymbol IS NULL)
	AND ([ExpiryDays] = @ExpiryDays OR @ExpiryDays IS NULL)
	AND ([ResetForcesChange] = @ResetForcesChange OR @ResetForcesChange IS NULL)
	AND ([PasswordHistoryCount] = @PasswordHistoryCount OR @PasswordHistoryCount IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPasswordRuleID]
	, [ClientID]
	, [MinLength]
	, [MinLowerCase]
	, [MinUpperCase]
	, [MinNumber]
	, [MinSymbol]
	, [ExpiryDays]
	, [ResetForcesChange]
	, [PasswordHistoryCount]
    FROM
	[dbo].[ClientPasswordRule] WITH (NOLOCK) 
    WHERE 
	 ([ClientPasswordRuleID] = @ClientPasswordRuleID AND @ClientPasswordRuleID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([MinLength] = @MinLength AND @MinLength is not null)
	OR ([MinLowerCase] = @MinLowerCase AND @MinLowerCase is not null)
	OR ([MinUpperCase] = @MinUpperCase AND @MinUpperCase is not null)
	OR ([MinNumber] = @MinNumber AND @MinNumber is not null)
	OR ([MinSymbol] = @MinSymbol AND @MinSymbol is not null)
	OR ([ExpiryDays] = @ExpiryDays AND @ExpiryDays is not null)
	OR ([ResetForcesChange] = @ResetForcesChange AND @ResetForcesChange is not null)
	OR ([PasswordHistoryCount] = @PasswordHistoryCount AND @PasswordHistoryCount is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPasswordRule_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Find] TO [sp_executeall]
GO
