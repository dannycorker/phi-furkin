SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-24
-- Description:	Get all rights for this User
-- =============================================
CREATE PROCEDURE [dbo].[GetUserRights]
	-- Add the parameters for the stored procedure here
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @GroupID int

	SELECT @GroupID = ClientPersonnelAdminGroupID
	FROM ClientPersonnel 
	WHERE ClientPersonnelID = @UserID

    -- Insert statements for procedure here
	select functiontypeid, leadtypeid, objectid, rightid 
	from grouprightsdynamic 
	where clientpersonneladmingroupid = @GroupID
	and not exists (select * from userrightsdynamic where userrightsdynamic.functiontypeid = grouprightsdynamic.functiontypeid and isnull(userrightsdynamic.leadtypeid,-1) = isnull(grouprightsdynamic.leadtypeid, -1) and userrightsdynamic.objectid = grouprightsdynamic.objectid and userrightsdynamic.clientpersonnelid = @UserID)
	union
	select functiontypeid, leadtypeid, objectid, rightid 
	from userrightsdynamic 
	where clientpersonnelid = @UserID
	/*union
	select functionid, -1, -1, rightid 
	from grouprights 
	where clientpersonneladmingroupid = @GroupID
	and not exists (select * from userrights where userrights.functionid = grouprights.functionid and userrights.clientpersonnelid = @UserID)
	union
	select functionid, -1, -1, rightid 
	from userrights 
	where clientpersonnelid = @UserID*/

END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserRights] TO [sp_executeall]
GO
