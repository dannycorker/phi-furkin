SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetMaximumQuestionOrder] @ClientQuestionnaireID int

As

SELECT     ClientQuestionnaireID, MAX(QuestionOrder) AS MaximumQuestionOrderValue
FROM         dbo.MasterQuestions
GROUP BY ClientQuestionnaireID
HAVING      (ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetMaximumQuestionOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMaximumQuestionOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMaximumQuestionOrder] TO [sp_executeall]
GO
