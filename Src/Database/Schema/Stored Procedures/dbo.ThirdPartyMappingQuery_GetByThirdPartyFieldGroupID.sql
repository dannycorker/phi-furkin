SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_GetByThirdPartyFieldGroupID]
(

	@ThirdPartyFieldGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyMappingQueryID],
					[ClientID],
					[ThirdPartySystemID],
					[ThirdPartyFieldGroupID],
					[QueryID],
					[Method],
					[ActionURI],
					[SchemaName],
					[BaseIndex]
				FROM
					[dbo].[ThirdPartyMappingQuery] WITH (NOLOCK) 
				WHERE
					[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_GetByThirdPartyFieldGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_GetByThirdPartyFieldGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_GetByThirdPartyFieldGroupID] TO [sp_executeall]
GO
