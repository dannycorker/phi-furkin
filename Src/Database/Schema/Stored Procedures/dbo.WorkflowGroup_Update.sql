SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the WorkflowGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_Update]
(

	@WorkflowGroupID int   ,

	@Name varchar (100)  ,

	@Description varchar (255)  ,

	@ManagerID int   ,

	@ClientID int   ,

	@Enabled bit   ,

	@SortOrder varchar (4)  ,

	@AssignedLeadsOnly bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[WorkflowGroup]
				SET
					[Name] = @Name
					,[Description] = @Description
					,[ManagerID] = @ManagerID
					,[ClientID] = @ClientID
					,[Enabled] = @Enabled
					,[SortOrder] = @SortOrder
					,[AssignedLeadsOnly] = @AssignedLeadsOnly
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[WorkflowGroupID] = @WorkflowGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Update] TO [sp_executeall]
GO
