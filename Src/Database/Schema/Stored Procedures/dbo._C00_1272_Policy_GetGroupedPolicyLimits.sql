SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-01-31
-- Description:	Gets the policy limits for the selected policy sections for a set of grouped claims
-- Used by:		Calculation
-- UPDATES:		ROH	2014-07-02	Pet Type selection
--				SB	2015-10-21	Change discussed with Robin... don't pass any non financial limits in as these are handled elsewhere.  Age when creating the rows and occurance limits manually.
--								Look up the optional coverages from the policy history row and then pull them in from the appropriate policy
--				JL 2016-06-13   Added default 1 paramter for 'IsForScarfDisplayOnly' so subsections with the same insured amount as thier parent section are not displayed. Amended other proc calls for calcs to call with 0
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetGroupedPolicyLimits] 
(
	@MatterID INT,
	@CurrentPolicyMatterID INT = NULL,
	@IsForScarfDisplayOnly BIT = 1 
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PolicyLeadID INT,
			@BirthDate DATE,
			@AgeInMonths INT,
			@AgeInYears INT,
			@PercentageDeduction INT,
			@PurchasePrice NUMERIC(18,2),
			@MaxPayout NUMERIC(18,2)

	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)

	IF @CurrentPolicyMatterID IS NULL
	BEGIN
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	END

	DECLARE @PolicyPetType INT, 
			@PreBreedRL VARCHAR(100)

	SELECT @PolicyPetType = rldvPetType.ValueInt, @PreBreedRL = ldvPetType.DetailValue
	FROM Lead l WITH (NOLOCK)
	INNER JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK) on ldvPetType.LeadID = l.LeadID and ldvPetType.DetailFieldID = 144272 /*Pet Type*/
	INNER JOIN dbo.ResourceListDetailValues rldvPetType WITH (NOLOCK) on rldvPetType.ResourceListID = ldvPetType.ValueInt and rldvPetType.DetailFieldID = 144269 /*Pet Type*/
	WHERE l.LeadID = @PolicyLeadID
	
	-- Now we need to see if there are any optional coverages
	DECLARE	@DateToUse DATE  
	SELECT	@DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@MatterID, NULL, NULL)  

	-- Now find the matching table row and get the coverages   
	DECLARE @OptionalCoverages VARCHAR(2000)
	SELECT @OptionalCoverages = tdvCover.DetailValue
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r  
	INNER JOIN dbo.TableDetailValues tdvCover WITH (NOLOCK) ON r.TableRowID = tdvCover.TableRowID AND tdvCover.DetailFieldID = 175737 /*Opt Cover List*/
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663 /*Start*/
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664 /*End*/
	WHERE @DateToUse >= tdvStart.ValueDate  
	AND @DateToUse <= tdvEnd.ValueDate  
	
	DECLARE @Output TABLE (ResourceListID INT, Section VARCHAR(1000),SubSection VARCHAR(1000),SumInsured DECIMAL (18,2), AllowedCount INT,LimitTypeID INT, LimitType VARCHAR(1000))
	
	INSERT INTO @Output (ResourceListID,Section,SubSection,SumInsured,AllowedCount,LimitTypeID,LimitType) 
	SELECT DISTINCT s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, tdvSumInsured.ValueMoney AS SumInsured,
					tdvCount.ValueInt AS AllowedCount, tdvLimitType.ValueInt AS LimitTypeID, llType.ItemValue AS LimitType
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350 /*Policy Section*/
	INNER JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190 /*Sub-Section*/
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON s.Out_ResourceListID = tdvRL.ResourceListID AND tdvRL.DetailFieldID = 144357 /*Policy Section*/
	INNER JOIN dbo.TableDetailValues tdvSumInsured WITH (NOLOCK) ON tdvRL.TableRowID = tdvSumInsured.TableRowID AND tdvSumInsured.DetailFieldID = 144358 /*Sum Insured*/
	LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 170013 /*Pet Type*/
	LEFT JOIN dbo.TableDetailValues tdvCount WITH (NOLOCK) ON tdvRL.TableRowID = tdvCount.TableRowID AND tdvCount.DetailFieldID = 144267 /*Non financial limit*/
	LEFT JOIN dbo.TableDetailValues tdvLimitType WITH (NOLOCK) ON tdvRL.TableRowID = tdvLimitType.TableRowID AND tdvLimitType.DetailFieldID = 175388 /*Custom limit type*/
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON tdvLimitType.ValueInt = llType.LookupListItemID
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) m ON r.MatterID = m.MatterID
	WHERE tdvRL.MatterID = @CurrentPolicyMatterID
	AND (tdvPetType.ValueInt IS NULL OR tdvPetType.ValueInt IN (0, @PolicyPetType))
	
	UNION
	
	/*Optional Coverages*/
	SELECT DISTINCT s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, c.SumInsured,
					c.AllowedCount, c.LimitTypeID, c.LimitType	
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350 /*Policy Section*/
	INNER JOIN dbo.fn_C00_1272_GetOptionalSectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190 /*Sub-Section*/
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.fn_C600_Scheme_GetOptionalCoverages(@CurrentPolicyMatterID) c ON s.Out_ResourceListID = c.ResourceListID
	INNER JOIN dbo.fnTableOfIDsFromCSV(@OptionalCoverages) id ON c.GroupID = id.AnyID
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) m ON r.MatterID = m.MatterID 
	WHERE (c.PetTypeID IS NULL OR c.PetTypeID IN (0, @PolicyPetType))

	IF @IsForScarfDisplayOnly = 1 /*For PHI, don't display subsections where the limit = the section limit for scarf display only*/ 
	BEGIN
		
		DELETE o FROM @Output o 
		where o.SubSection <> '-'
		and EXISTS (SELECT * FROM @Output op 
			where op.SumInsured = o.SumInsured
			and op.SubSection = '-' 
			and op.Section = o.Section) 
			
	END

	IF EXISTS (
		SELECT *
		FROM @Output o
		WHERE o.ResourceListID IN (148151, 151556,148148)
	)
	BEGIN
	
		SELECT @BirthDate = ValueDate
		FROM dbo.LeadDetailValues WITH (NOLOCK) 
		WHERE LeadID = @PolicyLeadID
		AND DetailFieldID = 144274

		SELECT @AgeInMonths =	CASE 
									WHEN DATEPART(DAY, @BirthDate) > DATEPART(DAY, @DateToUse)
									THEN DATEDIFF(MONTH, @BirthDate, @DateToUse) - 1
									ELSE DATEDIFF(MONTH, @BirthDate, @DateToUse)
								END

		SELECT @AgeInYears = @AgeInMonths / 12

		SELECT TOP 1 @PercentageDeduction = tdv_percentageDed.ValueMoney
		FROM TableDetailValues tdv_minAge WITH (NOLOCK)
		INNER JOIN TableDetailValues tdv_maxAge WITH (NOLOCK) ON tdv_maxAge.TableRowID = tdv_minAge.TableRowID and tdv_maxAge.DetailFieldID = 180046
		INNER JOIN TableDetailValues tdv_percentageDed WITH (NOLOCK) ON tdv_percentageDed.TableRowID = tdv_minAge.TableRowID and tdv_percentageDed.DetailFieldID = 180047
		LEFT JOIN TableDetailValues tdv_designatedBreed WITH (NOLOCK) ON tdv_designatedBreed.TableRowID = tdv_minAge.TableRowID and tdv_designatedBreed.DetailFieldID = 180050
		WHERE tdv_minAge.MatterID = @CurrentPolicyMatterID
		AND tdv_minAge.DetailFieldID = 180045
		AND tdv_minAge.ValueInt <=  @AgeInYears
		AND tdv_maxAge.ValueInt > @AgeInYears
		AND (tdv_designatedBreed.DetailValue LIKE '%' + @PreBreedRL + '%' OR tdv_designatedBreed.DetailValue = ''  OR tdv_designatedBreed.DetailValue IS NULL)
		ORDER BY tdv_designatedBreed.DetailValue DESC

		/*2018-07-19 ACE - This is actually supposed to be the claim value?!?*/
		SELECT @PurchasePrice = dbo.fnGetSimpleDvAsMoney (144339, @PolicyLeadID)
		
		IF @PurchasePrice IS NULL 
		BEGIN 
			/*If there is no purchase price, set it to the default, when/if i have more time we should put these to client level or similar*/ 
			SELECT @PurchasePrice = CASE @PolicyPetType WHEN 42989 THEN 150.00 ELSE 75.00 END  
		
		END
		
		-- check the current policy limits 

		--SELECT @PurchasePrice = tdv_Claim.ValueMoney
		--FROM TableDetailValues tdv_polSec WITH (NOLOCK)
		--INNER JOIN TableDetailValues tdv_Claim WITH (NOLOCK) ON tdv_Claim.TableRowID = tdv_polSec.TableRowID AND tdv_Claim.DetailFieldID = 144353
		--WHERE tdv_polSec.MatterID = @MatterID
		--AND tdv_polSec.DetailFieldID = 144350
		--AND tdv_polSec.ResourceListID IN (148151, 151556,148148)

		PRINT '@AgeInYears'
		PRINT @AgeInYears

		PRINT '@PurchasePrice'
		PRINT @PurchasePrice

		PRINT '@PercentageDeduction'
		PRINT @PercentageDeduction

		IF @PercentageDeduction IS NOT NULL
		BEGIN

			/*2018-07-23 Use the new cap or collar function to cap the value of the pay out to the sum insured.*/
			UPDATE o
			SET SumInsured = dbo.fn_C00_SimpleCapOrCollar(CASE WHEN dbo.fnGetSimpleDvAsInt(180049, @CurrentPolicyMatterID) = 1 THEN 
				@PurchasePrice - (@PurchasePrice * (@PercentageDeduction/100.00))
				ELSE
				SumInsured 
				END, SumInsured, 1)
			FROM @Output o
			WHERE o.ResourceListID IN (148151, 151556, 148148) 

		END

	END

	SELECT * 
	FROM @Output

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyLimits] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyLimits] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyLimits] TO [sp_executeall]
GO
