SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2012
-- Description:	Inserts an UploadedFileAttachment record
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFileAttachment__Insert]
		
	@UploadedFileID INT,
	@EventTypeID INT

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO UploadedFileAttachment
	(
		UploadedFileID, EventTypeID
	)
	VALUES
	(
		@UploadedFileID, @EventTypeID
	)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFileAttachment__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__Insert] TO [sp_executeall]
GO
