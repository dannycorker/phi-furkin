SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_Get_List]

AS


				
				SELECT
					[ClientRelationshipID],
					[ClientRelationshipName],
					[OutgoingClientID],
					[ReceivingClientID],
					[OutgoingLeadTypeID],
					[IncomingLeadTypeID],
					[OutgoingEventTypeID],
					[IncomingEventTypeID],
					[Enabled],
					[ClearLeadRefs]
				FROM
					[dbo].[ClientRelationship] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Get_List] TO [sp_executeall]
GO
