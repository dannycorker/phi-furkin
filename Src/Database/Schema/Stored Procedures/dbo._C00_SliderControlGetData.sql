SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-11-19
-- Description:	Get data for slider controls
--				Needs the number of fields to divide against, whether each is locked, the starting value, and the amount to divide
-- Updates:
-- 2013-11-12	SB	Changed input parameters and added title
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SliderControlGetData]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@SliderDetailFieldID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ValueInt		INT,
			@ValueToDivide	MONEY,
			@Title			VARCHAR(2000),
			@ClientID		INT
	
	DECLARE @Year1FieldID	INT,
			@Year2FieldID	INT,
			@Year3FieldID	INT,
			@Year4FieldID	INT,
			@Year5FieldID	INT,
			@Year6FieldID	INT,
			@Year7FieldID	INT,
			@YearFieldID	INT,
			@TermFieldID	INT,
			@Term			INT = 6
	
	DECLARE @SliderData TABLE ( SliderNumber INT, Locked BIT, Value MONEY, SliderName VARCHAR(2000) )

	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH (NOLOCK) 
	WHERE cu.CustomerID = @CustomerID
	
	IF @SliderDetailFieldID = 181923 /*Fee Slider*/
	BEGIN
		SELECT	
			@Year1FieldID	 = [725],
			@Year2FieldID	 = [726],
			@Year3FieldID	 = [727],
			@Year4FieldID	 = [728],
			@Year5FieldID	 = [729],
			@Year6FieldID	 = [730],
			@YearFieldID	 = [731],
			@Year7FieldID	 = [825],
			@TermFieldID	 = [733]
		FROM
			(	
			SELECT fm.ThirdPartyFieldID, fm.DetailFieldID
			FROM ThirdPartyFieldMapping fm WITH (NOLOCK) 
			WHERE fm.ThirdPartyFieldGroupID = 62
			and fm.ClientID = @ClientID
			)
			AS ToPivot		
		PIVOT		
		(		
			Max([DetailFieldID])
		FOR		
		[ThirdPartyFieldID]
			IN ( [731],[730],[729],[728],[727],[726],[725],[733],[825] )
		) AS Pivoted
		
		/*Special rules for 7-year terms*/
		SELECT @Term = ISNULL(li.ValueInt,6)
		FROM CustomerDetailValues cdv WITH (NOLOCK) 
		INNER JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = cdv.ValueInt
		WHERE cdv.DetailFieldID = @TermFieldID 
		and cdv.CustomerID = @CustomerID
	
		SELECT @ValueInt = cdv.ValueInt
		FROM CustomerDetailValues cdv WITH (NOLOCK) 
		WHERE cdv.CustomerID = @CustomerID 
		and cdv.DetailFieldID = @YearFieldID /*Year of plan*/
		
		SELECT @ValueToDivide = 100.00
		
		SELECT @ValueInt = ISNULL(@ValueInt,1)
	
		INSERT @SliderData (SliderNumber, Locked, Value, SliderName)
		SELECT SUBSTRING(df.FieldName,6,1), CASE WHEN SUBSTRING(df.FieldName,6,1) < @ValueInt THEN 1 ELSE 0 END, ISNULL(cdv.ValueMoney,0.00), LEFT(df.FieldName,6)
		FROM DetailFields df WITH (NOLOCK) 
		LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.DetailFieldID = df.DetailFieldID and cdv.CustomerID = @CustomerID
		WHERE df.DetailFieldID in(@Year1FieldID,@Year2FieldID,@Year3FieldID,@Year4FieldID,@Year5FieldID,@Year6FieldID) 
		or ( @Term = 7 and df.DetailFieldID = @Year7FieldID )

		SELECT @Title = 'Enter Details'
	END

	SELECT SliderNumber, Locked, Value, SliderName
	FROM @SliderData sd 
	
	SELECT ISNULL(@ValueToDivide,0.00) [ValueToDivide], @Title AS Title

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SliderControlGetData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SliderControlGetData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SliderControlGetData] TO [sp_executeall]
GO
