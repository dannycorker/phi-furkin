SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[ThirdPartySystemEvent__GetByMessageNameAndClientIDAndLeadTypeID]
(
	@MessageName VARCHAR(250),
	@ClientID int,
	@LeadTypeID int
)
AS


	SELECT *
	FROM
		[dbo].[ThirdPartySystemEvent]
	WHERE
		MessageName = @MessageName AND
		ClientID = @ClientID AND
		LeadTypeID = @LeadTypeID
		
	SELECT @@ROWCOUNT
		
			




GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByMessageNameAndClientIDAndLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent__GetByMessageNameAndClientIDAndLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByMessageNameAndClientIDAndLeadTypeID] TO [sp_executeall]
GO
