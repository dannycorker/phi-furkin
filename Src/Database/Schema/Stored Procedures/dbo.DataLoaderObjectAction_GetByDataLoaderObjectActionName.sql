SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderObjectAction table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionName]
(

	@DataLoaderObjectActionName varchar (250)  
)
AS


				SELECT
					[DataLoaderObjectActionID],
					[DataLoaderObjectActionName],
					[DataLoaderObjectActionDescription]
				FROM
					[dbo].[DataLoaderObjectAction] WITH (NOLOCK) 
				WHERE
										[DataLoaderObjectActionName] = @DataLoaderObjectActionName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionName] TO [sp_executeall]
GO
