SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderSectionLocaterType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterType_Insert]
(

	@DataLoaderSectionLocaterTypeID int   ,

	@LocaterTypeName varchar (250)  ,

	@LocaterTypeDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderSectionLocaterType]
					(
					[DataLoaderSectionLocaterTypeID]
					,[LocaterTypeName]
					,[LocaterTypeDescription]
					)
				VALUES
					(
					@DataLoaderSectionLocaterTypeID
					,@LocaterTypeName
					,@LocaterTypeDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Insert] TO [sp_executeall]
GO
