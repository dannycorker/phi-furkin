SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalUserGroupPanelItemChartingAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_Delete]
(

	@PortalUserGroupPanelItemChartingAccessID int   
)
AS


				DELETE FROM [dbo].[PortalUserGroupPanelItemChartingAccess] WITH (ROWLOCK) 
				WHERE
					[PortalUserGroupPanelItemChartingAccessID] = @PortalUserGroupPanelItemChartingAccessID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Delete] TO [sp_executeall]
GO
