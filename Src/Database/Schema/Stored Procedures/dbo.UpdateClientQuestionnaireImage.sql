SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.UpdateClientQuestionnaireImage    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[UpdateClientQuestionnaireImage]
@ClientQuestionnaireID int,
@QuestionnaireLogo image,
@QuestionnaireLogoFileName nvarchar(512)

AS

Update ClientQuestionnaires
SET QuestionnaireLogo = @QuestionnaireLogo,
QuestionnaireLogoFileName = @QuestionnaireLogoFileName
where ClientQuestionnaireID = @ClientQuestionnaireID




GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireImage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientQuestionnaireImage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireImage] TO [sp_executeall]
GO
