SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
CREATE PROC [dbo].[_C600_MI_ValidationRequests] 
(
@Mode INT = 0 -- 0=Open, 1=ThisMonth, 2=LastMonth, 3=ThisYear, other=AllTime
)
AS 
BEGIN

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	DECLARE @pMode INT = 0
	DECLARE @dSQL VARCHAR(MAX)
	DECLARE @PivotColumns VARCHAR(5000) = ''

	SELECT @pMode = @Mode -- SQL performance by not using parms in queries

	/* Setup Pivot Columns */
	SELECT @PivotColumns += ('[' + df.FieldName + '],')
			FROM DetailFields df WITH (NOLOCK) 
			INNER JOIN dbo.DetailFieldPages dfp on dfp.DetailFieldPageID = df.DetailFieldPageId
			where dfp.DetailFieldPageID = 16160
			order by df.FieldName
	SELECT @PivotColumns = LEFT(@PivotColumns,LEN(@PivotColumns)-1)		

	/* Build Dynamic SQL */
	SELECT @dSQL = '
	;WITH FraudFlags (MatterID, FieldID, FieldName, Checked) AS ( 
		SELECT mdvFF.MatterID, df.DetailFieldID, df.FieldName, mdvFF.ValueInt
		FROM MatterDetailValues mdvFF WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = mdvFF.DetailFieldID
		INNER JOIN dbo.DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID
		WHERE dfp.DetailFieldPageID = 16160
	)
	,ClaimData (MatterID, Affinity, Underwriter, CustomerSurname, CustomerPostcode, VetName, VetPostCode, ClaimValue, SLADays, ClientStatusID, Source, Status)
	AS (
	select 
		m.MatterID
		, rldvAff.DetailValue, lliUW.ItemValue
		, cu.LastName, cu.PostCode
		, rldvVetName.DetailValue, rldvVetPC.DetailValue
		, mdvClaimValue.ValueMoney, null, ca.ClientStatusID
		, CASE leSource.EventTypeID WHEN 119944 THEN ''Scan'' WHEN 121866 THEN ''Paper'' ELSE ''?'' END Source
		, ls.StatusName
	from Matter m WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
	INNER JOIN dbo.Customers cu WITH (NOLOCK) on cu.CustomerID = m.CustomerID
	INNER JOIN dbo.Cases ca WITH (NOLOCK) on ca.CaseID = m.CaseID
	INNER JOIN dbo.LeadStatus ls WITH (NOLOCK) on ls.StatusID = ca.ClientStatusID
	--Current Policy
	LEFT JOIN LeadDetailValues ldvCurPol WITH (NOLOCK) on ldvCurPol.LeadID = l.LeadID and ldvCurPol.DetailFieldID = 146193
	LEFT JOIN ResourceListDetailValues rldvAff WITH (NOLOCK) on rldvAff.ResourceListID = ldvCurPol.ValueInt and rldvAff.DetailFieldID = 144314
	LEFT JOIN ResourceListDetailValues rldvUW WITH (NOLOCK) on rldvUW.ResourceListID = ldvCurPol.ValueInt and rldvUW.DetailFieldID = 145661
	LEFT JOIN LookupListItems lliUW WITH (NOLOCK) on lliUW.LookupListItemID = rldvUW.ValueInt
	--Current Vet
	LEFT JOIN LeadDetailValues ldvCurVet WITH (NOLOCK) on ldvCurVet.LeadID = l.LeadID and ldvCurVet.DetailFieldID = 146215
	LEFT JOIN ResourceListDetailValues rldvVetName WITH (NOLOCK) on rldvVetName.ResourceListID = ldvCurVet.ValueInt and rldvVetName.DetailFieldID = 144473
	LEFT JOIN ResourceListDetailValues rldvVetPC WITH (NOLOCK) on rldvVetPC.ResourceListID = ldvCurVet.ValueInt and rldvVetPC.DetailFieldID = 144479
	--Claim
	LEFT JOIN MatterDetailValues mdvClaimValue WITH (NOLOCK) on mdvClaimValue.MatterID = m.MatterID and mdvClaimValue.DetailFieldID = 149850
	LEFT JOIN MatterDetailValues mdvClaimRecd WITH (NOLOCK) on mdvClaimRecd.MatterID = m.MatterID and mdvClaimRecd.DetailFieldID = 144520
	-- Source of claim form
	LEFT JOIN dbo.LeadEvent leSource WITH (NOLOCK) on leSource.CaseID = m.CaseID and leSource.EventTypeID in (119944,121843,121866,126460,126458)
	--Latest claim form event only
	WHERE m.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	AND NOT EXISTS (SELECT 1 FROM LeadEvent le2 WITH (NOLOCK) WHERE le2.CaseID = leSource.CaseID AND le2.LeadEventID > leSource.LeadEventID AND le2.EventTypeID IN (119944,121843,121866,126460,126458))
	)
	SELECT * FROM 
		(SELECT cp.UserName Username
		, c.Affinity, c.Underwriter, c.CustomerSurname, c.CustomerPostcode, c.VetName, c.VetPostCode, c.ClaimValue, c.SLADays Day#, c.Status, c.Source
		, leChecklist.WhenCreated Checklist, leRequested.WhenCreated ValidationRequested, leCheckedOK.WhenCreated Validated, leReleased.WhenCreated Released, leInvestigate.WhenCreated InvestigationRequired
		, m.LeadID, m.MatterID, f.FieldName, f.Checked
		FROM Matter m WITH (NOLOCK)
		INNER JOIN ClaimData c WITH (NOLOCK) ON c.MatterID = m.MatterID
		INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID AND cu.Test = 0
		INNER JOIN FraudFlags f WITH (NOLOCK) ON f.MatterID = m.MatterID
		INNER JOIN dbo.LeadEvent leChecklist WITH (NOLOCK) ON leChecklist.CaseID = m.CaseID AND leChecklist.EventTypeID = 125982
		INNER JOIN dbo.LeadEvent leRequested WITH (NOLOCK) ON leRequested.CaseID = m.CaseID AND leRequested.EventTypeID = 142117
		left JOIN dbo.LeadEvent leCheckedOK WITH (NOLOCK) ON leCheckedOK.CaseID = m.CaseID AND leCheckedOK.EventTypeID = 142119
		left JOIN dbo.LeadEvent leReleased WITH (NOLOCK) ON leReleased.CaseID = m.CaseID AND leReleased.EventTypeID = 142118
		left JOIN dbo.LeadEvent leInvestigate WITH (NOLOCK) ON leInvestigate.CaseID = m.CaseID AND leInvestigate.EventTypeID = 142120
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = leChecklist.WhoCreated
		WHERE ' 
		+ CASE @pMode 
			WHEN 0 THEN 'c.ClientStatusID IN (4689, 4690)'
			WHEN 1 THEN 'DATEDIFF(MONTH,leRequested.WhenCreated, dbo.fn_GetDate_Local()) = 0' 
			WHEN 2 THEN 'DATEDIFF(MONTH,leRequested.WhenCreated, dbo.fn_GetDate_Local()) = 1' 
			WHEN 3 THEN 'DATEDIFF(YEAR,leRequested.WhenCreated, dbo.fn_GetDate_Local()) = 0' 
			ELSE 'leRequested.WhenCreated IS NOT NULL' 
		  END
		+ '
		AND NOT EXISTS (SELECT 1 FROM LeadEvent le2 WITH (NOLOCK) WHERE le2.CaseID = leChecklist.CaseID and le2.EventTypeID = leChecklist.EventTypeID and le2.LeadEventID > leChecklist.LeadEventID)
		AND NOT EXISTS (SELECT 1 FROM LeadEvent le3 WITH (NOLOCK) WHERE le3.CaseID = leRequested.CaseID and le3.EventTypeID = leRequested.EventTypeID and le3.LeadEventID > leRequested.LeadEventID)
		AND NOT EXISTS (SELECT 1 FROM LeadEvent le4 WITH (NOLOCK) WHERE le4.CaseID = leCheckedOK.CaseID and le4.EventTypeID = leCheckedOK.EventTypeID and le4.LeadEventID > leCheckedOK.LeadEventID)
		AND NOT EXISTS (SELECT 1 FROM LeadEvent le5 WITH (NOLOCK) WHERE le5.CaseID = leReleased.CaseID and le5.EventTypeID = leReleased.EventTypeID and le5.LeadEventID > leReleased.LeadEventID)
		AND NOT EXISTS (SELECT 1 FROM LeadEvent le6 WITH (NOLOCK) WHERE le6.CaseID = leInvestigate.CaseID and le6.EventTypeID = leInvestigate.EventTypeID and le6.LeadEventID > leInvestigate.LeadEventID)
		) AS SourceTable
	PIVOT
		(
		MAX(Checked)
		FOR 
		FieldName IN (' + @PivotColumns +')
		) AS PivotTable
	ORDER BY Checklist, LeadID, MatterID'

	EXEC (@dSQL)
	--SELECT (@dSQL)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_ValidationRequests] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MI_ValidationRequests] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_ValidationRequests] TO [sp_executeall]
GO
