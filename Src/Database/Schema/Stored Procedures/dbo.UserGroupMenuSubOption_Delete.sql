SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UserGroupMenuSubOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuSubOption_Delete]
(

	@UserGroupMenuSubOptionID int   
)
AS


				DELETE FROM [dbo].[UserGroupMenuSubOption] WITH (ROWLOCK) 
				WHERE
					[UserGroupMenuSubOptionID] = @UserGroupMenuSubOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Delete] TO [sp_executeall]
GO
