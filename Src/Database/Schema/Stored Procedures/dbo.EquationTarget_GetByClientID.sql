SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EquationTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EquationTargetID],
					[ClientID],
					[EquationDetailFieldID],
					[DetailFieldID],
					[Target],
					[ObjectName],
					[PropertyName],
					[SqlFunctionID],
					[IsEquation],
					[IsColumnSum],
					[ColumnFieldID],
					[DetailFieldSubTypeID]
				FROM
					[dbo].[EquationTarget] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_GetByClientID] TO [sp_executeall]
GO
