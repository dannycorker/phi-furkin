SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadTypePageTitleTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleTargetControl_Update]
(

	@LeadTypePageTitleTargetControlID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@LastParsed datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadTypePageTitleTargetControl]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[LastParsed] = @LastParsed
				WHERE
[LeadTypePageTitleTargetControlID] = @LeadTypePageTitleTargetControlID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleTargetControl_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Update] TO [sp_executeall]
GO
