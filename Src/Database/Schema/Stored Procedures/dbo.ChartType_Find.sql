SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChartType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartType_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartTypeID int   = null ,

	@ChartTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartTypeID]
	, [ChartTypeName]
    FROM
	[dbo].[ChartType] WITH (NOLOCK) 
    WHERE 
	 ([ChartTypeID] = @ChartTypeID OR @ChartTypeID IS NULL)
	AND ([ChartTypeName] = @ChartTypeName OR @ChartTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartTypeID]
	, [ChartTypeName]
    FROM
	[dbo].[ChartType] WITH (NOLOCK) 
    WHERE 
	 ([ChartTypeID] = @ChartTypeID AND @ChartTypeID is not null)
	OR ([ChartTypeName] = @ChartTypeName AND @ChartTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Find] TO [sp_executeall]
GO
