SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryType_Update]
(

	@SqlQueryTypeID int   ,

	@TypeName nchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryType]
				SET
					[TypeName] = @TypeName
				WHERE
[SqlQueryTypeID] = @SqlQueryTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Update] TO [sp_executeall]
GO
