SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientOfficeLead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeLead_Insert]
(

	@ClientOfficeLeadID int    OUTPUT,

	@ClientID int   ,

	@ClientOfficeID int   ,

	@LeadID int   
)
AS


				
				INSERT INTO [dbo].[ClientOfficeLead]
					(
					[ClientID]
					,[ClientOfficeID]
					,[LeadID]
					)
				VALUES
					(
					@ClientID
					,@ClientOfficeID
					,@LeadID
					)
				-- Get the identity value
				SET @ClientOfficeLeadID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeLead_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Insert] TO [sp_executeall]
GO
