SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ThirdPartyMappingUserCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential_Get_List]

AS


				
				SELECT
					[ThirdPartyMappingUserCredentialID],
					[ThirdPartySystemID],
					[ClientID],
					[UserID],
					[UserName],
					[Password],
					[Salt],
					[Domain],
					[AttemptedLogins],
					[AccountDisabled],
					[ThirdPartyClientID],
					[LDAPConfigID],
					[ImpersonateThirdPartySystemID]
				FROM
					[dbo].[ThirdPartyMappingUserCredential] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Get_List] TO [sp_executeall]
GO
