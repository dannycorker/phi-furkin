SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientUsage table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientUsage_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientID int   = null ,

	@Validity varchar (100)  = null ,

	@ValidityHash varchar (250)  = null ,

	@Usage varchar (100)  = null ,

	@UsageHash varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientID]
	, [Validity]
	, [ValidityHash]
	, [Usage]
	, [UsageHash]
    FROM
	[dbo].[ClientUsage] WITH (NOLOCK) 
    WHERE 
	 ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Validity] = @Validity OR @Validity IS NULL)
	AND ([ValidityHash] = @ValidityHash OR @ValidityHash IS NULL)
	AND ([Usage] = @Usage OR @Usage IS NULL)
	AND ([UsageHash] = @UsageHash OR @UsageHash IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientID]
	, [Validity]
	, [ValidityHash]
	, [Usage]
	, [UsageHash]
    FROM
	[dbo].[ClientUsage] WITH (NOLOCK) 
    WHERE 
	 ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Validity] = @Validity AND @Validity is not null)
	OR ([ValidityHash] = @ValidityHash AND @ValidityHash is not null)
	OR ([Usage] = @Usage AND @Usage is not null)
	OR ([UsageHash] = @UsageHash AND @UsageHash is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientUsage_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Find] TO [sp_executeall]
GO
