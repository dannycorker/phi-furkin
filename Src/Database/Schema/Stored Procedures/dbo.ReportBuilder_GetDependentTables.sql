SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 17th July 2007
-- Description:	Gets all dependent tables for a given table
-- =============================================
CREATE PROCEDURE [dbo].[ReportBuilder_GetDependentTables]
	@Table varchar(100)
	
AS
BEGIN
	SELECT tableto AS DependentTable FROM reporttablerelationships WHERE tablefrom LIKE @Table
END





GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetDependentTables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportBuilder_GetDependentTables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetDependentTables] TO [sp_executeall]
GO
