SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2013-11-14
-- Description:	Validate and kill unwanted queries from iReporting
-- =============================================
CREATE PROCEDURE [dbo].[SqlQuery__KillQuery] 
	@QueryHeader VARCHAR(100), 
	@Debug BIT = 0 
AS
BEGIN
	SET NOCOUNT ON;

	/*
		In iReporting, we are now adding a GUID comment to the start of the SQL before executing it, so the query will look something like this:
		
		'/*666854D7-79AA-4E5C-816A-6704278D90D7*/ SELECT TOP (10000) * FROM dbo.LeadEvent le WITH (NOLOCK) '
		
		If the user realises they have not set any filters and therefore chooses to cancel the report, we just need to look for
		any query starting with that GUID, get the spid from Sql Server (eg 93) and issue a kill command (eg KILL 93).
		
		For testing, you can issue these commands in one window:
			DECLARE @Query VARCHAR(100) = '/*666854D7-79AA-4E5C-816A-6704278D90D7*/ WAITFOR DELAY ''05:00:00'' '

			EXEC(@Query)
			
		And these in another window (the 1 is the debug flag):
		
			DECLARE @QueryHeader VARCHAR(100) = '/*666854D7-79AA-4E5C-816A-6704278D90D7*/'

			EXEC dbo.SqlQuery__KillQuery @QueryHeader, 1
	*/
	
	DECLARE @SessionID INT, 
	@UserName NVARCHAR(128),
	@KillSQL VARCHAR(20)	
	
	/* Get the ID of the session which is running the query starting with the value of @QueryHeader that was passed in */
	SELECT @SessionID = session_id 
	FROM sys.dm_exec_connections deco 
	CROSS APPLY sys.dm_exec_sql_text(deco.most_recent_sql_handle) dest 
	WHERE dest.[text] LIKE @QueryHeader + '%' 

	/* For safety's sake, make sure that this is an AquariumNet*/
	IF @SessionID > 0
	BEGIN
		/*
		SELECT @UserName = dese.login_name 
		FROM sys.dm_exec_sessions dese 
		WHERE dese.session_id = @SessionID 
		IF @UserName = N'AquariumNet'
		BEGIN
		*/
		SELECT @KillSQL = 'KILL ' + CAST(@SessionID AS VARCHAR)
		
		IF @Debug = 1
		BEGIN
			/* Just output some useful details without killing anyone */
			SELECT @UserName = dese.login_name 
			FROM sys.dm_exec_sessions dese 
			WHERE dese.session_id = @SessionID 
			
			SELECT @UserName AS UserName, @KillSQL AS KillCommand, dest.[text] AS QueryText
			FROM sys.dm_exec_connections deco 
			CROSS APPLY sys.dm_exec_sql_text(deco.most_recent_sql_handle) dest 
			WHERE deco.session_id = @SessionID 
		END
		ELSE
		BEGIN
			/* Issue the kill command */
			EXEC (@KillSQL)
		END
		/*END*/
	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__KillQuery] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__KillQuery] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__KillQuery] TO [sp_executeall]
GO
