SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_Update]
(

	@FieldTargetID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (50)  ,

	@PropertyName varchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FieldTarget]
				SET
					[Target] = @Target
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[FieldTargetID] = @FieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Update] TO [sp_executeall]
GO
