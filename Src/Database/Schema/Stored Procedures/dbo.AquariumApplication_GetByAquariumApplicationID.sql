SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumApplication table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumApplication_GetByAquariumApplicationID]
(

	@AquariumApplicationID int   
)
AS


				SELECT
					[AquariumApplicationID],
					[ApplicationName],
					[ApplicationDescription],
					[CurrentVersion],
					[MinimumVersion],
					[CurrentVersionValidFrom],
					[CurrentVersionValidTo],
					[CurrentVersionDeployedOn]
				FROM
					[dbo].[AquariumApplication] WITH (NOLOCK) 
				WHERE
										[AquariumApplicationID] = @AquariumApplicationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_GetByAquariumApplicationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumApplication_GetByAquariumApplicationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_GetByAquariumApplicationID] TO [sp_executeall]
GO
