SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPreference table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreference_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPreferenceID int   = null ,

	@ClientID int   = null ,

	@ClientPreferenceTypeID int   = null ,

	@PreferenceValue varchar (255)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPreferenceID]
	, [ClientID]
	, [ClientPreferenceTypeID]
	, [PreferenceValue]
    FROM
	[dbo].[ClientPreference] WITH (NOLOCK) 
    WHERE 
	 ([ClientPreferenceID] = @ClientPreferenceID OR @ClientPreferenceID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPreferenceTypeID] = @ClientPreferenceTypeID OR @ClientPreferenceTypeID IS NULL)
	AND ([PreferenceValue] = @PreferenceValue OR @PreferenceValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPreferenceID]
	, [ClientID]
	, [ClientPreferenceTypeID]
	, [PreferenceValue]
    FROM
	[dbo].[ClientPreference] WITH (NOLOCK) 
    WHERE 
	 ([ClientPreferenceID] = @ClientPreferenceID AND @ClientPreferenceID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPreferenceTypeID] = @ClientPreferenceTypeID AND @ClientPreferenceTypeID is not null)
	OR ([PreferenceValue] = @PreferenceValue AND @PreferenceValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreference_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Find] TO [sp_executeall]
GO
