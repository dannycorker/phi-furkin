SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 19/09/2016
-- Description:	Creates history for the Account
-- =============================================
CREATE PROCEDURE [dbo].[AccountHistory__Backup]
	@AccountID INT,	-- the product to backup
	@UserID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	DECLARE @AccountCount INT
	SELECT @AccountCount = COUNT(AccountID) 
	FROM Account WITH (NOLOCK) WHERE AccountID=@AccountID
	IF(@AccountCount>0)-- a product already exists, so perform backup
	BEGIN
		INSERT INTO AccountHistory (AccountID, ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference, CardToken, MaskedCardNumber, ExpiryMonth, ExpiryYear, ExpiryDate, AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified, NextPaymentDate, NextPaymentTotal, CustomerPaymentScheduleID, OriginalWhoCreated, OriginalWhenCreated, OriginalWhoModified, OriginalWhenModified)
		SELECT                               AccountID, ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference, CardToken, MaskedCardNumber, ExpiryMonth, ExpiryYear, ExpiryDate, AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified, NextPaymentDate, NextPaymentTotal, CustomerPaymentScheduleID, @UserID, @WhenCreated, @UserID, @WhenCreated
		FROM Account WITH (NOLOCK) WHERE AccountID=@AccountID
    END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountHistory__Backup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccountHistory__Backup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountHistory__Backup] TO [sp_executeall]
GO
