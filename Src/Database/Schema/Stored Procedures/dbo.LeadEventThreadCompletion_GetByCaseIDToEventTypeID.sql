SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadEventThreadCompletion table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_GetByCaseIDToEventTypeID]
(

	@CaseID int   ,

	@ToEventTypeID int   
)
AS


				SELECT
					[LeadEventThreadCompletionID],
					[ClientID],
					[LeadID],
					[CaseID],
					[FromLeadEventID],
					[FromEventTypeID],
					[ThreadNumberRequired],
					[ToLeadEventID],
					[ToEventTypeID]
				FROM
					[dbo].[LeadEventThreadCompletion] WITH (NOLOCK)
				WHERE
					[CaseID] = @CaseID
					AND [ToEventTypeID] = @ToEventTypeID
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_GetByCaseIDToEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_GetByCaseIDToEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_GetByCaseIDToEventTypeID] TO [sp_executeall]
GO
