SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-24
-- Description:	Keep a debug log of what the LeadEvent Trigger is doing
-- =============================================
CREATE PROCEDURE [dbo].[LeadEventLog__InsertFromTrigger]
   @LeadEventID int,
   @EventTypeID int,
   @LastUpdated datetime,
   @WhenFollowedUp datetime,
   @NextEventID int,
   @StepNumber int,
   @Notes varchar(2000),
   @TriggerGUID uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [dbo].[LeadEventLog] (
		[LeadEventID],
		[EventTypeID],
		[LastUpdated],
		[WhenFollowedUp],
		[NextEventID],
		[StepNumber],
		[Notes],
		[TriggerGUID]
	 )
	 VALUES (
		@LeadEventID, 
		@EventTypeID, 
		@LastUpdated, 
		@WhenFollowedUp, 
		@NextEventID, 
		@StepNumber, 
		@Notes,
		@TriggerGUID
	 )

END





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventLog__InsertFromTrigger] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventLog__InsertFromTrigger] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventLog__InsertFromTrigger] TO [sp_executeall]
GO
