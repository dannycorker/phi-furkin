SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the TimeUnits table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TimeUnits_Update]
(

	@TimeUnitsID int   ,

	@TimeUnitsName varchar (50)  ,

	@SQLTimeUnits varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[TimeUnits]
				SET
					[TimeUnitsName] = @TimeUnitsName
					,[SQLTimeUnits] = @SQLTimeUnits
				WHERE
[TimeUnitsID] = @TimeUnitsID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimeUnits_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Update] TO [sp_executeall]
GO
