SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green/Paul Richardson
-- Create date: 14/01/2020
-- Description:	Creates a client
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateClient]
	@ClientID INT,
	@CompanyName VARCHAR(100),
	@OfficeName VARCHAR(50),
	@BuildingName VARCHAR(50),
	@OfficeAddress1 VARCHAR(200),
	@OfficeAddress2 VARCHAR(200),
	@OfficeTown VARCHAR(200),
	@OfficeCounty VARCHAR(50),
	@OfficePostcode VARCHAR(10),
	@Validity VARCHAR(100),
	@ValidityHash VARCHAR(250),
	@Usage VARCHAR(100),
	@UsageHash VARCHAR(250)
AS
BEGIN

	-- Client -------------------------------------------------------------------------

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	EXEC AquariusMaster.dbo.AQ_Client_Insert @ClientID
	
	SET IDENTITY_INSERT [dbo].[Clients] ON;

	INSERT INTO [dbo].[Clients]([ClientID], [CompanyName], [WebAddress], [IPAddress], [DefaultEmailAddress], [ClientTypeID], [AllowSMS], [SecurityCode], [LeadsBelongToOffices], [UseEventCosts], [UseEventUOEs], [UseEventDisbursements], [UseEventComments], [VerifyAddress], [UseTapi], [UseRPI], [UseGBAddress], [UsePinpoint], [UseSage], [UseSAS], [UseCreditCalculation], [FollowupWorkingDaysOnly], [AddLeadByQuestionnaire], [UseIncendia], [LanguageID], [CountryID], [UseMobileInterface], [UseGBValidation], [CurrencyID], [AllowSmsCommandProcessing])
	SELECT @ClientID, @CompanyName, N'', NULL, N'', 1, 0, NEWID(), NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
	
	SET IDENTITY_INSERT [dbo].[Clients] OFF;

	-- Client Usage -------------------------------------------------------------------------

	INSERT INTO [dbo].[ClientUsage]([ClientID], [Validity], [ValidityHash], [Usage], [UsageHash])
	SELECT @ClientID, @Validity, @ValidityHash, @Usage, @UsageHash

	-- ClientOption  -------------------------------------------------------------------------

	INSERT INTO [dbo].[ClientOption]([ClientID], [ZenDeskOff], [ZenDeskURLPrefix], [ZenDeskText], [ZenDeskTitle], [iDiaryOff], [WorkflowOff], [UserDirectoryOff], [eCatcherOff], [LeadAssignmentOff], [UserMessagesOff], [SystemMessagesOff], [UserPortalOff], [ReportSearchOff], [NormalSearchOff], [UseXero], [UseUltra], [UseCaseSummary], [UseSMSSurvey], [UseNewAddLead], [UseMoreProminentReminders], [DiallerInsertMethod], [DiallerPrimaryKey], [UseCHARMS], [TextMessageService], [UseThunderhead], [UseMemorableWordVerification], [ApplyInactivityTimeout], [InactivityTimeoutInSeconds], [InactivityRedirectUrl], [UseSentimentAnalysis], [ShowProcessInfoButton], [LatestRecordsFirst], [UseSecureMessage], [EventCommentTooltipOff], [EnableScripting], [EnableBilling], [UseEngageMail], [EnableSMSSTOP])
	SELECT @ClientID, 0, N'aquarium', N'How may we help you? Please fill in details below, and we will get back to you as soon as possible.', N'Aquarium Software Limited', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	-- ClientOffices -------------------------------------------------------------------------

	INSERT INTO [dbo].[ClientOffices]([ClientID], [OfficeName], [BuildingName], [Address1], [Address2], [Town], [County], [Country], [PostCode], [OfficeTelephone], [OfficeTelephoneExtension], [OfficeFax], [CountryID], [LanguageID], [SubClientID], [AdminClientPersonnelID], [CurrencyID])
	SELECT @ClientID, @OfficeName, @BuildingName, @OfficeAddress1, @OfficeAddress2, @OfficeTown, @OfficeCounty, N'', @OfficePostcode, N'', N'', NULL, 232, NULL, NULL, NULL, NULL

	DECLARE @ClientOfficeID INT
	SET @ClientOfficeID = SCOPE_IDENTITY()

	-- ClientPersonnel -------------------------------------------------------------------------

	DECLARE @ClientPersonnelID INT
	DECLARE @WebServiceUserClientPersonnelID INT
	DECLARE @AquariumAutomationUserClientPersonnelID INT

	EXEC dbo.ClientPersonnel_Insert @WebServiceUserClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 0, N'Web', N'', N'Service', N'Web Service User (App Login Excluded)', N'mash', 1, N'', N'', N'', N'', N'Web.Service442@aquarium-software.com', 0.0000, '', N'mash', 0, 0, NULL, '', 1, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL 
	EXEC dbo.ClientPersonnel_Insert @AquariumAutomationUserClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 0, N'Aquarium', N'', N'Automation', N'Aquarium Automation User (App Login Excluded)', N'mash', 1, N'', N'', N'', N'', N'Aquarium.Automation442@aquarium-software.com', 0.0000, '', N'mash', 0, 0, NULL, '', 1, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL
	EXEC dbo.ClientPersonnel_Insert @ClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 0, N'Jordan', N'', N'Knight', N'Administrator', N'tJkGiIYsXpg1reU9PWLS3w==', 1, N'', NULL, N'', N'', N'jordan.knight442@mfgroup.co.uk', 0.0000, '', N'ZJJfUKxfKJI=', 0, 0, NULL, '', 1, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL 
	EXEC dbo.ClientPersonnel_Insert @ClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 1, N'Paul', N'', N'Richardson', N'Chief Software Architect', N'sQdZyDQqVkJl7rygyrXAKQ==', 1, NULL, NULL, N'07764250509', N'', N'Paul.Richardson442@aquarium-software.com', 0.0000, '', N'IdSMrciAXF4=', 0, 0, NULL, '', 1, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL 
	EXEC dbo.ClientPersonnel_Insert @ClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 1, N'Alex', N'', N'Elger', N'Consultant', N'n644BQZQVO0=', 1, N'', N'', N'01619292531', N'', N'Alex.Elger442@aquarium-software.com', 0.0000, '', N'BOz7AB0BFbg=', 0, 0, NULL, '', 1, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL 
	EXEC dbo.ClientPersonnel_Insert @ClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 1, N'Jim', N'', N'Green', N'Development Manager', N'CwY5BYW1F4A=', 1, N'', N'', N'0161 929 2524', N'', N'Jim.Green442@aquarium-software.com', 150.0000, '', N'H2fxiLt1ujU=', 0, 0, NULL,'', 1, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL 
	EXEC dbo.ClientPersonnel_Insert @ClientPersonnelID OUTPUT, @ClientID, @ClientOfficeID, 1, N'Louis', N'', N'Bromilow', N'Support Consultant', N'W6mw01l28BvJz6dTtm9cLQ==', 1, N'', N'', N'', N'', N'Louis.Bromilow442@aquarium-software.com', 0.0000, '', N'r/HLel4u2p8=', 0, 0, NULL,'', 1, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL

	-- Folder -------------------------------------------------------------------------

	INSERT INTO [dbo].[Folder]([ClientID], [FolderTypeID], [FolderParentID], [FolderName], [FolderDescription], [WhenCreated], [WhoCreated], [Personal], [SourceID])
	SELECT @ClientID, 1, NULL, N'Automation Reports', N'Automation Reports', '20200113 14:12:56.613', @AquariumAutomationUserClientPersonnelID, 0, NULL

	DECLARE @FolderID INT
	SET @FolderID = SCOPE_IDENTITY()

	-- LeadType -------------------------------------------------------------------------

	INSERT INTO [dbo].[LeadType]([ClientID], [LeadTypeName], [LeadTypeDescription], [Enabled], [Live], [MatterDisplayName], [LastReferenceInteger], [ReferenceValueFormatID], [IsBusiness], [LeadDisplayName], [CaseDisplayName], [CustomerDisplayName], [LeadDetailsTabImageFileName], [CustomerTabImageFileName], [UseEventCosts], [UseEventUOEs], [UseEventDisbursements], [UseEventComments], [AllowMatterPageChoices], [AllowLeadPageChoices], [IsRPIEnabled], [FollowupWorkingDaysOnly], [MatterLastReferenceInteger], [MatterReferenceValueFormatID], [SourceID], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified], [CalculateAllMatters], [SmsGatewayID])
	SELECT @ClientID, N'Resources and Tables', N'Resources and Tables', 0, 0, N'', NULL, NULL, 0, N'', N'', N'', NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	DECLARE @LeadTypeID INT
	SET @LeadTypeID = SCOPE_IDENTITY()

	--SqlQuery -------------------------------------------------------------------------

	INSERT INTO [dbo].[SqlQuery]([ClientID], [QueryText], [QueryTitle], [AutorunOnline], [OnlineLimit], [BatchLimit], [SqlQueryTypeID], [FolderID], [IsEditable], [IsTemplate], [IsDeleted], [WhenCreated], [CreatedBy], [OwnedBy], [RunCount], [LastRundate], [LastRuntime], [LastRowcount], [MaxRuntime], [MaxRowcount], [AvgRuntime], [AvgRowcount], [Comments], [WhenModified], [ModifiedBy], [LeadTypeID], [ParentQueryID], [IsParent], [SqlQueryTemplateID], [OutputFormat], [ShowInCustomSearch], [OutputFileExtension], [OutputSeparatorCharmapID], [OutputEncapsulatorCharmapID], [SuppressHeaderRow], [LockAllTables], [SourceID])
	SELECT @ClientID, N'SELECT Cases.CaseID, Customers.CustomerID, Lead.LeadID, LeadEvent.EventTypeID, LeadEvent.WhenFollowedUp, LeadEvent.EventDeleted, Customers.Test, LeadEvent.LeadEventID  
			FROM Customers    
			INNER JOIN Lead  ON Lead.CustomerID = Customers.CustomerID  
			INNER JOIN Cases  ON Cases.LeadID = Lead.LeadID  
			INNER JOIN LeadEvent  ON LeadEvent.CaseID = Cases.CaseID  
			WHERE Customers.ClientID = ' + CAST(@ClientID AS VARCHAR) + '   
			AND Lead.ClientID = ' + CAST(@ClientID AS VARCHAR) + '   
			AND Cases.ClientID = ' + CAST(@ClientID AS VARCHAR) + '   
			AND LeadEvent.ClientID = ' + CAST(@ClientID AS VARCHAR) + '   
			AND (Customers.Test IS NULL  OR Customers.Test = 0)   
			AND (LeadEvent.EventDeleted = 0)   
			AND (LeadEvent.WhenFollowedUp IS NULL )', N'_Base', 0, 0, 0, 3, @FolderID, 1, 0, 0, '20200113 14:12:56.613', @AquariumAutomationUserClientPersonnelID, @AquariumAutomationUserClientPersonnelID, 0, NULL, 0, 0, 0, 0, 0, 0, N'', '20200113 14:12:56.613', @AquariumAutomationUserClientPersonnelID, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	DECLARE @QueryID INT
	SET @QueryID = SCOPE_IDENTITY()

	-- SqlQueryTables -------------------------------------------------------------------------

	INSERT INTO [dbo].[SqlQueryTables]([ClientID], [SqlQueryID], [SqlQueryTableName], [TableAlias], [TableDisplayOrder], [JoinType], [JoinText], [JoinTableID], [JoinRTRID], [TempTableID], [TempJoinTableID], [SourceID])
	SELECT @ClientID, @QueryID, N'Customers', N'', 0, N'', N'', NULL, NULL, NULL, NULL, NULL 

	DECLARE @CustomerQueryTable INT
	SET @CustomerQueryTable  = SCOPE_IDENTITY()

	INSERT INTO [dbo].[SqlQueryTables]([ClientID], [SqlQueryID], [SqlQueryTableName], [TableAlias], [TableDisplayOrder], [JoinType], [JoinText], [JoinTableID], [JoinRTRID], [TempTableID], [TempJoinTableID], [SourceID])
	SELECT @ClientID, @QueryID, N'Lead', N'', 1, N'INNER JOIN', N'Lead.CustomerID = Customers.CustomerID', NULL, 30, NULL, NULL, NULL 

	DECLARE @LeadQueryTable INT
	SET @LeadQueryTable  = SCOPE_IDENTITY()

	INSERT INTO [dbo].[SqlQueryTables]([ClientID], [SqlQueryID], [SqlQueryTableName], [TableAlias], [TableDisplayOrder], [JoinType], [JoinText], [JoinTableID], [JoinRTRID], [TempTableID], [TempJoinTableID], [SourceID])
	SELECT @ClientID, @QueryID, N'Cases', N'', 2, N'INNER JOIN', N'Cases.LeadID = Lead.LeadID', NULL, 41, NULL, NULL, NULL 

	DECLARE @CasesQueryTable INT
	SET @CasesQueryTable = SCOPE_IDENTITY()

	INSERT INTO [dbo].[SqlQueryTables]([ClientID], [SqlQueryID], [SqlQueryTableName], [TableAlias], [TableDisplayOrder], [JoinType], [JoinText], [JoinTableID], [JoinRTRID], [TempTableID], [TempJoinTableID], [SourceID])
	SELECT @ClientID, @QueryID, N'LeadEvent', N'', 3, N'INNER JOIN', N'LeadEvent.CaseID = Cases.CaseID', NULL, 63, NULL, NULL, NULL

	DECLARE @LeadEventQueryTable INT
	SET @LeadEventQueryTable = SCOPE_IDENTITY()

	-- SqlQueryColumns -------------------------------------------------------------------------

	INSERT INTO [dbo].[SqlQueryColumns]([ClientID], [SqlQueryID], [SqlQueryTableID], [CompleteOutputText], [ColumnAlias], [ShowColumn], [DisplayOrder], [SortOrder], [SortType], [TempColumnID], [TempTableID], [IsAggregate], [ColumnDataType], [ManipulatedDataType], [ColumnNaturalName], [SourceID], [ComplexValue])
	SELECT @ClientID, @QueryID, @CustomerQueryTable, N'Customers.CustomerID', N'', 1, 2, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'CustomerID', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadQueryTable, N'Lead.LeadID', N'', 1, 3, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'LeadID', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @CasesQueryTable, N'Cases.CaseID', N'', 1, 1, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'CaseID', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, N'LeadEvent.EventTypeID', N'', 1, 4, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'EventTypeID', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, N'LeadEvent.WhenFollowedUp', NULL, 1, 5, NULL, NULL, NULL, NULL, 0, N'DateTime', N'DateTime', N'WhenFollowedUp', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, N'LeadEvent.EventDeleted', NULL, 1, 6, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'EventDeleted', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @CustomerQueryTable, N'Customers.Test', NULL, 1, 7, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'Test', NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, N'LeadEvent.LeadEventID', N'', 1, 8, NULL, NULL, NULL, NULL, 0, N'Number', N'Number', N'LeadEventID', NULL, NULL

	-- SqlQueryCriteria -------------------------------------------------------------------------

	INSERT INTO [dbo].[SqlQueryCriteria]([ClientID], [SqlQueryID], [SqlQueryTableID], [SqlQueryColumnID], [CriteriaText], [Criteria1], [Criteria2], [CriteriaName], [SubQueryID], [SubQueryLinkType], [ParamValue], [TempTableID], [TempColumnID], [TempCriteriaID], [IsSecurityClause], [CriteriaSubstitutions], [IsParameterizable], [Comparison], [IsJoinClause], [SourceID])
	SELECT @ClientID, @QueryID, @CustomerQueryTable, NULL, N'Customers.ClientID = ' + CAST(@ClientID AS VARCHAR), N'', N'', N'Customers Security', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadQueryTable, NULL, N'Lead.ClientID = ' + CAST(@ClientID AS VARCHAR), N'', N'', N'Lead Security', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @CasesQueryTable, NULL, N'Cases.ClientID = ' + CAST(@ClientID AS VARCHAR), N'', N'', N'Cases Security', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, NULL, N'LeadEvent.ClientID = ' + CAST(@ClientID AS VARCHAR), N'', N'', N'LeadEvent Security', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @CustomerQueryTable, 1967160, N'(Customers.Test IS NULL  OR Customers.Test = 0)', N'0', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, 0, N'', 0, NULL, NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, 1967159, N'(LeadEvent.EventDeleted = 0)', N'0', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, 0, N'', 0, NULL, NULL, NULL UNION ALL
	SELECT @ClientID, @QueryID, @LeadEventQueryTable, 1967158, N'(LeadEvent.WhenFollowedUp IS NULL )', N'', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, 0, N'', 0, NULL, NULL, NULL
	
	-- __BatchErrorExclusion -------------------------------------------------------------------------

	INSERT INTO [dbo].[__BatchErrorExclusion]([ClientID], [FinishDate], [Reason])
	SELECT @ClientID, '20200128 00:00:00.000', N'New Client Created'

	-- __LoginExclusions  -------------------------------------------------------------------------

	INSERT INTO [dbo].[__LoginExclusions]([ClientID], [ClientPersonnelID], [Description])
	SELECT @ClientID, @WebServiceUserClientPersonnelID, N'Web Service User' UNION ALL
	SELECT @ClientID, @AquariumAutomationUserClientPersonnelID, N'Aquarium Automation User'

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateClient] TO [sp_executeall]
GO
