SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PanelItemTypes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemTypes_GetByPanelItemTypeID]
(

	@PanelItemTypeID int   
)
AS


				SELECT
					[PanelItemTypeID],
					[PanelItemTypeName]
				FROM
					[dbo].[PanelItemTypes] WITH (NOLOCK) 
				WHERE
										[PanelItemTypeID] = @PanelItemTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_GetByPanelItemTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemTypes_GetByPanelItemTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_GetByPanelItemTypeID] TO [sp_executeall]
GO
