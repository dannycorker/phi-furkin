SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowGroup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[WorkflowGroupID],
					[Name],
					[Description],
					[ManagerID],
					[ClientID],
					[Enabled],
					[SortOrder],
					[AssignedLeadsOnly],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[WorkflowGroup] WITH (NOLOCK) 
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_GetBySourceID] TO [sp_executeall]
GO
