SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerAnswers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_GetByCustomerAnswerID]
(

	@CustomerAnswerID int   
)
AS


				SELECT
					[CustomerAnswerID],
					[CustomerQuestionnaireID],
					[MasterQuestionID],
					[Answer],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[CustomerAnswers] WITH (NOLOCK) 
				WHERE
										[CustomerAnswerID] = @CustomerAnswerID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByCustomerAnswerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_GetByCustomerAnswerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByCustomerAnswerID] TO [sp_executeall]
GO
