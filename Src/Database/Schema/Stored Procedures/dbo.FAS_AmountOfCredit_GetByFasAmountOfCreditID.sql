SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FAS_AmountOfCredit table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_GetByFasAmountOfCreditID]
(

	@FasAmountOfCreditID int   
)
AS


				SELECT
					[FasAmountOfCreditID],
					[FasCheckerDataID],
					[Description],
					[CostAmount]
				FROM
					[dbo].[FAS_AmountOfCredit] WITH (NOLOCK) 
				WHERE
										[FasAmountOfCreditID] = @FasAmountOfCreditID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_GetByFasAmountOfCreditID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_GetByFasAmountOfCreditID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_GetByFasAmountOfCreditID] TO [sp_executeall]
GO
