SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the IncomingPostEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEvent_Update]
(

	@IncomingPostEventID int   ,

	@EventDateTime datetime   ,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@MatterID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[IncomingPostEvent]
				SET
					[EventDateTime] = @EventDateTime
					,[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[MatterID] = @MatterID
				WHERE
[IncomingPostEventID] = @IncomingPostEventID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEvent_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Update] TO [sp_executeall]
GO
