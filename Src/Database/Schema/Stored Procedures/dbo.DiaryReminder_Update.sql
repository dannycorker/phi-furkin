SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DiaryReminder table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryReminder_Update]
(

	@DiaryReminderID int   ,

	@ClientID int   ,

	@DiaryAppointmentID int   ,

	@ClientPersonnelID int   ,

	@ReminderTimeshiftID int   ,

	@ReminderDate datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DiaryReminder]
				SET
					[ClientID] = @ClientID
					,[DiaryAppointmentID] = @DiaryAppointmentID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[ReminderTimeshiftID] = @ReminderTimeshiftID
					,[ReminderDate] = @ReminderDate
				WHERE
[DiaryReminderID] = @DiaryReminderID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Update] TO [sp_executeall]
GO
