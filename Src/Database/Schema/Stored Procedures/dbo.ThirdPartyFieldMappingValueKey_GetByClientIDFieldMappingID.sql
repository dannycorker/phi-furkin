SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldMappingValueKey table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_GetByClientIDFieldMappingID]
(

	@ClientID int   ,

	@FieldMappingID int   
)
AS


				SELECT
					[ThirdPartyFieldMappingValueKeyID],
					[ClientID],
					[FieldMappingID],
					[DetailValue],
					[LookupListItemID],
					[ThirdPartyKey],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMappingValueKey] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [FieldMappingID] = @FieldMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByClientIDFieldMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByClientIDFieldMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByClientIDFieldMappingID] TO [sp_executeall]
GO
