SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPersonnel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_Update]
(

	@ClientPersonnelID int   ,

	@ClientID int   ,

	@ClientOfficeID int   ,

	@TitleID int   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@JobTitle varchar (100)  ,

	@Password varchar (65)  ,

	@ClientPersonnelAdminGroupID int   ,

	@MobileTelephone varchar (50)  ,

	@HomeTelephone varchar (50)  ,

	@OfficeTelephone varchar (50)  ,

	@OfficeTelephoneExtension varchar (50)  ,

	@EmailAddress varchar (255)  ,

	@ChargeOutRate money   ,

	@UserName varchar (201)   OUTPUT,

	@Salt varchar (50)  ,

	@AttemptedLogins int   ,

	@AccountDisabled bit   ,

	@ManagerID int   ,

	@Initials varchar (3)   OUTPUT,

	@LanguageID int   ,

	@SubClientID int   ,

	@ForcePasswordChangeOn datetime   ,

	@ThirdPartySystemId int   ,

	@CustomerID int   ,

	@IsAquarium bit   ,

	@AllowSmsCommandProcessing bit   ,

	@MemorableWord varchar (100)  ,

	@MemorableWordSalt varchar (65)  ,

	@MemorableWordAttempts int   ,

	@PendingActivation bit   
)
AS


				
				
				-- If we don't have an outer tranasction e.g. from the lead event 
				DECLARE @TranCount INT = @@TRANCOUNT
				IF @TranCount = 0
				BEGIN
					BEGIN TRAN
				END
				
				BEGIN TRY
		
				-- Updates the master record
				EXEC AquariusMaster.dbo.AQ_ClientPersonnel_Update @ClientPersonnelID, @EmailAddress, @ThirdPartySystemId
				
				-- Set context info to allow access to table
				DECLARE @ContextInfo VARBINARY(100) = CAST('ClientPersonnel' AS VARBINARY)
				SET CONTEXT_INFO @ContextInfo
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPersonnel]
				SET
					[ClientID] = @ClientID
					,[ClientOfficeID] = @ClientOfficeID
					,[TitleID] = @TitleID
					,[FirstName] = @FirstName
					,[MiddleName] = @MiddleName
					,[LastName] = @LastName
					,[JobTitle] = @JobTitle
					,[Password] = @Password
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[MobileTelephone] = @MobileTelephone
					,[HomeTelephone] = @HomeTelephone
					,[OfficeTelephone] = @OfficeTelephone
					,[OfficeTelephoneExtension] = @OfficeTelephoneExtension
					,[EmailAddress] = @EmailAddress
					,[ChargeOutRate] = @ChargeOutRate
					,[Salt] = @Salt
					,[AttemptedLogins] = @AttemptedLogins
					,[AccountDisabled] = @AccountDisabled
					,[ManagerID] = @ManagerID
					,[LanguageID] = @LanguageID
					,[SubClientID] = @SubClientID
					,[ForcePasswordChangeOn] = @ForcePasswordChangeOn
					,[ThirdPartySystemId] = @ThirdPartySystemId
					,[CustomerID] = @CustomerID
					,[IsAquarium] = @IsAquarium
					,[AllowSmsCommandProcessing] = @AllowSmsCommandProcessing
					,[MemorableWord] = @MemorableWord
					,[MemorableWordSalt] = @MemorableWordSalt
					,[MemorableWordAttempts] = @MemorableWordAttempts
					,[PendingActivation] = @PendingActivation
				WHERE
[ClientPersonnelID] = @ClientPersonnelID 
				
				
				-- If we don't have an outer tran we can commit here
				IF @TranCount = 0
				BEGIN
					COMMIT
				END
				
				-- Clear the things we set above
				SET CONTEXT_INFO 0x0
				
				END TRY			
				BEGIN CATCH    
					
					-- If we have any tran open then we need to rollback
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK
					END
					
					-- Clear the things we set above
					SET CONTEXT_INFO 0x0
					
					DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
					DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
					DECLARE @ErrorState INT = ERROR_STATE()

					RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
					
					
				END CATCH
				
				
				-- Select computed columns into output parameters
				SELECT
 @UserName = [UserName]
, @Initials = [Initials]
				FROM
					[dbo].[ClientPersonnel] WITH (NOLOCK) 
				WHERE
[ClientPersonnelID] = @ClientPersonnelID 
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Update] TO [sp_executeall]
GO
