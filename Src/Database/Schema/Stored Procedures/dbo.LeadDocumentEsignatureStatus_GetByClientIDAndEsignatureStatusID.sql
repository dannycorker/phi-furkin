SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_GetByClientIDAndEsignatureStatusID]
(
	@ClientID INT,
	@EsignatureStatusID INT   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadDocumentEsignatureStatusID],
					[ClientID],
					[LeadDocumentID],
					[ElectronicSignatureDocumentKey],
					[DocumentName],
					[SentTo],
					[DateSent],
					[EsignatureStatusID],
					[StatusCheckedDate]
				FROM
					[dbo].[LeadDocumentEsignatureStatus] WITH (NOLOCK) 
				WHERE
					[EsignatureStatusID] = @EsignatureStatusID AND
					ClientID=@ClientID
				
				SELECT @@ROWCOUNT
				
			




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_GetByClientIDAndEsignatureStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_GetByClientIDAndEsignatureStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_GetByClientIDAndEsignatureStatusID] TO [sp_executeall]
GO
