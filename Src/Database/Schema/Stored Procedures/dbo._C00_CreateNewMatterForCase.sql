SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 06-Jun-2011
-- Description:	Proc to Create a new matter to an existing case
-- JWG 2014-01-23 #25390 Set WhoModified etc 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateNewMatterForCase]
(
	@CaseID INT, 
	@WhoCreated INT
)


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@myERROR INT,
			@NewMatterID INT,
			@RefLetter VARCHAR(3),
			@MatterStatus INT,
			@CustomerID INT,
			@LeadTypeID INT,
			@ClientID INT ,
			@LeadID INT

	SELECT TOP 1 @CustomerID = Matter.CustomerID,
		@ClientID = Cases.ClientID,
		@MatterStatus = Matter.MatterStatus
	FROM Cases WITH (NOLOCK)
	INNER JOIN Matter WITH (NOLOCK)ON Matter.CaseID = Cases.CaseID
	WHERE Cases.CaseID = @CaseID

	SELECT @LeadID = c.LeadID
	FROM Cases c WITH (NOLOCK)
	WHERE c.CaseID = @CaseID

	SELECT @RefLetter = dbo.fnRefLetterFromCaseNum(1+COUNT(*)) 
	FROM matter WITH (NOLOCK)
	WHERE leadid = @LeadID

	/* Create Matter */
	/* JWG 2014-01-23 #25390 Set WhoModified etc */
	INSERT INTO Matter([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID], WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@ClientID,'',@CustomerID,@LeadID,@MatterStatus,@RefLetter,0,@CaseID, @WhoCreated, dbo.fn_GetDate_Local(), @WhoCreated, dbo.fn_GetDate_Local())

	SELECT @NewMatterID = SCOPE_IDENTITY()

	RETURN @NewMatterID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewMatterForCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateNewMatterForCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewMatterForCase] TO [sp_executeall]
GO
