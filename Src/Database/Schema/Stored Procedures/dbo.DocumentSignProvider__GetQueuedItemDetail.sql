SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetQueuedItemDetail]
	@QueueID INT,
	@ClientID INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	DSPQueueID, ClientID, LeadID, CaseID, OutLeadEventID, OutEventTypeID, OutDocumentTypeID, InEventTypeID, InLeadEventID, DSPTypeID, DSPAccountID, DSPUserID, DSPSignatureTemplate, DSPBrandingKey, DSPEnvelopeKey, DSPEnvelopeStatus, RequestXML, ResponseXML, WhenCreated, WhenModified
	FROM	DocumentSignProviderQueue WITH (NOLOCK) 
	WHERE	ClientID = @ClientID
	AND		DSPQueueID = @QueueID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetQueuedItemDetail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetQueuedItemDetail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetQueuedItemDetail] TO [sp_executeall]
GO
