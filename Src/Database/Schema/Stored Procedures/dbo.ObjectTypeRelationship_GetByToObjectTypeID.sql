SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectTypeRelationship table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_GetByToObjectTypeID]
(

	@ToObjectTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ObjectTypeRelationshipID],
					[FromObjectTypeID],
					[ToObjectTypeID],
					[RelationshipTypeID],
					[RelationshipName]
				FROM
					[dbo].[ObjectTypeRelationship] WITH (NOLOCK) 
				WHERE
					[ToObjectTypeID] = @ToObjectTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_GetByToObjectTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_GetByToObjectTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_GetByToObjectTypeID] TO [sp_executeall]
GO
