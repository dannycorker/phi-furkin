SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptNoteTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_Insert]
(

	@ScriptNoteTargetID int    OUTPUT,

	@ScriptID int   ,

	@ClientID int   ,

	@NoteTypeID int   ,

	@PriorityID int   ,

	@ApplyToAllCases bit   
)
AS


				
				INSERT INTO [dbo].[ScriptNoteTarget]
					(
					[ScriptID]
					,[ClientID]
					,[NoteTypeID]
					,[PriorityID]
					,[ApplyToAllCases]
					)
				VALUES
					(
					@ScriptID
					,@ClientID
					,@NoteTypeID
					,@PriorityID
					,@ApplyToAllCases
					)
				-- Get the identity value
				SET @ScriptNoteTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Insert] TO [sp_executeall]
GO
