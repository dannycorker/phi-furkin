SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTask_Insert]
(

	@TaskID int    OUTPUT,

	@ClientID int   ,

	@Taskname varchar (100)  ,

	@Description varchar (250)  ,

	@Enabled bit   ,

	@RunAtHour int   ,

	@RunAtMinute int   ,

	@RepeatTimeUnitsID int   ,

	@RepeatTimeQuantity int   ,

	@NextRunDateTime datetime   ,

	@WorkflowTask bit   ,

	@AlreadyRunning bit   ,

	@AutomatedTaskGroupID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@MaximumAllowableErrors int   ,

	@EventSubTypeThresholding bit   
)
AS


				
				INSERT INTO [dbo].[AutomatedTask]
					(
					[ClientID]
					,[Taskname]
					,[Description]
					,[Enabled]
					,[RunAtHour]
					,[RunAtMinute]
					,[RepeatTimeUnitsID]
					,[RepeatTimeQuantity]
					,[NextRunDateTime]
					,[WorkflowTask]
					,[AlreadyRunning]
					,[AutomatedTaskGroupID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[MaximumAllowableErrors]
					,[EventSubTypeThresholding]
					)
				VALUES
					(
					@ClientID
					,@Taskname
					,@Description
					,@Enabled
					,@RunAtHour
					,@RunAtMinute
					,@RepeatTimeUnitsID
					,@RepeatTimeQuantity
					,@NextRunDateTime
					,@WorkflowTask
					,@AlreadyRunning
					,@AutomatedTaskGroupID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@MaximumAllowableErrors
					,@EventSubTypeThresholding
					)
				-- Get the identity value
				SET @TaskID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Insert] TO [sp_executeall]
GO
