SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-07-07
-- Description:	SmsQuestionnaire__Describe 
-- =============================================
CREATE PROCEDURE [dbo].[SmsQuestionnaire__Describe]
	@SmsQuestionnaireID INT = 3
AS
BEGIN

	SET NOCOUNT ON;


	DECLARE	@ClientID			INT,
			@LeadTypeID			INT,
			@ClientPersonnelID	INT,
			@OutgoingNumber		VARCHAR(16)

	SELECT @ClientID = sq.ClientID, @LeadTypeID = opn.LeadTypeID, @ClientPersonnelID = cp.ClientPersonnelID, @OutgoingNumber = opn.OutgoingPhoneNumber
	FROM SMSQuestionnaire sq WITH (NOLOCK) 
	LEFT JOIN SMSOutgoingPhoneNumber opn WITH (NOLOCK) on opn.SMSQuestionnaireID = sq.SMSQuestionnaireID 
	LEFT JOIN ClientPersonnel cp WITH (NOLOCK) on cp.ClientID = sq.ClientID and cp.EmailAddress = 'sms.survey.user' + CONVERT(VARCHAR,cp.ClientID) + '@aquarium-software.com'
	WHERE sq.SMSQuestionnaireID = @SmsQuestionnaireID

	IF (@ClientID is null or @LeadTypeID is null or @ClientPersonnelID is null or ISNUMERIC(@OutgoingNumber) = 0)
	BEGIN
		SELECT 'ERROR: ' + CASE  
									WHEN @ClientID IS NULL THEN 'Missing Outgoing Phone Number'
									WHEN @LeadTypeID IS NULL THEN 'Missing LeadType'
									WHEN @ClientPersonnelID IS NULL THEN 'Missing SMS User'
									WHEN ISNUMERIC(@OutgoingNumber) = 0 THEN 'Number is not numeric'
									END [Error]
	END

	SELECT opn.OutgoingPhoneNumber, lt.LeadTypeName, cp.ClientPersonnelID, sq.*
	FROM SMSQuestionnaire sq WITH (NOLOCK) 
	LEFT JOIN SMSOutgoingPhoneNumber opn WITH (NOLOCK) on opn.SMSQuestionnaireID = sq.SMSQuestionnaireID
	LEFT JOIN LeadType lt WITH (NOLOCK) on lt.LeadTypeID = opn.LeadTypeID
	LEFT JOIN ClientPersonnel cp WITH (NOLOCK) on cp.ClientID = @ClientID and cp.EmailAddress = 'sms.survey.user' + CONVERT(VARCHAR,@ClientID) + '@aquarium-software.com'
	WHERE sq.SMSQuestionnaireID = @SmsQuestionnaireID

	SELECT * 
	FROM SMSQuestion sq WITH (NOLOCK) 
	WHERE sq.SMSQuestionnaireID = @SmsQuestionnaireID
	ORDER BY sq.QuestionOrder

	SELECT dtfrom.DocumentTypeName [From Event], dtto.DocumentTypeName [To Event], rm.*
	from SMSResponseMap rm WITH (NOLOCK) 
	INNER JOIN SMSQuestion sqfrom WITH (NOLOCK) on sqfrom.SMSQuestionID = rm.FromSMSQuestionID
	INNER JOIN DocumentType dtfrom WITH (NOLOCK) on dtfrom.DocumentTypeID = sqfrom.DocumentTypeID
	INNER JOIN SMSQuestion sqto   WITH (NOLOCK) on   sqto.SMSQuestionID =   rm.ToSMSQuestionID
	INNER JOIN DocumentType dtto WITH (NOLOCK) on dtto.DocumentTypeID = sqto.DocumentTypeID
	WHERE rm.SMSQuestionnaireID = @SmsQuestionnaireID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SmsQuestionnaire__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SmsQuestionnaire__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SmsQuestionnaire__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[SmsQuestionnaire__Describe] TO [sp_executehelper]
GO
