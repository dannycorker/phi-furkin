SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_Insert]
(

	@AccessRuleID int    OUTPUT,

	@ClientID int   ,

	@AccessRuleName varchar (100)  ,

	@AccessRuleDescription varchar (250)  ,

	@AccessRuleEnabled bit   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@PortalUserID int   ,

	@DataLoaderObjectTypeID int   ,

	@LeadTypeID int   ,

	@DetailFieldSubTypeID int   ,

	@DetailFieldPageID int   ,

	@DetailFieldID int   ,

	@ClientQuestionnaireID int   ,

	@ValueDecoder varchar (50)  ,

	@ValueDecoderColumnFieldID int   ,

	@ValueToCompare int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@AccessLevel tinyint   
)
AS


				
				INSERT INTO [dbo].[AccessRule]
					(
					[ClientID]
					,[AccessRuleName]
					,[AccessRuleDescription]
					,[AccessRuleEnabled]
					,[ClientPersonnelAdminGroupID]
					,[ClientPersonnelID]
					,[PortalUserID]
					,[DataLoaderObjectTypeID]
					,[LeadTypeID]
					,[DetailFieldSubTypeID]
					,[DetailFieldPageID]
					,[DetailFieldID]
					,[ClientQuestionnaireID]
					,[ValueDecoder]
					,[ValueDecoderColumnFieldID]
					,[ValueToCompare]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[AccessLevel]
					)
				VALUES
					(
					@ClientID
					,@AccessRuleName
					,@AccessRuleDescription
					,@AccessRuleEnabled
					,@ClientPersonnelAdminGroupID
					,@ClientPersonnelID
					,@PortalUserID
					,@DataLoaderObjectTypeID
					,@LeadTypeID
					,@DetailFieldSubTypeID
					,@DetailFieldPageID
					,@DetailFieldID
					,@ClientQuestionnaireID
					,@ValueDecoder
					,@ValueDecoderColumnFieldID
					,@ValueToCompare
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@AccessLevel
					)
				-- Get the identity value
				SET @AccessRuleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Insert] TO [sp_executeall]
GO
