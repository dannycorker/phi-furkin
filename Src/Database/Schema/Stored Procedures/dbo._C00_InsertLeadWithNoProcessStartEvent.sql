SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-03-2012
-- Description:	Inserts a lead for IC customer portal - work around for the IC batch job applying a process start
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertLeadWithNoProcessStartEvent]
	@LeadTypeID INT,
	@CustomerID INT,
	@ClientID INT,
	@UserID INT
AS
BEGIN

	SET NOCOUNT ON;

	Declare @NewMatterID int
	
	exec @NewMatterID = dbo._C00_CreateNewLeadForCust @CustomerID, @LeadTypeID, @ClientID, @UserID, 0, 1, 0

	Select CustomerID, LeadID, CaseID, MatterID from Matter WITH (NOLOCK) WHERE MatterID = @NewMatterID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertLeadWithNoProcessStartEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertLeadWithNoProcessStartEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertLeadWithNoProcessStartEvent] TO [sp_executeall]
GO
