SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Chris Townsend
-- Create date: 13th August 2007
-- Description:	Gets a list of all potential 'To' email addresses
--				that a user can send an 'Email Out' EventSubType to
--				2013-11-04 ACE Updated to return from Case and CustomerDetailValues
--				As well as selecting resource fields correctly.
--				2014-02-24 ACE Updated to use Third party Fields for Resources.
-- =============================================
CREATE PROCEDURE [dbo].[GetSendableEmailAddressesForLead] 

@leadID int,
@caseID int,
@clientID int

AS
BEGIN

	SET NOCOUNT ON

	DECLARE @SQLStatement varchar(MAX)

    -- get all ClientPersonnel email addresses for the current ClientID
	-- SELECT @SQLStatement = 
	-- 'SELECT null AS MatterID, UserName AS Description, EmailAddress AS EmailAddress FROM ClientPersonnel WHERE ClientID = ' + CAST(@clientID AS VARCHAR(20))

	IF @leadID is not null -- get all LeadDetail email address fields for the current lead
	BEGIN	
		
		SELECT @SQLStatement =  
		'
		-- UNION	
		
		SELECT null As MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, ldv.DetailValue AS EmailAddress, ldv.EncryptedValue
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK)  ON ldv.DetailFieldID = df.DetailFieldID
		INNER JOIN DetailFieldPages dfp WITH (NOLOCK)  ON df.DetailFieldPageID = dfp.DetailFieldPageID
		WHERE df.QuestionTypeID = 11 -- Type is "Email Address"
		AND ldv.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + 
		'AND ldv.LeadID = ' + CAST(@leadID AS VARCHAR(20)) + 
		'AND ldv.DetailValue <>'''' '

		SELECT @SQLStatement = @SQLStatement + 
			
			' UNION

			SELECT NULL AS MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, cdv.DetailValue AS EmailAddress, cdv.EncryptedValue
			FROM DetailFields df  WITH (NOLOCK) 
			INNER JOIN CustomerDetailValues cdv WITH (NOLOCK)  ON cdv.DetailFieldID = df.DetailFieldID
			INNER JOIN Lead l WITH (NOLOCK)  ON l.CustomerID = cdv.CustomerID
			INNER JOIN DetailFieldPages dfp WITH (NOLOCK)  ON df.DetailFieldPageID = dfp.DetailFieldPageID
			WHERE df.QuestionTypeID = 11 -- Type is "Email Address"
			AND cdv.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + '
			AND l.LeadID = ' + CAST(@LeadID AS VARCHAR(20)) + '
			AND cdv.DetailValue <> '''' '
			
		/*2015-03-11 ACE Added the below for contact email addresses*/
		SELECT @SQLStatement = @SQLStatement +
		' UNION
		
			SELECT NULL AS MatterID, ''Email Address Work'' As DetailPageCaption, ''Email Address Work'' As DetailPageName, c.Fullname AS Description, '''' as FieldEncrypt, c.EmailAddressWork AS EmailAddress, '''' AS EncryptedValue
			FROM Lead l WITH (NOLOCK)  
			INNER JOIN Contact c WITH (NOLOCK) ON c.CustomerID = l.CustomerID AND c.EmailAddressWork <> ''''
			WHERE l.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + '
			AND l.LeadID = ' + CAST(@LeadID AS VARCHAR(20))

		/*2013-11-04 ACE Updated with the below.*/
		SELECT @SQLStatement = @SQLStatement + 
			
			' UNION

			SELECT NULL AS MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, rldv.DetailValue AS EmailAddress, rldv.EncryptedValue
			FROM DetailFields df WITH (NOLOCK) 
			INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = df.DetailFieldID AND ldv.LeadID = ' + CAST(@LeadID AS VARCHAR(20)) + '
			INNER JOIN DetailFields df_email ON df_email.ClientID = df.ClientID AND df_email.LeadOrMatter = 4 AND df_email.QuestionTypeID = 11
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = ldv.ValueInt AND rldv.DetailFieldID = df_email.DetailFieldID
			INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df_email.DetailFieldPageID
			WHERE df.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + '
			AND df.QuestionTypeID = 14
			AND rldv.DetailValue <> ''''
			'

		IF @caseID is not null -- get all MatterDetail email address fields for the current case
		BEGIN

			SELECT @SQLStatement = @SQLStatement + 
				
				' UNION
				
				SELECT mdv.MatterID AS MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, mdv.DetailValue AS EmailAddress, mdv.EncryptedValue
				FROM DetailFields df  WITH (NOLOCK) 
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK)  ON mdv.DetailFieldID = df.DetailFieldID
				INNER JOIN Matter m WITH (NOLOCK)  ON m.MatterID = mdv.MatterID
				INNER JOIN DetailFieldPages dfp WITH (NOLOCK)  ON df.DetailFieldPageID = dfp.DetailFieldPageID
				WHERE df.QuestionTypeID = 11 -- Type is "Email Address"
				AND mdv.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + 
				'AND m.CaseID = ' + CAST(@caseID AS VARCHAR(20)) +
				'AND mdv.DetailValue <> '''' '

			SELECT @SQLStatement = @SQLStatement + 
				
				' UNION

				SELECT NULL AS MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, cdv.DetailValue AS EmailAddress, cdv.EncryptedValue
				FROM DetailFields df  WITH (NOLOCK) 
				INNER JOIN CaseDetailValues cdv WITH (NOLOCK)  ON cdv.DetailFieldID = df.DetailFieldID AND cdv.CaseID = ' + CAST(@caseID AS VARCHAR(20)) + '
				INNER JOIN DetailFieldPages dfp WITH (NOLOCK)  ON df.DetailFieldPageID = dfp.DetailFieldPageID
				WHERE df.QuestionTypeID = 11 -- Type is "Email Address"
				AND cdv.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + '
				AND cdv.DetailValue <> '''' '

		/*
			SELECT @SQLStatement = @SQLStatement +
				' UNION
				
				SELECT null As MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, rldv.DetailValue AS EmailAddress, rldv.EncryptedValue
				FROM DetailFields df WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK)  ON rldv.DetailFieldID = df.DetailFieldID
				INNER JOIN DetailFieldPages dfp WITH (NOLOCK)  ON df.DetailFieldPageID = dfp.DetailFieldPageID
				WHERE rldv.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + 
				'AND df.QuestionTypeID = 11 -- Type is "Email Address"
				AND rldv.DetailValue <>'''' '
				
		*/		
			/*2013-11-04 ACE Updated with the below.*/
			SELECT @SQLStatement = @SQLStatement + 
				
				' UNION

				SELECT m.MatterID, dfp.PageCaption As DetailPageCaption, dfp.PageName As DetailPageName, df.FieldCaption AS Description, df.Encrypt as FieldEncrypt, rldv.DetailValue AS EmailAddress, rldv.EncryptedValue
				FROM DetailFields df WITH (NOLOCK) 
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = df.DetailFieldID
				INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND m.CaseID = ' + CAST(@caseID AS VARCHAR(20)) + '
				INNER JOIN DetailFields df_email ON df_email.ClientID = m.ClientID AND df_email.LeadOrMatter = 4 AND df_email.QuestionTypeID = 11
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv.ValueInt AND rldv.DetailFieldID = df_email.DetailFieldID
				INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df_email.DetailFieldPageID
				WHERE df.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + '
				AND df.QuestionTypeID = 14
				AND rldv.DetailValue <> ''''
				'
		END
		
		/*
			2014-02-24 - ACE
			Added the following section in to use third party fields to extend the directory to allow the use
			of a resource. 3 Fields are required under group 64 for this to work. See Client 276 as an example
			2014-06-03 - Added Order by to sort the results
		*/
		IF EXISTS (SELECT * FROM ThirdPartyFieldMapping tpm WITH (NOLOCK) WHERE tpm.ClientID = @clientID AND tpm.ThirdPartyFieldGroupID = 64)
		BEGIN

			SELECT @SQLStatement = @SQLStatement + 
				
				' UNION

				SELECT NULL, rldv_pos.DetailValue As DetailPageCaption, rldv_name.DetailValue As DetailPageName, rldv_name.DetailValue AS Description, ''0'' as FieldEncrypt, rldv_email.DetailValue AS EmailAddress, rldv_email.EncryptedValue
				FROM ThirdPartyFieldMapping tpm_name WITH (NOLOCK)
				INNER JOIN ThirdPartyFieldMapping tpm_pos WITH (NOLOCK) ON tpm_pos.ThirdPartyFieldGroupID = tpm_name.ThirdPartyFieldGroupID AND tpm_pos.ThirdPartyFieldID = 828 /*Position*/
				INNER JOIN ThirdPartyFieldMapping tpm_email WITH (NOLOCK) ON tpm_email.ThirdPartyFieldGroupID = tpm_name.ThirdPartyFieldGroupID AND tpm_email.ThirdPartyFieldID = 829 /*Email*/
				INNER JOIN ResourceListDetailValues rldv_name WITH (NOLOCK) ON rldv_name.DetailFieldID = tpm_name.DetailFieldID /*Name*/
				INNER JOIN ResourceListDetailValues rldv_pos WITH (NOLOCK) ON rldv_pos.DetailFieldID = tpm_pos.DetailFieldID and rldv_pos.ResourceListID = rldv_name.ResourceListID /*Position*/
				INNER JOIN ResourceListDetailValues rldv_email WITH (NOLOCK) ON rldv_email.DetailFieldID = tpm_email.DetailFieldID and rldv_pos.ResourceListID = rldv_email.ResourceListID /*Email*/
				WHERE tpm_name.ClientID = ' + CAST(@clientID AS VARCHAR(20)) + '
				AND tpm_name.ThirdPartyFieldID = 827 /*Directory - Name*/
				Order by DetailPageName
				'
		
		END

	END

	EXEC(@SQLStatement)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[GetSendableEmailAddressesForLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSendableEmailAddressesForLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSendableEmailAddressesForLead] TO [sp_executeall]
GO
