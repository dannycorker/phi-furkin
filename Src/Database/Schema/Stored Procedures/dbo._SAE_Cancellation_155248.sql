SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Cancel Policy
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Cancellation_155248]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@ValueDate						DATE
			,@DOD							VARCHAR(10)
			,@ValueInt						INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

/* PH Cancel Policy (OOP) */
	IF @EventTypeID IN (155248)
	BEGIN

		/*2020-12-01 ALM SDAAG-252 Raise error if cancellation date is before claim dates*/
		IF EXISTS (SELECT * FROM _C600_ClaimData cd WITH (NOLOCK)
					INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = cd.PolicyID
					INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID
					WHERE cd.PolicyID = @LeadID
					AND mdv.DetailFieldID = 175334 /*Cancellation Date*/
					AND mdv.ValueDate < cd.ClaimTreatmentEnd)
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>The cancellation date should be after the claim treatment end date.</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END
	
		IF NOT EXISTS ( SELECT * FROM Cases ca WITH (NOLOCK) WHERE ca.CaseID=@CaseID AND ca.ClientStatusID=4702 )  
		BEGIN

			/*JEL 2019-07-04 Added for CR44. If we are trying to cancel cover before it starts, allow the cancellation to go through as if it is cancelling in it's cooling off period.
			The scenario can be identified by the cancellation date entered as between the inception date and the start date*/
			IF dbo.fnGetDvAsDate ( 175334, @CaseID ) BETWEEN dbo.fnGetDvAsDate ( 177418,@CaseID ) AND dbo.fnGetDvAsDate ( 170035, @CaseID )
			BEGIN
		
				SELECT @ValueDate = dbo.fnGetSimpleDVasDate(170036,@MatterId) 
				EXEC _C00_SimpleValueIntoField 175334, @ValueDate, @MatterID, @WhoCreated
			
			END		
			
			IF dbo.fnGetDvAsDate ( 175334, @CaseID ) NOT BETWEEN dbo.fnGetDvAsDate ( 170035,@CaseID ) AND dbo.fnGetDvAsDate ( 170037, @CaseID )
			BEGIN
		
				SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event. The date of cancellation is not between the policy start and end dates</br></font><font color="#edf4fa">'
				RAISERROR( @ErrorMessage, 16, 1 )
				RETURN
			
			END		

			-- transfer adjustment date to permanent home
			EXEC _C600_CopyFieldToField 175334, 170055, @MatterID, @WhoCreated
				
			-- morph into in process event, following up all threads 	
			EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 155261, 0

			EXEC _C00_SimpleValueIntoField 176990,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 176991,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 177079,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 177080,'',@MatterID,@Whocreated

			IF dbo.fnGetSimpleDvAsInt(170054,@MatterID) = 69911 /*Pet deceased*/
			BEGIN

				SELECT @DOD = dbo.fnGetSimpleDv(175334,@MatterID)
				
				EXEC dbo._C00_SimpleValueIntoField 144271, @DOD, @LeadID, @WhoCreated
				EXEC dbo._C00_SimpleValueIntoField 170055, @DOD, @MatterID, @WhoCreated 
				EXEC dbo._C00_SimpleValueIntoField 175452, @DOD, @MatterID, @WhoCreated 
				EXEC dbo._C00_SimpleValueIntoField 177026, @DOD, @MatterID, @WhoCreated 

			END
	
		END
		ELSE
		BEGIN
		
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event. The policy is already cancelled</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		
		END
		
		SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID) /*Cancellation Date*/
		SELECT @ValueInt = dbo.fnGetSimpleDvAsInt (178289,@WhoCreated)
		
		IF DATEDIFF(DAY,@ValueDate,dbo.fn_GetDate_Local()) > @ValueINT
		BEGIN 		
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event, date of cancellation is too far in the past. Please pass to manager</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		END
		
		--  clear temporary cancellation date ready for re-use
		EXEC dbo._C00_SimpleValueIntoField 175334, '', @MatterID
		
	END	
	
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Cancellation_155248] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Cancellation_155248] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Cancellation_155248] TO [sp_executeall]
GO
