SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LogLevel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LogLevel_Delete]
(

	@LogLevelID int   
)
AS


				DELETE FROM [dbo].[LogLevel] WITH (ROWLOCK) 
				WHERE
					[LogLevelID] = @LogLevelID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogLevel_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Delete] TO [sp_executeall]
GO
