SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-10
-- Description:	Is SMS Allowed 
-- returns (bit) 1 if allowed
-- returns (bit) 0 if not allowed
-- =============================================

CREATE PROCEDURE [dbo].[SMSStop_IsSMSAllowed]
(
	@MobileNumber	varchar(50)	
	, @LeadID		int
	, @ClientID		int	
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @isSMSAllowed	bit = 1
	, @isSMSStopEnabled		bit = 0
	, @isClientWideOptOut	bit = 1 
	, @LeadTypeID			int

	select @isClientWideOptOUT = case S.isLeadTypeSpecific when 0 then 1 else 0 end
	from smsstopconfig s with(nolock) 
	where s.clientid = @ClientID
	
	SELECT @isSMSStopEnabled  = isnull(co.EnableSMSSTOP, CAST(0 AS BIT))
	FROM ClientOption co WITH(NOLOCK) 
	WHERE co.ClientID = @ClientID
	
	IF @isSMSStopEnabled = 0 SET @isSMSAllowed = 1;
	ELSE IF @isSMSStopEnabled = 1 
	BEGIN

		IF @isClientWideOptOut = 1 
		BEGIN
			
			IF EXISTS (	
				SELECT * 
				FROM SMSStop so WITH(NOLOCK)
				WHERE so.ClientID = @ClientID
				AND so.MobileNumber = @MobileNumber 
				AND so.StopSMS = 1 
			)
			BEGIN
				
				SET @isSMSAllowed = 0

			END

		END
		ELSE IF @isClientWideOptOut = 0 /* LeadType specific opt out */
		BEGIN
			
			SELECT @LeadTypeID = l.LeadTypeID 
			FROM Lead l WITH(NOLOCK) 
			WHERE L.LeadID = @LeadID

			IF EXISTS (	
				SELECT * 
				FROM SMSStop so WITH(NOLOCK)
				WHERE so.ClientID = @ClientID
				AND so.LeadTypeID = @LeadTypeID				
				AND so.MobileNumber = @MobileNumber 
				AND so.StopSMS = 1 
			)
			BEGIN
				
				SET @isSMSAllowed = 0

			END

		END

	END

	SELECT @isSMSAllowed [isSMSAllowed]


END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_IsSMSAllowed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStop_IsSMSAllowed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_IsSMSAllowed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_IsSMSAllowed] TO [sp_executehelper]
GO
