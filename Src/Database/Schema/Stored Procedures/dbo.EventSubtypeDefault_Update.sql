SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventSubtypeDefault table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_Update]
(

	@EventSubtypeDefaultID int   ,

	@ClientID int   ,

	@EventSubtypeID int   ,

	@EventTypeID int   ,

	@LeadTypeID int   ,

	@ClientPersonnelID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventSubtypeDefault]
				SET
					[ClientID] = @ClientID
					,[EventSubtypeID] = @EventSubtypeID
					,[EventTypeID] = @EventTypeID
					,[LeadTypeID] = @LeadTypeID
					,[ClientPersonnelID] = @ClientPersonnelID
				WHERE
[EventSubtypeDefaultID] = @EventSubtypeDefaultID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Update] TO [sp_executeall]
GO
