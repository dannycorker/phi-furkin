SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartySystemEvent table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartySystemEventID],
					[ThirdPartySystemID],
					[ClientID],
					[SubClientID],
					[Name],
					[Description],
					[EventTypeID],
					[LeadTypeID],
					[ThirdPartySystemKey],
					[MessageName]
				FROM
					[dbo].[ThirdPartySystemEvent] WITH (NOLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_GetByLeadTypeID] TO [sp_executeall]
GO
