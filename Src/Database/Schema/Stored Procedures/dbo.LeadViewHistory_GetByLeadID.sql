SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadViewHistory table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_GetByLeadID]
(

	@LeadID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadViewHistoryID],
					[ClientPersonnelID],
					[ClientID],
					[LeadID],
					[WhenViewed]
				FROM
					[dbo].[LeadViewHistory] WITH (NOLOCK) 
				WHERE
					[LeadID] = @LeadID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByLeadID] TO [sp_executeall]
GO
