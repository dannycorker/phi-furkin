SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-03-20
-- Description:	#27602 Clean up targets that have unwanted RTF inside
-- =============================================
CREATE PROCEDURE [dbo].[RTFClean] 
	@String VARCHAR(MAX), 
	@ClosingBracketRequired BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @BadCharPos INT = 0, 
	@TestCharPos INT = 0,
	@Length INT

	/* Remove backwards pairs of curly braces first */
	SELECT @String = REPLACE(@String, '}{', '')

	/*
		Remove all other curly braces if this is not a format target.  But leave this type of target alone:
		
		[!Format(156065, {0:dddd dd MMM yyyy}, fr-CA)]
	*/
	IF @String NOT LIKE '[[]!Format%'
	BEGIN
		SELECT @String = REPLACE(REPLACE(@String, '{', ''), '}', '')
	
		/* #34867 This is to avoid breaking strings like [!Format(156065, {0:dd MMMM yyyy}, fr-CA)]  */
		
		/* Look for the first "\" character in the string */
		SELECT @Length = LEN(@String), 
		@BadCharPos = CHARINDEX('\', @String, 1), 
		@TestCharPos = @BadCharPos

		
		/* While we still have any "\" chars, walk forwards until a space is found and then get rid of all those chars, including the space */
		WHILE @BadCharPos > 0 AND @TestCharPos < @Length
		BEGIN
			
			SELECT @TestCharPos += 1
			
			/*
				By now we have a string like this:
				'[!A:FinalRestingY\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2631530 N\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid3356922 ]'
				
				BadCharPos is 18 initially
				TestCharPos will eventually be set to 29
				
				Stuff an empty string of '' in place of "\rtlch\fcs1 "
				
				Now the string will look like this:
				'[!A:FinalRestingY\af0 \ltrch\fcs0 \insrsid2631530 N\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid3356922 ]'
			*/
			IF SUBSTRING(@String, @TestCharPos, 1) = ' '
			BEGIN
				SELECT @String = STUFF(@String, @BadCharPos, ((@TestCharPos - @BadCharPos) + 1), '')
				
				/* Recalculate the variables and loop round again until all clean */
				SELECT @Length = LEN(@String), 
				@BadCharPos = CHARINDEX('\', @String, 1), 
				@TestCharPos = @BadCharPos
				
			END
		END
		
		/*
			Try to repair strings that end with \garbage
		*/
		IF @BadCharPos > 0
		BEGIN
			SELECT @String = dbo.fnGetCharsBeforeOrAfterSeparator(@String, '\', 1, 1)
		END
		
		/*
			 Add closing bracket if cleaning a tag like "[!A:FinalRestingYN]", but not for the resulting target "FinalRestingYN"
		*/
		IF ((@ClosingBracketRequired = 1) AND (@String NOT LIKE '%]'))
		BEGIN
			SELECT @String += ']'
		END
	
	END
	/*
		Return the clean string back to the app.  In this example it should now look like this:
		
		[!A:FinalRestingYN]
	*/
	SELECT @String
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RTFClean] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RTFClean] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RTFClean] TO [sp_executeall]
GO
