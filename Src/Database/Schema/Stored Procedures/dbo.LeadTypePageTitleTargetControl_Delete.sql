SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleTargetControl_Delete]
(

	@LeadTypePageTitleTargetControlID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleTargetControl] WITH (ROWLOCK) 
				WHERE
					[LeadTypePageTitleTargetControlID] = @LeadTypePageTitleTargetControlID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleTargetControl_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Delete] TO [sp_executeall]
GO
