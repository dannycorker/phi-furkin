SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptInputType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptInputType_Update]
(

	@ScriptInputTypeID int   ,

	@InputType varchar (50)  ,

	@Template varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptInputType]
				SET
					[InputType] = @InputType
					,[Template] = @Template
				WHERE
[ScriptInputTypeID] = @ScriptInputTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptInputType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Update] TO [sp_executeall]
GO
