SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Department table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department__FindNoLock]
(

	@SearchUsingOR bit   = null ,

	@DepartmentID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@DepartmentName varchar (100)  = null ,

	@DepartmentDescription varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DepartmentID]
	, [ClientID]
	, [CustomerID]
	, [DepartmentName]
	, [DepartmentDescription]
    FROM
	[dbo].[Department] with (nolock)
    WHERE 
	 ([DepartmentID] = @DepartmentID OR @DepartmentID is null)
	AND ([ClientID] = @ClientID OR @ClientID is null)
	AND ([CustomerID] = @CustomerID OR @CustomerID is null)
	AND ([DepartmentName] = @DepartmentName OR @DepartmentName is null)
	AND ([DepartmentDescription] = @DepartmentDescription OR @DepartmentDescription is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DepartmentID]
	, [ClientID]
	, [CustomerID]
	, [DepartmentName]
	, [DepartmentDescription]
    FROM
	[dbo].[Department] with (nolock)
    WHERE 
	 ([DepartmentID] = @DepartmentID AND @DepartmentID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([DepartmentName] = @DepartmentName AND @DepartmentName is not null)
	OR ([DepartmentDescription] = @DepartmentDescription AND @DepartmentDescription is not null)
	Select @@ROWCOUNT			
  END
				





GO
GRANT VIEW DEFINITION ON  [dbo].[Department__FindNoLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department__FindNoLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department__FindNoLock] TO [sp_executeall]
GO
