SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DashboardLayout table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardLayout_Find]
(

	@SearchUsingOR bit   = null ,

	@DashboardLayoutID int   = null ,

	@DashboardLayoutName varchar (200)  = null ,

	@TopLeftDimensions char (3)  = null ,

	@TopMiddleDimensions char (3)  = null ,

	@TopRightDimensions char (3)  = null ,

	@BottomLeftDimensions char (3)  = null ,

	@BottomMiddleDimensions char (3)  = null ,

	@BottomRightDimensions char (3)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DashboardLayoutID]
	, [DashboardLayoutName]
	, [TopLeftDimensions]
	, [TopMiddleDimensions]
	, [TopRightDimensions]
	, [BottomLeftDimensions]
	, [BottomMiddleDimensions]
	, [BottomRightDimensions]
    FROM
	[dbo].[DashboardLayout] WITH (NOLOCK) 
    WHERE 
	 ([DashboardLayoutID] = @DashboardLayoutID OR @DashboardLayoutID IS NULL)
	AND ([DashboardLayoutName] = @DashboardLayoutName OR @DashboardLayoutName IS NULL)
	AND ([TopLeftDimensions] = @TopLeftDimensions OR @TopLeftDimensions IS NULL)
	AND ([TopMiddleDimensions] = @TopMiddleDimensions OR @TopMiddleDimensions IS NULL)
	AND ([TopRightDimensions] = @TopRightDimensions OR @TopRightDimensions IS NULL)
	AND ([BottomLeftDimensions] = @BottomLeftDimensions OR @BottomLeftDimensions IS NULL)
	AND ([BottomMiddleDimensions] = @BottomMiddleDimensions OR @BottomMiddleDimensions IS NULL)
	AND ([BottomRightDimensions] = @BottomRightDimensions OR @BottomRightDimensions IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DashboardLayoutID]
	, [DashboardLayoutName]
	, [TopLeftDimensions]
	, [TopMiddleDimensions]
	, [TopRightDimensions]
	, [BottomLeftDimensions]
	, [BottomMiddleDimensions]
	, [BottomRightDimensions]
    FROM
	[dbo].[DashboardLayout] WITH (NOLOCK) 
    WHERE 
	 ([DashboardLayoutID] = @DashboardLayoutID AND @DashboardLayoutID is not null)
	OR ([DashboardLayoutName] = @DashboardLayoutName AND @DashboardLayoutName is not null)
	OR ([TopLeftDimensions] = @TopLeftDimensions AND @TopLeftDimensions is not null)
	OR ([TopMiddleDimensions] = @TopMiddleDimensions AND @TopMiddleDimensions is not null)
	OR ([TopRightDimensions] = @TopRightDimensions AND @TopRightDimensions is not null)
	OR ([BottomLeftDimensions] = @BottomLeftDimensions AND @BottomLeftDimensions is not null)
	OR ([BottomMiddleDimensions] = @BottomMiddleDimensions AND @BottomMiddleDimensions is not null)
	OR ([BottomRightDimensions] = @BottomRightDimensions AND @BottomRightDimensions is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardLayout_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Find] TO [sp_executeall]
GO
