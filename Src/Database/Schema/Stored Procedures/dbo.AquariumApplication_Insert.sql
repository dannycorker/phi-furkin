SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AquariumApplication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumApplication_Insert]
(

	@AquariumApplicationID int    OUTPUT,

	@ApplicationName varchar (50)  ,

	@ApplicationDescription varchar (250)  ,

	@CurrentVersion varchar (50)  ,

	@MinimumVersion varchar (50)  ,

	@CurrentVersionValidFrom datetime   ,

	@CurrentVersionValidTo datetime   ,

	@CurrentVersionDeployedOn datetime   
)
AS


				
				INSERT INTO [dbo].[AquariumApplication]
					(
					[ApplicationName]
					,[ApplicationDescription]
					,[CurrentVersion]
					,[MinimumVersion]
					,[CurrentVersionValidFrom]
					,[CurrentVersionValidTo]
					,[CurrentVersionDeployedOn]
					)
				VALUES
					(
					@ApplicationName
					,@ApplicationDescription
					,@CurrentVersion
					,@MinimumVersion
					,@CurrentVersionValidFrom
					,@CurrentVersionValidTo
					,@CurrentVersionDeployedOn
					)
				-- Get the identity value
				SET @AquariumApplicationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumApplication_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Insert] TO [sp_executeall]
GO
