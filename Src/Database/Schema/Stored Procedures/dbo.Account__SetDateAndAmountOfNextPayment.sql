SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 31/08/2016
-- Description:	Updates the account Next Payment Date and total
-- Mods
--	2016-09-26 DCM Changed to use ActualCollectionDate
--  2017-06-02 CPS Added StatusIDs 7 (Admin Fee)
-- =============================================
CREATE PROCEDURE [dbo].[Account__SetDateAndAmountOfNextPayment]
	@AccountID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @NextPaymentAmount NUMERIC(18,2),
			@NextPaymentDate VARCHAR(10)

	-- find earliest unprocessed date & sum amounts
	SELECT 
	@NextPaymentDate=CONVERT(VARCHAR(10),ppps.ActualCollectionDate,120), 
			@NextPaymentAmount=SUM(ppps.PaymentGross)
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	WHERE (ppps.PaymentStatusID IN (1,5,7) OR PaymentStatusID IS NULL)
		AND ppps.CustomerLedgerID IS NULL
		AND ppps.AccountID=@AccountID
		AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
						 WHERE (ppps2.PaymentStatusID IN (1,5,7) OR PaymentStatusID IS NULL) 
							AND ppps2.CustomerLedgerID IS NULL  
							AND ppps2.ActualCollectionDate < ppps.ActualCollectionDate
							AND ppps2.AccountID=ppps.AccountID )
	GROUP BY ppps.ActualCollectionDate

	
	UPDATE Account
	SET NextPaymentDate=@NextPaymentDate, 
		NextPaymentTotal=@NextPaymentAmount
	WHERE AccountID=@AccountID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__SetDateAndAmountOfNextPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__SetDateAndAmountOfNextPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__SetDateAndAmountOfNextPayment] TO [sp_executeall]
GO
