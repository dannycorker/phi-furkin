SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-09-26
-- Description:	Gets the collect a case appointment addresses
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CollectACase_GetAddresses]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @FieldAddress1 INT,
	@FieldAddress2 INT,
	@FieldTown INT,
	@FieldCounty INT,
	@FieldPostcode INT,
	@PageField INT
	
	SELECT @FieldAddress1=tpfm.ColumnFieldID, @PageField = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=339
			
	SELECT @FieldAddress2=tpfm.ColumnFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=340
	
	SELECT @FieldTown=tpfm.ColumnFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=341
	
	SELECT @FieldCounty=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=342
	
	SELECT @FieldPostcode=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=343
	
	IF @TableRowID IS NULL
	BEGIN
	
		SELECT tr.TableRowID TableRowID,
			tdvAddress1.DetailValue Address1,			
			tdvAddress2.DetailValue Address2,
			tdvTown.DetailValue Town, 
			tdvCounty.DetailValue County,
			tdvPostcode.DetailValue Postcode 
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvAddress1 WITH (NOLOCK) ON tdvAddress1.DetailFieldID = @FieldAddress1 AND tdvAddress1.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvAddress2 WITH (NOLOCK) ON tdvAddress2.DetailFieldID = @FieldAddress2 AND tdvAddress2.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvTown WITH (NOLOCK) ON tdvTown.DetailFieldID = @FieldTown AND tdvTown.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvCounty WITH (NOLOCK) ON tdvCounty.DetailFieldID = @FieldCounty AND tdvCounty.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvPostcode WITH (NOLOCK) ON tdvPostcode.DetailFieldID = @FieldPostcode AND tdvPostcode.TableRowID = tr.TableRowID
		WHERE tr.DetailFieldID = @PageField AND tr.CustomerID=@CustomerID
		
	END 
	ELSE
	BEGIN
	
		SELECT tr.TableRowID TableRowID,
			tdvAddress1.DetailValue Address1,			
			tdvAddress2.DetailValue Address2,
			tdvTown.DetailValue Town, 
			tdvCounty.DetailValue County,
			tdvPostcode.DetailValue Postcode 
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvAddress1 WITH (NOLOCK) ON tdvAddress1.DetailFieldID = @FieldAddress1 AND tdvAddress1.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvAddress2 WITH (NOLOCK) ON tdvAddress2.DetailFieldID = @FieldAddress2 AND tdvAddress2.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvTown WITH (NOLOCK) ON tdvTown.DetailFieldID = @FieldTown AND tdvTown.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvCounty WITH (NOLOCK) ON tdvCounty.DetailFieldID = @FieldCounty AND tdvCounty.TableRowID = tr.TableRowID
		INNER JOIN dbo.TableDetailValues tdvPostcode WITH (NOLOCK) ON tdvPostcode.DetailFieldID = @FieldPostcode AND tdvPostcode.TableRowID = tr.TableRowID
		WHERE tr.DetailFieldID = @PageField AND tr.CustomerID=@CustomerID AND tr.TableRowID = @TableRowID
		
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CollectACase_GetAddresses] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CollectACase_GetAddresses] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CollectACase_GetAddresses] TO [sp_executeall]
GO
