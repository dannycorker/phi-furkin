SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupRightsDynamicEditing table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamicEditing_GetByGroupRightsDynamicEditingID]
(

	@GroupRightsDynamicEditingID int   
)
AS


				SELECT
					[GroupRightsDynamicEditingID],
					[ClientPersonnelAdminGroupID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[GroupRightsDynamicEditing] WITH (NOLOCK) 
				WHERE
										[GroupRightsDynamicEditingID] = @GroupRightsDynamicEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_GetByGroupRightsDynamicEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamicEditing_GetByGroupRightsDynamicEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_GetByGroupRightsDynamicEditingID] TO [sp_executeall]
GO
