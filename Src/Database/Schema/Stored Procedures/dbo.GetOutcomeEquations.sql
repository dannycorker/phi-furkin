SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetOutcomeEquations] @OutcomeID int
AS

SELECT     dbo.OutcomeEquations.OutcomeID, dbo.OutcomeEquations.EquationID, dbo.Equations.EquationName, dbo.Equations.Equation, 
                      dbo.OutcomeEquations.OutcomeEquationID, dbo.Outcomes.ClientQuestionnaireID, dbo.Outcomes.ClientID 
FROM         dbo.OutcomeEquations INNER JOIN
                      dbo.Equations ON dbo.OutcomeEquations.EquationID = dbo.Equations.EquationID INNER JOIN
                      dbo.Outcomes ON dbo.OutcomeEquations.OutcomeID = dbo.Outcomes.OutcomeID
WHERE dbo.Outcomes.OutcomeID = @OutcomeID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeEquations] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeEquations] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeEquations] TO [sp_executeall]
GO
