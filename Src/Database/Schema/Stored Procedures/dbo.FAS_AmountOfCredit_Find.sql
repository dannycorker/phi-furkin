SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FAS_AmountOfCredit table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_Find]
(

	@SearchUsingOR bit   = null ,

	@FasAmountOfCreditID int   = null ,

	@FasCheckerDataID int   = null ,

	@Description varchar (512)  = null ,

	@CostAmount decimal (19, 4)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FasAmountOfCreditID]
	, [FasCheckerDataID]
	, [Description]
	, [CostAmount]
    FROM
	[dbo].[FAS_AmountOfCredit] WITH (NOLOCK) 
    WHERE 
	 ([FasAmountOfCreditID] = @FasAmountOfCreditID OR @FasAmountOfCreditID IS NULL)
	AND ([FasCheckerDataID] = @FasCheckerDataID OR @FasCheckerDataID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([CostAmount] = @CostAmount OR @CostAmount IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FasAmountOfCreditID]
	, [FasCheckerDataID]
	, [Description]
	, [CostAmount]
    FROM
	[dbo].[FAS_AmountOfCredit] WITH (NOLOCK) 
    WHERE 
	 ([FasAmountOfCreditID] = @FasAmountOfCreditID AND @FasAmountOfCreditID is not null)
	OR ([FasCheckerDataID] = @FasCheckerDataID AND @FasCheckerDataID is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([CostAmount] = @CostAmount AND @CostAmount is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Find] TO [sp_executeall]
GO
