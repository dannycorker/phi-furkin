SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientTypes_Delete]
(

	@ClientTypeID int   
)
AS


				DELETE FROM [dbo].[ClientTypes] WITH (ROWLOCK) 
				WHERE
					[ClientTypeID] = @ClientTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientTypes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Delete] TO [sp_executeall]
GO
