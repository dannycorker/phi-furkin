SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Returns all parameters for a specific calulation
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_CalculationParameters_Get]
(
	@ClientID INT,
	@CalculationID INT,
	@CalculationParameterID INT = NULL
)


AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	p.CalculationParameterID, p.Name, p.ParameterTypeID, t.Name AS ParameterType, p.Value, 
			p.WhenCreated, cpCreate.UserName AS WhoCreated, p.WhenModified, cpMod.UserName AS WhoModified, p.Description
	FROM dbo.CalcsEngine_CalculationParameters p WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON p.WhoCreated = cpCreate.ClientPersonnelID
	INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON p.WhoModified = cpMod.ClientPersonnelID
	INNER JOIN dbo.CalcsEngine_ParameterTypes t WITH (NOLOCK) ON p.ParameterTypeID = t.ParameterTypeID
	WHERE p.ClientID = @ClientID
	AND p.CalculationID = @CalculationID
	AND (@CalculationParameterID IS NULL OR p.CalculationParameterID = @CalculationParameterID)

	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationParameters_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_CalculationParameters_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationParameters_Get] TO [sp_executeall]
GO
