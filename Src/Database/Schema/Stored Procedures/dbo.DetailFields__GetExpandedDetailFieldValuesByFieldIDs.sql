SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-10-07
-- Description:	Used in the SDK this PROC gets matter or lead detail values by specified field ids.  It expands lookup lists, and resource lists nested 
-- in tables so the SDK can return a nicely formatted collection of objects
-- UPDATE - Added field caption to everything APART FROM Tables!  TODO.
-- UPDATE - Added support for customer detail fields
-- UPDATE - Added support for case detail fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetExpandedDetailFieldValuesByFieldIDs]

	@ClientID INT,
	@LeadID INT = 0,
	@MatterID INT = 0,
	@FieldIDs tvpInt READONLY,
	@CustomerID INT = 0,
	@CaseID INT = 0,
	@ExcludeHiddenFields bit = 0
AS
BEGIN
	
	IF @CaseID > 0
	BEGIN
		EXEC [dbo].[DetailFields__GetExpandedCaseValuesByFieldIDs] @ClientID, @CaseID, @FieldIDs, @ExcludeHiddenFields
	END
	ELSE
	BEGIN
		IF @MatterID > 0 
		BEGIN
			EXEC [dbo].[DetailFields__GetExpandedMatterValuesByFieldIDs] @ClientID, @MatterID, @FieldIDs, @ExcludeHiddenFields
		END
		ELSE
		BEGIN
			IF @LeadID > 0 
			BEGIN
				EXEC [dbo].[DetailFields__GetExpandedLeadValuesByFieldIDs] @ClientID, @LeadID, @FieldIDs, @ExcludeHiddenFields
			END
			ELSE IF @CustomerID > 0 
			BEGIN
				EXEC [dbo].[DetailFields__GetExpandedCustomerValuesByFieldIDs] @ClientID, @CustomerID, @FieldIDs, @ExcludeHiddenFields 
			END
			ELSE
			BEGIN
				EXEC [dbo].[DetailFields__GetExpandedClientValuesByFieldIDs] @ClientID, @FieldIDs, @ExcludeHiddenFields
			END
		END
	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedDetailFieldValuesByFieldIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetExpandedDetailFieldValuesByFieldIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedDetailFieldValuesByFieldIDs] TO [sp_executeall]
GO
