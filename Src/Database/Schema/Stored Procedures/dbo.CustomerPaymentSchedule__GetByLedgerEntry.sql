SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-20
-- Description:	Gets a customer payment schedule for a given ledger entry
--				There can be only one customer payment record per ledger entry since it is 
--				the consolidated purchase product payment schedule for a single date
-- =============================================
CREATE PROCEDURE [dbo].[CustomerPaymentSchedule__GetByLedgerEntry]

	@CustomerLedgerID INT,
	@CustomerID INT,
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT Top 1 * FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	WHERE cps.CustomerLedgerID=@CustomerLedgerID AND cps.CustomerID=@CustomerID AND cps.ClientID=@ClientID
	ORDER BY cps.PaymentDate DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__GetByLedgerEntry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerPaymentSchedule__GetByLedgerEntry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__GetByLedgerEntry] TO [sp_executeall]
GO
