SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett / Robin Hall
-- ALTER date: 2013-04-22 / 2015-12-17
-- Description:	SIG MI - Integration Failures
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MI_IntegrationFailures]
(
	@CheckFrom DATE = NULL
)
AS
BEGIN

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	SELECT @CheckFrom = ISNULL(@CheckFrom, CONVERT(DATE, DATEADD(DAY, -1, dbo.fn_GetDate_Local())))
	
	SELECT	
		'Automated Import / Export Failures' AS [Automated Import / Export Failures]
		,s.ScriptID, ss.AutomatedScriptScheduleID AS ScheduleID, s.ScriptName, ss.Name ScheduleName, ss.ServerIP
		, r.RunDate, r.DurationSeconds, r.RecordCount, LEFT(r.Description,31999) /*LB #32157*/
	FROM 
		[AquariusMaster].dbo.AutomatedScriptResult r WITH (NOLOCK) 
		INNER JOIN [AquariusMaster].dbo.AutomatedScriptSchedule ss WITH (NOLOCK) ON r.AutomatedScriptScheduleID = ss.AutomatedScriptScheduleID
		INNER JOIN [AquariusMaster].dbo.AutomatedScript s WITH (NOLOCK) ON r.ScriptID = s.ScriptID
	WHERE 
		r.ClientID = @ClientID
		AND r.Complete = 0
		AND ss.ServerIP != '127.0.0.1:52877'
		AND ss.ServerIP != 'localhost:52877'
		AND r.RunDate > @CheckFrom
	ORDER BY
		r.RunDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_IntegrationFailures] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MI_IntegrationFailures] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_IntegrationFailures] TO [sp_executeall]
GO
