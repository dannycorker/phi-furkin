SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClassNode table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_GetByClassNodeID]
(

	@ClassNodeID int   
)
AS


				SELECT
					[ClassNodeID],
					[ClientID],
					[ParentClassNodeID],
					[NodeType],
					[Name],
					[ClassDescription],
					[NodeOrder],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ClassNode] WITH (NOLOCK) 
				WHERE
										[ClassNodeID] = @ClassNodeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_GetByClassNodeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_GetByClassNodeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_GetByClassNodeID] TO [sp_executeall]
GO
