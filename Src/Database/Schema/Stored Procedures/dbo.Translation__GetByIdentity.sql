SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-06-2014
-- Description:	Gets a translation set
-- =============================================
CREATE PROCEDURE [dbo].[Translation__GetByIdentity]

	@ClientID INT, 
	@ID INT,	
	@LanguageID INT = NULL -- NULL OR 1 is default English

AS
BEGIN

	SET NOCOUNT ON;

	IF @LanguageID IS NULL
	BEGIN
		SELECT @LanguageID = 1
	END

	SELECT * 
	FROM 
		Translation t WITH (NOLOCK) 
	WHERE 
		t.ClientID = @ClientID AND 
		t.ID = @ID AND		
		t.LanguageID = @LanguageID	
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Translation__GetByIdentity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Translation__GetByIdentity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Translation__GetByIdentity] TO [sp_executeall]
GO
