SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeMandatoryEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_Update]
(

	@EventTypeMandatoryEventID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ToEventTypeID int   ,

	@RequiredEventID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeMandatoryEvent]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[ToEventTypeID] = @ToEventTypeID
					,[RequiredEventID] = @RequiredEventID
				WHERE
[EventTypeMandatoryEventID] = @EventTypeMandatoryEventID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Update] TO [sp_executeall]
GO
