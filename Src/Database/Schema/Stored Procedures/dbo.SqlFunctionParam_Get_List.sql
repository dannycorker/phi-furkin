SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlFunctionParam table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunctionParam_Get_List]
AS

SELECT
	[SqlFunctionParamID],
	[SqlFunctionID],
	[ParamName],
	[ParamDescription],
	[ParamOrder],
	[IsMandatory],
	[SqlFunctionDataTypeID],
	[WhenCreated],
	[WhenModified],
	[IsRecurring],
	[DecimalPlaces],
	[MinValue],
	[MaxValue],
	[MaxLength],
	[UseQueryColumn],
	[QuoteValue],
	[PrefixText],
	[SuffixText],
	[ListTypeId]
FROM
	[dbo].[SqlFunctionParam] WITH (NOLOCK)
		




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunctionParam_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunctionParam_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunctionParam_Get_List] TO [sp_executeall]
GO
