SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ValidationCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriterias_Get_List]

AS


				
				SELECT
					[ValidationCriteriaID],
					[Criteria]
				FROM
					[dbo].[ValidationCriterias] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriterias_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Get_List] TO [sp_executeall]
GO
