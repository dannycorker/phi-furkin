SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserGroupMenuSubOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuSubOptionID]
(

	@UserGroupMenuSubOptionID int   
)
AS


				SELECT
					[UserGroupMenuSubOptionID],
					[UserGroupMenuOptionID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[PanelItemName],
					[PanelItemCaption],
					[PanelItemURL],
					[ShowInFancyBox],
					[SubItemOrder]
				FROM
					[dbo].[UserGroupMenuSubOption] WITH (NOLOCK) 
				WHERE
										[UserGroupMenuSubOptionID] = @UserGroupMenuSubOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuSubOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuSubOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuSubOptionID] TO [sp_executeall]
GO
