SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFile_Get_List]

AS


				
				SELECT
					[DataLoaderFileID],
					[ClientID],
					[DataLoaderMapID],
					[FileStatusID],
					[SourceFileNameAndPath],
					[TargetFileName],
					[TargetFileLocation],
					[FileFormatID],
					[ScheduledDateTime],
					[DataLoadedDateTime],
					[UploadedBy],
					[RowsInFile]
				FROM
					[dbo].[DataLoaderFile] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Get_List] TO [sp_executeall]
GO
