SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-24
-- Description:	Get all function control records for this Group
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupFunctionControls]
	-- Add the parameters for the stored procedure here
	@GroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 

END






GO
GRANT VIEW DEFINITION ON  [dbo].[GetGroupFunctionControls] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetGroupFunctionControls] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetGroupFunctionControls] TO [sp_executeall]
GO
