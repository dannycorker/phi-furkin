SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventType_Delete]
(

	@RPIEventTypeID int   
)
AS


				DELETE FROM [dbo].[RPIEventType] WITH (ROWLOCK) 
				WHERE
					[RPIEventTypeID] = @RPIEventTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Delete] TO [sp_executeall]
GO
