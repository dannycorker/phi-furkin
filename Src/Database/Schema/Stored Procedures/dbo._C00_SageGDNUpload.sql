SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-20
-- Description:	Upload GDN XML
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SageGDNUpload]
	@GDNData VARCHAR(MAX),
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO LogXML (ClientID, ContextID, ContextVarchar, InvalidXML, LogDateTime, LogXMLEntry)
	VALUES (@ClientID, 93, 'XML Upload', NULL, dbo.fn_GetDate_Local(), @GDNData)
	
	INSERT INTO AsyncQueue (ClientID, WhenCreated, QueueTypeID, Status, Payload, Outcome, WhenCompleted)
	VALUES (@ClientID, dbo.fn_GetDate_Local(), 4, 1, @GDNData, NULL, NULL)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageGDNUpload] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SageGDNUpload] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageGDNUpload] TO [sp_executeall]
GO
