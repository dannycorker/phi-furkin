SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-09-02
-- Description:	Returns card counts for all active programs
-- UPDATE: Fixed as program is now a resource list rather than a lookup list
-- UPDATE: Now showing all cards!
-- UPDATE: IS include non card accounts
-- =============================================
CREATE PROCEDURE [dbo].[BostonMarks_GetCardCounts] 
	@ClientID INT,
	@ProgramTypeDetailFieldID INT,
	@StatusDetailFieldID INT,
	@StatusPendingLookupListID INT,
	@StatusActiveLookupListID INT,
	@StatusBlockedLookupListID INT,
	@StatusCanceledLookupListID INT,
	@SubClientID INT = 1
AS
BEGIN

	SELECT mv.DetailValue AS ProgramID, COUNT(*) AS CardCount
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON m.CustomerID = c.CustomerID
	INNER JOIN dbo.MatterDetailValues mv WITH (NOLOCK) ON m.MatterID = mv.MatterID AND mv.DetailFieldID = 117771
	LEFT JOIN dbo.MatterDetailValues cStatus WITH (NOLOCK) ON m.MatterID = cStatus.MatterID AND cStatus.DetailFieldID = 117802
	LEFT JOIN dbo.MatterDetailValues aStatus WITH (NOLOCK) ON m.MatterID = aStatus.MatterID AND aStatus.DetailFieldID = 118490
	-- Include all status apart from pending, ordered and order failed
	AND 
	(
		cStatus.DetailValue NOT IN ('19432','19420','25222','')
		OR
		aStatus.DetailValue NOT IN ('19432','19420','25222','')
	)
	WHERE c.ClientID = 183
	AND (c.Test IS NULL OR c.Test = 0)
	AND c.SubClientID = @SubClientID
	GROUP BY mv.DetailValue
	
	/*
	
	SELECT mv.DetailValue AS ProgramID, COUNT(*) AS CardCount
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON m.CustomerID = c.CustomerID
	INNER JOIN dbo.MatterDetailValues mv WITH (NOLOCK) ON m.MatterID = mv.MatterID AND mv.DetailFieldID = 117771
	INNER JOIN dbo.MatterDetailValues dvStatus WITH (NOLOCK) ON m.MatterID = dvStatus.MatterID AND dvStatus.DetailFieldID = 117802
	-- Include all status apart from pending, ordered and order failed
	AND (dvStatus.DetailValue != 19432 AND 
	dvStatus.DetailValue != 19420 AND 
	dvStatus.DetailValue != 25222 AND
	dvStatus.DetailValue != '')
	WHERE c.ClientID = 183
	AND (c.Test IS NULL OR c.Test = 0)
	AND c.SubClientID = @SubClientID
	GROUP BY mv.DetailValue
	
	*/

END
GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_GetCardCounts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BostonMarks_GetCardCounts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_GetCardCounts] TO [sp_executeall]
GO
