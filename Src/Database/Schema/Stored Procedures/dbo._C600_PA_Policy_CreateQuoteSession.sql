SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-10-21
-- Description:	Create Quote Session
-- Mods:
--	2017-02-02 DCM Removed logXML logging
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_CreateQuoteSession]
(
	@XmlRequest XML
)
AS
BEGIN

	DECLARE @ClientID INT
	SELECT	@ClientID = @XmlRequest.value('(//ClientId)[1]', 'INT')
	
	DECLARE @XmlResponse XML


	DECLARE @Now DATETIME = dbo.fn_GetDate_Local()
	

	DECLARE 
			@QuoteSessionID INT,
			@BrandID INT,
			@Postcode VARCHAR(50),
			@County VARCHAR(200),
			@FirstName VARCHAR(100),
			@LastName VARCHAR(100),
			@Email VARCHAR(255),
			@HomePhone VARCHAR(50), 
			@Mobile VARCHAR(50),
			@TrackingCode VARCHAR(50),
			@SignUpMethod INT,
			@LastPeriodExitPremium MONEY = 0,
			@LossRationPercentage MONEY = 0,
			@NumberPreviousClaims INT = 0,
			@PaymentFrequency VARCHAR(20),
			@PolicyYearNumber INT = 1,
			@CustomerID INT
			
	SELECT	@QuoteSessionID = @XmlRequest.value('(//SessionId)[1]', 'INT'),
			@BrandID = @XmlRequest.value('(//BrandingId)[1]', 'INT'),
			@Postcode = @XmlRequest.value('(//Postcode)[1]', 'VARCHAR(50)'),
			@County = @XmlRequest.value('(//County)[1]', 'VARCHAR(200)'),
			@FirstName = @XmlRequest.value('(//FirstName)[1]', 'VARCHAR(100)'),
			@LastName = @XmlRequest.value('(//LastName)[1]', 'VARCHAR(100)'),
			@Email = @XmlRequest.value('(//Email)[1]', 'VARCHAR(255)'),
			@HomePhone = @XmlRequest.value('(//HomePhone)[1]', 'varchar(50)'),
			@Mobile	= @XmlRequest.value('(//MobilePhone)[1]', 'varchar(50)'),
			@TrackingCode = @XmlRequest.value('(//TrackingCode)[1]', 'varchar(50)'),
			@SignUpMethod = CASE WHEN @XmlRequest.value('(//SignUpMethod)[1]', 'varchar(50)') = 'Online' THEN 74325 ELSE 74326 END,
			@PaymentFrequency = 'MO', -- default to monthly, payment frequency is only used for renewals
			@CustomerID = @XmlRequest.value('(//CustomerID)[1]', 'INT')

	
	INSERT dbo._C600_QuoteSessions(BrandID, FirstName, LastName, Email, PostCode, HomeTel, MobileTel, QuoteStart, QuoteCount, TrackingCode, CustomerID, LastQuoteStart)
	VALUES (@BrandID, @FirstName, @LastName, @Email, @Postcode, @HomePhone, @Mobile, @Now, 1, @TrackingCode, @CustomerID, @Now)
	
	SELECT @QuoteSessionID = SCOPE_IDENTITY()

	SELECT @QuoteSessionID SessionId
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_CreateQuoteSession] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_CreateQuoteSession] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_CreateQuoteSession] TO [sp_executeall]
GO
