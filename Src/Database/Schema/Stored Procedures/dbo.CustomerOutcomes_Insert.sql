SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomerOutcomes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOutcomes_Insert]
(

	@CustomerOutcomeID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@OutcomeID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[CustomerOutcomes]
					(
					[ClientQuestionnaireID]
					,[CustomerID]
					,[OutcomeID]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@CustomerID
					,@OutcomeID
					,@ClientID
					)
				-- Get the identity value
				SET @CustomerOutcomeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOutcomes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Insert] TO [sp_executeall]
GO
