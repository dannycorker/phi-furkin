SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_Get_List]

AS


				
				SELECT
					[SqlQueryEditingID],
					[ClientID],
					[SqlQueryID],
					[WhoIsEditing],
					[EditStartedAt],
					[QueryText],
					[QueryTitle],
					[LeadTypeID],
					[FolderID],
					[ParentQueryID],
					[LockAllTables]
				FROM
					[dbo].[SqlQueryEditing] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Get_List] TO [sp_executeall]
GO
