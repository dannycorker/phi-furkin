SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ExternalApplicationLogin table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_Insert]
(

	@ExternalApplicationLoginID int    OUTPUT,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@UserName varchar (255)  ,

	@Password varchar (65)  ,

	@Salt varchar (50)  ,

	@ClientPersonnelID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@AttemptedLogins int   ,

	@AccountDisabled bit   ,

	@CustomerID int   
)
AS


				
				INSERT INTO [dbo].[ExternalApplicationLogin]
					(
					[ThirdPartySystemID]
					,[ClientID]
					,[SubClientID]
					,[UserName]
					,[Password]
					,[Salt]
					,[ClientPersonnelID]
					,[ClientPersonnelAdminGroupID]
					,[AttemptedLogins]
					,[AccountDisabled]
					,[CustomerID]
					)
				VALUES
					(
					@ThirdPartySystemID
					,@ClientID
					,@SubClientID
					,@UserName
					,@Password
					,@Salt
					,@ClientPersonnelID
					,@ClientPersonnelAdminGroupID
					,@AttemptedLogins
					,@AccountDisabled
					,@CustomerID
					)
				-- Get the identity value
				SET @ExternalApplicationLoginID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Insert] TO [sp_executeall]
GO
