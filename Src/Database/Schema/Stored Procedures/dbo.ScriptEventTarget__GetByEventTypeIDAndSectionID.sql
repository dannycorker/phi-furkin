SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-03-2015
-- Description:	Get a script event target by the event type id and the section id
-- =============================================
CREATE PROCEDURE [dbo].[ScriptEventTarget__GetByEventTypeIDAndSectionID]

	@EventTypeID INT,
	@SectionID INT

AS
BEGIN

	SET NOCOUNT ON;
	SELECT * FROM ScriptEventTarget WITH (NOLOCK) 
	WHERE EventTypeID=@EventTypeID AND SectionID=@SectionID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget__GetByEventTypeIDAndSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget__GetByEventTypeIDAndSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget__GetByEventTypeIDAndSectionID] TO [sp_executeall]
GO
