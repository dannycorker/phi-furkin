SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReferenceValueDateFormat table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDateFormat_GetByReferenceValueDateFormatID]
(

	@ReferenceValueDateFormatID int   
)
AS


				SELECT
					[ReferenceValueDateFormatID],
					[DateFormat]
				FROM
					[dbo].[ReferenceValueDateFormat] WITH (NOLOCK) 
				WHERE
										[ReferenceValueDateFormatID] = @ReferenceValueDateFormatID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_GetByReferenceValueDateFormatID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDateFormat_GetByReferenceValueDateFormatID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_GetByReferenceValueDateFormatID] TO [sp_executeall]
GO
