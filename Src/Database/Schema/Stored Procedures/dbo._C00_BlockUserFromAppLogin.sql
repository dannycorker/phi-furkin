SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Daniel Corker (via Cathal Sherry)
-- Create date: 2016-03-01
--				Insert a __LoginExclusions record to block a user from front-end login
-- =============================================

CREATE PROCEDURE [dbo].[_C00_BlockUserFromAppLogin]
(
	@LogonExclusionID	INT	OUTPUT,
	@ClientID			INT,
	@ClientPersonnelID	INT,
	@Description		VARCHAR (200)  
)
AS
BEGIN

	/*Check that this user matches this client*/
	IF NOT EXISTS ( SELECT * 
	                FROM ClientPersonnel cp WITH ( NOLOCK ) 
	                WHERE cp.ClientPersonnelID = @ClientPersonnelID
	                AND cp.ClientID = @ClientID )
	BEGIN
		RAISERROR( 'Error:  This user does not match this client', 16, 1 )
		RETURN
	END
	
	IF EXISTS ( SELECT * FROM __LoginExclusions le WITH ( NOLOCK ) WHERE le.ClientPersonnelID = @ClientPersonnelID )
	BEGIN
		RAISERROR( 'Error:  This user is already blocked from app login', 16, 1 )
		RETURN
	END

	INSERT INTO [dbo].[__LoginExclusions]
		(
		[ClientID]
		,[ClientPersonnelID]
		,[Description]
		)
	VALUES
		(
		@ClientID
		,@ClientPersonnelID
		,@Description
		)
	
	-- Get the identity value
	SET @LogonExclusionID = SCOPE_IDENTITY()
END						

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BlockUserFromAppLogin] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BlockUserFromAppLogin] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BlockUserFromAppLogin] TO [sp_executeall]
GO
