SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2015
-- Description:	Gets a social customer by the screen name and userid
-- =============================================
CREATE PROCEDURE [dbo].[SocialCustomer__GetByScreenNameAndUserID]
	
	@ScreenName VARCHAR(250),
	@SocialUserID VARCHAR(50)

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM SocialCustomer WITH (NOLOCK) 
	WHERE ScreenName=@ScreenName AND SocialUserID = @SocialUserID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__GetByScreenNameAndUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialCustomer__GetByScreenNameAndUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__GetByScreenNameAndUserID] TO [sp_executeall]
GO
