SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartySystemEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_Insert]
(

	@ThirdPartySystemEventID int    OUTPUT,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@Name varchar (250)  ,

	@Description varchar (250)  ,

	@EventTypeID int   ,

	@LeadTypeID int   ,

	@ThirdPartySystemKey int   ,

	@MessageName varchar (250)  
)
AS


				
				INSERT INTO [dbo].[ThirdPartySystemEvent]
					(
					[ThirdPartySystemID]
					,[ClientID]
					,[SubClientID]
					,[Name]
					,[Description]
					,[EventTypeID]
					,[LeadTypeID]
					,[ThirdPartySystemKey]
					,[MessageName]
					)
				VALUES
					(
					@ThirdPartySystemID
					,@ClientID
					,@SubClientID
					,@Name
					,@Description
					,@EventTypeID
					,@LeadTypeID
					,@ThirdPartySystemKey
					,@MessageName
					)
				-- Get the identity value
				SET @ThirdPartySystemEventID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Insert] TO [sp_executeall]
GO
