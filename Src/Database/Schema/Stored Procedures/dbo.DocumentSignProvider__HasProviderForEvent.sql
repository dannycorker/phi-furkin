SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__HasProviderForEvent]
	@LeadEventID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DocumentTypeID INT,
			@EventTypeID INT

	SELECT	@DocumentTypeID = dt.DocumentTypeID, 
			@EventTypeID = et.EventTypeID
	FROM	LeadEvent le WITH (NOLOCK) 
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
	INNER JOIN DocumentType dt WITH (NOLOCK) ON  dt.DocumentTypeID = et.DocumentTypeID
	WHERE	le.LeadEventID = @LeadEventID
	AND		et.EventSubtypeID = 6
	
	SELECT	TOP 1 
			dspt.DSPTypeID ProviderID,
			dspu.UserName,
			dspu.Password,
			dspa.IntegrationKey,
			dspa.AccountKey,
			dspt.DSPSignatureTemplate,
			dspm.BrandingKey
	FROM	DocumentSignProviderMapping dspm WITH (NOLOCK)
	INNER JOIN DocumentSignProviderType dspt WITH (NOLOCK) ON dspt.DSPTypeID = dspm.DSPTypeID
	INNER JOIN DocumentSignProviderAccount dspa WITH (NOLOCK) ON dspa.DSPAccountID = dspm.DSPAccountID
	INNER JOIN DocumentSignProviderUser dspu WITH (NOLOCK) ON dspu.DSPUserID = dspm.DSPUserID
	WHERE	dspm.EmailOutETID = @EventTypeID AND (dspm.OutDocumentTypeID = @DocumentTypeID OR dspm.OutDocumentTypeID IS NULL)
	AND		dspm.[Enabled] = 1
	ORDER BY dspm.EmailOutETID, dspm.OutDocumentTypeID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__HasProviderForEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__HasProviderForEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__HasProviderForEvent] TO [sp_executeall]
GO
