SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AutomatedTaskParam table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_Update]
(

	@AutomatedTaskParamID int   ,

	@TaskID int   ,

	@ClientID int   ,

	@ParamName varchar (50)  ,

	@ParamValue varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AutomatedTaskParam]
				SET
					[TaskID] = @TaskID
					,[ClientID] = @ClientID
					,[ParamName] = @ParamName
					,[ParamValue] = @ParamValue
				WHERE
[AutomatedTaskParamID] = @AutomatedTaskParamID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Update] TO [sp_executeall]
GO
