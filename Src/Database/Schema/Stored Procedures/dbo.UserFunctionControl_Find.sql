SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the UserFunctionControl table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_Find]
(

	@SearchUsingOR bit   = null ,

	@UserFunctionControlID int   = null ,

	@ClientPersonnelID int   = null ,

	@ModuleID int   = null ,

	@FunctionTypeID int   = null ,

	@HasDescendants int   = null ,

	@RightID int   = null ,

	@LeadTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [UserFunctionControlID]
	, [ClientPersonnelID]
	, [ModuleID]
	, [FunctionTypeID]
	, [HasDescendants]
	, [RightID]
	, [LeadTypeID]
    FROM
	[dbo].[UserFunctionControl] WITH (NOLOCK) 
    WHERE 
	 ([UserFunctionControlID] = @UserFunctionControlID OR @UserFunctionControlID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ModuleID] = @ModuleID OR @ModuleID IS NULL)
	AND ([FunctionTypeID] = @FunctionTypeID OR @FunctionTypeID IS NULL)
	AND ([HasDescendants] = @HasDescendants OR @HasDescendants IS NULL)
	AND ([RightID] = @RightID OR @RightID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [UserFunctionControlID]
	, [ClientPersonnelID]
	, [ModuleID]
	, [FunctionTypeID]
	, [HasDescendants]
	, [RightID]
	, [LeadTypeID]
    FROM
	[dbo].[UserFunctionControl] WITH (NOLOCK) 
    WHERE 
	 ([UserFunctionControlID] = @UserFunctionControlID AND @UserFunctionControlID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ModuleID] = @ModuleID AND @ModuleID is not null)
	OR ([FunctionTypeID] = @FunctionTypeID AND @FunctionTypeID is not null)
	OR ([HasDescendants] = @HasDescendants AND @HasDescendants is not null)
	OR ([RightID] = @RightID AND @RightID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Find] TO [sp_executeall]
GO
