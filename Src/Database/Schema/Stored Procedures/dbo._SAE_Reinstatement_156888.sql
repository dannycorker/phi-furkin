SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Calculate reinstatement amount
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Reinstatement_156888]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PurchasedProductID			INT
			,@PurchasedProductPaymentScheduleID	INT
			,@CancellationDate				DATE

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


/* Calculate Re-instatement Amount */
	IF @EventTypeID IN (156888) 
	BEGIN
	
		-- find refund row to reverse this will be the last adhoc row added (if it is negative then a refund was given add that will need reversing)
		SELECT TOP 1 @PurchasedProductID = ppps.PurchasedProductID, @PurchasedProductPaymentScheduleID = ISNULL(ppps.PurchasedProductPaymentScheduleID, 0)
		FROM MatterDetailValues mpp WITH (NOLOCK)
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = mpp.ValueInt 
		LEFT JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON pp.PurchasedProductID = ppps.PurchasedProductID
		WHERE ppps.PurchasedProductPaymentScheduleTypeID = 7 /*Cancellation Adjustment*/
		AND mpp.MatterID=@MatterID
		AND mpp.DetailFieldID=177074 
		ORDER BY ppps.PurchasedProductPaymentScheduleID DESC

		EXEC _C00_SimpleValueIntoField 177139,@PurchasedProductPaymentScheduleID,@MatterID,@WhoCreated  -- ppps to reverse ( 0 if nothing to reverse )
		/*We already have this, so make sure we don't overwrite*/
		IF ISNULL(@PurchasedProductID,'')<> ''
		BEGIN
			EXEC _C00_SimpleValueIntoField 177031,@PurchasedProductID,@MatterID,@WhoCreated  -- purchased product ID
		END

		EXEC _C00_SimpleValueIntoField 177022,'Reinstatement',@MatterID,@WhoCreated  -- description
		EXEC _C00_SimpleValueIntoField 177377,1,@MatterID,@WhoCreated  -- adjustment value only

		/*JEL 2019-03-14 Ensure the cancellation date is written to the collection fields*/ 
		SELECT @CancellationDate = dbo.fnGetSimpleDvAsDATE(170055,@MatterID)
		EXEC _C00_SimpleValueIntoField 177026, @CancellationDate, @MatterID, @WhoCreated 
			
		EXEC dbo.Reinstatement__CreateFromThirdPartyFields @ClientID, @CaseID, @LeadEventID
		
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Reinstatement_156888] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Reinstatement_156888] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Reinstatement_156888] TO [sp_executeall]
GO
