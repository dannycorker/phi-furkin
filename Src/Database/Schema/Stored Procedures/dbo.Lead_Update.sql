SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Lead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_Update]
(

	@LeadID int   ,

	@ClientID int   ,

	@LeadRef varchar (100)  ,

	@CustomerID int   ,

	@LeadTypeID int   ,

	@AquariumStatusID int   ,

	@ClientStatusID int   ,

	@BrandNew bit   ,

	@Assigned bit   ,

	@AssignedTo int   ,

	@AssignedBy int   ,

	@AssignedDate datetime   ,

	@RecalculateEquations bit   ,

	@Password varchar (65)  ,

	@Salt varchar (25)  ,

	@WhenCreated datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Lead]
				SET
					[ClientID] = @ClientID
					,[LeadRef] = @LeadRef
					,[CustomerID] = @CustomerID
					,[LeadTypeID] = @LeadTypeID
					,[AquariumStatusID] = @AquariumStatusID
					,[ClientStatusID] = @ClientStatusID
					,[BrandNew] = @BrandNew
					,[Assigned] = @Assigned
					,[AssignedTo] = @AssignedTo
					,[AssignedBy] = @AssignedBy
					,[AssignedDate] = @AssignedDate
					,[RecalculateEquations] = @RecalculateEquations
					,[Password] = @Password
					,[Salt] = @Salt
					,[WhenCreated] = @WhenCreated
				WHERE
[LeadID] = @LeadID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Update] TO [sp_executeall]
GO
