SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-06-17
-- Description:	Wraps _C00_SimpleValueIntoField for calling from custom controls
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SimpleValueIntoField_FromControl]
(
	@DetailFieldID INT,
	@DetailValue VARCHAR(2000),
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@ClientPersonnelID INT
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @LeadOrMatter INT,
			@ObjectID INT

	SELECT	@LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID
	
	IF @LeadOrMatter = 1
	BEGIN
		SELECT @ObjectID = @LeadID
	END
	ELSE IF @LeadOrMatter = 2
	BEGIN
		SELECT @ObjectID = @MatterID
	END
	ELSE IF @LeadOrMatter = 10
	BEGIN
		SELECT @ObjectID = @CustomerID
	END
	ELSE IF @LeadOrMatter = 11
	BEGIN
		SELECT @ObjectID = @CaseID
	END
	ELSE IF @LeadOrMatter = 12
	BEGIN
		SELECT @ObjectID = @ClientID
	END
	ELSE IF @LeadOrMatter = 13
	BEGIN
		SELECT @ObjectID = @ClientPersonnelID
	END
	
	EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @DetailValue, @ObjectID, @ClientPersonnelID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoField_FromControl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SimpleValueIntoField_FromControl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoField_FromControl] TO [sp_executeall]
GO
