SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-11-03
-- Description:	Inserts a copy of an existing query column and sets the alias to be the column name (or alias if it has one) + the 
--              text 'Duplicate'
--
-- History    : 2011-11-03 Jan Wilson
--              Added check to ensure that the length of the alias text does not exceed the capacity of the column
--              2013-05-10 Jan Wilson
--              Added extra column to include in duplication
-- =============================================
CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__DuplicateEditingColumn]
(
	@SqlQueryEditingID INT,
	@SqlQueryEditingColumnID INT
)
AS
BEGIN
	INSERT INTO [dbo].[SqlQueryEditingColumns]
		(
		[ClientID]
		,[SqlQueryEditingID]
		,[SqlQueryEditingTableID]
		,[CompleteOutputText]
		,[ColumnAlias]
		,[ShowColumn]
		,[DisplayOrder]
		,[SortOrder]
		,[SortType]
		,[IsAggregate]
		,[ColumnDataType]
		,[ManipulatedDataType]
		,[ColumnNaturalName]
		,[RealSqlQueryColumnID]
		,[ComplexValue]
		)
	SELECT
		[ClientID]
		,[SqlQueryEditingID]
		,[SqlQueryEditingTableID]
		,[CompleteOutputText]
		,SUBSTRING(CASE WHEN RTRIM(ISNULL([ColumnAlias], '')) <> '' THEN [ColumnAlias] ELSE [ColumnNaturalName] END +' Duplicate',1,50)
		,[ShowColumn]
		,[DisplayOrder]
		,null
		,null
		,[IsAggregate]
		,[ColumnDataType]
		,[ManipulatedDataType]
		,[ColumnNaturalName]
		,[RealSqlQueryColumnID]
		,[ComplexValue]
	FROM 
		[dbo].[SqlQueryEditingColumns] WITH (NOLOCK)
	WHERE
		[SqlQueryEditingID] = @SqlQueryEditingID
	AND
		[SqlQueryEditingColumnID] = @SqlQueryEditingColumnID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__DuplicateEditingColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__DuplicateEditingColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__DuplicateEditingColumn] TO [sp_executeall]
GO
