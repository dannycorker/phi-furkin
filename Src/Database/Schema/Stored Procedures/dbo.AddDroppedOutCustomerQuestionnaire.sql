SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddDroppedOutCustomerQuestionnaire]
@CustomerID int,
@ClientQuestionnaireID int,
@TrackingID int,
@ClientID int

as

insert into DroppedOutCustomerQuestionnaires (CustomerID, ClientQuestionnaireID, TrackingID, SubmissionDate, ClientID)
values (@CustomerID, @ClientQuestionnaireID,@TrackingID, dbo.fn_GetDate_Local(), @ClientID )


declare @CustomerQuestionnaireID int

set @CustomerQuestionnaireID = SCOPE_IDENTITY()

select CustomerQuestionnaireID
from DroppedOutCustomerQuestionnaires
where CustomerQuestionnaireID = @CustomerQuestionnaireID
GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomerQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddDroppedOutCustomerQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomerQuestionnaire] TO [sp_executeall]
GO
