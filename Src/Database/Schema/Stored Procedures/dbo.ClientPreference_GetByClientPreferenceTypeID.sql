SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPreference table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreference_GetByClientPreferenceTypeID]
(

	@ClientPreferenceTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPreferenceID],
					[ClientID],
					[ClientPreferenceTypeID],
					[PreferenceValue]
				FROM
					[dbo].[ClientPreference] WITH (NOLOCK) 
				WHERE
					[ClientPreferenceTypeID] = @ClientPreferenceTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_GetByClientPreferenceTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreference_GetByClientPreferenceTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_GetByClientPreferenceTypeID] TO [sp_executeall]
GO
