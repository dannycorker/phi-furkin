SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-19
-- Description:	Get the rights that this User has to this LeadType
-- =============================================
CREATE PROCEDURE [dbo].[GetUserLeadTypeAccess]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@LeadTypeID int
AS
BEGIN

	DECLARE @Rights int

    -- Insert statements for procedure here
	SELECT @Rights = f.rightid 
	FROM dbo.fnLeadTypeSecure(@UserID) f 
	WHERE f.LeadTypeID = @LeadTypeID 

	IF @@ROWCOUNT < 1
	BEGIN
		SET @Rights = 0
	END

	SELECT @Rights

END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserLeadTypeAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserLeadTypeAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserLeadTypeAccess] TO [sp_executeall]
GO
