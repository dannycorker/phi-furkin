SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Location_Update]
(

	@LocationID int   ,

	@ClientID int   ,

	@LocationName varchar (255)  ,

	@Note varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Location]
				SET
					[ClientID] = @ClientID
					,[LocationName] = @LocationName
					,[Note] = @Note
					,[Enabled] = @Enabled
					,[Deleted] = @Deleted
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[LocationID] = @LocationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Location_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Update] TO [sp_executeall]
GO
