SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClassNode table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_GetByWhoModified]
(

	@WhoModified int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClassNodeID],
					[ClientID],
					[ParentClassNodeID],
					[NodeType],
					[Name],
					[ClassDescription],
					[NodeOrder],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ClassNode] WITH (NOLOCK) 
				WHERE
					[WhoModified] = @WhoModified
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_GetByWhoModified] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_GetByWhoModified] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_GetByWhoModified] TO [sp_executeall]
GO
