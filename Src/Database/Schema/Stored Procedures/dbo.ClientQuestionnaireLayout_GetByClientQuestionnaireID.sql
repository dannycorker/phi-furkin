SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientQuestionnaireLayout table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaireLayout_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientQuestionnaireLayoutID],
					[ClientQuestionnaireID],
					[ClientID],
					[PageNumber],
					[LayoutCss],
					[LayoutCssFileName],
					[QuestionColumns]
				FROM
					[dbo].[ClientQuestionnaireLayout] WITH (NOLOCK) 
				WHERE
					[ClientQuestionnaireID] = @ClientQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaireLayout_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_GetByClientQuestionnaireID] TO [sp_executeall]
GO
