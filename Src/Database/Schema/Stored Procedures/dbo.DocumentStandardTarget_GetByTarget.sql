SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentStandardTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_GetByTarget]
(

	@Target varchar (250)  
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[DocumentStandardTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[ObjectName],
					[PropertyName]
				FROM
					[dbo].[DocumentStandardTarget]
				WHERE
					[Target] = @Target
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByTarget] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_GetByTarget] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByTarget] TO [sp_executeall]
GO
