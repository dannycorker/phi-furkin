SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Outcomes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Outcomes_Update]
(

	@OutcomeID int   ,

	@ClientQuestionnaireID int   ,

	@OutcomeName varchar (50)  ,

	@OutcomeDescription varchar (MAX)  ,

	@SmsWhenFound bit   ,

	@MobileNumber varchar (50)  ,

	@EmailCustomer bit   ,

	@CustomersEmail varchar (MAX)  ,

	@ThankYouPage varchar (MAX)  ,

	@EmailFromAddress varchar (255)  ,

	@SmsToCustomer varchar (160)  ,

	@CustomersEmailSubject varchar (500)  ,

	@ClientID int   ,

	@EmailClient bit   ,

	@ClientEmail varchar (255)  ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Outcomes]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[OutcomeName] = @OutcomeName
					,[OutcomeDescription] = @OutcomeDescription
					,[SmsWhenFound] = @SmsWhenFound
					,[mobileNumber] = @MobileNumber
					,[EmailCustomer] = @EmailCustomer
					,[CustomersEmail] = @CustomersEmail
					,[ThankYouPage] = @ThankYouPage
					,[EmailFromAddress] = @EmailFromAddress
					,[SmsToCustomer] = @SmsToCustomer
					,[CustomersEmailSubject] = @CustomersEmailSubject
					,[ClientID] = @ClientID
					,[EmailClient] = @EmailClient
					,[ClientEmail] = @ClientEmail
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[OutcomeID] = @OutcomeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Outcomes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Update] TO [sp_executeall]
GO
