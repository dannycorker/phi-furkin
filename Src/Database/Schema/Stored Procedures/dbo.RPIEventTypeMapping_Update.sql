SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIEventTypeMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventTypeMapping_Update]
(

	@RPIEventTypeMappingID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@RPIEventTypeID int   ,

	@EventTypeID int   ,

	@IsEnabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIEventTypeMapping]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[RPIEventTypeID] = @RPIEventTypeID
					,[EventTypeID] = @EventTypeID
					,[IsEnabled] = @IsEnabled
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[RPIEventTypeMappingID] = @RPIEventTypeMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventTypeMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Update] TO [sp_executeall]
GO
