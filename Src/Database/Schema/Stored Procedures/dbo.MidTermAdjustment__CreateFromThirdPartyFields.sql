SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-07-2016
-- Description:	Performs a mid term adjustment
-- Modified By PR 22/08/2016 :-
-- Added default lookup for number of installments if its not filled in on the purchased product payment schedule
-- Added 	4404	RuleSetID	
--			4406	CostBreakDown
--			4411	ObjectTypeID
--			4412	ObjectID
-- 2016-08-23 DCM Set @AccountID for method 3 additional payment (back-dated MTA)
-- 2016-08-23 DCM Update CustomerPaymentScheduleID in PurchasedProductPaymentSchedule when method 3 has completed processing
-- 2016-08-24 DCM Added @AdjustmentDate to the argument list for MidTermAdjustment__OneOffPayment
-- 2016-08-25 DCM Changed position of one off payment insert for method 3 to prevent extra row being added
-- 2016-08-26 DCM Added PurchasedProduct backup and update
-- 2016-08-28 DCM Added PremiumCalculationDetailID as a third party field
-- 2016-08-31  PR Added Account__SetDateAndAmountOfNextPayment @AccountID	
-- 2016-09-07 DCM For method 3, adjustment in past update one off payment difference only if payment up front and first payment or not payment upfornt and last payment
-- 2016-09-07 DCM Don't include payment adjustment for period of including adjustment day with total adjustment for whole months already paid 
-- 2016-09-07 DCM Enhanced Method 3 to refund/collect difference in month of payment where payment is in future 
-- 2016-09-12 DCM Changed @WhenCreated columns to dbo.fn_GetDate_Local()
-- 2016-09-16 DCM Added setting PurchasedProductPaymentScheduleParentID & PurchasedProductPaymentScheduleTypeID
-- 2016-09-24 DCM Get regular and one-off payment wait from Billing Configuration
-- 2016-11-15 DCM Added adjustmentvalueonly i.e. don't add the value to the schedules just calculate it & place in defined DF
-- 2016-11-16 DCM Updated oneoffpaymentwait & regularpaymentwait to check DV = ''
-- 2016-11-23 CS  Limited insert into MidTermAdjustment.Description to 250 characters
-- 2016-11-24 DCM Update of PPPS where adjustment date in future or in unpaid month
-- 2016-11-25 CS  Set some LeadEvent comments
-- 2016-12-01 DCM Demoted method 3 to new method 5 & added new method 3 where adjustment value based on proportion of days remaining policy year
-- 2016-12-13 DCM modified one of payment calculation to give correct sign
-- 2017-07-19 CPS allow for Admin Fees with a different schedule type and no cover period
-- 2017-07-26 CPS look at the correct fields in PremiumDetailHistory
-- 2017-08-18 CPS include PurchasedProductPaymentScheduleTypeID 6 ("MTA") when looking at the existing schedule
-- 2017-08-22 CPS Set uncollected MTA adjustments to "ignored
-- 2017-09-25 CPS update admin fee
-- 2017-11-01 CPS call PurchasedProductHistory__Backup with @LeadEventID
-- 2018-03-19 JEL Added consideration for ClientAccount Update
-- 2018-04-17 JEL stopped rounded remainder payment ALWAYS being added to MTA new schedule even if not premium change
-- 2019-12-09 CPS for JIRA LPC-181 | We now use a WorldPay control so don't create the Card Transaction here
-- 2020-01-22 CPS for JIRA LPC-394 | Update Written Premium following the MTA completion as we need to know the total payable for the year first
-- 2020-04-14 GPR for JIRA AAG-634 | Altered assignment of @PremiumCalculationDetailID_DV, take the PremiumCalculationID from the WrittenPremium table as the output from ThirdPartyField is NULL
-- 2020-08-26 ACE PPET-73 Handle Transaction Fee 
-- 2020-12-04 CPS for JIRA SDPRU-182 | Use @RemainderPaymentVat instead of @RemainderPayment on the tax side of the equation when calculating adjusted net
-- 2021-01-20 ACE FURKIN-130 - Add Enrollment Fee
-- =============================================
CREATE PROCEDURE [dbo].[MidTermAdjustment__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT
AS
BEGIN

--declare
--	@ClientID INT = 384,
--	@CaseID INT = 6771,
--	@LeadEventID INT = 68834

	SET NOCOUNT ON;

	DECLARE @NewAccountID INT,
			@CustomerPaymentScheduleID INT,
			@ClientGatewayID INT,
			@StateCode VARCHAR(50),
			@TransactionFee NUMERIC(18,2),
			@AccountTypeID INT,
			@PaymentTypeLuli INT,
			@PaymentMethod INT,
			@ValidFrom DATE,
			@FirstPPPS INT /*GPR 2020-11-26*/
	
	DECLARE @LeadEventComments VARCHAR(2000) = ''
	SELECT @LeadEventComments += CHAR(13)+CHAR(10) + 'Adjustment Prepared' + CHAR(13)+CHAR(10)

	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @WhoCreated INT

	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID, @LeadID = m.LeadID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID = @CaseID
	
	SELECT @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID 
	FROM Lead l WITH (NOLOCK)
	WHERE l.LeadID = @LeadID

	/*GPR 2020-10-05 removed and replaced with later block for SDPRU-71*/
	/*ALM 2021-03-05 Joined to UnitedStates for FURKIN-342*/
	SELECT @StateCode = canada.StateCode
	FROM Customers c WITH (NOLOCK)
	JOIN UnitedStates canada WITH (NOLOCK) ON canada.StateName = c.County
	WHERE c.CustomerID = @CustomerID

	/*GPR 2020-10-05 for SDPRU-71*/
	/*ACE 2021-01-28 Commented out and back in*/
	--SELECT @StateCode = usa.StateCode
	--FROM Customers c WITH (NOLOCK)
	--INNER JOIN StateByZip z WITH (NOLOCK) ON z.Zip = c.Postcode
	--INNER JOIN UnitedStates usa WITH (NOLOCK) ON usa.StateID = z.StateID
	--WHERE c.CustomerID = @CustomerID
	
	SELECT @WhoCreated = le.WhoCreated 
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID

	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
	
	--4397	Description
	DECLARE @Description_DV VARCHAR(2000)
	SELECT @Description_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4397)		

	--4399	PremiumAmount
	DECLARE @PremiumAmount_DV VARCHAR(2000), @PremiumAmount NUMERIC(18,2)
	SELECT @PremiumAmount_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4399)		

	DECLARE @LogEntry VARCHAR(MAX),@MethodName VARCHAR(200)			

	SELECT	 @LogEntry = ISNULL(CONVERT(VARCHAR,@CaseID),'NULL')
			,@MethodName = '@CaseID'
	EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields',@MethodName,@LogEntry,@WhoCreated	
	
	BEGIN TRY  
		SELECT @PremiumAmount = CAST(@PremiumAmount_DV AS NUMERIC(18,2))
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Premium Amount'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'MidTermAdjustment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 
	
	SELECT @LeadEventComments += ' Premium Amount: ' + ISNULL(CONVERT(VARCHAR,@PremiumAmount),'NULL') + CHAR(13)+CHAR(10)
	
	--4414	PremiumNet
	DECLARE @PremiumNet_DV VARCHAR(2000), @PremiumNet NUMERIC(18,2)
	SELECT @PremiumNet_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4414)		
	
	BEGIN TRY  
		SELECT @PremiumNet = CAST(@PremiumNet_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Premium Net'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'MidTermAdjustment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 
	
	--4415	PremiumVAT
	DECLARE @PremiumVAT_DV VARCHAR(2000), @PremiumVAT NUMERIC(18,2)
	SELECT @PremiumVAT_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4415)		
	
	BEGIN TRY  
		SELECT @PremiumVAT = CAST(@PremiumVAT_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Premium VAT'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'MidTermAdjustment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 	
	
	--4401	AdjustmentDate	
	DECLARE @AdjustmentDate_DV VARCHAR(2000), @IsDateTime_AdjustmentDate BIT, @AdjustmentDate DATETIME
	SELECT @AdjustmentDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4401)		
	SELECT @IsDateTime_AdjustmentDate = dbo.fnIsDateTime(@AdjustmentDate_DV)		
	IF(@IsDateTime_AdjustmentDate=1)
	BEGIN
		SELECT @AdjustmentDate = CAST(@AdjustmentDate_DV AS DATETIME)
	END

	SELECT @LeadEventComments += ' Adjustment Date: ' + ISNULL(CONVERT(VARCHAR,@AdjustmentDate,103),'NULL') + CHAR(13)+CHAR(10)
	
	--4402	NotificationDate
	DECLARE @NotificationDate_DV VARCHAR(2000), @IsDateTime_NotificationDate BIT, @NotificationDate DATETIME
	SELECT @NotificationDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4402)		
	SELECT @IsDateTime_NotificationDate = dbo.fnIsDateTime(@AdjustmentDate_DV)		
	IF(@IsDateTime_NotificationDate=1)
	BEGIN
		SELECT @NotificationDate = CAST(@NotificationDate_DV AS DATETIME)
	END
	
	--4404	RuleSetID
	DECLARE @RuleSetID_DV VARCHAR(2000), @IsInt_RuleSetID BIT, @RuleSetID INT
	SELECT @RuleSetID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4404)	
	SELECT @IsInt_RuleSetID = dbo.fnIsInt(@RuleSetID_DV)		
	IF(@IsInt_RuleSetID=1)
	BEGIN
		SELECT @RuleSetID = CAST(@RuleSetID_DV AS INT)	
	END
	
	SELECT @LeadEventComments += ' RuleSetID: ' + ISNULL(CONVERT(VARCHAR,@RuleSetID),'NULL') + CHAR(13)+CHAR(10)
	
	--4406	CostBreakDown
	DECLARE @CostBreakDown_DV VARCHAR(2000), @CostBreakDownXML XML
	BEGIN TRY 
		SELECT @CostBreakDown_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4406)
		SET @CostBreakDownXML = CAST(@CostBreakDown_DV AS XML)
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Cost Breakdown'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'MidTermAdjustment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 	

	--4507 PremiumCalculationDetailID
	DECLARE @PremiumCalculationDetailID_DV VARCHAR(2000), @IsInt_PremiumCalculationDetailID BIT, @PremiumCalculationDetailID INT
	--SELECT @PremiumCalculationDetailID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4507)	
	
	/*GPR 2020-04-14 - Get the PremiumCalculationID from the WrittenPremium table as the output from ThirdPartyField is NULL*/
	SELECT @PremiumCalculationDetailID_DV = (SELECT TOP(1) wp.PremiumCalculationID
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.MatterID = @MatterID
	ORDER BY 1 DESC)

	SELECT @IsInt_PremiumCalculationDetailID = dbo.fnIsInt(@PremiumCalculationDetailID_DV)		
	IF(@IsInt_PremiumCalculationDetailID=1)
	BEGIN
		SELECT @PremiumCalculationDetailID = CAST(@PremiumCalculationDetailID_DV AS INT)	
	END
	
	SELECT @LeadEventComments += ' PremiumCalculationDetailID: ' + ISNULL(CONVERT(VARCHAR,@PremiumCalculationDetailID),'NULL') + CHAR(13)+CHAR(10)
	
	--4408	RegularPaymentWait
	DECLARE @RegularPaymentWait_DV VARCHAR(2000), @IsInt_RegularPaymentWait BIT, @RegularPaymentWait INT
	SELECT @RegularPaymentWait_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4408)	
	SELECT @IsInt_RegularPaymentWait = dbo.fnIsInt(@RegularPaymentWait_DV)		
	IF(@IsInt_RegularPaymentWait=1)
	BEGIN
		SELECT @RegularPaymentWait = CAST(@RegularPaymentWait_DV AS INT)	
	END
	IF ISNULL(@RegularPaymentWait_DV,'') = ''
	BEGIN
		SELECT @RegularPaymentWait=bf.RegularPaymentWait 
		FROM BillingConfiguration bf WITH (NOLOCK) 
		WHERE bf.ClientID=@ClientID
	END
	
	--4409	OneOffAdjustmentWait
	DECLARE @OneOffAdjustmentWait_DV VARCHAR(2000), @IsInt_OneOffAdjustmentWait BIT, @OneOffAdjustmentWait INT
	SELECT @OneOffAdjustmentWait_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4409)	

	SELECT @IsInt_OneOffAdjustmentWait = dbo.fnIsInt(@OneOffAdjustmentWait_DV)		
	IF(@IsInt_RegularPaymentWait=1)
	BEGIN
		SELECT @OneOffAdjustmentWait = CAST(@OneOffAdjustmentWait_DV AS INT)	
	END	
	IF ISNULL(@OneOffAdjustmentWait_DV,'') = ''
	BEGIN
		SELECT @OneOffAdjustmentWait=bf.OneOffAdjustmentWait 
		FROM BillingConfiguration bf WITH (NOLOCK) 
		WHERE bf.ClientID=@ClientID
	END

	--4410	MidTermAdjustmentCalculationMethodID
	DECLARE @MidTermAdjustmentCalculationMethodID_DV VARCHAR(2000), @IsInt_MidTermAdjustmentCalculationMethodID BIT, @MidTermAdjustmentCalculationMethodID INT, @ItemID_MidTermAdjustmentCalculationMethodID INT, @MidTermAdjustmentCalculationMethodID_ItemValue VARCHAR(500)
	SELECT @MidTermAdjustmentCalculationMethodID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4410)	
	SELECT @IsInt_MidTermAdjustmentCalculationMethodID = dbo.fnIsInt(@MidTermAdjustmentCalculationMethodID_DV)		
	IF(@IsInt_MidTermAdjustmentCalculationMethodID=1)
	BEGIN
		SELECT @ItemID_MidTermAdjustmentCalculationMethodID = CAST(@MidTermAdjustmentCalculationMethodID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual mid term adjustment calculation method
		SELECT @MidTermAdjustmentCalculationMethodID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_MidTermAdjustmentCalculationMethodID
		SELECT @MidTermAdjustmentCalculationMethodID = mtam.MidTermAdjustmentCalculationMethodID FROM MidTermAdjustmentCalculationMethod mtam WITH (NOLOCK) WHERE mtam.Method = @MidTermAdjustmentCalculationMethodID_ItemValue
	END		
	
	SELECT @LeadEventComments += ' MidTermAdjustmentMethodID: ' + ISNULL(CONVERT(VARCHAR,@MidTermAdjustmentCalculationMethodID),'NULL') + CHAR(13)+CHAR(10)
	
	--4411	ObjectTypeID
	DECLARE @ObjectTypeID_DV VARCHAR(2000), @IsInt_ObjectTypeID BIT, @ObjectTypeID INT
	SELECT @ObjectTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4411)	
	SELECT @IsInt_ObjectTypeID = dbo.fnIsInt(@ObjectTypeID_DV)		
	IF(@IsInt_ObjectTypeID=1)
	BEGIN
		SELECT @ObjectTypeID = CAST(@ObjectTypeID_DV AS INT)	
	END	
	
	--4412	ObjectID	
	DECLARE @ObjectID_DV VARCHAR(2000), @IsInt_ObjectID BIT, @ObjectID INT
	SELECT @ObjectID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4412)	
	SELECT @IsInt_ObjectID = dbo.fnIsInt(@ObjectID_DV)		
	IF(@IsInt_ObjectID=1)
	BEGIN
		SELECT @ObjectID = CAST(@ObjectID_DV AS INT)	
	END			
	
	--4413	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN
		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
		SELECT @AccountID=AccountID 
		FROM PurchasedProduct WITH (NOLOCK) 
		WHERE PurchasedProductID=@PurchasedProductID
	END	
	
	DECLARE @ClientAccountID INT, @NumberOfInstallments INT

	SELECT @ClientAccountID = p.ClientAccountID, @NumberOfInstallments = NumberOfInstallments, @ValidFrom = p.ValidFrom 
	FROM PurchasedProduct p WITH (NOLOCK) 
	WHERE p.PurchasedProductID = @PurchasedProductID
	
	/*GPR 2020-11-26*/
	SELECT @PaymentMethod = CASE @NumberOfInstallments
										WHEN 1 THEN 69944 /*Annually*/
										WHEN 26 THEN 293321 /*Fortnightly*/
										ELSE 69943 /*Monthly*/
									END


	IF @PaymentMethod = 69943 /*Monthly*/
	BEGIN
		SELECT TOP 1 @FirstPPPS = PurchasedProductPaymentScheduleID
		FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
		WHERE PurchasedProductID = @PurchasedProductID
		ORDER BY ActualCollectionDate ASC
	END
	ELSE
	BEGIN
		SELECT @FirstPPPS = 0
	END

	SELECT @LeadEventComments += ' PurchasedProductID: ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductID),'NULL') + CHAR(13)+CHAR(10)
	
	--4530	AdjustmentValueOnly (0=No update billing records; 1=Yes just record adjustment value in defined DF)
	DECLARE @AdjustmentValueOnly_DV VARCHAR(2000), @IsInt_AdjustmentValueOnly BIT, @AdjustmentValueOnly INT
	SELECT @AdjustmentValueOnly_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4530)	
	SELECT @IsInt_AdjustmentValueOnly = dbo.fnIsInt(@AdjustmentValueOnly_DV)		
	IF(@IsInt_AdjustmentValueOnly=1)
	BEGIN
		SELECT @AdjustmentValueOnly = CAST(@AdjustmentValueOnly_DV AS INT)
	END
	SELECT @AdjustmentValueOnly=CASE WHEN @AdjustmentValueOnly IS NULL THEN 0 ELSE
									CASE WHEN @AdjustmentValueOnly NOT IN (1,0) THEN 0
										ELSE @AdjustmentValueOnly 
									END
								END		
	
	SELECT @LogEntry = 'AdjustmentValueOnly: ' + CAST(@AdjustmentValueOnly as VARCHAR(10))
	EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,@WhoCreated
	
	SELECT @LeadEventComments += ' Adjustment Value Only: ' + ISNULL(CONVERT(VARCHAR,@AdjustmentValueOnly),'NULL') + CHAR(13)+CHAR(10)

	--4531	AdjustmentValueDFID
	DECLARE @AdjustmentValueDFID INT
	SELECT @AdjustmentValueDFID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 105, 4531)

	--4533	TermAdjustmentValueDFID
	DECLARE @TermAdjustmentValueDFID INT
	SELECT @TermAdjustmentValueDFID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 105, 4533)

	--4534	FirstMonthTotalDFID
	DECLARE @FirstMonthTotalDFID INT
	SELECT @FirstMonthTotalDFID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 105, 4534)
	
	DECLARE @RemainderUpFront BIT, @PaymentFrequencyID INT
	SELECT @NumberOfInstallments=NumberOfInstallments, @RemainderUpFront=RemainderUpFront, @PaymentFrequencyID=PaymentFrequencyID
	FROM PurchasedProduct WITH (NOLOCK) 
	WHERE PurchasedProductID=@PurchasedProductID 
	
	IF (@NumberOfInstallments IS NULL) -- if number of installments is null use the default as specified by the PaymentFrequency table
	BEGIN
		SELECT @NumberOfInstallments=DefaultInstallmentLength 
		FROM PaymentFrequency WITH (NOLOCK) 
		WHERE PaymentFrequencyID = @PaymentFrequencyID
	END
	
	DECLARE @MinimumAdjustmentValue MONEY
	SELECT @MinimumAdjustmentValue = bf.MinimumAdjustmentValue 
	FROM BillingConfiguration bf WITH (NOLOCK) 
	WHERE bf.ClientID = @ClientID


	DECLARE @Payment NUMERIC(36,18),
			@RoundedPayment NUMERIC(18,2),
			@RoundedCost NUMERIC(18,2),
			@RemainderPayment NUMERIC(18,2),
			@PaymentVAT NUMERIC(36,18),
			@RoundedPaymentVAT NUMERIC(18,2),
			@RoundedCostVAT NUMERIC(18,2),
			@RemainderPaymentVAT NUMERIC(18,2),			
			@PurchasedProductPaymentScheduleID INT,
			@FirstPurchasedProductPaymentScheduleID INT,
			@FirstPaymentDate DATETIME,			
			@NewPaymentDate DATETIME

	DECLARE @DifferenceInMonthlyPremium NUMERIC(18,2),
			@DifferenceInMonthlyPremiumVAT NUMERIC(18,2),
			@CoverFrom DATETIME,
			@CoverTo DATETIME,
			@WithinAdjustmentDatePurchasedProductPaymentScheduleID INT, 
			@WithinAdjustmentDatePaymentGross NUMERIC(18,2),
			@WithinAdjustmentDatePaymentVAT NUMERIC(18,2),
			@NumberOfDaysInPeriod INT,
			@NumberOfDaysLeftInPeriod INT,
			@OneOffPaymentForAdjustmentPeriod NUMERIC(18,2),
			@OneOffPaymentForAdjustmentPeriodVAT NUMERIC(18,2),
			@PaymentForRemainderOfSchedule NUMERIC(18,2),
			@PaymentNetForRemainderOfSchedule NUMERIC(18,2),
			@PaymentVATForRemainderOfSchedule NUMERIC(18,2),
			@PaymentStatusID INT,
			@OneOffPaymentGross NUMERIC(18,2),
			@OneOffPaymentNet NUMERIC(18,2),
			@OneOffPaymentVAT NUMERIC(18,2),
			@NewCoverTo DATETIME,
			@NewCoverFrom DATETIME,
			@TotalAdminFee	NUMERIC(18,2)
						

	DECLARE @Schedule TABLE
	(
		ID INT,
		PaymentDate DATETIME,
		PaymentNet NUMERIC(18,2),
		PaymentVAT NUMERIC(18,2),
		PaymentGross NUMERIC(18,2)
	)
	
	DECLARE @NewSchedule TABLE
	(
		ID INT,	
		CoverFrom DATETIME, 
		CoverTo DATETIME, 
		AccountID INT, 
		PaymentNet NUMERIC(18,2),
		PaymentVAT NUMERIC(18,2),
		PaymentGross NUMERIC(18,2),
		PaymentDate DATE		
	)
	
	DECLARE @NewScheduleNotReconciled TABLE
	(
		ID INT,	
		CoverFrom DATETIME, 
		CoverTo DATETIME, 
		AccountID INT, 
		PaymentNet NUMERIC(18,2),
		PaymentVAT NUMERIC(18,2),
		PaymentGross NUMERIC(18,2)		
	)
	
	/*ALM 2020-08-05 uncommented code SDAAG-84*/
	/*GPR 2020-05-27*/
	--SELECT @PremiumVAT = (SELECT TOP(1) wp.AnnualPriceForRiskLocalTax + wp.AnnualPriceForRiskNationalTax FROM WrittenPremium wp WITH (NOLOCK)
	--WHERE wp.MatterID = @MatterID
	--ORDER BY 1 DESC)

	--/*GPR 2020-06-15 SDAAG-60*/
	--SELECT @PremiumNet = (SELECT TOP(1) wp.AnnualPriceForRiskNET FROM WrittenPremium wp WITH (NOLOCK)
	--WHERE wp.MatterID = @MatterID
	--ORDER BY 1 DESC)

	--/*GPR 2020-06-15 SDAAG-60*/
	--SELECT @PremiumAmount = (SELECT TOP(1) wp.AnnualPriceForRiskGross FROM WrittenPremium wp WITH (NOLOCK)
	--WHERE wp.MatterID = @MatterID
	--ORDER BY 1 DESC)

	DECLARE @MidTermAdjustmentID INT						
	IF @AdjustmentValueOnly=0
	BEGIN

		--create mid term adjustment record
		INSERT INTO MidTermAdjustment (ClientID, CustomerID, PurchasedProductID, Description, PremiumNet, PremiumVAT, PremiumAmount, AdjustmentDate, NotificationDate, RuleSetID, CostBreakDown, RegularPaymentWait, OneOffAdjustmentWait, MidTermAdjustmentCalculationMethodID, ObjectTypeID, ObjectID, WhenCreated, WhoCreated)
		VALUES (@ClientID, @CustomerID, @PurchasedProductID, LEFT(@Description_DV,250), @PremiumNet, @PremiumVAT, @PremiumAmount, @AdjustmentDate, @NotificationDate, @RuleSetID, @CostBreakDownXML, @RegularPaymentWait, @OneOffAdjustmentWait, @MidTermAdjustmentCalculationMethodID, @ObjectTypeID, @ObjectID, @WhenCreated, @WhoCreated)
				
		SELECT @MidTermAdjustmentID = SCOPE_IDENTITY()	
			
		--Backup both schedules
		EXEC PurchasedProductPaymentScheduleHistory__Backup @PurchasedProductID, @WhoCreated, 0
		EXEC CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 0
		
		SELECT @LogEntry = CAST(@PurchasedProductID as VARCHAR(10))
		EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','@PurchasedProductID',@LogEntry,44412
		
		SELECT @LogEntry = CAST(@PremiumAmount as VARCHAR(10))
		EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','@PremiumAmount',@LogEntry,44412		
		
		SELECT @LogEntry = CAST(@PremiumNet as VARCHAR(10))
		EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','@PremiumNet',@LogEntry,44412

		SELECT @LogEntry = CAST(@PremiumVAT as VARCHAR(10))
		EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','@PremiumVat',@LogEntry,44412
		
		SELECT @TotalAdminFee = SUM(pps.PaymentGross)
        FROM dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
        WHERE pps.PurchasedProductID = @PurchasedProductID
        AND pps.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
        AND pps.PaymentStatusID <> 3 /*Ignored*/
		
		SELECT @TotalAdminFee = ISNULL(@TotalAdminFee,0.00)
	
		SELECT @AccountTypeID = a.AccountTypeID
		FROM Account a WITH (NOLOCK) 
		WHERE a.AccountID = @AccountID

		SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
				THEN 69930 /*Direct Debit*/
				ELSE 69931 /*Credit Card*/
				END

		SELECT @PaymentMethod = 69943 /*Monthly*/

		SELECT @TransactionFee = ISNULL(dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod), 0.00)

		-- Backup purchased product and update the existing record
		EXEC PurchasedProductHistory__Backup @PurchasedProductID, @WhoCreated, @LeadEventID

		UPDATE PurchasedProduct
		SET ProductCostGross=@PremiumAmount, --+ ISNULL(@TotalAdminFee,0), /*GPR 2021-02-09*/
			ProductCostNet=@PremiumNet, --+ @TotalAdminFee,
			ProductCostVAT=@PremiumVAT,
			PremiumCalculationDetailID=@PremiumCalculationDetailID
		WHERE PurchasedProductID=@PurchasedProductID

	END
		
	--Calculate  payments
	SELECT @NumberOfInstallments = COUNT (*) 
	FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
	WHERE pps.PurchasedProductID = @PurchasedProductID 
	AND pps.PurchasedProductPaymentScheduleTypeID = 1 

	SELECT @Payment = ( @PremiumAmount / @NumberOfInstallments )
	SELECT @RoundedPayment = ROUND(@Payment, 2)
	SELECT @RoundedCost = @RoundedPayment * @NumberOfInstallments
	SELECT @RemainderPayment = @PremiumAmount - @RoundedCost
	
	SELECT @PaymentVAT = (@PremiumVAT / @NumberOfInstallments)
	SELECT @RoundedPaymentVAT = ROUND(@PaymentVAT, 2)
	SELECT @RoundedCostVAT = @RoundedPaymentVAT * @NumberOfInstallments
	SELECT @RemainderPaymentVAT = @PremiumVAT - @RoundedCostVAT
		
	IF (dbo.fnIsDateTime(@NotificationDate_DV)=0) -- notification date not filled in so use the lead event when created date
	BEGIN

		SELECT @NotificationDate = WhenCreated 
		FROM LeadEvent WITH (NOLOCK) 
		WHERE LeadEventID = @LeadEventID

	END

	SELECT @NewPaymentDate=DATEADD(DAY, @RegularPaymentWait, @NotificationDate)	
	
	IF (dbo.fnIsDateTime(@AdjustmentDate_DV)=1) 
	BEGIN
		IF(@AdjustmentDate>@NewPaymentDate) -- if the adjustment date is greater than the calculated new payment date then use the adjustment date
		BEGIN
			SET @NewPaymentDate = @AdjustmentDate
		END
	END
	
	SELECT @LeadEventComments += ' Rounded Payment: '   + ISNULL(CONVERT(VARCHAR,@RoundedPayment  ),'NULL') + CHAR(13)+CHAR(10)			
	SELECT @LeadEventComments += ' Rounded Payment Tax: '   + ISNULL(CONVERT(VARCHAR,@RoundedPaymentVAT  ),'NULL') + CHAR(13)+CHAR(10)			
	SELECT @LeadEventComments += ' Remainder Payment: ' + ISNULL(CONVERT(VARCHAR,@RemainderPayment),'NULL') + CHAR(13)+CHAR(10)			
	SELECT @LeadEventComments += ' Remainder Up Front: ' + ISNULL(CONVERT(VARCHAR,@RemainderUpFront),'NULL') + CHAR(13)+CHAR(10)			
	
	IF(@NumberOfInstallments = 1) -- one off payment
	BEGIN
		EXEC MidTermAdjustment__OneOffPayment @MidTermAdjustmentID, 
											  @ClientID, 
											  @CaseID,	
											  @LeadEventID, 
											  @RoundedCost,	
											  @RoundedCostVAT, 
											  @NewPaymentDate,
											  @AdjustmentDate, 
											  @PurchasedProductID,
											  @AdjustmentValueOnly,
											  @AdjustmentValueDFID,
											  @OneOffAdjustmentWait,
											  @MinimumAdjustmentValue,
											  @TermAdjustmentValueDFID

	END
	ELSE --- Multiple installments
	BEGIN
	
		-- Adjust premium on all uncollected payments to the new premium, i.e. ignore adjustment on any payments already taken
		-- Adjust payment date on the next regular payment to be after the RegularPaymentWait
		-- First Payment in new set taken on 
		-- (LeadEvent.WhenCreated + (RegularPaymentWait in days) || (@NotificationDate + (RegularPaymentWait in days)) || (@AdjustmentDate))
		-- Which ever date is the greatest
		IF(@MidTermAdjustmentCalculationMethodID=1) --Look forward only
		BEGIN		
			
			-- Gets the first payment schedule and the first payment date
			SELECT TOP 1 @FirstPurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID, @FirstPaymentDate = PaymentDate
			FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
			WHERE PaymentStatusID=1 
			AND PurchasedProductID=@PurchasedProductID 
			AND ReconciledDate IS NULL		
			ORDER BY PaymentDate ASC
			
			--Chooses the FirstPayment date
			IF(@NewPaymentDate>@FirstPaymentDate)
			BEGIN
				SELECT @FirstPaymentDate = @NewPaymentDate
			END
			
			IF @RemainderUpFront=1 -- take the remainder up front
			BEGIN
				SELECT TOP 1 @PurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID
				FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
				WHERE PaymentStatusID=1 
				AND PurchasedProductID=@PurchasedProductID 
				AND ReconciledDate IS NULL		
				ORDER BY PaymentDate ASC
				
				/*JEL SDPRU-182*/
				UPDATE PurchasedProductPaymentSchedule			
				SET PaymentGross = (@RoundedPayment + @RemainderPayment),
					PaymentVAT = (@RoundedPaymentVAT + @RemainderPaymentVat),
					PaymentNet = ((@RoundedPayment + @RemainderPayment) - (@RoundedPaymentVAT + @RemainderPaymentVat)),
					TransactionFee = CASE PurchasedProductPaymentScheduleID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/
				WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
				AND PurchasedProductPaymentScheduleTypeID NOT IN (6, 7, 8) /*MTA Adjustment, Cancellation Adjustment, Reinstatement Adjustment*/ /*ALM/GPR 2020-11-26 for SDPRU-162 and SDPRU-165*/
			END
			ELSE -- take the remainder at the end
			BEGIN
				SELECT TOP 1 @PurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID
				FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
				WHERE PaymentStatusID=1 AND PurchasedProductID=@PurchasedProductID AND ReconciledDate IS NULL		
				ORDER BY PaymentDate DESC

				/*JEL SDPRU-182*/
				UPDATE PurchasedProductPaymentSchedule
				SET PaymentGross = (@RoundedPayment + @RemainderPayment),
					PaymentVAT = (@RoundedPaymentVAT + @RemainderPaymentVat),
					PaymentNet = ((@RoundedPayment + @RemainderPayment) - (@RoundedPaymentVAT + @RemainderPaymentVat)),
					TransactionFee = CASE PurchasedProductID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/					
				WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
				AND PurchasedProductPaymentScheduleTypeID NOT IN (6, 7, 8) /*MTA Adjustment, Cancellation Adjustment, Reinstatement Adjustment*/ /*ALM/GPR 2020-11-26 for SDPRU-162 and SDPRU-165*/
			END
			
			-- Updates the payment schedule except the payment the schedule that includes the remainder
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentGross = @RoundedPayment, PaymentVAT = @RoundedPaymentVAT, PaymentNet = (@RoundedPayment-@RoundedPaymentVAT), TransactionFee = CASE PurchasedProductPaymentScheduleID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/
			WHERE PaymentStatusID=1 
			AND PurchasedProductID=@PurchasedProductID 
			AND ReconciledDate IS NULL 
			AND PurchasedProductPaymentScheduleID<>@PurchasedProductPaymentScheduleID	
			
			-- Updates the first payment date
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentDate=@FirstPaymentDate
			WHERE PurchasedProductPaymentScheduleID=@FirstPurchasedProductPaymentScheduleID
			
			-- recalculate customer payment schedule	
			INSERT INTO @Schedule (ID,PaymentDate,PaymentNet,PaymentVAT, PaymentGross)
			SELECT pps.CustomerPaymentScheduleID AS ID, pps.PaymentDate, SUM(pps.PaymentNet) AS PaymentNet, SUM(pps.PaymentVAT) AS PaymentVAT, SUM(pps.PaymentGross) AS PaymentGross
			FROM dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK)
			WHERE (pps.CustomerID = @CustomerID) 
			AND pps.ReconciledDate IS NULL 
			GROUP BY pps.PaymentDate, pps.AccountID, pps.CustomerPaymentScheduleID
			
			UPDATE CustomerPaymentSchedule 
			SET PaymentDate=s.PaymentDate, PaymentNet=s.PaymentNet, PaymentVAT=s.PaymentVAT, PaymentGross=s.PaymentGross
			FROM @Schedule s
			WHERE s.ID = CustomerPaymentScheduleID
			
		END
		
		--Adjust premium on all uncollected payments that are due to be taken after the RegularPaymentWait to the new premium, 
		--i.e. ignore adjustment on any payments already taken or due to be taken before the RegularPaymentWait.
		IF(@MidTermAdjustmentCalculationMethodID=2) --Look forward only with wait
		BEGIN 
			
			-- Gets the first payment schedule and the first payment date greater than the wait period
			SELECT TOP 1 @FirstPurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID, @FirstPaymentDate = PaymentDate
			FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
			WHERE PaymentStatusID=1 
			AND PurchasedProductID=@PurchasedProductID 
			AND ReconciledDate IS NULL 
			AND PaymentDate>=@NewPaymentDate
			ORDER BY PaymentDate ASC
			
			IF @RemainderUpFront=1 -- take the remainder up front
			BEGIN
				SELECT TOP 1 @PurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID
				FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
				WHERE PaymentStatusID=1 
				AND PurchasedProductID=@PurchasedProductID 
				AND ReconciledDate IS NULL 
				AND PaymentDate>=@NewPaymentDate		
				ORDER BY PaymentDate ASC
				
				UPDATE PurchasedProductPaymentSchedule			
				SET PaymentGross = (@RoundedPayment + @RemainderPayment),
					PaymentVAT = (@RoundedPaymentVAT + @RemainderPaymentVat),
					PaymentNet = ((@RoundedPayment + @RemainderPayment) - (@RoundedPaymentVAT + @RemainderPaymentVat)), -- 2020-12-04 CPS for JIRA SDPRU-182 | Updated to use @RemainderPaymentVat here instead of @RemainderPayment
					TransactionFee = CASE PurchasedProductPaymentScheduleID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/
				WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID 
				AND PurchasedProductPaymentScheduleTypeID NOT IN (6, 7, 8) /*MTA Adjustment, Cancellation Adjustment, Reinstatement Adjustment*/ /*ALM/GPR 2020-11-26 for SDPRU-162 and SDPRU-165*/
			
				SELECT @LeadEventComments += CONVERT(VARCHAR,@@ROWCOUNT) + ' adjusted payments updated.' + CHAR(13)+CHAR(10)
			END
			ELSE -- take the remainder at the end
			BEGIN
				SELECT TOP 1 @PurchasedProductPaymentScheduleID = PurchasedProductPaymentScheduleID
				FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
				WHERE PaymentStatusID=1 
				AND PurchasedProductID=@PurchasedProductID 
				AND ReconciledDate IS NULL		
				ORDER BY PaymentDate DESC
				
				UPDATE PurchasedProductPaymentSchedule
				SET PaymentGross = (@RoundedPayment + @RemainderPayment),
					PaymentVAT = (@RoundedPaymentVAT + @RemainderPaymentVat),
					PaymentNet = ((@RoundedPayment + @RemainderPayment) - (@RoundedPaymentVAT + @RemainderPaymentVat)), -- 2020-12-04 CPS for JIRA SDPRU-182 | Updated to use @RemainderPaymentVat here instead of @RemainderPayment
					TransactionFee = CASE PurchasedProductID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/				
				WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
			END
			
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentGross = @RoundedPayment, PaymentVAT = @RoundedPaymentVAT, PaymentNet = (@RoundedPayment-@RoundedPaymentVAT)
			WHERE PaymentStatusID=1 
			AND PurchasedProductID=@PurchasedProductID 
			AND ReconciledDate IS NULL 
			AND PurchasedProductPaymentScheduleID<>@PurchasedProductPaymentScheduleID
			AND PaymentDate>=@NewPaymentDate
			
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentDate=@FirstPaymentDate
			WHERE PurchasedProductPaymentScheduleID=@FirstPurchasedProductPaymentScheduleID
		
			-- recalculate customer payment schedule	
			INSERT INTO @Schedule (ID,PaymentDate,PaymentNet,PaymentVAT, PaymentGross)
			SELECT pps.CustomerPaymentScheduleID AS ID, pps.PaymentDate, SUM(pps.PaymentNet) AS PaymentNet, SUM(pps.PaymentVAT) AS PaymentVAT, SUM(pps.PaymentGross) AS PaymentGross
			FROM dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK)
			WHERE (pps.CustomerID = @CustomerID) 
			AND pps.ReconciledDate IS NULL 
			AND pps.PaymentDate>=@NewPaymentDate
			GROUP BY pps.PaymentDate, pps.AccountID, pps.CustomerPaymentScheduleID
			
			UPDATE CustomerPaymentSchedule 
			SET PaymentDate=s.PaymentDate, PaymentNet=s.PaymentNet, PaymentVAT=s.PaymentVAT, PaymentGross=s.PaymentGross
			FROM @Schedule s
			WHERE s.ID = CustomerPaymentScheduleID
		
		END
		
		--For payments already taken, pro-rata the adjustment for the period since adjustment  
		--and make a single collection/refund for that paid adjustment value, 
		--and adjust premium on all uncollected payments to the new premium.
		--If the adjustment is in the future i.e. in a period that has not yet been collected, make a one-off adjustment for that first increment.				
		
		
		IF(@MidTermAdjustmentCalculationMethodID=3) --Collect/Refund in single payment
		BEGIN

			-- this is the date that they are moving house etc., and can be in the past
			IF (dbo.fnIsDateTime(@AdjustmentDate_DV)=1) 		
			BEGIN

				SELECT @NewPaymentDate = DATEADD(DAY,@OneOffAdjustmentWait,dbo.fn_GetDate_Local())
				DECLARE @NextRegularPaymentDate DATE
				DECLARE @PPPSIDs tvpInt
				
				SELECT @LeadEventComments += '  New Payment Date: ' + ISNULL(CONVERT(VARCHAR,@NewPaymentDate,103),'NULL') + CHAR(13)+CHAR(10)

				DECLARE @ValidTo DATE,
						@OriginalProductCost MONEY,
						@OriginalProductCostVAT MONEY,
						@NumberOfDaysLeft INT,
						@NumberOfDaysInTerm INT,
						@DifferenceProductCost MONEY,
						@DifferenceProductCostVAT MONEY,
						@AdjustmentTermAmount MONEY,
						@AdjustmentTermAmountVAT MONEY,
						@MonthsFutureDue MONEY,
						@MonthsFutureDueVAT MONEY,
						@MonthCoverFrom DATE,
						@MonthCoverTo DATE,
						@PaymentDate DATE,
						@ActualCollectionDate DATE,
						@DiscountedDays	INT,
						@DiscountedDaysLeft	INT,
						@LastDiscountDay	DATE
		
				IF @AdjustmentValueOnly=0
				BEGIN
					-- the previous payment amount has just been added to the history table
					SELECT TOP 1
						    @ValidFrom				= pp.ValidFrom
						   ,@ValidTo				= pp.ValidTo
						   ,@OriginalProductCost	= pp.ProductCostGross + @TotalAdminFee	/*CPS 2017-08-exclude Admin Fee*/
						   ,@OriginalProductCostVAT	= pp.ProductCostVAT
					FROM PurchasedProductHistory pp WITH (NOLOCK) 
					WHERE pp.PurchasedProductID=@PurchasedProductID
					ORDER BY PurchasedProductHistoryID DESC
				END
				ELSE
				BEGIN
					-- the previous payment amount is still in purchased product
					SELECT TOP 1
						    @ValidFrom				= pp.ValidFrom
						   ,@ValidTo				= pp.ValidTo
						   ,@OriginalProductCost	= pp.ProductCostGross + ISNULL(@TotalAdminFee,0)	/*CPS 2017-08-exclude Admin Fee*/
						   ,@OriginalProductCostVAT = pp.ProductCostVAT
					FROM PurchasedProduct pp WITH (NOLOCK) 
					WHERE pp.PurchasedProductID=@PurchasedProductID
					ORDER BY PurchasedProductID DESC
				END
				
				/*CPS 2017-08-22 Set uncollected MTA adjustments to "ignored"*/
				UPDATE pps
				SET PaymentStatusID = 3 /*Ignored*/
				FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK)
				WHERE pps.PurchasedProductID = @PurchasedProductID
				AND pps.PurchasedProductPaymentScheduleTypeID = 6 /*MTA*/
				AND pps.PaymentStatusID = 1 /*New*/
				AND pps.ActualCollectionDate >= @AdjustmentDate
				AND @AdjustmentValueOnly = 0 /*CPS 2017-11-02 for #46264*/

				--calculate the number of days left 
				SELECT @NumberOfDaysLeft = DATEDIFF(DAY,@AdjustmentDate,@ValidTo) + CASE WHEN ISNULL(bc.AdjustmentDayIsFirstDayOfNewPremium,0)=1 THEN 1 ELSE 0 END,
						@NumberOfDaysInTerm = DATEDIFF(DAY,@ValidFrom,@ValidTo) + CASE WHEN ISNULL(bc.AdjustmentDayIsFirstDayOfNewPremium,0)=1 THEN 1 ELSE 0 END
				FROM BillingConfiguration bc WITH (NOLOCK) 
				WHERE bc.ClientID=@ClientID 

				/*CPS 2017-09-19 - accomodate full-month discounts*/ 
				/*Count the number of disounted days total, and remaining, then reduce the term length accordingly*/
				SELECT	 @DiscountedDays = SUM ( DATEDIFF(DAY,pps.CoverFrom,pps.CoverTo)+1 )
						,@LastDiscountDay= MAX ( pps.CoverTo )
				FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK)
				WHERE pps.PurchasedProductPaymentScheduleTypeID = 4 /*Discount*/
				AND pps.PurchasedProductID = @PurchasedProductID
				
				SELECT @DiscountedDays = ISNULL(@DiscountedDays,0)
				
				SELECT @NumberOfDaysInTerm = @NumberOfDaysInTerm - @DiscountedDays /*Reduce the number of days in term to allow for discounts*/

				SELECT @DiscountedDaysLeft = DATEDIFF(DAY,@AdjustmentDate,@LastDiscountDay)+1 /*Count the discounted days still to come*/
				SELECT @DiscountedDaysLeft = CASE WHEN @DiscountedDaysLeft > 0 THEN @DiscountedDaysLeft ELSE 0 END
				SELECT @NumberOfDaysLeft = @NumberOfDaysLeft - @DiscountedDaysLeft /*Reduce the number of remaining days to allow for discounts*/

				SELECT @LeadEventComments += '  Number of Days Left: ' + ISNULL(CONVERT(VARCHAR,@NumberOfDaysLeft),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Number of Days in Term: ' + ISNULL(CONVERT(VARCHAR,@NumberOfDaysInTerm),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Discounted Days: ' + ISNULL(CONVERT(VARCHAR,@DiscountedDays),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Discounted Days Left: ' + ISNULL(CONVERT(VARCHAR,@DiscountedDaysLeft),'NULL') + CHAR(13)+CHAR(10)
				
				-- find remaining term adjustment amount
				SELECT @DifferenceProductCost = @PremiumAmount - @OriginalProductCost
				SELECT @DifferenceProductCostVAT = @PremiumVAT - @OriginalProductCostVAT

				/*CPS 2017-09-25 CAST as DECIMAL to prevent rounding errors*/
				SELECT @AdjustmentTermAmount	= ROUND((@DifferenceProductCost / CAST(@NumberOfDaysInTerm as DECIMAL(18,8)) * CAST(@NumberOfDaysLeft as DECIMAL(18,8))), 2)
				SELECT @AdjustmentTermAmountVAT = ROUND(@DifferenceProductCostVAT / CAST(@NumberOfDaysInTerm as DECIMAL(18,8)) * CAST(@NumberOfDaysLeft as DECIMAL(18,8)),2)

				SELECT @LeadEventComments += '  Original Product Cost: ' + ISNULL(CONVERT(VARCHAR,@OriginalProductCost),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Product Cost Difference: ' + ISNULL(CONVERT(VARCHAR,@DifferenceProductCost),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Adjustment Term Amount: ' + ISNULL(CONVERT(VARCHAR,@AdjustmentTermAmount),'NULL') + CHAR(13)+CHAR(10)
		
				-- get dates
				SELECT @PurchasedProductPaymentScheduleID=MIN(s.PurchasedProductPaymentScheduleID)
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
				WHERE s.PurchasedProductID=@PurchasedProductID
				AND s.PaymentStatusID=1
				AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID
				AND @AdjustmentDate<=s.CoverTo
				AND s.PurchasedProductPaymentScheduleTypeID IN ( 1 /*Scheduled*/, 6 /*MTA*/ )

				SELECT @LeadEventComments += '  PurchasedProductPaymentScheduleID: ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductPaymentScheduleID),'NULL') + CHAR(13)+CHAR(10)
				
				SELECT @MonthCoverFrom=s.CoverFrom, @MonthCoverTo=s.CoverTo, @PaymentDate=s.PaymentDate, 
						@ActualCollectionDate=CASE WHEN s.ActualCollectionDate<@NewPaymentDate THEN @NewPaymentDate ELSE s.ActualCollectionDate END
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID

				-- calculate any first month adjustment
				SELECT @MonthsFutureDue=SUM( s.PaymentGross - (@RoundedPayment + CASE WHEN @MonthCoverFrom=s.CoverFrom THEN @RemainderPayment ELSE 0 END) )
				
					  ,@MonthsFutureDueVAT=SUM( s.PaymentVAT - (@RoundedPaymentVAT + CASE WHEN @MonthCoverFrom=s.CoverFrom THEN @RemainderPaymentVAT ELSE 0 END))
											 
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
				INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = s.PurchasedProductID				
				WHERE s.PurchasedProductID=@PurchasedProductID
				AND s.PaymentStatusID IN ( 1 ) /*New*/ 
				AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID
				AND @AdjustmentDate<=ISNULL(s.CoverTo,s.ActualCollectionDate)
				AND s.PurchasedProductPaymentScheduleTypeID IN ( 1 /*Scheduled*/ )
				GROUP BY pp.ProductAdditionalFee

				-- NB this calculation works because when there is a reduction in premium @AdjustmentTermAmount will be negative and @MonthsFutureDue will be positive
				-- whilst when there is an increase in premium @AdjustmentTermAmount will be positive and @MonthsFutureDue will be negative
				SELECT @OneOffPaymentGross	= @AdjustmentTermAmount		+ @MonthsFutureDue


				SELECT @OneOffPaymentVAT	= @AdjustmentTermAmountVAT	+ @MonthsFutureDueVAT
				SELECT @OneOffPaymentNET	= @OneOffPaymentGross		- @OneOffPaymentVAT

				
				SELECT @LeadEventComments += '  Adjustment Term Amount: ' + ISNULL(CONVERT(VARCHAR,@AdjustmentTermAmount),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Months Future Due: ' + ISNULL(CONVERT(VARCHAR,@MonthsFutureDue),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  One Off Payment Gross: ' + ISNULL(CONVERT(VARCHAR,@OneOffPaymentGross),'NULL') + CHAR(13)+CHAR(10)
				SELECT @LeadEventComments += '  Minimum Adjustment Value: ' + ISNULL(CONVERT(VARCHAR,@MinimumAdjustmentValue),'NULL') + CHAR(13)+CHAR(10)
				
				-- get dates
				SELECT @PurchasedProductPaymentScheduleID=MIN(s.PurchasedProductPaymentScheduleID)
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
				WHERE s.PurchasedProductID=@PurchasedProductID
				AND s.PaymentStatusID=1
				AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID
				AND @AdjustmentDate<=s.CoverTo
				AND s.PurchasedProductPaymentScheduleTypeID IN ( 1 /*Scheduled*/, 6 /*MTA*/ )

				SELECT @LeadEventComments += '  PurchasedProductPaymentScheduleID: ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductPaymentScheduleID),'NULL') + CHAR(13)+CHAR(10)
				
				SELECT @MonthCoverFrom=s.CoverFrom, @MonthCoverTo=s.CoverTo, @PaymentDate=s.PaymentDate, 
						@ActualCollectionDate=CASE WHEN s.ActualCollectionDate<@NewPaymentDate THEN @NewPaymentDate ELSE s.ActualCollectionDate END
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID
				
				IF ABS(@OneOffPaymentGross) < @MinimumAdjustmentValue
					SELECT @OneOffPaymentGross=0
								
				IF @AdjustmentValueOnly = 0
				BEGIN
					
					/*JEL 2018-04-17 We are doing a proper MTA with premium change, so any remainder amounts from mismatched rounding do need adding into the schedule
					unless of course this is an MTA back to inception, in which case we need to keep track of the rounded payment*/
					IF @OriginalProductCost <> @PremiumAmount AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
																			  INNER JOIN PurchasedProductPaymentSchedule pp WITH (NOLOCK) on pp.PurchasedProductID = p.PurchasedProductID
																			  WHERE p.PurchasedProductID = @PurchasedProductID
																			  AND pp.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
																			  AND p.PurchasedProductPaymentScheduleTypeID = 1 
																			  AND pp.CoverFrom > p.CoverFrom) 
					BEGIN 
		
						UPDATE PurchasedProductPaymentSchedule
						SET PaymentGross = @RoundedPayment + CASE WHEN PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID THEN @RemainderPayment ELSE 0 END, 
							PaymentVAT = @RoundedPaymentVAT + CASE WHEN PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID THEN @RemainderPaymentVAT ELSE 0 END,
							PaymentNet = (@RoundedPayment - @RoundedPaymentVAT) + CASE WHEN PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID THEN @RemainderPayment - @RemainderPaymentVAT ELSE 0 END,
							TransactionFee = CASE PurchasedProductPaymentScheduleID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/
						WHERE PurchasedProductID=@PurchasedProductID
						AND PaymentStatusID=1
						AND PurchasedProductPaymentScheduleID=PurchasedProductPaymentScheduleParentID
						AND @AdjustmentDate<=CoverTo
						AND PurchasedProductPaymentScheduleTypeID IN ( 1 /*Scheduled*/) --, 6 /*MTA*/ ) /*ALM/GPR removed 6 for SDPRU-162 and SDPRU-165*/
					
					END 
					ELSE
					/*This MTA did change the risk factors, but the premiums are the same so we don't want to apply the rounded remainder again*/  
					BEGIN 
						
						UPDATE PurchasedProductPaymentSchedule
						SET PaymentGross = @RoundedPayment, 
							PaymentVAT = @RoundedPaymentVAT,
							PaymentNet = (@RoundedPayment - @RoundedPaymentVAT),
							TransactionFee = CASE PurchasedProductPaymentScheduleID WHEN @FirstPPPS THEN 0.00 ELSE ISNULL(@TransactionFee, 0.00) END/*GPR 2020-11-26*/
						WHERE PurchasedProductID=@PurchasedProductID
						AND PaymentStatusID=1
						AND PurchasedProductPaymentScheduleID=PurchasedProductPaymentScheduleParentID
						AND @AdjustmentDate<=CoverTo
						AND PurchasedProductPaymentScheduleTypeID IN ( 1 /*Scheduled*/)--, 6 /*MTA*/ ) /*ALM/GPR removed 6 for SDPRU-162 and SDPRU-165*/

					END 
											  
					-- reset the date of first regular payment if due prior to wait
					UPDATE PurchasedProductPaymentSchedule
					SET ActualCollectionDate=@ActualCollectionDate
					WHERE PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID
										
					IF @OneOffPaymentGross <> 0
					BEGIN
						
						IF @AccountTypeID = 2
						BEGIN 
					
							/*If negative then go through the transaction proc to do (potentially) multiple refund transactions.*/
							IF @OneOffPaymentGross < 0.00
							BEGIN

								/*2020-09-15 ACE call transactional refund proc..*/
								EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, 
										@OneOffPaymentNET, @OneOffPaymentVAT, @OneOffPaymentGross,
										1 /*Premium*/, @MonthCoverFrom, @MonthCoverTo, @WhenCreated, 6 /*MTA*/, @WhoCreated, 0, NULL, @TransactionFee /*GPR 2021-03-05 changed type from 7 to 6 for FURKIN-341*/


							END
							ELSE
							BEGIN

								SELECT  @OneOffPaymentGross = SUM(@OneOffPaymentNET + @OneOffPaymentVAT)

								EXEC _C00_LogIt 'Info','GPR','Net+Tax',@OneOffPaymentGross, 58550

								-- finally insert new purchased product payment schedule record for @OneOffPayment
								INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID,ClientAccountID, TransactionFee)
								VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, dbo.fn_GetDate_Local(), @MonthCoverFrom, @MonthCoverTo, 
									dbo.fn_GetDate_Local(), @OneOffPaymentNET, @OneOffPaymentVAT, @OneOffPaymentGross/*@OneOffPaymentGross/* + @TransactionFee*/*/, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(), 6 /*MTA*/, @ClientAccountID, 0.00) /*ALM/GPR 2020-11-26 TransactionFee = 0.00*/
							
								SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()

							END
					
						END 
						ELSE 
						BEGIN 
						
							/*If negative then go through the transaction proc to do (potentially) multiple refund transactions.*/
							IF @OneOffPaymentGross < 0.00
							BEGIN

								/*2020-09-15 ACE call transactional refund proc..*/
								EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, 
										@OneOffPaymentNET, @OneOffPaymentVAT, @OneOffPaymentGross,
										1 /*Premium*/, @MonthCoverFrom, @MonthCoverTo, @PaymentDate, 7, @WhoCreated, 0, NULL, @TransactionFee

							END
							ELSE
							BEGIN

								SELECT  @OneOffPaymentGross = SUM(@OneOffPaymentNET + @OneOffPaymentVAT)

								EXEC _C00_LogIt 'Info','GPR','Net+Tax',@OneOffPaymentGross, 58550

								-- finally insert new purchased product payment schedule record for @OneOffPayment
								INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID,ClientAccountID, TransactionFee)
								VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @ActualCollectionDate, @MonthCoverFrom, @MonthCoverTo, 
									@PaymentDate, @OneOffPaymentNET, @OneOffPaymentVAT, @OneOffPaymentGross/*GPR 2020-11-22 + @TransactionFee*/, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(), 6 /*MTA*/, @ClientAccountID, 0.00) /*ALM/GPR 2020-11-26 TransactionFee = 0.00*/
							
								SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()

							END

						END 

					END
			
					-- finally rebuild the customer payment schedule
					EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @AdjustmentDate, @WhoCreated
					
					/*2018-04-20 ACE - If card payer do the following*/
					IF (
							SELECT a.AccountTypeID 
							FROM Account a WITH (NOLOCK) 
							WHERE a.AccountID = @AccountID
						) = 2 
						AND @OneOffPaymentGross > 0 /*Dont do this for refunds as trans proc will hadle account creation(s)*/
						AND @PurchasedProductPaymentScheduleID > 0
					BEGIN

						/*So, the account is a card type. Lets check to see if we have an existing card, if not we will need to make a stub account record*/
						--IF NOT EXISTS (
						--	SELECT *
						--	FROM Account a WITH (NOLOCK)
						--	WHERE a.AccountID = @AccountID
						--	AND (a.ExpiryDate > dbo.fn_GetDate_Local() AND a.CardToken IS NOT NULL AND CONVERT(DATE, a.WhenCreated) <> CONVERT(DATE, GETDATE()))
						--)
						IF NOT EXISTS (/*GPR 2020-11-20 for PPET-732*/
										SELECT a.AccountID
										FROM Account a WITH (NOLOCK)
										WHERE a.CustomerID = @CustomerID
										AND a.AccountUseID = 1 /*ALM 2020-11-06 SDPRU-134*/
										AND a.Active = 1)
						BEGIN

							/*2021-01-18 ACE Use Client AccountID*/
							EXEC @NewAccountID = Billing__CreatePremiumAccountAccountRecord @ClientID, @MatterID, 2, NULL, NULL, 'CardAccount', @ClientAccountID, @CustomerID, @WhoCreated, 2

						END
						ELSE
						BEGIN

							SELECT @NewAccountID = @AccountID

						END

						SELECT @CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
						FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
						WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

						/*Set the accounts on the PPPS and CPS for the new card as required.*/
						UPDATE ppps
						SET AccountID = @NewAccountID
						FROM PurchasedProductPaymentSchedule ppps
						WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

						UPDATE cps
						SET AccountID = @NewAccountID
						FROM CustomerPaymentSchedule cps
						WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

						SELECT @ClientGatewayID = dbo.fn_C00_GetClientGateway(@MatterID)

						/*
							Final check.... if we have a "pending" cps records for this customer for one off mta catch ups.
							If we do then remove them...
						*/

						IF EXISTS (
							SELECT *
							FROM CardTransaction ct WITH (NOLOCK)
							WHERE ct.CustomerID = @CustomerID
							AND ct.Description = 'One Off MTA Catch Up'
							AND ISNULL(ct.StatusMsg, ct.ErrorMessage) IS NULL
						)
						BEGIN

							UPDATE ct
							SET StatusMsg = 'Ignored'
							FROM CardTransaction ct WITH (NOLOCK)
							WHERE ct.CustomerID = @CustomerID
							AND ct.Description = 'One Off MTA Catch Up'
							AND ISNULL(ct.StatusMsg, ct.ErrorMessage) IS NULL

						END

						-- 2019-12-09 CPS for JIRA LPC-181 | We now use a WorldPay control so don't create the Card Transaction here
						--EXEC dbo.Billing__CreatePaymentAndCTForCPS @CustomerPaymentScheduleID, @ClientGatewayID, 1, 'One Off MTA Catch Up', @LeadEventID, @WhoCreated

					END

				END
				ELSE
				BEGIN
						  
					EXEC _C00_SimpleValueIntoField @AdjustmentValueDFID,@OneOffPaymentGross,@MatterID

					EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@DifferenceProductCost,@MatterID /*CPS 2017-09-22*/
					--EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@AdjustmentTermAmount,@MatterID 
					
					-- first month payment
					SELECT @AdjustmentTermAmount=@OneOffPaymentGross+@RoundedPayment + CASE WHEN @MonthCoverFrom=@ValidFrom THEN @RemainderPayment ELSE 0 END

					EXEC _C00_SimpleValueIntoField @FirstMonthTotalDFID,@AdjustmentTermAmount,@MatterID 

				END
					
			END		
		
		END

		--For payments already taken, pro-rata the adjustment for the period since adjustment  
		--and make a single collection/refund for that paid adjustment value, 
		--and adjust premium on all uncollected payments to the new premium.
		--If the adjustment is in the future i.e. in a period that has not yet been collected, make a one-off adjustment for that first increment.				
		
		
		IF(@MidTermAdjustmentCalculationMethodID=5) --Collect/Refund in single payment
		BEGIN

			-- this is the date that they are moving house etc., and can be in the past
			IF (dbo.fnIsDateTime(@AdjustmentDate_DV)=1) 		
			BEGIN

				SELECT @NewPaymentDate = DATEADD(DAY,@OneOffAdjustmentWait,dbo.fn_GetDate_Local())
				SELECT @NextRegularPaymentDate = MIN(ppps.PaymentDate)
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
				WHERE ppps.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID 
				AND ppps.CustomerLedgerID IS NULL

				IF @NewPaymentDate > @NextRegularPaymentDate
				BEGIN
				
					INSERT INTO @PPPSIDs ( AnyID )
					SELECT ppps.PurchasedProductPaymentScheduleID
					FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
					WHERE ppps.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID 
					AND PaymentDate=@NextRegularPaymentDate
					
				END		
											
				-- get payment schedule data for adjustment date
				SELECT	@CoverFrom=CoverFrom, 
						@CoverTo=CoverTo, 
						@WithinAdjustmentDatePaymentGross=PaymentGross, 						
						@WithinAdjustmentDatePaymentVAT=PaymentVAT, 
						@WithinAdjustmentDatePurchasedProductPaymentScheduleID=PurchasedProductPaymentScheduleID,
						@PaymentStatusID=PaymentStatusID
				FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
				WHERE CoverFrom<=@AdjustmentDate 
				AND CoverTo>=@AdjustmentDate 
				AND PurchasedProductID=@PurchasedProductID

				-- Calculate adjustment for cover month that adjustment occurred
				-- We take remainder in first payment and this is first payment OR
				-- we take remainder in last payment and this is the last payment
				IF ( @RemainderUpFront=1 
					AND 0 = ( SELECT COUNT(*) FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
							  WHERE p.PurchasedProductID=@PurchasedProductID 
								AND p.PaymentStatusID IN (2,6,7) )
				   )
				   OR
				   ( @RemainderUpFront=0 
					AND ( SELECT CASE WHEN pp.NumberOfInstallments IS NULL THEN pf.DefaultInstallmentLength ELSE pp.NumberOfInstallments END
						  FROM PurchasedProduct pp WITH (NOLOCK) 
						  INNER JOIN PaymentFrequency pf WITH (NOLOCK) ON pp.PaymentFrequencyID=pf.PaymentFrequencyID
						  WHERE pp.PurchasedProductID=@PurchasedProductID )
						  =
						  ( SELECT COUNT(*) FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
						    WHERE p.PurchasedProductID=@PurchasedProductID 
							AND p.PaymentStatusID IN (2,6,7) )
				   )	
				BEGIN	--								New monthly Premium + Remainder       Old monthly Premium
					SET @DifferenceInMonthlyPremium = (@RoundedPayment + @RemainderPayment) - @WithinAdjustmentDatePaymentGross				
					SET @DifferenceInMonthlyPremiumVAT = (@RoundedPaymentVAT + @RemainderPaymentVAT) - @WithinAdjustmentDatePaymentVAT
				END
				ELSE
				BEGIN
					SET @DifferenceInMonthlyPremium = @RoundedPayment - @WithinAdjustmentDatePaymentGross				
					SET @DifferenceInMonthlyPremiumVAT = @RoundedPaymentVAT - @WithinAdjustmentDatePaymentVAT
				END
				
				-- we need to adjust for the proportional uptake of the beginning or end of period remainder amount (@OddRateToUse)
				-- this will not be correct if there are sequential MTAs but the issue will be a matter of a penny or so.
				-- find max & min dates for this product; find remainder amount; find proportion used, add to one-off adjustment
				DECLARE @OldSchedule TABLE ( PurchasedProductID INT, PurchasedProductPaymentScheduleID INT, PeriodGross MONEY, PeriodTax MONEY, CustomerLedgerID INT, CoverFrom DATE, CoverTo DATE, RNO INT )
				DECLARE @AdjustmentTermCount INT, @TermStart DATE, @TermEnd DATE, @NormalRate MONEY, @OddRate MONEY, @OddRateToUse MONEY, @NormalRateTax MONEY, @OddRateTax MONEY, @OddRateToUseTax MONEY

				INSERT INTO @OldSchedule
				SELECT ppps.PurchasedProductID, ppps.PurchasedProductPaymentScheduleID, ppps.PaymentGross, ppps.PaymentVAT, ppps.CustomerLedgerID, ppps.CoverFrom, ppps.CoverTo, 
						ROW_NUMBER() OVER ( PARTITION BY ppps.PurchasedProductID ORDER BY ppps.CoverFrom ) 
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
				WHERE ppps.PurchasedProductID=@PurchasedProductID
				AND ppps.PurchasedProductPaymentScheduleID=ppps.PurchasedProductPaymentScheduleParentID
				AND ppps.PaymentGroupedIntoID IS NULL 
				ORDER BY ppps.CoverFrom

				SELECT @TermStart=MIN(CoverFrom) FROM @OldSchedule
				SELECT @TermEnd=MAX(CoverTo) FROM @OldSchedule
				
				IF @RemainderUpFront=1
				BEGIN
				
					SELECT @OddRate=PeriodGross FROM @OldSchedule WHERE RNO=1 
					SELECT @NormalRate=PeriodGross FROM @OldSchedule WHERE RNO=2 
					SELECT @OddRateTax=PeriodTax FROM @OldSchedule WHERE RNO=1 
					SELECT @NormalRateTax=PeriodTax FROM @OldSchedule WHERE RNO=2 
				
				END
				ELSE
				BEGIN

					SELECT @OddRate=PeriodGross FROM @OldSchedule WHERE RNO=12 
					SELECT @NormalRate=PeriodGross FROM @OldSchedule WHERE RNO=11 
					SELECT @OddRateTax=PeriodTax FROM @OldSchedule WHERE RNO=12 
					SELECT @NormalRateTax=PeriodTax FROM @OldSchedule WHERE RNO=11 
				
				END
				
				SELECT @OddRateToUse =  ( CAST(DATEDIFF(DAY,@TermStart,@TermEnd) AS INT) - CAST(DATEDIFF(DAY,@TermStart,@AdjustmentDate) AS INT) + 1 ) * ( (@OddRate-@NormalRate) / CAST(DATEDIFF(DAY,@TermStart,@TermEnd) AS INT) )
				SELECT @OddRateToUseTax =  ( CAST(DATEDIFF(DAY,@TermStart,@TermEnd) AS INT) - CAST(DATEDIFF(DAY,@TermStart,@AdjustmentDate) AS INT) + 1 ) * ( (@OddRateTax-@NormalRateTax) / CAST(DATEDIFF(DAY,@TermStart,@TermEnd) AS INT) )
				
				SET @NumberOfDaysInPeriod = DATEDIFF(DAY,@CoverFrom,@CoverTo) + 1				
				SET @NumberOfDaysLeftInPeriod = DATEDIFF(DAY,@AdjustmentDate,@CoverTo) + 1
				SET @OneOffPaymentForAdjustmentPeriod = ROUND ( ( (@DifferenceInMonthlyPremium / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod ) + @OddRateToUse, 2)
				SET @OneOffPaymentForAdjustmentPeriodVAT = ROUND ( ( (@DifferenceInMonthlyPremiumVAT / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod ) + @OddRateToUse, 2)

				SELECT @LogEntry = 'Payment Status: ' + CAST(@PaymentStatusID as VARCHAR(10))
				EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
		
				IF @PaymentStatusID IN (2,6,7) -- adjustment date is in a period for which the premium has already been collected  --(@AdjustmentDate<@WhenCreated) -- date is in the past so calculate single payment from that point to the first un collected payment
				BEGIN
					
					-- Calculate remainder up until the first payment that has not been reconciled
					INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID, PaymentDate)
					SELECT PurchasedProductPaymentScheduleID, 
						   (@RoundedPayment - PaymentGross) - (@RoundedPaymentVAT - PaymentVAT), 
						   @RoundedPaymentVAT - PaymentVAT, 
						   @RoundedPayment - PaymentGross,
						   CoverFrom, 
						   CoverTo, 
						   AccountID,
						   PaymentDate
					FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
					WHERE CoverFrom>@CoverTo--PaymentDate>@AdjustmentDate 
					AND PurchasedProductID = @PurchasedProductID 
					AND ReconciledDate IS NOT NULL

					SELECT @PaymentForRemainderOfSchedule = ISNULL(SUM(PaymentGross),0) FROM @NewSchedule
					SELECT @PaymentNetForRemainderOfSchedule = ISNULL(SUM(PaymentNet),0) FROM @NewSchedule
					SELECT @PaymentVATForRemainderOfSchedule = ISNULL(SUM(PaymentVAT),0) FROM @NewSchedule

					SELECT @OneOffPaymentGross = @OneOffPaymentForAdjustmentPeriod + @PaymentForRemainderOfSchedule
					SELECT @OneOffPaymentVAT = @OneOffPaymentForAdjustmentPeriodVAT + @PaymentVATForRemainderOfSchedule
					SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT
					
					IF ABS(@OneOffPaymentGross) < @MinimumAdjustmentValue
						SELECT @OneOffPaymentGross=0

					--SELECT @NewCoverTo = @AdjustmentDate
					--SELECT @NewCoverFrom = CASE WHEN @CoverFrom < MIN(CoverFrom) THEN @CoverFrom ELSE MIN(CoverFrom) END FROM @NewSchedule
					SELECT @NewCoverFrom = @AdjustmentDate
					SELECT @NewCoverTo = MAX(CoverTo) FROM @NewSchedule
					IF @NewCoverTo IS NULL 
						SELECT @NewCoverTo=@CoverTo

					SELECT @AccountID=AccountID 
					FROM PurchasedProduct WITH (NOLOCK) 
					WHERE PurchasedProductID=@PurchasedProductID							
		
					IF @AdjustmentValueOnly = 0
					BEGIN
					
						-- update the PurchasedProductPaymentSchedule for rows where an amount has been updated
						UPDATE PurchasedProductPaymentSchedule
						SET PurchasedProductPaymentSchedule.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
						FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
						INNER JOIN dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK) ON pps.PaymentDate = cps.PaymentDate AND pps.CustomerID=cps.CustomerID
						WHERE cps.CustomerID=@CustomerID 
						AND cps.CustomerLedgerID IS NULL
						
						-- update the remainder of the schedule with the new premium payment -- @RoundedPayment --
						UPDATE PurchasedProductPaymentSchedule
						SET PaymentGross =  @RoundedPayment, 
							PaymentVAT = @RoundedPaymentVAT,
							PaymentNet = (@RoundedPayment - @RoundedPaymentVAT)
						WHERE PurchasedProductID = @PurchasedProductID 
						AND ReconciledDate IS NULL 
						AND PurchasedProductPaymentScheduleID NOT IN (SELECT ID FROM @NewSchedule) 
						AND PurchasedProductPaymentScheduleID <> @WithinAdjustmentDatePurchasedProductPaymentScheduleID
				
						SELECT @LogEntry = 'OneOffPayment: ' + CAST(@OneOffPaymentGross as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
											
						IF @OneOffPaymentGross <> 0
						BEGIN
							
							SELECT @AccountTypeID = a.AccountTypeID
							FROM Account a WITH (NOLOCK) 
							WHERE a.AccountID = @AccountID

							SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
									THEN 69930 /*Direct Debit*/
									ELSE 69931 /*Credit Card*/
									END

							SELECT @PaymentMethod = 69943 /*Monthly*/

							SELECT @TransactionFee = dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod)

							EXEC [dbo].[_C00_LogIt] 'Info', 'MidTermAdjustment__CreateFromThirdPartyFields', '@TransactionFee', @TransactionFee, 58550 /*GPR 2020-10-05*/

							IF @AccountTypeID = 2
							BEGIN 
						
								-- Insert new purchased product payment schedule record for @OneOffPayment							
								INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, ClientAccountID, TransactionFee)
								VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, dbo.fn_GetDate_Local(), @NewCoverFrom, @NewCoverTo, dbo.fn_GetDate_Local(), @OneOffPaymentNET, @OneOffPaymentVAT,(@OneOffPaymentNET + @OneOffPaymentVAT)/* + @TransactionFee*/, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(),@ClientAccountID, @TransactionFee)
							
								SELECT @LogEntry = 'Inserted One-off Payment into Sch schedule'
								EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
							
							END 
							ELSE 
							BEGIN 
							
								-- Insert new purchased product payment schedule record for @OneOffPayment							
								INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, ClientAccountID, TransactionFee)
								VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @NewPaymentDate, @NewCoverFrom, @NewCoverTo, @NewPaymentDate, @OneOffPaymentNET, @OneOffPaymentVAT, ISNULL(NULLIF(@OneOffPaymentGross,''), @OneOffPaymentNET) /* + @TransactionFee*/, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(),@ClientAccountID, @TransactionFee)
							
								SELECT @LogEntry = 'Inserted One-off Payment into Sch schedule'
								EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
							
							END  			
							
						END
						
						SELECT @LogEntry = CAST(@AccountID as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
						
						SELECT @LogEntry = CAST(@NewPaymentDate as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
						
						SELECT @LogEntry = CAST(@WhoCreated as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
						-- finally rebuild the customer payment schedule
						
						EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @NewPaymentDate, @WhoCreated
						
					END
					ELSE
					BEGIN

						-- adjustment amount for cover period of adjustment date
						 EXEC _C00_SimpleValueIntoField @AdjustmentValueDFID,@OneOffPaymentGross,@MatterID 
						
						-- adjustment amount for remainder of annual cover period
						SELECT @AdjustmentTermAmount=SUM(PeriodGross), @AdjustmentTermCount=COUNT(PeriodGross) 
						FROM @OldSchedule
						WHERE CustomerLedgerID IS NULL 
						AND CoverFrom>@AdjustmentDate

						SELECT @AdjustmentTermAmount = ( (@RoundedPayment*@AdjustmentTermCount) + @OneOffPaymentGross + CASE WHEN @AdjustmentTermCount=12 THEN @RemainderPayment ELSE 0 END ) - @AdjustmentTermAmount

						EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@AdjustmentTermAmount,@MatterID 
							
					END

					
				END
				ELSE -- @AdjustmentDate is in the future (or in unpaid period) so simply adjust the schedule from that date provided its after the @AdjustmentDate
					 -- and add an adjustment payment for the first period of adjusted payments if the adjustment payment <> 0 
				BEGIN
										
					SELECT @OneOffPaymentGross = @OneOffPaymentForAdjustmentPeriod
					SELECT @OneOffPaymentVAT = @OneOffPaymentForAdjustmentPeriodVAT
					SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT
					
					IF ABS(@OneOffPaymentGross) < @MinimumAdjustmentValue
						SELECT @OneOffPaymentGross=0

					IF @AdjustmentValueOnly = 0
					BEGIN

						UPDATE PurchasedProductPaymentSchedule
						SET PaymentGross = @RoundedPayment,  -- GPR
							PaymentVAT = @RoundedPaymentVAT,
							PaymentNet = (@RoundedPayment - @RoundedPaymentVAT)
						WHERE PurchasedProductID=@PurchasedProductID 
						AND ReconciledDate IS NULL 
						AND CoverFrom > @AdjustmentDate
							  
						-- reset the date of first regular payment if due prior to wait

						IF @OneOffPaymentGross <> 0
						BEGIN

							
							SELECT @AccountTypeID = a.AccountTypeID
							FROM Account a WITH (NOLOCK) 
							WHERE a.AccountID = @AccountID

							SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
									THEN 69930 /*Direct Debit*/
									ELSE 69931 /*Credit Card*/
									END

							SELECT @PaymentMethod = 69943 /*Monthly*/

							SELECT @TransactionFee = dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod)

							EXEC [dbo].[_C00_LogIt] 'Info', 'MidTermAdjustment__CreateFromThirdPartyFields', '@TransactionFee', @TransactionFee, 58550 /*GPR 2020-10-05*/

							IF @AccountTypeID = 2
							BEGIN 
							
								-- finally insert new purchased product payment schedule record for @OneOffPayment							
								INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, ClientAccountID, TransactionFee)
								VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, dbo.fn_GetDate_Local(), @CoverFrom, @CoverTo, dbo.fn_GetDate_Local(), @OneOffPaymentNET, @OneOffPaymentVAT, (@OneOffPaymentNET + @OneOffPaymentVAT) /*+ @TransactionFee*/, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(),@ClientAccountID, @TransactionFee)
							
							END
							ELSE 
							BEGIN 
																					
								SELECT TOP 1 @NewPaymentDate=p.PaymentDate 
								FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
								WHERE PurchasedProductID = @PurchasedProductID 
								AND ReconciledDate IS NULL 
								AND PaymentDate >= @AdjustmentDate
								ORDER BY p.PaymentDate
								
								-- finally insert new purchased product payment schedule record for @OneOffPayment							
								INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, ClientAccountID, TransactionFee)
								VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @NewPaymentDate, @CoverFrom, @CoverTo, @NewPaymentDate, @OneOffPaymentNET, @OneOffPaymentVAT,(@OneOffPaymentNET + @OneOffPaymentVAT)/* + @TransactionFee*/, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(),@ClientAccountID, @TransactionFee)
							
							END 
						END

						SELECT @LogEntry = CAST(@AccountID as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
						
						SELECT @LogEntry = CAST(@NewPaymentDate as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
						
						SELECT @LogEntry = CAST(@WhoCreated as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
						
						-- finally rebuild the customer payment schedule
						EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @AdjustmentDate, @WhoCreated
						
					END
					ELSE
					BEGIN
					
						SELECT @LogEntry = CAST(@AdjustmentValueDFID as VARCHAR(10))
						EXEC _C00_logit  'PROC', 'MidTermAdjustment__CreateFromThirdPartyFields','LogEntry',@LogEntry,44412
							  
						EXEC _C00_SimpleValueIntoField @AdjustmentValueDFID,@OneOffPaymentGross,@MatterID
						
						-- adjustment amount for remainder of annual cover period
						SELECT @AdjustmentTermAmount=SUM(PeriodGross), @AdjustmentTermCount=COUNT(PeriodGross) 
						FROM @OldSchedule
						WHERE CustomerLedgerID IS NULL 
						AND CoverFrom>@AdjustmentDate

						SELECT @AdjustmentTermAmount = ( (@RoundedPayment*@AdjustmentTermCount) + @OneOffPaymentGross + CASE WHEN @AdjustmentTermCount=12 THEN @RemainderPayment ELSE 0 END ) - @AdjustmentTermAmount

						EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@AdjustmentTermAmount,@MatterID 
					
					END

				END
				
				IF @AdjustmentValueOnly = 0
				BEGIN

					-- update regular payment dates where necessary
					UPDATE ppps
					SET PaymentDate=@NewPaymentDate
					FROM PurchasedProductPaymentSchedule ppps
					INNER JOIN @PPPSIDs pid ON ppps.PurchasedProductPaymentScheduleID=pid.AnyID
				END
					
			END		
		
		END
		

		--For payments already taken, calculate the pro-rata adjustment value for the period since adjustment  
		--and adjust premium on all uncollected payments to the new premium  + the paid adjustment value distributed over the remaining payments.
		IF(@MidTermAdjustmentCalculationMethodID=4) -- Collect/Refund over remaining policy period
		BEGIN
		
			DECLARE @RemainingPeriods INT -- the number of periods left that have not been collected
			
			SELECT @RemainingPeriods = COUNT(*) 
			FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
			WHERE PurchasedProductID=@PurchasedProductID 
			AND CustomerID = @CustomerID 
			AND ReconciledDate IS NULL 
			AND PaymentDate>=@NewPaymentDate

			--Purchased product payment schedule that falls within the new payment period
			SELECT	@CoverFrom=CoverFrom, 
					@CoverTo=CoverTo, 
					@WithinAdjustmentDatePaymentGross=PaymentGross, 						
					@WithinAdjustmentDatePaymentVAT=PaymentVAT, 
					@WithinAdjustmentDatePurchasedProductPaymentScheduleID=PurchasedProductPaymentScheduleID
			FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
			WHERE CoverFrom <= @NewPaymentDate 
			AND CoverTo >= @NewPaymentDate 
			AND PurchasedProductID = @PurchasedProductID
			
			IF @RemainderUpFront=1 -- take the remainder up front
			BEGIN	--								New monthly Premium + Remainder       Old monthly Premium
				SET @DifferenceInMonthlyPremium = (@RoundedPayment + @RemainderPayment) - @WithinAdjustmentDatePaymentGross				
				SET @DifferenceInMonthlyPremiumVAT = (@RoundedPaymentVAT + @RemainderPaymentVAT) - @WithinAdjustmentDatePaymentVAT
			END
			ELSE
			BEGIN
				SET @DifferenceInMonthlyPremium = @RoundedPayment - @WithinAdjustmentDatePaymentGross				
				SET @DifferenceInMonthlyPremiumVAT = @RoundedPaymentVAT - @WithinAdjustmentDatePaymentVAT
			END
					
			SET @NumberOfDaysInPeriod = DATEDIFF(DAY,@CoverFrom,@CoverTo) + 1				
			SET @NumberOfDaysLeftInPeriod = DATEDIFF(DAY,@AdjustmentDate,@CoverTo) + 1
			SET @OneOffPaymentForAdjustmentPeriod = (@DifferenceInMonthlyPremium / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod
			SET @OneOffPaymentForAdjustmentPeriodVAT = (@DifferenceInMonthlyPremiumVAT / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod				

			-- Calculate remainder up until the first payment that has not been reconciled
			INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID)
			SELECT PurchasedProductPaymentScheduleID, 
				   (@RoundedPayment - PaymentGross) - (@RoundedPaymentVAT - PaymentVAT), 
				   @RoundedPaymentVAT - PaymentVAT, 
				   @RoundedPayment - PaymentGross,
				   CoverFrom, 
				   CoverTo, 
				   AccountID
			FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
			WHERE PaymentDate>@AdjustmentDate AND PurchasedProductID=@PurchasedProductID AND ReconciledDate IS NOT NULL

			SELECT @PaymentForRemainderOfSchedule = SUM(PaymentGross) FROM @NewSchedule
			SELECT @PaymentNetForRemainderOfSchedule = SUM(PaymentNet) FROM @NewSchedule
			SELECT @PaymentVATForRemainderOfSchedule = SUM(PaymentVAT) FROM @NewSchedule
			
			SELECT @OneOffPaymentGross = @OneOffPaymentForAdjustmentPeriod + @PaymentForRemainderOfSchedule
			SELECT @OneOffPaymentVAT = @OneOffPaymentForAdjustmentPeriodVAT + @PaymentVATForRemainderOfSchedule
			SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT				
					
			DECLARE @RedistributeValuePaymentGross NUMERIC(18,2)
			DECLARE @RedistributeValuePaymentNET NUMERIC(18,2)
			DECLARE @RedistributeValuePaymentVAT NUMERIC(18,2)
			
			-- redistribution value @OneOffPaymentGross / @RemainingPeriods
			SELECT @RedistributeValuePaymentGross = @OneOffPaymentGross / @RemainingPeriods
			SELECT @RedistributeValuePaymentVAT = @OneOffPaymentVAT / @RemainingPeriods
			SELECT @RedistributeValuePaymentNET = @RedistributeValuePaymentGross - @RedistributeValuePaymentVAT
							
			INSERT INTO @NewScheduleNotReconciled (ID, PaymentNet, PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID)
			SELECT PurchasedProductPaymentScheduleID, 
				   (@RoundedPayment + @RedistributeValuePaymentGross) - (@RoundedPaymentVAT + @RedistributeValuePaymentVAT),
				   @RoundedPaymentVAT + @RedistributeValuePaymentVAT, 
				   @RoundedPayment + @RedistributeValuePaymentGross, 
				   CoverFrom, 
				   CoverTo, 
				   AccountID
			FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
			WHERE PaymentDate > @AdjustmentDate 
			AND PurchasedProductID = @PurchasedProductID 
			AND ReconciledDate IS NULL 
			AND PurchasedProductPaymentScheduleID NOT IN (SELECT ID FROM @NewSchedule)
			
			-- update purchased product schedule
			UPDATE PurchasedProductPaymentSchedule		    
			SET
				PurchasedProductPaymentSchedule.PaymentGross = NewSchedule.PaymentGross,
				PurchasedProductPaymentSchedule.PaymentVAT = NewSchedule.PaymentVAT,
				PurchasedProductPaymentSchedule.PaymentNet = NewSchedule.PaymentNet
			FROM PurchasedProductPaymentSchedule AS PurchasedProductPaymentSchedule
			INNER JOIN @NewScheduleNotReconciled AS NewSchedule ON PurchasedProductPaymentSchedule.PurchasedProductPaymentScheduleID = NewSchedule.ID
			
		END
	
	END
		
	IF @AdjustmentValueOnly = 0
	BEGIN

		--link purchased product payment schedule to customer payment schedule on payment date
		UPDATE PurchasedProductPaymentSchedule
		SET PurchasedProductPaymentSchedule.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK) ON pps.PaymentDate = cps.PaymentDate
		WHERE cps.CustomerID = @CustomerID 
		AND cps.CustomerLedgerID = NULL

		-- set PPPS type to Adhoc and parent ID to itself for the one-off payment records added
		UPDATE PurchasedProductPaymentSchedule
		SET PurchasedProductPaymentScheduleParentID = PurchasedProductPaymentScheduleID,
			PurchasedProductPaymentScheduleTypeID = 6 /*MTA*/
			--PurchasedProductPaymentScheduleTypeID = 2
		WHERE PurchasedProductPaymentScheduleParentID IS NULL
		AND PurchasedProductID = @PurchasedProductID /*CPS 2017-09-25 stop this from updating ALL products*/

		IF @AccountID>0
		BEGIN
			EXEC Account__SetDateAndAmountOfNextPayment @AccountID	
		END
		
		-- 2020-01-22 CPS for JIRA LPC-394 | Update Written Premium following the MTA completion as we need to know the total payable for the year first
		EXEC WrittenPremium_UpdateAdjustment @ClientID, @CaseID, @LeadEventID

	END


	/*GPR 2020-06-16 Update the PurchasedProduct CostGross, CostNet, CostVAT*/
	-- Backup purchased product and update the existing record
	EXEC PurchasedProductHistory__Backup @PurchasedProductID, @WhoCreated, @LeadEventID

	--/*JEL - Brutish fix Start to check Transaction fee is correctly applied to unpaid rows*/ 

	--IF @PaymentTypeLuli IS NULL 
	--BEGIN
	--	SELECT @AccountTypeID = a.AccountTypeID
	--	FROM Account a WITH (NOLOCK) 
	--	WHERE a.AccountID = @AccountID
			
	--	SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
	--							  THEN 69930 /*Direct Debit*/
	--							  ELSE 69931 /*Credit Card*/
	--							  END
	--END 

	--SELECT @TransactionFee = dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod)
		
	--UPDATE PurchasedProductPaymentSchedule
	--SET TransactionFee = ISNULL(@TransactionFee,0.00) 
	--FROM PurchasedProductPaymentSchedule
	--WHERE PurchasedProductID=@PurchasedProductID
	--AND PaymentStatusID IN (1,2)
	--AND PurchasedProductPaymentScheduleTypeID IN (1,3)
	--AND TransactionFee <> @TransactionFee
		 
	--/*JEL - Brutish fix End to check Transaction fee is correctly applied*/ 

	UPDATE PurchasedProductPaymentSchedule
	SET PaymentGross = PaymentNet + ISNULL(PaymentVAT,0.00)
	FROM PurchasedProductPaymentSchedule
	WHERE PurchasedProductID = @PurchasedProductID
	AND PurchasedProductPaymentScheduleTypeID = 6 /*MTA Adjustment*/
	AND PaymentStatusID NOT IN (6,4) /*Paid, Failed*/

	/*GPR 2020-09-25 Added for SDPRU-54*/
	UPDATE PurchasedProductPaymentSchedule
	SET PaymentGross = PaymentNet + ISNULL(PaymentVAT,0.00) + ISNULL(TransactionFee,0.00) /*GPR 2020-12-03 ISNULL TransactionFee to Zero as part of fixes for MTA issues*/
	FROM PurchasedProductPaymentSchedule
	WHERE PurchasedProductID = @PurchasedProductID
	AND PaymentStatusID IN (1,2)

	SELECT @LeadEventComments += ' Adjustment Date: ' + ISNULL(CONVERT(VARCHAR(10),@AdjustmentDate, 120),'NULL') + CHAR(13)+CHAR(10)
	SELECT @LeadEventComments += ' Valid From: ' + ISNULL(CONVERT(VARCHAR(10),@ValidFrom, 120),'NULL') + CHAR(13)+CHAR(10)

	--EXEC _C00_LogIt 'Info', 'MidTermAdjustment__CreateFromThirdPartyFields', 'StateCode', @StateCode, 63159

	/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
	IF @ClientID = 607 AND @AdjustmentDate = @ValidFrom /*Only on MTA to inception*/
	BEGIN

		/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
		EXEC _C607_MTA_EnrollmentFee @CustomerID, @LeadID, @MatterID, @PurchasedProductID, @StateCode, @AccountID, @ClientAccountID, @WhoCreated

	END

	EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @AdjustmentDate, @WhoCreated /*ALM 2020-11-11*/

	;
	WITH SummedPayments AS (
		SELECT ppps.PurchasedProductID,
				SUM(ppps.PaymentGross) AS [SummedGross],
				SUM(ppps.PaymentNet) AS [SummedNet],
				SUM(ppps.PaymentVAT) AS [SummedVAT],
				SUM(ppps.TransactionFee) AS [SummedTransFee]
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		WHERE ppps.PurchasedProductID = @PurchasedProductID 
		AND ppps.PaymentStatusID IN (6,1,2)
		AND ppps.PurchasedProductPaymentScheduleTypeID NOT IN (5)
		GROUP BY ppps.PurchasedProductID
	)

	UPDATE pp
	SET ProductCostGross = s.SummedGross,
		ProductCostNet = s.SummedNet,
		ProductCostVAT = s.SummedVAT,
		ProductAdditionalFee = s.SummedTransFee
	FROM PurchasedProduct pp
	INNER JOIN SummedPayments s ON s.PurchasedProductID = pp.PurchasedProductID
	WHERE pp.PurchasedProductID = @PurchasedProductID

	EXEC _C00_SetLeadEventComments @LeadEventID, @LeadEventComments, 1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[MidTermAdjustment__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MidTermAdjustment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MidTermAdjustment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
