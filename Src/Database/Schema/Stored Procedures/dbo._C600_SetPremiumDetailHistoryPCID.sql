SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-10-05
-- Description:	Update the PremiumCalculationID in the Premium Detail History table with the
--				value from the Policy Premiums table
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SetPremiumDetailHistoryPCID] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PDHTableRowID INT,
			@PPTableRowID INT,
			@PremiumCalculationID VARCHAR(50)
	
	SELECT TOP 1 @PDHTableRowID=pcid.TableRowID 
	FROM TableDetailValues pcid WITH (NOLOCK) 
	INNER JOIN TableDetailValues stat WITH (NOLOCK) ON pcid.TableRowID=stat.TableRowID AND stat.DetailFieldID=175722 
		AND stat.ValueInt=72326
	WHERE pcid.MatterID=@MatterID AND pcid.DetailFieldID=175709 
		AND pcid.ValueInt=0
	ORDER BY pcid.TableRowID DESC
	
	IF @PDHTableRowID IS NOT NULL
	BEGIN
	
		SELECT TOP 1 @PPTableRowID=pcid.TableRowID,@PremiumCalculationID=pcid.DetailValue 
		FROM TableDetailValues pcid WITH (NOLOCK) 
		INNER JOIN TableDetailValues stat WITH (NOLOCK) ON pcid.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078 
			AND stat.ValueInt=72154
		WHERE pcid.MatterID=@MatterID AND pcid.DetailFieldID=175710 
		ORDER BY pcid.TableRowID DESC
		
		UPDATE TableDetailValues SET DetailValue=@PremiumCalculationID WHERE TableRowID=@PDHTableRowID AND DetailFieldID=175709
	
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetPremiumDetailHistoryPCID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SetPremiumDetailHistoryPCID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetPremiumDetailHistoryPCID] TO [sp_executeall]
GO
