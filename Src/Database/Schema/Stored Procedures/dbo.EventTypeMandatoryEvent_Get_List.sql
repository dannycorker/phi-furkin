SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeMandatoryEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_Get_List]

AS


				
				SELECT
					[EventTypeMandatoryEventID],
					[ClientID],
					[LeadTypeID],
					[ToEventTypeID],
					[RequiredEventID]
				FROM
					[dbo].[EventTypeMandatoryEvent] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Get_List] TO [sp_executeall]
GO
