SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryCriteriaSetMember table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryIDSqlQueryColumnID]
(

	@SqlQueryID int   ,

	@SqlQueryColumnID int   
)
AS


				SELECT
					[SqlQueryCriteriaSetMemberID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryCriteriaSetID],
					[SqlQueryColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryCriteriaSetMember] WITH (NOLOCK)
				WHERE
					[SqlQueryID] = @SqlQueryID
					AND [SqlQueryColumnID] = @SqlQueryColumnID
				SELECT @@ROWCOUNT
					
			




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryIDSqlQueryColumnID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryIDSqlQueryColumnID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryIDSqlQueryColumnID] TO [sp_executeall]
GO
