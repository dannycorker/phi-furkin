SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumOptionType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOptionType_GetByAquariumOptionTypeID]
(

	@AquariumOptionTypeID int   
)
AS


				SELECT
					[AquariumOptionTypeID],
					[AquariumOptionTypeName],
					[AquariumOptionTypeDescription]
				FROM
					[dbo].[AquariumOptionType] WITH (NOLOCK) 
				WHERE
										[AquariumOptionTypeID] = @AquariumOptionTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_GetByAquariumOptionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOptionType_GetByAquariumOptionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_GetByAquariumOptionTypeID] TO [sp_executeall]
GO
