SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PageGroupDefaultPageLink table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroupDefaultPageLink_Find]
(

	@SearchUsingOR bit   = null ,

	@PageGroupDefaultPageLinkID int   = null ,

	@ClientID int   = null ,

	@PageGroupID int   = null ,

	@DetailFieldPageID int   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PageGroupDefaultPageLinkID]
	, [ClientID]
	, [PageGroupID]
	, [DetailFieldPageID]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[PageGroupDefaultPageLink] WITH (NOLOCK) 
    WHERE 
	 ([PageGroupDefaultPageLinkID] = @PageGroupDefaultPageLinkID OR @PageGroupDefaultPageLinkID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PageGroupID] = @PageGroupID OR @PageGroupID IS NULL)
	AND ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PageGroupDefaultPageLinkID]
	, [ClientID]
	, [PageGroupID]
	, [DetailFieldPageID]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[PageGroupDefaultPageLink] WITH (NOLOCK) 
    WHERE 
	 ([PageGroupDefaultPageLinkID] = @PageGroupDefaultPageLinkID AND @PageGroupDefaultPageLinkID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PageGroupID] = @PageGroupID AND @PageGroupID is not null)
	OR ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroupDefaultPageLink_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Find] TO [sp_executeall]
GO
