SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DroppedOutCustomerMessages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerMessages_Get_List]

AS


				
				SELECT
					[DroppedOutCustomerMessageID],
					[ClientID],
					[ClientQuestionnaireID],
					[EmailFromAddress],
					[EmailSubject],
					[Email],
					[SmsMessage]
				FROM
					[dbo].[DroppedOutCustomerMessages] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerMessages_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Get_List] TO [sp_executeall]
GO
