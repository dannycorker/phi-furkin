SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2021-02-09
-- Description:	Import HSBC XML Response data into BACS Payment Failures [WIP]
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_ImportResponse_HsbcCanada]
	 @XMLRaw VARCHAR(MAX)
	,@FileName VARCHAR(100) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XML XML = @XMLRaw,
			@ClientID INT = dbo.fnGetPrimaryClientID()

	/*NB Incoming sort code can have hyphens in them...*/
	INSERT INTO BACSPaymentFailures (ClientID, FileName, FailureFileType, RowNum, PaymentID, AccountNumber, SortCode, Name, PaymentAmount, BACSProcessedDate, FailureReason, FailureCode, Reference, ImportStatusID, ImportedDate, Comments)
		OUTPUT inserted.*
	SELECT	 @ClientID																[ClientID]
			,@FileName																[FileName]
			,'HSBC_PSRv3'															[FailureFileType]
			,ROW_NUMBER() OVER (ORDER BY (SELECT 1))								[RowNum]
			,b.value('(OrgnlEndToEndId)[1]','VARCHAR(2000)')						[OrgnlEndToEndId] -- AKA PaymentID
			,b.value('(//CdtrAcct/Id/Othr/Id)[1]','VARCHAR(2000)')					[PayerAccountNumber]
			,b.value('(//CdtrAgt/FinInstnId/ClrSysMmbId/MmbId)[1]','VARCHAR(2000)')	[PayerSortCode]
			,b.value('(OrgnlTxRef/Cdtr/Nm)[1]','VARCHAR(2000)')						[PayerName]
			,b.value('(OrgnlTxRef/Amt/InstdAmt)[1]','DECIMAL(18,2)')				[Amount]
			,b.value('(OrgnlTxRef/ReqdExctnDt)[1]','VARCHAR(2000)')					[OriginalProcessingDate]
			,b.value('(TxSts)[1]','VARCHAR(2000)')									[ReasonDescription]
			,'Refer to Payer'														[ReasonCode] -- 2021-02-19 CPS for JIRA FURKIN-71 | Allow [Payment__PostPaymentProcedure] to recognise this failure
			,b.value('(OrgnlInstrId)[1]','VARCHAR(2000)')							[Reference]
			,1																		[ImportStatusID]
			,dbo.fn_GetDate_Local()													[ImportedDate]
			,OBJECT_NAME(@@ProcID)													[Comments]

	FROM @XML.nodes('(//CstmrPmtStsRpt/OrgnlPmtInfAndSts/TxInfAndSts)') a(b)
	WHERE b.value('(TxSts)[1]','VARCHAR(2000)') <> 'ACCP' -- Only log records that don't come back as "accepted"
	OPTION (OPTIMIZE FOR ( @XML = NULL ))

	EXEC Billing__ReconcilePaymentFailures @ClientID

	SELECT *
	FROM BacsPaymentFailures b
	WHERE b.FileName = @FileName

END




GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_ImportResponse_HsbcCanada] TO [sp_executeall]
GO
