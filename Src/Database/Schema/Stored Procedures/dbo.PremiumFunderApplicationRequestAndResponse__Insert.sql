SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-11-18
-- Description:	Insert into PremiumFunderApplicationRequest and PremiumFunderApplicationResponse
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplicationRequestAndResponse__Insert]
	@PremiumFunderApplicationRequestID INT = 0, /*Application Request*/
	@PremiumFunderRequestID INT = 0, /*Quote Request, links the Quote Request/Response with the Application Request/Response*/
	@ClientID INT,
	@CustomerID INT = 0,
	@MatterID INT = NULL,
	@Title INT = NULL,
	@Firstname VARCHAR(50) = NULL,
	@Lastname VARCHAR(50) = NULL,
	@Address1 VARCHAR(50) = NULL,
	@Address2 VARCHAR(50) = NULL,
	@Town VARCHAR(50) = NULL,
	@PostCode VARCHAR(10) = NULL,
	@Email VARCHAR(250) = NULL,
	@externalQuoteReference NVARCHAR(250),
	@RequestJson NVARCHAR(MAX),
	@ResponseJson NVARCHAR(MAX),
	@WhenCreated DATETIME,
    @WhoCreated INT,
	@QuoteSessionID INT = NULL

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @StubCustomerID INT
	
	EXEC _C00_LogIt 'Info','PremiumFunderApplicationRequestAndResponse__Insert','@PremiumFunderApplicationRequestID = ', @PremiumFunderApplicationRequestID, @WhoCreated

	IF @PremiumFunderApplicationRequestID = 0
	BEGIN
		INSERT INTO PremiumFunderApplicationRequest ([ClientID],[CustomerID],[MatterID],[RequestJson],[PremiumFunderRequestID],[externalQuoteReference], [WhenCreated],[WhoCreated])
		VALUES (@ClientID, @CustomerID, @MatterID, @RequestJson, @PremiumFunderRequestID, @externalQuoteReference, @WhenCreated, @WhoCreated)
     
		SELECT @PremiumFunderApplicationRequestID = SCOPE_IDENTITY()

		INSERT INTO [PremiumFunderApplicationResponse] ([PremiumFunderApplicationRequestID], [ClientID],[CustomerID],[MatterID],[ResponseJson],[PremiumFunderRequestID],[WhenCreated],[WhoCreated])
		VALUES (@PremiumFunderApplicationRequestID, @ClientID, @CustomerID, @MatterID, @ResponseJson, @PremiumFunderRequestID, @WhenCreated, @WhoCreated)

	END
    ELSE
	BEGIN

		UPDATE request
		SET RequestJson = @RequestJson, externalQuoteReference = @externalQuoteReference, WhenCreated = @WhenCreated, WhoCreated = @WhoCreated
		FROM PremiumFunderApplicationRequest request WITH (NOLOCK)
		WHERE request.PremiumFunderApplicationRequestID = @PremiumFunderApplicationRequestID

		UPDATE response
		SET ResponseJson = @ResponseJson, WhenCreated = @WhenCreated, WhoCreated = @WhoCreated
		FROM PremiumFunderApplicationResponse response WITH (NOLOCK)
		WHERE response.PremiumFunderApplicationRequestID = @PremiumFunderApplicationRequestID

	END

	/*GPR 2019-11-27 - If the Customer for the ID that we have does not currently have an Address1 value then we need to update the address - this Customer was a Stub for PremiumFunding*/
	SELECT @StubCustomerID = CustomerID FROM Customers WHERE Address1 IS NULL AND CustomerID = @CustomerID

	IF @StubCustomerID IS NOT NULL
	BEGIN
		UPDATE c
		SET Address1 = @Address1, Address2 = @Address2, Town = @Town, Postcode = @PostCode
		FROM Customers c WITH (NOLOCK) WHERE c.CustomerID = @CustomerID 
	END

	IF @MatterID IS NOT NULL
	BEGIN
		UPDATE PremiumFunderApplicationPolicy SET PremiumFunderRequestID = @PremiumFunderRequestID
		WHERE PAMatterID = @MatterID
	END

	SELECT @PremiumFunderApplicationRequestID AS PremiumFunderApplicationRequestID, @CustomerID AS CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationRequestAndResponse__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplicationRequestAndResponse__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationRequestAndResponse__Insert] TO [sp_executeall]
GO
