SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIEventTypeMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventTypeMapping_Delete]
(

	@RPIEventTypeMappingID int   
)
AS


				DELETE FROM [dbo].[RPIEventTypeMapping] WITH (ROWLOCK) 
				WHERE
					[RPIEventTypeMappingID] = @RPIEventTypeMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventTypeMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Delete] TO [sp_executeall]
GO
