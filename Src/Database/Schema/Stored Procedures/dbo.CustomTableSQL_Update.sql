SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-08
-- Description:	Update CustomTableSQL
-- =============================================
CREATE PROCEDURE [dbo].[CustomTableSQL_Update]
	@CustomTableSQLID INT,
	@ClientID int,
	@DetailFieldID int,
	@QueryText varchar,
	@HeaderColumns varchar(2000),
	@SQLQueryID int,
	@RTFBorderOn bit,
	@RTFTwips int,
	@HTMLTableProperties varchar(200),
	@AutoSizeColumns bit

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [CustomTableSQL]
	SET		[ClientID] = @ClientID, 
			[DetailFieldID] = @DetailFieldID, 
			[QueryText] = @QueryText, 
			[HeaderColumns] = @HeaderColumns, 
			[SQLQueryID] = @SQLQueryID,
			[RTFBorderOn] = @RTFBorderOn,
			[RTFTwips] = @RTFTwips,
			[HTMLTableProperties] = @HTMLTableProperties,
			[AutoSizeColumns] = @AutoSizeColumns
	FROM CustomTableSQL
	WHERE CustomTableSQL.CustomTableSQLID = @CustomTableSQLID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQL_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL_Update] TO [sp_executeall]
GO
