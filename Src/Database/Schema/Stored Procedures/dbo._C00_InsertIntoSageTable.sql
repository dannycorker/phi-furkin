SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Dave Morgan
-- Create date: 23-Mar-2011
-- Description:	Inserts row into invoice&credit notes, payments or bad debts table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertIntoSageTable]
(
	@ClientID int,
	@LeadID int,
	@MatterID int,
	@TablePageID int,
	@TableLeadDFID int,
	@InvoiceNumber varchar(30),
	@Date varchar(30),
	@CaseNumber varchar(30),
	@Details varchar(500) = null,
	@NominalCode varchar(30) = null,
	@BankCode varchar(30) = null,
	@TaxCode varchar(30) = null,
	@InvoiceNetAmount varchar(30) = null,
	@InvoiceTaxAmount varchar(30) = null,
	@Type varchar(30) = null,
	@CreditNoteNetAmount varchar(30) = null,
	@CreditNoteTaxAmount varchar(30) = null,
	@Amount varchar(30) = null,
	@SentToSage varchar(5) = 'False'
)

AS
BEGIN
	SET NOCOUNT ON;

DECLARE		@DatePosted varchar(50) = '', -- dcm 12/9/11 changed from convert(char(10), dbo.fn_GetDate_Local(), 126), -- This is YYYY-MM-DD format
			@TableRowID int
			
			DECLARE @SageColumns TABLE 
			(
				ColumnHeader varchar(50), 
				ColumnValue varchar(500)
			)

			INSERT INTO @SageColumns (ColumnHeader,ColumnValue) VALUES
				('Invoice Number',@InvoiceNumber),
				('Date',@Date),
				('Date Posted',@DatePosted),
				('Details',@Details),
				('Nominal Code',@NominalCode),
				('Bank Code',@BankCode),
				('Tax Code',@TaxCode),
				('Type',@Type),
				('Invoice Net Amount',@InvoiceNetAmount),
				('Invoice Tax Amount',@InvoiceTaxAmount),
				('Credit Note Net Amount',@CreditNoteNetAmount),
				('Credit Note Tax Amount',@CreditNoteTaxAmount),
				('Case ID',@CaseNumber),
				('Sent To Sage',@SentToSage),
				('Sage Unique Ref',''),
				('Amount',@Amount),
				('Matter ID',CONVERT(varchar(30),@MatterID))

			-- Insert the table row
			INSERT INTO TableRows(ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit) 
			VALUES (@ClientID, @LeadID, @MatterID, @TableLeadDFID, @TablePageID, 'true')

			SELECT @TableRowID = SCOPE_IDENTITY()

			INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
			SELECT @TableRowID, NULL, df.DetailFieldID, sc.ColumnValue, @LeadID, @MatterID, @ClientID 
			FROM @SageColumns sc
			INNER JOIN DetailFields df WITH (NOLOCK) ON  df.FieldName=sc.ColumnHeader and df.DetailFieldPageID=@TablePageID 

			return @TableRowID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertIntoSageTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertIntoSageTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertIntoSageTable] TO [sp_executeall]
GO
