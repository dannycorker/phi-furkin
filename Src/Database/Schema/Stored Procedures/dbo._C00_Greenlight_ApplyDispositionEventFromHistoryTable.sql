SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-06-30
-- Description:	Apply a disposition event from a greenlight call history table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Greenlight_ApplyDispositionEventFromHistoryTable]
	 @TableRowID		INT
	,@ClientPersonnelID	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE	 @TableDetailFieldID	INT
			,@ClientID				INT
			,@EventToApply			INT
			,@CaseID				INT
			,@Disposition			VARCHAR(2000)
			,@StatusID				INT
			,@LeadTypeID			INT
			,@AutomatedEventQueueID	INT = -1

	SELECT @ClientID = tr.ClientID
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.TableRowID = @TableRowID
	
	SELECT	 @Disposition		= tdv_disp.DetailValue
			,@CaseID			= tdv_case.ValueInt
			,@StatusID			= c.ClientStatusID
			,@LeadTypeID		= l.LeadTypeID
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_disp WITH (NOLOCK) on tdv_disp.TableRowID = tr.TableRowID and tdv_disp.DetailFieldID = 177335 /*Outcome of Call*/
	INNER JOIN TableDetailValues tdv_case WITH (NOLOCK) on tdv_case.TableRowID = tr.TableRowID and tdv_case.DetailFieldID = 177358 /*CaseID*/
	INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = tdv_case.ValueInt
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = c.LeadID	
	WHERE tr.TableRowID = @TableRowID
	
	SELECT @TableDetailFieldID = 177357 /*Greenlight Config*/
	
	-- GL Disposition, LeadTypeID, EventTypeID, ApplyIfThisStatus, BF Notes
	DECLARE @DispositionTable TABLE ( TableRowID INT, Disposition VARCHAR(2000), LeadTypeID INT, EventTypeID INT, ApplyIfThisStatus INT, Notes VARCHAR(2000) )
	
	/*Pivot the config table*/
	INSERT @DispositionTable ( TableRowID, Disposition, LeadTypeID, EventTypeID, ApplyIfThisStatus, Notes )
	SELECT TableRowID, [Disposition],[LeadTypeID],[EventTypeID],[ApplyIfThisStatus],[Notes]
	FROM		
	    (	
		SELECT tr.TableRowID, df.FieldCaption, tdv.DetailValue
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
		WHERE tr.DetailFieldID = 177357--@TableDetailFieldID
		AND df.FieldCaption IN('Disposition','LeadTypeID','EventTypeID','ApplyIfThisStatus','Notes')
		)
	    AS ToPivot		
	PIVOT		
	(		
	    Max([DetailValue])
	FOR		
	[FieldCaption]
	    IN ( [Disposition],[LeadTypeID],[EventTypeID],[ApplyIfThisStatus],[Notes] )
	) AS Pivoted
	
	/*Pick the first appropriate row for this status, lead type and outcome*/
	SELECT TOP(1)
		@EventToApply = CASE WHEN dt.EventTypeID > 0 THEN dt.EventTypeID ELSE -1 END 
	FROM @DispositionTable dt 
	WHERE ( dt.Disposition = @Disposition or ISNULL(dt.Disposition,'') = '')
	AND	dt.LeadTypeID = @LeadTypeID
	AND ( dt.ApplyIfThisStatus = @StatusID or ISNULL(dt.ApplyIfThisStatus,0) = 0 )
	ORDER BY dt.Disposition DESC, dt.ApplyIfThisStatus DESC
	
	INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
	SELECT TOP(1)
		m.ClientID, m.CustomerID, m.LeadID, m.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @ClientPersonnelID, @EventToApply, @ClientPersonnelID, CASE WHEN et.InProcess = 0 THEN -1 ELSE 0 END, 5, 0, 0
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID 
	INNER JOIN LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadEventID = c.LatestNonNoteLeadEventID
	CROSS JOIN EventType et WITH (NOLOCK) 
	WHERE m.CaseID = @CaseID
	AND et.EventTypeID = @EventToApply
	
	SELECT @AutomatedEventQueueID = SCOPE_IDENTITY()
	
	/*Update the call history table*/
	EXEC dbo._C00_SimpleValueIntoField 177391, @AutomatedEventQueueID, @TableRowID /*AutomatedEventQueueID*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Greenlight_ApplyDispositionEventFromHistoryTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Greenlight_ApplyDispositionEventFromHistoryTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Greenlight_ApplyDispositionEventFromHistoryTable] TO [sp_executeall]
GO
