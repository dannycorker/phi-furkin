SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_Insert]
(

	@DocumentTargetControlID int    OUTPUT,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@LastParsed datetime   
)
AS


				
				INSERT INTO [dbo].[DocumentTargetControl]
					(
					[ClientID]
					,[DocumentTypeID]
					,[LastParsed]
					)
				VALUES
					(
					@ClientID
					,@DocumentTypeID
					,@LastParsed
					)
				-- Get the identity value
				SET @DocumentTargetControlID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Insert] TO [sp_executeall]
GO
