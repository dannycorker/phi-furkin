SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIFieldGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIFieldGroupID int   = null ,

	@GroupName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIFieldGroupID]
	, [GroupName]
    FROM
	[dbo].[RPIFieldGroup] WITH (NOLOCK) 
    WHERE 
	 ([RPIFieldGroupID] = @RPIFieldGroupID OR @RPIFieldGroupID IS NULL)
	AND ([GroupName] = @GroupName OR @GroupName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIFieldGroupID]
	, [GroupName]
    FROM
	[dbo].[RPIFieldGroup] WITH (NOLOCK) 
    WHERE 
	 ([RPIFieldGroupID] = @RPIFieldGroupID AND @RPIFieldGroupID is not null)
	OR ([GroupName] = @GroupName AND @GroupName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Find] TO [sp_executeall]
GO
