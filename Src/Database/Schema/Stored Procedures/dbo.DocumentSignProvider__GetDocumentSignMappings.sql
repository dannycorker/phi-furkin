SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- Modified By PR on 16/03/2016 Added DeclinedOrVoidedEventTypeID
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetDocumentSignMappings]
	@ClientID INT,
	@LeadTypeID INT,
	@Enabled BIT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	dspm.DSPMappingID, dspm.DSPTypeID, dspm.DSPAccountID, dspm.DSPUserID, dspm.ClientID, dspm.EmailOutETID, dspm.OutDocumentTypeID, dspm.DocInEventTypeID, dspm.BrandingKey, dspm.[Enabled],
			dspt.DSPTypeName +' ('+ dspu.UserName +')' DSPDetail, 
			etOut.EventTypeName EventOutDetail, 
			etIn.EventTypeName EventInDetail, etDeclined.EventTypeName DeclinedOrVoidedEventDetail
	FROM	DocumentSignProviderMapping dspm WITH (NOLOCK)
	INNER JOIN DocumentSignProviderType dspt WITH (NOLOCK)  ON dspt.DSPTypeID = dspm.DSPTypeID
	INNER JOIN DocumentSignProviderAccount dspa WITH (NOLOCK)  ON dspa.DSPAccountID = dspm.DSPAccountID
	INNER JOIN DocumentSignProviderUser dspu WITH (NOLOCK)  ON dspu.DSPUserID = dspm.DSPUserID
	INNER JOIN EventType etOut WITH (NOLOCK) ON etOut.EventTypeID = dspm.EmailOutETID
	INNER JOIN EventType etIn WITH (NOLOCK) ON etIn.EventTypeID = dspm.DocInEventTypeID
	LEFT JOIN EventType etDeclined WITH (NOLOCK) On etDeclined.EventTypeID = dspm.DeclinedOrVoidedEventTypeID
	WHERE	dspm.ClientID = @ClientID
	AND		dspm.[Enabled] = @Enabled
	AND		dspa.IntegrationKey IS NOT NULL
	AND		dspu.UserName IS NOT NULL
	AND		dspt.DSPTypeName  IS NOT NULL
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetDocumentSignMappings] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetDocumentSignMappings] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetDocumentSignMappings] TO [sp_executeall]
GO
