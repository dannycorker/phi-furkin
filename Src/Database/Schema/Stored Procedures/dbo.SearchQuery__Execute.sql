SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
	Author : Ian Slack
	Dated : 2012-03-19
	Executes a search query and returns customer data and related
	detail values based on detail fields in search query
*/
CREATE PROCEDURE [dbo].[SearchQuery__Execute]
(
	@ClientID int,
	@SubClientID int,
	@ClientPersonnelAdminGroupID int,
	@ClientPersonnelID int,
	@SearchQueryID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY
		
	DECLARE
		@SelectColumns varchar(MAX) = '',
		@PivotColumns varchar(MAX) = '',
		@WhereClause  varchar(MAX) = '',
		@CustomerID INT = 0

	SELECT	@CustomerID = CustomerID
	FROM	ClientPersonnel WITH (NOLOCK) 
	WHERE	ClientID = @ClientID
	AND		SubClientID = @SubClientID
	AND		ClientPersonnelID = @ClientPersonnelID
	AND		ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	AND		ThirdPartySystemId = 11
	
	IF (@ClientPersonnelAdminGroupID = 273) 
	BEGIN 
		SELECT @CustomerID = NULL
	END
	
	CREATE TABLE #SearchQueryValue ([DetailFieldID] [int] NOT NULL,[SearchOperatorID] [int] NOT NULL,[FieldValue] varchar(2000) NULL,[FieldValueID] [int] NULL,[LookupListID] [int] NULL, [SearchFieldName] varchar(50) NOT NULL, [ObjectTypeID] [int] NULL)
	CREATE TABLE #UserCustomerIDs ([CustomerID] [int] NOT NULL,[FullName] varchar(50) NULL)

	INSERT INTO #UserCustomerIDs
	SELECT CustomerID, Fullname FROM dbo.fn_C188_hierarchy(@SubClientID,@CustomerID)

	INSERT INTO #SearchQueryValue
	SELECT	sf.DetailFieldID,
			so.SearchOperatorID,
			REPLACE(sqv.FieldValue, '''', ''''''),
			sqv.FieldValueID,
			df.LookupListID,
			sf.SearchFieldName,
			df.ObjectTypeID
	FROM	SearchQuery sq WITH (NOLOCK) 
	INNER JOIN SearchQueryValue sqv WITH (NOLOCK) ON sq.SearchQueryID = sqv.SearchQueryID
	INNER JOIN SearchField sf WITH (NOLOCK) ON sqv.SearchFieldID = sf.SearchFieldID
	INNER JOIN SearchFieldOperator sfo WITH (NOLOCK) ON sf.SearchFieldID = sfo.SearchFieldID AND sfo.SearchFieldOperatorID = sqv.SearchFieldOperatorID
	INNER JOIN SearchOperator so WITH (NOLOCK) ON sfo.SearchOperatorID = so.SearchOperatorID
	LEFT JOIN DetailFields df WITH (NOLOCK) ON sf.DetailFieldID = df.DetailFieldID
	WHERE	sq.ClientID = @ClientID
	AND		sq.SubClientID = @SubClientID
	AND		sq.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	AND		sq.ClientPersonnelID = @ClientPersonnelID
	AND		sq.SearchQueryID = @SearchQueryID

	SELECT	@SelectColumns += '['+ CAST(DetailFieldID AS VARCHAR(20)) +'] ['+ SearchFieldName +'],',
			@PivotColumns += '['+ CAST(DetailFieldID AS VARCHAR(20)) +'],'
	FROM	#SearchQueryValue
	GROUP BY DetailFieldID, SearchFieldName
	
	SELECT	@WhereClause += '['+ CAST(DetailFieldID AS VARCHAR(20)) +']' +
			CASE SearchOperatorID
				WHEN 1 THEN '= ''' + FieldValue + ''' AND '
				WHEN 2 THEN '<>''' + FieldValue + ''' AND '
				WHEN 3 THEN 'LIKE ''%' + FieldValue + '%'' AND '
				WHEN 4 THEN 'NOT LIKE ''%' + FieldValue + '%'' AND '
				WHEN 5 THEN '>= ''' + FieldValue + ''' AND '
				WHEN 6 THEN '<= ''' + FieldValue + ''' AND '
			END 
	FROM	#SearchQueryValue
	
	SELECT	@SelectColumns = LEFT(@SelectColumns, LEN(@SelectColumns)-1),
			@PivotColumns = LEFT(@PivotColumns, LEN(@PivotColumns)-1)

	DECLARE	@SqlQuery NVARCHAR(MAX) = 
	N'SELECT [CustomerID] [Employee ID], [EmployeeName] [Employee Name], '+@SelectColumns+'
	FROM	
	(
		SELECT	uc.CustomerID, uc.FullName EmployeeName, cdv.DetailFieldID, ISNULL(ISNULL(ISNULL(o.Name, lli.ItemValue), cdv.DetailValue), '''') DetailValue
		FROM	#UserCustomerIDs uc
		INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.CustomerID = uc.CustomerID
		INNER JOIN #SearchQueryValue sdv ON sdv.DetailFieldID = cdv.DetailFieldID
		LEFT JOIN LookupListItems lli WITH (NOLOCK) on cdv.ValueInt = lli.LookupListItemID AND sdv.LookupListID = lli.LookupListID
		LEFT JOIN Objects o WITH (NOLOCK) on o.ObjectTypeID = sdv.ObjectTypeID AND o.ObjectID = cdv.ValueInt
		UNION ALL
		SELECT	uc.CustomerID, uc.FullName EmployeeName, ldv.DetailFieldID, ISNULL(ISNULL(lli.ItemValue, ldv.DetailValue),'''') DetailValue
		FROM	#UserCustomerIDs uc
		INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = uc.CustomerID
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID
		INNER JOIN #SearchQueryValue sdv ON sdv.DetailFieldID = ldv.DetailFieldID
		LEFT JOIN LookupListItems lli WITH (NOLOCK) on ldv.ValueInt = lli.LookupListItemID AND sdv.LookupListID = lli.LookupListID
		UNION ALL
		SELECT	c.CustomerID, uc.FullName EmployeeName, c.DetailFieldID, ISNULL(c.DetailValue, '''') DetailValue 
		FROM	#UserCustomerIDs uc
		INNER JOIN [dbo].[fnUnpivotCustomersWithDetailFields] (@ClientID,@SubClientID) c ON uc.CustomerID = c.CustomerID
		UNION ALL
		SELECT	uc.CustomerID, uc.FullName EmployeeName, tdv.DetailFieldID, ISNULL(ISNULL(lli.ItemValue, tdv.DetailValue),'''') DetailValue
		FROM	#UserCustomerIDs uc
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.CustomerID = uc.CustomerID
		INNER JOIN #SearchQueryValue sdv ON sdv.DetailFieldID = tdv.DetailFieldID
		LEFT JOIN LookupListItems lli WITH (NOLOCK) on tdv.ValueInt = lli.LookupListItemID AND sdv.LookupListID = lli.LookupListID
	) t 
	PIVOT 
	(
		MAX(DetailValue) 
		FOR [DetailFieldID] IN 
		(
			'+@PivotColumns+'
		)
	) p
	WHERE '+ @WhereClause +' 1=1'
	
	SELECT	@SelectColumns FieldMappings
	
	exec sp_executesql @SqlQuery,
	N'@ClientID int, @SubClientID int',
	@ClientID, @SubClientID

	DROP TABLE #SearchQueryValue, #UserCustomerIDs
		
	END TRY
	BEGIN CATCH
		DROP TABLE #SearchQueryValue, #UserCustomerIDs
	END CATCH

END



GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery__Execute] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery__Execute] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery__Execute] TO [sp_executeall]
GO
