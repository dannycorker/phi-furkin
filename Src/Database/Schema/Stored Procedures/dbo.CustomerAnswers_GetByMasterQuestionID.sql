SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerAnswers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_GetByMasterQuestionID]
(

	@MasterQuestionID int   
)
AS


				SELECT
					[CustomerAnswerID],
					[CustomerQuestionnaireID],
					[MasterQuestionID],
					[Answer],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[CustomerAnswers] WITH (NOLOCK) 
				WHERE
										[MasterQuestionID] = @MasterQuestionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByMasterQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_GetByMasterQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByMasterQuestionID] TO [sp_executeall]
GO
