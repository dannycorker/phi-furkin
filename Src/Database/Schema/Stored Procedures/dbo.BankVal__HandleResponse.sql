SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-10-24
-- Description:	Handle BankVal Response
-- AquariumEventSubType that handled
-- 37	BankVal Validate a sort code and account number bankValPlus2(AccountNumber, Sortcode, UserID, PIN, Format)
-- 38	BankVal Lookup a sortcode getBranchDetails2(Sortcode, UserID, PIN, Format)
-- 39	BankVal Get the IBAN and SWIFT Code for a UK bank account deriveIbanBic(Sortcode, AccountNumber, UserID, PIN, Format)
-- 40	BankVal Validate Building Society Roll Number refValidate(Sortcode, AccountNumber, BuildingSocietyRollNumber, UserID, PIN, Format)
-- 41	BankVal Lookup SWIFT bank details getBankDetails2(SwiftBIC, UserID, PIN, Format)
-- 42	BankVal Validate an IBAN ibanValidate(IBAN, UserID, PIN, Format)
-- 43	BankVal Lookup ABA bank details getAbaDetails(ABA, UserID, PIN, Format)
-- 44	BankVal Get addresses in a postcode addressList(Postcode, UserID, PIN, Format)
-- 45	BankVal Validate an address addressCheck(Postcode, Premise, UserID, PIN, Format)
-- MODIFIED IS 2015-04-02: Ticket:32195 reassign aquarium event subtype id
-- =============================================
CREATE PROCEDURE [dbo].[BankVal__HandleResponse]
	@EventTypeID INT,
	@AquariumEventSubtypeID INT,
	@ServiceMethod VARCHAR(100) = NULL,
	@ClientID INT,
	@LeadID INT,
	@CaseID INT,
	@ClientPersonnelID INT,
	@Format VARCHAR(50),
	@Response VARCHAR(MAX),
	@HandleInDatabase BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Result VARCHAR(500),
			@Xml XML
	

	-- DO BILLING BASED ON @ClientPersonnelID if response has no errors	
	EXEC dbo.ClientBillingDetail__Insert @ClientID = @ClientID, @SourceAquariumOptionID = 85, @ModuleID = 1, @WhenCreated = NULL, @WhoCreated = @ClientPersonnelID, @ChargeInPence = 2, @Notes = 'BankVal__HandleResponse' 	
	
	IF @HandleInDatabase = 1
	BEGIN
	
		IF @AquariumEventSubtypeID = 37
		BEGIN
			
			SELECT @XML = @Response
			SELECT @Result = n.c.value('(result)[1]', 'varchar(2000)')
			FROM @Xml.nodes('//bankvalplus') n(c)
			
			IF @Result='VALID' 
			BEGIN
				SELECT @Result=''
			END
			ELSE IF @Result='INVALID - Modulus Check Failed'
			BEGIN
				SELECT @Result='Bank validation failed: Account number is not valid for the sort code'
			END
			ELSE
			BEGIN
				SELECT @Result='Bank validation failed: ' + @Result
			END

			SELECT 
				@Result Error, 
				'' Warning
		END
		ELSE
		BEGIN
			SELECT 
				'Handle response has no available' Error, 
				'Handle response has no available' Warning
		END
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[BankVal__HandleResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankVal__HandleResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankVal__HandleResponse] TO [sp_executeall]
GO
