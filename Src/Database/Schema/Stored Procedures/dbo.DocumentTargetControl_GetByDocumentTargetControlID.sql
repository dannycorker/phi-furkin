SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentTargetControl table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_GetByDocumentTargetControlID]
(

	@DocumentTargetControlID int   
)
AS


				SELECT
					[DocumentTargetControlID],
					[ClientID],
					[DocumentTypeID],
					[LastParsed]
				FROM
					[dbo].[DocumentTargetControl] WITH (NOLOCK) 
				WHERE
										[DocumentTargetControlID] = @DocumentTargetControlID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_GetByDocumentTargetControlID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_GetByDocumentTargetControlID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_GetByDocumentTargetControlID] TO [sp_executeall]
GO
