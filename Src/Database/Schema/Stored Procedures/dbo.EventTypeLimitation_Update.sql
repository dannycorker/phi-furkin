SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeLimitation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation_Update]
(

	@EventTypeLimitationID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@EventTypeIDFrom int   ,

	@EventTypeIDTo int   ,

	@LimitationDays int   ,

	@Description varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeLimitation]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[EventTypeIDFrom] = @EventTypeIDFrom
					,[EventTypeIDTo] = @EventTypeIDTo
					,[LimitationDays] = @LimitationDays
					,[Description] = @Description
				WHERE
[EventTypeLimitationID] = @EventTypeLimitationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Update] TO [sp_executeall]
GO
