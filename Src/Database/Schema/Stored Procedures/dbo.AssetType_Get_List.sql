SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AssetType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_Get_List]

AS


				
				SELECT
					[AssetTypeID],
					[AssetSubTypeID],
					[AssetTypeName],
					[Note],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[AssetType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Get_List] TO [sp_executeall]
GO
