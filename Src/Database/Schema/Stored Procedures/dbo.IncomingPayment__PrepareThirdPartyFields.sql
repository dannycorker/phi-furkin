SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2016-12-04
-- Description:	Populate payments third party fields for a Policy Admin matter
-- 2019-12-04 CPS for JIRA LPC-181 | Raise error if field mapping is missing
-- 2019-12-18 CPS for JIRA LPC-181 | Allow passing the LeadEventID so we can write comments
-- =============================================
CREATE PROCEDURE [dbo].[IncomingPayment__PrepareThirdPartyFields]
	 @PolicyAdminMatterID				INT
	,@RunAsUserID						INT
	,@PaymentTypeID						VARCHAR(2000)
	,@PaymentRef						VARCHAR(2000)
	,@PaymentDescription				VARCHAR(2000)
	,@PaymentNet						VARCHAR(2000)
	,@PaymentTax						VARCHAR(2000)
	,@PaymentGross						VARCHAR(2000)
	,@PaymentScheduleID					VARCHAR(2000)
	,@PaymentStatusID					VARCHAR(2000)
	,@PaymentDateTime					VARCHAR(2000)
	,@PaymentDateReceived				VARCHAR(2000)
	,@PaymentRelatedOrderRef			VARCHAR(2000)
	,@PaymentRelatedRequestDescription	VARCHAR(2000)
	,@PaymentPayeeFullName				VARCHAR(2000)
	,@PaymentMaskedAccountNumber		VARCHAR(2000)
	,@PaymentAccountTypeDescription		VARCHAR(2000)
	,@PaymentDateReconciled				VARCHAR(2000)
	,@PaymentReconciliationOutcome		VARCHAR(2000)
	,@PaymentComments					VARCHAR(2000)
	,@PaymentPaymentFileName			VARCHAR(2000)
	,@PaymentFailureCode				VARCHAR(2000)
	,@PaymentFailureReason				VARCHAR(2000)
	,@LeadEventID						INT = NULL
AS
BEGIN
PRINT 'IncomingPayment__PrepareThirdPartyFields'
	SET NOCOUNT ON;
	
	DECLARE  @ClientID			INT
			,@FieldList			VARCHAR(2000) = ''
			,@LeadID			INT
			,@LeadTypeID		INT
			,@ObjectID			VARCHAR(2000) = CONVERT(VARCHAR,@PolicyAdminMatterID)
			,@ObjectTypeID		VARCHAR(2000) = '2'
			,@PaymentCurrencyID	VARCHAR(2000) = '1'
			,@RowCount			INT
			,@ErrorMessage		VARCHAR(2000)
			,@EventComments		VARCHAR(2000) = ''
			
	DECLARE  @DetailValueData  dbo.tvpDetailValueData
			,@ThirdPartyFields dbo.tvpInt
	
	SELECT @ClientID = m.ClientID, @LeadID = m.LeadID, @LeadTypeID = l.LeadTypeID
	FROM Matter m WITH ( NOLOCK )
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
	WHERE m.MatterID = @PolicyAdminMatterID
	
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No Matter matched for ID ' + ISNULL(CONVERT(VARCHAR,@PolicyAdminMatterID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END

	/*Find the relevant ThirdPartyFields*/
	INSERT @ThirdPartyFields ( AnyID )
	SELECT pf.ThirdPartyFieldID
	FROM ThirdPartyField pf WITH ( NOLOCK )
	WHERE pf.ThirdPartyFieldID IN (4370,4373,4372,4374,4375,4376,4396,4386,4387,4383,4369,4371,4377,4378,4379,4380,4381,4382,4384,4385,4388,4389,4390,4391)

	/*Compile a comma-separated list of detial fields*/
	SELECT @FieldList += CONVERT(VARCHAR,fm.DetailFieldID) + ','
	FROM ThirdPartyFieldMapping fm WITH ( NOLOCK )
	INNER JOIN @ThirdPartyFields tpf on tpf.AnyID = fm.ThirdPartyFieldID
	WHERE fm.LeadTypeID = @LeadTypeID

	IF @@RowCount = 0
	BEGIN
		SELECT @ErrorMessage = '<br><br><font color="red">Error: No Mapped Third Party Fields identified for this LeadTypeID ' + CONVERT(VARCHAR,@LeadTypeID) + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END

	/*Trim the trailing comma*/
	SELECT @FieldList = LEFT(@FieldList,LEN(@FieldList)-1)
	
	/*Make sure we have MatterDetailValues records*/
	EXEC _C00_CreateDetailFields @FieldList, @LeadID, 0

	/*Compile a tvp of detail value data to save.  If the incoming variable is NULL, use the existing value*/
	INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
	SELECT @ClientID, fm.DetailFieldID, 2, ISNULL(mdv.MatterDetailValueID,-1), @PolicyAdminMatterID,
		CASE fm.ThirdPartyFieldID 
			WHEN 4370 THEN COALESCE(@PaymentTypeID						,mdv.DetailValue,'')
			WHEN 4373 THEN COALESCE(@PaymentRef							,mdv.DetailValue,'')
			WHEN 4372 THEN COALESCE(@PaymentDescription					,mdv.DetailValue,'')
			WHEN 4374 THEN COALESCE(@PaymentNet							,mdv.DetailValue,'')
			WHEN 4375 THEN COALESCE(@PaymentTax							,mdv.DetailValue,'')
			WHEN 4376 THEN COALESCE(@PaymentGross						,mdv.DetailValue,'')
			WHEN 4396 THEN COALESCE(@PaymentScheduleID					,mdv.DetailValue,'')
			WHEN 4386 THEN COALESCE(@ObjectID							,mdv.DetailValue,'')
			WHEN 4387 THEN COALESCE(@ObjectTypeID						,mdv.DetailValue,'')
			WHEN 4383 THEN COALESCE(@PaymentStatusID					,mdv.DetailValue,'')
			WHEN 4369 THEN COALESCE(@PaymentDateTime					,mdv.DetailValue,'')
			WHEN 4371 THEN COALESCE(@PaymentDateReceived				,mdv.DetailValue,'')
			WHEN 4377 THEN COALESCE(@PaymentCurrencyID					,mdv.DetailValue,'')
			WHEN 4378 THEN COALESCE(@PaymentRelatedOrderRef				,mdv.DetailValue,'')
			WHEN 4379 THEN COALESCE(@PaymentRelatedRequestDescription	,mdv.DetailValue,'')
			WHEN 4380 THEN COALESCE(@PaymentPayeeFullName				,mdv.DetailValue,'')
			WHEN 4381 THEN COALESCE(@PaymentMaskedAccountNumber			,mdv.DetailValue,'')
			WHEN 4382 THEN COALESCE(@PaymentAccountTypeDescription		,mdv.DetailValue,'')
			WHEN 4384 THEN COALESCE(@PaymentDateReconciled				,mdv.DetailValue,'')
			WHEN 4385 THEN COALESCE(@PaymentReconciliationOutcome		,mdv.DetailValue,'')
			WHEN 4388 THEN COALESCE(@PaymentComments					,mdv.DetailValue,'')
			WHEN 4389 THEN COALESCE(@PaymentPaymentFileName				,mdv.DetailValue,'')
			WHEN 4390 THEN COALESCE(@PaymentFailureCode					,mdv.DetailValue,'')
			WHEN 4391 THEN COALESCE(@PaymentFailureReason				,mdv.DetailValue,'')			
			ELSE ''
		END 
	FROM ThirdPartyFieldMapping fm WITH ( NOLOCK )
	INNER JOIN @ThirdPartyFields tpf on tpf.AnyID = fm.ThirdPartyFieldID
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = @PolicyAdminMatterID AND mdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.LeadTypeID = @LeadTypeID

	/*!!!!!!!! NO SQL HERE !!!!!!!!*/	
	
	SELECT @RowCount = @@ROWCOUNT

	/*Save the detial values, storing history*/
	IF @RowCount > 0
	BEGIN
		SELECT @EventComments += CONVERT(VARCHAR,@RowCount) + ' IncomingPayment fields updated for MatterID ' + ISNULL(CONVERT(VARCHAR,@PolicyAdminMatterID),'NULL') + CHAR(13)+CHAR(10)
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID
	END

	IF @LeadEventID <> 0 AND @EventComments <> ''
	BEGIN
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

	RETURN @RowCount

END
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPayment__PrepareThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPayment__PrepareThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPayment__PrepareThirdPartyFields] TO [sp_executeall]
GO
