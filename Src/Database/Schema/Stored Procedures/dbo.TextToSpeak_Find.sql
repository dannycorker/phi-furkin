SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the TextToSpeak table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TextToSpeak_Find]
(

	@SearchUsingOR bit   = null ,

	@TextToSpeakID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@ClientID int   = null ,

	@PageNumber int   = null ,

	@SpeakText varchar (512)  = null ,

	@IsShown bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TextToSpeakID]
	, [ClientQuestionnaireID]
	, [ClientID]
	, [PageNumber]
	, [SpeakText]
	, [IsShown]
    FROM
	[dbo].[TextToSpeak] WITH (NOLOCK) 
    WHERE 
	 ([TextToSpeakID] = @TextToSpeakID OR @TextToSpeakID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PageNumber] = @PageNumber OR @PageNumber IS NULL)
	AND ([SpeakText] = @SpeakText OR @SpeakText IS NULL)
	AND ([IsShown] = @IsShown OR @IsShown IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TextToSpeakID]
	, [ClientQuestionnaireID]
	, [ClientID]
	, [PageNumber]
	, [SpeakText]
	, [IsShown]
    FROM
	[dbo].[TextToSpeak] WITH (NOLOCK) 
    WHERE 
	 ([TextToSpeakID] = @TextToSpeakID AND @TextToSpeakID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PageNumber] = @PageNumber AND @PageNumber is not null)
	OR ([SpeakText] = @SpeakText AND @SpeakText is not null)
	OR ([IsShown] = @IsShown AND @IsShown is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TextToSpeak_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Find] TO [sp_executeall]
GO
