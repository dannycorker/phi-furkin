SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DashboardItemType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_GetByDashboardItemTypeID]
(

	@DashboardItemTypeID int   
)
AS


				SELECT
					[DashboardItemTypeID],
					[DashboardItemTypeName],
					[ClientID]
				FROM
					[dbo].[DashboardItemType] WITH (NOLOCK) 
				WHERE
										[DashboardItemTypeID] = @DashboardItemTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_GetByDashboardItemTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_GetByDashboardItemTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_GetByDashboardItemTypeID] TO [sp_executeall]
GO
