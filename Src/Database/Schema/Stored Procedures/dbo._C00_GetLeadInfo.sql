SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-01-09
-- Description:	Get New Lead Info Details
-- ACE 2015-03-03 Fixed LeadOrMatter=2 (Should have been 1)
-- CS  2016-03-17 Allowed an override report for hyperlinks
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetLeadInfo] 
	@LeadID INT,
	@CaseID INT = NULL,
	@ClientPersonnelID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @ClientID						INT
			,@CustomerID					INT
			,@Message						VARCHAR(2000) = ''
			,@LeadTypeID					INT
			,@CaseDisplayName				VARCHAR(100) = 'Case'
			,@HyperlinkReportID				INT
			,@QueryText						NVARCHAR(MAX)
			,@ClientPersonnelAdminGroupID	INT
				
	SELECT @ClientID = l.ClientID, @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID
	FROM Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID

	SELECT @CaseDisplayName = lt.CaseDisplayName /*CS 2015-06-01 Use this LeadType's case alias*/
	FROM LeadType lt WITH ( NOLOCK )
	WHERE lt.LeadTypeID = @LeadTypeID 
	AND lt.CaseDisplayName <> ''
	
	/*Pick up the admin group of this user*/
	SELECT @ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID
	FROM ClientPersonnel cp WITH ( NOLOCK ) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID
	

	SELECT @Message = 'Run procedure _C00_GetLeadInfo with params LeadID ' + CONVERT(VARCHAR,@LeadID) + ' @CaseID ' + CONVERT(VARCHAR,@CaseID) + ' ClientPersonnelID ' + CONVERT(VARCHAR,@ClientPersonnelID)
	
	EXEC dbo._C00_LogIt 'Proc', 'Custom Part', '_C00_GetLeadInfo', @Message, @ClientPersonnelID

	; WITH Data AS (
		/*Get lead info to show on the lead info box*/
		SELECT df.FieldCaption, CASE df.Lookup WHEN 1 THEN dbo.fnGetSimpleDvLuli(df.DetailFieldID, CASE df.LeadOrMatter WHEN 1 THEN @LeadID WHEN 10 THEN @CustomerID ELSE NULL END) ELSE dbo.fnGetSimpleDv(df.DetailFieldID, CASE df.LeadOrMatter WHEN 1 THEN @LeadID WHEN 10 THEN @CustomerID ELSE NULL END) END AS [Value], df.FieldOrder
		FROM ThirdPartyFieldMapping t WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
		INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID
		WHERE t.ThirdPartyFieldGroupID = 86
		AND t.ClientID = @ClientID
		AND t.ColumnFieldID IS NULL
		AND t.LeadTypeID = @LeadTypeID

		UNION

		SELECT df.FieldCaption, rldv.DetailValue AS [Value], df.FieldOrder
		FROM ThirdPartyFieldMapping t WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
		INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID
		INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = t.ColumnFieldID AND rldv.ResourceListID = dbo.fnGetSimpleDvAsInt(df.DetailFieldID, CASE df.LeadOrMatter WHEN 1 THEN @LeadID WHEN 10 THEN @CustomerID ELSE NULL END)
		WHERE t.ThirdPartyFieldGroupID = 86
		AND t.ClientID = @ClientID
		AND t.ColumnFieldID > 0
		AND t.LeadTypeID = @LeadTypeID

	)
	
	SELECT d.FieldCaption, d.Value
	FROM Data d
	ORDER BY d.FieldOrder

	/*Find out whether we're using overrides for the hyperlink section*/
	SELECT TOP(1) @HyperlinkReportID = o.QueryID, @QueryText = sq.QueryText
	FROM Overrides o WITH ( NOLOCK ) 
	INNER JOIN SqlQuery sq WITH ( NOLOCK ) on sq.QueryID = o.QueryID
	WHERE o.ClientID = @ClientID
	AND (o.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID or o.ClientPersonnelAdminGroupID is NULL)
	AND (sq.LeadTypeID = @LeadTypeID or sq.LeadTypeID is NULL)
	AND (o.OverrideTypeID = 6)
	ORDER BY o.ClientPersonnelAdminGroupID DESC, sq.LeadTypeID DESC
	
	IF @HyperlinkReportID > 0
	BEGIN
		PRINT @HyperlinkReportID
		PRINT @QueryText
		EXECUTE sp_executesql 
			@QueryText, 
			N'@ClientID INT,@LeadID INT,@CaseID INT,@ClientPersonnelID INT', 
			@ClientID, @LeadID,@CaseID,@ClientPersonnelID
	END
	ELSE
	BEGIN
		SELECT CASE  
				WHEN et.EventSubtypeID = 19 THEN '../../../CustomersEventDetails.aspx?leid=' + CONVERT(VARCHAR,le.LeadEventID) + '&lid=' + CONVERT(VARCHAR,le.LeadID) + '&cid=' + CONVERT(VARCHAR,@CustomerID)
											ELSE '../../../ShowImage.aspx?LeadDocumentID=' + CONVERT(VARCHAR,le.LeadDocumentID) END AS [Link]
			, 'View ' + ISNULL(ld.LeadDocumentTitle + ' ','') + et.EventTypeName + ' (' + @CaseDisplayName + ' ' + CONVERT(VARCHAR,ca.CaseNum) + ')' AS [Text], 'Created By ' + cp.UserName + ' on ' + CONVERT(VARCHAR(20),le.WhenCreated) AS [Hoverover]
		FROM EventType et WITH (NOLOCK)
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND le.LeadID = @LeadID AND le.LeadDocumentID > 0
		INNER JOIN LeadDocument ld WITH (NOLOCK) ON ld.LeadDocumentID = le.LeadDocumentID
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = le.CaseID
		WHERE et.KeyEvent = 1
		AND le.EventDeleted = 0
		and not exists ( SELECT * /*CS 2015-05-22 Only show the latest event for each case*/
						 FROM LeadEvent sle WITH ( NOLOCK ) 
						 WHERE sle.CaseID = le.CaseID 
						 AND sle.EventTypeID = le.EventTypeID 
						 AND sle.EventDeleted = 0
						 AND sle.WhenCreated > le.WhenCreated )
		ORDER BY le.WhenCreated DESC
	END
	                 
	PRINT '_C00_LeadInfoCustomSql START'
	EXEC dbo._C00_LeadInfoCustomSql @LeadID, @ClientPersonnelID , @CaseID
	PRINT '_C00_LeadInfoCustomSql END'

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetLeadInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetLeadInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetLeadInfo] TO [sp_executeall]
GO
