SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Object:  Stored Procedure dbo.GetOutcomeFromOutcomeID    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[GetOutcomeFromOutcomeID] @OutcomeID int

AS

Select OutcomeID, ClientQuestionnaireID, OutcomeName, OutcomeDescription, SmsWhenFound, MobileNumber, EmailCustomer, CustomersEmail, ThankYouPage, EmailFromAddress, SmsToCustomer, CustomersEmailSubject, ClientID, EmailClient, ClientEmail
from Outcomes
where OutcomeID = @OutcomeID




GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeFromOutcomeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeFromOutcomeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeFromOutcomeID] TO [sp_executeall]
GO
