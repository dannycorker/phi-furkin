SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddQuestionnaireFont    Script Date: 08/09/2006 12:22:41 ******/
CREATE PROCEDURE [dbo].[AddQuestionnaireFont]

@ClientQuestionnaireID int,
@PartNameID int, 
@FontFamily nvarchar(50),
@FontSize nvarchar(50),
@FontColour nvarchar(50),
@FontWeight nvarchar(50),
@FontAlignment nvarchar(50), 
@ClientID int

AS

insert into QuestionnaireFonts (ClientQuestionnaireID, PartNameID, FontFamily, FontSize, FontColour, FontWeight, FontAlignment, ClientID)
values (@ClientQuestionnaireID, @PartNameID, @FontFamily, @FontSize, @FontColour, @FontWeight, @FontAlignment, @ClientID)

declare @QuestionnaireFontID int

set @QuestionnaireFontID = SCOPE_IDENTITY()

Select QuestionnaireFontID from QuestionnaireFonts where QuestionnaireFontID = @QuestionnaireFontID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddQuestionnaireFont] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddQuestionnaireFont] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddQuestionnaireFont] TO [sp_executeall]
GO
