SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-17
-- Description:	AI Dashboard - Dataloader
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard_Dataloader]
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SummaryOnly = 1 
	BEGIN
	
		DECLARE @Data TABLE
		(
			DLFSID VARCHAR(50),
			Status VARCHAR(50),
			FileStatusName VARCHAR(50),
			FileCount INT,
			LastFileScheduledAt DATETIME,
			LastFileDataloadedAt DATETIME
		)
		INSERT INTO @Data (DLFSID, Status, FileStatusName, FileCount, LastFileScheduledAt, LastFileDataloadedAt) EXEC dbo.AIDashboard_Dataloader 0
		
		IF EXISTS (SELECT * FROM @Data WHERE Status = 'Red')
		BEGIN
			SELECT 'Red' AS [Status]
		END
		ELSE IF EXISTS (SELECT * FROM @Data WHERE Status = 'Amber')
		BEGIN
			SELECT 'Amber' AS [Status]
		END
		ELSE
		BEGIN
			SELECT 'Green' AS [Status]
		END 
		
	END
	ELSE
	BEGIN
		
		;WITH LatestFilesByStatus AS 
		(
			SELECT dlf.FileStatusID, 
			dlf.ScheduledDateTime, 
			dlf.DataLoadedDateTime, 
			ROW_NUMBER() OVER(PARTITION BY dlf.FileStatusID ORDER BY COALESCE(DataLoadedDateTime, ScheduledDateTime) DESC) as rn 
			FROM dbo.DataLoaderFile dlf WITH (NOLOCK) 
		),
		CountsByStatus AS 
		(
			SELECT dlf.FileStatusID, 
			COUNT(*) as [FileCount] 
			FROM dbo.DataLoaderFile dlf WITH (NOLOCK)
			GROUP BY dlf.FileStatusID 
		) 
		SELECT 
		dlfs.DataLoaderFileStatusID as [DLFSID], 
		CASE dlfs.DataLoaderFileStatusID 
			WHEN 3 THEN 
				CASE  
				WHEN cbs.FileCount IS NULL THEN 'Green' 
				WHEN cbs.FileCount > 1 THEN 'Red' 
				WHEN DATEDIFF(mm, lfbs.ScheduledDateTime, dbo.fn_GetDate_Local()) > 30 THEN 'Red' 
				ELSE 'Amber' 
				END 
			ELSE 'Green' 
		END as [DL Status], 
		dlfs.FileStatusName, 
		COALESCE(cbs.FileCount, 0) as [FileCount], 
		lfbs.ScheduledDateTime as [Last File Scheduled At], 
		lfbs.DataLoadedDateTime as [Last File Dataloaded At] 
		FROM dbo.DataLoaderFileStatus dlfs WITH (NOLOCK) 
		LEFT JOIN CountsByStatus cbs WITH (NOLOCK) ON cbs.FileStatusID = dlfs.DataLoaderFileStatusID 
		LEFT JOIN LatestFilesByStatus lfbs ON lfbs.FileStatusID = dlfs.DataLoaderFileStatusID AND lfbs.rn = 1 
		ORDER BY dlfs.DataLoaderFileStatusID 
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_Dataloader] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard_Dataloader] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_Dataloader] TO [sp_executeall]
GO
