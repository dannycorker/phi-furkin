SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ResourceList table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceList_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[ResourceListID],
					[ClientID],
					[DetailFieldPageID],
					[ResourceListHelperCode],
					[ResourceListHelperName],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ResourceList] WITH (NOLOCK) 
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_GetBySourceID] TO [sp_executeall]
GO
