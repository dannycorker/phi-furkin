SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ResourceList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceList_Delete]
(

	@ResourceListID int   
)
AS


				DELETE FROM [dbo].[ResourceList] WITH (ROWLOCK) 
				WHERE
					[ResourceListID] = @ResourceListID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Delete] TO [sp_executeall]
GO
