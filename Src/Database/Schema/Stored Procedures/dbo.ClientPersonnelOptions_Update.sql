SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPersonnelOptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_Update]
(

	@ClientPersonnelOptionID int   ,

	@ClientPersonnelID int   ,

	@ClientPersonnelOptionTypeID int   ,

	@OptionValue varchar (255)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPersonnelOptions]
				SET
					[ClientPersonnelID] = @ClientPersonnelID
					,[ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID
					,[OptionValue] = @OptionValue
					,[ClientID] = @ClientID
				WHERE
[ClientPersonnelOptionID] = @ClientPersonnelOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Update] TO [sp_executeall]
GO
