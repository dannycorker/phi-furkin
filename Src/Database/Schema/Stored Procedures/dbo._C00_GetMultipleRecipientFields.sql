SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 12/10/2015
-- Description:	Gets a list of detail fields used on multiple recipient documents
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetMultipleRecipientFields]

	@ClientID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT df.DetailFieldID, df.LeadOrMatter, df.QuestionTypeID, df.FieldName, df.ResourceListDetailFieldPageID, df.TableDetailFieldPageID
	FROM DocumentType dt WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID=dt.MultipleRecipientDataSourceID 
	WHERE dt.SendToMultipleRecipients=1 AND dt.ClientID=@ClientID AND dt.MultipleRecipientDataSourceID IS NOT NULL

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetMultipleRecipientFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetMultipleRecipientFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetMultipleRecipientFields] TO [sp_executeall]
GO
