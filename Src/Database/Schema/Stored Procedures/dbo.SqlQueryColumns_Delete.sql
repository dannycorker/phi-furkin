SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryColumns_Delete]
(

	@SqlQueryColumnID int   
)
AS


				DELETE FROM [dbo].[SqlQueryColumns] WITH (ROWLOCK) 
				WHERE
					[SqlQueryColumnID] = @SqlQueryColumnID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryColumns_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Delete] TO [sp_executeall]
GO
