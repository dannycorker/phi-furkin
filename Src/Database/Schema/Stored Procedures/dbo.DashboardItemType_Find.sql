SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DashboardItemType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_Find]
(

	@SearchUsingOR bit   = null ,

	@DashboardItemTypeID int   = null ,

	@DashboardItemTypeName int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DashboardItemTypeID]
	, [DashboardItemTypeName]
	, [ClientID]
    FROM
	[dbo].[DashboardItemType] WITH (NOLOCK) 
    WHERE 
	 ([DashboardItemTypeID] = @DashboardItemTypeID OR @DashboardItemTypeID IS NULL)
	AND ([DashboardItemTypeName] = @DashboardItemTypeName OR @DashboardItemTypeName IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DashboardItemTypeID]
	, [DashboardItemTypeName]
	, [ClientID]
    FROM
	[dbo].[DashboardItemType] WITH (NOLOCK) 
    WHERE 
	 ([DashboardItemTypeID] = @DashboardItemTypeID AND @DashboardItemTypeID is not null)
	OR ([DashboardItemTypeName] = @DashboardItemTypeName AND @DashboardItemTypeName is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Find] TO [sp_executeall]
GO
