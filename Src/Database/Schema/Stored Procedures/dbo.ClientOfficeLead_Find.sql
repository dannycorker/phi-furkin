SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientOfficeLead table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeLead_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientOfficeLeadID int   = null ,

	@ClientID int   = null ,

	@ClientOfficeID int   = null ,

	@LeadID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientOfficeLeadID]
	, [ClientID]
	, [ClientOfficeID]
	, [LeadID]
    FROM
	[dbo].[ClientOfficeLead] WITH (NOLOCK) 
    WHERE 
	 ([ClientOfficeLeadID] = @ClientOfficeLeadID OR @ClientOfficeLeadID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientOfficeID] = @ClientOfficeID OR @ClientOfficeID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientOfficeLeadID]
	, [ClientID]
	, [ClientOfficeID]
	, [LeadID]
    FROM
	[dbo].[ClientOfficeLead] WITH (NOLOCK) 
    WHERE 
	 ([ClientOfficeLeadID] = @ClientOfficeLeadID AND @ClientOfficeLeadID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientOfficeID] = @ClientOfficeID AND @ClientOfficeID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeLead_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Find] TO [sp_executeall]
GO
