SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditingGrouping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingGrouping_Insert]
(

	@SqlQueryEditingGroupingID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@GroupByClause varchar (2000)  ,

	@HavingClause varchar (500)  ,

	@HavingColumnID int   ,

	@HavingCriteria1 varchar (250)  ,

	@HavingCriteria2 varchar (250)  
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditingGrouping]
					(
					[ClientID]
					,[SqlQueryEditingID]
					,[GroupByClause]
					,[HavingClause]
					,[HavingColumnID]
					,[HavingCriteria1]
					,[HavingCriteria2]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryEditingID
					,@GroupByClause
					,@HavingClause
					,@HavingColumnID
					,@HavingCriteria1
					,@HavingCriteria2
					)
				-- Get the identity value
				SET @SqlQueryEditingGroupingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingGrouping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_Insert] TO [sp_executeall]
GO
