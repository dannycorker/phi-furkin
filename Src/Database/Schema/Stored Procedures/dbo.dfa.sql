SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-02
-- Description:	List all DetailFieldAliases for a client and leadtype
-- =============================================
CREATE PROCEDURE [dbo].[dfa] 
	@ClientID int, 
	@LeadTypeID int = NULL, 
	@ShowLinks bit = 0,
	@LeadOrMatter int = NULL,
	@QuestionTypeID int = NULL  
AS
BEGIN
	SET NOCOUNT ON;

	IF @ShowLinks = 0
	BEGIN
		SELECT * 
		FROM dbo.DetailFieldAlias dfa (nolock) 
		WHERE (dfa.ClientID = @ClientID) 
		AND (dfa.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
		ORDER BY dfa.DetailFieldAlias, dfa.ClientID, dfa.LeadTypeID 
		
		PRINT '
		SELECT * 
		FROM dbo.DetailFieldAlias dfa (nolock) 
		WHERE (dfa.ClientID = ' + cast(@ClientID as varchar) + ') 
		ORDER BY dfa.DetailFieldAlias, dfa.ClientID, dfa.LeadTypeID 
		'
	END
	ELSE
	BEGIN
		SELECT * 
		FROM dbo.DetailFieldAlias dfa (nolock) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID =  dfa.DetailFieldID 
		WHERE (dfa.ClientID = @ClientID) 
		AND (dfa.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
		AND (df.LeadOrMatter = @LeadOrMatter OR @LeadOrMatter IS NULL) 
		AND (df.QuestionTypeID = @QuestionTypeID OR @QuestionTypeID IS NULL) 
		ORDER BY dfa.DetailFieldAlias, df.ClientID, df.LeadTypeID, df.FieldCaption, df.LeadOrMatter, df.QuestionTypeID  
		
		PRINT '
		SELECT * 
		FROM dbo.DetailFieldAlias dfa (nolock) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID =  dfa.DetailFieldID 
		WHERE (dfa.ClientID = ' + cast(@ClientID as varchar) + ') ' + CASE WHEN @LeadTypeID IS NULL THEN '' ELSE '
		AND (dfa.LeadTypeID = ' + cast(@LeadTypeID as varchar) + ') ' END + CASE WHEN @LeadOrMatter IS NULL THEN '' ELSE '
		AND (dfa.LeadOrMatter = ' + cast(@LeadOrMatter as varchar) + ') ' END + CASE WHEN @QuestionTypeID IS NULL THEN '' ELSE '
		AND (dfa.QuestionTypeID = ' + cast(@QuestionTypeID as varchar) + ') ' END + ' 
		ORDER BY dfa.DetailFieldAlias, df.ClientID, df.LeadTypeID, df.FieldCaption, df.LeadOrMatter, df.QuestionTypeID  
		'
	END
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[dfa] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dfa] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dfa] TO [sp_executeall]
GO
