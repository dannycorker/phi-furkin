SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[test1]		
(		
	@SessionId	int = 1234,
	@XmlRequest XML = null
)		
AS		
BEGIN

SET @XmlRequest =

'<QuoteDefinition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <SessionId xsi:nil="true" />
  <Customer>
    <FirstName>dsffds</FirstName>
    <LastName>fdsfdsa</LastName>
    <Email>fdsdsf@dsfasd</Email>
    <TitleId>2</TitleId>
    <HomePhone />
    <Mobile />
    <AllowSms>false</AllowSms>
    <AllowEmail>false</AllowEmail>
    <AllowPost>false</AllowPost>
    <AllowPhone>false</AllowPhone>
    <DateOfBirth>2000-03-15T00:00:00</DateOfBirth>
    <CustomerId>0</CustomerId>
    <CommunicationPreference xsi:nil="true" />
    <CommunicationFormatPreference xsi:nil="true" />
    <MarketingPreference xsi:nil="true" />
    <CampaignCode />
  </Customer>
  <Address>
    <Address1>address22</Address1>
    <Address2>address22</Address2>
    <City>town2</City>
    <Postcode>postcode2</Postcode>
    <County>county2</County>
  </Address>
  <PetQuotes>
    <PetDefinition>
      <BreedId>156472</BreedId>
      <SpeciesId>42989</SpeciesId>
      <Name>dfsfda</Name>
      <PurchasePrice>0</PurchasePrice>
      <DateOfBirth>2018-01-09T00:00:00</DateOfBirth>
      <GenderId>5168</GenderId>
      <HasMicrochip>true</HasMicrochip>
      <IsNeutered>true</IsNeutered>
      <MicrochipNo />
      <SelectedProductId xsi:nil="true" />
      <QuoteStartDate>2018-04-13T00:00:00</QuoteStartDate>
      <Premiums />
      <Conditions />
      <UnderwritingAnswers />
      <CoverForIllness>true</CoverForIllness>
      <CoverForMedicalConditions>true</CoverForMedicalConditions>
    </PetDefinition>
  </PetQuotes>
</QuoteDefinition>'

 -- SET @XmlRequest.modify('delete(/QuoteDefinition/SessionId)');-- need to apply ths sessionId to the xml.
 -- SET @XmlRequest.modify('insert <SessionId>test</SessionId> as first into (/QuoteDefinition)[1]');-- need to apply ths sessionId to the xml.
  SET @XmlRequest.modify('replace value of (/QuoteDefinition/SessionId/text())[1] with sql:variable("@SessionID")');

select @XmlRequest;

End
GO
GRANT VIEW DEFINITION ON  [dbo].[test1] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[test1] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[test1] TO [sp_executeall]
GO
