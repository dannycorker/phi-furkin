SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the BillItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_GetByBillID]
(

	@BillID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[BillItemID],
					[ClientID],
					[BillID],
					[Description],
					[ClientPersonnelID],
					[WorkUnits],
					[Total],
					[ChargeOutRate],
					[TotalIsCustomValue]
				FROM
					[dbo].[BillItem] WITH (NOLOCK) 
				WHERE
					[BillID] = @BillID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_GetByBillID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_GetByBillID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_GetByBillID] TO [sp_executeall]
GO
