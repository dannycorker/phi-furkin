SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ValidationCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriterias_Update]
(

	@ValidationCriteriaID int   ,

	@Criteria nvarchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ValidationCriterias]
				SET
					[Criteria] = @Criteria
				WHERE
[ValidationCriteriaID] = @ValidationCriteriaID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriterias_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Update] TO [sp_executeall]
GO
