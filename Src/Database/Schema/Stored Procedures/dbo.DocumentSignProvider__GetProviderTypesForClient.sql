SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetProviderTypesForClient]
	@ClientID INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	DSPTypeID
	FROM	DocumentSignProviderAccount WITH (NOLOCK) 
	WHERE	ClientID = @ClientID
	AND		[Enabled] = 1
	GROUP BY DSPTypeID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetProviderTypesForClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetProviderTypesForClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetProviderTypesForClient] TO [sp_executeall]
GO
