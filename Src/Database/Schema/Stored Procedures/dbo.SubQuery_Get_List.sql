SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SubQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQuery_Get_List]

AS


				
				SELECT
					[SubQueryID],
					[ClientID],
					[SubQueryName],
					[SubQueryDesc],
					[SubQueryText]
				FROM
					[dbo].[SubQuery] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Get_List] TO [sp_executeall]
GO
