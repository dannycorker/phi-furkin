SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-12
-- Description:	Gets a list of tablerows based upon subtype id and the given identity
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetBySubtypeIDAndIdentity]
(
	@identity INT, 
	@detailFieldSubTypeID INT, 
	@detailFieldID INT, 	
	@detailFieldPageID INT,
	@clientID INT
)

AS
BEGIN

	--SELECT * FROM TableRows WHERE ClientID = @ClientID

	DECLARE @simpleClause VARCHAR(MAX) = '',
	@query VARCHAR(MAX) = ''
	
	SELECT @simpleClause = CASE 
		WHEN @detailFieldSubTypeID = 1 THEN ' AND (tr.LeadID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 2 THEN ' AND (tr.MatterID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 10 THEN ' AND (tr.CustomerID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 11 THEN ' AND (tr.CaseID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 12 THEN 'AND (tr.ClientID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 13 THEN ' AND (tr.ClientPersonnelID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 14 THEN ' AND (tr.ContactID = ' + CAST(@identity AS VARCHAR) + ')'
	END

	
    SET @query = 'SELECT * FROM [dbo].[TableRows] tr WITH (NOLOCK)     
    WHERE (DetailFieldID = ' + CAST(@detailFieldID AS VARCHAR) + ') AND    
    ([DetailFieldPageID] = ' + CAST(@detailFieldPageID AS VARCHAR) + ') AND
    ([ClientID] = ' + CAST(@ClientID AS VARCHAR) + ') ' + @simpleClause					

	EXEC (@query)
						
	SELECT @@ROWCOUNT			

END

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetBySubtypeIDAndIdentity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetBySubtypeIDAndIdentity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetBySubtypeIDAndIdentity] TO [sp_executeall]
GO
