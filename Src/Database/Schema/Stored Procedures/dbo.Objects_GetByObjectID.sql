SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Objects table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Objects_GetByObjectID]
(

	@ObjectID int   
)
AS


				SELECT
					[ObjectID],
					[ClientID],
					[SubClientID],
					[ObjectTypeID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Name],
					[Details],
					[SourceID]
				FROM
					[dbo].[Objects] WITH (NOLOCK) 
				WHERE
										[ObjectID] = @ObjectID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_GetByObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Objects_GetByObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_GetByObjectID] TO [sp_executeall]
GO
