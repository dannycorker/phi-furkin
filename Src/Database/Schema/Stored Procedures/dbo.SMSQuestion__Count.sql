SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-04-2013
-- Description:	Counts the number of questions in the given group
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestion__Count]

	@SMSQuestionGroupID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	SELECT Count(*) SMSQuestionCount 
	FROM SMSQuestionGroupMember WITH (NOLOCK) 
	WHERE SMSQuestionGroupID=@SMSQuestionGroupID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__Count] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion__Count] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__Count] TO [sp_executeall]
GO
