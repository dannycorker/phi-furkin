SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the IncendiaConfigurationClient table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_GetByIncendiaConfigurationClientID]
(

	@IncendiaConfigurationClientID int   
)
AS


				SELECT
					[IncendiaConfigurationClientID],
					[AquariumClientID],
					[Username],
					[Password],
					[URI]
				FROM
					[dbo].[IncendiaConfigurationClient] WITH (NOLOCK) 
				WHERE
										[IncendiaConfigurationClientID] = @IncendiaConfigurationClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_GetByIncendiaConfigurationClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_GetByIncendiaConfigurationClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_GetByIncendiaConfigurationClientID] TO [sp_executeall]
GO
