SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SMSQuestionnaire table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_Get_List]

AS


				
				SELECT
					[SMSQuestionnaireID],
					[ClientID],
					[Title],
					[Description],
					[Enabled],
					[ToTarget],
					[CreateCustomerIfNotFound],
					[UseSentimentAnalysis],
					[RuleSetID],
					[IsVoice],
					[Timeout],
					[Repeat],
					[RepeatPrefix]
				FROM
					[dbo].[SMSQuestionnaire] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Get_List] TO [sp_executeall]
GO
