SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-07-16
-- Description:	Evaluates a rule based on a set of inputs
-- ROH	2015-09-14	Added checkpoints and deltas
-- SB	2016-03-02	Forced the rule order
-- IS	2016-08-17	Added Evaluate Ruleset By RuleSet/ChangeSet/ByEffectiveDate
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Evaluate_RuleSet_V3]
(
	@ID INT,
	@Input VARCHAR(2000) = NULL,
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@Debug BIT = 0,
	@Overrides dbo.tvpIntVarcharVarchar READONLY,
	@Output XML = NULL OUTPUT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	CREATE TABLE #Rules 
	(
		ID INT IDENTITY,
		RuleID INT,
		RuleName VARCHAR(200)
	)
	
	-- IS 2016-08-17 Added Evaluate Ruleset By RuleSet/ByChangeSet/ByEffectiveDate
	DECLARE @OverrideRuleSetID INT = NULL
	
	SELECT @OverrideRuleSetID = [dbo].[fn_RulesEngine_GetRuleSetOverride](@ID, @Overrides)
	
	IF @OverrideRuleSetID != @ID
	BEGIN
		exec _C00_LogIt 'Info', 'RulesEngine_Evaluate_RuleSet', 'EvaluateByRuleSetOverride', @OverrideRuleSetID, @ID 
		
		SELECT @ID = @OverrideRuleSetID
	END
	ELSE
	BEGIN
		exec _C00_LogIt 'Info', 'RulesEngine_Evaluate_RuleSet', 'EvaluateByRuleSet', 'No evaluated ruleset override found', @ID 
	END
	-- IS 2016-08-17 Added Evaluate Ruleset By RuleSet/ByChangeSet/ByEffectiveDate
	
	
	INSERT #Rules (RuleID, RuleName)
	SELECT r.RuleID, r.Name
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	WHERE r.RuleSetID = @ID
	ORDER BY r.RuleOrder
		
	DECLARE @Count INT,
			@Index INT = 0
			
	SELECT @Count = COUNT(*)
	FROM #Rules
	
	CREATE TABLE #InOut 
	(
		ID INT IDENTITY,
		RuleID INT,
		RuleCheckpoint VARCHAR(50),
		Input VARCHAR(2000),
		Transform VARCHAR(50),
		Value VARCHAR(2000),
		Output VARCHAR(2000),
		Delta VARCHAR(2000),
		Raw XML,
		RuleSetID INT,
		StartTime DATETIME,
		MsTime INT
	)
	
	WHILE @Index < @Count
	BEGIN
	
		SELECT @Index += 1
		
		DECLARE @RuleID INT = NULL
		SELECT @RuleID = RuleID
		FROM #Rules
		WHERE ID = @Index
		
		DECLARE @StartTime DATETIME = dbo.fn_GetDate_Local()
		
		DECLARE @Xml XML = NULL	
		EXEC dbo.RulesEngine_Evaluate_Rule_V3 @RuleID, @Input, @CustomerID, @LeadID, @CaseID, @MatterID, @Overrides = @Overrides, @Output = @Xml OUTPUT
		
		INSERT #InOut (Input, Transform, Value, Output, Delta, RuleCheckpoint, Raw, RuleSetID, StartTime)
		SELECT	n.c.value('@Input', 'VARCHAR(2000)'),
				n.c.value('@Transform', 'VARCHAR(2000)'),
				n.c.value('@Value', 'VARCHAR(2000)'),
				n.c.value('@Output', 'VARCHAR(2000)'),
				n.c.value('@Delta', 'VARCHAR(2000)'),
				n.c.value('@RuleCheckpoint', 'VARCHAR(2000)'),
				n.c.query('.'),
				@ID,
				@StartTime
		FROM @Xml.nodes('Rule') AS n(c)
		
		SELECT @Input = Output
		FROM #InOut
		WHERE RuleID IS NULL
		
		UPDATE #InOut
		SET RuleID = @RuleID
		WHERE RuleID IS NULL
		
		UPDATE	#InOut
		SET		MsTime = DATEDIFF(ms, StartTime, dbo.fn_GetDate_Local())
		WHERE RuleID = @RuleID
		AND		RuleSetID = @ID
		AND		MsTime IS NULL

	END
	
	IF @Debug = 1
	BEGIN
	
		SELECT r.ID, r.RuleID, r.RuleName, i.RuleCheckpoint, i.Input, i.Transform, i.Value, i.Output, i.Delta, Raw
		FROM #Rules r
		INNER JOIN #InOut i ON r.RuleID = i.RuleID
		ORDER BY r.ID
	
		SELECT	RuleSetID, *
		FROM	#InOut
		WHERE	RuleSetID = @ID
	
	END
	
	SELECT @Output = 
	(
		SELECT Raw.query('//Rule') 
		FROM #InOut
		ORDER BY ID
		FOR XML PATH (''), ROOT ('RuleSet')
	)

	DROP TABLE #Rules
	DROP TABLE #InOut

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Evaluate_RuleSet_V3] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Evaluate_RuleSet_V3] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Evaluate_RuleSet_V3] TO [sp_executeall]
GO
