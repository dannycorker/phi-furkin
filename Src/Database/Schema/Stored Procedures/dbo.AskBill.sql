SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2018-06-19
-- Description:	James' Bill
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID. If CountryID = 14 then do not return AccountMandate from AskBill
-- =============================================
CREATE PROCEDURE [dbo].[AskBill]
(
	@CustomerID INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	/*GPR 2020-03-02 for AAG-202*/
	DECLARE @CountryID INT
	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	SELECT * FROM CustomerLedger cl WITH ( NOLOCK ) where cl.CustomerID = @CustomerID 
	SELECT * FROM Payment p WITH ( NOLOCK ) where p.CustomerID = @CustomerID 
	SELECT * FROM CustomerPaymentSchedule cp WITH ( NOLOCK ) where cp.CustomerID = @CustomerID
	SELECT * FROM PurchasedProduct p with (NOLOCK) where p.CustomerID = @CustomerID 
	SELECT * FROM PurchasedProductPaymentSchedule p WITH ( NOLOCK ) where p.CustomerID = @CustomerID 
	SELECT * FROM Account A WITH ( NOLOCK ) where a.CustomerID = @CustomerID 
	IF @CountryID = 14 /*Australia*/
	BEGIN
		SELECT * FROM AccountMandate am WITH ( NOLOCK ) where am.CustomerID = @CustomerID 
	END
	SELECT * FROM CardTransaction ct WITH ( NOLOCK ) where ct.CustomerID = @CustomerID 
	/*Premium Funder Integration: PCL*/
	SELECT * FROM PremiumFunderRequest quoterequest WITH (NOLOCK) WHERE quoterequest.CustomerID = @CustomerID 
	SELECT * FROM PremiumFunderResponse quoteresponse WITH (NOLOCK) WHERE quoteresponse.CustomerID = @CustomerID 
	SELECT * FROM PremiumFunderApplicationRequest applicationrequest WITH (NOLOCK) WHERE applicationrequest.CustomerID = @CustomerID 
	SELECT * FROM PremiumFunderApplicationResponse applicationresponse WITH (NOLOCK) WHERE applicationresponse.CustomerID = @CustomerID 

	SELECT 'CustomerLedger tracking'
	
	SELECT cl.TransactionGross, cl.CustomerLedgerID, cps.CustomerPaymentScheduleID, pps.PurchasedProductPaymentScheduleID, ppps.PurchasedProductPaymentScheduleID FROM CustomerLedger cl with (NOLOCK) 
	LEFT JOIN CustomerPaymentSchedule cps with (NOLOCK) on cps.CustomerLedgerID = cl.CustomerLedgerID 
	left join PurchasedProductPaymentSchedule pps with (NOLOCK) on pps.CustomerLedgerID = cl.CustomerLedgerID 
	left join PurchasedProductPaymentSchedule ppps with (NOLOCK) on ppps.ContraCustomerLedgerID = cl.CustomerLedgerID 
	WHere cl.CustomerID = @CustomerID 

	SELECT 'Cashbook'
	
	SELECT CONVERT(DATE,CustomerLedger.TransactionDate,101) as [OpenGI process date], 
	convert(char(5), CustomerLedger.TransactionDate, 108) [time], 
	pol.DetailValue  as [Policy ref],
	'Aquarium Automation' as [User],
	CASE ppps.PurchasedProductPaymentScheduleTypeID WHEN  '6' THEN 'MTA' 
	WHEN  '7' THEN 'CNL'
	WHEN  '8' THEN 'RNS' ELSE 'NB' END,
	CASE Account.AccountTypeID WHEN 3 THEN ppps.PaymentGross ELSE 0 END as [(Cheque)],
	CASE Account.AccountTypeID WHEN 2 THEN ppps.PaymentGross ELSE 0 END as [(credit card)],
	CASE Account.AccountTypeID WHEN 1 THEN ppps.PaymentGross ELSE 0 END as [(bank credits)],
	ppps.PaymentGross as [Total], 
	'' as [Bank Statment From],
	DATEPART(Year,p.ValidFrom) as [Policy Year], 
	c.FriendlyName as [Afinity],
	'' as [Claim Year]
	FROM CustomerLedger CustomerLedger WITH (NOLOCK) 
	INNER JOIN PurchasedProductPaymentSchedule ppps WITH ( NOLOCK ) on ppps.CustomerLedgerID = CustomerLedger.CustomerLedgerID
	INNER JOIN PurchasedProduct p WITH ( NOLOCK ) on p.PurchasedProductID = ppps.PurchasedProductID
	INNER JOIN Account Account WITH (NOLOCK) ON Account.AccountID = ppps.AccountID
	INNER JOIN Customers Customers WITH (NOLOCK) ON CustomerLedger.CustomerID = Customers.CustomerID
	INNER JOIN ClientAccount c WITH ( NOLOCK ) on c.ClientAccountID = ppps.ClientAccountID 
	INNER JOIN MatterDetailValues pol WITH ( NOLOCK ) on pol.MatterID = p.ObjectID and pol.DetailFieldID = 170050
	WHERE ppps.PaymentGross <> 0 
	AND Account.AccountUseID = 1 
	AND Customers.Test = 0 
	AND Customers.ClientID = @ClientID 
	AND CustomerLedger.customerID = @CustomerID 
	UNION All
	/*Failed*/ 
	SELECT CONVERT(DATE,CustomerLedger.TransactionDate,101) as [OpenGI process date], 
	convert(char(5), CustomerLedger.TransactionDate, 108) [time], 
	pol.DetailValue as [Policy ref],
	'Aquarium Automation' as [User],
	CASE ppps.PurchasedProductPaymentScheduleTypeID WHEN  '6' THEN 'MTA' 
	WHEN  '7' THEN 'CNL'
	WHEN  '8' THEN 'RNS' ELSE 'NB' END,
	CASE Account.AccountTypeID WHEN 3 THEN -ppps.PaymentGross ELSE 0 END as [(Cheque)],
	CASE Account.AccountTypeID WHEN 2 THEN -ppps.PaymentGross ELSE 0 END as [(credit card)],
	CASE Account.AccountTypeID WHEN 1 THEN -ppps.PaymentGross ELSE 0 END as [(bank credits)],
	-ppps.PaymentGross as [Total], 
	'' as [Bank Statment From],
	DATEPART(Year,p.ValidFrom) as [Policy Year], 
	c.FriendlyName as [Afinity]
	, '' as [Claim Year]
	FROM CustomerLedger CustomerLedger WITH (NOLOCK) 
	INNER JOIN PurchasedProductPaymentSchedule ppps WITH ( NOLOCK ) on ppps.ContraCustomerLedgerID = CustomerLedger.CustomerLedgerID
	INNER JOIN PurchasedProduct p WITH ( NOLOCK ) on p.PurchasedProductID = ppps.PurchasedProductID
	INNER JOIN Account Account WITH (NOLOCK) ON Account.AccountID = ppps.AccountID AND Account.AccountUseID = 1 
	INNER JOIN Customers Customers WITH (NOLOCK) ON CustomerLedger.CustomerID = Customers.CustomerID
	INNER JOIN ClientAccount c WITH ( NOLOCK ) on c.ClientAccountID = ppps.ClientAccountID 
	INNER JOIN MatterDetailValues pol WITH ( NOLOCK ) on pol.MatterID = p.ObjectID and pol.DetailFieldID = 170050
	WHERE ppps.PaymentGross <> 0 
	AND Customers.Test = 0 
	AND Customers.ClientID = @ClientID 
	AND CustomerLedger.customerID = @CustomerID 
	UNION all
	SELECT CONVERT(DATE,CustomerLedger.TransactionDate,101) as [OpenGI process date], 
	convert(char(5), CustomerLedger.TransactionDate, 108) [time], 
	pol.DetailValue as [Policy ref],
	'Aquarium Automation' as [User],
	'CL' as [Type],
	CASE Account.AccountTypeID WHEN 3 THEN cps.PaymentGross ELSE 0 END as [(Cheque)],
	CASE Account.AccountTypeID WHEN 2 THEN cps.PaymentGross ELSE 0 END as [(credit card)],
	CASE Account.AccountTypeID WHEN 1 THEN cps.PaymentGross ELSE 0 END as [(bank credits)],
	cps.PaymentGross as [Total], 
	'' as [Bank Statment From],
	DATEPART(Year,mdv.ValueDate) as [Policy Year], 
	c.FriendlyName as [Afinity],
	DATEPART(Year,mdv.ValueDate) as [Claim Year]
	FROM CustomerLedger CustomerLedger WITH (NOLOCK) 
	INNER JOIN CustomerPaymentSchedule cps WITH ( NOLOCK ) on cps.CustomerLedgerID = CustomerLedger.CustomerLedgerID 
	INNER JOIN Account Account WITH (NOLOCK) ON Account.AccountID = cps.AccountID AND Account.AccountUseID = 2
	INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = cps.RelatedObjectID and mdv.DetailFieldID = 144892 
	INNER JOIN Customers Customers WITH (NOLOCK) ON CustomerLedger.CustomerID = Customers.CustomerID
	INNER JOIN ClientAccount c WITH ( NOLOCK ) on c.ClientAccountID = cps.ClientAccountID 
	INNER JOIN MatterDetailValues pol WITH ( NOLOCK ) on pol.MatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(mdv.MatterID) and pol.DetailFieldID = 170050
	WHERE  cps.PaymentGross <> 0 
	AND Customers.Test = 0
	AND Customers.ClientID = @ClientID
	AND CustomerLedger.customerID = @CustomerID 


END




GO
GRANT VIEW DEFINITION ON  [dbo].[AskBill] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AskBill] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AskBill] TO [sp_executeall]
GO
