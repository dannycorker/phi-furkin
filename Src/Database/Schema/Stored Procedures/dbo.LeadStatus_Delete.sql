SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_Delete]
(

	@StatusID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[LeadStatus] WITH (ROWLOCK) 
				WHERE
					[StatusID] = @StatusID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Delete] TO [sp_executeall]
GO
