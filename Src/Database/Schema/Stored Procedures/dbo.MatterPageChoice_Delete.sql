SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the MatterPageChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_Delete]
(

	@MatterPageChoiceID int   
)
AS


				DELETE FROM [dbo].[MatterPageChoice] WITH (ROWLOCK) 
				WHERE
					[MatterPageChoiceID] = @MatterPageChoiceID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Delete] TO [sp_executeall]
GO
