SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-06-2016
-- Description:	creates account information
-- =============================================
CREATE PROCEDURE [dbo].[Account__Insert]	
	@ClientID INT, 
	@CustomerID INT, 
	@AccountHolderName VARCHAR(250) = null, 
	@FriendlyName VARCHAR(250) = null, 
	@AccountNumber VARCHAR(50) = null, 
	@Sortcode VARCHAR(20) = null, 
	@Reference VARCHAR(250) = null, 
	@CardToken VARCHAR(250) = null, 
	@MaskedCardNumber VARCHAR(24) = null, 
	@ExpiryMonth INT = null, 
	@ExpiryYear INT = null, 
	@ExpiryDate DATETIME = null, 
	@AccountTypeID INT = null,
	@Active BIT = null, 
	@WhoCreated INT, 
	@WhenCreated DATETIME, 
	@WhoModified INT, 
	@WhenModified DATETIME
AS
BEGIN
		
	SET NOCOUNT ON;
	
	INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference, CardToken, MaskedCardNumber, ExpiryMonth, ExpiryYear, ExpiryDate, AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@ClientID, @CustomerID, @AccountHolderName, @FriendlyName, @AccountNumber, @Sortcode, @Reference, @CardToken, @MaskedCardNumber, @ExpiryMonth, @ExpiryYear, @ExpiryDate, @AccountTypeID, @Active, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified)
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__Insert] TO [sp_executeall]
GO
