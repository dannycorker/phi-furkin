SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldGroup_Insert]
(

	@RPIFieldGroupID int    OUTPUT,

	@GroupName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[RPIFieldGroup]
					(
					[GroupName]
					)
				VALUES
					(
					@GroupName
					)
				-- Get the identity value
				SET @RPIFieldGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Insert] TO [sp_executeall]
GO
