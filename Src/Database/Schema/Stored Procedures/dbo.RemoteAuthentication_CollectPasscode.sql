SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-09-23
-- Description:	Attempt to collect a passcode (GUID) that was set up by a call to RemoteAuthentication_CreatePasscode earlier
-- Validate the caller's credentials and create a GUID as a logon token for the emailaddress who will try to logon later.
-- =============================================
CREATE PROCEDURE [dbo].[RemoteAuthentication_CollectPasscode] 
	@Passcode VARCHAR(50), 
	@IPAddress VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Passcode is the GUID that was set up by RemoteAuthentication_CreatePasscode earlier.
		IPAddress is the address of the person trying to collect/consume the token
	*/

	
	/*
		Check that this passcode exists and is valid
		Looks like I forgot to use @ThirdPartySystemID at all
		Record pass/fail in the history table
	*/
	DECLARE @IsValid BIT = 0, 
	@ValidUntil DATETIME2(0) = NULL, 
	@ClientID INT = NULL, 
	@ClientPersonnelID INT = NULL, 
	@UserName VARCHAR(255) = '',
	@AttemptedLogins INT = NULL, 
	@AccountDisabled BIT = NULL, 
	@ThirdPartySystemID INT = 42, 
	@Notes VARCHAR(500) = '', 
	@WhenFirstUsed DATETIME2(0) = NULL, 
	@WhenLastUsed DATETIME2(0) = NULL, 
	@UsageCount INT = 0
	
	/*
		Try to authenticate the user from the passcode passed in. 
		Note that they may have locked out their account or been disabled since the passcode was set up.
		Should also check IP security within the app after authentication.
	*/
	SELECT TOP (1) @IsValid = ra.IsValid, 
	@ValidUntil = ra.ValidUntil, 
	@WhenFirstUsed = ra.WhenFirstUsed, 
	@WhenLastUsed = ra.WhenLastUsed, 
	@UsageCount = ra.UsageCount,
	@ClientID = cp.ClientID, 
	@ClientPersonnelID = cp.ClientPersonnelID, 
	@UserName = cp.EmailAddress, 
	@AttemptedLogins = cp.AttemptedLogins, 
	@AccountDisabled = cp.AccountDisabled 
	FROM dbo.RemoteAuth ra WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = ra.ClientPersonnelID 
	WHERE ra.AuthKey = @Passcode 
	AND cp.ThirdPartySystemId = 0

	/* Check for anything invalid */
	IF @@ROWCOUNT < 1
	BEGIN
		SELECT @Notes = 'INVALID: Passcode not found at all; '
	END
	
	/* This is a valid client IP Address (never mind Daviker, this is about where the end user is accessing the system from) */
	/*IF @IPAddress NOT IN ('127.0.0.1', '::1')
	BEGIN
		SELECT @IsValid = 0, @Notes += 'INVALID: IP address security breached for this client; '
	END*/
	
	IF @AccountDisabled = 1
	BEGIN
		SELECT @IsValid = 0, @Notes += 'INVALID: Account disabled; '
	END
	
	IF @AttemptedLogins > 2
	BEGIN
		SELECT @IsValid = 0, @Notes += 'INVALID: Attempted logins > 2; '
	END
	
	IF @ValidUntil < dbo.fn_GetDate_Local()
	BEGIN
		SELECT @IsValid = 0, @Notes += 'INVALID: This token has expired; '
	END
	
	IF @WhenFirstUsed IS NOT NULL
	BEGIN
		/* Originally we were going to let users collect the token many times, but it has reverted back to single-use-only now */
		SELECT @IsValid = 0, @Notes += 'INVALID: This token has already been collected; '
	END

	/* Hooray - all valid */
	IF @IsValid = 1
	BEGIN
		/* Update the RemoteAuth record first */
		UPDATE dbo.RemoteAuth 
		SET WhenFirstUsed = CASE WHEN WhenFirstUsed IS NULL THEN dbo.fn_GetDate_Local() ELSE WhenFirstUsed END, /* Only set this the first time it is ever used */
		WhenLastUsed = dbo.fn_GetDate_Local(), 
		UsageCount += 1 
		WHERE AuthKey = @Passcode 
	END
	
	/* Always select our results back to the app first */
	SELECT @IsValid AS IsValid, 
	@Notes AS Notes 
	
	/* Then log the attempt (valid or not) and return the user details back to the app */
	INSERT INTO dbo.LoginHistory (
		UserName, 
		LoginDateTime, 
		IsSuccess, 
		ClientID, 
		ClientPersonnelID, 
		AppLogin, 
		AttemptedLogins, 
		AccountDisabled, 
		Notes,
		ThirdPartySystemID,
		HostName,
		UserIPDetails
	) 
	VALUES 
	( 
		@UserName, 
		dbo.fn_GetDate_Local(), 
		@IsValid, 
		@ClientID, 
		@ClientPersonnelID, 
		0, 
		@AttemptedLogins, 
		@AccountDisabled, 
		@Notes,
		@ThirdPartySystemID,			
		HOST_NAME(),
		@IPAddress
	)	
		
	SELECT 
	cp.ClientID, 
	cp.ClientOfficeID,
	cp.FirstName, 
	cp.MiddleName, 
	cp.LastName, 
	cp.ClientPersonnelID,
	cp.ClientPersonnelAdminGroupID,  
	cp.EmailAddress,
	cp.AttemptedLogins,
	cp.AccountDisabled,
	cp.ChargeOutRate, 
	cp.LanguageID, 
	co.CountryID, 
	ISNULL(cp.IsAquarium, 0) AS IsAquarium 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = cp.ClientOfficeID 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID 
	AND @IsValid = 1 

	UNION

	SELECT 
	-1 AS ClientID, 
	NULL AS ClientOfficeID,
	NULL AS FirstName, 
	NULL AS MiddleName, 
	NULL AS LastName, 
	cp.ClientPersonnelID,
	cp.ClientPersonnelAdminGroupID,  
	cp.EmailAddress,
	cp.AttemptedLogins,
	cp.AccountDisabled,
	cp.ChargeOutRate, 
	cp.LanguageID, 
	co.CountryID, 
	ISNULL(cp.IsAquarium, 0) AS IsAquarium  
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = cp.ClientOfficeID
	WHERE cp.ClientPersonnelID = @ClientPersonnelID
	AND @IsValid = 0 
	 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoteAuthentication_CollectPasscode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoteAuthentication_CollectPasscode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoteAuthentication_CollectPasscode] TO [sp_executeall]
GO
