SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectDetailValues_GetByObjectDetailValueID]
(

	@ObjectDetailValueID int   
)
AS


				SELECT
					[ObjectDetailValueID],
					[ClientID],
					[SubClientID],
					[ObjectID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime]
				FROM
					[dbo].[ObjectDetailValues] WITH (NOLOCK) 
				WHERE
										[ObjectDetailValueID] = @ObjectDetailValueID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_GetByObjectDetailValueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectDetailValues_GetByObjectDetailValueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_GetByObjectDetailValueID] TO [sp_executeall]
GO
