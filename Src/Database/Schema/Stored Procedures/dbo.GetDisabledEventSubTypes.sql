SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-02-2012
-- Description:	Gets a list of the vent sub types that are disabled
-- =============================================
CREATE PROCEDURE [dbo].[GetDisabledEventSubTypes]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;		
	
	Declare @CountOfEnabled int = 0
	
	SELECT @CountOfEnabled = Count(*) FROM EventSubtype es WITH (NOLOCK) 
	WHERE es.Available = 1
		
	IF @CountOfEnabled = 0
	BEGIN
		Select 'All event subtypes are disabled' EventSubtypeName
	END
	ELSE
	BEGIN
		SELECT * FROM EventSubtype es WITH (NOLOCK) 
		WHERE es.Available = 0
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetDisabledEventSubTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDisabledEventSubTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDisabledEventSubTypes] TO [sp_executeall]
GO
