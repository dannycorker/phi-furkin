SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
/*  
----------------------------------------------------------------------------------------------------  
  
-- Created By: Aquarium Software (https://www.aquarium-software.com)  
-- Purpose: Inserts a record into the ClientQuestionnaires table  
-- JWG 2012-12-21 Now gets the new ID from a common AquariusMaster table to avoid conflicts with other Aquarius DBs  
----------------------------------------------------------------------------------------------------  
*/  
  
  
CREATE PROCEDURE [dbo].[ClientQuestionnaires_Insert]  
(  
  
 @ClientQuestionnaireID int    OUTPUT,  
  
 @ClientID int   ,  
  
 @QuestionnaireTitle varchar (255)  ,  
  
 @QuestionnaireDescription varchar (MAX)  ,  
  
 @QuestionsPerPage int   ,  
  
 @QuestionnaireLogo image   ,  
  
 @QuestionnaireLogoFileName varchar (512)  ,  
  
 @QuestionnaireFlowDiagram varchar (MAX)  ,  
  
 @QuestionnaireIntroductionText varchar (MAX)  ,  
  
 @HideIntro bit   ,  
  
 @QuestionnaireFooterText varchar (MAX)  ,  
  
 @HideFooter bit   ,  
  
 @QuestionnaireAddressLookup bit   ,  
  
 @Published bit   ,  
  
 @CustomerInformationAtStart bit   ,  
  
 @LinkedQuestionnaireClientQuestionnaireID int   ,  
  
 @QuestionnaireFooterIframe varchar (MAX)  ,  
  
 @QuestionnaireHeaderIframe varchar (MAX)  ,  
  
 @QuestionnaireFooterInternal bit   ,  
  
 @QuestionnaireHeaderInternal bit   ,  
  
 @QuestionnaireFooterIframeHeight int   ,  
  
 @QuestionnaireHeaderIframeHeight int   ,  
  
 @QuestionnaireFooterIframeWidth int   ,  
  
 @QuestionnaireHeaderIframeWidth int   ,  
  
 @QuestionnaireBackGroundImage image   ,  
  
 @QuestionnaireBackGroundImageFileName varchar (512)  ,  
  
 @DefaultEmailAddress varchar (255)  ,  
  
 @MailingListType int   ,  
  
 @FrameMode bit   ,  
  
 @LayoutCss varchar (MAX)  ,  
  
 @LayoutCssFileName varchar (512)  ,  
  
 @ImportDirectlyIntoLeadManager bit   ,  
  
 @RunAsClientPersonnelID int   ,  
  
 @RememberAnswers bit   ,  
   
 @SourceID int ,  
  
 @WhoCreated int ,  
  
 @WhenCreated datetime ,  
  
 @WhoModified int ,  
  
 @WhenModified datetime  
  
)  
AS  
BEGIN  
  
 /*  
 INSERT INTO ClientQuestionnaires (ClientID, QuestionnaireTitle, QuestionnaireDescription, QuestionsPerPage, QuestionnaireIntroductionText, HideIntro, QuestionnaireFooterText, HideFooter, QuestionnaireFooterIframe, QuestionnaireHeaderIframe, QuestionnaireFooterInternal, QuestionnaireHeaderInternal, QuestionnaireFooterIframeHeight, QuestionnaireHeaderIframeHeight, QuestionnaireFooterIframeWidth, QuestionnaireHeaderIframeWidth, FrameMode, LayoutCss, LayoutCssFileName, ImportDirectlyIntoLeadManager, RunAsClientPersonnelID, RememberAnswers)  
 VALUES (@ClientID, @QuestionnaireTitle, @QuestionnaireDescription,@QuestionsPerPage, @QuestionnaireIntroductionText, @HideIntro, @QuestionnaireFooterText, @HideFooter, @QuestionnaireFooterIframe, @QuestionnaireHeaderIframe, @QuestionnaireFooterInternal, @QuestionnaireHeaderInternal,@QuestionnaireFooterIframeHeight, @QuestionnaireHeaderIframeHeight, @QuestionnaireFooterIframeWidth, @QuestionnaireHeaderIframeWidth, @FrameMode, @LayoutCss, @LayoutCssFileName, @ImportDirectlyIntoLeadManager, @RunAsClientPersonnelID, @RememberAnswers)  
 */  
 
 -- If we don't have an outer tranasction e.g. from the lead event   
 DECLARE @TranCount INT = @@TRANCOUNT  
 IF @TranCount = 0  
 BEGIN  
  BEGIN TRAN  
 END  
   
 BEGIN TRY  
    
  -- Get a new key from the master DB  
  DECLARE @MasterClientQuestionnaireID INT,  
  @DatabaseName SYSNAME = DB_NAME(),  
  @ServerName SYSNAME = NULL  
    
  EXEC @MasterClientQuestionnaireID = AquariusMaster.dbo.AQ_ClientQuestionnaire_Insert @ClientID, @DatabaseName, @ServerName  
    
  -- Set context info to allow access to table  
  DECLARE @ContextInfo VARBINARY(100) = CAST('ClientQuestionnaires' AS VARBINARY)  
  SET CONTEXT_INFO @ContextInfo  
    
  SET IDENTITY_INSERT [dbo].[ClientQuestionnaires] ON  
    
  INSERT INTO [dbo].[ClientQuestionnaires]  
  (  
  ClientQuestionnaireID  
  ,[ClientID]  
  ,[QuestionnaireTitle]  
  ,[QuestionnaireDescription]  
  ,[QuestionsPerPage]  
  ,[QuestionnaireLogo]  
  ,[QuestionnaireLogoFileName]  
  ,[QuestionnaireFlowDiagram]  
  ,[QuestionnaireIntroductionText]  
  ,[HideIntro]  
  ,[QuestionnaireFooterText]  
  ,[HideFooter]  
  ,[QuestionnaireAddressLookup]  
  ,[Published]  
  ,[CustomerInformationAtStart]  
  ,[LinkedQuestionnaireClientQuestionnaireID]  
  ,[QuestionnaireFooterIframe]  
  ,[QuestionnaireHeaderIframe]  
  ,[QuestionnaireFooterInternal]  
  ,[QuestionnaireHeaderInternal]  
  ,[QuestionnaireFooterIframeHeight]  
  ,[QuestionnaireHeaderIframeHeight]  
  ,[QuestionnaireFooterIframeWidth]  
  ,[QuestionnaireHeaderIframeWidth]  
  ,[QuestionnaireBackGroundImage]  
  ,[QuestionnaireBackGroundImageFileName]  
  ,[DefaultEmailAddress]  
  ,[MailingListType]  
  ,[FrameMode]  
  ,[LayoutCss]  
  ,[LayoutCssFileName]  
  ,[ImportDirectlyIntoLeadManager]  
  ,[RunAsClientPersonnelID]  
  ,[RememberAnswers]  
  ,[SourceID]  
  ,[WhoCreated]  
  ,[WhenCreated]  
  ,[WhoModified]  
  ,[WhenModified]  
  )  
  VALUES  
  (  
  @MasterClientQuestionnaireID  
  ,@ClientID  
  ,@QuestionnaireTitle  
  ,@QuestionnaireDescription  
  ,@QuestionsPerPage  
  ,@QuestionnaireLogo  
  ,@QuestionnaireLogoFileName  
  ,@QuestionnaireFlowDiagram  
  ,@QuestionnaireIntroductionText  
  ,@HideIntro  
  ,@QuestionnaireFooterText  
  ,@HideFooter  
  ,@QuestionnaireAddressLookup  
  ,@Published  
  ,@CustomerInformationAtStart  
  ,@LinkedQuestionnaireClientQuestionnaireID  
  ,@QuestionnaireFooterIframe  
  ,@QuestionnaireHeaderIframe  
  ,@QuestionnaireFooterInternal  
  ,@QuestionnaireHeaderInternal  
  ,@QuestionnaireFooterIframeHeight  
  ,@QuestionnaireHeaderIframeHeight  
  ,@QuestionnaireFooterIframeWidth  
  ,@QuestionnaireHeaderIframeWidth  
  ,@QuestionnaireBackGroundImage  
  ,@QuestionnaireBackGroundImageFileName  
  ,@DefaultEmailAddress  
  ,@MailingListType  
  ,@FrameMode  
  ,@LayoutCss  
  ,@LayoutCssFileName  
  ,@ImportDirectlyIntoLeadManager  
  ,@RunAsClientPersonnelID  
  ,@RememberAnswers  
  ,@SourceID  
  ,@WhoCreated  
  ,@WhenCreated  
  ,@WhoModified  
  ,@WhenModified  
  )  
    
  -- If we don't have an outer tran we can commit here  
  IF @TranCount = 0  
  BEGIN  
   COMMIT  
  END  
    
  -- Clear the things we set above  
  SET IDENTITY_INSERT [dbo].[ClientQuestionnaires] OFF  
  SET CONTEXT_INFO 0x0  
    
 END TRY     
 BEGIN CATCH      
    
  -- If we have any tran open then we need to rollback  
  IF @@TRANCOUNT > 0  
  BEGIN  
   ROLLBACK  
  END  
    
  -- Clear the things we set above  
  SET IDENTITY_INSERT [dbo].[ClientQuestionnaires] OFF  
  SET CONTEXT_INFO 0x0  
    
  DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()  
  DECLARE @ErrorSeverity INT = ERROR_SEVERITY()  
  DECLARE @ErrorState INT = ERROR_STATE()  
  
  RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)  
    
 END CATCH  
   
 -- This scope_identity should match the @MasterClientQuestionnaireID  
 SET @ClientQuestionnaireID = SCOPE_IDENTITY()  
   
END  
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Insert] TO [sp_executeall]
GO
