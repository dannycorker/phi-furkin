SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Cases table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_GetByLeadID]
(

	@LeadID int   
)
AS


				SELECT
					[CaseID],
					[LeadID],
					[ClientID],
					[CaseNum],
					[CaseRef],
					[ClientStatusID],
					[AquariumStatusID],
					[DefaultContactID],
					[LatestLeadEventID],
					[LatestInProcessLeadEventID],
					[LatestOutOfProcessLeadEventID],
					[LatestNonNoteLeadEventID],
					[LatestNoteLeadEventID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ProcessStartLeadEventID]
				FROM
					[dbo].[Cases] WITH (NOLOCK) 
				WHERE
										[LeadID] = @LeadID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByLeadID] TO [sp_executeall]
GO
