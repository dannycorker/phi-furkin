SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUser table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser__GetSaltByUserName]
(
	@Username varchar(50)   
)
AS


				SELECT
					[Salt]
				FROM
					[dbo].[PortalUser]
				WHERE
					[Username] = @Username

			Select @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetSaltByUserName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser__GetSaltByUserName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetSaltByUserName] TO [sp_executeall]
GO
