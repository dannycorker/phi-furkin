SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadDocumentEsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_Insert]
(

	@LeadDocumentEsignatureStatusID int    OUTPUT,

	@ClientID int   ,

	@LeadDocumentID int   ,

	@ElectronicSignatureDocumentKey varchar (50)  ,

	@DocumentName varchar (255)  ,

	@SentTo varchar (255)  ,

	@DateSent datetime   ,

	@EsignatureStatusID int   ,

	@StatusCheckedDate datetime   
)
AS


				
				INSERT INTO [dbo].[LeadDocumentEsignatureStatus]
					(
					[ClientID]
					,[LeadDocumentID]
					,[ElectronicSignatureDocumentKey]
					,[DocumentName]
					,[SentTo]
					,[DateSent]
					,[EsignatureStatusID]
					,[StatusCheckedDate]
					)
				VALUES
					(
					@ClientID
					,@LeadDocumentID
					,@ElectronicSignatureDocumentKey
					,@DocumentName
					,@SentTo
					,@DateSent
					,@EsignatureStatusID
					,@StatusCheckedDate
					)
				-- Get the identity value
				SET @LeadDocumentEsignatureStatusID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Insert] TO [sp_executeall]
GO
