SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-08-09
-- Description:	TEST proc for claim calculation testing
-- =============================================
CREATE PROCEDURE [dbo].[__SB_Test_CoInsurance] 
(
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@Output XML = NULL OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CoInsRows TABLE
	(
		ID INT IDENTITY,
		Name VARCHAR(50),
		FromDate DATE,
		ToDate DATE,
		CoInsurance DECIMAL(18, 2)
	)
	INSERT @CoInsRows (Name, FromDate, ToDate, CoInsurance) VALUES
	('A & I More', '2012-08-06', '2013-08-05', 0.20),
	('A & I Max', '2013-08-06', '2014-08-05', 0.10)

	SELECT @Output = 
	( 
		SELECT *
		FROM @CoInsRows
		FOR XML PATH ('Row'), ROOT ('Data')
	)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[__SB_Test_CoInsurance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__SB_Test_CoInsurance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__SB_Test_CoInsurance] TO [sp_executeall]
GO
