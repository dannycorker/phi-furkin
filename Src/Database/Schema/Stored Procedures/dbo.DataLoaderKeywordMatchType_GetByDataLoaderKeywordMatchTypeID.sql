SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderKeywordMatchType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderKeywordMatchType_GetByDataLoaderKeywordMatchTypeID]
(

	@DataLoaderKeywordMatchTypeID int   
)
AS


				SELECT
					[DataLoaderKeywordMatchTypeID],
					[MatchTypeName],
					[MatchTypeDescription]
				FROM
					[dbo].[DataLoaderKeywordMatchType] WITH (NOLOCK) 
				WHERE
										[DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_GetByDataLoaderKeywordMatchTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderKeywordMatchType_GetByDataLoaderKeywordMatchTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_GetByDataLoaderKeywordMatchTypeID] TO [sp_executeall]
GO
