SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[TableDetailValues__GetByParams]
(
	
	@LeadID int,
	
	@DetailFieldID int,

	@TableRowID int,

	@ClientID int,

	@MatterID int   = null

)

AS

    SELECT
	  [TableDetailValueID]
	, [TableRowID]
	, [ResourceListID]
	, [DetailFieldID]
	, [DetailValue]
	, [LeadID]
	, [MatterID]
	, [ClientID]
	, [EncryptedValue]
	, [ErrorMsg]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]	
	, [CustomerID]	
	, [CaseID]
	, [ClientPersonnelID]	
	, [ContactID]
    FROM
	[dbo].[TableDetailValues] WITH (NOLOCK) 
    WHERE 
	 
	([TableRowID] = @TableRowID)	
	AND ([DetailFieldID] = @DetailFieldID)	
	AND ([LeadID] = @LeadID)
	AND ([MatterID] = @MatterID OR @MatterID is null)
	AND ([ClientID] = @ClientID)
						
	Select @@ROWCOUNT		




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByParams] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByParams] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByParams] TO [sp_executeall]
GO
