SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the IncendiaConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationAquarium_Insert]
(

	@IncendiaConfigurationAquariumID int    OUTPUT,

	@AccountID varchar (50)  
)
AS


				
				INSERT INTO [dbo].[IncendiaConfigurationAquarium]
					(
					[AccountID]
					)
				VALUES
					(
					@AccountID
					)
				-- Get the identity value
				SET @IncendiaConfigurationAquariumID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationAquarium_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Insert] TO [sp_executeall]
GO
