SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderObjectField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_GetByDataTypeID]
(

	@DataTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderObjectFieldID],
					[DataLoaderObjectTypeID],
					[FieldName],
					[DataTypeID],
					[IsMandatory],
					[IsNormalisedField],
					[IsABitField],
					[IsClientOwned],
					[IsUniqueTableID],
					[IsWriteOnce],
					[HelperText]
				FROM
					[dbo].[DataLoaderObjectField] WITH (NOLOCK) 
				WHERE
					[DataTypeID] = @DataTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_GetByDataTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_GetByDataTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_GetByDataTypeID] TO [sp_executeall]
GO
