SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientTypes_Get_List]

AS


				
				SELECT
					[ClientTypeID],
					[ClientTypeName]
				FROM
					[dbo].[ClientTypes] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientTypes_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Get_List] TO [sp_executeall]
GO
