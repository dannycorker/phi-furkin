SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 2008-11-03
-- Description:	Gets all lead resource list info ready for pivoting
-- =============================================

CREATE PROCEDURE [dbo].[GetResourceListLeadValuesForMultiRecipientList]

	@LeadID int,
	@DetailFieldID int
AS
BEGIN

	SELECT     

		ldv.LeadDetailValueID,
		ldv.ClientID, 
		ldv.LeadID, 
		ldv.DetailFieldID, 
		ldv.ErrorMsg, 
		ldv.OriginalDetailValueID, 
		ldv.OriginalLeadID, 
		ldv.EncryptedValue, 

		rdv.ResourceListDetailValueID, 
		rdv.ResourceListID, 
		rdv.LeadOrMatter, 
		rdv.DetailValue,

		df.FieldName, 
		df.FieldCaption, 
		df.QuestionTypeID, 
		df.Required, 
		df.Lookup, 
		df.LookupListID, 
		df.LeadTypeID, 
		df.Enabled, 
		df.DetailFieldPageID, 
		df.FieldOrder, 
		df.ResourceListDetailFieldPageID, 
		df.TableDetailFieldPageID, 
		df.Hidden, 
		df.Encrypt

		FROM         

		LeadDetailValues ldv WITH (NOLOCK)

		INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) ON rdv.ResourceListID = ldv.DetailValue 
		INNER JOIN DetailFields df WITH (NOLOCK) ON rdv.DetailFieldID = df.DetailFieldID

		WHERE		ldv.DetailFieldID = @DetailFieldID
					AND ldv.LeadID = @LeadID
					AND df.Enabled = 1

		ORDER BY rdv.ResourceListID, df.FieldOrder

END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListLeadValuesForMultiRecipientList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetResourceListLeadValuesForMultiRecipientList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListLeadValuesForMultiRecipientList] TO [sp_executeall]
GO
