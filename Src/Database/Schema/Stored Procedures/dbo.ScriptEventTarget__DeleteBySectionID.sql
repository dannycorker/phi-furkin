SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-03-2015
-- Description:	Deletes the event targets via the section id
-- =============================================
CREATE PROCEDURE [dbo].[ScriptEventTarget__DeleteBySectionID]	
	@SectionID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DELETE FROM ScriptEventTarget
	WHERE SectionID=@SectionID
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget__DeleteBySectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget__DeleteBySectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget__DeleteBySectionID] TO [sp_executeall]
GO
