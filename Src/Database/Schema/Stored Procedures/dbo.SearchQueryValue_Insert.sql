SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SearchQueryValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_Insert]
(

	@SearchQueryValueID int    OUTPUT,

	@ClientID int   ,

	@SubClientID int   ,

	@SearchQueryID int   ,

	@SearchFieldID int   ,

	@SearchFieldOperatorID int   ,

	@FieldValue varchar (2000)  ,

	@FieldValueID int   
)
AS


				
				INSERT INTO [dbo].[SearchQueryValue]
					(
					[ClientID]
					,[SubClientID]
					,[SearchQueryID]
					,[SearchFieldID]
					,[SearchFieldOperatorID]
					,[FieldValue]
					,[FieldValueID]
					)
				VALUES
					(
					@ClientID
					,@SubClientID
					,@SearchQueryID
					,@SearchFieldID
					,@SearchFieldOperatorID
					,@FieldValue
					,@FieldValueID
					)
				-- Get the identity value
				SET @SearchQueryValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Insert] TO [sp_executeall]
GO
