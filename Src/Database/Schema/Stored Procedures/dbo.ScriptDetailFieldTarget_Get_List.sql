SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget_Get_List]

AS


				
				SELECT
					[ScriptDetailFieldTargetID],
					[ScriptID],
					[ClientID],
					[Target],
					[DetailFieldID],
					[ColumnFieldID],
					[Format],
					[SectionID]
				FROM
					[dbo].[ScriptDetailFieldTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Get_List] TO [sp_executeall]
GO
