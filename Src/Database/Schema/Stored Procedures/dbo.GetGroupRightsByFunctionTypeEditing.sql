SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-01
-- Description:	Get all EDITING rights for this Group
--		These records only exist while the group is
--      actually being edited. Then they get deleted.
--              2008-07-16: Encrypted Detail Fields
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupRightsByFunctionTypeEditing]
	-- Add the parameters for the stored procedure here
	@GroupID int,
	@FunctionTypeID int,
	@LeadTypeID int,
	@OfficeID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Main function type
	SELECT gfc.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	gfc.GroupFunctionControlEditingID as 'UniqueID',
	-1 as 'LeadTypeID' 
	FROM GroupFunctionControlEditing gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID = @FunctionTypeID
	AND gfc.LeadTypeID IS NULL
	AND @LeadTypeID = -1

	UNION

	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	gfc.GroupFunctionControlEditingID as 'UniqueID',  
	gfc.LeadTypeID as 'LeadTypeID' 
	FROM GroupFunctionControlEditing gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID 
	INNER JOIN LeadType lt ON lt.LeadTypeID = gfc.LeadTypeID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID = @FunctionTypeID
	AND gfc.LeadTypeID = lt.LeadTypeID
	AND gfc.LeadTypeID = @LeadTypeID

	UNION

	-- Very special case to show all the possible
	-- virtual lead types when the user clicks "Lead Types"
	SELECT gfc.FunctionTypeID as 'ID', 
	lt.LeadTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	4 as 'Source', 
	gfc.GroupFunctionControlEditingID as 'UniqueID', 
	gfc.LeadTypeID as 'LeadTypeID' 
	FROM GroupFunctionControlEditing gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID
	INNER JOIN LeadType lt ON lt.LeadTypeID = gfc.LeadTypeID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID = 23
	AND @FunctionTypeID = 7
	AND gfc.LeadTypeID IS NOT NULL
	AND @LeadTypeID = -1

	UNION

    -- Direct descendant function types of the main function type
	SELECT ft.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	gfc.GroupFunctionControlEditingID as 'UniqueID',  
	-1 as 'LeadTypeID' 
	FROM GroupFunctionControlEditing gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND gfc.LeadTypeID IS NULL 
	AND @LeadTypeID = -1

	UNION

    -- Special case for virtual function type 23, representing individual lead types
	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	gfc.GroupFunctionControlEditingID as 'UniqueID',  
	gfc.LeadTypeID as 'LeadTypeID' 
	FROM GroupFunctionControlEditing gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID 
	INNER JOIN LeadType lt ON lt.LeadTypeID = gfc.LeadTypeID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND gfc.LeadTypeID = lt.LeadTypeID
	AND gfc.LeadTypeID = @LeadTypeID

	UNION

	-- Detailed functions within the main function type
	SELECT grd.ObjectID as 'ID', 
	f.FunctionDescription as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	3 as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	-1 as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd INNER JOIN Functions f ON f.FunctionID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID 
	AND grd.LeadTypeID IS NULL
	AND @LeadTypeID = -1 -- Only show these when general rights are requested, not when EventType rights by lead type (12, 13 and 25) are being requested! 
	AND @FunctionTypeID <> 26 /* Exclude NoteTypes by Client */

	UNION

	-- Specific: Outcomes
	SELECT grd.ObjectID as 'ID', 
	o.OutcomeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	-1 as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd INNER JOIN Outcomes o ON o.OutcomeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 9 -- 9=Outcomes 
	AND grd.LeadTypeID IS NULL

	UNION

	-- Specific: Lead Types by Office
	SELECT grd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd INNER JOIN LeadType lt ON lt.LeadTypeID = grd.LeadTypeID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 11 -- 11=Lead Types by Office
	AND grd.LeadTypeID IS NOT NULL
	AND grd.ObjectID = @OfficeID 
/*
	SELECT grd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID'  
	FROM GroupRightsDynamicEditing grd INNER JOIN LeadType lt ON lt.LeadTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 11 -- 11=Lead Types by Office
	AND grd.LeadTypeID IS NOT NULL
	--N.B. No direct join on @LeadTypeID here. It's not offices by lead type.
*/
	UNION

	-- Specific: Pages by Lead Type
	SELECT grd.ObjectID as 'ID', 
	dfp.PageName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd INNER JOIN DetailFieldPages dfp ON dfp.DetailFieldPageID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 12 -- 12=Pages 
	AND grd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Event Types by Lead Type
	SELECT grd.ObjectID as 'ID', 
	et.EventTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd INNER JOIN EventType et ON et.EventTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 13 -- 13=Events 
	AND grd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Encrypted Detail Fields by Lead Type
	SELECT grd.ObjectID as 'ID', 
	df.FieldName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamicEditing grd INNER JOIN DetailFields df ON df.DetailFieldID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 25 -- 25=Encrypted Detail Fields 
	AND grd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Note Types for this Client
	SELECT grd.ObjectID as 'ID', 
	nt.NoteTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd 
	INNER JOIN NoteType nt WITH (NOLOCK) ON nt.NoteTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 26 -- 26=Notes 
	AND @LeadTypeID = -1
	
	UNION
	
	-- Specific: Lead Type rights
	SELECT grd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', -- Virtual function type 23 (variable by lead type)
	grd.GroupRightsDynamicEditingID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID' 
	FROM GroupRightsDynamicEditing grd INNER JOIN LeadType lt ON lt.LeadTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 23 -- 23= Specific Lead Type
	AND grd.LeadTypeID = @LeadTypeID 

	ORDER BY 5, 2, 1

END
























GO
GRANT VIEW DEFINITION ON  [dbo].[GetGroupRightsByFunctionTypeEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetGroupRightsByFunctionTypeEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetGroupRightsByFunctionTypeEditing] TO [sp_executeall]
GO
