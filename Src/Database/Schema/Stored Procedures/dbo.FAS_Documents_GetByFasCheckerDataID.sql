SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FAS_Documents table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_GetByFasCheckerDataID]
(

	@FasCheckerDataID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FasDocumentID],
					[FasCheckerDataID],
					[DocumentBlob]
				FROM
					[dbo].[FAS_Documents] WITH (NOLOCK) 
				WHERE
					[FasCheckerDataID] = @FasCheckerDataID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_GetByFasCheckerDataID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_GetByFasCheckerDataID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_GetByFasCheckerDataID] TO [sp_executeall]
GO
