SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetDroppedOutCustomersThatAnsweredQuestionnaire] @ClientQuestionnaireID int

AS

SELECT     dbo.DroppedOutCustomers.CustomerID, dbo.DroppedOutCustomers.ClientID, dbo.DroppedOutCustomers.TitleID, dbo.DroppedOutCustomers.FirstName, dbo.DroppedOutCustomers.MiddleName, 
                      dbo.DroppedOutCustomers.LastName, dbo.DroppedOutCustomers.EmailAddress, dbo.DroppedOutCustomers.DayTimeTelephoneNumber, dbo.DroppedOutCustomers.HomeTelephone, 
                      dbo.DroppedOutCustomers.MobileTelephone, dbo.DroppedOutCustomers.CompanyTelephone, dbo.DroppedOutCustomers.WorksTelephone,
                      dbo.DroppedOutCustomers.Address1, dbo.DroppedOutCustomers.Address2, dbo.DroppedOutCustomers.Town, dbo.DroppedOutCustomers.County, 
                      dbo.DroppedOutCustomers.PostCode, dbo.DroppedOutCustomers.Test, dbo.DroppedOutCustomers.CompanyName, dbo.DroppedOutCustomers.Occupation, dbo.DroppedOutCustomers.Employer,
                      dbo.DroppedOutCustomers.ReminderEmailSentDate, dbo.DroppedOutCustomers.ReminderSmsSentDate, 
	        dbo.DroppedOutCustomerQuestionnaires.CustomerQuestionnaireID, dbo.DroppedOutCustomerQuestionnaires.ClientQuestionnaireID
FROM         dbo.DroppedOutCustomers INNER JOIN
                      dbo.DroppedOutCustomerQuestionnaires ON dbo.DroppedOutCustomers.CustomerID = dbo.DroppedOutCustomerQuestionnaires.CustomerID
WHERE     (dbo.DroppedOutCustomerQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomersThatAnsweredQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDroppedOutCustomersThatAnsweredQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomersThatAnsweredQuestionnaire] TO [sp_executeall]
GO
