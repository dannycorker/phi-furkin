SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-11-2012
-- Description:	Inserts an event group member
-- =============================================
CREATE PROCEDURE [dbo].[EventGroupMember__Insert]

	@ClientID INT, 
	@EventGroupID INT, 
	@EventTypeID INT, 
	@EventGroupMemberDescription VARCHAR(2000), 
	@WhoCreated INT, 
	@WhenCreated DATETIME, 
	@WhoModified INT, 
	@WhenModified DATETIME, 
	@ChangeNotes VARCHAR(2000), 
	@SourceID INT

AS
BEGIN

	DECLARE @EventGroupMemberID INT

	INSERT INTO EventGroupMember (
		ClientID, 
		EventGroupID, 
		EventTypeID, 
		EventGroupMemberDescription, 
		WhoCreated, 
		WhenCreated, 
		WhoModified, 
		WhenModified, 
		ChangeNotes, 
		SourceID)
	VALUES (
		@ClientID, 
		@EventGroupID, 
		@EventTypeID, 
		@EventGroupMemberDescription, 
		@WhoCreated, 
		@WhenCreated, 
		@WhoModified, 
		@WhenModified, 
		@ChangeNotes, 
		@SourceID)


	SET @EventGroupMemberID = SCOPE_IDENTITY()

END	






GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroupMember__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroupMember__Insert] TO [sp_executeall]
GO
