SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-03-10
-- Description:	Explain the current Event Choices etc for a Case
-- =============================================
CREATE PROCEDURE [dbo].[GetEventThreadsInProgressDebug] 
	
	@LeadID int,
	@CaseNum int = 1 

AS
BEGIN

	DECLARE @CaseID int

	SELECT @CaseID = CaseID 
	FROM Cases 
	WHERE LeadID = @LeadID 
	AND CaseNum = @CaseNum

	SELECT 'Lead Details' as [Lead Details], @LeadID as [Lead ID], @CaseNum as [Case Num], @CaseID as [Case ID]

	SELECT 'Open Threads', le.leadeventid as [From LeadEventID], et.eventtypeid as [From EventTypeID], et.eventtypename as [From EventTypeName]
	FROM Cases c
	INNER JOIN leadevent le on c.caseid = le.caseid 
	INNER JOIN eventtype et on le.eventtypeid = et.eventtypeid 
	WHERE le.whenfollowedup IS NULL
	AND le.EventDeleted = 0 
	AND c.caseid = @caseid

	IF @@ROWCOUNT < 1
	BEGIN
		SELECT 'NO Open Threads'
	END


	SELECT 'All Next Choices', et.eventtypeid as [From EventTypeID], ec.NextEventTypeID as [Next EventTypeID], net.eventtypename as [Next EventTypeName], ec.threadnumber, ec.Description, et.InProcess, et.Enabled as [From Et Enabled], net.Enabled as [Next Et Enabled], net.AvailableManually as [NextEtAvailableManually] 
	FROM Cases c
	INNER JOIN leadevent le on c.caseid = le.caseid 
	INNER JOIN eventtype et on le.eventtypeid = et.eventtypeid 
	INNER JOIN EventChoice ec on ec.EventTypeID = et.EventTypeID AND ec.EscalationEvent = 0
	INNER JOIN EventType net on net.EventTypeID = ec.NextEventTypeID 
	WHERE le.whenfollowedup IS NULL
	AND le.EventDeleted = 0 
	AND et.InProcess = 1 
	and et.Enabled = 1 
	and net.Enabled = 1
	and c.caseid = @caseid
	AND NOT EXISTS (
		-- Partially followed-up event threads must not show event choices that have already happened
		SELECT * 
		FROM LeadEventThreadCompletion letc 
		WHERE letc.CaseID = @caseid
		AND letc.FromEventTypeID = le.eventtypeid
		AND letc.FromLeadEventID = le.leadeventid
		AND letc.ThreadNumberRequired = ec.threadnumber
		AND letc.ToLeadEventID IS NOT NULL 
	)

	IF @@ROWCOUNT < 1
	BEGIN
		SELECT 'NO Next Choices at all'
	END


	-- Relevant list of in-process events.
	-- Excludes redundant events from expired threads
	select 'Valid Threads', le.leadeventid as [leadeventid], et.eventtypeid, et.eventtypename, ec.threadnumber as [threadnumber], ec.Description  
	from cases c 
	inner join leadevent le on c.caseid = le.caseid 
	inner join eventtype et on le.eventtypeid = et.eventtypeid 
	inner join EventChoice ec on ec.EventTypeID = et.EventTypeID AND ec.EscalationEvent = 0
	inner join EventType net on net.EventTypeID = ec.NextEventTypeID 
	where le.whenfollowedup IS NULL
	and le.EventDeleted = 0 
	and et.InProcess = 1 
	and et.Enabled = 1 
	and net.Enabled = 1
	and c.caseid = @caseid
	AND NOT EXISTS (
		-- Partially followed-up event threads must not show event choices that have already happened
		SELECT * 
		FROM LeadEventThreadCompletion letc 
		WHERE letc.CaseID = @caseid
		AND letc.FromEventTypeID = le.eventtypeid
		AND letc.FromLeadEventID = le.leadeventid
		AND letc.ThreadNumberRequired = ec.threadnumber
		AND letc.ToLeadEventID IS NOT NULL 
	)

	/*
	This section has been moved to proc GetEventChoicesByCaseAndEvent now.
	*/
	UNION

	-- Special case where there are no events for this case so far.
	-- Show all possible process-start events for this lead type.
	select 'First Ever Event', -6 as [leadeventid], -6 as [eventtypeid], 'Start' as [eventtypename], 1 as [threadnumber], 'Start' as [description]
	from eventtype fet 
	inner join lead l on l.leadtypeid = fet.leadtypeid 
	inner join cases c on c.leadid = l.leadid 
	where c.caseid = @CaseID 
	and fet.eventsubtypeid = 10 
	and not exists (
		select * from leadevent where caseid = @CaseID
	)
	ORDER BY [leadeventid], [threadnumber]

	IF @@ROWCOUNT < 1
	BEGIN
		SELECT 'NO threads or first event type'
	END


END

























GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventThreadsInProgressDebug] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventThreadsInProgressDebug] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventThreadsInProgressDebug] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[GetEventThreadsInProgressDebug] TO [sp_executehelper]
GO
