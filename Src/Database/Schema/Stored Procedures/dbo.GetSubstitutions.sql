SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetSubstitutions] 

as

select SubstitutionsID, SubstitutionsVariable, SubstitutionsDisplayName, SubstitutionsDescription from Substitutions



GO
GRANT VIEW DEFINITION ON  [dbo].[GetSubstitutions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSubstitutions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSubstitutions] TO [sp_executeall]
GO
