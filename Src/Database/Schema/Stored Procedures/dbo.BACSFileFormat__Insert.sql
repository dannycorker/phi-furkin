SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05/08/2016
-- Description:	Inserts a BACS File Format record
-- =============================================
CREATE PROCEDURE [dbo].[BACSFileFormat__Insert] 
	
	@ClientID INT, 
	@FormatName VARCHAR(250), 
	@FormatDescription VARCHAR(500), 
	@FileGenerationRoutine VARCHAR(250), 
	@WhoCreated INT, 
	@WhenCreated DATETIME,
	@WhoModified INT,
	@WhenModified DATETIME

AS
BEGIN
		
	SET NOCOUNT ON;

    INSERT INTO BACSFileFormat(ClientID, FormatName, FormatDescription, FileGenerationRoutine, WhoCreated, WhenCreated, WhoModified, WhenModified)
    VALUES (@ClientID, @FormatName, @FormatDescription, @FileGenerationRoutine, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BACSFileFormat__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__Insert] TO [sp_executeall]
GO
