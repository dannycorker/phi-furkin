SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AquariumOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_Find]
(

	@SearchUsingOR bit   = null ,

	@AquariumOptionID int   = null ,

	@AquariumOptionTypeID int   = null ,

	@AquariumOptionName varchar (50)  = null ,

	@AquariumOptionDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AquariumOptionID]
	, [AquariumOptionTypeID]
	, [AquariumOptionName]
	, [AquariumOptionDescription]
    FROM
	[dbo].[AquariumOption] WITH (NOLOCK) 
    WHERE 
	 ([AquariumOptionID] = @AquariumOptionID OR @AquariumOptionID IS NULL)
	AND ([AquariumOptionTypeID] = @AquariumOptionTypeID OR @AquariumOptionTypeID IS NULL)
	AND ([AquariumOptionName] = @AquariumOptionName OR @AquariumOptionName IS NULL)
	AND ([AquariumOptionDescription] = @AquariumOptionDescription OR @AquariumOptionDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AquariumOptionID]
	, [AquariumOptionTypeID]
	, [AquariumOptionName]
	, [AquariumOptionDescription]
    FROM
	[dbo].[AquariumOption] WITH (NOLOCK) 
    WHERE 
	 ([AquariumOptionID] = @AquariumOptionID AND @AquariumOptionID is not null)
	OR ([AquariumOptionTypeID] = @AquariumOptionTypeID AND @AquariumOptionTypeID is not null)
	OR ([AquariumOptionName] = @AquariumOptionName AND @AquariumOptionName is not null)
	OR ([AquariumOptionDescription] = @AquariumOptionDescription AND @AquariumOptionDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Find] TO [sp_executeall]
GO
