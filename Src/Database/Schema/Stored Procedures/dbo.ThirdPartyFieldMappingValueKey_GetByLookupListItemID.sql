SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldMappingValueKey table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_GetByLookupListItemID]
(

	@LookupListItemID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyFieldMappingValueKeyID],
					[ClientID],
					[FieldMappingID],
					[DetailValue],
					[LookupListItemID],
					[ThirdPartyKey],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMappingValueKey] WITH (NOLOCK) 
				WHERE
					[LookupListItemID] = @LookupListItemID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByLookupListItemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByLookupListItemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByLookupListItemID] TO [sp_executeall]
GO
