SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypePaymentGateway table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypePaymentGatewayID int   = null ,

	@ClientID int   = null ,

	@EventTypeID int   = null ,

	@ClientPaymentGatewayID int   = null ,

	@TransactionTypeID int   = null ,

	@CardMaskingMethodID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypePaymentGatewayID]
	, [ClientID]
	, [EventTypeID]
	, [ClientPaymentGatewayID]
	, [TransactionTypeID]
	, [CardMaskingMethodID]
    FROM
	[dbo].[EventTypePaymentGateway] WITH (NOLOCK) 
    WHERE 
	 ([EventTypePaymentGatewayID] = @EventTypePaymentGatewayID OR @EventTypePaymentGatewayID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([ClientPaymentGatewayID] = @ClientPaymentGatewayID OR @ClientPaymentGatewayID IS NULL)
	AND ([TransactionTypeID] = @TransactionTypeID OR @TransactionTypeID IS NULL)
	AND ([CardMaskingMethodID] = @CardMaskingMethodID OR @CardMaskingMethodID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypePaymentGatewayID]
	, [ClientID]
	, [EventTypeID]
	, [ClientPaymentGatewayID]
	, [TransactionTypeID]
	, [CardMaskingMethodID]
    FROM
	[dbo].[EventTypePaymentGateway] WITH (NOLOCK) 
    WHERE 
	 ([EventTypePaymentGatewayID] = @EventTypePaymentGatewayID AND @EventTypePaymentGatewayID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([ClientPaymentGatewayID] = @ClientPaymentGatewayID AND @ClientPaymentGatewayID is not null)
	OR ([TransactionTypeID] = @TransactionTypeID AND @TransactionTypeID is not null)
	OR ([CardMaskingMethodID] = @CardMaskingMethodID AND @CardMaskingMethodID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Find] TO [sp_executeall]
GO
