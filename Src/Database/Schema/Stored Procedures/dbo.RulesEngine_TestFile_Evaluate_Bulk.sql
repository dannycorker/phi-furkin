SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:           Ian Slack
-- Create date: 2016-08-18
-- Description:      Evaluates a rulesets based on bulk inputs
-- IS  2016-08-17    Added Evaluate Ruleset By RuleSet/ChangeSet/ByEffectiveDate
-- CPS 2017-11-10    Add cutoff time to prevent timeouts
-- CPS 2017-11-18    Scale the cut off time based on recent failures
-- CPS 2017-11-21    Do the latest file first
-- GPR 2017-12-06	 Locked down to Client 600
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_TestFile_Evaluate_Bulk]
(
       @TestFileID INT = null,
       @BatchSize INT = 10000
)
AS
BEGIN
       
       SET NOCOUNT ON;
       
       DECLARE 
              @Input VARCHAR(2000) = NULL,
              @CustomerID INT = NULL,
              @LeadID INT = NULL,
              @CaseID INT = NULL,
              @MatterID INT = NULL,
              @Overrides dbo.tvpIntVarcharVarchar,
              @OutputXML XML = NULL,
              @EvaluateByEffectiveDate DATE = NULL,
              @EvaluateByChangeSetID INT = NULL,
              @ClientID INT = dbo.fnGetPrimaryClientID(), 
              @RuleSetID INT = NULL,
              @TestFileItemID INT = NULL,
              @RuleID INT = NULL,
              @RuleName VARCHAR(2000), 
              @RuleCheckpoint VARCHAR(2000), 
              @InputValue VARCHAR(2000), 
              @Transform VARCHAR(200), 
              @Value VARCHAR(2000), 
              @OutputValue VARCHAR(2000),
              @TotalRows INT = 0,
              @RowID INT = 1,
              @WhenCreated DATETIME,
              @CutoffTime DATETIME,
              @RecentFailures      INT
              
       ;WITH Last5Results as
       (
       SELECT TOP 5 1-atr.Complete [Failure]
       FROM AutomatedTaskResult atr WITH ( NOLOCK )
       WHERE atr.TaskID = 20840 /*SYS RulesEngine TestFile Evaluate Bulk*/
       )
       SELECT @RecentFailures = SUM(lr.Failure) + 1
       FROM Last5Results lr 
              
       SELECT @Cutofftime = DATEADD(SECOND,240/@RecentFailures,CURRENT_TIMESTAMP) /*240 seconds = 4 minutes.*/

       --DROP TABLE #TestSet
       CREATE TABLE #TestSet (
              RowID INT IDENTITY(1,1),
              RuleSetID INT,
              TestFileItemID INT, 
              TestFileID INT, 
              ClientID INT, 
              Input VARCHAR(2000), 
              CustomerID INT, 
              LeadID INT, 
              CaseID INT, 
              MatterID INT, 
              EvaluateByEffectiveDate DATE, 
              EvaluateByChangeSetID INT,
              ErrorMessage VARCHAR(2000),
              Processed DATETIME
       )
       
       
       -- batch jobs time out after 300 seconds, so 10 seconds per evaluation means we can only do 30 evaluations
       INSERT INTO #TestSet (RuleSetID,TestFileItemID,TestFileID,ClientID,Input,CustomerID,LeadID,CaseID,MatterID,EvaluateByEffectiveDate,EvaluateByChangeSetID,ErrorMessage,Processed)
       SELECT TOP (@BatchSize) tf.RuleSetID,tfi.TestFileItemID,tfi.TestFileID,tfi.ClientID,tfi.Input,tfi.CustomerID,tfi.LeadID,tfi.CaseID,tfi.MatterID,tfi.EvaluateByEffectiveDate,tfi.EvaluateByChangeSetID,NULL,NULL
       FROM RulesEngine_TestFile tf WITH (NOLOCK)
       INNER JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON tf.ChangeSetID = cs.ChangeSetID
       INNER JOIN RulesEngine_TestFileItem tfi WITH (NOLOCK) ON tfi.TestFileID = tf.TestFileID
       LEFT JOIN RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfir.TestFileItemID = tfi.TestFileItemID
       WHERE  tfir.TestFileItemResultID IS NULL
       AND           (tf.TestFileID = @TestFileID OR @TestFileID IS NULL)
       AND (tf.ClientID = @ClientID)
       ORDER BY tf.TestFileID DESC
       
       SELECT @TotalRows = COUNT(*)
       FROM #TestSet
       
       WHILE @RowID <= @TotalRows AND CURRENT_TIMESTAMP < @CutoffTime -- CPS 2017-11-10 add CutoffTime
       BEGIN
              
              SELECT @ClientID = ClientID,
                           @RuleSetID = RuleSetID,
                           @TestFileItemID = TestFileItemID,
                           @Input = Input,
                           @CustomerID = CustomerID,
                           @LeadID = LeadID,
                           @CaseID = CaseID,
                           @MatterID = MatterID,
                           @EvaluateByEffectiveDate = EvaluateByEffectiveDate,
                           @EvaluateByChangeSetID = EvaluateByChangeSetID,
                           @WhenCreated = dbo.fn_GetDate_Local()
              FROM   #TestSet row
              WHERE  row.RowID = @RowID
              
              DELETE FROM @Overrides

              BEGIN TRY
              
                     IF @EvaluateByEffectiveDate IS NOT NULL
                     BEGIN
                           INSERT INTO @Overrides VALUES (0, 'EvaluateByEffectiveDate', @EvaluateByEffectiveDate)
                     END
                     ELSE IF @EvaluateByChangeSetID IS NOT NULL
                     BEGIN
                           INSERT INTO @Overrides VALUES (0, 'EvaluateByChangeSetID', @EvaluateByChangeSetID)
                     END
                     
                     INSERT INTO @Overrides
                     SELECT rp.ParameterTypeID, rp.Value,
                           CASE WHEN rp.DataTypeID = 6 THEN CAST(ll.LookupListItemID AS VARCHAR(250)) ELSE tfo.ParameterValue END
                     FROM   RulesEngine_TestFileItemOverride tfo WITH (NOLOCK)
                     LEFT JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = tfo.RuleParameterID
                     LEFT JOIN DetailFields df WITH (NOLOCK)  ON rp.DataTypeID = 6 AND CAST(df.DetailFieldID AS VARCHAR(250)) = rp.Value
                     LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID AND ll.ItemValue = tfo.ParameterValue
                     WHERE  TestFileItemID = @TestFileItemID

                     EXEC [dbo].[RulesEngine_Evaluate_RuleSet] @RuleSetID, @Input, @CustomerID, @LeadID, @CaseID, @MatterID, 0, @Overrides, @OutputXML OUTPUT

                     SELECT @RuleID = t.r.value('@RuleID[1]', 'INT'), 
                                  @InputValue = t.r.value('@Input[1]', 'VARCHAR(2000)'),
                                  @Transform = t.r.value('@Transform[1]', 'VARCHAR(200)'),
                                  @Value = t.r.value('@Value[1]', 'VARCHAR(2000)'),
                                  @OutputValue = t.r.value('@Output[1]', 'VARCHAR(2000)')
                     FROM   @OutputXML.nodes('//Rule') as t(r)
                     
                     SELECT @RuleName = r.Name,
                                  @RuleCheckpoint = r.RuleCheckpoint
                     FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
                     WHERE RuleID = @RuleID
                     AND ClientID = @ClientID

                     ;WITH DataSet AS
                     (
                           SELECT 
                                  t.r.value('@RuleID[1]', 'INT') RuleID, 
                                  t.r.value('@Input[1]', 'VARCHAR(2000)') InputValue,
                                  t.r.value('@Transform[1]', 'VARCHAR(200)') Transform,
                                  t.r.value('@Value[1]', 'VARCHAR(2000)') Value,
                                  t.r.value('@Output[1]', 'VARCHAR(2000)') OutputValue
                           FROM   @OutputXML.nodes('//Rule') as t(r)
                     )
                     INSERT INTO RulesEngine_TestFileItemResult (TestFileItemID, ClientID, RuleID, RuleName, RuleCheckpoint, InputValue, Transform, Value, OutputValue, WhenCreated, WhenProcessed)
                     SELECT @TestFileItemID, @ClientID, r.RuleID, r.Name, r.RuleCheckpoint, ds.InputValue, ds.Transform, ds.Value, ds.OutputValue, @WhenCreated, dbo.fn_GetDate_Local()
                     FROM   DataSet ds
                     LEFT JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = ds.RuleID AND r.ClientID = @ClientID
                     
                     IF @@ROWCOUNT = 0
                     BEGIN
                     
                           INSERT INTO RulesEngine_TestFileItemResult (TestFileItemID, ClientID, ErrorMessage)
                           SELECT @TestFileItemID, @ClientID, 'Rules engine evaluation produced no output'
                     
                     END
                     
                     
              END TRY
              BEGIN CATCH
              
                     DECLARE @ErrorMessage VARCHAR(2000)
                     SELECT @ErrorMessage = ERROR_MESSAGE()
              
                     INSERT INTO RulesEngine_TestFileItemResult (TestFileItemID, ClientID, ErrorMessage)
                     SELECT @TestFileItemID, @ClientID, @ErrorMessage
              
                     UPDATE #TestSet
                     SET           ErrorMessage = @ErrorMessage
                     WHERE  RowID = @RowID
                     
              END CATCH
              
              UPDATE #TestSet
              SET           Processed = dbo.fn_GetDate_Local()
              WHERE  RowID = @RowID
                     
              SELECT @RowID += 1

       END
       
       SELECT TOP (@BatchSize) *
       FROM   #TestSet ts
       WHERE ts.Processed is not NULL -- CPS 2017-11-10
       
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Evaluate_Bulk] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_TestFile_Evaluate_Bulk] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Evaluate_Bulk] TO [sp_executeall]
GO
