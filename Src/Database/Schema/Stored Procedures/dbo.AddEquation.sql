SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Add an Equation from eCatcher
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[AddEquation]
	@EquationName NVARCHAR(50),
	@Equation TEXT,
	@ClientQuestionnaireID INT,
	@ClientID INT, 
	@WhoEdited INT 
AS
BEGIN

	INSERT INTO Equations (EquationName, Equation, ClientQuestionnaireID, ClientID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@EquationName, @Equation, @ClientQuestionnaireID, @ClientID, @WhoEdited, dbo.fn_GetDate_Local(), @WhoEdited, dbo.fn_GetDate_Local())

	DECLARE @EquationID INT

	SET @EquationID = SCOPE_IDENTITY()

	SELECT EquationID
	FROM Equations
	WHERE EquationID = @EquationID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AddEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddEquation] TO [sp_executeall]
GO
