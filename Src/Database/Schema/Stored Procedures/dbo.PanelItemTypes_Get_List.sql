SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PanelItemTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemTypes_Get_List]

AS


				
				SELECT
					[PanelItemTypeID],
					[PanelItemTypeName]
				FROM
					[dbo].[PanelItemTypes] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemTypes_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Get_List] TO [sp_executeall]
GO
