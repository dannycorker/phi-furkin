SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 20--06-2016
-- Description:	Gets a cusomer payment schedule entry by the given CustomerPaymentScheduleID
-- Modified By AE - 14/09/2016 Added Failure reason
-- =============================================
CREATE PROCEDURE [dbo].[CustomerPaymentSchedule__GetByID]
	@CustomerPaymentScheduleID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT cps.*, p.FailureReason FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	LEFT JOIN Payment p WITH (NOLOCK) on p.CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID
	WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID AND cps.ClientID=@ClientID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__GetByID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerPaymentSchedule__GetByID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__GetByID] TO [sp_executeall]
GO
