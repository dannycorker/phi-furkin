SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Begin Reinstate a Lapsed Policy
-- 2020-06-18 GPR | Update procedure to use WrittenPremium table in place of PremiumDetailHistory
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Renewal_158896]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PurchasedProductID			INT
			,@AccountID						INT
			,@ValueDate						DATE
			,@ValidTo						DATE
			,@RenewalTableRowID				INT
			,@Now							DATETIME = dbo.fn_GetDate_Local()
			,@TableRowID					INT
			,@InceptionDate					DATE
			,@SchemeID						INT
			,@ValueMoney					MONEY
			,@VolExcess						MONEY = 0
			,@OptCover						VARCHAR(MAX)				
			,@NewTableRowID					INT
/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

			
	IF @EventTypeID = 158896
		BEGIN 

		/*GPR 2020-09-20 Don't forget the Wellness History!*/
		EXEC [_C600_PA_Policy_UpdateWellnessHistory] @MatterID, @LeadEventID /*GPR 2020-09-17*/


		/*We have already prepped a renewal for this policy so set the schedule to ignored and add a row to premium detail history*/ 

		SELECT TOP 1 @PurchasedProductID =  p.PurchasedProductID, @AccountID = p.AccountID, @ValueDate = p.ValidFrom , @ValidTo = DATEADD(DAY,-1,DATEADD(Year,1,p.ValidTo))  FROM PurchasedProduct p with (NOLOCK)
		WHERE p.ObjectID = @MatterID 
		Order By PurchasedProductID DESC 

		/*Update The PurchasedProduct's end date and set it's schedules to ignore*/ 
		UPDATE PurchasedProduct 
		SET ValidTo = @ValidTo
		WHERE PurchasedProductID = @PurchasedProductID 

		UPDATE PurchasedProductPaymentSchedule 
		SET PaymentStatusID = 1
		WHERE PurchasedProductID = @PurchasedProductID 
		AND PaymentStatusID IN (3)

		EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @ValueDate, @WhoCreated /*ALM 2020-11-11*/

		/*If this is a migrated policy, the account will have been imported as inactive,
		  Locate these accounts and set them to active*/ 

		  UPDATE a 
		  SET Active = 1 
		  FROM Account a 
		  INNER JOIN PurchasedProduct p WITH (NOLOCK) on a.AccountID = p.AccountID 
		  WHERE p.PurchasedProductID = @PurchasedProductID 
		  AND EXISTS (
			/*ACE 2018-06-15 - Only set up mandates where we have positive payments, if we only have a refund we dont need a mandate*/
			SELECT *
			FROM CustomerPaymentSchedule cps WITH (NOLOCK)
			WHERE cps.AccountID = a.AccountID
			AND cps.PaymentGross > 0.00
			AND cps.PaymentStatusID IN (1,5,2)
		)

		/*Deal with the WP*/
		/*find the renewal Cancel Table Row*/ 
		--SELECT TOP 1 @RenewalTableRowID = tr.tableRowID, @ValueDate = frm.ValueDate FROM TableRows tr WITH (NOLOCK)
		--INNER JOIN TableDetailValues adj with (NOLOCK) on adj.TableROwID = tr.TableRowID AND adj.DetailFIeldID = 175398 /*Adjustment Type*/ 
		--INNER JOIN tabledetailValues frm with (NOLOCK) on frm.TableRowID = tr.TableRowID AND frm.DetailFieldID = 175346 /*Adjustment From Date*/
		--WHERE tr.MatterID = @MatterID 
		--AND tr.DetailFieldID = 175336
		--AND adj.ValueINT = 76523 /*Renewal Cancel*/ 
		--AND frm.ValueDate >= @ValueDate
		--ORDER by tr.TableRowID desc

		--/*Set the to Date on renewal Table row*/ 
		--EXEC _C00_SimpleValueINTOField 175347,@Now,@RenewalTableRowID 

		/*GPR 2020-06-18*/
		DECLARE @WrittenPremiumID INT

		SELECT TOP (1)
		@WrittenPremiumID = wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 5 /*Lapsed*/
		ORDER BY wp.WrittenPremiumID DESC	

		UPDATE WrittenPremium
		SET Active = 1, AdjustmentTypeID = 3
		WHERE WrittenPremiumID = @WrittenPremiumID
		AND MatterID = @MatterID
		AND Active = 0

	--	EXEC TableRows_Insert  @TableRowID OUTPUT, @ClientID,NULL,@MatterID,175336,19011,1,1,NULL,NULL,NULL,NULL,@RenewalTableRowID
	
	--	PRINT @TableRowID 

	--	/*Insert the same values as the renewal row except for the premium and commision*/ 
	--	INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,MatterID,ClientID)
	--	SELECT @TableRowID, tdv.DetailFieldID,tdv.DetailValue, tdv.MatterID,tdv.ClientID FROM TableDetailValues tdv with (NOLOCK) 
	--	WHERE tdv.TableRowID = @RenewalTableRowID 
	--	AND DetailFieldID NOT IN (180170,180171,175397,177899,175398,175347)

	--	/*Set the adjustment amount to be negative*/
	
	--	SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(175397,@RenewalTableRowID) * -1) 
	--	EXEC _C00_SimpleValueIntoField 175397,@ValueMoney,@TableRowID
	
	--	SELECT @ValueMoney = (dbo.fnGetSimpleDVasMoney(177899,@RenewalTableRowID) * -1) 
	--	EXEC _C00_SimpleValueIntoField 177899,@ValueMoney,@TableRowID
		
	--	EXEC _C00_SimpleValueIntoField 175398,76535,@TableRowID

		/*Update the policy status to Live*/ 
		EXEC _C00_SimpleValueIntoField 170038 , 43002 , @MatterID , @WhoCreated 

	--	EXEC _C00_SimpleValueIntoField 177074,@PurchasedProductID,@MatterID,@WhoCreated

	--	EXEC _C00_SimpleValueIntoField 170036, @ValueDate, @MatterID,@WhoCreated

	--	EXEC _C00_SimpleValueIntoField 170037, @ValidTo, @MatterID,@WhoCreated

	--			-- add new row to policy history
	--	SELECT @InceptionDate=dbo.fnGetDvAsDate(170035,@CaseID)

	--	SELECT TOP 1 @SchemeID = tdvScheme.ResourceListID
	--				, @VolExcess = ISNULL(tdvVolExcess.ValueMoney, 0)
	--				, @OptCover=ISNULL(tdvOptCover.DetailValue,'')
	--				,@TableRowID = r.TableRowID 
	--	FROM dbo.TableRows r WITH (NOLOCK) 
	--	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	--	INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 145666
	--	LEFT JOIN dbo.TableDetailValues tdvVolExcess WITH (NOLOCK) ON r.TableRowID = tdvVolExcess.TableRowID AND tdvVolExcess.DetailFieldID = 145667
	--	LEFT JOIN dbo.TableDetailValues tdvOptCover WITH (NOLOCK) ON r.TableRowID = tdvOptCover.TableRowID AND tdvOptCover.DetailFieldID = 175737
	--	WHERE tdvStatus.ValueInt=43004
	--		AND r.MatterID=@MatterID
	--	ORDER BY r.TableRowID DESC

	--	/*Update the last Row to be not live*/ 
	--	EXEC _C00_SimpleValueIntoField 145666, 74536 , @TableRowID , @WhoCreated

	--	SELECT @TableRowID = NULL	

	--	EXEC _C00_1273_Policy_CreatePolicyHistory  @MatterID, @SchemeID, @InceptionDate, @ValueDate, @ValidTo, @ValueDate, @VolExcess, @OptCover

	--	SELECT @NewTableRowID=ISNULL(dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow (@CaseID, @ValueDate),0)

	--	/*Write the PPSID to the table*/ 
	--	EXEC _C00_SimpleValueIntoField 177419,@PurchasedProductID,@NewTableRowID, @WhoCreated

	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_158896] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Renewal_158896] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_158896] TO [sp_executeall]
GO
