SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptStandardTarget_Insert]
(

	@ScriptStandardTargetID int    OUTPUT,

	@ScriptID int   ,

	@ClientID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  
)
AS


				
				INSERT INTO [dbo].[ScriptStandardTarget]
					(
					[ScriptID]
					,[ClientID]
					,[Target]
					,[ObjectName]
					,[PropertyName]
					)
				VALUES
					(
					@ScriptID
					,@ClientID
					,@Target
					,@ObjectName
					,@PropertyName
					)
				-- Get the identity value
				SET @ScriptStandardTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Insert] TO [sp_executeall]
GO
