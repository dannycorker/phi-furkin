SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EquationTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_Insert]
(

	@EquationTargetID int    OUTPUT,

	@ClientID int   ,

	@EquationDetailFieldID int   ,

	@DetailFieldID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@SqlFunctionID int   ,

	@IsEquation bit   ,

	@IsColumnSum bit   ,

	@ColumnFieldID int   ,

	@DetailFieldSubTypeID int   
)
AS


				
				INSERT INTO [dbo].[EquationTarget]
					(
					[ClientID]
					,[EquationDetailFieldID]
					,[DetailFieldID]
					,[Target]
					,[ObjectName]
					,[PropertyName]
					,[SqlFunctionID]
					,[IsEquation]
					,[IsColumnSum]
					,[ColumnFieldID]
					,[DetailFieldSubTypeID]
					)
				VALUES
					(
					@ClientID
					,@EquationDetailFieldID
					,@DetailFieldID
					,@Target
					,@ObjectName
					,@PropertyName
					,@SqlFunctionID
					,@IsEquation
					,@IsColumnSum
					,@ColumnFieldID
					,@DetailFieldSubTypeID
					)
				-- Get the identity value
				SET @EquationTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Insert] TO [sp_executeall]
GO
