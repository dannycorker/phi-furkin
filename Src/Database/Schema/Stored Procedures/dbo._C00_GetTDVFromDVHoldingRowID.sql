SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-27
-- Description:	Get TableDetailValues where a MatterDetailValue holds the TableRowID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetTDVFromDVHoldingRowID] 
	@ClientID int, 
	@AnyID int,
	@DetailFieldSubTypeID int,
	@DetailFieldID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TableRowID int
	
	/* LeadDetailValues holds the TableRowID */
    IF @DetailFieldSubTypeID = 1
    BEGIN
		SELECT TOP 1 @TableRowID = ldv.ValueInt 
		FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
		WHERE ldv.LeadID = @AnyID 
		AND ldv.DetailFieldID = @DetailFieldID 
		AND ldv.ClientID = @ClientID
	END
	
	/* MatterDetailValues holds the TableRowID */
    IF @DetailFieldSubTypeID = 2
    BEGIN
		SELECT TOP 1 @TableRowID = mdv.ValueInt 
		FROM dbo.MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.MatterID = @AnyID 
		AND mdv.DetailFieldID = @DetailFieldID 
		AND mdv.ClientID = @ClientID 
	END
	
	/* Go get the values (if a TableRowID was found) */
	IF @TableRowID > 0
	BEGIN
		/* 
			Decode lookup list items to their display values, but preserve the real
			values from TDV in case they need to be bound to dropdowns etc. 
		*/
		SELECT tdv.*, COALESCE(luli.ItemValue, tdv.DetailValue) AS DisplayValue  
		FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID 
		LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
		WHERE tdv.TableRowID = @TableRowID 
		AND tdv.ClientID = @ClientID 
		ORDER BY tdv.DetailFieldID 
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetTDVFromDVHoldingRowID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetTDVFromDVHoldingRowID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetTDVFromDVHoldingRowID] TO [sp_executeall]
GO
