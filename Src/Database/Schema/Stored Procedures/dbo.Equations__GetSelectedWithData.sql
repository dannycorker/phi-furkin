SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-06-06
-- Description:	Get Equationy stuff for PR for a particular event
--				2013-06-07 ACE Added in the fallback position to allow
--				Events without any fields added to calculate all.
-- MODIFIED:	2014-07-21	SB	Updated to use view
-- MODIFIED:	2015-02-04	ACE	Updated to use customer tables (IE "IN (@LeadTypeID,0)") #30916
--				2015-10-16  ACE Added support for child equations #35012
-- =============================================
CREATE PROCEDURE [dbo].[Equations__GetSelectedWithData]
	@EventTypeID INT,
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@ClientID INT,
	@ClientPersonnelID INT = NULL,
	@ContactID INT = NULL,
	@LeadTypeID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DetailFields TABLE (
		[DetailFieldID] [INT],
		[ClientID] [INT] ,
		[LeadOrMatter] [TINYINT] ,
		[FieldName] [VARCHAR](50) ,
		[FieldCaption] [VARCHAR](100) ,
		[QuestionTypeID] [INT] ,
		[Required] [BIT] ,
		[Lookup] [BIT] ,
		[LookupListID] [INT] ,
		[LeadTypeID] [INT] ,
		[Enabled] [BIT] ,
		[DetailFieldPageID] [INT] ,
		[FieldOrder] [INT] ,
		[MaintainHistory] [BIT] ,
		[EquationText] [VARCHAR](2000) ,
		[MasterQuestionID] [INT] ,
		[FieldSize] [INT] ,
		[LinkedDetailFieldID] [INT] ,
		[ValidationCriteriaFieldTypeID] [INT] ,
		[ValidationCriteriaID] [INT] ,
		[MinimumValue] [VARCHAR](50) ,
		[MaximumValue] [VARCHAR](50) ,
		[RegEx] [VARCHAR](2000) ,
		[ErrorMessage] [VARCHAR](250) ,
		[ResourceListDetailFieldPageID] [INT] ,
		[TableDetailFieldPageID] [INT] ,
		[DefaultFilter] [VARCHAR](250) ,
		[ColumnEquationText] [VARCHAR](2000) ,
		[Editable] [BIT] ,
		[Hidden] [BIT] ,
		[LastReferenceInteger] [INT] ,
		[ReferenceValueFormatID] [INT] ,
		[Encrypt] [BIT] ,
		[ShowCharacters] [INT] ,
		[NumberOfCharactersToShow] [INT] ,
		[TableEditMode] [INT] ,
		[DisplayInTableView] [BIT] 
	)

	DECLARE @InsertCount INT

	EXEC dbo.DetailValues__CreateAll @ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientPersonnelID, @ContactID

	INSERT INTO @DetailFields
	EXEC dbo.DetailFields__GetAllByIDAndEvent @EventTypeID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientID, @ClientPersonnelID, @ContactID

	/*Add this on live*/
    INSERT INTO @DetailFields
    SELECT ef.[DetailFieldID],ef.[ClientID],ef.[LeadOrMatter],ef.[FieldName],ef.[FieldCaption],ef.[QuestionTypeID],ef.[Required],ef.[Lookup],ef.[LookupListID],ef.[LeadTypeID],ef.[Enabled],ef.[DetailFieldPageID],ef.[FieldOrder],ef.[MaintainHistory],ef.[EquationText],ef.[MasterQuestionID],ef.[FieldSize],ef.[LinkedDetailFieldID],ef.[ValidationCriteriaFieldTypeID],ef.[ValidationCriteriaID],ef.[MinimumValue],ef.[MaximumValue],ef.[RegEx],ef.[ErrorMessage],ef.[ResourceListDetailFieldPageID],ef.[TableDetailFieldPageID],ef.[DefaultFilter],ef.[ColumnEquationText],ef.[Editable],ef.[Hidden],ef.[LastReferenceInteger],ef.[ReferenceValueFormatID],ef.[Encrypt],ef.[ShowCharacters],ef.[NumberOfCharactersToShow],ef.[TableEditMode],ef.[DisplayInTableView]
    from dbo.fnDetailFieldsShared(@ClientID) df
    INNER JOIN dbo.fnDetailFieldsShared(@ClientID) ef ON ef.DetailFieldPageID = df.TableDetailFieldPageID and ef.QuestionTypeID = 10
	INNER JOIN EventTypeEquation e WITH (NOLOCK) ON e.DetailFieldID = df.DetailFieldID and e.EventTypeID = @EventTypeID
    where df.LeadTypeID IN (0,@LeadTypeID)
    and df.QuestionTypeID IN (16,19)
    
	IF NOT EXISTS (
		SELECT *
		FROM @DetailFields
    )
    BEGIN
    
		EXEC dbo.Equations__GetAllWithData @CustomerID, @LeadID, @CaseID, @MatterID, @ClientID, @ClientPersonnelID, @ContactID, @LeadTypeID
    
    END
    ELSE
    BEGIN
    
		SELECT @InsertCount = 1

		WHILE @InsertCount > 0 --AND @TestID <=6
		BEGIN
		
			INSERT INTO @DetailFields 
			SELECT ef.[DetailFieldID],ef.[ClientID],ef.[LeadOrMatter],ef.[FieldName],ef.[FieldCaption],ef.[QuestionTypeID],ef.[Required],ef.[Lookup],ef.[LookupListID],ef.[LeadTypeID],ef.[Enabled],ef.[DetailFieldPageID],ef.[FieldOrder],ef.[MaintainHistory],ef.[EquationText],ef.[MasterQuestionID],ef.[FieldSize],ef.[LinkedDetailFieldID],ef.[ValidationCriteriaFieldTypeID],ef.[ValidationCriteriaID],ef.[MinimumValue],ef.[MaximumValue],ef.[RegEx],ef.[ErrorMessage],ef.[ResourceListDetailFieldPageID],ef.[TableDetailFieldPageID],ef.[DefaultFilter],ef.[ColumnEquationText],ef.[Editable],ef.[Hidden],ef.[LastReferenceInteger],ef.[ReferenceValueFormatID],ef.[Encrypt],ef.[ShowCharacters],ef.[NumberOfCharactersToShow],ef.[TableEditMode],ef.[DisplayInTableView]
			FROM @DetailFields df
			INNER JOIN EquationTarget et WITH (NOLOCK) on et.DetailFieldID = df.DetailFieldID
			INNER JOIN DetailFields ef WITH (NOLOCK) ON ef.DetailFieldID = et.EquationDetailFieldID
			WHERE ef.QuestionTypeID = 10
			AND NOT EXISTS (
				SELECT *
				FROM @DetailFields e
				WHERE e.DetailFieldID = ef.DetailFieldID
			)

			SELECT @InsertCount = @@ROWCOUNT

		END
    
		/* 
			The app demands an int for LeadOrMatter, so it falls over trying to convert 1 to an int.
			This CAST helps it out.
		*/
		SELECT df.DetailFieldID, CAST(df.LeadOrMatter as int) as [LeadOrMatter], df.EquationText
		FROM @DetailFields df
		WHERE df.QuestionTypeID = 10
		ORDER BY df.FieldOrder

		SELECT e.EquationDetailFieldID, e.DetailFieldID, e.ColumnFieldID, e.Target, e.IsEquation, e.IsColumnSum, e.DetailFieldSubTypeID, 
			ISNULL( CASE df_target.QuestionTypeID  
			WHEN 4 THEN COALESCE(luli_cdv.ItemValue, luli_ldv.ItemValue, luli_cadv.ItemValue, luli_mdv.ItemValue, luli_cldv.ItemValue, luli_cpdv.ItemValue, luli_codv.ItemValue) 
			WHEN 2 THEN COALESCE(luli_cdv.ItemValue, luli_ldv.ItemValue, luli_cadv.ItemValue, luli_mdv.ItemValue, luli_cldv.ItemValue, luli_cpdv.ItemValue, luli_codv.ItemValue) 
	--		WHEN 5 THEN CONVERT(VARCHAR(10),COALESCE(cdv.ValueDate, ldv.ValueDate, cadv.ValueDate, mdv.ValueDate, cldv.ValueDate, cpdv.ValueDate, codv.ValueDate),103)
			ELSE COALESCE(cdv.DetailValue, ldv.DetailValue, cadv.DetailValue, mdv.DetailValue, cldv.DetailValue, cpdv.DetailValue, codv.DetailValue) 
			END, '') AS DetailValue
		FROM @DetailFields df
		INNER JOIN EquationTarget e WITH (NOLOCK) ON e.EquationDetailFieldID = df.DetailFieldID
		LEFT JOIN fnDetailFieldsShared(@ClientID) df_target ON df_target.DetailFieldID = e.DetailFieldID
		LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK)  ON cdv.DetailFieldID = e.DetailFieldID AND cdv.CustomerID = @CustomerID
		LEFT JOIN LookupListItems luli_cdv WITH (NOLOCK) ON cdv.ValueInt = luli_cdv.LookupListItemID AND luli_cdv.LookupListID = df_target.LookupListID
		LEFT JOIN LeadDetailValues ldv WITH (NOLOCK)  ON ldv.DetailFieldID = e.DetailFieldID AND ldv.LeadID = @LeadID
		LEFT JOIN LookupListItems luli_ldv WITH (NOLOCK) ON ldv.ValueInt = luli_ldv.LookupListItemID AND luli_ldv.LookupListID = df_target.LookupListID
		LEFT JOIN CaseDetailValues cadv WITH (NOLOCK)  ON cadv.DetailFieldID = e.DetailFieldID AND cadv.CaseID = @CaseID
		LEFT JOIN LookupListItems luli_cadv WITH (NOLOCK) ON cadv.ValueInt = luli_cadv.LookupListItemID AND luli_cadv.LookupListID = df_target.LookupListID
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK)  ON mdv.DetailFieldID = e.DetailFieldID AND mdv.MatterID = @MatterID
		LEFT JOIN LookupListItems luli_mdv WITH (NOLOCK) ON mdv.ValueInt = luli_mdv.LookupListItemID AND luli_mdv.LookupListID = df_target.LookupListID
		LEFT JOIN ClientDetailValues cldv WITH (NOLOCK)  ON cldv.DetailFieldID = e.DetailFieldID AND cldv.ClientID = @ClientID
		LEFT JOIN LookupListItems luli_cldv WITH (NOLOCK) ON cldv.ValueInt = luli_cldv.LookupListItemID AND luli_cldv.LookupListID = df_target.LookupListID
		LEFT JOIN ClientPersonnelDetailValues cpdv WITH (NOLOCK)  ON cpdv.DetailFieldID = e.DetailFieldID AND cpdv.ClientPersonnelID = @ClientPersonnelID
		LEFT JOIN LookupListItems luli_cpdv WITH (NOLOCK) ON cpdv.ValueInt = luli_cpdv.LookupListItemID AND luli_cpdv.LookupListID = df_target.LookupListID
		LEFT JOIN ContactDetailValues codv WITH (NOLOCK)  ON codv.DetailFieldID = e.DetailFieldID AND codv.ContactID = @ContactID
		LEFT JOIN LookupListItems luli_codv WITH (NOLOCK) ON codv.ValueInt = luli_codv.LookupListItemID AND luli_codv.LookupListID = df_target.LookupListID
		WHERE df.QuestionTypeID = 10

	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__GetSelectedWithData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations__GetSelectedWithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__GetSelectedWithData] TO [sp_executeall]
GO
