SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================		
-- Author:	Neil Afflick
-- Create date: 2018-05-30
-- Description:	Get the savedQuoteKey given the sessionId
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_GetSavedQuoteKey]
(		
	@SessionID	VARCHAR(30)
)		
AS		
BEGIN
	select SavedQuoteKey from  dbo._C600_SavedQuote where QuoteSessionID = @SessionID;
End





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_GetSavedQuoteKey] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_GetSavedQuoteKey] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_GetSavedQuoteKey] TO [sp_executeall]
GO
