SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-08-25
-- Description:	This proc returns a resource list and all its values - used in the SDK for displaying 'global client values'
-- =============================================
CREATE PROCEDURE [dbo].[ResourceList__GetResourceListDetailFieldValuesByResourceListID]
	@ClientID INT,
	@ResourceListPageID INT
AS
BEGIN
	
	SELECT r.ResourceListID, r.DetailFieldID AS ResourceListDetailFieldID, r.ResourceListDetailValueID, 
			r.DetailValue AS ResourceListDetailValue, COALESCE(f.Encrypt, 0) AS ResourceListEncrypt, r.EncryptedValue AS ResourceListEncryptedValue,
			li.ItemValue AS ResourceListLookupListValue, ll.LookupListID
	FROM 
		dbo.DetailFields f WITH (NOLOCK)
		INNER JOIN dbo.ResourceListDetailValues r WITH (NOLOCK) ON f.DetailFieldID = r.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND r.ValueInt = li.LookupListItemID
	WHERE f.DetailFieldPageID = @ResourceListPageID
	AND f.ClientID = @ClientID
	AND f.Enabled = 1
	ORDER BY r.ResourceListID, f.FieldOrder

END



GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByResourceListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByResourceListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByResourceListID] TO [sp_executeall]
GO
