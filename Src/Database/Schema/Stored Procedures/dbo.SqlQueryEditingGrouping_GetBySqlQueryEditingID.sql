SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingGrouping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingGroupingID],
					[ClientID],
					[SqlQueryEditingID],
					[GroupByClause],
					[HavingClause],
					[HavingColumnID],
					[HavingCriteria1],
					[HavingCriteria2]
				FROM
					[dbo].[SqlQueryEditingGrouping] WITH (NOLOCK) 
				WHERE
					[SqlQueryEditingID] = @SqlQueryEditingID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingID] TO [sp_executeall]
GO
