SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Paul Richardson>
-- Create date: <25-08-2010>
-- Description:	<Gets the parameterized criteria for the given SqlQuery>
-- UPDATES:		Simon Brushett 2011-04-26: Now left join so that parameters can be returned where there is no column - e.g. when getting params to EXEC a proc
--				Simon Brushett 2011-04-26: Also returns the Criteria Name from the sql criteria table
--				Alex Elger 2012-05-16: NOLOCKED SP
-- =============================================
CREATE PROCEDURE [dbo].[SqlQueryCriteria__GetParameterizedByQueryID]
	@QueryID int
AS
BEGIN

	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  dbo.SqlQueryCriteria.SqlQueryCriteriaID, dbo.SqlQueryCriteria.CriteriaSubstitutions,
	        dbo.SqlQueryCriteria.Criteria1, dbo.SqlQueryCriteria.Criteria2, 
	        dbo.SqlQueryCriteria.IsParameterizable, dbo.SqlQueryColumns.ColumnAlias, 
            dbo.SqlQueryColumns.ColumnNaturalName, dbo.SqlQueryColumns.ColumnDataType,
            dbo.SqlQueryCriteria.CriteriaName
    FROM    dbo.SqlQueryCriteria WITH (NOLOCK) 
    LEFT JOIN dbo.SqlQueryColumns WITH (NOLOCK) ON dbo.SqlQueryCriteria.SqlQueryColumnID = dbo.SqlQueryColumns.SqlQueryColumnID
	WHERE  (dbo.SqlQueryCriteria.IsParameterizable = 1) and (dbo.SqlQueryCriteria.SqlQueryID = @QueryID)
END





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria__GetParameterizedByQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteria__GetParameterizedByQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria__GetParameterizedByQueryID] TO [sp_executeall]
GO
