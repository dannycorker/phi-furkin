SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-11-2012
-- Description:	EventGroup Update
-- JWG 2013-01-28 Added EventGroupMember records
-- =============================================
CREATE PROCEDURE [dbo].[EventGroup__Update]

	@EventGroupID INT,
	@ClientID INT,
	@EventGroupName VARCHAR(50), 
	@EventGroupDescription VARCHAR(2000), 
	@WhoCreated INT, 
	@WhenCreated DATETIME, 
	@WhoModified INT, 
	@WhenModified DATETIME, 
	@ChangeNotes VARCHAR(2000), 
	@MembersToAdd TvpIntInt READONLY, 
	@MembersToRemove TvpIntInt READONLY, 
	@SourceID INT = NULL, 
	@LeadTypeID INT = NULL

AS
BEGIN

	UPDATE EventGroup
	SET ClientID = @ClientID, 
		EventGroupName = @EventGroupName, 
		EventGroupDescription = @EventGroupDescription, 
		WhoCreated = @WhoCreated, 
		WhenCreated = @WhenCreated, 
		WhoModified = @WhoModified, 
		WhenModified = @WhenModified, 
		ChangeNotes = @ChangeNotes, 
		SourceID = @SourceID, 
		LeadTypeID = @LeadTypeID
	WHERE EventGroupID = @EventGroupID
	
	/* Add the EventGroupMember records now (if any were passed in) */
	IF EXISTS (SELECT * FROM @MembersToAdd)
	BEGIN
	
		INSERT INTO dbo.EventGroupMember (ClientID, EventGroupID, EventGroupMemberDescription, EventTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		SELECT @ClientID, @EventGroupID, '', m.ID2, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified
		FROM @MembersToAdd m
		
	END
	
	/* Remove any unwanted EventGroupMember records (if any were passed in) */
	IF EXISTS (SELECT * FROM @MembersToRemove)
	BEGIN
	
		DELETE dbo.EventGroupMember 
		FROM dbo.EventGroupMember egm 
		INNER JOIN @MembersToRemove m ON m.ID1 = egm.EventGroupID AND m.ID2 = egm.EventTypeID /* ID1 = group, ID2 = eventtype */
		WHERE egm.ClientID = @ClientID
		AND egm.EventGroupID = @EventGroupID
		
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroup__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__Update] TO [sp_executeall]
GO
