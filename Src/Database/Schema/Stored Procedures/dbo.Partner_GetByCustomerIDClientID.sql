SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Partner table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Partner_GetByCustomerIDClientID]
(

	@CustomerID int   ,

	@ClientID int   
)
AS


				SELECT
					[PartnerID],
					[CustomerID],
					[ClientID],
					[UseCustomerAddress],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[HomeTelephone],
					[MobileTelephone],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[Occupation],
					[Employer],
					[DateOfBirth],
					[FullName],
					[CountryID],
					[WhenModified],
					[WhoModified],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Partner] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_GetByCustomerIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner_GetByCustomerIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_GetByCustomerIDClientID] TO [sp_executeall]
GO
