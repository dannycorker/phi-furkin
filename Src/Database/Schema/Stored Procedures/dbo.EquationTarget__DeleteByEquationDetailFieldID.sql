SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-24
-- Description:	Deletes EquationTargets via the Equation Detail Field ID
-- =============================================
CREATE PROCEDURE [dbo].[EquationTarget__DeleteByEquationDetailFieldID]
(

	@EquationDetailFieldID int   
)
AS


		DELETE FROM [dbo].[EquationTarget] WITH (ROWLOCK) 
		WHERE
			[EquationDetailFieldID] = @EquationDetailFieldID
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget__DeleteByEquationDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget__DeleteByEquationDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget__DeleteByEquationDetailFieldID] TO [sp_executeall]
GO
