SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderFileAbort table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_Get_List]

AS


				
				SELECT
					[DataLoaderFileAbortID],
					[ClientID],
					[DateTimeFileAborted],
					[ReasonForAbort],
					[DataLoaderFileID],
					[FileAbortedOn],
					[LastRowImported]
				FROM
					[dbo].[DataLoaderFileAbort] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Get_List] TO [sp_executeall]
GO
