SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-07
-- Description:	Mark all procs for recompilation
-- 2017-09-18 LB Added @Call flag to enable you to recompile from within another proc, specifying the calling proc so that it's excluded from the generated list 
-- =============================================
CREATE PROCEDURE [dbo].[RecompileAllSprocs]
	@PrintOnly bit = 0,
	@Call VARCHAR(100) = 'All'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @stmnt varchar(500)

	DECLARE SPCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'EXEC sp_recompile [' + i.ROUTINE_NAME + ']'
		FROM INFORMATION_SCHEMA.ROUTINES i 
		WHERE i.ROUTINE_TYPE = 'PROCEDURE'
		AND i.ROUTINE_NAME <> @Call AND i.ROUTINE_NAME <> 'RecompileAllSprocs'
		AND i.ROUTINE_SCHEMA = 'dbo' 
		ORDER BY i.ROUTINE_NAME

	OPEN SPCursor

	FETCH RELATIVE 1 FROM SPCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF @PrintOnly = 0
		BEGIN
			EXEC(@stmnt)
		END
		ELSE
		BEGIN
			PRINT @stmnt
		END

		FETCH NEXT FROM SPCursor INTO @stmnt
		
	END

	CLOSE SPCursor
	DEALLOCATE SPCursor
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileAllSprocs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RecompileAllSprocs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileAllSprocs] TO [sp_executeall]
GO
