SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SMSQuestion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_Delete]
(

	@SMSQuestionID int   
)
AS


				DELETE FROM [dbo].[SMSQuestion] WITH (ROWLOCK) 
				WHERE
					[SMSQuestionID] = @SMSQuestionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Delete] TO [sp_executeall]
GO
