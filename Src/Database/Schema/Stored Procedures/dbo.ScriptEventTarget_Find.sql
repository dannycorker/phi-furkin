SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptEventTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptEventTargetID int   = null ,

	@ScriptID int   = null ,

	@SectionID int   = null ,

	@ClientID int   = null ,

	@Target varchar (250)  = null ,

	@EventTypeID int   = null ,

	@EventTypeName varchar (250)  = null ,

	@EventSubtypeID int   = null ,

	@Format varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptEventTargetID]
	, [ScriptID]
	, [SectionID]
	, [ClientID]
	, [Target]
	, [EventTypeID]
	, [EventTypeName]
	, [EventSubtypeID]
	, [Format]
    FROM
	[dbo].[ScriptEventTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptEventTargetID] = @ScriptEventTargetID OR @ScriptEventTargetID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([SectionID] = @SectionID OR @SectionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([EventTypeName] = @EventTypeName OR @EventTypeName IS NULL)
	AND ([EventSubtypeID] = @EventSubtypeID OR @EventSubtypeID IS NULL)
	AND ([Format] = @Format OR @Format IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptEventTargetID]
	, [ScriptID]
	, [SectionID]
	, [ClientID]
	, [Target]
	, [EventTypeID]
	, [EventTypeName]
	, [EventSubtypeID]
	, [Format]
    FROM
	[dbo].[ScriptEventTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptEventTargetID] = @ScriptEventTargetID AND @ScriptEventTargetID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([SectionID] = @SectionID AND @SectionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([EventTypeName] = @EventTypeName AND @EventTypeName is not null)
	OR ([EventSubtypeID] = @EventSubtypeID AND @EventSubtypeID is not null)
	OR ([Format] = @Format AND @Format is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Find] TO [sp_executeall]
GO
