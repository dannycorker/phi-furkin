SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-02-02
-- Description:	Handle response from create customer
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BrainTree_HandleXMLPaymentMethodCreateResponse]
	@MatterID INT,
	@ClientPersonnelID INT,
	@BrainTreePaymentMethodCreateID INT,
	@XMLPayload VARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE c
	SET ResponseXML = @XMLPayload
	FROM BrainTreePaymentMethodCreate c
	WHERE c.BrainTreePaymentMethodCreateID = @BrainTreePaymentMethodCreateID

END
GO
