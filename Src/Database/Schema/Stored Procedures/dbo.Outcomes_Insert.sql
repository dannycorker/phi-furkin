SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Outcomes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Outcomes_Insert]
(

	@OutcomeID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@OutcomeName varchar (50)  ,

	@OutcomeDescription varchar (MAX)  ,

	@SmsWhenFound bit   ,

	@MobileNumber varchar (50)  ,

	@EmailCustomer bit   ,

	@CustomersEmail varchar (MAX)  ,

	@ThankYouPage varchar (MAX)  ,

	@EmailFromAddress varchar (255)  ,

	@SmsToCustomer varchar (160)  ,

	@CustomersEmailSubject varchar (500)  ,

	@ClientID int   ,

	@EmailClient bit   ,

	@ClientEmail varchar (255)  ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[Outcomes]
					(
					[ClientQuestionnaireID]
					,[OutcomeName]
					,[OutcomeDescription]
					,[SmsWhenFound]
					,[mobileNumber]
					,[EmailCustomer]
					,[CustomersEmail]
					,[ThankYouPage]
					,[EmailFromAddress]
					,[SmsToCustomer]
					,[CustomersEmailSubject]
					,[ClientID]
					,[EmailClient]
					,[ClientEmail]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@OutcomeName
					,@OutcomeDescription
					,@SmsWhenFound
					,@MobileNumber
					,@EmailCustomer
					,@CustomersEmail
					,@ThankYouPage
					,@EmailFromAddress
					,@SmsToCustomer
					,@CustomersEmailSubject
					,@ClientID
					,@EmailClient
					,@ClientEmail
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @OutcomeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Outcomes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Insert] TO [sp_executeall]
GO
