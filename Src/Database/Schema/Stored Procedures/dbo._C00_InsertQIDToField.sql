SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex ELger
-- Create date: 2010-07-26
-- Description:	Insert ClientQuestionnaireID into Field
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertQIDToField]
(
	@LeadEventID int,
	@QuestionnaireFieldID int
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE	@QuestionnaireID varchar(2000) 

	SELECT @QuestionnaireID = cq.ClientQuestionnaireID
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = le.LeadID 
	INNER JOIN dbo.CustomerQuestionnaires cq WITH (NOLOCK) on cq.CustomerID = l.CustomerID 
	WHERE le.LeadEventID = @LeadEventID 
	
	IF @QuestionnaireID IS NOT NULL
	BEGIN
	
		EXEC dbo._C00_ValueIntoField @LeadEventID, @QuestionnaireFieldID, @QuestionnaireID
		
	END
		
	/*Declare	@myERROR int,
	@LeadID int,
	@LeadTypeID int,
	@ClientID int,
	@MatterID int,
	@LeadOrMatterField varchar(100)

	Select
	@LeadID = Lead.LeadID,
	@LeadTypeID = Lead.LeadTypeID,
	@ClientID = Lead.ClientID,
	@MatterID = Matter.MatterID
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	inner join Matter on Matter.LeadID = Lead.LeadID
	where leadevent.leadeventid = @LeadEventID 

	Select 
	@LeadOrMatterField = LeadOrMatter
	From DetailFields 
	Where DetailFieldID = @CampaignFieldID


	if @LeadOrMatterField = 1
	Begin

		/* Insert Tracking ID's into Lead Feild*/
		INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		select @ClientID, @LeadID, @CampaignFieldID, Tracking.TrackingName
		FROM Lead
		INNER JOIN Customers on Customers.CustomerID = Lead.CustomerID
		INNER JOIN CustomerQuestionnaires on CustomerQuestionnaires.CustomerID = Customers.CustomerID
		INNER JOIN Tracking on Tracking.TrackingID = CustomerQuestionnaires.TrackingID
		where lead.leadid = @LeadID

	end

	if @LeadOrMatterField = 2
	Begin

		/* Insert Tracking ID's into Matter Feild*/
		INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		select @ClientID, @LeadID, @MatterID, @CampaignFieldID, Tracking.TrackingName
		FROM Lead
		INNER JOIN Customers on Customers.CustomerID = Lead.CustomerID
		INNER JOIN CustomerQuestionnaires on CustomerQuestionnaires.CustomerID = Customers.CustomerID
		INNER JOIN Tracking on Tracking.TrackingID = CustomerQuestionnaires.TrackingID
		where lead.leadid = @LeadID

	END*/
	
	RETURN @QuestionnaireID
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertQIDToField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertQIDToField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertQIDToField] TO [sp_executeall]
GO
