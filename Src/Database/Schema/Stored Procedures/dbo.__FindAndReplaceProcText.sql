SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-02-11
-- Description:	Replace text within procs. Useful for updating all references to [AquariusMaster] with [AquariusMasterPreLiveFebruary],
--              or [AquariusMaster] with [467998-SQLCLUS2\SQL2].[AquariusMaster] for example.
-- JWG 2015-01-07 Debug mode and Dev and ESCAPE '!' added to cater for [Server] appearing as a regex to sql server.
-- =============================================
CREATE PROCEDURE [dbo].[__FindAndReplaceProcText]
	@SearchName sysname = 'AquariusMaster.', 
	@ReplaceName sysname = 'AquariusMasterPreLiveMarch.', 
	@DebugOnly BIT = 1
AS
BEGIN
	SET NOCOUNT ON;

	/*IF DB_NAME() NOT LIKE '%PreLive%' AND DB_NAME() NOT LIKE '%Dev%'
	BEGIN
		PRINT 'This is too dangerous to run on a live database!'
		RETURN
	END*/
	
	/* List all matching proc names */
	DECLARE @Procs TABLE (ProcName NVARCHAR(200), ProcText NVARCHAR(MAX))
	
	DECLARE @rc INT, 
	@p NVARCHAR(200), 
	@s NVARCHAR(MAX)
	
	/* List all procs that reference the live AquariusMaster database */
	INSERT @Procs (ProcName, ProcText) 
	SELECT OBJECT_NAME(OBJECT_ID), REPLACE(REPLACE(m.definition, 'CREATE ', 'ALTER '), @SearchName, @ReplaceName) 
	FROM sys.all_sql_modules m WITH (NOLOCK) 
	WHERE m.definition LIKE '%' + REPLACE(@SearchName, '[', '![') + '%' ESCAPE '!' 
	AND OBJECT_NAME(OBJECT_ID) NOT IN ('_C38_ClientCreate', '__FindAndReplaceProcText') /* Exclude this proc! */
	ORDER BY OBJECT_NAME(OBJECT_ID) 

	/* Capture how many loops are required */
	SET @rc = @@ROWCOUNT

	/* SET commands once only */
	SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
		
	/*
		Loop round all the procs in order, EXECing the new definition for each one
	*/
	WHILE @rc > 0
	BEGIN
		
		SELECT TOP 1 @p = p.ProcName, 
		@s = p.ProcText 
		FROM @Procs p 
		
		/* Show the proc name for later checking */
		PRINT @p
		
		IF @DebugOnly = 1
		BEGIN
			/* Show the new text as it will appear */
			PRINT ''
			PRINT @s
			PRINT ''
		END
		ELSE
		BEGIN
		
			/* EXEC the sp_helptext statement and store the results in the first table */
			EXEC (@s)
			
		END
		
		/* Remove this proc from the to-do list */
		DELETE @Procs WHERE ProcName = @p
		
		SELECT @rc -= 1
	END	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[__FindAndReplaceProcText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__FindAndReplaceProcText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__FindAndReplaceProcText] TO [sp_executeall]
GO
