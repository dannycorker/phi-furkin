SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-06-01
-- Description:	Create a complete customer with all appropriate sub-tables using dummy data
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateCustomerComplete]
	@ClientID INT,
	@ClientPersonnelID INT,
	@LeadTypeID INT,
	@Debug BIT = 1
AS
BEGIN

	SET NOCOUNT ON;

	/*
	ACE 2014-06-01
	
	The purpose of this script is to create a Customer, Lead, Case, Matter, Event history and all detail values.
	
	Tables included are as follows:
		
		Customers 
			CustomerDetailValues 
			CustomerMatchKey 
			CustomerOffice 
			Department 
				Contact
					ContactDetailValues
			Partner 
				PartnerMatchKey 
			LeadDetailValues 
			LeadDocument/LeadDocumentFS 
				LeadDocumentEsignatureStatus 
				CaseDetailValues 
				LeadEvent (no trigger please!) 
				LeadEventThreadCompletion 
					Matter
					MatterDetailValues 
					MatterPageChoice 
			PortalUser 
				PortalUserCase 
				PortalUserOption 
			TableRows 
				TableDetailValues 
			ClientPersonnelFavourite
			LeadViewHistory
			DiaryAppointment
			DiaryReminder
				
		eCatcher tables might need to be included one day, but no requirement for them as yet.
	*/
	
	DECLARE 
	@NewCustomerID INT,
	@NewPartnerID INT,
	@NewLeadID INT, 
	@NewCaseID INT, 
	@NewMatterID INT, 
	@NewPortalUserID INT, 
	@NewLeadDocumentID INT,
	@NewDepartmentID INT,
	@NewContactID INT,
	@DetailFieldID INT,
	@DateTime DATETIME = dbo.fn_GetDate_Local(),
	@VarBinary VARBINARY(MAX) = convert(varbinary(max),''),
	@EventTypeID INT,
	@FromLeadEventID INT,
	@DetailFieldPageID INT,
	@TableRowID INT,
	@DiaryAppointmentID INT,
	@LogEntry VARCHAR(200)
	
	IF (SELECT ClientID FROM ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @ClientPersonnelID) <> @ClientID
	BEGIN
	
		/*Client and CP mismatch reject creation*/
		SELECT @LogEntry = 'Error:  The ClientPersonnelID and ClientID dont match.'
		RAISERROR( @LogEntry, 16, 1 )
		RETURN
	
	END
	
	IF (SELECT ClientID FROM LeadType lt WITH (NOLOCK) WHERE lt.LeadTypeID = @LeadTypeID) <> @ClientID
	BEGIN
	
		/*Client and CP mismatch reject creation*/
		SELECT @LogEntry = 'Error:  The LeadTypeID and ClientID dont match.'
		RAISERROR( @LogEntry, 16, 1 )
		RETURN
	
	END
	
	/* Copy the Customer first of all */
	INSERT INTO dbo.Customers(ClientID, TitleID, IsBusiness, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, HomeTelephone, HomeTelephoneVerifiedAndValid, MobileTelephone, MobileTelephoneVerifiedAndValid, CompanyTelephone, CompanyTelephoneVerifiedAndValid, WorksTelephone, WorksTelephoneVerifiedAndValid, Address1, Address2, Town, County, PostCode, Website, HasDownloaded, DownloadedOn, AquariumStatusID, ClientStatusID, Test, CompanyName, Occupation, Employer, PhoneNumbersVerifiedOn, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, DefaultContactID, DefaultOfficeID, AddressVerified, CountryID, SubClientID, CustomerRef, WhoChanged, WhenChanged, ChangeSource, EmailAddressVerifiedAndValid, EmailAddressVerifiedOn, Comments, AllowSmsCommandProcessing)	
	SELECT @ClientID, 1, 1, 'Fred', 'Test', 'Smith', 'fred@smith.com', '01619275620', NULL, '01619275620', NULL, '', NULL, NULL, NULL, NULL, NULL, '126a Ashley Rd', '', 'Hale', '', 'WA14 2UN', '', 0, NULL, 2, NULL, 0, '', 'Employed', 'Aquarium Software', NULL, NULL, NULL, NULL, '2014-06-02', NULL, NULL, 0, NULL, NULL, '', @ClientPersonnelID, dbo.fn_GetDate_Local(), 'Added By _C00_CreateCustomerComplete', NULL, NULL, '', NULL
	
	SELECT @NewCustomerID = SCOPE_IDENTITY()
	
	IF @Debug = 1
	BEGIN
		PRINT 'Customer done CustomerID: ' + CONVERT(VARCHAR,@NewCustomerID)
	END
	
	SELECT @DetailFieldID = df.DetailFieldID 
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 10
	AND df.QuestionTypeID NOT IN (6,16,19) /*Date Range, Table, Basic Table*/
	
	IF @DetailFieldID > 0
	BEGIN
	
		/* CustomerDetailValues */
		INSERT dbo.CustomerDetailValues(ClientID, CustomerID, DetailFieldID, DetailValue)
		SELECT @ClientID, @NewCustomerID, @DetailFieldID, 'CDV DetailValue'
		
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, CustomerID) 
		SELECT @ClientID, @DetailFieldID, 10, 'CDV DetailValue', dbo.fn_GetDate_Local(), @ClientPersonnelID, @NewCustomerID 
		
		INSERT INTO DetailFieldLock (ClientID, DetailFieldID, DetailFieldPageID, LeadOrMatter, LeadOrMatterID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@ClientID, @DetailFieldID, 0, 10, @NewCustomerID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		IF @Debug = 1
		BEGIN
			PRINT 'Customer Detail Value Created. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID)
		END
		
		SELECT @DetailFieldID = NULL
	
	END
	
	/* CustomerMatchKey */
	INSERT dbo.CustomerMatchKey(ClientID, CustomerID, FirstName, LastName, Address1, Postcode, DateOfBirth)
	SELECT @ClientID, @NewCustomerID, 'Fred', 'Smith', 'Address1', 'Postcode', dbo.fn_GetDate_Local()

	IF @Debug = 1
	BEGIN
		PRINT 'Customer match key done'
	END
	
	/* CustomerOffice */
	INSERT dbo.CustomerOffice(ClientID, CustomerID, OfficeName, MainPhoneNumber, FaxNumber, Address1, Address2, Town, County, Postcode, Country, Notes, CountryID)
	SELECT @ClientID, @NewCustomerID, 'Poplar House', '01619275620', '01619275620', '126a Ashley Rd', 'Hale', '', '', 'WA14 2UN', 'UK', 'No Notes', 232
	
	IF @Debug = 1
	BEGIN
		PRINT 'Customer office done'
	END
	
	/* Department */
	INSERT dbo.Department(ClientID, CustomerID, DepartmentName, DepartmentDescription)
	SELECT @ClientID, @NewCustomerID, 'IT Support', 'IT Support Department'

	SET @NewDepartmentID = SCOPE_IDENTITY()
	
	IF @Debug = 1
	BEGIN
		PRINT 'Department done. ID: ' + CONVERT(VARCHAR,@NewDepartmentID)
	END

	/* Create new Contact */
	INSERT dbo.Contact(ClientID, CustomerID, TitleID, Firstname, Middlename, Lastname, EmailAddressWork, EmailAddressOther, DirectDial, MobilePhoneWork, MobilePhoneOther, Address1, Address2, Town, County, Postcode, Country, OfficeID, DepartmentID, JobTitle, Notes, CountryID, LanguageID)
	SELECT @ClientID, @NewCustomerID, 1, 'Bob', 'Test Contact', 'Smith', 'bob.smith@aquarium-software.com', 'bob.smith@gmail.com', '01619275620', '07767620304', '', '126a Ashley Rd', 'Hale', '', '', 'WA14 2UN', '', NULL, NULL, 'Builder', 'No notes for Bob the builder', 232, NULL
	
	SET @NewContactID = SCOPE_IDENTITY()
	
	IF @Debug = 1
	BEGIN
		PRINT 'Contact done. ID: ' + CONVERT(VARCHAR,@NewContactID)
	END
	
	SELECT @DetailFieldID = df.DetailFieldID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 14
	AND df.QuestionTypeID NOT IN (6,16,19) /*Date Range, Table, Basic Table*/
	
	IF @DetailFieldID > 0
	BEGIN

		/* ContactDetailValues */
		INSERT dbo.ContactDetailValues(ClientID, ContactID, DetailFieldID, DetailValue)
		SELECT @ClientID, @NewContactID, @DetailFieldID, 'A Contact DetailValue'
	
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, ContactID) 
		SELECT @ClientID, @DetailFieldID, 14, 'A Contact DetailValue', dbo.fn_GetDate_Local(), @ClientPersonnelID, @NewContactID 

		INSERT INTO DetailFieldLock (ClientID, DetailFieldID, DetailFieldPageID, LeadOrMatter, LeadOrMatterID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@ClientID, @DetailFieldID, 0, 14, @NewContactID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		IF @Debug = 1
		BEGIN
			PRINT 'ContactDetailValues done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID)
		END
	
		SELECT @DetailFieldID = NULL
	
	END
	
	/* Create the partner record */
	INSERT INTO dbo.Partner (CustomerID, ClientID, UseCustomerAddress, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone, Address1, Address2, Town, County, PostCode, Occupation, Employer, DateOfBirth, CountryID)
	SELECT @NewCustomerID, @ClientID, 1, 1, 'Mary', 'Test', 'Smith', 'mary.smith@gmail.com', '01619275620', '01619275620', '07767620304', NULL, NULL, NULL, NULL, NULL, 'House wife..', 'Bob', dbo.fn_GetDate_Local(), 232
	
	SELECT @NewPartnerID = SCOPE_IDENTITY()

	IF @Debug = 1
	BEGIN
		PRINT 'Partner done. PartnerID: ' + CAST(@NewPartnerID AS VARCHAR) 
	END

	/* PartnerMatchKey */
	INSERT dbo.PartnerMatchKey(ClientID, PartnerID, FirstName, LastName, Address1, Postcode, DateOfBirth)
	SELECT @ClientID, @NewPartnerID, 'Fred', 'Smith', 'Address1', 'Postcode', dbo.fn_GetDate_Local()

	IF @Debug = 1
	BEGIN
		PRINT 'Partner match key done'
	END

	/* Create lead */
	INSERT dbo.Lead(ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
	SELECT @ClientID, 'Lead Created By _C00_CreateCustomerComplete', @NewCustomerID, @LeadTypeID, 2, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, dbo.fn_GetDate_Local()
	
	SELECT @NewLeadID = SCOPE_IDENTITY()

	IF @Debug = 1
	BEGIN
		PRINT 'Lead done. LeadID: ' + CAST(@NewLeadID AS VARCHAR) 
	END
		
	SELECT @DetailFieldID = df.DetailFieldID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 1
	AND df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID NOT IN (6,16,19) /*Date Range, Table, Basic Table*/
	
	IF @DetailFieldID > 0
	BEGIN

		/* LeadDetailValues */
		INSERT dbo.LeadDetailValues(ClientID, LeadID, DetailFieldID, DetailValue)
		SELECT @ClientID, @NewLeadID, @DetailFieldID, 'Lead DetailValue'
	
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, LeadID) 
		SELECT @ClientID, @DetailFieldID, 1, 'Lead DetailValue', dbo.fn_GetDate_Local(), @ClientPersonnelID, @NewLeadID 

		INSERT INTO DetailFieldLock (ClientID, DetailFieldID, DetailFieldPageID, LeadOrMatter, LeadOrMatterID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@ClientID, @DetailFieldID, 0, 1, @NewLeadID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		IF @Debug = 1
		BEGIN
			PRINT 'LeadDetailValues done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID)
		END
	
		SELECT @DetailFieldID = NULL
	
	END

	EXEC @NewLeadDocumentID = dbo._C00_CreateLeadDocument @ClientID, @NewLeadID, @VarBinary, 'Test Document', NULL, @DateTime, @ClientPersonnelID, 'Example File'
	
	IF @Debug = 1
	BEGIN
		PRINT 'LeadDocument done. LeadDocumentID: ' + CAST(@NewLeadDocumentID AS VARCHAR) 
	END
	
	INSERT INTO LeadDocumentEsignatureStatus (ClientID, LeadDocumentID, ElectronicSignatureDocumentKey, DocumentName, SentTo, DateSent, EsignatureStatusID, StatusCheckedDate)
	VALUES (@ClientID, @NewLeadDocumentID, 'MASH', 'TestDocument', 'sales@aquarium-software.com', dbo.fn_GetDate_Local(), 1, dbo.fn_GetDate_Local())
	
	IF @Debug = 1
	BEGIN
		PRINT 'LeadDocumentEsignatureStatus done'
	END

	/* Create new case as required.*/
	INSERT dbo.Cases(LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
	SELECT @NewLeadID, @ClientID, 1, 'Case 1', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), NULL

	SELECT @NewCaseID = SCOPE_IDENTITY()

	IF @Debug = 1
	BEGIN
		PRINT 'Case done. New CaseID: ' + CAST(@NewCaseID AS VARCHAR) 
	END

	SELECT @DetailFieldID = df.DetailFieldID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 11
	AND df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID NOT IN (6,16,19) /*Date Range, Table, Basic Table*/
	
	IF @DetailFieldID > 0
	BEGIN

		/* CaseDetailValues */
		INSERT dbo.CaseDetailValues(ClientID, CaseID, DetailFieldID, DetailValue)
		SELECT @ClientID, @NewCaseID, @DetailFieldID, 'Case DetailValue'
	
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, CaseID) 
		SELECT @ClientID, @DetailFieldID, 11, 'Case DetailValue', dbo.fn_GetDate_Local(), @ClientPersonnelID, @NewCaseID 

		INSERT INTO DetailFieldLock (ClientID, DetailFieldID, DetailFieldPageID, LeadOrMatter, LeadOrMatterID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@ClientID, @DetailFieldID, 0, 11, @NewCaseID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		IF @Debug = 1
		BEGIN
			PRINT 'CaseDetailValues done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID)
		END
	
		SELECT @DetailFieldID = NULL
	
	END

	/* LeadEvent & LETC */
	IF @Debug = 1
	BEGIN
		PRINT 'Starting LeadEvents'
	END

	/* Set CONTEXT_INFO so that the LeadEvent trigger knows we are just adding some records here */
	DECLARE @ContextInfo VARBINARY(100) = CAST('NoSAE' AS VARBINARY)
	SET CONTEXT_INFO @ContextInfo
	
	/*Start off with a letter out*/
	SELECT TOP 1 @EventTypeID = et.EventTypeID
	FROM EventType et WITH (NOLOCK) 
	WHERE et.LeadTypeID = @LeadTypeID 
	AND et.EventSubtypeID = 4
	AND et.InProcess = 0
	
	IF @EventTypeID > 0
	BEGIN
	
		/* Now we need a letter out event.. */
		INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
		SELECT @ClientID, @NewLeadID, dbo.fn_GetDate_Local(), @ClientPersonnelID, NULL, '', @EventTypeID, NULL, NULL, NULL, NULL, NULL, @NewCaseID, @NewLeadDocumentID, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
		
		SELECT @FromLeadEventID = SCOPE_IDENTITY()

		IF @Debug = 1
		BEGIN
			PRINT 'Letter out event done. LeadEvent ID: ' + CONVERT(VARCHAR,@FromLeadEventID) + ' EventtypeID ' + CONVERT(VARCHAR,@EventTypeID)
		END
	
		SELECT @EventTypeID = NULL
	
	END
	
	/*Now we need a process start*/
	SELECT TOP 1 @EventTypeID = et.EventTypeID
	FROM EventType et WITH (NOLOCK) 
	WHERE et.LeadTypeID = @LeadTypeID 
	AND et.EventSubtypeID = 10
	
	IF @EventTypeID > 0
	BEGIN
	
		/* Create the new LeadEvent Process Start */
		INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
		SELECT @ClientID, @NewLeadID, dbo.fn_GetDate_Local(), @ClientPersonnelID, NULL, '', @EventTypeID, NULL, NULL, NULL, NULL, NULL, @NewCaseID, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

		SELECT @FromLeadEventID = SCOPE_IDENTITY()

		IF @Debug = 1
		BEGIN
			PRINT 'Process start event done. LeadEvent ID: ' + CONVERT(VARCHAR,@FromLeadEventID) + ' EventtypeID ' + CONVERT(VARCHAR,@EventTypeID)
		END
	
		/* Now we will do LETC..*/
		INSERT INTO LeadEventThreadCompletion (ClientID, LeadID, CaseID, FromLeadEventID, FromEventTypeID, ThreadNumberRequired, ToLeadEventID, ToEventTypeID)
		VALUES (@ClientID, @NewLeadID, @NewCaseID, @FromLeadEventID, @EventTypeID, 1, NULL, NULL)

		IF @Debug = 1
		BEGIN
			PRINT 'LETC done'
		END
	
	END

	/* Clear the CONTEXT_INFO  */
	SET CONTEXT_INFO 0x0

	/* Create Matter record. */
	INSERT dbo.Matter(ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	SELECT @ClientID, 'Matter created by _C00_CreateCustomerComplete', @NewCustomerID, @NewLeadID, 2, 'A', 0, @NewCaseID, @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local()
	
	SELECT @NewMatterID = SCOPE_IDENTITY()

	IF @Debug = 1
	BEGIN
		PRINT 'Matter done. MatterID: ' + CAST(@NewMatterID AS VARCHAR) 
	END
	
	SELECT @DetailFieldID = df.DetailFieldID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 2
	AND df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID NOT IN (6,16,19) /*Date Range, Table, Basic Table*/
	
	IF @DetailFieldID > 0
	BEGIN

		/* MatterDetailValues */
		INSERT dbo.MatterDetailValues(ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		SELECT @ClientID, @NewLeadID, @NewMatterID, @DetailFieldID, 'Matter DetailValue'
	
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, MatterID) 
		SELECT @ClientID, @DetailFieldID, 2, 'Matter DetailValue', dbo.fn_GetDate_Local(), @ClientPersonnelID, @NewMatterID 

		INSERT INTO DetailFieldLock (ClientID, DetailFieldID, DetailFieldPageID, LeadOrMatter, LeadOrMatterID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@ClientID, @DetailFieldID, 0, 2, @NewMatterID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		IF @Debug = 1
		BEGIN
			PRINT 'MatterDetailValues done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID)
		END
	
		SELECT @DetailFieldID = NULL
	
	END

	SELECT @DetailFieldPageID = dfp.DetailFieldPageID
	FROM DetailFieldPages dfp WITH (NOLOCK) 
	WHERE dfp.LeadTypeID = @LeadTypeID
	AND dfp.LeadOrMatter = 2
	
	IF @DetailFieldPageID > 0
	BEGIN

		/* MatterPageChoice */
		INSERT dbo.MatterPageChoice(ClientID, MatterID, DetailFieldPageID, WhoModified, WhenModified) 
		SELECT @ClientID, @NewMatterID, @DetailFieldPageID, @ClientPersonnelID, dbo.fn_GetDate_Local() 
		
		IF @Debug = 1
		BEGIN
			PRINT 'MatterPage Choice done. Page ID: ' + CONVERT(VARCHAR,@DetailFieldPageID)
		END
	
		SELECT @DetailFieldPageID = NULL

	END
							
	/* PortalUser */
	/* We cannot insert into this directly.  Call the helper proc to set up an account in the AquariusMaster database first. */
	EXEC @NewPortalUserID = [PortalUser_Insert_Helper] @ClientID, @NewCustomerID, @NewCustomerID, 'A', 'A', @ClientPersonnelID, 0, 0, NULL

	IF @Debug = 1
	BEGIN
		PRINT 'PortalUser Done. Portal User ID: ' + CONVERT(VARCHAR,@NewPortalUserID)
	END
	
	/* PortalUserCase */
	INSERT INTO dbo.PortalUserCase(PortalUserID, LeadID, CaseID, ClientID)
	SELECT @NewPortalUserID, @NewLeadID, @NewCaseID, @ClientID 
			
	IF @Debug = 1
	BEGIN
		PRINT 'PortalUserCase done'
	END
			
	/* PortalUserOption */
	INSERT INTO dbo.PortalUserOption(ClientID, PortalUserID, PortalUserOptionTypeID, OptionValue)
	SELECT @ClientID, @NewPortalUserID, 1, '654564a5dsas4DSA64'

	IF @Debug = 1
	BEGIN
		PRINT 'PortalUserOption done'
	END
			
	/* Table Rows */
	IF @Debug = 1
	BEGIN
		PRINT 'Starting TableRows'
	END
	
	SELECT @DetailFieldID = df.DetailFieldID, @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 10
	AND df.QuestionTypeID IN (16,19)
	
	/* These can be held at a variety of levels now, but ignore ClientPersonnel */
	/* Create the new rows first, then populate the values */
	IF @DetailFieldID > 0
	BEGIN

		INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID) 
		SELECT @ClientID, @DetailFieldID, @DetailFieldPageID, 1, 1, @NewCustomerID
		
		SELECT @TableRowID = SCOPE_IDENTITY()
		
		EXEC dbo._C00_CompleteTableRow @TableRowID 

		IF @Debug = 1
		BEGIN
			PRINT 'Customer Table done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' TableRowID: '+ CONVERT(VARCHAR,@TableRowID)
		END
	
		SELECT @DetailFieldID = NULL

	END

	SELECT @DetailFieldID = df.DetailFieldID, @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 1
	AND df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID IN (16,19)
	
	IF @DetailFieldID > 0
	BEGIN

		INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, LeadID) 
		SELECT @ClientID, @DetailFieldID, @DetailFieldPageID, 1, 1, @NewLeadID

		SELECT @TableRowID = SCOPE_IDENTITY()

		EXEC dbo._C00_CompleteTableRow @TableRowID 

		IF @Debug = 1
		BEGIN
			PRINT 'Lead Table done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' TableRowID: '+ CONVERT(VARCHAR,@TableRowID)
		END
	
		SELECT @DetailFieldID = NULL

	END

	SELECT @DetailFieldID = df.DetailFieldID, @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 11
	AND df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID IN (16,19)
	
	IF @DetailFieldID > 0
	BEGIN

		INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CaseID) 
		SELECT @ClientID, @DetailFieldID, @DetailFieldPageID, 1, 1, @NewCaseID

		SELECT @TableRowID = SCOPE_IDENTITY()

		EXEC dbo._C00_CompleteTableRow @TableRowID 

		IF @Debug = 1
		BEGIN
			PRINT 'Case Table done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' TableRowID: '+ CONVERT(VARCHAR,@TableRowID)
		END
	
		SELECT @DetailFieldID = NULL

	END

	SELECT @DetailFieldID = df.DetailFieldID, @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 2
	AND df.LeadTypeID = @LeadTypeID
	AND df.QuestionTypeID IN (16,19)
	
	IF @DetailFieldID > 0
	BEGIN

		INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, MatterID, LeadID) 
		SELECT @ClientID, @DetailFieldID, @DetailFieldPageID, 1, 1, @NewMatterID, @NewLeadID

		SELECT @TableRowID = SCOPE_IDENTITY()

		EXEC dbo._C00_CompleteTableRow @TableRowID, @NewMatterID 

		IF @Debug = 1
		BEGIN
			PRINT 'Matter Table done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' TableRowID: '+ CONVERT(VARCHAR,@TableRowID)
		END
	
		SELECT @DetailFieldID = NULL

	END

	SELECT @DetailFieldID = df.DetailFieldID, @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.LeadOrMatter = 14
	AND df.QuestionTypeID IN (16,19)
	
	IF @DetailFieldID > 0
	BEGIN

		INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ContactID) 
		SELECT @ClientID, @DetailFieldID, @DetailFieldPageID, 1, 1, @NewContactID
	
		SELECT @TableRowID = SCOPE_IDENTITY()

		EXEC dbo._C00_CompleteTableRow @TableRowID 

		IF @Debug = 1
		BEGIN
			PRINT 'Contact Table done. Field ID: ' + CONVERT(VARCHAR,@DetailFieldID) + ' TableRowID: '+ CONVERT(VARCHAR,@TableRowID)
		END
	
		SELECT @DetailFieldID = NULL

	END
	
	INSERT INTO ClientPersonnelFavourite (ClientID, LeadID, ClientPersonnelID)
	VALUES (@ClientID, @NewLeadID, @ClientPersonnelID)
	
	IF @Debug = 1
	BEGIN
		PRINT 'ClientPersonnelFavorite done'
	END

	INSERT INTO LeadViewHistory (ClientID, ClientPersonnelID, LeadID, WhenViewed)
	VALUES (@ClientID, @ClientPersonnelID, @NewLeadID, dbo.fn_GetDate_Local())

	IF @Debug = 1
	BEGIN
		PRINT 'LeadViewhistory done'
	END

	INSERT INTO DiaryAppointment (ClientID, DiaryAppointmentTitle, DiaryAppointmentText, CreatedBy, DueDate, EndDate, AllDayEvent, Completed, LeadID, CaseID, CustomerID, Version, ExportVersion, RecurrenceInfo, DiaryAppointmentEventType, ResourceInfo, TempReminderTimeshiftID, StatusID, LabelID)
	VALUES (@ClientID, 'Example Appointment', 'Example Appointment', @ClientPersonnelID, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), 1, 0, @NewLeadID, @NewCaseID, @NewCustomerID, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)

	SELECT @DiaryAppointmentID = SCOPE_IDENTITY()

	IF @Debug = 1
	BEGIN
		PRINT 'DiaryAppointment done. Diary AppointmentID: ' + CONVERT(VARCHAR,@DiaryAppointmentID)
	END

	INSERT INTO DiaryReminder (ClientID, DiaryAppointmentID, ClientPersonnelID, ReminderTimeshiftID, ReminderDate)	
	VALUES (@ClientID, @DiaryAppointmentID, @ClientPersonnelID, 1, dbo.fn_GetDate_Local())

	IF @Debug = 1
	BEGIN
		PRINT 'DiaryReminder done'
	END

	SELECT @NewCustomerID AS [New CustomerID],
			@NewLeadID AS [New LeadID],
			@NewCaseID AS [New CaseID],
			@NewMatterID AS [New MatterID]

	RETURN @NewCustomerID
	
	/*
		Tables we are ignoring because they are only for eCatcher:

		ClientCustomerMails
		CustomerAnswers
		CustomerOutcomes
		CustomerQuestionnaires
		DroppedOutCustomers
		DroppedOutCustomerAnswers
		DroppedOutCustomerMessages
		DroppedOutCustomerQuestionnaires
		OutcomeDelayedEmails
	*/

	/*
		Tables we are ignoring because they are only for history, that has not happened to the new records yet:

		CustomerHistory (We get this for free when inserting into the customers table)

		AutomatedEventQueue
		CaseTransferMapping
		DocumentBatchDetail
		FileExportEvent
		IncomingPostEvent
		IncomingPostEventValue 
		RPIClaimStatus
		TableDetailValuesHistory (multi)
		WorkflowTask
		WorkflowTaskCompleted
		
		{ClientOfficeLead is trigger-maintained}
		{LeadAssignment is trigger-maintained}

	*/

	/*
		Tables we are ignoring because they are defunct:

		Bill
		QuillCaseMapping
		QuillCustomerMapping
		SmsMessages
	*/

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateCustomerComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateCustomerComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateCustomerComplete] TO [sp_executeall]
GO
