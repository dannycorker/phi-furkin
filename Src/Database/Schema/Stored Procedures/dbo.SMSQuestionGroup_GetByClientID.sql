SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestionGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSQuestionGroupID],
					[ClientID],
					[SMSQuestionnaireID],
					[GroupName],
					[SMSQuestionOrderID],
					[GroupOrder]
				FROM
					[dbo].[SMSQuestionGroup] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_GetByClientID] TO [sp_executeall]
GO
