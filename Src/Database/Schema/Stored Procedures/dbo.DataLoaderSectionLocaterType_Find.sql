SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderSectionLocaterType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterType_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderSectionLocaterTypeID int   = null ,

	@LocaterTypeName varchar (250)  = null ,

	@LocaterTypeDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderSectionLocaterTypeID]
	, [LocaterTypeName]
	, [LocaterTypeDescription]
    FROM
	[dbo].[DataLoaderSectionLocaterType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID OR @DataLoaderSectionLocaterTypeID IS NULL)
	AND ([LocaterTypeName] = @LocaterTypeName OR @LocaterTypeName IS NULL)
	AND ([LocaterTypeDescription] = @LocaterTypeDescription OR @LocaterTypeDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderSectionLocaterTypeID]
	, [LocaterTypeName]
	, [LocaterTypeDescription]
    FROM
	[dbo].[DataLoaderSectionLocaterType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID AND @DataLoaderSectionLocaterTypeID is not null)
	OR ([LocaterTypeName] = @LocaterTypeName AND @LocaterTypeName is not null)
	OR ([LocaterTypeDescription] = @LocaterTypeDescription AND @LocaterTypeDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Find] TO [sp_executeall]
GO
