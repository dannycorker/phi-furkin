SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserRightsDynamic table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamic_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SELECT
					[UserRightsDynamicID],
					[ClientPersonnelID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[UserRightsDynamic] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelID] = @ClientPersonnelID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamic_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_GetByClientPersonnelID] TO [sp_executeall]
GO
