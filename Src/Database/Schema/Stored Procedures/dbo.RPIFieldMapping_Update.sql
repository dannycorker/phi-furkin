SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_Update]
(

	@RPIFieldMappingID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@RPIFieldID int   ,

	@DetailFieldID int   ,

	@IsEnabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@ColumnDetailFieldID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIFieldMapping]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[RPIFieldID] = @RPIFieldID
					,[DetailFieldID] = @DetailFieldID
					,[IsEnabled] = @IsEnabled
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[ColumnDetailFieldID] = @ColumnDetailFieldID
				WHERE
[RPIFieldMappingID] = @RPIFieldMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Update] TO [sp_executeall]
GO
