SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetQuestionnaireColours    Script Date: 08/09/2006 12:22:41 ******/
CREATE PROCEDURE [dbo].[GetQuestionnaireColours] @ClientQuestionnaireID int

AS

Select QuestionnaireColourID, ClientQuestionnaireID, Page, Header, Intro, Body, Footer, ClientID 
From QuestionnaireColours
Where ClientQuestionnaireID = @ClientQuestionnaireID





GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireColours] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionnaireColours] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireColours] TO [sp_executeall]
GO
