SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-10-06
-- Description:	Event Functions
-- =============================================
CREATE PROCEDURE [dbo].[ef]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM [EquationFunctions]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ef] TO [sp_executeall]
GO
