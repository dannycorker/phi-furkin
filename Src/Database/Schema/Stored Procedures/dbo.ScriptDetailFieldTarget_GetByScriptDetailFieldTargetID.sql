SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget_GetByScriptDetailFieldTargetID]
(

	@ScriptDetailFieldTargetID int   
)
AS


				SELECT
					[ScriptDetailFieldTargetID],
					[ScriptID],
					[ClientID],
					[Target],
					[DetailFieldID],
					[ColumnFieldID],
					[Format],
					[SectionID]
				FROM
					[dbo].[ScriptDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[ScriptDetailFieldTargetID] = @ScriptDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_GetByScriptDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget_GetByScriptDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_GetByScriptDetailFieldTargetID] TO [sp_executeall]
GO
