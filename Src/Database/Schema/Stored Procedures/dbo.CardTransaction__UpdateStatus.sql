SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 31/07/2017
-- Description:	Card Transaction update status, error, error code
-- 2019-12-03 CPS for JIRA LPC-174 | Brought over from Aquarius427Dev
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__UpdateStatus]
	@CardTransactionID INT, 
	@ClientID INT, 	
	@StatusMsg VARCHAR(250) = NULL, 
	@ErrorCode VARCHAR(100)= NULL, 
	@ErrorMessage VARCHAR(500) = NULL
AS
BEGIN
		
	SET NOCOUNT ON;
	
	UPDATE CardTransaction
	SET	StatusMsg = @StatusMsg,
		ErrorCode = @ErrorCode, 
		ErrorMessage = @ErrorMessage
	WHERE CardTransactionID=@CardTransactionID AND ClientID=@ClientID
    	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__UpdateStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__UpdateStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__UpdateStatus] TO [sp_executeall]
GO
