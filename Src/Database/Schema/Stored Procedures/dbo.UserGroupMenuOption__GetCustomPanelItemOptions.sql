SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-03-24
-- Description:	Gets a list of custom panel requirements for a particular UserGroup (Add, Hide, Rename) #31839
-- =============================================
CREATE PROCEDURE [dbo].[UserGroupMenuOption__GetCustomPanelItemOptions]
(
	@ClientPersonnelAdminGroupID int,
	@ClientPersonnelID int,
	@ClientID int,
	@MasterPageName VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @StartURL VARCHAR(2000)

	DECLARE @Table Table (Client_ID VARCHAR(2000), Client_Secret VARCHAR(2000), RedirectURL VARCHAR(2000), StartURL VARCHAR(2000))

	INSERT INTO @Table (Client_ID, Client_Secret, RedirectURL, StartURL)
	EXEC Cronofy__GetLogonDetails @ClientID, @ClientPersonnelID

	SELECT @StartURL = t.StartURL
	FROM @Table t

	/*If there are any for my client, page and admin group show them, otherwise try for client and page*/
	IF EXISTS (
		SELECT *
		FROM dbo.UserGroupMenuOption pugmo WITH (NOLOCK) 
		WHERE pugmo.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID 
		AND pugmo.ClientID = @ClientID
		AND pugmo.MasterPageName = @MasterPageName
	)
	BEGIN
	
		/* Rebrand the ordinary "Your Leads" button */
		SELECT pugmo.PanelItemAction, 
		pugmo.PanelItemName, 
		pugmo.PanelItemCaption, 
		pugmo.PanelItemIcon, 
		CONVERT(VARCHAR(2000),pugmo.PanelItemURL) as [PanelItemURL],
		pugmo.UserGroupMenuOptionID,
		pugmo.ShowInFancyBox,
		pugmo.ItemOrder
		FROM dbo.UserGroupMenuOption pugmo WITH (NOLOCK) 
		WHERE pugmo.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID 
		AND pugmo.ClientID = @ClientID
		AND pugmo.MasterPageName = @MasterPageName
		
		UNION 
		
		SELECT 'ADD' AS [PanelItemAction],
		'Link Email Account' AS [PanelItemName], 
		'Link Email Account' AS [PanelItemCaption], 
		'email_add.png' AS [PanelItemIcon], 
		@StartURL AS [PanelItemURL],
		9999 AS [UserGroupMenuOptionID],
		'false' AS [ShowInFancyBox],
		999 AS [ItemOrder]
		FROM ClientPreference cp WITH (NOLOCK) 
		WHERE cp.ClientID = @ClientID
		AND cp.ClientPreferenceTypeID = 2
		AND cp.PreferenceValue = 1
		AND NOT EXISTS (
			SELECT *
			FROM CronofyTokens ct WITH (NOLOCK)
			WHERE ct.ClientPersonnelID = @ClientPersonnelID
		)

		ORDER BY ItemOrder

	END
	ELSE
	BEGIN
	
		/* Rebrand the ordinary "Your Leads" button */
		SELECT pugmo.PanelItemAction, 
		pugmo.PanelItemName, 
		pugmo.PanelItemCaption, 
		pugmo.PanelItemIcon, 
		CONVERT(VARCHAR(2000),pugmo.PanelItemURL) as [PanelItemURL],
		pugmo.UserGroupMenuOptionID,
		pugmo.ShowInFancyBox,
		pugmo.ItemOrder
		FROM dbo.UserGroupMenuOption pugmo WITH (NOLOCK) 
		WHERE pugmo.ClientPersonnelAdminGroupID IS NULL
		AND pugmo.ClientID = @ClientID
		AND pugmo.MasterPageName = @MasterPageName
		
		UNION 
		
		SELECT 'ADD' AS [PanelItemAction],
		'Link Email Account' AS [PanelItemName], 
		'Link Email Account' AS [PanelItemCaption], 
		'email_add.png' AS [PanelItemIcon], 
		@StartURL AS [PanelItemURL],
		9999 AS [UserGroupMenuOptionID],
		'false' AS [ShowInFancyBox],
		999 AS [ItemOrder]
		FROM ClientPreference cp WITH (NOLOCK) 
		WHERE cp.ClientID = @ClientID
		AND cp.ClientPreferenceTypeID = 2
		AND cp.PreferenceValue = 1
		AND NOT EXISTS (
			SELECT *
			FROM CronofyTokens ct WITH (NOLOCK)
			WHERE ct.ClientPersonnelID = @ClientPersonnelID
		)

		ORDER BY ItemOrder

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemOptions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemOptions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemOptions] TO [sp_executeall]
GO
