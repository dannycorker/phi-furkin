SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseDetailValues table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseDetailValues_GetByCaseID]
(

	@CaseID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CaseDetailValueID],
					[ClientID],
					[CaseID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[CaseDetailValues] WITH (NOLOCK) 
				WHERE
					[CaseID] = @CaseID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_GetByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseDetailValues_GetByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_GetByCaseID] TO [sp_executeall]
GO
