SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 19-07-2019
-- Description:	Checks the given password against a list of common passwords.
-- If the Password is found with then list it returns true else it returns false.
-- =============================================
CREATE PROCEDURE [dbo].[CommonPassword__Check]
	@Password VARCHAR(100),
	@Result BIT OUTPUT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	EXEC AquariusMaster.dbo.CommonPassword__Check 'wicked', @Result OUTPUT
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CommonPassword__Check] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CommonPassword__Check] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CommonPassword__Check] TO [sp_executeall]
GO
