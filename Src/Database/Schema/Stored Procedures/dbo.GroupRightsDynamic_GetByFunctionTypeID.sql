SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupRightsDynamic table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamic_GetByFunctionTypeID]
(

	@FunctionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[GroupRightsDynamicID],
					[ClientPersonnelAdminGroupID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[GroupRightsDynamic] WITH (NOLOCK) 
				WHERE
					[FunctionTypeID] = @FunctionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_GetByFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamic_GetByFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_GetByFunctionTypeID] TO [sp_executeall]
GO
