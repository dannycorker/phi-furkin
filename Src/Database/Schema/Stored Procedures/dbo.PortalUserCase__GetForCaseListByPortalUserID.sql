SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 20080626
-- Description:	Gets a useful list of Cases that the current PortalUser has access to.
--				JWG 2009-07-09 Added Status and AssignedTo options.
--              JWG 2010-01-22 Join to all Cases if puc.CaseID is NULL
--				JWG 2014-05-13 #26507 LatestRecordsFirst 
-- =============================================
CREATE PROCEDURE  [dbo].[PortalUserCase__GetForCaseListByPortalUserID]
	@PortalUserID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LatestRecordsFirst BIT = 0
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	INNER JOIN dbo.PortalUser pu WITH (NOLOCK) ON pu.ClientID = cl.ClientID
	WHERE pu.PortalUserID = @PortalUserID
	

	SELECT		puc.PortalUserCaseID, 
				puc.CaseID, 
				l.LeadID, 
				l.CustomerID, 
				l.WhenCreated, 
				c.Fullname, 
				COALESCE(ls_case.StatusDescription, ls_lead.StatusDescription, '') AS [StatusDescription],
				ISNULL(cp.UserName, '') AS [AssignedToUserName]
	FROM        dbo.PortalUserCase puc (nolock) 
	INNER JOIN	dbo.Lead l (nolock) ON puc.LeadID = l.LeadID 
	INNER JOIN	dbo.Customers c (nolock) ON l.CustomerID = c.CustomerID 
	INNER JOIN  dbo.PortalUser pu WITH (NOLOCK) ON pu.PortalUserID = puc.PortalUserID
	LEFT JOIN  dbo.PortalUserGroup pug WITH (NOLOCK) ON pug.PortalUserGroupID = pu.PortalUserGroupID
	INNER JOIN	dbo.Cases ca (nolock) ON ca.LeadID = l.LeadID AND (ca.CaseID = puc.CaseID OR puc.CaseID IS NULL)
	LEFT JOIN   dbo.LeadStatus ls_case (nolock) ON ls_case.StatusID = ca.ClientStatusID 
	LEFT JOIN   dbo.LeadStatus ls_lead (nolock) ON ls_lead.StatusID = l.ClientStatusID 
	LEFT JOIN   dbo.ClientPersonnel cp (nolock) ON cp.ClientPersonnelID = l.AssignedTo 
	WHERE		(puc.PortalUserID = @PortalUserID) 
	AND (pug.HideOwnCustomer IS NULL OR pug.HideOwnCustomer = 0 OR c.CustomerID <> pu.CustomerID)
	/*ORDER BY    l.LeadID, puc.CaseID */
	ORDER BY 
		/*
			JWG 2014-05-13 #26507 LatestRecordsFirst
			The dynamic part is really the ASC/DESC rather than the column to order by, but this is the only way to do it at present.
		*/
		CASE @LatestRecordsFirst WHEN 1 THEN l.LeadID END DESC, 
		CASE @LatestRecordsFirst WHEN 0 THEN l.LeadID END ASC, 
		CASE @LatestRecordsFirst WHEN 1 THEN puc.CaseID END DESC, 
		CASE @LatestRecordsFirst WHEN 0 THEN puc.CaseID END ASC 
	
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetForCaseListByPortalUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__GetForCaseListByPortalUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetForCaseListByPortalUserID] TO [sp_executeall]
GO
