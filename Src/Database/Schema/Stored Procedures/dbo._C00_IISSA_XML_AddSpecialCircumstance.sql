SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-06-14
-- Description:	Add a special circumstance to an irish insolvency customer
-- =============================================
CREATE PROCEDURE [dbo].[_C00_IISSA_XML_AddSpecialCircumstance]
	 @CustomerID				INT
	,@PrimaryApplicant			BIT = 1
	,@Amount					DECIMAL(18,2)
	,@Details					VARCHAR(2000)
	,@Comment					VARCHAR(2000)
	,@SpecialCircumstanceUNID	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @TableRowID				INT
			,@ClientID					INT
			,@LeadTypeID				INT = 0
			,@ThirdPartyFieldGroupID	INT = 85
			,@TableDetailFieldID		INT 
			,@TableDetailFieldPageID	INT
			,@AmountDetailFieldID		INT
			,@DetailsDetailFieldID		INT
			,@CommentDetailFieldID		INT
			,@ApplicantDetailFieldID	INT
			,@ApplicantNarrative		VARCHAR(2000)

	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH ( NOLOCK )
	WHERE cu.CustomerID = @CustomerID

	SELECT	 @TableDetailFieldID = df.DetailFieldID, @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM	DetailFields df WITH ( NOLOCK )
	WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 4205) /*Detail of special circumstance costs*/

	IF @TableDetailFieldID is NULL or @TableDetailFieldPageID is NULL
	BEGIN
		PRINT 'Error.  No field identified for ThirdPartyFieldID 4205'
		PRINT @Amount
		PRINT @Details
		PRINT @TableDetailFieldID
		RETURN
	END
	ELSE
	BEGIN
		/*Find the appropriate column fields*/
		SELECT	 @AmountDetailFieldID	= dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 4206) /*Amount*/
				,@CommentDetailFieldID	= dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 4208) /*Comment*/
				,@DetailsDetailFieldID	= dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 4207) /*Reason for Special Circumstance*/
				,@ApplicantDetailFieldID= dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, @ThirdPartyFieldGroupID, 4209) /*Applicant*/
				,@ApplicantNarrative	= CASE WHEN @PrimaryApplicant = 0 THEN 'Partner' ELSE 'Applicant' END 
	
		/*Create a special circumstances table row*/
		INSERT TableRows ( ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit, SourceID )
		VALUES ( @ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID, 1, 0, @SpecialCircumstanceUNID )
		
		SELECT @TableRowID = SCOPE_IDENTITY()

		EXEC dbo._C00_SimpleValueIntoField		@AmountDetailFieldID	, @Amount				, @TableRowID
		EXEC dbo._C00_SimpleValueIntoField		@CommentDetailFieldID	, @Comment				, @TableRowID
		EXEC dbo._C00_SimpleValueIntoField		@DetailsDetailFieldID	, @Details				, @TableRowID
		EXEC dbo._C00_SimpleValueIntoFieldLuli	@ApplicantDetailFieldID	, @ApplicantNarrative	, @TableRowID

		/*Complete the table row*/
		EXEC _C00_CompleteTableRow @TableRowID, NULL, 0, 0
		
	END	

	/*Reutrn the table RowID*/
	RETURN @TableRowID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IISSA_XML_AddSpecialCircumstance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_IISSA_XML_AddSpecialCircumstance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IISSA_XML_AddSpecialCircumstance] TO [sp_executeall]
GO
