SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Messages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_Delete]
(

	@MessageID int   
)
AS


				DELETE FROM [dbo].[Messages] WITH (ROWLOCK) 
				WHERE
					[MessageID] = @MessageID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Delete] TO [sp_executeall]
GO
