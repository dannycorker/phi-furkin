SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PortalUserOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOptionType_Get_List]

AS


				
				SELECT
					[PortalUserOptionTypeID],
					[OptionTypeName]
				FROM
					[dbo].[PortalUserOptionType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOptionType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Get_List] TO [sp_executeall]
GO
