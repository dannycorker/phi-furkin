SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryEditingCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_Get_List]

AS


				
				SELECT
					[SqlQueryEditingCriteriaID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[SqlQueryEditingColumnID],
					[CriteriaText],
					[Criteria1],
					[Criteria2],
					[CriteriaName],
					[SubQueryID],
					[SubQueryLinkType],
					[ParamValue],
					[IsSecurityClause],
					[CriteriaSubstitutions],
					[IsParameterizable],
					[Comparison],
					[IsJoinClause]
				FROM
					[dbo].[SqlQueryEditingCriteria] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Get_List] TO [sp_executeall]
GO
