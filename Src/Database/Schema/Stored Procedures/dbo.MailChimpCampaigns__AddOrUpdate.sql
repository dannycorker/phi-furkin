SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-23
-- Description:	Add/Update mailchimp campaigns
-- =============================================
CREATE PROCEDURE [dbo].[MailChimpCampaigns__AddOrUpdate]
	@ClientID INT,
	@CampaignID VARCHAR(100),
	@SentDate DATETIME,
	@Title VARCHAR(200),
	@EmailSent INT,
	@Unsubscribed INT = -1,
	@Status VARCHAR(20),
	@LastCompletePoll DATETIME = NULL,
	@MailchimpAccount VARCHAR(50) = ''
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (
		SELECT *
		FROM MailChimpCampaigns mc WITH (NOLOCK)
		WHERE mc.ClientID = @ClientID
		AND mc.CampaignID = @CampaignID
	)
	BEGIN
	
		/*We have a campaign, update it*/
		UPDATE mc
		SET SentDate = dbo.fnRegionalToGmt(@SentDate, 35), Title = @Title, EmailSent = @EmailSent, Unsubscribed = CASE WHEN @Unsubscribed = -1 THEN Unsubscribed ELSE @Unsubscribed END, Status = @Status, LastCompletePoll = ISNULL(@LastCompletePoll, LastCompletePoll), MailchimpAccount = @MailchimpAccount
		FROM MailChimpCampaigns mc 
		WHERE mc.ClientID = @ClientID
		AND mc.CampaignID = @CampaignID
	
	END
	ELSE
	BEGIN
	
		/*We dont have a campaign, create it*/
		INSERT INTO MailChimpCampaigns (ClientID, CampaignID, SentDate, Title, EmailSent, Unsubscribed, Status, MailchimpAccount)
		VALUES (@ClientID, @CampaignID,  dbo.fnRegionalToGmt(@SentDate, 35), @Title, @EmailSent, CASE WHEN @Unsubscribed = -1 THEN 0 ELSE @Unsubscribed END, @Status, @MailchimpAccount)

	END

	SELECT ISNULL(mc.LastCompletePoll, '2000-01-01') AS [LastPollDate]
	FROM MailChimpCampaigns mc 
	WHERE mc.ClientID = @ClientID
	AND mc.CampaignID = @CampaignID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimpCampaigns__AddOrUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MailChimpCampaigns__AddOrUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimpCampaigns__AddOrUpdate] TO [sp_executeall]
GO
