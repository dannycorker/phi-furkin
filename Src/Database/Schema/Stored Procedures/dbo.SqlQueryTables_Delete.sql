SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_Delete]
(

	@SqlQueryTableID int   
)
AS


				DELETE FROM [dbo].[SqlQueryTables] WITH (ROWLOCK) 
				WHERE
					[SqlQueryTableID] = @SqlQueryTableID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Delete] TO [sp_executeall]
GO
