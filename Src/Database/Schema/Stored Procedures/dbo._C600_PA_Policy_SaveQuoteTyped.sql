SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================		
-- Author:	Ian Slack		
-- Create date: 2014-10-26		
-- Description:	Save Quote	
-- Mods:
-- 2017-02-02 DCM Removed logXML log		
-- 2017-12-14 GPR Updated to use WhenExpires to use AffinityDetails QuoteLapse field
-- 2018-03-15 MAB Removed literal lapse period of 30 days and replaced with AffinityDetails QuoteLapse field
-- 2018-03-15 MAB Added values for WhenCreated & WhenExpire for INSERT into _C600_SavedQuote to prevent NOT NULL constraint violation
-- 2018-03-15 MAB  Modified node definition for extraction of BrandID
-- 2019-06-21 GPR applied fix for #57791
-- 2021-05-10 AAD for FURKIN-611 - Remove hard-typed ResourceListID = 2000184 filter
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Policy_SaveQuoteTyped]		
(		
	@XmlRequest NVARCHAR(MAX), --XML,		
	@Email VARCHAR(255),		
	@SecretQuestionID VARCHAR(50),		
	@SecretQuestionValue VARCHAR(250),		
	@SavedQuoteKey VARCHAR(50) = NULL,
	@QuoteSessionID INT = NULL,
	@AggregatorID VARCHAR(50) = NULL
)		
AS		
BEGIN		

	DECLARE @QuoteLapse INT, 		-- GPR 2017-12-14 PM28 Client600
			@BrandID INT,	 		-- GPR 2017-12-14 PM28 Client600
			@ClientID			INT = dbo.fnGetPrimaryClientID(),
			@ExpiryDate DATETIME	-- MAB 2018-03-15,
			/*GPR 2021-02-15*/
			,@FeeTableRowID	INT
			,@EnrollmentFee NUMERIC(18,2)
			,@TaxTableRowID INT
			,@Tax	NUMERIC(18,2)
			,@Total NUMERIC(18,2)
			,@ProvinceLookupListItemID	INT
			,@PetIndex	INT
			,@StartDate	DATE
			,@County	VARCHAR(200)
			,@CustomerID	INT = 0
	
	DECLARE @XmlRequestIn XML = @XmlRequest
	--DECLARE @XMLData VARCHAR(MAX) = CAST(@XmlRequest AS VARCHAR(MAX))		
	EXEC [dbo]._C00_LogItXML @ClientID, @QuoteSessionID, '[_C600_PA_Policy_SaveQuote_Typed] Response', @XmlRequest	
	DECLARE @AggregatorDefault VARCHAR(10) = 'false' -- GPR 2019-06-21	
	
	SELECT	TOP 1 @SavedQuoteKey = SavedQuoteKey 
	FROM	_C600_SavedQuote WITH (NOLOCK) 
	WHERE	(QuoteSessionID = @QuoteSessionID AND @QuoteSessionID > 0)
	ORDER BY SavedQuoteID DESC

	EXEC [dbo]._C00_LogIt  'Info','_C600_PA_Policy_SaveQuoteTyped', 'SavedQuoteKey-Paul-1', @SavedQuoteKey, 58552
	
	IF @SavedQuoteKey IS NULL		
	BEGIN		
		SELECT @SavedQuoteKey = NEWID()		
	END	
	EXEC [dbo]._C00_LogIt  'Info','_C600_PA_Policy_SaveQuoteTyped', 'SavedQuoteKey-Paul-2', @SavedQuoteKey, 58552
	/*GPR 2019-06-21 for #57791*/
 	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotSms/text())[1] with sql:variable("@AggregatorDefault")');
	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotEmail/text())[1] with sql:variable("@AggregatorDefault")');
	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotPhone/text())[1] with sql:variable("@AggregatorDefault")');
	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotPost/text())[1] with sql:variable("@AggregatorDefault")');	


		/*GPR 2021-02-15*/

	--<PolicyValueEnrollmentFees>
	--		<EnrollmentFee>
	--			<FeeTableRowID></FeeTableRowID>
	--			<EnrollmentFee></EnrollmentFee>
	--			<TaxTableRowID></TaxTableRowID>
	--			<Tax></Tax>
	--			<Total></Total>
	--		</EnrollmentFee>
	--</PolicyValueEnrollmentFees>

	SET @XmlRequestIn.modify('insert(<PolicyValueEnrollmentFees>Test</PolicyValueEnrollmentFees>) as last into (/BuyPolicyRequest[1]/PolicyValues[1])');



	/* 2018-03-15 Mark Beaumont	- calculate expiry date */
	SELECT @BrandID = @XmlRequestIn.value('(//BrandingId)[1]', 'INT') -- GPR 2017-12-14 PM28 Client600 | MAB 2018-03-15 use //BrandingId to work with all XML request types
	SELECT @QuoteLapse = quotelapse.DetailValue FROM ResourceListDetailValues quotelapse WITH (NOLOCK) WHERE quotelapse.ResourceListID = @BrandID AND quotelapse.DetailFieldID = 179717 -- GPR 2017-12-14 PM28 Client600 /* @BrandID instead of 2000184. 2021-05-10 AAD for FURKIN-611 */
	SELECT @ExpiryDate = dbo.fn_GetDate_Local() + @QuoteLapse
		


	/*GPR 2021-02-15 for FURKIN-256*/
	/*
	<PolicyValueEnrollmentFees>
          <EnrollmentFee>
            <FeeTableRowID>FeeTableRowID</FeeTableRowID>
            <EnrollmentFee>EnrollmentFee</EnrollmentFee>
            <TaxTableRowID>TaxTableRowID</TaxTableRowID>
            <Tax>Tax</Tax>
            <Total>Total</Total>
          </EnrollmentFee>
        </PolicyValueEnrollmentFees>
	*/
	SET @XmlRequestIn.modify('insert(<PolicyValueEnrollmentFees><EnrollmentFee></EnrollmentFee></PolicyValueEnrollmentFees>) as last into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues)[1]');
	SET @XmlRequestIn.modify('insert(<FeeTableRowID>FeeTableRowID</FeeTableRowID>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<EnrollmentFee>EnrollmentFee</EnrollmentFee>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<TaxTableRowID>TaxTableRowID</TaxTableRowID>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<Tax>Tax</Tax>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<Total>Total</Total>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	
	SELECT @County = @XmlRequestIn.value('(//County)[1]' ,'VARCHAR(200)')
	SELECT @ProvinceLookupListItemID = CASE @County 
					WHEN 'AB' THEN 61739
					WHEN 'BC' THEN 61740
					WHEN 'MB' THEN 61741
					WHEN 'NB' THEN 61742
					WHEN 'NL' THEN 61743
					WHEN 'NS' THEN 61744
					WHEN 'NT' THEN 61745
					WHEN 'NU' THEN 61746
					WHEN 'ON' THEN 61747
					WHEN 'PE' THEN 61748
					WHEN 'QC' THEN 61749
					WHEN 'SK' THEN 61750
					WHEN 'YT' THEN 61751
				END
	SELECT @PetIndex = 1
	SELECT @StartDate = ISNULL(@XmlRequestIn.value('(//StartDate)[1]' ,'DATE'), dbo.fn_GetDate_Local())

	SELECT @FeeTableRowID = FeeTableRowID, @EnrollmentFee = EnrollmentFee, @TaxTableRowID = TaxTableRowID, @Tax = Tax, @Total = Total
	FROM dbo.fn_C600_GetEnrollmentFee(@BrandID, @CustomerID, @ProvinceLookupListItemID, @PetIndex, @StartDate)

	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/FeeTableRowID/text())[1] with sql:variable("@FeeTableRowID")');
	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/EnrollmentFee/text())[1] with sql:variable("@EnrollmentFee")');
	
	IF @County IS NOT NULL /*GPR 2021-02-15 County doesn't exist in the XML until the Customers page which is after the first Save opportunity. Only do the tax and total replacements once we have a County*/
	BEGIN
		SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/TaxTableRowID/text())[1] with sql:variable("@TaxTableRowID")');
		SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Tax/text())[1] with sql:variable("@Tax")');
		SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Total/text())[1] with sql:variable("@Total")');
	END


	IF NOT EXISTS (SELECT SavedQuoteID FROM _C600_SavedQuote WITH (NOLOCK) WHERE SavedQuoteKey = @SavedQuoteKey)		
	BEGIN	
		/* 2018-03-15 Mark Beaumont	- added values for WhenCreated & WhenExpire to prevent NOT NULL constraint violation */
		INSERT INTO _C600_SavedQuote (SavedQuoteXML, EmailAddress, SecretQuestionID, SecretQuestionValue, WhenCreated, WhenExpire, SavedQuoteKey, QuoteSessionID, AggregatorID)		
		VALUES (@XmlRequestIn, ISNULL(@Email, 'NOT_PROVIDED'), ISNULL(@SecretQuestionID,'6'), ISNULL(@SecretQuestionValue, 'NOT_PROVIDED'), dbo.fn_GetDate_Local(), @ExpiryDate, @SavedQuoteKey, @QuoteSessionID, @AggregatorID)		
	END		
	ELSE		
	BEGIN

		UPDATE _C600_SavedQuote		
		SET		
			SavedQuoteXML = ISNULL(@XmlRequestIn, SavedQuoteXML),		
			EmailAddress = ISNULL(@Email,EmailAddress),		
			SecretQuestionID = ISNULL(@SecretQuestionID,SecretQuestionID),		
			SecretQuestionValue = ISNULL(@SecretQuestionValue, SecretQuestionValue),		
			WhenCreated = dbo.fn_GetDate_Local(),		
			WhenExpire = @ExpiryDate		-- 2018-03-15 Mark Beaumont	- use lapse period as defined for affinity (PM28)
		WHERE SavedQuoteKey = @SavedQuoteKey		
	END		
	
	
	-- Update the header quote session table
	;WITH CustomerData AS(
	SELECT	
		@XmlRequestIn.value('(//CustomerInfo/Postcode)[1]', 'VARCHAR(50)') Postcode,
		@XmlRequestIn.value('(//CustomerInfo/County)[1]', 'VARCHAR(200)') County,
		@XmlRequestIn.value('(//CustomerInfo/FirstName)[1]', 'VARCHAR(100)') FirstName,
		@XmlRequestIn.value('(//CustomerInfo/LastName)[1]', 'VARCHAR(100)') LastName,
		@XmlRequestIn.value('(//CustomerInfo/Email)[1]', 'VARCHAR(255)') Email,
		@XmlRequestIn.value('(//CustomerInfo/HomePhone)[1]', 'varchar(50)') HomePhone,
		@XmlRequestIn.value('(//CustomerInfo/MobilePhone)[1]', 'varchar(50)') Mobile
	)
	UPDATE qs
	SET 
		qs.FirstName = ISNULL(cd.FirstName, qs.FirstName),
		qs.LastName = ISNULL(cd.LastName,qs.LastName),
		qs.Email = ISNULL(cd.Email,qs.Email),
		qs.PostCode = ISNULL(cd.Postcode,qs.Postcode),
		qs.HomeTel = ISNULL(cd.HomePhone,qs.HomeTel),
		qs.MobileTel = ISNULL(cd.Mobile,qs.MobileTel),
		qs.QuoteCount = qs.QuoteCount + 1
	FROM _C600_QuoteSessions qs 
	INNER JOIN CustomerData cd ON qs.QuoteSessionID = @QuoteSessionID
		
	SELECT	TOP 1 SavedQuoteID, SavedQuoteKey, CAST(SavedQuoteXML AS NVARCHAR(MAX)) SavedQuoteXML, EmailAddress, SecretQuestionID, SecretQuestionValue, WhenCreated, WhenExpire, AggregatorID, QuoteSessionID, RetrievedDate
	FROM	_C600_SavedQuote WITH (NOLOCK)		
	WHERE	SavedQuoteKey = @SavedQuoteKey		
	ORDER BY SavedQuoteID DESC		
		
END		


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuoteTyped] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_SaveQuoteTyped] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuoteTyped] TO [sp_executeall]
GO
