SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16/06/2016
-- Description:	Creates history for the CustomerPaymentSchedule and clears the CustomerPaymentSchedule
-- 2017-04-07 CS  Don't delete processed payment records
-- 2019-01-29 JEL Added Payent statusID 5 so we don't double collect retries 
-- 2019-01-31 JEL Added FromDate filter so we have the same logic in Rebuild as here
-- =============================================
CREATE PROCEDURE [dbo].[CustomerPaymentScheduleHistory__Backup]
	@CustomerID INT,	-- the schedule to backup
	@UserID INT,		-- backup created by
	@Delete BIT			-- when true deletes the CustomerPaymentSchedule
AS
BEGIN
		
	SET NOCOUNT ON;

	/*JEL, changed to reflect logic in Rebuild*/ 
	DECLARE @FromDate DATE = dbo.fn_GetDate_Local()

	/*Check we are not about to loose track of a payment because we've reconciled too early*/ 
	SELECT TOP 1 @FromDate = p.PaymentDate 
	FROM CustomerPaymentSchedule p WITH (NOLOCK) 
	WHERE p.PaymentDate < @FromDate 
	AND p.PaymentStatusID IN (1,2,5)
	AND p.CustomerLedgerID IS NULL
	AND p.CustomerID = @CustomerID
	ORDER BY p.PaymentDate ASC

	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	DECLARE @CustomerPaymentScheduleCount INT
	SELECT @CustomerPaymentScheduleCount = COUNT(CustomerPaymentScheduleID) 
	FROM CustomerPaymentSchedule WITH (NOLOCK) WHERE CustomerID=@CustomerID
	IF(@CustomerPaymentScheduleCount>0)-- a schedule of payments already exists, so perform backup
	BEGIN
		INSERT INTO CustomerPaymentScheduleHistory (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, OriginalCustomerPaymentScheduleID, OriginalWhoCreated, OriginalWhenCreated, WhoCreated, WhenCreated, ClientAccountID, RelatedObjectID, RelatedObjectTypeID)
		SELECT ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, @UserID, @WhenCreated, cps.ClientAccountID, RelatedObjectID, RelatedObjectTypeID
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		WHERE CustomerID=@CustomerID
		AND cps.PaymentDate >= @FromDate
    END
    
    -- Do not delete items in the list that have a CustomerLedger Entry
	IF(@Delete=1) 
	BEGIN

		/*
			2018-06-06 ACE 
			Only remove premium type payments/refunds (Standard premium, MTA etc), leave claim payments in place as 
			they (even on a policy cancellation) will need to be paid out. Example being dead pet claim.
		*/
		DELETE c
		FROM CustomerPaymentSchedule c
		INNER JOIN ClientAccount ca ON ca.ClientAccountID = c.ClientAccountID AND ca.AccountUseID = 1 
		WHERE CustomerID = @CustomerID 
		AND CustomerLedgerID IS NULL
		AND PaymentStatusID IN ( 1 , 5)  /*Don't delete processed payments*/
		AND c.PaymentDate >= @FromDate /*Don't wipe out payments we arnt about to put back*/ 

	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentScheduleHistory__Backup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerPaymentScheduleHistory__Backup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentScheduleHistory__Backup] TO [sp_executeall]
GO
