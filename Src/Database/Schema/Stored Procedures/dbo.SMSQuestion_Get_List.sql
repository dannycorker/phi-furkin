SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SMSQuestion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_Get_List]

AS


				
				SELECT
					[SMSQuestionID],
					[ClientID],
					[SMSQuestionnaireID],
					[Question],
					[QuestionOrder],
					[DocumentTypeID],
					[Enabled],
					[Timeout],
					[Repeat],
					[RepeatPrefix],
					[DetailFieldToSave],
					[EvaluateResponse],
					[VoiceAction],
					[RedirectTo],
					[GatherNumDigits],
					[RecordingMaxLength]
				FROM
					[dbo].[SMSQuestion] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Get_List] TO [sp_executeall]
GO
