SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AquariumStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@AquariumStatusID int   = null ,

	@AquariumStatusName varchar (50)  = null ,

	@AquariumStatusDescription varchar (250)  = null ,

	@Usable bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AquariumStatusID]
	, [AquariumStatusName]
	, [AquariumStatusDescription]
	, [Usable]
    FROM
	[dbo].[AquariumStatus] WITH (NOLOCK) 
    WHERE 
	 ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID IS NULL)
	AND ([AquariumStatusName] = @AquariumStatusName OR @AquariumStatusName IS NULL)
	AND ([AquariumStatusDescription] = @AquariumStatusDescription OR @AquariumStatusDescription IS NULL)
	AND ([Usable] = @Usable OR @Usable IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AquariumStatusID]
	, [AquariumStatusName]
	, [AquariumStatusDescription]
	, [Usable]
    FROM
	[dbo].[AquariumStatus] WITH (NOLOCK) 
    WHERE 
	 ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([AquariumStatusName] = @AquariumStatusName AND @AquariumStatusName is not null)
	OR ([AquariumStatusDescription] = @AquariumStatusDescription AND @AquariumStatusDescription is not null)
	OR ([Usable] = @Usable AND @Usable is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Find] TO [sp_executeall]
GO
