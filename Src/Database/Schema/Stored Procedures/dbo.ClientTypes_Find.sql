SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientTypes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientTypes_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientTypeID int   = null ,

	@ClientTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientTypeID]
	, [ClientTypeName]
    FROM
	[dbo].[ClientTypes] WITH (NOLOCK) 
    WHERE 
	 ([ClientTypeID] = @ClientTypeID OR @ClientTypeID IS NULL)
	AND ([ClientTypeName] = @ClientTypeName OR @ClientTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientTypeID]
	, [ClientTypeName]
    FROM
	[dbo].[ClientTypes] WITH (NOLOCK) 
    WHERE 
	 ([ClientTypeID] = @ClientTypeID AND @ClientTypeID is not null)
	OR ([ClientTypeName] = @ClientTypeName AND @ClientTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientTypes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Find] TO [sp_executeall]
GO
