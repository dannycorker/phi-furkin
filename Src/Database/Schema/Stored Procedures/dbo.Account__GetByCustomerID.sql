SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-06-2016
-- Description:	Gets all the accounts for a given customer id
-- Modified By PR 29/09/2016 Only return active accounts
-- =============================================
CREATE PROCEDURE [dbo].[Account__GetByCustomerID]	
	@CustomerID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT * 
	FROM Account account WITH (NOLOCK) 
	LEFT JOIN dbo.AccountType at WITH (NOLOCK) ON at.AccountTypeID = account.AccountTypeID
	WHERE account.ClientID=@ClientID 
	AND account.CustomerID=@CustomerID 
	AND account.Active=1
	ORDER BY account.WhenCreated DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetByCustomerID] TO [sp_executeall]
GO
