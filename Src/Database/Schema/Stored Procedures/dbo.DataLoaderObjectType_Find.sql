SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderObjectType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectType_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@ObjectTypeName varchar (250)  = null ,

	@ObjectTypeDescription varchar (2000)  = null ,

	@ProcessingOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderObjectTypeID]
	, [ObjectTypeName]
	, [ObjectTypeDescription]
	, [ProcessingOrder]
    FROM
	[dbo].[DataLoaderObjectType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([ObjectTypeName] = @ObjectTypeName OR @ObjectTypeName IS NULL)
	AND ([ObjectTypeDescription] = @ObjectTypeDescription OR @ObjectTypeDescription IS NULL)
	AND ([ProcessingOrder] = @ProcessingOrder OR @ProcessingOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderObjectTypeID]
	, [ObjectTypeName]
	, [ObjectTypeDescription]
	, [ProcessingOrder]
    FROM
	[dbo].[DataLoaderObjectType] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([ObjectTypeName] = @ObjectTypeName AND @ObjectTypeName is not null)
	OR ([ObjectTypeDescription] = @ObjectTypeDescription AND @ObjectTypeDescription is not null)
	OR ([ProcessingOrder] = @ProcessingOrder AND @ProcessingOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Find] TO [sp_executeall]
GO
