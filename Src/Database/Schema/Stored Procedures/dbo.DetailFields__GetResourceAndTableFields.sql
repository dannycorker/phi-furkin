SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 23-11-07
-- Description:	Gets a list of DetailFieldIDs and a concat of Pagename + : + Fieldname
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetResourceAndTableFields]
	-- Add the parameters for the stored procedure here
	@LeadTypeID int,
	@ClientID int
AS
	SET NOCOUNT ON

	SELECT
		[DetailFieldID],
		[FieldName],
		[LeadOrMatter]
	FROM
		dbo.fnDetailFieldsShared(@ClientID)
	WHERE
		[LeadTypeID] = @LeadTypeID
		AND [ClientID] = @ClientID
		AND [QuestionTypeID] in (14,16)









GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetResourceAndTableFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetResourceAndTableFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetResourceAndTableFields] TO [sp_executeall]
GO
