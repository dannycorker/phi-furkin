SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget_Insert]
(

	@DocumentDetailFieldTargetID int    OUTPUT,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@Notes varchar (250)  ,

	@ExcelSheetLocation varchar (50)  ,

	@Format varchar (2000)  ,

	@DocumentTypeVersionID int   
)
AS


				
				INSERT INTO [dbo].[DocumentDetailFieldTarget]
					(
					[ClientID]
					,[DocumentTypeID]
					,[Target]
					,[DetailFieldID]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[Notes]
					,[ExcelSheetLocation]
					,[Format]
					,[DocumentTypeVersionID]
					)
				VALUES
					(
					@ClientID
					,@DocumentTypeID
					,@Target
					,@DetailFieldID
					,@TemplateTypeID
					,@DetailFieldAlias
					,@Notes
					,@ExcelSheetLocation
					,@Format
					,@DocumentTypeVersionID
					)
				-- Get the identity value
				SET @DocumentDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Insert] TO [sp_executeall]
GO
