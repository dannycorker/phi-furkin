SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        Simon Brushett
-- Create date:   2013-05-30
-- Description:   Inserts or updates a rule

-- Modified By:   Austin Davies
-- Modified:      2014-06-03
-- Description:   Include RuleOrder so the column can
--                be updated
-- 2015-09-14 DCM Temp - set rulecheckpoint=description
-- 2015-09-14 DCM Handle rule order properly on update
-- 2016-06-07 IS Pass RuleCheckpoint, RuleValidFrom, RuleValidTo from the UI
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Rules_InsertUpdateRule]
(
      @RuleID INT,
      @Name VARCHAR(200),
      @RuleSetID INT,
      @ClientID INT,
      @UserID INT,
      @Description VARCHAR(MAX),
      @RuleOrder INT = NULL,
      @RuleCheckpoint VARCHAR(50) = NULL,
      @RuleValidFrom DATETIME = NULL,
      @RuleValidTo DATETIME = NULL
)
AS
BEGIN
      
      SET NOCOUNT ON;
      
      SELECT @RuleCheckpoint = ISNULL(@RuleCheckpoint, @Description)
      
      IF @RuleID > 0
      BEGIN
      
            UPDATE dbo.RulesEngine_Rules
            SET Name = @Name,
                  WhenModified = dbo.fn_GetDate_Local(), 
                  WhoModified = @UserID,
                  Description = @Description,
                  RuleOrder = CASE WHEN @RuleOrder=0 THEN RuleOrder ELSE @RuleOrder END,
                  RuleCheckpoint = @RuleCheckpoint,
                  RuleValidFrom = @RuleValidFrom,
                  RuleValidTo = @RuleValidTo
            WHERE RuleID = @RuleID
      
      END
      ELSE
      BEGIN
            
            DECLARE @MaxOrder INT
            SELECT @MaxOrder = MAX(r.RuleOrder)
            FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
            WHERE r.RuleSetID = @RuleSetID
            
            INSERT INTO dbo.RulesEngine_Rules (Name, RuleSetID, ClientID, RuleOrder, WhenCreated, WhoCreated, WhenModified, WhoModified, Description, RuleCheckpoint, RuleValidFrom, RuleValidTo)
            VALUES (@Name, @RuleSetID, @ClientID, ISNULL(@MaxOrder, 0) + 1, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @Description, @RuleCheckpoint, @RuleValidFrom, @RuleValidTo)
            
            SELECT @RuleID = SCOPE_IDENTITY()
      
      END

      EXEC dbo.RulesEngine_Rules_GetRulesByRuleSetID @ClientID, @RuleSetID, @RuleID
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_InsertUpdateRule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Rules_InsertUpdateRule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_InsertUpdateRule] TO [sp_executeall]
GO
