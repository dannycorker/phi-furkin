SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-06-2016
-- Description:	Gets a product payment schedule
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentSchedule__GetByCustomerID]
	@CustomerID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	SELECT schedule.PurchasedProductPaymentScheduleID, schedule.ClientID	, schedule.CustomerID, schedule.AccountID, schedule.PurchasedProductID, schedule.ActualCollectionDate, schedule.CoverFrom, schedule.CoverTo, schedule.PaymentDate, schedule.PaymentNet, schedule.PaymentVAT, schedule.PaymentGross, schedule.PaymentStatusID, schedule.CustomerLedgerID, schedule.ReconciledDate, schedule.CustomerPaymentScheduleID, schedule.WhoCreated	, schedule.WhenCreated, schedule.PurchasedProductPaymentScheduleParentID, schedule.PurchasedProductPaymentScheduleTypeID, schedule.PaymentGroupedIntoID, ps.*
	FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
	LEFT JOIN dbo.PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = schedule.PaymentStatusID
	WHERE schedule.CustomerID=@CustomerID AND schedule.ClientID=@ClientID
	ORDER BY schedule.PaymentDate ASC    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentSchedule__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetByCustomerID] TO [sp_executeall]
GO
