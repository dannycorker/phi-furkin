SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldPages table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailFieldPageID],
					[ClientID],
					[LeadOrMatter],
					[LeadTypeID],
					[PageName],
					[PageCaption],
					[PageOrder],
					[Enabled],
					[ResourceList],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[IsShared]
				FROM
					[dbo].[DetailFieldPages] WITH (NOLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_GetByLeadTypeID] TO [sp_executeall]
GO
