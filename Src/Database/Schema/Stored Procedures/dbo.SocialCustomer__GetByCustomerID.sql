SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-11-2015
-- Description:	Gets a Social Customer by Customer ID
-- =============================================
CREATE PROCEDURE [dbo].[SocialCustomer__GetByCustomerID]
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM SocialCustomer WITH (NOLOCK) 
	WHERE CustomerID=@CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialCustomer__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__GetByCustomerID] TO [sp_executeall]
GO
