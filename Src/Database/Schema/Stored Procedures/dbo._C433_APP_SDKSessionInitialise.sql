SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-10-14
-- Description:	Called when the internal quote and buy app starts up
-- =============================================
CREATE PROCEDURE [dbo].[_C433_APP_SDKSessionInitialise] 
(
	@EmailAddress VARCHAR(200),
	@SessionKey VARCHAR(200),
	@ThirdPartySystemId INT
)

AS
BEGIN
	
	IF EXISTS 
	(
		SELECT * 
		FROM dbo.ActiveSession WITH (NOLOCK) 
		WHERE	EmailAddress = @EmailAddress
		AND		ThirdPartySystemId = @ThirdPartySystemID
	) 
	BEGIN
		UPDATE dbo.ActiveSession
		SET SessionID = @SessionKey
		WHERE EmailAddress = @EmailAddress
		AND	ThirdPartySystemId = @ThirdPartySystemID
	END
	ELSE
	BEGIN
		INSERT dbo.ActiveSession (EmailAddress, SessionID, ThirdPartySystemId)
		VALUES (@EmailAddress, @SessionKey, @ThirdPartySystemId)
	END
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C433_APP_SDKSessionInitialise] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C433_APP_SDKSessionInitialise] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C433_APP_SDKSessionInitialise] TO [sp_executeall]
GO
