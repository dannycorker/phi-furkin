SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Equations table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Equations_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[EquationID],
					[EquationName],
					[Equation],
					[ClientQuestionnaireID],
					[ClientID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Equations] WITH (NOLOCK) 
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_GetBySourceID] TO [sp_executeall]
GO
