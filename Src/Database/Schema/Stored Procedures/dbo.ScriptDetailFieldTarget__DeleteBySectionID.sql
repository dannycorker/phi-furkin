SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-03-2015
-- Description:	Deletes all the script detial field targets for the given section
-- =============================================
CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget__DeleteBySectionID]
(
	@SectionID INT
)
AS

	DELETE FROM [dbo].[ScriptDetailFieldTarget] 
	WHERE [SectionID] = @SectionID
			


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__DeleteBySectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget__DeleteBySectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__DeleteBySectionID] TO [sp_executeall]
GO
