SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =========================================================
-- Author:		Jan Wilson
-- Create date: 2014-06-03
-- Description: Deletes a rule parameter including
--              its parameter options and output coordinates
-- =========================================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleParameters_Delete]
(
	@RuleParameterID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE oc
	FROM dbo.RulesEngine_OutputCoordinates oc WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) ON oc.ParameterOptionID = po.ParameterOptionID
	INNER JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = po.RuleParameterID
	WHERE rp.RuleParameterID = @RuleParameterID

	DELETE po 
	FROM dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON po.RuleParameterID = rp.RuleParameterID
	WHERE rp.RuleParameterID = @RuleParameterID
	
	DELETE FROM dbo.RulesEngine_RuleParameters 
	WHERE RuleParameterID = @RuleParameterID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleParameters_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_Delete] TO [sp_executeall]
GO
