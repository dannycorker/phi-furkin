SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventChoice table through an index
-- MODIFIED	2014-07-25	SB	Limited event choice by client ID due to shared lead types
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice__GetMaxWeightingByEventTypeID]
(
	@EventTypeID int,
	@ClientID INT = NULL  
)
AS

		SELECT
			isnull(MAX([Weighting]),0)
		FROM
			[dbo].[EventChoice] c
		WHERE
			c.[EventTypeID] = @EventTypeID
			AND EscalationEvent = 0
			AND (c.ClientID = @ClientID OR @ClientID IS NULL)


GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetMaxWeightingByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetMaxWeightingByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetMaxWeightingByEventTypeID] TO [sp_executeall]
GO
