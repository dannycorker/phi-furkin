SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailFieldPages table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailFieldPageID int   = null ,

	@ClientID int   = null ,

	@LeadOrMatter tinyint   = null ,

	@LeadTypeID int   = null ,

	@PageName varchar (50)  = null ,

	@PageCaption varchar (50)  = null ,

	@PageOrder int   = null ,

	@Enabled bit   = null ,

	@ResourceList bit   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@IsShared bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailFieldPageID]
	, [ClientID]
	, [LeadOrMatter]
	, [LeadTypeID]
	, [PageName]
	, [PageCaption]
	, [PageOrder]
	, [Enabled]
	, [ResourceList]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [IsShared]
    FROM
	dbo.fnDetailFieldPagesShared(@ClientID)
    WHERE 
	 ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadOrMatter] = @LeadOrMatter OR @LeadOrMatter IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([PageName] = @PageName OR @PageName IS NULL)
	AND ([PageCaption] = @PageCaption OR @PageCaption IS NULL)
	AND ([PageOrder] = @PageOrder OR @PageOrder IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([ResourceList] = @ResourceList OR @ResourceList IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailFieldPageID]
	, [ClientID]
	, [LeadOrMatter]
	, [LeadTypeID]
	, [PageName]
	, [PageCaption]
	, [PageOrder]
	, [Enabled]
	, [ResourceList]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [IsShared]
    FROM
	dbo.fnDetailFieldPagesShared(@ClientID) 
    WHERE 
	 ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadOrMatter] = @LeadOrMatter AND @LeadOrMatter is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([PageName] = @PageName AND @PageName is not null)
	OR ([PageCaption] = @PageCaption AND @PageCaption is not null)
	OR ([PageOrder] = @PageOrder AND @PageOrder is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([ResourceList] = @ResourceList AND @ResourceList is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Find] TO [sp_executeall]
GO
