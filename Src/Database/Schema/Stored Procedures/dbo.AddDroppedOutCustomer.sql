SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddDroppedOutCustomer]
@ClientID int,
@TitleID int,
@FirstName nvarchar(100),
@MiddleName nvarchar(100),
@LastName nvarchar(100),
@EmailAddress nvarchar(255),
@DayTimeTelephoneNumber nvarchar(50),
@HomeTelephone nvarchar(50),
@MobileTelephone nvarchar(50),
@CompanyTelephone nvarchar(50),
@WorksTelephone nvarchar(50),
@Address1 nvarchar(200),
@Address2 nvarchar(200), 
@Town nvarchar(200),
@County nvarchar(200),
@PostCode nvarchar(10),
@Test bit,
@CompanyName nvarchar(100),
@Occupation nvarchar(100),
@Employer nvarchar(100)

as

insert into DroppedOutCustomers (ClientID, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone,
CompanyTelephone, WorksTelephone,  Address1, Address2, Town, County, PostCode, Test, CompanyName, Occupation, Employer)
values (@ClientID, @TitleID, @FirstName, @MiddleName, @LastName, @EmailAddress, @DayTimeTelephoneNumber, @HomeTelephone, @MobileTelephone,
@CompanyTelephone, @WorksTelephone, @address1,@Address2, @Town, @County, @PostCode, @Test, @CompanyName, @Occupation, @Employer)

declare @CustomerID int

set @CustomerID = SCOPE_IDENTITY()

Select CustomerID from DroppedOutCustomers where CustomerID = @CustomerID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddDroppedOutCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomer] TO [sp_executeall]
GO
