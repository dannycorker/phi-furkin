SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-21
-- Description:	Grant access to this object to the specified user or group
-- JWG 2014-01-31 #25493 Set the level of access (0=none, 1=view, 4=full)
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnelAccess__Grant]
	@AccessRuleID int,
	@DataLoaderObjectTypeID int, 
	@ClientPersonnelAdminGroupID int, 
	@ClientPersonnelID int, 
	@PortalUserID int,
	@LeadEventID int, 
	@AccessLevel tinyint = 4
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
	@ClientID int,
	@LeadID int, 
	@CaseID int,
	@WhoCreated int

	SELECT 
	@ClientID = le.ClientID, 
	@LeadID = le.LeadID, 
	@CaseID = CASE WHEN @DataLoaderObjectTypeID = 3 THEN le.CaseID ELSE NULL END, 
	@WhoCreated = le.WhoCreated
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID
	
	IF @PortalUserID > 0
	BEGIN
		/* Use the existing PortalUserCase table for portal access */
		EXEC [dbo].[PortalUserCase__GrantAccess] @PortalUserID, @LeadID, @CaseID, @ClientID, @AccessLevel
	END
	ELSE
	BEGIN
		/* Use the new table ClientPersonnelAccess for user/group access to leads and cases */
		INSERT INTO dbo.ClientPersonnelAccess (
			ClientID, 
			ClientPersonnelAdminGroupID, 
			ClientPersonnelID, 
			LeadID, 
			CaseID, 
			AccessRuleID, 
			SourceLeadEventID, 
			SourceID, 
			WhoCreated, 
			WhenCreated, 
			WhoModified, 
			WhenModified, 
			AccessLevel
		)
		VALUES
		( 
			@ClientID, 
			@ClientPersonnelAdminGroupID, 
			@ClientPersonnelID, 
			@LeadID, 
			@CaseID, 
			@AccessRuleID, 
			@LeadEventID, 
			NULL, 
			@WhoCreated, 
			dbo.fn_GetDate_Local(), 
			@WhoCreated, 
			dbo.fn_GetDate_Local(), 
			ISNULL(@AccessLevel, 4) 
		)
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess__Grant] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess__Grant] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess__Grant] TO [sp_executeall]
GO
