SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CaseTransferRestrictedField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_Insert]
(

	@CaseTransferRestrictedFieldID int    OUTPUT,

	@ClientRelationshipID int   ,

	@DetailFieldID int   
)
AS


				
				INSERT INTO [dbo].[CaseTransferRestrictedField]
					(
					[ClientRelationshipID]
					,[DetailFieldID]
					)
				VALUES
					(
					@ClientRelationshipID
					,@DetailFieldID
					)
				-- Get the identity value
				SET @CaseTransferRestrictedFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Insert] TO [sp_executeall]
GO
