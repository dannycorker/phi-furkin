SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date:	2019-11-26
-- Description:	Errors a request in the Liberty Mediate API queue.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Mediate_Error]
(
    @AppKey         [NVARCHAR](50),
    @Queue          [NVARCHAR](50),
    @Flow           [NVARCHAR](50),
    @Action         [NVARCHAR](50),
    @Method         [NVARCHAR](50),
	@ObjectID		[NVARCHAR](MAX),
    @RequestBody    [NVARCHAR](MAX),
    @RequestType    [NVARCHAR](50),
    @ResponseBody   [NVARCHAR](MAX),
    @ResponseType   [NVARCHAR](50),
    @Exception      [NVARCHAR](2000)='',
    @ExceptionCount INT=0,
    @QueueID        [bigint]=0,
	@ParentObjectID	[NVARCHAR](MAX) = NULL
) AS
BEGIN
 
	SET NOCOUNT ON

    UPDATE  [dbo].[_C00_Mediate_Queue]
    SET     [Action] = @Action,
			[ModifyDate] = dbo.fn_GetDate_Local(),
			[ResponseBody] = @ResponseBody,
            [Exception] =	CASE WHEN 
								ISNULL(@Exception, '') > '' 
							THEN 
								CASE WHEN
									ISNULL([Exception], '') > ''
								THEN
									[Exception] + ' | ' + @Exception
								ELSE
									@Exception
								END
							ELSE
								[Exception]
							END,
            [ExceptionCount] += ISNULL(CASE WHEN @Exception > '' THEN 1 END, 0)
    OUTPUT inserted.*
    WHERE [QueueID] = @QueueID
    AND ExceptionCount < 5
    AND [Queue] = @Queue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Error] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Mediate_Error] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Error] TO [sp_executeall]
GO
