SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientRelationship table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship__GetGrid]
AS

	SELECT
		[ClientRelationshipID],
		ClientRelationshipName,
		c1.CompanyName as FromClient,
		c2.CompanyName as ToClient,
		l.LeadTypeName as FromLeadType,
		l1.LeadTypeName as ToLeadType,
		e.EventTypeName as FromEventType,
		e1.EventTypeName as ToEventType,
		cr.Enabled,
		cr.ClearLeadRefs
	FROM
		[dbo].[ClientRelationship] cr
	INNER JOIN 
		[dbo].[Clients] c1 on c1.ClientID = cr.OutgoingClientID
	INNER JOIN 
		[dbo].[Clients] c2 on c2.ClientID = cr.ReceivingClientID
	INNER JOIN
		[dbo].[LeadType] l on cr.OutgoingLeadTypeID = l.LeadTypeID
	INNER JOIN
		[dbo].[LeadType] l1 on cr.IncomingLeadTypeID = l1.LeadTypeID
	INNER JOIN 
		[dbo].[EventType] e on cr.OutgoingEventTypeID = e.EventTypeID
	INNER JOIN 
		[dbo].[EventType] e1 on cr.IncomingEventTypeID = e1.EventTypeID





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship__GetGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship__GetGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship__GetGrid] TO [sp_executeall]
GO
