SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-10
-- Description:	Helpful details about DetailFields
--				ACE - 20100315 Added WITH for NOLOCKS
--				CS 2013-09-10 - Added fn_C00_GetUrlByDatabase() to control URLs
-- JWG 2015-02-11 #30930 ForceEditableOverride
-- CS  2016-08-29 Include CustomViewRouting information
-- CS  2016-11-29 Include ThirdPartyFieldMapping information
-- CS  2017-03-09 Include CustomTableSQL information
-- 2020-01-21 CPS for LPC | Added OrderBy df.FieldOrder
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__Describe]
	@DetailFieldID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LorM tinyint, 
	@LeadTypeID int,
	@QTID tinyint, 
	@Lulid int, 
	@PageID int, 
	@RLPageID int, 
	@TablePageID int, 
	@TableRLDFID int, -- Older tables always contain a ResourceList as one of the columns. Show all these details as well
	@QueryText VARCHAR(MAX) = ''
	
	-- Get some useful values to decide what else to look up
	SELECT @LorM = LeadOrMatter, 
	@LeadTypeID = LeadTypeID,
	@QTID = QuestionTypeID, 
	@Lulid = LookupListID, 
	@PageID = DetailFieldPageID, 
	@RLPageID = ResourceListDetailFieldPageID, 
	@TablePageID = TableDetailFieldPageID 
	FROM dbo.DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID 
	
	/* ACE - 20100315 Added DetailFieldAlias */
	-- Headline Information
	SELECT 'Headline Information' as [Headline Information], df.DetailFieldID, df.FieldCaption, qt.Description, dfst.DetailFieldSubTypeDescription, df.DetailFieldPageID, df.ClientID, lt.LeadTypeName, dfa.DetailFieldAlias 
	, dbo.fn_C00_GetUrlByDatabase() + 'DetailFieldEdit.aspx?MenuID=pnl5m&ltid=' + convert(varchar,@LeadTypeID) + '&lom=2&dfid=' + convert(varchar,@DetailFieldID) + '&dfp=' + convert(varchar,@PageID) [Edit URL]
	FROM dbo.DetailFields df WITH (NOLOCK) 
	LEFT JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = df.LeadTypeID 
	INNER JOIN dbo.DetailFieldSubType dfst WITH (NOLOCK) ON dfst.DetailFieldSubTypeID = df.LeadOrMatter
	INNER JOIN dbo.QuestionTypes qt WITH (NOLOCK) ON qt.QuestionTypeID = df.QuestionTypeID
	LEFT JOIN dbo.DetailFieldAlias dfa WITH (NOLOCK) on dfa.DetailFieldID = df.DetailFieldID
	WHERE df.DetailFieldID = @DetailFieldID 
	
	-- Show all values
	SELECT 'Detail Field Information' as [Detail Field Information], df.* 
	FROM dbo.DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID 
	
	IF EXISTS ( SELECT * 
	            FROM ThirdPartyFieldMapping fm WITH ( NOLOCK ) 
	            WHERE @DetailFieldID IN ( fm.DetailFieldID, fm.ColumnFieldID ) )
	BEGIN
		SELECT 'ThirdPartyField Information' [ThirdPartyField Information]
			,ps.ThirdPartySystemId, ps.SystemName
			,pf.ThirdPartyFieldID, pf.FieldName
			,fg.ThirdPartyFieldGroupID, fg.GroupName
			,fm.*
		FROM ThirdPartyFieldMapping fm WITH ( NOLOCK ) 
		INNER JOIN ThirdPartyField pf WITH ( NOLOCK ) on pf.ThirdPartyFieldID = fm.ThirdPartyFieldID
		INNER JOIN ThirdPartySystem ps WITH ( NOLOCK ) on ps.ThirdPartySystemId = pf.ThirdPartySystemId
		LEFT JOIN ThirdPartyFieldGroup fg WITH ( NOLOCK ) on fg.ThirdPartyFieldGroupID = fm.ThirdPartyFieldGroupID
		WHERE @DetailFieldID IN ( fm.DetailFieldID, fm.ColumnFieldID )
		ORDER BY ps.ThirdPartySystemId, fm.ThirdPartyFieldID
	END
	
	IF EXISTS ( SELECT * 
	            FROM CustomTableSQL cts WITH ( NOLOCK ) 
	            WHERE cts.DetailFieldID = @DetailFieldID )
	BEGIN
		SELECT 'CustomTableSQL Information' [CustomTableSQL Information], ts.*, sq.QueryTitle
		FROM CustomTableSQL ts WITH ( NOLOCK ) 
		LEFT JOIN SqlQuery sq WITH ( NOLOCK ) on sq.QueryID = ts.SQLQueryID
		WHERE ts.DetailFieldID = @DetailFieldID
		
		SELECT @QueryText += ISNULL(ts.QueryText,sq.QueryText) + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)
		FROM CustomTableSQL ts WITH ( NOLOCK ) 
		INNER JOIN SqlQuery sq WITH (NOLOCK) on sq.QueryID = ts.SQLQueryID
		WHERE ts.DetailFieldID = @DetailFieldID
		
		PRINT @QueryText
		
	END
	
	/*Custom View Routing.  Show early in the output, but only give a resultset if there are records to include.*/
	IF EXISTS ( SELECT * 
	            FROM CustomViewRouting vr WITH ( NOLOCK ) 
	            WHERE @DetailFieldID IN ( vr.FieldID, vr.MapsToFieldID ) )
	BEGIN
		SELECT 'CustomViewRouting Information' [CustomViewRouting Information], * 
		FROM CustomViewRouting vr WITH ( NOLOCK ) 
		WHERE @DetailFieldID IN ( vr.FieldID, vr.MapsToFieldID )
	END
	
	-- Page Info
	SELECT 'Page Info' as [Page Info], dfp.* 
	FROM dbo.DetailFieldPages dfp WITH (NOLOCK)
	WHERE dfp.DetailFieldPageID = @PageID 

	-- Resource List 
	IF @LorM = 4 
	BEGIN
		SELECT 'Resource List Field' as [Resource List Field], df.* 
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.ResourceListDetailFieldPageID = @PageID 
		ORDER BY df.FieldOrder
	END
	
	-- Other fields on the same page
	SELECT 'Fields On This Page' as [Fields On This Page], df.* , dfa.*
	FROM dbo.DetailFields df WITH (NOLOCK)
	LEFT Join DetailFieldAlias dfa WITH (NOLOCK) on dfa.DetailFieldID = df.DetailFieldID
	WHERE df.DetailFieldPageID = @PageID 
	ORDER BY df.FieldOrder

	-- Lookup List info
	IF @QTID IN(4,2) /*CS 2015-05-31.  Added Radio Buttons*/
	BEGIN
		SELECT 'Lookup List Info' as [Lookup List Info], lu.* 
		FROM dbo.LookupList lu WITH (NOLOCK) 
		WHERE lu.LookupListID = @Lulid 

		SELECT luli.* 
		FROM dbo.LookupListItems luli WITH (NOLOCK) 
		WHERE luli.LookupListID = @Lulid 
	END
	
	-- Resource List info
	IF @QTID = 14
	BEGIN

		SELECT 'Show Resource Values' [Show Resource Values], 'rldv null,null,' + convert(varchar,@RLPageID)	
		
		SELECT 'Resource List Fields' as [Resource List Fields], df.* 
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldPageID = @RLPageID 
		ORDER BY df.FieldOrder

		SELECT 'Resource List Users' as [Resource List Users], df.* 
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.ResourceListDetailFieldPageID = @RLPageID 
		ORDER BY df.FieldOrder
	END
	
	-- Table info
	IF @QTID IN ( 16, 19 )
	BEGIN
		SELECT 'Table Fields' as [Table Fields], df.* 
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldPageID = @TablePageID 
		ORDER BY df.FieldOrder

		SELECT 'Table Users' as [Table Users], df.* 
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.TableDetailFieldPageID = @TablePageID 
		ORDER BY df.FieldOrder

		SELECT TOP 1 @TableRLDFID = df.DetailFieldID 
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldPageID = @TablePageID 
		AND df.QuestionTypeID = 14 
		ORDER BY df.FieldOrder

		-- Older tables always contain a ResourceList as one of the columns. Show all these details as well
		-- by calling this proc again with the Table-ResourceList-DetailFieldID
		IF @TableRLDFID IS NOT NULL
		BEGIN
			EXEC dbo.DetailFields__Describe @TableRLDFID
		END

	END
	
	/*Helper fields - added by CS 2012-01-09*/
	SELECT 'Helper Field on' [Helper Field], et.EventTypeID, et.EventTypeName, ef.ForceEditableOverride
	FROM dbo.EventTypeHelperField ef WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ef.EventTypeID 
	WHERE ef.DetailFieldID = @DetailFieldID 

	/*Mandatory fields - added by CS 2012-01-09*/	
	SELECT 'Mandatory Field on' [Mandatory Field], et.EventTypeID, et.EventTypeName
	FROM dbo.EventTypeMandatoryField ef WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ef.EventTypeID 
	WHERE ef.DetailFieldID = @DetailFieldID 
	
	/*Equation Targets - added by CS 2013-01-18*/
	SELECT 'Equation Targets' [Equation Targets],et.EquationTargetID, et.DetailFieldID, et.Target, df.FieldName, df.LeadTypeID, df.Enabled
	FROM EquationTarget et WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = et.DetailFieldID
	WHERE et.EquationDetailFieldID = @DetailFieldID

	SELECT 'Referenced by Equations' [Referenced by Equations],et.EquationTargetID, df.DetailFieldID, df.FieldName, df.EquationText, df.LeadTypeID
	FROM EquationTarget et WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = et.EquationDetailFieldID
	WHERE et.DetailFieldID = @DetailFieldID

	/*Linked to Events - added by CS 2012-05-06*/
	SELECT 'Linked to Event' [EventTypeFieldCompletion],ec.EventTypeFieldCompletionID,et.EventTypeID,et.EventTypeName, et.InProcess,ao_i.AquariumOptionName [Insert Option], ao_d.AquariumOptionName [Delete Option]
	FROM EventTypeFieldCompletion ec WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ec.EventTypeID 
	INNER JOIN dbo.AquariumOption ao_i WITH (NOLOCK) on ao_i.AquariumOptionID = ec.InsertOptionID
	INNER JOIN dbo.AquariumOption ao_d WITH (NOLOCK) on ao_d.AquariumOptionID = ec.DeleteOptionID
	WHERE ec.DetailFieldID = @DetailFieldID
	
	/* Links to eCatcher */
	SELECT 'eCatcher Links' as [eCatcher Links], dfl.*, mq.*   
	FROM dbo.DetailFieldLink dfl WITH (NOLOCK) 
	INNER JOIN dbo.MasterQuestions mq WITH (NOLOCK) on mq.MasterQuestionID = dfl.MasterQuestionID 
	WHERE dfl.DetailFieldID = @DetailFieldID 
	ORDER BY dfl.MasterQuestionID 
	
	-- Show all history for this field
	SELECT 'Detail Field History' as [Detail Field History], dfh.* 
	FROM dbo.DetailFieldsHistory dfh WITH (NOLOCK)
	WHERE dfh.DetailFieldID = @DetailFieldID 
	ORDER BY dfh.DetailFieldHistoryID 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__Describe] TO [sp_executehelper]
GO
