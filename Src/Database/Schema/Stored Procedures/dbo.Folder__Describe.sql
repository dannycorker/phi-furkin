SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Describe a folder and follow the hierarchy all the way up to the root folder
-- =============================================
CREATE PROCEDURE [dbo].[Folder__Describe]
	@FolderID int 
AS
BEGIN
	SET NOCOUNT ON;

	with folder_hierarchy(folderid, clientid, foldertypeid, folderparentid, foldername, whocreated, personal) 
	as 
	(
		select f1.folderid, f1.clientid, f1.foldertypeid, f1.folderparentid, f1.foldername, f1.whocreated, f1.personal 
		from dbo.folder f1 WITH (NOLOCK) 
		where f1.folderid = @folderid 
		
		union all
		
		select f2.folderid, f2.clientid, f2.foldertypeid, f2.folderparentid, f2.foldername, f2.whocreated, f2.personal 
		from dbo.folder f2 WITH (NOLOCK) 
		inner join folder_hierarchy fh on f2.folderid = fh.folderparentid
	)

	select f.folderid, f.folderparentid, f.foldername, f.personal, cp.username, c.companyname, ft.foldertypename, f.foldertypeid, f.whocreated, f.clientid 
	from folder_hierarchy f WITH (NOLOCK) --order by folderid
	inner join dbo.clients c WITH (NOLOCK) on c.clientid = f.clientid 
	inner join dbo.clientpersonnel cp WITH (NOLOCK) on cp.clientpersonnelid = f.whocreated 
	inner join dbo.foldertype ft WITH (NOLOCK) on ft.foldertypeid = f.foldertypeid
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[Folder__Describe] TO [sp_executehelper]
GO
