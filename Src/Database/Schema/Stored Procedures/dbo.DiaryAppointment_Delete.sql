SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DiaryAppointment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_Delete]
(

	@DiaryAppointmentID int   
)
AS


				DELETE FROM [dbo].[DiaryAppointment] WITH (ROWLOCK) 
				WHERE
					[DiaryAppointmentID] = @DiaryAppointmentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Delete] TO [sp_executeall]
GO
