SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerOutcomes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOutcomes_GetByCustomerIDOutcomeID]
(

	@CustomerID int   ,

	@OutcomeID int   
)
AS


				SELECT
					[CustomerOutcomeID],
					[ClientQuestionnaireID],
					[CustomerID],
					[OutcomeID],
					[ClientID]
				FROM
					[dbo].[CustomerOutcomes] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
					AND [OutcomeID] = @OutcomeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_GetByCustomerIDOutcomeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOutcomes_GetByCustomerIDOutcomeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_GetByCustomerIDOutcomeID] TO [sp_executeall]
GO
