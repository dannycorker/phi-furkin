SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 15/08/2007
-- Description:	Used to get the next reminder that is now due and
--				are not yet completed
-- =============================================
CREATE PROCEDURE [dbo].[DiaryReminder__GetCurrentRemindersByClientPersonnelID] 

	@ClientPersonnelID int,
	@ClientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		TOP 1
				DiaryReminder.DiaryReminderID,
				DiaryReminder.ReminderDate,
				DiaryAppointment.DiaryAppointmentTitle,
				DiaryAppointment.DiaryAppointmentText,
				DiaryAppointment.DueDate, 
				DiaryAppointment.EndDate,
				DiaryAppointment.AllDayEvent,
				DiaryAppointment.LeadID,
				DiaryAppointment.CaseID,
				DiaryAppointment.ExportVersion  
	FROM		DiaryReminder WITH (NOLOCK)
	INNER JOIN	DiaryAppointment WITH (NOLOCK) ON DiaryReminder.DiaryAppointmentID = DiaryAppointment.DiaryAppointmentID
	WHERE		(DiaryReminder.ClientPersonnelID = @ClientPersonnelID)
	AND			(DiaryAppointment.Completed = 0)
	AND			(DiaryReminder.ReminderTimeshiftID <>1)
	AND			(DiaryReminder.ReminderDate <= dbo.fn_GetDate_Local())
	AND			(DiaryReminder.ClientID = @ClientID)
	ORDER BY	DiaryReminder.ReminderDate
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder__GetCurrentRemindersByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder__GetCurrentRemindersByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder__GetCurrentRemindersByClientPersonnelID] TO [sp_executeall]
GO
