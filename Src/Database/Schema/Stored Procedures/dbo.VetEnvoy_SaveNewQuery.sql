SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-24
-- Description:	Saves a new query to the db
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_SaveNewQuery]
(
	@ClientID INT,
	@AccountID INT,
	@ConversationID UNIQUEIDENTIFIER,
	@Xml XML,
	@SenderID UNIQUEIDENTIFIER,
	@HistoryID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ID INT
	
	IF NOT EXISTS (SELECT * FROM VetEnvoy_Queries WHERE ConversationID = @ConversationID)
	BEGIN
	
		INSERT dbo.VetEnvoy_Queries(ClientID, AccountID, ConversationID, Xml, Received, SenderID)
		VALUES (@ClientID, @AccountID, @ConversationID, @Xml, dbo.fn_GetDate_Local(), @SenderID) 
		
		SELECT @ID = SCOPE_IDENTITY()
		
	END
	ELSE
	BEGIN
	
		SELECT @ID = QueryID
		FROM dbo.VetEnvoy_Queries WITH (NOLOCK) 
		WHERE ConversationID = @ConversationID
	
	END
	
	DECLARE @Sql NVARCHAR(2000)
	SELECT @Sql = 'EXEC dbo._C' + CAST(@ClientID AS VARCHAR(50)) + '_VetEnvoy_SaveNewQuery ' + CAST(@ID AS VARCHAR(50))
	EXEC sp_executesql @Sql
	
	UPDATE dbo.VetEnvoy_ConversationHistory
	SET Actioned = dbo.fn_GetDate_Local()
	WHERE HistoryID = @HistoryID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SaveNewQuery] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_SaveNewQuery] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SaveNewQuery] TO [sp_executeall]
GO
