SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 14-08-09
-- Description:	Gets a list of DetailFields for use in the ReferenceGenerator
-- Modified By: Paul Richardson
-- Description: Removed DetailFieldSubType check
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE  [dbo].[DetailFields__GetForReferenceValueGenerator]
	@ClientID int,
	@LeadTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		df.DetailFieldID,
		df.ClientID,
		df.QuestionTypeID,
		df.LeadOrMatter,
		(dfp.PageCaption + ' : ' + df.FieldName + ' (' + CONVERT(varchar(5), df.DetailFieldID) + ') ') AS DDLText
	FROM dbo.fnDetailFieldsShared(@ClientID) df
	INNER JOIN DetailFieldPages dfp ON dfp.DetailFieldPageID = df.DetailFieldPageID
	WHERE (df.ClientID = @ClientID)
	AND (df.QuestionTypeID <> 14)
	/*AND (df.LeadOrMatter = 1 OR df.LeadOrMatter = 2)*/
	AND (df.QuestionTypeID <> 16)
	AND (df.LeadTypeID = @LeadTypeID)
	ORDER BY DDLText
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForReferenceValueGenerator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetForReferenceValueGenerator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForReferenceValueGenerator] TO [sp_executeall]
GO
