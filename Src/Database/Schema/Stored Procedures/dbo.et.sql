SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all EventTypes for a client and leadtype
-- =============================================
CREATE PROCEDURE [dbo].[et] 
	@ClientID int = NULL, 
	@LeadTypeID int = NULL, 
	@EventSubtypeID int = NULL  
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.EventType et(nolock) 
	WHERE (et.ClientID = @ClientID OR @ClientID IS NULL) 
	AND (et.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
	AND (et.EventSubtypeID = @EventSubtypeID OR @EventSubtypeID IS NULL) 
	ORDER BY et.ClientID, et.EventTypeName, et.LeadTypeID, et.EventSubtypeID 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[et] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[et] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[et] TO [sp_executeall]
GO
