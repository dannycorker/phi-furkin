SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-01
-- Description:	Delete from QuickSearchOptions until the DAL has been built..
-- =============================================
CREATE PROCEDURE [dbo].[QuickSearchOptions__Delete]
	@QuickSearchOptionID INT
AS
BEGIN

	SET NOCOUNT ON;

	/*
		SP Used to populate the QuickSearchOptions table
		Standard DDL options in the app are (Caption/Code):
		"Last name", "ln"
		"Business name", "bn"
		helper.PostcodeText, "pc"
		"Phone number", "pn"
		customerCaption + " ID", "cu"
		caseCaption + " ID", "aid"
		matterCaption + " ID", "mid"
		globalLeadAlias + " Ref", "lr"
		matterCaption + " Ref", "mr"
		"Email address", "ea"
	*/

	DELETE q
	FROM QuickSearchOptions q
	WHERE q.QuickSearchOptionID = @QuickSearchOptionID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[QuickSearchOptions__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuickSearchOptions__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuickSearchOptions__Delete] TO [sp_executeall]
GO
