SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientRelationship table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientRelationshipID int   = null ,

	@ClientRelationshipName varchar (200)  = null ,

	@OutgoingClientID int   = null ,

	@ReceivingClientID int   = null ,

	@OutgoingLeadTypeID int   = null ,

	@IncomingLeadTypeID int   = null ,

	@OutgoingEventTypeID int   = null ,

	@IncomingEventTypeID int   = null ,

	@Enabled bit   = null ,

	@ClearLeadRefs bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientRelationshipID]
	, [ClientRelationshipName]
	, [OutgoingClientID]
	, [ReceivingClientID]
	, [OutgoingLeadTypeID]
	, [IncomingLeadTypeID]
	, [OutgoingEventTypeID]
	, [IncomingEventTypeID]
	, [Enabled]
	, [ClearLeadRefs]
    FROM
	[dbo].[ClientRelationship] WITH (NOLOCK) 
    WHERE 
	 ([ClientRelationshipID] = @ClientRelationshipID OR @ClientRelationshipID IS NULL)
	AND ([ClientRelationshipName] = @ClientRelationshipName OR @ClientRelationshipName IS NULL)
	AND ([OutgoingClientID] = @OutgoingClientID OR @OutgoingClientID IS NULL)
	AND ([ReceivingClientID] = @ReceivingClientID OR @ReceivingClientID IS NULL)
	AND ([OutgoingLeadTypeID] = @OutgoingLeadTypeID OR @OutgoingLeadTypeID IS NULL)
	AND ([IncomingLeadTypeID] = @IncomingLeadTypeID OR @IncomingLeadTypeID IS NULL)
	AND ([OutgoingEventTypeID] = @OutgoingEventTypeID OR @OutgoingEventTypeID IS NULL)
	AND ([IncomingEventTypeID] = @IncomingEventTypeID OR @IncomingEventTypeID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([ClearLeadRefs] = @ClearLeadRefs OR @ClearLeadRefs IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientRelationshipID]
	, [ClientRelationshipName]
	, [OutgoingClientID]
	, [ReceivingClientID]
	, [OutgoingLeadTypeID]
	, [IncomingLeadTypeID]
	, [OutgoingEventTypeID]
	, [IncomingEventTypeID]
	, [Enabled]
	, [ClearLeadRefs]
    FROM
	[dbo].[ClientRelationship] WITH (NOLOCK) 
    WHERE 
	 ([ClientRelationshipID] = @ClientRelationshipID AND @ClientRelationshipID is not null)
	OR ([ClientRelationshipName] = @ClientRelationshipName AND @ClientRelationshipName is not null)
	OR ([OutgoingClientID] = @OutgoingClientID AND @OutgoingClientID is not null)
	OR ([ReceivingClientID] = @ReceivingClientID AND @ReceivingClientID is not null)
	OR ([OutgoingLeadTypeID] = @OutgoingLeadTypeID AND @OutgoingLeadTypeID is not null)
	OR ([IncomingLeadTypeID] = @IncomingLeadTypeID AND @IncomingLeadTypeID is not null)
	OR ([OutgoingEventTypeID] = @OutgoingEventTypeID AND @OutgoingEventTypeID is not null)
	OR ([IncomingEventTypeID] = @IncomingEventTypeID AND @IncomingEventTypeID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([ClearLeadRefs] = @ClearLeadRefs AND @ClearLeadRefs is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Find] TO [sp_executeall]
GO
