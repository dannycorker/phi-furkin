SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Matter table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_Insert]
(

	@MatterID int    OUTPUT,

	@ClientID int   ,

	@MatterRef varchar (100)  ,

	@CustomerID int   ,

	@LeadID int   ,

	@MatterStatus tinyint   ,

	@RefLetter varchar (3)  ,

	@BrandNew bit   ,

	@CaseID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime2   ,

	@WhoModified int   ,

	@WhenModified datetime2   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[Matter]
					(
					[ClientID]
					,[MatterRef]
					,[CustomerID]
					,[LeadID]
					,[MatterStatus]
					,[RefLetter]
					,[BrandNew]
					,[CaseID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@MatterRef
					,@CustomerID
					,@LeadID
					,@MatterStatus
					,@RefLetter
					,@BrandNew
					,@CaseID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@SourceID
					)
				-- Get the identity value
				SET @MatterID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Insert] TO [sp_executeall]
GO
