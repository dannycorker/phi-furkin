SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetEventChoices] @EventTypeID int 

AS

SELECT  Description, EventTypeID, NextEventTypeID, EventChoiceID
FROM    dbo.EventChoice
WHERE   (EventTypeID = @EventTypeID)
AND		EscalationEvent = 0



GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoices] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventChoices] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoices] TO [sp_executeall]
GO
