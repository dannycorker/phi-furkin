SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SiteMap table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SiteMap_Find]
(

	@SearchUsingOR bit   = null ,

	@SiteMapID int   = null ,

	@Url varchar (255)  = null ,

	@Title varchar (255)  = null ,

	@Description varchar (255)  = null ,

	@ParentID int   = null ,

	@Secured bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SiteMapID]
	, [Url]
	, [Title]
	, [Description]
	, [ParentID]
	, [Secured]
    FROM
	[dbo].[SiteMap] WITH (NOLOCK) 
    WHERE 
	 ([SiteMapID] = @SiteMapID OR @SiteMapID IS NULL)
	AND ([Url] = @Url OR @Url IS NULL)
	AND ([Title] = @Title OR @Title IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([ParentID] = @ParentID OR @ParentID IS NULL)
	AND ([Secured] = @Secured OR @Secured IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SiteMapID]
	, [Url]
	, [Title]
	, [Description]
	, [ParentID]
	, [Secured]
    FROM
	[dbo].[SiteMap] WITH (NOLOCK) 
    WHERE 
	 ([SiteMapID] = @SiteMapID AND @SiteMapID is not null)
	OR ([Url] = @Url AND @Url is not null)
	OR ([Title] = @Title AND @Title is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([ParentID] = @ParentID AND @ParentID is not null)
	OR ([Secured] = @Secured AND @Secured is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SiteMap_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Find] TO [sp_executeall]
GO
