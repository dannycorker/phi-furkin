SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteMasterQuestion    Script Date: 08/09/2006 12:22:51 ******/

CREATE PROCEDURE [dbo].[DeleteMasterQuestion] @MasterQuestionID int

AS

Delete From QuestionPossibleAnswers
Where MasterQuestionID = @MasterQuestionID

Delete From MasterQuestions
Where MasterQuestionID = @MasterQuestionID




GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteMasterQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteMasterQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteMasterQuestion] TO [sp_executeall]
GO
