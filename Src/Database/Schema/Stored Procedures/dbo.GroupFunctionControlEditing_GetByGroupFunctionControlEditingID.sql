SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupFunctionControlEditing table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing_GetByGroupFunctionControlEditingID]
(

	@GroupFunctionControlEditingID int   
)
AS


				SELECT
					[GroupFunctionControlEditingID],
					[ClientPersonnelAdminGroupID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[GroupFunctionControlEditing] WITH (NOLOCK) 
				WHERE
										[GroupFunctionControlEditingID] = @GroupFunctionControlEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_GetByGroupFunctionControlEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing_GetByGroupFunctionControlEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_GetByGroupFunctionControlEditingID] TO [sp_executeall]
GO
