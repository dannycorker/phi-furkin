SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestionGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_GetBySMSQuestionnaireID]
(

	@SMSQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSQuestionGroupID],
					[ClientID],
					[SMSQuestionnaireID],
					[GroupName],
					[SMSQuestionOrderID],
					[GroupOrder]
				FROM
					[dbo].[SMSQuestionGroup] WITH (NOLOCK) 
				WHERE
					[SMSQuestionnaireID] = @SMSQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_GetBySMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
