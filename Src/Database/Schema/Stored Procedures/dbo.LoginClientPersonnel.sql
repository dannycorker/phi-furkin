SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Unknown
-- Create date: Unknown
-- Description:	List all DetailFields for a client, leadtype, leadormatter and questiontypeid.
-- 2008-01-02 JWG Send back a dummy record if the username is ok but the password is wrong.
-- 2009-04-30 JWG Use a new @AppLogin flag to prevent LeadX from logging on to the main app, even though they can logon to the web service.
-- 2009-11-25 JWG Store login attempts in a history table for checks/reporting.
-- 2010-03-31 ACE Added __LogonExclusions
-- 2012-08-13 JWG Added IsAquarium flag to identify power users
-- 2012-09-12 JWG Added TP, HostName and IPDetails
-- 2016-01-21 PR Added LoginHistoryID - to store against LoginHistoryBrowserInformation see ticket #33718
-- =============================================
CREATE PROCEDURE [dbo].[LoginClientPersonnel] 
	@UserName VARCHAR(255), 
	@Password VARCHAR(65), 
	@AppLogin BIT = 0, 
	@AppInfo VARCHAR(50) = NULL,
	@ThirdPartySystemID INT = NULL,
	@UserIPDetails VARCHAR(50) = NULL
	
AS
BEGIN
	
	/*
		Scenarios:
		
		1) User not found
		2) Password not valid
		3) Account disabled
		4) User cannot login like this (eg not from the App)
		
		There are some that we can't cater for here though:
		a) User can only login from restricted IP Addresses
		b) Too many failed logins
	*/
	DECLARE @ClientID INT, 
	@ClientPersonnelID INT,
	@AttemptedLogins INT,
	@AccountDisabled BIT,
	@StoredPassword VARCHAR(65), 
	@IsSuccess BIT = 1,
	@Notes VARCHAR(2000) = 'Passed to the app for verification'
	
	SELECT @ClientID = ClientID, 
	@ClientPersonnelID = ClientPersonnelID,
	@AttemptedLogins = AttemptedLogins,
	@AccountDisabled = AccountDisabled,
	@StoredPassword = [Password] 
	FROM ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnel.EmailAddress = @UserName 
	AND ThirdPartySystemId = 0

	/*
		1) If all NULL at this point, then user not found
	*/
	IF @ClientPersonnelID IS NULL
	BEGIN
		SELECT @IsSuccess = 0, @Notes = 'User not found'
	END
	ELSE
	BEGIN
		/*
			2) If password doesn't match then FAIL
		*/
		IF ((@StoredPassword IS NULL) OR (@StoredPassword = '') OR (@Password IS NULL) OR (@StoredPassword <> @Password))
		BEGIN
			SELECT @IsSuccess = 0, @Notes = 'Password Incorrect'
		END
		ELSE
		BEGIN
		
			/*
				3) If account disabled then FAIL
			*/
			IF @AccountDisabled = 1
			BEGIN
				SELECT @IsSuccess = 0, @Notes = 'Account Disabled'
			END
			ELSE
			BEGIN
			
				/*
					4) If restricted user then FAIL
				*/
				IF (@AppLogin = 1 AND EXISTS(SELECT * FROM __LoginExclusions l WITH (NOLOCK) WHERE l.ClientPersonnelID = @ClientPersonnelID))
				BEGIN
					SELECT @IsSuccess = 0, @Notes = 'Restricted User'
				END
				
			END
			
		END
		
	END
	
	
	/*
		Log the results
	*/
	INSERT INTO dbo.LoginHistory (
		UserName, 
		LoginDateTime, 
		IsSuccess, 
		ClientID, 
		ClientPersonnelID, 
		AppLogin, 
		AttemptedLogins, 
		AccountDisabled, 
		Notes,
		ThirdPartySystemID,
		HostName,
		UserIPDetails
	) 
	VALUES 
	( 
		@UserName, 
		dbo.fn_GetDate_Local(), 
		@IsSuccess, 
		@ClientID, 
		@ClientPersonnelID, 
		@AppLogin, 
		@AttemptedLogins, 
		@AccountDisabled, 
		@Notes,
		@ThirdPartySystemID,			
		HOST_NAME(),
		@UserIPDetails
	)	
		
	DECLARE @LoginHistoryID INT
	SELECT @LoginHistoryID = SCOPE_IDENTITY()
		
	SELECT 
	cp.ClientID, 
	cp.ClientOfficeID,
	cp.FirstName, 
	cp.MiddleName, 
	cp.LastName, 
	cp.ClientPersonnelID,
	cp.ClientPersonnelAdminGroupID,  
	cp.EmailAddress,
	cp.AttemptedLogins,
	cp.AccountDisabled,
	cp.ChargeOutRate, 
	cp.LanguageID, 
	co.CountryID, 
	ISNULL(cp.IsAquarium, 0) AS IsAquarium ,
	@LoginHistoryID LoginHistoryID
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = cp.ClientOfficeID 
	WHERE cp.[EmailAddress] = @UserName 
	AND cp.[Password] = @Password 
	AND @IsSuccess = 1 
	AND ThirdPartySystemId = 0

	UNION

	SELECT 
	-1 AS ClientID, 
	NULL AS ClientOfficeID,
	NULL AS FirstName, 
	NULL AS MiddleName, 
	NULL AS LastName, 
	cp.ClientPersonnelID,
	cp.ClientPersonnelAdminGroupID,  
	cp.EmailAddress,
	cp.AttemptedLogins,
	cp.AccountDisabled,
	cp.ChargeOutRate, 
	cp.LanguageID, 
	co.CountryID, 
	ISNULL(cp.IsAquarium, 0) AS IsAquarium ,
	@LoginHistoryID LoginHistoryID
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = cp.ClientOfficeID
	WHERE cp.[EmailAddress] = @UserName 
	AND cp.[Password] <> @Password 
	AND ThirdPartySystemId = 0


END
GO
GRANT VIEW DEFINITION ON  [dbo].[LoginClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LoginClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LoginClientPersonnel] TO [sp_executeall]
GO
