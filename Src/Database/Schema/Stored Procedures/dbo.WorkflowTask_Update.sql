SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the WorkflowTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTask_Update]
(

	@WorkflowTaskID int   ,

	@WorkflowGroupID int   ,

	@AutomatedTaskID int   ,

	@Priority int   ,

	@AssignedTo int   ,

	@AssignedDate datetime   ,

	@LeadID int   ,

	@CaseID int   ,

	@EventTypeID int   ,

	@ClientID int   ,

	@FollowUp bit   ,

	@Important bit   ,

	@CreationDate datetime   ,

	@Escalated bit   ,

	@EscalatedBy int   ,

	@EscalationReason varchar (255)  ,

	@EscalationDate datetime   ,

	@Disabled bit   ,

	@DisabledReason varchar (255)  ,

	@DisabledDate datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[WorkflowTask]
				SET
					[WorkflowGroupID] = @WorkflowGroupID
					,[AutomatedTaskID] = @AutomatedTaskID
					,[Priority] = @Priority
					,[AssignedTo] = @AssignedTo
					,[AssignedDate] = @AssignedDate
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[EventTypeID] = @EventTypeID
					,[ClientID] = @ClientID
					,[FollowUp] = @FollowUp
					,[Important] = @Important
					,[CreationDate] = @CreationDate
					,[Escalated] = @Escalated
					,[EscalatedBy] = @EscalatedBy
					,[EscalationReason] = @EscalationReason
					,[EscalationDate] = @EscalationDate
					,[Disabled] = @Disabled
					,[DisabledReason] = @DisabledReason
					,[DisabledDate] = @DisabledDate
				WHERE
[WorkflowTaskID] = @WorkflowTaskID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Update] TO [sp_executeall]
GO
