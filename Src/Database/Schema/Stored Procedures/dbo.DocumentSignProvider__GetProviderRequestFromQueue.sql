SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- Modified By PR on 16/03/2016 Added 	DeclinedOrVoidedEventTypeID, DeclinedOrVoidedLeadEventID
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetProviderRequestFromQueue]
	@OutLeadEventID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	
		dspa.DSPTypeID,
		dspu.UserName,
		dspu.Password,
		dspa.IntegrationKey,
		dspa.AccountKey,
		dspq.DSPSignatureTemplate,
		dspq.DSPBrandingKey,
		dspq.ClientID,
	    dspq.LeadID,
	    dspq.CaseID,
	    dspq.InEventTypeID,
	    dspa.DefaultEnvelopeXML,
	    dspa.EndPointURL,
	    dspq.DeclinedOrVoidedEventTypeID,
	    dspq.DeclinedOrVoidedLeadEventID
	FROM	DocumentSignProviderQueue dspq WITH (NOLOCK) 
	INNER JOIN DocumentSignProviderAccount dspa WITH (NOLOCK) ON dspa.DSPAccountID = dspq.DSPAccountID
	INNER JOIN DocumentSignProviderUser dspu WITH (NOLOCK) ON dspu.DSPUserID = dspq.DSPUserID
	WHERE	OutLeadEventID = @OutLeadEventID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetProviderRequestFromQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetProviderRequestFromQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetProviderRequestFromQueue] TO [sp_executeall]
GO
