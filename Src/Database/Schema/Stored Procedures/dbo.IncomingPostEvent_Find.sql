SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the IncomingPostEvent table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEvent_Find]
(

	@SearchUsingOR bit   = null ,

	@IncomingPostEventID int   = null ,

	@EventDateTime datetime   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@MatterID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [IncomingPostEventID]
	, [EventDateTime]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [MatterID]
    FROM
	[dbo].[IncomingPostEvent] WITH (NOLOCK) 
    WHERE 
	 ([IncomingPostEventID] = @IncomingPostEventID OR @IncomingPostEventID IS NULL)
	AND ([EventDateTime] = @EventDateTime OR @EventDateTime IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [IncomingPostEventID]
	, [EventDateTime]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [MatterID]
    FROM
	[dbo].[IncomingPostEvent] WITH (NOLOCK) 
    WHERE 
	 ([IncomingPostEventID] = @IncomingPostEventID AND @IncomingPostEventID is not null)
	OR ([EventDateTime] = @EventDateTime AND @EventDateTime is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEvent_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Find] TO [sp_executeall]
GO
