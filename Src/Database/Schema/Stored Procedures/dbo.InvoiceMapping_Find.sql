SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the InvoiceMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@InvoiceMappingID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@SourceInvoiceNumberField int   = null ,

	@SourceTotalClaimAmountField int   = null ,

	@SourceFeesToPostToSageNetField int   = null ,

	@SourceFeesToPostToSageVatField int   = null ,

	@InvoiceTypeCode varchar (50)  = null ,

	@CreditNoteTypeCode varchar (50)  = null ,

	@InvoiceNumberField int   = null ,

	@DateField int   = null ,

	@DatePostedField int   = null ,

	@DetailsField int   = null ,

	@SentToSageField int   = null ,

	@NominalCodeField int   = null ,

	@TaxCodeField int   = null ,

	@InvoiceNetAmountField int   = null ,

	@InvoiceTaxAmountField int   = null ,

	@TypeField int   = null ,

	@MatterIDField int   = null ,

	@CaseNumberField int   = null ,

	@CreditNoteNetAmountField int   = null ,

	@CreditNoteTaxAmountField int   = null ,

	@NominalCode varchar (50)  = null ,

	@TaxCode varchar (50)  = null ,

	@TableRowsDetailFieldID int   = null ,

	@TableRowsDetailFieldPageID int   = null ,

	@FeesInvoicedToSageField int   = null ,

	@FeesPaidFromSageField int   = null ,

	@FeesInvoicedOtherField int   = null ,

	@FeesPaidOtherField int   = null ,

	@CaseBalanceField int   = null ,

	@SageCompanyName varchar (250)  = null ,

	@CostCentre varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [InvoiceMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [SourceInvoiceNumberField]
	, [SourceTotalClaimAmountField]
	, [SourceFeesToPostToSageNetField]
	, [SourceFeesToPostToSageVatField]
	, [InvoiceTypeCode]
	, [CreditNoteTypeCode]
	, [InvoiceNumberField]
	, [DateField]
	, [DatePostedField]
	, [DetailsField]
	, [SentToSageField]
	, [NominalCodeField]
	, [TaxCodeField]
	, [InvoiceNetAmountField]
	, [InvoiceTaxAmountField]
	, [TypeField]
	, [MatterIDField]
	, [CaseNumberField]
	, [CreditNoteNetAmountField]
	, [CreditNoteTaxAmountField]
	, [NominalCode]
	, [TaxCode]
	, [TableRowsDetailFieldID]
	, [TableRowsDetailFieldPageID]
	, [FeesInvoicedToSageField]
	, [FeesPaidFromSageField]
	, [FeesInvoicedOtherField]
	, [FeesPaidOtherField]
	, [CaseBalanceField]
	, [SageCompanyName]
	, [CostCentre]
    FROM
	[dbo].[InvoiceMapping] WITH (NOLOCK) 
    WHERE 
	 ([InvoiceMappingID] = @InvoiceMappingID OR @InvoiceMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([SourceInvoiceNumberField] = @SourceInvoiceNumberField OR @SourceInvoiceNumberField IS NULL)
	AND ([SourceTotalClaimAmountField] = @SourceTotalClaimAmountField OR @SourceTotalClaimAmountField IS NULL)
	AND ([SourceFeesToPostToSageNetField] = @SourceFeesToPostToSageNetField OR @SourceFeesToPostToSageNetField IS NULL)
	AND ([SourceFeesToPostToSageVatField] = @SourceFeesToPostToSageVatField OR @SourceFeesToPostToSageVatField IS NULL)
	AND ([InvoiceTypeCode] = @InvoiceTypeCode OR @InvoiceTypeCode IS NULL)
	AND ([CreditNoteTypeCode] = @CreditNoteTypeCode OR @CreditNoteTypeCode IS NULL)
	AND ([InvoiceNumberField] = @InvoiceNumberField OR @InvoiceNumberField IS NULL)
	AND ([DateField] = @DateField OR @DateField IS NULL)
	AND ([DatePostedField] = @DatePostedField OR @DatePostedField IS NULL)
	AND ([DetailsField] = @DetailsField OR @DetailsField IS NULL)
	AND ([SentToSageField] = @SentToSageField OR @SentToSageField IS NULL)
	AND ([NominalCodeField] = @NominalCodeField OR @NominalCodeField IS NULL)
	AND ([TaxCodeField] = @TaxCodeField OR @TaxCodeField IS NULL)
	AND ([InvoiceNetAmountField] = @InvoiceNetAmountField OR @InvoiceNetAmountField IS NULL)
	AND ([InvoiceTaxAmountField] = @InvoiceTaxAmountField OR @InvoiceTaxAmountField IS NULL)
	AND ([TypeField] = @TypeField OR @TypeField IS NULL)
	AND ([MatterIDField] = @MatterIDField OR @MatterIDField IS NULL)
	AND ([CaseNumberField] = @CaseNumberField OR @CaseNumberField IS NULL)
	AND ([CreditNoteNetAmountField] = @CreditNoteNetAmountField OR @CreditNoteNetAmountField IS NULL)
	AND ([CreditNoteTaxAmountField] = @CreditNoteTaxAmountField OR @CreditNoteTaxAmountField IS NULL)
	AND ([NominalCode] = @NominalCode OR @NominalCode IS NULL)
	AND ([TaxCode] = @TaxCode OR @TaxCode IS NULL)
	AND ([TableRowsDetailFieldID] = @TableRowsDetailFieldID OR @TableRowsDetailFieldID IS NULL)
	AND ([TableRowsDetailFieldPageID] = @TableRowsDetailFieldPageID OR @TableRowsDetailFieldPageID IS NULL)
	AND ([FeesInvoicedToSageField] = @FeesInvoicedToSageField OR @FeesInvoicedToSageField IS NULL)
	AND ([FeesPaidFromSageField] = @FeesPaidFromSageField OR @FeesPaidFromSageField IS NULL)
	AND ([FeesInvoicedOtherField] = @FeesInvoicedOtherField OR @FeesInvoicedOtherField IS NULL)
	AND ([FeesPaidOtherField] = @FeesPaidOtherField OR @FeesPaidOtherField IS NULL)
	AND ([CaseBalanceField] = @CaseBalanceField OR @CaseBalanceField IS NULL)
	AND ([SageCompanyName] = @SageCompanyName OR @SageCompanyName IS NULL)
	AND ([CostCentre] = @CostCentre OR @CostCentre IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [InvoiceMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [SourceInvoiceNumberField]
	, [SourceTotalClaimAmountField]
	, [SourceFeesToPostToSageNetField]
	, [SourceFeesToPostToSageVatField]
	, [InvoiceTypeCode]
	, [CreditNoteTypeCode]
	, [InvoiceNumberField]
	, [DateField]
	, [DatePostedField]
	, [DetailsField]
	, [SentToSageField]
	, [NominalCodeField]
	, [TaxCodeField]
	, [InvoiceNetAmountField]
	, [InvoiceTaxAmountField]
	, [TypeField]
	, [MatterIDField]
	, [CaseNumberField]
	, [CreditNoteNetAmountField]
	, [CreditNoteTaxAmountField]
	, [NominalCode]
	, [TaxCode]
	, [TableRowsDetailFieldID]
	, [TableRowsDetailFieldPageID]
	, [FeesInvoicedToSageField]
	, [FeesPaidFromSageField]
	, [FeesInvoicedOtherField]
	, [FeesPaidOtherField]
	, [CaseBalanceField]
	, [SageCompanyName]
	, [CostCentre]
    FROM
	[dbo].[InvoiceMapping] WITH (NOLOCK) 
    WHERE 
	 ([InvoiceMappingID] = @InvoiceMappingID AND @InvoiceMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([SourceInvoiceNumberField] = @SourceInvoiceNumberField AND @SourceInvoiceNumberField is not null)
	OR ([SourceTotalClaimAmountField] = @SourceTotalClaimAmountField AND @SourceTotalClaimAmountField is not null)
	OR ([SourceFeesToPostToSageNetField] = @SourceFeesToPostToSageNetField AND @SourceFeesToPostToSageNetField is not null)
	OR ([SourceFeesToPostToSageVatField] = @SourceFeesToPostToSageVatField AND @SourceFeesToPostToSageVatField is not null)
	OR ([InvoiceTypeCode] = @InvoiceTypeCode AND @InvoiceTypeCode is not null)
	OR ([CreditNoteTypeCode] = @CreditNoteTypeCode AND @CreditNoteTypeCode is not null)
	OR ([InvoiceNumberField] = @InvoiceNumberField AND @InvoiceNumberField is not null)
	OR ([DateField] = @DateField AND @DateField is not null)
	OR ([DatePostedField] = @DatePostedField AND @DatePostedField is not null)
	OR ([DetailsField] = @DetailsField AND @DetailsField is not null)
	OR ([SentToSageField] = @SentToSageField AND @SentToSageField is not null)
	OR ([NominalCodeField] = @NominalCodeField AND @NominalCodeField is not null)
	OR ([TaxCodeField] = @TaxCodeField AND @TaxCodeField is not null)
	OR ([InvoiceNetAmountField] = @InvoiceNetAmountField AND @InvoiceNetAmountField is not null)
	OR ([InvoiceTaxAmountField] = @InvoiceTaxAmountField AND @InvoiceTaxAmountField is not null)
	OR ([TypeField] = @TypeField AND @TypeField is not null)
	OR ([MatterIDField] = @MatterIDField AND @MatterIDField is not null)
	OR ([CaseNumberField] = @CaseNumberField AND @CaseNumberField is not null)
	OR ([CreditNoteNetAmountField] = @CreditNoteNetAmountField AND @CreditNoteNetAmountField is not null)
	OR ([CreditNoteTaxAmountField] = @CreditNoteTaxAmountField AND @CreditNoteTaxAmountField is not null)
	OR ([NominalCode] = @NominalCode AND @NominalCode is not null)
	OR ([TaxCode] = @TaxCode AND @TaxCode is not null)
	OR ([TableRowsDetailFieldID] = @TableRowsDetailFieldID AND @TableRowsDetailFieldID is not null)
	OR ([TableRowsDetailFieldPageID] = @TableRowsDetailFieldPageID AND @TableRowsDetailFieldPageID is not null)
	OR ([FeesInvoicedToSageField] = @FeesInvoicedToSageField AND @FeesInvoicedToSageField is not null)
	OR ([FeesPaidFromSageField] = @FeesPaidFromSageField AND @FeesPaidFromSageField is not null)
	OR ([FeesInvoicedOtherField] = @FeesInvoicedOtherField AND @FeesInvoicedOtherField is not null)
	OR ([FeesPaidOtherField] = @FeesPaidOtherField AND @FeesPaidOtherField is not null)
	OR ([CaseBalanceField] = @CaseBalanceField AND @CaseBalanceField is not null)
	OR ([SageCompanyName] = @SageCompanyName AND @SageCompanyName is not null)
	OR ([CostCentre] = @CostCentre AND @CostCentre is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Find] TO [sp_executeall]
GO
