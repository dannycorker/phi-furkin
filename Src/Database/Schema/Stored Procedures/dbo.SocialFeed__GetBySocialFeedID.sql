SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2015
-- Description:	Gets a Social Feed by Identity
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__GetBySocialFeedID]

	@SocialFeedID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM SocialFeed WITH (NOLOCK) 
	WHERE SocialFeedID=@SocialFeedID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetBySocialFeedID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__GetBySocialFeedID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetBySocialFeedID] TO [sp_executeall]
GO
