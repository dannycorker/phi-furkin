SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelFavourite table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite_GetByClientPersonnelFavouriteID]
(

	@ClientPersonnelFavouriteID int   
)
AS


				SELECT
					[ClientPersonnelFavouriteID],
					[ClientID],
					[ClientPersonnelID],
					[LeadID]
				FROM
					[dbo].[ClientPersonnelFavourite] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelFavouriteID] = @ClientPersonnelFavouriteID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_GetByClientPersonnelFavouriteID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite_GetByClientPersonnelFavouriteID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_GetByClientPersonnelFavouriteID] TO [sp_executeall]
GO
