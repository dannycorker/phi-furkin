SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-05
-- Description:	Save a single quote set for one pet, for reporting
-- GPR 2019-10-10 created for Aquarius603Dev / LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[Quote_SaveQuoteSingleCompleteSet]
(
	 @QuoteSessionID			INT = NULL
	,@TestFileItemID			INT = NULL
	,@MatterID					INT = NULL
	,@WhoCreated				INT = NULL
	,@SourceID					INT = NULL
	,@ChangeNotes				VARCHAR(2000) = NULL
	,@ProductID					INT = NULL
	,@PetName					VARCHAR(2000) = NULL
	,@Overrides					dbo.tvpIntVarcharVarchar READONLY
	,@RuleSetID					INT = NULL
	,@RulesEngineOutcome		DECIMAL(18,2) = NULL
	,@SaveCheckpoints			BIT = NULL
	,@QuoteXml					XML = NULL
	,@ResponseXml				XML = NULL
	,@ParentQuotePetProductID	INT = NULL
	,@LeadEventID				INT = NULL
	,@Excess					DECIMAL(18,2) = NULL
	,@CustomerDoB				DATE = NULL
	,@ExistingCustomerID		INT = NULL
	,@YearStart					DATE = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE  @QuotePetID		INT
			,@QuoteID			INT
			,@PetCount			INT = 1
			,@ProductCount		INT = 1
			,@QuotePetProductID	INT

	EXEC @QuoteID			= Quote_SaveQuote @QuoteSessionID, @MatterID, @PetCount, @QuoteXml, @ResponseXml, @WhoCreated, @SourceID, @ChangeNotes
	EXEC @QuotePetID		= Quote_SaveQuotePet @QuoteID, @QuoteSessionID, @MatterID, @WhoCreated, @SourceID, @Overrides, @PetName, @ProductCount, @LeadEventID, @CustomerDoB, @YearStart, @ExistingCustomerID
	EXEC @QuotePetProductID	= Quote_SaveQuotePetProduct @QuotePetID, @QuoteSessionID, @QuoteID, @WhoCreated, @SourceID, @ChangeNotes, @ProductID, @RuleSetID, @RulesEngineOutcome, @SaveCheckpoints, @ResponseXml, @ParentQuotePetProductID, @Excess
	EXEC					  Quote_CompleteQuote @QuoteID

	RETURN @QuotePetProductID
END











GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuoteSingleCompleteSet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Quote_SaveQuoteSingleCompleteSet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuoteSingleCompleteSet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuoteSingleCompleteSet] TO [sp_executehelper]
GO
