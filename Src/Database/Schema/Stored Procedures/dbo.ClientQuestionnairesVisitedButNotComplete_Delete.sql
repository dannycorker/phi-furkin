SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientQuestionnairesVisitedButNotComplete table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedButNotComplete_Delete]
(

	@ClientQuestionnairesVisitedButNotCompleteID int   
)
AS


				DELETE FROM [dbo].[ClientQuestionnairesVisitedButNotComplete] WITH (ROWLOCK) 
				WHERE
					[ClientQuestionnairesVisitedButNotCompleteID] = @ClientQuestionnairesVisitedButNotCompleteID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Delete] TO [sp_executeall]
GO
