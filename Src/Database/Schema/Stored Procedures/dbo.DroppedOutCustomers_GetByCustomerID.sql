SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DroppedOutCustomers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SELECT
					[CustomerID],
					[ClientID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[HomeTelephone],
					[MobileTelephone],
					[CompanyTelephone],
					[WorksTelephone],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[HasDownloaded],
					[DownloadedOn],
					[AquariumStatusID],
					[ClientStatusID],
					[Test],
					[CompanyName],
					[Occupation],
					[Employer],
					[ReminderEmailSentDate],
					[ReminderSmsSentDate],
					[CountryID]
				FROM
					[dbo].[DroppedOutCustomers] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_GetByCustomerID] TO [sp_executeall]
GO
