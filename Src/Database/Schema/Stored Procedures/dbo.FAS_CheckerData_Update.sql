SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FAS_CheckerData table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_CheckerData_Update]
(

	@FasCheckerDataID int   ,

	@FasUniqueReferenceNumber varchar (100)  ,

	@TitleOfAgreement varchar (512)  ,

	@IsDebtorsNameOrAddressMissing bit   ,

	@IsCreditorsNameOrAddressMissing bit   ,

	@AgreementDebtorSignatureDate datetime   ,

	@NameOfCreditor varchar (512)  ,

	@AmountOfCredit decimal (18, 0)  ,

	@TotalCashPrice decimal (18, 0)  ,

	@DurationOfAgreementInMonths int   ,

	@AnnualPercentageRate float   ,

	@RateOfInterest float   ,

	@FixedOrVariableRateOfInterest varchar (50)  ,

	@TotalInterest decimal (18, 0)  ,

	@TotalChargeFees decimal (18, 0)  ,

	@TotalChargeForCredit decimal (18, 0)  ,

	@FirstPayment decimal (18, 0)  ,

	@ConstantRegularOrSubsequentPayment decimal (18, 0)  ,

	@LastPayment decimal (18, 0)  ,

	@DeferredOrBallonPayment decimal (18, 0)  ,

	@TotalPayments decimal (18, 0)  ,

	@AdvancePayment decimal (18, 0)  ,

	@TotalAmountPayable decimal (18, 0)  ,

	@Category char (1)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FAS_CheckerData]
				SET
					[FasUniqueReferenceNumber] = @FasUniqueReferenceNumber
					,[TitleOfAgreement] = @TitleOfAgreement
					,[IsDebtorsNameOrAddressMissing] = @IsDebtorsNameOrAddressMissing
					,[IsCreditorsNameOrAddressMissing] = @IsCreditorsNameOrAddressMissing
					,[AgreementDebtorSignatureDate] = @AgreementDebtorSignatureDate
					,[NameOfCreditor] = @NameOfCreditor
					,[AmountOfCredit] = @AmountOfCredit
					,[TotalCashPrice] = @TotalCashPrice
					,[DurationOfAgreementInMonths] = @DurationOfAgreementInMonths
					,[AnnualPercentageRate] = @AnnualPercentageRate
					,[RateOfInterest] = @RateOfInterest
					,[FixedOrVariableRateOfInterest] = @FixedOrVariableRateOfInterest
					,[TotalInterest] = @TotalInterest
					,[TotalChargeFees] = @TotalChargeFees
					,[TotalChargeForCredit] = @TotalChargeForCredit
					,[FirstPayment] = @FirstPayment
					,[ConstantRegularOrSubsequentPayment] = @ConstantRegularOrSubsequentPayment
					,[LastPayment] = @LastPayment
					,[DeferredOrBallonPayment] = @DeferredOrBallonPayment
					,[TotalPayments] = @TotalPayments
					,[AdvancePayment] = @AdvancePayment
					,[TotalAmountPayable] = @TotalAmountPayable
					,[Category] = @Category
				WHERE
[FasCheckerDataID] = @FasCheckerDataID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_CheckerData_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Update] TO [sp_executeall]
GO
