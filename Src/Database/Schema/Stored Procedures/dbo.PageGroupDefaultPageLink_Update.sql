SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PageGroupDefaultPageLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroupDefaultPageLink_Update]
(

	@PageGroupDefaultPageLinkID int   ,

	@ClientID int   ,

	@PageGroupID int   ,

	@DetailFieldPageID int   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PageGroupDefaultPageLink]
				SET
					[ClientID] = @ClientID
					,[PageGroupID] = @PageGroupID
					,[DetailFieldPageID] = @DetailFieldPageID
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[PageGroupDefaultPageLinkID] = @PageGroupDefaultPageLinkID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroupDefaultPageLink_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Update] TO [sp_executeall]
GO
