SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AutoAdjudication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_Update]
(

	@AutoAdjudicationID int   ,

	@EventChoiceID int   ,

	@AutoAdjudicationTypeID int   ,

	@ObjectID int   ,

	@AutoAdjudicationFieldTypeID int   ,

	@Comparison varchar (50)  ,

	@Value1 varchar (250)  ,

	@Value2 varchar (250)  ,

	@AppliesTo varchar (50)  ,

	@Count int   ,

	@ValueTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AutoAdjudication]
				SET
					[EventChoiceID] = @EventChoiceID
					,[AutoAdjudicationTypeID] = @AutoAdjudicationTypeID
					,[ObjectID] = @ObjectID
					,[AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID
					,[Comparison] = @Comparison
					,[Value1] = @Value1
					,[Value2] = @Value2
					,[AppliesTo] = @AppliesTo
					,[Count] = @Count
					,[ValueTypeID] = @ValueTypeID
				WHERE
[AutoAdjudicationID] = @AutoAdjudicationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Update] TO [sp_executeall]
GO
