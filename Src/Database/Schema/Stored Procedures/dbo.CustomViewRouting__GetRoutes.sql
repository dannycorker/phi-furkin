SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-06
-- Description:	Gets all the defined custom view routes that the app should serve.
-- Modified:	2014-09-29	SB	Added MapsToLeadTypeID field to allow client 0 lead type views to be created and shared
-- =============================================
CREATE PROCEDURE [dbo].[CustomViewRouting__GetRoutes]

AS

SELECT CustomViewRouteID, ClientID, DetailFieldSubTypeID, LeadTypeID, PageID, FieldID, MapsToFieldID, MapsToClientID, MapsToLeadTypeID
FROM dbo.CustomViewRouting WITH (NOLOCK) 


GO
GRANT VIEW DEFINITION ON  [dbo].[CustomViewRouting__GetRoutes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomViewRouting__GetRoutes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomViewRouting__GetRoutes] TO [sp_executeall]
GO
