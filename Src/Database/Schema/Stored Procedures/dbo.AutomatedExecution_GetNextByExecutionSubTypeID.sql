SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-08-04
-- Description:	Execute SQL after specified time has passed
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedExecution_GetNextByExecutionSubTypeID] 
(
	@ExecutionSubTypeID INT,
	@ClientID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SQLToRun NVARCHAR(MAX),
			@AutomatedExecutionID INT
	
	SELECT TOP 1 @SQLToRun = SQLToRun, @AutomatedExecutionID = AutomatedExecutionID
	FROM AutomatedExecution ae WITH (NOLOCK) 
	WHERE ae.ExecutionSubTypeID = @ExecutionSubTypeID
	AND ae.Attempted = 0
	AND ae.EarliestRunDateTime < dbo.fn_GetDate_Local()
	AND (ae.ClientID = @ClientID OR @ClientID = -1)
	ORDER BY EarliestRunDateTime ASC
	
	IF @SQLToRun <> ''
	BEGIN
	
		BEGIN TRY 
		
			EXEC (@SQLToRun)
			
			UPDATE AutomatedExecution
			SET CompletionDateTime = dbo.fn_GetDate_Local(), Attempted = 1
			WHERE AutomatedExecutionID = @AutomatedExecutionID
			
		END TRY  
		BEGIN CATCH  
		
			UPDATE AutomatedExecution
			SET ErrorDateTime = dbo.fn_GetDate_Local(), Attempted = 1
			WHERE AutomatedExecutionID = @AutomatedExecutionID
		
		END CATCH  
			
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedExecution_GetNextByExecutionSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedExecution_GetNextByExecutionSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedExecution_GetNextByExecutionSubTypeID] TO [sp_executeall]
GO
