SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateFieldWithDetailValue]
(
	@ToDetailFieldID int,
	@FromDetailFieldID int,
	@CaseID int,
	@ClientPersonnelID int
)


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int 
	);

	Declare	@myERROR int,
	@LeadID int,
	@LeadOrMatterToField int,
	@LeadOrMatterFromField int,
	@DetailValue varchar(100),
	@MatterID int,
	@MaintainHistory int,
	@ClientID int

	INSERT INTO @LeadsAndMatters (LeadID, MatterID)
	SELECT DISTINCT m.LeadID, m.MatterID 
	FROM Matter m 
	WHERE m.CaseID = @CaseID


	Select @LeadOrMatterToField = LeadOrMatter, @MaintainHistory=MaintainHistory, @ClientID = ClientID
	From DetailFields
	Where DetailFieldID = @ToDetailFieldID

	Select @LeadOrMatterFromField = LeadOrMatter
	From DetailFields
	Where DetailFieldID = @FromDetailFieldID

	Select @LeadID = Cases.LeadID
	From Cases
	Where CaseID = @CaseID

	Exec _C00_CreateDetailFields @ToDetailFieldID, @LeadID

	If @LeadOrMatterFromField = 1
	Begin 

		Select @DetailValue = ldv.DetailValue
		From LeadDetailValues ldv
		Where ldv.DetailFieldID = @FromDetailFieldID
		and ldv.LeadID = @LeadID


	End

	If @LeadOrMatterFromField = 2
	Begin 

		Select @LeadID = Matter.LeadID, @MatterID = Matter.MatterID
		From Matter
		Where CaseID = @CaseID

		Select @DetailValue = mdv.DetailValue
		From MatterDetailValues mdv
		Where mdv.DetailFieldID = @FromDetailFieldID
		and mdv.MatterID = @MatterID
	End


	If @LeadOrMatterToField = 1 and @DetailValue is not NULL
	Begin 

		Update LeadDetailValues
		Set DetailValue = @DetailValue
		Where LeadDetailValues.LeadID = @LeadID
		and LeadDetailValues.DetailFieldID = @ToDetailFieldID

		If @MaintainHistory = 1
		Begin

		INSERT INTO DetailValueHistory(ClientID,DetailFieldID,LeadOrMatter,LeadID,MatterID,FieldValue,WhenSaved,ClientPersonnelID,EncryptedValue)
		Select @ClientID, @ToDetailFieldID, @LeadOrMatterToField, @LeadID, NULL, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID, NULL

		End

	END

	If @LeadOrMatterToField = 2 and @DetailValue is not NULL
	Begin 


		Select @LeadID = Matter.LeadID, @MatterID = Matter.MatterID
		From Matter
		Where CaseID = @CaseID

		Update MatterDetailValues
		Set DetailValue = @DetailValue
		Where MatterDetailValues.MatterID = @MatterID
		and MatterDetailValues.DetailFieldID = @ToDetailFieldID

		If @MaintainHistory = 1
		Begin

		INSERT INTO DetailValueHistory(ClientID,DetailFieldID,LeadOrMatter,LeadID,MatterID,FieldValue,WhenSaved,ClientPersonnelID,EncryptedValue)
		Select @ClientID, @ToDetailFieldID, @LeadOrMatterToField, @LeadID, @MatterID, @DetailValue, dbo.fn_GetDate_Local(), 529, NULL

		End
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateFieldWithDetailValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateFieldWithDetailValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateFieldWithDetailValue] TO [sp_executeall]
GO
