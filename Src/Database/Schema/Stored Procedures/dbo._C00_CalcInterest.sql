SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-04-08
-- Description:	Calc Interest
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CalcInterest]
	@LeadEventID int,
	@ChargeDate int,
	@ChargeAmount int,
	@CompoundInterest int,
	@ChargableDays int,
	@SetInterestRate int,
	@8PCTInterest int

AS
BEGIN
	SET NOCOUNT ON;

	Declare @myERROR int,
			@EventTypeID int,
			@CaseID int,
			@LeadID int,
			@LeadTypeID int,
			@ClientID int

	Select @EventTypeID = LeadEvent.EventTypeID, 
	@CaseID = LeadEvent.CaseID, 
	@LeadID = LeadEvent.LeadID,
	@LeadTypeID = EventType.LeadTypeID,
	@ClientID = LeadEvent.ClientID
	from LeadEvent 
	inner join EventType ON EventType.EventTypeID = LeadEvent.EventTypeID 
	Where LeadEventID = @LeadEventID

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int
	)

	INSERT @RequiredFields 
	SELECT @CompoundInterest 
	UNION SELECT @ChargableDays
	UNION SELECT @SetInterestRate
	UNION SELECT @8PCTInterest
	/* UNION SELECT nnnm */
	/* Keep these in numerical order to avoid chaos */

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int ,
		TableRowID int
	);

	INSERT INTO @LeadsAndMatters (LeadID, MatterID, TableRowID)
	SELECT DISTINCT tdv.LeadID, tdv.MatterID, tdv.TableRowID 
	FROM TableDetailValues tdv 
	WHERE tdv.DetailFieldID = @ChargeDate 
	AND tdv.LeadID = @LeadID

	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, EncryptedValue)
	SELECT lam.TableRowID, NULL, rf.DetailFieldID, '', lam.LeadID, lam.MatterID, @ClientID, NULL
	FROM @LeadsAndMatters lam 
	CROSS JOIN @RequiredFields rf 
	WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WHERE tdv.TableRowID = lam.TableRowID AND tdv.DetailFieldID = rf.DetailFieldID) 

	Update tdv
	Set tdv.DetailValue = case isdate(tdv2.DetailValue) when 1 then datediff(dd, convert(datetime,tdv2.DetailValue), dbo.fn_GetDate_Local()) else '-99999' end 
	From TableDetailValues tdv
	Inner Join TableDetailValues tdv2 on tdv.TableRowID = tdv2.TableRowID and tdv2.DetailFieldID = @ChargeDate
	Inner Join Matter on tdv.MatterID = Matter.MatterID
	Inner Join Cases On Cases.CaseID = Matter.CaseID
	where tdv.DetailFieldID = @ChargableDays
	and tdv.MatterID = Matter.MatterID
	and Cases.CaseID = @CaseID 

	Update tdv
	Set tdv.DetailValue = convert(decimal(18,2), (convert(money,tdv2.DetailValue)*0.08)*(convert(money,tdv3.DetailValue)/365))
	From TableDetailValues tdv
	Inner Join TableDetailValues tdv2 on tdv.TableRowID = tdv2.TableRowID and tdv2.DetailFieldID = @ChargeAmount
	Inner Join TableDetailValues tdv3 on tdv.TableRowID = tdv3.TableRowID and tdv3.DetailFieldID = @ChargableDays
	Inner Join Matter on tdv.MatterID = Matter.MatterID
	Inner Join Cases On Cases.CaseID = Matter.CaseID
	where tdv.DetailFieldID = @8PCTInterest
	and tdv.MatterID = Matter.MatterID
	and tdv2.DetailValue <> ''
	and Cases.CaseID = @CaseID 

	Update tdv
	Set tdv.DetailValue = convert(decimal(18,2), ( ( convert(money,tdv2.DetailValue) * power((1+convert(decimal(18,10),tdv3.DetailValue)/365), convert(decimal(18,10),tdv4.DetailValue)) ) ) - convert(money,tdv2.DetailValue))
	From TableDetailValues tdv
	Inner Join TableDetailValues tdv2 on tdv.TableRowID = tdv2.TableRowID and tdv2.DetailFieldID = @ChargeAmount
	Inner Join TableDetailValues tdv3 on tdv.TableRowID = tdv3.TableRowID and tdv3.DetailFieldID = @SetInterestRate
	Inner Join TableDetailValues tdv4 on tdv.TableRowID = tdv4.TableRowID and tdv4.DetailFieldID = @ChargableDays
	Inner Join Matter on tdv.MatterID = Matter.MatterID
	Inner Join Cases On Cases.CaseID = Matter.CaseID
	where tdv.DetailFieldID = @CompoundInterest
	and tdv2.DetailValue <> ''
	and tdv3.DetailValue <> ''
	and tdv.MatterID = Matter.MatterID
	and Cases.CaseID = @CaseID 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CalcInterest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CalcInterest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CalcInterest] TO [sp_executeall]
GO
