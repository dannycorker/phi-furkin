SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Dashboard table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_Get_List]

AS


				
				SELECT
					[DashboardID],
					[DashboardName],
					[DashboardDescription],
					[OwnerID],
					[DashboardLayoutID],
					[ClientID]
				FROM
					[dbo].[Dashboard] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Get_List] TO [sp_executeall]
GO
