SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadTypeLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_Insert]
(

	@LeadTypeLinkID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@OutcomeID int   
)
AS


				
				INSERT INTO [dbo].[LeadTypeLink]
					(
					[ClientID]
					,[LeadTypeID]
					,[OutcomeID]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@OutcomeID
					)
				-- Get the identity value
				SET @LeadTypeLinkID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Insert] TO [sp_executeall]
GO
