SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUser table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SELECT
					[PortalUserID],
					[ClientID],
					[CustomerID],
					[Username],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[Enabled],
					[AttemptedLogins],
					[PortalUserGroupID]
				FROM
					[dbo].[PortalUser] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByCustomerID] TO [sp_executeall]
GO
