SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-13
-- Description:	Get all EventTypeAutomatedTask records for this EventType
--              with task name included
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedTask__GetDSByEventTypeID]
(
	@EventTypeID int   
)
AS
BEGIN

	SELECT etat.EventTypeAutomatedTaskID,
	etat.EventTypeID,
	etat.AutomatedTaskID,
	etat.TimeDelayInSeconds,
	etat.AddTimeDelayIfAboutToRun,
	at.TaskName
	FROM [dbo].[EventTypeAutomatedTask] etat 
	INNER JOIN [dbo].[AutomatedTask] at ON at.TaskID = etat.AutomatedTaskID 
	WHERE etat.EventTypeID = @EventTypeID 
		
END			



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask__GetDSByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask__GetDSByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask__GetDSByEventTypeID] TO [sp_executeall]
GO
