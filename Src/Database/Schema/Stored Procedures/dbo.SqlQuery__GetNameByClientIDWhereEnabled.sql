SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery__GetNameByClientIDWhereEnabled]
(
	@ClientID int   
)
AS

				SET ANSI_NULLS OFF
				
				SELECT
					[QueryID],
					[QueryTitle]
				FROM
					[dbo].[SqlQuery]
				WHERE
					[ClientID] = @ClientID
					AND [FolderID] <> -1
				ORDER BY 
					[QueryTitle] asc

				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetNameByClientIDWhereEnabled] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__GetNameByClientIDWhereEnabled] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetNameByClientIDWhereEnabled] TO [sp_executeall]
GO
