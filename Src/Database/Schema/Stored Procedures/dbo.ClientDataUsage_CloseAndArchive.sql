SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-12-10
-- Description:	Close and live records and move them to the history tables
-- =============================================
CREATE PROCEDURE [dbo].[ClientDataUsage_CloseAndArchive]
	@BillingRunID INT = 0, 
	@ArchivedBy VARCHAR(50) = ''
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ArchivedBy = ''
	BEGIN
		SELECT @ArchivedBy = SUSER_NAME()
	END
	
	/* Can't decide whether or not to set a finish time and runtime of the run being killed off */
	/* For now, close the ID passed in, or all */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET WhenFinished = dbo.fn_GetDate_Local(), 
	TimeInSeconds = DATEDIFF(SECOND, WhenStarted, WhenFinished), 
	UserNotes = ISNULL(UserNotes, '') + CASE WHEN UserNotes > '' THEN '; ' ELSE '' END + 'Forcibly closed by ' + @ArchivedBy
	WHERE WhenFinished IS NULL 
	AND (@BillingRunID = 0 OR ClientDataUsageBillingRunID = @BillingRunID)
	
	INSERT INTO dbo.ClientDataUsageBillingRunHistory(ClientDataUsageBillingRunID, WhoArchived, WhenArchived, WhoStarted, WhenStarted, WhenFinished, TimeInSeconds, CurrentStage, UserNotes)
	SELECT ClientDataUsageBillingRunID, @ArchivedBy, dbo.fn_GetDate_Local(), WhoStarted, WhenStarted, WhenFinished, TimeInSeconds, CurrentStage, UserNotes 
	FROM dbo.ClientDataUsageBillingRun c 
	ORDER BY c.ClientDataUsageBillingRunID
	
	INSERT INTO dbo.ClientDataUsageHistory(ClientDataUsageID, ClientDataUsageBillingRunID, ClientID, DocumentBytes, ClientImagesBytes, StandardTablesBytes, CustomTablesBytes, TotalBytes, UserNotes)
	SELECT ClientDataUsageID, ClientDataUsageBillingRunID, ClientID, DocumentBytes, ClientImagesBytes, StandardTablesBytes, CustomTablesBytes, TotalBytes, UserNotes 
	FROM dbo.ClientDataUsage c 
	ORDER BY c.ClientDataUsageBillingRunID, c.ClientDataUsageID
	
	/* Clear out the live records now that the history exists */
	DELETE dbo.ClientDataUsage
	
	DELETE dbo.ClientDataUsageBillingRun
	
	DELETE dbo.ClientDataUsageWork

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDataUsage_CloseAndArchive] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientDataUsage_CloseAndArchive] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDataUsage_CloseAndArchive] TO [sp_executeall]
GO
