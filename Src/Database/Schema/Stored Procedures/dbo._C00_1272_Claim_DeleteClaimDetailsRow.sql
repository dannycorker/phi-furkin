SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-03-05
-- Used by:		DF 147723
-- Description:	Deletes a claim details row as long as it has not been paid
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_DeleteClaimDetailsRow] 
(
	@TableRowID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CaseID INT,
			@ClientID INT
	SELECT	@CaseID = m.CaseID,
			@ClientID = m.ClientID
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON r.MatterID = m.MatterID 
	WHERE r.TableRowID = @TableRowID
	
	IF dbo.fn_C00_1272_Claim_AllowedTo('edit', @CaseID) = 1
	BEGIN
	
		DELETE TableRows
		WHERE TableRowID = @TableRowID
		AND ClientID = @ClientID
	
	END
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_DeleteClaimDetailsRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_DeleteClaimDetailsRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_DeleteClaimDetailsRow] TO [sp_executeall]
GO
