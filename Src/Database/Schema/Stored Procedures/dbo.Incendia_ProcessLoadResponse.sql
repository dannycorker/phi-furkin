SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-10-28
-- Description:	Updated DFs based on the successful batch load response from Incendia
-- =============================================
CREATE PROCEDURE [dbo].[Incendia_ProcessLoadResponse] 
	@ClientID INT,
	@FileID INT
AS
BEGIN
	
	-- For every lead event card load that was included in this file we update the Date/Time Confirmed field
	-- If there were any failures, we will set them in a seperate call
	
	INSERT INTO dbo.TableDetailValues (ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue) 
	SELECT f.ClientID, le.LeadID, m.MatterID, t.TableRowID, 117538, convert(varchar, dbo.fn_GetDate_Local(), 120) /* Format 120 looks like this: "2010-08-31 12:39:24" */	
	FROM dbo.FileExportEvent f WITH (NOLOCK) 
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON f.LeadEventID = le.LeadEventID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON le.CaseID = m.CaseID 
	INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON f.ClientID = t.ClientID
	AND le.LeadID = t.LeadID AND m.MatterID = t.MatterID AND le.LeadEventID = t.ValueInt
	WHERE f.FileID = @FileID
	AND f.ClientID = @ClientID
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessLoadResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Incendia_ProcessLoadResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessLoadResponse] TO [sp_executeall]
GO
