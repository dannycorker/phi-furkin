SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType_Insert]
(

	@DocumentTypeID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@DocumentTypeName varchar (50)  ,

	@DocumentTypeDescription varchar (250)  ,

	@Header varchar (MAX)  ,

	@Template varchar (MAX)  ,

	@Footer varchar (MAX)  ,

	@CanBeAutoSent bit   ,

	@EmailSubject varchar (255)  ,

	@EmailBodyText varchar (MAX)  ,

	@InputFormat varchar (24)  ,

	@OutputFormat varchar (24)  ,

	@Enabled bit   ,

	@RecipientsTo varchar (MAX)  ,

	@RecipientsCC varchar (MAX)  ,

	@RecipientsBCC varchar (MAX)  ,

	@ReadOnlyTo bit   ,

	@ReadOnlyCC bit   ,

	@ReadOnlyBCC bit   ,

	@SendToMultipleRecipients bit   ,

	@MultipleRecipientDataSourceType int   ,

	@MultipleRecipientDataSourceID int   ,

	@SendToAllByDefault bit   ,

	@ExcelTemplatePath varchar (1024)  ,

	@FromDetails varchar (500)  ,

	@ReadOnlyFrom bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@FolderID int   ,

	@IsThunderheadTemplate bit   ,

	@ThunderheadUniqueTemplateID varchar (200)  ,

	@ThunderheadDocumentFormat varchar (10)  ,

	@DocumentTitleTemplate varchar (MAX)  ,

	@CreateNewVersionWhenSaved bit   
)
AS


				
				INSERT INTO [dbo].[DocumentType]
					(
					[ClientID]
					,[LeadTypeID]
					,[DocumentTypeName]
					,[DocumentTypeDescription]
					,[Header]
					,[Template]
					,[Footer]
					,[CanBeAutoSent]
					,[EmailSubject]
					,[EmailBodyText]
					,[InputFormat]
					,[OutputFormat]
					,[Enabled]
					,[RecipientsTo]
					,[RecipientsCC]
					,[RecipientsBCC]
					,[ReadOnlyTo]
					,[ReadOnlyCC]
					,[ReadOnlyBCC]
					,[SendToMultipleRecipients]
					,[MultipleRecipientDataSourceType]
					,[MultipleRecipientDataSourceID]
					,[SendToAllByDefault]
					,[ExcelTemplatePath]
					,[FromDetails]
					,[ReadOnlyFrom]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[FolderID]
					,[IsThunderheadTemplate]
					,[ThunderheadUniqueTemplateID]
					,[ThunderheadDocumentFormat]
					,[DocumentTitleTemplate]
					,[CreateNewVersionWhenSaved]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@DocumentTypeName
					,@DocumentTypeDescription
					,@Header
					,@Template
					,@Footer
					,@CanBeAutoSent
					,@EmailSubject
					,@EmailBodyText
					,@InputFormat
					,@OutputFormat
					,@Enabled
					,@RecipientsTo
					,@RecipientsCC
					,@RecipientsBCC
					,@ReadOnlyTo
					,@ReadOnlyCC
					,@ReadOnlyBCC
					,@SendToMultipleRecipients
					,@MultipleRecipientDataSourceType
					,@MultipleRecipientDataSourceID
					,@SendToAllByDefault
					,@ExcelTemplatePath
					,@FromDetails
					,@ReadOnlyFrom
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@FolderID
					,@IsThunderheadTemplate
					,@ThunderheadUniqueTemplateID
					,@ThunderheadDocumentFormat
					,@DocumentTitleTemplate
					,@CreateNewVersionWhenSaved
					)
				-- Get the identity value
				SET @DocumentTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_Insert] TO [sp_executeall]
GO
