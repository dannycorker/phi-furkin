SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-04-2013
-- Description:	Gets all the SMSQuestionOrder records
--				These records describe how the questions are chosen from the 
--				SMSQuestionGroup(s).
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestionOrder_GetAll]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM SMSQuestionOrder WITH (NOLOCK) 

END



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionOrder_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionOrder_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionOrder_GetAll] TO [sp_executeall]
GO
