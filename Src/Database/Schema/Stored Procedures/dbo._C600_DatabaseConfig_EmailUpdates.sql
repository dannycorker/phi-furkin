SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-08-29
-- Description:	Update batch jobs and email out events to map with the appropriate database code
-- 2021-01-18 CPS for TRUP | Removed document options because it looks like somebody has replaced this with DetailFieldID 313946 ("Email Prefix")
-- =============================================
CREATE PROCEDURE [dbo].[_C600_DatabaseConfig_EmailUpdates]
(
	@Debug BIT = 1
)

AS
BEGIN

	DECLARE @ConfigDataItem VARCHAR(2000) = 'EmailPrefix'
	DECLARE @ThisDbPrefix	VARCHAR(2000) = ISNULL(dbo.fn_C600_GetDatabaseSpecificConfigValue(@ConfigDataItem),'')
	DECLARE @EscapedPrefix	VARCHAR(2000) = replace(replace(replace(@ThisDbPrefix,'[','\['),']','\]'),'-','\-')

	DECLARE @Prefixes TABLE ( TableRowID INT, Prefix VARCHAR(2000), EscapedPrefix VARCHAR(2000) )
	INSERT @Prefixes ( TableRowID, Prefix, EscapedPrefix )
	SELECT tr.TableRowID, tdvPrefix.DetailValue, replace(replace(replace(tdvPrefix.DetailValue,'[','\['),']','\]'),'-','\-')
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvLabel WITH ( NOLOCK ) on tdvLabel.TableRowID = tr.TableRowID AND tdvLabel.DetailFieldID = 178653 /*Config Data Name*/
	INNER JOIN TableDetailValues tdvPrefix WITH ( NOLOCK ) on tdvPrefix.TableRowID = tr.TableRowID AND tdvPrefix.DetailFieldID IN ( 178654,178655,178656,178657 )
	WHERE tr.DetailFieldID = 178658 /*Database Specific Config*/
	AND tdvLabel.DetailValue = @ConfigDataItem
	AND tdvPrefix.DetailValue <> ''
		UNION
	SELECT 0, '[PPET-DEV]','\[PPET\-DEV\]'

	IF @Debug = 1
	BEGIN
		SELECT @ThisDbPrefix [@ThisDbPrefix], @EscapedPrefix [@EscapedPrefix]

		SELECT * 
		FROM @Prefixes pr 

		SELECT 'Update Prefix - AutomatedTask' [Action], at.TaskID, at.Taskname, atp.ParamValue, REPLACE(atp.ParamValue,pr.Prefix,@ThisDbPrefix) [New]
		FROM AutomatedTaskParam atp WITH ( NOLOCK ) 
		INNER JOIN AutomatedTask at WITH ( NOLOCK ) on at.TaskID = atp.TaskID
		INNER JOIN @Prefixes pr on atp.ParamValue like '%' + pr.EscapedPrefix + '%' escape '\'
		WHERE atp.ParamName = 'Message_Header'
		AND pr.Prefix <> @ThisDbPrefix
		--AND @ThisDbPrefix <> ''
			UNION
		SELECT 'Add Prefix - AutomatedTask' [Action], at.TaskID, at.Taskname, atp.ParamValue, @ThisDbPrefix + ' ' + atp.ParamValue [New]
		FROM AutomatedTaskParam atp WITH ( NOLOCK )
		INNER JOIN AutomatedTask at WITH ( NOLOCK ) on at.TaskID = atp.TaskID
		WHERE atp.ParamName = 'Message_Header'
		AND atp.ParamValue <> ''
		AND atp.ParamValue not like '%' + @EscapedPrefix + '%' escape '\'
		--AND @ThisDbPrefix <> ''

		--SELECT 'Update Prefix - DocumentType' [Action], dt.DocumentTypeID, dt.DocumentTypeName, dt.EmailSubject, REPLACE(dt.EmailSubject,pr.Prefix,@ThisDbPrefix) [New]
		--FROM DocumentType dt WITH ( NOLOCK ) 
		--INNER JOIN @Prefixes pr on dt.EmailSubject like '%' + pr.EscapedPrefix + '%' escape '\'
		--WHERE dt.EmailSubject <> ''
		--AND pr.Prefix <> @ThisDbPrefix
		----AND @ThisDbPrefix <> ''
		--	UNION
		--SELECT 'Add Prefix - DocumentType' [Action], dt.DocumentTypeID, dt.DocumentTypeName, dt.EmailSubject, @ThisDbPrefix + ' ' + dt.EmailSubject
		--FROM DocumentType dt WITH ( NOLOCK ) 
		--WHERE dt.EmailSubject <> ''
		--AND dt.EmailSubject not like '%' + @EscapedPrefix + '%' escape '\'
		----AND @ThisDbPrefix <> ''

		--SELECT 'Update Prefix - DocumentTypeVersion' [Action], dt.DocumentTypeID, dt.DocumentTypeName, dt.EmailSubject, REPLACE(dt.EmailSubject,pr.Prefix,@ThisDbPrefix) [New]
		--FROM DocumentType dt WITH ( NOLOCK ) 
		--INNER JOIN @Prefixes pr on dt.EmailSubject like '%' + pr.EscapedPrefix + '%' escape '\'
		--WHERE dt.EmailSubject <> ''
		--AND pr.Prefix <> @ThisDbPrefix
		----AND @ThisDbPrefix <> ''
		--	UNION
		--SELECT 'Add Prefix - DocumentTypeVersion' [Action], dt.DocumentTypeID, dt.DocumentTypeName, dt.EmailSubject, @ThisDbPrefix + ' ' + dt.EmailSubject
		--FROM DocumentType dt WITH ( NOLOCK ) 
		--WHERE dt.EmailSubject <> ''
		--AND dt.EmailSubject not like '%' + @EscapedPrefix + '%' escape '\'
		----AND @ThisDbPrefix <> ''	
	END
	ELSE
	BEGIN
		UPDATE atp 
		SET ParamValue = REPLACE(atp.ParamValue,pr.Prefix,@ThisDbPrefix)
		FROM AutomatedTaskParam atp WITH ( NOLOCK ) 
		INNER JOIN AutomatedTask at WITH ( NOLOCK ) on at.TaskID = atp.TaskID
		INNER JOIN @Prefixes pr on atp.ParamValue like '%' + pr.EscapedPrefix + '%' escape '\'
		WHERE atp.ParamName = 'Message_Header'
		AND pr.Prefix <> @ThisDbPrefix
		--AND @ThisDbPrefix <> ''
		
		UPDATE atp 
		SET ParamValue = @ThisDbPrefix + ' ' + atp.ParamValue
		FROM AutomatedTaskParam atp WITH ( NOLOCK )
		INNER JOIN AutomatedTask at WITH ( NOLOCK ) on at.TaskID = atp.TaskID
		WHERE atp.ParamName = 'Message_Header'
		AND atp.ParamValue <> ''
		AND atp.ParamValue not like '%' + @EscapedPrefix + '%' escape '\'
		--AND @ThisDbPrefix <> ''

		--UPDATE dt
		--SET EmailSubject = REPLACE(dt.EmailSubject,pr.Prefix,@ThisDbPrefix)
		--FROM DocumentType dt WITH ( NOLOCK ) 
		--INNER JOIN @Prefixes pr on dt.EmailSubject like '%' + pr.EscapedPrefix + '%' escape '\'
		--WHERE dt.EmailSubject <> ''
		--AND pr.Prefix <> @ThisDbPrefix
		----AND @ThisDbPrefix <> ''
		
		--UPDATE dt
		--SET EmailSubject = @ThisDbPrefix + ' ' + dt.EmailSubject
		--FROM DocumentType dt WITH ( NOLOCK ) 
		--WHERE dt.EmailSubject <> ''
		--AND dt.EmailSubject not like '%' + @EscapedPrefix + '%' escape '\'
		----AND @ThisDbPrefix <> ''

		--UPDATE dt
		--SET EmailSubject = REPLACE(dt.EmailSubject,pr.Prefix,@ThisDbPrefix)
		--FROM DocumentTypeVersion dt WITH ( NOLOCK ) 
		--INNER JOIN @Prefixes pr on dt.EmailSubject like '%' + pr.EscapedPrefix + '%' escape '\'
		--WHERE dt.EmailSubject <> ''
		--AND pr.Prefix <> @ThisDbPrefix
		----AND @ThisDbPrefix <> ''
		
		--UPDATE dt
		--SET EmailSubject = @ThisDbPrefix + ' ' + dt.EmailSubject
		--FROM DocumentTypeVersion dt WITH ( NOLOCK ) 
		--WHERE dt.EmailSubject <> ''
		--AND dt.EmailSubject not like '%' + @EscapedPrefix + '%' escape '\'
		----AND @ThisDbPrefix <> ''
	END
	
END









GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DatabaseConfig_EmailUpdates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_DatabaseConfig_EmailUpdates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DatabaseConfig_EmailUpdates] TO [sp_executeall]
GO
