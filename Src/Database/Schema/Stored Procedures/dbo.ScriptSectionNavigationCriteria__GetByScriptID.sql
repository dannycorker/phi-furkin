SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 29/06/2015
-- Description:	Gets the script section navigation criteria
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria__GetByScriptID]
	@ClientID INT,
	@ScriptID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT	df.QuestionTypeID, df.FieldName DetailFieldName,
			fss.SectionOrder FromStepNumber, fss.SectionTitle FromStepSectionName,
			tss.SectionOrder ToStepNumber, tss.SectionTitle ToStepSectionName,
			nc.* 
	FROM ScriptSectionNavigationCriteria nc WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = nc.DetailFieldID
	INNER JOIN ScriptSection fss WITH (NOLOCK) ON fss.ScriptSectionID = nc.FromStepSectionID AND fss.ScriptID=@ScriptID
	INNER JOIN ScriptSection tss WITH (NOLOCK) ON tss.ScriptSectionID = nc.ToStepSectionID AND fss.ScriptID=@ScriptID
	WHERE nc.ClientID=@ClientID 
	ORDER BY FromStepSectionID, ToStepSectionID ASC
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__GetByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria__GetByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__GetByScriptID] TO [sp_executeall]
GO
