SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventChoiceAutoAdjudicationFields view
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoiceAutoAdjudicationFields_Get_List]

AS


				
				SELECT
					[COLUMN_NAME],
					[DB_NAME],
					[DATA_TYPE]
				FROM
					[dbo].[EventChoiceAutoAdjudicationFields]
					
				Select @@ROWCOUNT			
			





GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoiceAutoAdjudicationFields_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoiceAutoAdjudicationFields_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoiceAutoAdjudicationFields_Get_List] TO [sp_executeall]
GO
