SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Chris Townsend
-- Create date: 2008-11-03
-- Description:	Gets a resource list detail value for a specific
--				detail field by resource list ID
-- =============================================


CREATE PROCEDURE [dbo].[ResourceListDetailValues__GetByResourceListIDandDetailFieldID]
(
	@ResourceListID int,
	@DetailFieldID int   
)
AS


				SELECT
					[ResourceListDetailValueID],
					[ResourceListID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime]					
				FROM
					[dbo].[ResourceListDetailValues] WITH (NOLOCK)
				WHERE
					[ResourceListID] = @ResourceListID
					AND					
					[DetailFieldID] = @DetailFieldID
			Select @@ROWCOUNT
					
			






GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues__GetByResourceListIDandDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues__GetByResourceListIDandDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues__GetByResourceListIDandDetailFieldID] TO [sp_executeall]
GO
