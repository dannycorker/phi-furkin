SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalUserOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_Insert]
(

	@PortalUserOptionID int    OUTPUT,

	@ClientID int   ,

	@PortalUserID int   ,

	@PortalUserOptionTypeID int   ,

	@OptionValue varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[PortalUserOption]
					(
					[ClientID]
					,[PortalUserID]
					,[PortalUserOptionTypeID]
					,[OptionValue]
					)
				VALUES
					(
					@ClientID
					,@PortalUserID
					,@PortalUserOptionTypeID
					,@OptionValue
					)
				-- Get the identity value
				SET @PortalUserOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Insert] TO [sp_executeall]
GO
