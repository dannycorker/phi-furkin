SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Titles table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Titles_Delete]
(

	@TitleID int   
)
AS


				DELETE FROM [dbo].[Titles] WITH (ROWLOCK) 
				WHERE
					[TitleID] = @TitleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Titles_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Delete] TO [sp_executeall]
GO
