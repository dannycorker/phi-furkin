SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeOption_Delete]
(

	@EventTypeOptionID int   
)
AS


				DELETE FROM [dbo].[EventTypeOption] WITH (ROWLOCK) 
				WHERE
					[EventTypeOptionID] = @EventTypeOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Delete] TO [sp_executeall]
GO
