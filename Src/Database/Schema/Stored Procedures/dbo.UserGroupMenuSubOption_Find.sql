SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the UserGroupMenuSubOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuSubOption_Find]
(

	@SearchUsingOR bit   = null ,

	@UserGroupMenuSubOptionID int   = null ,

	@UserGroupMenuOptionID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@PanelItemName varchar (50)  = null ,

	@PanelItemCaption varchar (50)  = null ,

	@PanelItemURL varchar (250)  = null ,

	@ShowInFancyBox bit   = null ,

	@SubItemOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [UserGroupMenuSubOptionID]
	, [UserGroupMenuOptionID]
	, [ClientID]
	, [ClientPersonnelAdminGroupID]
	, [PanelItemName]
	, [PanelItemCaption]
	, [PanelItemURL]
	, [ShowInFancyBox]
	, [SubItemOrder]
    FROM
	[dbo].[UserGroupMenuSubOption] WITH (NOLOCK) 
    WHERE 
	 ([UserGroupMenuSubOptionID] = @UserGroupMenuSubOptionID OR @UserGroupMenuSubOptionID IS NULL)
	AND ([UserGroupMenuOptionID] = @UserGroupMenuOptionID OR @UserGroupMenuOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([PanelItemName] = @PanelItemName OR @PanelItemName IS NULL)
	AND ([PanelItemCaption] = @PanelItemCaption OR @PanelItemCaption IS NULL)
	AND ([PanelItemURL] = @PanelItemURL OR @PanelItemURL IS NULL)
	AND ([ShowInFancyBox] = @ShowInFancyBox OR @ShowInFancyBox IS NULL)
	AND ([SubItemOrder] = @SubItemOrder OR @SubItemOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [UserGroupMenuSubOptionID]
	, [UserGroupMenuOptionID]
	, [ClientID]
	, [ClientPersonnelAdminGroupID]
	, [PanelItemName]
	, [PanelItemCaption]
	, [PanelItemURL]
	, [ShowInFancyBox]
	, [SubItemOrder]
    FROM
	[dbo].[UserGroupMenuSubOption] WITH (NOLOCK) 
    WHERE 
	 ([UserGroupMenuSubOptionID] = @UserGroupMenuSubOptionID AND @UserGroupMenuSubOptionID is not null)
	OR ([UserGroupMenuOptionID] = @UserGroupMenuOptionID AND @UserGroupMenuOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([PanelItemName] = @PanelItemName AND @PanelItemName is not null)
	OR ([PanelItemCaption] = @PanelItemCaption AND @PanelItemCaption is not null)
	OR ([PanelItemURL] = @PanelItemURL AND @PanelItemURL is not null)
	OR ([ShowInFancyBox] = @ShowInFancyBox AND @ShowInFancyBox is not null)
	OR ([SubItemOrder] = @SubItemOrder AND @SubItemOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Find] TO [sp_executeall]
GO
