SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AquariumEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventType_Delete]
(

	@AquariumEventTypeID int   
)
AS


				DELETE FROM [dbo].[AquariumEventType] WITH (ROWLOCK) 
				WHERE
					[AquariumEventTypeID] = @AquariumEventTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Delete] TO [sp_executeall]
GO
