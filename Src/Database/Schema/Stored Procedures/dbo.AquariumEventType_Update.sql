SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AquariumEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventType_Update]
(

	@AquariumEventTypeID int   ,

	@OriginalAquariumEventTypeID int   ,

	@AquariumEventTypeName varchar (50)  ,

	@AquariumEventTypeDescription varchar (250)  ,

	@AquariumStatusAfterEvent int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AquariumEventType]
				SET
					[AquariumEventTypeID] = @AquariumEventTypeID
					,[AquariumEventTypeName] = @AquariumEventTypeName
					,[AquariumEventTypeDescription] = @AquariumEventTypeDescription
					,[AquariumStatusAfterEvent] = @AquariumStatusAfterEvent
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[AquariumEventTypeID] = @OriginalAquariumEventTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Update] TO [sp_executeall]
GO
