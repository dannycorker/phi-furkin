SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-31
-- Description:	Delete all Rights for redundant objects
-- eg an Encrypted field that is no longer encrypted
-- =============================================
Create PROCEDURE [dbo].[DeleteSecurityForDeletedObject]
	@FunctionTypeID int,
	@LeadTypeID int,
	@ObjectID int,
	@Level int,
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRAN

	-- If an Outcome has just been deleted, then look for any Groups and Users with 
	-- rights to this Outcome, and remove them.
	IF @FunctionTypeID = 9
	BEGIN

		-- Groups
		DELETE GroupRightsDynamic 
		WHERE FunctionTypeID = @FunctionTypeID 
		AND LeadTypeID IS NULL
		AND @LeadTypeID = -1
		AND ObjectID = @ObjectID
		
		-- Users with different rights to the rest of the group
		DELETE UserRightsDynamic 
		WHERE FunctionTypeID = @FunctionTypeID 
		AND LeadTypeID IS NULL
		AND @LeadTypeID = -1
		AND ObjectID = @ObjectID

	END

	-- If a new Page Type (12) or Event Type (13) or Encrypted Field (25) has just been 
	-- removed then delete any rights to it.
	IF @FunctionTypeID = 12 OR @FunctionTypeID = 13 OR @FunctionTypeID = 25
	BEGIN

		-- Groups
		DELETE GroupRightsDynamic 
		WHERE FunctionTypeID = @FunctionTypeID 
		AND LeadTypeID = @LeadTypeID
		AND ObjectID = @ObjectID
		
		-- Users with different rights to the rest of the group
		DELETE UserRightsDynamic 
		WHERE FunctionTypeID = @FunctionTypeID 
		AND LeadTypeID = @LeadTypeID
		AND ObjectID = @ObjectID

	END

	COMMIT
END













GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteSecurityForDeletedObject] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteSecurityForDeletedObject] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteSecurityForDeletedObject] TO [sp_executeall]
GO
