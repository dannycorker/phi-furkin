SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeEquation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeEquation_Insert]
(

	@EventTypeEquationID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@DetailFieldID int   
)
AS


				
				INSERT INTO [dbo].[EventTypeEquation]
					(
					[ClientID]
					,[EventTypeID]
					,[DetailFieldID]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@DetailFieldID
					)
				-- Get the identity value
				SET @EventTypeEquationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Insert] TO [sp_executeall]
GO
