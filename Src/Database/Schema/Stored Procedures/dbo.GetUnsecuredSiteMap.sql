SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetUnsecuredSiteMap    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetUnsecuredSiteMap] AS


SELECT     Secured, ParentID, Description, Title, Url, SiteMapID
FROM         dbo.SiteMap
WHERE     (Secured = 0) OR
                      (Secured IS NULL)




GO
GRANT VIEW DEFINITION ON  [dbo].[GetUnsecuredSiteMap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUnsecuredSiteMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUnsecuredSiteMap] TO [sp_executeall]
GO
