SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-12-05
-- Description:	Saves the policy details and financials on a matter
-- Mods
-- DCM 2014-12-15 Added customer match field.
-- DCM 2014-12-19 Added @PremiumCalcType
-- JW  2014-12-22 Switched around the values (69943, 69944)
--                for payment intervals of 1 and 12.
-- DCM 2015-01-06 Switched @PremiumCalcType values to account for JW's 2014-12-22 change
-- DCM 2015-01-06 Calculate Date first payment from payment day & getdate
-- DCM 2015-01-26 Pet Number
-- SB  2015-05-19 Fix to use the start date passed in the XML
-- SB  2015-06-15 Check the client personnelID and update to automation user if not valid
-- DCM 2015-09-23 Set 170038 by _C00_SimpleValueIntoField so that DetailHistory is also set
-- DCM 2015-10-12 Changed multipet logic
-- DCM 2016-06-23 Added terms date matter detail value
-- DCM 2016-08-09 Add extra matter fields for PP
-- SB  2016-11-10 Added save for tracking code and link back to campaign code
-- dcm 2016-12-19 Added policy sale date
-- CPS 2017-05-29 added XML log
-- CPS 2017-05-29 Updated Signup Method
-- CPS 2017-06-16 Changed EndDate calculation to subtract one day for ticket #43798
-- MAB 2018-02-27 Calculate end date for One Month Free policy
-- MAB 2018-02-28 Set sign up method for One Month Free (OMF) 
-- MAB 2018-03-03 Re-factored so that OMF does not try to capture any payment info as it now does not create a Collections lead
-- MAB 2018-03-09 Added set match type for breeder portal
-- GPR 2018-07-20 Added @SourceResourceListID DF:180112
-- JEL 2018-09-14 Swapped Tracking Code logic and Campaign code logic to use the correct notes in the buy XML post APP change
-- GPR 2018-09-24 Altered Tracking Code logic for C600 Defect #1012
-- GPR 2018-10-16 Add QuoteSessionID to PolicyAdminMatter
-- GPR 2018-11-08 Added correct ClientPersonnelIDs for Aggregator and updated LuLi IDs for assignment of @SignUpMethod
-- GPR 2019-03-28 Added block to set SourceResourceListID = OMF where Policy product is OneMonthFree
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- JEL 2020-07-20 JEL Added logic to flip the OneInc PaymentMethod to either CC or DD 
-- ALM 2020-08-05 Added @Excess and @CoPay PPET-7
-- IES 20200820 [SDPRU-9] excess+copay or defaults
-- ALM 2020-08-20 Wrap @Exccess and @Copay changes in CountryID to prevent errors creating policies on AAG
-- GPR 2020-10-15 Added more UW to trigger discounts and surcharges for 605 (SDPRU-77)
-- ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee (Shred xml and xave to matter fields)
-- ACE 2021-01-28 Added history to matter field direct insert
-- NG  2021-04-27 Updated to use 607 proc for variable attachments
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_AddPolicyDetails]
(
	@LeadID INT,
	@TotalPets INT, 
	@Pet XML,
	@XmlRequest XML,
	@Debug BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID(),
			@ClientPersonnelId INT,
			@LogEntry VARCHAR(MAX),
			@CountryID INT,
			@Now DATETIME = dbo.fn_GetDate_Local(),
			@PetIndex INT

	DECLARE @Policies TABLE (LeadID INT, PolicyNumber INT)

	SELECT	@ClientID = @XmlRequest.value('(//ClientId)[1]', 'INT'),
			@ClientPersonnelId = @XmlRequest.value('(//ClientPersonnelId)[1]', 'INT')

	
	DECLARE @DetailValueHistory TABLE (LeadOrMatter INT, CustomerID INT, LeadID INT, CaseID INT, MatterID INT, DetailFieldID INT, DetailValue VARCHAR(2000))

	EXEC _C00_LogItXml @ClientID, @LeadID, '_C00_1273_Policy_AddPolicyDetails', @XmlRequest /*CPS 2017-05-29*/
	
	/* 2018-03-03 Mark Beaumont - determine if this is a One Month Free (OMF) policy */
	DECLARE	@IsOneMonthFreePolicy BIT
	SELECT	@IsOneMonthFreePolicy = CASE WHEN @XmlRequest.value('(BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValue/ProductId)[1]', 'INT') = 151577 THEN 1 ELSE 0 END

	IF NOT EXISTS
	(
		SELECT *
		FROM dbo.ClientPersonnel WITH (NOLOCK)
		WHERE ClientPersonnelID = @ClientPersonnelId
		AND ClientID = @ClientID
	)
	BEGIN
		SELECT @ClientPersonnelId = dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@ClientID,53,'CFG|AqAutomationCPID',0)
	END
	
	DECLARE @StartDate DATE
	SELECT @StartDate = ISNULL(@Pet.value('(//StartDate)[1]', 'DATETIME'), @Now + 1)

	IF @StartDate < CAST(@Now as DATE) 
	BEGIN 

		SELECT @StartDate = CAST(@Now as DATE) 

	END
	
	DECLARE @AgeInYears INT,
			@DeductibleGroupID INT = 0

	-- Get BrandingID from XML
	DECLARE @BrandingID INT
	SELECT @BrandingID = @XmlRequest.value('(//BrandingId)[1]', 'INT')
	
	DECLARE @MatterID INT
	IF @Debug = 0
	BEGIN
		DECLARE @CaseID INT
		
		DECLARE @CaseNum INT = 0

		SELECT TOP 1 @CaseNum = CaseNum
		FROM dbo.Cases WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		
		SELECT @CaseNum += 1
		
		INSERT INTO dbo.Cases (LeadID, ClientID, CaseNum, CaseRef, WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@LeadID, @ClientID, @CaseNum, 'Case ' + CAST(@CaseNum AS VARCHAR), @ClientPersonnelId, dbo.fn_GetDate_Local(), @ClientPersonnelId, dbo.fn_GetDate_Local())
		
		SELECT @CaseID = SCOPE_IDENTITY()
		
		DECLARE @PolicyAdminLeadTypeID INT = 1492
				
		-- Look up from the shares
		SELECT @PolicyAdminLeadTypeID = s.SharedTo
		FROM dbo.LeadTypeShare s WITH (NOLOCK) 
		WHERE s.ClientID = @ClientID
		AND s.SharedFrom = 1273		
			
		DECLARE @RefLetter VARCHAR(3),
				@CustomerID INT
				
		SELECT @RefLetter = dbo.fnRefLetterFromCaseNum(1 + COUNT(*)) 
		FROM Matter WITH (NOLOCK)
		WHERE LeadID = @LeadID
		
		SELECT @CustomerID = CustomerID
		FROM dbo.Lead WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		
		INSERT INTO dbo.Matter(ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID)
		VALUES (@ClientID, '', @CustomerID, @LeadID, 1, @RefLetter, 0, @CaseID)
		
		SELECT @MatterID = SCOPE_IDENTITY()

		SELECT @CountryID = cu.CountryID 
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID 
		
	END

	-- Get payment details from the XML
	DECLARE @PaymentDay VARCHAR(2000),
			@PaymentMethodID VARCHAR(2000),
			@PaymentIntervalID VARCHAR(2000),
			@SortCode VARCHAR(2000),
			@AccountNumber VARCHAR(2000),
			@BankName VARCHAR(2000),
			@BankAccountName VARCHAR(2000),
			@CollectionsMatterID INT,
			@CreditCardToken VARCHAR(2000),
			@CreditCardName VARCHAR(2000),
			@CreditCardMasked VARCHAR(2000),
			@CreditCardExpire DATE,
			@FirstPaymentDate DATE,
			@ExistingCust BIT,
			@CustomerMatch VARCHAR(2000),
			@PetNumber INT,
			@QuoteSessionID INT,
			@TrackingCode VARCHAR(2000),
			@CampaignCode VARCHAR(2000),
			@ExistingCustomerID	INT,
			@SourceResourceListID INT,
			@Channel INT,
			@OneIncPaymentMethod VARCHAR(2000),
			@WellnessProductRLID INT,
			@IsInternetPartner			BIT = 0,
			@IsStrategicPartner			BIT = 0,
			@IsCorporateGroupBenefit	BIT = 0,
			@IsBroker					BIT = 0,
			@FeeTableRowID				INT,
			/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee (Shred xml and xave to matter fields)*/
			@EnrollmentFee				NUMERIC(18,2),
			@TaxTableRowID				INT,
			@EnrollmentFeeTax			NUMERIC(18,2),
			@EnrollmentFeeTotal			NUMERIC(18,2)
			
	SELECT	@PaymentDay			= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/PaymentDay)[1]', 'VARCHAR(2000)'),
			@PaymentMethodID	= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/PaymentMethodTypeId)[1]', 'VARCHAR(2000)'),
			@PaymentIntervalID	= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/PaymentIntervalId)[1]', 'VARCHAR(2000)'),
			@SortCode			= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/SortCode)[1]', 'varchar(2000)'),
			@AccountNumber		= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/AccountNumber)[1]', 'varchar(2000)'),
			@BankName			= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/BankName)[1]', 'varchar(2000)'),
			@BankAccountName	= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/BankAccountName)[1]', 'varchar(2000)'),
			@CreditCardToken	= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/CreditCardToken)[1]', 'varchar(2000)'),
			@CreditCardName		= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/CreditCardHolderName)[1]', 'varchar(2000)'),
			@CreditCardMasked	= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/CreditCardNumberMasked)[1]', 'varchar(2000)'),
			@CreditCardExpire	= @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/CreditCardExpireDate)[1]', 'date'),
			--@FirstPaymentDate	= dbo.fn_GetDate_Local() + 1,
			@ExistingCust		= @XmlRequest.value('(//ExistingPolicyHolder)[1]', 'BIT'),
			@PetNumber			= @XmlRequest.value('(//OtherPetsInsured)[1]', 'INT'),
			@QuoteSessionID		= @XmlRequest.value('(//SessionId)[1]', 'INT'),
			@TrackingCode		= @XmlRequest.value('(//TrackingCode)[1]', 'VARCHAR(2000)'),
			@ExistingCustomerID	= @XmlRequest.value('(//CustomerId)[1]', 'VARCHAR(2000)'),
			@CampaignCode       = @XmlRequest.value('(//DiscountCodes/string)[1]', 'VARCHAR(2000)'),
			@OneIncPaymentMethod = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/OneIncPaymentMethod)[1]', 'varchar(2000)'),
			/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee (Shred xml and save to matter fields)*/
			@FeeTableRowID		= @XmlRequest.value('(//PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/FeeTableRowID)[1]', 'INT'),
			@EnrollmentFee		= @XmlRequest.value('(//PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Fee)[1]', 'NUMERIC(18,2)'),
			@TaxTableRowID		= @XmlRequest.value('(//PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/TaxTableRowID)[1]', 'INT'),
			@EnrollmentFeeTax	= @XmlRequest.value('(//PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Tax)[1]', 'NUMERIC(18,2)'),
			@EnrollmentFeeTotal	= @XmlRequest.value('(//PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Total)[1]', 'NUMERIC(18,2)')

			
			/*GPR 2021-03-08 for FURKIN-364*/
			IF @EnrollmentFee IS NULL
			BEGIN

					DECLARE @StateCode VARCHAR(2),
							@ProvinceLookupListItemID INT

					SELECT @StateCode = canada.StateCode
					FROM Customers c WITH (NOLOCK)
					JOIN UnitedStates canada WITH (NOLOCK) ON canada.StateName = c.County
					WHERE c.CustomerID = @CustomerID

					SELECT @ProvinceLookupListItemID = CASE @StateCode 
									WHEN 'AB' THEN 61739
									WHEN 'BC' THEN 61740
									WHEN 'MB' THEN 61741
									WHEN 'NB' THEN 61742
									WHEN 'NL' THEN 61743
									WHEN 'NS' THEN 61744
									WHEN 'NT' THEN 61745
									WHEN 'NU' THEN 61746
									WHEN 'ON' THEN 61747
									WHEN 'PE' THEN 61748
									WHEN 'QC' THEN 61749
									WHEN 'SK' THEN 61750
									WHEN 'YT' THEN 61751
								END

					--EXEC _C00_Logit 'Info', 'EnrollmentFee', 'BrandingID', @BrandingID, 58550
					--EXEC _C00_Logit 'Info', 'EnrollmentFee', 'CustomerID', @CustomerID, 58550
					--EXEC _C00_Logit 'Info', 'EnrollmentFee', 'ProvinceLLID', @ProvinceLookupListItemID, 58550
					--EXEC _C00_Logit 'Info', 'EnrollmentFee', 'StartDate', @StartDate, 58550
					
					/*ALM 2021-03-09 FURKIN-364*/
					/*Work out pet number*/
					INSERT INTO @Policies (LeadID, PolicyNumber)
					SELECT l.LeadID, ROW_NUMBER() OVER (PARTITION BY (SELECT 1) ORDER BY l.LeadID)
					FROM Lead l WITH (NOLOCK)
					WHERE l.CustomerID = @CustomerID
					AND l.leadTypeID = 1492

					SELECT @PetIndex = p.PolicyNumber
					FROM @Policies p
					WHERE p.LeadID = @LeadID

					SELECT 
					@FeeTableRowID		= e.FeeTableRowID,
					@EnrollmentFee		= e.EnrollmentFee,
					@TaxTableRowID		= e.TaxTableRowID,
					@EnrollmentFeeTax	= e.Tax,
					@EnrollmentFeeTotal	= e.Total
					FROM dbo.fn_C600_GetEnrollmentFee(@BrandingID, @CustomerID, @ProvinceLookupListItemID, @PetIndex, @StartDate) e

			END

			--EXEC _C00_Logit 'Info', 'EnrollmentFee', 'EnrollmentFee', @EnrollmentFeeTotal, 58550


	/*JEL - OneInc has taken the Payment method node, so a new one has been added to communicate the payment method. For downstream processing pick the correct value*/ 
	IF ISNULL(@OneIncPaymentMethod,'') <> '' 
	BEGIN 

		SELECT @PaymentMethodID = CASE @OneIncPaymentMethod WHEN 'CreditCard' THEN '69931' ELSE '69930' END 

	END 
	IF @ClientID = 605
	BEGIN 
	
		SELECT @PaymentDay = DATEPART(DAY,@StartDate) 
	
	END	
	IF @PaymentMethodID = 193355
	BEGIN 
	
		SELECT @PaymentIntervalID = 4 /*GPR 2021-02-19 changed to 4 for Monthly for FURKIN-255*/ --69931 
	
	END  
	
	
	/*GPR 2018-10-22 Renewal Auto/Manual Toggle
	  JEL 2020-07-20 remove toggle, set auto by default*/ 
	EXEC _C00_SimpleValueIntoField 180067, 'false', @MatterID /*Auto Renewal*/

	/*GPR 2020-05-19 for AAG-743 | remove override as we now have Monthly/Annual DD and CC options*/
	--IF ISNULL(@PaymentIntervalID,0) = 0  
	--BEGIN 
	
	--	SELECT @PaymentIntervalID = CASE @PaymentMethodID WHEN  69930 THEN 1 WHEN 76618 /* GPR 2019-11-07 Premium Funding*/ THEN 2 ELSE 12 END 

	--	SELECT @LogEntry = 'Interval Overridden '  + @PaymentMethodID  +  ' LeadID ' + CAST(@LeadID as VARCHAR) 

	--	exec _C00_LogIt 'Info', '_C600_AddPolicyDetails', '_C600_AddPolicyDetails PaymentInterval Error', @LogEntry, @ClientPersonnelID
			
	--END
	
	/*GPR 2018-10-16 Add QuoteSessionID to PolicyAdminMatter*/
	EXEC _C00_SimpleValueIntoField 180189,@QuoteSessionID,@MatterID	
		
	-- We can now get the tracking directly from the buy request but fall back to the quote just in case
	IF @TrackingCode IS NULL
	BEGIN
	
		SELECT @TrackingCode = ISNULL(TrackingCode,'E195EB3076E582F6AF70A0FE8E241D62')
		FROM dbo._C600_QuoteSessions WITH (NOLOCK) 
		WHERE QuoteSessionID = @QuoteSessionID	
		
		SELECT @SourceResourceListID = dbo.fn_C600_GetSourceFromTrackingCode(@TrackingCode,2 /*ResourceListID*/) /*GPR 2018-09-26*/		
		
	END
	ELSE
	BEGIN
		
		SELECT @SourceResourceListID = dbo.fn_C600_GetSourceFromTrackingCode(@TrackingCode,2 /*ResourceListID*/) /*GPR 2018-07-27*/		
		
	END
		
	-- Map to the campaign code RL
	IF @CampaignCode IS NOT NULL
	BEGIN
		
		SELECT @CampaignCode = ResourceListID
		FROM dbo.ResourceListDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = 177363
		AND DetailValue = @CampaignCode
		
	END
	
	--PRINT '@SourceResourceListID = ' + ISNULL(CONVERT(VARCHAR,@SourceResourceListID),'NULL')
	/* Deal with customer matching and pet number 
	
		Customer matching: 
			If the Q&B customer has claimed that they are an existing customer, shunt the processing into
			the customer matching sub-process (by setting the detail field to 'Match Claimed')

			Or if the Purchase is for more than one pet, add the second and above policies to the same collections matter, by setting
			the customer match detail field to 'Resolved - MATCH' and the match collections matterID.
			
			Otherwise no match is claimed and processing proceeds as normal for a single policy purchase 
			
		Pet number: 
			If a customer match is claimed, the pet number is set to a minimum of 2, so that any potential multiple pet discount is
			applied. The pet number is reset to its correct value during the matching sub-process.
			
			Otherwise the pet number is set to its proper value.
			
		Note.  For Alpha release 1, before we force MTA on existing policies on a customer that is about to become multipet, we
			   arrange that multipet Q&B buys get the discount on all pets in the Q&B buy by setting the first pet number to 2
	
	*/ 
	SELECT @PetNumber=@TotalPets
	
	/* GPR 2019-04-26 #1632 If we have an OMF Policy for this Customer increment the PetNumber by 1*/
	IF EXISTS (SELECT l.CustomerID FROM Lead l WITH (NOLOCK) 
				   INNER JOIN Matter m WITH (NOLOCK)  on m.LeadID = l.LeadID
				   INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170034 /*Product*/ 
				   WHERE l.LeadTypeID = 1492 /*Policy Admin*/ 
				   AND mdv.ValueInt = 151577 /*One month Free policy*/ 
				   AND l.CustomerID = @CustomerID)
	BEGIN
		SELECT @PetNumber = @PetNumber + 1
	END	
	
	IF @PetNumber > 2 OR @ExistingCustomerID > 0
	BEGIN
				
		/*Reduce the number of Pets by 1 for each one month free policy as these are not valid for multi pet.*/ 
		IF (@PetNumber - (SELECT COUNT(*)  FROM Lead l WITH (NOLOCK) 
				   INNER JOIN Matter m WITH (NOLOCK)  on m.LeadID = l.LeadID
				   INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170034 /*Product*/ 
				   WHERE l.LeadTypeID = 1492 /*Policy Admin*/ 
				   AND mdv.ValueInt = 151577 /*One month Free policy*/ 
				   AND l.CustomerID = @CustomerID)) > 2
		BEGIN 
			
			/*Existing Policy is not one month free*/ 
			SELECT @CustomerMatch = '72330' -- Resolved - QandB MATCH
			
			-- This isn't the real collections matterID, that won't have been created yet, but
			-- it will be used by SAE to find the collections matter ID
			SELECT TOP 1 @CollectionsMatterID=m.MatterID 
			FROM Customers c WITH (NOLOCK) 
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID AND l.LeadTypeID=1492
			INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID
			WHERE c.CustomerID=@CustomerID
			ORDER BY m.MatterID

			EXEC _C00_SimpleValueIntoField 175274,5144,@MatterID  -- use existing account details
			EXEC _C00_SimpleValueIntoField 175285,@CollectionsMatterID,@MatterID -- Matched Customer ID
		END 
		ELSE 
		BEGIN 
			/*If deducting the one month free policy leaves us with only one pet, then we want to follow the defaul setup*/ 
			SELECT @CustomerMatch = '72011' -- default to 'Match Not Claimed'
			
		END 
	END
	ELSE
	BEGIN
		SELECT @CustomerMatch = '72011' -- default to 'Match Not Claimed'
	END
	
	/*GPR 2019-05-08 Reworked for #1680*/	
	SELECT @CustomerID = CustomerID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	EXEC _C00_LogIt 'Info', 'CustomerID', '_C00_1273_Policy_AddPolicyDetails', @CustomerID, @ClientPersonnelID

	DECLARE @MatterCount INT

	SELECT @MatterCount = COUNT(m.MatterID) 
	FROM Lead l WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
	WHERE l.LeadTypeID = 1492 /*Policy Admin*/ 
	AND l.CustomerID = @CustomerID
					   
	SELECT @MatterCount = @MatterCount - (SELECT COUNT(*)  FROM Lead l WITH (NOLOCK) 
				   INNER JOIN Matter m WITH (NOLOCK)  on m.LeadID = l.LeadID
				   INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170034 /*Product*/ 
				   WHERE l.LeadTypeID = 1492 /*Policy Admin*/ 
				   AND mdv.ValueInt = 151577 /*One month Free policy*/ 
				   AND l.CustomerID = @CustomerID)				   		   
					   
	EXEC _C00_LogIt 'Info', 'Count of Matters', '_C00_1273_Policy_AddPolicyDetails', @MatterCount, @ClientPersonnelID
	EXEC _C00_LogIt 'Info', 'MatterID', '_C00_1273_Policy_AddPolicyDetails', @MatterID, @ClientPersonnelID
	
	IF @IsOneMonthFreePolicy = 0 /* GPR 2019-05-10 for C600 Defect 1699*/
	BEGIN
	
		IF @ExistingCust = 0
		BEGIN

			IF @MatterCount > 1
			BEGIN
				EXEC _C00_LogIt 'Info', 'Testing', '_C00_1273_Policy_AddPolicyDetails', 'No', @ClientPersonnelID
				EXEC dbo._C00_SimpleValueIntoField 180261, 5145, @MatterID, 58552 /*Send Email: No*/
			END
			ELSE /*@MatterCount = 1*/
			BEGIN
				EXEC _C00_LogIt 'Info', 'Testing', '_C00_1273_Policy_AddPolicyDetails', 'Yes Single or First Pet', @ClientPersonnelID
				EXEC dbo._C00_SimpleValueIntoField 180261, 5144, @MatterID, 58552 /*Send Email: Yes*/
			END

		END
		ELSE
		BEGIN /*Existing Customer*/
		
			IF @MatterID = (SELECT TOP(1) m.MatterID FROM Lead l WITH (NOLOCK) 
						   INNER JOIN Matter m WITH (NOLOCK)  ON m.LeadID = l.LeadID
						   LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170034 /*Product*/ AND mdv.ValueInt <> 151577 /*One month Free policy*/
						   WHERE l.LeadTypeID = 1492 /*Policy Admin*/ 
						   AND l.CustomerID = @CustomerID
						   ORDER BY l.WhenCreated ASC)
			BEGIN
					/*Existing Customer and first non-OMF*/
					EXEC _C00_LogIt 'Info', 'Testing', '_C00_1273_Policy_AddPolicyDetails', 'Yes', @ClientPersonnelID
					EXEC dbo._C00_SimpleValueIntoField 180261, 5144, @MatterID, 58552 /*Send Email: Yes*/
			END
			ELSE
			BEGIN
					/*Existing Customer, Multi and not first non-OMF*/
					EXEC _C00_LogIt 'Info', 'Testing', '_C00_1273_Policy_AddPolicyDetails', 'No', @ClientPersonnelID
					EXEC dbo._C00_SimpleValueIntoField 180261, 5145, @MatterID, 58552 /*Send Email: No*/
			END

		END

	END
	
	
	IF @ExistingCust = 1
	BEGIN
		--SELECT @CustomerMatch = '72008' -- Match claimed /*GPR 2020-12-08 removed for SDPRU-185*/ 
		IF @PetNumber=1 
			SELECT @PetNumber += 1
	END	


	IF @IsOneMonthFreePolicy = 1	/* 2018-03-03 Mark Beaumont - no payment setup for OMF */
		/* 2018-03-09 Mark Beaumont - Set the type of match to "breeder portal" to fire "Create linked leads and complete set-up OMF" */
		EXEC dbo._C00_SimpleValueIntoField	175280, 74515, @MatterID
	ELSE
	BEGIN
		
		-- set first payment date
		DECLARE @SeedDate DATE, @LastDayOfMonth INT
		SELECT @SeedDate = CONVERT(DATE,dbo.fn_GetDate_Local())

		IF CONVERT(INT,@PaymentMethodID) = 69931 -- Credit Card
		BEGIN
		
			/*ACE 2018-06-25 - If its CC then set the preferred pyament date to be the start date*/
			SELECT @FirstPaymentDate=@SeedDate,
					@PaymentDay = DATEPART(DD,@StartDate)
		
		END
		ELSE -- Direct Debit
		BEGIN
		
			-- default payment day if for some reason it hasn't been set
			IF CONVERT(INT,ISNULL(@PaymentDay,'0'))=0 SELECT @PaymentDay=CONVERT(VARCHAR(2),DATEPART(DAY,@StartDate))
			
			IF CONVERT(INT,@PaymentDay)<DATEPART(DAY,dbo.fn_GetDate_Local())
			BEGIN
			
				SELECT @SeedDate = CONVERT(DATE,DATEADD(MONTH,1,dbo.fn_GetDate_Local()))
			
			END
			SELECT @LastDayOfMonth=DATEPART(DAY,DATEADD(MILLISECOND, -3, DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,0,@SeedDate)) + 1, 0)))

			IF @LastDayOfMonth>CONVERT(INT,@PaymentDay) SELECT @LastDayOfMonth=CONVERT(INT,@PaymentDay)

			SELECT @FirstPaymentDate=dbo.fnGetNextWorkingDate (CONVERT(DATE,
																		CONVERT(VARCHAR(4),DATEPART(YEAR,@SeedDate)) 
																			+ '-' 
																			+ RIGHT('0' +  CONVERT(VARCHAR(2),DATEPART(MONTH,@SeedDate)),2) 
																			+ '-' 
																			+  RIGHT('0' + CONVERT(VARCHAR(2),@LastDayOfMonth),2))
																,0)	
		END
				
		IF @Debug = 1
		BEGIN
					
			SELECT @PaymentDay			AS PaymentDay,
				   @PaymentMethodID		AS PaymentMethodTypeId,
				   @PaymentIntervalID	AS PaymentIntervalId,	
				   @SortCode			AS SortCode,	
				   @AccountNumber		AS AccountNumber,	
				   @BankName			AS BankName,	
				   @BankAccountName		AS BankAccountName,	
				   @CreditCardToken		AS CreditCardToken,	
				   @CreditCardName		AS CreditCardHolderName,	
				   @CreditCardMasked	AS CreditCardNumberMasked,	
				   @CreditCardExpire	AS CreditCardExpireDate,
				   @FirstPaymentDate	AS FirstPaymentDate,
				   @ExistingCust		AS ExistingCust,
				   @CustomerMatch		AS CustomerMatch,
				   @PetNumber			AS PetNumber,
				   @QuoteSessionID		AS QuoteSessionID,
				   @TrackingCode		AS TrackingCode,
				   @CampaignCode		AS CampaignCode
			
		END
		ELSE
		BEGIN
		
			DECLARE @CollectionsFields TABLE (FieldID INT)
			INSERT @CollectionsFields (FieldID) 
			VALUES (170168), -- PaymentDay
					(170114), -- PaymentMethodId
					(170176), -- PaymentIntervalId
					(170105), -- SortCode
					(170106), -- AccountNumber
					(170170), -- BankName
					(170104), -- BankAccountName
					(170171), -- CreditCardToken
					(170172), -- CreditCardHolderName
					(170173), -- CreditCardNumberMasked
					(170174), -- CreditCardExpireDate
					(170175), -- FirstPaymentDate
					(175280), -- CustomerMatch
					(175453), -- PetNumber
					(175564),  -- Tracking Code
					(175488)  -- Campaign Code RL	
			
			INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue)
			SELECT	@ClientID, @MatterID, @LeadID, f.FieldID,
					CASE f.FieldID
						WHEN 170168 THEN ISNULL(@PaymentDay, '')
						WHEN 170114 THEN ISNULL(@PaymentMethodID, '')
						WHEN 170176 THEN	CASE @PaymentIntervalID
												WHEN '4' THEN '69943' /*Monthly*/
												WHEN '5' THEN '69944' /*Annually*/
												WHEN '2' THEN '193289' /*GPR 2019-11-05 altered to include Premium Credit Arrangement*/
												WHEN '7' THEN '293321' /*ACE 2020-10-19 altered to include fortnightly*/
												ELSE ''
											END
						WHEN 170105 THEN ISNULL(@SortCode, '')
						WHEN 170106 THEN ISNULL(@AccountNumber, '')		
						WHEN 170170 THEN ISNULL(@BankName, '')		
						WHEN 170104 THEN ISNULL(@BankAccountName, '')				
						WHEN 170171 THEN ISNULL(@CreditCardToken, '')				
						WHEN 170172 THEN ISNULL(@CreditCardName, '')				
						WHEN 170173 THEN ISNULL(@CreditCardMasked, '')			
						WHEN 170174 THEN ISNULL(CONVERT(VARCHAR(2000), @CreditCardExpire, 120), '')			
						WHEN 170175 THEN ISNULL(CONVERT(VARCHAR(2000), @FirstPaymentDate, 120), '')	
						WHEN 175280 THEN @CustomerMatch		
						WHEN 175453 THEN ISNULL(CAST(@PetNumber AS VARCHAR),'')
						WHEN 175564 THEN ISNULL(@TrackingCode, '')
						WHEN 175488 THEN ISNULL(@CampaignCode, '')
						ELSE ''	
					END	 
			FROM @CollectionsFields f
		
		END
	END		/* 2018-03-03 Mark Beaumont - end of no payment for OMF */
		
	DECLARE @MatterFields TABLE (FieldID INT)
	INSERT @MatterFields(FieldID) 
	VALUES (170034),	-- Policy Scheme
			(177418),	-- Sale	
			(170035),	-- Inception	
			(170036),	-- Start
			(170037),	-- End
			(176925),   -- Terms
			--(170038),	-- Status	
			(176954),	-- Lifetime Claimed
			(176955),	-- Lifetime Earnt
			(176961),	-- Premium at last period exit
			(176964),	-- SignUpMethod
			(177110),	-- Renewal method
			(180112),	-- Quote Tracking Code /*GPR 2018-07-20*/
			(315877),	-- FeeTableRowID
			(315878),	-- EnrollmentFee
			(315879),	-- TaxTableRowID
			(315880),	-- Taxes
			(315881),	-- Total
			(177484)    -- Initial Admin Fee Charged
	
	DECLARE @ProductID VARCHAR(2000),
			@EndDate DATE
			
	SELECT @ProductID = @Pet.value('(//ProductId)[1]', 'VARCHAR(2000)')
	
	/* Mark Beaumont 2018-02-27 - calculate the end date of One Month Free policies as one calendar month minus one day from the start date */
	IF @IsOneMonthFreePolicy = 1
		SELECT @EndDate = DATEADD(DAY,-1,DATEADD(MONTH, 1, @StartDate))
		--SELECT @EndDate = DATEADD(MONTH, 1, @StartDate) /*GPR C600 Defect #1416*/, /*GPR 2019-03-25 removed for #1552*/
		
	ELSE
		--SELECT @EndDate = DATEADD(YEAR, 1, @StartDate)
		SELECT @EndDate = DATEADD(DAY,-1,DATEADD(YEAR, 1, @StartDate)) /*CPS 2017-06-16*/
		
	DECLARE @Policy VARCHAR(100)
	
	SELECT @Policy = rdvProduct.DetailValue
	FROM dbo.ResourceList r WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvProduct WITH (NOLOCK) ON r.ResourceListID = rdvProduct.ResourceListID AND rdvProduct.DetailFieldID = 146200
	WHERE r.ResourceListID = @ProductID
	
	DECLARE @DiscountCode VARCHAR(100)
	SELECT @DiscountCode = @XmlRequest.value('(//DiscountCodes/string)[1]', 'VARCHAR(100)')
	
	/*GPR 2018-11-08 Added correct ClientPersonnelIDs for Aggregator and updated LuLi IDs*/
	DECLARE @SignUpMethod INT
	/* Mark Beaumont 2018-02-28 - Set sign up method for One Month Free */
	SELECT	@SignUpMethod = CASE WHEN @ClientPersonnelId IN (3868,3865,3867,3866) THEN 74564 /*Aggregator*/
							ELSE 
								CASE WHEN @ClientPersonnelId = 3836 THEN 76344 /*One Month Free (OMF)*/
								WHEN @ClientPersonnelId = 58552 THEN 74326 /*Online*/
								ELSE 74325 /*Offline*/
								END
							END
							
	/*GPR 2018-11-28 #1239 - If we have a source of Aggregator, we should update the SignUpMethod to be Aggregator*/
	
	SELECT @SourceResourceListID = dbo.fn_C600_GetSourceFromTrackingCode(@TrackingCode,2 /*ResourceListID*/)
	
	SELECT @Channel = rldv.ValueInt 
	FROM ResourceListDetailValues rldv WITH (NOLOCK) 
	WHERE rldv.ResourceListID = @SourceResourceListID 
	AND rldv.DetailFieldID = 180102 /*Channel*/
	
	IF @Channel = 76444 /*Aggregator*/
	BEGIN
	      SELECT @SignUpMethod = 74564 /*Aggregator*/
	END
	
	/*GPR 2019-03-28 give One Month Free policies a source/channel of OMF*/
	IF @IsOneMonthFreePolicy = 1
	BEGIN
		  SELECT @SourceResourceListID = 158083 -- OMF
		  
		  /*GPR 2019-05-07 Date of Purchase*/
		  
		  DECLARE @PurchaseDate DATE = CAST(@Now AS DATE)
		  
		  EXEC _C00_SimplevalueIntoField 170178, @PurchaseDate, @LeadID
		  
	END

	DECLARE @RenewalMethodId VARCHAR(100)
	SELECT @RenewalMethodId = @XmlRequest.value('(//RenewalMethodId)[1]', 'VARCHAR(100)')
	
	IF @Debug = 1
	BEGIN
		
		SELECT @ProductID AS ProductID, @StartDate AS StartDate, @EndDate AS EndDate
		
	END
	ELSE
	BEGIN
	
		UPDATE dbo.Matter
		SET MatterRef = @Policy
		WHERE MatterID = @MatterID
		AND ClientID = @ClientID

		IF @EnrollmentFeeTotal = 0.00 AND @EnrollmentFee > 0.00
		BEGIN

			SELECT @EnrollmentFeeTotal = @EnrollmentFee + @EnrollmentFeeTax

		END

		INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue)
		OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO @DetailValueHistory (MatterID, DetailFieldID, DetailValue, LeadOrMatter)
		SELECT	@ClientID, @MatterID, @LeadID, f.FieldID,
				CASE f.FieldID
					WHEN 170034 THEN ISNULL(CAST(@ProductID as VARCHAR), '')
					WHEN 177418 THEN ISNULL(CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)		,'')
					WHEN 170035 THEN ISNULL(CONVERT(VARCHAR, @StartDate, 120)					,'')
					WHEN 170036 THEN ISNULL(CONVERT(VARCHAR, @StartDate, 120)					,'')
					WHEN 176925 THEN ISNULL(CONVERT(VARCHAR, @StartDate, 120)					,'')
					WHEN 170037 THEN ISNULL(CONVERT(VARCHAR, @EndDate, 120)						,'')
					--WHEN 170038 THEN '43002' -- Live
					WHEN 176954 THEN ISNULL('0.00'												,'')
					WHEN 176955 THEN ISNULL('0.00'												,'')
					WHEN 176961 THEN ISNULL('0.00'												,'')
					WHEN 176964 THEN ISNULL(CAST(@SignUpMethod AS VARCHAR)						,'')
					WHEN 177110 THEN ISNULL(ISNULL(@RenewalMethodId, '')						,'')
					WHEN 180112 THEN ISNULL(ISNULL(CONVERT(VARCHAR,@SourceResourceListID), '-1'),'') /*Quote Tracking Code		GPR 2018-07-20*/
					WHEN 315877 THEN ISNULL(CONVERT(VARCHAR(100), @FeeTableRowID)				,'')
					/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee (Shred xml and xave to matter fields)*/
					WHEN 315878 THEN ISNULL(CONVERT(VARCHAR(100), @EnrollmentFee)				,'')
					WHEN 315879 THEN ISNULL(CONVERT(VARCHAR(100), @TaxTableRowID)				,'')
					WHEN 315880 THEN ISNULL(CONVERT(VARCHAR(100), @EnrollmentFeeTax)			,'')
					WHEN 315881 THEN ISNULL(CONVERT(VARCHAR(100), @EnrollmentFeeTotal)			,'')
					WHEN 177484 THEN ISNULL(CONVERT(VARCHAR(100), @EnrollmentFeeTotal)			,'') /*ALM 2021-03-10 FURKIN-378*/
					ELSE ''	
				END	
		FROM @MatterFields f		

		EXEC _C00_SimpleValueIntoField 170038,'43002',@MatterID

			
		DECLARE @VolExcess MONEY = 0
		SELECT @VolExcess = @Pet.value('(//VoluntaryExcess)[1]', 'MONEY')


		/*IES - 20200820 [SDPRU-9]*/
		DECLARE	@PostCodeGroup VARCHAR(10) = NULL
		DECLARE @Postcode VARCHAR(50)	= @XmlRequest.value('(//Postcode)[1]', 'VARCHAR(50)')
		DECLARE @BreedID INT			= @Pet.value('(//BreedId)[1]', 'INT')
		DECLARE @BirthDate DATE			= @Pet.value('(//BirthDate)[1]', 'DATE')
		DECLARE @SpeciesID INT			= @Pet.value('(//SpeciesId)[1]', 'INT')
		DECLARE @VetFeesID INT			= dbo.fn_C00_1272_GetVetFeesResourceList(@ClientID)
		DECLARE @IsWorkingDog BIT,
				@IsVeteran BIT,
				@IsVetOrStaff BIT,
				@IsRescue BIT, 
				@IsMedical BIT,
				@IsWellness BIT,
				@WorkingDog INT = 5145,
				@ExamFeesIncluded INT = 5145,
				@WellnessProduct INT = 5145

		DECLARE @Breed VARCHAR(2000) = (
			SELECT	DetailValue
			FROM	dbo.ResourceListDetailValues WITH(NOLOCK) 
			WHERE	ResourceListID = @BreedID 
			AND		DetailFieldID = 144270
		)

		DECLARE @Excess MONEY
		DECLARE @CoPay MONEY 

		-- 2021-01-26 CPS for Rules Engine Testing
		-- 2021-01-27 GPR Added Excess/Deductible Option Group for FURKIN-142
		-- 2021-01-28 GPR reviewed used field IDs, happy with existing.3
		IF @CountryID = 39
		BEGIN
			
			SELECT @Excess = @Pet.value('(//Excess)[1]', 'MONEY')
			SELECT @CoPay = @Pet.value('(//CoInsurance)[1]', 'MONEY')

			EXEC _C00_SimpleValueIntoField 313930,@CoPay,@LeadID
			EXEC _C00_SimpleValueIntoField 313932,@Excess,@LeadID
			
			/*GPR 2021-01-27 for FURKIN-142*/
			--IF @BrandingID IN (2000184) /*FURKIN*/
			--BEGIN

				SELECT @AgeInYears = DATEDIFF(YEAR,@BirthDate,@StartDate)

				SELECT @DeductibleGroupID = [dbo].[fn_C600_CalculateDeductibleOptionGroup](CAST(@Excess AS INT), @AgeInYears, @SpeciesID)

				EXEC _C00_LogIt @ClientID, @LeadID, '@DeductibleGroupID', @DeductibleGroupID, 58550 /*GPR 2021-01-28*/

				EXEC _C00_SimpleValueIntoField 315882,@DeductibleGroupID,@LeadID
			--END

			/*NG 2021-04-27 Update DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)*/
			EXEC dbo._C600_Add_StateVarianceDetailFieldsByBrand_RLID @BrandingID, @MatterID

		END

		IF @CountryID = 233 /*United States*/
		BEGIN 
			/*ALM 2020-08-05 Added @Excess and @CoPay PPET-7*/
			SELECT @Excess =  @Pet.value('(//Excess)[1]', 'MONEY')

			IF @Excess IS NULL /*GPR 2020-08-24*/
			BEGIN
				/*IES - 20200820 [SDPRU-9]*/
				SELECT @Excess = dbo.fn_C00_1272_Policy_GetPolicyExcessOrDefault(@ClientID, @MatterID, @PostCodeGroup, @Breed, @BirthDate, @StartDate, @SpeciesID, 0, @StartDate, @StartDate, 1, 1, @Excess)
			END

			EXEC _C00_SimpleValueIntoField 313932,@Excess,@LeadID

			/*ALM 2020-08-05 Added @Excess and @CoPay PPET-7*/
			SELECT @CoPay = @Pet.value('(//CoInsurance)[1]', 'MONEY')

			IF @CoPay IS NULL /*GPR 2020-08-24*/
			BEGIN	
				/*IES - 20200820 [SDPRU-9]*/
				SELECT @CoPay = dbo.fn_C00_1272_Policy_GetPolicyCoPayOrDefault(@ClientID, @MatterID, @StartDate, @BirthDate, @SpeciesID, 0, @Breed, @CoPay)
			END
			
			EXEC _C00_SimpleValueIntoField 313930,@CoPay,@LeadID

			/*ALM 2020-09-02 PPET-200*/
			/*GPR 2020-09-07 altered for SDPRU-33*/
			/*WorkingDog*/
			;WITH Underwriting AS
			(
			SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
			FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsWorkingDog = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 147277 /*Is your pet a service animal or used for commercial purposes?*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsWorkingDog = 1 AND @SpeciesID = 42989 /*GPR 2020-09-15 added Species check*/
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313929, 5144, @LeadID

				SELECT @WorkingDog = 5144
				
				SELECT @IsWorkingDog = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313929, 5145, @LeadID
			END

			/*Is the customer a veteran?*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsVeteran = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2001392 /*Is the customer a veteran?*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsVeteran = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313933, 5144, @LeadID /*ALM 2020-12-17 SDPRU-199*/
				
				SELECT @IsVeteran = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313933, 5145, @LeadID /*ALM 2020-12-17 SDPRU-199*/
			END

			/*Customer employed as Vet/Staff*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsVetOrStaff = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2001393 /*Customer employed as Vet/Staff*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsVetOrStaff = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313936, 5144, @LeadID /*ALM 2020-12-17 SDPRU-199*/
				
				SELECT @IsVetOrStaff = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313936, 5145, @LeadID /*ALM 2020-12-17 SDPRU-199*/
			END

			/*Pet is Rescue*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsRescue = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2001394 /*Is your pet a rescue animal?*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsRescue = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 177501, 5144, @LeadID
				
				SELECT @IsRescue = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 177501, 5145, @LeadID
			END

			/*Customer is Medical Professional*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsMedical = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2001395 /*Is the customer a medical professional?*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsMedical = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313935, 5144, @LeadID /*ALM 2020-12-17 SDPRU-199*/
				
				SELECT @IsMedical = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313935, 5145, @LeadID /*ALM 2020-12-17 SDPRU-199*/
			END
				
			/*Wellness*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsWellness = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2002259 /*Wellness Discount rating factor*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsWellness = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313934, 5144, @LeadID
				
				SELECT @IsWellness = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313934, 5145, @LeadID
			END


			/*GPR 2020-10-07 for SDPRU-77*/
			/*Is the customer from a known Internet partner you know about?*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsInternetPartner = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2002304 /*Internet Partner*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsInternetPartner = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313938, '5144', @LeadID

				SELECT @IsInternetPartner = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313938, '5145', @LeadID
			END

			/*Is the customer from a known strategic partner you know about?*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsStrategicPartner = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2002305 /*Strategic Partner*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsStrategicPartner = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313939, '5144', @LeadID
				
				SELECT @IsStrategicPartner = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313939, '5145', @LeadID
			END

			/*Is the customer from a known employer we provide benefits to?*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsCorporateGroupBenefit = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2002306 /*Corporate Group Benefit*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsCorporateGroupBenefit = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313937, '5144', @LeadID
				
				SELECT @IsCorporateGroupBenefit = 0 /*Reset for the next pet*/
			END
			ELSE
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 313937, '5145', @LeadID
			END

			/*Is the customer coming in from a broker with a surcharge?*/
			;WITH Underwriting AS
			(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @XMLRequest.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			)
			SELECT @IsBroker = 1
			FROM Underwriting uw 
			WHERE uw.QuestionId = 2002307 /*Corporate Group Benefit*/
			AND uw.AnswerId = 5144 /*Yes*/

			IF @IsBroker = 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 176964, '193317', @MatterID
				
				SELECT @IsBroker = 0 /*Reset for the next pet*/
			END

		END
		
		DECLARE @PassedOptionalCoverages VARCHAR(2000) = NULL
		SELECT @PassedOptionalCoverages = @Pet.value('(//OptionalCoverages)[1]', 'VARCHAR(2000)')

		/*GPR/ALM 2020-09-19 changed to use fnTableOfValuesFromCSV in place of = */
		/*GPR 2020-09-04 for PPET-394*/
		IF EXISTS (
			SELECT AnyValue 
			FROM dbo.fnTableOfValuesFromCSV(@PassedOptionalCoverages) 
			WHERE AnyValue = 2002260
		) /*Exam Fees*/
		BEGIN
			EXEC _C00_SimpleValueIntoField 313931, 5144, @LeadID
	
			/*GPR 2020-09-13 for _C605_Add_StateVarienceDetailFields_RLID*/
			SELECT @ExamFeesIncluded = 5144
		END
		--ELSE /*GPR 2020-09-04 removed for SDRPRU-26 as this subtracted the exam fee*/
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 313931, 5145, @LeadID
		--END
		
		/*GPR/ALM 2020-09-19 changed to use fnTableOfValuesFromCSV*/
		/*GPR/ALM 2020-09-14 for PPET-510 WELLNESS*/
		--IF @PassedOptionalCoverages IN (2002261, 2002262, 2002263) /*Wellness*/
		IF EXISTS (
			SELECT AnyValue 
			FROM dbo.fnTableOfValuesFromCSV(@PassedOptionalCoverages) 
			WHERE AnyValue IN (2002261, 2002262, 2002263)
		)
		BEGIN

			SELECT @WellnessProductRLID = AnyValue FROM dbo.fnTableOfValuesFromCSV(@PassedOptionalCoverages) WHERE AnyValue IN (2002261, 2002262, 2002263)

			EXEC _C00_SimpleValueIntoField 314235, @WellnessProductRLID, @MatterID /*Current Wellness Product*/

			SELECT @WellnessProduct = 5144

			IF @WellnessProductRLID = 2002261 -- Low
			BEGIN
				EXEC _C00_SimpleValueIntoField 314015, 5144, @LeadID -- Wellness Low
			END

			IF @WellnessProductRLID = 2002262 -- Medium
			BEGIN
				EXEC _C00_SimpleValueIntoField 314013, 5144, @LeadID -- Wellness Medium
			END

			IF @WellnessProductRLID = 2002263 -- High
			BEGIN
				EXEC _C00_SimpleValueIntoField 314014, 5144, @LeadID -- Wellness High
			END

			-- GPR 2020-09-16 Moved to after the Historical Policy TableRow is created
			/*Insert into Wellness History row*/
			--EXEC _C600_PA_Policy_UpdateWellnessHistory @MatterID, NULL

		END

		EXEC dbo._C00_1273_Policy_CreatePolicyHistory @MatterID, @ProductID, @StartDate, @StartDate, @EndDate, @StartDate, @VolExcess, @PassedOptionalCoverages, @Excess, @CoPay
		
		/*GPR 2020-09-16 for PPET-510 WELLNESS*/
		--IF @PassedOptionalCoverages IN (2002261, 2002262, 2002263) /*Wellness*/
		IF EXISTS (
			SELECT AnyValue 
			FROM dbo.fnTableOfValuesFromCSV(@PassedOptionalCoverages) 
			WHERE AnyValue IN (2002261, 2002262, 2002263)
		)
		BEGIN

			/*Insert into Wellness History row*/
			EXEC _C600_PA_Policy_UpdateWellnessHistory @MatterID, NULL

		END

		EXEC _C00_SimpleValueIntoField 170039,@VolExcess,@MatterID

		/*NG 2021-04-27 vary for 607*/
		IF @ClientID = 607
		BEGIN
			/*NG 2021-04-27 Update DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)*/
			EXEC dbo._C600_Add_StateVarianceDetailFieldsByBrand_RLID @BrandingID, @MatterID
		END
		ELSE
		BEGIN
			/*ALM/GPR 2020-09-13 Update DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)*/
			EXEC dbo._C605_Add_StateVarianceDetailFields_RLID @ProductID, @ExamFeesIncluded, @WorkingDog, @WellnessProduct, @MatterID
		END

		-- Apply process start
		DECLARE @LeadEventID INT
		EXEC @LeadEventID = dbo._C00_AddProcessStart @CaseID, @ClientPersonnelId
		
		/*ACE 2021-01-28 Detail value history anyone!?!*/
		INSERT INTO DetailValueHistory (ClientID, CustomerID, LeadID, CaseID, MatterID, LeadOrMatter, DetailFieldID, FieldValue, WhenSaved, ClientPersonnelID)
		SELECT @ClientID, d.CustomerID, d.LeadID, d.CaseID, d.MatterID, d.LeadOrMatter, d.DetailFieldID, d.DetailValue, @Now, @ClientPersonnelId
		FROM @DetailValueHistory d

	PRINT 'ACE'

	END	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_AddPolicyDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_AddPolicyDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_AddPolicyDetails] TO [sp_executeall]
GO
