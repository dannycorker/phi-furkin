SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetOutcomeEquation] @OutcomeEquationID int
AS

SELECT     dbo.OutcomeEquations.OutcomeID, dbo.OutcomeEquations.EquationID, dbo.Equations.EquationName, dbo.Equations.Equation, 
                      dbo.OutcomeEquations.OutcomeEquationID, dbo.Outcomes.ClientQuestionnaireID, dbo.Outcomes.ClientID 
FROM         dbo.OutcomeEquations INNER JOIN
                      dbo.Equations ON dbo.OutcomeEquations.EquationID = dbo.Equations.EquationID INNER JOIN
                      dbo.Outcomes ON dbo.OutcomeEquations.OutcomeID = dbo.Outcomes.OutcomeID

WHERE OutcomeEquationID = @OutcomeEquationID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeEquation] TO [sp_executeall]
GO
