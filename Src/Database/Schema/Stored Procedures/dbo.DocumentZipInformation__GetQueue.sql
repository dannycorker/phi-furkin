SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-19
-- Description:	Populate grid for DocumentZips page
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipInformation__GetQueue] 
	@ClientID int 
AS
BEGIN
	SET NOCOUNT ON;
	
	/* THIS IS WORK IN PROGRESS! */
		
	/* List all DZIs for this/every client in this/every status */
	;
	WITH InnerSql AS 
	(
		SELECT dz.DocumentZipInformationID, dz.StatusID, count(*) as dz_count 
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		WHERE dzi.StatusID IN (35, 37, 38) 
		GROUP BY dz.DocumentZipInformationID, dz.StatusID 
		/* Maybe UNPIVOT these counts ? */
	)
	SELECT 
	CASE WHEN dzi.ClientID = @ClientID THEN dzi.[ZipPrefix] ELSE '' END as [ZipPrefix], 
	i.dz_count as [ZipCount] /* Want to see done and todo separately? */ 
	FROM InnerSql i 
	INNER JOIN dbo.DocumentZipInformation dzi WITH (NOLOCK) ON dzi.DocumentZipInformationID = i.DocumentZipInformationID 
	INNER JOIN dbo.AquariumOption ao WITH (NOLOCK) ON ao.AquariumOptionID = dzi.StatusID 
	ORDER BY dzi.[DocumentZipInformationID] DESC 
		
END






GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__GetQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetQueue] TO [sp_executeall]
GO
