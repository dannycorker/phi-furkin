SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Outcomes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Outcomes_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[OutcomeID],
					[ClientQuestionnaireID],
					[OutcomeName],
					[OutcomeDescription],
					[SmsWhenFound],
					[mobileNumber],
					[EmailCustomer],
					[CustomersEmail],
					[ThankYouPage],
					[EmailFromAddress],
					[SmsToCustomer],
					[CustomersEmailSubject],
					[ClientID],
					[EmailClient],
					[ClientEmail],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Outcomes] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Outcomes_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_GetByClientID] TO [sp_executeall]
GO
