SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldMappingValueKey table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_GetByThirdPartyFieldMappingValueKeyID]
(

	@ThirdPartyFieldMappingValueKeyID int   
)
AS


				SELECT
					[ThirdPartyFieldMappingValueKeyID],
					[ClientID],
					[FieldMappingID],
					[DetailValue],
					[LookupListItemID],
					[ThirdPartyKey],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMappingValueKey] WITH (NOLOCK) 
				WHERE
										[ThirdPartyFieldMappingValueKeyID] = @ThirdPartyFieldMappingValueKeyID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByThirdPartyFieldMappingValueKeyID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByThirdPartyFieldMappingValueKeyID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_GetByThirdPartyFieldMappingValueKeyID] TO [sp_executeall]
GO
