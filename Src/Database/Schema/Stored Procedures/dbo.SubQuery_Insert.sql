SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SubQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQuery_Insert]
(

	@SubQueryID int    OUTPUT,

	@ClientID int   ,

	@SubQueryName varchar (50)  ,

	@SubQueryDesc varchar (500)  ,

	@SubQueryText varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[SubQuery]
					(
					[ClientID]
					,[SubQueryName]
					,[SubQueryDesc]
					,[SubQueryText]
					)
				VALUES
					(
					@ClientID
					,@SubQueryName
					,@SubQueryDesc
					,@SubQueryText
					)
				-- Get the identity value
				SET @SubQueryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Insert] TO [sp_executeall]
GO
