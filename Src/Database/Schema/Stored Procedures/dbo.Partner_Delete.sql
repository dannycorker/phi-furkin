SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Partner table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Partner_Delete]
(

	@PartnerID int   
)
AS


				DELETE FROM [dbo].[Partner] WITH (ROWLOCK) 
				WHERE
					[PartnerID] = @PartnerID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Delete] TO [sp_executeall]
GO
