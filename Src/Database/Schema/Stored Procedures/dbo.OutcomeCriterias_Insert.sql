SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the OutcomeCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_Insert]
(

	@OutcomeCriteriaID int    OUTPUT,

	@OutcomeID int   ,

	@QuestionPossibleAnswerID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[OutcomeCriterias]
					(
					[OutcomeID]
					,[QuestionPossibleAnswerID]
					,[ClientID]
					)
				VALUES
					(
					@OutcomeID
					,@QuestionPossibleAnswerID
					,@ClientID
					)
				-- Get the identity value
				SET @OutcomeCriteriaID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Insert] TO [sp_executeall]
GO
