SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-07-06
-- Description:	SQL After Note Update processing
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SANU]
	@LeadEventID int

AS
BEGIN
	SET NOCOUNT ON;

--declare @LeadEventID INT = 894

	DECLARE @myERROR INT,
			@CaseID INT

	SELECT  
	@CaseID = LeadEvent.CaseID
	FROM LeadEvent 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=LeadEvent.LeadID
	WHERE LeadEventID = @LeadEventID

	IF EXISTS ( SELECT * FROM LeadEvent WITH (NOLOCK) WHERE EventDeleted=1 AND LeadEventID=@LeadEventID)
	BEGIN

		-- Note deleted - delete any other related notes of this type 	
		EXEC _C600_DeleteANote @CaseID, NULL, @LeadEventID
			
	END
	ELSE
	BEGIN
		
		-- Note updated - update any other related notes of this type 
		UPDATE snk
		SET NotePriority=src.NotePriority,
			Comments=src.Comments,
			LeadDocumentID=src.LeadDocumentID
		FROM LeadEvent src
		INNER JOIN LeadEvent snk WITH (NOLOCK) ON snk.HoldLeadEventID=src.HoldLeadEventID AND snk.LeadEventID<>src.LeadEventID
		WHERE src.LeadEventID=@LeadEventID
		 
			
	END
		
END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SANU] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SANU] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SANU] TO [sp_executeall]
GO
