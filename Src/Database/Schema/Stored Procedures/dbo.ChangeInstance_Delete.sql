SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChangeInstance table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeInstance_Delete]
(

	@ChangeInstanceID int   
)
AS


				DELETE FROM [dbo].[ChangeInstance] WITH (ROWLOCK) 
				WHERE
					[ChangeInstanceID] = @ChangeInstanceID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeInstance_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Delete] TO [sp_executeall]
GO
