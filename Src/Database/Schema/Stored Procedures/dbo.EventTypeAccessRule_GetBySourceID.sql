SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAccessRule table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAccessRule_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[EventTypeAccessRuleID],
					[ClientID],
					[EventTypeID],
					[AccessRuleID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[EventTypeAccessRule] WITH (NOLOCK) 
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRule_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_GetBySourceID] TO [sp_executeall]
GO
