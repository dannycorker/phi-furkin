SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PanelItemTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemTypes_Insert]
(

	@PanelItemTypeID int    OUTPUT,

	@PanelItemTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[PanelItemTypes]
					(
					[PanelItemTypeName]
					)
				VALUES
					(
					@PanelItemTypeName
					)
				-- Get the identity value
				SET @PanelItemTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemTypes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Insert] TO [sp_executeall]
GO
