SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PaymentGateway table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentGateway_Find]
(

	@SearchUsingOR bit   = null ,

	@PaymentGatewayID int   = null ,

	@Gateway varchar (100)  = null ,

	@Name varchar (100)  = null ,

	@Description varchar (300)  = null ,

	@Url varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PaymentGatewayID]
	, [Gateway]
	, [Name]
	, [Description]
	, [Url]
    FROM
	[dbo].[PaymentGateway] WITH (NOLOCK) 
    WHERE 
	 ([PaymentGatewayID] = @PaymentGatewayID OR @PaymentGatewayID IS NULL)
	AND ([Gateway] = @Gateway OR @Gateway IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Url] = @Url OR @Url IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PaymentGatewayID]
	, [Gateway]
	, [Name]
	, [Description]
	, [Url]
    FROM
	[dbo].[PaymentGateway] WITH (NOLOCK) 
    WHERE 
	 ([PaymentGatewayID] = @PaymentGatewayID AND @PaymentGatewayID is not null)
	OR ([Gateway] = @Gateway AND @Gateway is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Url] = @Url AND @Url is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Find] TO [sp_executeall]
GO
