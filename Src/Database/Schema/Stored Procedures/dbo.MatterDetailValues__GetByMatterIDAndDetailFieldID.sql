SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MatterDetailValues__GetByMatterIDAndDetailFieldID]
(
@LeadID int,
@MatterID int,
@DetailFieldID int,
@ClientID int
)
AS
SELECT * FROM [dbo].[MatterDetailValues] WITH (NOLOCK) 
WHERE [MatterID] = @MatterID
  AND [DetailFieldID] = @DetailFieldID
  AND [LeadID] = @LeadID
  AND [ClientID] = @ClientID

Select @@ROWCOUNT
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByMatterIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues__GetByMatterIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByMatterIDAndDetailFieldID] TO [sp_executeall]
GO
