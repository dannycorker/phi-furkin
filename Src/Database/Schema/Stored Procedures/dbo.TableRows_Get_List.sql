SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the TableRows table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_Get_List]

AS


				
				SELECT
					[TableRowID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailFieldPageID],
					[DenyEdit],
					[DenyDelete],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID],
					[SourceID]
				FROM
					[dbo].[TableRows] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Get_List] TO [sp_executeall]
GO
