SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelDetailValues table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPersonnelDetailValueID],
					[ClientPersonnelID],
					[ClientID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ClientPersonnelDetailValues] WITH (NOLOCK) 
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelID] TO [sp_executeall]
GO
