SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-11-26
-- Description:	Returns a count of the nodes selected by the xpath expression
-- =============================================
CREATE PROCEDURE [dbo].[XML_NodeCount] 
	@Xml XML,
	@XPath VARCHAR(1000),
	@Namespace VARCHAR(1000) = NULL,
	@NamespacePrefix VARCHAR(50)= NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EncodedXPath VARCHAR(2000)
	SELECT @EncodedXPath = REPLACE(@XPath, '''', '''''')
	
	DECLARE @ReturnVal INT
	
	DECLARE @Sql NVARCHAR(1000)
	SELECT @Sql = ''
	IF @Namespace IS NOT NULL AND @NamespacePrefix IS NOT NULL
	BEGIN
		SELECT @Sql += 'WITH XMLNAMESPACES (''' + @Namespace + ''' as ' + @NamespacePrefix + ') '
	END
	SELECT @Sql += N'SELECT @ReturnValOut = CAST(CAST(@Xml.query(''count(' + @EncodedXPath + N')'') AS VARCHAR) AS INT)' 
	
	EXEC sp_executesql @Sql, N'@Xml XML, @ReturnValOut INT OUTPUT', @Xml, @ReturnValOut = @ReturnVal OUTPUT
	
	RETURN @ReturnVal
END




GO
GRANT VIEW DEFINITION ON  [dbo].[XML_NodeCount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[XML_NodeCount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[XML_NodeCount] TO [sp_executeall]
GO
