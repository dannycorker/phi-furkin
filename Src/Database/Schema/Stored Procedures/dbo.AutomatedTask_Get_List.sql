SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTask_Get_List]

AS


				
				SELECT
					[TaskID],
					[ClientID],
					[Taskname],
					[Description],
					[Enabled],
					[RunAtHour],
					[RunAtMinute],
					[RepeatTimeUnitsID],
					[RepeatTimeQuantity],
					[NextRunDateTime],
					[WorkflowTask],
					[AlreadyRunning],
					[AutomatedTaskGroupID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[MaximumAllowableErrors],
					[EventSubTypeThresholding]
				FROM
					[dbo].[AutomatedTask] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Get_List] TO [sp_executeall]
GO
