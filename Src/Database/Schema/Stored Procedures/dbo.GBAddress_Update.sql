SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the GBAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GBAddress_Update]
(

	@GBAddressID int   ,

	@ClientID int   ,

	@WebServiceURL varchar (500)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[GBAddress]
				SET
					[ClientID] = @ClientID
					,[WebServiceURL] = @WebServiceURL
				WHERE
[GBAddressID] = @GBAddressID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GBAddress_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Update] TO [sp_executeall]
GO
