SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: Push Lookup List Items to eCatcher QPA's
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateeCatcherQPA]
@LookupListID int,
@MasterQuestionID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ClientID int,
			@ClientQuestionnaireID int

	SELECT @ClientID = lul.ClientID
	FROM LookupList lul WITH (NOLOCK) 
	WHERE lul.LookupListID = @LookupListID
	
	SELECT @ClientQuestionnaireID = mq.ClientQuestionnaireID 
	From MasterQuestions mq WITH (NOLOCK) 
	WHERE mq.MasterQuestionID = @MasterQuestionID

	INSERT INTO QuestionPossibleAnswers (ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch, LinkedQuestionnaireQuestionPossibleAnswerID, ClientID)
	SELECT @ClientQuestionnaireID, @MasterQuestionID, luli.ItemValue, 0, NULL, @ClientID
	FROM LookupListItems luli WITH (NOLOCK)
	WHERE luli.LookupListID = @LookupListID
	AND luli.Enabled = 1
	AND NOT EXISTS (
		SELECT *
		FROM QuestionPossibleAnswers qpa WITH (NOLOCK)
		WHERE qpa.MasterQuestionID = @MasterQuestionID
		and qpa.AnswerText = luli.ItemValue
	)

	delete QuestionPossibleAnswers
	from QuestionPossibleAnswers
	inner join LookupListItems luli on luli.ItemValue = QuestionPossibleAnswers.AnswerText and luli.LookupListID = @LookupListID and luli.Enabled = 0
	where QuestionPossibleAnswers.MasterQuestionID = @MasterQuestionID

	SELECT @@ROWCOUNT as [Number of New Records Added]

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateeCatcherQPA] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateeCatcherQPA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateeCatcherQPA] TO [sp_executeall]
GO
