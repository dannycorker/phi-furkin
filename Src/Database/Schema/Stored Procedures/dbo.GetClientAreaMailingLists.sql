SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetClientAreaMailingLists] @ClientQuestionnaireID int

AS

Select ClientAreaMailingLists.Name, Email, PostCode, OutcomeID, YellowPagesAreaCode, ClientID, ClientQuestionnaireID, ClientAreaMailingListID, OnHold
From ClientAreaMailingLists
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingLists] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientAreaMailingLists] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingLists] TO [sp_executeall]
GO
