SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-05-16
-- Description:	Update Customer Payment Plan Details
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateCustNextPaymentDue]
@ClientID int = 0,
@Debug bit = 0
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Nextdate int

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int,
		DetailFieldAlias varchar(100)
	)
	
	DECLARE @History TABLE (CustomerID int, DetailFieldID int, DetailValue varchar(10))

	DECLARE @RowsAltered Table (AlterationType varchar(50), Alterations int)

	If @Debug = 1
	Begin
		Select dbo.fn_GetDate_Local(), 'Start'
	End

	INSERT @RequiredFields (DetailFieldID, DetailFieldAlias)
	select dfb.DetailFieldID, DetailFieldAlias
	from DetailFieldAlias dfb 
	Inner Join DetailFields df on df.DetailFieldID = dfb.DetailFieldID and df.LeadOrMatter IN (10)
	Where dfb.DetailFieldAlias IN ('Payment Frequency','Day of Month for Payment','Next Payment Due Date') 
	and dfb.LeadTypeID = 0
	and dfb.ClientID = @ClientID

	If @Debug = 1
	Begin
		Select dbo.fn_GetDate_Local(), 'RF Done'
	End

	DECLARE @TableRows TABLE (
		TableRowID int
	)

	/* Create all necessary MatterDetailValues records that don't already exist */
	INSERT INTO CustomerDetailValues (ClientID,CustomerID,DetailFieldID,DetailValue)
	SELECT @ClientID,c.CustomerID,rf.DetailFieldID,'' 
	FROM Customers c
	CROSS JOIN @RequiredFields rf 
	WHERE NOT EXISTS (SELECT * FROM CustomerDetailValues cdv WHERE cdv.CustomerID = c.CustomerID AND cdv.DetailFieldID = rf.DetailFieldID) 
	AND c.ClientID = @ClientID
	AND (c.Test = 0 or c.Test IS NULL)

	INSERT INTO @RowsAltered (AlterationType, Alterations)
	SELECT 'Missing Customer DetailValues', @@ROWCOUNT


	If @Debug = 1
	Begin
		Select dbo.fn_GetDate_Local(), 'RF Inserted'
	End

	/*	Update Next Payment Date uing Working Days - Monthly*/
	update CustomerDetailValues 
	set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), convert(int,luli.itemvalue), 11), 126)
	output inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue into @History (CustomerID, DetailFieldID, DetailValue)
	from Customers c
	INNER JOIN CustomerDetailValues ON CustomerDetailValues.CustomerID = c.CustomerID 
	inner join CustomerDetailValues cdv2 on cdv2.CustomerID = CustomerDetailValues.CustomerID /*Date of Month for Payment*/
	Inner Join @RequiredFields rf on rf.DetailFieldID = cdv2.DetailFieldID and rf.DetailFieldAlias = 'Day of Month for Payment'
	inner join CustomerDetailValues cdv on cdv.CustomerID = CustomerDetailValues.CustomerID 
	inner join LookupListItems luli_dateofmonth ON luli_dateofmonth.LookupListItemID = cdv.ValueInt and luli_dateofmonth.ItemValue = 'Monthly'
	Inner Join @RequiredFields rf2 on rf2.DetailFieldID = cdv.DetailFieldID and rf2.DetailFieldAlias = 'Payment Frequency'
	inner join lookuplistitems luli on luli.lookuplistitemid = cdv2.ValueInt 
	Inner Join @RequiredFields rf3 on rf3.DetailFieldID = CustomerDetailValues.DetailFieldID and rf3.DetailFieldAlias = 'Next Payment Due Date'
	where c.ClientID = @ClientID
	AND (c.Test = 0 or c.Test IS NULL)
	and (CustomerDetailValues.ValueDate IS NULL or datediff(dd, isnull(CustomerDetailValues.ValueDate, dbo.fn_GetDate_Local()), dbo.fn_GetDate_Local()) >= 0)

	INSERT INTO @RowsAltered (AlterationType, Alterations)
	SELECT 'Monthly', @@ROWCOUNT

	/*	Update Next Payment Date uing Working Days - Quarterly*/
	update CustomerDetailValues 
	set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dateadd(mm,2,dbo.fn_GetDate_Local()), convert(int,luli.itemvalue), 11), 126)
	output inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue into @History (CustomerID, DetailFieldID, DetailValue)
	from Customers c
	INNER JOIN CustomerDetailValues ON CustomerDetailValues.CustomerID = c.CustomerID 
	inner join CustomerDetailValues cdv2 on cdv2.CustomerID = CustomerDetailValues.CustomerID /*Date of Month for Payment*/
	Inner Join @RequiredFields rf on rf.DetailFieldID = cdv2.DetailFieldID and rf.DetailFieldAlias = 'Day of Month for Payment'
	inner join CustomerDetailValues cdv on cdv.CustomerID = CustomerDetailValues.CustomerID 
	inner join LookupListItems luli_dateofmonth ON luli_dateofmonth.LookupListItemID = cdv.ValueInt and luli_dateofmonth.ItemValue = 'Quarterly'
	Inner Join @RequiredFields rf2 on rf2.DetailFieldID = cdv.DetailFieldID and rf2.DetailFieldAlias = 'Payment Frequency'
	inner join lookuplistitems luli on luli.lookuplistitemid = cdv2.ValueInt 
	Inner Join @RequiredFields rf3 on rf3.DetailFieldID = CustomerDetailValues.DetailFieldID and rf3.DetailFieldAlias = 'Next Payment Due Date'
	where c.ClientID = @ClientID
	AND (c.Test = 0 or c.Test IS NULL)
	and (CustomerDetailValues.ValueDate IS NULL or datediff(dd, isnull(CustomerDetailValues.ValueDate, dbo.fn_GetDate_Local()), dbo.fn_GetDate_Local()) >= 0)

	INSERT INTO @RowsAltered (AlterationType, Alterations)
	SELECT 'Quarterly', @@ROWCOUNT

	If datepart(dd,dbo.fn_GetDate_Local()) between 1 and 6 Select @Nextdate = 4
	If datepart(dd,dbo.fn_GetDate_Local()) between 7 and 13 Select @Nextdate = 11
	If datepart(dd,dbo.fn_GetDate_Local()) between 14 and 20 Select @Nextdate = 18
	If datepart(dd,dbo.fn_GetDate_Local()) between 21 and 27 Select @Nextdate = 25
	If @Nextdate is NULL Select @Nextdate = 4

	/*	Update Next Payment Date uing Working Days - Weekly*/
	update CustomerDetailValues 
	set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), @Nextdate, 0), 126)
	output inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue into @History (CustomerID, DetailFieldID, DetailValue)
	from Customers c
	INNER JOIN CustomerDetailValues ON CustomerDetailValues.CustomerID = c.CustomerID 
	inner join CustomerDetailValues cdv2 on cdv2.CustomerID = CustomerDetailValues.CustomerID /*Date of Month for Payment*/
	Inner Join @RequiredFields rf on rf.DetailFieldID = cdv2.DetailFieldID and rf.DetailFieldAlias = 'Day of Month for Payment'
	inner join CustomerDetailValues cdv on cdv.CustomerID = CustomerDetailValues.CustomerID 
	inner join LookupListItems luli_dateofmonth ON luli_dateofmonth.LookupListItemID = cdv.ValueInt and luli_dateofmonth.ItemValue = 'Weekly'
	Inner Join @RequiredFields rf2 on rf2.DetailFieldID = cdv.DetailFieldID and rf2.DetailFieldAlias = 'Payment Frequency'
	inner join lookuplistitems luli on luli.lookuplistitemid = cdv2.ValueInt 
	Inner Join @RequiredFields rf3 on rf3.DetailFieldID = CustomerDetailValues.DetailFieldID and rf3.DetailFieldAlias = 'Next Payment Due Date'
	where c.ClientID = @ClientID
	AND (c.Test = 0 or c.Test IS NULL)
	and (CustomerDetailValues.ValueDate IS NULL or datediff(dd, isnull(CustomerDetailValues.ValueDate, dbo.fn_GetDate_Local()), dbo.fn_GetDate_Local()) >= 0)

	INSERT INTO @RowsAltered (AlterationType, Alterations)
	SELECT 'Weekly', @@ROWCOUNT

	INSERT INTO DetailValueHistory (ClientID, CustomerID, DetailFieldID, FieldValue, LeadOrMatter, ClientPersonnelID, WhenSaved)
	SELECT @ClientID, h.CustomerID, h.DetailFieldID, h.DetailValue, 10, 5076, dbo.fn_GetDate_Local()
	FROM @History h

	SELECT *
	FROM @RowsAltered

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateCustNextPaymentDue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateCustNextPaymentDue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateCustNextPaymentDue] TO [sp_executeall]
GO
