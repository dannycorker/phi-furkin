SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Equations table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Equations_Insert]
(

	@EquationID int    OUTPUT,

	@EquationName varchar (50)  ,

	@Equation varchar (MAX)  ,

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[Equations]
					(
					[EquationName]
					,[Equation]
					,[ClientQuestionnaireID]
					,[ClientID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@EquationName
					,@Equation
					,@ClientQuestionnaireID
					,@ClientID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @EquationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Insert] TO [sp_executeall]
GO
