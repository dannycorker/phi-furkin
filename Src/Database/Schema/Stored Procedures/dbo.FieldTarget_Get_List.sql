SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_Get_List]

AS


				
				SELECT
					[FieldTargetID],
					[Target],
					[ObjectName],
					[PropertyName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[FieldTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Get_List] TO [sp_executeall]
GO
