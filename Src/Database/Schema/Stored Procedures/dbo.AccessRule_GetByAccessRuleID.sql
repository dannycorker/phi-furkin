SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AccessRule table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_GetByAccessRuleID]
(

	@AccessRuleID int   
)
AS


				SELECT
					[AccessRuleID],
					[ClientID],
					[AccessRuleName],
					[AccessRuleDescription],
					[AccessRuleEnabled],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[PortalUserID],
					[DataLoaderObjectTypeID],
					[LeadTypeID],
					[DetailFieldSubTypeID],
					[DetailFieldPageID],
					[DetailFieldID],
					[ClientQuestionnaireID],
					[ValueDecoder],
					[ValueDecoderColumnFieldID],
					[ValueToCompare],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[AccessLevel]
				FROM
					[dbo].[AccessRule] WITH (NOLOCK) 
				WHERE
										[AccessRuleID] = @AccessRuleID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_GetByAccessRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_GetByAccessRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_GetByAccessRuleID] TO [sp_executeall]
GO
