SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateLoginAttemptsClientPersonnel]
@EmailAddress nvarchar(255),
@Attempts int

 AS

Update ClientPersonnel
SET  AttemptedLogins = @Attempts
where EmailAddress = @EmailAddress



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateLoginAttemptsClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateLoginAttemptsClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateLoginAttemptsClientPersonnel] TO [sp_executeall]
GO
