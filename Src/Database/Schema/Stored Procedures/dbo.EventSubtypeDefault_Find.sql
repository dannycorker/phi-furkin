SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventSubtypeDefault table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_Find]
(

	@SearchUsingOR bit   = null ,

	@EventSubtypeDefaultID int   = null ,

	@ClientID int   = null ,

	@EventSubtypeID int   = null ,

	@EventTypeID int   = null ,

	@LeadTypeID int   = null ,

	@ClientPersonnelID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventSubtypeDefaultID]
	, [ClientID]
	, [EventSubtypeID]
	, [EventTypeID]
	, [LeadTypeID]
	, [ClientPersonnelID]
    FROM
	[dbo].[EventSubtypeDefault] WITH (NOLOCK) 
    WHERE 
	 ([EventSubtypeDefaultID] = @EventSubtypeDefaultID OR @EventSubtypeDefaultID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventSubtypeID] = @EventSubtypeID OR @EventSubtypeID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventSubtypeDefaultID]
	, [ClientID]
	, [EventSubtypeID]
	, [EventTypeID]
	, [LeadTypeID]
	, [ClientPersonnelID]
    FROM
	[dbo].[EventSubtypeDefault] WITH (NOLOCK) 
    WHERE 
	 ([EventSubtypeDefaultID] = @EventSubtypeDefaultID AND @EventSubtypeDefaultID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventSubtypeID] = @EventSubtypeID AND @EventSubtypeID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Find] TO [sp_executeall]
GO
