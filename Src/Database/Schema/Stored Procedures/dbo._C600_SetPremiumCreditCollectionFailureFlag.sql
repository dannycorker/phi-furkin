SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2019-11-05
-- Description:	Premium Funding - Premium Collection Failure, Add or remove flag.
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SetPremiumCreditCollectionFailureFlag]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@EventTypeID INT,
			@CustomerID INT

	SELECT @EventTypeID = LeadEvent.EventTypeID, @CustomerID = Lead.CustomerID
	FROM LeadEvent LeadEvent WITH (NOLOCK)
	INNER JOIN Lead Lead WITH (NOLOCK) ON Lead.LeadID = LeadEvent.LeadID
	WHERE LeadEvent.LeadEventID = @LeadEventID

	/*Premium Collection Failure - Apply Flag*/
	IF @EventTypeID = 158934
	BEGIN
		EXEC _C00_SimpleValueIntoField 313780, 'true', @CustomerID
	END
	
	/*Premium Collection Failure - Remove Flag*/
	IF @EventTypeID = 158935
	BEGIN
		EXEC _C00_SimpleValueIntoField 313780, 'false', @CustomerID
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetPremiumCreditCollectionFailureFlag] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SetPremiumCreditCollectionFailureFlag] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetPremiumCreditCollectionFailureFlag] TO [sp_executeall]
GO
