SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AssetType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_GetByAssetSubTypeID]
(

	@AssetSubTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AssetTypeID],
					[AssetSubTypeID],
					[AssetTypeName],
					[Note],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[AssetType] WITH (NOLOCK) 
				WHERE
					[AssetSubTypeID] = @AssetSubTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_GetByAssetSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_GetByAssetSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_GetByAssetSubTypeID] TO [sp_executeall]
GO
