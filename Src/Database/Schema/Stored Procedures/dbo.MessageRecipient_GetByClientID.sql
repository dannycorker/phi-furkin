SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MessageRecipient table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[MessageRecipientID],
					[MessageID],
					[ClientPersonnelID],
					[PortalUserID],
					[ClientID]
				FROM
					[dbo].[MessageRecipient] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_GetByClientID] TO [sp_executeall]
GO
