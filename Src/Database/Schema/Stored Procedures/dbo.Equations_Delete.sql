SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Equations table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Equations_Delete]
(

	@EquationID int   
)
AS


				DELETE FROM [dbo].[Equations] WITH (ROWLOCK) 
				WHERE
					[EquationID] = @EquationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Delete] TO [sp_executeall]
GO
