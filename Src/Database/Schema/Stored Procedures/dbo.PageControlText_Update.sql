SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PageControlText table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_Update]
(

	@PageControlTextID int   ,

	@ClientID int   ,

	@LanguageID int   ,

	@PageName varchar (250)  ,

	@ControlName varchar (250)  ,

	@ControlText varchar (MAX)  ,

	@TooltipText varchar (MAX)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PageControlText]
				SET
					[ClientID] = @ClientID
					,[LanguageID] = @LanguageID
					,[PageName] = @PageName
					,[ControlName] = @ControlName
					,[ControlText] = @ControlText
					,[TooltipText] = @TooltipText
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[PageControlTextID] = @PageControlTextID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Update] TO [sp_executeall]
GO
