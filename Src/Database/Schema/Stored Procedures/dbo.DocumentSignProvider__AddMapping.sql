SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- Modified BY PR 16/03/2016 Added @DeclinedOrVoidedEventTypeID
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__AddMapping]
	@ClientID INT,
	@DSPUserID INT,
	@OutEventTypeID INT,
	@InEventTypeID INT,
	@DSPBrandingKey VARCHAR(50),
	@DeclinedOrVoidedEventTypeID INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	INSERT INTO DocumentSignProviderMapping (DSPTypeID, DSPAccountID, DSPUserID, ClientID, EmailOutETID, OutDocumentTypeID, DocInEventTypeID, BrandingKey, [Enabled], DeclinedOrVoidedEventTypeID)
	SELECT	dspu.DSPTypeID, dspu.DSPAccountID, dspu.DSPUserID, dspu.ClientID, outET.EventTypeID, outET.DocumentTypeID, inET.EventTypeID, @DSPBrandingKey, 1, declinedEvent.EventTypeID
	FROM	DocumentSignProviderUser dspu WITH (NOLOCK)
	INNER JOIN EventType outET WITH (NOLOCK) ON outET.EventTypeID = @OutEventTypeID
	INNER JOIN EventType inET WITH (NOLOCK) ON inET.EventTypeID = @InEventTypeID
	LEFT JOIN DocumentSignProviderMapping dspmExisits WITH (NOLOCK) ON dspmExisits.ClientID = @ClientID AND  dspmExisits.DSPUserID = @DSPUserID AND dspmExisits.EmailOutETID = @OutEventTypeID AND dspmExisits.DocInEventTypeID = @InEventTypeID AND dspmExisits.BrandingKey = @DSPBrandingKey
	LEFT JOIN EventType declinedEvent WITH (NOLOCK) on declinedEvent.EventTypeID=@DeclinedOrVoidedEventTypeID
	WHERE	dspu.ClientID = @ClientID
	AND		dspu.DSPUserID = @DSPUserID
	AND		dspmExisits.DSPMappingID IS NULL
	
	SELECT SCOPE_IDENTITY() DSPMappingID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__AddMapping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__AddMapping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__AddMapping] TO [sp_executeall]
GO
