SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-09-03
-- Description:	Inserts into the AutomatedTaskCommandControl Table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AutomatedTaskCommandLine_Insert] 
	@ClientID INT,
	@AutomatedTaskCommandControlID INT,
	@LineType VARCHAR(200),
	@LineText VARCHAR(2000),
	@LineNotes VARCHAR(2000),
	@LineOrder VARCHAR(2000),
	@Enabled INT

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [AutomatedTaskCommandLine]	([ClientID],[AutomatedTaskCommandControlID],[LineType],[LineText],[LineNotes],[LineOrder],[Enabled])
	VALUES (@ClientID,@AutomatedTaskCommandControlID,@LineType,@LineText,@LineNotes,@LineOrder,@Enabled)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandLine_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AutomatedTaskCommandLine_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandLine_Insert] TO [sp_executeall]
GO
