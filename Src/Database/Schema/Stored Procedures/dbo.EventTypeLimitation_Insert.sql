SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeLimitation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation_Insert]
(

	@EventTypeLimitationID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@EventTypeIDFrom int   ,

	@EventTypeIDTo int   ,

	@LimitationDays int   ,

	@Description varchar (50)  
)
AS


				
				INSERT INTO [dbo].[EventTypeLimitation]
					(
					[ClientID]
					,[LeadTypeID]
					,[EventTypeIDFrom]
					,[EventTypeIDTo]
					,[LimitationDays]
					,[Description]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@EventTypeIDFrom
					,@EventTypeIDTo
					,@LimitationDays
					,@Description
					)
				-- Get the identity value
				SET @EventTypeLimitationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Insert] TO [sp_executeall]
GO
