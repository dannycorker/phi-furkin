SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Location_Delete]
(

	@LocationID int   
)
AS


				DELETE FROM [dbo].[Location] WITH (ROWLOCK) 
				WHERE
					[LocationID] = @LocationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Location_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Delete] TO [sp_executeall]
GO
