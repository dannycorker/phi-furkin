SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-10-26
-- Description:	Gets all the accounts for a given customer id
-- =============================================
CREATE PROCEDURE [dbo].[Account__GetAllByCustomerID]	
	@CustomerID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT * FROM Account account WITH (NOLOCK) 
	LEFT JOIN dbo.AccountType at WITH (NOLOCK) on at.AccountTypeID = account.AccountTypeID
	WHERE account.ClientID=@ClientID AND account.CustomerID=@CustomerID 
	ORDER BY account.WhenCreated DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetAllByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__GetAllByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetAllByCustomerID] TO [sp_executeall]
GO
