SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AssetSubType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetSubType_Find]
(

	@SearchUsingOR bit   = null ,

	@AssetSubTypeID int   = null ,

	@AssetSubTypeName varchar (255)  = null ,

	@Note varchar (255)  = null ,

	@Enabled bit   = null ,

	@Deleted bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AssetSubTypeID]
	, [AssetSubTypeName]
	, [Note]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[AssetSubType] WITH (NOLOCK) 
    WHERE 
	 ([AssetSubTypeID] = @AssetSubTypeID OR @AssetSubTypeID IS NULL)
	AND ([AssetSubTypeName] = @AssetSubTypeName OR @AssetSubTypeName IS NULL)
	AND ([Note] = @Note OR @Note IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AssetSubTypeID]
	, [AssetSubTypeName]
	, [Note]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[AssetSubType] WITH (NOLOCK) 
    WHERE 
	 ([AssetSubTypeID] = @AssetSubTypeID AND @AssetSubTypeID is not null)
	OR ([AssetSubTypeName] = @AssetSubTypeName AND @AssetSubTypeName is not null)
	OR ([Note] = @Note AND @Note is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetSubType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Find] TO [sp_executeall]
GO
