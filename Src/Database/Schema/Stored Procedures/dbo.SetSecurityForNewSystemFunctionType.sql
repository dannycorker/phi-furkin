SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-18
-- Description:	Create default Rights for new system FunctionType records
-- =============================================
CREATE PROCEDURE [dbo].[SetSecurityForNewSystemFunctionType]
	@FunctionTypeID int,
	@Level int
AS
BEGIN
	SET NOCOUNT ON;

	-- Groups first
	INSERT INTO GroupFunctionControl (
		ClientPersonnelAdminGroupID,
		FunctionTypeID,
		ModuleID,
		HasDescendants,
		LeadTypeID,
		RightID
	)
	SELECT 
		gfc.ClientPersonnelAdminGroupID,
		@FunctionTypeID, 
		gfc.ModuleID,
		0,
		NULL, 
		@Level
	FROM dbo.FunctionType ft 
	INNER JOIN dbo.FunctionType ft_Parent ON ft_Parent.FunctionTypeID = ft.ParentFunctionTypeID 
	INNER JOIN dbo.GroupFunctionControl gfc ON gfc.FunctionTypeID = ft_Parent.FunctionTypeID 
	WHERE ft.FunctionTypeID = @FunctionTypeID 
	AND gfc.LeadTypeID IS NULL 
	AND gfc.HasDescendants = 1 

	-- Ditto for users
	INSERT INTO UserFunctionControl (
		ClientPersonnelID,
		FunctionTypeID,
		ModuleID,
		HasDescendants,
		LeadTypeID,
		RightID
	)
	SELECT 
		ufc.ClientPersonnelID,
		@FunctionTypeID, 
		ufc.ModuleID,
		0,
		NULL, 
		@Level
	FROM dbo.FunctionType ft 
	INNER JOIN dbo.FunctionType ft_Parent ON ft_Parent.FunctionTypeID = ft.ParentFunctionTypeID 
	INNER JOIN dbo.UserFunctionControl ufc ON ufc.FunctionTypeID = ft_Parent.FunctionTypeID 
	WHERE ft.FunctionTypeID = @FunctionTypeID 
	AND ufc.LeadTypeID IS NULL 
	AND ufc.HasDescendants = 1 
	

END











GO
GRANT VIEW DEFINITION ON  [dbo].[SetSecurityForNewSystemFunctionType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SetSecurityForNewSystemFunctionType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SetSecurityForNewSystemFunctionType] TO [sp_executeall]
GO
