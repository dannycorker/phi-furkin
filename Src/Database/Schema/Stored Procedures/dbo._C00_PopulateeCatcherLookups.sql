SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-10-24
-- Description:	Populate eCatcher Lookups with LM LookupLists
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateeCatcherLookups]
@MasterQuestionID int,
@LookuplistID int,
@DropListPriorToPopulation bit = 0,
@DefaultBranch int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ClientID int,
			@ClientQuestionnaireID int

	SELECT @ClientID = mq.ClientID, @ClientQuestionnaireID = mq.ClientQuestionnaireID
	FROM MasterQuestions mq WITH (NOLOCK)
	WHERE mq.MasterQuestionID = @MasterQuestionID

	IF @DropListPriorToPopulation = 1
	BEGIN
	
		DELETE FROM QuestionPossibleAnswers
		WHERE MasterQuestionID = @MasterQuestionID
	
	END

	INSERT INTO QuestionPossibleAnswers (ClientID, ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch, LinkedQuestionnaireQuestionPossibleAnswerID)
	SELECT @ClientID, @ClientQuestionnaireID, @MasterQuestionID, luli.ItemValue, @DefaultBranch, NULL
	FROM LookupListItems luli WITH (NOLOCK)
	WHERE luli.LookupListID = @LookuplistID
	ORDER BY luli.ItemValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateeCatcherLookups] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateeCatcherLookups] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateeCatcherLookups] TO [sp_executeall]
GO
