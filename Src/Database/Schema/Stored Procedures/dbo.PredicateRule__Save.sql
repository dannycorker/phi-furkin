SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Predicate Rule Save (Insert/Update)
-- =============================================
CREATE PROCEDURE [dbo].[PredicateRule__Save] 
	@PredicateRuleID INT, 
	@RuleName VARCHAR(2000) = '', 
	@RuleTypeID INT, 
	@FieldTypeID INT, 
	@FieldID INT, 
	@FieldComparisonTypeID INT, 
	@FieldValueTypeID INT, 
	@FieldValue1 VARCHAR(2000) = NULL, 
	@FieldValue2 VARCHAR(2000) = NULL, 
	@EventTypeID INT, 
	@AppliesTo INT,
	@Count INT,
	@RuleSQL VARCHAR(2000),
	@LeadTypeID INT, 
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	IF (@PredicateRuleID = 0)
	BEGIN
		INSERT INTO PredicateRule (RuleName, RuleTypeID, FieldTypeID, FieldID, FieldComparisonTypeID, FieldValueTypeID, FieldValue1, FieldValue2, EventTypeID, AppliesTo, [Count], RuleSQL, LeadTypeID, ClientID)
		VALUES (@RuleName, @RuleTypeID, @FieldTypeID, @FieldID, @FieldComparisonTypeID, @FieldValueTypeID, @FieldValue1, @FieldValue2, @EventTypeID, @AppliesTo, @Count, @RuleSQL, @LeadTypeID, @ClientID)
		
		SELECT @PredicateRuleID = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE PredicateRule
		SET
			RuleName = @RuleName, 
			RuleTypeID = @RuleTypeID, 
			FieldTypeID = @FieldTypeID, 
			FieldID = @FieldID, 
			FieldComparisonTypeID = @FieldComparisonTypeID, 
			FieldValueTypeID = @FieldValueTypeID, 
			FieldValue1 = @FieldValue1, 
			FieldValue2 = @FieldValue2, 
			EventTypeID = @EventTypeID, 
			AppliesTo = @AppliesTo,
			[Count] = @Count,
			RuleSQL = @RuleSQL
		WHERE
			PredicateRuleID = @PredicateRuleID
		AND LeadTypeID = @LeadTypeID
		AND ClientID = @ClientID
	END

	SELECT	pr.*, df.FieldName, et.EventTypeName
	FROM	PredicateRule pr WITH (NOLOCK)
	LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = pr.FieldID AND df.ClientID = @ClientID
	LEFT JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = pr.EventTypeID AND et.LeadTypeID = @LeadTypeID AND et.ClientID = @ClientID
	WHERE	pr.PredicateRuleID = @PredicateRuleID
	AND		pr.LeadTypeID = @LeadTypeID
	AND		pr.ClientID = @ClientID
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRule__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PredicateRule__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRule__Save] TO [sp_executeall]
GO
