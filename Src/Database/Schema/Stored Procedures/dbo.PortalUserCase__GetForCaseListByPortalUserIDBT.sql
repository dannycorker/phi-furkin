SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 20080626
-- Description:	Gets a useful list of Cases that the current PortalUser has access to.
--				JWG 2009-07-09 Added Status and AssignedTo options.
--				ACE 2010-02-01 Added Description and LeadType for BT
-- =============================================
CREATE PROCEDURE  [dbo].[PortalUserCase__GetForCaseListByPortalUserIDBT]
	@PortalUserID int,
	@LeadTypes varchar(1) = 'o',
	@OpenCases bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	Declare @OpenStatus Table (StatusID int, OpenCase int)
	
	Insert Into @OpenStatus
	select 718,	1
	UNION
	select 719,	1
	UNION
	select 720,	1
	UNION
	select 721,	1
	UNION
	select 722,	1
	UNION
	select 723,	1
	UNION
	select 724,	1
	UNION
	select 794,	1
	UNION
	select 858,	1
	UNION
	select 1071, 0
	UNION
	select 1072, 1
	UNION
	select 1261, 1
	UNION
	select 1262, 1
	UNION
	select 1263, 1
	UNION
	select 1264, 1
	UNION
	select 1265, 0
	UNION
	select 1266, 1
	UNION
	select 1267, 1
	UNION
	select 1268, 1
	
	Declare @LeadType Table (LeadTypeID int, FaultOrOrder varchar(1))
	
	Insert Into @LeadType
	Select 727, 'o'
	UNION
	Select 726, 'o'
	UNION
	Select 510, 'f'
	UNION
	Select 509, 'o'

	SELECT		puc.PortalUserCaseID, 
				puc.CaseID, 
				l.LeadID, 
				l.CustomerID, 
				convert(varchar(20) ,isnull(le.WhenCreated, ''), 120) as [WhenCreated], 
				isnull(c.Fullname + ' ' + l.LeadRef, '') as [FullName], 
				isnull(lt.LeadTypeName + '\' + COALESCE(ls_case.StatusDescription, ls_lead.StatusDescription, '') , '')AS [StatusDescription],
				ISNULL(cp.UserName, '') AS [AssignedToUserName],
				ISNULL(mdv.DetailValue, '') as DetailValue,
				ISNULL(mdv_appt.DetailValue, '') as [Appt],
				replace(ISNULL(mdv_Close.ValueDate, ''), '1900-01-01', '') as [Closure],
				ISNULL(mdv_custnote.DetailValue, '') as [CustomerNote],
				ca.ClientStatusID
	FROM        dbo.PortalUserCase puc (nolock) 
	INNER JOIN	dbo.Lead l (nolock) ON puc.LeadID = l.LeadID 
	INNER JOIN @LeadType let on let.LeadTypeID = l.LeadTypeID and let.FaultOrOrder = @LeadTypes
	INNER JOIN dbo.LeadType lt (nolock) on lt.LeadTypeID = l.LeadTypeID
	INNER JOIN	dbo.Customers c (nolock) ON l.CustomerID = c.CustomerID and (c.Test = 0 or c.Test IS NULL)
	LEFT JOIN	dbo.Cases ca (nolock) ON ca.CaseID = puc.CaseID 
	INNER Join @OpenStatus os on os.StatusID = ca.ClientStatusID and os.OpenCase = @OpenCases
	LEFT JOIN   dbo.LeadStatus ls_case (nolock) ON ls_case.StatusID = ca.ClientStatusID 
	LEFT JOIN   dbo.LeadStatus ls_lead (nolock) ON ls_lead.StatusID = l.ClientStatusID 
	LEFT JOIN   dbo.ClientPersonnel cp (nolock) ON cp.ClientPersonnelID = l.AssignedTo 
	LEFT JOIN  Matter m  (nolock) on m.CaseID = puc.CaseID
	LEFT JOIN MatterDetailValues mdv (nolock) on mdv.MatterID = m.MatterID and mdv.DetailFieldID IN (69526,92930)
	LEFT JOIN MatterDetailValues mdv_appt (nolock) on mdv_appt.MatterID = m.MatterID and mdv_appt.DetailFieldID IN (92840,92832,92932)
	LEFT JOIN MatterDetailValues mdv_Close (nolock) on mdv_Close.MatterID = m.MatterID and mdv_Close.DetailFieldID IN (92835,93052)
	LEFT JOIN MatterDetailValues mdv_custnote (nolock) on mdv_custnote.MatterID = m.MatterID and mdv_custnote.DetailFieldID IN (93124,93125)
	INNER JOIN LeadEvent le (nolock) on le.CaseID = puc.CaseID and le.EventDeleted = 0
	INNER JOIN EventType et (nolock) on et.EventTypeID = le.EventTypeID and et.EventSubtypeID = 10
	LEFT JOIN PortalStatusOrder pso (nolock) ON pso.StatusID = ca.ClientStatusID 
	WHERE		(puc.PortalUserID = @PortalUserID)
	ORDER BY    pso.PortalOrder, puc.CaseID


	--ORDER BY    l.LeadID, puc.CaseID 
	
    -- Original code
	/*SELECT		PortalUserCase.PortalUserCaseID,
				PortalUserCase.CaseID,
				Lead.LeadID,
				Lead.CustomerID,
				Lead.WhenCreated,
				Customers.Fullname
	FROM        PortalUserCase
	INNER JOIN	Lead ON PortalUserCase.LeadID = Lead.LeadID
	INNER JOIN	Customers ON Lead.CustomerID = Customers.CustomerID
	WHERE		(PortalUserCase.PortalUserID = @PortalUserID)
	ORDER BY    Lead.LeadID, PortalUserCase.CaseID */
	
	exec _C00_LogIt 'Portal', 'Get Case List', '','', @PortalUserID
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetForCaseListByPortalUserIDBT] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__GetForCaseListByPortalUserIDBT] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetForCaseListByPortalUserIDBT] TO [sp_executeall]
GO
