SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CaseTransferMappingID],
					[ClientRelationshipID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID],
					[NewClientID],
					[NewCustomerID],
					[NewLeadID],
					[NewCaseID],
					[NewMatterID],
					[CaseTransferStatusID],
					[NewLeadTypeID]
				FROM
					[dbo].[CaseTransferMapping] WITH (NOLOCK) 
				WHERE
					[CustomerID] = @CustomerID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByCustomerID] TO [sp_executeall]
GO
