SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FieldValidation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_Insert]
(

	@FieldValidationID int    OUTPUT,

	@ClientID int   ,

	@FieldID int   ,

	@ValidationGroup varchar (50)  ,

	@ValidationType int   ,

	@MaxValue int   ,

	@MinValue int   ,

	@ErrorMessage varchar (50)  ,

	@AccessLevelRestrictions varchar (50)  
)
AS


				
				INSERT INTO [dbo].[FieldValidation]
					(
					[ClientID]
					,[FieldID]
					,[ValidationGroup]
					,[ValidationType]
					,[MaxValue]
					,[MinValue]
					,[ErrorMessage]
					,[AccessLevelRestrictions]
					)
				VALUES
					(
					@ClientID
					,@FieldID
					,@ValidationGroup
					,@ValidationType
					,@MaxValue
					,@MinValue
					,@ErrorMessage
					,@AccessLevelRestrictions
					)
				-- Get the identity value
				SET @FieldValidationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Insert] TO [sp_executeall]
GO
