SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventType_Insert]
(

	@RPIEventTypeID int   ,

	@RPIEventTypeName varchar (50)  ,

	@RPIEventTypeDescription varchar (500)  ,

	@IsAquariumTriggered bit   ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[RPIEventType]
					(
					[RPIEventTypeID]
					,[RPIEventTypeName]
					,[RPIEventTypeDescription]
					,[IsAquariumTriggered]
					,[IsEnabled]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@RPIEventTypeID
					,@RPIEventTypeName
					,@RPIEventTypeDescription
					,@IsAquariumTriggered
					,@IsEnabled
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Insert] TO [sp_executeall]
GO
