SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartySystem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystem_Insert]
(

	@ThirdPartySystemId int    OUTPUT,

	@SystemName varchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ThirdPartySystem]
					(
					[SystemName]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@SystemName
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ThirdPartySystemId = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Insert] TO [sp_executeall]
GO
