SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-16
-- Description:	Get Equations for Processing by document blob utility
-- =============================================
CREATE PROCEDURE [dbo].[DocumentBlob__GetEquationsToProcess]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT df.DetailFieldID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.ClientID = @ClientID
	AND df.QuestionTypeID = 10
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBlob__GetEquationsToProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBlob__GetEquationsToProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBlob__GetEquationsToProcess] TO [sp_executeall]
GO
