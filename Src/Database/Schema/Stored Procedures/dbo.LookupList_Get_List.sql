SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LookupList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_Get_List]

AS


				
				SELECT
					[LookupListID],
					[LookupListName],
					[LookupListDescription],
					[ClientID],
					[Enabled],
					[SortOptionID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[LeadTypeID],
					[IsShared]
				FROM
					[dbo].[LookupList] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Get_List] TO [sp_executeall]
GO
