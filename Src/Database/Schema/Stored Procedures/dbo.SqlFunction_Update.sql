SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlFunction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunction_Update]
(

	@SqlFunctionID int   ,

	@OriginalSqlFunctionID int   ,

	@SqlFunctionName varchar (50)  ,

	@FunctionAlias varchar (50)  ,

	@FunctionDescription varchar (250)  ,

	@RestrictedToDataTypeID int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@IsPreferred bit   ,

	@Example varchar (250)  ,

	@ExampleResult varchar (250)  ,

	@ResultDataTypeID int   ,

	@ExpressionMask varchar (250)  ,

	@IsAggregate bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlFunction]
				SET
					[SqlFunctionID] = @SqlFunctionID
					,[SqlFunctionName] = @SqlFunctionName
					,[FunctionAlias] = @FunctionAlias
					,[FunctionDescription] = @FunctionDescription
					,[RestrictedToDataTypeID] = @RestrictedToDataTypeID
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
					,[IsPreferred] = @IsPreferred
					,[Example] = @Example
					,[ExampleResult] = @ExampleResult
					,[ResultDataTypeID] = @ResultDataTypeID
					,[ExpressionMask] = @ExpressionMask
					,[IsAggregate] = @IsAggregate
				WHERE
[SqlFunctionID] = @OriginalSqlFunctionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunction_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Update] TO [sp_executeall]
GO
