SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType_Update]
(

	@LeadTypeID int   ,

	@ClientID int   ,

	@LeadTypeName varchar (50)  ,

	@LeadTypeDescription varchar (255)  ,

	@Enabled bit   ,

	@Live bit   ,

	@MatterDisplayName varchar (100)  ,

	@LastReferenceInteger int   ,

	@ReferenceValueFormatID int   ,

	@IsBusiness bit   ,

	@LeadDisplayName varchar (100)  ,

	@CaseDisplayName varchar (100)  ,

	@CustomerDisplayName varchar (100)  ,

	@LeadDetailsTabImageFileName varchar (255)  ,

	@CustomerTabImageFileName varchar (255)  ,

	@UseEventCosts bit   ,

	@UseEventUOEs bit   ,

	@UseEventDisbursements bit   ,

	@UseEventComments bit   ,

	@AllowMatterPageChoices bit   ,

	@AllowLeadPageChoices bit   ,

	@IsRPIEnabled bit   ,

	@FollowupWorkingDaysOnly bit   ,

	@MatterLastReferenceInteger int   ,

	@MatterReferenceValueFormatID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@CalculateAllMatters bit   ,

	@SmsGatewayID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadType]
				SET
					[ClientID] = @ClientID
					,[LeadTypeName] = @LeadTypeName
					,[LeadTypeDescription] = @LeadTypeDescription
					,[Enabled] = @Enabled
					,[Live] = @Live
					,[MatterDisplayName] = @MatterDisplayName
					,[LastReferenceInteger] = @LastReferenceInteger
					,[ReferenceValueFormatID] = @ReferenceValueFormatID
					,[IsBusiness] = @IsBusiness
					,[LeadDisplayName] = @LeadDisplayName
					,[CaseDisplayName] = @CaseDisplayName
					,[CustomerDisplayName] = @CustomerDisplayName
					,[LeadDetailsTabImageFileName] = @LeadDetailsTabImageFileName
					,[CustomerTabImageFileName] = @CustomerTabImageFileName
					,[UseEventCosts] = @UseEventCosts
					,[UseEventUOEs] = @UseEventUOEs
					,[UseEventDisbursements] = @UseEventDisbursements
					,[UseEventComments] = @UseEventComments
					,[AllowMatterPageChoices] = @AllowMatterPageChoices
					,[AllowLeadPageChoices] = @AllowLeadPageChoices
					,[IsRPIEnabled] = @IsRPIEnabled
					,[FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly
					,[MatterLastReferenceInteger] = @MatterLastReferenceInteger
					,[MatterReferenceValueFormatID] = @MatterReferenceValueFormatID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[CalculateAllMatters] = @CalculateAllMatters
					,[SmsGatewayID] = @SmsGatewayID
				WHERE
[LeadTypeID] = @LeadTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Update] TO [sp_executeall]
GO
