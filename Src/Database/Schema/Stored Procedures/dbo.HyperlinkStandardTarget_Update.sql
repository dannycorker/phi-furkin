SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the HyperlinkStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget_Update]
(

	@HyperlinkStandardTargetID int   ,

	@ClientID int   ,

	@HyperlinkDetailFieldID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@TemplateTypeID int   ,

	@Notes varchar (250)  ,

	@IsSpecial bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[HyperlinkStandardTarget]
				SET
					[ClientID] = @ClientID
					,[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
					,[Target] = @Target
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
					,[TemplateTypeID] = @TemplateTypeID
					,[Notes] = @Notes
					,[IsSpecial] = @IsSpecial
				WHERE
[HyperlinkStandardTargetID] = @HyperlinkStandardTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Update] TO [sp_executeall]
GO
