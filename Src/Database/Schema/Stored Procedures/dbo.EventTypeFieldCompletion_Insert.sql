SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeFieldCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_Insert]
(

	@EventTypeFieldCompletionID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@DetailFieldID int   ,

	@InsertOptionID int   ,

	@DeleteOptionID int   
)
AS


				
				INSERT INTO [dbo].[EventTypeFieldCompletion]
					(
					[ClientID]
					,[EventTypeID]
					,[DetailFieldID]
					,[InsertOptionID]
					,[DeleteOptionID]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@DetailFieldID
					,@InsertOptionID
					,@DeleteOptionID
					)
				-- Get the identity value
				SET @EventTypeFieldCompletionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Insert] TO [sp_executeall]
GO
