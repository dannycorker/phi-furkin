SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalCaseListDisplay table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalCaseListDisplay_GetByField1ColumnDetailFieldID]
(

	@Field1ColumnDetailFieldID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalCaseListDisplayID],
					[ClientID],
					[LeadTypeID],
					[Field1Caption],
					[Field1TypeID],
					[Field1DetailFieldID],
					[Field1ColumnDetailFieldID],
					[Field2Caption],
					[Field2TypeID],
					[Field2DetailFieldID],
					[Field2ColumnDetailFieldID],
					[Field3Caption],
					[Field3TypeID],
					[Field3DetailFieldID],
					[Field3ColumnDetailFieldID],
					[Field4Caption],
					[Field4TypeID],
					[Field4DetailFieldID],
					[Field4ColumnDetailFieldID],
					[Field5Caption],
					[Field5TypeID],
					[Field5DetailFieldID],
					[Field5ColumnDetailFieldID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SqlQueryText]
				FROM
					[dbo].[PortalCaseListDisplay]
				WHERE
					[Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_GetByField1ColumnDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalCaseListDisplay_GetByField1ColumnDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_GetByField1ColumnDetailFieldID] TO [sp_executeall]
GO
