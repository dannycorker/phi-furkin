SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- ALTER date: 2012-02-02
-- Description:	Call up to the AquariusMaster database to do the find
-- =============================================
CREATE PROCEDURE [dbo].[AIFind] 
	@TargetString VARCHAR(1000) = '', 
	@TargetTypeList VARCHAR(10) = 'FN,IF,P,TF,TR,V', /* SELECT * FROM AquariusMaster.dbo.SqlServerObjectType */
	@WildcardSearch BIT = 1,
	@SystemDatabases BIT = 0,
	@UserDatabases BIT = 1,
	@DBSearchList VARCHAR(1000) = 'Aquarius',
	@SearchBody BIT = 0,
	@DebugMode BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	EXECUTE AS LOGIN = 'AquariumNet'

	EXEC AquariusMaster.dbo.AIFind @TargetString, @TargetTypeList,@WildcardSearch,@SystemDatabases,@UserDatabases,@DBSearchList,@SearchBody,@DebugMode
	
	REVERT

END




GO
GRANT VIEW DEFINITION ON  [dbo].[AIFind] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIFind] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIFind] TO [sp_executeall]
GO
