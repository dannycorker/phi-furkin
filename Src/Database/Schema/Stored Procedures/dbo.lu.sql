SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-03-28
-- Description:	LookupList helper proc
-- =============================================
CREATE PROCEDURE [dbo].[lu] 
	@ClientID int = NULL, 
	@LookupListID int = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ClientID IS NULL
	BEGIN
		SELECT 'All Lists' AS [All Lists], lu.* 
		FROM dbo.LookupList lu WITH (NOLOCK) 
		WHERE ((@LookupListID IS NULL) OR (lu.LookupListID = @LookupListID))
		ORDER BY lu.LookupListName, lu.ClientID 
	
	END
	ELSE
	BEGIN
		SELECT 'Client ' + CAST(lu.ClientID AS VARCHAR) + ' Lists' AS [Client Lists], lu.* 
		FROM dbo.LookupList lu WITH (NOLOCK) 
		WHERE lu.ClientID IN (0, @ClientID) 
		AND ((@LookupListID IS NULL) OR (lu.LookupListID = @LookupListID))
		ORDER BY lu.ClientID DESC, lu.LookupListName 
		
		/* Show the items for a specific list */
		IF @LookupListID > 0
		BEGIN
			SELECT 'List ' + CAST(@LookupListID AS VARCHAR) + ' Items' AS [List Items], luli.* 
			FROM dbo.LookupListItems luli WITH (NOLOCK) 
			WHERE luli.LookupListID = @LookupListID 
			ORDER BY luli.LookupListItemID 
		END
				
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[lu] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[lu] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[lu] TO [sp_executeall]
GO
