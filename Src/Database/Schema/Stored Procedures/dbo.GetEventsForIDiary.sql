SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2007-12-10
-- Description:	Get iDiary event and limitation list
-- JWG 2009-10-05 Show 50 events now
-- ACE 2011-01-04 NOLOCKED SP
-- JWG 2015-04-02 #31806 Filter by date range
-- ACE 2016-01-21 #36340 Add SqlQueryOverride
-- =============================================
CREATE PROCEDURE [dbo].[GetEventsForIDiary] 
	
	@UserID int,
	@SortBy int = 1, 
	@FromDate datetime = '1901-01-01', 
	@ToDate datetime = '2499-01-01'

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TopXRows int = 50,
			@ClientID int,
			@ClientPersonnelAdminGroupID int,
			@SQLOverrideQuery NVARCHAR(MAX)
			
	/*
	CS using Claim4Refunds as a test case 2012-09-13
	*/
	SELECT @ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID, @ClientID = cp.ClientID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @UserID
	
	SELECT TOP (1) @SQLOverrideQuery = sq.QueryText
	FROM Overrides ovr WITH (NOLOCK)
	INNER JOIN SqlQuery sq WITH (NOLOCK) ON sq.QueryID = ovr.QueryID AND sq.ClientID = @ClientID
	WHERE ovr.ClientID = @ClientID
	AND ovr.OverrideTypeID = 2 /*iDiary override*/
	AND ovr.ClientPersonnelAdminGroupID IN (@ClientPersonnelAdminGroupID, -1)
	ORDER BY ovr.ClientPersonnelAdminGroupID DESC 
	/*ORDER DESC So we get our group first, if no group set for me then use fallback (IE -1)*/
	
	IF @SQLOverrideQuery IS NOT NULL
	BEGIN
	
		EXECUTE sp_executesql 
			@SQLOverrideQuery, 
			N'@UserID INT, @SortBy int, @FromDate datetime, @ToDate datetime, @ClientPersonnelAdminGroupID INT, @ClientID INT', 
			@UserID, @SortBy, @FromDate, @ToDate, @ClientPersonnelAdminGroupID, @ClientID

	END
	ELSE
	BEGIN
	
		SELECT @ClientID = cp.ClientID
		FROM ClientPersonnel cp WITH (NOLOCK)
		WHERE cp.ClientPersonnelID = @UserID
		
		SELECT @TopxRows = case @ClientID 
			WHEN 184 THEN 100
			ELSE 50 
			END

		;	
		-- Get leads assigned to this user; these
		-- are the only ones we are going to show
		WITH LeadsAssignedToMe AS (
			select LeadID, CustomerID  
			from Lead  WITH (NOLOCK)
			where assignedto = @UserID 
			AND aquariumstatusid = 2 
		),

		-- For these leads, look for any "Process Deadlines"
		-- that the case is working towards
		LimitationOSEvents AS (
			select c.caseid, le.leadeventid, etl.eventtypeidfrom, etl.eventtypeidto, etl.limitationdays, etl.description, convert(varchar, datediff(dd, dbo.fn_GetDate_Local(), le.whencreated) + etl.limitationdays) as DaysToLimitation, rank() over(order by eventtypeidfrom) as [Ranking], datediff(dd, dbo.fn_GetDate_Local(), le.whencreated) + etl.limitationdays AS [NumDaysToLimitation]
			from LeadsAssignedToMe l  WITH (NOLOCK)
			inner join cases c WITH (NOLOCK) on l.leadid = c.leadid 
			inner join leadevent le WITH (NOLOCK) on c.caseid = le.caseid 
 			inner join eventtypelimitation etl WITH (NOLOCK) on le.eventtypeid = etl.eventtypeidfrom 
			where le.EventDeleted = 0 
			and not exists (
				select * 
				from leadevent le2  WITH (NOLOCK)
				where c.caseid = le2.caseid 
				and le2.EventDeleted = 0 
				and le2.eventtypeid = etl.eventtypeidto 
			)
		)

		-- Combine the sql clauses above into one big select statement:
		select top(@TopXRows) c.leadid, l.customerid, cu.fullname, c.caseid, c.caseref, et.eventtypeid, et.eventtypename, le.leadeventid, le.whencreated, 
		convert(varchar, datediff(dd, dbo.fn_GetDate_Local(), le.FollowupDateTime))AS 'Followup', isnull(LimitationOSEvents.description, '') AS 'Deadline', 
		CASE LimitationOSEvents.Ranking WHEN NULL THEN '' ELSE isnull(LimitationOSEvents.DaysToLimitation, '') END AS [DaysRemaining], 
		dbo.fnNextEventChoice(le.eventtypeid) AS NextEvent 
		from LeadsAssignedToMe l  WITH (NOLOCK)
		inner join cases c WITH (NOLOCK) on l.leadid = c.leadid 
		inner join customers cu WITH (NOLOCK) on l.customerid = cu.customerid 
		inner join leadevent le WITH (NOLOCK) on c.caseid = le.caseid 
		inner join eventtype et WITH (NOLOCK) on le.eventtypeid = et.eventtypeid 
		left join LimitationOSEvents WITH (NOLOCK) on c.caseid = LimitationOSEvents.caseid and LimitationOSEvents.Ranking = 1 
		where c.aquariumstatusid = 2 
		and le.EventDeleted = 0 
		and et.InProcess = 1 
		and et.Enabled = 1 
		and le.whenfollowedup IS NULL 
		and le.WhenCreated BETWEEN @FromDate AND @ToDate /* JWG 2015-04-02 #31806 */
		and (le.ClientID <> 213 or le.EventTypeID not in(136042,136043,136044,136046,136047,136048,136049,136051,136052,136055,136056,136058,136059,136060,136061,136063,136064,136067,136068,136070,136071,136098,137277))
		and	(c.ClientStatusID is null or c.ClientStatusID not in(4329,4511) )
		and exists (
			-- This block ensures that there is somewhere to go from this event, 
			-- not just to the event. Excludes escalation events.  An inner join
			-- to the EventChoice table does not achieve this, because it returns
			-- all possible choices in the result set.
			SELECT *  
			FROM EventChoice ec WITH (NOLOCK) 
			WHERE ec.EventTypeID = et.eventtypeid 
			AND ec.EscalationEvent = 0
		)
		and (le.FollowupDateTime is not null or le.ClientID <> 204) /*CS 2012-09-10 for ticket #16052*/
		--and (@ClientPersonnelAdminGroupID <> 325 OR exists ( SELECT mdv.ValueMoney
		--               FROM MatterDetailValues mdv WITH (NOLOCK) 
		--                                                     INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = mdv.MatterID 
		--                                                     WHERE mdv.DetailFieldID = 92321
		--                                                     and m.CaseID = c.CaseID  
		--                                                     and mdv.ValueMoney > 0.00 ) ) /*CS 2012-09-13 for Patrick at C4R - want to allow custom SQL filters*/

		--order by LimitationOSEvents.NumDaysToLimitation ASC, IsNull(le.FollowupDateTime, '2049-12-31') ASC, le.whencreated ASC
		order by
			/*CASE @SortBy
				-- Below line replaced for AlexE to resolve ordering where
				-- NULL Followup values were appearing at the top of the list
				-- WHEN 2 THEN convert(varchar, IsNull(le.FollowupDateTime, '2049-12-31')) -- Do NOT mix datatypes in this block!
				WHEN 2 THEN isnull(datediff(dd, dbo.fn_GetDate_Local(), le.FollowupDateTime), 999)
				WHEN 1 THEN isnull(LimitationOSEvents.description, '')
			END ASC
			,le.whencreated ASC*/
			/*
				JWG 2015-04-02 #31806 What does my 2009 comment say above?  Do NOT mix datatypes! 
				The above has been failing with "Conversion failed when converting the varchar value 'Deadline 1 ' to data type int." 
				since 2011 for anyone who uses limitation events. Which is in fact probably no-one except my test account 93.
			*/
			CASE WHEN @SortBy = 1 THEN isnull(LimitationOSEvents.description, '') ELSE NULL END,
			CASE WHEN @SortBy = 2 THEN isnull(datediff(dd, dbo.fn_GetDate_Local(), le.FollowupDateTime), 999) ELSE NULL END,
			le.WhenCreated ASC

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventsForIDiary] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventsForIDiary] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventsForIDiary] TO [sp_executeall]
GO
