SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the MasterQuestions table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_Find]
(

	@SearchUsingOR bit   = null ,

	@MasterQuestionID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@QuestionTypeID int   = null ,

	@QuestionText varchar (MAX)  = null ,

	@QuestionOrder int   = null ,

	@DefaultAnswerID int   = null ,

	@Mandatory bit   = null ,

	@QuestionToolTip varchar (255)  = null ,

	@LinkedQuestionnaireMasterQuestionID int   = null ,

	@Active bit   = null ,

	@MasterQuestionStatus int   = null ,

	@TextboxHeight int   = null ,

	@ClientID int   = null ,

	@AnswerPosition int   = null ,

	@DisplayAnswerAs int   = null ,

	@NumberOfAnswersPerRow int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@QuestionPossibleAnswerSortOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MasterQuestionID]
	, [ClientQuestionnaireID]
	, [QuestionTypeID]
	, [QuestionText]
	, [QuestionOrder]
	, [DefaultAnswerID]
	, [Mandatory]
	, [QuestionToolTip]
	, [LinkedQuestionnaireMasterQuestionID]
	, [Active]
	, [MasterQuestionStatus]
	, [TextboxHeight]
	, [ClientID]
	, [AnswerPosition]
	, [DisplayAnswerAs]
	, [NumberOfAnswersPerRow]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [QuestionPossibleAnswerSortOrder]
    FROM
	[dbo].[MasterQuestions] WITH (NOLOCK) 
    WHERE 
	 ([MasterQuestionID] = @MasterQuestionID OR @MasterQuestionID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([QuestionTypeID] = @QuestionTypeID OR @QuestionTypeID IS NULL)
	AND ([QuestionText] = @QuestionText OR @QuestionText IS NULL)
	AND ([QuestionOrder] = @QuestionOrder OR @QuestionOrder IS NULL)
	AND ([DefaultAnswerID] = @DefaultAnswerID OR @DefaultAnswerID IS NULL)
	AND ([Mandatory] = @Mandatory OR @Mandatory IS NULL)
	AND ([QuestionToolTip] = @QuestionToolTip OR @QuestionToolTip IS NULL)
	AND ([LinkedQuestionnaireMasterQuestionID] = @LinkedQuestionnaireMasterQuestionID OR @LinkedQuestionnaireMasterQuestionID IS NULL)
	AND ([Active] = @Active OR @Active IS NULL)
	AND ([MasterQuestionStatus] = @MasterQuestionStatus OR @MasterQuestionStatus IS NULL)
	AND ([TextboxHeight] = @TextboxHeight OR @TextboxHeight IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([AnswerPosition] = @AnswerPosition OR @AnswerPosition IS NULL)
	AND ([DisplayAnswerAs] = @DisplayAnswerAs OR @DisplayAnswerAs IS NULL)
	AND ([NumberOfAnswersPerRow] = @NumberOfAnswersPerRow OR @NumberOfAnswersPerRow IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([QuestionPossibleAnswerSortOrder] = @QuestionPossibleAnswerSortOrder OR @QuestionPossibleAnswerSortOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MasterQuestionID]
	, [ClientQuestionnaireID]
	, [QuestionTypeID]
	, [QuestionText]
	, [QuestionOrder]
	, [DefaultAnswerID]
	, [Mandatory]
	, [QuestionToolTip]
	, [LinkedQuestionnaireMasterQuestionID]
	, [Active]
	, [MasterQuestionStatus]
	, [TextboxHeight]
	, [ClientID]
	, [AnswerPosition]
	, [DisplayAnswerAs]
	, [NumberOfAnswersPerRow]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [QuestionPossibleAnswerSortOrder]
    FROM
	[dbo].[MasterQuestions] WITH (NOLOCK) 
    WHERE 
	 ([MasterQuestionID] = @MasterQuestionID AND @MasterQuestionID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([QuestionTypeID] = @QuestionTypeID AND @QuestionTypeID is not null)
	OR ([QuestionText] = @QuestionText AND @QuestionText is not null)
	OR ([QuestionOrder] = @QuestionOrder AND @QuestionOrder is not null)
	OR ([DefaultAnswerID] = @DefaultAnswerID AND @DefaultAnswerID is not null)
	OR ([Mandatory] = @Mandatory AND @Mandatory is not null)
	OR ([QuestionToolTip] = @QuestionToolTip AND @QuestionToolTip is not null)
	OR ([LinkedQuestionnaireMasterQuestionID] = @LinkedQuestionnaireMasterQuestionID AND @LinkedQuestionnaireMasterQuestionID is not null)
	OR ([Active] = @Active AND @Active is not null)
	OR ([MasterQuestionStatus] = @MasterQuestionStatus AND @MasterQuestionStatus is not null)
	OR ([TextboxHeight] = @TextboxHeight AND @TextboxHeight is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([AnswerPosition] = @AnswerPosition AND @AnswerPosition is not null)
	OR ([DisplayAnswerAs] = @DisplayAnswerAs AND @DisplayAnswerAs is not null)
	OR ([NumberOfAnswersPerRow] = @NumberOfAnswersPerRow AND @NumberOfAnswersPerRow is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([QuestionPossibleAnswerSortOrder] = @QuestionPossibleAnswerSortOrder AND @QuestionPossibleAnswerSortOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Find] TO [sp_executeall]
GO
