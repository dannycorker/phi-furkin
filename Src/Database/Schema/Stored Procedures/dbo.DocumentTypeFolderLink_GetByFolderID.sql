SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentTypeFolderLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink_GetByFolderID]
(

	@FolderID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentTypeID],
					[FolderID]
				FROM
					[dbo].[DocumentTypeFolderLink] WITH (NOLOCK) 
				WHERE
					[FolderID] = @FolderID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_GetByFolderID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink_GetByFolderID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_GetByFolderID] TO [sp_executeall]
GO
