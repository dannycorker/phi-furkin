SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Chart table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartID int   = null ,

	@QueryID int   = null ,

	@ClientID int   = null ,

	@ChartTitle varchar (150)  = null ,

	@ChartDescription varchar (MAX)  = null ,

	@ChartTypeID int   = null ,

	@XAxisColumn varchar (100)  = null ,

	@CreatedBy int   = null ,

	@CreatedOn datetime   = null ,

	@LastEditedBy int   = null ,

	@LastEditedOn datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartID]
	, [QueryID]
	, [ClientID]
	, [ChartTitle]
	, [ChartDescription]
	, [ChartTypeID]
	, [XAxisColumn]
	, [CreatedBy]
	, [CreatedOn]
	, [LastEditedBy]
	, [LastEditedOn]
    FROM
	[dbo].[Chart] WITH (NOLOCK) 
    WHERE 
	 ([ChartID] = @ChartID OR @ChartID IS NULL)
	AND ([QueryID] = @QueryID OR @QueryID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ChartTitle] = @ChartTitle OR @ChartTitle IS NULL)
	AND ([ChartDescription] = @ChartDescription OR @ChartDescription IS NULL)
	AND ([ChartTypeID] = @ChartTypeID OR @ChartTypeID IS NULL)
	AND ([XAxisColumn] = @XAxisColumn OR @XAxisColumn IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([CreatedOn] = @CreatedOn OR @CreatedOn IS NULL)
	AND ([LastEditedBy] = @LastEditedBy OR @LastEditedBy IS NULL)
	AND ([LastEditedOn] = @LastEditedOn OR @LastEditedOn IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartID]
	, [QueryID]
	, [ClientID]
	, [ChartTitle]
	, [ChartDescription]
	, [ChartTypeID]
	, [XAxisColumn]
	, [CreatedBy]
	, [CreatedOn]
	, [LastEditedBy]
	, [LastEditedOn]
    FROM
	[dbo].[Chart] WITH (NOLOCK) 
    WHERE 
	 ([ChartID] = @ChartID AND @ChartID is not null)
	OR ([QueryID] = @QueryID AND @QueryID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ChartTitle] = @ChartTitle AND @ChartTitle is not null)
	OR ([ChartDescription] = @ChartDescription AND @ChartDescription is not null)
	OR ([ChartTypeID] = @ChartTypeID AND @ChartTypeID is not null)
	OR ([XAxisColumn] = @XAxisColumn AND @XAxisColumn is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([CreatedOn] = @CreatedOn AND @CreatedOn is not null)
	OR ([LastEditedBy] = @LastEditedBy AND @LastEditedBy is not null)
	OR ([LastEditedOn] = @LastEditedOn AND @LastEditedOn is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Find] TO [sp_executeall]
GO
