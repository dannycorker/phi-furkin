SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_Update]
(

	@SqlQueryTableID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryTableName varchar (50)  ,

	@TableAlias varchar (50)  ,

	@TableDisplayOrder int   ,

	@JoinType varchar (25)  ,

	@JoinText varchar (500)  ,

	@JoinTableID int   ,

	@JoinRTRID int   ,

	@TempTableID int   ,

	@TempJoinTableID int   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryTables]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[SqlQueryTableName] = @SqlQueryTableName
					,[TableAlias] = @TableAlias
					,[TableDisplayOrder] = @TableDisplayOrder
					,[JoinType] = @JoinType
					,[JoinText] = @JoinText
					,[JoinTableID] = @JoinTableID
					,[JoinRTRID] = @JoinRTRID
					,[TempTableID] = @TempTableID
					,[TempJoinTableID] = @TempJoinTableID
					,[SourceID] = @SourceID
				WHERE
[SqlQueryTableID] = @SqlQueryTableID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Update] TO [sp_executeall]
GO
