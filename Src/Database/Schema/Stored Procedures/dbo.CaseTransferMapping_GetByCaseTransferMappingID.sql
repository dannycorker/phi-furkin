SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_GetByCaseTransferMappingID]
(

	@CaseTransferMappingID int   
)
AS


				SELECT
					[CaseTransferMappingID],
					[ClientRelationshipID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID],
					[NewClientID],
					[NewCustomerID],
					[NewLeadID],
					[NewCaseID],
					[NewMatterID],
					[CaseTransferStatusID],
					[NewLeadTypeID]
				FROM
					[dbo].[CaseTransferMapping] WITH (NOLOCK) 
				WHERE
										[CaseTransferMappingID] = @CaseTransferMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByCaseTransferMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_GetByCaseTransferMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByCaseTransferMappingID] TO [sp_executeall]
GO
