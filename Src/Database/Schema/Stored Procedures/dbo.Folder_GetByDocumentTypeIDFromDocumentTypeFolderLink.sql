SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records through a junction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_GetByDocumentTypeIDFromDocumentTypeFolderLink]
(

	@DocumentTypeID int   
)
AS


SELECT dbo.[Folder].[FolderID]
       ,dbo.[Folder].[ClientID]
       ,dbo.[Folder].[FolderTypeID]
       ,dbo.[Folder].[FolderParentID]
       ,dbo.[Folder].[FolderName]
       ,dbo.[Folder].[FolderDescription]
       ,dbo.[Folder].[WhenCreated]
       ,dbo.[Folder].[WhoCreated]
       ,dbo.[Folder].[Personal]
       ,dbo.[Folder].[SourceID]
  FROM dbo.[Folder] WITH (NOLOCK)
 WHERE EXISTS (SELECT 1
                 FROM dbo.[DocumentTypeFolderLink] WITH (NOLOCK)
                WHERE dbo.[DocumentTypeFolderLink].[DocumentTypeID] = @DocumentTypeID
                  AND dbo.[DocumentTypeFolderLink].[FolderID] = dbo.[Folder].[FolderID]
                  )
				SELECT @@ROWCOUNT			
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_GetByDocumentTypeIDFromDocumentTypeFolderLink] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_GetByDocumentTypeIDFromDocumentTypeFolderLink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_GetByDocumentTypeIDFromDocumentTypeFolderLink] TO [sp_executeall]
GO
