SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Inject CSV into a document template 
-- JWG 2011-01-31 Cater for LeadDocumentFS 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InjectCSVIntoLeadDoc]
(
	@LeadID int,
	@CaseID int,
	@WhoUploaded int,
	@EventTypeID int,
	@SQLQueryID int
)


AS
BEGIN

	SET NOCOUNT ON;

	declare @ClientID int, 
			@LeadDocumentTitle varchar(1000),
			@LeadDocumentID int,
			@Leftno int,
			@SQLString varchar(max),
			@InfoLine varchar(1000)

	Select 	@LeadDocumentTitle = 'CSV Created By AE'

	declare @ResultSetTable table (QueryContents varchar(max))

	/* Get Query From DB*/
	Select @SqlString = QueryText, @ClientID = ClientID
	From SqlQuery
	where QueryID = @SQLQueryID

	/* Append CaseID to Query to only get info for the current case */
	Select @SQLString = @SQLString + 'and Cases.CaseID = ' + Convert(varchar,@CaseID)

	/* Get the char no at the end of the line to get only output header*/
	select @Leftno = CHARINDEX(char(13), @SQLString, 1)

	/* Get output header */
	Select @InfoLine = replace(left(@SQLString, @LeftNo), 'Select ', '')

	/* Set all fields to varchar to avoid sql converting varchar to int*/
	Select @SQLString = replace(@SQLString, ',', '), '''') + '','' + isnull(convert(varchar,')
	Select @SQLString = replace(@SQLString, 'Select', 'Select isnull(convert(varchar,')
	Select @SQLString = replace(@SQLString, 'From', '),'''') From')

	/* Inject results of query to temp table*/
	insert into @ResultSetTable (QueryContents)
	exec (@SqlString)


	/* Send contents of temp table into lead document */
	/*
	INSERT INTO [dbo].[LeadDocument]([ClientID],[LeadID],[DocumentTypeID],[LeadDocumentTitle],[UploadDateTime],[WhoUploaded],[DocumentBLOB],[FileName],[EmailBLOB],[DocumentFormat],[EmailFrom],[EmailTo],[CcList],[BccList],[ElectronicSignatureDocumentKey])
	SELECT @ClientID,@LeadID,NULL,@LeadDocumentTitle,dbo.fn_GetDate_Local(),@WhoUploaded,@InfoLine + QueryContents,'csv.csv',NULL,NULL,NULL,NULL,NULL,NULL,NULL
	From @ResultSetTable

	Select @LeadDocumentID = SCOPE_IDENTITY()
	*/
	
	DECLARE @VB varbinary(max)
	
	SELECT @VB = convert(varbinary(max), @InfoLine + r.QueryContents) 
	FROM @ResultSetTable r 
	
	EXEC @LeadDocumentID = dbo._C00_CreateLeadDocument @ClientID = @ClientID, @LeadID = @LeadID, @DocumentBLOB = @VB, @LeadDocumentTitle = @LeadDocumentTitle, @WhoUploaded = @WhoUploaded, @FileName = 'csv.csv' 

	/* Create a new LeadEvent for the document */
	INSERT INTO [dbo].[LeadEvent]([ClientID],[LeadID],[WhenCreated],[WhoCreated],[Cost],[Comments],[EventTypeID],[NoteTypeID],[FollowupDateTime],[WhenFollowedUp],[AquariumEventType],[NextEventID],[CaseID],[LeadDocumentID],[NotePriority],[DocumentQueueID],[EventDeleted],[WhoDeleted],[DeletionComments],[ContactID],[BaseCost],[DisbursementCost],[DisbursementDescription],[ChargeOutRate],[UnitsOfEffort],[CostEnteredManually],[IsOnHold],[HoldLeadEventID])
	Select @ClientID,@LeadID,dateadd(mi,1,dbo.fn_GetDate_Local()),@WhoUploaded,'','',@EventTypeID,NULL,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local(),NULL,NULL,@CaseID,@LeadDocumentID,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InjectCSVIntoLeadDoc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InjectCSVIntoLeadDoc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InjectCSVIntoLeadDoc] TO [sp_executeall]
GO
