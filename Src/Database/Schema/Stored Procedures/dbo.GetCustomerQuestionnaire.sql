SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetCustomerQuestionnaire] @CustomerID int, @ClientQuestionnaireID int

AS

Select CustomerQuestionnaireID, ClientQuestionnaireID, CustomerID, SubmissionDate, TrackingID, Referrer, SearchTerms, ClientID 
From CustomerQuestionnaires WITH (NOLOCK) 
Where ((CustomerID = @CustomerID)  AND (ClientQuestionnaireID = @ClientQuestionnaireID) )


GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerQuestionnaire] TO [sp_executeall]
GO
