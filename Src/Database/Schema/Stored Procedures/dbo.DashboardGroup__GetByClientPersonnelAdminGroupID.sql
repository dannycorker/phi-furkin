SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-06
-- Description:	Get a list of Dashboard Groups for the app
-- =============================================
CREATE PROCEDURE [dbo].[DashboardGroup__GetByClientPersonnelAdminGroupID]
	@ClientID INT,
	@ClientPersonnelAdminGroupID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM dbo.DashboardGroup d WITH (NOLOCK)
	WHERE d.ClientID = @ClientID
	AND (d.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID OR d.ClientPersonnelAdminGroupID IS NULL)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardGroup__GetByClientPersonnelAdminGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardGroup__GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardGroup__GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
