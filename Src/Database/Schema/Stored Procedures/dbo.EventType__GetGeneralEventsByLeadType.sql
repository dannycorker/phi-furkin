SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/06/2018
-- Description:	Gets a list of general events by lead type Id
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetGeneralEventsByLeadType]
	@LeadTypeID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT EventTypeID, EventTypeName FROM EventType WITH (NOLOCK) 
	WHERE LeadTypeID=@LeadTypeID AND EventSubtypeID=1
	ORDER BY EventTypeName ASC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetGeneralEventsByLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetGeneralEventsByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetGeneralEventsByLeadType] TO [sp_executeall]
GO
