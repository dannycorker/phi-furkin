SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Matter table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_GetByLeadID]
(

	@LeadID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[MatterID],
					[ClientID],
					[MatterRef],
					[CustomerID],
					[LeadID],
					[MatterStatus],
					[RefLetter],
					[BrandNew],
					[CaseID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[Matter] WITH (NOLOCK) 
				WHERE
					[LeadID] = @LeadID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByLeadID] TO [sp_executeall]
GO
