SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LookupList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_Insert]
(

	@LookupListID int    OUTPUT,

	@LookupListName varchar (500)  ,

	@LookupListDescription varchar (2000)  ,

	@ClientID int   ,

	@Enabled bit   ,

	@SortOptionID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@LeadTypeID int   ,

	@IsShared bit   
)
AS


				
				INSERT INTO [dbo].[LookupList]
					(
					[LookupListName]
					,[LookupListDescription]
					,[ClientID]
					,[Enabled]
					,[SortOptionID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[LeadTypeID]
					,[IsShared]
					)
				VALUES
					(
					@LookupListName
					,@LookupListDescription
					,@ClientID
					,@Enabled
					,@SortOptionID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@LeadTypeID
					,@IsShared
					)
				-- Get the identity value
				SET @LookupListID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Insert] TO [sp_executeall]
GO
