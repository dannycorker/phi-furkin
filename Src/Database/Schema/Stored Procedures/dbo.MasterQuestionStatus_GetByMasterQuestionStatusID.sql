SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MasterQuestionStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestionStatus_GetByMasterQuestionStatusID]
(

	@MasterQuestionStatusID int   
)
AS


				SELECT
					[MasterQuestionStatusID],
					[MasterQuestionStatusName],
					[MasterQuestonStatusDescription]
				FROM
					[dbo].[MasterQuestionStatus] WITH (NOLOCK) 
				WHERE
										[MasterQuestionStatusID] = @MasterQuestionStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_GetByMasterQuestionStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestionStatus_GetByMasterQuestionStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_GetByMasterQuestionStatusID] TO [sp_executeall]
GO
