SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-20
-- Description:	If unwanted duplicate customer or lead records exist, you can use this proc to move all subordinate 
--              records (Partner, Lead, Case, Matter etc) to one Customer record from another.
-- 2010-09-21 JWG Added fnRefLetterFromCaseNum for leads with more than 26 matters
-- 2011-01-31 JWG Cater for LeadDocumentFS 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MergeCust]
(
	@FromCustomerID INT,
	@ToCustomerID INT,
	@CaseOrLead INT, --Lead=1 and Case = 2
	@ToLeadID INT = NULL
)
	
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @FromLeadID INT,
			@unid INT 

	DECLARE @LeadIDs TABLE(
			LeadID INT
	);

	DECLARE @t TABLE (
			caseid INT, 
			unid INT, 
			REF VARCHAR(10)
	);

	INSERT INTO @LeadIDs (LeadID)
	SELECT LeadID
	FROM Lead
	WHERE CustomerID = @FromCustomerID

	UPDATE Contact SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	
	IF (SELECT COUNT(*) FROM Partner WHERE CustomerID = @ToCustomerID) = 0
	BEGIN
		UPDATE Partner SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	END
	
	UPDATE PortalUser SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE DocumentQueue SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE DiaryAppointment SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE Work1 SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE Department SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	
	/* Move Lead to the new Customer */
	IF @CaseOrLead <> 2
	BEGIN
		UPDATE Lead SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	END
	
	UPDATE OutcomeDelayedEmails SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE CustomerQuestionnaires SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE Matter SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE CaseTransferMapping SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE CustomerOutcomes SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE DroppedOutCustomerQuestionnaires SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE PhoneNumberVerifications SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	UPDATE Bill SET CustomerID = @ToCustomerID WHERE CustomerID = @FromCustomerID
	
	/* Move Cases and all other tables below Lead level to a new Lead */
	IF @CaseOrLead = 2
	BEGIN

		UPDATE LeadDetailValues SET LeadID = @ToLeadID FROM LeadDetailValues INNER JOIN @LeadIDs li ON li.LeadID = LeadDetailValues.LeadID
		UPDATE PortalUserCase SET LeadID = @ToLeadID FROM PortalUserCase INNER JOIN @LeadIDs li ON li.LeadID = PortalUserCase.LeadID
		UPDATE DocumentQueue SET LeadID = @ToLeadID FROM DocumentQueue INNER JOIN @LeadIDs li ON li.LeadID = DocumentQueue.LeadID
		UPDATE DiaryAppointment SET LeadID = @ToLeadID FROM DiaryAppointment INNER JOIN @LeadIDs li ON li.LeadID = DiaryAppointment.LeadID
		UPDATE LeadEventThreadCompletion SET LeadID = @ToLeadID FROM LeadEventThreadCompletion INNER JOIN @LeadIDs li ON li.LeadID = LeadEventThreadCompletion.LeadID
		/* JWG 2012-07-12 Some old documents may exist in a read-only archive database, but LeadID is not stored in those databases, so this is not a problem */
		UPDATE LeadDocument SET LeadID = @ToLeadID FROM LeadDocument INNER JOIN @LeadIDs li ON li.LeadID = LeadDocument.LeadID
		UPDATE dbo.LeadDocumentFS SET LeadID = @ToLeadID FROM dbo.LeadDocumentFS INNER JOIN @LeadIDs li ON li.LeadID = LeadDocumentFS.LeadID
		UPDATE WorkflowTaskCompleted SET LeadID = @ToLeadID FROM WorkflowTaskCompleted INNER JOIN @LeadIDs li ON li.LeadID = WorkflowTaskCompleted.LeadID
		UPDATE Work1 SET LeadID = @ToLeadID FROM Work1 INNER JOIN @LeadIDs li ON li.LeadID = Work1.LeadID
		UPDATE WorkflowTask SET LeadID = @ToLeadID FROM WorkflowTask INNER JOIN @LeadIDs li ON li.LeadID = WorkflowTask.LeadID
		UPDATE Cases SET LeadID = @ToLeadID FROM Cases INNER JOIN @LeadIDs li ON li.LeadID = Cases.LeadID
		UPDATE SmsMessages SET LeadID = @ToLeadID FROM SmsMessages INNER JOIN @LeadIDs li ON li.LeadID = SmsMessages.LeadID
		UPDATE Matter SET LeadID = @ToLeadID FROM Matter INNER JOIN @LeadIDs li ON li.LeadID = Matter.LeadID
		UPDATE CaseTransferMapping SET LeadID = @ToLeadID FROM CaseTransferMapping INNER JOIN @LeadIDs li ON li.LeadID = CaseTransferMapping.LeadID
		UPDATE LeadViewHistory SET LeadID = @ToLeadID FROM LeadViewHistory INNER JOIN @LeadIDs li ON li.LeadID = LeadViewHistory.LeadID
		UPDATE TableRows SET LeadID = @ToLeadID FROM TableRows INNER JOIN @LeadIDs li ON li.LeadID = TableRows.LeadID
		UPDATE TableDetailValues SET LeadID = @ToLeadID FROM TableDetailValues INNER JOIN @LeadIDs li ON li.LeadID = TableDetailValues.LeadID
		UPDATE DetailValueHistory SET LeadID = @ToLeadID FROM DetailValueHistory INNER JOIN @LeadIDs li ON li.LeadID = DetailValueHistory.LeadID
		UPDATE ClientOfficeLead SET LeadID = @ToLeadID FROM ClientOfficeLead INNER JOIN @LeadIDs li ON li.LeadID = ClientOfficeLead.LeadID
		UPDATE Bill SET LeadID = @ToLeadID FROM Bill INNER JOIN @LeadIDs li ON li.LeadID = Bill.LeadID
		UPDATE LeadEvent SET LeadID = @ToLeadID FROM LeadEvent INNER JOIN @LeadIDs li ON li.LeadID = LeadEvent.LeadID
		UPDATE MatterDetailValues SET LeadID = @ToLeadID FROM MatterDetailValues INNER JOIN @LeadIDs li ON li.LeadID = MatterDetailValues.LeadID
		UPDATE TableRowPivot SET LeadID = @ToLeadID FROM TableRowPivot INNER JOIN @LeadIDs li ON li.LeadID = TableRowPivot.LeadID

	END

	SELECT @unid = 0

	INSERT INTO @t (caseid, unid, REF) 
	SELECT a.aid, a.rid, dbo.fnRefLetterFromCaseNum(a.rid)   
	FROM (
	SELECT CaseID AS aid, (ROW_NUMBER() OVER ( ORDER BY CaseID ) ) AS rid
	FROM dbo.Cases 
	WHERE LeadID = @ToLeadID
	) AS a

	UPDATE Matter
	SET RefLetter = REF
	FROM Matter
	INNER JOIN @t refs ON refs.caseid = Matter.CaseID

	UPDATE Cases
	SET CaseRef = 'Case ' + CONVERT(VARCHAR(100),unid), CaseNum = unid
	FROM Cases
	INNER JOIN @t refs ON refs.caseid = Cases.CaseID

	UPDATE Customers
	SET Test = 1
	WHERE CustomerID = @FromCustomerID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MergeCust] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MergeCust] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MergeCust] TO [sp_executeall]
GO
