SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-04
-- Description:	Calls the fnGetUserLeadCaseAccess function to get Cases for this Lead, 
--				paying attention to restricted Users/Groups
-- Updated      2012/08/07
--              Jan Wilson
--              Added column ProcessStartLeadEventID
-- =============================================
CREATE PROCEDURE [dbo].[Cases__GetByLeadIDAndUserID]
(
	@LeadID int,
	@UserID int   
)
AS


				SELECT
					c.[CaseID],
					c.[LeadID],
					c.[ClientID],
					c.[CaseNum],
					c.[CaseRef],
					c.[ClientStatusID],
					c.[AquariumStatusID],
					c.[DefaultContactID],
					c.[LatestLeadEventID],
					c.[LatestInProcessLeadEventID],
					c.[LatestOutOfProcessLeadEventID],
					c.[LatestNonNoteLeadEventID],
					c.[LatestNoteLeadEventID],
					c.[WhoCreated],
					c.[WhenCreated],
					c.[WhoModified],
					c.[WhenModified],
					c.[ProcessStartLeadEventID]
				FROM
					[dbo].[Cases] c WITH (NOLOCK)
				INNER JOIN dbo.fnGetUserLeadCaseAccess(@UserID, @LeadID) ca ON c.CaseID = ca.CaseID
				WHERE
					[LeadID] = @LeadID
				SELECT @@ROWCOUNT
					
			






GO
GRANT VIEW DEFINITION ON  [dbo].[Cases__GetByLeadIDAndUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases__GetByLeadIDAndUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases__GetByLeadIDAndUserID] TO [sp_executeall]
GO
