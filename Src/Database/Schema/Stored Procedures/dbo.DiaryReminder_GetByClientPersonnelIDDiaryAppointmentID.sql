SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DiaryReminder table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryReminder_GetByClientPersonnelIDDiaryAppointmentID]
(

	@ClientPersonnelID int   ,

	@DiaryAppointmentID int   
)
AS


				SELECT
					[DiaryReminderID],
					[ClientID],
					[DiaryAppointmentID],
					[ClientPersonnelID],
					[ReminderTimeshiftID],
					[ReminderDate]
				FROM
					[dbo].[DiaryReminder] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelID] = @ClientPersonnelID
					AND [DiaryAppointmentID] = @DiaryAppointmentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_GetByClientPersonnelIDDiaryAppointmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder_GetByClientPersonnelIDDiaryAppointmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_GetByClientPersonnelIDDiaryAppointmentID] TO [sp_executeall]
GO
