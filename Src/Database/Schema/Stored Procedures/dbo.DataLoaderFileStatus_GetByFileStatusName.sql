SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFileStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileStatus_GetByFileStatusName]
(

	@FileStatusName varchar (250)  
)
AS


				SELECT
					[DataLoaderFileStatusID],
					[FileStatusName],
					[FileStatusDescription]
				FROM
					[dbo].[DataLoaderFileStatus] WITH (NOLOCK) 
				WHERE
										[FileStatusName] = @FileStatusName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_GetByFileStatusName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileStatus_GetByFileStatusName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_GetByFileStatusName] TO [sp_executeall]
GO
