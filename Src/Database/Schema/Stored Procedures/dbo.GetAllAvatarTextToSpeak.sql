SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetAllAvatarTextToSpeak] @ClientQuestionnaireID int

AS

Select TextToSpeakID, ClientQuestionnaireID, ClientID, PageNumber, SpeakText, IsShown
From TextToSpeak
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllAvatarTextToSpeak] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAllAvatarTextToSpeak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllAvatarTextToSpeak] TO [sp_executeall]
GO
