SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 08/01/2014
-- Description:	Inserts a record into the pending account table
-- =============================================
CREATE PROCEDURE [dbo].[PendingAccount__InsertRecord]

	@ClientID INT, 
	@ClientPersonnelID INT, 
	@GUID VARCHAR(36), 
	@CreatedDate DateTime, 
	@LinkSentBy INT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO PendingAccount (ClientID, ClientPersonnelID, [GUID], CreatedDate, LinkSentBy)
	VALUES (@ClientID, @ClientPersonnelID, @GUID, @CreatedDate, @LinkSentBy)
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__InsertRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PendingAccount__InsertRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__InsertRecord] TO [sp_executeall]
GO
