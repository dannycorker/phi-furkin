SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-11
-- Description:	Get Hold-process events list
-- =============================================
CREATE PROCEDURE [dbo].[GetHoldProcessEventList] 
	@LeadTypeID int, 
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT EventTypeID, EventTypeName 
	FROM EventType INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON EventType.EventTypeID = fnEventTypeSecure.objectid
	WHERE LeadTypeID = @LeadTypeID 
	AND EventType.EventSubtypeID = 9
	AND EventType.Enabled = 1
	AND EventType.AvailableManually = 1
	ORDER BY EventTypeName

END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetHoldProcessEventList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetHoldProcessEventList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetHoldProcessEventList] TO [sp_executeall]
GO
