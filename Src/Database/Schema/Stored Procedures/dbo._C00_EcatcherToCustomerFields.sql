SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-07-22
-- Description:	Import eCatcher to Customer Fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_EcatcherToCustomerFields]
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TableRowID INT,
			@ClientQuestionnaireID INT,
			@CustomerQuestionnaireID INT, 
			@AddressLine1 VARCHAR(100),
			@AddressLine2 VARCHAR(100),
			@Town VARCHAR(100),
			@County VARCHAR(100),
			@Postcode VARCHAR(100),
			@Title VARCHAR(100),
			@TitleInt INT,
			@FirstName VARCHAR(100),
			@LastName VARCHAR(100),
			@DateOfBirth VARCHAR(2000),
			@Message VARCHAR(2000)

	/*
		This proc will import the missing values into customer 
		2 Parts, 
		1. To take the free text fields and copy them over
		2. To take the dropdown values and convert them to lookup list item id's
	*/
	
	/*Get the questionnaire ID as we will need this..*/
	SELECT @ClientQuestionnaireID = cq.ClientQuestionnaireID, @CustomerQuestionnaireID = cq.CustomerQuestionnaireID
	FROM CustomerQuestionnaires cq
	WHERE cq.CustomerID = @CustomerID

	SELECT @Message = 'ClientQuestionnaireID ' + CONVERT(VARCHAR,ISNULL(@ClientQuestionnaireID,0))
	
	/*If there is a table row in client 267 then get the detail that matched this questionnaire*/
	SELECT @TableRowID = tdv.TableRowID
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.DetailFieldID = 220432
	AND tdv.ValueInt = @ClientQuestionnaireID
	
	SELECT @Message = @Message + ' TableRowID ' + CONVERT(VARCHAR,ISNULL(@TableRowID,0))

	IF @TableRowID > 0
	BEGIN 
	
		/*We have a resource, therefor it's an Irish client and the address/partner details will need to be added manually..*/
		SELECT @AddressLine1 = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220433 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @AddressLine2 = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220434 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @Town = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220435 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @County = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220436 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @Postcode = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220437 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @Title = cpa.AnswerText
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN QuestionPossibleAnswers cpa with (nolock) on cpa.QuestionPossibleAnswerID = ca.QuestionPossibleAnswerID
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220438 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
		
		/*Get the title id from the title name if possible*/
		SELECT @TitleInt = t.TitleID
		FROM Titles t WITH (NOLOCK)
		WHERE t.Title = @Title
		
		SELECT @FirstName = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220439 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @LastName = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220440 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		SELECT @DateOfBirth = ca.Answer
		FROM CustomerAnswers ca with (nolock) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = 220441 AND tdv.ValueInt = ca.MasterQuestionID AND tdv.TableRowID = @TableRowID
		WHERE ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	
		/*Update the customers details*/
		UPDATE Customers
		SET Address1 = @AddressLine1, Address2 = @AddressLine2, Town = @Town, County = @County, PostCode = @Postcode
		FROM Customers 
		WHERE Customers.CustomerID = @CustomerID
		
		SELECT @Message = @Message + ' Address1 ' + ISNULL(@AddressLine1,'')

		IF @FirstName <> '' OR @LastName <> ''
		BEGIN
		
			/*Add partner if required*/
			INSERT INTO Partner (CustomerID, ClientID, UseCustomerAddress, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone, Address1, Address2, Town, County, PostCode, Occupation, Employer, DateOfBirth, CountryID)
			SELECT CustomerID, ClientID, 1, @TitleInt, @FirstName, '', @LastName, '', '', '', '', '', '', '', '', '', '', '', @DateOfBirth, CountryID
			FROM Customers c WITH (NOLOCK)
			WHERE c.CustomerID = @CustomerID
		
			SELECT @Message = @Message + ' Partner added '

		END
	
	END

	PRINT @Message

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_EcatcherToCustomerFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_EcatcherToCustomerFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_EcatcherToCustomerFields] TO [sp_executeall]
GO
