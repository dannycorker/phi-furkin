SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-08-01
-- Description:	Retries tasks that have failed with 
--              the 'Value cannot be null. Parameter name: connection' error
--              Separates runtime by @interval seconds
--				To include client add to @Client table below 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AutomatedEventQueue_Retry]


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Candidates TABLE (AEQID INT, RNO INT)

	DECLARE @Client TABLE (ClientID INT, StartDate DATE, IncludeInProcessEvents INT)

	DECLARE @Interval INT = 30 -- seconds between runtime for each row

	INSERT INTO @Client VALUES  -- clientID; date after which to look for AutomatedEventQueue records; include In Process events
	(241,'2014-07-27',1)

	INSERT INTO @Candidates
	SELECT aeq.AutomatedEventQueueID, ROW_NUMBER() OVER(PARTITION BY 1 ORDER BY aeq.AutomatedEventQueueID ASC) as [RNO] 
	FROM AutomatedEventQueue aeq WITH (NOLOCK)
	INNER JOIN @Client cl ON aeq.ClientID=cl.ClientID
	INNER JOIN EventType et WITH (NOLOCK) ON aeq.AutomatedEventTypeID=et.EventTypeID
	WHERE aeq.ClientID=cl.ClientID and (aeq.ErrorMessage like '%Deadlocked%' OR aeq.ErrorMessage like '%Value cannot be null%Parameter name: connection%')
	and aeq.WhenCreated > cl.StartDate
	and et.InProcess <= cl.IncludeInProcessEvents
	AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.EventTypeID=aeq.AutomatedEventTypeID and le.CaseID=aeq.CaseID and le.EventDeleted=0 and le.WhenCreated > aeq.SuccessDateTime)

	--SELECT *,DATEADD(SECOND,RNO*@Interval,dbo.fn_GetDate_Local()) FROM @Candidates order by RNO 

	UPDATE aeq 
	SET SchedulerID=NULL,ServerName=NULL,LockDateTime=NULL,ErrorMessage=NULL,ErrorDateTime=NULL,SuccessDateTime=NULL,EarliestRunDateTime=DATEADD(SECOND,c.RNO*@Interval,dbo.fn_GetDate_Local())
	FROM AutomatedEventQueue aeq
	INNER JOIN @Candidates c ON c.AEQID=aeq.AutomatedEventQueueID

	SELECT ''

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedEventQueue_Retry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AutomatedEventQueue_Retry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedEventQueue_Retry] TO [sp_executeall]
GO
