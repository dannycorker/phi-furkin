SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-05-24
-- Description:	Execute sql passed over a linked connection
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ExecuePassedInSQL]
	@SQL NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	EXEC (@SQL)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ExecuePassedInSQL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ExecuePassedInSQL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ExecuePassedInSQL] TO [sp_executeall]
GO
