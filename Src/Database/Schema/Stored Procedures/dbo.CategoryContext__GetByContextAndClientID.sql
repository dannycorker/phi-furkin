SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-10-2014
-- Description:	Gets the CategoryContext for the given context
-- =============================================
CREATE PROCEDURE [dbo].[CategoryContext__GetByContextAndClientID]

	@ClientID INT,
	@Context VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM CategoryContext cc WITH (NOLOCK) 
	WHERE @Context like '%' + cc.Context +'%' AND cc.ClientID=@ClientID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[CategoryContext__GetByContextAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CategoryContext__GetByContextAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CategoryContext__GetByContextAndClientID] TO [sp_executeall]
GO
