SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By:  ()
-- Purpose: Select records from the DetailFieldAlias table through a foreign key
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields__GetCustomerAliasedFieldsByLeadTypeID]
(
	@ClientID int,
	@LeadTypeID int
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					df.*,a.DetailFieldAlias
				FROM
					dbo.fnDetailFieldsShared(@ClientID) df
				INNER JOIN [dbo].[DetailFieldAlias] a on df.DetailFieldID = a.DetailFieldID
				WHERE
					df.[ClientID] = @ClientID
					AND df.[LeadTypeID] = @LeadTypeID
					AND a.[DetailFieldAlias] like 'Customer%'
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetCustomerAliasedFieldsByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetCustomerAliasedFieldsByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetCustomerAliasedFieldsByLeadTypeID] TO [sp_executeall]
GO
