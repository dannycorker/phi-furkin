SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Livingstone
-- Create date: 2019-07-17
-- Description:	Merge two Customers together
--				Soft Delete old customer
--				Always merge customer that was created second (@OldCustomerID) into the customer that was created first (@NewCustomerID). This way the correct marketing prefereces get maintained in accordance with
--				L&G business rules.
-- 2019-08-05 PL  Update the LeadID on the Collections Cases that get moved.
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[Support_Merge_Policies]
	@OldCustomerID INT,
	@NewCustomerID INT,
	@TicketNumber INT

AS
BEGIN

	DECLARE
		@Notes VARCHAR(2000) = CONCAT('Soft Deletion of Customer ',@OldCustomerID, ' as part of ticket number #',@TicketNumber),
		@DateForDeletion DATETIME = dbo.fn_GetDate_Local(),
		@NewCollectionsLead INT,
		@SMS INT,
		@Email INT,	
		@Post INT,
		@Phone INT,
		@WhoCreated INT = 58552,
		@CountryID INT

	DECLARE
		@AccountsToUpdate Table
		(
			AccountID INT,
			ClientID INT,
			OldCustomerID INT,
			NewCustomerID INT, 
			CollectionsMatterID INT,
			CollectionsCaseID INT,
			OldCollectionsLeadID INT,
			NewCollectionsLeadID INT
		)

	DECLARE 
		@CaseAndMatterToUpdate Table
		(
			LeadID INT,
			CaseID INT,
			CurrentCaseNum INT,
			NewCaseNum INT,
			MatterID INT,
			CurrentRefLetter VARCHAR(10),
			NewRefLetter VARCHAR(10)
		)

	BEGIN
		/*Move Lead to New Customer*/
		UPDATE l
		SET l.CustomerID = @NewCustomerID
		FROM Lead l
		WHERE l.CustomerID = @OldCustomerID
		AND l.LeadTypeID IN (1492, 1490)

		/*Move Collection Cases to New Customer's Collection Lead*/
		INSERT INTO @AccountsToUpdate (AccountID, ClientID, OldCustomerID, NewCustomerID, CollectionsMatterID)
		SELECT a.AccountID, a.ClientID, a.CustomerID, @NewCustomerID, a.ObjectID
		FROM Account a WITH (NOLOCK)
		WHERE a.CustomerID = @OldCustomerID

		/*GPR 2020-03-02 for AAG-149*/
		SELECT @CountryID = cl.CountryID FROM Customers c WITH (NOLOCK)
		INNER JOIN Clients cl WITH (NOLOCK) ON cl.ClientID = c.ClientID
		WHERE c.CustomerID = @OldCustomerID

		UPDATE atu
		SET atu.OldCollectionsLeadID = c.LeadID, atu.CollectionsCaseID = c.CaseID
		FROM @AccountsToUpdate atu
		INNER JOIN Matter m WITH (NOLOCK) ON atu.CollectionsMatterID = m.MatterID
		INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = m.CaseID
		WHERE atu.CollectionsMatterID = m.MatterID
		AND atu.OldCustomerID = @OldCustomerID

		UPDATE atu
		SET atu.NewCollectionsLeadID = l.LeadID
		FROM @AccountsToUpdate atu
		INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = atu.NewCustomerID AND l.LeadTypeID = 1493
		WHERE l.CustomerID = atu.NewCustomerID

		SELECT TOP 1 @NewCollectionsLead = atu.NewCollectionsLeadID
		FROM @AccountsToUpdate atu
		WHERE atu.NewCustomerID = @NewCustomerID

		UPDATE c
		SET c.LeadID = atu.NewCollectionsLeadID
		FROM Cases c WITH (NOLOCK)
		INNER JOIN @AccountsToUpdate atu ON atu.CollectionsCaseID = c.CaseID
		WHERE atu.CollectionsCaseID = c.CaseID

		/* 2019-08-05 PL  Update the LeadID on the Collections Cases that get moved.*/
		UPDATE le 
		SET le.LeadID = atu.NewCollectionsLeadID
		FROM LeadEvent le 
		INNER JOIN @AccountsToUpdate atu on atu.CollectionsCaseID = le.CaseID
		WHERE atu.CollectionsCaseID = le.CaseID
		AND atu.OldCollectionsLeadID = le.LeadID

		UPDATE m
		SET m.LeadID = atu.NewCollectionsLeadID
		FROM Matter m WITH (NOLOCK)
		INNER JOIN @AccountsToUpdate atu ON m.CaseID = atu.CollectionsCaseID
		WHERE m.LeadID = atu.OldCollectionsLeadID

		/*Move Purchased Product to New Customer*/
		UPDATE pp
		SET pp.CustomerID = @NewCustomerID
		FROM PurchasedProduct pp
		WHERE pp.CustomerID = @OldCustomerID

		/*Move Purchased Product History to New Customer*/
		UPDATE pph
		SET pph.CustomerID = @NewCustomerID
		FROM PurchasedProductHistory pph
		WHERE pph.CustomerID = @OldCustomerID

		/*Move Purchased Product Payment Schedule to New Customer*/
		UPDATE ppps
		SET ppps.CustomerID = @NewCustomerID
		FROM PurchasedProductPaymentSchedule ppps
		WHERE ppps.CustomerID = @OldCustomerID

		/*Move Purchased Product Payment Schedule History to New Customer*/
		UPDATE pppsh
		SET pppsh.CustomerID = @NewCustomerID
		FROM PurchasedProductPaymentScheduleHistory pppsh
		WHERE pppsh.CustomerID = @OldCustomerID

		/*Move Customer Payment Schedule to New Customer*/
		UPDATE cps
		SET cps.CustomerID = @NewCustomerID
		FROM CustomerPaymentSchedule cps
		WHERE cps.CustomerID = @OldCustomerID

		/*Move Customer Payment Schedule History to New Customer*/
		UPDATE cpsh
		SET cpsh.CustomerID = @NewCustomerID
		FROM CustomerPaymentScheduleHistory cpsh
		WHERE cpsh.CustomerID = @OldCustomerID

		/*Move Customer Ledger to New Customer*/
		UPDATE cl
		SET cl.CustomerID = @NewCustomerID
		FROM CustomerLedger cl
		WHERE cl.CustomerID = @OldCustomerID

		/*Move Payment Table to New Customer*/
		UPDATE p
		SET p.CustomerID = @NewCustomerID
		FROM Payment p
		WHERE p.CustomerID = @OldCustomerID

		/*Move Accounts to New Customer*/
		UPDATE a
		SET a.CustomerID = @NewCustomerID
		FROM Account a
		WHERE a.CustomerID = @OldCustomerID

		/*GPR 2020-03-02 for AAG-202*/
		IF @CountryID <> 14 /*Australia*/
		BEGIN
			/*Move any Account Mandates to New Customer*/
			UPDATE am
			SET am.CustomerID = @NewCustomerID
			FROM AccountMandate am
			WHERE am.CustomerID = @OldCustomerID
		END

		/*Move Accounts History to New Customer*/
		UPDATE ah
		SET ah.CustomerID = @NewCustomerID
		FROM AccountHistory ah
		WHERE ah.CustomerID = @OldCustomerID

		/*Clean Data Up*/

		/*Update Marketing Preferences*/
		SELECT
			@SMS = dbo.fnGetSimpleDvAsInt(177152,@OldCustomerID),
			@Email = dbo.fnGetSimpleDvAsInt(177153,@OldCustomerID),
			@Post = dbo.fnGetSimpleDvAsInt(177154,@OldCustomerID),
			@Phone = dbo.fnGetSimpleDvAsInt(177371,@OldCustomerID)

		EXEC _C00_SimpleValueIntoField 177152, @SMS, @NewCustomerID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 177153, @Email, @NewCustomerID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 177154, @Post, @NewCustomerID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 177371, @Phone, @NewCustomerID, @WhoCreated

		/*Update the CaseNum and RefLetter on the New Customer Collections Lead*/
		;WITH myCases AS
		(
			SELECT c.CaseID, ROW_NUMBER () OVER(PARTITION BY (LeadID) ORDER BY (CaseID)) AS [RN]
			FROM Cases c WITH (NOLOCK)
			WHERE c.LeadID = @NewCollectionsLead
		)

		INSERT INTO @CaseAndMatterToUpdate (LeadID, CaseID, CurrentCaseNum, NewCaseNum, MatterID, CurrentRefLetter, NewRefLetter)
		SELECT c.LeadID, c.CaseID, c.CaseNum, REPLACE(c.CaseNum, c.CaseNum, mc.[RN]) [NewCaseNum], m.MatterID, m.RefLetter, dbo.fnRefLetterFromCaseNum(mC.RN) [NewMatterRef]
		FROM Cases c WITH (NOLOCK)
		INNER JOIN myCases mC ON mC.CaseID = c.CaseID
		INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID
		WHERE c.LeadID = @NewCollectionsLead

		UPDATE c
		SET c.CaseNum = camtu.NewCaseNum, c.CaseRef = CONCAT('Case ',camtu.NewCaseNum)
		FROM Cases c
		INNER JOIN @CaseAndMatterToUpdate camtu ON camtu.LeadID = c.LeadID
		WHERE c.CaseID = camtu.CaseID

		UPDATE m
		SET m.RefLetter = camtu.NewRefLetter
		FROM Matter m
		INNER JOIN @CaseAndMatterToUpdate camtu ON camtu.LeadID = m.LeadID
		WHERE m.MatterID = camtu.MatterID

		--/*Soft Delete Old Customer*/
		--EXEC SoftDeleteDataForDPA @OldCustomerID, 10, @DateForDeletion, @Notes, 0

		UPDATE Customers
		SET Test=1
		WHERE CustomerID = @OldCustomerID

	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Support_Merge_Policies] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Support_Merge_Policies] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Support_Merge_Policies] TO [sp_executeall]
GO
