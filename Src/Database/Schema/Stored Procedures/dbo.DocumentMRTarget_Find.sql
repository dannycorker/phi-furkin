SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DocumentMRTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentMRTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@DocumentMRTargetID int   = null ,

	@ClientID int   = null ,

	@DocumentTypeID int   = null ,

	@Target varchar (250)  = null ,

	@DetailFieldID int   = null ,

	@ColumnField int   = null ,

	@TemplateTypeID int   = null ,

	@DetailFieldAlias varchar (500)  = null ,

	@ColumnFieldAlias varchar (500)  = null ,

	@ObjectName varchar (250)  = null ,

	@PropertyName varchar (250)  = null ,

	@Notes varchar (250)  = null ,

	@DocumentTypeVersionID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DocumentMRTargetID]
	, [ClientID]
	, [DocumentTypeID]
	, [Target]
	, [DetailFieldID]
	, [ColumnField]
	, [TemplateTypeID]
	, [DetailFieldAlias]
	, [ColumnFieldAlias]
	, [ObjectName]
	, [PropertyName]
	, [Notes]
	, [DocumentTypeVersionID]
    FROM
	[dbo].[DocumentMRTarget] WITH (NOLOCK) 
    WHERE 
	 ([DocumentMRTargetID] = @DocumentMRTargetID OR @DocumentMRTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ColumnField] = @ColumnField OR @ColumnField IS NULL)
	AND ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([DetailFieldAlias] = @DetailFieldAlias OR @DetailFieldAlias IS NULL)
	AND ([ColumnFieldAlias] = @ColumnFieldAlias OR @ColumnFieldAlias IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([DocumentTypeVersionID] = @DocumentTypeVersionID OR @DocumentTypeVersionID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DocumentMRTargetID]
	, [ClientID]
	, [DocumentTypeID]
	, [Target]
	, [DetailFieldID]
	, [ColumnField]
	, [TemplateTypeID]
	, [DetailFieldAlias]
	, [ColumnFieldAlias]
	, [ObjectName]
	, [PropertyName]
	, [Notes]
	, [DocumentTypeVersionID]
    FROM
	[dbo].[DocumentMRTarget] WITH (NOLOCK) 
    WHERE 
	 ([DocumentMRTargetID] = @DocumentMRTargetID AND @DocumentMRTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ColumnField] = @ColumnField AND @ColumnField is not null)
	OR ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([DetailFieldAlias] = @DetailFieldAlias AND @DetailFieldAlias is not null)
	OR ([ColumnFieldAlias] = @ColumnFieldAlias AND @ColumnFieldAlias is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([DocumentTypeVersionID] = @DocumentTypeVersionID AND @DocumentTypeVersionID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_Find] TO [sp_executeall]
GO
