SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EquationTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_GetByObjectName]
(

	@ObjectName varchar (250)  
)
AS


				SELECT
					[EquationTargetID],
					[ClientID],
					[EquationDetailFieldID],
					[DetailFieldID],
					[Target],
					[ObjectName],
					[PropertyName],
					[SqlFunctionID],
					[IsEquation],
					[IsColumnSum],
					[ColumnFieldID],
					[DetailFieldSubTypeID]
				FROM
					[dbo].[EquationTarget] WITH (NOLOCK) 
				WHERE
										[ObjectName] = @ObjectName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_GetByObjectName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_GetByObjectName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_GetByObjectName] TO [sp_executeall]
GO
