SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Predicate Rule Delete
-- =============================================
CREATE PROCEDURE [dbo].[PredicateRuleAssigned__Delete] 
	@PredicateRuleAssignedID INT, 
	@ClientID INT
AS
BEGIN

	DELETE
	FROM	PredicateRuleAssigned
	WHERE	PredicateRuleAssignedID = @PredicateRuleAssignedID
	AND		ClientID = @ClientID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRuleAssigned__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PredicateRuleAssigned__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRuleAssigned__Delete] TO [sp_executeall]
GO
