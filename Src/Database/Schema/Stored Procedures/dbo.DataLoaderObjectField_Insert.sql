SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderObjectField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_Insert]
(

	@DataLoaderObjectFieldID int   ,

	@DataLoaderObjectTypeID int   ,

	@FieldName varchar (250)  ,

	@DataTypeID int   ,

	@IsMandatory bit   ,

	@IsNormalisedField bit   ,

	@IsABitField bit   ,

	@IsClientOwned bit   ,

	@IsUniqueTableID bit   ,

	@IsWriteOnce bit   ,

	@HelperText varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderObjectField]
					(
					[DataLoaderObjectFieldID]
					,[DataLoaderObjectTypeID]
					,[FieldName]
					,[DataTypeID]
					,[IsMandatory]
					,[IsNormalisedField]
					,[IsABitField]
					,[IsClientOwned]
					,[IsUniqueTableID]
					,[IsWriteOnce]
					,[HelperText]
					)
				VALUES
					(
					@DataLoaderObjectFieldID
					,@DataLoaderObjectTypeID
					,@FieldName
					,@DataTypeID
					,@IsMandatory
					,@IsNormalisedField
					,@IsABitField
					,@IsClientOwned
					,@IsUniqueTableID
					,@IsWriteOnce
					,@HelperText
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Insert] TO [sp_executeall]
GO
