SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the GroupAndUserEditingControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupAndUserEditingControl_Delete]
(

	@EditingID int   
)
AS


				DELETE FROM [dbo].[GroupAndUserEditingControl] WITH (ROWLOCK) 
				WHERE
					[EditingID] = @EditingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupAndUserEditingControl_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Delete] TO [sp_executeall]
GO
