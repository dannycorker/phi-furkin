SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventType_Update]
(

	@RPIEventTypeID int   ,

	@OriginalRPIEventTypeID int   ,

	@RPIEventTypeName varchar (50)  ,

	@RPIEventTypeDescription varchar (500)  ,

	@IsAquariumTriggered bit   ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIEventType]
				SET
					[RPIEventTypeID] = @RPIEventTypeID
					,[RPIEventTypeName] = @RPIEventTypeName
					,[RPIEventTypeDescription] = @RPIEventTypeDescription
					,[IsAquariumTriggered] = @IsAquariumTriggered
					,[IsEnabled] = @IsEnabled
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[RPIEventTypeID] = @OriginalRPIEventTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Update] TO [sp_executeall]
GO
