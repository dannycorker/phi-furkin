SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new Case and Matter for a Lead 
-- 2010-04-19 JWG Added WhoCreated
-- 2010-09-21 JWG Added fnRefLetterFromCaseNum for leads with more than 26 matters
-- 2011-06-06 ACE Nolocked SP
-- 2015-12-07 ACE Added who and when modified and when created / updated
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateNewCaseForLead]
(
	@LeadID int, 
	@WhoCreated int,
	@AddMatter bit = 1
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare	@myERROR int,
			@NewMatterID int,
			@NewLeadID int,
			@NewCaseID int,
			@CaseNum int,
			@RefLetter varchar(3),
			@ClientStatusID int,
			@AquariumStatusID int,
			@MatterStatus int,
			@DefaultContactID varchar(100),
			@CustomerID int,
			@LeadTypeID int,
			@ClientID int 

	Select top 1 @CaseNum = CaseNum, 
		/* @ClientStatusID = Cases.ClientStatusID, Commented our by JG and ES 2010-08-06 as this was taking the status of the previous case and applying to the new case */
		@AquariumStatusID = Cases.AquariumStatusID,
		@MatterStatus = Matter.MatterStatus,
		@DefaultContactID = DefaultContactID,
		@CustomerID = Matter.CustomerID,
		@LeadTypeID = Lead.LeadTypeID,
		@ClientID = Lead.ClientID
	From Cases WITH (NOLOCK)
	Inner Join Matter WITH (NOLOCK)On Matter.CaseID = Cases.CaseID
	Inner Join Lead WITH (NOLOCK)on Lead.LeadID = Cases.LeadID
	Where Cases.LeadID = @LeadID
	Order By Cases.CaseID Desc

	/* Create Case*/
	INSERT INTO Cases (LeadID,ClientID,CaseNum,CaseRef,ClientStatusID,AquariumStatusID,DefaultContactID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@LeadID,@ClientID,@CaseNum + 1,'Case ' + convert(varchar,(@CaseNum + 1)),@ClientStatusID,@AquariumStatusID,@DefaultContactID, @WhoCreated, dbo.fn_GetDate_Local(), @WhoCreated, dbo.fn_GetDate_Local())

	Select @NewCaseID = SCOPE_IDENTITY()


	IF @AddMatter = 1
	BEGIN
		select @RefLetter = dbo.fnRefLetterFromCaseNum(1+COUNT(*)) 
		from matter WITH (NOLOCK)
		where leadid = @LeadID

		/* Create Matter */
		INSERT INTO Matter([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID], WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@ClientID,'',@CustomerID,@LeadID,@MatterStatus,@RefLetter,0,@NewCaseID, @WhoCreated, dbo.fn_GetDate_Local(), @WhoCreated, dbo.fn_GetDate_Local())
	
		Select @NewMatterID = SCOPE_IDENTITY()

		Return @NewMatterID
	END
	ELSE
	BEGIN
		RETURN @NewCaseID
	END
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewCaseForLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateNewCaseForLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewCaseForLead] TO [sp_executeall]
GO
