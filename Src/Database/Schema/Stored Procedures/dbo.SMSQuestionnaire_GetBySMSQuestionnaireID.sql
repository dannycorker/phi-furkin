SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestionnaire table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_GetBySMSQuestionnaireID]
(

	@SMSQuestionnaireID int   
)
AS


				SELECT
					[SMSQuestionnaireID],
					[ClientID],
					[Title],
					[Description],
					[Enabled],
					[ToTarget],
					[CreateCustomerIfNotFound],
					[UseSentimentAnalysis],
					[RuleSetID],
					[IsVoice],
					[Timeout],
					[Repeat],
					[RepeatPrefix]
				FROM
					[dbo].[SMSQuestionnaire] WITH (NOLOCK) 
				WHERE
										[SMSQuestionnaireID] = @SMSQuestionnaireID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_GetBySMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
