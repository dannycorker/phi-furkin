SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptLock table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_Get_List]

AS


				
				SELECT
					[ScriptLockID],
					[ScriptID],
					[ClientID],
					[WhoIsEditing],
					[EditStartedAt]
				FROM
					[dbo].[ScriptLock] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Get_List] TO [sp_executeall]
GO
