SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ActiveSession table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_GetByThirdPartySystemId]
(

	@ThirdPartySystemId int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SessionID],
					[EmailAddress],
					[ThirdPartySystemId]
				FROM
					[dbo].[ActiveSession] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemId] = @ThirdPartySystemId
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetByThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_GetByThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetByThirdPartySystemId] TO [sp_executeall]
GO
