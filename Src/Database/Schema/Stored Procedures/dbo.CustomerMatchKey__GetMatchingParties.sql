SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-30
-- Description:	Get all parties with matching keys
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMatchKey__GetMatchingParties]
	@ClientID INT,	
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250),
	@HasPartner bit = 0,
	@PFirstName varchar(250) = NULL,
	@PLastName varchar(250) = NULL,
	@PAddress1 varchar(250) = NULL,
	@PPostcode varchar(250) = NULL,
	@PDateOfBirth varchar(250) = NULL,
	@JointSingle INT = NULL
AS
BEGIN

	WITH pmk as (
		SELECT *, CASE WHEN p.FirstName = @FirstName THEN 1 ELSE 0 END as [FNameMatch],
				CASE WHEN p.LastName = @LastName THEN 1 ELSE 0 END as [LNameMatch],
				CASE WHEN p.Address1 = @Address1 THEN 1 ELSE 0 END as [A1Match],
				CASE WHEN p.Postcode = @PostCode THEN 1 ELSE 0 END as [PostcodeMatch],
				CASE WHEN p.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [DobMatch],
				CASE WHEN p.FirstName = @FirstName THEN 1 ELSE 0 END +
				CASE WHEN p.LastName = @LastName THEN 1 ELSE 0 END +
				CASE WHEN p.Address1 = @Address1 THEN 1 ELSE 0 END +
				CASE WHEN p.Postcode = @PostCode THEN 1 ELSE 0 END +
				CASE WHEN p.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [TotalMatch]
		FROM PartnerMatchKey p WITH (NOLOCK)
		WHERE p.ClientID = @ClientID
		AND (
			p.FirstName = @FirstName
			OR
			p.LastName = @LastName
			OR 
			p.Address1 = @Address1
			OR
			p.Postcode = @PostCode
			OR
			p.DateOfBirth = @DateOfBirth
			)
	),
	pmkc as (
		SELECT *, 
				CASE WHEN p.FirstName = @PFirstName THEN 1 ELSE 0 END +
				CASE WHEN p.LastName = @PLastName THEN 1 ELSE 0 END +
				CASE WHEN p.Address1 = @PAddress1 THEN 1 ELSE 0 END +
				CASE WHEN p.Postcode = @PPostCode THEN 1 ELSE 0 END +
				CASE WHEN p.DateOfBirth = @PDateOfBirth THEN 1 ELSE 0 END as [TotalMatch]
		FROM PartnerMatchKey p WITH (NOLOCK)
		WHERE p.ClientID = @ClientID
		AND @HasPartner = 1
		AND (
			p.FirstName = @PFirstName
			OR
			p.LastName = @PLastName
			OR 
			p.Address1 = @PAddress1
			OR
			p.Postcode = @PPostCode
			OR
			p.DateOfBirth = @PDateOfBirth
			)
	),
	cmk as (
		SELECT *, CASE WHEN c.FirstName = @FirstName THEN 1 ELSE 0 END as [FNameMatch],
				CASE WHEN c.LastName = @LastName THEN 1 ELSE 0 END as [LNameMatch],
				CASE WHEN c.Address1 = @Address1 THEN 1 ELSE 0 END as [A1Match],
				CASE WHEN c.Postcode = @PostCode THEN 1 ELSE 0 END as [PostcodeMatch],
				CASE WHEN c.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [DobMatch],
				CASE WHEN c.FirstName = @FirstName THEN 1 ELSE 0 END +
				CASE WHEN c.LastName = @LastName THEN 1 ELSE 0 END +
				CASE WHEN c.Address1 = @Address1 THEN 1 ELSE 0 END +
				CASE WHEN c.Postcode = @PostCode THEN 1 ELSE 0 END +
				CASE WHEN c.DateOfBirth = @DateOfBirth THEN 1 ELSE 0 END as [TotalMatch]
		FROM CustomerMatchKey c WITH (NOLOCK)
		WHERE c.ClientID = @ClientID
		AND (
			c.FirstName = @FirstName
			OR
			c.LastName = @LastName
			OR 
			c.Address1 = @Address1
			OR
			c.Postcode = @PostCode
			OR
			c.DateOfBirth = @DateOfBirth
			)
	),
	cmkc as (
		SELECT *, 
				CASE WHEN c.FirstName = @PFirstName THEN 1 ELSE 0 END +
				CASE WHEN c.LastName = @PLastName THEN 1 ELSE 0 END +
				CASE WHEN c.Address1 = @PAddress1 THEN 1 ELSE 0 END +
				CASE WHEN c.Postcode = @PPostCode THEN 1 ELSE 0 END +
				CASE WHEN c.DateOfBirth = @PDateOfBirth THEN 1 ELSE 0 END as [TotalMatch]
		FROM CustomerMatchKey c WITH (NOLOCK)
		WHERE c.ClientID = @ClientID
		AND @HasPartner = 1
		AND (
			c.FirstName = @PFirstName
			OR
			c.LastName = @PLastName
			OR 
			c.Address1 = @PAddress1
			OR
			c.Postcode = @PPostCode
			OR
			c.DateOfBirth = @PDateOfBirth
			)
	)

	SELECT 
		p.ClientID,
		c.CustomerID,
		p.PartnerID,
		p.FirstName, 
		p.LastName, 
		CASE p.UseCustomerAddress WHEN 1 THEN c.Address1 ELSE p.Address1 END Address1,
		CASE p.UseCustomerAddress WHEN 1 THEN c.PostCode ELSE p.Postcode END Postcode,
		c.Fullname + CASE WHEN p.FullName IS NULL THEN '' ELSE ' and ' + p.FullName END as [FullName],
		p.EmailAddress,
		p.HomeTelephone,
		p.MobileTelephone,
		p.Occupation,
		p.DateOfBirth,
		pmk.Firstname FirstnameKey, 
		pmk.Lastname LastnameKey, 
		pmk.Address1 Address1Key, 
		pmk.Postcode PostcodeKey,
		pmk.TotalMatch,
		'P' as [PartnerOrCustomer]
	FROM pmk WITH (NOLOCK)
	INNER JOIN dbo.Partner p WITH (NOLOCK) on p.PartnerID = pmk.PartnerID
	INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = p.CustomerID AND c.Test = 0
	WHERE pmk.TotalMatch > 3

	UNION

	SELECT 	c.ClientID,
			c.CustomerID,		 
			NULL as [PartnerID],
			c.FirstName, 
			c.LastName, 
			c.Address1, 
			c.PostCode,
			c.Fullname + CASE WHEN p.FullName IS NULL THEN '' ELSE ' and ' + p.FullName END as [FullName],
			c.EmailAddress,
			c.HomeTelephone,
			c.MobileTelephone,
			c.Occupation,
			c.DateOfBirth,
			cmk.Firstname FirstnameKey, 
			cmk.Lastname LastnameKey, 
			cmk.Address1 Address1Key, 
			cmk.Postcode PostcodeKey,
			cmk.TotalMatch,
			'C' as [PartnerOrCustomer]
	FROM cmk
	INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = cmk.CustomerID AND c.Test = 0
	LEFT JOIN dbo.Partner p WITH (NOLOCK) on p.CustomerID = c.CustomerID
	WHERE cmk.TotalMatch > 3

	UNION

	SELECT 
		p.ClientID,
		c.CustomerID,
		p.PartnerID,
		p.FirstName, 
		p.LastName, 
		CASE p.UseCustomerAddress WHEN 1 THEN c.Address1 ELSE p.Address1 END Address1,
		CASE p.UseCustomerAddress WHEN 1 THEN c.PostCode ELSE p.Postcode END Postcode,
		c.Fullname + CASE WHEN p.FullName IS NULL THEN '' ELSE ' and ' + p.FullName END as [FullName],
		p.EmailAddress,
		p.HomeTelephone,
		p.MobileTelephone,
		p.Occupation,
		p.DateOfBirth,
		pmkc.Firstname FirstnameKey, 
		pmkc.Lastname LastnameKey, 
		pmkc.Address1 Address1Key, 
		pmkc.Postcode PostcodeKey,
		pmkc.TotalMatch,
		'P' as [PartnerOrCustomer]
	FROM pmkc WITH (NOLOCK)
	INNER JOIN dbo.Partner p WITH (NOLOCK) on p.PartnerID = pmkc.PartnerID
	INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = p.CustomerID AND c.Test = 0
	WHERE pmkc.TotalMatch > 3

	UNION

	SELECT 	c.ClientID,
			c.CustomerID,		 
			NULL as [PartnerID],
			c.FirstName, 
			c.LastName, 
			c.Address1, 
			c.PostCode,
			c.Fullname + CASE WHEN p.FullName IS NULL THEN '' ELSE ' and ' + p.FullName END as [FullName],
			c.EmailAddress,
			c.HomeTelephone,
			c.MobileTelephone,
			c.Occupation,
			c.DateOfBirth,
			cmkc.Firstname FirstnameKey, 
			cmkc.Lastname LastnameKey, 
			cmkc.Address1 Address1Key, 
			cmkc.Postcode PostcodeKey,
			cmkc.TotalMatch,
			'C' as [PartnerOrCustomer]
	FROM cmkc
	INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = cmkc.CustomerID AND c.Test = 0
	LEFT JOIN dbo.Partner p WITH (NOLOCK) on p.CustomerID = c.CustomerID
	WHERE cmkc.TotalMatch > 3

	ORDER BY PartnerOrCustomer

END



GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetMatchingParties] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMatchKey__GetMatchingParties] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetMatchingParties] TO [sp_executeall]
GO
