SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ExternalApplicationLogin table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_Delete]
(

	@ExternalApplicationLoginID int   
)
AS


				DELETE FROM [dbo].[ExternalApplicationLogin] WITH (ROWLOCK) 
				WHERE
					[ExternalApplicationLoginID] = @ExternalApplicationLoginID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Delete] TO [sp_executeall]
GO
