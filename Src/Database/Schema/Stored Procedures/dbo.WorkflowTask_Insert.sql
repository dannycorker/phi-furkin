SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the WorkflowTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTask_Insert]
(

	@WorkflowTaskID int    OUTPUT,

	@WorkflowGroupID int   ,

	@AutomatedTaskID int   ,

	@Priority int   ,

	@AssignedTo int   ,

	@AssignedDate datetime   ,

	@LeadID int   ,

	@CaseID int   ,

	@EventTypeID int   ,

	@ClientID int   ,

	@FollowUp bit   ,

	@Important bit   ,

	@CreationDate datetime   ,

	@Escalated bit   ,

	@EscalatedBy int   ,

	@EscalationReason varchar (255)  ,

	@EscalationDate datetime   ,

	@Disabled bit   ,

	@DisabledReason varchar (255)  ,

	@DisabledDate datetime   
)
AS


				
				INSERT INTO [dbo].[WorkflowTask]
					(
					[WorkflowGroupID]
					,[AutomatedTaskID]
					,[Priority]
					,[AssignedTo]
					,[AssignedDate]
					,[LeadID]
					,[CaseID]
					,[EventTypeID]
					,[ClientID]
					,[FollowUp]
					,[Important]
					,[CreationDate]
					,[Escalated]
					,[EscalatedBy]
					,[EscalationReason]
					,[EscalationDate]
					,[Disabled]
					,[DisabledReason]
					,[DisabledDate]
					)
				VALUES
					(
					@WorkflowGroupID
					,@AutomatedTaskID
					,@Priority
					,@AssignedTo
					,@AssignedDate
					,@LeadID
					,@CaseID
					,@EventTypeID
					,@ClientID
					,@FollowUp
					,@Important
					,@CreationDate
					,@Escalated
					,@EscalatedBy
					,@EscalationReason
					,@EscalationDate
					,@Disabled
					,@DisabledReason
					,@DisabledDate
					)
				-- Get the identity value
				SET @WorkflowTaskID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Insert] TO [sp_executeall]
GO
