SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Titles table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Titles_Update]
(

	@TitleID int   ,

	@Title varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Titles]
				SET
					[Title] = @Title
				WHERE
[TitleID] = @TitleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Titles_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Update] TO [sp_executeall]
GO
