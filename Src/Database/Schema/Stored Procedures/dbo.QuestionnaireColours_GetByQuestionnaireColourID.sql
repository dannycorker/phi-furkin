SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireColours table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_GetByQuestionnaireColourID]
(

	@QuestionnaireColourID int   
)
AS


				SELECT
					[QuestionnaireColourID],
					[ClientQuestionnaireID],
					[Page],
					[Header],
					[Intro],
					[Body],
					[Footer],
					[ClientID]
				FROM
					[dbo].[QuestionnaireColours] WITH (NOLOCK) 
				WHERE
										[QuestionnaireColourID] = @QuestionnaireColourID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_GetByQuestionnaireColourID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_GetByQuestionnaireColourID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_GetByQuestionnaireColourID] TO [sp_executeall]
GO
