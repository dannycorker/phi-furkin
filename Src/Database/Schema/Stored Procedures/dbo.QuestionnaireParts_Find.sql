SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuestionnaireParts table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireParts_Find]
(

	@SearchUsingOR bit   = null ,

	@PartNameID int   = null ,

	@PartName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PartNameID]
	, [PartName]
    FROM
	[dbo].[QuestionnaireParts] WITH (NOLOCK) 
    WHERE 
	 ([PartNameID] = @PartNameID OR @PartNameID IS NULL)
	AND ([PartName] = @PartName OR @PartName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PartNameID]
	, [PartName]
    FROM
	[dbo].[QuestionnaireParts] WITH (NOLOCK) 
    WHERE 
	 ([PartNameID] = @PartNameID AND @PartNameID is not null)
	OR ([PartName] = @PartName AND @PartName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireParts_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Find] TO [sp_executeall]
GO
