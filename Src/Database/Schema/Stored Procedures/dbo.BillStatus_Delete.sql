SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the BillStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillStatus_Delete]
(

	@BillStatusID int   
)
AS


				DELETE FROM [dbo].[BillStatus] WITH (ROWLOCK) 
				WHERE
					[BillStatusID] = @BillStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Delete] TO [sp_executeall]
GO
