SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetSiteMap    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetSiteMap]

As

Select SiteMapID, Title, SiteMap.Description, ParentID , Secured
from SiteMap




GO
GRANT VIEW DEFINITION ON  [dbo].[GetSiteMap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSiteMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSiteMap] TO [sp_executeall]
GO
