SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the BillStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillStatus_GetByName]
(

	@Name varchar (50)  
)
AS


				SELECT
					[BillStatusID],
					[Name],
					[Description]
				FROM
					[dbo].[BillStatus] WITH (NOLOCK) 
				WHERE
										[Name] = @Name
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_GetByName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillStatus_GetByName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_GetByName] TO [sp_executeall]
GO
