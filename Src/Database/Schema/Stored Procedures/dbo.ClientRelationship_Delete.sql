SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_Delete]
(

	@ClientRelationshipID int   
)
AS


				DELETE FROM [dbo].[ClientRelationship] WITH (ROWLOCK) 
				WHERE
					[ClientRelationshipID] = @ClientRelationshipID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Delete] TO [sp_executeall]
GO
