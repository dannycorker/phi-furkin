SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2007-08-17
-- Description:	List missing mandatory fields for a Lead and Case
-- Modified: 2010-12-2, Paul Richardson
-- Modified Description: Added Case and Customer Detail Values
-- =============================================
CREATE PROCEDURE [dbo].[GetMissingMandatoryFields__oldVersion]  
	-- Add the parameters for the stored procedure here
	@EventTypeID int, 
	@LeadID int, 
	@CaseID int, 
	@CustomerID int = null,
	@ClientID int = null	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	-- List all missing mandatory Lead and Matter fields
	-- Normal values must not be blank
	-- ResourceList Values must not be zero
	-- In the second part of the UNION, we need Matter for the result set, 
	-- but it's not linked to the other tables. It is filtered by the 
	-- parameters @LeadID and @CaseID in the WHERE clause below.	
	SELECT 'Client Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize]
	FROM dbo.EventTypeMandatoryField etmf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE etmf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 12
	AND df.Enabled = 1 
	AND NOT EXISTS(
		SELECT * 
		FROM dbo.ClientDetailValues cldv (nolock) 
		WHERE cldv.ClientID = @ClientID 
		AND cldv.DetailFieldID = df.DetailFieldID 
		AND ((df.QuestionTypeID = 4 AND cldv.DetailValue <> '0' AND cldv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND cldv.DetailValue <> '0' AND cldv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND cldv.DetailValue <> ''))
	)			
	UNION ALL	
	SELECT 'Client Personnel Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize]
	FROM dbo.EventTypeMandatoryField cpdmf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON cpdmf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE cpdmf.EventTypeID = 40244
	AND df.LeadOrMatter = 13
	AND df.Enabled = 1 
	AND NOT EXISTS(
		SELECT * 
		FROM dbo.ClientPersonnelDetailValues clpdv (nolock) 
		WHERE clpdv.ClientID = 2 and clpdv.ClientPersonnelID=4
		AND clpdv.DetailFieldID = df.DetailFieldID 
		AND ((df.QuestionTypeID = 4 AND clpdv.DetailValue <> '0' AND clpdv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND clpdv.DetailValue <> '0' AND clpdv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND clpdv.DetailValue <> ''))
	)	
	UNION ALL
	SELECT 'Lead Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize]
	FROM dbo.EventTypeMandatoryField etmf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE etmf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 1
	AND df.Enabled = 1 
	AND NOT EXISTS(
		SELECT * 
		FROM dbo.LeadDetailValues ldv (nolock) 
		WHERE ldv.LeadID = @LeadID 
		AND ldv.DetailFieldID = df.DetailFieldID 
		AND ((df.QuestionTypeID = 4 AND ldv.DetailValue <> '0' AND ldv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND ldv.DetailValue <> '0' AND ldv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND ldv.DetailValue <> ''))
	)
	UNION ALL 	
	SELECT 'Customer Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize]
	FROM dbo.EventTypeMandatoryField etmf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE etmf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 10
	AND df.Enabled = 1 
	AND NOT EXISTS(
		SELECT * 
		FROM dbo.CustomerDetailValues cdv (nolock) 
		WHERE cdv.CustomerID = @CustomerID 
		AND cdv.DetailFieldID = df.DetailFieldID 
		AND ((df.QuestionTypeID = 4 AND cdv.DetailValue <> '0' AND cdv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND cdv.DetailValue <> '0' AND cdv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND cdv.DetailValue <> ''))
	)	
	UNION ALL
	SELECT 'Case Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize]
	FROM dbo.EventTypeMandatoryField etmf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE etmf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 11
	AND df.Enabled = 1 
	AND NOT EXISTS(
		SELECT * 
		FROM dbo.CaseDetailValues csdv (nolock) 
		WHERE csdv.CaseID = @CaseID 
		AND csdv.DetailFieldID = df.DetailFieldID 
		AND ((df.QuestionTypeID = 4 AND csdv.DetailValue <> '0' AND csdv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND csdv.DetailValue <> '0' AND csdv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND csdv.DetailValue <> ''))
	)		
	UNION ALL
	SELECT 'Matter Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], m1.RefLetter as [MatterRefLetter], df.DetailFieldID, m1.MatterID as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize]
	FROM dbo.EventTypeMandatoryField etmf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	CROSS JOIN dbo.Matter m1 (nolock) 
	WHERE etmf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 2
	AND df.Enabled = 1 
	AND m1.LeadID = @LeadID
	AND m1.CaseID = @CaseID
	AND NOT EXISTS(
		SELECT * 
		FROM dbo.MatterDetailValues mdv (nolock) 
		INNER JOIN dbo.Matter m (nolock) ON mdv.MatterID = m.MatterID
		INNER JOIN dbo.Cases c (nolock) ON m.CaseID = c.CaseID
		WHERE mdv.LeadID = @LeadID 
		AND mdv.MatterID = m1.MatterID
		AND c.CaseID = @CaseID
		AND mdv.DetailFieldID = df.DetailFieldID 
		AND ((df.QuestionTypeID = 4 AND mdv.DetailValue <> '0' AND mdv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND mdv.DetailValue <> '0' AND mdv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND mdv.DetailValue <> ''))
	)
	ORDER BY [MissingType], [MatterRefLetter], [PageDisplayOrder], [FieldDisplayOrder]
	
	/* Some callers need the count back as the return code */
	RETURN @@ROWCOUNT
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryFields__oldVersion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMissingMandatoryFields__oldVersion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryFields__oldVersion] TO [sp_executeall]
GO
