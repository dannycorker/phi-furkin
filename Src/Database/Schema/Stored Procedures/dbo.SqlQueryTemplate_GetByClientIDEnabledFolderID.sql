SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryTemplate table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_GetByClientIDEnabledFolderID]
(

	@ClientID int   ,

	@Enabled bit   ,

	@FolderID int   
)
AS


				SELECT
					[SqlQueryTemplateID],
					[ClientID],
					[Name],
					[Description],
					[TemplatePath],
					[DataWorksheetNumber],
					[DataStartCell],
					[TemplateFormat],
					[FolderID],
					[CreatedBy],
					[CreatedOn],
					[ModifiedBy],
					[ModifiedOn],
					[Enabled]
				FROM
					[dbo].[SqlQueryTemplate] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [Enabled] = @Enabled
					AND [FolderID] = @FolderID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_GetByClientIDEnabledFolderID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_GetByClientIDEnabledFolderID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_GetByClientIDEnabledFolderID] TO [sp_executeall]
GO
