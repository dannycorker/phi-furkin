SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Objects table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Objects_Delete]
(

	@ObjectID int   
)
AS


				DELETE FROM [dbo].[Objects] WITH (ROWLOCK) 
				WHERE
					[ObjectID] = @ObjectID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Objects_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Delete] TO [sp_executeall]
GO
