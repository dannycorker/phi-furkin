SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FAS_CheckerData table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_CheckerData_Insert]
(

	@FasCheckerDataID int    OUTPUT,

	@FasUniqueReferenceNumber varchar (100)  ,

	@TitleOfAgreement varchar (512)  ,

	@IsDebtorsNameOrAddressMissing bit   ,

	@IsCreditorsNameOrAddressMissing bit   ,

	@AgreementDebtorSignatureDate datetime   ,

	@NameOfCreditor varchar (512)  ,

	@AmountOfCredit decimal (18, 0)  ,

	@TotalCashPrice decimal (18, 0)  ,

	@DurationOfAgreementInMonths int   ,

	@AnnualPercentageRate float   ,

	@RateOfInterest float   ,

	@FixedOrVariableRateOfInterest varchar (50)  ,

	@TotalInterest decimal (18, 0)  ,

	@TotalChargeFees decimal (18, 0)  ,

	@TotalChargeForCredit decimal (18, 0)  ,

	@FirstPayment decimal (18, 0)  ,

	@ConstantRegularOrSubsequentPayment decimal (18, 0)  ,

	@LastPayment decimal (18, 0)  ,

	@DeferredOrBallonPayment decimal (18, 0)  ,

	@TotalPayments decimal (18, 0)  ,

	@AdvancePayment decimal (18, 0)  ,

	@TotalAmountPayable decimal (18, 0)  ,

	@Category char (1)  
)
AS


				
				INSERT INTO [dbo].[FAS_CheckerData]
					(
					[FasUniqueReferenceNumber]
					,[TitleOfAgreement]
					,[IsDebtorsNameOrAddressMissing]
					,[IsCreditorsNameOrAddressMissing]
					,[AgreementDebtorSignatureDate]
					,[NameOfCreditor]
					,[AmountOfCredit]
					,[TotalCashPrice]
					,[DurationOfAgreementInMonths]
					,[AnnualPercentageRate]
					,[RateOfInterest]
					,[FixedOrVariableRateOfInterest]
					,[TotalInterest]
					,[TotalChargeFees]
					,[TotalChargeForCredit]
					,[FirstPayment]
					,[ConstantRegularOrSubsequentPayment]
					,[LastPayment]
					,[DeferredOrBallonPayment]
					,[TotalPayments]
					,[AdvancePayment]
					,[TotalAmountPayable]
					,[Category]
					)
				VALUES
					(
					@FasUniqueReferenceNumber
					,@TitleOfAgreement
					,@IsDebtorsNameOrAddressMissing
					,@IsCreditorsNameOrAddressMissing
					,@AgreementDebtorSignatureDate
					,@NameOfCreditor
					,@AmountOfCredit
					,@TotalCashPrice
					,@DurationOfAgreementInMonths
					,@AnnualPercentageRate
					,@RateOfInterest
					,@FixedOrVariableRateOfInterest
					,@TotalInterest
					,@TotalChargeFees
					,@TotalChargeForCredit
					,@FirstPayment
					,@ConstantRegularOrSubsequentPayment
					,@LastPayment
					,@DeferredOrBallonPayment
					,@TotalPayments
					,@AdvancePayment
					,@TotalAmountPayable
					,@Category
					)
				-- Get the identity value
				SET @FasCheckerDataID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_CheckerData_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Insert] TO [sp_executeall]
GO
