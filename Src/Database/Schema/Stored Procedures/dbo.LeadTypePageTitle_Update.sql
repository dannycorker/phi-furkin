SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadTypePageTitle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitle_Update]
(

	@LeadTypeID int   ,

	@OriginalLeadTypeID int   ,

	@PageTitle varchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadTypePageTitle]
				SET
					[LeadTypeID] = @LeadTypeID
					,[PageTitle] = @PageTitle
				WHERE
[LeadTypeID] = @OriginalLeadTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitle_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Update] TO [sp_executeall]
GO
