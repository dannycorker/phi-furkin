SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ChartPalette table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartPalette_Get_List]

AS


				
				SELECT
					[ChartPaletteID],
					[ChartPaletteName],
					[ChartPalettePreviewImageUrl]
				FROM
					[dbo].[ChartPalette] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartPalette_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Get_List] TO [sp_executeall]
GO
