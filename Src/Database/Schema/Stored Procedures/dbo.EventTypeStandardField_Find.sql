SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeStandardField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeStandardFieldID int   = null ,

	@EventTypeID int   = null ,

	@DataLoaderObjectFieldID int   = null ,

	@ClientID int   = null ,

	@Mandatory bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeStandardFieldID]
	, [EventTypeID]
	, [DataLoaderObjectFieldID]
	, [ClientID]
	, [Mandatory]
    FROM
	[dbo].[EventTypeStandardField] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeStandardFieldID] = @EventTypeStandardFieldID OR @EventTypeStandardFieldID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID OR @DataLoaderObjectFieldID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Mandatory] = @Mandatory OR @Mandatory IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeStandardFieldID]
	, [EventTypeID]
	, [DataLoaderObjectFieldID]
	, [ClientID]
	, [Mandatory]
    FROM
	[dbo].[EventTypeStandardField] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeStandardFieldID] = @EventTypeStandardFieldID AND @EventTypeStandardFieldID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID AND @DataLoaderObjectFieldID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Mandatory] = @Mandatory AND @Mandatory is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Find] TO [sp_executeall]
GO
