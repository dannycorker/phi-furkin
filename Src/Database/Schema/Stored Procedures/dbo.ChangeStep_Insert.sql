SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChangeStep table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStep_Insert]
(

	@ChangeStepID int    OUTPUT,

	@ClientID int   ,

	@ChangeInstanceID int   ,

	@StepTypeID int   ,

	@ChangeDocumentID int   ,

	@Comment varchar (255)  ,

	@ExternalUrl varchar (255)  ,

	@ChangeStepStateID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ChangeStep]
					(
					[ClientID]
					,[ChangeInstanceID]
					,[StepTypeID]
					,[ChangeDocumentID]
					,[Comment]
					,[ExternalUrl]
					,[ChangeStepStateID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@ChangeInstanceID
					,@StepTypeID
					,@ChangeDocumentID
					,@Comment
					,@ExternalUrl
					,@ChangeStepStateID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @ChangeStepID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStep_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Insert] TO [sp_executeall]
GO
