SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadDocumentEsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_Get_List]

AS


				
				SELECT
					[LeadDocumentEsignatureStatusID],
					[ClientID],
					[LeadDocumentID],
					[ElectronicSignatureDocumentKey],
					[DocumentName],
					[SentTo],
					[DateSent],
					[EsignatureStatusID],
					[StatusCheckedDate]
				FROM
					[dbo].[LeadDocumentEsignatureStatus] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Get_List] TO [sp_executeall]
GO
