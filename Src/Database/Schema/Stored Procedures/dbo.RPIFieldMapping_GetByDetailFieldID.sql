SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIFieldMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_GetByDetailFieldID]
(

	@DetailFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[RPIFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[RPIFieldID],
					[DetailFieldID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ColumnDetailFieldID]
				FROM
					[dbo].[RPIFieldMapping] WITH (NOLOCK) 
				WHERE
					[DetailFieldID] = @DetailFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_GetByDetailFieldID] TO [sp_executeall]
GO
