SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserOption table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_GetByPortalUserOptionTypeID]
(

	@PortalUserOptionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalUserOptionID],
					[ClientID],
					[PortalUserID],
					[PortalUserOptionTypeID],
					[OptionValue]
				FROM
					[dbo].[PortalUserOption] WITH (NOLOCK) 
				WHERE
					[PortalUserOptionTypeID] = @PortalUserOptionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_GetByPortalUserOptionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_GetByPortalUserOptionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_GetByPortalUserOptionTypeID] TO [sp_executeall]
GO
