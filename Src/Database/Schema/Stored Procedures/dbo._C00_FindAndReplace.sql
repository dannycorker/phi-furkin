SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 21-APR-2010
-- Description:	Proc to Find and replace in Docs 
--				2014-01-17 ACE Added Subject into the mix
--				2014-12-10 CS  Added DocumentMRTargets
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FindAndReplace]
(
@ClientID int,
@FromText varchar(1000), 
@ToText varchar(max),
@LeadTypeID int = NULL,
@DoFieldTargets bit = 0,
@SpecificDocumentTypeID INT = NULL	/*CS 2014-12-01*/
)


AS
BEGIN
	SET NOCOUNT ON;

	update documenttype
	set template = replace(convert(varchar(max), template), @FromText, @ToText),
			EmailBodyText = replace(convert(varchar(max), EmailBodyText), @FromText, @ToText),
			EmailSubject = replace(convert(varchar(max), EmailSubject), @FromText, @ToText)
	from documenttype
	where clientid = @ClientID
	and (LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL)
	AND (@SpecificDocumentTypeID is NULL or DocumentType.DocumentTypeID = @SpecificDocumentTypeID)

	IF @DoFieldTargets = 1
	BEGIN
	
		Update DocumentDetailFieldTarget
		Set Target = REPLACE(target, @FromText, @ToText), 
			DetailFieldAlias = REPLACE(DetailFieldAlias, @FromText, @ToText)
		From DocumentDetailFieldTarget
		Where DocumentDetailFieldTarget.ClientID = @ClientID
		AND (@SpecificDocumentTypeID is NULL or DocumentDetailFieldTarget.DocumentTypeID = @SpecificDocumentTypeID)
	
		Update DocumentStandardTarget
		Set Target = REPLACE(DocumentStandardTarget.target, @FromText, @ToText), ObjectName = f.ObjectName, PropertyName = f.PropertyName
		From DocumentStandardTarget
		INNER JOIN FieldTarget f ON f.Target = @ToText
		Where DocumentStandardTarget.ClientID = @ClientID
		AND DocumentStandardTarget.Target = @FromText
		AND (@SpecificDocumentTypeID is NULL or DocumentStandardTarget.DocumentTypeID = @SpecificDocumentTypeID)

		Update DocumentSpecialisedDetailFieldTarget
		Set Target = REPLACE(target, @FromText, @ToText), 
			DetailFieldAlias = REPLACE(DetailFieldAlias, @FromText, @ToText), 
			ColumnFieldAlias = REPLACE(ColumnFieldAlias, @FromText, @ToText)
		From DocumentSpecialisedDetailFieldTarget
		Where DocumentSpecialisedDetailFieldTarget.ClientID = @ClientID
		AND (@SpecificDocumentTypeID is NULL or DocumentSpecialisedDetailFieldTarget.DocumentTypeID = @SpecificDocumentTypeID)

		Update DocumentMRTarget
		Set Target = REPLACE(target, @FromText, @ToText), 
			DetailFieldAlias = REPLACE(DetailFieldAlias, @FromText, @ToText), 
			ColumnFieldAlias = REPLACE(ColumnFieldAlias, @FromText, @ToText)
		From DocumentMRTarget
		Where DocumentMRTarget.ClientID = @ClientID
		AND (@SpecificDocumentTypeID is NULL or DocumentMRTarget.DocumentTypeID = @SpecificDocumentTypeID)
	
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindAndReplace] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindAndReplace] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindAndReplace] TO [sp_executeall]
GO
