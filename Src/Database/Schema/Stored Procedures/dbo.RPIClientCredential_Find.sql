SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIClientCredential table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientCredential_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIClientCredentialID int   = null ,

	@ClientID int   = null ,

	@UserName varchar (250)  = null ,

	@Password varchar (50)  = null ,

	@AsUser varchar (250)  = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIClientCredentialID]
	, [ClientID]
	, [UserName]
	, [Password]
	, [AsUser]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[RPIClientCredential] WITH (NOLOCK) 
    WHERE 
	 ([RPIClientCredentialID] = @RPIClientCredentialID OR @RPIClientCredentialID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([UserName] = @UserName OR @UserName IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([AsUser] = @AsUser OR @AsUser IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIClientCredentialID]
	, [ClientID]
	, [UserName]
	, [Password]
	, [AsUser]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[RPIClientCredential] WITH (NOLOCK) 
    WHERE 
	 ([RPIClientCredentialID] = @RPIClientCredentialID AND @RPIClientCredentialID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([UserName] = @UserName AND @UserName is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([AsUser] = @AsUser AND @AsUser is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientCredential_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Find] TO [sp_executeall]
GO
