SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryColumns table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryColumns_GetBySqlQueryTableID]
(

	@SqlQueryTableID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryColumnID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryTableID],
					[CompleteOutputText],
					[ColumnAlias],
					[ShowColumn],
					[DisplayOrder],
					[SortOrder],
					[SortType],
					[TempColumnID],
					[TempTableID],
					[IsAggregate],
					[ColumnDataType],
					[ManipulatedDataType],
					[ColumnNaturalName],
					[SourceID],
					[ComplexValue]
				FROM
					[dbo].[SqlQueryColumns] WITH (NOLOCK) 
				WHERE
					[SqlQueryTableID] = @SqlQueryTableID
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_GetBySqlQueryTableID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryColumns_GetBySqlQueryTableID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_GetBySqlQueryTableID] TO [sp_executeall]
GO
