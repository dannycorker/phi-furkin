SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupFunctionControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControl_GetByFunctionTypeID]
(

	@FunctionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[GroupFunctionControlID],
					[ClientPersonnelAdminGroupID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[GroupFunctionControl] WITH (NOLOCK) 
				WHERE
					[FunctionTypeID] = @FunctionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_GetByFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControl_GetByFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_GetByFunctionTypeID] TO [sp_executeall]
GO
