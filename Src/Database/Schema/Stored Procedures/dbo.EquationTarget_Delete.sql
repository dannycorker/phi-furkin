SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EquationTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_Delete]
(

	@EquationTargetID int   
)
AS


				DELETE FROM [dbo].[EquationTarget] WITH (ROWLOCK) 
				WHERE
					[EquationTargetID] = @EquationTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Delete] TO [sp_executeall]
GO
