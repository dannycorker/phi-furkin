SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-31
-- Description:	Get a WorkflowTask for this user
-- JWG	2010-03-03	Prevent Disabled tasks from being selected at all
-- SB	2011-10-25	Added new sort order to workflow group to allow task selection to take newest first or oldest first
-- JWG  2012-07-02  New feature "AssignedLeadsOnly" for workflow groups. Will only assign tasks for your own leads if ticked.
-- JEL  2018-11-08 Change to the ordering to allow prioritisation within an overarching SLA Regardless of the priority of a task, go by the day part of the date first
-- =============================================
CREATE PROCEDURE [dbo].[WorkflowTask_GetATaskForThisUser]

	@UserID int 

AS
BEGIN
	
	SET NOCOUNT ON

	DECLARE @ClientID int,
	@WorkflowTaskID int,
	@AssignedAlready bit,
	@LockResult int,
	@LogEntry varchar(2000)

	SELECT @ClientID = cp.ClientID, 
	@WorkflowTaskID = null, 
	@AssignedAlready = 0 
	FROM ClientPersonnel cp 
	WHERE cp.ClientPersonnelID = @UserID 
	
	/* Only ever select a maximum of 1 row throughout this proc */
	SET ROWCOUNT 1

	-- Look for the first existing Important task that is already assigned to this user (if any)
	SELECT @WorkflowTaskID = wt.WorkflowTaskID 
	FROM WorkflowTask wt 
	WHERE wt.AssignedTo = @UserID 
	AND wt.Important = 1 
	AND wt.Disabled = 0 

	-- If found, note that it is already assigned to this user
	IF @WorkflowTaskID IS NOT NULL
	BEGIN

		SELECT @AssignedAlready = 1

	END
	ELSE
	BEGIN

		-- Get first new Important task (if any) for this entire Client
		BEGIN TRAN

		-- Use system proc sp_getapplock to limit this to one request at a time:
		-- @Resource: the unique name for this lock
        -- @LockMode: 'Shared', 'Update', 'IntentShared', 'IntentExclusive', 'Exclusive'
        -- @LockOwner: Scope of the lock: 'Transaction' or 'Session'
        -- @LockTimeout: Timeout in miliseconds
        -- @DbPrincipal: The DB Principal for access permisions ('dbo', 'public', 'guest')
		EXEC @LockResult = sp_getapplock 
            @Resource = 'WorkflowTask_GetATaskForThisUser_Important',
            @LockMode = 'Exclusive',
            @LockOwner = 'Transaction',
            @LockTimeout = 10000,
            @DbPrincipal = 'public'

		-- 0 and 1 are the valid return values from sp_getapplock
		IF @LockResult NOT IN (0, 1)
		BEGIN
			RAISERROR ( 'Unable to acquire Lock', 16, 1 )
			ROLLBACK
			RETURN
		END 
		ELSE
		BEGIN
			
			SELECT @WorkflowTaskID = wt.WorkflowTaskID 
			FROM WorkflowTask wt 
			WHERE wt.ClientID = @ClientID 
			AND wt.Important = 1 
			AND wt.AssignedTo IS NULL 
			AND wt.Disabled = 0 
			ORDER BY wt.Important DESC 

			IF @WorkflowTaskID IS NOT NULL
			BEGIN
				UPDATE WorkflowTask 
				SET AssignedTo = @UserID, AssignedDate = dbo.fn_GetDate_Local() 
				WHERE WorkflowTaskID = @WorkflowTaskID
			END
			
			EXEC @LockResult = sp_releaseapplock 
							@Resource = 'WorkflowTask_GetATaskForThisUser_Important',
							@DbPrincipal = 'public',
							@LockOwner = 'Transaction'
		END

		COMMIT

	END -- Important Task section


	-- If nothing yet from the Important tasks, keep looking for 
	-- ordinary tasks that this user is qualified to handle.
	IF @WorkflowTaskID IS NULL
	BEGIN

		-- Look for the first existing ordinary task that is already assigned to this user
		SELECT @WorkflowTaskID = wt.WorkflowTaskID 
		FROM WorkflowTask wt 
		INNER JOIN WorkflowGroup wg ON wg.WorkflowGroupID = wt.WorkflowGroupID AND wg.Enabled = 1 
		INNER JOIN WorkflowGroupAssignment wga ON wga.WorkflowGroupID = wg.WorkflowGroupID
		WHERE wt.AssignedTo = @UserID 
		AND wt.Important = 0 
		AND wt.Disabled = 0 
		ORDER BY wga.Priority, wt.Priority 

		-- If found, note that it is already assigned to this user
		IF @WorkflowTaskID IS NOT NULL
		BEGIN

			SELECT @AssignedAlready = 1

		END
		ELSE
		BEGIN

			-- (Normal scenario)
			-- Look for the first ordinary task that this user can handle
			BEGIN TRAN

			EXEC @LockResult = sp_getapplock 
                @Resource = 'WorkflowTask_GetATaskForThisUser_Ordinary',
                @LockMode = 'Exclusive',
                @LockOwner = 'Transaction',
                @LockTimeout = 10000,
                @DbPrincipal = 'public'

			-- 0 and 1 are the valid return values from sp_getapplock
			IF @LockResult NOT IN (0, 1)
			BEGIN
				RAISERROR ( 'Unable to acquire Lock', 16, 1 )
				ROLLBACK
				RETURN
			END 
			ELSE
			BEGIN
			
				-- Last chance for all other users to get a task...
				-- Try to get a task from the highest priority group and type...
				-- Also now order by the workflow group sorting to get either oldest or newest first
				-- WorkflowGroup.AssignedLeadsOnly: for groups with this flag set to 1, you will only get tasks for leads assigned to you.
				SELECT @WorkflowTaskID = wt.WorkflowTaskID 
				FROM WorkflowGroupAssignment wga 
				INNER JOIN WorkflowGroup wg ON wg.WorkflowGroupID = wga.WorkflowGroupID AND wg.Enabled = 1 
				INNER JOIN WorkflowTask wt ON wt.WorkflowGroupID = wg.WorkflowGroupID 
				INNER JOIN dbo.Lead l ON l.LeadID = wt.LeadID 
				WHERE wga.ClientPersonnelID = @UserID 
				AND wt.Important = 0 
				AND wt.AssignedTo IS NULL
				AND wt.Disabled = 0 
				AND (wg.AssignedLeadsOnly = 0 OR wg.AssignedLeadsOnly IS NULL OR l.AssignedTo = @UserID)
				ORDER BY wga.Priority, CASE wg.SortOrder 
										WHEN 'ASC' THEN CAST(wt.CreationDate as DATE)
										WHEN NULL THEN CAST(wt.CreationDate as DATE)
									END ASC,
									CASE wg.SortOrder WHEN 'DESC' THEN CAST(wt.CreationDate as DATE) END DESC, wt.Priority
				/*JEL 2018-11-08 Change to the ordering to allow prioritisation within an overarching SLA
				Regardless of the priority of a task, go by the day part of the date first*/ 
				/*ORDER BY wga.Priority, wt.Priority, CASE wg.SortOrder 
														WHEN 'ASC' THEN wt.WorkflowTaskID 
														WHEN NULL THEN wt.WorkflowTaskID 
													END ASC,
													CASE wg.SortOrder WHEN 'DESC' THEN wt.WorkflowTaskID END DESC*/
				/* You cannot dynamically specify ASC or DESC in Sql, but this block specifies {WorkflowTaskID or nothing} ASC, followed by {nothing or WorkflowTaskID DESC} which does work! */
				
				IF @WorkflowTaskID IS NOT NULL
				BEGIN
					UPDATE WorkflowTask 
					SET AssignedTo = @UserID, AssignedDate = dbo.fn_GetDate_Local() 
					WHERE WorkflowTaskID = @WorkflowTaskID
				END
				
				EXEC @LockResult = sp_releaseapplock 
								@Resource = 'WorkflowTask_GetATaskForThisUser_Ordinary',
								@DbPrincipal = 'public',
								@LockOwner = 'Transaction'
				
			END

			COMMIT

		END

	END -- Ordinary Task section

	IF @ClientID = 197 /*CS for Testing*/
	BEGIN
		exec _C00_LogIt 'Debug', 'WorkflowTask_GetATaskForThisUser', 'C197 Log', @WorkflowTaskID, @UserID
	END

	-- Now return the results
	IF @WorkflowTaskID IS NOT NULL
	BEGIN

		-- Select the values for the application to display
		SELECT wt.WorkflowTaskID, l.CustomerID, wt.LeadID, wt.CaseID 
		FROM WorkflowTask wt 
		INNER JOIN Lead l ON l.LeadID = wt.LeadID 
		WHERE WorkflowTaskID = @WorkflowTaskID 

	END
	ELSE
	BEGIN

		-- If we reach this point, then there is literally no work for this user to do.
		SELECT 0 as WorkflowTaskID, 0 as CustomerID, 0 as LeadID, 0 as CaseID 

	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetATaskForThisUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_GetATaskForThisUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetATaskForThisUser] TO [sp_executeall]
GO
