SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetOutcomeDelayedEmails] 
@ClientQuestionnaireID int
AS
Select OutcomeDelayedEmailID, CustomerID, ClientQuestionnaireID, DateOutcomeEmailSent, ClientID 
From OutcomeDelayedEmails
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeDelayedEmails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeDelayedEmails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeDelayedEmails] TO [sp_executeall]
GO
