SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the MasterQuestionStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestionStatus_Update]
(

	@MasterQuestionStatusID int   ,

	@OriginalMasterQuestionStatusID int   ,

	@MasterQuestionStatusName varchar (50)  ,

	@MasterQuestonStatusDescription varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[MasterQuestionStatus]
				SET
					[MasterQuestionStatusID] = @MasterQuestionStatusID
					,[MasterQuestionStatusName] = @MasterQuestionStatusName
					,[MasterQuestonStatusDescription] = @MasterQuestonStatusDescription
				WHERE
[MasterQuestionStatusID] = @OriginalMasterQuestionStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestionStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Update] TO [sp_executeall]
GO
