SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-06-23
-- Description:	Looks up the key mapping at returns the corresponding IDs
-- =============================================
CREATE PROCEDURE [dbo].[KeyIdentifierMappings__GetMapping]
(
	@KeyVal VARCHAR(50), 
	@ClientID INT
)


AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.KeyIdentifierMappings WITH (NOLOCK) 
	WHERE KeyVal = @KeyVal
	AND ClientID = @ClientID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[KeyIdentifierMappings__GetMapping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[KeyIdentifierMappings__GetMapping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[KeyIdentifierMappings__GetMapping] TO [sp_executeall]
GO
