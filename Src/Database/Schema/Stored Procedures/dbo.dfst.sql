SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-06-17
-- Description:	List Detail Field Sub Types
-- =============================================
CREATE PROCEDURE [dbo].[dfst]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select *
	from DetailFieldSubType with (nolock)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[dfst] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dfst] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dfst] TO [sp_executeall]
GO
