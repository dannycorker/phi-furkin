SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
Modified	2014-07-14	SB	Changed to use the new view for lead type sharing
*/

CREATE PROCEDURE [dbo].[NoteType__GetByClientPersonnelIDAndMinRightRequired]
(
	@ClientPersonnelID int,
	@MinimumRightsRequired int
)
AS

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @ClientPersonnelID

	SELECT nt.* 
	FROM dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) ns 
	INNER JOIN dbo.fnNoteTypeShared(@ClientID) nt ON nt.NoteTypeID = ns.ObjectID
	WHERE (nt.[Enabled] = 1 OR nt.[Enabled] IS NULL) /* Added by AMG 2013-05-17 */
	ORDER BY nt.NoteTypeName /*Added by CS 2011-09-08 for ticket #6271 */

	SELECT @@ROWCOUNT



GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType__GetByClientPersonnelIDAndMinRightRequired] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType__GetByClientPersonnelIDAndMinRightRequired] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType__GetByClientPersonnelIDAndMinRightRequired] TO [sp_executeall]
GO
