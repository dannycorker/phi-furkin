SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-11-2012
-- Description:	Gets a list of Event Type Automated Events by Event Type ID
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedEvent__GetByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	/* 
		JWG 2012-12-18 These columns are all used by the EventTypeEdit page, including UserName now.
	*/
	SELECT et.EventTypeName, eta.*, ISNULL(cp.UserName, '') as [RunAsUserName]
	FROM EventTypeAutomatedEvent eta WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = eta.AutomatedEventTypeID 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = eta.RunAsUserID 
	WHERE eta.EventTypeID = @EventTypeID 
	AND eta.ClientID = @ClientID 

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedEvent__GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__GetByEventTypeID] TO [sp_executeall]
GO
