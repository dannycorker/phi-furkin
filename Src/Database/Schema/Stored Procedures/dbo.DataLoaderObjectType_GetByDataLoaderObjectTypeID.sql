SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderObjectType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectType_GetByDataLoaderObjectTypeID]
(

	@DataLoaderObjectTypeID int   
)
AS


				SELECT
					[DataLoaderObjectTypeID],
					[ObjectTypeName],
					[ObjectTypeDescription],
					[ProcessingOrder]
				FROM
					[dbo].[DataLoaderObjectType] WITH (NOLOCK) 
				WHERE
										[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_GetByDataLoaderObjectTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectType_GetByDataLoaderObjectTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_GetByDataLoaderObjectTypeID] TO [sp_executeall]
GO
