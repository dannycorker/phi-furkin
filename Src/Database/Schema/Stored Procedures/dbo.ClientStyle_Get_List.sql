SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_Get_List]

AS


				
				SELECT
					[ClientStyleID],
					[ClientID],
					[StyleRuleID],
					[StyleValue]
				FROM
					[dbo].[ClientStyle] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Get_List] TO [sp_executeall]
GO
