SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetMinFromTable] @TableName varchar(100), @ColumnName varchar(100), @OptionalWhereClause varchar(1000) AS 

declare @SqlStatement varchar(2000)

BEGIN

	select @SqlStatement = 'SELECT IsNull(convert(char, MIN(' + @ColumnName + ')), ''0'') FROM ' + @TableName + ' (nolock) ' + @OptionalWhereClause

	print @SqlStatement
	exec(@SqlStatement)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetMinFromTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMinFromTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMinFromTable] TO [sp_executeall]
GO
