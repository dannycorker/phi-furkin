SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SMSResponseMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSResponseMap_Update]
(

	@SMSResponseMapID int   ,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@FromSMSQuestionGroupID int   ,

	@FromSMSQuestionID int   ,

	@FromAnswer varchar (256)  ,

	@ToSMSQuestionGroupID int   ,

	@ToSMSQuestionID int   ,

	@EndOfQuestionnaire bit   ,

	@Score int   ,

	@ConfidenceRangeStart int   ,

	@ConfidenceRangeEnd int   ,

	@RuleID int   ,

	@ProcessingOrder int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SMSResponseMap]
				SET
					[ClientID] = @ClientID
					,[SMSQuestionnaireID] = @SMSQuestionnaireID
					,[FromSMSQuestionGroupID] = @FromSMSQuestionGroupID
					,[FromSMSQuestionID] = @FromSMSQuestionID
					,[FromAnswer] = @FromAnswer
					,[ToSMSQuestionGroupID] = @ToSMSQuestionGroupID
					,[ToSMSQuestionID] = @ToSMSQuestionID
					,[EndOfQuestionnaire] = @EndOfQuestionnaire
					,[Score] = @Score
					,[ConfidenceRangeStart] = @ConfidenceRangeStart
					,[ConfidenceRangeEnd] = @ConfidenceRangeEnd
					,[RuleID] = @RuleID
					,[ProcessingOrder] = @ProcessingOrder
				WHERE
[SMSResponseMapID] = @SMSResponseMapID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Update] TO [sp_executeall]
GO
