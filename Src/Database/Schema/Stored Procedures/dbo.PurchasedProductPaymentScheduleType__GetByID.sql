SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 29/09/2016
-- Description:	Get a PurchasedProductPaymentScheduleType by ID
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentScheduleType__GetByID]
	@PurchasedProductPaymentScheduleTypeID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM PurchasedProductPaymentScheduleType WITH (NOLOCK) 
	WHERE PurchasedProductPaymentScheduleTypeID = @PurchasedProductPaymentScheduleTypeID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentScheduleType__GetByID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentScheduleType__GetByID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentScheduleType__GetByID] TO [sp_executeall]
GO
