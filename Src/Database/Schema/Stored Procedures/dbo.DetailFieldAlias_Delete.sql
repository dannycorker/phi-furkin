SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailFieldAlias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_Delete]
(

	@DetailFieldAliasID int   
)
AS


				DELETE FROM [dbo].[DetailFieldAlias] WITH (ROWLOCK) 
				WHERE
					[DetailFieldAliasID] = @DetailFieldAliasID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Delete] TO [sp_executeall]
GO
