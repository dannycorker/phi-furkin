SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChangeDocument table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_Delete]
(

	@ChangeDocumentID int   
)
AS


				DELETE FROM [dbo].[ChangeDocument] WITH (ROWLOCK) 
				WHERE
					[ChangeDocumentID] = @ChangeDocumentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Delete] TO [sp_executeall]
GO
