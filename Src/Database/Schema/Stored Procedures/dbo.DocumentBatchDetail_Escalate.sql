SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-04
-- Description:	Updates a document batch detail record  
--				with a lock indicating who the document
--				needs to be escalated to.

-- Parameters:  @DocumentBatchDetailID
--              The document batch entry to update
--				@ClientPersonnelId
--              The user to escalate to.
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail_Escalate]
	@DocumentBatchDetailID INT,
	@ClientPersonnelID INT
AS
BEGIN

	IF (@DocumentBatchDetailID IS NOT NULL AND @ClientPersonnelID IS NOT NULL)
	BEGIN
	
		UPDATE DocumentBatchDetail
			SET LockedBy = @ClientPersonnelID, LockDate = dbo.fn_GetDate_Local()
		WHERE	
			@DocumentBatchDetailID = DocumentBatchDetailID					
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_Escalate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail_Escalate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_Escalate] TO [sp_executeall]
GO
