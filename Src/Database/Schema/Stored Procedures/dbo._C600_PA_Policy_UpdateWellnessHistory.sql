SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      Gavin Reynolds / Alexandra Maguire
-- Create date: 2020-09-14
-- Description: Store wellness detail history. Proc called at New Business, MTA, Renewal, Reinstatement, Cancellation
-- GPR 2020-09-17 Update Policy History
-- GPR 2020-09-18 Evaluate State Varience Document ResourceList field value
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_UpdateWellnessHistory]
(
     @MatterID           INT = NULL
    ,@LeadEventID        INT = NULL
)
AS
BEGIN

	IF @MatterID IS NULL AND @LeadEventID IS NULL 
	BEGIN 

		RETURN;

	END

    DECLARE  @ErrorMessage      VARCHAR(2000) = '',
             @EventTypeID		INT,
             @TableRowID		INT = 0,
			 @WellnessRLID		INT, 
			 @EndDate			DATE,
			 @StartDate			DATE, 
			 @HPTableRowID		INT, 
			 @ClientID			INT, 
			 @ExpiredTableRow	INT,
			 @ExamFee			VARCHAR(3),
			 @WellnessLevel		VARCHAR(20),
			 @CoPay				VARCHAR(10),
			 @Excess			VARCHAR(10),
			 @LeadID			INT

	DECLARE @WellnessFields TABLE (FieldID INT)

    SELECT @EventTypeID =   (SELECT EventTypeID
                            FROM LeadEvent WITH (NOLOCK)
                            WHERE LeadEventID = @LeadEventID)

	SELECT @ClientID = dbo.fnGetPrimaryClientID()

	EXEC [dbo].[_C00_LogIt] 'Info', '_C600_PA_Policy_UpdateWellnessHistory', 'LeadEventID', @LeadEventID, 58552
	EXEC [dbo].[_C00_LogIt] 'Info', '_C600_PA_Policy_UpdateWellnessHistory', 'Matter PASSED IN', @MatterID, 58552

	IF @MatterID IS NULL 
	BEGIN 

		SELECT @MatterID = m.MatterID
		FROM LeadEvent le WITH (NOLOCK) 
		JOIN Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
		WHERE le.LeadEventID = @LeadEventID

		EXEC [dbo].[_C00_LogIt] 'Info', '_C600_PA_Policy_UpdateWellnessHistory', 'Matter NULL', @MatterID, 58552

	END

	IF @LeadID IS NULL
	BEGIN
		SELECT @LeadID = m.LeadID
		FROM Matter m WITH (NOLOCK)
		WHERE m.MatterID = @MatterID
	END
    
	/*Set the values*/
	SELECT @WellnessRLID = dbo.fnGetSimpleDvAsInt(314235, @MatterID)
    
	SELECT @EndDate = dbo.fnGetSimpleDvAsDate(170037, @MatterID)

	/*GPR/ALM 2020-09-19 if we're creating the first row use the PolicyStart else use the Adjustemnt Date*/
	--IF (SELECT COUNT(*) FROM TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = 314240 AND tr.MatterID = @MatterID) >= 1
	--BEGIN
	--	SELECT @StartDate = dbo.fn_GetDate_Local()
	--END
	--ELSE
	--BEGIN
	--	SELECT @StartDate = dbo.fnGetSimpleDvAsDate(170036, @MatterID) --dbo.fn_GetDate_Local()
	--END

	/*GPR/ALM 2020-09-23 If MTA then we have an AdjustmentDate*/
	IF dbo.fnGetSimpleDvAsDate(175442, @MatterID) IS NOT NULL
	BEGIN

			IF (SELECT COUNT(*) FROM TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = 314240 AND tr.MatterID = @MatterID) >= 1
			BEGIN
				SELECT @StartDate =  dbo.fnGetSimpleDvAsDate(175442, @MatterID) --dbo.fn_GetDate_Local()
			END
			ELSE
			BEGIN
				SELECT @StartDate = dbo.fnGetSimpleDvAsDate(170036, @MatterID)
			END

	END
	ELSE
	BEGIN /*new business, renewal*/
		SELECT @StartDate = dbo.fnGetSimpleDvAsDate(170036, @MatterID) --dbo.fn_GetDate_Local()
	END

		

	SELECT TOP 1 @HPTableRowID = tr.TableRowID 
	FROM TableRows tr WITH (NOLOCK) 
	JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID
	WHERE tr.DetailFieldID = 170033
	AND tdv.DetailFieldID = 145664 
	AND tdv.ValueDate > dbo.fn_GetDate_Local() 
	AND tr.MatterID = @MatterID 

			/*If an existing wellness table row then update it to expired*/
			SELECT @ExpiredTableRow = tr.TableRowID 
			FROM TableRows tr WITH (NOLOCK) 
			JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID
			WHERE tr.MatterID = @MatterID
			AND tr.DetailFieldID = 314240
			AND tdv.DetailFieldID = 314236
			AND tdv.ValueInt = 74577
			ORDER BY 1 DESC
			--AND tr.TableRowID <> @TableRowID

			IF @ExpiredTableRow > 0
			BEGIN 
		
				SELECT @EndDate = DATEADD(DAY,-1,@StartDate)

				EXEC dbo._C00_SimpleValueIntoField 314236, 74578, @ExpiredTableRow
				EXEC dbo._C00_SimpleValueIntoField 314239, @EndDate, @ExpiredTableRow

				EXEC [dbo].[_C00_LogIt] 'Info', '_C600_PA_Policy_UpdateWellnessHistory', 'ExpiredTableRowID Updated', @ExpiredTableRow, 58552
    
			END

	IF @WellnessRLID > 0
	BEGIN
			
		IF @LeadEventID > 0
		BEGIN
			EXEC @TableRowID = _C00_CreateTableRowFromLeadEvent @LeadEventID,314240, 1, 1
		END
		ELSE
		BEGIN
			/*Create the new row.  Most objectIDs will be NULL*/
			INSERT TableRows ( ClientID, MatterID, DetailFieldID, DetailFieldPageID )
			SELECT TOP 1 m.ClientID, @MatterID, 314240, 19227
			FROM Matter m WITH ( NOLOCK )
			WHERE m.MatterID = @MatterID
			
			/*Pick up the new ID*/
			SELECT @TableRowID = SCOPE_IDENTITY()
			
		END
		
		IF @TableRowID > 0
		BEGIN
		
			SELECT @EndDate = dbo.fnGetSimpleDvAsDate(170037, @MatterID)

			-- Latest Policy History Row
			SELECT TOP 1 @HPTableRowID = tr.TableRowID 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID 
			
			EXEC _C00_SimpleValueIntoField 314234, @WellnessRLID, @TableRowID -- Wellness Type
			EXEC _C00_SimpleValueIntoField 314236, 74577, @TableRowID -- Active Status
			EXEC _C00_SimpleValueIntoField 314237, @StartDate, @TableRowID -- Start Date
			EXEC _C00_SimpleValueIntoField 314239, @EndDate, @TableRowID -- End Date
			EXEC _C00_SimpleValueIntoField 314241, @HPTableRowID, @TableRowID -- Historical Policy TableRowID


			EXEC [dbo].[_C00_LogIt] 'Info', '_C600_PA_Policy_UpdateWellnessHistory', 'WellnessHistoryTableRowID Updated', @TableRowID, 58552
		END
	END


	/*UPDATE POLICY HISTORY*/
	
	-- Latest Policy History Row
	SELECT TOP 1 @HPTableRowID = tr.TableRowID 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.DetailFieldID = 170033
	AND tr.MatterID = @MatterID 

	-- Get the latest Policy History row and update the ExamFee, WellnessLevel, Copay, Excess
	DECLARE @ProductID INT
	SET @ExamFee = ISNULL(dbo.fnGetSimpleDvLuli(313931, @LeadID),'No')
	SET @WellnessLevel = ISNULL(dbo.fnGetSimpleRLDv(314235, 314005, @MatterID, 0),'')
	SET @CoPay = dbo.fnGetSimpleDv(313930, @LeadID)
	SET @Excess  = dbo.fnGetSimpleDv(313932, @LeadID)

	EXEC _C00_SimpleValueIntoField 314011, @ExamFee, @HPTableRowID /*ALM 2020-10-08*/
	EXEC _C00_SimpleValueIntoField 314267, @WellnessLevel, @HPTableRowID
	EXEC _C00_SimpleValueIntoField 177415, @CoPay, @HPTableRowID
	EXEC _C00_SimpleValueIntoField 177416, @Excess, @HPTableRowID

	/*GPR 2020-09-18*/
	/*UPDATE STATE VARIANCE DETAIL FIELDS*/
	DECLARE @ExamFeesIncluded INT
	DECLARE @WorkingDog INT
	DECLARE @WellnessProduct INT

	SELECT @ProductID = dbo.fnGetSimpleDvAsInt(170034, @MatterID)
	SELECT @ExamFeesIncluded = dbo.fnGetSimpleDvAsInt(313931, @LeadID)
	SELECT @WorkingDog = dbo.fnGetSimpleDvAsInt(313929, @LeadID)
	SELECT @WellnessProduct = CASE @WellnessRLID WHEN 0 THEN 5145 ELSE 5144 END

	/*ALM/GPR 2020-09-18 Update DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)*/
	EXEC dbo._C605_Add_StateVarianceDetailFields_RLID @ProductID, @ExamFeesIncluded, @WorkingDog, @WellnessProduct, @MatterID


	EXEC [dbo].[_C00_LogIt] 'Info', '_C600_PA_Policy_UpdateWellnessHistory', 'HistoricalPolicyTableRowID Updated', @HPTableRowID, 58552


    RETURN @TableRowID
    
END
GO
