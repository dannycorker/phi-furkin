SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- Modified By PR Added DeclinedOrVoidedEventTypeID
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__SendToQueue]
	@OutLeadEventID INT,
	@DSPEnvelopeStatus VARCHAR(50),
    @RequestXML XML = null,
    @DSPEnvelopeKey VARCHAR(50) = null,
    @ResponseXML XML  = null,
    @InLeadEventId INT = 0,
    @DeclinedOrVoidedLeadEventID INT = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE 
		@DSPQueueID INT, 
		@ClientID INT, 
		@LeadID INT, 
		@CaseID INT, 
		@OutEventTypeID INT, 
		@OutDocumentTypeID INT, 
		@InEventTypeID INT,  
		@DSPTypeID INT, 
		@DSPAccountID INT, 
		@DSPUserID INT, 
		@DSPBrandingKey VARCHAR(50), 
		@DSPSignatureTemplate VARCHAR(250), 
		@WhenCreated DATETIME, 
		@WhenModified DATETIME,
		@DeclinedOrVoidedEventTypeID INT
	
	SELECT	
			@ClientID = le.ClientID,
			@LeadID = le.LeadID,
			@CaseID = le.CaseID,
			@OutEventTypeID = et.EventTypeID,
			@OutDocumentTypeID = et.DocumentTypeID, 
			@InEventTypeID = dspm.DocInEventTypeID,
			@DSPTypeID = dspm.DSPTypeID,
			@DSPAccountID = dspm.DSPAccountID,
			@DSPUserID = dspm.DSPUserID,
			@DSPBrandingKey = dspm.BrandingKey,
			@DSPSignatureTemplate = ISNULL(dspt.DSPSignatureTemplate, '<<Signature{0}>>'),
			@WhenCreated = dbo.fn_GetDate_Local(),
			@WhenModified = dbo.fn_GetDate_Local(),
			@DeclinedOrVoidedEventTypeID = dspm.DeclinedOrVoidedEventTypeID
	FROM	LeadEvent le WITH (NOLOCK) 
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
	INNER JOIN DocumentSignProviderMapping dspm WITH (NOLOCK) ON dspm.EmailOutETID = et.EventTypeID AND dspm.OutDocumentTypeID = et.DocumentTypeID --(dspm.OutDocumentTypeID = et.DocumentTypeID OR dspm.OutDocumentTypeID IS NULL)
	INNER JOIN DocumentSignProviderType dspt WITH (NOLOCK)  ON dspt.DSPTypeID = dspm.DSPTypeID
	WHERE	le.LeadEventID = @OutLeadEventID
	AND		et.EventSubtypeID = 6
	

	IF NOT EXISTS (SELECT * FROM DocumentSignProviderQueue WITH (NOLOCK) WHERE OutLeadEventID = @OutLeadEventID)
	BEGIN
		INSERT INTO DocumentSignProviderQueue 
		(
			ClientID, 
			LeadID, 
			CaseID, 
			OutLeadEventID, 
			OutEventTypeID, 
			OutDocumentTypeID, 
			InEventTypeID, 
			InLeadEventID, 
			DSPTypeID, 
			DSPAccountID, 
			DSPUserID, 
			DSPSignatureTemplate,
			DSPBrandingKey, 
			DSPEnvelopeKey, 
			DSPEnvelopeStatus, 
			WhenCreated, 
			WhenModified,
			RequestXML, 
			ResponseXML,
			DeclinedOrVoidedEventTypeID, 
			DeclinedOrVoidedLeadEventID
		)
		VALUES 
		(
			@ClientID, 
			@LeadID,
			@CaseID, 
			@OutLeadEventID, 
			@OutEventTypeID, 
			@OutDocumentTypeID, 
			@InEventTypeID, 
			@InLeadEventID, 
			@DSPTypeID, 
			@DSPAccountID, 
			@DSPUserID, 
			@DSPSignatureTemplate,
			@DSPBrandingKey, 
			@DSPEnvelopeKey, 
			@DSPEnvelopeStatus, 
			@WhenCreated, 
			@WhenModified,
			@RequestXML,
			@ResponseXML,
			@DeclinedOrVoidedEventTypeID,
			@DeclinedOrVoidedLeadEventID
		)
	END
	
	ELSE
	BEGIN
	
		UPDATE DocumentSignProviderQueue
		SET 
			DSPEnvelopeStatus = ISNULL(@DSPEnvelopeStatus,DSPEnvelopeStatus),
			RequestXML = ISNULL(@RequestXML,RequestXML),
			DSPEnvelopeKey = ISNULL(@DSPEnvelopeKey, DSPEnvelopeKey),
			ResponseXML = ISNULL(@ResponseXML, ResponseXML),
			WhenModified = ISNULL(@WhenModified, WhenModified),
			InLeadEventId = @InLeadEventId,
			DeclinedOrVoidedLeadEventID = @DeclinedOrVoidedLeadEventID
		WHERE OutLeadEventID = @OutLeadEventID
		
	END
	
	SELECT	DSPQueueID
	FROM	DocumentSignProviderQueue
	WHERE	OutLeadEventID = @OutLeadEventID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__SendToQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__SendToQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__SendToQueue] TO [sp_executeall]
GO
