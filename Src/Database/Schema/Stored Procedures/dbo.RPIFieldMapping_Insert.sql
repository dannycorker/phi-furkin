SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_Insert]
(

	@RPIFieldMappingID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@RPIFieldID int   ,

	@DetailFieldID int   ,

	@IsEnabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@ColumnDetailFieldID int   
)
AS


				
				INSERT INTO [dbo].[RPIFieldMapping]
					(
					[ClientID]
					,[LeadTypeID]
					,[RPIFieldID]
					,[DetailFieldID]
					,[IsEnabled]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[ColumnDetailFieldID]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@RPIFieldID
					,@DetailFieldID
					,@IsEnabled
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@ColumnDetailFieldID
					)
				-- Get the identity value
				SET @RPIFieldMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Insert] TO [sp_executeall]
GO
