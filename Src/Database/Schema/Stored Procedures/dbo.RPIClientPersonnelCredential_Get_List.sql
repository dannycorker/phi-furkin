SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the RPIClientPersonnelCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientPersonnelCredential_Get_List]

AS


				
				SELECT
					[RPIClientPersonnelCredentialID],
					[ClientID],
					[ClientPersonnelID],
					[UserName],
					[Password],
					[AsUser],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[RPIClientPersonnelCredential] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientPersonnelCredential_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Get_List] TO [sp_executeall]
GO
