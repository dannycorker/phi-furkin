SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Substitutions table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Substitutions_Find]
(

	@SearchUsingOR bit   = null ,

	@SubstitutionsID int   = null ,

	@SubstitutionsVariable varchar (50)  = null ,

	@SubstitutionsDisplayName varchar (50)  = null ,

	@SubstitutionsDescription varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SubstitutionsID]
	, [SubstitutionsVariable]
	, [SubstitutionsDisplayName]
	, [SubstitutionsDescription]
    FROM
	[dbo].[Substitutions] WITH (NOLOCK) 
    WHERE 
	 ([SubstitutionsID] = @SubstitutionsID OR @SubstitutionsID IS NULL)
	AND ([SubstitutionsVariable] = @SubstitutionsVariable OR @SubstitutionsVariable IS NULL)
	AND ([SubstitutionsDisplayName] = @SubstitutionsDisplayName OR @SubstitutionsDisplayName IS NULL)
	AND ([SubstitutionsDescription] = @SubstitutionsDescription OR @SubstitutionsDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SubstitutionsID]
	, [SubstitutionsVariable]
	, [SubstitutionsDisplayName]
	, [SubstitutionsDescription]
    FROM
	[dbo].[Substitutions] WITH (NOLOCK) 
    WHERE 
	 ([SubstitutionsID] = @SubstitutionsID AND @SubstitutionsID is not null)
	OR ([SubstitutionsVariable] = @SubstitutionsVariable AND @SubstitutionsVariable is not null)
	OR ([SubstitutionsDisplayName] = @SubstitutionsDisplayName AND @SubstitutionsDisplayName is not null)
	OR ([SubstitutionsDescription] = @SubstitutionsDescription AND @SubstitutionsDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Substitutions_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Find] TO [sp_executeall]
GO
