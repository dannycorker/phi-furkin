SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerOffice table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_GetByOfficeID]
(

	@OfficeID int   
)
AS


				SELECT
					[OfficeID],
					[ClientID],
					[CustomerID],
					[OfficeName],
					[MainPhoneNumber],
					[FaxNumber],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Postcode],
					[Country],
					[Notes],
					[CountryID]
				FROM
					[dbo].[CustomerOffice] WITH (NOLOCK) 
				WHERE
										[OfficeID] = @OfficeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_GetByOfficeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_GetByOfficeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_GetByOfficeID] TO [sp_executeall]
GO
