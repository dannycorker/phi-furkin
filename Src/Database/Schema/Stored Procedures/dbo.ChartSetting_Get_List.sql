SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ChartSetting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSetting_Get_List]

AS


				
				SELECT
					[ChartSettingID],
					[ChartID],
					[SettingName],
					[SettingValue],
					[ClientID]
				FROM
					[dbo].[ChartSetting] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSetting_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Get_List] TO [sp_executeall]
GO
