SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-04-2013
-- Description:	Gets The SMSQuestions for a given group in ascending order
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestion__GetBySMSQuestionGroupIDInOrder]

	@SMSQuestionGroupID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT q.* FROM SMSQuestionGroupMember g WITH (NOLOCK) 
	INNER JOIN dbo.SMSQuestion q WITH (NOLOCK) ON q.SMSQuestionID = g.SMSQuestionID
	WHERE g.SMSQuestionGroupID=@SMSQuestionGroupID order by QuestionOrder asc
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__GetBySMSQuestionGroupIDInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion__GetBySMSQuestionGroupIDInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__GetBySMSQuestionGroupIDInOrder] TO [sp_executeall]
GO
