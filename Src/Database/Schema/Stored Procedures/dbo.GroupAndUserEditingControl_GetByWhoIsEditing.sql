SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupAndUserEditingControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupAndUserEditingControl_GetByWhoIsEditing]
(

	@WhoIsEditing int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EditingID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[WhoIsEditing],
					[EditStartedAt]
				FROM
					[dbo].[GroupAndUserEditingControl] WITH (NOLOCK) 
				WHERE
					[WhoIsEditing] = @WhoIsEditing
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_GetByWhoIsEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupAndUserEditingControl_GetByWhoIsEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_GetByWhoIsEditing] TO [sp_executeall]
GO
