SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientOfficeIPAddresses table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_Update]
(

	@ClientOfficeIpAddressID int   ,

	@ClientOfficeID int   ,

	@ClientOfficeIPAddress char (15)  ,

	@ClientID int   ,

	@Notes varchar (200)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientOfficeIPAddresses]
				SET
					[ClientOfficeID] = @ClientOfficeID
					,[ClientOfficeIPAddress] = @ClientOfficeIPAddress
					,[ClientID] = @ClientID
					,[Notes] = @Notes
				WHERE
[ClientOfficeIpAddressID] = @ClientOfficeIpAddressID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Update] TO [sp_executeall]
GO
