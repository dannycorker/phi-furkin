SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PortalUserGroupPanelItemChartingAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_Get_List]

AS


				
				SELECT
					[PortalUserGroupPanelItemChartingAccessID],
					[ClientID],
					[PortalUserGroupID],
					[PanelItemChartingID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupPanelItemChartingAccess] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Get_List] TO [sp_executeall]
GO
