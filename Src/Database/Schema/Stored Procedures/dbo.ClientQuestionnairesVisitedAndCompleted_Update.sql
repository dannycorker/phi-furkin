SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientQuestionnairesVisitedAndCompleted table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedAndCompleted_Update]
(

	@ClientQuestionnairesVisitedAndCompletedID int   ,

	@ClientQuestionnaireID int   ,

	@VisitedDate datetime   ,

	@IPAddress varchar (50)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientQuestionnairesVisitedAndCompleted]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[VisitedDate] = @VisitedDate
					,[IPAddress] = @IPAddress
					,[ClientID] = @ClientID
				WHERE
[ClientQuestionnairesVisitedAndCompletedID] = @ClientQuestionnairesVisitedAndCompletedID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_Update] TO [sp_executeall]
GO
