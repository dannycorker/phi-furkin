SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentMRTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentMRTarget_GetByDocumentMRTargetID]
(

	@DocumentMRTargetID int   
)
AS


				SELECT
					[DocumentMRTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[ObjectName],
					[PropertyName],
					[Notes],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentMRTarget] WITH (NOLOCK) 
				WHERE
										[DocumentMRTargetID] = @DocumentMRTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_GetByDocumentMRTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget_GetByDocumentMRTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_GetByDocumentMRTargetID] TO [sp_executeall]
GO
