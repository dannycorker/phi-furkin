SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2012-03-14
-- Description:	Fetch helper fields for an event, along with any known values
-- =============================================
CREATE PROCEDURE [dbo].[GetHelperFieldsWithValues]  
	@EventTypeID INT, 
	@LeadID INT, 
	@CaseID INT,	
	@ClientPersonnelID INT 
AS
BEGIN

	DECLARE @OutputTable TABLE 
	(
		MissingType VARCHAR(100), 
		FieldCaption VARCHAR(200), 
		MatterRefLetter VARCHAR(10), 
		DetailFieldID INT, 
		MatterID INT, 
		PageDisplayOrder INT, 
		FieldDisplayOrder INT, 
		DetailValue VARCHAR(2000)

	)
	
	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 12
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	

		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, DetailValue)
		SELECT 'Client Details' AS [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' AS [FieldCaption], '' AS [MatterRefLetter], df.DetailFieldID, 0 AS [MatterID], dfp.PageOrder AS [PageDisplayOrder], df.FieldOrder AS [FieldDisplayOrder], dv.DetailValue
		FROM dbo.EventTypeHelperField ethf (NOLOCK) 
		INNER JOIN dbo.DetailFields df (NOLOCK) ON ethf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		LEFT JOIN dbo.ClientDetailValues dv WITH (NOLOCK) ON dv.DetailFieldID = ethf.DetailFieldID AND dv.ClientID = ethf.ClientID
		WHERE ethf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 12
		AND df.Enabled = 1 
	
	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 13
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, DetailValue)
		SELECT 'Client Personnel Details' AS [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' AS [FieldCaption], '' AS [MatterRefLetter], df.DetailFieldID, 0 AS [MatterID], dfp.PageOrder AS [PageDisplayOrder], df.FieldOrder AS [FieldDisplayOrder], dv.DetailValue
		FROM dbo.EventTypeHelperField ethf (NOLOCK) 
		INNER JOIN dbo.DetailFields df (NOLOCK) ON ethf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		LEFT JOIN dbo.ClientPersonnelDetailValues dv WITH (NOLOCK) ON dv.DetailFieldID = ethf.DetailFieldID AND dv.ClientPersonnelID = @ClientPersonnelID 
		WHERE ethf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 13
		AND df.Enabled = 1 	
	
	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 1
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN

		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, DetailValue)
		SELECT 'Lead Details' AS [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' AS [FieldCaption], '' AS [MatterRefLetter], df.DetailFieldID, 0 AS [MatterID], dfp.PageOrder AS [PageDisplayOrder], df.FieldOrder AS [FieldDisplayOrder], dv.DetailValue
		FROM dbo.EventTypeHelperField ethf (NOLOCK) 
		INNER JOIN dbo.DetailFields df (NOLOCK) ON ethf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		LEFT JOIN dbo.LeadDetailValues dv WITH (NOLOCK) ON dv.DetailFieldID = ethf.DetailFieldID AND dv.LeadID = @LeadID
		WHERE ethf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 1
		AND df.Enabled = 1 
	
	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 10
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, DetailValue)
		SELECT 'Customer Details' AS [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' AS [FieldCaption], '' AS [MatterRefLetter], df.DetailFieldID, 0 AS [MatterID], dfp.PageOrder AS [PageDisplayOrder], df.FieldOrder AS [FieldDisplayOrder], dv.DetailValue
		FROM dbo.EventTypeHelperField ethf (NOLOCK) 
		INNER JOIN dbo.DetailFields df (NOLOCK) ON ethf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		CROSS JOIN dbo.Lead l1 (NOLOCK) 
		LEFT JOIN dbo.CustomerDetailValues dv WITH (NOLOCK) ON dv.DetailFieldID = ethf.DetailFieldID AND dv.CustomerID = l1.CustomerID
		WHERE ethf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 10
		AND df.Enabled = 1 
		AND l1.LeadID = @LeadID

	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 11
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, DetailValue)
		SELECT 'Case Details' AS [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' AS [FieldCaption], '' AS [MatterRefLetter], df.DetailFieldID, 0 AS [MatterID], dfp.PageOrder AS [PageDisplayOrder], df.FieldOrder AS [FieldDisplayOrder], dv.DetailValue
		FROM dbo.EventTypeHelperField ethf (NOLOCK) 
		INNER JOIN dbo.DetailFields df (NOLOCK) ON ethf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		LEFT JOIN dbo.CaseDetailValues dv WITH (NOLOCK) ON dv.DetailFieldID = ethf.DetailFieldID AND dv.CaseID = @CaseID
		WHERE ethf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 11
		AND df.Enabled = 1 	
	
	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 2
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, DetailValue)
		SELECT 'Matter Details' AS [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' AS [FieldCaption], m1.RefLetter AS [MatterRefLetter], df.DetailFieldID, m1.MatterID AS [MatterID], dfp.PageOrder AS [PageDisplayOrder], df.FieldOrder AS [FieldDisplayOrder], dv.DetailValue
		FROM dbo.EventTypeHelperField ethf (NOLOCK) 
		INNER JOIN dbo.DetailFields df (NOLOCK) ON ethf.DetailFieldID = df.DetailFieldID
		INNER JOIN dbo.DetailFieldPages dfp (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		CROSS JOIN dbo.Matter m1 (NOLOCK) 
		LEFT JOIN dbo.MatterDetailValues dv WITH (NOLOCK) ON dv.DetailFieldID = ethf.DetailFieldID AND dv.MatterID = m1.MatterID
		WHERE ethf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 2
		AND df.Enabled = 1 
		AND m1.LeadID = @LeadID
		AND m1.CaseID = @CaseID
	END

	SELECT *
	FROM @OutputTable
	ORDER BY [MissingType], [MatterRefLetter], [PageDisplayOrder], [FieldDisplayOrder]
	
	/* Some callers need the count back as the return code */
	RETURN @@ROWCOUNT

END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetHelperFieldsWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetHelperFieldsWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetHelperFieldsWithValues] TO [sp_executeall]
GO
