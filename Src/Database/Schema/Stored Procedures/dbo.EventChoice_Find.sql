SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventChoice table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice_Find]
(

	@SearchUsingOR bit   = null ,

	@EventChoiceID int   = null ,

	@EventTypeID int   = null ,

	@Description varchar (50)  = null ,

	@NextEventTypeID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@ThreadNumber int   = null ,

	@EscalationEvent bit   = null ,

	@Field varchar (50)  = null ,

	@LogicalOperator varchar (50)  = null ,

	@Value1 varchar (50)  = null ,

	@Value2 varchar (50)  = null ,

	@SqlClauseForInclusion varchar (2000)  = null ,

	@Weighting int   = null ,

	@ValueTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventChoiceID]
	, [EventTypeID]
	, [Description]
	, [NextEventTypeID]
	, [ClientID]
	, [LeadTypeID]
	, [ThreadNumber]
	, [EscalationEvent]
	, [Field]
	, [LogicalOperator]
	, [Value1]
	, [Value2]
	, [SqlClauseForInclusion]
	, [Weighting]
	, [ValueTypeID]
    FROM
	[dbo].[EventChoice] WITH (NOLOCK) 
    WHERE 
	 ([EventChoiceID] = @EventChoiceID OR @EventChoiceID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([NextEventTypeID] = @NextEventTypeID OR @NextEventTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ThreadNumber] = @ThreadNumber OR @ThreadNumber IS NULL)
	AND ([EscalationEvent] = @EscalationEvent OR @EscalationEvent IS NULL)
	AND ([Field] = @Field OR @Field IS NULL)
	AND ([LogicalOperator] = @LogicalOperator OR @LogicalOperator IS NULL)
	AND ([Value1] = @Value1 OR @Value1 IS NULL)
	AND ([Value2] = @Value2 OR @Value2 IS NULL)
	AND ([SqlClauseForInclusion] = @SqlClauseForInclusion OR @SqlClauseForInclusion IS NULL)
	AND ([Weighting] = @Weighting OR @Weighting IS NULL)
	AND ([ValueTypeID] = @ValueTypeID OR @ValueTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventChoiceID]
	, [EventTypeID]
	, [Description]
	, [NextEventTypeID]
	, [ClientID]
	, [LeadTypeID]
	, [ThreadNumber]
	, [EscalationEvent]
	, [Field]
	, [LogicalOperator]
	, [Value1]
	, [Value2]
	, [SqlClauseForInclusion]
	, [Weighting]
	, [ValueTypeID]
    FROM
	[dbo].[EventChoice] WITH (NOLOCK) 
    WHERE 
	 ([EventChoiceID] = @EventChoiceID AND @EventChoiceID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([NextEventTypeID] = @NextEventTypeID AND @NextEventTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ThreadNumber] = @ThreadNumber AND @ThreadNumber is not null)
	OR ([EscalationEvent] = @EscalationEvent AND @EscalationEvent is not null)
	OR ([Field] = @Field AND @Field is not null)
	OR ([LogicalOperator] = @LogicalOperator AND @LogicalOperator is not null)
	OR ([Value1] = @Value1 AND @Value1 is not null)
	OR ([Value2] = @Value2 AND @Value2 is not null)
	OR ([SqlClauseForInclusion] = @SqlClauseForInclusion AND @SqlClauseForInclusion is not null)
	OR ([Weighting] = @Weighting AND @Weighting is not null)
	OR ([ValueTypeID] = @ValueTypeID AND @ValueTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Find] TO [sp_executeall]
GO
