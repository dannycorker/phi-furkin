SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumOption table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_GetByAquariumOptionTypeID]
(

	@AquariumOptionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AquariumOptionID],
					[AquariumOptionTypeID],
					[AquariumOptionName],
					[AquariumOptionDescription]
				FROM
					[dbo].[AquariumOption] WITH (NOLOCK) 
				WHERE
					[AquariumOptionTypeID] = @AquariumOptionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_GetByAquariumOptionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_GetByAquariumOptionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_GetByAquariumOptionTypeID] TO [sp_executeall]
GO
