SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DashboardItemType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_Delete]
(

	@DashboardItemTypeID int   
)
AS


				DELETE FROM [dbo].[DashboardItemType] WITH (ROWLOCK) 
				WHERE
					[DashboardItemTypeID] = @DashboardItemTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Delete] TO [sp_executeall]
GO
