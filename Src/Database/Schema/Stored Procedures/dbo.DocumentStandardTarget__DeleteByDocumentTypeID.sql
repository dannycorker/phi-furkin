SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-07-2010
-- Description:	Deletes all the document standard target 
--              records that have the give document type id
-- =============================================
CREATE PROCEDURE [dbo].[DocumentStandardTarget__DeleteByDocumentTypeID]
(

	@DocumentTypeID int   
)
AS


				DELETE FROM [dbo].[DocumentStandardTarget] WITH (ROWLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID AND DocumentTypeVersionID IS NULL




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget__DeleteByDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget__DeleteByDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget__DeleteByDocumentTypeID] TO [sp_executeall]
GO
