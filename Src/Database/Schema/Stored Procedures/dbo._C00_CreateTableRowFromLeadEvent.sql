SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-10-09
-- Description:	Create a full table row from a LeadEvent, updating SourceID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateTableRowFromLeadEvent]
(
 	 @LeadEventID			INT
 	,@TableDetailFieldID	INT
 	,@DenyEdit				BIT
 	,@DenyDelete			BIT
)
AS
BEGIN
PRINT OBJECT_NAME(@@ProcID)

	DECLARE	 @TableRowID		INT
			,@LeadOrMatter		INT
			,@ObjectID			INT
			,@CustomerID		INT
			,@LeadID			INT
			,@CaseID			INT
			,@MatterID			INT
			,@ClientPersonnelID	INT
			,@ClientID			INT
			,@DetailFieldPageID	INT
			,@ErrorMessage		VARCHAR(2000) = ''
	
	SELECT   @LeadOrMatter = df.LeadOrMatter
			,@DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH ( NOLOCK ) 
	WHERE df.DetailFieldID = @TableDetailFieldID
	AND df.QuestionTypeID IN ( 16 /*Table*/ ,19 /*Basic Table*/ )
	
	/*Pick up the appropriate object IDs*/
	SELECT TOP 1	 @CustomerID 		= CASE WHEN @LeadOrMatter = 10 THEN m.CustomerID	END 
					,@LeadID	 		= CASE WHEN @LeadOrMatter =  1 THEN m.LeadID		END 
					,@CaseID	 		= CASE WHEN @LeadOrMatter = 11 THEN m.CaseID		END 
					,@MatterID	 		= CASE WHEN @LeadOrMatter =  2 THEN m.MatterID		END 
					,@ClientPersonnelID = CASE WHEN @LeadOrMatter = 13 THEN le.WhoCreated	END 
					,@ClientID			= m.ClientID
	FROM LeadEvent le WITH ( NOLOCK ) 
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = le.CaseID
	WHERE le.LeadEventID = @LeadEventID
	ORDER BY m.MatterID
	
	IF @ClientID <> 0
	BEGIN
	
		/*Create the new row.  Most objectIDs will be NULL*/
		INSERT TableRows ( ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, CustomerID, CaseID, ClientPersonnelID, SourceID )
		VALUES ( @ClientID, @LeadID, @MatterID, @TableDetailFieldID, @DetailFieldPageID, @CustomerID, @CaseID, @ClientPersonnelID, @LeadEventID )
		
		/*Pick up the new ID*/
		SELECT @TableRowID = SCOPE_IDENTITY()
		
		/*Fill in all of the TableDetailValues, and @DenyEdit/@DenyDelete*/
		EXEC _C00_CompleteTableRow @TableRowID, @MatterID, @DenyEdit, @DenyDelete	
	END
	ELSE
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No match found for LeadEventID ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL') + ', LeadOrMatter ' + ISNULL(CONVERT(VARCHAR,@LeadOrMatter),'NULL') + ', TableDetailField ' + ISNULL(CONVERT(VARCHAR,@TableDetailFieldID),'NULL') + ' </font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	RETURN @TableRowID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateTableRowFromLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateTableRowFromLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateTableRowFromLeadEvent] TO [sp_executeall]
GO
