SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderLog table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderLog_GetByDataLoaderLogID]
(

	@DataLoaderLogID int   
)
AS


				SELECT
					[DataLoaderLogID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderMapSectionID],
					[DataLoaderFileID],
					[DataLoaderFieldDefinitionID],
					[DataLoaderObjectFieldID],
					[DetailFieldID],
					[ObjectName],
					[RowIndex],
					[ColIndex],
					[NodeName],
					[Message],
					[LogLevel]
				FROM
					[dbo].[DataLoaderLog] WITH (NOLOCK) 
				WHERE
										[DataLoaderLogID] = @DataLoaderLogID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_GetByDataLoaderLogID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderLog_GetByDataLoaderLogID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_GetByDataLoaderLogID] TO [sp_executeall]
GO
