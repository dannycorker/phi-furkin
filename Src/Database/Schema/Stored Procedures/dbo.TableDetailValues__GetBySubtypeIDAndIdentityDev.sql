SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-12
-- Description:	Gets a list of table detail values based upon subtype id and the given identity
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityDev]
(
	@tableRowID int, 
	@identity int, 
	@detailFieldSubTypeID int, 
	@detailFieldID int, 		
	@clientID int
)

AS

	SELECT * FROM TableDetailValues WHERE ClientID = @ClientID

	/*declare @simpleClause varchar(max) = '',
	@query VARCHAR(MAX) = ''
	
	SELECT @simpleClause = CASE 
		WHEN @detailFieldSubTypeID = 1 THEN ' AND (td.LeadID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 2 THEN ' AND (td.MatterID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 10 THEN ' AND (td.CustomerID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 11 THEN ' AND (td.CaseID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 12 THEN 'AND (td.ClientID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 13 THEN ' AND (td.ClientPersonnelID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 14 THEN ' AND (td.ContactID = ' + Cast(@identity as VarChar) + ')'
	END

    SET @query = 'SELECT * 
    FROM
	[dbo].[TableDetailValues] td WITH (NOLOCK) 
    WHERE 	 
	([TableRowID] = ' + Cast(@TableRowID as VarChar) + ')	
	AND ([DetailFieldID] = ' + Cast(@DetailFieldID as VarChar) + ')	
	AND ([ClientID] = ' + Cast(@ClientID as VarChar) + ') ' + @simpleClause

	exec (@query)*/
						
	Select @@ROWCOUNT		




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityDev] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityDev] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityDev] TO [sp_executeall]
GO
