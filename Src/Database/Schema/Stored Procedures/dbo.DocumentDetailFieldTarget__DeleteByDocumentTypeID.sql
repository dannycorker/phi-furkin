SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-07-2010
-- Description:	Deletes all the document detail field target 
--              records that have the give document type id
-- =============================================
CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeID]
(

	@DocumentTypeID int   
)
AS


				DELETE FROM [dbo].[DocumentDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID AND DocumentTypeVersionID IS NULL




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeID] TO [sp_executeall]
GO
