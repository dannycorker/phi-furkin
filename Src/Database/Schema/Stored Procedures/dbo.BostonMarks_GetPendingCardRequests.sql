SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 27/08/2010
-- Description:	Returns all pending card requests for boston marks clients
-- IS: 2014-03-04: Added CustomerType Security
-- =============================================
CREATE PROCEDURE [dbo].[BostonMarks_GetPendingCardRequests] 
	@ClientID INT,
	@DateRequestedDetailFieldID INT,
	@StatusDetailFieldID INT,
	@StatusPendingLookupListID INT,
	@SubClientID INT = 1,
	@ClientPersonnelID INT = NULL
AS
BEGIN
	SELECT c.CustomerID, c.FullName AS Customer, c.Town, c.PostCode, dvRequested.DetailValue AS [Date Requested], ll.ItemValue AS [Card Status], m.LeadID, m.MatterID, m.CaseID,
			ISNULL(dvErrorCode.DetailValue, '') AS ErrorCode, ISNULL(dvErrorDescription.DetailValue, '') AS ErrorDescription, ISNULL(dvResult.DetailValue, '') AS Result,
			ISNULL(dvSent.DetailValue, '') As [DateSent], c.CustomerRef AS [CustomerRef], dvWhoRequested.DetailValue AS [WhoRequested], dvLineOne.DetailValue Personalisation,
			ISNULL(o.Name, 'Not assigned') As [CustomerType]
	FROM dbo.Customers c WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CustomerID = m.CustomerID 
	INNER JOIN dbo.MatterDetailValues dvRequested WITH (NOLOCK) ON m.MatterID = dvRequested.MatterID AND dvRequested.DetailFieldID = 117772
	INNER JOIN dbo.MatterDetailValues dvStatus WITH (NOLOCK) ON m.MatterID = dvStatus.MatterID AND dvStatus.DetailFieldID = 117802
	INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON dvStatus.DetailValue = ll.LookupListItemID
	LEFT JOIN dbo.MatterDetailValues dvResponse WITH (NOLOCK) ON m.MatterID = dvResponse.MatterID AND dvResponse.DetailFieldID = 121311 -- Response date
	LEFT JOIN dbo.MatterDetailValues dvResult WITH (NOLOCK) ON m.MatterID = dvResult.MatterID AND dvResult.DetailFieldID = 121310 -- Result 0 = fail, 1 = win!
	LEFT JOIN dbo.MatterDetailValues dvErrorCode WITH (NOLOCK) ON m.MatterID = dvErrorCode.MatterID AND dvErrorCode.DetailFieldID = 121312 -- Error code
	LEFT JOIN dbo.MatterDetailValues dvErrorDescription WITH (NOLOCK) ON m.MatterID = dvErrorDescription.MatterID AND dvErrorDescription.DetailFieldID = 121313 -- Error description
	LEFT JOIN dbo.MatterDetailValues dvSent WITH (NOLOCK) ON m.MatterID = dvSent.MatterID AND dvSent.DetailFieldID = 122101 -- Date Sent
	--INNER JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON m.LeadID = ldv.LeadID AND ldv.DetailFieldID = 117895 -- Customer Ref 
	LEFT OUTER JOIN dbo.MatterDetailValues dvWhoRequested WITH (NOLOCK) ON m.MatterID = dvWhoRequested.MatterID AND dvWhoRequested.DetailFieldID = 122287 -- Who requested
	LEFT OUTER JOIN dbo.MatterDetailValues dvLineOne WITH (NOLOCK) ON m.MatterID = dvLineOne.MatterID AND dvLineOne.DetailFieldID = 117546
	LEFT JOIN dbo.MatterDetailValues mdvCustomerType WITH (NOLOCK) ON mdvCustomerType.MatterID = m.MatterID AND mdvCustomerType.DetailFieldID = 173559
	LEFT JOIN Objects o WITH (NOLOCK) ON o.ObjectID = mdvCustomerType.ValueInt

	WHERE c.ClientID = 183
	AND (c.Test IS NULL OR c.Test = 0)
	AND c.SubClientID = @SubClientID
	AND (
			( /*CS 2014-06-05.  Remove this requirement for SubClient 91 only*/
			(ISNULL(dvResponse.DetailValue, '') = '' OR
			(ISNULL(dvResponse.DetailValue, '') != '' AND dvResult.DetailValue = '0'))
			)

		)
	/*KN 2014-11-24 Added New subclients to the list */
	--AND ( dvSent.ValueDate IS NULL OR @SubClientID <> 91 )
	AND ( dvSent.ValueDate IS NULL OR ISNULL(dbo.fnGetRLDvAsInt(117771,176066,m.CaseID),0) = 0 )
	AND ( 
		@ClientPersonnelID IS NULL 
		OR 
		mdvCustomerType.ValueInt IN (
			SELECT  FromObjectID
			FROM	ObjectDetailValues o WITH (NOLOCK) 
			INNER JOIN ObjectLink ol WITH (NOLOCK) ON o.ObjectID = ol.ToObjectID AND ObjectTypeRelationshipID = 26
			WHERE	o.DetailFieldID = 173560
			AND		o.ValueInt = @ClientPersonnelID
			UNION ALL
			SELECT 0
		)
	)
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_GetPendingCardRequests] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BostonMarks_GetPendingCardRequests] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_GetPendingCardRequests] TO [sp_executeall]
GO
