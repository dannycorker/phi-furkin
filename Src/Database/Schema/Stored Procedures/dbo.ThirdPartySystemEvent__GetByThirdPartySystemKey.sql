SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-10-2012
-- Description:	Gets a ThirdPartySystemEvent with the given LeadTypeID and [ThirdPartySystemKey]
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemKey]
	@ClientID INT,
	@LeadTypeID INT,
	@ThirdPartySystemID INT,
	@ThirdPartySystemKey INT
AS
BEGIN

	SELECT * FROM ThirdPartySystemEvent WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND
		  LeadTypeID = @LeadTypeID AND
		  ThirdPartySystemID = @ThirdPartySystemID AND
		  ThirdPartySystemKey = @ThirdPartySystemKey

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemKey] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemKey] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByThirdPartySystemKey] TO [sp_executeall]
GO
