SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Messages table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_GetByMessageID]
(

	@MessageID int   
)
AS


				SELECT
					[MessageID],
					[ClientPersonnelIDTo],
					[ClientPersonnelIDFrom],
					[DateSent],
					[DateRead],
					[Status],
					[Subject],
					[MessageText],
					[DateReplied],
					[DateForwarded],
					[PreviousMessageID],
					[ClientID],
					[PortalUserIDTo],
					[PortalUserIDFrom]
				FROM
					[dbo].[Messages] WITH (NOLOCK) 
				WHERE
										[MessageID] = @MessageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_GetByMessageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_GetByMessageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_GetByMessageID] TO [sp_executeall]
GO
