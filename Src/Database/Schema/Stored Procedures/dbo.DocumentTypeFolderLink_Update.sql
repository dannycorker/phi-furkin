SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentTypeFolderLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink_Update]
(

	@DocumentTypeID int   ,

	@OriginalDocumentTypeID int   ,

	@FolderID int   ,

	@OriginalFolderID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentTypeFolderLink]
				SET
					[DocumentTypeID] = @DocumentTypeID
					,[FolderID] = @FolderID
				WHERE
[DocumentTypeID] = @OriginalDocumentTypeID 
AND [FolderID] = @OriginalFolderID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Update] TO [sp_executeall]
GO
