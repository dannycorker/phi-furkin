SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget_Delete]
(

	@ScriptDetailFieldTargetID int   
)
AS


				DELETE FROM [dbo].[ScriptDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[ScriptDetailFieldTargetID] = @ScriptDetailFieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Delete] TO [sp_executeall]
GO
