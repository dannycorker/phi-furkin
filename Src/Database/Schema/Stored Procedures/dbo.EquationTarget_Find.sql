SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EquationTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@EquationTargetID int   = null ,

	@ClientID int   = null ,

	@EquationDetailFieldID int   = null ,

	@DetailFieldID int   = null ,

	@Target varchar (250)  = null ,

	@ObjectName varchar (250)  = null ,

	@PropertyName varchar (250)  = null ,

	@SqlFunctionID int   = null ,

	@IsEquation bit   = null ,

	@IsColumnSum bit   = null ,

	@ColumnFieldID int   = null ,

	@DetailFieldSubTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EquationTargetID]
	, [ClientID]
	, [EquationDetailFieldID]
	, [DetailFieldID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [SqlFunctionID]
	, [IsEquation]
	, [IsColumnSum]
	, [ColumnFieldID]
	, [DetailFieldSubTypeID]
    FROM
	[dbo].[EquationTarget] WITH (NOLOCK) 
    WHERE 
	 ([EquationTargetID] = @EquationTargetID OR @EquationTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EquationDetailFieldID] = @EquationDetailFieldID OR @EquationDetailFieldID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([SqlFunctionID] = @SqlFunctionID OR @SqlFunctionID IS NULL)
	AND ([IsEquation] = @IsEquation OR @IsEquation IS NULL)
	AND ([IsColumnSum] = @IsColumnSum OR @IsColumnSum IS NULL)
	AND ([ColumnFieldID] = @ColumnFieldID OR @ColumnFieldID IS NULL)
	AND ([DetailFieldSubTypeID] = @DetailFieldSubTypeID OR @DetailFieldSubTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EquationTargetID]
	, [ClientID]
	, [EquationDetailFieldID]
	, [DetailFieldID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [SqlFunctionID]
	, [IsEquation]
	, [IsColumnSum]
	, [ColumnFieldID]
	, [DetailFieldSubTypeID]
    FROM
	[dbo].[EquationTarget] WITH (NOLOCK) 
    WHERE 
	 ([EquationTargetID] = @EquationTargetID AND @EquationTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EquationDetailFieldID] = @EquationDetailFieldID AND @EquationDetailFieldID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([SqlFunctionID] = @SqlFunctionID AND @SqlFunctionID is not null)
	OR ([IsEquation] = @IsEquation AND @IsEquation is not null)
	OR ([IsColumnSum] = @IsColumnSum AND @IsColumnSum is not null)
	OR ([ColumnFieldID] = @ColumnFieldID AND @ColumnFieldID is not null)
	OR ([DetailFieldSubTypeID] = @DetailFieldSubTypeID AND @DetailFieldSubTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Find] TO [sp_executeall]
GO
