SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*
----------------------------------------------------------------------------------------------------

-- Created By:  ()
-- Purpose: Select records from the DetailFieldAlias table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias__GetAvailableFieldAliases]
(

	@ClientID int,
	@LeadTypeID int,
	@DetailFieldID int = null
)
AS


				SET ANSI_NULLS OFF
				
				SELECT distinct [DetailFieldAlias]
				FROM [dbo].[DetailFieldAlias] WITH (NOLOCK)
				WHERE [ClientID] = @ClientID
					AND 
					(
						(
							(@LeadTypeID is null AND LeadTypeID is not null) OR (LeadTypeID <> @LeadTypeID OR LeadTypeID is null)
						)
						OR 
						(
							@DetailFieldID is null OR  DetailFieldID = @DetailFieldID
						)
					)
					AND [DetailFieldAlias] 
						NOT IN
						(
							SELECT [DetailFieldAlias] 
							FROM [DetailFieldAlias] WITH (NOLOCK)
							WHERE
							(
								(@LeadTypeID is null AND LeadTypeID is null) 
								OR 
								LeadTypeID = @LeadTypeID
							)
							AND
							(
								@DetailFieldID is null 
								OR  
								DetailFieldID <> @DetailFieldID
							)
							AND [ClientID] = @ClientID
						)
				ORDER BY 1


				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			










GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias__GetAvailableFieldAliases] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias__GetAvailableFieldAliases] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias__GetAvailableFieldAliases] TO [sp_executeall]
GO
