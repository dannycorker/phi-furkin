SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_GetByDocumentTypeID]
(

	@DocumentTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSQuestionID],
					[ClientID],
					[SMSQuestionnaireID],
					[Question],
					[QuestionOrder],
					[DocumentTypeID],
					[Enabled],
					[Timeout],
					[Repeat],
					[RepeatPrefix],
					[DetailFieldToSave],
					[EvaluateResponse],
					[VoiceAction],
					[RedirectTo],
					[GatherNumDigits],
					[RecordingMaxLength]
				FROM
					[dbo].[SMSQuestion] WITH (NOLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_GetByDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_GetByDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_GetByDocumentTypeID] TO [sp_executeall]
GO
