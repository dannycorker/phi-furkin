SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptNoteTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_GetByScriptID]
(

	@ScriptID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptNoteTargetID],
					[ScriptID],
					[ClientID],
					[NoteTypeID],
					[PriorityID],
					[ApplyToAllCases]
				FROM
					[dbo].[ScriptNoteTarget] WITH (NOLOCK) 
				WHERE
					[ScriptID] = @ScriptID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_GetByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_GetByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_GetByScriptID] TO [sp_executeall]
GO
