SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the HyperlinkStandardTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@HyperlinkStandardTargetID int   = null ,

	@ClientID int   = null ,

	@HyperlinkDetailFieldID int   = null ,

	@Target varchar (250)  = null ,

	@ObjectName varchar (250)  = null ,

	@PropertyName varchar (250)  = null ,

	@TemplateTypeID int   = null ,

	@Notes varchar (250)  = null ,

	@IsSpecial bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [HyperlinkStandardTargetID]
	, [ClientID]
	, [HyperlinkDetailFieldID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [TemplateTypeID]
	, [Notes]
	, [IsSpecial]
    FROM
	[dbo].[HyperlinkStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([HyperlinkStandardTargetID] = @HyperlinkStandardTargetID OR @HyperlinkStandardTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([HyperlinkDetailFieldID] = @HyperlinkDetailFieldID OR @HyperlinkDetailFieldID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([IsSpecial] = @IsSpecial OR @IsSpecial IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [HyperlinkStandardTargetID]
	, [ClientID]
	, [HyperlinkDetailFieldID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [TemplateTypeID]
	, [Notes]
	, [IsSpecial]
    FROM
	[dbo].[HyperlinkStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([HyperlinkStandardTargetID] = @HyperlinkStandardTargetID AND @HyperlinkStandardTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([HyperlinkDetailFieldID] = @HyperlinkDetailFieldID AND @HyperlinkDetailFieldID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([IsSpecial] = @IsSpecial AND @IsSpecial is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Find] TO [sp_executeall]
GO
