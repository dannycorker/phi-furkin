SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ActiveSession table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_Update]
(

	@SessionID varchar (50)  ,

	@EmailAddress varchar (255)  ,

	@OriginalEmailAddress varchar (255)  ,

	@ThirdPartySystemId int   ,

	@OriginalThirdPartySystemId int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ActiveSession]
				SET
					[SessionID] = @SessionID
					,[EmailAddress] = @EmailAddress
					,[ThirdPartySystemId] = @ThirdPartySystemId
				WHERE
[EmailAddress] = @OriginalEmailAddress 
AND [ThirdPartySystemId] = @OriginalThirdPartySystemId 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Update] TO [sp_executeall]
GO
