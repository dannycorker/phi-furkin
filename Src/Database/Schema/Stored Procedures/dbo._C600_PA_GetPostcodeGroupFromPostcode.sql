SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-05-31
-- Description:	Take a postcode and return its postcode group
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_GetPostcodeGroupFromPostcode]
(
	 @PostCode		VARCHAR(2000)	= 'CM18 1AB'
	,@RuleID		INT				= 8426
	,@PostCodeGroup	VARCHAR(10)		OUTPUT
)
AS
BEGIN

	DECLARE  @Overrides dbo.tvpIntVarcharVarchar
	INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
	VALUES ( 2,'PostCode',@PostCode )

	DECLARE  @Output XML

	EXEC RulesEngine_Evaluate_Rule @ID = @RuleID, @Overrides = @Overrides, @Output = @Output OUTPUT

	SELECT @PostCodeGroup = @Output.value('(Rule/@Output)[1]','VARCHAR(10)')

	RETURN

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_GetPostcodeGroupFromPostcode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_GetPostcodeGroupFromPostcode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_GetPostcodeGroupFromPostcode] TO [sp_executeall]
GO
