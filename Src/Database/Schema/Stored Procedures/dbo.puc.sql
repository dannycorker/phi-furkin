SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-12-10
-- Description:	List all Portal Users with associated Leads & Cases
-- =============================================
CREATE PROCEDURE [dbo].[puc] 
	@ClientID int = NULL,
	@PortalUserID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT puc.*, pu.ClientID, pu.CustomerID, pu.ClientPersonnelID, pu.Username, cl.CompanyName, c.Fullname 
	FROM dbo.PortalUser pu (nolock) 
	INNER JOIN dbo.PortalUserCase puc (nolock) on puc.PortalUserID = pu.PortalUserID 
	INNER JOIN dbo.Clients cl (nolock) on cl.ClientID = pu.ClientID 
	INNER JOIN dbo.Customers c (nolock) on c.CustomerID = pu.CustomerID 
	WHERE (pu.ClientID = @ClientID OR @ClientID IS NULL) 
	AND (pu.PortalUserID = @PortalUserID OR @PortalUserID IS NULL) 
	ORDER BY puc.PortalUserID, puc.LeadID, puc.CaseID  
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[puc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[puc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[puc] TO [sp_executeall]
GO
