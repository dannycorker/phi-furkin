SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- ALTER date: 2011-10-21
-- Description:	Check for AutomatedEventQueue records, lock some for this scheduler and select them for processing
-- AMG 2014-01-27 Now uses a new column on the AutomatedEventQueue table to determine which tasks to run
--				  This column is derived from a new 'Delay' column on EventTypeAutomatedEvent
-- JWG 2014-06-11 #27045 New proc AquariusMaster.dbo.GetIsMaintenanceWindow replaces the local function, to make it accessible from everywhere
--                Just add a record to the AquariusMaster.dbo.SpecialMaintenanceWindow table now if you need a long outage for any reason.
-- CS  2014-12-08 Merged with Aquarius version
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__PrepareEventsToProcess]
	@SchedulerID INT, 
	@ServerName VARCHAR(50) 
AS
BEGIN
	SET NOCOUNT ON;

	/* Avoid running any tasks during Housekeeping, Scheduler restarts or server maintenance */
	DECLARE @IsMaint BIT

	EXEC @IsMaint = [AquariusMaster].dbo.GetIsMaintenanceWindow

	/*
		JWG 2014-06-11 #27045 Use the common proc on AquariusMaster instead of each local DB having its own version.
		IF dbo.fnIsMaintenanceWindow(dbo.fn_GetDate_Local()) = 1
	*/
	IF @IsMaint = 1	
	BEGIN
		RETURN
	END

	DECLARE @MaxEventCount INT = 20, 
	@LockResult INT

	DECLARE @AutomatedEventQueues TABLE (
		AutomatedEventQueueID INT
	)

	/* Start a transaction for the benefit of sp_getapplock */
	BEGIN TRAN

	-- Use system proc sp_getapplock to limit this to one request at a time:
	-- @Resource: the unique name for this lock
	-- @LockMode: 'Shared', 'Update', 'IntentShared', 'IntentExclusive', 'Exclusive'
	-- @LockOwner: Scope of the lock: 'Transaction' or 'Session'
	-- @LockTimeout: Timeout in miliseconds
	-- @DbPrincipal: The DB Principal for access permisions ('dbo', 'public', 'guest')
	EXEC @LockResult = sp_getapplock 
		@Resource = 'AutomatedEventQueue__PrepareEventsToProcess',
		@LockMode = 'Exclusive',
		@LockOwner = 'Transaction',
		@LockTimeout = 30000,
		@DbPrincipal = 'public'

	-- 0 and 1 are the valid return values from sp_getapplock
	IF @LockResult NOT IN (0, 1)
	BEGIN
		RAISERROR ( 'AutomatedEventQueue__PrepareEventsToProcess: Unable to acquire Lock', 16, 1 )
		ROLLBACK
		RETURN
	END 
	ELSE
	BEGIN
	
		/* LB 2014-11-18 Try Automations that deadlocked again.*/
		UPDATE AutomatedEventQueue
		SET ErrorMessage = NULL, SuccessDateTime = NULL, ErrorDateTime = NULL, LockDateTime = NULL
		FROM AutomatedEventQueue WITH (NOLOCK) 
		WHERE (ErrorMessage like '%Deadlocked%' OR ErrorMessage like '%Value cannot be null%Parameter name: connection%')
		AND ErrorCount <= 5
		AND WhenCreated > dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
		
		/* 
			Get a list of events which are not already earmarked for a scheduler to do.
			/*LB 2014-05-20 Changed processing order with CTE. Clients with fewer pending events are given priority*/
		*/

		;With cte (ClientID,EventTypeID,CountOrder) as 
		(
		Select at.ClientID,at.AutomatedEventTypeID,Count(AutomatedEventQueueID) 
		FROM dbo.AutomatedEventQueue AT 
		WHERE at.LockDateTime IS NULL 
		AND at.ErrorMessage IS NULL
		AND dbo.fn_GetDate_Local() > ISNULL(AT.EarliestRunDateTime,DATEADD(MS,-10,dbo.fn_GetDate_Local()))
		GROUP BY at.ClientID,at.AutomatedEventTypeID
		)

		INSERT @AutomatedEventQueues (AutomatedEventQueueID)
		SELECT TOP (@MaxEventCount) at.AutomatedEventQueueID
		FROM dbo.AutomatedEventQueue AT 
		INNER JOIN cte on cte.ClientID = at.ClientID AND cte.EventTypeID = at.AutomatedEventTypeID
		WHERE at.LockDateTime IS NULL 
		AND at.ErrorMessage IS NULL
		/* AMG 2014-01-27 Honour the new EventTypeAutomatedEvent delay column */
		AND dbo.fn_GetDate_Local() > ISNULL(AT.EarliestRunDateTime,DATEADD(MS,-10,dbo.fn_GetDate_Local()))

		ORDER BY cte.CountOrder asc, AT.EarliestRunDateTime, AT.WhenCreated

		IF @@ROWCOUNT > 0
		BEGIN

			/*
				UPDATE the AutomatedEventQueue records that are about to be actioned.
				Use OUTPUT inserted.* clause to select the results straight back out to the waiting scheduler.
			*/
			UPDATE dbo.AutomatedEventQueue 
			SET SchedulerID = @SchedulerID, 
			ServerName = @ServerName, 
			LockDateTime = dbo.fn_GetDate_Local() 
			OUTPUT inserted.* 
			FROM dbo.AutomatedEventQueue AT 
			INNER JOIN @AutomatedEventQueues t ON t.AutomatedEventQueueID = at.AutomatedEventQueueID

		END

		/* Release the lock */
		EXEC @LockResult = sp_releaseapplock 
						@Resource = 'AutomatedEventQueue__PrepareEventsToProcess',
						@DbPrincipal = 'public',
						@LockOwner = 'Transaction'

	END

	COMMIT

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__PrepareEventsToProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__PrepareEventsToProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__PrepareEventsToProcess] TO [sp_executeall]
GO
