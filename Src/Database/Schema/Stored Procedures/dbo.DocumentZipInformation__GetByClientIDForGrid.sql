SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-26
-- Description:	Select Doc Zip Info for the app (ZipFiles.aspx)
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipInformation__GetByClientIDForGrid] 
	@ClientID int,
	@ShowDownloaded bit = 0
AS
BEGIN

	;
	WITH InnerSql AS 
	(
		/* Count all the individual zips per header record first */
		SELECT dzi.DocumentZipInformationID, count(*) as dz_count 
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		WHERE dzi.ClientID = @ClientID 
		AND dzi.StatusID IN (31, 32, 34) 
		GROUP BY dzi.DocumentZipInformationID 
	),
	IncompleteZipsByFile AS 
	(
		/* Count all the individual zips per header record first */
		SELECT i.DocumentZipInformationID, dz.DocumentZipFileID, count(*) as incomplete_count 
		FROM InnerSql i 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = i.DocumentZipInformationID AND dz.StatusID NOT IN (31) 
		GROUP BY i.DocumentZipInformationID, dz.DocumentZipFileID 
	)
	/* Now return all the other helpful info about the header record*/
	SELECT dzi.[DocumentZipInformationID], 
	dzi.[ClientID], 
	dzi.[ZipPrefix], 
	dzf.[DocumentZipFileID], 
	dzf.[FileName] as [FileName], 
	/*dzf.[ServerMapPath] as [FullFileAndPath],*/
	dzf.ZipCount as [ZipCount], 
	iz.incomplete_count as [Incomplete],
	ao.[AquariumOptionName], 
	ao.[AquariumOptionDescription], 
	CASE WHEN dzi.[WhenStored] IS NULL THEN '' ELSE convert(varchar(16), dzi.[WhenStored], 120) + ' by ' + cp_stored.UserName END as [StoredDetails], 
	CASE WHEN dzi.[WhenCreated] IS NULL THEN '' ELSE convert(varchar(16), dzi.[WhenCreated], 120) END as [CreatedDetails], 
	CASE WHEN dzf.[WhenDownloaded] IS NULL THEN '' ELSE convert(varchar(16), dzf.[WhenDownloaded], 120) + ' by ' + cp_downloaded.UserName END as [DownloadedDetails], 
	COALESCE(dzi.[EmailOnSuccess], '') as [SuccessEmailDetails], 
	COALESCE(dzi.[EmailOnError], '') as [ErrorEmailDetails] 
	FROM InnerSql i 
	INNER JOIN dbo.DocumentZipInformation dzi WITH (NOLOCK) ON dzi.DocumentZipInformationID = i.DocumentZipInformationID 
	INNER JOIN dbo.DocumentZipFile dzf WITH (NOLOCK) ON dzf.DocumentZipInformationID = i.DocumentZipInformationID 
	LEFT JOIN IncompleteZipsByFile iz ON iz.DocumentZipFileID = dzf.DocumentZipFileID 
	INNER JOIN dbo.AquariumOption ao WITH (NOLOCK) ON ao.AquariumOptionID = dzf.StatusID 
	LEFT JOIN dbo.ClientPersonnel cp_stored WITH (NOLOCK) ON cp_stored.ClientPersonnelID = dzi.WhoStored 
	LEFT JOIN dbo.ClientPersonnel cp_downloaded WITH (NOLOCK) ON cp_downloaded.ClientPersonnelID = dzf.WhoDownloaded 
	WHERE dzi.[ClientID] = @ClientID
	AND (@ShowDownloaded = 1 OR dzf.WhenDownloaded IS NULL)
	AND dzi.StatusID = 31
	ORDER BY dzi.[DocumentZipInformationID] DESC 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetByClientIDForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__GetByClientIDForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetByClientIDForGrid] TO [sp_executeall]
GO
