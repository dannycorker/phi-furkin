SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-09
-- Description:	Admin dashboard account overview
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminDashboardAccountOverview] 
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @ZipFailCount int = 0, 
	--@BatchFailCount int = 0 
	
	/* Document Zip failures */
	--SELECT @ZipFailCount = COUNT(*) 
	--FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
	--WHERE dzi.StatusID IN (32, 34) 
	--AND dzi.ClientID = @ClientID 

	--/* Batch Job failures */
	--SELECT @BatchFailCount = COUNT(*) 
	--FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
	--WHERE atr.RunDate > (dbo.fn_GetDate_Local() - 1) 
	--AND atr.RecordCount = -1 
	--AND atr.ClientID = @ClientID 
	--AND NOT EXISTS (
	--	SELECT * 
	--	FROM dbo.AutomatedTaskResult atr2 WITH (NOLOCK) 
	--	WHERE atr2.TaskID = atr.TaskID 
	--	AND atr2.AutomatedTaskResultID > atr.AutomatedTaskResultID 
	--	AND atr2.RecordCount > -1 
	--)
	
	--/* Send back the results */
	--SELECT @ZipFailCount as [ZipFailCount], @BatchFailCount as [BatchFailCount] 
	
	SELECT '1' AS [ID], 'compress.png' AS [icon], 'Failed ZIPs' AS [Type], (SELECT COUNT(*) 
	FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
	WHERE dzi.StatusID IN (32, 34) 
	AND dzi.ClientID = @ClientID) AS [Count]
UNION
	/* Batch Job failures */
	SELECT '2' AS [ID], 'page_white_stack.png' AS [icon], 'Failed Batch Jobs' AS [Type], (SELECT COUNT(*) 
	FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
	WHERE atr.RunDate > (dbo.fn_GetDate_Local() - 1) 
	--AND atr.RecordCount = -1 
	AND atr.Complete = 0
	AND atr.ClientID = @ClientID 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.AutomatedTaskResult atr2 WITH (NOLOCK) 
		WHERE atr2.TaskID = atr.TaskID 
		AND atr2.AutomatedTaskResultID > atr.AutomatedTaskResultID 
		AND atr2.RecordCount > -1 
	)) AS [Count]
UNION	
	/* Latest PatchNotes */
	SELECT '3' AS [ID], 'package_green.png' AS [icon], 'Latest Patch Notes' AS [Type], (SELECT COUNT(*) 
	FROM PatchNote pn WITH (NOLOCK) 
	WHERE pn.PatchID = (SELECT TOP(1) 
		PatchID FROM Patch WITH (NOLOCK) 
		WHERE AdminOnly = 0 ORDER BY PatchID DESC
	)) AS [Count]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardAccountOverview] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAdminDashboardAccountOverview] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardAccountOverview] TO [sp_executeall]
GO
