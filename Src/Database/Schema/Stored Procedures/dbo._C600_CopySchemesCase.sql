SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-07
-- Description:	Copies scheme data, excluding leadevent info, from case to case
--              Modified from _C600_CopySchemesCase
-- 2017-05-02 CPS Added error in case of pre-existing table rows
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CopySchemesCase]
	@FromMatterID INT,
	@ToMatterID INT,
	@CopyCaseDVs INT = 0
AS
BEGIN
PRINT OBJECT_NAME(@@ProcID)
	SET NOCOUNT ON;
--declare
--	@FromMatterID INT = 50009023,
--	@ToMatterID INT = 50021366,
--	@CopyCaseDVs INT = 1
	
		
	DECLARE @FromCaseID INT,
			@ToCaseID INT,
			@ToLeadID INT,
			@CaseNum INT,
			@DetailFieldSubTypeID INT, /*AKA LeadOrMatter*/
			@MatterList VARCHAR(2000),
			@RefLetterNumberStart INT,
			@LogMessage VARCHAR(MAX),
			@LogCounter INT,
			@ClientID INT,
			@CDVsCopied INT,
			@ClientPersonnelID INT
			,@SpeciesLookupListItemID	INT
			,@ErrorMessage	VARCHAR(2000) = ''
			
	DECLARE @AnyInt TABLE (MyInt INT, MyInt2 INT)
	
	DECLARE @TableRowIDs TABLE (OldTableRowID INT, NewTableRowID INT)

	/*
		6. Copy Case DetailValues
		7. Copy MDV's
		8. Copy tablerows and associated detailvalues
	*/

	SELECT @FromCaseID=m.CaseID, 
		@ClientPersonnelID=CASE WHEN @ClientPersonnelID IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @ClientPersonnelID END
	FROM Matter m WITH (NOLOCK) WHERE MatterID=@FromMatterID

	SELECT @ToLeadID=m.LeadID,@ToCaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE MatterID=@ToMatterID
	
	SELECT @SpeciesLookupListItemID = dbo.fnGetSimpleDvAsInt(177451,@FromCaseID) /*Override Pet Type*/
	
	/*CPS 2017-05-02*/
	IF EXISTS ( SELECT * 
	            FROM TableRows tr WITH ( NOLOCK ) 
	            WHERE tr.MatterID = @ToMatterID )
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  There are already config table rows in place for MatterID ' + ISNULL(CONVERT(VARCHAR,@ToMatterID),'NULL') + ' </font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END

	/*Insert the case detail values swapping out the CaseIDs where the field isnt in the list passed into the proc*/
	IF @CopyCaseDVs=1
	BEGIN
		INSERT INTO CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue, ErrorMsg, EncryptedValue, SourceID)
		SELECT c.ClientID, @ToCaseID, c.DetailFieldID, DetailValue, ErrorMsg, EncryptedValue, c.SourceID
		FROM CaseDetailValues c WITH (NOLOCK)
		WHERE c.CaseID = @FromCaseID
	END
	
	SELECT @LogCounter = @@ROWCOUNT
	
	SELECT @CDVsCopied = @LogCounter

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' CaseDetailValues On LeadID: ' + CONVERT(VARCHAR,@ToLeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C600_CopySchemesCase', 'CaseDetailValues', @LogMessage, @ClientPersonnelID

	/*
		Here we need to do the matter detail values swapping out the matterid(s) as we go.
		We also need to exclude any fields that are in the list to exclude.
	*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue, ErrorMsg, OriginalDetailValueID, OriginalLeadID, EncryptedValue, SourceID)
	SELECT mdv.ClientID, @ToLeadID, @ToMatterID, mdv.DetailFieldID, DetailValue, ErrorMsg, OriginalDetailValueID, OriginalLeadID, EncryptedValue, mdv.SourceID
	FROM MatterDetailValues mdv WITH (NOLOCK)
	WHERE mdv.MatterID=@FromMatterID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' MatterDetailValues On LeadID: ' + CONVERT(VARCHAR,@ToLeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C600_CopySchemesCase', 'MatterDetailValues', @LogMessage, @ClientPersonnelID

	/*Copy the tablerows for the case*/
	IF @CopyCaseDVs=1
	BEGIN
		INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, SourceID)
		OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowIDs (OldTableRowID, NewTableRowID)
		SELECT ClientID, NULL, NULL, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, @ToCaseID, ClientPersonnelID, ContactID, TableRowID
		FROM TableRows t WITH (NOLOCK)
		WHERE t.CaseID = @FromCaseID
	END
	
	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' TableRows (Case) On LeadID: ' + CONVERT(VARCHAR,@ToLeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C600_CopySchemesCase', 'TableRows', @LogMessage, @ClientPersonnelID

	/*Copy the tablerows for the Matter*/
	INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, SourceID)
	OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowIDs (OldTableRowID, NewTableRowID)
	SELECT ClientID, NULL, @ToMatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, NULL, ClientPersonnelID, ContactID, TableRowID
	FROM TableRows t WITH (NOLOCK)
	WHERE t.MatterID=@FromMatterID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' TableRows (Matter) On LeadID: ' + CONVERT(VARCHAR,@ToLeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C600_CopySchemesCase', 'TableRows', @LogMessage, @ClientPersonnelID

	/*
		Insert the TableDetailValues, inner join on the tablerows table variable to swap out the
		old table row ids for the new ones and left joing onto anyint to get the matter ids where
		required. We will also use this to set the case id correctly if the matter id is null..
	*/
	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, EncryptedValue, ErrorMsg, CustomerID, CaseID, ClientPersonnelID, ContactID)
	SELECT tr.NewTableRowID, ResourceListID, DetailFieldID, DetailValue, CASE WHEN t.LeadID IS NULL THEN NULL ELSE @ToLeadID END, CASE WHEN t.MatterID IS NULL THEN NULL ELSE @ToMatterID END, ClientID, EncryptedValue, ErrorMsg, CustomerID, CASE WHEN t.CaseID IS NULL THEN NULL ELSE @ToCaseID END, ClientPersonnelID, ContactID
	FROM TableDetailValues t WITH (NOLOCK)
	INNER JOIN @TableRowIDs tr ON tr.OldTableRowID = t.TableRowID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' TableDetailValues (Matter) On LeadID: ' + CONVERT(VARCHAR,@ToLeadID)
	EXEC dbo._C00_LogIt 'SP', '_C600_CopySchemesCase', 'TableDetailValues', @LogMessage, @ClientPersonnelID


	/*change product/scheme to the '_copy' version*/
	UPDATE Mdv
	SET DetailValue = CASE WHEN mdv.DetailValue = 151551					THEN		153363	 /*L&G NEW - Lifetime 2000*/
											 WHEN mdv.DetailValue = 151565						THEN		153364	/*L&G NEW - Lifetime 4000*/
											 WHEN mdv.DetailValue = 151566						THEN		153365	/*L&G NEW - Accident Only*/
											 WHEN mdv.DetailValue = 151567						THEN		153366   /*L&G NEW - 12 Month 2000*/
											 WHEN mdv.DetailValue=  151568	 					THEN		153367  /*L&G NEW - 12 Month 5000*/ 
											 WHEN mdv.DetailValue = 151569						THEN		153368  /*L&G NEW - Lifetime 10000*/
											 WHEN mdv.DetailValue = 151570						THEN		153369  /*L&G OLD - Accident Only*/	
											 WHEN mdv.DetailValue = 151571						THEN		153370  /*L&G OLD - Accident & Illness*/
											 WHEN mdv.DetailValue = 151572						THEN		153371  /*L&G OLD - Accident & Illness Plus*/
											 WHEN mdv.DetailValue = 151573						THEN		153372  /*Buddies - Standard*/
											 WHEN mdv.DetailValue = 151575						THEN		153373  /*Buddies - Premier*/
											 WHEN mdv.DetailValue = 151576						THEN		153374  /*Buddies - Premier Plus*/
											 WHEN mdv.DetailValue = 151577						THEN		153375 /*Buddies - OMF*/
											ELSE 0 END
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN MatterDetailValues Mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID
	WHERE mdv.DetailFieldID = 145689
	AND m.CaseID = @ToCaseID
		
	SELECT *
	FROM MatterDetailValues


	IF @SpeciesLookupListItemID > 0
	BEGIN
		UPDATE tdv
		SET DetailValue = @SpeciesLookupListItemID
		FROM TableDetailValues tdv WITH ( NOLOCK ) 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
		WHERE tdv.MatterID = @ToMatterID
		AND df.LookupListID = 3725 /*Pet Type*/
		/*!! NO SQL HERE !!*/
		SELECT @LogCounter = @@ROWCOUNT

		UPDATE mdv
		SET DetailValue = @SpeciesLookupListItemID
		FROM MatterDetailValues mdv WITH ( NOLOCK ) 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = mdv.DetailFieldID
		WHERE mdv.MatterID = @ToMatterID
		AND df.LookupListID = 3725 /*Pet Type*/
		/*!! NO SQL HERE !!*/
		SELECT @LogCounter += @@ROWCOUNT
		
		SELECT @LogMessage = 'Updated ' + CONVERT(VARCHAR,@LogCounter) + ' values to set species to ' + li.ItemValue
		FROM LookupListItems li WITH ( NOLOCK )
		WHERE li.LookupListItemID = @SpeciesLookupListItemID
		
		EXEC dbo._C00_LogIt 'SP', '_C600_CopySchemesCase', 'PetOverride', @LogMessage, @ClientPersonnelID
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CopySchemesCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CopySchemesCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CopySchemesCase] TO [sp_executeall]
GO
