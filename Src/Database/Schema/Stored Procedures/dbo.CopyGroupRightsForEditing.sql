SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-29
-- Description:	Copy all Group Rights etc to temp/work table
-- =============================================
CREATE PROCEDURE [dbo].[CopyGroupRightsForEditing]
	-- Add the parameters for the stored procedure here
	@GroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT GroupFunctionControlEditing (ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)
	SELECT ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID
	FROM GroupFunctionControl
	WHERE ClientPersonnelAdminGroupID = @GroupID

	INSERT GroupRightsDynamicEditing (ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
	SELECT ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID
	FROM GroupRightsDynamic
	WHERE ClientPersonnelAdminGroupID = @GroupID

	-- Create a default set for brand new Groups
	INSERT GroupFunctionControlEditing (ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID)
	SELECT @GroupID, ModuleID, FunctionTypeID, 0, 0
	FROM FunctionType
	WHERE ParentFunctionTypeID IS NULL
	AND NOT EXISTS (
		SELECT *
		FROM GroupFunctionControl 
		WHERE ClientPersonnelAdminGroupID = @GroupID
	)
	
END








GO
GRANT VIEW DEFINITION ON  [dbo].[CopyGroupRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CopyGroupRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CopyGroupRightsForEditing] TO [sp_executeall]
GO
