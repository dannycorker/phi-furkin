SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-03-10
-- Description:	Explain the current Event Choices etc for a Case
-- =============================================
CREATE PROCEDURE [dbo].[GetEventChoicesInProgressDebug] 
	
	@CaseID int,
	@FromEventTypeID int,
	@RunAsThisUser int,
	@ThreadNumber int

AS
BEGIN
	
	DECLARE @ClientID int, 
	@CustomerID int,
	@LeadID int

	DECLARE @PossibleEvents1 TABLE (
		EventChoiceID int, 
		NextEventTypeID int, 
		NextEventTypeName varchar(50), 
		ThreadNumber int, 
		ChoiceDescription varchar(50)
	)

	DECLARE @PossibleEvents2 TABLE (
		ThreadNumber int, 
		Counter int
	)

	DECLARE @CursorEvents TABLE (
		EventChoiceID int
	) 

	DECLARE @AAResult TABLE (
		ResultCode tinyint	
	)

	SELECT 'All Possible Choices', ec.EventChoiceID, ec.NextEventTypeID, net.EventTypeName, ec.ThreadNumber, ec.Description 
	FROM EventChoice ec 
	INNER JOIN EventType net ON net.EventTypeID = ec.NextEventTypeID 
	WHERE ec.EventTypeID = @FromEventTypeID 
	AND ec.ThreadNumber = @ThreadNumber
	AND ec.EscalationEvent = 0

	IF @@ROWCOUNT < 1
	BEGIN 
		SELECT 'NO Possible Choices'
	END


	-- Get the base list of event choices leading away from the current event
	INSERT INTO @PossibleEvents1(EventChoiceID, NextEventTypeID, NextEventTypeName, ThreadNumber, ChoiceDescription)
	SELECT ec.EventChoiceID, ec.NextEventTypeID, net.EventTypeName, ec.ThreadNumber, ec.Description 
	FROM EventChoice ec 
	INNER JOIN EventType net ON net.EventTypeID = ec.NextEventTypeID 
	WHERE net.Enabled = 1
	AND net.AvailableManually = 1
	AND ec.EventTypeID = @FromEventTypeID 
	AND ec.ThreadNumber = @ThreadNumber
	AND ec.EscalationEvent = 0

	UNION

	-- Special case where there are no events for this case so far.
	-- Show all possible process-start events for this lead type.
	select NULL as [EventChoiceID], fet.eventtypeid as [NextEventTypeID], fet.eventtypename as [EventTypeName], 1 as [ThreadNumber], fet.eventtypename as [Description]
	from eventtype fet 
	inner join lead l on l.leadtypeid = fet.leadtypeid 
	inner join cases c on c.leadid = l.leadid 
	where c.caseid = @CaseID 
	and fet.eventsubtypeid = 10 
	and not exists (
		select * from leadevent where caseid = @CaseID
	)

	SELECT 'Choices Before Auto-Adj', p1.*
	FROM @PossibleEvents1 p1

	IF @@ROWCOUNT < 1
	BEGIN 
		SELECT 'NO Valid Choices Before Auto-Adj'
	END


	-- Now have a crack at autoadjudication!
	SELECT @ClientID = Lead.ClientID, 
	@CustomerID = Lead.CustomerID, 
	@LeadID = Lead.LeadID 
	FROM Cases 
	INNER JOIN Lead ON Lead.LeadID = Cases.LeadID 
	WHERE Cases.CaseID = @CaseID 

	-- For each event type that could now be chosen in theory,
	-- work out which are not possible, and delete them from @PossibleEvents1
	DECLARE @DeleteStr varchar(2000),
	@TestEventChoiceID int

	-- Get all the potential event types into a working table we can 
	-- browse through one record at a time.
	INSERT INTO @CursorEvents(EventChoiceID)
	SELECT EventChoiceID FROM @PossibleEvents1

	SET ROWCOUNT 1

	SELECT @TestEventChoiceID = EventChoiceID
	FROM @CursorEvents

	SET ROWCOUNT 0

	WHILE @TestEventChoiceID > 0 
	BEGIN
		SELECT @DeleteStr = [SqlClauseForInclusion] FROM EventChoice WHERE EventChoiceID = @TestEventChoiceID
		
		IF @DeleteStr IS NOT NULL AND rtrim(@DeleteStr) <> ''
		BEGIN
			SELECT @DeleteStr = dbo.fnSubstituteSqlParams(@DeleteStr, @RunAsThisUser, @ClientID, @CustomerID, @LeadID, @CaseID, NULL, NULL)

			SELECT @DeleteStr = 'SELECT CASE WHEN ' + @DeleteStr + ' THEN 1 ELSE 0 END '

			SELECT @DeleteStr

			INSERT @AAResult
			EXEC(@DeleteStr)

			DELETE @PossibleEvents1 
			WHERE EventChoiceID = @TestEventChoiceID AND NOT EXISTS (select * FROM @AAResult WHERE ResultCode = 1)

			IF @@ROWCOUNT = 1
			BEGIN
				SELECT 'Event Choice Deleted : ' + convert(varchar, @TestEventChoiceID)
			END

			DELETE @AAResult
		END

		DELETE FROM @CursorEvents
		WHERE EventChoiceID = @TestEventChoiceID

		SET ROWCOUNT 1

		SELECT @TestEventChoiceID = EventChoiceID
		FROM @CursorEvents

		IF @@ROWCOUNT < 1
		BEGIN
			SELECT @TestEventChoiceID = -1
		END

		SET ROWCOUNT 0
	END

	-- Group the data so we can pass a counter to the app
	INSERT INTO @PossibleEvents2(ThreadNumber, Counter)
	SELECT p1.ThreadNumber, COUNT(*) 
	FROM @PossibleEvents1 p1
	GROUP BY p1.ThreadNumber

	-- Finally, select the values
	SELECT 'Final Choices', p1.NextEventTypeID as [EventTypeID], 
	p1.NextEventTypeName as [EventTypeName], 
	p1.ThreadNumber as [ThreadNumber], 
	p1.ChoiceDescription as [ChoiceDescription], 
	p2.Counter as [ChoicesInThread]
	FROM @PossibleEvents1 p1 
	INNER JOIN @PossibleEvents2 p2 ON p1.ThreadNumber = p2.ThreadNumber 
	ORDER BY p1.ThreadNumber, p1.NextEventTypeName

	SELECT 'Now the application checks for user rights'

END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoicesInProgressDebug] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventChoicesInProgressDebug] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoicesInProgressDebug] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[GetEventChoicesInProgressDebug] TO [sp_executehelper]
GO
