SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumEventSubtype table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_GetByAquariumEventSubtypeID]
(

	@AquariumEventSubtypeID int   
)
AS


				SELECT
					[AquariumEventSubtypeID],
					[AquariumEventTypeID],
					[AquariumEventSubtypeName],
					[AquariumEventSubtypeDescription],
					[WhenCreated],
					[WhenModified],
					[ClientID],
					[StoredProcedure]
				FROM
					[dbo].[AquariumEventSubtype] WITH (NOLOCK) 
				WHERE
										[AquariumEventSubtypeID] = @AquariumEventSubtypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_GetByAquariumEventSubtypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_GetByAquariumEventSubtypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_GetByAquariumEventSubtypeID] TO [sp_executeall]
GO
