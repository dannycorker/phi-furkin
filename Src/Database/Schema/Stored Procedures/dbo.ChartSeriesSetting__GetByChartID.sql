SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01 May 2008

-- Created By:  Chris Townsend
-- Purpose: Takes a ChartID and returns all ChartSeriesSettings 
			for the ChartSeries that belong to that Chart
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting__GetByChartID]
(
	@ChartID int
)

AS

SELECT  css.[ChartSeriesSettingID]
		,css.[ChartSeriesID]
		,css.[SettingName]
		,css.[SettingValue]
		,css.[ClientID]



FROM ChartSeriesSetting css 

INNER JOIN ChartSeries cs ON css.ChartSeriesID = cs.ChartSeriesID
INNER JOIN Chart c ON c.ChartID = cs.ChartID

WHERE c.ChartID = @ChartID






GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting__GetByChartID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting__GetByChartID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting__GetByChartID] TO [sp_executeall]
GO
