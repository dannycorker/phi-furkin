SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2016-02-16
-- Description:	Get a customer with lookup "expanded"
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetExpandedByCustomerID]
(
	@CustomerID INT
)
AS
BEGIN

	SELECT c.*, t.Title
	FROM dbo.Customers c WITH (NOLOCK) 
	LEFT JOIN dbo.Titles t WITH (NOLOCK) ON c.TitleID = t.TitleID
	WHERE c.CustomerID = @CustomerID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetExpandedByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetExpandedByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetExpandedByCustomerID] TO [sp_executeall]
GO
