SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddLogEntry]
@LogDateTime datetime,
@TypeOfLogEntry varchar (6),
@ClassName varchar (512),
@MethodName varchar (512),
@LogEntry varchar (MAX),
@ClientPersonnelID int
AS

IF @ClassName = 'CustomPartRouting.cs'
BEGIN
	RETURN
END

INSERT INTO Logs (LogDateTime, TypeOfLogEntry, ClassName, MethodName,  LogEntry, ClientPersonnelID)
VALUES (@LogDateTime, @TypeOfLogEntry, @ClassName, @MethodName, @LogEntry, @ClientPersonnelID)

DECLARE @LogID INT
SELECT @LogID = SCOPE_IDENTITY()

IF @ClassName LIKE '%timeout%'
BEGIN
	SELECT 'There has been a timeout, please click back and try again (' + CAST(@LogID AS VARCHAR) + ')'
END
ELSE IF @ClassName LIKE '%deadlock%'
BEGIN
	SELECT 'There has been a deadlock, please click back and try again (' + CAST(@LogID AS VARCHAR) + ')'
END
ELSE
BEGIN
	SELECT @LogID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[AddLogEntry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddLogEntry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddLogEntry] TO [sp_executeall]
GO
