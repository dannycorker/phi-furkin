SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-07-20
-- Description:	Policy Premiums – Get by MatterID, current annual Gross, Net and Tax 
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetPolicyPremiumDataByPolicy]
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT TOP 1 pp.PurchasedProductID[PurchasedProductID], pp.ProductCostGross[AnnualGross], pp.ProductCostNet[AnnualNet], pp.ProductCostVAT[AnnualTax] 
	FROM PurchasedProduct pp WITH (NOLOCK) 
	JOIN Matter m WITH (NOLOCK) ON m.MatterID = pp.ObjectID
	WHERE m.MatterID = @MatterID
	AND pp.ClientID = @ClientID
	AND m.ClientID = @ClientID
	ORDER BY pp.PurchasedProductID DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPolicyPremiumDataByPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetPolicyPremiumDataByPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPolicyPremiumDataByPolicy] TO [sp_executeall]
GO
