SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the HyperlinkSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget_Update]
(

	@HyperlinkSpecialisedDetailFieldTargetID int   ,

	@ClientID int   ,

	@HyperlinkDetailFieldID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@RowField int   ,

	@Notes varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[HyperlinkSpecialisedDetailFieldTarget]
				SET
					[ClientID] = @ClientID
					,[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[ColumnField] = @ColumnField
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[ColumnFieldAlias] = @ColumnFieldAlias
					,[RowField] = @RowField
					,[Notes] = @Notes
				WHERE
[HyperlinkSpecialisedDetailFieldTargetID] = @HyperlinkSpecialisedDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Update] TO [sp_executeall]
GO
