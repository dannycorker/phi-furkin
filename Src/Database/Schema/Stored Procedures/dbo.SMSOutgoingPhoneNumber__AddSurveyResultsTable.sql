SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-03-2013
-- Description:	Adds the survey results table to a matter level page.
-- 1. Checks to see if the SMS Survey Results table definition exists within the resources and tables lead type
--		If the resoures and tables lead type does not exist it creates it.
--		If the SMS Survey Results basic table page does not exist it creates it in the resources and tables lead type
--		together with its columns.
-- 2. Checks to see if a page with the questionnaire title exists at the matter level.
--		If the page does not exist it creates it and adds a Survey Results Table detail field to it.
-- 3. Maps the newly added field using third party mapping so that the SMS Questionnaire stored procedures can pick those fields up
--
-- Modified By PR on 23/01/2014 - Added the following SMS Survey Result columns:-
--									Language, Sentiment Polarity, Sentiment Confidence, Mixed Sentiment, Rules Engine Score
-- Modified By PR on 14/05/2014 - Added Net promotor score to the survey result table
-- Modified By PR on 27/08/2014 - Added SMS Categorisation table and mapping
-- Modified By PR on 09/09/2014 - Fixed NPS mapping
-- Modified By PR on 09/09/2014 - Fixed Categorisation mapping
-- Modified By PR on 16/09/2014 - Fixed Table Categorisation mapping creation
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__AddSurveyResultsTable] 
		@ClientID INT,
		@LeadTypeID INT,		-- LeadType where the SMS survey table is to be placed
		@SMSQuestionnaireID INT
AS
BEGIN

		-- Does the resources and tables lead type exist for this client
		DECLARE @ResourcesAndTableLeadTypeID INT
		SELECT TOP 1 @ResourcesAndTableLeadTypeID=LeadTypeID FROM LeadType WITH (NOLOCK) WHERE LeadTypeName LIKE '%Resources%tables%' AND ClientID=@ClientID

		-- If not create the resources and tables lead type 
		IF @ResourcesAndTableLeadTypeID IS NULL OR @ResourcesAndTableLeadTypeID=0
		BEGIN
			-- Create Resources And Tables LeadType since it does not exist	
			INSERT INTO LeadType (ClientID,LeadTypeName,LeadTypeDescription,Enabled,Live,MatterDisplayName,LastReferenceInteger,ReferenceValueFormatID,IsBusiness,LeadDisplayName,CaseDisplayName,CustomerDisplayName,LeadDetailsTabImageFileName,CustomerTabImageFileName,UseEventCosts,UseEventUOEs,UseEventDisbursements,UseEventComments,AllowMatterPageChoices,AllowLeadPageChoices,IsRPIEnabled,FollowupWorkingDaysOnly,MatterLastReferenceInteger,MatterReferenceValueFormatID)
			VALUES (@ClientID,'Resources and Tables','Resources and Tables',1,1,'',NULL,NULL,NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
			
			SET @ResourcesAndTableLeadTypeID = SCOPE_IDENTITY()
		END

		-- Does the SMS Survey Results basic table exist under the resources and tables lead type
		DECLARE @DetailFieldPageID INT -- for the  SMS Survey Results basic table at the resource and table lead type 
		SELECT @DetailFieldPageID=DetailFieldPageID FROM DetailFieldPages WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND PageName = 'SMS Survey Results' 

		-- If not create the SMS Survey Results basic table under the resources and tables lead type
		IF @DetailFieldPageID IS NULL OR @DetailFieldPageID=0
		BEGIN
			-- Create SMS Survey Results basic table page since it does not exist at the Resources And Table Leadtype
			INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled)
			VALUES (@ClientID,8,@ResourcesAndTableLeadTypeID,'SMS Survey Results','SMS Survey Results',0,1)
			
			SET @DetailFieldPageID = SCOPE_IDENTITY()	
			
			-- Create SMS Survey Results table columns
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable)
			VALUES (@ClientID,8,'Date SMS Sent','Date SMS Sent',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,1,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'SMS Out EventType ID','SMS Out EventType ID',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,2,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'SMS Question ID','SMS Question ID',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,3,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'SMS Message To Customer','SMS Message To Customer',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,4,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),	
				   (@ClientID,8,'SMS Message Reply From Customer','SMS Message Reply From Customer',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,5,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Date SMS Reply Received','Date SMS Reply Received',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,6,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'SMS Response EventType ID','SMS Response EventTypeID',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,7,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'SMS Response Question ID','SMS Response Question ID',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Unrecognised Response','Unrecognised Response',3,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Score','Score',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Language','Language',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Sentiment Polarity','Sentiment Polarity',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Sentiment Confidence','Sentiment Confidence',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Mixed Sentiment','Mixed Sentiment',3,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'Rules Engine Score','Rules Engine Score',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1),
				   (@ClientID,8,'NPS','NPS',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageID,8,0,'',NULL,NULL,1,1,NULL,NULL,1,0,0,0,0,0,0,1)
			
		END
		
		-- Does the Sms Category Results basic table exist under the resources and tables lead type
		DECLARE @DetailFieldPageCategoryResultsID INT
		SELECT @DetailFieldPageCategoryResultsID=DetailFieldPageID FROM DetailFieldPages WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND PageName = 'Sms Category Results'  	
			
		-- If not create the SMS Survey Results basic table under the resources and tables lead type
		IF @DetailFieldPageCategoryResultsID IS NULL OR @DetailFieldPageCategoryResultsID=0
		BEGIN
		
			INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled)
			VALUES (@ClientID, 8, @ResourcesAndTableLeadTypeID,'Sms Category Results', 'Sms Category Results',1,1)

			SET @DetailFieldPageCategoryResultsID = SCOPE_IDENTITY()

			-- Create sms category result columns
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
			VALUES 
			(@ClientID, 8, 'Category','Category',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageCategoryResultsID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1),
			(@ClientID, 8, 'Words','Words',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageCategoryResultsID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1),
			(@ClientID, 8, 'Word Count','Word Count',1,0,0,NULL,@ResourcesAndTableLeadTypeID,1,@DetailFieldPageCategoryResultsID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)
		
		END

		-- Get the sms questionnaire title from the SMSQuestionnaire
		DECLARE @SMSQuestionnaireTitle NVARCHAR(250) = 'SMS Survey'
		--SELECT @SMSQuestionnaireTitle=Title FROM SMSQuestionnaire WITH (NOLOCK) WHERE SMSQuestionnaireID=@SMSQuestionnaireID

		-- Check to see if the matter level page exists with the questionaire title
		DECLARE @PAGEID_EXISTS INT
		SELECT @PAGEID_EXISTS=DetailFieldPageID FROM DetailFieldPages WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND PageName = @SMSQuestionnaireTitle AND LeadOrMatter=2

		DECLARE @DetailFieldPageID_FOR_QUESTIONNAIRE INT
		DECLARE @DetailFieldID_FOR_TABLE_ON_PAGE INT
		DECLARE @DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE INT
		-- if it does not exist - create it and add the SMS Survey Results table to it
		IF @PAGEID_EXISTS IS NULL OR @PAGEID_EXISTS=0
		BEGIN
			-- Insert detail field page with the questionnaire title at the matter level
			INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled)
			VALUES (@ClientID,2,@LeadTypeID,@SMSQuestionnaireTitle,@SMSQuestionnaireTitle,0,1)
					
			SET @DetailFieldPageID_FOR_QUESTIONNAIRE = SCOPE_IDENTITY()
			
			-- Insert detail field to hold survey results
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable)
			VALUES (@ClientID,2,'SMS Survey Results','SMS Survey Results',19,0,0,NULL,@LeadTypeID,1,@DetailFieldPageID_FOR_QUESTIONNAIRE,1,0,NULL,NULL,NULL,1,1,NULL,1,0,0,0,0,@DetailFieldPageID,0,0,1)
			
			SET @DetailFieldID_FOR_TABLE_ON_PAGE = SCOPE_IDENTITY()
			
			--Place Sms Response Categorisation Results on page
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
			VALUES (@ClientID, 2, 'SMS Response Categorisation Results','SMS Response Categorisation Results',19,0,0,NULL,@LeadTypeID,1,@DetailFieldPageID_FOR_QUESTIONNAIRE,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,@DetailFieldPageCategoryResultsID,'','',1,0,0,0,0,0,0,0,1)
			
			SET @DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE = SCOPE_IDENTITY()
					
		END
		ELSE -- does exist on page so set field idents
		BEGIN
			SELECT @DetailFieldID_FOR_TABLE_ON_PAGE=DetailFieldID FROM DetailFields WITH (NOLOCK) WHERE FieldName = 'SMS Survey Results' AND ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND LeadOrMatter=2 AND TableDetailFieldPageID=@DetailFieldPageID
			
			SELECT @DetailFieldPageID_FOR_QUESTIONNAIRE=DetailFieldPageID FROM DetailFieldPages WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND PageName = @SMSQuestionnaireTitle AND LeadOrMatter=2			
			
			--does the sms response categorisation table exist on the sms survey page
			DECLARE @SmsResponseCatFieldID INT
			SELECT @SmsResponseCatFieldID=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadOrMatter=2 AND LeadTypeID=@LeadTypeID AND FieldName = 'SMS Response Categorisation Results'
			-- if not Place Sms Response Categorisation Results table on page
			IF @SmsResponseCatFieldID IS NULL OR @SmsResponseCatFieldID=0
			BEGIN 
				INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
				VALUES (@ClientID, 2, 'SMS Response Categorisation Results','SMS Response Categorisation Results',19,0,0,NULL,@LeadTypeID,1,@DetailFieldPageID_FOR_QUESTIONNAIRE,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,@DetailFieldPageCategoryResultsID,'','',1,0,0,0,0,0,0,0,1)
			END
			
			SELECT @DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE=DetailFieldID FROM DetailFields WHERE FieldName = 'SMS Response Categorisation Results' AND ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND LeadOrMatter=2 AND TableDetailFieldPageID=@DetailFieldPageCategoryResultsID
		END

		-- Check to see if the third party mapping has been created
		DECLARE @CountOfMapping INT 
		SELECT @CountOfMapping = COUNT(*) FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE 
		ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND DetailFieldID=@DetailFieldID_FOR_TABLE_ON_PAGE

		IF(@CountOfMapping=0) -- mapping does not exist so create it
		BEGIN
		
			DECLARE @DETAIL_FIELD_ID_Date_SMS_Sent INT						-- 372
			SELECT @DETAIL_FIELD_ID_Date_SMS_Sent=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Date SMS Sent' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Out_EventTypeID INT				-- 373
			SELECT @DETAIL_FIELD_ID_SMS_Out_EventTypeID=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='SMS Out EventType ID' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Question_ID INT					-- 374
			SELECT @DETAIL_FIELD_ID_SMS_Question_ID=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='SMS Question ID' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Message_To_Customer INT			-- 375
			SELECT @DETAIL_FIELD_ID_SMS_Message_To_Customer=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='SMS Message To Customer' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Message_Reply_From_Customer INT	-- 376
			SELECT @DETAIL_FIELD_ID_SMS_Message_Reply_From_Customer=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='SMS Message Reply From Customer' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_Date_SMS_Reply_Received INT			-- 377
			SELECT @DETAIL_FIELD_ID_Date_SMS_Reply_Received=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Date SMS Reply Received' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Response_EventTypeID INT			-- 378
			SELECT @DETAIL_FIELD_ID_SMS_Response_EventTypeID=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='SMS Response EventType ID' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Response_Question_ID INT			-- 379
			SELECT @DETAIL_FIELD_ID_SMS_Response_Question_ID=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='SMS Response Question ID' AND DetailFieldPageID=@DetailFieldPageID
			
			DECLARE @DETAIL_FIELD_ID_SMS_Unrecognised_Response INT			-- 384
			SELECT @DETAIL_FIELD_ID_SMS_Unrecognised_Response=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Unrecognised Response' AND DetailFieldPageID=@DetailFieldPageID
			
			DECLARE @DETAIL_FIELD_ID_SMS_Score INT							-- 385
			SELECT @DETAIL_FIELD_ID_SMS_Score=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Score' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_Language INT						-- 820
			SELECT @DETAIL_FIELD_ID_SMS_Language=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Language' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_SentimentPolarity INT				-- 821
			SELECT @DETAIL_FIELD_ID_SMS_SentimentPolarity=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Sentiment Polarity' AND DetailFieldPageID=@DetailFieldPageID

			DECLARE @DETAIL_FIELD_ID_SMS_SentimentConfidence INT			-- 822
			SELECT @DETAIL_FIELD_ID_SMS_SentimentConfidence=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Sentiment Confidence' AND DetailFieldPageID=@DetailFieldPageID
			
			DECLARE @DETAIL_FIELD_ID_SMS_MixedSentiment INT					-- 823
			SELECT @DETAIL_FIELD_ID_SMS_MixedSentiment=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Mixed Sentiment' AND DetailFieldPageID=@DetailFieldPageID			

			DECLARE @DETAIL_FIELD_ID_SMS_RulesEngineScore INT				-- 824
			SELECT @DETAIL_FIELD_ID_SMS_RulesEngineScore=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Rules Engine Score' AND DetailFieldPageID=@DetailFieldPageID			
			
			DECLARE @DETAIL_FIELD_ID_SMS_NPS INT							-- 865
			SELECT @DETAIL_FIELD_ID_SMS_NPS=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='NPS' AND DetailFieldPageID=@DetailFieldPageID	

			-- Map the SMS Survey Results table in third party mapping
			INSERT INTO ThirdPartyFieldMapping (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
			VALUES (@ClientID,@LeadTypeID,372,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_Date_SMS_Sent,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,373,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Out_EventTypeID,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,374,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Question_ID,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,375,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Message_To_Customer,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,376,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Message_Reply_From_Customer,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,377,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_Date_SMS_Reply_Received,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,378,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Response_EventTypeID,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,379,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Response_Question_ID,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,384,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Unrecognised_Response,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,385,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Score,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),				   
				   (@ClientID,@LeadTypeID,820,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_Language,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,821,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_SentimentPolarity,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,822,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_SentimentConfidence,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,823,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_MixedSentiment,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,824,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_RulesEngineScore,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,865,@DetailFieldID_FOR_TABLE_ON_PAGE,@DETAIL_FIELD_ID_SMS_NPS,24,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
	   
		END
		
		-- Check to see if the third party mapping has been created
		DECLARE @CountOfSMSCategoryMapping INT 
		SELECT @CountOfSMSCategoryMapping = COUNT(*) FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE 
		ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND DetailFieldID=@DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE

		IF(@CountOfSMSCategoryMapping=0) -- mapping does not exist so create it
		BEGIN
		
			DECLARE @DETAIL_FIELD_ID_Category INT							-- 900
			SELECT @DETAIL_FIELD_ID_Category=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Category' AND DetailFieldPageID=@DetailFieldPageCategoryResultsID

			DECLARE @DETAIL_FIELD_ID_Words INT								-- 901
			SELECT @DETAIL_FIELD_ID_Words=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Words' AND DetailFieldPageID=@DetailFieldPageCategoryResultsID

			DECLARE @DETAIL_FIELD_ID_Word_Count INT							-- 902
			SELECT @DETAIL_FIELD_ID_Word_Count=DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@ResourcesAndTableLeadTypeID AND FieldName='Word Count' AND DetailFieldPageID=@DetailFieldPageCategoryResultsID
		
			INSERT INTO ThirdPartyFieldMapping (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
			VALUES (@ClientID,@LeadTypeID,900,@DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE,@DETAIL_FIELD_ID_Category,67,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,901,@DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE,@DETAIL_FIELD_ID_Words,67,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
				   (@ClientID,@LeadTypeID,902,@DetailFieldID_FOR_SMS_CATEGORISATION_TABLE_ON_PAGE,@DETAIL_FIELD_ID_Word_Count,67,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())		
		
		END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__AddSurveyResultsTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__AddSurveyResultsTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__AddSurveyResultsTable] TO [sp_executeall]
GO
