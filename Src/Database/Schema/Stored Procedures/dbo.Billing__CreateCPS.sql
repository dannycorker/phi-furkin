SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-03-26
-- Description:	Create a customer payment schedule request 
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreateCPS]
	@ClientID INT,
	@ClientAccountID INT,
	@CustomerID INT,
	@AccountID INT,
	@ActualCollectionDate DATE,
	@PaymentDate DATE,
	@PaymentNet NUMERIC(18,2),
	@PaymentTax NUMERIC(18,2),
	@PaymentGross NUMERIC(18,2),
	@WhoCreated INT,
	@RelatedObjectID INT,
	@RelatedObjectTypeID INT

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CustomerPaymentScheduleID INT

	INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, WhoCreated, WhenCreated, SourceID, RelatedObjectID, RelatedObjectTypeID, ClientAccountID, WhoModified, WhenModified)
	SELECT @ClientID, 
			@CustomerID, 
			@AccountID, 
			@ActualCollectionDate, 
			@PaymentDate, 
			@PaymentNet, 
			@PaymentTax, 
			@PaymentGross, 
			1, 
			NULL AS [CustomerLedgerID], 
			NULL AS [ReconciledDate], 
			@WhoCreated, 
			dbo.fn_GetDate_Local() AS [WhenCreated], 
			NULL AS [SourceID], 
			@RelatedObjectID, 
			@RelatedObjectTypeID, 
			@ClientAccountID, 
			@WhoCreated, 
			dbo.fn_GetDate_Local() AS [WhenModified]

	SELECT @CustomerPaymentScheduleID = SCOPE_IDENTITY()

	RETURN @CustomerPaymentScheduleID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreateCPS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreateCPS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreateCPS] TO [sp_executeall]
GO
