SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 12-05-2014
-- Description:	Upates the sms events with the sms outgoing phone numbers sms gateway
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__UpdateEventSmsGatewayID]

	@SMSQuestionnaireID INT,
	@LeadTypeID INT,
	@SmsGatewayID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE EventType
	SET SmsGatewayID=@SmsGatewayID
	WHERE EventTypeID IN 
	(
		SELECT et.EventTypeID EventTypeID FROM SMSQuestionEventType smsEvent WITH (NOLOCK) 
		INNER JOIN SMSQuestion q WITH (NOLOCK) on q.SMSQuestionID = smsEvent.SMSQuestionID and q.SMSQuestionnaireID=@SMSQuestionnaireID
		INNER JOIN EventType et WITH (NOLOCK) on et.EventTypeID = smsEvent.EventTypeID and et.LeadTypeID=@LeadTypeID
	)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__UpdateEventSmsGatewayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__UpdateEventSmsGatewayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__UpdateEventSmsGatewayID] TO [sp_executeall]
GO
