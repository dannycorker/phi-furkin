SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-25
-- Description:	List all MatterDetailValues records for a record
-- =============================================
CREATE PROCEDURE [dbo].[mdv] 
	@MatterID int = NULL,
	@LeadID int = NULL,
	@LeadTypeID int = NULL, 
	@DetailFieldID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	/* Option to show values for a particular field */
	IF @MatterID IS NULL AND @LeadID IS NULL AND @LeadTypeID IS NULL AND @DetailFieldID IS NOT NULL
	BEGIN
		SELECT TOP 100 mdv.* 
		FROM dbo.MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.DetailFieldID = @DetailFieldID
	END
	ELSE
	BEGIN
		
		/* Grab a sample lead for the lead type passed in */
		IF @MatterID IS NOT NULL
		BEGIN
		
			/* Show all values for this exact Matter */
			SELECT mdv.*, df.DetailFieldID, df.FieldName, df.FieldCaption, df.LeadOrMatter, df.QuestionTypeID, df.[Enabled], df.EquationText 
			FROM dbo.MatterDetailValues mdv (nolock) 
			INNER JOIN dbo.DetailFields df (nolock) on df.DetailFieldID = mdv.DetailFieldID 
			WHERE mdv.MatterID = @MatterID 
			AND (@DetailFieldID IS NULL OR df.DetailFieldID = @DetailFieldID)
			ORDER BY mdv.DetailFieldID, mdv.MatterDetailValueID 

			IF @LeadTypeID IS NULL
			BEGIN
				/* Need the LeadType in order to show all missing fields later */
				SELECT TOP 1 @LeadTypeID = l.LeadTypeID 
				FROM dbo.Matter m (nolock) 
				INNER JOIN dbo.Lead l (nolock) on l.LeadID = m.LeadID
				WHERE m.MatterID = @MatterID 
			END

			/* Show all missing fields for this lead type */	
			IF @LeadTypeID IS NOT NULL
			BEGIN
				SELECT 'Missing Fields' as [Missing Fields], df.DetailFieldID, df.FieldName, df.FieldCaption, df.LeadOrMatter, df.QuestionTypeID, df.[Enabled], df.EquationText 
				FROM dbo.DetailFields df (nolock) 
				LEFT JOIN dbo.MatterDetailValues mdv (nolock) on df.DetailFieldID = mdv.DetailFieldID AND mdv.MatterID = @MatterID AND df.LeadOrMatter = 2 
				WHERE df.LeadTypeID = @LeadTypeID 
				AND mdv.DetailFieldID IS NULL
				AND (@DetailFieldID IS NULL OR df.DetailFieldID = @DetailFieldID)
				ORDER BY df.DetailFieldID 
			END
						
		END
		ELSE
		BEGIN
			IF @MatterID IS NULL AND @LeadID IS NULL AND @LeadTypeID IS NOT NULL
			BEGIN
				/* Get an example lead for the leadtype passed in */
				SELECT TOP 1 @LeadID = LeadID 
				FROM dbo.Lead l (nolock) 
				WHERE l.LeadTypeID = @LeadTypeID 
				ORDER BY l.LeadID DESC
			END
			
			IF @LeadID IS NOT NULL
			BEGIN
				/* Show all values for all Matters for this lead */
				SELECT mdv.*, df.DetailFieldID, df.FieldName, df.FieldCaption, df.LeadOrMatter, df.LeadOrMatter, df.QuestionTypeID, df.[Enabled], df.EquationText 
				FROM dbo.MatterDetailValues mdv (nolock) 
				INNER JOIN dbo.DetailFields df (nolock) on df.DetailFieldID = mdv.DetailFieldID 
				WHERE mdv.LeadID = @LeadID 
				AND (@DetailFieldID IS NULL OR df.DetailFieldID = @DetailFieldID)
				ORDER BY mdv.MatterID, mdv.DetailFieldID, mdv.MatterDetailValueID 
				
			END

			IF @LeadTypeID IS NULL
			BEGIN
				/* Need the LeadType in order to show all missing fields later */
				SELECT TOP 1 @LeadTypeID = l.LeadTypeID 
				FROM dbo.Lead l (nolock) 
				WHERE l.LeadID = @LeadID 
			END

			/* Show all missing fields for this lead type */	
			IF @LeadTypeID IS NOT NULL
			BEGIN
				SELECT 'Missing Fields' as [Missing Fields], df.DetailFieldID, df.FieldName, df.FieldCaption, df.LeadOrMatter, df.QuestionTypeID, df.[Enabled], df.EquationText 
				FROM dbo.DetailFields df (nolock) 
				LEFT JOIN dbo.MatterDetailValues mdv (nolock) on df.DetailFieldID = mdv.DetailFieldID AND mdv.LeadID = @LeadID 
				WHERE df.LeadTypeID = @LeadTypeID 
				AND df.LeadOrMatter = 2 
				AND mdv.DetailFieldID IS NULL
				AND (@DetailFieldID IS NULL OR df.DetailFieldID = @DetailFieldID)
				ORDER BY df.DetailFieldID 
			END
			
		END	
		
	END
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[mdv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[mdv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[mdv] TO [sp_executeall]
GO
