SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-16
-- Description:	Sums the amounts claimed and approved for payments for a matter and writes this info to a MDF
-- Used by:		SAE
-- Modified 2013-04-25 ROH to use initial claim total if no claim rows yet exist
-- ROH 2015-10-13 Bugfix - missing WHERE clause.
-- CW  2016-11-01 Write the end value of claim to matter for use in documents
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SumClaimedAndApproved] 
(
	@MatterID INT,
	@LeadID INT,
	@ClaimRejection BIT = 0
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	DECLARE @ClaimTotal MONEY = 0
	DECLARE @ApprovedTotal MONEY = 0
	DECLARE @Value VARCHAR(2000) = ''
	
	-- Determine whether claim is rejected rather than rely on calling proc to know
	-- ROH 2013-05-10
	SELECT @ClaimRejection = CASE WHEN ca.ClientStatusID IN (3819,4687) THEN 1 ELSE 0 END
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.Cases ca WITH (NOLOCK) on ca.CaseID = m.CaseID
	WHERE m.MatterID = @MatterID

	-- If no claim rows use initially entered claim amount	
	IF NOT EXISTS (SELECT 1 FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = 144353 /*Claim*/ AND MatterID = @MatterID)
	BEGIN
		
		SELECT @ClaimTotal = ValueMoney
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.DetailFieldID = 149850 /*Total Claim Amount*/
		AND mdv.MatterID = @MatterID
		
	END
	ELSE
	BEGIN
	
		-- We sum up the claim and total figures for all rows on this matter and save them to a matter field
		SELECT @ClaimTotal = SUM(ValueMoney)
		FROM dbo.TableDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = 144353 /*Claim*/
		AND MatterID = @MatterID
		
		SELECT @ApprovedTotal = SUM(tdvTotal.ValueMoney)
		FROM dbo.TableRows r WITH (NOLOCK)
		INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352 /*Total*/
		INNER JOIN dbo.TableDetailValues tdvApproved WITH (NOLOCK) ON r.TableRowID = tdvApproved.TableRowID AND tdvApproved.DetailFieldID = 144362 /*Approved*/
		WHERE r.MatterID = @MatterID
		AND tdvApproved.ValueDate IS NOT NULL
	
	END
	
	IF @ClaimTotal > 0
	BEGIN
		SELECT @Value = '£' + CAST(@ClaimTotal AS VARCHAR)
	END
	
	IF @ApprovedTotal > 0
	BEGIN
		SELECT @Value += ' - £' + CAST(@ApprovedTotal AS VARCHAR)
		EXEC DBO._C00_SimpleValueIntoField 176931,@ApprovedTotal,@MatterID /*Populates EOB Cliamed Amount on Internal tab of Pet Claim Matter detail values*/
	END
	
	IF @ClaimRejection = 1
	BEGIN
		SELECT @Value = '£' + CAST(ISNULL(@ClaimTotal, '0.00') AS VARCHAR) + ' - £0.00'
	END
	
	IF EXISTS (SELECT * FROM dbo.MatterDetailValues WHERE DetailFieldID = 147608 AND MatterID = @MatterID) /*Claim - Paid*/
	BEGIN
		
		UPDATE dbo.MatterDetailValues
		SET DetailValue = @Value
		WHERE DetailFieldID = 147608 /*Claim - Paid*/
		AND MatterID = @MatterID
		AND ClientID = @ClientID
		
	END
	ELSE
	BEGIN
		
		INSERT dbo.MatterDetailValues (ClientID, DetailFieldID, DetailValue, LeadID, MatterID)
		VALUES (@ClientID, 147608, @Value, @LeadID, @MatterID) /*Claim - Paid*/
	
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SumClaimedAndApproved] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SumClaimedAndApproved] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SumClaimedAndApproved] TO [sp_executeall]
GO
