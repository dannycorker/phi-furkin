SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderFileFormat table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileFormat_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderFileFormatID int   = null ,

	@FileFormatName varchar (250)  = null ,

	@FileFormatDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderFileFormatID]
	, [FileFormatName]
	, [FileFormatDescription]
    FROM
	[dbo].[DataLoaderFileFormat] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileFormatID] = @DataLoaderFileFormatID OR @DataLoaderFileFormatID IS NULL)
	AND ([FileFormatName] = @FileFormatName OR @FileFormatName IS NULL)
	AND ([FileFormatDescription] = @FileFormatDescription OR @FileFormatDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderFileFormatID]
	, [FileFormatName]
	, [FileFormatDescription]
    FROM
	[dbo].[DataLoaderFileFormat] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileFormatID] = @DataLoaderFileFormatID AND @DataLoaderFileFormatID is not null)
	OR ([FileFormatName] = @FileFormatName AND @FileFormatName is not null)
	OR ([FileFormatDescription] = @FileFormatDescription AND @FileFormatDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileFormat_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Find] TO [sp_executeall]
GO
