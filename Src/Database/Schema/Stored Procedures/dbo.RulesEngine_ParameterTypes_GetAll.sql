SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-30
-- Description:	Returns all parameter types for the rule parameters
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterTypes_GetAll]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.RulesEngine_ParameterTypes WITH (NOLOCK) 
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterTypes_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterTypes_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterTypes_GetAll] TO [sp_executeall]
GO
