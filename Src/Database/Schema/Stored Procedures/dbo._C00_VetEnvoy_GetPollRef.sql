SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-21
-- Description:	Retrieves the last poll ref from a client level field for polling
-- =============================================
CREATE PROCEDURE [dbo].[_C00_VetEnvoy_GetPollRef]
(
	@ClientID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PollRef INT
	
	SELECT TOP 1 @PollRef = ValueInt
	FROM dbo.ClientDetailValues WITH (NOLOCK) 
	WHERE ClientID = 224
	AND DetailFieldID = 162656
	
	SELECT ISNULL(@PollRef, 0) AS PolRef

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_GetPollRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_VetEnvoy_GetPollRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_GetPollRef] TO [sp_executeall]
GO
