SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-10-2015
-- Description:	Save the customers thirdparty lead ID
-- =============================================
CREATE PROCEDURE [dbo].[Script__SaveCustomerThirdPartyLeadID]
	@CustomerID INT,
	@ClientID INT,
	@UserID INT,
	@ThirdPartyLeadID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ThirdPartyLeadID_DetailFieldID INT
	
	SELECT @ThirdPartyLeadID_DetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4200 AND LeadTypeID=0 -- Customer level field

    IF @ThirdPartyLeadID_DetailFieldID>0 --ThirdPartyLeadID has been mapped
    BEGIN
    	
    	DECLARE @TP_LeadID Varchar(2000)
    	SELECT @TP_LeadID = CONVERT(VARCHAR(2000), @ThirdPartyLeadID)
    	
    	EXEC _C00_SimpleValueIntoField @ThirdPartyLeadID_DetailFieldID, @TP_LeadID, @CustomerID, @UserID
		
    END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__SaveCustomerThirdPartyLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__SaveCustomerThirdPartyLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__SaveCustomerThirdPartyLeadID] TO [sp_executeall]
GO
