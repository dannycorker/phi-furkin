SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/11/2019
-- Description:	Inserts a quote agreement adjustment request/response
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderQuoteAgreementAdjustmentRequestAndResponse__Insert]
	@PremiumFunderQuoteAgreementAdjustmentRequestID INT = 0, 
	@PremiumFunderQuoteAgreementAdjustmentResponseID INT = 0, 
	@ClientID INT, 
	@CustomerID INT, 
	@MatterID INT = NULL, 
	@AdjustmentAmount DECIMAL(18,2),
	@AdjustmentType VARCHAR(16),
	@EffectiveDate DateTime,
	@RequestJson NVARCHAR(MAX),
	@ResponseJson NVARCHAR(MAX),
	@PremiumFunderRequestID INT, 
	@WhenCreated DATETIME, 
	@WhoCreated INT
AS
BEGIN

	SET NOCOUNT ON;

	IF @PremiumFunderQuoteAgreementAdjustmentRequestID > 0
	BEGIN

		UPDATE PremiumFunderQuoteAgreementAdjustmentRequest
		SET RequestJson = @RequestJson
		WHERE PremiumFunderQuoteAgreementAdjustmentRequestID = @PremiumFunderQuoteAgreementAdjustmentRequestID

	END
	ELSE
	BEGIN
		
		INSERT INTO PremiumFunderQuoteAgreementAdjustmentRequest ([ClientID], [CustomerID], [MatterID], [AdjustmentAmount], [AdjustmentType], [EffectiveDate], [RequestJson], [PremiumFunderRequestID], [WhenCreated], [WhoCreated])
		VALUES (@ClientID, @CustomerID, @MatterID, @AdjustmentAmount, @AdjustmentType, @EffectiveDate, @RequestJson, @PremiumFunderRequestID, @WhenCreated, @WhoCreated)

		SELECT PremiumFunderQuoteAgreementAdjustmentRequest = @PremiumFunderQuoteAgreementAdjustmentRequestID

	END
		
	IF @PremiumFunderQuoteAgreementAdjustmentResponseID > 0
	BEGIN

		UPDATE PremiumFunderQuoteAgreementAdjustmentResponse
		SET ResponseJson = @ResponseJson
		WHERE PremiumFunderQuoteAgreementAdjustmentResponseID = @PremiumFunderQuoteAgreementAdjustmentResponseID

	END
	ELSE
	BEGIN

		INSERT INTO PremiumFunderQuoteAgreementAdjustmentResponse ([PremiumFunderQuoteAgreementAdjustmentRequestID], [ClientID], [CustomerID], [MatterID], [ResponseJson], [PremiumFunderRequestID], [WhenCreated], [WhoCreated])
		VALUES (@PremiumFunderQuoteAgreementAdjustmentRequestID, @ClientID, @CustomerID, @MatterID, @ResponseJson, @PremiumFunderRequestID, @WhenCreated, @WhoCreated)

		SELECT @PremiumFunderQuoteAgreementAdjustmentResponseID = SCOPE_IDENTITY()

	END

	SELECT @PremiumFunderQuoteAgreementAdjustmentRequestID PremiumFunderQuoteAgreementAdjustmentRequestID, @PremiumFunderQuoteAgreementAdjustmentResponseID PremiumFunderQuoteAgreementAdjustmentResponseID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderQuoteAgreementAdjustmentRequestAndResponse__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderQuoteAgreementAdjustmentRequestAndResponse__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderQuoteAgreementAdjustmentRequestAndResponse__Insert] TO [sp_executeall]
GO
