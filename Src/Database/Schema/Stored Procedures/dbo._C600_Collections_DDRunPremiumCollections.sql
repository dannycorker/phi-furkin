SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 22-Oct-2014
-- Description:	Collections DD premium collection requests
-- Mods
--	2016-11-14 DCM record fielname in Payment table
--  2017-08-30 CPS use fn_C600_GetDatabaseSpecificConfigValue to get the correct file path by database for local
--  2017-09-19 JEL use fn_C600_GetDatabaseSpecificConfigValue to get the correct file path by database for sftp
--	2018-03-13 ACE Updated to use blackbox billing
--  2018-03-24 JEL Added receiptorPayment logic so we get the filename right
--  2018-03-24 JEL added client Account Logic 
--  2018-04-09 JEL Changed file name logic to get from ClientAccount and file path from single table row
--  2019-09-23 JML added some PRINT to help log time taken by this process for ticket #59376
--  2019-09-23 JML Changed logic to use #table over tvpGenericPaymentData and OPENXML rather than XML shreading to enhance performance for resolution to #59376
--  2019-09-23 JML brought in logic from _C600_Collections_DDCreateOutput and added to the proc to enhance performance for resolution to #59376
--	2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
--	2020-03-11 GPR for AAG-327 | Added call to fnGetAuBank for Australian Banks
--  2020-03-17 JEL Added to remove trailing space
--  2020-03-18 PR  Added @UseOutputFileFormat, when true it forces the scheduler to simply write the file, with no extra processing.
--	2020-03-27 GPR Added replacement of SUN in file name with SUN from ClientAccount for AAG-569
-- 2021-02-02 CPS for JIRA Furkin-184 | Call to [_C600_Collections_ClaimPaymentXml_Get] to retrieve HSBC-formatted XML
-- 2021-03-04 ACE - Use Client fields for test user details not aquarium_support!
-- 2021-04-21 ACE - FURKIN-62 Use new field for port number
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_DDRunPremiumCollections] 
(
	@XmlData XML	
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE 
			@AccountID INT,
			@Cert VARCHAR(2000),
			@ClientID INT = dbo.fnGetPrimaryClientID() /*2018-03-15 ACE Set the clientID*/,
			@CustomerID INT, 
			--@EffectiveDate DATE,
			@EPSTableFinal dbo.tvpGenericPaymentData,
			--@EventToAdd INT,
			@FileFormatID INT,
			@Filename VARCHAR(500),
			@LeadEventID INT, 
			@LocalFolder VARCHAR(500),
			@MatterID INT, 
			--@PayRef VARCHAR(50),
			@PW VARCHAR(100),
			@ServerName VARCHAR(500),
			@SFTPServerPortNumber INT,
			@Seq INT,
			@SFTPFolder VARCHAR(500),
			@TransactionCode VARCHAR(10),
			@Username VARCHAR(100),
			@WhoCreated INT = dbo.fn_C600_GetAqAutomationUser(),
			@TableRowID INT,
			@ClientAccountID INT,
			@ReceiptOrPayment INT,
			@FileAndFolderEntryToUse INT,
			@DebugMessage VARCHAR(2000) = '',
			@LogType	VARCHAR(20) = '',
			@StartTime	DATETIME = CURRENT_TIMESTAMP,
			@RunSeconds	INT
		
	SELECT TOP 1 @ReceiptOrPayment =  bacs.value('(ReceiptOrPayment)[1]','varchar(100)')
	FROM @XMLData.nodes('/row') AS Tbl(bacs)
	
	EXEC dbo.LogXML__AddEntry @ClientID, 0, 'DDRunPremiumCollections', @XmlData, NULL

	DECLARE @PaymentsToUpdate TABLE (PaymentID INT)

	/*Pick up the client Account ID for this run, will be uniform for each record per run*/  
	SELECT TOP 1 @ClientAccountID =  bacs.value('(ClientAccountID)[1]','varchar(100)')
	FROM @XMLData.nodes('/row') AS Tbl(bacs)
			
	SELECT @XmlData = CAST('<a>' + CONVERT(VARCHAR(MAX),@XmlData) + '</a>' as XML) -- OpenXml syntax requires a parent to the XML package, which is not supplied by the scheduler :(

	/*
	CPS 2019-05-15 following ongoing timeout issues on large collection days

	OPENXML Syntax treats the XML package as a proper document rather than converting it every time.  
	Doing it this way MASSIVELY improves performance vs a temp-table and nodes().
	For 8k records I cancelled the #TempTable, OptimiseFor @Xml = NULL option after an hour and a half with no results.
	The same XML package using OpenXml runs in a second.
	*/
	DECLARE @XmlDocumentID int
	EXEC sp_xml_preparedocument @XmlDocumentID OUTPUT, @XmlData -- Save the XML properly in a way that SSMS can read quickly.  Pick up the ID of the saved XML document so we can clear it later.
       
	SELECT @DebugMessage = CONVERT(VARCHAR,Current_Timestamp,121) + ' ' + 'Starting OpenXml approach.'
	PRINT @DebugMessage

	--INSERT @EPSTable (AccountName, AccountNumber, SortCode, Amount, BacsTransactionCode, Reference, Done, Rno)
	SELECT *, 0 [Done], ROW_NUMBER() OVER( PARTITION BY AccountNumber, SortCode ORDER BY AccountNumber ASC) [Rno]
	INTO #EPSTable
	FROM OPENXML(@XmlDocumentID, '/a/row', 2 )
	WITH (  CustomerID					INT    -- <CustomerID>...</CustomerID>
			,AccountHolderName			varchar(100)
			,AccountNumber				varchar(100)
			,SortCode					varchar(100)
			,Amount						money
			,TransactionCode			varchar(100)
			,Reference					varchar(100)
			,PaymentID					int
			--,Bank						VARCHAR(3) -- eg., ANZ  /*GPR 2020-03-11 removed, now using fnGetAuBank further down Sproc*/
			)

	SELECT @DebugMessage = CONVERT(VARCHAR,Current_Timestamp,121) + ' ' + CONVERT(VARCHAR,COUNT(*)) + ' #EPSTable'
	FROM #EPSTable r
	PRINT @DebugMessage

	EXEC sp_xml_removedocument @XmlDocumentID -- Clear out the saved XML package so we're not hanging on to that data unnecessarily

	IF EXISTS ( SELECT * FROM #EPSTable ) -- Modified By PR 10/03/2020 
	BEGIN

		-- Pick up the sftp location and details
		SELECT	     @SFTPFolder=dbo.fn_C600_GetDatabaseSpecificConfigValue('SFTPdb') /*JEL 2017-09-19*/--,@SFTPFolder=CASE WHEN ut.ValueInt=5144 THEN tf.DetailValue ELSE lf.DetailValue END
					,@LocalFolder = dbo.fn_C600_GetDatabaseSpecificConfigValue('SharedResourcesPath') + '\BACS\FromAquarium_Local\'  /*CPS 2017-08-30 (with James' approval!)*/
					,@ServerName	=CASE WHEN ut.ValueInt=5144 THEN tserv.DetailValue	ELSE serv.DetailValue	END
					,@SFTPServerPortNumber	= CASE WHEN ut.ValueInt=5144 THEN tserv_port.ValueInt	ELSE serv_port.ValueInt	END
					,@Username		=CASE WHEN ut.ValueInt=5144 THEN tun.DetailValue	ELSE un.DetailValue		END
					,@PW			=CASE WHEN ut.ValueInt=5144 THEN tpw.DetailValue	ELSE pw.DetailValue		END
					,@Cert			=CASE WHEN ut.ValueInt=5144 THEN tcrt.DetailValue	ELSE crt.DetailValue	END
		FROM ClientDetailValues tun WITH (NOLOCK) 
		INNER JOIN ClientDetailValues tpw WITH (NOLOCK) ON tun.ClientID=tpw.ClientID AND tpw.DetailFieldID=177170 
		INNER JOIN ClientDetailValues tcrt WITH (NOLOCK) ON tun.ClientID=tcrt.ClientID AND tcrt.DetailFieldID=177171 
		INNER JOIN ClientDetailValues tserv WITH (NOLOCK) ON tun.ClientID=tserv.ClientID AND tserv.DetailFieldID=177173 
		INNER JOIN ClientDetailValues tserv_port WITH (NOLOCK) ON tun.ClientID = tserv_port.ClientID AND tserv_port.DetailFieldID = 315909
		INNER JOIN ClientDetailValues un WITH (NOLOCK) ON tun.ClientID=un.ClientID AND un.DetailFieldID=177115 
		INNER JOIN ClientDetailValues pw WITH (NOLOCK) ON tun.ClientID=pw.ClientID AND pw.DetailFieldID=177116 
		INNER JOIN ClientDetailValues crt WITH (NOLOCK) ON tun.ClientID=crt.ClientID AND crt.DetailFieldID=177121 
		INNER JOIN ClientDetailValues serv WITH (NOLOCK) ON tun.ClientID=serv.ClientID AND serv.DetailFieldID=177172 
		INNER JOIN ClientDetailValues serv_port WITH (NOLOCK) ON tun.ClientID = serv_port.ClientID AND serv_port.DetailFieldID = 315910
		INNER JOIN ClientDetailValues ut WITH (NOLOCK) ON tun.ClientID=ut.ClientID AND ut.DetailFieldID=177168 
		WHERE tun.ClientID = @ClientID
		AND tun.DetailFieldID=177169 /*Test SFTPUserName*/


		/*Get the Filename from the ClientAccount Table*/	
		IF @ReceiptOrPayment = 1 /*Premium Collection*/ 
		BEGIN 
		
			SELECT @Filename = c.IncomingPaymentFileName 
			FROM ClientAccount c WITH ( NOLOCK ) 
			WHERE c.ClientAccountID = @ClientAccountID

		END 
		ELSE IF @ReceiptOrPayment = 2 /*Premium Refund or Claims Payment*/ 
		BEGIN 
	
			SELECT @Filename = c.OutgoingPaymentFileName 
			FROM ClientAccount c WITH ( NOLOCK ) 
			WHERE c.ClientAccountID = @ClientAccountID
		
		END

		/*GPR 2020-03-27*/
		DECLARE @SUN VARCHAR(10)
		DECLARE @SUNlength INT
		SELECT @SUN = ca.SUN FROM ClientAccount ca WITH (NOLOCK)
		WHERE ca.ClientAccountID = @ClientAccountID

		SELECT @SUNlength = LEN(@SUN)
		/*Replace out the date fields*/ 
		SELECT	 @Filename=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Filename
					,'|DD|',RIGHT('0' + CAST(DATEPART(DD,dbo.fn_GetDate_Local()) AS VARCHAR),2))
					,'|MM|',RIGHT('0' + CAST(DATEPART(MM,dbo.fn_GetDate_Local()) AS VARCHAR),2))
					,'|YYYY|',RIGHT('0' + CAST(DATEPART(YYYY,dbo.fn_GetDate_Local()) AS VARCHAR),4))
					,'|HH|',RIGHT('0' + CAST(DATEPART(HH,dbo.fn_GetDate_Local()) AS VARCHAR),2))
					,'|NN|',RIGHT('0' + CAST(DATEPART(MINUTE,dbo.fn_GetDate_Local()) AS VARCHAR),2))
					,'|SS|',RIGHT('0' + CAST(DATEPART(SS,dbo.fn_GetDate_Local()) AS VARCHAR),2))
					,'SUN',RIGHT('0'  + @SUN,@SUNlength)) /*GPR 2020-03-27*/
				
		-- 2021-02-08 CPS for JIRA FURKIN-184 | Build the appropriate file after we've generated the filename so we can pass that filename into the procedure
		DECLARE  @XmlOutput		XML 
				,@BacsFileID	INT

		IF 2 /*Claim*/ = ( SELECT AccountUseID FROM ClientAccount ca WITH (NOLOCK) WHERE ca.ClientAccountID = @ClientAccountID )
		BEGIN
			EXEC _C600_Collections_ClaimPaymentXml_HsbcCanada @XmlData = @XmlData, @ClientAccountID = @ClientAccountID, @FileName = @FileName, @XmlOutput = @XmlOutput OUTPUT
		END
		ELSE
		BEGIN
			SELECT @XmlOutput = '<Pending>Premium Collection Not Included in FURKIN-13</Pending>'
		END
	
		-- Modified By PR 2020-03-18 - forces scheduler to simply write the file, with no extra processing.
		DECLARE @UseOutputFileFormat BIT = 0

		-- 2021-02-08 CPS for JIRA FURKIN-184 | Write these to a temp table so we can both log and output them
		SELECT @Filename AS [ExportFileName],
				@LocalFolder AS [ExportFilePath],
				0  AS [OutputHeaderRow],
				@ServerName AS [SFTPServer],
				@Username AS [SFTPUserName],
				@PW AS [SFTPPassword],
				@SFTPFolder AS [SFTPFolder],
				@Cert AS [SFTPCertificate],
				ISNULL(@SFTPServerPortNumber, 22) AS [SFTPPort],
				@UseOutputFileFormat [UseOutputFileFormat]
		INTO #FileHeaders

		SELECT *
		FROM #FileHeaders WITH (NOLOCK) 
	
		-- update payment status (to processed) & record filename
		UPDATE p
		SET PaymentFileName=@Filename
		FROM Payment p 
		INNER JOIN #EPSTable pr ON p.PaymentID=pr.PaymentID
		
		-- create BACS file entry
		INSERT INTO dbo.BACSFile (ClientID, BACSFileFormatID, [FileName], ExportLocation, NumberOfRowsInFile, TotalCreditValue, TotalDebitValue, TotalValue, ExportDate, WhoCreated, WhenCreated) 
		SELECT @ClientID,12,@Filename,@SFTPFolder,COUNT(*),SUM(Amount),0,SUM(Amount),dbo.fn_GetDate_Local(),@WhoCreated,dbo.fn_GetDate_Local()
		FROM #EPSTable e

		SELECT @BacsFileID = SCOPE_IDENTITY()

		SELECT '<?xml version="1.0" encoding="UTF-8"?>' + REPLACE(CAST(@XmlOutput as VARCHAR(MAX)),' xmlns=""','')

		EXEC _C00_LogItXml @ClientID, @BacsFileID, '_C600_Collections_DDRunPremiumCollections', @XmlOutput, @Filename

		DECLARE @FileHeaderXml XML = 
		(
		SELECT *
		FROM #FileHeaders fhx WITH (NOLOCK) 
		FOR XML PATH('')
		)
		EXEC _C00_LogItXml @ClientID, @BacsFileID, '_C600_Collections_DDRunPremiumCollections', @FileHeaderXml, @Filename
							
	END

	SELECT @RunSeconds = DATEDIFF(SECOND,@StartTime,CURRENT_TIMESTAMP)
	SELECT @DebugMessage = 'Procedure Completed in ' + ISNULL(CONVERT(VARCHAR,@RunSeconds),'NULL') + ' seconds.' + CASE WHEN @RunSeconds > 25 THEN '. Raise Billing Warning.' ELSE '' END
	SELECT @LogType = CASE WHEN @RunSeconds > 25 THEN 'Error' ELSE 'Info' END

	EXEC _C00_LogIt @LogType,'_C600_Collections_DDRunPremiumCollections','ProcedureEnd',@DebugMessage,58552 /*Aq Automation*/

END








GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_DDRunPremiumCollections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_DDRunPremiumCollections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_DDRunPremiumCollections] TO [sp_executeall]
GO
