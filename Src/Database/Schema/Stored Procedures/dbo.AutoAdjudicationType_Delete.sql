SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AutoAdjudicationType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_Delete]
(

	@AutoAdjudicationTypeID int   
)
AS


				DELETE FROM [dbo].[AutoAdjudicationType] WITH (ROWLOCK) 
				WHERE
					[AutoAdjudicationTypeID] = @AutoAdjudicationTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Delete] TO [sp_executeall]
GO
