SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DashboardItemType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DashboardItemTypeID],
					[DashboardItemTypeName],
					[ClientID]
				FROM
					[dbo].[DashboardItemType] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_GetByClientID] TO [sp_executeall]
GO
