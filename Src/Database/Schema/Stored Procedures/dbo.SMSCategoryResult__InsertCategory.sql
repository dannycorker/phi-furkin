SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-08-01
-- Description:	Creates an SMS Response Categorisation Results row for the given category
-- =============================================
CREATE PROCEDURE [dbo].[SMSCategoryResult__InsertCategory]
	@MatterID INT,
	@Category VARCHAR(250),
	@WordOrPhrase VARCHAR(250)
	

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadTypeID INT
	DECLARE @LeadID INT
	DECLARE @ClientID INT
	SELECT @LeadID=LeadID, @ClientID=ClientID FROM Matter WITH (NOLOCK)  WHERE MatterID=@MatterID
	SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

	DECLARE @TableFieldID INT
	DECLARE @CategoryColumnDetailFieldID INT	
	SELECT @TableFieldID=DetailFieldID, @CategoryColumnDetailFieldID=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=900 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID
	
	DECLARE @WordOrPhraseColumnDetailFieldID INT	
	SELECT @WordOrPhraseColumnDetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=901 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID

	DECLARE @WordCountColumnDetailFieldID INT	
	SELECT @WordCountColumnDetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=902 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID

	DECLARE @TableRowID INT

	IF @CategoryColumnDetailFieldID IS NOT NULL AND @CategoryColumnDetailFieldID<>0
	BEGIN
		
		DECLARE @DetailFieldPageID INT
		
		SELECT @DetailFieldPageID=DetailFieldPageID 
		FROM DetailFields WITH (NOLOCK) WHERE DetailFieldID=@TableFieldID
	
		INSERT INTO TableRows (ClientID, MatterID, DetailFieldID,DetailFieldPageID)
		VALUES (@ClientID, @MatterID, @TableFieldID, @DetailFieldPageID)			
		SET @TableRowID = SCOPE_IDENTITY()

		INSERT INTO TableDetailValues (TableRowID, DetailFieldID, DetailValue, MatterID,ClientID)
		VALUES (@TableRowID, @CategoryColumnDetailFieldID, @Category,@MatterID,@ClientID)
		
		INSERT INTO TableDetailValues (TableRowID, DetailFieldID, DetailValue, MatterID,ClientID)
		VALUES (@TableRowID, @WordOrPhraseColumnDetailFieldID, @WordOrPhrase,@MatterID,@ClientID)

		INSERT INTO TableDetailValues (TableRowID, DetailFieldID, DetailValue, MatterID,ClientID)
		VALUES (@TableRowID, @WordCountColumnDetailFieldID, '1', @MatterID,@ClientID)
	
	END
	
	SELECT @TableRowID TableRowID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__InsertCategory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCategoryResult__InsertCategory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__InsertCategory] TO [sp_executeall]
GO
