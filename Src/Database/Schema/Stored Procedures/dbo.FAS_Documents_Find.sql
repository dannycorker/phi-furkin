SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FAS_Documents table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_Find]
(

	@SearchUsingOR bit   = null ,

	@FasDocumentID int   = null ,

	@FasCheckerDataID int   = null ,

	@DocumentBlob image   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FasDocumentID]
	, [FasCheckerDataID]
	, [DocumentBlob]
    FROM
	[dbo].[FAS_Documents] WITH (NOLOCK) 
    WHERE 
	 ([FasDocumentID] = @FasDocumentID OR @FasDocumentID IS NULL)
	AND ([FasCheckerDataID] = @FasCheckerDataID OR @FasCheckerDataID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FasDocumentID]
	, [FasCheckerDataID]
	, [DocumentBlob]
    FROM
	[dbo].[FAS_Documents] WITH (NOLOCK) 
    WHERE 
	 ([FasDocumentID] = @FasDocumentID AND @FasDocumentID is not null)
	OR ([FasCheckerDataID] = @FasCheckerDataID AND @FasCheckerDataID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Find] TO [sp_executeall]
GO
