SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the InvoiceMapping table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMapping_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [InvoiceMappingID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([InvoiceMappingID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [InvoiceMappingID]'
				SET @SQL = @SQL + ' FROM [dbo].[InvoiceMapping] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[InvoiceMappingID], O.[ClientID], O.[LeadTypeID], O.[SourceInvoiceNumberField], O.[SourceTotalClaimAmountField], O.[SourceFeesToPostToSageNetField], O.[SourceFeesToPostToSageVatField], O.[InvoiceTypeCode], O.[CreditNoteTypeCode], O.[InvoiceNumberField], O.[DateField], O.[DatePostedField], O.[DetailsField], O.[SentToSageField], O.[NominalCodeField], O.[TaxCodeField], O.[InvoiceNetAmountField], O.[InvoiceTaxAmountField], O.[TypeField], O.[MatterIDField], O.[CaseNumberField], O.[CreditNoteNetAmountField], O.[CreditNoteTaxAmountField], O.[NominalCode], O.[TaxCode], O.[TableRowsDetailFieldID], O.[TableRowsDetailFieldPageID], O.[FeesInvoicedToSageField], O.[FeesPaidFromSageField], O.[FeesInvoicedOtherField], O.[FeesPaidOtherField], O.[CaseBalanceField], O.[SageCompanyName], O.[CostCentre]
				FROM
				    [dbo].[InvoiceMapping] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[InvoiceMappingID] = PageIndex.[InvoiceMappingID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[InvoiceMapping] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMapping_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_GetPaged] TO [sp_executeall]
GO
