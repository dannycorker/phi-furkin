SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 20-Nov-2014
-- Description:	Move all detail values to a new LeadID
-- Mods
--  2015-10-13 DCM Introduce the possibility of a match on a Q&B multipet purchase
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MoveLeadToExistingCustomer]
(
	@ExistingCustomerID INT,
	@MatchCustomerID INT,
	@MatchLeadID INT,
	@MatchCaseID INT,
	@MatchMatterID INT,
	@WhoCreated INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LogEntry VARCHAR(2000)
	SELECT @LogEntry='Args: @ExistingCustomerID - ' + CONVERT(VARCHAR(30),@ExistingCustomerID) + 
					'; @MatchCustomerID- ' + CONVERT(VARCHAR(30),@MatchCustomerID) +
					'; @MatchLeadID - ' + CONVERT(VARCHAR(30),@MatchLeadID) +
					'; @MatchCaseID - ' + CONVERT(VARCHAR(30),@MatchCaseID) +
					'; @MatchMatterID - ' + CONVERT(VARCHAR(30),@MatchMatterID) 
	EXEC _C00_LogIt 'Info', '_C600_MoveLeadToExistingCustomer', 'Collecting Test Data', @LogEntry, 33135 

--declare
--	@ExistingCustomerID INT = 20013739,
--	@MatchCustomerID INT = 20013738,
--	@MatchLeadID INT = 30022092,
--	@MatchCaseID INT = 40029376,
--	@MatchMatterID INT = 50029403,
--	@WhoCreated INT = 38911


	/*Declaration for table variable for the cheque data to pass into SP*/
	DECLARE @CaseID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@ColMatterID VARCHAR(50),
			@LeadID INT,
			@MatchType VARCHAR(50),
			@MatterID INT,
			@MatterID1 INT,
			@MattersToCopy tvpIntInt,
			@NumMatters INT
	
	/* If the customer has multiple leads copy all of them in leadid order setting 
	   the match fields in all of them as required:
	   
		 * if an exisitng account is to be used set all match fields identically; or
		
		 * if a new account is to be created set the match fields as if the new leads had come from Q&B
		
	*/
	INSERT INTO @MattersToCopy
	SELECT * FROM dbo.fn_C00_1272_Policy_GetPreMatchPetList(@MatchCustomerID)
	UPDATE @MattersToCopy SET ID2=0
	
	SELECT @NumMatters=COUNT(*) FROM @MattersToCopy

	IF @NumMatters > 1
	BEGIN
	
		-- set the matching identifiers
		SELECT @ColMatterID=dbo.fnGetDv(175279,@MatchCaseID),
				@MatchType=dbo.fnGetDv(175280,@MatchCaseID)

		IF CAST(@ColMatterID AS INT) > 0 -- using an existing collections matters
		BEGIN

			UPDATE mdv
			SET DetailValue=CASE mdv.DetailFieldID 
					WHEN 175285 THEN @ExistingCustomerID
					WHEN 175279 THEN @ColMatterID
					WHEN 175280 THEN @MatchType
					END
			FROM MatterDetailValues mdv 
			INNER JOIN @MattersToCopy mtc ON mdv.MatterID=mtc.ID1
			WHERE mdv.DetailFieldID IN (175285,175279,175280)
		
		END
		ELSE -- creating a new collections matter
		BEGIN
		
			SELECT @MatterID1=MIN(ID1) FROM @MattersToCopy

			UPDATE mdv
			SET DetailValue=CASE mdv.DetailFieldID 
					WHEN 175285 THEN CAST(@ExistingCustomerID AS VARCHAR)
					WHEN 175279 THEN ''
					WHEN 175280 THEN '72009'
					END
			FROM MatterDetailValues mdv 
			WHERE mdv.DetailFieldID IN (175285,175279,175280) AND mdv.MatterID=@MatterID1
		
			UPDATE mdv
			SET DetailValue=CASE mdv.DetailFieldID 
					WHEN 175285 THEN CAST(@MatterID1 AS VARCHAR)
					WHEN 175279 THEN ''
					WHEN 175280 THEN '72330'
					END
			FROM MatterDetailValues mdv 
			INNER JOIN @MattersToCopy mtc ON mdv.MatterID=mtc.ID1
			WHERE mdv.DetailFieldID IN (175285,175279,175280)
				AND mdv.MatterID<>@MatterID1			
		
		END
	
	END

	/* Create a new Policy Admin lead, case, matter for the Customer */
	WHILE EXISTS ( SELECT * FROM @MattersToCopy WHERE ID2=0 )
	BEGIN
	
		SELECT TOP 1 @MatchMatterID=ID1 FROM @MattersToCopy WHERE ID2=0 ORDER BY ID1
		
		SELECT @MatchLeadID=LeadID,@MatchCaseID=CaseID 
		FROM Matter WITH (NOLOCK) 
		WHERE MatterID=@MatchMatterID

		EXEC @MatterID = _C00_CreateNewLeadForCust @ExistingCustomerID,1492,@ClientID,@WhoCreated,1,1,0
		SELECT @LeadID=m.LeadID, @CaseID=m.CaseID 
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID=@MatterID

		/* update Leadid in tables:
			Cases
			LeadDetailValues
			Matter
			MatterDetailValues
			TableDetailValues
			TableRows
		*/
		
		UPDATE LeadDetailValues SET LeadID=@LeadID WHERE ClientID=@ClientID AND LeadID=@MatchLeadID 
		UPDATE MatterDetailValues SET LeadID=@LeadID WHERE ClientID=@ClientID AND LeadID=@MatchLeadID 
		UPDATE TableDetailValues SET LeadID=@LeadID, 
			CustomerID=CASE WHEN CustomerID IS NOT NULL THEN @ExistingCustomerID ELSE NULL END, 
			CaseID=CASE WHEN CaseID IS NOT NULL THEN @CaseID ELSE NULL END
			WHERE ClientID=@ClientID AND LeadID=@MatchLeadID 
		UPDATE TableRows SET LeadID=@LeadID, 
			CustomerID=CASE WHEN CustomerID IS NOT NULL THEN @ExistingCustomerID ELSE NULL END,
			CaseID=CASE WHEN CaseID IS NOT NULL THEN @CaseID ELSE NULL END
			WHERE ClientID=@ClientID AND LeadID=@MatchLeadID
		-- exchange Matters		 
		UPDATE Matter SET LeadID=@LeadID, CustomerID=@ExistingCustomerID, CaseID=@CaseID WHERE ClientID=@ClientID AND MatterID=@MatchMatterID 
		UPDATE Matter SET LeadID=@MatchLeadID, CustomerID=@MatchCustomerID, CaseID=@MatchCaseID WHERE ClientID=@ClientID AND MatterID=@MatterID 

		UPDATE @MattersToCopy SET ID2=1 WHERE ID1=@MatchMatterID
	
	END
	

	/* add start event to new case */	
	EXEC dbo.AutomatedTask__RunNow 19828

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MoveLeadToExistingCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MoveLeadToExistingCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MoveLeadToExistingCustomer] TO [sp_executeall]
GO
