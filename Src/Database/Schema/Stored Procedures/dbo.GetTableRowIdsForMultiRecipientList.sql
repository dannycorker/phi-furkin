SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Chris Townsend
-- Create date: 2009-23-04
-- Description:	Gets a list of TableRowIDs for the scheduler to invoke multiple recipient events
-- =============================================

CREATE PROCEDURE [dbo].[GetTableRowIdsForMultiRecipientList]

	@ClientID int,
	@DetailFieldPageID int,
	@CaseID int
	
AS
BEGIN

	SELECT     
		tr.TableRowID, 
		m.CaseID 
	FROM 
		TableRows tr
	INNER JOIN 
		Matter m on m.MatterID = tr.MatterID
	WHERE
		tr.ClientID = @ClientID
		AND tr.DetailFieldPageID = @DetailFieldPageID
		AND m.CaseID = @CaseID

END








GO
GRANT VIEW DEFINITION ON  [dbo].[GetTableRowIdsForMultiRecipientList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTableRowIdsForMultiRecipientList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTableRowIdsForMultiRecipientList] TO [sp_executeall]
GO
