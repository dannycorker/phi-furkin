SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType_Insert]
(

	@LeadTypeID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeName varchar (50)  ,

	@LeadTypeDescription varchar (255)  ,

	@Enabled bit   ,

	@Live bit   ,

	@MatterDisplayName varchar (100)  ,

	@LastReferenceInteger int   ,

	@ReferenceValueFormatID int   ,

	@IsBusiness bit   ,

	@LeadDisplayName varchar (100)  ,

	@CaseDisplayName varchar (100)  ,

	@CustomerDisplayName varchar (100)  ,

	@LeadDetailsTabImageFileName varchar (255)  ,

	@CustomerTabImageFileName varchar (255)  ,

	@UseEventCosts bit   ,

	@UseEventUOEs bit   ,

	@UseEventDisbursements bit   ,

	@UseEventComments bit   ,

	@AllowMatterPageChoices bit   ,

	@AllowLeadPageChoices bit   ,

	@IsRPIEnabled bit   ,

	@FollowupWorkingDaysOnly bit   ,

	@MatterLastReferenceInteger int   ,

	@MatterReferenceValueFormatID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@CalculateAllMatters bit   ,

	@SmsGatewayID int   
)
AS


				
				INSERT INTO [dbo].[LeadType]
					(
					[ClientID]
					,[LeadTypeName]
					,[LeadTypeDescription]
					,[Enabled]
					,[Live]
					,[MatterDisplayName]
					,[LastReferenceInteger]
					,[ReferenceValueFormatID]
					,[IsBusiness]
					,[LeadDisplayName]
					,[CaseDisplayName]
					,[CustomerDisplayName]
					,[LeadDetailsTabImageFileName]
					,[CustomerTabImageFileName]
					,[UseEventCosts]
					,[UseEventUOEs]
					,[UseEventDisbursements]
					,[UseEventComments]
					,[AllowMatterPageChoices]
					,[AllowLeadPageChoices]
					,[IsRPIEnabled]
					,[FollowupWorkingDaysOnly]
					,[MatterLastReferenceInteger]
					,[MatterReferenceValueFormatID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[CalculateAllMatters]
					,[SmsGatewayID]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeName
					,@LeadTypeDescription
					,@Enabled
					,@Live
					,@MatterDisplayName
					,@LastReferenceInteger
					,@ReferenceValueFormatID
					,@IsBusiness
					,@LeadDisplayName
					,@CaseDisplayName
					,@CustomerDisplayName
					,@LeadDetailsTabImageFileName
					,@CustomerTabImageFileName
					,@UseEventCosts
					,@UseEventUOEs
					,@UseEventDisbursements
					,@UseEventComments
					,@AllowMatterPageChoices
					,@AllowLeadPageChoices
					,@IsRPIEnabled
					,@FollowupWorkingDaysOnly
					,@MatterLastReferenceInteger
					,@MatterReferenceValueFormatID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@CalculateAllMatters
					,@SmsGatewayID
					)
				-- Get the identity value
				SET @LeadTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Insert] TO [sp_executeall]
GO
