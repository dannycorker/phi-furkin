SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the TemplateType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TemplateType_Find]
(

	@SearchUsingOR bit   = null ,

	@TemplateTypeID int   = null ,

	@TemplateTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TemplateTypeID]
	, [TemplateTypeName]
    FROM
	[dbo].[TemplateType] WITH (NOLOCK) 
    WHERE 
	 ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([TemplateTypeName] = @TemplateTypeName OR @TemplateTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TemplateTypeID]
	, [TemplateTypeName]
    FROM
	[dbo].[TemplateType] WITH (NOLOCK) 
    WHERE 
	 ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([TemplateTypeName] = @TemplateTypeName AND @TemplateTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TemplateType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Find] TO [sp_executeall]
GO
