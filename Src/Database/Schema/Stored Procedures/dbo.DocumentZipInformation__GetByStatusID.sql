SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By:	Aquarium Software (https://www.aquarium-software.com)
-- Author:		Paul Richardson
-- Purpose:		Gets a record(s) from DocumentZipInformation table where the StatusID = @StatusID
-- Date:		2009-02-13
-- Updates:		2010-01-13: CT: Added JOINs to DocumentZip and DocumentQueue to get	'WhoCreated' 
--              2010-02-26 JWG Only get the top 1 record for each dzi.  Some clients zip together all sorts
--                         of documents created by different users, which was creating duplicates even with DISTINCT applied.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[DocumentZipInformation__GetByStatusID] 
(
	@StatusID int   
)
AS
BEGIN

	IF CAST(CURRENT_TIMESTAMP as TIME) BETWEEN '06:15:00' AND '20:00:00'
	BEGIN
	
		DECLARE @Rerun NVARCHAR(MAX) = ''

		SELECT @Rerun += 'exec DocumentZip__KillAndCure '+CAST(d.DocumentZipInformationID as VARCHAR(10))+','+CAST(d.DocumentsPerZip as VARCHAR(10))+',0 '
		FROM DocumentZipInformation d WITH (NOLOCK) 
		WHERE d.StatusID = 34
		AND d.WhenCreated > DATEADD(day,-10,dbo.fn_GetDate_Local())
		AND NOT EXISTS (SELECT * FROM DocumentZip dz WITH (NOLOCK) 
						WHERE dz.DocumentZipInformationID = d.DocumentZipInformationID 
						AND dz.StatusID NOT IN (35,31))
		IF @Rerun > ''
		BEGIN
			EXEC (@Rerun)
		END
		
	END
	;
	WITH d AS (
		SELECT dzi.[DocumentZipInformationID],
		dzi.[ClientID],
		dzi.[ServerMapPath],
		dzi.[SecurePath],
		dzi.[HostPath],
		dzi.[WordTempDirectory],
		dzi.[DocumentsPerZip],
		dzi.[ZipPrefix],
		dzi.[StatusID],
		IsNull(dzi.[TriggeredByAutomatedTaskID], 0) AS [TriggeredByAutomatedTaskID],
		IsNull(dzi.[TriggeredByDocumentTypeID], 0) AS [TriggeredByDocumentTypeID],
		dq.WhoCreated AS 'ClientPersonnelID', 
		row_number() over(partition by dzi.[DocumentZipInformationID] order by dq.WhoCreated) as rn, 
		IsNull(dzi.EmailOnSuccess, '') AS [EmailOnSuccess],
		IsNull(dzi.EmailOnError, '') AS [EmailOnError] 
		FROM [dbo].[DocumentZipInformation] dzi
		INNER JOIN DocumentZip dz ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID
		INNER JOIN DocumentQueue dq ON dq.DocumentQueueID = dz.DocumentQueueID
		WHERE dzi.[StatusID] = @StatusID
	) 
	SELECT * FROM d
	WHERE d.rn = 1 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetByStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__GetByStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__GetByStatusID] TO [sp_executeall]
GO
