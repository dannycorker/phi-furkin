SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryCriteriaSet table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSet_GetBySqlQueryID]
(

	@SqlQueryID int   
)
AS


				SELECT
					[SqlQueryCriteriaSetID],
					[ClientID],
					[SqlQueryID],
					[SetName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[SqlQueryCriteriaSet] WITH (NOLOCK) 
				WHERE
										[SqlQueryID] = @SqlQueryID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_GetBySqlQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSet_GetBySqlQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_GetBySqlQueryID] TO [sp_executeall]
GO
