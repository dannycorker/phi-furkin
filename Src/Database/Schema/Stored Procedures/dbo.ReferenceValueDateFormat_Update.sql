SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReferenceValueDateFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDateFormat_Update]
(

	@ReferenceValueDateFormatID int   ,

	@DateFormat varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReferenceValueDateFormat]
				SET
					[DateFormat] = @DateFormat
				WHERE
[ReferenceValueDateFormatID] = @ReferenceValueDateFormatID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDateFormat_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Update] TO [sp_executeall]
GO
