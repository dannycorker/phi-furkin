SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FunctionType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_GetByFunctionTypeID]
(

	@FunctionTypeID int   
)
AS


				SELECT
					[FunctionTypeID],
					[ModuleID],
					[ParentFunctionTypeID],
					[FunctionTypeName],
					[Level],
					[IsVirtual],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[FunctionType] WITH (NOLOCK) 
				WHERE
										[FunctionTypeID] = @FunctionTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_GetByFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_GetByFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_GetByFunctionTypeID] TO [sp_executeall]
GO
