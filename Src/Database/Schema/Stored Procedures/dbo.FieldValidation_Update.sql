SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FieldValidation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_Update]
(

	@FieldValidationID int   ,

	@ClientID int   ,

	@FieldID int   ,

	@ValidationGroup varchar (50)  ,

	@ValidationType int   ,

	@MaxValue int   ,

	@MinValue int   ,

	@ErrorMessage varchar (50)  ,

	@AccessLevelRestrictions varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FieldValidation]
				SET
					[ClientID] = @ClientID
					,[FieldID] = @FieldID
					,[ValidationGroup] = @ValidationGroup
					,[ValidationType] = @ValidationType
					,[MaxValue] = @MaxValue
					,[MinValue] = @MinValue
					,[ErrorMessage] = @ErrorMessage
					,[AccessLevelRestrictions] = @AccessLevelRestrictions
				WHERE
[FieldValidationID] = @FieldValidationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Update] TO [sp_executeall]
GO
