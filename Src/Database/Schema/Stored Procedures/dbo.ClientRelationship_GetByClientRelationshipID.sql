SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientRelationship table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_GetByClientRelationshipID]
(

	@ClientRelationshipID int   
)
AS


				SELECT
					[ClientRelationshipID],
					[ClientRelationshipName],
					[OutgoingClientID],
					[ReceivingClientID],
					[OutgoingLeadTypeID],
					[IncomingLeadTypeID],
					[OutgoingEventTypeID],
					[IncomingEventTypeID],
					[Enabled],
					[ClearLeadRefs]
				FROM
					[dbo].[ClientRelationship] WITH (NOLOCK) 
				WHERE
										[ClientRelationshipID] = @ClientRelationshipID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_GetByClientRelationshipID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_GetByClientRelationshipID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_GetByClientRelationshipID] TO [sp_executeall]
GO
