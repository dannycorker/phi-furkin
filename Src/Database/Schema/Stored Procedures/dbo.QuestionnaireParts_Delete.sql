SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuestionnaireParts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireParts_Delete]
(

	@PartNameID int   
)
AS


				DELETE FROM [dbo].[QuestionnaireParts] WITH (ROWLOCK) 
				WHERE
					[PartNameID] = @PartNameID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireParts_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Delete] TO [sp_executeall]
GO
