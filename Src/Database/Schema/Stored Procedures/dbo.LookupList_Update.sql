SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LookupList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_Update]
(

	@LookupListID int   ,

	@LookupListName varchar (500)  ,

	@LookupListDescription varchar (2000)  ,

	@ClientID int   ,

	@Enabled bit   ,

	@SortOptionID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@LeadTypeID int   ,

	@IsShared bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LookupList]
				SET
					[LookupListName] = @LookupListName
					,[LookupListDescription] = @LookupListDescription
					,[ClientID] = @ClientID
					,[Enabled] = @Enabled
					,[SortOptionID] = @SortOptionID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[LeadTypeID] = @LeadTypeID
					,[IsShared] = @IsShared
				WHERE
[LookupListID] = @LookupListID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Update] TO [sp_executeall]
GO
