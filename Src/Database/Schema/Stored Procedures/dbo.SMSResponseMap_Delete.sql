SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SMSResponseMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSResponseMap_Delete]
(

	@SMSResponseMapID int   
)
AS


				DELETE FROM [dbo].[SMSResponseMap] WITH (ROWLOCK) 
				WHERE
					[SMSResponseMapID] = @SMSResponseMapID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_Delete] TO [sp_executeall]
GO
