SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Add an Outcome from eCatcher
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[AddOutcome]
	@ClientQuestionnaireID INT,
	@OutcomeName NVARCHAR(50),
	@OutcomeDescription TEXT,
	@SmsWhenFound BIT,
	@MobileNumber NVARCHAR(50),
	@EmailCustomer BIT,
	@CustomersEmail TEXT,
	@ThankYouPage TEXT,
	@EmailFromAddress NVARCHAR(255),
	@SmsToCustomer NVARCHAR(160),
	@CustomersEmailSubject NVARCHAR(500),
	@ClientID INT,
	@EmailClient BIT,
	@ClientEmail NVARCHAR(255),
	@WhoEdited INT 
AS
BEGIN

	INSERT INTO Outcomes (ClientQuestionnaireID, OutcomeName, OutcomeDescription,SmsWhenFound, MobileNumber, EmailCustomer, CustomersEmail, ThankYouPage, EmailFromAddress,SmsToCustomer, CustomersEmailSubject, ClientID, EmailClient, ClientEmail, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@ClientQuestionnaireID, @OutcomeName, @OutcomeDescription, @SmsWhenFound, @MobileNumber, @EmailCustomer, @CustomersEmail, @ThankYouPage, @EmailFromAddress, @SmsToCustomer, @CustomersEmailSubject, @ClientID, @EmailClient, @ClientEmail, @WhoEdited, dbo.fn_GetDate_Local(), @WhoEdited, dbo.fn_GetDate_Local())

	DECLARE @OutcomeID INT

	SET @OutcomeID = SCOPE_IDENTITY()

	SELECT OutcomeID  FROM Outcomes  WHERE OutcomeID = @OutcomeID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcome] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddOutcome] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcome] TO [sp_executeall]
GO
