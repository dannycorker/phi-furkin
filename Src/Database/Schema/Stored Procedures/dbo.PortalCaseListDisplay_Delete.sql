SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalCaseListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalCaseListDisplay_Delete]
(

	@PortalCaseListDisplayID int   
)
AS


				DELETE FROM [dbo].[PortalCaseListDisplay] WITH (ROWLOCK) 
				WHERE
					[PortalCaseListDisplayID] = @PortalCaseListDisplayID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalCaseListDisplay_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Delete] TO [sp_executeall]
GO
