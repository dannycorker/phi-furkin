SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteClientQuestionnaire    Script Date: 08/09/2006 12:22:46 ******/
CREATE PROCEDURE [dbo].[DeleteClientQuestionnaire] @ClientQuestionnaireID int
AS
Delete From ClientQuestionnaires
Where ClientQuestionnaireID = @ClientQuestionnaireID




GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteClientQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientQuestionnaire] TO [sp_executeall]
GO
