SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-10-06
-- Description:	Get Quote Aggregator
-- 2019-06-21 GPR applied fix for #57791
-- 2019-10-11 GPR removed redundent code blocks (previously commented out and not used in C60X build) for LPC-35
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_GetQuoteAggregator]
(
	@XmlRequest XML,
	@AggregatorID VARCHAR(250)
)
AS
BEGIN

	DECLARE @QuoteSessionID INT
	DECLARE @StrippedXML XML
	DECLARE @XmlQuoteResponse XML = ''
	DECLARE @SavedQuoteKey UNIQUEIDENTIFIER = NEWID()
	DECLARE @SavedQuoteKeyXML XML = '<SavedQuoteKey>' + CAST(@SavedQuoteKey AS VARCHAR(36)) +'</SavedQuoteKey>'
	DECLARE @RetrieveQuoteUrlXML XML
	DECLARE @AggregatorDefault VARCHAR(10) = 'false' -- GPR 2019-06-21
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	
	/*GPR 2019-10-09 updated for C603*/
	IF DB_NAME() = 'Aquarius603Dev'
	BEGIN
		SET @RetrieveQuoteUrlXML = '<RetrieveQuoteUrl>https://lpc-dev.aquarium-software.com/quotebuyapi/Quote/RetrieveQuote?SavedQuoteKey='+ CAST(@SavedQuoteKey AS VARCHAR(36)) +'</RetrieveQuoteUrl>'
	END
	ELSE IF DB_NAME() = 'Aquarius603QA'
	BEGIN
		SET @RetrieveQuoteUrlXML = '<RetrieveQuoteUrl>https://lpc-dev.aquarium-software.com/quotebuyapi/Quote/RetrieveQuote?SavedQuoteKey='+ CAST(@SavedQuoteKey AS VARCHAR(36)) +'</RetrieveQuoteUrl>'
	END
	ELSE IF DB_NAME() = 'Aquarius603'
	BEGIN
		SET @RetrieveQuoteUrlXML = '<RetrieveQuoteUrl>https://lpc-dev.aquarium-software.com/quotebuyapi/Quote/RetrieveQuote?SavedQuoteKey='+ CAST(@SavedQuoteKey AS VARCHAR(36)) +'</RetrieveQuoteUrl>'
	END
	
	--	2016-08-09 IS: Key Aggregator quote
	DECLARE @EmailAddress VARCHAR(255)
	DECLARE @PostCode  VARCHAR(255)
	DECLARE @StartDate  VARCHAR(255)
	
	--	2016-10-06 IS  StartDate is not being evaluated because it need to be aligned at a petquote level 
	SELECT	@EmailAddress = ISNULL(t.c.value('Email[1]','VARCHAR(255)'), 'NOT_PROVIDED'),
			@PostCode = t.c.value('Postcode[1]','VARCHAR(255)'),
			@StartDate = t.c.value('StartDate[1]','VARCHAR(255)')
	FROM	@XmlRequest.nodes('/SessionCache/QuoteDetails/CustomerDetailsViewModel') t(c)

	DECLARE @XmlQuoteRequest XML = @XmlRequest.query('
	for $root in //SessionCache
	return
	<Quote>
		<ClientId>{(sql:variable("@ClientID"))}</ClientId>
		<AggregatorId>{(sql:variable("@AggregatorID"))}</AggregatorId>
		
			<BrandingId>{data($root/BrandingId)}</BrandingId>
		
	{
	for $customer in //CustomerDetailsViewModel
	return
		<CustomerInfo>
			<ExistingPolicyHolder>{data($customer/ExistingPolicyHolder)}</ExistingPolicyHolder>
			<StartDate>{data($customer/StartDate)}</StartDate>
			<TitleId>{data($customer/TitleId)}</TitleId>
			<FirstName>{data($customer/FirstName)}</FirstName>
			<LastName>{data($customer/LastName)}</LastName>
			<Email>{data($customer/Email)}</Email>
			<ConfirmEmail>{data($customer/ConfirmEmail)}</ConfirmEmail>
			<InManualAddressMode>{data($customer/InManualAddressMode)}</InManualAddressMode>
			<Address1>{data($customer/Address1)}</Address1>
			<Address2>{data($customer/Address2)}</Address2>
			<TownCity>{data($customer/TownCity)}</TownCity>
			<Postcode>{data($customer/Postcode)}</Postcode>
			<HomePhone>{data($customer/HomePhone)}</HomePhone>
			<Country>{data($customer/Country)}</Country>
		</CustomerInfo>
	}
	
	{
	for $pet in //PetDetailsViewModel/PetDetailsViewModel
	return
		<PetQuote>
			<PetInfo>
				<PetName>{data($pet/PetName)}</PetName>
				<SpeciesId>{data($pet/SpeciesId)}</SpeciesId>
				<BreedId>{data($pet/BreedId)}</BreedId>
				<Gender>{data($pet/Gender)}</Gender>
				<BirthDate>{data($pet/BirthDate)}</BirthDate>
				<IsNeutered>{data($pet/IsNeutered)}</IsNeutered>
				<HasMicrochip>{data($pet/HasMicrochip)}</HasMicrochip>
				<PetColour>{data($pet/PetColour)}</PetColour>
				<PurchasePrice>{data($pet/PurchasePrice)}</PurchasePrice>
				<PetRef>{data($pet/PetRef)}</PetRef>
			</PetInfo>
			<StartDate>{(sql:variable("@StartDate"))}</StartDate>
		</PetQuote>
	}
	</Quote>
	')
	
	/*GPR 2019-06-21 for #57791*/
 	SET @XmlQuoteRequest.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotSms/text())[1] with sql:variable("@AggregatorDefault")');
	SET @XmlQuoteRequest.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotEmail/text())[1] with sql:variable("@AggregatorDefault")');
	SET @XmlQuoteRequest.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotPhone/text())[1] with sql:variable("@AggregatorDefault")');
	SET @XmlQuoteRequest.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotPost/text())[1] with sql:variable("@AggregatorDefault")');
	
	EXEC dbo._C600_PA_Policy_GetQuoteValues @XmlQuoteRequest, NULL, NULL, @XmlQuoteResponse OUTPUT
	
	-- remove products with £0.0 annual premium
	SET @XmlRequest.modify('insert sql:variable("@SavedQuoteKeyXML") into (/SessionCache)[1]')
	SET @XmlRequest.modify('insert sql:variable("@RetrieveQuoteUrlXML") into (/SessionCache)[1]')
	SET @XmlRequest.modify('insert sql:variable("@XmlQuoteResponse") into (/SessionCache/QuoteDetails)[1]') 
	SET @XmlRequest.modify('delete /SessionCache/QuoteDetails/CoverDetailsViewModel')
	SET @XmlRequest.modify('delete //*[not(node())]')
	
	SELECT @XmlRequest = REPLACE(CAST(@XmlRequest AS VARCHAR(MAX)), 'GetQuoteValuesResponse', 'CoverDetailsViewModel')
	
	SELECT	@QuoteSessionID = T.c.value('SessionId[1]','INT')
	FROM	@XmlQuoteResponse.nodes('/GetQuoteValuesResponse') T(c)
	
	--  2016-01-26 IS: Ticket #34372, HOTFIX, Session is getting stripped by the application
	DECLARE @XmlQuoteSessionID XML = '<SessionId>'+CAST(@QuoteSessionID AS VARCHAR(50))+'</SessionId>'
	SET @XmlRequest.modify('insert sql:variable("@XmlQuoteSessionID") into (/SessionCache/QuoteDetails)[1]') 

	--	2016-08-09 IS: Key Aggregator quote
	INSERT INTO _C600_SavedQuote (SavedQuoteKey, SavedQuoteXML, EmailAddress, SecretQuestionID, SecretQuestionValue, WhenCreated, WhenExpire, AggregatorID, QuoteSessionID)
	VALUES (@SavedQuoteKey, @XmlRequest, @EmailAddress, 6, @PostCode, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local()+30, @AggregatorID, @QuoteSessionID)

	SELECT	@XmlRequest AS XmlResponse,
			@SavedQuoteKey AS SavedQuoteKey

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetQuoteAggregator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_GetQuoteAggregator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetQuoteAggregator] TO [sp_executeall]
GO
