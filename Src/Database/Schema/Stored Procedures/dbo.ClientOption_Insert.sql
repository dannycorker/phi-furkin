SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOption_Insert]
(

	@ClientOptionID int    OUTPUT,

	@ClientID int   ,

	@ZenDeskOff bit   ,

	@ZenDeskURLPrefix varchar (50)  ,

	@ZenDeskText varchar (200)  ,

	@ZenDeskTitle varchar (50)  ,

	@IDiaryOff bit   ,

	@WorkflowOff bit   ,

	@UserDirectoryOff bit   ,

	@ECatcherOff bit   ,

	@LeadAssignmentOff bit   ,

	@UserMessagesOff bit   ,

	@SystemMessagesOff bit   ,

	@UserPortalOff bit   ,

	@ReportSearchOff bit   ,

	@NormalSearchOff bit   ,

	@UseXero bit   ,

	@UseUltra bit   ,

	@UseCaseSummary bit   ,

	@UseMoreProminentReminders bit   ,

	@UseSMSSurvey bit   ,

	@UseNewAddLead bit   ,

	@DiallerInsertMethod varchar (50)  ,

	@DiallerPrimaryKey varchar (50)  ,

	@UseCHARMS bit   ,

	@TextMessageService int   ,

	@UseThunderhead bit   ,

	@UseMemorableWordVerification bit   ,

	@ApplyInactivityTimeout bit   ,

	@InactivityTimeoutInSeconds int   ,

	@InactivityRedirectUrl varchar (50)  ,

	@UseSentimentAnalysis bit   ,

	@ShowProcessInfoButton bit   ,

	@LatestRecordsFirst bit   ,

	@UseSecureMessage bit   ,

	@EventCommentTooltipOff bit   ,

	@EnableScripting bit   ,

	@EnableBilling bit   ,

	@UseDocumentTypeVersioning bit   ,

	@EnableSMSSTOP bit   ,

	@UseEngageMail bit   ,

	@PreventForcedLogout bit   
)
AS


				
				INSERT INTO [dbo].[ClientOption]
					(
					[ClientID]
					,[ZenDeskOff]
					,[ZenDeskURLPrefix]
					,[ZenDeskText]
					,[ZenDeskTitle]
					,[iDiaryOff]
					,[WorkflowOff]
					,[UserDirectoryOff]
					,[eCatcherOff]
					,[LeadAssignmentOff]
					,[UserMessagesOff]
					,[SystemMessagesOff]
					,[UserPortalOff]
					,[ReportSearchOff]
					,[NormalSearchOff]
					,[UseXero]
					,[UseUltra]
					,[UseCaseSummary]
					,[UseMoreProminentReminders]
					,[UseSMSSurvey]
					,[UseNewAddLead]
					,[DiallerInsertMethod]
					,[DiallerPrimaryKey]
					,[UseCHARMS]
					,[TextMessageService]
					,[UseThunderhead]
					,[UseMemorableWordVerification]
					,[ApplyInactivityTimeout]
					,[InactivityTimeoutInSeconds]
					,[InactivityRedirectUrl]
					,[UseSentimentAnalysis]
					,[ShowProcessInfoButton]
					,[LatestRecordsFirst]
					,[UseSecureMessage]
					,[EventCommentTooltipOff]
					,[EnableScripting]
					,[EnableBilling]
					,[UseDocumentTypeVersioning]
					,[EnableSMSSTOP]
					,[UseEngageMail]
					,[PreventForcedLogout]
					)
				VALUES
					(
					@ClientID
					,@ZenDeskOff
					,@ZenDeskURLPrefix
					,@ZenDeskText
					,@ZenDeskTitle
					,@IDiaryOff
					,@WorkflowOff
					,@UserDirectoryOff
					,@ECatcherOff
					,@LeadAssignmentOff
					,@UserMessagesOff
					,@SystemMessagesOff
					,@UserPortalOff
					,@ReportSearchOff
					,@NormalSearchOff
					,@UseXero
					,@UseUltra
					,@UseCaseSummary
					,@UseMoreProminentReminders
					,@UseSMSSurvey
					,@UseNewAddLead
					,@DiallerInsertMethod
					,@DiallerPrimaryKey
					,@UseCHARMS
					,@TextMessageService
					,@UseThunderhead
					,@UseMemorableWordVerification
					,@ApplyInactivityTimeout
					,@InactivityTimeoutInSeconds
					,@InactivityRedirectUrl
					,@UseSentimentAnalysis
					,@ShowProcessInfoButton
					,@LatestRecordsFirst
					,@UseSecureMessage
					,@EventCommentTooltipOff
					,@EnableScripting
					,@EnableBilling
					,@UseDocumentTypeVersioning
					,@EnableSMSSTOP
					,@UseEngageMail
					,@PreventForcedLogout
					)
				-- Get the identity value
				SET @ClientOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_Insert] TO [sp_executeall]
GO
