SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Clients table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientID int   = null ,

	@CompanyName varchar (100)  = null ,

	@WebAddress varchar (255)  = null ,

	@IPAddress varchar (50)  = null ,

	@DefaultEmailAddress varchar (255)  = null ,

	@ClientTypeID int   = null ,

	@AllowSMS bit   = null ,

	@SecurityCode varchar (36)  = null ,

	@LeadsBelongToOffices bit   = null ,

	@UseEventCosts bit   = null ,

	@UseEventUOEs bit   = null ,

	@UseEventDisbursements bit   = null ,

	@UseEventComments bit   = null ,

	@VerifyAddress bit   = null ,

	@UseTapi bit   = null ,

	@UseRPI bit   = null ,

	@UseGBAddress bit   = null ,

	@UsePinpoint bit   = null ,

	@UseSage bit   = null ,

	@UseSAS bit   = null ,

	@UseCreditCalculation bit   = null ,

	@FollowupWorkingDaysOnly bit   = null ,

	@AddLeadByQuestionnaire bit   = null ,

	@UseIncendia bit   = null ,

	@LanguageID int   = null ,

	@CountryID int   = null ,

	@UseMobileInterface bit   = null ,

	@UseGBValidation bit   = null ,

	@CurrencyID int   = null ,

	@AllowSmsCommandProcessing bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientID]
	, [CompanyName]
	, [WebAddress]
	, [IPAddress]
	, [DefaultEmailAddress]
	, [ClientTypeID]
	, [AllowSMS]
	, [SecurityCode]
	, [LeadsBelongToOffices]
	, [UseEventCosts]
	, [UseEventUOEs]
	, [UseEventDisbursements]
	, [UseEventComments]
	, [VerifyAddress]
	, [UseTapi]
	, [UseRPI]
	, [UseGBAddress]
	, [UsePinpoint]
	, [UseSage]
	, [UseSAS]
	, [UseCreditCalculation]
	, [FollowupWorkingDaysOnly]
	, [AddLeadByQuestionnaire]
	, [UseIncendia]
	, [LanguageID]
	, [CountryID]
	, [UseMobileInterface]
	, [UseGBValidation]
	, [CurrencyID]
	, [AllowSmsCommandProcessing]
    FROM
	[dbo].[Clients] WITH (NOLOCK) 
    WHERE 
	 ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([WebAddress] = @WebAddress OR @WebAddress IS NULL)
	AND ([IPAddress] = @IPAddress OR @IPAddress IS NULL)
	AND ([DefaultEmailAddress] = @DefaultEmailAddress OR @DefaultEmailAddress IS NULL)
	AND ([ClientTypeID] = @ClientTypeID OR @ClientTypeID IS NULL)
	AND ([AllowSMS] = @AllowSMS OR @AllowSMS IS NULL)
	AND ([SecurityCode] = @SecurityCode OR @SecurityCode IS NULL)
	AND ([LeadsBelongToOffices] = @LeadsBelongToOffices OR @LeadsBelongToOffices IS NULL)
	AND ([UseEventCosts] = @UseEventCosts OR @UseEventCosts IS NULL)
	AND ([UseEventUOEs] = @UseEventUOEs OR @UseEventUOEs IS NULL)
	AND ([UseEventDisbursements] = @UseEventDisbursements OR @UseEventDisbursements IS NULL)
	AND ([UseEventComments] = @UseEventComments OR @UseEventComments IS NULL)
	AND ([VerifyAddress] = @VerifyAddress OR @VerifyAddress IS NULL)
	AND ([UseTapi] = @UseTapi OR @UseTapi IS NULL)
	AND ([UseRPI] = @UseRPI OR @UseRPI IS NULL)
	AND ([UseGBAddress] = @UseGBAddress OR @UseGBAddress IS NULL)
	AND ([UsePinpoint] = @UsePinpoint OR @UsePinpoint IS NULL)
	AND ([UseSage] = @UseSage OR @UseSage IS NULL)
	AND ([UseSAS] = @UseSAS OR @UseSAS IS NULL)
	AND ([UseCreditCalculation] = @UseCreditCalculation OR @UseCreditCalculation IS NULL)
	AND ([FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly OR @FollowupWorkingDaysOnly IS NULL)
	AND ([AddLeadByQuestionnaire] = @AddLeadByQuestionnaire OR @AddLeadByQuestionnaire IS NULL)
	AND ([UseIncendia] = @UseIncendia OR @UseIncendia IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([UseMobileInterface] = @UseMobileInterface OR @UseMobileInterface IS NULL)
	AND ([UseGBValidation] = @UseGBValidation OR @UseGBValidation IS NULL)
	AND ([CurrencyID] = @CurrencyID OR @CurrencyID IS NULL)
	AND ([AllowSmsCommandProcessing] = @AllowSmsCommandProcessing OR @AllowSmsCommandProcessing IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientID]
	, [CompanyName]
	, [WebAddress]
	, [IPAddress]
	, [DefaultEmailAddress]
	, [ClientTypeID]
	, [AllowSMS]
	, [SecurityCode]
	, [LeadsBelongToOffices]
	, [UseEventCosts]
	, [UseEventUOEs]
	, [UseEventDisbursements]
	, [UseEventComments]
	, [VerifyAddress]
	, [UseTapi]
	, [UseRPI]
	, [UseGBAddress]
	, [UsePinpoint]
	, [UseSage]
	, [UseSAS]
	, [UseCreditCalculation]
	, [FollowupWorkingDaysOnly]
	, [AddLeadByQuestionnaire]
	, [UseIncendia]
	, [LanguageID]
	, [CountryID]
	, [UseMobileInterface]
	, [UseGBValidation]
	, [CurrencyID]
	, [AllowSmsCommandProcessing]
    FROM
	[dbo].[Clients] WITH (NOLOCK) 
    WHERE 
	 ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([WebAddress] = @WebAddress AND @WebAddress is not null)
	OR ([IPAddress] = @IPAddress AND @IPAddress is not null)
	OR ([DefaultEmailAddress] = @DefaultEmailAddress AND @DefaultEmailAddress is not null)
	OR ([ClientTypeID] = @ClientTypeID AND @ClientTypeID is not null)
	OR ([AllowSMS] = @AllowSMS AND @AllowSMS is not null)
	OR ([SecurityCode] = @SecurityCode AND @SecurityCode is not null)
	OR ([LeadsBelongToOffices] = @LeadsBelongToOffices AND @LeadsBelongToOffices is not null)
	OR ([UseEventCosts] = @UseEventCosts AND @UseEventCosts is not null)
	OR ([UseEventUOEs] = @UseEventUOEs AND @UseEventUOEs is not null)
	OR ([UseEventDisbursements] = @UseEventDisbursements AND @UseEventDisbursements is not null)
	OR ([UseEventComments] = @UseEventComments AND @UseEventComments is not null)
	OR ([VerifyAddress] = @VerifyAddress AND @VerifyAddress is not null)
	OR ([UseTapi] = @UseTapi AND @UseTapi is not null)
	OR ([UseRPI] = @UseRPI AND @UseRPI is not null)
	OR ([UseGBAddress] = @UseGBAddress AND @UseGBAddress is not null)
	OR ([UsePinpoint] = @UsePinpoint AND @UsePinpoint is not null)
	OR ([UseSage] = @UseSage AND @UseSage is not null)
	OR ([UseSAS] = @UseSAS AND @UseSAS is not null)
	OR ([UseCreditCalculation] = @UseCreditCalculation AND @UseCreditCalculation is not null)
	OR ([FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly AND @FollowupWorkingDaysOnly is not null)
	OR ([AddLeadByQuestionnaire] = @AddLeadByQuestionnaire AND @AddLeadByQuestionnaire is not null)
	OR ([UseIncendia] = @UseIncendia AND @UseIncendia is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([UseMobileInterface] = @UseMobileInterface AND @UseMobileInterface is not null)
	OR ([UseGBValidation] = @UseGBValidation AND @UseGBValidation is not null)
	OR ([CurrencyID] = @CurrencyID AND @CurrencyID is not null)
	OR ([AllowSmsCommandProcessing] = @AllowSmsCommandProcessing AND @AllowSmsCommandProcessing is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Find] TO [sp_executeall]
GO
