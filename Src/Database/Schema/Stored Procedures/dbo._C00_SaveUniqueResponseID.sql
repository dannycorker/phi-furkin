SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-10-2014
-- Description:	Saves the Unique Response ID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SaveUniqueResponseID]

	@ClientID INT,
	@LeadTypeID INT,
	@ResponseID varchar(2000),
	@MatterID INT,
	@UserID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DetailFieldID INT
	SELECT @DetailFieldID = DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=1330 and ClientID = @ClientID and LeadTypeID=@LeadTypeID

	EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @ResponseID, @MatterID , @UserID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SaveUniqueResponseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SaveUniqueResponseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SaveUniqueResponseID] TO [sp_executeall]
GO
