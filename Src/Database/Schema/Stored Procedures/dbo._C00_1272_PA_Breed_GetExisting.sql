SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-05-15
-- Description:	Gets the existing breed
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_PA_Breed_GetExisting]
(
	@LeadID INT
)
AS
BEGIN
	
	SELECT ldv.ValueInt AS BreedID, rdvBreed.DetailValue AS BreedValue, rdvSpecies.ValueInt AS SpeciesID
	FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 	
	INNER JOIN dbo.ResourceListDetailValues rdvBreed WITH (NOLOCK) ON ldv.ValueInt = rdvBreed.ResourceListID AND rdvBreed.DetailFieldID = 144270
	INNER JOIN dbo.ResourceListDetailValues rdvSpecies WITH (NOLOCK) ON ldv.ValueInt = rdvSpecies.ResourceListID AND rdvSpecies.DetailFieldID = 144269
	WHERE ldv.LeadID = @LeadID
	AND ldv.DetailFieldID = 144272
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_PA_Breed_GetExisting] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_PA_Breed_GetExisting] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_PA_Breed_GetExisting] TO [sp_executeall]
GO
