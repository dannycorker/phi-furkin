SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-11-02
-- Description:	Match rows in the card load history table to find loads that are successful or unsuccessful
-- =============================================
CREATE PROCEDURE [dbo].[Incendia_MatchLoadResponses] 
	@ResultToMatch BIT
AS
BEGIN

SELECT Cases.CaseID, Customers.CustomerID, Lead.LeadID, LeadEvent.EventTypeID, LeadEvent.WhenFollowedUp, LeadEvent.EventDeleted, Customers.Test, LeadEvent.LeadEventID
FROM Customers WITH (NOLOCK) 
INNER JOIN Lead WITH (NOLOCK) ON Lead.CustomerID = Customers.CustomerID
INNER JOIN Cases WITH (NOLOCK) ON Cases.LeadID = Lead.LeadID
INNER JOIN LeadEvent WITH (NOLOCK) ON LeadEvent.CaseID = Cases.CaseID
INNER JOIN dbo.Matter m WITH (NOLOCK) ON Cases.CaseID = m.CaseID
-- Join on the lead event ID stored in the table row
INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON t.ClientID = 183 AND t.LeadID = Lead.LeadID AND t.MatterID = m.MatterID AND t.ValueInt = LeadEvent.LeadEventID AND t.DetailFieldID = 117899
-- Now join on the response date to make sure we have had a response
INNER JOIN dbo.TableDetailValues t2 WITH (NOLOCK) ON t.TableRowID = t2.TableRowID AND t2.DetailFieldID = 117538
-- Now left join to the error and error description fields
LEFT JOIN dbo.TableDetailValues t3 WITH (NOLOCK) ON t.TableRowID = t3.TableRowID AND t3.DetailFieldID = 117539
LEFT JOIN dbo.TableDetailValues t4 WITH (NOLOCK) ON t.TableRowID = t4.TableRowID AND t4.DetailFieldID = 117540
WHERE Customers.ClientID = 183 
AND Lead.ClientID = 183 
AND Cases.ClientID = 183 
AND LeadEvent.ClientID = 183 
AND (Customers.Test IS NULL  OR Customers.Test = 0) 
AND LeadEvent.EventDeleted = 0
AND LeadEvent.WhenFollowedUp IS NULL 
AND ((@ResultToMatch = 1 AND ISNULL(t3.DetailValue, '') = '' AND ISNULL(t4.DetailValue, '') = '') OR
	(@ResultToMatch = 0 AND (ISNULL(t3.DetailValue, '') != '' OR ISNULL(t4.DetailValue, '') != '')))
AND LeadEvent.EventTypeID IN (92136, 131161) -- Added this line to prevent SMS being sent early in the morning if no sms is sent after an instant load (or batch instant)


END


GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_MatchLoadResponses] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Incendia_MatchLoadResponses] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_MatchLoadResponses] TO [sp_executeall]
GO
