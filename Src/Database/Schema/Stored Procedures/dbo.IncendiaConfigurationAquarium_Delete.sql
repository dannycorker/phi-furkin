SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the IncendiaConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationAquarium_Delete]
(

	@IncendiaConfigurationAquariumID int   
)
AS


				DELETE FROM [dbo].[IncendiaConfigurationAquarium] WITH (ROWLOCK) 
				WHERE
					[IncendiaConfigurationAquariumID] = @IncendiaConfigurationAquariumID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationAquarium_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_Delete] TO [sp_executeall]
GO
