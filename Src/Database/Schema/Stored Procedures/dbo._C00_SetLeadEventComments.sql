SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-08-21
-- Description:	Stop Cathal writing update leadevent in SAE
-- 2018-07-30 CPS stop LeadEvent updates from blowing up the rating webservice by updating the same event that the transaction just created
-- 2019-03-20 CPS add a carriage return rather than a hyphen
-- 2019-04-09 CPS add ISNULL() 
-- 2019-10-23 CPS for JIRA LPC-35 | Add a note with calculation comments for a WebMethod event
-- 2019-10-23 CPS for JIRA LPC-35 | Sad.  Removed the note addition because apparently it's the whole LeadEvent table that's locked, not just the one record.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SetLeadEventComments]
	 @LeadEventID	 INT
	,@Comments		 VARCHAR(2000)
	,@AddOrOverwrite INT = 1 /*1 = Add, 2 = Overwrite*/
AS
BEGIN

	SET NOCOUNT ON;

	/*
		Update the leadevent to set the comments as required.
		If AddOrOverwrite is set to 1 then add the comments to the existing 
		comments otherwise just overwrite the comments with the comment passed in
	*/
	IF 31 /*Web Method Call*/ = ( SELECT EventSubtypeID FROM dbo.EventType et WITH ( NOLOCK ) INNER JOIN dbo.LeadEvent le WITH ( NOLOCK ) on le.EventTypeID = et.EventTypeID WHERE le.LeadEventID = @LeadEventID )
	BEGIN
		EXEC _C00_LogIt 'Info', '_C00_SetLeadEventComments',@LeadEventID,@Comments,@AddOrOverwrite
	END
	ELSE
	BEGIN
		UPDATE dbo.LeadEvent
		SET Comments = CASE @AddOrOverwrite WHEN 1 THEN LEFT(ISNULL(Comments,'') + CHAR(13)+CHAR(10) + @Comments,2000) ELSE ISNULL(@Comments,'') END 
		WHERE LeadEventID = @LeadEventID
	END

END













GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetLeadEventComments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SetLeadEventComments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetLeadEventComments] TO [sp_executeall]
GO
