SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PanelItemCharting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_Update]
(

	@PanelItemChartingID int   ,

	@ClientID int   ,

	@PanelItemChartingName varchar (250)  ,

	@PanelID int   ,

	@CreatedBy int   ,

	@ChartID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PanelItemCharting]
				SET
					[ClientID] = @ClientID
					,[PanelItemChartingName] = @PanelItemChartingName
					,[PanelID] = @PanelID
					,[CreatedBy] = @CreatedBy
					,[ChartID] = @ChartID
				WHERE
[PanelItemChartingID] = @PanelItemChartingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Update] TO [sp_executeall]
GO
