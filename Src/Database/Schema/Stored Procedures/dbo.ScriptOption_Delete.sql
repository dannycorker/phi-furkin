SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_Delete]
(

	@ScriptOptionID int   
)
AS


				DELETE FROM [dbo].[ScriptOption] WITH (ROWLOCK) 
				WHERE
					[ScriptOptionID] = @ScriptOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Delete] TO [sp_executeall]
GO
