SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-08-26
-- Description:	Add Process Start After Creating Case
-- 2013-09-11 JWG Set ProcessStartLeadEventID and LatestNonNoteLeadEventID as well
-- 2014-09-04 LB Now calls SAE for added process start events.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AddProcessStart]
(
	@CaseID INT,
	@WhoCreated INT,
	@EventTypeToAdd INT = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @LeadID INT, 
			@ClientID INT, 
			@LeadTypeID INT, 
			@PSEventTypeID INT, 
			@FollowupTimeUnitsID INT, 
			@FollowupQuantity INT,
			@FollowUpDateTime DATETIME,
			@ClientStatusID INT,
			@LeadEventID INT,
			@EventTypeDescription VARCHAR(250),
			@Cost MONEY,
			@ChargeoutRate MONEY,
			@SAE NVARCHAR(MAX)

	SELECT @LeadID = c.LeadID, @ClientID = c.ClientID, @LeadTypeID = l.LeadTypeID
	FROM Cases c WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID
	WHERE c.CaseID = @CaseID
	
	SELECT @ChargeoutRate = ChargeOutRate
	FROM ClientPersonnel cp WITH (NOLOCK)
	WHERE cp.ClientPersonnelID = @WhoCreated

	SELECT @PSEventTypeID = EventTypeID, @FollowupTimeUnitsID = FollowupTimeUnitsID, @FollowupQuantity = FollowupQuantity, @ClientStatusID = StatusAfterEvent, @EventTypeDescription = EventTypeDescription, @Cost = UnitsOfEffort * @ChargeoutRate
	FROM EventType WITH (NOLOCK)
	WHERE LeadTypeID = @LeadTypeID
	AND ((EventSubtypeID = 10
	AND Enabled = 1
	AND AvailableManually = 1
	AND @EventTypeToAdd IS NULL
	)
	OR 
	(EventTypeID = @EventTypeToAdd))
	
	IF @FollowupQuantity IS NOT NULL
	BEGIN
	
		IF @FollowupTimeUnitsID < 4 SELECT @FollowUpDateTime = DATEADD(MINUTE, @FollowupQuantity, dbo.fn_GetDate_Local())
		IF @FollowupTimeUnitsID = 4 SELECT @FollowUpDateTime = DATEADD(HH, @FollowupQuantity, dbo.fn_GetDate_Local())
		IF @FollowupTimeUnitsID = 5 SELECT @FollowUpDateTime = DATEADD(DD, @FollowupQuantity, dbo.fn_GetDate_Local())
		IF @FollowupTimeUnitsID > 5 SELECT @FollowUpDateTime = DATEADD(WEEK, @FollowupQuantity, dbo.fn_GetDate_Local())
	
	END

	INSERT INTO [dbo].[LeadEvent]
           ([ClientID]
           ,[LeadID]
           ,[WhenCreated]
           ,[WhoCreated]
           ,[Cost]
           ,[Comments]
           ,[EventTypeID]
           ,[NoteTypeID]
           ,[FollowupDateTime]
           ,[WhenFollowedUp]
           ,[AquariumEventType]
           ,[NextEventID]
           ,[CaseID]
           ,[LeadDocumentID]
           ,[NotePriority]
           ,[DocumentQueueID]
           ,[EventDeleted]
           ,[WhoDeleted]
           ,[DeletionComments]
           ,[ContactID]
           ,[BaseCost]
           ,[DisbursementCost]
           ,[DisbursementDescription]
           ,[ChargeOutRate]
           ,[UnitsOfEffort]
           ,[CostEnteredManually]
           ,[IsOnHold]
           ,[HoldLeadEventID])
     VALUES
           (@ClientID
           ,@LeadID
           ,dbo.fn_GetDate_Local()
           ,@WhoCreated
           ,@Cost
           ,@EventTypeDescription
           ,@PSEventTypeID
           ,NULL
           ,@FollowUpDateTime
           ,NULL
           ,NULL
           ,NULL
           ,@CaseID
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)

	SELECT @LeadEventID = SCOPE_IDENTITY()

	UPDATE Cases
	SET ClientStatusID = @ClientStatusID, 
	LatestInProcessLeadEventID = @LeadEventID, 
	LatestLeadEventID = @LeadEventID, 
	LatestNonNoteLeadEventID = @LeadEventID, 
	ProcessStartLeadEventID = @LeadEventID 
	WHERE CaseID = @CaseID 

	IF @@NESTLEVEL = 0
	BEGIN
		
		SELECT @SAE = REPLACE(PostUpdateSql,'@LeadEventID',@LeadEventID) 
		FROM EventTypeSql e WITH (NOLOCK) 
		WHERE e.PostUpdateSql LIKE '%SAE%'
		AND e.EventTypeID = @PSEventTypeID
		
		IF ISNULL(@SAE,'') <> ''
		BEGIN
			EXEC (@SAE)
		END
		
	END
	
	RETURN @LeadEventID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddProcessStart] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AddProcessStart] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddProcessStart] TO [sp_executeall]
GO
