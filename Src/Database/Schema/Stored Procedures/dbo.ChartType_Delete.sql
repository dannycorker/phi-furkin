SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChartType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartType_Delete]
(

	@ChartTypeID int   
)
AS


				DELETE FROM [dbo].[ChartType] WITH (ROWLOCK) 
				WHERE
					[ChartTypeID] = @ChartTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Delete] TO [sp_executeall]
GO
