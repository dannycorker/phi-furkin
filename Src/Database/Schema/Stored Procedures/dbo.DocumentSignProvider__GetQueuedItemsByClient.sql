SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetQueuedItemsByClient]
	@ClientID INT,
	@Period INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @PeriodStart DATE
	
	SELECT @PeriodStart = 
	CASE @Period
		WHEN 0 THEN dbo.fn_GetDate_Local()
		WHEN 1 THEN dbo.fn_GetDate_Local() - 7
		WHEN 2 THEN dbo.fn_GetDate_Local() - 30
		WHEN 3 THEN dbo.fn_GetDate_Local() - 366
		WHEN 4 THEN dbo.fn_GetDate_Local() - 1000
		ELSE dbo.fn_GetDate_Local()
	END
	
	
	SELECT	DSPQueueID, ClientID, LeadID, CaseID, OutLeadEventID, OutEventTypeID, OutDocumentTypeID, InEventTypeID, InLeadEventID, DSPTypeID, DSPAccountID, DSPUserID, DSPSignatureTemplate, DSPBrandingKey, DSPEnvelopeKey, DSPEnvelopeStatus, '' RequestXML, '' ResponseXML, WhenCreated, WhenModified
	FROM	DocumentSignProviderQueue WITH (NOLOCK) 
	WHERE	ClientID = @ClientID
	AND		WhenModified > @PeriodStart
	ORDER BY WhenModified DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetQueuedItemsByClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetQueuedItemsByClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetQueuedItemsByClient] TO [sp_executeall]
GO
