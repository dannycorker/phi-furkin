SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteOutcomeEquation] @OutcomeEquationID int
AS
Delete From OutcomeEquations
Where OutcomeEquationID = @OutcomeEquationID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteOutcomeEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeEquation] TO [sp_executeall]
GO
