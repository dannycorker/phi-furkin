SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-19
-- Description:	Populate detail fields based on events 
--              JWG 2009-06-01 New "Insert" option called "Always Delete" (5)
--              This enables administrators to force the user to enter a
--              value every single time they apply an event of this type.
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeFieldDefaultFromLeadEvent] 
	@LeadEventID int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @EventTypeID int,
			@LeadID int,
			@DFList varchar(max),
			@CaseID int

	SELECT @EventTypeID = EventTypeID,
	@LeadID = LeadID, 
	@CaseID = CaseID
	FROM LeadEvent 
	WHERE LeadEventID = @LeadEventID

	IF (Select COUNT(*) From EventTypeFieldDefault (nolock) Where EventTypeID = @EventTypeID) > 0 
	BEGIN
		SELECT @DFList = ''

		-- Recursive field population; this creates a string of values eg
		-- 1234,5678,
		SELECT @DFList = @DFList + convert(varchar, DetailFieldID) + ',' 
		FROM EventTypeFieldDefault (nolock)
		Where EventTypeID = @EventTypeID 

		-- Remove the trailing comma
		SELECT @DFList = LEFT(@DFList, (LEN(@DFList) - 1) )

		-- Create the fields
		EXEC dbo._C00_CreateDetailFields @DFList, @LeadID

		Update LeadDetailValues
		Set DetailValue = etfd.DefaultValue
		From LeadDetailValues ldv (nolock)
		Inner Join EventTypeFieldDefault etfd (nolock) on etfd.DetailFieldID = ldv.DetailFieldID
		Inner Join DetailFields df (nolock) on df.DetailFieldID = etfd.DetailFieldID and df.LeadOrMatter = 1
		Where ldv.LeadID = @LeadID
		
		Update MatterDetailValues
		Set DetailValue = etfd.DefaultValue
		From MatterDetailValues mdv (nolock)
		Inner Join EventTypeFieldDefault etfd (nolock) on etfd.DetailFieldID = mdv.DetailFieldID
		Inner Join DetailFields df (nolock) on df.DetailFieldID = etfd.DetailFieldID and df.LeadOrMatter = 1
		Inner Join Matter m (nolock) on m.MatterID = mdv.MatterID and m.CaseID = @CaseID

	End

END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldDefaultFromLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldDefaultFromLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldDefaultFromLeadEvent] TO [sp_executeall]
GO
