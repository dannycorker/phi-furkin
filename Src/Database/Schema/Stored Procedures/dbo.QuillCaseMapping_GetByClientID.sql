SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillCaseMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[QuillCaseMappingID],
					[ClientID],
					[CaseID],
					[QuillCaseCode]
				FROM
					[dbo].[QuillCaseMapping] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_GetByClientID] TO [sp_executeall]
GO
