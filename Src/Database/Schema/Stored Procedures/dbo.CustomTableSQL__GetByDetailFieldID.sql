SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-20
-- Description:	Get Custom Table SQL Stuff
-- =============================================
CREATE PROCEDURE [dbo].[CustomTableSQL__GetByDetailFieldID] 
	@DetailFieldID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM CustomTableSQL c WITH (NOLOCK)
	WHERE c.DetailFieldID = @DetailFieldID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL__GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQL__GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQL__GetByDetailFieldID] TO [sp_executeall]
GO
