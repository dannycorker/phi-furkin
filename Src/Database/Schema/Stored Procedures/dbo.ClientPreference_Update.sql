SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPreference table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreference_Update]
(

	@ClientPreferenceID int   ,

	@ClientID int   ,

	@ClientPreferenceTypeID int   ,

	@PreferenceValue varchar (255)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPreference]
				SET
					[ClientID] = @ClientID
					,[ClientPreferenceTypeID] = @ClientPreferenceTypeID
					,[PreferenceValue] = @PreferenceValue
				WHERE
[ClientPreferenceID] = @ClientPreferenceID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreference_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Update] TO [sp_executeall]
GO
