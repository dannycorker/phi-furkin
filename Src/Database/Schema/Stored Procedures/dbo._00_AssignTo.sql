SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-05-23
-- Description:	SQL Afterevent for 3J Finance
-- =============================================
CREATE PROCEDURE [dbo].[_00_AssignTo]
	@LeadID int,
	@AssignToUserID int

AS
BEGIN
	SET NOCOUNT ON;

	/* Update the lead to be assigned to the Authorised User...*/
	Update Lead
	Set AssignedTo = @AssignToUserID, Assigned = 1, AssignedBy = @AssignToUserID, AssignedDate = dbo.fn_GetDate_Local()
	Where LeadID = @LeadID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_00_AssignTo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_00_AssignTo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_00_AssignTo] TO [sp_executeall]
GO
