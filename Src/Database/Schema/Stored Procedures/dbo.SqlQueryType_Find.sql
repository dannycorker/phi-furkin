SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryType_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryTypeID int   = null ,

	@TypeName nchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryTypeID]
	, [TypeName]
    FROM
	[dbo].[SqlQueryType] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryTypeID] = @SqlQueryTypeID OR @SqlQueryTypeID IS NULL)
	AND ([TypeName] = @TypeName OR @TypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryTypeID]
	, [TypeName]
    FROM
	[dbo].[SqlQueryType] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryTypeID] = @SqlQueryTypeID AND @SqlQueryTypeID is not null)
	OR ([TypeName] = @TypeName AND @TypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Find] TO [sp_executeall]
GO
