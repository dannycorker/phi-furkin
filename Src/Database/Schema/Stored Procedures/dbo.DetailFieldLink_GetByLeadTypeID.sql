SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailFieldLinkID],
					[ClientID],
					[DetailFieldID],
					[MasterQuestionID],
					[LeadTypeID]
				FROM
					[dbo].[DetailFieldLink] WITH (NOLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_GetByLeadTypeID] TO [sp_executeall]
GO
