SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClassNode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_Get_List]

AS


				
				SELECT
					[ClassNodeID],
					[ClientID],
					[ParentClassNodeID],
					[NodeType],
					[Name],
					[ClassDescription],
					[NodeOrder],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ClassNode] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Get_List] TO [sp_executeall]
GO
