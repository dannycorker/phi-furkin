SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-05-02
-- Description:	Runs the premium calculations for quotes, monthly and renewals
-- Mods
--	2016-12-14 DCM Added RuleSetID to the argument list
--	2016-02-23 DCM added EvaluateByEffectiveDate override
--  2017-08-14 CPS for #45019 add PostCode as an override so that it can be used in custom rule parameter functions
--	2017-12-18 CPS for JEL allow scheme selection by species
--  2018-02-14 GPR added 'Discount Code' to Discount RuleCheckpoint list
--	2018-02-21 GPR added 'Administration Fee' to Admin Fee RuleCheckpoint list
--  2018-02-27 GPR added MultiPet Discount flag/relvalue at New and Renewal (yes/no) for C600
--  2018-03-05 GPR get MatterDetailValues for Multipet MTA as RenewalMatterID from @PassedOverrides
--  2018-03-09 GPR removed @Brand override
--  2018-04-23 GPR added non written premium affecting discount to @Data table
--  2018-04-30 GPR assigned PetNumber to count of active policies - used when rerating for a Policy purchased after the sale of the initial Policy (C600 Defect 227)
--  2018-11-08 GPR Added block to step down discount at Renewal for Buddies
--	2019-04-08 GPR Added override for PetNumber buddies renewal #1593 / #56145
--	2019-10-11 GPR removed redundent code blocks (previously commented out and not used in C60X build) for LPC-35
-- =============================================
 
CREATE PROCEDURE [dbo].[_C00_1273_Policy_CalculatePremium]
(
	@CaseID INT = NULL, -- NULL for quotes
	@OverrideStartDate DATE = NULL, -- for renewals and quotes
	@OverrideTermsDate DATE = NULL, -- for renewals and quotes
	@OverrideScheme INT = NULL, -- for renewals and quotes
	@PassedOverrides dbo.tvpIntVarcharVarchar READONLY, -- passed in for quotes but empty for normal calcs and renewals
	@PremiumDate DATE = NULL, -- MUST be passed in for monthly premium calc.  NULL for renewals and quotes
	@RuleSetID INT = NULL,
	@LeadEventID		INT = NULL
)
AS
BEGIN

--declare
--	--@CaseID INT = 40036270, -- NULL for quotes
--	--@OverrideStartDate DATE = NULL, -- for renewals and quotes
--	--@OverrideTermsDate DATE = NULL, -- for renewals and quotes
--	--@OverrideScheme INT = NULL, -- for renewals and quotes
--	--@PassedOverrides dbo.tvpIntVarcharVarchar, -- passed in for quotes but empty for normal calcs and renewals
--	--@PremiumDate DATE = '2016-01-18' -- MUST be passed in for monthly premium calc.  NULL for renewals and quotes
--	@CaseID INT = NULL, -- NULL for quotes
--	@OverrideStartDate DATE = '2016-11-17', -- for renewals and quotes
--	@OverrideTermsDate DATE = '2016-11-17', -- for renewals and quotes
--	@OverrideScheme INT = 134926, -- for renewals and quotes
--	@PassedOverrides dbo.tvpIntVarcharVarchar, -- passed in for quotes but empty for normal calcs and renewals
--	@PremiumDate DATE = NULL -- MUST be passed in for monthly premium calc.  NULL for renewals and quotes

--	INSERT @PassedOverrides (AnyID, AnyValue1, AnyValue2) VALUES
--	(2, 'Postcode', 'WA14 2UN'),
--	(2, 'County', NULL),
--	(5, '144269', '42989'), -- species 42989 - dog, 42990 -cat
--	(5, '144270', 'Mixed Breed, Small (under 24lbs fully grown)'), -- breed 'Mixed Breed, Small (under 24lbs fully grown)', 'Tosa'
--	(1, '144275', '5169'), -- gender 5168 - male, 5169 - female
--	(1, '144274', '2013-06-18'), -- pet dob
--	(1, '152783', '5145'), -- neutered 5144 yes, 5145 no
--	(1, '144339', '350'), -- pruchase price
--	(6, '145667', '0'), -- volex
--	(1, 'PetNumber', '1'), -- petnumber
--	(1, '175488', ''), -- marketing code
--	(2, 'Multipet', '0'),
--	(6, '175737', ''), -- optional coverages
--	(2, 'Brand', 'Pet Protect'), -- brand
--	(4,'fn_C00_1273_RulesEngine_PostCodeSector','WA14 2'), -- post code sector
--	(1,'170030','micro123'), -- microchip no
--	(1,'144339','350'), -- pet value
--	(2,'ItemName',''), -- item name
--	(4,'fn_C00_1273_RulesEngine_PremiumAtLastPeriodExit',''),
--	(4,'fn_C00_1273_RulesEngine_LossRatioPercentage',''),
--	(4,'fn_C00_1273_RulesEngine_GetNumberOfClaims',''),
--	(4,'fn_C00_1273_RulesEngine_PaymentFrequency','MO'), -- payment freq MO,AN
--	(2,'PolicyYearNumber','1'),
--	(2,'ProductName','Dog Extra')

	DECLARE  @XmlLog	XML
			,@Postcode	VARCHAR(2000) = ''
			,@SpeciesID	INT
			,@NonWPDiscount MONEY
			,@ClientID	INT = dbo.fnGetPrimaryClientID()
			
	SELECT @XmlLog = 
	(
		SELECT * 
		FROM @PassedOverrides o 
		FOR XML AUTO
	)
	
	EXEC _C00_LogItXML @ClientID, 0, '_C00_1273_Policy_CalculatePremium @PassedOverrides', @XmlLog

	PRINT 'Starting _C00_1273_Policy_CalculatePremium'

	DECLARE @Data TABLE
	(
		TermsDate DATE,
		SchemeID INT,
		PolicyMatterID INT,
		RuleSetID INT,
		AnnualPremium MONEY,
		Discount MONEY,
		PremiumLessDiscount MONEY,
		FirstMonthly MONEY,
		RecurringMonthly MONEY,
		Net MONEY,
		Commission MONEY,
		GrossNet MONEY,
		DiscountGrossNet MONEY,
		PAFIfBeforeIPT MONEY,
		PAFBeforeIPTGrossNet MONEY,
		IPT MONEY,
		IPTGrossNet MONEY,
		PAFIfAfterIPT MONEY,
		GrossGross MONEY,
		PremiumCalculationID INT,
		PremiumLessTax MONEY,
		TaxLevel1 MONEY,
		TaxLevel2 MONEY
	)	
	
	IF @CaseID IS NULL
	BEGIN
		SELECT @Postcode=AnyValue2 FROM @PassedOverrides WHERE AnyValue1='Postcode'
	END

	DECLARE @TermsDate DATE = @OverrideTermsDate
	IF @TermsDate IS NULL
	BEGIN
		SELECT @TermsDate = dbo.fn_C00_1273_GetPremiumCalculationDate(@CaseID, @PremiumDate)
	END

	PRINT '@TermsDate'
	PRINT @TermsDate
	
	DECLARE @StartDate DATE = @OverrideStartDate
	IF @StartDate IS NULL
	BEGIN
		SELECT @StartDate = dbo.fn_C00_1273_GetPremiumCalculationStartDate(@CaseID, @PremiumDate)
	END

	PRINT '@StartDate'
	PRINT @StartDate

	DECLARE @SchemeID INT = @OverrideScheme
	IF @SchemeID IS NULL
	BEGIN
		SELECT @SchemeID = dbo.fn_C00_1273_GetPremiumCalculationScheme(@CaseID, @PremiumDate)
		PRINT '@SchemeID = dbo.fn_C00_1273_GetPremiumCalculationScheme(' + ISNULL(CONVERT(VARCHAR,@CaseID),'NULL') + ',' + ISNULL('''' + CONVERT(VARCHAR,@PremiumDate) + '''','NULL') + ')'
	END
	
	PRINT '@SchemeID = ' + ISNULL(CONVERT(VARCHAR,@SchemeID),'NULL')

	/*CPS 2017-12-18 for JEL allow selection by species*/
	SELECT @SpeciesID = po.AnyValue2
	FROM @PassedOverrides po 
	WHERE po.AnyValue1 = '144269'

	DECLARE @PolicyMatterID INT
	SELECT @PolicyMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies(@SchemeID, @TermsDate,@SpeciesID)
	PRINT '@PolicyMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies(' + ISNULL(CONVERT(VARCHAR,@SchemeID),'NULL') + ',' + ISNULL('''' + CONVERT(VARCHAR,@TermsDate) + '''','NULL') + ''',' + ISNULL(CONVERT(VARCHAR,@SpeciesID),'NULL') + ')'
	
	SELECT @ClientID=ClientID 
	FROM Matter WITH (NOLOCK) 
	WHERE MatterID=@PolicyMatterID
	
	DECLARE @CustomerID INT
	IF @CaseID IS NOT NULL
	BEGIN
		SELECT @CustomerID=l.CustomerID 
		FROM Cases ca WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON ca.LeadID=l.LeadID
		WHERE CaseID=@CaseID
	END
	
	IF @RuleSetID IS NULL 
	BEGIN
		PRINT '@RuleSetID = dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(' + ISNULL(CONVERT(VARCHAR,@PolicyMatterID),'NULL') + ',' + ISNULL('''' + CONVERT(VARCHAR,@TermsDate) + '''','NULL') + ')'
		SELECT @RuleSetID = dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(@PolicyMatterID, @TermsDate)
	END
	
	DECLARE @Overrides dbo.tvpIntVarcharVarchar
		
	INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
	SELECT AnyID, AnyValue1, AnyValue2
	FROM @PassedOverrides
	-- Default values for terms date and policy year start
	INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
	(2, 'TermsDate', CONVERT(VARCHAR, @TermsDate, 120)),
	(2, 'EvaluateByEffectiveDate', CONVERT(VARCHAR, @TermsDate, 120)),
	(2, 'YearStart', CONVERT(VARCHAR, @StartDate, 120))

	-- Renewal year number
	DECLARE @RenewalYearNumber INT
	IF NOT EXISTS ( SELECT * FROM @PassedOverrides WHERE AnyValue1='PolicyYearNumber')
	BEGIN
		SELECT @RenewalYearNumber=dbo.fn_C00_1273_GetPolicyYearNumber (@CaseID,@TermsDate)
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'PolicyYearNumber', CAST(@RenewalYearNumber AS VARCHAR))
	END
	
	/*GPR 2018-03-26*/
	DECLARE @PetNumber INT, @Matter INT, @PetCount INT

	IF EXISTS (SELECT * FROM @PassedOverrides WHERE AnyValue1 = 'fn_C00_1273_RulesEngine_PetCount')
	BEGIN
	SELECT @PetCount = AnyValue2 FROM @PassedOverrides WHERE AnyValue1='fn_C00_1273_RulesEngine_PetCount'
		
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(4, 'fn_C00_1273_RulesEngine_PetCount', CAST(@PetCount AS VARCHAR))
	END

	IF EXISTS (SELECT * FROM @PassedOverrides WHERE AnyValue1='PetNumber')
	BEGIN
		SELECT @PetNumber = AnyValue2 FROM @PassedOverrides WHERE AnyValue1='PetNumber'
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'PetNumber', CAST(@PetNumber AS VARCHAR))
	END	
	
	DECLARE @NewBusiness BIT /*GPR 2018-04-03*/ /*GPR 2018-10-15 Modified for Discounting at Renewals*/
	SELECT @NewBusiness = AnyValue2 FROM @PassedOverrides WHERE AnyValue1='NewBusiness'
	
	IF @NewBusiness = 1 AND EXISTS ( SELECT * FROM @PassedOverrides WHERE AnyValue1='RenewalMatterID')  /*GPR 2018-04-20 Changed new biz bit to 1*/
	BEGIN	
		SELECT @Matter = AnyValue2 FROM @PassedOverrides WHERE AnyValue1='RenewalMatterID'
		SELECT @PetNumber = petnumber.ValueInt -1 FROM MatterDetailValues petnumber WITH ( NOLOCK ) WHERE petnumber.DetailFieldID = 175453 AND petnumber.MatterID = @Matter
	
			SELECT @PetNumber = COUNT(l.LeadID)
						FROM (Lead l
						INNER JOIN Customers c ON c.CustomerID = l.CustomerID)
						INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID AND l.LeadTypeID = 1492 -- Policy Admin
						INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170038 --Policy Status
						WHERE mdv.ValueInt = 43002 -- Active
						AND c.CustomerID = @CustomerID
						GROUP BY c.CustomerID
		
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'PetNumber', CAST(@PetNumber AS VARCHAR))		 /*GPR 2018-04-30*/
	END

	-- Product Name
	DECLARE @ProductName VARCHAR(100)
	IF NOT EXISTS ( SELECT * FROM @PassedOverrides WHERE AnyValue1='ProductName')
	BEGIN
		SELECT @ProductName = dbo.fn_C00_1273_GetProductName(@SchemeID)
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'ProductName', CAST(@ProductName AS VARCHAR))
	END
	
	-- Item Name
	DECLARE @ItemName VARCHAR(100)
	IF NOT EXISTS ( SELECT * FROM @PassedOverrides WHERE AnyValue1='ItemName')
	BEGIN
		-- item name replaced with product name
		SELECT @ItemName = dbo.fn_C00_1273_GetProductName(@SchemeID)--dbo.fn_C00_1273_GetItemName(@SchemeID)
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'ItemName', CAST(@ItemName AS VARCHAR))
	END

	-- get a GUID for this evaluation (to speed up eval) but also used to record results of calc below
	DECLARE @GUID INT, @DummyXML XML
	
	Select @DummyXML = '<ClientId>' + CAST(@ClientID AS VARCHAR) + '</ClientId>'	
	
	DECLARE @AqAutomation INT = 58552
	INSERT dbo.PremiumCalculationDetail(ClientID, ProductCostBreakdown, WhoCreated, WhenCreated)
	VALUES (@ClientID,@DummyXML,@AqAutomation,dbo.fn_GetDate_Local())
	/*!!NO SQL HERE!!*/
	SELECT @GUID = SCOPE_IDENTITY()

	IF NOT EXISTS ( SELECT * /*CPS 2017-08-14 for #45019*/
	                FROM @Overrides o 
	                WHERE o.AnyValue1 = 'PostCode'
	                AND o.AnyValue2 <> '' )
		AND
	  @CaseID <> 0
	BEGIN
		SELECT @Postcode = cu.PostCode
		FROM Cases c WITH ( NOLOCK ) 
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = c.LeadID
		INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID
		WHERE c.CaseID = @CaseID
	
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
		VALUES ( 2, 'Postcode', @Postcode )
		PRINT @Postcode
		--EXEC _C00_LogIt @TypeOfLogEntry, @ClassName, @MethodName, @LogEntry, @ClientPersonnelID
		EXEC _C00_LogIt 'Info', '_C00_1273_Policy_CalculatePremium', 'Add Postcode to Overrides', @Postcode, @CaseID
	END	
		

	INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
	(2,'GUID',CAST(@GUID AS VARCHAR(300)))
PRINT '@GUID = ' + ISNULL(CONVERT(VARCHAR,@GUID),'NULL')

	IF not exists ( SELECT * 
	                FROM @Overrides o 
	                WHERE o.AnyValue1 = 'NewBusiness' )
	BEGIN
		INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
		VALUES (2,'NewBusiness','1')
	END

	DECLARE @AnnualPremium MONEY,
			@Output XML
	
	EXEC dbo.RulesEngine_Evaluate_RuleSet @RuleSetID, @CaseID = @CaseID, @Overrides = @Overrides, @Output = @Output OUTPUT

	SELECT @RuleSetID = r.RuleSetID
	FROM RulesEngine_Rules r WITH ( NOLOCK ) 
	WHERE r.RuleID = @Output.value('(//@RuleID)[1]','INT')
	-- This query isn't ordered so not guarenteed to get the last rule output
	
	IF @CaseID <> 0
	BEGIN
		EXEC _C00_LogItXML @ClientID, @CaseID, 'CalculationOutput', @Output
	END
	ELSE
	BEGIN
		EXEC _C00_LogItXML @ClientID, 0, 'CalculationOutput', @Output
	END
	
	SELECT @AnnualPremium = r.c.value('(@Output)[1]', 'MONEY')
    FROM @Output.nodes('//Rule[last()]') AS r(c)
PRINT '@AnnualPremium (1) = ' + ISNULL(CONVERT(VARCHAR,@AnnualPremium),'NULL')	

	DECLARE @GrossNet MONEY = 0.0, @GrossNetWithDiscount MONEY = 0.0, @GrossGross MONEY = 0.0,
			@IPT MONEY = 0.0, @IPTGrossNet MONEY = 0.0, @Commission MONEY = 0.0, @Discount MONEY = 0.0, 
			@PAF MONEY = 0.0, @PAFIfBeforeIPT MONEY=0.0, @PAFBeforeIPTGrossNet MONEY=0.0, @PAFIfAfterIPT MONEY=0.0,
			@FirstMonthly MONEY = 0.0, @RecurringMonthly MONEY = 0.0, @GrossGrossPlusDiscount MONEY = 0.0
			,@DiscountedMonths	INT
			,@MatterID			INT
		
	SELECT	 @DiscountedMonths	= dbo.fn_C600_GetDiscountMonthsFreeForMatter(m.MatterID)
	
			/*CPS 2017-07-31 work out the AdminFee*/
			,@PAFIfAfterIPT		= ISNULL ( 
								  ( SELECT SUM(pps.PaymentGross)
			     				    FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK ) 
			     				    WHERE pps.PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(m.MatterID,dbo.fn_GetDate_Local())
			     					AND pps.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
			     				  ) ,0.00 )
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.CaseID = @CaseID

	-- Strip rule checkpoint values from the output and store
	EXEC _C00_1273_Premium_StoreCalculationValues @ClientID, @Output, @GUID
PRINT '@GUID'
PRINT @GUID	
	-- update PremiumCalculationDetail with the real xml output
	UPDATE PremiumCalculationDetail 
	SET ProductCostBreakdown=@Output
	WHERE PremiumCalculationDetailID=@GUID
PRINT '@AnnualPremium = ' + ISNULL(CONVERT(VARCHAR,@AnnualPremium),'NULL')	

	-- Calculate key premium data
	IF @AnnualPremium > 0
	BEGIN
				
		SELECT	@FirstMonthly = FirstMonthly,
				@RecurringMonthly = RecurringMonthly
		FROM dbo.fn_C00_1273_SplitPremium(@AnnualPremium)
		SELECT @GrossGross=@AnnualPremium,@GrossGrossPlusDiscount=@AnnualPremium
				
		-- BEU need to get all checkpoints into premiumcalc store (use guid and call from within ruleset?)
		-- extract IPT from the stored calc values
		SELECT TOP 1 @IPT=ROUND(CAST(pcdv.RuleInputOutputDelta AS MONEY),2) 
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID=@GUID AND pcdv.RuleCheckpoint='IPT'
		
		-- PM24 GPR 2017-12-15
		SELECT TOP 1 @Discount=SUM(ROUND(CAST(pcdv.RuleInputOutputDelta AS MONEY),2))
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		--WHERE pcdv.PremiumCalculationDetailID=@GUID AND pcdv.RuleCheckpoint IN ( 'Apply available discount','Apply Available MultiPet Discount','MultiPet Discount','Premium Override Discount fixed value','Premium Override Discount percentage', 'Discount Code', 'MultiPet Relativity' ) -- GPR 2018-02-14 added 'Discount Code' to list, GPR 2018-02-16 added 'MultiPet Relativity' for Client600 Discount
		WHERE pcdv.PremiumCalculationDetailID=@GUID AND pcdv.RuleCheckpoint IN ( 'Apply available discount','Apply Available MultiPet Discount','MultiPet Discount','MultiPet Relativity', 'Discount','Discount Code') /*All Discounts*/

		-- GPR 2018-04-23
		SELECT TOP 1 @NonWPDiscount=SUM(ROUND(CAST(pcdv.RuleInputOutputDelta AS MONEY),2))
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID=@GUID AND pcdv.RuleCheckpoint IN ('Apply available discount','Apply Available MultiPet Discount','MultiPet Discount','MultiPet Relativity', 'Discount') /*WP Affecting*/

		-- GPR 2018-04-23
		SELECT TOP 1 @NonWPDiscount=SUM(ROUND(CAST(pcdv.RuleInputOutputDelta AS MONEY),2))
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID=@GUID AND pcdv.RuleCheckpoint IN ('Discount Code') /*Not WP Affecting*/

		SELECT @GrossNetWithDiscount = (@GrossGrossPlusDiscount) --+ ISNULL(@Discount,0.00)) /*GPR 2017-12-15*/

		SELECT TOP 1 @PAFIfAfterIPT=ROUND(CAST(pcdv.RuleInputOutputDelta AS MONEY),2)
		FROM PremiumCalculationDetailValues pcdv WITH ( NOLOCK )
		WHERE pcdv.PremiumCalculationDetailID=@GUID AND pcdv.RuleCheckpoint IN ('Admin Fee', 'Administration Fee') /*GPR 2018-02-21 Added 'Administration Fee'*/

		INSERT @Data (TermsDate, SchemeID, PolicyMatterID, RuleSetID, AnnualPremium, Discount, PremiumLessDiscount, FirstMonthly, RecurringMonthly, Net, Commission, GrossNet, DiscountGrossNet, PAFIfBeforeIPT, PAFBeforeIPTGrossNet, IPT, IPTGrossNet, PAFIfAfterIPT, GrossGross, PremiumCalculationID, PremiumLessTax, TaxLevel1, TaxLevel2)
		VALUES (@TermsDate, @SchemeID, @PolicyMatterID, @RuleSetID, @GrossGrossPlusDiscount, @Discount, @GrossGross, @FirstMonthly, @RecurringMonthly, @AnnualPremium, @Commission, @GrossNet, @GrossNetWithDiscount, @PAFIfBeforeIPT, @PAFBeforeIPTGrossNet, @IPT, @IPTGrossNet, @PAFIfAfterIPT, @GrossGross, @GUID, 0.00, 0.00, 0.00) /*GPR 2018-04-23 Added NonWPDiscount*/

	END
	
	
	/*GPR 2019-10-10 LPC-36*/
	DECLARE  @QuotePetProductID	INT = 0

	/*CPS 2018-06-05 log a quote set for reporting*/
	IF @CaseID <> 0 -- not quote and buy
	BEGIN
		DECLARE  @WhoCreated					INT
				,@ProcName						VARCHAR(2000) = OBJECT_NAME(@@ProcID)
				,@PremiumCalculationOutputXml	XML = @Output

		/*Get Missing Overrides*/
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
		SELECT ps.ParameterTypeID, ps.Value, CASE WHEN ps.ParameterTypeID = 5 THEN dbo.fnGetRLDv(ps.Parent,ps.Value,@CaseID)
												  ELSE dbo.fnGetDv(ps.Value,@CaseID)
											 END
		FROM RulesEngine_RuleParameters_PreSet ps WITH ( NOLOCK )
		WHERE ps.ParameterTypeID IN ( 1,5 )
		AND NOT EXISTS ( SELECT *
		                 FROM @Overrides ovr 
						 WHERE ovr.AnyValue1 = ps.[Value] )

		/*Pick up creation detail from the lead event*/
		SELECT	 @WhoCreated = le.WhoCreated
				,@MatterID	 = m.MatterID
		FROM LeadEvent le WITH ( NOLOCK )
		INNER JOIN Matter m WITH ( NOLOCK ) on m.CaseID = le.CaseID
		WHERE le.LeadEventID = @LeadEventID

		/*Save the quote stack*/
		EXEC @QuotePetProductID = Quote_SaveQuoteSingleCompleteSet NULL, NULL, @MatterID, @WhoCreated, @LeadEventID, @ProcName, NULL, NULL, @Overrides, @RuleSetID, @AnnualPremium, 1, NULL, @PremiumCalculationOutputXml,  NULL, @LeadEventID
	END

	SELECT * 
	FROM @Data

	RETURN @QuotePetProductID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremium] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CalculatePremium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremium] TO [sp_executeall]
GO
