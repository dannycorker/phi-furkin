SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UserFunctionControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_Delete]
(

	@UserFunctionControlID int   
)
AS


				DELETE FROM [dbo].[UserFunctionControl] WITH (ROWLOCK) 
				WHERE
					[UserFunctionControlID] = @UserFunctionControlID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_Delete] TO [sp_executeall]
GO
