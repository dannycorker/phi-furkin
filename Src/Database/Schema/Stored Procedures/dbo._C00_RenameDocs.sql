SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Update documents
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RenameDocs]
(
	@DocumentTypeID nvarchar(100),
	@DocumentTypeName nvarchar(1000)
)

AS
BEGIN
	SET NOCOUNT ON;

	Update DocumentType
	Set DocumentTypeName = @DocumentTypeName, DocumentTypeDescription = @DocumentTypeName
	From DocumentType
	Where DocumentTypeID = @DocumentTypeID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RenameDocs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RenameDocs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RenameDocs] TO [sp_executeall]
GO
