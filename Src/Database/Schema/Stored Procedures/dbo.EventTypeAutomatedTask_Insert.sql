SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeAutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAutomatedTask_Insert]
(

	@EventTypeAutomatedTaskID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@AutomatedTaskID int   ,

	@TimeDelayInSeconds int   ,

	@AddTimeDelayIfAboutToRun bit   
)
AS


				
				INSERT INTO [dbo].[EventTypeAutomatedTask]
					(
					[ClientID]
					,[EventTypeID]
					,[AutomatedTaskID]
					,[TimeDelayInSeconds]
					,[AddTimeDelayIfAboutToRun]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@AutomatedTaskID
					,@TimeDelayInSeconds
					,@AddTimeDelayIfAboutToRun
					)
				-- Get the identity value
				SET @EventTypeAutomatedTaskID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Insert] TO [sp_executeall]
GO
