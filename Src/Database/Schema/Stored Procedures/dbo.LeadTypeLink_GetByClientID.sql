SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypeLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadTypeLinkID],
					[ClientID],
					[LeadTypeID],
					[OutcomeID]
				FROM
					[dbo].[LeadTypeLink] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_GetByClientID] TO [sp_executeall]
GO
