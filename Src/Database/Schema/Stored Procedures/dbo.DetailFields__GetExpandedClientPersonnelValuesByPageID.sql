SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-09-08
-- Description:	Used in the SDK this PROC gets matter or lead detail values by page.  It expands lookup lists, and resource lists nested 
-- in tables so the SDK can return a nicely formatted collection of objects
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetExpandedClientPersonnelValuesByPageID]
	@ClientID INT,
	@ClientPersonnelID INT,
	@PageID INT
AS
BEGIN
	
	DECLARE @FieldIDs tvpInt 
	
	INSERT @FieldIDs (AnyID)
	SELECT DetailFieldID 	
	FROM dbo.fnDetailFieldsShared(@ClientID)
	WHERE DetailFieldPageID = @PageID 
	AND LeadOrMatter = 13

	EXEC [dbo].[DetailFields__GetExpandedClientPersonnelValuesByFieldIDs] @ClientID, @ClientPersonnelID, @FieldIDs

END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedClientPersonnelValuesByPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetExpandedClientPersonnelValuesByPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedClientPersonnelValuesByPageID] TO [sp_executeall]
GO
