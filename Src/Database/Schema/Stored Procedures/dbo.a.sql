SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-03-26
-- Description:	One proc to rule them all. 
-- WhatToShow = 0 (all) by default. 1 = procs only, 2 = AquariumHelperObject options only.
-- =============================================
CREATE PROCEDURE [dbo].[a] 
	@WhatToShow INT = 0
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @t TABLE (
		ProcName VARCHAR(30), 
		ProcDescStart INT, 
		ProcDescEnd INT, 
		ProcDesc VARCHAR(1000), 
		ProcParams VARCHAR(500)
	)

	DECLARE @Params VARCHAR(1000) = '', 
	@ProcDescStart INT = 0,
	@ProcDescEnd INT = 0, 
	@ProcDesc VARCHAR(1000), 
	@CurrentProc VARCHAR(30) = 'a',
	@LastProc VARCHAR(30) = '',
	@InfiniteLoopTest INT = 1

	IF @WhatToShow IN (0, 1)
	BEGIN
		INSERT INTO @t(ProcName, ProcDescStart, ProcParams)
		SELECT r.ROUTINE_NAME, 
		CHARINDEX('Description', r.ROUTINE_DEFINITION, 1), 
		'' 
		FROM INFORMATION_SCHEMA.ROUTINES r
		WHERE r.ROUTINE_TYPE = 'PROCEDURE' 
		AND LEN(r.ROUTINE_NAME) < 9 
		AND r.ROUTINE_NAME NOT LIKE '[_]C%'
		ORDER BY r.ROUTINE_NAME

		WHILE @CurrentProc IS NOT NULL AND @InfiniteLoopTest < 1000 
		BEGIN
			
			SELECT @ProcDescStart = t.ProcDescStart 
			FROM @t t 
			WHERE t.ProcName = @CurrentProc 
			
			IF @ProcDescStart > 0
			BEGIN
				SELECT @ProcDescEnd = CHARINDEX('-- =', r.ROUTINE_DEFINITION, @ProcDescStart) 
				FROM INFORMATION_SCHEMA.ROUTINES r 
				WHERE r.ROUTINE_NAME = @CurrentProc 
				
				IF @ProcDescEnd > @ProcDescStart
				BEGIN
					SELECT @ProcDesc = SUBSTRING(r.ROUTINE_DEFINITION, @ProcDescStart + 13, @ProcDescEnd - @ProcDescStart - 13)
					FROM INFORMATION_SCHEMA.ROUTINES r 
					WHERE r.ROUTINE_NAME = @CurrentProc 
				END
				
			END
			
			SELECT @Params += p.PARAMETER_NAME + ' (' + p.DATA_TYPE + '), ' 
			FROM INFORMATION_SCHEMA.ROUTINES r 
			INNER JOIN INFORMATION_SCHEMA.PARAMETERS p ON p.SPECIFIC_NAME = r.SPECIFIC_NAME 
			WHERE r.ROUTINE_NAME = @CurrentProc 
			ORDER BY p.ORDINAL_POSITION 
			
			IF @Params > ''
			BEGIN
				SELECT @Params = SUBSTRING(@Params, 1, LEN(@Params) - 1) 
			END
			
			UPDATE @t 
			SET ProcDescEnd = @ProcDescEnd, 
			ProcDesc = @ProcDesc, 
			ProcParams = COALESCE(@Params, '') 
			WHERE ProcName = @CurrentProc 
			
			SELECT @LastProc = @CurrentProc, 
			@CurrentProc = NULL, 
			@ProcDescStart = 0, 
			@ProcDescEnd = 0, 
			@Params = '', 
			@ProcDescStart = 0, 
			@ProcDescEnd = 0, 
			@InfiniteLoopTest += 1  
			
			UPDATE @t 
			SET ProcParams = @Params 
			WHERE ProcName = @CurrentProc 
			
			SELECT TOP 1 @CurrentProc = t.ProcName 
			FROM @t t 
			WHERE t.ProcName > @LastProc 
			
		END
		
		SELECT * 
		FROM @t 
		ORDER BY ProcName 
		
	END
	
	IF @WhatToShow IN (0, 2)
	BEGIN
		SELECT aho.MenuID, aho.SubmenuID, aho.ProcName, aho.Notes, aho.IsExecutable, aho.HasIntParam, aho.NeedsIntParam, COALESCE(aho.IntParamName, '') AS IntParamName 
		FROM dbo.AquariumHelperObject aho WITH (NOLOCK)  
		ORDER BY aho.MenuID, aho.SubmenuID 
		
		EXEC dbo.aq 
	END
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[a] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[a] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[a] TO [sp_executeall]
GO
