SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingSystemCredential table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential__GetByThirdPartySystemIDAndClientID]
(

	@ThirdPartySystemID int,
	@ClientID int
	
)
AS


				SELECT
					[ThirdPartyMappingSystemCredentialID],
					[ThirdPartySystemID],
					[ClientID],
					[ConsumerKey],
					[ConsumerSecret]
				FROM
					[dbo].[ThirdPartyMappingSystemCredential]
				WHERE
					[ThirdPartySystemID] = @ThirdPartySystemID AND
					[ClientID] = @ClientID
					
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential__GetByThirdPartySystemIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential__GetByThirdPartySystemIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential__GetByThirdPartySystemIDAndClientID] TO [sp_executeall]
GO
