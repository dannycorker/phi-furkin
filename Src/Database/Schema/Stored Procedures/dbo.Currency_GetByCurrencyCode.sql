SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Currency table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Currency_GetByCurrencyCode]
(

	@CurrencyCode char (3)  
)
AS


				SELECT
					[CurrencyID],
					[CurrencyCode],
					[CurrencyName],
					[WhenCreated],
					[WhenModified],
					[MajorNamePlural],
					[MajorNameSingular],
					[MinorNamePlural],
					[MinorNameSingular],
					[CurrencySymbol]
				FROM
					[dbo].[Currency] WITH (NOLOCK) 
				WHERE
										[CurrencyCode] = @CurrencyCode
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_GetByCurrencyCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Currency_GetByCurrencyCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_GetByCurrencyCode] TO [sp_executeall]
GO
