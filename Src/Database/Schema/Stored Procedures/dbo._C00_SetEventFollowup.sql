SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 10-Nov-2010
-- Description:	Proc to change followup on event
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SetEventFollowup]
(
@LeadEventID int,
@DateFieldID int,
@TimeFieldID int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@CaseID int,
			@DateTimeForFollowup varchar(16)

	SELECT @CaseID = le.CaseID
	FROM LeadEvent le WITH (NOLOCK)
	WHERE le.leadeventid = @LeadEventID 

	Select @DateTimeForFollowup = dbo.fnGetDv(@DateFieldID,@CaseID) + ' ' + dbo.fnGetDv(@TimeFieldID,@CaseID)
	
	IF ISDATE(@DateTimeForFollowup) = 1
	BEGIN
	
		IF @DateTimeForFollowup > dbo.fn_GetDate_Local()
		BEGIN

			UPDATE LeadEvent
			SET FollowupDateTime = @DateTimeForFollowup
			WHERE LeadEventID = @LeadEventID
			
		END
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetEventFollowup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SetEventFollowup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetEventFollowup] TO [sp_executeall]
GO
