SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[EventType_SummaryCountByLeadType]
(
	@LeadTypeID int
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT et.EventTypeID, et.EventTypeName, et.LeadTypeID, et.EventSubtypeID, COUNT(*) AS [LeadEventCount]
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID AND et.LeadTypeID = @LeadTypeID
	WHERE le.WhenFollowedUp is null
	AND le.EventDeleted = 0
	GROUP BY et.EventTypeID, et.EventTypeName, et.LeadTypeID, et.EventSubtypeID
	--ORDER BY et.EventTypeID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_SummaryCountByLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_SummaryCountByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_SummaryCountByLeadType] TO [sp_executeall]
GO
