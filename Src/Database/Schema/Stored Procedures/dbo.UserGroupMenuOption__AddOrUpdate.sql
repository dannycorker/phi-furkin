SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-13
-- Description:	Add/Save user Group Menu Option
-- =============================================
CREATE PROCEDURE [dbo].[UserGroupMenuOption__AddOrUpdate]
	@UserGroupMenuOptionID INT = NULL,
	@ClientPersonnelAdminGroupID INT,
	@ClientID INT,
	@MasterPageName VARCHAR(20),
	@PanelItemAction VARCHAR(10),
	@PanelItemName VARCHAR(50),
	@PanelItemCaption VARCHAR(50),
	@PanelItemIcon VARCHAR(50),
	@PanelItemURL VARCHAR(250),
	@WhoCreated INT = NULL,
	@WhoModified INT,
	@Comments VARCHAR(250),
	@ItemOrder INT,
	@ShowInFancyBox BIT
AS
BEGIN

	SET NOCOUNT ON;

	IF @UserGroupMenuOptionID > 0
	BEGIN
	
		UPDATE u
		SET ClientPersonnelAdminGroupID = CASE WHEN @ClientPersonnelAdminGroupID = -1 THEN NULL ELSE @ClientPersonnelAdminGroupID END, 
				ClientID = @ClientID, 
				MasterPageName = @MasterPageName, 
				PanelItemAction = @PanelItemAction,
				PanelItemName = @PanelItemName,
				PanelItemCaption = @PanelItemCaption,
				PanelItemIcon = @PanelItemIcon,
				PanelItemURL = @PanelItemURL,
				WhoModified = @WhoModified,
				WhenModified = dbo.fn_GetDate_Local(),
				Comments = @Comments,
				ItemOrder = @ItemOrder,
				ShowInFancyBox = @ShowInFancyBox
		FROM UserGroupMenuOption u
		WHERE u.UserGroupMenuOptionID = @UserGroupMenuOptionID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.UserGroupMenuOption (ClientPersonnelAdminGroupID, ClientID, MasterPageName, PanelItemAction, PanelItemName, PanelItemCaption, PanelItemIcon, PanelItemURL, WhoCreated, WhenCreated, WhoModified, WhenModified, Comments, ItemOrder, ShowInFancyBox)
		VALUES (CASE WHEN @ClientPersonnelAdminGroupID = -1 THEN NULL ELSE @ClientPersonnelAdminGroupID END, @ClientID, @MasterPageName, @PanelItemAction, @PanelItemName, @PanelItemCaption, @PanelItemIcon, @PanelItemURL, @WhoCreated, dbo.fn_GetDate_Local(), @WhoModified, dbo.fn_GetDate_Local(), @Comments, @ItemOrder, @ShowInFancyBox)
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__AddOrUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption__AddOrUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__AddOrUpdate] TO [sp_executeall]
GO
