SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUser table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@Username varchar (50)  = null ,

	@Password varchar (65)  = null ,

	@Salt varchar (25)  = null ,

	@ClientPersonnelID int   = null ,

	@Enabled bit   = null ,

	@AttemptedLogins int   = null ,

	@PortalUserGroupID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserID]
	, [ClientID]
	, [CustomerID]
	, [Username]
	, [Password]
	, [Salt]
	, [ClientPersonnelID]
	, [Enabled]
	, [AttemptedLogins]
	, [PortalUserGroupID]
    FROM
	[dbo].[PortalUser] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserID] = @PortalUserID OR @PortalUserID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([Username] = @Username OR @Username IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([Salt] = @Salt OR @Salt IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([AttemptedLogins] = @AttemptedLogins OR @AttemptedLogins IS NULL)
	AND ([PortalUserGroupID] = @PortalUserGroupID OR @PortalUserGroupID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserID]
	, [ClientID]
	, [CustomerID]
	, [Username]
	, [Password]
	, [Salt]
	, [ClientPersonnelID]
	, [Enabled]
	, [AttemptedLogins]
	, [PortalUserGroupID]
    FROM
	[dbo].[PortalUser] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserID] = @PortalUserID AND @PortalUserID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([Username] = @Username AND @Username is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([Salt] = @Salt AND @Salt is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([AttemptedLogins] = @AttemptedLogins AND @AttemptedLogins is not null)
	OR ([PortalUserGroupID] = @PortalUserGroupID AND @PortalUserGroupID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Find] TO [sp_executeall]
GO
