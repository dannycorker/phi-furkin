SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PageGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroup_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PageGroupID],
					[ClientID],
					[LeadTypeID],
					[PageGroupName],
					[PageGroupDescription],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[PageGroup] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroup_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_GetByClientID] TO [sp_executeall]
GO
