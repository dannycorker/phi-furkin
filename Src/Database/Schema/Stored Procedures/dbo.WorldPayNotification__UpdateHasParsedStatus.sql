SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 02/08/2017
-- Description:	Updates the status of a world pay notification to parsed
-- =============================================
CREATE PROCEDURE [dbo].[WorldPayNotification__UpdateHasParsedStatus]
	@WorldPayNotificationID INT,
	@HasBeenParsed BIT
AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE WorldPayNotification
	SET HasBeenParsed=@HasBeenParsed, ParsedOn = dbo.fn_GetDate_Local()	
	WHERE WorldPayNotificationID=@WorldPayNotificationID    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayNotification__UpdateHasParsedStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorldPayNotification__UpdateHasParsedStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayNotification__UpdateHasParsedStatus] TO [sp_executeall]
GO
