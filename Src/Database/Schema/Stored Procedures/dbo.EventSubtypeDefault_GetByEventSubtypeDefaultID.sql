SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventSubtypeDefault table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_GetByEventSubtypeDefaultID]
(

	@EventSubtypeDefaultID int   
)
AS


				SELECT
					[EventSubtypeDefaultID],
					[ClientID],
					[EventSubtypeID],
					[EventTypeID],
					[LeadTypeID],
					[ClientPersonnelID]
				FROM
					[dbo].[EventSubtypeDefault] WITH (NOLOCK) 
				WHERE
										[EventSubtypeDefaultID] = @EventSubtypeDefaultID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_GetByEventSubtypeDefaultID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_GetByEventSubtypeDefaultID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_GetByEventSubtypeDefaultID] TO [sp_executeall]
GO
