SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-01
-- Description:	Usage check for the table and ID passed in. 
--              It tells users where to go and look for records to edit.
--              This will also allow us to offer a delete option for records which are not being used at all.
-- =============================================
CREATE PROCEDURE [dbo].[UsageChecks] 
	@TableName varchar(250), 
	@ClientID int, 
	@IDToCheck int 
AS
BEGIN
	SET NOCOUNT ON;

	SET NOCOUNT ON;

	DECLARE @UsageCount int, @TempUsageCount int, @UsageList varchar(max)

	SELECT @UsageCount = 0, @UsageList = ''

	/*
		There are 2 scenarios to check for:
		1) When inserting a new record (@IDToIgnore IS NULL), make sure that no record already has the
		   value we are about to insert;  
		2) When updating an existing record, make sure that no other record is using the value.
	*/

	IF @TableName = 'LookupList'
    BEGIN
		SELECT @UsageCount = COUNT(*)
		FROM dbo.DetailFields df (nolock) 
		WHERE df.LookupListID = @IDToCheck 
		AND df.ClientID = @ClientID

		IF @UsageCount > 0
		BEGIN
			-- Create a concatenated string of information about all the fields using this list.
			-- Format is FIELD + ID + PAGE + LEADTYPE 
			SELECT @UsageList = @UsageList + '"' + df.FieldName + '" (' + convert(varchar,df.DetailFieldID) + ') on page "' + dfp.PageName + '" for ' + lt.LeadTypeName + ' (' + convert(varchar, lt.LeadTypeID) + ') <BR />'
			FROM dbo.DetailFields df (nolock) 
			INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
			INNER JOIN dbo.LeadType lt (nolock) ON lt.LeadTypeID = df.LeadTypeID
			WHERE df.LookupListID = @IDToCheck 
			AND df.ClientID = @ClientID

			SELECT @UsageList = LEFT(@UsageList, LEN(@UsageList) - 7) -- to remove the final ' <BR />'

			SELECT @UsageList = 'This Lookup List is being used by the following field' + CASE @UsageCount WHEN 1 THEN '' ELSE 's' END + ':<BR />' + @UsageList
		END
	END


	/*
		Resource lists are a bit of a nightmare because there is no tangible object to go and check.
		1) A DetailFieldPages record has a column called ResourceList. When this is set to 1
		   then we know this page represents a resource list (call this "RL Page" for short).
		   The title of the page is effectively the name of the resource list.
		   Example: DetailFieldPageID 581 = "Source/ATE/Medical Agency/Process Agency" for Farley Dwek.

		2) One or many DetailFields records are created on that RL Page.
		   For each field within the resource list, DetailFields.DetailFieldPageID = dfp.DetailFieldPageID 
		   from step 1 above. 
		   Example: DetailFieldID 4543 = "Res - ATE Provider Name", 
		                          4544 = "Res - ATE Provider Address (block)" etc.

		3) The user enters some data into the fields created in step 2 above.
		   These values are stored in the ResourceListDetailValues table and each set of records 
		   is linked to a common ResourceList record with the same value of ResourceListID.
		   Example: ResourceListID = 830 has entries for field 4543 of "ATE Ltd", 
		                                                       4544 of "50 Algitha Road  Skegness  Lincolnshire  PA25 2AW" etc

		4) Within an ordinary Lead or Matter details page, one field is created with a QuestionTypeID = 14
		   to specify that this is where the RL Page will be displayed.
		   Example: DetailFieldID 6677 ("Source") specifies ResourceListDetailFieldID = 581
		            NB so does field 23848 ("xxx-spare") and there could be many more.

		5) The LeadDetailValues.DetailValue or MatterDetailValues.DetailValue for an individual Lead or Matter
		   gets set to the ResourceListID that the user picks from the list on screen.
		   Example: MatterID 230653 has a value of "830" in field 6677, so as we can see from the details 
		            shown in step 3 above, this will render the following information on the screen:
		            "ATE Ltd", 
		            "50 Algitha Road  Skegness  Lincolnshire  PA25 2AW" 
		            "01754 760 377"   and so on

		So, usage count can come from the number of detail fields that implement the RL Page,
		and/or it can also come from how many resource list entries have actually been selected
		for use within LeadDetailValues and MatterDetailValues.

		Call this proc with 'ResourceList' to see which pages the RL appears on
		Call this proc with 'ResourceListDetails' to see how many times the RL entries are used

	*/
	IF @TableName = 'ResourceList'
    BEGIN

		SELECT @UsageCount = COUNT(*)
		FROM dbo.DetailFields df (nolock) 
		WHERE df.ResourceListDetailFieldPageID = @IDToCheck 
		AND df.ClientID = @ClientID

		IF @UsageCount > 0
		BEGIN
			-- Create a concatenated string of information about all the fields using this list.
			-- Format is FIELD + ID + PAGE + LEADTYPE 
			SELECT @UsageList = @UsageList + '"' + df.FieldName + '" (' + convert(varchar,df.DetailFieldID) + ') on page "' + dfp.PageName + '" for ' + lt.LeadTypeName + ' (' + convert(varchar, lt.LeadTypeID) + ') <BR />'
			FROM dbo.DetailFields df (nolock) 
			INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
			INNER JOIN dbo.LeadType lt (nolock) ON lt.LeadTypeID = df.LeadTypeID
			WHERE df.ResourceListDetailFieldPageID = @IDToCheck 
			AND df.ClientID = @ClientID

			SELECT @UsageList = LEFT(@UsageList, LEN(@UsageList) - 7) -- to remove the final ' <BR />'

			SELECT @UsageList = 'This Resource List is being used by the following field' + CASE @UsageCount WHEN 1 THEN '' ELSE 's' END + ':<BR />' + @UsageList
		END
	END

	/*
		Please see all the notes above the section for 'ResourceList' as well.
		This block counts how many LeadDetailValues, MatterDetailValues and TableDetailValues have chosen 
		any value from the resource list specified (as opposed to just having a blank in that field).
	*/
	IF @TableName = 'ResourceListDetails'
    BEGIN

		-- LeadDetailValues
		SELECT @TempUsageCount = COUNT(*)
		FROM dbo.DetailFields df (nolock) 
		INNER JOIN dbo.LeadDetailValues AS dv (nolock) ON dv.DetailFieldID = df.DetailFieldID 
		WHERE df.ResourceListDetailFieldPageID = @IDToCheck 
		AND df.ClientID = @ClientID 
		AND dv.DetailValue <> '' 

		SELECT @UsageList = 'This Resource List is being used by ' + convert(varchar, @TempUsageCount) + ' Lead Detail Values record' + CASE @TempUsageCount WHEN 1 THEN '' ELSE 's' END + ', ',
		@UsageCount = @UsageCount + @TempUsageCount

		-- MatterDetailValues
		SELECT @TempUsageCount = COUNT(*)
		FROM dbo.DetailFields df (nolock) 
		INNER JOIN dbo.MatterDetailValues AS dv (nolock) ON dv.DetailFieldID = df.DetailFieldID 
		WHERE df.ResourceListDetailFieldPageID = @IDToCheck 
		AND df.ClientID = @ClientID 
		AND dv.DetailValue <> '' 

		SELECT @UsageList = @UsageList + convert(varchar, @TempUsageCount) + ' Matter Detail Values record' + CASE @TempUsageCount WHEN 1 THEN '' ELSE 's' END + ', and ',
		@UsageCount = @UsageCount + @TempUsageCount

		-- TableDetailValues
		SELECT @TempUsageCount = COUNT(*)
		FROM dbo.DetailFields df (nolock) 
		INNER JOIN dbo.TableDetailValues AS dv (nolock) ON dv.DetailFieldID = df.DetailFieldID 
		WHERE df.ResourceListDetailFieldPageID = @IDToCheck 
		AND df.ClientID = @ClientID 
		AND dv.DetailValue <> '' 

		SELECT @UsageList = @UsageList + convert(varchar, @TempUsageCount) + ' Table Detail Values record' + CASE @TempUsageCount WHEN 1 THEN '' ELSE 's' END + '.',
		@UsageCount = @UsageCount + @TempUsageCount

	END


	IF @TableName = 'LeadType'
    BEGIN
		SELECT @UsageCount = COUNT(*)
		FROM dbo.Lead l (nolock)
		WHERE l.LeadTypeID = @IDToCheck 
		AND l.ClientID = @ClientID

		IF @UsageCount > 0
		BEGIN
			-- Create a user-friendly message if any records were found. 
			SELECT @UsageList = 'This Lead Type has ' + convert(varchar, @UsageCount) + ' associated Lead record' +  + CASE @UsageCount WHEN 1 THEN '.' ELSE 's.' END
		END
	END
	
	IF @TableName = 'DetailFieldStyle'
    BEGIN
		SELECT @UsageCount = COUNT(*)
		FROM dbo.DetailFields df (nolock)
		WHERE df.DetailFieldStyleID = @IDToCheck 
		AND df.ClientID = @ClientID

		IF @UsageCount > 0
		BEGIN
			-- Create a user-friendly message if any records were found. 
			
			SELECT @UsageList = '<ul>' -- Start with a UL tag to show messages in a nice friendly bulleted list
			-- Add in a new LI for each record found
			SELECT @UsageList = @UsageList + '<li>"' + df.FieldName + '" (' + CONVERT(varchar,df.DetailFieldID) + ') on page "' + dfp.PageName + '" for ' + lt.LeadTypeName + ' (' + convert(varchar, lt.LeadTypeID) + ') </li>'
			FROM dbo.DetailFields df (nolock) 
			INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
			INNER JOIN dbo.LeadType lt (nolock) ON lt.LeadTypeID = df.LeadTypeID
			WHERE df.DetailFieldStyleID = @IDToCheck 
			AND df.ClientID = @ClientID
			
			--SELECT @UsageList = LEFT(@UsageList, LEN(@UsageList) - 7) -- to remove the final ' <BR />'
			
			SELECT @UsageList = @UsageList + '</ul>' -- Add the trailing End UL tag
			
		END
	END



	SELECT @UsageCount as [UsageCount], @UsageList as [UsageList]
END









GO
GRANT VIEW DEFINITION ON  [dbo].[UsageChecks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UsageChecks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UsageChecks] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[UsageChecks] TO [sp_executehelper]
GO
