SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSeriesSetting table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting_GetByChartSeriesSettingID]
(

	@ChartSeriesSettingID int   
)
AS


				SELECT
					[ChartSeriesSettingID],
					[ChartSeriesID],
					[SettingName],
					[SettingValue],
					[ClientID]
				FROM
					[dbo].[ChartSeriesSetting] WITH (NOLOCK) 
				WHERE
										[ChartSeriesSettingID] = @ChartSeriesSettingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_GetByChartSeriesSettingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting_GetByChartSeriesSettingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_GetByChartSeriesSettingID] TO [sp_executeall]
GO
