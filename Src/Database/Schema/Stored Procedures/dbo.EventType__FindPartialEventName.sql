SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-03-2015
-- Description:	Gets a list of event types that match the give partial event name
-- 2016-10-12 LB added enabled Check
-- =============================================
CREATE PROCEDURE [dbo].[EventType__FindPartialEventName]
	@ClientID INT,
	@EventTypeName VARCHAR(50),
	@LeadTypeID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM EventType WITH (NOLOCK) 
	WHERE EventTypeName LIKE '%' + @EventTypeName + '%' AND ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND Enabled = 1

END

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__FindPartialEventName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__FindPartialEventName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__FindPartialEventName] TO [sp_executeall]
GO
