SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlFunction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunction_Insert]
(

	@SqlFunctionID int   ,

	@SqlFunctionName varchar (50)  ,

	@FunctionAlias varchar (50)  ,

	@FunctionDescription varchar (250)  ,

	@RestrictedToDataTypeID int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@IsPreferred bit   ,

	@Example varchar (250)  ,

	@ExampleResult varchar (250)  ,

	@ResultDataTypeID int   ,

	@ExpressionMask varchar (250)  ,

	@IsAggregate bit   
)
AS


				
				INSERT INTO [dbo].[SqlFunction]
					(
					[SqlFunctionID]
					,[SqlFunctionName]
					,[FunctionAlias]
					,[FunctionDescription]
					,[RestrictedToDataTypeID]
					,[WhenCreated]
					,[WhenModified]
					,[IsPreferred]
					,[Example]
					,[ExampleResult]
					,[ResultDataTypeID]
					,[ExpressionMask]
					,[IsAggregate]
					)
				VALUES
					(
					@SqlFunctionID
					,@SqlFunctionName
					,@FunctionAlias
					,@FunctionDescription
					,@RestrictedToDataTypeID
					,@WhenCreated
					,@WhenModified
					,@IsPreferred
					,@Example
					,@ExampleResult
					,@ResultDataTypeID
					,@ExpressionMask
					,@IsAggregate
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunction_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Insert] TO [sp_executeall]
GO
