SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Functions table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_Find]
(

	@SearchUsingOR bit   = null ,

	@FunctionID int   = null ,

	@FunctionName varchar (50)  = null ,

	@FunctionDescription varchar (2000)  = null ,

	@FunctionTypeID int   = null ,

	@ModuleID int   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FunctionID]
	, [FunctionName]
	, [FunctionDescription]
	, [FunctionTypeID]
	, [ModuleID]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[Functions] WITH (NOLOCK) 
    WHERE 
	 ([FunctionID] = @FunctionID OR @FunctionID IS NULL)
	AND ([FunctionName] = @FunctionName OR @FunctionName IS NULL)
	AND ([FunctionDescription] = @FunctionDescription OR @FunctionDescription IS NULL)
	AND ([FunctionTypeID] = @FunctionTypeID OR @FunctionTypeID IS NULL)
	AND ([ModuleID] = @ModuleID OR @ModuleID IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FunctionID]
	, [FunctionName]
	, [FunctionDescription]
	, [FunctionTypeID]
	, [ModuleID]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[Functions] WITH (NOLOCK) 
    WHERE 
	 ([FunctionID] = @FunctionID AND @FunctionID is not null)
	OR ([FunctionName] = @FunctionName AND @FunctionName is not null)
	OR ([FunctionDescription] = @FunctionDescription AND @FunctionDescription is not null)
	OR ([FunctionTypeID] = @FunctionTypeID AND @FunctionTypeID is not null)
	OR ([ModuleID] = @ModuleID AND @ModuleID is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Find] TO [sp_executeall]
GO
