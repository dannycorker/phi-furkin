SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the StyleRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[StyleRule_Insert]
(

	@StyleRuleID int    OUTPUT,

	@RuleClass varchar (100)  ,

	@RuleDescription varchar (255)  ,

	@RuleValue varchar (100)  ,

	@AquariumOnly bit   ,

	@IncludeLinkText bit   
)
AS


				
				INSERT INTO [dbo].[StyleRule]
					(
					[RuleClass]
					,[RuleDescription]
					,[RuleValue]
					,[AquariumOnly]
					,[IncludeLinkText]
					)
				VALUES
					(
					@RuleClass
					,@RuleDescription
					,@RuleValue
					,@AquariumOnly
					,@IncludeLinkText
					)
				-- Get the identity value
				SET @StyleRuleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StyleRule_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Insert] TO [sp_executeall]
GO
