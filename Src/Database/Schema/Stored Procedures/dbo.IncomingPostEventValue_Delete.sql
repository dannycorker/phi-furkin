SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the IncomingPostEventValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEventValue_Delete]
(

	@IncomingPostEventValueID int   
)
AS


				DELETE FROM [dbo].[IncomingPostEventValue] WITH (ROWLOCK) 
				WHERE
					[IncomingPostEventValueID] = @IncomingPostEventValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEventValue_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Delete] TO [sp_executeall]
GO
