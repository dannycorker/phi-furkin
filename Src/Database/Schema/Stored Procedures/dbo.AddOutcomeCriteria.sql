SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddOutcomeCriteria    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[AddOutcomeCriteria]

@OutcomeID  int,
@QuestionPossibleAnswerID int, 
@ClientID int

as

insert into OutcomeCriterias (OutcomeID, QuestionPossibleAnswerID, ClientID)
values (@OutcomeID, @QuestionPossibleAnswerID, @ClientID)

declare @OutcomeCriteriaID int

set @OutcomeCriteriaID = SCOPE_IDENTITY()

select OutcomeCriteriaID
from OutcomeCriterias
where OutcomeCriteriaID = @OutcomeCriteriaID






GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcomeCriteria] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddOutcomeCriteria] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcomeCriteria] TO [sp_executeall]
GO
