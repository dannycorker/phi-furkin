SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadDocument table
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_Get_List]

AS
BEGIN

	/* This should never be used! */
	RETURN -1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Get_List] TO [sp_executeall]
GO
