SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadTypePageTitleStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_Update]
(

	@LeadTypePageTitleStandardTargetID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@TemplateTypeID int   ,

	@Notes varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadTypePageTitleStandardTarget]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[Target] = @Target
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
					,[TemplateTypeID] = @TemplateTypeID
					,[Notes] = @Notes
				WHERE
[LeadTypePageTitleStandardTargetID] = @LeadTypePageTitleStandardTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Update] TO [sp_executeall]
GO
