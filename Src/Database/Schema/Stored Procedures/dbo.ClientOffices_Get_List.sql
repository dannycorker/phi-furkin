SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientOffices table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_Get_List]

AS


				
				SELECT
					[ClientOfficeID],
					[ClientID],
					[OfficeName],
					[BuildingName],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Country],
					[PostCode],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[OfficeFax],
					[CountryID],
					[LanguageID],
					[SubClientID],
					[AdminClientPersonnelID],
					[CurrencyID]
				FROM
					[dbo].[ClientOffices] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Get_List] TO [sp_executeall]
GO
