SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Equations table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Equations_Find]
(

	@SearchUsingOR bit   = null ,

	@EquationID int   = null ,

	@EquationName varchar (50)  = null ,

	@Equation varchar (MAX)  = null ,

	@ClientQuestionnaireID int   = null ,

	@ClientID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EquationID]
	, [EquationName]
	, [Equation]
	, [ClientQuestionnaireID]
	, [ClientID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[Equations] WITH (NOLOCK) 
    WHERE 
	 ([EquationID] = @EquationID OR @EquationID IS NULL)
	AND ([EquationName] = @EquationName OR @EquationName IS NULL)
	AND ([Equation] = @Equation OR @Equation IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EquationID]
	, [EquationName]
	, [Equation]
	, [ClientQuestionnaireID]
	, [ClientID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[Equations] WITH (NOLOCK) 
    WHERE 
	 ([EquationID] = @EquationID AND @EquationID is not null)
	OR ([EquationName] = @EquationName AND @EquationName is not null)
	OR ([Equation] = @Equation AND @Equation is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Find] TO [sp_executeall]
GO
