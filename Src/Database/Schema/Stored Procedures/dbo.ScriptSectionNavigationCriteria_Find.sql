SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptSectionNavigationCriteria table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptSectionNavigationCriteriaID int   = null ,

	@ClientID int   = null ,

	@FromStepSectionID int   = null ,

	@ToStepSectionID int   = null ,

	@WizardButton varchar (12)  = null ,

	@DetailFieldID int   = null ,

	@CriteriaOperator varchar (12)  = null ,

	@Value varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptSectionNavigationCriteriaID]
	, [ClientID]
	, [FromStepSectionID]
	, [ToStepSectionID]
	, [WizardButton]
	, [DetailFieldID]
	, [CriteriaOperator]
	, [Value]
    FROM
	[dbo].[ScriptSectionNavigationCriteria] WITH (NOLOCK) 
    WHERE 
	 ([ScriptSectionNavigationCriteriaID] = @ScriptSectionNavigationCriteriaID OR @ScriptSectionNavigationCriteriaID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FromStepSectionID] = @FromStepSectionID OR @FromStepSectionID IS NULL)
	AND ([ToStepSectionID] = @ToStepSectionID OR @ToStepSectionID IS NULL)
	AND ([WizardButton] = @WizardButton OR @WizardButton IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([CriteriaOperator] = @CriteriaOperator OR @CriteriaOperator IS NULL)
	AND ([Value] = @Value OR @Value IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptSectionNavigationCriteriaID]
	, [ClientID]
	, [FromStepSectionID]
	, [ToStepSectionID]
	, [WizardButton]
	, [DetailFieldID]
	, [CriteriaOperator]
	, [Value]
    FROM
	[dbo].[ScriptSectionNavigationCriteria] WITH (NOLOCK) 
    WHERE 
	 ([ScriptSectionNavigationCriteriaID] = @ScriptSectionNavigationCriteriaID AND @ScriptSectionNavigationCriteriaID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FromStepSectionID] = @FromStepSectionID AND @FromStepSectionID is not null)
	OR ([ToStepSectionID] = @ToStepSectionID AND @ToStepSectionID is not null)
	OR ([WizardButton] = @WizardButton AND @WizardButton is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([CriteriaOperator] = @CriteriaOperator AND @CriteriaOperator is not null)
	OR ([Value] = @Value AND @Value is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Find] TO [sp_executeall]
GO
