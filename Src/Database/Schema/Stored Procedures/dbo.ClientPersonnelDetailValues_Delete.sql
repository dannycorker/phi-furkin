SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnelDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelDetailValues_Delete]
(

	@ClientPersonnelDetailValueID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnelDetailValues] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelDetailValueID] = @ClientPersonnelDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_Delete] TO [sp_executeall]
GO
