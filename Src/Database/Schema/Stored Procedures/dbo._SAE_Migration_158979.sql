SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM SOFTWARE
-- Create date: 2020-02-04
-- Description:	Sql After Event - Set Migrated Policy to Cancelled
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Migration_158979]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	/*GPR 2020-01-16 MTC in the Renewal Window (LPC-366 / LPC-382*/
	IF @EventTypeID = 158979 --begin - Set Migrated Policy to Cancelled
	BEGIN
		
		/*Migrated Policy that has Renewed on Aquarium*/
		IF EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=@CaseID AND le.EventDeleted=0 AND le.EventTypeID = 158850 /*Migrated Policy Process Start*/
		AND EXISTS (SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=@CaseID AND le.EventDeleted=0 AND le.EventTypeID IN (150151) /*Renew Policy*/))
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>This Policy is a migrated Policy that HAS renewed on Aquarium. Please use the standard Cancellation process to Cancel this Policy.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END

		/*NonMigrated Policy that is on Aquarium*/
		IF NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=@CaseID AND le.EventDeleted=0 AND le.EventTypeID = 158850 /*Migrated Policy Process Start*/)
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>This Event can only be applied to Migrated Policies pre-renewal. Please use the standard Cancellation process to Cancel this Policy.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END

	
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Migration_158979] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Migration_158979] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Migration_158979] TO [sp_executeall]
GO
