SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2014
-- Description:	Find customers via name
--				Does not include business customers
--				Ordered by full name ascending
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByName]

	@ClientID INT,
	@Name VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND IsBusiness=0
	  AND Fullname like '%' + @Name + '%'
	ORDER BY Fullname ASC
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByName] TO [sp_executeall]
GO
