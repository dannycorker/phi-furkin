SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptEventTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_GetByScriptEventTargetID]
(

	@ScriptEventTargetID int   
)
AS


				SELECT
					[ScriptEventTargetID],
					[ScriptID],
					[SectionID],
					[ClientID],
					[Target],
					[EventTypeID],
					[EventTypeName],
					[EventSubtypeID],
					[Format]
				FROM
					[dbo].[ScriptEventTarget] WITH (NOLOCK) 
				WHERE
										[ScriptEventTargetID] = @ScriptEventTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_GetByScriptEventTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_GetByScriptEventTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_GetByScriptEventTargetID] TO [sp_executeall]
GO
