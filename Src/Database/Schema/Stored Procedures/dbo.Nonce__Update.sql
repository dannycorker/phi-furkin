SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Ben Crinion
-- Create date: 27 - Feb - 2008
-- Description:	Update the Nonce with a new value.
-- =============================================
CREATE PROCEDURE [dbo].[Nonce__Update]
	@Value tinyint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Nonce set NonceValue = @Value
END



GO
GRANT VIEW DEFINITION ON  [dbo].[Nonce__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Nonce__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Nonce__Update] TO [sp_executeall]
GO
