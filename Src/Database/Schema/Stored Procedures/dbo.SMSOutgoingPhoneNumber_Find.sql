SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SMSOutgoingPhoneNumber table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_Find]
(

	@SearchUsingOR bit   = null ,

	@SMSOutgoingPhoneNumberID int   = null ,

	@ClientID int   = null ,

	@SMSQuestionnaireID int   = null ,

	@LeadTypeID int   = null ,

	@OutgoingPhoneNumber varchar (16)  = null ,

	@SMSSurveyStartEventTypeID int   = null ,

	@MMSLetterInEvent int   = null ,

	@SmsGatewayID int   = null ,

	@HasIntroductionText bit   = null ,

	@IntroductionTextFrom varchar (250)  = null ,

	@IntroductionText varchar (2000)  = null ,

	@DelayInSeconds int   = null ,

	@IntroductionTextEventTypeID int   = null ,

	@IntroductionTextDocumentTypeID int   = null ,

	@NoCLIRejectText varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SMSOutgoingPhoneNumberID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [LeadTypeID]
	, [OutgoingPhoneNumber]
	, [SMSSurveyStartEventTypeID]
	, [MMSLetterInEvent]
	, [SmsGatewayID]
	, [HasIntroductionText]
	, [IntroductionTextFrom]
	, [IntroductionText]
	, [DelayInSeconds]
	, [IntroductionTextEventTypeID]
	, [IntroductionTextDocumentTypeID]
	, [NoCLIRejectText]
    FROM
	[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
    WHERE 
	 ([SMSOutgoingPhoneNumberID] = @SMSOutgoingPhoneNumberID OR @SMSOutgoingPhoneNumberID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SMSQuestionnaireID] = @SMSQuestionnaireID OR @SMSQuestionnaireID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([OutgoingPhoneNumber] = @OutgoingPhoneNumber OR @OutgoingPhoneNumber IS NULL)
	AND ([SMSSurveyStartEventTypeID] = @SMSSurveyStartEventTypeID OR @SMSSurveyStartEventTypeID IS NULL)
	AND ([MMSLetterInEvent] = @MMSLetterInEvent OR @MMSLetterInEvent IS NULL)
	AND ([SmsGatewayID] = @SmsGatewayID OR @SmsGatewayID IS NULL)
	AND ([HasIntroductionText] = @HasIntroductionText OR @HasIntroductionText IS NULL)
	AND ([IntroductionTextFrom] = @IntroductionTextFrom OR @IntroductionTextFrom IS NULL)
	AND ([IntroductionText] = @IntroductionText OR @IntroductionText IS NULL)
	AND ([DelayInSeconds] = @DelayInSeconds OR @DelayInSeconds IS NULL)
	AND ([IntroductionTextEventTypeID] = @IntroductionTextEventTypeID OR @IntroductionTextEventTypeID IS NULL)
	AND ([IntroductionTextDocumentTypeID] = @IntroductionTextDocumentTypeID OR @IntroductionTextDocumentTypeID IS NULL)
	AND ([NoCLIRejectText] = @NoCLIRejectText OR @NoCLIRejectText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SMSOutgoingPhoneNumberID]
	, [ClientID]
	, [SMSQuestionnaireID]
	, [LeadTypeID]
	, [OutgoingPhoneNumber]
	, [SMSSurveyStartEventTypeID]
	, [MMSLetterInEvent]
	, [SmsGatewayID]
	, [HasIntroductionText]
	, [IntroductionTextFrom]
	, [IntroductionText]
	, [DelayInSeconds]
	, [IntroductionTextEventTypeID]
	, [IntroductionTextDocumentTypeID]
	, [NoCLIRejectText]
    FROM
	[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
    WHERE 
	 ([SMSOutgoingPhoneNumberID] = @SMSOutgoingPhoneNumberID AND @SMSOutgoingPhoneNumberID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SMSQuestionnaireID] = @SMSQuestionnaireID AND @SMSQuestionnaireID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([OutgoingPhoneNumber] = @OutgoingPhoneNumber AND @OutgoingPhoneNumber is not null)
	OR ([SMSSurveyStartEventTypeID] = @SMSSurveyStartEventTypeID AND @SMSSurveyStartEventTypeID is not null)
	OR ([MMSLetterInEvent] = @MMSLetterInEvent AND @MMSLetterInEvent is not null)
	OR ([SmsGatewayID] = @SmsGatewayID AND @SmsGatewayID is not null)
	OR ([HasIntroductionText] = @HasIntroductionText AND @HasIntroductionText is not null)
	OR ([IntroductionTextFrom] = @IntroductionTextFrom AND @IntroductionTextFrom is not null)
	OR ([IntroductionText] = @IntroductionText AND @IntroductionText is not null)
	OR ([DelayInSeconds] = @DelayInSeconds AND @DelayInSeconds is not null)
	OR ([IntroductionTextEventTypeID] = @IntroductionTextEventTypeID AND @IntroductionTextEventTypeID is not null)
	OR ([IntroductionTextDocumentTypeID] = @IntroductionTextDocumentTypeID AND @IntroductionTextDocumentTypeID is not null)
	OR ([NoCLIRejectText] = @NoCLIRejectText AND @NoCLIRejectText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Find] TO [sp_executeall]
GO
