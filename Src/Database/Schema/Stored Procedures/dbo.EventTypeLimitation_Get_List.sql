SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeLimitation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation_Get_List]

AS


				
				SELECT
					[EventTypeLimitationID],
					[ClientID],
					[LeadTypeID],
					[EventTypeIDFrom],
					[EventTypeIDTo],
					[LimitationDays],
					[Description]
				FROM
					[dbo].[EventTypeLimitation] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Get_List] TO [sp_executeall]
GO
