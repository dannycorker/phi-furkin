SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CheckMateFieldMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_GetByCheckMateFieldMappingID]
(

	@CheckMateFieldMappingID int   
)
AS


				SELECT
					[CheckMateFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[CheckMateFieldID],
					[FieldID],
					[ColumnFieldID],
					[DataLoaderObjectTypeID],
					[CheckMateFieldGroupID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[CheckMateFieldMapping] WITH (NOLOCK) 
				WHERE
										[CheckMateFieldMappingID] = @CheckMateFieldMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_GetByCheckMateFieldMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_GetByCheckMateFieldMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_GetByCheckMateFieldMappingID] TO [sp_executeall]
GO
