SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIFieldGroup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldGroup_GetByRPIFieldGroupID]
(

	@RPIFieldGroupID int   
)
AS


				SELECT
					[RPIFieldGroupID],
					[GroupName]
				FROM
					[dbo].[RPIFieldGroup] WITH (NOLOCK) 
				WHERE
										[RPIFieldGroupID] = @RPIFieldGroupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_GetByRPIFieldGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldGroup_GetByRPIFieldGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_GetByRPIFieldGroupID] TO [sp_executeall]
GO
