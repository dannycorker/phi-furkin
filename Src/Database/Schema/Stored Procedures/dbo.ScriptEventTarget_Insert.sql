SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptEventTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_Insert]
(

	@ScriptEventTargetID int    OUTPUT,

	@ScriptID int   ,

	@SectionID int   ,

	@ClientID int   ,

	@Target varchar (250)  ,

	@EventTypeID int   ,

	@EventTypeName varchar (250)  ,

	@EventSubtypeID int   ,

	@Format varchar (250)  
)
AS


				
				INSERT INTO [dbo].[ScriptEventTarget]
					(
					[ScriptID]
					,[SectionID]
					,[ClientID]
					,[Target]
					,[EventTypeID]
					,[EventTypeName]
					,[EventSubtypeID]
					,[Format]
					)
				VALUES
					(
					@ScriptID
					,@SectionID
					,@ClientID
					,@Target
					,@EventTypeID
					,@EventTypeName
					,@EventSubtypeID
					,@Format
					)
				-- Get the identity value
				SET @ScriptEventTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Insert] TO [sp_executeall]
GO
