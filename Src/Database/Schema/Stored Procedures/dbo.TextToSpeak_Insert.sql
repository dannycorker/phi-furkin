SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the TextToSpeak table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TextToSpeak_Insert]
(

	@TextToSpeakID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@PageNumber int   ,

	@SpeakText varchar (512)  ,

	@IsShown bit   
)
AS


				
				INSERT INTO [dbo].[TextToSpeak]
					(
					[ClientQuestionnaireID]
					,[ClientID]
					,[PageNumber]
					,[SpeakText]
					,[IsShown]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@ClientID
					,@PageNumber
					,@SpeakText
					,@IsShown
					)
				-- Get the identity value
				SET @TextToSpeakID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TextToSpeak_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Insert] TO [sp_executeall]
GO
