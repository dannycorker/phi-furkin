SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Inserts a record into the notification table and returns the NotificationID
-- =============================================
CREATE PROCEDURE [dbo].[Notification__Insert]

   @NotificationID INT OUTPUT,
   @ClientID INT,
   @SubClientID INT,
   @ThirdPartySystemID INT,
   @Message VARCHAR(MAX),
   @DateMessageCreated DATETIME,
   @MessageCreatedBy INT,
   @NotificationTypeID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [Notification]
           ([ClientID]
           ,[SubClientID]
           ,[ThirdPartySystemID]
           ,[MESSAGE]
           ,[DateMessageCreated]
           ,[MessageCreatedBy]
           ,[NotificationTypeID])
     VALUES
          (@ClientID,
           @SubClientID, 
           @ThirdPartySystemID, 
           @Message, 
           @DateMessageCreated, 
           @MessageCreatedBy, 
           @NotificationTypeID )
           
	-- Get the identity value
	SELECT SCOPE_IDENTITY() NotificationID          

END




GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Notification__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__Insert] TO [sp_executeall]
GO
