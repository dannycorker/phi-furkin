SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-30
-- Description:	Returns rule output data for a specific rule 
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleOutputs_GetOutputsByRuleID_Backup]
(
	@ClientID INT,
	@RuleID INT
)


AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	p.RuleParameterID, p.Name, p.ParameterTypeID, p.Value, p.DataTypeID
	FROM dbo.RulesEngine_RuleParameters p WITH (NOLOCK)
	WHERE p.ClientID = @ClientID
	AND p.RuleID = @RuleID
	
	
	;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON df.DetailFieldID = p.Value
		WHERE p.RuleID = @RuleID
		AND df.ClientID = @ClientID
		AND p.DataTypeID = 6
	)
		
	SELECT	o.ParameterOptionID, o.OperatorID, op.Name AS Operator, o.RuleParameterID, o.Val1, o.Val2,
			CASE
				WHEN ll.LookupListItemID IS NOT NULL THEN
						REPLACE(op.OperatorText, '{0}', ll.ItemValue)
				ELSE	REPLACE(REPLACE(op.OperatorText, '{0}', o.Val1), '{1}', ISNULL(o.Val2, ''))
			END AS OperatorText
	FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON o.OperatorID = op.OperatorID
	INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON o.RuleParameterID = p.RuleParameterID
	LEFT JOIN LookupList ll ON ll.LookupListItemID =	CASE 
															WHEN p.DataTypeID = 6 THEN o.Val1
															ELSE NULL
														END
	WHERE o.ClientID = @ClientID
	AND p.RuleID = @RuleID
	ORDER BY o.OptionOrder
	
	SELECT o.RuleID, o.RuleOutputID, o.TransformID, o.Value 
	FROM dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) 
	WHERE o.RuleID = @RuleID
	
	SELECT c.OutputCoordinateID, c.RuleOutputID, c.ParameterOptionID
	FROM dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) ON c.RuleOutputID = o.RuleOutputID
	WHERE o.RuleID = @RuleID
	
	SELECT 'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord
	UNION ALL
	SELECT s.Name AS Title, '#/ruleset/' + CAST(r.RuleSetID AS VARCHAR) + '/rules' AS Path, 2 AS Ord
	FROM dbo.RulesEngine_RuleSets s WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleSetID = s.RuleSetID
	WHERE r.RuleID = @RuleID
	UNION ALL
	SELECT r.Name AS Title, '#/rule/' + CAST(r.RuleID AS VARCHAR) AS Path, 3 AS Ord
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	WHERE r.RuleID = @RuleID
	UNION ALL
	SELECT 'Values in Grid' AS Title, '#/rule/' + CAST(r.RuleID AS VARCHAR) + '/outputs_grid' AS Path, 4 AS Ord
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	WHERE r.RuleID = @RuleID
	ORDER BY Ord
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_GetOutputsByRuleID_Backup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleOutputs_GetOutputsByRuleID_Backup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_GetOutputsByRuleID_Backup] TO [sp_executeall]
GO
