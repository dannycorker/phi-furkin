SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Customers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_Insert]
(

	@CustomerID int    OUTPUT,

	@ClientID int   ,

	@TitleID int   ,

	@IsBusiness bit   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@EmailAddress varchar (255)  ,

	@DayTimeTelephoneNumber varchar (50)  ,

	@DayTimeTelephoneNumberVerifiedAndValid bit   ,

	@HomeTelephone varchar (50)  ,

	@HomeTelephoneVerifiedAndValid bit   ,

	@MobileTelephone varchar (50)  ,

	@MobileTelephoneVerifiedAndValid bit   ,

	@CompanyTelephone varchar (50)  ,

	@CompanyTelephoneVerifiedAndValid bit   ,

	@WorksTelephone varchar (50)  ,

	@WorksTelephoneVerifiedAndValid bit   ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (200)  ,

	@PostCode varchar (50)  ,

	@Website varchar (200)  ,

	@HasDownloaded bit   ,

	@DownloadedOn datetime   ,

	@AquariumStatusID int   ,

	@ClientStatusID int   ,

	@Test bit   ,

	@CompanyName varchar (100)  ,

	@Occupation varchar (100)  ,

	@Employer varchar (100)  ,

	@Fullname varchar (201)   OUTPUT,

	@PhoneNumbersVerifiedOn datetime   ,

	@DoNotEmail bit   ,

	@DoNotSellToThirdParty bit   ,

	@AgreedToTermsAndConditions bit   ,

	@DateOfBirth datetime   ,

	@DefaultContactID int   ,

	@DefaultOfficeID int   ,

	@AddressVerified bit   ,

	@CountryID int   ,

	@SubClientID int   ,

	@CustomerRef varchar (100)  ,

	@WhoChanged int   ,

	@WhenChanged datetime   ,

	@ChangeSource varchar (200)  ,

	@EmailAddressVerifiedAndValid bit   ,

	@EmailAddressVerifiedOn datetime   ,

	@Comments varchar (MAX)  ,

	@AllowSmsCommandProcessing bit   ,

	@LanguageID int   ,

	@Longitude numeric (25, 18)  ,

	@Latitude numeric (25, 18)  
)
AS


				
				INSERT INTO [dbo].[Customers]
					(
					[ClientID]
					,[TitleID]
					,[IsBusiness]
					,[FirstName]
					,[MiddleName]
					,[LastName]
					,[EmailAddress]
					,[DayTimeTelephoneNumber]
					,[DayTimeTelephoneNumberVerifiedAndValid]
					,[HomeTelephone]
					,[HomeTelephoneVerifiedAndValid]
					,[MobileTelephone]
					,[MobileTelephoneVerifiedAndValid]
					,[CompanyTelephone]
					,[CompanyTelephoneVerifiedAndValid]
					,[WorksTelephone]
					,[WorksTelephoneVerifiedAndValid]
					,[Address1]
					,[Address2]
					,[Town]
					,[County]
					,[PostCode]
					,[Website]
					,[HasDownloaded]
					,[DownloadedOn]
					,[AquariumStatusID]
					,[ClientStatusID]
					,[Test]
					,[CompanyName]
					,[Occupation]
					,[Employer]
					,[PhoneNumbersVerifiedOn]
					,[DoNotEmail]
					,[DoNotSellToThirdParty]
					,[AgreedToTermsAndConditions]
					,[DateOfBirth]
					,[DefaultContactID]
					,[DefaultOfficeID]
					,[AddressVerified]
					,[CountryID]
					,[SubClientID]
					,[CustomerRef]
					,[WhoChanged]
					,[WhenChanged]
					,[ChangeSource]
					,[EmailAddressVerifiedAndValid]
					,[EmailAddressVerifiedOn]
					,[Comments]
					,[AllowSmsCommandProcessing]
					,[LanguageID]
					,[Longitude]
					,[Latitude]
					)
				VALUES
					(
					@ClientID
					,@TitleID
					,@IsBusiness
					,@FirstName
					,@MiddleName
					,@LastName
					,@EmailAddress
					,@DayTimeTelephoneNumber
					,@DayTimeTelephoneNumberVerifiedAndValid
					,@HomeTelephone
					,@HomeTelephoneVerifiedAndValid
					,@MobileTelephone
					,@MobileTelephoneVerifiedAndValid
					,@CompanyTelephone
					,@CompanyTelephoneVerifiedAndValid
					,@WorksTelephone
					,@WorksTelephoneVerifiedAndValid
					,@Address1
					,@Address2
					,@Town
					,@County
					,@PostCode
					,@Website
					,@HasDownloaded
					,@DownloadedOn
					,@AquariumStatusID
					,@ClientStatusID
					,@Test
					,@CompanyName
					,@Occupation
					,@Employer
					,@PhoneNumbersVerifiedOn
					,@DoNotEmail
					,@DoNotSellToThirdParty
					,@AgreedToTermsAndConditions
					,@DateOfBirth
					,@DefaultContactID
					,@DefaultOfficeID
					,@AddressVerified
					,@CountryID
					,@SubClientID
					,@CustomerRef
					,@WhoChanged
					,@WhenChanged
					,@ChangeSource
					,@EmailAddressVerifiedAndValid
					,@EmailAddressVerifiedOn
					,@Comments
					,@AllowSmsCommandProcessing
					,@LanguageID
					,@Longitude
					,@Latitude
					)
				-- Get the identity value
				SET @CustomerID = SCOPE_IDENTITY()
									
				-- Select computed columns into output parameters
				SELECT
 @Fullname = [Fullname]
				FROM
					[dbo].[Customers] WITH (NOLOCK) 
				WHERE
[CustomerID] = @CustomerID 
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Insert] TO [sp_executeall]
GO
