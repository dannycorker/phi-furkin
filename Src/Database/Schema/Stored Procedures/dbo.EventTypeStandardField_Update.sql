SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeStandardField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_Update]
(

	@EventTypeStandardFieldID int   ,

	@EventTypeID int   ,

	@DataLoaderObjectFieldID int   ,

	@ClientID int   ,

	@Mandatory bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeStandardField]
				SET
					[EventTypeID] = @EventTypeID
					,[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
					,[ClientID] = @ClientID
					,[Mandatory] = @Mandatory
				WHERE
[EventTypeStandardFieldID] = @EventTypeStandardFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Update] TO [sp_executeall]
GO
