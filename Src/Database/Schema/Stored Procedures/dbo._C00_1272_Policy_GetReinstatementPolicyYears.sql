SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-08
-- Description:	Gets the policy years for a customers policy but only if it is reinstatement.  If not... we return nothing.
-- Used by:		DF 145673
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetReinstatementPolicyYears] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	

	DECLARE @LeadID INT,
			@CaseID INT,
			@CurrentPolicyMatterID INT 
	SELECT @LeadID = LeadID, @CaseID = CaseID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID	
	
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	
	DECLARE @ClaimRows TABLE
	(
		ID INT,
		MatterID INT,
		ResourceListID INT,
		ParentResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		DateOfLoss DATE,
		TreatmentStart DATE,
		TreatmentEnd DATE
	)
	
	;WITH ClaimRowData AS 
	(
	SELECT	ROW_NUMBER() OVER(ORDER BY CASE WHEN m.CaseID = @CaseID THEN 1 ELSE 0 END, ISNULL(tdvPaid.ValueDateTime, dbo.fn_GetDate_Local()), tdvStart.ValueDate, r.TableRowID) AS ID,
			tdvSettle.MatterID, tdvRLID.ResourceListID, ISNULL(s.Out_ResourceListID, tdvRLID.ResourceListID) AS ParentResourceListID, 
			ISNULL(mdvDateOfLoss.ValueDate, tdvStart.ValueDate) AS DateOfLoss, tdvStart.ValueDate AS TreatmentStart, tdvEnd.ValueDate AS TreatmentEnd,  
			llSection.ItemValue AS SECTION, 
			llSubSection.ItemValue AS SubSection

	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m ON r.MatterID = m.MatterID -- get related condition claims or all if a reinstatement policy
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON m.CaseID = c.CaseID 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.ResourceListDetailValues rdvSection ON tdvRLID.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection ON rdvSection.ValueInt = llSection.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection ON tdvRLID.ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection ON rdvSubSection.ValueInt = llSubSection.LookupListItemID
	LEFT JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID AND s.ResourceListID != s.Out_ResourceListID
	INNER JOIN dbo.TableDetailValues tdvClaim WITH (NOLOCK) ON r.TableRowID = tdvClaim.TableRowID AND tdvClaim.DetailFieldID = 144353
	INNER JOIN dbo.TableDetailValues tdvSettle WITH (NOLOCK) ON r.TableRowID = tdvSettle.TableRowID AND tdvSettle.DetailFieldID = 145678
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 144349 
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 144351
	LEFT JOIN dbo.TableDetailValues tdvPaid WITH (NOLOCK) ON r.TableRowID = tdvPaid.TableRowID AND tdvPaid.DetailFieldID = 144362 -- 144354 we now use approved and not paid
	LEFT JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892

	WHERE r.DetailFieldID = 144355
	AND r.DetailFieldPageID = 16157
	AND r.ClientID = 384
	AND (c.ClientStatusID IS NULL OR c.ClientStatusID NOT IN (3819)) -- Exclude rejected claims
	AND (tdvPaid.ValueDate IS NOT NULL OR c.CaseID = @CaseID) -- All paid rows and the rows from this claim
	and m.MatterID <> @MatterID
	)
	INSERT @ClaimRows (ID, MatterID, ResourceListID, ParentResourceListID, DateOfLoss, TreatmentStart, TreatmentEnd,  Section, SubSection)
	
	SELECT * 
	FROM ClaimRowData

	DECLARE @DateToUse DATE
	SELECT @DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@MatterID, NULL, NULL)
	
	-- We only want to return rows here if the policy type at loss was reinstatement
	DECLARE @PolicyTypeAtDateOfLoss INT
	SELECT @PolicyTypeAtDateOfLoss = rdvScheme.ValueInt
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	INNER JOIN dbo.TableDetailValues tdvPolicy WITH (NOLOCK) ON r.TableRowID = tdvPolicy.TableRowID AND tdvPolicy.DetailFieldID = 145665
	INNER JOIN dbo.ResourceListDetailValues rdvScheme WITH (NOLOCK) ON tdvPolicy.ResourceListID = rdvScheme.ResourceListID AND rdvScheme.DetailFieldID = 144319
	WHERE tdvStart.ValueDate <= @DateToUse
	AND tdvEnd.ValueDate > @DateToUse
	
	DECLARE @Include BIT = 0
	IF @PolicyTypeAtDateOfLoss = 42933
	BEGIN
		SELECT @Include = 1
	END

	SELECT  tdvStart.ValueDate AS PolicyStart, tdvEnd.ValueDate AS PolicyEnd, c.ParentResourceListID, c.ResourceListID, c.Section , c.SubSection, c.TreatmentStart, c.TreatmentEnd 
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	INNER JOIN dbo.LeadTypeRelationship lt WITH (NOLOCK) on lt.FromMatterID = r.MatterID and lt.ToLeadTypeID = 1490
	Inner join @ClaimRows c on c.MatterID = lt.ToMatterID 
	WHERE @Include = 1
	ORDER BY tdvStart.ValueDate DESC
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetReinstatementPolicyYears] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetReinstatementPolicyYears] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetReinstatementPolicyYears] TO [sp_executeall]
GO
