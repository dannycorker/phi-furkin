SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChangeStepState table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStepState_Insert]
(

	@ChangeStepStateID int    OUTPUT,

	@StateDescription varchar (255)  
)
AS


				
				INSERT INTO [dbo].[ChangeStepState]
					(
					[StateDescription]
					)
				VALUES
					(
					@StateDescription
					)
				-- Get the identity value
				SET @ChangeStepStateID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStepState_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Insert] TO [sp_executeall]
GO
