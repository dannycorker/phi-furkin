SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClassNodeAuth table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_Get_List]

AS


				
				SELECT
					[ClassNodeAuthID],
					[ClientID],
					[ClassNodeID],
					[ClientPersonnelAdminGroupID],
					[RightsLevel],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ClassNodeAuth] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Get_List] TO [sp_executeall]
GO
