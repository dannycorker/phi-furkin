SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-08
-- Description:	Clear up all the WorkWins... tables after use
-- =============================================
CREATE PROCEDURE [dbo].[ClearAllWorkWinsTables] 
	@WorkWinSeedID int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM dbo.WorkWinsCasesWithBlocks 
	WHERE ( WorkWinSeedID = @WorkWinSeedID OR @WorkWinSeedID < 0 ) 

	DELETE FROM dbo.WorkWinsCasesWithWins 
	WHERE ( WorkWinSeedID = @WorkWinSeedID OR @WorkWinSeedID < 0 ) 

	DELETE FROM dbo.WorkWinsFirstValidSave 
	WHERE ( WorkWinSeedID = @WorkWinSeedID OR @WorkWinSeedID < 0 ) 

	DELETE FROM dbo.WorkWinsLatestValidSave 
	WHERE ( WorkWinSeedID = @WorkWinSeedID OR @WorkWinSeedID < 0 ) 

	DELETE FROM dbo.WorkWinsPossibleValidMatters 
	WHERE ( WorkWinSeedID = @WorkWinSeedID OR @WorkWinSeedID < 0 ) 

	DELETE FROM dbo.WorkWinsCurrent 
	WHERE ( WorkWinSeedID = @WorkWinSeedID OR @WorkWinSeedID < 0 ) 

END






GO
GRANT VIEW DEFINITION ON  [dbo].[ClearAllWorkWinsTables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClearAllWorkWinsTables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClearAllWorkWinsTables] TO [sp_executeall]
GO
