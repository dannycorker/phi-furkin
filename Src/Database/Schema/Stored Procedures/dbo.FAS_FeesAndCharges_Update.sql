SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FAS_FeesAndCharges table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_FeesAndCharges_Update]
(

	@FasFeesAndChargesID int   ,

	@FasCheckerDataID int   ,

	@Description varchar (512)  ,

	@Amount decimal (18, 0)  ,

	@WhenPaid varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FAS_FeesAndCharges]
				SET
					[FasCheckerDataID] = @FasCheckerDataID
					,[Description] = @Description
					,[Amount] = @Amount
					,[WhenPaid] = @WhenPaid
				WHERE
[FasFeesAndChargesID] = @FasFeesAndChargesID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_FeesAndCharges_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Update] TO [sp_executeall]
GO
