SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AccessRule table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_Find]
(

	@SearchUsingOR bit   = null ,

	@AccessRuleID int   = null ,

	@ClientID int   = null ,

	@AccessRuleName varchar (100)  = null ,

	@AccessRuleDescription varchar (250)  = null ,

	@AccessRuleEnabled bit   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientPersonnelID int   = null ,

	@PortalUserID int   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@LeadTypeID int   = null ,

	@DetailFieldSubTypeID int   = null ,

	@DetailFieldPageID int   = null ,

	@DetailFieldID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@ValueDecoder varchar (50)  = null ,

	@ValueDecoderColumnFieldID int   = null ,

	@ValueToCompare int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@AccessLevel tinyint   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AccessRuleID]
	, [ClientID]
	, [AccessRuleName]
	, [AccessRuleDescription]
	, [AccessRuleEnabled]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [PortalUserID]
	, [DataLoaderObjectTypeID]
	, [LeadTypeID]
	, [DetailFieldSubTypeID]
	, [DetailFieldPageID]
	, [DetailFieldID]
	, [ClientQuestionnaireID]
	, [ValueDecoder]
	, [ValueDecoderColumnFieldID]
	, [ValueToCompare]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [AccessLevel]
    FROM
	[dbo].[AccessRule] WITH (NOLOCK) 
    WHERE 
	 ([AccessRuleID] = @AccessRuleID OR @AccessRuleID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([AccessRuleName] = @AccessRuleName OR @AccessRuleName IS NULL)
	AND ([AccessRuleDescription] = @AccessRuleDescription OR @AccessRuleDescription IS NULL)
	AND ([AccessRuleEnabled] = @AccessRuleEnabled OR @AccessRuleEnabled IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([PortalUserID] = @PortalUserID OR @PortalUserID IS NULL)
	AND ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([DetailFieldSubTypeID] = @DetailFieldSubTypeID OR @DetailFieldSubTypeID IS NULL)
	AND ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([ValueDecoder] = @ValueDecoder OR @ValueDecoder IS NULL)
	AND ([ValueDecoderColumnFieldID] = @ValueDecoderColumnFieldID OR @ValueDecoderColumnFieldID IS NULL)
	AND ([ValueToCompare] = @ValueToCompare OR @ValueToCompare IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([AccessLevel] = @AccessLevel OR @AccessLevel IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AccessRuleID]
	, [ClientID]
	, [AccessRuleName]
	, [AccessRuleDescription]
	, [AccessRuleEnabled]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [PortalUserID]
	, [DataLoaderObjectTypeID]
	, [LeadTypeID]
	, [DetailFieldSubTypeID]
	, [DetailFieldPageID]
	, [DetailFieldID]
	, [ClientQuestionnaireID]
	, [ValueDecoder]
	, [ValueDecoderColumnFieldID]
	, [ValueToCompare]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [AccessLevel]
    FROM
	[dbo].[AccessRule] WITH (NOLOCK) 
    WHERE 
	 ([AccessRuleID] = @AccessRuleID AND @AccessRuleID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([AccessRuleName] = @AccessRuleName AND @AccessRuleName is not null)
	OR ([AccessRuleDescription] = @AccessRuleDescription AND @AccessRuleDescription is not null)
	OR ([AccessRuleEnabled] = @AccessRuleEnabled AND @AccessRuleEnabled is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([PortalUserID] = @PortalUserID AND @PortalUserID is not null)
	OR ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([DetailFieldSubTypeID] = @DetailFieldSubTypeID AND @DetailFieldSubTypeID is not null)
	OR ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([ValueDecoder] = @ValueDecoder AND @ValueDecoder is not null)
	OR ([ValueDecoderColumnFieldID] = @ValueDecoderColumnFieldID AND @ValueDecoderColumnFieldID is not null)
	OR ([ValueToCompare] = @ValueToCompare AND @ValueToCompare is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([AccessLevel] = @AccessLevel AND @AccessLevel is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Find] TO [sp_executeall]
GO
