SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Jim Green 2008-09-11
-- Purpose: Gets all records from the TimeUnits table that are worth showing to the user (default 4 = Hours)
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TimeUnits__GetSensibleOnly] 
	@MinimumToUse tinyint = 4
AS

BEGIN
	SET NOCOUNT ON;

	SELECT
		[TimeUnitsID],
		[TimeUnitsName],
		[SQLTimeUnits]
	FROM
		[dbo].[TimeUnits]
	WHERE					
		[TimeUnitsID] >= @MinimumToUse

	UNION

	SELECT
		0 as [TimeUnitsID],
		'Please Select...' as [TimeUnitsName],
		0 as [SQLTimeUnits]

	ORDER BY [TimeUnitsID]

END




GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits__GetSensibleOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimeUnits__GetSensibleOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits__GetSensibleOnly] TO [sp_executeall]
GO
