SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderFileStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileStatus_Update]
(

	@DataLoaderFileStatusID int   ,

	@OriginalDataLoaderFileStatusID int   ,

	@FileStatusName varchar (250)  ,

	@FileStatusDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderFileStatus]
				SET
					[DataLoaderFileStatusID] = @DataLoaderFileStatusID
					,[FileStatusName] = @FileStatusName
					,[FileStatusDescription] = @FileStatusDescription
				WHERE
[DataLoaderFileStatusID] = @OriginalDataLoaderFileStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Update] TO [sp_executeall]
GO
