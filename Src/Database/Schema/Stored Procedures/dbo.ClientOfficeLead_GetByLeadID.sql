SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOfficeLead table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeLead_GetByLeadID]
(

	@LeadID int   
)
AS


				SELECT
					[ClientOfficeLeadID],
					[ClientID],
					[ClientOfficeID],
					[LeadID]
				FROM
					[dbo].[ClientOfficeLead] WITH (NOLOCK) 
				WHERE
										[LeadID] = @LeadID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeLead_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_GetByLeadID] TO [sp_executeall]
GO
