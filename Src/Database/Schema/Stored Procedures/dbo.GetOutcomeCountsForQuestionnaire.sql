SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetOutcomeCountsForQuestionnaire] @ClientQuestionnaireID int

AS

SELECT     ClientQuestionnaireID, COUNT(CustomerID) AS CustomerCount, OutcomeID, CustomerOutcomes.ClientID 
FROM         dbo.CustomerOutcomes
GROUP BY ClientQuestionnaireID, OutcomeID, CustomerOutcomes.ClientID 
HAVING      (ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCountsForQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeCountsForQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCountsForQuestionnaire] TO [sp_executeall]
GO
