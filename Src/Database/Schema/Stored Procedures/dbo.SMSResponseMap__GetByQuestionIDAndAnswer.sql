SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-04-2013
-- Description:	Gets a list of Responses by there from idents and the from answer#
-- Modified: PR 19/09/2013 removed like filter - so all the possible responses are returned and can be
--			 properly analysed within the SMS Processor application
-- Modified: PR 24/01/2014 Added the from answer back in
-- Modified: PR 27/02/2014 removed from answer check since its only required for the vodafone demo
-- =============================================
CREATE PROCEDURE [dbo].[SMSResponseMap__GetByQuestionIDAndAnswer]
(
	@SMSQuestionID INT,
	@FromAnswer VARCHAR(25)
)
AS


				SET ANSI_NULLS ON
				
				SELECT *
					
				FROM
					[dbo].[SMSResponseMap] WITH (NOLOCK) 
				WHERE
					[FromSMSQuestionID] = @SMSQuestionID 
					
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByQuestionIDAndAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap__GetByQuestionIDAndAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByQuestionIDAndAnswer] TO [sp_executeall]
GO
