SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-07-2013
-- Description:	Updates the table row with the message identity and the date and time the message was sent
-- Modified By PR 25-02-2014 Only return a table row id when it has a value
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateMobileTelephoneTable]

	@ClientID INT,
	@LeadID INT,
	@CaseID INT,
	@MobileTelephone VARCHAR(16),
	@MessageID VARCHAR(2000)

AS
BEGIN

	DECLARE @LeadTypeID INT
	DECLARE @MatterID INT

	SELECT @LeadTypeID = LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID
	
	--Selects the newest matter
	SELECT @MatterID = MatterID FROM Matter WITH (NOLOCK) WHERE CaseID=@CaseID ORDER BY MatterID DESC

	DECLARE @TableDetailFieldID INT
	DECLARE @MobileDetailFieldID INT
	DECLARE @AttemptedDetailFieldID INT
	DECLARE @DeliveredDetailFieldID INT
	DECLARE @DateTimeDetailFieldID INT
	DECLARE @MessageIdentDetailFieldID INT
	DECLARE @EventToApplyDetailFieldID INT

	SELECT @TableDetailFieldID = DetailFieldID, @MobileDetailFieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=650

	SELECT @AttemptedDetailFieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=651

	SELECT @DeliveredDetailFieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=652

	SELECT @DateTimeDetailFieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=653

	SELECT @MessageIdentDetailFieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=654
	
	SELECT @EventToApplyDetailFieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=655
	

	DECLARE @TableRowID INT
	SELECT @TableRowID=TableRowID FROM TableDetailValues WITH (NOLOCK) 
	WHERE 
			ClientID=@ClientID AND 
			MatterID=@MatterID AND 
			DetailFieldID = @MobileDetailFieldID AND
			DetailValue = @MobileTelephone

	IF @TableRowID IS NOT NULL
	BEGIN

		DECLARE @RightNow VARCHAR(2000)
		SELECT @RightNow=CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)

		EXEC _C00_SimpleValueIntoField @AttemptedDetailFieldID, 'true', @TableRowID
		EXEC _C00_SimpleValueIntoField @DateTimeDetailFieldID, @RightNow, @TableRowID
		EXEC _C00_SimpleValueIntoField @DeliveredDetailFieldID, 'false', @TableRowID
		EXEC _C00_SimpleValueIntoField @MessageIdentDetailFieldID, @MessageID, @TableRowID
	
		SELECT @TableRowID TableRowID
	
	END	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateMobileTelephoneTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateMobileTelephoneTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateMobileTelephoneTable] TO [sp_executeall]
GO
