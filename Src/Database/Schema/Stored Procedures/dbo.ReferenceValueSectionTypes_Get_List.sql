SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ReferenceValueSectionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueSectionTypes_Get_List]

AS


				
				SELECT
					[ReferenceValueSectionTypeID],
					[SectionTypeName]
				FROM
					[dbo].[ReferenceValueSectionTypes] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueSectionTypes_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Get_List] TO [sp_executeall]
GO
