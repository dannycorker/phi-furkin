SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DocumentType__GetByDocumentTypeIDBasic]
(
@DocumentTypeID int
)
AS
BEGIN
 
SELECT * FROM [dbo].[DocumentType] WITH (NOLOCK)
WHERE [DocumentTypeID] = @DocumentTypeID
 
SELECT @@ROWCOUNT
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByDocumentTypeIDBasic] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByDocumentTypeIDBasic] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByDocumentTypeIDBasic] TO [sp_executeall]
GO
