SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-03-24
-- Description:	Shreds the Claim XML into Aquarium Detail Field Values
--				ACE 2014-08-04 Updated to use thirdparty fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_IISSA_AddClaim]

	@ClientID INT, 
	@ClientPersonnelID INT, 
	@XMLVarchar VARCHAR(MAX) = NULL, 
	@XMLLogID INT = NULL, 
	@LeadDocumentID INT = NULL, 
	@Debug BIT = 0, 
	@LeadTypeID INT = 1262 /* Irish Insolvency System for C267 */

AS
BEGIN
	
	SET NOCOUNT ON;
	
	/*
		JWG 2014-03-24 Multi-pass approach to this new work.
		
		1) Quick demo.  This will use a sample of real field IDs for one client only.  
		Use a letter-in event against a dummy customer to create a document; 
		Use SAE to call this proc with the LeadDocumentID;
		Read the document blob and convert it into XML
		
		2) Full implementation.  This will map all fields using 3rd party mapping.
		Clients will call the web service, passing in the XML;
		The app will log the XML and then validate it against the XSD;
		The app will call this proc, passing in the XML and the log id
	
	*/
	
	/************************************************************************************************************************************************************/
	/*																																							*/
	/*																Variable Declarations																		*/
	/*																																							*/
	/************************************************************************************************************************************************************/
	DECLARE 
	
	/* General Details */
	@XML XML,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@LeadRef VARCHAR(100), 
	@LeadViewHistoryID INT,
	@TheDateTime DATETIME = dbo.fn_GetDate_Local(),
	@TheDateTimePlus1s DATETIME = DATEADD(SECOND, 1, dbo.fn_GetDate_Local()), 
	
	/* XML Personal */
	@Title VARCHAR(100),
	@TitleID INT = 0,
	@FirstName VARCHAR(100),
	@LastName VARCHAR(100),
	@DateOfBirth VARCHAR(100),
	@HomeTel VARCHAR(100),
	@MobileTel VARCHAR(100),
	@EmailAddress VARCHAR(100),
	@AddressLine1 VARCHAR(100),
	@AddressLine2 VARCHAR(100),
	@AddressLine3 VARCHAR(100),
	@Town VARCHAR(100),
	@County VARCHAR(100),
	@Postcode VARCHAR(100) = 'Ireland',
	@InterlockingLU VARCHAR(2000),
	@OwnRefNo VARCHAR(2000),
	@OwnDescription VARCHAR(2000),
	@CaseUNID VARCHAR(2000),
	@CaseCreated VARCHAR(2000),
	@CaseCompleted VARCHAR(2000),
	@CaseType VARCHAR(2000),
	@CaseDescription VARCHAR(2000),
	@PIPName VARCHAR(2000),
	@PIPRef VARCHAR(2000),
	@AppType VARCHAR(2000),
	@ApplicationTypeLU VARCHAR(2000) = '',
	@BirthCertName VARCHAR(2000),
	@Gender VARCHAR(2000),
	@GenderLU VARCHAR(2000),
	@PrevNames VARCHAR(2000),
	@PrevNamesLU VARCHAR(2000),
	@PPSN VARCHAR(2000),
	@Nationality VARCHAR(2000),	
	@BirthCountry VARCHAR(2000),	
	@BirthCountryLU VARCHAR(2000) = '',	
	@AQ_PIA_Q1 VARCHAR(2000),
	@AQ_PIA_Q2 VARCHAR(2000),
	@AQ_PIA_Q2A VARCHAR(2000),
	@AQ_PIA_Q3 VARCHAR(2000),
	@AQ_PIA_Q4 VARCHAR(2000),
	@AQ_PIA_Q5 VARCHAR(2000),
	@AQ_PIA_Q6 VARCHAR(2000),
	@INIT_Q1 VARCHAR(2000),
	@INIT_Q2 VARCHAR(2000),
	@INIT_Q3 VARCHAR(2000),
	@INIT_Q4 VARCHAR(2000),
	@INIT_Q5 VARCHAR(2000),
	@PersonalComment VARCHAR(2000), 
	@JurisdictionCounty VARCHAR(2000),
	@JurisdictionCourt VARCHAR(2000),
	@JurisdictionReason VARCHAR(2000),
	
	/* XML Employment */
	@EmploymentStatus VARCHAR(2000), 
	@EmploymentComment VARCHAR(2000), 
	@Occupation VARCHAR(2000),
	@EmployerName VARCHAR(2000),
	@LengthOfServiceRaw VARCHAR(2000), 
	@LengthOfServiceYears VARCHAR(2000) = '', 
	@LengthOfServiceMonths VARCHAR(2000) = '', 
	@EmployerAddressLine1 VARCHAR(100),
	@EmployerAddressLine2 VARCHAR(100),
	@EmployerAddressLine3 VARCHAR(100),
	@EmployerTown VARCHAR(100),
	@EmployerCounty VARCHAR(100),
	@EmployerCountry VARCHAR(100) = 'Ireland',
	
	/* XML RLE */
	@RLEOneOrTwoAdultsRaw VARCHAR(2000), 
	@RLEOneOrTwoAdultsLU VARCHAR(2000), 
	@RLEAnyDependentChildren VARCHAR(2000), 
	@RLEAnyDependentChildrenLU VARCHAR(2000), 
	@RLEDependentChildrenAges VARCHAR(2000), 
	@RLEOwnAMotor VARCHAR(2000), 
	@RLEOwnAMotorLU VARCHAR(2000), 
	@RLENeedAMotor VARCHAR(2000), 
	@RLENeedAMotorLU VARCHAR(2000), 
	@RLEAnyChildcareCosts VARCHAR(2000), 
	@RLEAnyChildcareCostsLU VARCHAR(2000), 
	@RLEChildcareCosts VARCHAR(2000), 
	@RLEMonthlyRent VARCHAR(2000), 
	@RLESpecialCircumstances VARCHAR(2000), 
	@RLESpecialCircumstancesAmount VARCHAR(2000), 
	@RLEComment VARCHAR(2000), 
	
	/*ThirdParty Mapping DetailFieldID's*/
	@ApplicationTypeFieldID INT,
	@CountryofBirthFieldID INT,
	@GenderFieldID INT,
	@NumberOfAdultsInhouseholdFieldID INT,
	@EnterNameofPIPFieldID INT,
	@PIPRefFieldID INT,
	@BirthCertNameFieldID INT,
	@PPSNFieldID1 INT,
	@PPSNFieldID2 INT,
	@NationalityFieldID INT,
	@EmploymentStatusFieldID INT,
	@PrevNamesLUFieldID INT,
	@PrevNamesFieldID INT,
	@AQ_PIA_Q1FieldID INT,
	@AQ_PIA_Q2FieldID INT,
	@AQ_PIA_Q3FieldID INT,
	@AQ_PIA_Q4FieldID INT,
	@AQ_PIA_Q5FieldID INT,
	@AQ_PIA_Q6FieldID INT,
	@INIT_Q1FieldID INT,
	@INIT_Q2FieldID INT,
	@INIT_Q3FieldID INT,
	@INIT_Q4FieldID INT,
	@INIT_Q5FieldID INT,
	@PersonalCommentFieldID INT,
	@JurisdictionCountyFieldID INT,
	@JurisdictionCourtFieldID INT,
	@JurisdictionReasonFieldID INT,
	@EmployerCountryFieldID INT,
	@EmployerAddressLine1FieldID INT,
	@EmployerAddressLine2FieldID INT,
	@EmployerAddressLine3FieldID INT,
	@EmployerTownFieldID INT,
	@EmployerCountyFieldID INT,
	@LengthOfServiceYearsFieldID INT,
	@LengthOfServiceMonthsFieldID INT,
	@RLEAnyDependentChildrenLUFieldID INT,
	@RLEDependentChildrenAgesFieldID INT,
	@RLEOwnAMotorLUFieldID INT,
	@RLENeedAMotorLUFieldID INT,
	@RLEAnyChildcareCostsLUFieldID INT,
	@RLEChildcareCostsFieldID INT,
	@RLEMonthlyRentFieldID INT,
	@RLESpecialCircumstancesAmountFieldID INT,
	@RLECommentFieldID INT,
	@CaseCreatedFieldID INT,
	@CaseCompletedFieldID INT,
	@InterlockingLUFieldID INT,
	@OwnRefNoFieldID INT,
	@OwnDescriptionFieldID INT,

	@DetailFieldPageID INT,

	@EnoughVariablesAlready bit=1

	/* Tables for repeating XML nodes */
	DECLARE @InsertedTableRows TABLE (UNID INT, TableRowID INT)

	DECLARE @Fields TABLE (DetailFieldID INT)

	SELECT @ApplicationTypeFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 903
	AND tpfm.ClientID = @ClientID

	SELECT @CountryofBirthFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 904
	AND tpfm.ClientID = @ClientID

	SELECT @GenderFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 905
	AND tpfm.ClientID = @ClientID
	
	SELECT @NumberOfAdultsInhouseholdFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 906
	AND tpfm.ClientID = @ClientID
	
	SELECT @EnterNameofPIPFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 907
	AND tpfm.ClientID = @ClientID

	SELECT @PIPRefFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 908
	AND tpfm.ClientID = @ClientID

	SELECT @BirthCertNameFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 909
	AND tpfm.ClientID = @ClientID

	SELECT @PPSNFieldID1 = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 910
	AND tpfm.ClientID = @ClientID

	SELECT @PPSNFieldID2 = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 911
	AND tpfm.ClientID = @ClientID
	
	SELECT @NationalityFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 912
	AND tpfm.ClientID = @ClientID

	SELECT @EmploymentStatusFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 913
	AND tpfm.ClientID = @ClientID
	
	SELECT @PrevNamesLUFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 914
	AND tpfm.ClientID = @ClientID

	SELECT @PrevNamesFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 915
	AND tpfm.ClientID = @ClientID

	SELECT @AQ_PIA_Q1FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 916
	AND tpfm.ClientID = @ClientID

	SELECT @AQ_PIA_Q2FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 917
	AND tpfm.ClientID = @ClientID

	SELECT @AQ_PIA_Q3FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 918
	AND tpfm.ClientID = @ClientID

	SELECT @AQ_PIA_Q4FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 919
	AND tpfm.ClientID = @ClientID

	SELECT @AQ_PIA_Q5FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 920
	AND tpfm.ClientID = @ClientID

	SELECT @AQ_PIA_Q6FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 921
	AND tpfm.ClientID = @ClientID

	SELECT @INIT_Q1FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 922
	AND tpfm.ClientID = @ClientID

	SELECT @INIT_Q2FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 923
	AND tpfm.ClientID = @ClientID

	SELECT @INIT_Q3FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 924
	AND tpfm.ClientID = @ClientID

	SELECT @INIT_Q4FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 925
	AND tpfm.ClientID = @ClientID

	SELECT @INIT_Q5FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 926
	AND tpfm.ClientID = @ClientID
	
	SELECT @PersonalCommentFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 927
	AND tpfm.ClientID = @ClientID
	
	SELECT @JurisdictionCountyFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 928
	AND tpfm.ClientID = @ClientID

	SELECT @JurisdictionCourtFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 929
	AND tpfm.ClientID = @ClientID
	
	SELECT @JurisdictionReasonFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 930
	AND tpfm.ClientID = @ClientID
	
	SELECT @EmployerCountryFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 931
	AND tpfm.ClientID = @ClientID
	
	SELECT @EmployerAddressLine1FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 932
	AND tpfm.ClientID = @ClientID

	SELECT @EmployerAddressLine2FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 933
	AND tpfm.ClientID = @ClientID

	SELECT @EmployerAddressLine3FieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 934
	AND tpfm.ClientID = @ClientID

	SELECT @EmployerTownFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ThirdPartyFieldID = 935
	AND tpfm.ClientID = @ClientID
	
	SELECT @EmployerCountyFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 936 
	AND tpfm.ClientID = @ClientID 
	
	SELECT @LengthOfServiceYearsFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 937
	AND tpfm.ClientID = @ClientID 
	
	SELECT @LengthOfServiceMonthsFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 938
	AND tpfm.ClientID = @ClientID 
	
	SELECT @RLEAnyDependentChildrenLUFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 939
	AND tpfm.ClientID = @ClientID 
	
	SELECT @RLEDependentChildrenAgesFieldID  = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 940
	AND tpfm.ClientID = @ClientID 

	SELECT @RLEOwnAMotorLUFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 941
	AND tpfm.ClientID = @ClientID 

	SELECT @RLENeedAMotorLUFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 942
	AND tpfm.ClientID = @ClientID  

	SELECT @RLEAnyChildcareCostsLUFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 943
	AND tpfm.ClientID = @ClientID  

	SELECT @RLEChildcareCostsFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 944
	AND tpfm.ClientID = @ClientID
	
	SELECT @RLEMonthlyRentFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 945
	AND tpfm.ClientID = @ClientID
	
	SELECT @RLESpecialCircumstancesAmountFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 946
	AND tpfm.ClientID = @ClientID
	
	SELECT @RLECommentFieldID  = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 947
	AND tpfm.ClientID = @ClientID

	SELECT @CaseCreatedFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 948
	AND tpfm.ClientID = @ClientID
	
	SELECT @CaseCompletedFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 949
	AND tpfm.ClientID = @ClientID
	
	SELECT @InterlockingLUFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 950
	AND tpfm.ClientID = @ClientID
	
	SELECT @OwnRefNoFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 951
	AND tpfm.ClientID = @ClientID

	SELECT @OwnDescriptionFieldID = tpfm.DetailFieldID 
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE tpfm.ThirdPartyFieldID = 952
	AND tpfm.ClientID = @ClientID

	/************************************************************************************************************************************************************/
	/*																																							*/
	/*																Basic XML Parsing																			*/
	/*																																							*/
	/************************************************************************************************************************************************************/
	IF @XMLVarchar IS NOT NULL
	BEGIN
		/* Simply convert the varchar to XML so that we can query it */
		SELECT @XML = CAST(@XMLVarchar AS XML)
	END
	ELSE
	BEGIN
		IF @LeadDocumentID > 0 
		BEGIN
			/* Get the XML so that we can query it */
			SELECT @XML = CAST(CAST(v.DocumentBLOB AS VARCHAR(MAX)) AS XML) 
			FROM dbo.vLeadDocumentList v 
			WHERE v.LeadDocumentID = @LeadDocumentID 
		END
		ELSE
		BEGIN
			RAISERROR ('You must pass in the XML or the LeadDocumentID to read', 16, 1)
			RETURN
		END
	END
	
	/* Debug */
	IF @Debug = 1
	BEGIN
		SELECT @XML
	END
	

	SELECT 
	@LeadRef = n.c.value('(CaseDetails/CaseRefNo)[1]', 'varchar(2000)'), --CaseRefNo,
	@OwnRefNo = n.c.value('(CaseDetails/OwnRefNo)[1]', 'varchar(2000)'), --OwnRefNo,
	@CaseUNID = n.c.value('(CaseDetails/CaseUNID)[1]', 'varchar(2000)'), --CaseUNID,
	@PIPName = n.c.value('(CaseDetails/PIPName)[1]', 'varchar(2000)'), --PIPName,
	@PIPRef = n.c.value('(CaseDetails/PIPRef)[1]', 'varchar(2000)'), --PIPRef,
	@CaseType = n.c.value('(CaseDetails/CaseType)[1]', 'varchar(2000)'), --CaseType,
	@AppType = n.c.value('(CaseDetails/AppType)[1]', 'varchar(2000)'), --AppType,
	@CaseDescription = n.c.value('(CaseDetails/CaseDescription)[1]', 'varchar(2000)'), --CaseDescription,
	@OwnDescription = n.c.value('(AppForm/OwnDescription)[1]', 'varchar(2000)'), --OwnDescription,
	@AddressLine1 = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Line1)[1]', 'varchar(2000)'), 100), --Line1,
	@AddressLine2 = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Line2)[1]', 'varchar(2000)'), 100), --Line2,
	@AddressLine3 = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Line3)[1]', 'varchar(2000)'), 100), --Line3,
	@Town = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Town)[1]', 'varchar(2000)'), 100), --Town,
	@County = LEFT(n.c.value('(AppForm/PPRAddress/Irish/County)[1]', 'varchar(2000)'), 100), --County,
	@AQ_PIA_Q1 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q1)[1]', 'varchar(2000)'), --AQ_PIA_Q1,
	@AQ_PIA_Q2 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q2)[1]', 'varchar(2000)'), --AQ_PIA_Q2,
	@AQ_PIA_Q2A = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q2A)[1]', 'varchar(2000)'), --AQ_PIA_Q2A,
	@AQ_PIA_Q3 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q3)[1]', 'varchar(2000)'), --AQ_PIA_Q3,
	@AQ_PIA_Q4 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q4)[1]', 'varchar(2000)'), --AQ_PIA_Q4,
	@AQ_PIA_Q5 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q5)[1]', 'varchar(2000)'), --AQ_PIA_Q5,
	@AQ_PIA_Q6 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q6)[1]', 'varchar(2000)'), --AQ_PIA_Q6,
	@Title = LEFT(n.c.value('(AppForm/Debtor/Personal/Title)[1]', 'varchar(2000)'), 100), --Title,
	@FirstName = LEFT(n.c.value('(AppForm/Debtor/Personal/FirstName)[1]', 'varchar(2000)'), 100), --FirstName,
	@LastName = LEFT(n.c.value('(AppForm/Debtor/Personal/LastName)[1]', 'varchar(2000)'), 100), --LastName,
	@BirthCertName = n.c.value('(AppForm/Debtor/Personal/BirthCertName)[1]', 'varchar(2000)'), --BirthCertName,
	@PrevNames = n.c.value('(AppForm/Debtor/Personal/PreviouslyKnownNames)[1]', 'varchar(2000)'), --PreviouslyKnownNames,
	@PPSN = n.c.value('(AppForm/Debtor/Personal/PPSN)[1]', 'varchar(2000)'), --PPSN,
	@Gender = n.c.value('(AppForm/Debtor/Personal/Gender)[1]', 'varchar(2000)'), --Gender,
	@DateOfBirth = n.c.value('(AppForm/Debtor/Personal/BirthDate)[1]', 'varchar(2000)'), --BirthDate,
	@BirthCountry = n.c.value('(AppForm/Debtor/Personal/BirthCountry)[1]', 'varchar(2000)'), --BirthCountry,
	@Nationality = n.c.value('(AppForm/Debtor/Personal/Nationality)[1]', 'varchar(2000)'), --Nationality,
	--n.c.value('(AppForm/Debtor/Personal/MaritalStatus)[1]', 'varchar(2000)'), --MaritalStatus,
	@HomeTel = LEFT(n.c.value('(AppForm/Debtor/Personal/Phone)[1]', 'varchar(2000)'), 100), --Phone,
	@MobileTel = LEFT(n.c.value('(AppForm/Debtor/Personal/Mobile)[1]', 'varchar(2000)'), 100), --Mobile,
	@EmailAddress = LEFT(n.c.value('(AppForm/Debtor/Personal/Email)[1]', 'varchar(2000)'), 100), --Email,
	@PersonalComment = n.c.value('(AppForm/Debtor/Personal/Comment)[1]', 'varchar(2000)'), --Comment,
	@EmploymentStatus = n.c.value('(AppForm/Debtor/Employment/Employed/EmploymentStatus)[1]', 'varchar(2000)'), --EmploymentStatus,
	@Occupation = n.c.value('(AppForm/Debtor/Employment/Employed/Occupation)[1]', 'varchar(2000)'), --Occupation,
	@EmployerName = n.c.value('(AppForm/Debtor/Employment/Employed/EmployerName)[1]', 'varchar(2000)'), --EmployerName,
	@EmployerAddressLine1 = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Line1)[1]', 'varchar(2000)'), 100), --Line1,
	@EmployerAddressLine2 = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Line2)[1]', 'varchar(2000)'), 100), --Line2,
	@EmployerAddressLine3 = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Line3)[1]', 'varchar(2000)'), 100), --Line3,
	@EmployerTown = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Town)[1]', 'varchar(2000)'), 100), --Town,
	@EmployerCounty = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/County)[1]', 'varchar(2000)'), 100), --County,
	@LengthOfServiceRaw = n.c.value('(AppForm/Debtor/Employment/Employed/LengthOfService)[1]', 'varchar(2000)'), --LengthOfService,
	@EmploymentComment = n.c.value('(AppForm/Debtor/Employment/Employed/Comment)[1]', 'varchar(2000)'), --Comment,
	@INIT_Q1 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q1)[1]', 'varchar(2000)'), --II_PIA_Q1 1. Has debtor received advice from a PIP?,
	@INIT_Q2 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q2)[1]', 'varchar(2000)'), --II_PIA_Q2 2. Has the PIP confirmed the advice in writing?,
	@INIT_Q3 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q3)[1]', 'varchar(2000)'), --II_PIA_Q3 3: Has the debtor instructed the PIP in writing to make a proposal?,
	@INIT_Q4 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q4)[1]', 'varchar(2000)'), --II_PIA_Q4 4: Does the debtor agree to receiving notices electronically from the ISI?,
	@INIT_Q5 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q5)[1]', 'varchar(2000)'), --II_PIA_Q5 5: Does the debtor agree to receiving notices electronically from the court?,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q1)[1]', 'varchar(2000)'), --IS_PIA_Q1,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q2)[1]', 'varchar(2000)'), --IS_PIA_Q2,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q3)[1]', 'varchar(2000)'), --IS_PIA_Q3,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q4)[1]', 'varchar(2000)'), --IS_PIA_Q4,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q5)[1]', 'varchar(2000)'), --IS_PIA_Q5,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q1)[1]', 'varchar(2000)'), --PI_PIA_Q1,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q2)[1]', 'varchar(2000)'), --PI_PIA_Q2,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q3)[1]', 'varchar(2000)'), --PI_PIA_Q3,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q4)[1]', 'varchar(2000)'), --PI_PIA_Q4,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q6)[1]', 'varchar(2000)'), --PI_PIA_Q6,
	--n.c.value('(AppForm/Debtor/OtherEligibilityCriteria/OC_Q1)[1]', 'varchar(2000)'), --OC_Q1,
	--n.c.value('(AppForm/Debtor/OtherEligibilityCriteria/OC_Q1A)[1]', 'varchar(2000)'), --OC_Q1A,
	--n.c.value('(AppForm/Debtor/OtherEligibilityCriteria/OC_Q2)[1]', 'varchar(2000)'), --OC_Q2,
	@RLEOneOrTwoAdultsRaw = n.c.value('(AppForm/RLE/Details/RLE_Details_Q1)[1]', 'varchar(2000)'), --RLE_Details_Q1, one or two adults?
	@RLEAnyDependentChildren = n.c.value('(AppForm/RLE/Details/RLE_Details_Q2)[1]', 'varchar(2000)'), --RLE_Details_Q2, dependent children Y/N & 2A child age category/ies
	--	@RLEDependentChildrenAges = ?,
	@RLEOwnAMotor = n.c.value('(AppForm/RLE/Details/RLE_Details_Q3)[1]', 'varchar(2000)'), --RLE_Details_Q3, own a motor vehicle & 3A do you require it?
	@RLENeedAMotor = n.c.value('(AppForm/RLE/Details/RLE_Details_Q3_1)[1]', 'varchar(2000)'), --RLE_Details_Q3, own a motor vehicle & 3A do you require it?
	@RLEAnyChildcareCosts = n.c.value('(AppForm/RLE/Details/RLE_Details_Q4)[1]', 'varchar(2000)'), --RLE_Details_Q4, any childcare costs & 4A what is the amount?
	@RLEChildcareCosts = n.c.value('(AppForm/RLE/Details/RLE_Details_Q4_1)[1]', 'varchar(2000)'), --RLE_Details_Q4, any childcare costs & 4A what is the amount?
	@RLEMonthlyRent = n.c.value('(AppForm/RLE/Details/RLE_Details_Q5)[1]', 'varchar(2000)'), --RLE_Details_Q5, monthly rent/mortgage amount
	@RLESpecialCircumstances = n.c.value('(AppForm/RLE/Details/RLE_Details_Q6)[1]', 'varchar(2000)'), --RLE_Details_Q6, special circumstance costs
	@RLESpecialCircumstancesAmount = n.c.value('(AppForm/RLE/Details/RLE_Details_Q6_1)[1]', 'varchar(2000)'), --RLE_Details_Q6, special circumstance costs
	@RLEComment = n.c.value('(AppForm/RLE/Details/Comment)[1]', 'varchar(2000)'), --Comment,
	
	@JurisdictionCounty = n.c.value('(AppForm/Jurisdiction/County)[1]', 'varchar(2000)'), --County,
	@JurisdictionCourt = n.c.value('(AppForm/Jurisdiction/Court)[1]', 'varchar(2000)'), --Court,
	@JurisdictionReason = n.c.value('(AppForm/Jurisdiction/Reason)[1]', 'varchar(2000)'), --Reason,
	@CaseCreated = n.c.value('(KeyDates/CaseCreated)[1]', 'varchar(2000)'), --CaseCreated,
	@CaseCompleted = n.c.value('(KeyDates/CaseCompleted)[1]', 'varchar(2000)') --CaseCompleted
	FROM @Xml.nodes('ISICase') n(c) 


	/************************************************************************************************************************************************************/
	/*																																							*/
	/*																Create Cust/Lead/Case/Matter																*/
	/*																																							*/
	/************************************************************************************************************************************************************/

	/* Set Aquarium TitleID */
	SELECT @TitleID = t.TitleID 
	FROM dbo.Titles t WITH (NOLOCK) 
	WHERE t.Title = @Title 
	
	/* Merge address lines if necessary */
	IF @AddressLine3 > ''
	BEGIN
		SELECT @AddressLine2 += CASE WHEN @AddressLine2 = '' THEN '' ELSE ', ' END + @AddressLine3
	END
	
	/* Format dates so that a COMPUTER can read them */
	/* TODO */
	SET @DateOfBirth = '1990-03-17'
	
	/* Create the new Customer, Lead, Case, Matter */
	EXEC @CustomerID = dbo._C00_CreateNewCustomerFull @ClientID, @LeadTypeID, @ClientPersonnelID, @TitleID, 0, @FirstName, '', @LastName, @EmailAddress, @MobileTel, NULL, @HomeTel, NULL, @MobileTel, NULL, '', NULL, '', NULL, @AddressLine1, @AddressLine2, @Town, @County, @Postcode, '', 0, NULL, 2, NULL, 0, '', @Occupation, @EmployerName, NULL, NULL, NULL, NULL, @DateOfBirth, NULL, NULL, NULL, NULL, NULL, @CaseUNID, NULL, @TheDateTime, NULL, NULL, NULL, ''
	
	IF @CustomerID > 0
	BEGIN
		
		/* Get all the other new IDs to send back to the app */
		SELECT  @LeadID = m.LeadID, 
				@CaseID = m.CaseID, 
				@MatterID = m.MatterID 
		FROM dbo.Matter m WITH (NOLOCK)
		WHERE m.CustomerID = @CustomerID 
		
		/* Add the process start event as it isnt added by the _C00_Create Customer SP */
		EXEC dbo._C00_AddProcessStart @CaseID, @ClientPersonnelID 
		
		/*
			Populate the Aquarium fields at the right levels (Customer/Matter)
		*/
		UPDATE TOP (1) dbo.Lead 
		SET LeadRef = @LeadRef 
		WHERE LeadID = @LeadID 
		
		/********************************************************************************************************************************************************/
		/*																																						*/
		/*															Encode ISI strings into Aquarium LookupListIDs												*/
		/*																																						*/
		/********************************************************************************************************************************************************/
		/*Set up client 0 lookups first*/
		SELECT @InterlockingLU = CASE @AppType WHEN 'Joint' THEN '5144' ELSE '5145' END /* Yes No */
		SELECT @PrevNamesLU = CASE @PrevNames WHEN '' THEN '5145' ELSE '5144' END /* No Yes */
		SELECT @RLEAnyDependentChildrenLU = CASE @RLEAnyDependentChildren WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		SELECT @RLEOwnAMotorLU = CASE @RLEOwnAMotor WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		SELECT @RLENeedAMotorLU = CASE @RLENeedAMotor WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		SELECT @RLEAnyChildcareCostsLU = CASE @RLEAnyChildcareCosts WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */

		/*Use the lookups from the detail field based on the application type*/
		SELECT @ApplicationTypeLU = luli.LookupListItemID
		FROM DetailFields df WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID
		WHERE df.DetailFieldID = @ApplicationTypeFieldID
		AND ((@AppType = 'Individual' AND luli.ItemValue = 'Single') OR (@AppType = 'Joint' AND luli.ItemValue = 'Interlocking'))
		
		SELECT @BirthCountryLU = luli.LookupListItemID
		FROM DetailFields df WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID
		WHERE df.DetailFieldID = @CountryofBirthFieldID
		AND luli.ItemValue = @BirthCountry

		SELECT @GenderLU = luli.LookupListItemID /* Male/Female */
		FROM DetailFields df WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID
		WHERE df.DetailFieldID = @GenderFieldID
		AND luli.ItemValue = @Gender

		/* Example: "P1Y2M" which is wierd, so turn that into "1Y2" and then get chars before/after the "Y" as years and months respectively */
		SELECT @LengthOfServiceRaw = REPLACE(REPLACE(@LengthOfServiceRaw, 'P', ''), 'M', '')
		SELECT @LengthOfServiceYears = dbo.fnGetCharsBeforeOrAfterSeparator(@LengthOfServiceRaw, ',', 1, 1), 
		@LengthOfServiceMonths = dbo.fnGetCharsBeforeOrAfterSeparator(@LengthOfServiceRaw, ',', 1, 0)

		SELECT @RLEOneOrTwoAdultsLU = luli.LookupListItemID
		FROM DetailFields df
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID
		WHERE df.DetailFieldID = @NumberOfAdultsInhouseholdFieldID
		AND luli.ItemValue = CASE WHEN @RLEOneOrTwoAdultsRaw = '1' OR @RLEOneOrTwoAdultsRaw LIKE 'One%' THEN '1' WHEN @RLEOneOrTwoAdultsRaw = '2' OR @RLEOneOrTwoAdultsRaw LIKE 'Two%' THEN '2' ELSE '' END 
		
		/* Create CustomerDetailValues */
		EXEC dbo._C00_SimpleValueIntoField @EnterNameofPIPFieldID, @PIPName, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @PIPRefFieldID, @PIPRef, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @BirthCertNameFieldID, @BirthCertName, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @PPSNFieldID1, @PPSN, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @PPSNFieldID2, @PPSN, @CustomerID, @ClientPersonnelID /* Yes, deliberate repeated use of @PPSN */
		EXEC dbo._C00_SimpleValueIntoField @GenderFieldID, @GenderLU, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @NationalityFieldID, @Nationality, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @CountryofBirthFieldID, @BirthCountryLU, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @EmploymentStatusFieldID, @EmploymentStatus, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @PrevNamesLUFieldID, @PrevNamesLU, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @PrevNamesFieldID, @PrevNames, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @AQ_PIA_Q1FieldID, @AQ_PIA_Q1, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @AQ_PIA_Q2FieldID, @AQ_PIA_Q2, @CustomerID, @ClientPersonnelID
		--EXEC dbo._C00_SimpleValueIntoField TBA, @AQ_PIA_Q2A, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @AQ_PIA_Q3FieldID, @AQ_PIA_Q3, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @AQ_PIA_Q4FieldID, @AQ_PIA_Q4, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @AQ_PIA_Q5FieldID, @AQ_PIA_Q5, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @AQ_PIA_Q6FieldID, @AQ_PIA_Q6, @CustomerID, @ClientPersonnelID
		
		EXEC dbo._C00_SimpleValueIntoField @INIT_Q1FieldID, @INIT_Q1, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @INIT_Q2FieldID, @INIT_Q2, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @INIT_Q3FieldID, @INIT_Q3, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @INIT_Q4FieldID, @INIT_Q4, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @INIT_Q5FieldID, @INIT_Q5, @CustomerID, @ClientPersonnelID
		
		
		EXEC dbo._C00_SimpleValueIntoField @PersonalCommentFieldID, @PersonalComment, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @JurisdictionCountyFieldID, @JurisdictionCounty, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @JurisdictionCourtFieldID, @JurisdictionCourt, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @JurisdictionReasonFieldID, @JurisdictionReason, @CustomerID, @ClientPersonnelID
		
		/* Employment */
		SET @EmployerCountry = 12851 /* Ireland */
		EXEC dbo._C00_SimpleValueIntoField @EmployerCountryFieldID, @EmployerCountry, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @EmployerAddressLine1FieldID, @EmployerAddressLine1, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @EmployerAddressLine2FieldID, @EmployerAddressLine2, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @EmployerAddressLine3FieldID, @EmployerAddressLine3, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @EmployerTownFieldID, @EmployerTown, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @EmployerCountyFieldID, @EmployerCounty, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @LengthOfServiceYearsFieldID, @LengthOfServiceYears, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @LengthOfServiceMonthsFieldID, @LengthOfServiceMonths, @CustomerID, @ClientPersonnelID
		
		/* RLE */
		EXEC dbo._C00_SimpleValueIntoField @NumberOfAdultsInhouseholdFieldID, @RLEOneOrTwoAdultsLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLEAnyDependentChildrenLUFieldID, @RLEAnyDependentChildrenLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLEDependentChildrenAgesFieldID, @RLEDependentChildrenAges, @CustomerID, @ClientPersonnelID /* TODO: not in XML, populate our table? LU? */
		EXEC dbo._C00_SimpleValueIntoField @RLEOwnAMotorLUFieldID, @RLEOwnAMotorLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLENeedAMotorLUFieldID, @RLENeedAMotorLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLEAnyChildcareCostsLUFieldID, @RLEAnyChildcareCostsLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLEChildcareCostsFieldID, @RLEChildcareCosts, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLEMonthlyRentFieldID, @RLEMonthlyRent, @CustomerID, @ClientPersonnelID
		--EXEC dbo._C00_SimpleValueIntoField , @RLESpecialCircumstances, @CustomerID, @ClientPersonnelID /* TODO: LU and new field required */
		EXEC dbo._C00_SimpleValueIntoField @RLESpecialCircumstancesAmountFieldID, @RLESpecialCircumstancesAmount, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField @RLECommentFieldID, @RLEComment, @CustomerID, @ClientPersonnelID
		
		EXEC dbo._C00_SimpleValueIntoField @CaseCreatedFieldID, @CaseCreated, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @CaseCompletedFieldID, @CaseCompleted, @CustomerID, @ClientPersonnelID
		
		
		/* Create MatterDetailValues */
		EXEC dbo._C00_SimpleValueIntoField @ApplicationTypeFieldID, @ApplicationTypeLU, @MatterID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @InterlockingLUFieldID, @InterlockingLU, @MatterID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @OwnRefNoFieldID, @OwnRefNo, @MatterID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField @OwnDescriptionFieldID, @OwnDescription, @MatterID, @ClientPersonnelID
		--@CaseType
		--@CaseDescription
		
		/********************************************************************************************************************************************************/
		/*																																						*/
		/*															Populate tables from repeating nodes														*/
		/*																																						*/
		/********************************************************************************************************************************************************/
	
		/********************************************************************************************************************************************************/
		/*															RLE Dependent Children																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblChildren TABLE (
			UNID INT IDENTITY(1,1), 
			Category VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		/* Insert the childred into a table variable*/
		INSERT INTO @TblChildren (
			Category, 
			Comment
		)
		SELECT 
		n.c.value('(Category)[1]', 'varchar(2000)') AS Category,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//RLE/Child') n(c);

		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*InvestmentProperty*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 953)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 953), @DetailFieldPageID, c.UNID 
			FROM @TblChildren c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblChildren c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 
			
			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 954)) /* Category */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 955)) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 954) THEN c.Category /* TODO: LU? */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 955) THEN c.Comment
									ELSE ''
									END
			FROM @TblChildren c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields
		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset PrincipalPrivateResidence															*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetPPR TABLE (
			UNID INT IDENTITY(1,1), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetPPR (
			AddressLine1,
			AddressLine2,
			AddressLine3,
			AddressTown,
			AddressCounty,
			OriginalCost,
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			DebtorOwnership, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(AssetDetail/OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(AssetDetail/PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(AssetDetail/PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(AssetDetail/CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(AssetDetail/DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment 
		FROM @xml.nodes('//PFS/Asset/PrincipalPrivateResidence') n(c); 

		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*InvestmentProperty*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 956)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 956), @DetailFieldPageID, c.UNID 
			FROM @TblAssetPPR c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetPPR c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 957)) /* Line 1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 958)) /* Line 2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 959)) /* Line 3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 960)) /* Town */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 961)) /* TODO: this is the ZIP field but should be County (we only have a ZIP field and Country, not County) */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 962)) /* Original Cost  */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 963)) /* Purchase Date Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 964)) /* Purchase Date Year */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 965)) /* Current Market Value */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 966)) /* Debtor Ownership (we have ownership % and interest value columns)*/
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 967)) /* Asset Unique ID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 968)) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 957) THEN c.AddressLine1 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 958) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 959) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 960) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 961) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 962) THEN c.OriginalCost
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 963) THEN CAST((SELECT luli.LookupListItemID FROM LookupListItems luli WITH (NOLOCK) INNER JOIN DetailFields df WITH (NOLOCK) ON df.LookupListID = luli.LookupListID AND df.DetailFieldID = f.DetailFieldID WHERE luli.ItemValue = LEFT(DateName( month , DateAdd( month , cast(c.PurchaseMonth as INT) , -1 ) ),3) ) AS VARCHAR)
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 964) THEN c.PurchaseYear
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 965) THEN c.CurrentMarketValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 966) THEN c.DebtorOwnership
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 967) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 968) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetPPR c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END

		
		/********************************************************************************************************************************************************/
		/*															PFS Asset InvestmentProperty																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetInvestmentProperty TABLE (
			UNID INT IDENTITY(1,1), 
			PType VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Title VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			MonthlyIncome VARCHAR(2000), 
			MonthlyExpenditure VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetInvestmentProperty (
			PType, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Title, 
			OriginalCost, 
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			DebtorOwnership, 
			MonthlyIncome, 
			MonthlyExpenditure, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Type)[1]', 'varchar(2000)') AS Type,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Title)[1]', 'varchar(2000)') AS Title,
		n.c.value('(AssetDetail/OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(AssetDetail/PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(AssetDetail/PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(AssetDetail/CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(AssetDetail/DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetDetail/MonthlyIncome)[1]', 'varchar(2000)') AS MonthlyIncome,
		n.c.value('(MonthlyExpenditure)[1]', 'varchar(2000)') AS MonthlyExpenditure,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/InvestmentProperty') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*InvestmentProperty*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 969)
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 969), @DetailFieldPageID, c.UNID 
			FROM @TblAssetInvestmentProperty c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetInvestmentProperty c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 970)) /* PType */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 971)) /* Line1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 972)) /* Line2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 973)) /* Line3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 974)) /* Town */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 975)) /* County */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 976)) /* Title */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 977)) /* OriginalCost */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 978)) /* PurchaseMonth */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 979)) /* PurchaseYear */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 980)) /* CurrentMarketValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 981)) /* DebtorOwnership */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 982)) /* MonthlyIncome */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 983)) /* MonthlyExpenditure */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 984)) /* Asset Unique ID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 985)) /* Comment */
			
			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 970) THEN c.PType
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 971) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 972) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 973) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 974) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 975) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 976) THEN c.Title
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 977) THEN c.OriginalCost
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 978) THEN CAST((SELECT luli.LookupListItemID FROM LookupListItems luli WITH (NOLOCK) INNER JOIN DetailFields df WITH (NOLOCK) ON df.LookupListID = luli.LookupListID AND df.DetailFieldID = f.DetailFieldID WHERE luli.ItemValue = LEFT(DateName( month , DateAdd( month , cast(c.PurchaseMonth as INT) , -1 ) ),3) ) AS VARCHAR)
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 979) THEN c.PurchaseYear
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 980) THEN c.CurrentMarketValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 981) THEN c.DebtorOwnership
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 982) THEN c.MonthlyIncome
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 983) THEN c.MonthlyExpenditure
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 984) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 985) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetInvestmentProperty c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields
			
		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset PlantEquipmentTools																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetPlantEquipmentTools TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetPlantEquipmentTools (
			PDescription, 
			OriginalCost, 
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			DebtorOwnership, 
			IsAssetInTheState, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(InvestmentDetails/OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(InvestmentDetails/PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(InvestmentDetails/PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(InvestmentDetails/CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(InvestmentDetails/DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/PlantEquipmentTools') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*PlantEquipmentTools*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 986)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 986), @DetailFieldPageID, c.UNID 
			FROM @TblAssetPlantEquipmentTools c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetPlantEquipmentTools c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 987)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 988)) /* OriginalCost */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 989)) /* PurchaseMonth */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 990)) /* PurchaseYear */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 991)) /* CurrentMarketValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 992)) /* DebtorOwnership */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 993)) /* IsAssetInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 994)) /* Asset Unique ID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 995)) /* Comment */
			
			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 987) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 988) THEN c.OriginalCost
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 989) THEN CAST((SELECT luli.LookupListItemID FROM LookupListItems luli WITH (NOLOCK) INNER JOIN DetailFields df WITH (NOLOCK) ON df.LookupListID = luli.LookupListID AND df.DetailFieldID = f.DetailFieldID WHERE luli.ItemValue = LEFT(DateName( month , DateAdd( month , cast (c.PurchaseMonth as INT) , -1 ) ),3) ) AS VARCHAR)
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 990) THEN c.PurchaseYear
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 991) THEN c.CurrentMarketValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 992) THEN c.DebtorOwnership
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 993) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 994) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 995) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetPlantEquipmentTools c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields
			
		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset Vehicle																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetVehicle TABLE (
			UNID INT IDENTITY(1,1), 
			Make VARCHAR(2000), 
			Model VARCHAR(2000), 
			Year VARCHAR(2000), 
			RegistrationNumber VARCHAR(2000), 
			Mileage VARCHAR(2000), 
			NeedForVehicle VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			SubjectToFinance VARCHAR(2000), 
			AdaptedForDisabledUse VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetVehicle (
			Make,
			Model,
			Year,
			RegistrationNumber,
			Mileage,
			NeedForVehicle,
			OriginalCost,
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			SubjectToFinance, 
			AdaptedForDisabledUse, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Make)[1]', 'varchar(2000)') AS Make,
		n.c.value('(Model)[1]', 'varchar(2000)') AS Model,
		n.c.value('(Year)[1]', 'varchar(2000)') AS Year,
		n.c.value('(RegistrationNumber)[1]', 'varchar(2000)') AS RegistrationNumber,
		n.c.value('(Milage)[1]', 'varchar(2000)') AS Milage,
		n.c.value('(NeedForVehicle)[1]', 'varchar(2000)') AS NeedForVehicle,
		n.c.value('(OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(SubjectToFinance)[1]', 'varchar(2000)') AS SubjectToFinance,
		n.c.value('(AdaptedForDisabledUse)[1]', 'varchar(2000)') AS AdaptedForDisabledUse,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/Vehicle') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Vehicle*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 996)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 996), @DetailFieldPageID, c.UNID 
			FROM @TblAssetVehicle c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetVehicle c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 997)) /* Make */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 998)) /* Model */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 999)) /* Year YYYY */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1000)) /* Registration Number */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1001)) /* Milage (Kilometres) ("milage" in the XML") */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1002)) /* Need for vehicle */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1003)) /* Original Cost */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1004)) /* Purchase Date Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1005)) /* Purchase Date Year */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1006)) /* Current Market Value */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1007)) /* SubjectToFinance */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1008)) /* AdaptedForDisabledUse */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1009)) /* IsTheAssetLocatedInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1010)) /* AssetUniqueIDFieldMissing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1011)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 997) THEN c.Make 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 998) THEN c.Model
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 999) THEN c.Year
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1000) THEN c.RegistrationNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1001) THEN c.Mileage
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1002) THEN c.NeedForVehicle
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1003) THEN c.OriginalCost
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1004) THEN CAST((SELECT luli.LookupListItemID FROM LookupListItems luli WITH (NOLOCK) INNER JOIN DetailFields df WITH (NOLOCK) ON df.LookupListID = luli.LookupListID AND df.DetailFieldID = f.DetailFieldID WHERE luli.ItemValue = LEFT(DateName( month , DateAdd( month , cast(c.PurchaseMonth as INT) , -1 ) ),3) ) AS VARCHAR)
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1005) THEN c.PurchaseYear
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1006) THEN c.CurrentMarketValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1007) THEN CASE c.SubjectToFinance WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1008) THEN CASE c.AdaptedForDisabledUse WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1009) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1010) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1011) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetVehicle c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset StockInTrade																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetStockInTrade TABLE (
			UNID INT IDENTITY(1,1), 
			CurrentMarketValue VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetStockInTrade (
			CurrentMarketValue,
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(CurrentMarketValue)[1]', 'varchar(2000)') AS Make,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/StockInTrade') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*StockInTrade*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1012)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1012), @DetailFieldPageID, c.UNID 
			FROM @TblAssetStockInTrade c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetStockInTrade c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1013)) /* CurrentMarketValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1014)) /* IsTheAssetLocatedInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1015)) /* AssetUniqueIDFieldMissing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1016)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1013) THEN c.CurrentMarketValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1014) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1015) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1016) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetStockInTrade c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END

		
		/********************************************************************************************************************************************************/
		/*															PFS Asset MoneyOwedToYou																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetMoneyOwedToYou TABLE (
			UNID INT IDENTITY(1,1), 
			DebtorName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			BookValue VARCHAR(2000), 
			RealisableValue VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetMoneyOwedToYou (
			DebtorName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			BookValue, 
			RealisableValue, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(DebtorName)[1]', 'varchar(2000)') AS DebtorName,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(BookValue)[1]', 'varchar(2000)') AS BookValue,
		n.c.value('(RealisableValue)[1]', 'varchar(2000)') AS RealisableValue,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/MoneyOwedToYou') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*MoneyOwedToYou*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1017)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1017), @DetailFieldPageID, c.UNID 
			FROM @TblAssetMoneyOwedToYou c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetMoneyOwedToYou c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1018)) /* DebtorName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1019)) /* Line1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1020)) /* Line2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1021)) /* Line3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1022)) /* Town */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1023)) /* County */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1024)) /* BookValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1025)) /* RealisableValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1026)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1027)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1018) THEN c.DebtorName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1019) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1020) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1021) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1022) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1023) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1024) THEN c.BookValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1025) THEN c.RealisableValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1026) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1027) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetMoneyOwedToYou c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
					
		/********************************************************************************************************************************************************/
		/*															PFS Asset BankAccount																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetBankAccount TABLE (
			UNID INT IDENTITY(1,1), 
			BankName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			IBANBIC VARCHAR(2000), 
			Balance VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetBankAccount (
			BankName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			AccountName, 
			AccountNumber, 
			IBANBIC, 
			Balance, 
			DebtorOwnership, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(BankName)[1]', 'varchar(2000)') AS BankName,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(IBANBIC)[1]', 'varchar(2000)') AS IBANBIC,
		n.c.value('(Balance)[1]', 'varchar(2000)') AS Balance,
		n.c.value('(DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/BankAccount') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*BankAccount*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1028)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1028), @DetailFieldPageID, c.UNID 
			FROM @TblAssetBankAccount c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetBankAccount c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1029)) /* BankName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1030)) /* Line1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1031)) /* Line2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1032)) /* Line3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1033)) /* Town */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1034)) /* County */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1035)) /* AccountName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1036)) /* AccountNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1037)) /* IBANBIC */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1038)) /* Balance */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1039)) /* DebtorOwnership */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1040)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1041)) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1029) THEN c.BankName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1030) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1031) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1032) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1033) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1034) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1035) THEN c.AccountName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1036) THEN c.AccountNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1037) THEN c.IBANBIC
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1038) THEN c.Balance
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1039) THEN c.DebtorOwnership
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1040) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1041) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetBankAccount c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset CreditUnionAccount																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetCreditUnionAccount TABLE (
			UNID INT IDENTITY(1,1), 
			CreditUnionName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetCreditUnionAccount (
			CreditUnionName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			AccountName, 
			AccountNumber, 
			CurrentMarketValue, 
			DebtorOwnership, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(CreditUnionName)[1]', 'varchar(2000)') AS CreditUnionName,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/CreditUnionAccount') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*CreditUnionAccount*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1042)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1042), @DetailFieldPageID, c.UNID 
			FROM @TblAssetCreditUnionAccount c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetCreditUnionAccount c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1043)) /* CreditUnionName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1044)) /* Line1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1045)) /* Line2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1046)) /* Line3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1047)) /* Town */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1048)) /* County */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1049)) /* AccountName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1050)) /* AccountNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1051)) /* CurrentMarketValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1052)) /* DebtorOwnership */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1053)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1054)) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1043) THEN c.CreditUnionName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1044) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1045) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1046) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1047) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1048) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1049) THEN c.AccountName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1050) THEN c.AccountNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1051) THEN c.CurrentMarketValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1052) THEN c.DebtorOwnership
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1053) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1054) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetCreditUnionAccount c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset CashOnHand																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetCashOnHand TABLE (
			UNID INT IDENTITY(1,1), 
			Amount VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetCashOnHand (
			Amount,
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Amount)[1]', 'varchar(2000)') AS Amount,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/CashOnHand') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*CashOnHand*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1055)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1055), @DetailFieldPageID, c.UNID 
			FROM @TblAssetCashOnHand c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetCashOnHand c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1056)) /* Amount */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1057)) /* IsTheAssetLocatedInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1058)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1059)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1056) THEN c.Amount
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1057) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1058) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1059) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetCashOnHand c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset ProspectiveAsset																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetProspectiveAsset TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			ReceiptEstimatedDate VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetProspectiveAsset (
			PDescription, 
			EstimatedValue, 
			ReceiptEstimatedDate, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(ReceiptEstimatedDate)[1]', 'varchar(2000)') AS ReceiptEstimatedDate,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/ProspectiveAsset') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*ProspectiveAsset*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1060)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1060), @DetailFieldPageID, c.UNID 
			FROM @TblAssetProspectiveAsset c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetProspectiveAsset c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1061)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1062)) /* EstimatedValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1063)) /* ReceiptEstimatedDate */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1064)) /* IsTheAssetLocatedInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1065)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1066)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1061) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1062) THEN c.EstimatedValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1063) THEN c.ReceiptEstimatedDate
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1064) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1065) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1066) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetProspectiveAsset c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset ContingentAsset																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetContingentAsset TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			ReceiptEstimatedDate VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetContingentAsset (
			PDescription, 
			EstimatedValue, 
			ReceiptEstimatedDate, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(ReceiptEstimatedDate)[1]', 'varchar(2000)') AS ReceiptEstimatedDate,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/ContingentAsset') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*ContingentAsset*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1067)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1067), @DetailFieldPageID, c.UNID 
			FROM @TblAssetContingentAsset c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetContingentAsset c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1068)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1069)) /* EstimatedValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1070)) /* ReceiptEstimatedDate */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1071)) /* IsTheAssetLocatedInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1072)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1073)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1068) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1069) THEN c.EstimatedValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1070) THEN c.ReceiptEstimatedDate
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1071) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1072) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1073) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetContingentAsset c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset Other																				*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetOther TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetOther (
			PDescription, 
			EstimatedValue, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/Other') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Other*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1074)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1074), @DetailFieldPageID, c.UNID 
			FROM @TblAssetOther c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetOther c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1075)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1076)) /* EstimatedValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1077)) /* IsTheAssetLocatedInTheState */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1078)) /* AssetUniqueID field missing */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1079)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1075) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1076) THEN c.EstimatedValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1077) THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1078) THEN c.AssetUniqueId
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1079) THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetOther c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt PPRL = Principal Private Residence Lender											*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtPPRL TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			IBANBIC VARCHAR(2000), 
			MonthlyRepaymentContract VARCHAR(2000), 
			MonthlyRepaymentActual VARCHAR(2000), 
			RemainingTerm VARCHAR(2000), 
			OriginalAmountBorrowed VARCHAR(2000), 
			PurposeOfLoan VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			JointAndSeveralLiability VARCHAR(2000), 
			ArrearsIncludedInAmountDue VARCHAR(2000), 
			Restructured VARCHAR(2000), 
			CurrentInterestRate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtPPRL (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AccountName, 
			AccountNumber, 
			IBANBIC, 
			MonthlyRepaymentContract, 
			MonthlyRepaymentActual, 
			RemainingTerm, 
			OriginalAmountBorrowed, 
			PurposeOfLoan, 
			AmountDue, 
			JointAndSeveralLiability, 
			ArrearsIncludedInAmountDue, 
			Restructured, 
			CurrentInterestRate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(IBANBIC)[1]', 'varchar(2000)') AS IBANBIC,
		n.c.value('(MonthlyRepaymentContract)[1]', 'varchar(2000)') AS MonthlyRepaymentContract,
		n.c.value('(MonthlyRepaymentActual)[1]', 'varchar(2000)') AS MonthlyRepaymentActual,
		n.c.value('(RemainingTerm)[1]', 'varchar(2000)') AS RemainingTerm,
		n.c.value('(OriginalAmountBorrowed)[1]', 'varchar(2000)') AS OriginalAmountBorrowed,
		n.c.value('(PurposeOfLoan)[1]', 'varchar(2000)') AS PurposeOfLoan,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(JointAndSeveralLiability)[1]', 'varchar(2000)') AS JointAndSeveralLiability,
		n.c.value('(ArrearsIncludedInAmountDue)[1]', 'varchar(2000)') AS ArrearsIncludedInAmountDue,
		n.c.value('(Restructured)[1]', 'varchar(2000)') AS Restructured,
		n.c.value('(CurrentInterestRate)[1]', 'varchar(2000)') AS CurrentInterestRate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/PPRL') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*PPRL*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1080)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1080), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtPPRL c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtPPRL c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1081)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1082)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1083)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1084)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1085)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1086)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1087)) /* TODO: should be AddressCounty but says ZIP in Aquarium */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1088)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1089)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1090)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1091)) /* AccountName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1092)) /* AccountNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1093)) /* IBANBIC */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1094)) /* MonthlyRepaymentContract */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1095)) /* MonthlyRepaymentActual */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1096)) /* RemainingTerm */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1097)) /* OriginalAmountBorrowed */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1098)) /* PurposeOfLoan */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1099)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1100)) /* JointAndSeveralLiability */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1101)) /* ArrearsIncludedInAmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1102)) /* Restructured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1103)) /* CurrentInterestRate */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1104)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1081) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1082) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1083) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1084) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1085) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1086) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1087) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1088) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1089) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1090) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1091) THEN c.AccountName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1092) THEN c.AccountNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1093) THEN c.IBANBIC
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1094) THEN c.MonthlyRepaymentContract
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1095) THEN c.MonthlyRepaymentActual
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1096) THEN c.RemainingTerm
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1097) THEN c.OriginalAmountBorrowed
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1098) THEN c.PurposeOfLoan
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1099) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1100) THEN CASE c.JointAndSeveralLiability WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1101) THEN CASE c.ArrearsIncludedInAmountDue WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1102) THEN CASE c.Restructured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1103) THEN c.CurrentInterestRate
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1104) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtPPRL c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		

		/********************************************************************************************************************************************************/
		/*															PFS Debt FinancialInstitutions																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtFinancialInstitutions TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			IBANBIC VARCHAR(2000), 
			MonthlyRepaymentContract VARCHAR(2000), 
			MonthlyRepaymentActual VARCHAR(2000), 
			RemainingTerm VARCHAR(2000), 
			OriginalAmountBorrowed VARCHAR(2000), 
			PurposeOfLoan VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			JointAndSeveralLiability VARCHAR(2000), 
			ArrearsIncludedInAmountDue VARCHAR(2000), 
			Restructured VARCHAR(2000), 
			CurrentInterestRate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtFinancialInstitutions (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AccountName, 
			AccountNumber, 
			IBANBIC, 
			MonthlyRepaymentContract, 
			MonthlyRepaymentActual, 
			RemainingTerm, 
			OriginalAmountBorrowed, 
			PurposeOfLoan, 
			AmountDue, 
			JointAndSeveralLiability, 
			ArrearsIncludedInAmountDue, 
			Restructured, 
			CurrentInterestRate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(IBANBIC)[1]', 'varchar(2000)') AS IBANBIC,
		n.c.value('(MonthlyRepaymentContract)[1]', 'varchar(2000)') AS MonthlyRepaymentContract,
		n.c.value('(MonthlyRepaymentActual)[1]', 'varchar(2000)') AS MonthlyRepaymentActual,
		n.c.value('(RemainingTerm)[1]', 'varchar(2000)') AS RemainingTerm,
		n.c.value('(OriginalAmountBorrowed)[1]', 'varchar(2000)') AS OriginalAmountBorrowed,
		n.c.value('(PurposeOfLoan)[1]', 'varchar(2000)') AS PurposeOfLoan,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(JointAndSeveralLiability)[1]', 'varchar(2000)') AS JointAndSeveralLiability,
		n.c.value('(ArrearsIncludedInAmountDue)[1]', 'varchar(2000)') AS ArrearsIncludedInAmountDue,
		n.c.value('(Restructured)[1]', 'varchar(2000)') AS Restructured,
		n.c.value('(CurrentInterestRate)[1]', 'varchar(2000)') AS CurrentInterestRate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/FinancialInstitutions') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*FinancialInstitutions*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1105)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1105), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtFinancialInstitutions c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtFinancialInstitutions c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1106)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1107)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1108)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1109)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1110)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1111)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1112)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1113)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1114)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1115)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1116)) /* AccountName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1117)) /* AccountNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1118)) /* IBANBIC */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1119)) /* MonthlyRepaymentContract */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1120)) /* MonthlyRepaymentActual */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1121)) /* RemainingTerm (months)*/
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1122)) /* OriginalAmountBorrowed */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1123)) /* PurposeOfLoan */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1124)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1125)) /* JointAndSeveralLiability */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1126)) /* ArrearsIncludedInAmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1127)) /* Restructured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1128)) /* CurrentInterestRate */
				   --,() /* TODO: Comment field not present yet */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1106) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1107) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1108) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1109) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1110) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1111) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1112) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1113) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1114) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1115) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1116) THEN c.AccountName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1117) THEN c.AccountNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1118) THEN c.IBANBIC
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1119) THEN c.MonthlyRepaymentContract
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1120) THEN c.MonthlyRepaymentActual
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1121) THEN c.RemainingTerm
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1122) THEN c.OriginalAmountBorrowed
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1123) THEN c.PurposeOfLoan
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1124) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1125) THEN CASE c.JointAndSeveralLiability WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1126) THEN CASE c.ArrearsIncludedInAmountDue WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1127) THEN CASE c.Restructured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1128) THEN c.CurrentInterestRate
									--WHEN  THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtFinancialInstitutions c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		

		/********************************************************************************************************************************************************/
		/*															PFS Debt CreditUnion																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtCreditUnion TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			MonthlyRepaymentContract VARCHAR(2000), 
			MonthlyRepaymentActual VARCHAR(2000), 
			RemainingTerm VARCHAR(2000), 
			OriginalAmountBorrowed VARCHAR(2000), 
			PurposeOfLoan VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			JointAndSeveralLiability VARCHAR(2000), 
			ArrearsIncludedInAmountDue VARCHAR(2000), 
			Restructured VARCHAR(2000), 
			RestructuringDetails VARCHAR(2000), 
			CurrentInterestRate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtCreditUnion (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AccountName, 
			AccountNumber, 
			MonthlyRepaymentContract, 
			MonthlyRepaymentActual, 
			RemainingTerm, 
			OriginalAmountBorrowed, 
			PurposeOfLoan, 
			AmountDue, 
			JointAndSeveralLiability, 
			ArrearsIncludedInAmountDue, 
			Restructured, 
			RestructuringDetails, 
			CurrentInterestRate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(MonthlyRepaymentContract)[1]', 'varchar(2000)') AS MonthlyRepaymentContract,
		n.c.value('(MonthlyRepaymentActual)[1]', 'varchar(2000)') AS MonthlyRepaymentActual,
		n.c.value('(RemainingTerm)[1]', 'varchar(2000)') AS RemainingTerm,
		n.c.value('(OriginalAmountBorrowed)[1]', 'varchar(2000)') AS OriginalAmountBorrowed,
		n.c.value('(PurposeOfLoan)[1]', 'varchar(2000)') AS PurposeOfLoan,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(JointAndSeveralLiability)[1]', 'varchar(2000)') AS JointAndSeveralLiability,
		n.c.value('(ArrearsIncludedInAmountDue)[1]', 'varchar(2000)') AS ArrearsIncludedInAmountDue,
		n.c.value('(Restructured)[1]', 'varchar(2000)') AS Restructured,
		n.c.value('(RestructuringDetails)[1]', 'varchar(2000)') AS RestructuringDetails,
		n.c.value('(CurrentInterestRate)[1]', 'varchar(2000)') AS CurrentInterestRate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/CreditUnion') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*CreditUnion*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1129)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1129), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtCreditUnion c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtCreditUnion c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1130)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1131)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1132)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1133)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1134)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1135)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1136)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1137)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1138)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1139)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1140)) /* AccountName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1141)) /* AccountNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1142)) /* MonthlyRepaymentContract */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1143)) /* MonthlyRepaymentActual */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1144)) /* RemainingTerm (months)*/
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1145)) /* OriginalAmountBorrowed */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1146)) /* PurposeOfLoan */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1147)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1148)) /* JointAndSeveralLiability */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1149)) /* ArrearsIncludedInAmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1150)) /* Restructured */
				   --,(2) /* RestructuringDetails */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1151)) /* CurrentInterestRate */
				   --,(2) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1130) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1131) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1132) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1133) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1134) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1135) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1136) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1137) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1138) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1139) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1140) THEN c.AccountName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1141) THEN c.AccountNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1142) THEN c.MonthlyRepaymentContract
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1143) THEN c.MonthlyRepaymentActual
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1144) THEN c.RemainingTerm
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1145) THEN c.OriginalAmountBorrowed
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1146) THEN c.PurposeOfLoan
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1147) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1148) THEN CASE c.JointAndSeveralLiability WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1149) THEN CASE c.ArrearsIncludedInAmountDue WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1150) THEN CASE c.Restructured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									--WHEN 2 THEN c.RestructuringDetails
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1151) THEN c.CurrentInterestRate
									--WHEN 2 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtCreditUnion c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		



		/********************************************************************************************************************************************************/
		/*															PFS Debt ExcludableRevenue																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtExcludableRevenue TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			OtherDetail VARCHAR(2000), 
			Permitted VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			PreferentialAmount VARCHAR(2000), 
			InstallmentArrangement VARCHAR(2000), 
			InstallmentAmount VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtExcludableRevenue (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			OtherDetail, 
			Permitted, 
			AmountDue, 
			PreferentialAmount, 
			InstallmentArrangement, 
			InstallmentAmount, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(OtherDetail)[1]', 'varchar(2000)') AS OtherDetail,
		n.c.value('(Permitted)[1]', 'varchar(2000)') AS Permitted,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(PreferentialAmount)[1]', 'varchar(2000)') AS PreferentialAmount,
		n.c.value('(InstallmentArrangement)[1]', 'varchar(2000)') AS InstallmentArrangement,
		n.c.value('(InstallmentAmount)[1]', 'varchar(2000)') AS InstallmentAmount,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/ExcludableRevenue') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*ExcludableRevenue*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1152)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1152), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtExcludableRevenue c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtExcludableRevenue c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1153)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1154)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1155)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1156)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1157)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1158)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1159)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1160)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1161)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1162)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1163)) /* PType */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1164)) /* OtherDetail */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1165)) /* Permitted */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1166)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1168)) /* PreferentialAmount */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1169)) /* InstallmentArrangement */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1170)) /* InstallmentAmount */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1171)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1153) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1154) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1155) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1156) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1157) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1158) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1159) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1160) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1161) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1162) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1163) THEN c.PType
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1164) THEN c.OtherDetail
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1165) THEN CASE c.Permitted WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1166) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1168) THEN c.PreferentialAmount
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1169) THEN CASE c.InstallmentArrangement WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1170) THEN c.InstallmentAmount
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1171) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtExcludableRevenue c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt ExcludableNonRevenue																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtExcludableNonRevenue TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			OtherDetail VARCHAR(2000), 
			Permitted VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			PreferentialAmount VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtExcludableNonRevenue (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			OtherDetail, 
			Permitted, 
			AmountDue, 
			PreferentialAmount, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(OtherDetail)[1]', 'varchar(2000)') AS OtherDetail,
		n.c.value('(Permitted)[1]', 'varchar(2000)') AS Permitted,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(PreferentialAmount)[1]', 'varchar(2000)') AS PreferentialAmount,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/ExcludableNonRevenue') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*ExcludableNonRevenue*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1172)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1172), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtExcludableNonRevenue c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtExcludableNonRevenue c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1173)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1174)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1175)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1176)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1177)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1178)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1179)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1180)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1181)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1182)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1183)) /* PType */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1184)) /* OtherDetail */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1185)) /* Permitted */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1186)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1187)) /* PreferentialAmount */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1188)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1173) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1174) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1175) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1176) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1177) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1178) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1179) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1180) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1181) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1182) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1183) THEN c.PType
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1184) THEN c.OtherDetail
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1185) THEN CASE c.Permitted WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1186) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1187) THEN c.PreferentialAmount
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1188) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtExcludableNonRevenue c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/********************************************************************************************************************************************************/
		/*															PFS Debt Employees																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtEmployees TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			FirstName VARCHAR(2000), 
			Surname VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			PreferentialAmount VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtEmployees (
			DebtSecured,  
			FirstName, 
			Surname, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			AmountDue, 
			PreferentialAmount, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Individual/FirstName)[1]', 'varchar(2000)') AS FirstName,
		n.c.value('(Creditor/Individual/Surname)[1]', 'varchar(2000)') AS Surname,
		n.c.value('(Creditor/Individual/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Individual/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Individual/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Individual/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Individual/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Individual/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Individual/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS Type,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(PreferentialAmount)[1]', 'varchar(2000)') AS PreferentialAmount,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Employees') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Employees*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1189)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1189), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtEmployees c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtEmployees c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1190)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1191)) /* FirstName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1192)) /* Surname */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1193)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1194)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1195)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1196)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1197)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1198)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1199)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1200)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1201)) /* PType */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1202)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1203)) /* PreferentialAmount */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1204)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1190) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1191) THEN c.FirstName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1192) THEN c.Surname
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1193) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1194) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1195) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1196) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1197) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1198) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1199) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1200) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1201) THEN c.PType
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1202) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1203) THEN c.PreferentialAmount
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1204) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtEmployees c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/********************************************************************************************************************************************************/
		/*															PFS Debt Equipment																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtEquipment TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtEquipment (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			AccountNumber, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Equipment') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Equipment*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1205)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1205), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtEquipment c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtEquipment c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1206)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1207)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1208)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1209)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1210)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1211)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1212)) /* TODO: AddressCounty but we only have a ZIP field to store it in */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1213)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1214)) /* PhoneNumber */
				   --,() /* Incurred6Month */
				   --,() /* PType */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1215)) /* AccountNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1216)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1217)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1206) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1207) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1208) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1209) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1210) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1211) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1212) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1213) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1214) THEN c.PhoneNumber
									--WHEN  THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									--WHEN  THEN c.PType
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1215) THEN c.AccountNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1216) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1217) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtEquipment c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/********************************************************************************************************************************************************/
		/*															PFS Debt TradeCreditors																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtTradeCreditors TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtTradeCreditors (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/TradeCreditors') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*TradeCreditors*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1218)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1218), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtTradeCreditors c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtTradeCreditors c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1219)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1220)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1221)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1222)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1223)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1224)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1225)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1226)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1227)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1228)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1229)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1230)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1219) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1220) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1221) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1222) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1223) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1224) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1225) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1226) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1227) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1228) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1229) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1230) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtTradeCreditors c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt ConnectedCreditors																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtConnectedCreditors TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			LoanPurpose VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Connection VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtConnectedCreditors (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			LoanPurpose, 
			AmountDue, 
			Connection, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(LoanPurpose)[1]', 'varchar(2000)') AS LoanPurpose,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Connection)[1]', 'varchar(2000)') AS Connection,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/ConnectedCreditors') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*ConnectedCreditors*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1231)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1231), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtConnectedCreditors c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtConnectedCreditors c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1232)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1233)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1234)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1235)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1236)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1237)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1238)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1239)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1240)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1241)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1242)) /* LoanPurpose */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1243)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1244)) /* Connection */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1245)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1232) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1233) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1234) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1235) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1236) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1237) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1238) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1239) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1240) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1241) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1242) THEN c.LoanPurpose
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1243) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1244) THEN c.Connection
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1245) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtConnectedCreditors c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Other																				*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtOther TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PDescription VARCHAR(2000), 
			DebtPurpose VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtOther (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PDescription, 
			DebtPurpose, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(DebtPurpose)[1]', 'varchar(2000)') AS DebtPurpose,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Other') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Other*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1246)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1246), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtOther c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtOther c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1247)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1248)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1249)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1250)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1251)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1252)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1253)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1254)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1255)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1256)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1257)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1258)) /* DebtPurpose */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1259)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1260)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1247) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1248) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1249) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1250) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1251) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1252) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1253) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1254) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1255) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1256) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1257) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1258) THEN c.DebtPurpose
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1259) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1260) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtOther c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Prospective																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtProspective TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			EstimatedPaymentDate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtProspective (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PDescription, 
			EstimatedValue, 
			EstimatedPaymentDate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(EstimatedPaymentDate)[1]', 'varchar(2000)') AS EstimatedPaymentDate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Prospective') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Prospective*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1261)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1261), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtProspective c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtProspective c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1262)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1263)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1264)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1265)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1266)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1267)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1268)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1269)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1270)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1271)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1272)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1273)) /* EstimatedValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1274)) /* EstimatedPaymentDate */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1275)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1262) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1263) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1264) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1265) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1266) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1267) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1268) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1269) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1270) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1271) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1272) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1273) THEN c.EstimatedValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1274) THEN c.EstimatedPaymentDate
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1275) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtProspective c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Contingent																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtContingent TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			FirstName VARCHAR(2000), 
			Surname VARCHAR(2000), 
			AddressCountry VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			EstimatedPaymentDate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtContingent (
			DebtSecured,  
			FirstName, 
			Surname, 
			AddressCountry, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PDescription, 
			EstimatedValue, 
			EstimatedPaymentDate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Individual/FirstName)[1]', 'varchar(2000)') AS FirstName,
		n.c.value('(Creditor/Individual/Surname)[1]', 'varchar(2000)') AS Surname,
		n.c.value('(Creditor/Individual/Address/NonIrish/Country)[1]', 'varchar(2000)') AS Country,
		n.c.value('(Creditor/Individual/Address/NonIrish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Individual/Address/NonIrish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Individual/Address/NonIrish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Individual/Address/NonIrish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Individual/Address/NonIrish/PostCode)[1]', 'varchar(2000)') AS PostCode,
		n.c.value('(Creditor/Individual/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Individual/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(EstimatedPaymentDate)[1]', 'varchar(2000)') AS EstimatedPaymentDate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Contingent') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Contingent*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1276)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1276), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtContingent c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtContingent c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1277)) /* DebtSecured */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1278)) /* FirstName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1279)) /* Surname */
				   --,() /* AddressCountry */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1280)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1281)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1282)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1283)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1284)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1285)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1286)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1287)) /* Incurred6Month */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1288)) /* PDescription */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1289)) /* EstimatedValue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1290)) /* EstimatedPaymentDate */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1291)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1277) THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1278) THEN c.FirstName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1279) THEN c.Surname
									--,() /* AddressCountry */
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1280) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1281) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1282) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1283) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1284) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1285) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1286) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1287) THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1288) THEN c.PDescription
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1289) THEN c.EstimatedValue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1290) THEN c.EstimatedPaymentDate
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1291) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtContingent c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Excluded																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtExcluded TABLE (
			UNID INT IDENTITY(1,1), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtExcluded (
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			PType, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Excluded') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*Get the Page ID for the table */
			SELECT @DetailFieldPageID = NULL
		
			SELECT @DetailFieldPageID = df.DetailFieldPageID /*Excluded*/
			FROM DetailFields df WITH (NOLOCK)  
			WHERE df.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1292)

			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1292), @DetailFieldPageID, c.UNID 
			FROM @TblPFSDebtExcluded c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtExcluded c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1293)) /* OrganisationName */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1294)) /* AddressLine1 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1295)) /* AddressLine2 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1296)) /* AddressLine3 */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1297)) /* AddressTown */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1298)) /* AddressCounty */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1299)) /* Email */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1300)) /* PhoneNumber */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1301)) /* PType */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1302)) /* AmountDue */
				   ,(dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1303)) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1293) THEN c.OrganisationName
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1294) THEN c.AddressLine1
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1295) THEN c.AddressLine2
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1296) THEN c.AddressLine3
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1297) THEN c.AddressTown
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1298) THEN c.AddressCounty
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1299) THEN c.Email
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1300) THEN c.PhoneNumber
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1301) THEN c.PType
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1302) THEN c.AmountDue
									WHEN dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, @LeadTypeID, 69, 1303) THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtExcluded c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/* Income */
		
		/* Expenses */
		
		
		/********************************************************************************************************************************************************/
		/*																																						*/
		/*															End of data mappings																		*/
		/*																																						*/
		/********************************************************************************************************************************************************/
		
		
		
		/********************************************************************************************************************************************************/
		/*															Add a dashboard entry for the demo															*/
		/********************************************************************************************************************************************************/
		EXECUTE dbo.LeadViewHistory_Insert @LeadViewHistoryID OUTPUT, @ClientPersonnelID = @ClientPersonnelID, @ClientID = @ClientID, @LeadID = @LeadID, @WhenViewed = @TheDateTimePlus1s
		
	END
	
	/* Result should include the case id so that the calling service can log it */
    SELECT @CustomerID AS CustomerID, 
			@LeadID AS LeadID, 
			@CaseID AS CaseID, 
			@MatterID AS MatterID, 
	'' AS InfoMessage 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IISSA_AddClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_IISSA_AddClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_IISSA_AddClaim] TO [sp_executeall]
GO
