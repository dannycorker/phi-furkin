SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds / Alex Maguire
-- Create date: 2021-03-12
-- Description:	Charge Back Received
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Collections_160532]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@TermsDate						DATE
			,@AdjustmentDate				DATE
			,@Count							INT
			,@ValueInt						INT
			,@DetailValueData				dbo.tvpDetailValueData
			,@EventComments					VARCHAR(2000)
			,@NextEventID					INT
			,@IdId							dbo.tvpIntInt
			,@WorkflowTaskID				INT

			DECLARE @NumberedValues TABLE ( ID INT Identity, Value VARCHAR(2000) )


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	DECLARE @DateToUse DATE
			,@Reference VARCHAR(200)
			,@ProjectedCancellationDate DATE
			,@CustomerPaymentScheduleRows VARCHAR(2000)

	DECLARE @CLMap TABLE (FromCLID INT, ToCCLID INT)
	DECLARE @CPSRowsTable TABLE (CustomerPaymentScheduleID INT)
	DECLARE @UpdatedRows TABLE (Table_Name VARCHAR(100), ID INT)


	IF @EventTypeID = 160532
	BEGIN

		/*Perform Charge Back*/
    
		/*Get the row(s) to fail*/
		SELECT @CustomerPaymentScheduleRows = dbo.fnGetSimpleDv(315896, @MatterID)

		/*Convert the csv string to a table of values*/
		INSERT INTO @CPSRowsTable (CustomerPaymentScheduleID)
		SELECT d.AnyID
		FROM dbo.fnTableOfIDsFromCSV(@CustomerPaymentScheduleRows) d

		/*Missed payments are marked as 'failed' on the Purchased Product Payment Schedule.*/
		UPDATE cps
		SET PaymentStatusID = 4 /*Failed*/
		OUTPUT 'CustomerPaymentSchedule', inserted.CustomerPaymentScheduleID INTO @UpdatedRows (Table_Name, ID)
		FROM CustomerPaymentSchedule cps 
		INNER JOIN @CPSRowsTable c ON c.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		WHERE cps.CustomerID = @CustomerID

		/*Missed payments are marked as 'failed' on the Customer Payment Schedule.*/
		UPDATE ppps
		SET PaymentStatusID = 4 /*Failed*/
		OUTPUT 'PurchasedProductPaymentSchedule', inserted.PurchasedProductPaymentScheduleID INTO @UpdatedRows (Table_Name, ID)
		FROM PurchasedProductPaymentSchedule ppps 
		INNER JOIN @CPSRowsTable c ON c.CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
		WHERE ppps.CustomerID = @CustomerID

		/*Matching Customer Ledger payments are reversed.*/
		INSERT INTO CustomerLedger ([ClientID], [CustomerID], [EffectivePaymentDate], [FailureCode], [FailureReason], [TransactionDate], [TransactionReference], [TransactionDescription], [TransactionNet], [TransactionVAT], [TransactionGross], [LeadEventID], [ObjectID], [ObjectTypeID], [PaymentID], [OutgoingPaymentID], [WhoCreated], [WhenCreated], [SourceID])
		OUTPUT inserted.SourceID, inserted.CustomerLedgerID INTO @CLMap(FromCLID, ToCCLID)
		SELECT cl.[ClientID], 
				cl.[CustomerID], 
				dbo.fn_GetDate_Local() AS [EffectivePaymentDate], 
				cl.[FailureCode], 
				'Charge Back', /*Charge backs must be individually identifiable as such (i.e. rather than a normal failed record).*/
				cl.[TransactionDate], 
				cl.[TransactionReference], 
				'Charge Back',--cl.[TransactionDescription], /*Charge backs must be individually identifiable as such (i.e. rather than a normal failed record).*/
				cl.[TransactionNet]*-1, 
				cl.[TransactionVAT]*-1, 
				cl.[TransactionGross]*-1, 
				cl.[LeadEventID], 
				cl.[ObjectID], 
				cl.[ObjectTypeID], 
				cl.[PaymentID], 
				cl.[OutgoingPaymentID], 
				cl.[WhoCreated], 
				dbo.fn_GetDate_Local() AS [WhenCreated],
				cl.CustomerLedgerID AS [SourceID]
		FROM @CPSRowsTable r
		INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = r.CustomerPaymentScheduleID
		INNER JOIN CustomerLedger cl WITH (NOLOCK) ON cl.CustomerLedgerID = cps.CustomerLedgerID

		/*Reversed CL rows to be added to PPPS as contra cl    */
		UPDATE ppps
		SET ContraCustomerLedgerID = clm.ToCCLID
		FROM PurchasedProductPaymentSchedule ppps 
		INNER JOIN @CPSRowsTable c ON c.CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
		INNER JOIN @CLMap clm ON clm.FromCLID = ppps.CustomerLedgerID

		/*Calculate Projected Cancellation Date and store against the Policy*/
		SELECT @MatterID = [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections](@MatterID)
		SELECT @DateToUse = dbo.fn_GetDate_Local()
		SELECT @ProjectedCancellationDate = DATEADD(DAY,30,@DateToUse)
		
		EXEC _C00_SimpleValueIntoField 315274, @ProjectedCancellationDate, @MatterID, 58552

		/*Immediate Notice*/
		EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID, 160531, 58552, NULL
	
	END
		
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
