SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the NoteType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType_Update]
(

	@NoteTypeID int   ,

	@ClientID int   ,

	@NoteTypeName varchar (50)  ,

	@NoteTypeDescription varchar (250)  ,

	@DefaultPriority smallint   ,

	@AlertColour nchar (7)  ,

	@NormalColour nchar (7)  ,

	@Enabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SourceID int   ,

	@LeadTypeID int   ,

	@IsShared bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[NoteType]
				SET
					[ClientID] = @ClientID
					,[NoteTypeName] = @NoteTypeName
					,[NoteTypeDescription] = @NoteTypeDescription
					,[DefaultPriority] = @DefaultPriority
					,[AlertColour] = @AlertColour
					,[NormalColour] = @NormalColour
					,[Enabled] = @Enabled
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[SourceID] = @SourceID
					,[LeadTypeID] = @LeadTypeID
					,[IsShared] = @IsShared
				WHERE
[NoteTypeID] = @NoteTypeID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Update] TO [sp_executeall]
GO
