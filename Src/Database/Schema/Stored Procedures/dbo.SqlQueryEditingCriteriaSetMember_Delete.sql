SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryEditingCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_Delete]
(

	@SqlQueryEditingCriteriaSetMemberID int   
)
AS


				DELETE FROM [dbo].[SqlQueryEditingCriteriaSetMember] WITH (ROWLOCK) 
				WHERE
					[SqlQueryEditingCriteriaSetMemberID] = @SqlQueryEditingCriteriaSetMemberID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Delete] TO [sp_executeall]
GO
