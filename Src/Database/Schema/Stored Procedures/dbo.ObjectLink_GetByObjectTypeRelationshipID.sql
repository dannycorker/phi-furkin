SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_GetByObjectTypeRelationshipID]
(

	@ObjectTypeRelationshipID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ObjectLinkID],
					[FromObjectID],
					[ToObjectID],
					[ObjectTypeRelationshipID]
				FROM
					[dbo].[ObjectLink] WITH (NOLOCK) 
				WHERE
					[ObjectTypeRelationshipID] = @ObjectTypeRelationshipID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_GetByObjectTypeRelationshipID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_GetByObjectTypeRelationshipID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_GetByObjectTypeRelationshipID] TO [sp_executeall]
GO
