SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-16
-- Description:	Saves the base data for the FNOL process and resets the policy sections
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SaveKeyDatesClearPolicySections] 
(
	@MatterID INT,
	@DateOfLoss DATE,
	@TreatmentStart DATE,
	@TreatmentEnd DATE,
	@ConditionParent INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	-- Clear out the old relationships
	DELETE dbo.TableRows
	WHERE LeadID = @LeadID
	AND DetailFieldID = 146357
	AND DetailFieldPageID = 16395
	AND TableRowID IN
	(
		SELECT TableRowID
		FROM dbo.TableDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = 146355
		AND LeadID = @LeadID
		AND ValueInt = @MatterID
	)
	
	-- Add the new ones if necessary
	IF @ConditionParent > 0
	BEGIN
	
		INSERT INTO dbo.TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID)
		VALUES (@ClientID, @LeadID, 146357, 16395)
		
		DECLARE @TableRowID INT
		SELECT @TableRowID = SCOPE_IDENTITY()
		
		INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, LeadID, ClientID) VALUES
		(@TableRowID, 146355, @MatterID, @LeadID, @ClientID),
		(@TableRowID, 146356, @ConditionParent, @LeadID, @ClientID)
		
		-- Get date of loss from parent
		SELECT @DateOfLoss = ValueDate 
		FROM dbo.MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @ConditionParent
		AND DetailFieldID = 144892
		-- Or default to treatment if null
		SELECT @DateOfLoss = ISNULL(@DateOfLoss, @TreatmentStart)
		
	END
	
	EXEC dbo._C00_SimpleValueIntoField 144892, @DateOfLoss, @MatterID
	EXEC dbo._C00_SimpleValueIntoField 144366, @TreatmentStart, @MatterID
	EXEC dbo._C00_SimpleValueIntoField 145674, @TreatmentEnd, @MatterID
	
	-- Clear out claim details
	DELETE dbo.TableRows
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144355
	AND DetailFieldPageID = 16157

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveKeyDatesClearPolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SaveKeyDatesClearPolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveKeyDatesClearPolicySections] TO [sp_executeall]
GO
