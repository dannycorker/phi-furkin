SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-01
-- Description:	Count rows in a table using the system catalogs, instead of count(*)
-- =============================================
CREATE PROC [dbo].[rc] 
(
	@TableName SYSNAME = NULL, 
	@SortOrder TINYINT = 1 
)
AS
BEGIN
	
	DECLARE @RowCount BIGINT
	
	IF @TableName IS NULL
	BEGIN
		
		/* All tables */
		IF @SortOrder IS NULL
		BEGIN
			SELECT OBJECT_NAME(p.object_id) AS [TableName], COALESCE(SUM(p.rows), 0) AS [ROWCOUNT]
			FROM sys.partitions p 
			INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id AND a.type = 1 -- row-data only, not LOB
			WHERE p.index_id IN (0, 1) -- 0 = Heap tables, 1 = tables with clustered indexes
			GROUP BY OBJECT_NAME(p.object_id) 
			ORDER BY [TableName] ASC
		END
		ELSE
		BEGIN
			SELECT OBJECT_NAME(p.object_id) AS [TableName], COALESCE(SUM(p.rows), 0) AS [ROWCOUNT]
			FROM sys.partitions p 
			INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id AND a.type = 1 -- row-data only, not LOB
			WHERE p.index_id IN (0, 1) -- 0 = Heap tables, 1 = tables with clustered indexes
			GROUP BY OBJECT_NAME(p.object_id) 
			ORDER BY [ROWCOUNT] DESC
		END
		
	END
	ELSE
	BEGIN
		
		/* Specific table */
		/* Remove unwanted prefixes, so that Server.Database.Owner.Product becomes just Product */
		SELECT @TableName = PARSENAME(@TableName, 1)
		SELECT @RowCount = COALESCE(SUM(p.rows), 0)
		FROM sys.partitions p 
		INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id AND a.type = 1 -- row-data only, not LOB
		WHERE p.object_id = OBJECT_ID(@TableName)
		AND p.index_id IN (0, 1) -- 0 = Heap tables, 1 = tables with clustered indexes
		
		SELECT @RowCount  AS [ROWCOUNT]
		
		RETURN @RowCount
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[rc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[rc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[rc] TO [sp_executeall]
GO
