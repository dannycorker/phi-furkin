SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-11-02
-- Description:	Unlock a ClientPersonnel record
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__Unlock]
	@UserID int
AS
BEGIN

	UPDATE dbo.ClientPersonnel 
	SET AttemptedLogins = 0 
	WHERE ClientPersonnelID = @UserID 
	
	SELECT * FROM dbo.ClientPersonnel (nolock) WHERE ClientPersonnelID = @UserID

END






GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__Unlock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__Unlock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__Unlock] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__Unlock] TO [sp_executehelper]
GO
