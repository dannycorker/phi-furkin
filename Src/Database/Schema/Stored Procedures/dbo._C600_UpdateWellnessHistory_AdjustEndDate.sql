SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-17
-- Description:	Adjust the EndDate of the Wellness History row by Policy History TableRowID
-- =============================================
CREATE PROCEDURE [dbo].[_C600_UpdateWellnessHistory_AdjustEndDate]
	
	@HistoricalPolicyTableRowID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @WellnessHistoryTableRowID INT, @EndDate DATE

	DECLARE @MatterID INT = (SELECT MatterID FROM TableRows WITH (NOLOCK) WHERE TableRowID = @HistoricalPolicyTableRowID)

	SELECT @WellnessHistoryTableRowID = (SELECT TOP(1) tr.TableRowID FROM TableRows tr WITH (NOLOCK)
										INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 314241 -- Historical Policy TableRowID
										INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 314236 -- Active
										WHERE tr.DetailFieldID = 314240 /*Wellness History*/
										AND tdv.ValueInt = @HistoricalPolicyTableRowID
										AND tdv1.ValueInt = 74577 /*Active*/
										ORDER BY 1 DESC)
	
	IF @WellnessHistoryTableRowID IS NOT NULL
	BEGIN

		SELECT @EndDate = dbo.fnGetSimpleDv(170037, @MatterID)
						
		EXEC _C00_SimpleValueIntoField 314239, @EndDate, @WellnessHistoryTableRowID

	END

END
GO
