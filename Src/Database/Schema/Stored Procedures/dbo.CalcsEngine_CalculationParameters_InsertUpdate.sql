SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Inserts or updates a calculation parameter
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_CalculationParameters_InsertUpdate]
(
	@CalculationParameterID INT,
	@Name VARCHAR(200),
	@CalculationID INT,
	@ParameterTypeID INT,
	@Value VARCHAR(200),
	@ClientID INT,
	@UserID INT,
	@Description VARCHAR(MAX)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF @CalculationParameterID > 0
	BEGIN
	
		UPDATE dbo.CalcsEngine_CalculationParameters
		SET Name = @Name,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID,
			ParameterTypeID = @ParameterTypeID,
			Value = @Value,
			Description = @Description
		WHERE CalculationParameterID = @CalculationParameterID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.CalcsEngine_CalculationParameters (Name, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, Description, ParameterTypeID, Value, CalculationID)
		VALUES (@Name, @ClientID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @Description, @ParameterTypeID, @Value, @CalculationID)
		
		SELECT @CalculationParameterID = SCOPE_IDENTITY()
	
	END

	EXEC dbo.CalcsEngine_CalculationParameters_Get @ClientID, @CalculationID, @CalculationParameterID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationParameters_InsertUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_CalculationParameters_InsertUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationParameters_InsertUpdate] TO [sp_executeall]
GO
