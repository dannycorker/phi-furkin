SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AccessRule table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AccessRuleID],
					[ClientID],
					[AccessRuleName],
					[AccessRuleDescription],
					[AccessRuleEnabled],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[PortalUserID],
					[DataLoaderObjectTypeID],
					[LeadTypeID],
					[DetailFieldSubTypeID],
					[DetailFieldPageID],
					[DetailFieldID],
					[ClientQuestionnaireID],
					[ValueDecoder],
					[ValueDecoderColumnFieldID],
					[ValueToCompare],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[AccessLevel]
				FROM
					[dbo].[AccessRule] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_GetByClientID] TO [sp_executeall]
GO
