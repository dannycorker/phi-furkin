SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Partner table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Partner_Find]
(

	@SearchUsingOR bit   = null ,

	@PartnerID int   = null ,

	@CustomerID int   = null ,

	@ClientID int   = null ,

	@UseCustomerAddress bit   = null ,

	@TitleID int   = null ,

	@FirstName varchar (100)  = null ,

	@MiddleName varchar (100)  = null ,

	@LastName varchar (100)  = null ,

	@EmailAddress varchar (255)  = null ,

	@DayTimeTelephoneNumber varchar (50)  = null ,

	@HomeTelephone varchar (50)  = null ,

	@MobileTelephone varchar (50)  = null ,

	@Address1 varchar (200)  = null ,

	@Address2 varchar (200)  = null ,

	@Town varchar (200)  = null ,

	@County varchar (200)  = null ,

	@PostCode varchar (50)  = null ,

	@Occupation varchar (100)  = null ,

	@Employer varchar (100)  = null ,

	@DateOfBirth datetime   = null ,

	@FullName varchar (201)  = null ,

	@CountryID int   = null ,

	@WhenModified datetime   = null ,

	@WhoModified int   = null ,

	@Longitude numeric (25, 18)  = null ,

	@Latitude numeric (25, 18)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PartnerID]
	, [CustomerID]
	, [ClientID]
	, [UseCustomerAddress]
	, [TitleID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [EmailAddress]
	, [DayTimeTelephoneNumber]
	, [HomeTelephone]
	, [MobileTelephone]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [PostCode]
	, [Occupation]
	, [Employer]
	, [DateOfBirth]
	, [FullName]
	, [CountryID]
	, [WhenModified]
	, [WhoModified]
	, [Longitude]
	, [Latitude]
    FROM
	[dbo].[Partner] WITH (NOLOCK) 
    WHERE 
	 ([PartnerID] = @PartnerID OR @PartnerID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([UseCustomerAddress] = @UseCustomerAddress OR @UseCustomerAddress IS NULL)
	AND ([TitleID] = @TitleID OR @TitleID IS NULL)
	AND ([FirstName] = @FirstName OR @FirstName IS NULL)
	AND ([MiddleName] = @MiddleName OR @MiddleName IS NULL)
	AND ([LastName] = @LastName OR @LastName IS NULL)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress IS NULL)
	AND ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber OR @DayTimeTelephoneNumber IS NULL)
	AND ([HomeTelephone] = @HomeTelephone OR @HomeTelephone IS NULL)
	AND ([MobileTelephone] = @MobileTelephone OR @MobileTelephone IS NULL)
	AND ([Address1] = @Address1 OR @Address1 IS NULL)
	AND ([Address2] = @Address2 OR @Address2 IS NULL)
	AND ([Town] = @Town OR @Town IS NULL)
	AND ([County] = @County OR @County IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
	AND ([Occupation] = @Occupation OR @Occupation IS NULL)
	AND ([Employer] = @Employer OR @Employer IS NULL)
	AND ([DateOfBirth] = @DateOfBirth OR @DateOfBirth IS NULL)
	AND ([FullName] = @FullName OR @FullName IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([Longitude] = @Longitude OR @Longitude IS NULL)
	AND ([Latitude] = @Latitude OR @Latitude IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PartnerID]
	, [CustomerID]
	, [ClientID]
	, [UseCustomerAddress]
	, [TitleID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [EmailAddress]
	, [DayTimeTelephoneNumber]
	, [HomeTelephone]
	, [MobileTelephone]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [PostCode]
	, [Occupation]
	, [Employer]
	, [DateOfBirth]
	, [FullName]
	, [CountryID]
	, [WhenModified]
	, [WhoModified]
	, [Longitude]
	, [Latitude]
    FROM
	[dbo].[Partner] WITH (NOLOCK) 
    WHERE 
	 ([PartnerID] = @PartnerID AND @PartnerID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([UseCustomerAddress] = @UseCustomerAddress AND @UseCustomerAddress is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([FirstName] = @FirstName AND @FirstName is not null)
	OR ([MiddleName] = @MiddleName AND @MiddleName is not null)
	OR ([LastName] = @LastName AND @LastName is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber AND @DayTimeTelephoneNumber is not null)
	OR ([HomeTelephone] = @HomeTelephone AND @HomeTelephone is not null)
	OR ([MobileTelephone] = @MobileTelephone AND @MobileTelephone is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	OR ([Occupation] = @Occupation AND @Occupation is not null)
	OR ([Employer] = @Employer AND @Employer is not null)
	OR ([DateOfBirth] = @DateOfBirth AND @DateOfBirth is not null)
	OR ([FullName] = @FullName AND @FullName is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([Longitude] = @Longitude AND @Longitude is not null)
	OR ([Latitude] = @Latitude AND @Latitude is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Find] TO [sp_executeall]
GO
