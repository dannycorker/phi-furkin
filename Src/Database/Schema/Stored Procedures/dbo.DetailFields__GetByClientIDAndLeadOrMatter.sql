SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alistair Jones
-- Create date: 07 August 2007
-- Description:	<Description,,>
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetByClientIDAndLeadOrMatter] 

	@ClientID int, 
	@LeadTypeID int, 
	@LeadOrMatter int 

AS
BEGIN
				
				SELECT
					*
				FROM
					dbo.fnDetailFieldsShared(@ClientID)
				WHERE
					[ClientID] = @ClientID AND
					[LeadTypeID] = @LeadTypeID AND
					[LeadOrMatter] = @LeadOrMatter AND
					[Enabled] = 1 
				ORDER BY 
					[FieldName]

				Select @@ROWCOUNT

END







GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatter] TO [sp_executeall]
GO
