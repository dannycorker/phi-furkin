SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-16
-- Description:	Update Class Node Authorisation
-- =============================================
CREATE PROCEDURE [dbo].[ClassNodeAuth__Update] 
	@ClientPersonnelID INT = NULL,
	@ClassNodeAuth INT = NULL,
	@ClassNodeID INT = NULL,
	@ClientID INT = NULL,
	@ClientPersonnelAdminGroup INT = NULL,
	@RightsLevel INT = NULL
	-- @WhoCreated = No change
	-- @WhenCreated = No change
	-- @WhoModified = ClientPersonnelID,
	-- @WhenModified = Current DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE cna
	SET
		cna.[ClientID] = @ClientID,
		cna.[ClassNodeID] = @ClassNodeID,
		cna.[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroup,
		cna.[RightsLevel] = @RightsLevel,
		cna.[WhoModified] = @ClientPersonnelID,
		cna.[WhenModified] = dbo.fn_GetDate_Local()
	FROM [dbo].[ClassNodeAuth] AS cna
	INNER JOIN [ClientPersonnel] AS cp
		ON cp.[ClientID] = cna.[ClientID]
	WHERE
		[ClassNodeID] = @ClassNodeID
		AND @ClientPersonnelID = cp.ClientPersonnelID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth__Update] TO [sp_executeall]
GO
