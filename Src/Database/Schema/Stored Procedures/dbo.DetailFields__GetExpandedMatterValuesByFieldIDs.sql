SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-03-17
-- Description:	Used in the SDK this PROC gets matter or lead detail values by specified field ids.  It expands lookup lists, and resource lists nested 
-- in tables so the SDK can return a nicely formatted collection of objects
--
-- TODO: Need to add TableFieldCaption to Tables as in Basic Tables
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- MODIFIED: 2015-06-05 PR  Added Editable to simple values, resource lists, tables and basic tables
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetExpandedMatterValuesByFieldIDs]
	@ClientID INT,
	@MatterID INT,
	@FieldIDs tvpInt READONLY,
	@ExcludeHiddenFields bit = 0
AS
BEGIN
	
	DECLARE @LeadID INT
	
	SELECT @LeadID=LeadID FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID
	
	-- Matter detail values
	SELECT f.DetailFieldID, f.QuestionTypeID, f.Editable
	FROM 
		[dbo].[MatterDetailValues] v WITH (NOLOCK)
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f ON v.DetailFieldID = f.DetailFieldID
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
	WHERE
		v.MatterID = @MatterID
		AND f.ClientID = @ClientID
		AND f.Enabled = 1
	ORDER BY f.FieldOrder 
	
	-- Simple values
	SELECT v.DetailFieldID, v.MatterDetailValueID AS DetailValueID, v.DetailValue, COALESCE(f.Encrypt, 0) AS Encrypt, v.EncryptedValue,
			li.ItemValue AS LookupListValue, f.FieldCaption, ll.LookupListID, f.FieldOrder, f.Required, f.QuestionTypeID, f.FieldSize, v.MatterID, @LeadID LeadID, f.Editable
	FROM
		[dbo].[MatterDetailValues] v WITH (NOLOCK)
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f ON v.DetailFieldID = f.DetailFieldID
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND v.ValueInt = li.LookupListItemID
	WHERE
		v.MatterID = @MatterID
		AND v.ClientID = @ClientID
		AND (f.QuestionTypeID != 14 AND f.QuestionTypeID != 16 AND f.QuestionTypeID != 19)
		AND f.Enabled = 1
	ORDER BY f.FieldOrder
	
	-- Resource lists
	SELECT v.DetailFieldID, r.ResourceListID AS DetailValueID, r.DetailFieldID AS ResourceListDetailFieldID, r.ResourceListDetailValueID, 
			r.DetailValue AS ResourceListDetailValue, COALESCE(f.Encrypt, 0) AS ResourceListEncrypt, v.EncryptedValue AS ResourceListEncryptedValue,
			li.ItemValue AS ResourceListLookupListValue, f.FieldCaption, f2.FieldCaption AS ResourceListFieldCaption, f.QuestionTypeID, v.MatterID, @LeadID LeadID, ll.LookupListID, f.Editable
	FROM
		[dbo].[MatterDetailValues] v WITH (NOLOCK)
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f ON v.DetailFieldID = f.DetailFieldID
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		INNER JOIN dbo.ResourceListDetailValues r WITH (NOLOCK) ON r.ResourceListID = v.ValueInt
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f2 ON r.DetailFieldID = f2.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f2.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND r.ValueInt = li.LookupListItemID
	WHERE
		v.MatterID = @MatterID
		AND v.ClientID = @ClientID
		AND f.QuestionTypeID = 14
		AND f.Enabled = 1
		AND ((f2.Hidden = 0 AND @ExcludeHiddenFields = 1) OR (@ExcludeHiddenFields = 0))
	ORDER BY v.DetailFieldID, f.FieldOrder
	
	
	-- Tables
	SELECT f.DetailFieldID, t.DetailFieldID AS TableDetailFieldID, t.TableDetailValueID, t.DetailValue, t.TableRowID,
			rl.DetailFieldID AS ResourceListDetailFieldID, rl.ResourceListDetailValueID, rl.DetailValue AS ResourceListDetailValue, 
			COALESCE(f1.Encrypt, 0) AS Encrypt, t.EncryptedValue, COALESCE(f2.Encrypt, 0) AS ResourceListEncrypt, t.EncryptedValue AS ResourceListEncryptedValue,
			t.ResourceListID, li.ItemValue AS LookupListValue, li2.ItemValue AS ResourceListLookupListValue, f1.QuestionTypeID, t.MatterID, @LeadID LeadID, f.Editable

	FROM
		dbo.fnDetailFieldsShared(@ClientID) f
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON t.TableRowID = r.TableRowID
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f1 ON t.DetailFieldID = f1.DetailFieldID
		LEFT OUTER JOIN dbo.ResourceListDetailValues rl WITH (NOLOCK) ON rl.ResourceListID = t.ResourceListID
		LEFT OUTER JOIN dbo.fnDetailFieldsShared(@ClientID) f2 ON rl.DetailFieldID = f2.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f1.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND t.ValueInt = li.LookupListItemID
		LEFT JOIN dbo.LookupList ll2  WITH (NOLOCK) ON f2.LookupListID = ll2.LookupListID
		LEFT JOIN dbo.LookupListItems li2 WITH (NOLOCK) ON ll2.LookupListID = li2.LookupListID AND rl.ValueInt = li2.LookupListItemID
	WHERE
		t.MatterID = @MatterID	
		AND f.ClientID = @ClientID
		AND f.QuestionTypeID = 16
		AND f.Enabled = 1
	ORDER BY f.DetailFieldID, f.FieldOrder, t.TableRowID, f1.FieldOrder
	
	
	-- Basic tables
	SELECT f.DetailFieldID, t.DetailFieldID AS TableDetailFieldID, t.TableDetailValueID, t.DetailValue, t.TableRowID, 
			COALESCE(f1.Encrypt, 0) AS Encrypt, t.EncryptedValue, li.ItemValue AS LookupListValue, f.FieldCaption, f1.FieldCaption AS TableFieldCaption, f1.QuestionTypeID, t.MatterID, @LeadID LeadID, ll.LookupListID, f.Editable
	FROM
		dbo.fnDetailFieldsShared(@ClientID) f
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON t.TableRowID = r.TableRowID
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f1 ON t.DetailFieldID = f1.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f1.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND t.ValueInt = li.LookupListItemID
	WHERE
		t.MatterID = @MatterID	
		AND f.ClientID = @ClientID
		AND f.QuestionTypeID = 19
		AND f.Enabled = 1
	ORDER BY f.DetailFieldID, f.FieldOrder, t.TableRowID, f1.FieldOrder

END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedMatterValuesByFieldIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetExpandedMatterValuesByFieldIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedMatterValuesByFieldIDs] TO [sp_executeall]
GO
