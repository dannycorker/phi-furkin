SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-05-23
-- Description:	Copy Matter Page from one Case to another
-- DCM 16/07/2014 Added Lead and Customer level page copies
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateDetailFieldsBypage]
@DetailFieldPageID int,
@FromCaseID int, 
@ToCaseID int 

AS
BEGIN
	SET NOCOUNT ON;



	DECLARE @CustomerID INT,
			@LeadID INT,
			@MatterID INT,
			@PageType INT,
			@ToObjectID INT

	Select @LeadID = ca.LeadID, @CustomerID=l.CustomerID
	From Cases ca
	INNER JOIN Lead l WITH (NOLOCK) ON ca.LeadID=l.LeadID
	Where ca.CaseID = @FromCaseID
	
	SELECT @PageType=dfp.LeadOrMatter 
	FROM DetailFieldPages dfp WITH (NOLOCK) 
	WHERE DetailFieldPageID=@DetailFieldPageID 

	IF @PageType=1 -- Lead level page
	BEGIN
	
		SELECT @ToObjectID=ca.LeadID FROM Cases ca WITH (NOLOCK) WHERE ca.CaseID=@ToCaseID

		DELETE ldv
		FROM LeadDetailValues ldv 
		INNER JOIN DetailFields df ON df.DetailFieldID = ldv.DetailFieldID AND df.DetailFieldPageID = @DetailFieldPageID
		WHERE ldv.LeadID=@ToObjectID
		
		INSERT INTO LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
		SELECT df.ClientID, @ToObjectID, ldv.DetailFieldID, ldv.DetailValue
		FROM LeadDetailValues ldv
		INNER JOIN DetailFields df ON df.DetailFieldID = ldv.DetailFieldID AND df.DetailFieldPageID = @DetailFieldPageID
		WHERE ldv.LeadID=@LeadID
	
	END
	
	ELSE IF @PageType=2 -- Matter level page
	BEGIN
	
		Select top 1 @MatterID = MatterID
		From Matter
		Where CaseID = @ToCaseID 
		Order By MatterID desc
		
		Delete MatterDetailValues
		from MatterDetailValues mdv 
		Inner Join Matter m on m.MatterID = mdv.MatterID and m.CaseID = @ToCaseID
		Inner Join DetailFields df on df.DetailFieldID = mdv.DetailFieldID and df.DetailFieldPageID = @DetailFieldPageID
		
		Insert Into MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		Select df.ClientID, mdv.LeadID, @MatterID, mdv.DetailFieldID, mdv.DetailValue
		From MatterDetailValues mdv
		Inner Join DetailFields df on df.DetailFieldID = mdv.DetailFieldID and df.DetailFieldPageID = @DetailFieldPageID
		Inner Join Matter m on m.MatterID = mdv.MatterID and m.CaseID = @FromCaseID
	
	END
	
	ELSE IF @PageType=10 -- Customer Level Page
	BEGIN
	
		SELECT @ToObjectID=l.CustomerID 
		FROM Cases ca WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON ca.LeadID=l.LeadID
		WHERE ca.CaseID=@ToCaseID

		DELETE cdv
		FROM CustomerDetailValues cdv 
		INNER JOIN DetailFields df ON df.DetailFieldID = cdv.DetailFieldID AND df.DetailFieldPageID = @DetailFieldPageID
		WHERE cdv.CustomerID=@ToObjectID

		INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		SELECT df.ClientID, @ToObjectID, cdv.DetailFieldID, cdv.DetailValue
		FROM CustomerDetailValues cdv
		INNER JOIN DetailFields df ON df.DetailFieldID = cdv.DetailFieldID AND df.DetailFieldPageID = @DetailFieldPageID
		WHERE cdv.CustomerID=@CustomerID
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDetailFieldsBypage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateDetailFieldsBypage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDetailFieldsBypage] TO [sp_executeall]
GO
