SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-02-03
-- Description:	Returns options for a rule... using for the SMS survey integration demo so may not be needed in the long run
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterOptions_GetOptionsByRuleID]
(
	@ClientID INT,
	@RuleID INT
)


AS
BEGIN
	
	SET NOCOUNT ON;
	
	
	SELECT	o.OperatorID, o.Val1, o.Val2, p.Name AS ParameterType, p.RuleParameterID
	FROM dbo.RulesEngine_RuleParameters p WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) ON p.RuleParameterID = o.RuleParameterID	
	WHERE o.ClientID = @ClientID
	AND p.RuleID = @RuleID
	ORDER BY p.RuleParameterID, o.OptionOrder

	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_GetOptionsByRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterOptions_GetOptionsByRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_GetOptionsByRuleID] TO [sp_executeall]
GO
