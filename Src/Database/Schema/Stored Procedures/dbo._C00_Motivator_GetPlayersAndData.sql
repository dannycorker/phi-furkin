SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-05-17
-- Description:	Gets all the data needed for the sales motivator game
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Motivator_GetPlayersAndData] 
(
	@Username VARCHAR(2000),
	@MatterID INT,
	@LastRowID INT,
	@LastDateString VARCHAR(2000) = ''
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LastDate DATETIME
	
	IF @LastDateString = 'null'
	BEGIN
		SELECT @LastDateString = ''
	END
	
	IF @LastDateString > '' 
	BEGIN
		SELECT @LastDate = CAST(@LastDateString AS DATETIME)
	END
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE EmailAddress = @Username
	AND ThirdPartySystemId = 0 -- For now we are forcing on the main credentials
	
	DECLARE @ManualDataFieldID INT,
			@ManualDataPageID INT,
			@PlayerColumnID INT,
			@SalesAmountFieldID INT,
			
			@EventDataClientPersonnelFieldID INT,
			@EventDataClientPersonnelPageID INT,
			@PlayerNameColumnID INT,
			@ClientPersonnelIdFieldID INT,
			
			@EventDataEventFieldID INT,
			@EventDataEventPageID INT,
			@EventIDColumnID INT,
			@EventWeightingColumnID INT,
			
			@CountEventsFromFieldID INT,
			
			@PacemakerField INT,
			@PacemakerRateField INT,
			@SetGameTimerFiled INT,
			@TimerHoursField INT,
			@TimerMinutesField INT,
			@EndGameNowField INT,
			@UseEventDataField INT,
			@UseAssignedToField INT
			
	
	
	IF @ClientID = 224
	BEGIN
	
		SELECT	@ManualDataFieldID = 151141,
				@ManualDataPageID = 16915,
				@PlayerColumnID = 151139,
				@SalesAmountFieldID = 151140,
				
				@EventDataClientPersonnelFieldID = 151176,
				@EventDataClientPersonnelPageID = 16916,
				@PlayerNameColumnID = 151175,
				@ClientPersonnelIdFieldID = 151174,
				
				@EventDataEventFieldID = 151177,
				@EventDataEventPageID = 16916,
				@EventIDColumnID = 153379,
				@EventWeightingColumnID = 153381,
				
				@CountEventsFromFieldID = 151178,
				
				@PacemakerField = 151142,
				@PacemakerRateField = 151143,
				@SetGameTimerFiled = 151144,
				@TimerHoursField = 151145,
				@TimerMinutesField = 151146,
				@EndGameNowField = 151147,
				@UseEventDataField = 151193,
				@UseAssignedToField = 156756
	
	END
	ELSE IF @ClientID = 3
	BEGIN
	
		SELECT	@ManualDataFieldID = 153401,
				@ManualDataPageID = 17189,
				@PlayerColumnID = 153405,
				@SalesAmountFieldID = 153406,
				
				@EventDataClientPersonnelFieldID = 153398,
				@EventDataClientPersonnelPageID = 17186,
				@PlayerNameColumnID = 153408,
				@ClientPersonnelIdFieldID = 153407,
				
				@EventDataEventFieldID = 153399,
				@EventDataEventPageID = 17186,
				@EventIDColumnID = 153402,
				@EventWeightingColumnID = 153404,
				
				@CountEventsFromFieldID = 153400,
				
				@PacemakerField = 153393,
				@PacemakerRateField = 153392,
				@SetGameTimerFiled = 153394,
				@TimerHoursField = 153395,
				@TimerMinutesField = 153396,
				@EndGameNowField = 153397,
				@UseEventDataField = 153391,
				@UseAssignedToField = 156761
	
	END
	ELSE IF @ClientID = 166 /*Chase Alexander*/
	BEGIN
	
		SELECT	@ManualDataFieldID	= 162720,
				@ManualDataPageID	= 18195,
				@PlayerColumnID		= 162724,
				@SalesAmountFieldID	= 162725,

				@EventDataClientPersonnelFieldID	= 162721,
				@EventDataClientPersonnelPageID		= 18194,
				@PlayerNameColumnID					= 162727,
				@ClientPersonnelIdFieldID			= 162726,

				@EventDataEventFieldID	= 162722,
				@EventDataEventPageID	= 18194,
				@EventIDColumnID		= 162728,
				@EventWeightingColumnID	= 162730,

				@CountEventsFromFieldID	= 162723,

				@PacemakerField		= 162714,
				@PacemakerRateField	= 162713,
				@SetGameTimerFiled	= 162715,
				@TimerHoursField	= 162716,
				@TimerMinutesField	= 162717,
				@EndGameNowField	= 162718,
				@UseEventDataField	= 162712,
				@UseAssignedToField	= 162719
	
	END
	ELSE IF @ClientID = 204
	BEGIN
	
		SELECT	@EventDataClientPersonnelFieldID = 158493,
				@EventDataClientPersonnelPageID = 17778,
				@PlayerNameColumnID = 158495,
				@ClientPersonnelIdFieldID = 158496,
				
				@EventDataEventFieldID = 158500,
				@EventDataEventPageID = 17778,
				@EventIDColumnID = 158497,
				@EventWeightingColumnID = 158499,
				
				@CountEventsFromFieldID = 158501,
				
				@PacemakerField = 158487,
				@PacemakerRateField = 158486,
				@SetGameTimerFiled = 158488,
				@TimerHoursField = 158489,
				@TimerMinutesField = 158490,
				@EndGameNowField = 158491,
				@UseEventDataField = 158485,
				@UseAssignedToField = 158484
	
	END
	
	
	DECLARE @UseAssignmentData BIT,
			@UseEventData BIT
			
	SELECT @UseAssignmentData = ValueInt
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = @UseAssignedToField
	
	SELECT @UseEventData = ValueInt
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = @UseEventDataField
	
	IF @UseAssignmentData = 1 OR @UseEventData = 1
	BEGIN
		SELECT tdvID.ValueInt AS ID, tdvName.DetailValue AS Name
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvID WITH (NOLOCK) ON r.TableRowID = tdvID.TableRowID AND tdvID.DetailFieldID = @ClientPersonnelIdFieldID
		INNER JOIN dbo.TableDetailValues tdvName WITH (NOLOCK) ON r.TableRowID = tdvName.TableRowID AND tdvName.DetailFieldID = @PlayerNameColumnID
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = @EventDataClientPersonnelFieldID
		AND r.DetailFieldPageID = @EventDataClientPersonnelPageID
		AND tdvID.ValueInt > 0 -- This prevents the row being returned before the data has been set	
		ORDER BY r.TableRowID
	END
	
	IF @UseAssignmentData = 1 AND @UseEventData = 1 /*Events applied to Leads assigned to Me*/ and @ClientID = 166 /*Only for 166 at the moment*/
	BEGIN
	
		;WITH InnerSql AS 
		(
			SELECT le.WhoCreated AS ID, le.LeadEventID, 
					CASE 
						WHEN ISNULL(tdvWeighting.ValueInt, 0) = 0 THEN 1 
						ELSE tdvWeighting.ValueInt 
					END AS Weighting
			FROM dbo.TableRows r WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdvID WITH (NOLOCK) ON r.TableRowID = tdvID.TableRowID AND tdvID.DetailFieldID = @ClientPersonnelIdFieldID
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.WhoCreated = tdvID.ValueInt
			INNER JOIN dbo.Lead l WITH (NOLOCK) on le.LeadID = l.LeadID
			INNER JOIN dbo.TableDetailValues tdvEventType WITH (NOLOCK) ON le.EventTypeID = tdvEventType.ValueInt AND tdvEventType.DetailFieldID = @EventIDColumnID
			INNER JOIN dbo.TableRows r2 WITH (NOLOCK) ON tdvEventType.TableRowID = r2.TableRowID
			INNER JOIN dbo.TableDetailValues tdvWeighting WITH (NOLOCK) ON r2.TableRowID = tdvWeighting.TableRowID AND tdvWeighting.DetailFieldID = @EventWeightingColumnID
			INNER JOIN dbo.MatterDetailValues mdvDateFrom WITH (NOLOCK) ON r.MatterID = mdvDateFrom.MatterID AND mdvDateFrom.DetailFieldID = @CountEventsFromFieldID
			WHERE r.MatterID = @MatterID
			AND r.DetailFieldID = @EventDataClientPersonnelFieldID
			AND r.DetailFieldPageID = @EventDataClientPersonnelPageID 
			AND r2.MatterID = @MatterID
			AND r2.DetailFieldID = @EventDataEventFieldID
			AND r2.DetailFieldPageID = @EventDataEventPageID 
			AND le.WhenCreated >= mdvDateFrom.ValueDate
			AND (@LastRowID = 0 OR le.LeadEventID > @LastRowID)
			AND tdvID.ValueInt > 0 -- This prevents the row being returned before the data has been set
			AND le.EventDeleted = 0	
			and exists ( SELECT * 
			             FROM LeadEvent sle WITH (NOLOCK) 
			             WHERE sle.LeadID = l.LeadID 
			             and sle.EventTypeID = 113462 /*PACK: Send Combined Pack to Queue*/
			             and sle.EventDeleted = 0
			             and sle.LeadEventID > le.LeadEventID  ) /*CS 2013-02-25 Didn't want to client-specific hack this one, but Seb's yelling at Ed and I don't have time to develop a config*/
		)
		
		SELECT ID, SUM(Weighting) AS Amount, (SELECT MAX(LeadEventID) FROM InnerSql) AS LastRowID
		FROM InnerSql
		GROUP BY ID
		
	END
	ELSE IF @UseAssignmentData = 1 -- We are counting lead assignments
	BEGIN
		;WITH InnerSql AS 
		(
			SELECT l.AssignedTo AS ID, l.AssignedDate
			FROM dbo.TableRows r WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdvID WITH (NOLOCK) ON r.TableRowID = tdvID.TableRowID AND tdvID.DetailFieldID = @ClientPersonnelIdFieldID
			INNER JOIN dbo.Lead l WITH (NOLOCK) ON tdvID.ValueInt = l.AssignedTo
			INNER JOIN dbo.MatterDetailValues mdvDateFrom WITH (NOLOCK) ON r.MatterID = mdvDateFrom.MatterID AND mdvDateFrom.DetailFieldID = @CountEventsFromFieldID
			WHERE r.MatterID = @MatterID
			AND r.DetailFieldID = @EventDataClientPersonnelFieldID
			AND r.DetailFieldPageID = @EventDataClientPersonnelPageID 
			--AND l.AssignedDate >= mdvDateFrom.ValueDate
			AND (@LastDate IS NULL AND l.AssignedDate >= mdvDateFrom.ValueDate) OR l.AssignedDate > @LastDate
			AND tdvID.ValueInt > 0 -- This prevents the row being returned before the data has been set
		)
		
		SELECT ID, COUNT(*) AS Amount, CONVERT(VARCHAR, (SELECT MAX(AssignedDate) FROM InnerSql), 121) AS LastDate
		FROM InnerSql
		GROUP BY ID
	END
	ELSE IF @UseEventData = 1 -- We are in event counting mode 
	BEGIN
		
		;WITH InnerSql AS 
		(
			SELECT le.WhoCreated AS ID, le.LeadEventID, 
					CASE 
						WHEN ISNULL(tdvWeighting.ValueInt, 0) = 0 THEN 1 
						ELSE tdvWeighting.ValueInt 
					END AS Weighting
			FROM dbo.TableRows r WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdvID WITH (NOLOCK) ON r.TableRowID = tdvID.TableRowID AND tdvID.DetailFieldID = @ClientPersonnelIdFieldID
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON tdvID.ValueInt = le.WhoCreated
			INNER JOIN dbo.TableDetailValues tdvEventType WITH (NOLOCK) ON le.EventTypeID = tdvEventType.ValueInt AND tdvEventType.DetailFieldID = @EventIDColumnID
			INNER JOIN dbo.TableRows r2 WITH (NOLOCK) ON tdvEventType.TableRowID = r2.TableRowID
			INNER JOIN dbo.TableDetailValues tdvWeighting WITH (NOLOCK) ON r2.TableRowID = tdvWeighting.TableRowID AND tdvWeighting.DetailFieldID = @EventWeightingColumnID
			INNER JOIN dbo.MatterDetailValues mdvDateFrom WITH (NOLOCK) ON r.MatterID = mdvDateFrom.MatterID AND mdvDateFrom.DetailFieldID = @CountEventsFromFieldID
			WHERE r.MatterID = @MatterID
			AND r.DetailFieldID = @EventDataClientPersonnelFieldID
			AND r.DetailFieldPageID = @EventDataClientPersonnelPageID 
			AND r2.MatterID = @MatterID
			AND r2.DetailFieldID = @EventDataEventFieldID
			AND r2.DetailFieldPageID = @EventDataEventPageID 
			AND le.WhenCreated >= mdvDateFrom.ValueDate
			AND (@LastRowID = 0 OR le.LeadEventID > @LastRowID)
			AND tdvID.ValueInt > 0 -- This prevents the row being returned before the data has been set
			AND le.EventDeleted = 0	
		)
		
		SELECT ID, SUM(Weighting) AS Amount, (SELECT MAX(LeadEventID) FROM InnerSql) AS LastRowID
		FROM InnerSql
		GROUP BY ID
	
	END
	ELSE -- We are using the raw data typed into the tables
	BEGIN
	
		SELECT DISTINCT llPlayer.LookupListItemID AS ID, llPlayer.ItemValue AS Name, r.TableRowID
		FROM dbo.TableRows r WITH (NOLOCK)
		INNER JOIN dbo.TableDetailValues tdvPlayer WITH (NOLOCK) ON r.TableRowID = tdvPlayer.TableRowID AND tdvPlayer.DetailFieldID = 151139
		INNER JOIN dbo.LookupListItems llPlayer WITH (NOLOCK) ON tdvPlayer.ValueInt = llPlayer.LookupListItemID
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = @ManualDataFieldID
		AND r.DetailFieldPageID = @ManualDataPageID
		ORDER BY r.TableRowID
		
		
		;WITH InnerSql AS 
		(
			SELECT tdvPlayer.ValueInt AS ID, tdvSale.ValueMoney AS Amount, r.TableRowID
			FROM dbo.TableRows r WITH (NOLOCK)
			INNER JOIN dbo.TableDetailValues tdvPlayer WITH (NOLOCK) ON r.TableRowID = tdvPlayer.TableRowID AND tdvPlayer.DetailFieldID = 151139
			INNER JOIN dbo.TableDetailValues tdvSale WITH (NOLOCK) ON r.TableRowID = tdvSale.TableRowID AND tdvSale.DetailFieldID = 151140
			WHERE r.MatterID = @MatterID
			AND r.DetailFieldID = @ManualDataFieldID
			AND r.DetailFieldPageID = @ManualDataPageID
			AND r.TableRowID > @LastRowID
			AND tdvPlayer.ValueInt > 0 -- This prevents the row being returned before the data has been set		
		)
		
		SELECT ID, SUM(Amount) AS Amount, (SELECT MAX(TableRowID) FROM InnerSql) AS LastRowID
		FROM InnerSql
		GROUP BY ID
		
	END
	
	DECLARE @SetTimer INT
	SELECT @SetTimer = ValueInt
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = @SetGameTimerFiled
	
	IF @SetTimer = 1
	BEGIN
		
		UPDATE dbo.MatterDetailValues
		SET DetailValue = 0
		WHERE MatterID = @MatterID
		AND DetailFieldID = @SetGameTimerFiled 
	
	END
	
	SELECT	mdvPacemaker.ValueInt AS Pacemaker, mdvPacemakerRate.ValueMoney AS PacemakerRate,
			@SetTimer AS SetTimer, mdvTimerHours.ValueInt AS TimerHours, mdvTimerMins.ValueInt AS TimerMinutes, mdvEndGame.ValueInt AS EndGameNow
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvPacemaker WITH (NOLOCK) ON m.MatterID = mdvPacemaker.MatterID AND mdvPacemaker.DetailFieldID = @PacemakerField
	INNER JOIN dbo.MatterDetailValues mdvPacemakerRate WITH (NOLOCK) ON m.MatterID = mdvPacemakerRate.MatterID AND mdvPacemakerRate.DetailFieldID = @PacemakerRateField
	INNER JOIN dbo.MatterDetailValues mdvTimerHours WITH (NOLOCK) ON m.MatterID = mdvTimerHours.MatterID AND mdvTimerHours.DetailFieldID = @TimerHoursField
	INNER JOIN dbo.MatterDetailValues mdvTimerMins WITH (NOLOCK) ON m.MatterID = mdvTimerMins.MatterID AND mdvTimerMins.DetailFieldID = @TimerMinutesField
	INNER JOIN dbo.MatterDetailValues mdvEndGame WITH (NOLOCK) ON m.MatterID = mdvEndGame.MatterID AND mdvEndGame.DetailFieldID = @EndGameNowField
	WHERE m.MatterID = @MatterID
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Motivator_GetPlayersAndData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Motivator_GetPlayersAndData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Motivator_GetPlayersAndData] TO [sp_executeall]
GO
