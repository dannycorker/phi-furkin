SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteClientAreaMailingList] @ClientAreaMailingListID int

AS

Delete From ClientAreaMailingLists
Where ClientAreaMailingListID = @ClientAreaMailingListID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientAreaMailingList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteClientAreaMailingList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientAreaMailingList] TO [sp_executeall]
GO
