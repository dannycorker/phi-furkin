SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_GetByScriptOptionID]
(

	@ScriptOptionID int   
)
AS


				SELECT
					[ScriptOptionID],
					[ClientID],
					[ClientPersonnelID],
					[ScriptEditorTheme],
					[ShowLineNumbers],
					[CollapseTopPanel],
					[CollapseLeftPanel],
					[CollapseRightPanel]
				FROM
					[dbo].[ScriptOption] WITH (NOLOCK) 
				WHERE
										[ScriptOptionID] = @ScriptOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_GetByScriptOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_GetByScriptOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_GetByScriptOptionID] TO [sp_executeall]
GO
