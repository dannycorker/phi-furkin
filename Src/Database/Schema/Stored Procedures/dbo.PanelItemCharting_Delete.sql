SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PanelItemCharting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_Delete]
(

	@PanelItemChartingID int   
)
AS


				DELETE FROM [dbo].[PanelItemCharting] WITH (ROWLOCK) 
				WHERE
					[PanelItemChartingID] = @PanelItemChartingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Delete] TO [sp_executeall]
GO
