SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 29 May 2007

-- Created By:  Chris, Jim
-- Purpose: Select all events for the current Lead Type, as long as the user has EDIT access.
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetGridByLeadTypeIDSecureEditOnly] 
(
	@LeadTypeID int,
	@UserID int
)
AS

	SET ANSI_NULLS OFF
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

		SELECT
		[EventTypeID],
		[EventTypeName],
		[EventTypeDescription],
		[EventSubtypeName],
		[LeadTypeID],
		[Enabled],
		[IsShared]
	FROM
		dbo.fnEventTypeShared(@ClientID) et 
	INNER JOIN 
		fnEventTypeSecure(@UserID, @LeadTypeID) f ON et.EventTypeID = f.objectid
	INNER JOIN 
		EventSubType st on et.EventSubTypeID = st.EventSubTypeID
	WHERE
		et.LeadTypeID = @LeadTypeID
		AND f.rightid > 1
	ORDER BY [EventTypeName]

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON













GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetGridByLeadTypeIDSecureEditOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetGridByLeadTypeIDSecureEditOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetGridByLeadTypeIDSecureEditOnly] TO [sp_executeall]
GO
