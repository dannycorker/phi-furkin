SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardsom
-- Create date: 2012-12-10
-- Description:	Gets the call history for a given lead
-- =============================================
CREATE PROCEDURE [dbo].[__GetCallHistoryByLeadID]

	@LeadID INT
	
AS
BEGIN

DECLARE @TableRowID INT,
			@DetailFieldPageID INT,
			@DetailFieldID INT,
			@364FieldID INT, -- RowID
			@365FieldID INT, -- ListID
			@366FieldID INT, -- Schema Name
			@367FieldID INT, -- CaseID
			@368FieldID INT, -- TransactionID
			@369FieldID INT, -- AgentsName
			@370FieldID INT, -- Approximate Date Time Of Call
			@ClientID INT,
			@LeadTypeID INT,
			@CustomerID INT			
	
	SELECT @CustomerID=CustomerID, @LeadTypeID = LeadTypeID, @LeadID=LeadID, @ClientID=ClientID FROM Lead WITH (NOLOCK) WHERE LeadID = @LeadID
	
	
	SELECT @364FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 364 -- RowID
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @365FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 365 -- ListID
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @366FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 366 -- Schema Name
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @367FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 367 -- CaseID
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @368FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 368 -- TransactionID
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @369FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 369 -- AgentsName
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @370FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 370 --  Approximate Date Time Of Call
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @DetailFieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 364
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID

	SELECT	tr.TableRowID,
			tdvRowID.DetailValue RowID,
			tdvListID.DetailValue ListID,
			tdvSchemaName.DetailValue SchemaName,
			tdvTransactionID.DetailValue TransactionID,
			tdvAgentsName.DetailValue AgentsName,
			tdvApproximate.DetailValue [Approximate Date Time Of Call],
			tdvCaseID.DetailValue CaseID
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRowID WITH (NOLOCK) ON tdvRowID.TableRowID = tr.TableRowID AND tdvRowID.DetailFieldID = @364FieldID
	INNER JOIN dbo.TableDetailValues tdvListID WITH (NOLOCK) ON tdvListID.TableRowID = tr.TableRowID AND tdvListID.DetailFieldID = @365FieldID
	INNER JOIN dbo.TableDetailValues tdvSchemaName WITH (NOLOCK) ON tdvSchemaName.TableRowID = tr.TableRowID AND tdvSchemaName.DetailFieldID = @366FieldID
	INNER JOIN dbo.TableDetailValues tdvTransactionID WITH (NOLOCK) ON tdvTransactionID.TableRowID = tr.TableRowID AND tdvTransactionID.DetailFieldID = @368FieldID
	INNER JOIN dbo.TableDetailValues tdvAgentsName WITH (NOLOCK) ON tdvAgentsName.TableRowID = tr.TableRowID AND tdvAgentsName.DetailFieldID = @369FieldID
	INNER JOIN dbo.TableDetailValues tdvApproximate WITH (NOLOCK) ON tdvApproximate.TableRowID = tr.TableRowID AND tdvApproximate.DetailFieldID = @370FieldID	
	INNER JOIN dbo.TableDetailValues tdvCaseID WITH (NOLOCK) ON tdvCaseID.TableRowID = tr.TableRowID AND tdvCaseID.DetailFieldID = @367FieldID
	WHERE tr.CustomerID = @CustomerID AND tr.DetailFieldID = @DetailFieldID AND tr.DetailFieldPageID = @DetailFieldPageID


END




GO
GRANT VIEW DEFINITION ON  [dbo].[__GetCallHistoryByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetCallHistoryByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetCallHistoryByLeadID] TO [sp_executeall]
GO
