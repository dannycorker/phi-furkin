SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












/*
----------------------------------------------------------------------------------------------------
-- Date Created: 03 January 2008

-- Created By:  Chris
-- Purpose: Get a list of substitutions that are valid for use in the Lead Type Page Title

----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetSubstitutionsForPageTitles]

AS

	SELECT SubstitutionsVariable, SubstitutionsDisplayName FROM Substitutions
	WHERE SubstitutionsID BETWEEN 1 AND 33
	OR SubstitutionsID IN (35,39,47,49,50,51)

















GO
GRANT VIEW DEFINITION ON  [dbo].[GetSubstitutionsForPageTitles] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSubstitutionsForPageTitles] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSubstitutionsForPageTitles] TO [sp_executeall]
GO
