SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnelOptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_Delete]
(

	@ClientPersonnelOptionID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnelOptions] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelOptionID] = @ClientPersonnelOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Delete] TO [sp_executeall]
GO
