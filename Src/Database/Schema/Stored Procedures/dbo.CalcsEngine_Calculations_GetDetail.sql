SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Returns details for a specific calculation
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_Calculations_GetDetail]
(
	@ClientID INT,
	@CalculationID INT
)


AS
BEGIN
	
	SET NOCOUNT ON;

	EXEC dbo.CalcsEngine_CalculationParameters_Get @ClientID, @CalculationID
	EXEC dbo.CalcsEngine_CalculationActions_Get @ClientID, @CalculationID
	
	
	SELECT 'Calculation Engine' AS Title, '#/calculations' AS Path, 1 AS Ord
	UNION ALL
	SELECT c.Name AS Title, '#/calculation/' + CAST(c.CalculationID AS VARCHAR) + '/details' AS Path, 2 AS Ord
	FROM dbo.CalcsEngine_Calculations c WITH (NOLOCK) 
	WHERE c.CalculationID = @CalculationID
	ORDER BY Ord
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Calculations_GetDetail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_Calculations_GetDetail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Calculations_GetDetail] TO [sp_executeall]
GO
