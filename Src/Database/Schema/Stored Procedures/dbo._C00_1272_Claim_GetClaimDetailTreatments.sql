SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-03-05
-- Description:	Gets all the treatment costs for a single claim row
-- Used by:		Popup called from DF 147723
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetClaimDetailTreatments] 
(
	@TableRowID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT	tdvClaimRowID.ValueInt AS ClaimTableRowID, tdvTreatment.ValueInt AS TreatmentID, llTreatment.ItemValue AS Treatment, tdvQuantity.ValueInt AS Quantity,
			tdvTotalCost.ValueMoney AS TotalCost
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvClaimRowID WITH (NOLOCK) ON r.TableRowID = tdvClaimRowID.TableRowID AND tdvClaimRowID.DetailFieldID = 162700 
	INNER JOIN dbo.TableDetailValues tdvTreatment WITH (NOLOCK) ON r.TableRowID = tdvTreatment.TableRowID AND tdvTreatment.DetailFieldID = 162625
	INNER JOIN dbo.LookupListItems llTreatment WITH (NOLOCK) ON tdvTreatment.ValueInt = llTreatment.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvQuantity WITH (NOLOCK) ON r.TableRowID = tdvQuantity.TableRowID AND tdvQuantity.DetailFieldID = 162626
	INNER JOIN dbo.TableDetailValues tdvTotalCost WITH (NOLOCK) ON r.TableRowID = tdvTotalCost.TableRowID AND tdvTotalCost.DetailFieldID = 162627
	WHERE tdvClaimRowID.ValueInt = @TableRowID
	AND r.DetailFieldID = 162699
	AND r.DetailFieldPageID = 16188
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetClaimDetailTreatments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetClaimDetailTreatments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetClaimDetailTreatments] TO [sp_executeall]
GO
