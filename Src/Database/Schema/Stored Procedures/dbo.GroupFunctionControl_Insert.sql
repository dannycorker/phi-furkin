SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the GroupFunctionControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControl_Insert]
(

	@GroupFunctionControlID int    OUTPUT,

	@ClientPersonnelAdminGroupID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants bit   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[GroupFunctionControl]
					(
					[ClientPersonnelAdminGroupID]
					,[ModuleID]
					,[FunctionTypeID]
					,[HasDescendants]
					,[RightID]
					,[LeadTypeID]
					)
				VALUES
					(
					@ClientPersonnelAdminGroupID
					,@ModuleID
					,@FunctionTypeID
					,@HasDescendants
					,@RightID
					,@LeadTypeID
					)
				-- Get the identity value
				SET @GroupFunctionControlID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControl_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_Insert] TO [sp_executeall]
GO
