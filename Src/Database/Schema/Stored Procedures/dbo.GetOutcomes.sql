SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Object:  Stored Procedure dbo.GetOutcomes    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[GetOutcomes] @ClientQuestionnaireID int

AS

Select OutcomeID, ClientQuestionnaireID, OutcomeName, OutcomeDescription, SmsWhenFound, MobileNumber, EmailCustomer, CustomersEmail, ThankYouPage, EmailFromAddress, SmsToCustomer, CustomersEmailSubject, ClientID, EmailClient, ClientEmail
from Outcomes
where ClientQuestionnaireID = @ClientQuestionnaireID




GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomes] TO [sp_executeall]
GO
