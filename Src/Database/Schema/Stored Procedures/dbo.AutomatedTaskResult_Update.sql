SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AutomatedTaskResult table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_Update]
(

	@AutomatedTaskResultID int   ,

	@TaskID int   ,

	@ClientID int   ,

	@RunDate datetime   ,

	@Description varchar (2500)  ,

	@RecordCount int   ,

	@Complete bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AutomatedTaskResult]
				SET
					[TaskID] = @TaskID
					,[ClientID] = @ClientID
					,[RunDate] = @RunDate
					,[Description] = @Description
					,[RecordCount] = @RecordCount
					,[Complete] = @Complete
				WHERE
[AutomatedTaskResultID] = @AutomatedTaskResultID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Update] TO [sp_executeall]
GO
