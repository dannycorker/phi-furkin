SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 09/08/2016
-- Description:	Creates a CardTransaction Record from ThirdPartyFields
--				Gets the Amount from the thirdparty field, this can be overwritten by the PurchasedProductPaymentSchedule amount if the id has been set,
--				this in turn can be overwritten by the amount in the CustomerProductPaymentSchedule if the id has been set.
--	Modified By PR On 23/08/2016 : Obfuscated Card Number In Detail Field.
--	Modified By PR On 25/10/2016 : removed un-necessary logging select
--  Modified By PR On 28/10/2016 : added customer ref num
--  Modified By PR On 02/11/2016 : added card type id
--  Modified By DCM On 09/01/2017 : made when created full datetime
--  Modified By DCM On 09/01/2017 : added creating customer ledger entry
-- 2019-10-07 CPS for JIRA LPC-28  | Store CustomerPaymentScheduleID if one exists
-- 2019-12-19 CPS for JIRA LPC-248 | Added CardTransactionPolicy update to allow the CC Payer batch to complete following arrears collection
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__CreateFromThirdPartyFields]

	@ClientID INT,
	@CaseID INT, 
	@WhoCreated INT,
	@EventTypePaymentGatewayID INT,
	@ClientPaymentGatewayID INT,
	@TransactionTypeID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	-- Gets the customer, lead, matter identities and the leadtype
	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @DetailFieldID INT
	SELECT @LeadID=cs.LeadID FROM Cases cs WITH (NOLOCK) WHERE cs.CaseID = @CaseID
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@LeadID
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.CaseID=@CaseID
	
	
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created

	DECLARE @LogEntry VARCHAR(MAX),@MethodName VARCHAR(200)	
	--4460	Amount
	DECLARE @Amount_DV VARCHAR(2000), @Amount NUMERIC(18,2), @AmountNet NUMERIC(18,2), @AmountVat NUMERIC(18,2)
	SELECT @Amount_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4460)		
		
	BEGIN TRY  
		SELECT @Amount = CAST(@Amount_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		--SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Amount'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		--EXEC dbo._C00_LogIt 'Error', 'CardTransaction__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 

	--4452	PurchasedProductPaymentScheduleID
	DECLARE @PurchasedProductPaymentScheduleID_DV VARCHAR(2000), @IsInt_PurchasedProductPaymentScheduleID BIT, @PurchasedProductPaymentScheduleID INT
	SELECT @PurchasedProductPaymentScheduleID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4452)
	SELECT @IsInt_PurchasedProductPaymentScheduleID = dbo.fnIsInt(@PurchasedProductPaymentScheduleID_DV)
	IF(@IsInt_PurchasedProductPaymentScheduleID=1)
	BEGIN
		SELECT @PurchasedProductPaymentScheduleID = CAST(@PurchasedProductPaymentScheduleID_DV AS INT)
		-- if there is a valid customer payment schedule id then overide the following values:-
		-- Amount
		SELECT @Amount=pps.PaymentGross, @AmountNet=pps.PaymentNet, @AmountVAT=pps.PaymentVAT
		FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) WHERE pps.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID

	END 

	--4453	CustomerPaymentScheduleID
	DECLARE @CustomerPaymentScheduleID_DV VARCHAR(2000), @IsInt_CustomerPaymentScheduleID BIT, @CustomerPaymentScheduleID INT
	SELECT @CustomerPaymentScheduleID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4453)
	SELECT @IsInt_CustomerPaymentScheduleID = dbo.fnIsInt(@CustomerPaymentScheduleID_DV)
	IF(@IsInt_CustomerPaymentScheduleID=1)
	BEGIN
		SELECT @CustomerPaymentScheduleID = CAST(@CustomerPaymentScheduleID_DV AS INT)
		-- if there is a valid customer payment schedule id then overide the following values:-
		-- Amount
		IF(@CustomerPaymentScheduleID>0)
		BEGIN
			SELECT @Amount=cps.PaymentGross, @AmountNet=cps.PaymentNet, @AmountVAT=cps.PaymentVAT
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) WHERE cps.CustomerPaymentScheduleID=@CustomerPaymentScheduleID

			SELECT @DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4396)
			EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @CustomerPaymentScheduleID, @MatterID /*CustomerPaymentScheduleID*/
		END
	END
	
	--4454	ClientPaymentGatewayID -- passed in
	--DECLARE @ClientPaymentGatewayID_DV VARCHAR(2000), @IsInt_ClientPaymentGatewayID BIT, @ClientPaymentGatewayID INT
	--SELECT @ClientPaymentGatewayID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4454)
	--SELECT @IsInt_ClientPaymentGatewayID = dbo.fnIsInt(@ClientPaymentGatewayID_DV)
	--IF(@IsInt_ClientPaymentGatewayID=1)
	--BEGIN
	--	SELECT @ClientPaymentGatewayID = CAST(@ClientPaymentGatewayID_DV AS INT)
	--END	
	
	--4455	PaymentTypeID
	DECLARE @PaymentTypeID_DV VARCHAR(2000), @IsInt_PaymentTypeID BIT, @PaymentTypeID INT
	SELECT @PaymentTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4455)
	SELECT @IsInt_PaymentTypeID = dbo.fnIsInt(@PaymentTypeID_DV)
	IF(@IsInt_PaymentTypeID=1)
	BEGIN
		SELECT @PaymentTypeID = CAST(@PaymentTypeID_DV AS INT)
	END	

	--4456	PreValidate
	DECLARE @PreValidate_DV VARCHAR(2000), @IsBit_PreValidate BIT, @PreValidate BIT
	SELECT @PreValidate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4456)
	SELECT @IsBit_PreValidate = dbo.fnIsBit(@PreValidate_DV)
	IF(@IsBit_PreValidate=1)
	BEGIN
		SELECT @PreValidate = CAST(@PreValidate_DV AS BIT)
	END		
	
	--4457	OrderID
	DECLARE @OrderID_DV VARCHAR(2000)
	SELECT @OrderID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4457)	


	--4458	ReferenceNumber
	DECLARE @ReferenceNumber_DV VARCHAR(2000)
	SELECT @ReferenceNumber_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4458)	
	
	--4459	Description
	DECLARE @Description_DV VARCHAR(2000)
	SELECT @Description_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4459)	

	--4461	CurrencyID
	DECLARE @CurrencyID_DV VARCHAR(2000), @IsInt_CurrencyID BIT, @CurrencyID INT
	SELECT @CurrencyID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4461)
	SELECT @IsInt_CurrencyID = dbo.fnIsInt(@CurrencyID_DV)
	IF(@IsInt_CurrencyID=1)
	BEGIN
		SELECT @CurrencyID = CAST(@CurrencyID_DV AS INT)
	END	
	
	--4462	FirstName
	DECLARE @FirstName_DV VARCHAR(2000)
	SELECT @FirstName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4462)	
		
	--4463	LastName
	DECLARE @LastName_DV VARCHAR(2000)
	SELECT @LastName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4463)	
		
	--4464	CardName
	DECLARE @CardName_DV VARCHAR(2000)
	SELECT @CardName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4464)	

	--4465	Number
	DECLARE @Number_DV VARCHAR(2000)
	SELECT @Number_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4465)
	
	--Obfuscate Card Number
	DECLARE @CardMaskingMethodID INT
	SELECT @CardMaskingMethodID=CardMaskingMethodID FROM EventTypePaymentGateway WITH (NOLOCK) WHERE EventTypePaymentGatewayID=@EventTypePaymentGatewayID		
	EXEC dbo.ObfuscateCardNumber @CardMaskingMethodID,@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4465
		
	--4466	ExpiryMonth
	DECLARE @ExpiryMonth_DV VARCHAR(2000)
	SELECT @ExpiryMonth_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4466)	
		
	--4467	ExpiryYear
	DECLARE @ExpiryYear_DV VARCHAR(2000)
	SELECT @ExpiryYear_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4467)	

	--4468	Code  CV2 / CVV2 code for enhanced fraud control. This is the 3 or 4 digit value on credit cards that helps to ensure the person submitting the credit card details is the actual card holder
	DECLARE @Code_DV VARCHAR(2000)
	SELECT @Code_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4468)	
	
	--4469	AuthCode
	DECLARE @AuthCode_DV VARCHAR(2000)
	SELECT @AuthCode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4469)	
	
	--4470	AvsCode
	DECLARE @AvsCode_DV VARCHAR(2000)
	SELECT @AvsCode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4470)	
	
	--4471	AvsIndicator
	DECLARE @AvsIndicator_DV VARCHAR(2000)
	SELECT @AvsIndicator_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4471)		
	
	--4472	Address
	DECLARE @Address_DV VARCHAR(2000)
	SELECT @Address_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4472)		
		
	--4473	Address2
	DECLARE @Address2_DV VARCHAR(2000)
	SELECT @Address2_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4473)		
		
	--4474	City
	DECLARE @City_DV VARCHAR(2000)
	SELECT @City_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4474)		
	
	--4475	StateProvince
	DECLARE @StateProvince_DV VARCHAR(2000)
	SELECT @StateProvince_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4475)		

	--4476	Country
	DECLARE @Country_DV VARCHAR(2000)
	SELECT @Country_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4476)		
	
	--4477	ZipPostal
	DECLARE @ZipPostal_DV VARCHAR(2000)
	SELECT @ZipPostal_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4477)		
	
	--4478	BankAccountType
	DECLARE @BankAccountType_DV VARCHAR(2000)
	SELECT @BankAccountType_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4478)		
		
	--4479	BankCode
	DECLARE @BankCode_DV VARCHAR(2000)
	SELECT @BankCode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4479)		
	
	--4480	BankName
	DECLARE @BankName_DV VARCHAR(2000)
	SELECT @BankName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4480)			
	
	--4481	Company
	DECLARE @Company_DV VARCHAR(2000)
	SELECT @Company_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4481)			
		
	--4482	GatewayCustomerID
	DECLARE @GatewayCustomerID_DV VARCHAR(2000)
	SELECT @GatewayCustomerID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4482)			
	
	--4483	TransactionID
	DECLARE @TransactionID_DV VARCHAR(2000)
	SELECT @TransactionID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4483)			
		
	--4484	TransactionTypeID
	--DECLARE @TransactionTypeID_DV VARCHAR(2000), @IsInt_TransactionTypeID BIT, @TransactionTypeID INT
	--SELECT @TransactionTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4484)
	--SELECT @IsInt_TransactionTypeID = dbo.fnIsInt(@TransactionTypeID_DV)
	--IF(@IsInt_TransactionTypeID=1)
	--BEGIN
	--	SELECT @TransactionTypeID = CAST(@TransactionTypeID_DV AS INT)
	--END	
		
	--4485	Email
	DECLARE @Email_DV VARCHAR(2000)
	SELECT @Email_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4485)			

	--4486	Fax
	DECLARE @Fax_DV VARCHAR(2000)
	SELECT @Fax_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4486)			
	
	--4487	Phone
	DECLARE @Phone_DV VARCHAR(2000)
	SELECT @Phone_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4487)			
	
	--4488	Partner
	DECLARE @Partner_DV VARCHAR(2000)
	SELECT @Partner_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4488)			
	
	--4489	Certificate
	DECLARE @Certificate_DV VARCHAR(2000)
	SELECT @Certificate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4489)			
	
	--4490	ClientIP
	DECLARE @ClientIP_DV VARCHAR(2000)
	SELECT @ClientIP_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4490)			
	
	--4491	Referrer
	DECLARE @Referrer_DV VARCHAR(2000)
	SELECT @Referrer_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4491)			
		
	--4492	ErrorCode
	DECLARE @ErrorCode_DV VARCHAR(2000)
	SELECT @ErrorCode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4492)			
		
	--4493	ErrorMessage
	DECLARE @ErrorMessage_DV VARCHAR(2000)
	SELECT @ErrorMessage_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4493)			

	--4505	ObjectID
	DECLARE @ObjectID_DV VARCHAR(2000), @IsInt_ObjectID BIT, @ObjectID INT
	SELECT @ObjectID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4505)
	SELECT @IsInt_ObjectID = dbo.fnIsInt(@ObjectID_DV)
	IF(@IsInt_ObjectID=1)
	BEGIN
		SELECT @ObjectID = CAST(@ObjectID_DV AS INT)
	END	
	
	--4506	ObjectTypeID
	DECLARE @ObjectTypeID_DV VARCHAR(2000), @IsInt_ObjectTypeID BIT, @ObjectTypeID INT
	SELECT @ObjectTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4506)
	SELECT @IsInt_ObjectTypeID = dbo.fnIsInt(@ObjectTypeID_DV)
	IF(@IsInt_ObjectTypeID=1)
	BEGIN
		SELECT @ObjectTypeID = CAST(@ObjectTypeID_DV AS INT)
	END	
	
	--4527	CardTypeID
	DECLARE @CardTypeID_DV VARCHAR(2000), @IsInt_CardTypeID BIT, @CardTypeID INT
	SELECT @CardTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4527)
	SELECT @IsInt_CardTypeID = dbo.fnIsInt(@CardTypeID_DV)
	IF(@IsInt_CardTypeID=1)
	BEGIN
		SELECT @CardTypeID = CAST(@CardTypeID_DV AS INT)
	END	
	
	--4524	57	CustomerRefNum
	DECLARE @CustomerRefNum_DV VARCHAR(2000)
	SELECT @CustomerRefNum_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4524)			

	DECLARE @CardTransactionID INT = NULL, @CustomerLedgerID INT = NULL
	--IF ((@Amount IS NOT NULL) AND (@Amount>0)) -- amount must be greater than zero  before being passed to the payement processor
	--BEGIN
	
		INSERT INTO CardTransaction (ClientID, CustomerID, ObjectID, ObjectTypeID, PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID, ClientPaymentGatewayID, PaymentTypeID, PreValidate, OrderID, ReferenceNumber, Description, Amount, CurrencyID, FirstName, LastName, CardName, Number, ExpiryMonth, ExpiryYear, Code, AuthCode, AvsCode, AvsIndicator, Address, Address2, City, StateProvince, Country, ZipPostal, BankAccountType, BankCode, BankName, Company, GatewayCustomerID, TransactionID, TransactionTypeID, Email, Fax, Phone, Partner, Certificate, ClientIP, Referrer, ErrorCode, ErrorMessage, WhoCreated, WhenCreated, CardTypeID, CustomerRefNum)	
		VALUES (@ClientID, @CustomerID, @ObjectID, @ObjectTypeID, @PurchasedProductPaymentScheduleID, @CustomerPaymentScheduleID, @ClientPaymentGatewayID, @PaymentTypeID, @PreValidate, @OrderID_DV, @ReferenceNumber_DV, @Description_DV, @Amount, @CurrencyID, @FirstName_DV, @LastName_DV, @CardName_DV, @Number_DV, @ExpiryMonth_DV, @ExpiryYear_DV, @Code_DV, @AuthCode_DV, @AvsCode_DV, @AvsIndicator_DV, @Address_DV, @Address2_DV, @City_DV, @StateProvince_DV, @Country_DV, @ZipPostal_DV, @BankAccountType_DV, @BankCode_DV, @BankName_DV, @Company_DV, @GatewayCustomerID_DV, @TransactionID_DV, @TransactionTypeID, @Email_DV, @Fax_DV, @Phone_DV, @Partner_DV, @Certificate_DV, @ClientIP_DV, @Referrer_DV, @ErrorCode_DV, @ErrorMessage_DV, @WhoCreated, dbo.fn_GetDate_Local(), @CardTypeID, @CustomerRefNum_DV)

		SELECT @CardTransactionID = SCOPE_IDENTITY()		

		-- Insert Customer Ledger Entry		
		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		VALUES (@ClientID, @CustomerID, NULL, NULL, NULL, @WhenCreated, CAST(@CardTransactionID AS VARCHAR), NULL, @AmountNet, @AmountVat, @Amount, NULL, @ObjectID, @ObjectTypeID, @CardTransactionID, NULL, @WhoCreated, dbo.fn_GetDate_Local())		
		 	
	 	SELECT @CustomerLedgerID = SCOPE_IDENTITY()	
		 		
		UPDATE PurchasedProductPaymentSchedule 
		Set PaymentStatusID = 2, ReconciledDate = @WhenCreated, CustomerLedgerID = @CustomerLedgerID
		From PurchasedProductPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE CustomerPaymentSchedule  
		Set PaymentStatusID = 2, ReconciledDate = @WhenCreated, CustomerLedgerID = @CustomerLedgerID
		From CustomerPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		-- 2019-12-19 CPS for JIRA LPC-248 | Added CardTransactionPolicy update to allow the CC Payer batch to complete following arrears collection
		INSERT INTO CardTransactionPolicy (CardTransactionID,PAMatterID,WhoCreated,WhenCreated)
		VALUES (@CardTransactionID, @MatterID, @WhoCreated, @WhenCreated) 

		--reconcile and post to payment processor is done in the event
		--the event updates the card transaction with pass/fail information from the payment processor
		--Hard exceptions are thrown in the event if the amount is zero or the payment processor declines or fails the payment

		--PRINT @CardTransactionID
	--END
	

	DECLARE @CardTransactionMapID INT = NULL
	IF ((@Amount IS NOT NULL) AND (@Amount>0)) -- amount must be greater than zero  before being passed to the payement processor
	BEGIN
		-- Production or Test
		DECLARE @DataBaseName VARCHAR(100) = DB_NAME()
		IF(@DataBaseName='Aquarius603') -- TODO Change to live environment for LTP
		BEGIN
			EXEC [AquariusMaster].dbo.CardTransactionMap__Insert @CardTransactionID, @ClientID, @DataBaseName, 'TSTDB1', @CardTransactionMapID OUTPUT	
		END
		ELSE
		BEGIN
			EXEC [AquariusMaster].dbo.CardTransactionMap__Insert @CardTransactionID, @ClientID, @DataBaseName, 'TSTDB1', @CardTransactionMapID OUTPUT	
		END

	END

	SELECT [CardTransactionID], [ClientID], [CustomerID], [ObjectID], [ObjectTypeID], [PurchasedProductPaymentScheduleID], [CustomerPaymentScheduleID], [ClientPaymentGatewayID], [PaymentTypeID], [PreValidate]
	, NULLIF(dbo.fnExtractNumber(OrderID),'') AS [OrderID] 
	, [ReferenceNumber], [Description], [Amount], [CurrencyID], [FirstName], [LastName], [CardName], [Number], [ExpiryMonth], [ExpiryYear], [Code], [AuthCode], [AvsCode], [AvsIndicator], [Address], [Address2], [City], [StateProvince], [Country], [ZipPostal], [BankAccountType], [BankCode], [BankName], [Company], [GatewayCustomerID], [TransactionID], [TransactionTypeID], [Email], [Fax], [Phone], [Partner], [Certificate], [ClientIP], [Referrer], [ErrorCode], [ErrorMessage], [WhoCreated], [WhenCreated], [CardTypeID], [StatusMsg], [CustomerRefNum], [ParentID], [Filename], [AccountID], [OriginalAmount]
	, @CardTransactionMapID CardTransactionMapID 
	FROM CardTransAction ct WITH (NOLOCK) 
	WHERE CardTransactionID = @CardTransactionID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__CreateFromThirdPartyFields] TO [sp_executeall]
GO
