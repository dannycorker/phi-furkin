SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-21
-- Description:	Writes the payment response to the payment table and the
-- standard payment response fields
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WritePaymentResponse]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT,
	@PaymentResponseStatus VARCHAR(2000),
	@PaymentResponseStatusDetail VARCHAR(2000),
	@PaymentResponseVPSTxId VARCHAR(2000),
	@PaymentResponseSecurityKey VARCHAR(2000),
	@PaymentResponseTxAuthNo VARCHAR(2000),
	@PaymentResponseCV2Status VARCHAR(2000),
	@PaymentResponseAddressResult VARCHAR(2000),
	@PaymentResponsePostCodeResult VARCHAR(2000),
	@PaymentResponseCV2Result VARCHAR(2000),
	@PaymentResponseThreeDSecure VARCHAR(2000),
	@PaymentResponseCaav VARCHAR(2000)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TransactionDate DATETIME
	SET @TransactionDate = CURRENT_TIMESTAMP

	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 523 THEN @PaymentResponseStatus
																	WHEN 524 THEN @PaymentResponseStatusDetail
																	WHEN 525 THEN @PaymentResponseVPSTxId
																	WHEN 526 THEN @PaymentResponseSecurityKey
																	WHEN 527 THEN @PaymentResponseTxAuthNo
																	WHEN 528 THEN @PaymentResponseCV2Status
																	WHEN 529 THEN @PaymentResponseAddressResult
																	WHEN 530 THEN @PaymentResponsePostCodeResult
																	WHEN 531 THEN @PaymentResponseCV2Result
																	WHEN 532 THEN @PaymentResponseThreeDSecure
																	WHEN 533 THEN @PaymentResponseCaav
																	WHEN 626 THEN CONVERT(VARCHAR,@TransactionDate,120)
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 626)
	
	DECLARE @PaymentResponseStatusDetailFieldID INT
	SELECT @PaymentResponseStatusDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=582
	EXEC _C00_SimpleValueIntoField @PaymentResponseStatusDetailFieldID, @PaymentResponseStatus, @CustomerID
	
	DECLARE @PaymentResponseStatusDetailDetailFieldID INT
	SELECT @PaymentResponseStatusDetailDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=583
	EXEC _C00_SimpleValueIntoField @PaymentResponseStatusDetailDetailFieldID, @PaymentResponseStatusDetail, @CustomerID

	DECLARE @PaymentResponseVPSTxIdDetailFieldID INT
	SELECT @PaymentResponseVPSTxIdDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=584
	EXEC _C00_SimpleValueIntoField @PaymentResponseVPSTxIdDetailFieldID, @PaymentResponseVPSTxId, @CustomerID

	DECLARE @PaymentResponseSecurityKeyDetailFieldID INT
	SELECT @PaymentResponseSecurityKeyDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=585
	EXEC _C00_SimpleValueIntoField @PaymentResponseSecurityKeyDetailFieldID, @PaymentResponseSecurityKey, @CustomerID
	
	DECLARE @PaymentResponseTxAuthNoDetailFieldID INT
	SELECT @PaymentResponseTxAuthNoDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=586
	EXEC _C00_SimpleValueIntoField @PaymentResponseTxAuthNoDetailFieldID, @PaymentResponseTxAuthNo, @CustomerID

	DECLARE @PaymentResponseCV2StatusDetailFieldID INT
	SELECT @PaymentResponseCV2StatusDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=587
	EXEC _C00_SimpleValueIntoField @PaymentResponseCV2StatusDetailFieldID, @PaymentResponseCV2Status, @CustomerID

	DECLARE @PaymentResponseAddressResultDetailFieldID INT
	SELECT @PaymentResponseAddressResultDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=588	
	EXEC _C00_SimpleValueIntoField @PaymentResponseAddressResultDetailFieldID, @PaymentResponseAddressResult, @CustomerID	
	
	DECLARE @PaymentResponsePostCodeResultDetailFieldID INT
	SELECT @PaymentResponsePostCodeResultDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=589
	EXEC _C00_SimpleValueIntoField @PaymentResponsePostCodeResultDetailFieldID, @PaymentResponsePostCodeResult, @CustomerID	
	
	DECLARE @PaymentResponseCV2ResultDetailFieldID INT
	SELECT @PaymentResponseCV2ResultDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=590
	EXEC _C00_SimpleValueIntoField @PaymentResponseCV2ResultDetailFieldID, @PaymentResponseCV2Result, @CustomerID
	
	DECLARE @PaymentResponseThreeDSecureDetailFieldID INT
	SELECT @PaymentResponseThreeDSecureDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=591					
	EXEC _C00_SimpleValueIntoField @PaymentResponseThreeDSecureDetailFieldID, @PaymentResponseThreeDSecure, @CustomerID
	
	DECLARE @PaymentResponseCaavDetailFieldID INT
	SELECT @PaymentResponseCaavDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=592						
	EXEC _C00_SimpleValueIntoField @PaymentResponseCaavDetailFieldID, @PaymentResponseCaav, @CustomerID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WritePaymentResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WritePaymentResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WritePaymentResponse] TO [sp_executeall]
GO
