SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_Handle
-- =============================================
CREATE PROCEDURE [dbo].[API_Handle]
	@QID UNIQUEIDENTIFIER,
	@Handled VARCHAR(50)
AS
BEGIN

	DECLARE @XmlOut NVARCHAR(MAX) = '<response></response>'

	UPDATE API_Queue
		SET HandleXml = @XmlOut,
			HandleDT = SYSDATETIME(),
			QueueByteSize = ISNULL(QueueByteSize, 0) + DATALENGTH(ISNULL(@XmlOut,''))
	WHERE	QueueID = @QID
	AND		HandleDT IS NULL
	
	SELECT @QID QueueID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Handle] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Handle] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Handle] TO [sp_executeall]
GO
