SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientQuestionnairesVisited table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisited_Insert]
(

	@ClientQuestionnairesVisitedID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@VisitedDate datetime   ,

	@IPAddress varchar (50)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[ClientQuestionnairesVisited]
					(
					[ClientQuestionnaireID]
					,[VisitedDate]
					,[IPAddress]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@VisitedDate
					,@IPAddress
					,@ClientID
					)
				-- Get the identity value
				SET @ClientQuestionnairesVisitedID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisited_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_Insert] TO [sp_executeall]
GO
