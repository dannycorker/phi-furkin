SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the IncomingPostEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEvent_GetByIncomingPostEventID]
(

	@IncomingPostEventID int   
)
AS


				SELECT
					[IncomingPostEventID],
					[EventDateTime],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID]
				FROM
					[dbo].[IncomingPostEvent] WITH (NOLOCK) 
				WHERE
										[IncomingPostEventID] = @IncomingPostEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_GetByIncomingPostEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEvent_GetByIncomingPostEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_GetByIncomingPostEventID] TO [sp_executeall]
GO
