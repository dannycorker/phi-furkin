SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-08-2015
-- Description:	Gets a chat by Chat Guid
-- =============================================
CREATE PROCEDURE [dbo].[Chat__GetByChatGuid]
	@ClientID INT,
	@ChatGuid VARCHAR(50)
AS
BEGIN
	
	
	SET NOCOUNT ON;

	SELECT * FROM Chat WITH (NOLOCK) 
	WHERE ChatGuid = @ChatGuid AND ClientID=@ClientID
    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__GetByChatGuid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chat__GetByChatGuid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__GetByChatGuid] TO [sp_executeall]
GO
