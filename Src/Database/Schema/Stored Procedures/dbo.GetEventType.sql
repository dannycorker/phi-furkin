SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetEventType] @EventTypeID int


AS

SELECT     EventTypeID, ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort, 
			FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, 
			AquariumEventAfterEvent, EventSubtypeID
FROM         dbo.EventType
WHERE     (EventTypeID = @EventTypeID)





GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventType] TO [sp_executeall]
GO
