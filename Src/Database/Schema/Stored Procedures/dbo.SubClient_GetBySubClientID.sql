SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SubClient table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubClient_GetBySubClientID]
(

	@SubClientID int   
)
AS


				SELECT
					[SubClientID],
					[ClientID],
					[CompanyName],
					[ClientTypeID],
					[LanguageID],
					[CountryID],
					[WhenCreated]
				FROM
					[dbo].[SubClient] WITH (NOLOCK) 
				WHERE
										[SubClientID] = @SubClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_GetBySubClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubClient_GetBySubClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_GetBySubClientID] TO [sp_executeall]
GO
