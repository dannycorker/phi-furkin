SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetClientAreaMailingList] @ClientAreaMailingListID int

AS

Select ClientAreaMailingLists.Name, Email, PostCode, OutcomeID, YellowPagesAreaCode, ClientID, ClientQuestionnaireID, ClientAreaMailingListID, OnHold
From ClientAreaMailingLists
Where ClientAreaMailingListID = @ClientAreaMailingListID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientAreaMailingList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingList] TO [sp_executeall]
GO
