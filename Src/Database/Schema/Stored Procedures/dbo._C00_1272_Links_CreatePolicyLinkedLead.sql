SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-29
-- Description:	Creates a linked policy lead from the claim side
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Links_CreatePolicyLinkedLead] 
(
	@ClaimLeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadTypeRelationshipID INT
	
	DECLARE @ClientID INT,
			@CustomerID INT,
			@ClaimLeadTypeID INT
	SELECT @ClientID = ClientID, @CustomerID = CustomerID, @ClaimLeadTypeID = LeadTypeID
	FROM dbo.Lead WITH (NOLOCK)
	WHERE LeadID = @ClaimLeadID
	
	DECLARE @PolicyLeadTypeID INT
	SELECT @PolicyLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare s WITH (NOLOCK)  
	WHERE ClientID = @ClientID
	AND s.SharedFrom = 1273
	
	DECLARE @AquariumAutomationID INT
	SELECT @AquariumAutomationID = dbo.fn_C00_GetAutomationUser(@ClientID)
	
	DECLARE @PolicyLeadID INT	
	EXEC @PolicyLeadID = dbo._C00_CreateNewLeadForCust @CustomerID, @PolicyLeadTypeID, @ClientID, @AquariumAutomationID, 1
	
	INSERT INTO dbo.LeadTypeRelationship (FromLeadTypeID, ToLeadTypeID, FromLeadID, ToLeadID, ClientID)
	VALUES (@PolicyLeadTypeID, @ClaimLeadTypeID, @PolicyLeadID, @ClaimLeadID, @ClientID)
	
	SELECT @LeadTypeRelationshipID = SCOPE_IDENTITY()
	
	EXEC _C00_LogIt 'Info', '_C600_LeadTypeRelationships', '_C00_1272_Links_CreatePolicyLinkedLead', @LeadTypeRelationshipID, 58540 /*Cathal Sherry*/

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Links_CreatePolicyLinkedLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Links_CreatePolicyLinkedLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Links_CreatePolicyLinkedLead] TO [sp_executeall]
GO
