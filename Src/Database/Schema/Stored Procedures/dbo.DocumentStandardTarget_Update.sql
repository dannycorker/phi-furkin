SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_Update]
(

	@DocumentStandardTargetID int   ,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@TemplateTypeID int   ,

	@Notes varchar (250)  ,

	@IsSpecial bit   ,

	@ExcelSheetLocation varchar (50)  ,

	@DocumentTypeVersionID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentStandardTarget]
				SET
					[ClientID] = @ClientID
					,[DocumentTypeID] = @DocumentTypeID
					,[Target] = @Target
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
					,[TemplateTypeID] = @TemplateTypeID
					,[Notes] = @Notes
					,[IsSpecial] = @IsSpecial
					,[ExcelSheetLocation] = @ExcelSheetLocation
					,[DocumentTypeVersionID] = @DocumentTypeVersionID
				WHERE
[DocumentStandardTargetID] = @DocumentStandardTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Update] TO [sp_executeall]
GO
