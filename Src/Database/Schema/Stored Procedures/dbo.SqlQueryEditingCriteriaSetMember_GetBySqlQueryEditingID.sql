SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteriaSetMember table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingCriteriaSetMemberID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingCriteriaSetID],
					[SqlQueryEditingColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryEditingCriteriaSetMember] WITH (NOLOCK) 
				WHERE
					[SqlQueryEditingID] = @SqlQueryEditingID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingID] TO [sp_executeall]
GO
