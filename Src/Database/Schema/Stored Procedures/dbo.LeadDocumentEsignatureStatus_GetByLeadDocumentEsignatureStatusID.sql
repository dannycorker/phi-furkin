SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDocumentEsignatureStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_GetByLeadDocumentEsignatureStatusID]
(

	@LeadDocumentEsignatureStatusID int   
)
AS


				SELECT
					[LeadDocumentEsignatureStatusID],
					[ClientID],
					[LeadDocumentID],
					[ElectronicSignatureDocumentKey],
					[DocumentName],
					[SentTo],
					[DateSent],
					[EsignatureStatusID],
					[StatusCheckedDate]
				FROM
					[dbo].[LeadDocumentEsignatureStatus] WITH (NOLOCK) 
				WHERE
										[LeadDocumentEsignatureStatusID] = @LeadDocumentEsignatureStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_GetByLeadDocumentEsignatureStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_GetByLeadDocumentEsignatureStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_GetByLeadDocumentEsignatureStatusID] TO [sp_executeall]
GO
