SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-05-20
-- Description:	WritePaymentRequest
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WritePaymentRequest]
	@ClientID INT,
	@CustomerID INT,
	@PaymentRequestVendorTxCode VARCHAR(2000),
	@PaymentRequestAmount VARCHAR(2000),
	@PaymentRequestCurrency VARCHAR(2000),
	@PaymentRequestDescription VARCHAR(2000),
	@PaymentRequestCardHolder VARCHAR(2000),
	@PaymentRequestCardNumber VARCHAR(2000),
	@PaymentRequestExpiryDate VARCHAR(2000),
	@PaymentRequestCV2 VARCHAR(2000),
	@PaymentRequestCardType VARCHAR(2000),
	@PaymentRequestTxType VARCHAR(2000),
	@PaymentRequestBillingSurname VARCHAR(2000),
	@PaymentRequestBillingFirstnames VARCHAR(2000),
	@PaymentRequestBillingAddress1 VARCHAR(2000),
	@PaymentRequestBillingCity VARCHAR(2000),
	@PaymentRequestBillingPostCode VARCHAR(2000),
	@PaymentRequestBillingCountry VARCHAR(2000),
	@PaymentRequestDeliverySurname VARCHAR(2000),
	@PaymentRequestDeliveryFirstnames VARCHAR(2000),
	@PaymentRequestDeliveryAddress1 VARCHAR(2000),
	@PaymentRequestDeliveryCity VARCHAR(2000),
	@PaymentRequestDeliveryPostCode VARCHAR(2000),
	@PaymentRequestDeliveryCountry VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TableDetailFieldID INT,
			@TableDetailFieldPageID INT,
			@TableRowID INT

	SELECT TOP (1) @TableDetailFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ClientID = @ClientID 
	AND tpfm.ThirdPartyFieldID = 501
	
	SELECT @TableDetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @TableDetailFieldID

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
	VALUES (@ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID)

	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 501 THEN @PaymentRequestVendorTxCode
																	WHEN 502 THEN @PaymentRequestAmount
																	WHEN 503 THEN @PaymentRequestCurrency
																	WHEN 504 THEN @PaymentRequestDescription
																	WHEN 505 THEN @PaymentRequestCardHolder
																	WHEN 506 THEN '' --@PaymentRequestCardNumber
																	WHEN 507 THEN '' --@PaymentRequestExpiryDate
																	WHEN 508 THEN '' --@PaymentRequestCV2
																	WHEN 509 THEN @PaymentRequestCardType
																	WHEN 510 THEN @PaymentRequestTxType
																	WHEN 511 THEN @PaymentRequestBillingSurname
																	WHEN 512 THEN @PaymentRequestBillingFirstnames
																	WHEN 513 THEN @PaymentRequestBillingAddress1
																	WHEN 514 THEN @PaymentRequestBillingCity
																	WHEN 515 THEN @PaymentRequestBillingPostCode
																	WHEN 516 THEN @PaymentRequestBillingCountry
																	WHEN 517 THEN @PaymentRequestDeliverySurname
																	WHEN 518 THEN @PaymentRequestDeliveryFirstnames
																	WHEN 519 THEN @PaymentRequestDeliveryAddress1
																	WHEN 520 THEN @PaymentRequestDeliveryCity
																	WHEN 521 THEN @PaymentRequestDeliveryPostCode
																	WHEN 522 THEN @PaymentRequestDeliveryCountry																	
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522)
	
	
	DECLARE @VendorTxIDDetailFieldID INT	
	
	SELECT @VendorTxIDDetailFieldID=DetailFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=593		
	
	EXEC _C00_SimpleValueIntoField @VendorTxIDDetailFieldID, @PaymentRequestVendorTxCode, @CustomerID
	
	--Return the tableRowID
	SELECT @TableRowID TableRowID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WritePaymentRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WritePaymentRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WritePaymentRequest] TO [sp_executeall]
GO
