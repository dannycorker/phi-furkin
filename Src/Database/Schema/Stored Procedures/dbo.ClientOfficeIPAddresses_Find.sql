SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientOfficeIPAddresses table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientOfficeIpAddressID int   = null ,

	@ClientOfficeID int   = null ,

	@ClientOfficeIPAddress char (15)  = null ,

	@ClientID int   = null ,

	@Notes varchar (200)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientOfficeIpAddressID]
	, [ClientOfficeID]
	, [ClientOfficeIPAddress]
	, [ClientID]
	, [Notes]
    FROM
	[dbo].[ClientOfficeIPAddresses] WITH (NOLOCK) 
    WHERE 
	 ([ClientOfficeIpAddressID] = @ClientOfficeIpAddressID OR @ClientOfficeIpAddressID IS NULL)
	AND ([ClientOfficeID] = @ClientOfficeID OR @ClientOfficeID IS NULL)
	AND ([ClientOfficeIPAddress] = @ClientOfficeIPAddress OR @ClientOfficeIPAddress IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientOfficeIpAddressID]
	, [ClientOfficeID]
	, [ClientOfficeIPAddress]
	, [ClientID]
	, [Notes]
    FROM
	[dbo].[ClientOfficeIPAddresses] WITH (NOLOCK) 
    WHERE 
	 ([ClientOfficeIpAddressID] = @ClientOfficeIpAddressID AND @ClientOfficeIpAddressID is not null)
	OR ([ClientOfficeID] = @ClientOfficeID AND @ClientOfficeID is not null)
	OR ([ClientOfficeIPAddress] = @ClientOfficeIPAddress AND @ClientOfficeIPAddress is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Find] TO [sp_executeall]
GO
