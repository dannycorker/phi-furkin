SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:          Simon Brushett
-- Create date:		2018-12-04
-- Description:     Get Calculated Values - Merge of override code from _C600_PA_Policy_GetQuoteValues and _C00_1273_Policy_CalculatePremium
-- PL  2019-08-29  Added Customer Matching for Aggregator Quotes for Mulitpet Discount #59311
-- 2019-10-11 GPR removed redundent code blocks (previously commented out and not used in C60X build) for LPC-35
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- SB  2020-08-10	Collected passed variables and set as overrides
-- IES 20200820 [SDPRU-9] excess+copay or defaults
-- SB  2020-08-27	Prudent don't want tax including in the premium when called via the API or IQB
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_GetCalculatedValues]
(
	  @XmlRequest	XML
)
AS
BEGIN

	--EXEC _C00_LogItXML 605, 0, '_C600_PA_Policy_GetCalculatedValues', @XmlRequest

	DECLARE @Overrides dbo.tvpIntVarcharVarchar
	DECLARE @FunctionOverrides dbo.tvpIntVarcharVarchar
	DECLARE @PolicyOverrides TABLE (PetID INT, PolicySchemeID INT, RuleSetID INT, AnyID INT, AnyValue1 VARCHAR(2000), AnyValue2 VARCHAR(2000))

	DECLARE @SignUpMethod INT,
			@TrackingCode VARCHAR(50),
			@SourceLookupListItemID INT,
			@AggregatorID VARCHAR(100),
			@CustomerID INT,
			@CaseID INT,
			@Postcode VARCHAR(50),
			@PostCodeGroup VARCHAR(10),
			@County VARCHAR(200),
			@Address1 VARCHAR(2000),
			@FirstName VARCHAR(2000),
			@LastName VARCHAR(2000),
			@BrandID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@IsWorkingPet			BIT,
			@IsVeteran				BIT,
			@IsVetOrStaff			BIT,
			@IsRescue				BIT,
			@IsMedical				BIT,
			@ClientCountryID		INT,
			@IsWellness				BIT
			,@IsInternetPartner			BIT = 0
			,@IsStrategicPartner		BIT = 0
			,@IsCorporateGroupBenefit	BIT = 0
			,@IsBroker					BIT = 0

	-- We need the country ID in order to handle tax correctly
	SELECT @ClientCountryID = CountryID
	FROM dbo.Clients (NOLOCK)
	WHERE ClientID = @ClientID

	SELECT	@SignUpMethod = ISNULL( ( SELECT li.LookupListItemID FROM LookupListItems li WITH ( NOLOCK ) WHERE li.LookupListID = 6126 AND li.ItemValue = @XmlRequest.value('(//SignUpMethod)[1]', 'varchar(50)') ),74326) /*Direct*/ /*CPS 2017-05-29*/,
			@TrackingCode = @XmlRequest.value('(//TrackingCode)[1]', 'varchar(50)'),
			@AggregatorID = @XmlRequest.value('(//AggregatorID)[1]', 'VARCHAR(100)'),
			@CustomerID	= @XmlRequest.value('(//CustomerId)[1]', 'INT'),
			@Postcode = @XmlRequest.value('(//Postcode)[1]', 'VARCHAR(50)'),
			@County = @XmlRequest.value('(//County)[1]', 'VARCHAR(200)')

	--RulesEngineDBCall-BAD
	--EXEC _C600_PA_GetPostcodeGroupFromPostcode @PostCode = @PostCode, @PostCodeGroup = @PostCodeGroup OUTPUT
	
	-- Used to get the excess and co-pay options for vet fees
	DECLARE @VetFeesID INT
	SELECT @VetFeesID = dbo.fn_C00_1272_GetVetFeesResourceList(@ClientID)

	/*GPR 2018-07-19 adopted from 433*/
	SELECT @SourceLookupListItemID = fn.LookupListItemID
	FROM dbo.fn_C600_GetDetailsFromTrackingCode(@TrackingCode) fn

	/*GPR 2018-11-28 #1239 Get Channel From Source Resource List ID - this is based on TrackingCode*/
	DECLARE @Channel INT, @SourceResourceListID INT		
		
	SELECT @SourceResourceListID = fn.ResourceListID
	FROM dbo.fn_C600_GetDetailsFromTrackingCode(@TrackingCode) fn		
		
	SELECT @Channel = rldv.ValueInt FROM ResourceListDetailValues rldv WITH ( NOLOCK ) WHERE rldv.ResourceListID = @SourceResourceListID AND rldv.DetailFieldID = 180102 /*Channel*/

	IF @Channel = 76444 /*Aggregator*/
	BEGIN
		SELECT @SignUpMethod = 74564 /*Aggregator*/
	END
		
	/*AHOD 2018-11-30 AHOD Added to effect Product ordering in _C600_PA_Policy_GetProducts*/
	SELECT @AggregatorID = ISNULL(@AggregatorID,'')
	IF @AggregatorID <>''
	BEGIN
		SELECT @SignUpMethod = 74564 /*Aggregator*/
	END

	INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
	(2, 'Postcode', @Postcode)
	DECLARE @IPT MONEY
	SELECT @IPT=dbo.fn_C00_1273_RulesEngine_GetIPT (NULL, NULL, NULL, NULL, @Overrides)
	DELETE @Overrides

	-- Get pets from XML
	-- Loop pets
	DECLARE @Pets TABLE 
	(
		ID INT IDENTITY,
		PetXml XML
	)

	INSERT @Pets (PetXml)
	SELECT r.c.query('.')
	FROM @XmlRequest.nodes('//PetQuote') r(c)

	DECLARE @PetNumber INT, 
			@ExistingCust BIT
	SELECT	@PetNumber= @XmlRequest.value('(//OtherPetsInsured)[1]', 'INT'),
			@ExistingCust= @XmlRequest.value('(//ExistingPolicyHolder)[1]', 'BIT')

	IF @PetNumber= 0 AND @ExistingCust=1     /*GPR 2018-02-20 - uncommented 2018-02-23*/
	BEGIN
		SELECT @PetNumber = 1
	END

	DECLARE @Policies TABLE
	(
		ID INT IDENTITY,
		GroupedID INT,
		PetID INT,
		PolicyMatterID INT,
		PolicyLeadID INT,
		PolicySchemeID INT,
		ProductName VARCHAR(2000),
		AnnualPremium MONEY,
		Discount MONEY,
		PremiumLessDiscount MONEY,
		FirstMonthly MONEY,
		RecurringMonthly MONEY,
		--PolicyTypeID INT, -- 42931 = 12 month, 42932 = max ben, 42933 = reinstatement
		PolicyType VARCHAR(2000),
		PolicyDocUrl VARCHAR(2000),
		InfoDocUrl VARCHAR(2000),
		Brand VARCHAR(100),
		ItemName VARCHAR(100)
	)

	DECLARE @PetCount INT,
			@PetIndex INT = 0
	SELECT @PetCount = COUNT(*)
	FROM @Pets
		
	WHILE @PetIndex < @PetCount
	BEGIN
		
		SELECT @PetIndex += 1
		SELECT @PetNumber += 1  -- GPR 2018-02-20

		DECLARE @PetXml XML = NULL
		SELECT @PetXml = PetXml
		FROM @Pets
		WHERE ID = @PetIndex

		SELECT @PetNumber = @PetXML.value('(//PetRef)[1]', 'INT') + 1 -- CPS 2018-02-20 for GPR

		IF @CustomerID > 0 /* GPR 2018-03-02 for C600 DefectID 128 - exisiting customer multipet */
			AND EXISTS ( SELECT * 
				FROM Matter m WITH ( NOLOCK ) 
				INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170038 /*Policy Status*/
				INNER JOIN MatterDetailValues mdv2 WITH ( NOLOCK ) ON mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = 170034 /*Current Policy*/
				WHERE m.CustomerID = @CustomerID
				AND mdv.ValueInt IN ( 43002 /* Live/Active */, 74573 /* With Underwriting */ )
				--AND m.MatterRef <> 'One Month Free' /*GPR 2018-03-22 Added to avoid OMF conversion from gaining Multipet on first policy on Customer lookup -- C600-B specific*/
				AND mdv2.ValueInt <> 151577 /*GPR 2018-03-23 OMF*/
				)
		BEGIN
			SELECT @PetNumber += 1
		END

		DECLARE @IsMultipet INT
		SELECT @IsMultipet = 0
		
		IF @ExistingCust = 1 OR @PetCount > 1 OR 
				(
				@CustomerID > 0 /*CPS 2017-06-02*/
				and exists ( SELECT * 
				             FROM Matter m WITH ( NOLOCK ) 
				             INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170038 /*Policy Status*/
				             INNER JOIN MatterDetailValues mdv2 WITH ( NOLOCK ) ON mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = 170034 /*Current Policy*/
				             WHERE m.CustomerID = @CustomerID
				             AND mdv.ValueInt IN ( 43002 /*Live*/, 74573 /*With Underwriting*/ )
				             --AND m.MatterRef <> 'One Month Free' /*GPR 2018-03-22 Added to avoid OMF conversion from gaining Multipet on first policy on Customer lookup -- C600-B specific*/
				             AND mdv2.ValueInt <> 151577 /*GPR 2018-03-23 OMF*/
				           )
				)
		SELECT @IsMultipet = 1
		-- PL  2019-08-29  Added Customer Matching for Aggregator Quotes for Mulitpet Discount #59311
		/*
			CPS 2019-08-27 for JIRA BAULAG-64 || PL  2019-08-28 Added match for customer / multipet discount #58967
			If there is still no match claimed, check the DB for this customer and apply MultiPet if we find a probable match
		*/
		IF (@CustomerID = 0 OR @CustomerID is NULL)
			 BEGIN
				SELECT 
					@Address1 = @XmlRequest.value('(//Address1)[1]' ,'VARCHAR(100)'),
					@Postcode = @XmlRequest.value('(//Postcode)[1]' ,'VARCHAR(50)'),
					@FirstName = @XmlRequest.value('(//FirstName)[1]','VARCHAR(50)'),
					@LastName = @XmlRequest.value('(//LastName)[1]' ,'VARCHAR(50)'),
					@BrandID = @XmlRequest.value('(//BrandingId)[1]' ,'VARCHAR(50)')

			  SELECT TOP (1)
					@CustomerID = cu.CustomerID
			  FROM dbo.fn_C600_GetPossibleCustomerMatchesForMultipet(@BrandID,@ClientID,@Postcode,@LastName,@FirstName,@Address1,@XmlRequest, '') cu

			  IF @CustomerID <> 0
			  BEGIN
				  SET @XmlRequest.modify('replace value of (/BuyPolicyRequest/CustomerInfo/CustomerId/text())[1] with sql:variable("@CustomerID")');
			  END

			  DECLARE @TempLog VARCHAR(2000) 
			  SELECT @TempLog = ISNULL(CONVERT(VARCHAR,@BrandID),'NULL') + ',' + ISNULL(CONVERT(VARCHAR,@ClientID),'NULL') + ',' + ISNULL(@Postcode,'NULL') + ',' + ISNULL(@LastName,'NULL') + ',' + ISNULL(@FirstName,'NULL') + ',' + ISNULL(@Address1,'NULL') + ' | @CustomerID = ' + ISNULL(CONVERT(VARCHAR,@CustomerID),'NULL')

			  EXEC _C00_LogIt 'Info', '_C600_PA_Policy_GetCalculatedValues', 'Probable customer match search', @TempLog, 58552 /*Aquarium Automation*/

			END

		DECLARE @StartDate DATE = NULL,
				@TermsDate DATE = NULL
		SELECT	@StartDate = @PetXml.value('(//StartDate)[1]', 'DATE'),
				@TermsDate = @StartDate -- TODO... get terms date from quote when we allow save and retrieve quote


		-- Lookup pet details
		DECLARE @SpeciesID INT = NULL
		SELECT @SpeciesID = @PetXml.value('(//SpeciesId)[1]', 'INT')

		DECLARE @BreedID INT = NULL
		SELECT @BreedID = @PetXml.value('(//BreedId)[1]', 'INT')

		DECLARE @Breed VARCHAR(2000)
		SELECT @Breed = DetailValue
		FROM dbo.ResourceListDetailValues WITH (NOLOCK) 
		WHERE ResourceListID = @BreedID 
		AND DetailFieldID = 144270

		DECLARE @Gender CHAR(1),
				@GenderID INT
		SELECT @Gender = @PetXml.value('(//Gender)[1]', 'CHAR(1)')
		SELECT @GenderID =	CASE @Gender
								WHEN 'M' THEN 5168
								WHEN 'F' THEN 5169
							END

		DECLARE @BirthDate DATE = NULL
		SELECT	@BirthDate = @PetXml.value('(//BirthDate)[1]', 'DATE')

		DECLARE @IsNeutered BIT = NULL
		SELECT @IsNeutered = @PetXml.value('(//IsNeutered)[1]', 'BIT')

		DECLARE @PurchasePrice MONEY = 0
		SELECT @PurchasePrice = @PetXml.value('(//PurchasePrice)[1]', 'MONEY')

		DECLARE @MicrochipNumber VARCHAR(2000)
		SELECT @MicrochipNumber = @PetXML.value('(//PetInfo/MicrochipNo)[1]', 'varchar(2000)')

		-- Get policies
		-- Loop policies
		INSERT @Policies (PetID, PolicyMatterID, PolicyLeadID, PolicySchemeID, ProductName, PolicyType, PolicyDocUrl, InfoDocUrl, Brand, ItemName)
		EXEC dbo._C600_PA_Policy_GetProducts @PetIndex, @XmlRequest, @PetXml, NULL, @SignUpMethod

		;WITH OrderedPolicies AS 
		(
			SELECT *, 
			ROW_NUMBER() OVER(PARTITION BY PetID ORDER BY ID) as rn 
			FROM @Policies
		)
		UPDATE p
		SET GroupedID = o.rn
		FROM @Policies p
		INNER JOIN OrderedPolicies o ON p.ID = o.ID

		DECLARE @PolicyCount INT = 0,
				@PolicyGroupIndex INT = 0
		SELECT @PolicyCount = COUNT(*) 
		FROM @Policies
		WHERE PetID = @PetIndex

		WHILE @PolicyGroupIndex < @PolicyCount
		BEGIN
			
			SELECT @PolicyGroupIndex += 1

			DECLARE @PolicySchemeID INT = NULL,
					@PolicyMatterID INT = NULL,
					@RuleSetID INT = NULL,
					@ItemName VARCHAR(500),
					@Brand VARCHAR(500),
					@ProductName VARCHAR(500)

			SELECT	@PolicySchemeID = PolicySchemeID,
					@PolicyMatterID = PolicyMatterID,
					@ItemName = ItemName,
					@Brand = Brand,
					@ProductName = ProductName
			FROM @Policies
			WHERE GroupedID = @PolicyGroupIndex
			AND PetID = @PetIndex

			INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
			(2, 'ProductName', @ProductName),
			(2, 'ItemName', @ProductName)

		/*GPR 2019-03-18*/
			IF NOT EXISTS (SELECT *
                        FROM @Overrides o
                        WHERE o.AnyValue1 = 'NewBusiness' )
			BEGIN
				 INSERT @Overrides (AnyID, AnyValue1, AnyValue2 )
				 VALUES (2,'NewBusiness','1')
			END
        
			
			PRINT '@RuleSetID = dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(' + ISNULL(CONVERT(VARCHAR,@PolicyMatterID),'NULL') + ', ''' + ISNULL(CONVERT(VARCHAR,@TermsDate,121),'NULL') + ''')'
			SELECT @RuleSetID = dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(@PolicyMatterID, @TermsDate)

			-- Get overrides
			/*GPR 2018-02-19 pass PetCount to allow MultiPet discount to be controlled within the Rules Engine. 600 grant multi-pet discount to all pets*/
			/*CPS 2018-02-20 for GPR*/
			DECLARE @TotalPetsInRequest INT = 0,
					@Aggressive BIT = 0
					
			;WITH PetList AS
			(
			SELECT 1 [N]
			FROM @XmlRequest.nodes('(//PetQuote)') a(b)
			)
			SELECT @TotalPetsInRequest = COUNT(*)
			FROM PetList
				
			INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
			VALUES ( 4, 'fn_C00_1273_RulesEngine_PetCount',@TotalPetsInRequest + ISNULL(dbo.fn_C00_1273_RulesEngine_PetCount(@CustomerID, 0, 0, 0, @Overrides),0) )
				

			--;WITH Underwriting AS
			--(
			--SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
			--FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
			--)
			--SELECT @Aggressive = 1
			--FROM Underwriting uw 
			--WHERE dbo.fnGetSimpleDvAsInt(177492,uw.QuestionId) = 1 /*Yes Blocks Third Party Liability Cover*/
			--AND uw.AnswerId = 5144 /*Yes*/
				
			--IF @Aggressive = 1
			--BEGIN
			--	INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
			--	VALUES ( 1,'177452','5144' )
					
			--	SELECT @Aggressive = 0 /*Reset for the next pet*/
			--END
			--ELSE
			--BEGIN
			--	INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
			--	VALUES ( 1,'177452','5145' )
			--END
			
			/*GPR 2020-07-27 for PPET-178*/
				/*WorkingDog*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsWorkingPet = 1
				FROM Underwriting uw 
				WHERE uw.AnswerId = 5144 /*Yes*/
				AND uw.QuestionId = 147277

				IF @IsWorkingPet = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313929','5144' )
					
					SELECT @IsWorkingPet = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313929','5145' )
				END

				/*Is the customer a veteran?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsVeteran = 1
				FROM Underwriting uw 
				WHERE uw.AnswerId = 5144 /*Yes*/
				AND uw.QuestionId = 2001392

				IF @IsVeteran = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313933','5144' )
					
					SELECT @IsVeteran = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313933','5145' )
				END

				/*Customer employed as Vet/Staff*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsVetOrStaff = 1
				FROM Underwriting uw 
				WHERE uw.AnswerId = 5144 /*Yes*/
				AND uw.QuestionId = 2001393

				IF @IsVetOrStaff = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313936','5144' )
					
					SELECT @IsVetOrStaff = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313936','5145' )
				END

				/*Pet is Rescue*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsRescue = 1
				FROM Underwriting uw 
				WHERE uw.AnswerId = 5144 /*Yes*/
				AND uw.QuestionId = 2001394

				IF @IsRescue = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'177501','5144' )
					
					SELECT @IsVetOrStaff = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'177501','5145' )
				END

				/*Customer is Medical Professional*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsMedical = 1
				FROM Underwriting uw 
				WHERE uw.AnswerId = 5144 /*Yes*/
				AND uw.QuestionId = 2001395

				IF @IsMedical = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313935','5144' )
					
					SELECT @IsMedical = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313935','5145' )
				END

				/*Wellness*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsWellness = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002259 /*Wellness Discount rating factor*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsWellness = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313934','5144' )
					
					SELECT @IsWellness = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313934','5145' )
				END


				/*GPR 2020-10-07 for SDPRU-77*/
				/*Is the customer from a known Internet partner you know about?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsInternetPartner = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002304 /*Internet Partner*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsInternetPartner = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313938','5144' )
					
					SELECT @IsInternetPartner = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313938','5145' )
				END

				/*Is the customer from a known strategic partner you know about?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsStrategicPartner = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002305 /*Strategic Partner*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsStrategicPartner = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313939','5144' )
					
					SELECT @IsStrategicPartner = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313939','5145' )
				END

				/*Is the customer from a known employer we provide benefits to?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsCorporateGroupBenefit = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002306 /*Corporate Group Benefit*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsCorporateGroupBenefit = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313937','5144' )
					
					SELECT @IsCorporateGroupBenefit = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313937','5145' )
				END

				/*Is the customer coming in from a broker with a surcharge?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsBroker = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002307 /*Corporate Group Benefit*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsBroker = 1
				BEGIN
					--INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					--VALUES ( 1,'176964','Broker' )
					
					SELECT @SignUpMethod = 193317
					
					SELECT @IsBroker = 0 /*Reset for the next pet*/
				END

			-- Any others from _C00_1273_Policy_CalculatePremium into @Overrides

			-- Now get the product specific values


			/*ALM 2020-08-05 Added @Excess and @CoPay PPET-7*/

			DECLARE @PassedExcess MONEY =  @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/Excess)[1]', 'MONEY')
			/*IES - 20200820 [SDPRU-9]*/
			SELECT @PassedExcess = dbo.fn_C00_1272_Policy_GetPolicyExcessOrDefault(@ClientID, @PolicyMatterID, @PostCodeGroup, @Breed, @BirthDate, @StartDate, @SpeciesID, 0, @StartDate, @StartDate, 1, 1, @PassedExcess)

			DECLARE @PassedCoInsurance MONEY = @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/CoInsurance)[1]', 'MONEY')
			/*IES - 20200820 [SDPRU-9]*/
			SELECT @PassedCoInsurance = dbo.fn_C00_1272_Policy_GetPolicyCoPayOrDefault(@ClientID, @PolicyMatterID, @StartDate, @BirthDate, @SpeciesID, 0, @Breed, @PassedCoInsurance)

			INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
			(1, 313932, @PassedExcess),
			(1, 313930, @PassedCoInsurance)

			DECLARE @PassedOptionalCoverages VARCHAR(2000) =  @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/OptionalCoverages)[1]', 'VARCHAR(2000)')

			-- Optional coverages are RLIDs
			-- Join to collect the override type and key and set to 'yes'
			-- But validate on the scheme that they are allowed 
			INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
			SELECT rdvType.ValueInt, rdvKey.DetailValue, '5144'
			FROM dbo.fnTableOfValuesFromCSV(@PassedOptionalCoverages) c
			INNER JOIN dbo.ResourceList rl (NOLOCK) ON c.AnyValue = rl.ResourceListID
			INNER JOIN dbo.TableDetailValues tdvRL (NOLOCK) ON tdvRL.DetailFieldID = 175733 AND rl.ResourceListID = tdvRL.ResourceListID
			INNER JOIN dbo.TableRows r (NOLOCK) ON tdvRL.TableRowID = r.TableRowID AND r.MatterID = @PolicyMatterID
			INNER JOIN dbo.ResourceListDetailValues rdvType (NOLOCK) ON rl.ResourceListID = rdvType.ResourceListID AND rdvType.DetailFieldID = 314008
			INNER JOIN dbo.ResourceListDetailValues rdvKey (NOLOCK) ON rl.ResourceListID = rdvKey.ResourceListID AND rdvKey.DetailFieldID = 314009
	
	
			-- Insert into @PolicyOverrides
			-- Delete from @Overrides
			INSERT INTO @PolicyOverrides (PetID, PolicySchemeID, RuleSetID, AnyID, AnyValue1, AnyValue2)
			SELECT @PetIndex, @PolicySchemeID, @RuleSetID, AnyID, AnyValue1, AnyValue2
			FROM @Overrides
			
			DELETE FROM @Overrides

		END
		-- End loop policies
		
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'TermsDate', CONVERT(VARCHAR, @TermsDate, 120)),
		(2, 'EvaluateByEffectiveDate', CONVERT(VARCHAR, @TermsDate, 120)),
		(2, 'YearStart', CONVERT(VARCHAR, @StartDate, 120))

		DECLARE @PassedVolExcess MONEY = NULL, -- not used
				@LastPeriodExitPremium MONEY = 0,
				@LossRationPercentage MONEY = 0,
				@NumberPreviousClaims INT = 0

		DECLARE @PaymentFrequency VARCHAR(20) = 'MO', -- default to monthly, payment frequency is only used for renewals
				@PolicyYearNumber INT = 1

		DECLARE @DiscountCode VARCHAR(100)
		SELECT @DiscountCode = @XmlRequest.value('(//DiscountCodes/string)[1]', 'VARCHAR(100)')

		EXEC _C00_LogItXml @ClientID, 0, '_C600_PA_Policy_GetCalculatedValues', @DiscountCode

		-- Get base overrides from _C00_1273_Policy_GetOverrides into @Overrides
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
		EXEC dbo._C00_1273_Policy_GetOverrides  @Postcode, 
												@County, 
												@SpeciesID, 
												@Breed, 
												@GenderID, 
												@BirthDate,
												@IsNeutered, 
												@PurchasePrice, 
												@PassedVolExcess,
												@PetNumber, 
												@DiscountCode, 
												@IsMultipet, 
												@PassedOptionalCoverages,
												@SignUpMethod,
												@LastPeriodExitPremium,
												@LossRationPercentage,
												@NumberPreviousClaims,
												@PaymentFrequency,
												@PolicyYearNumber,
												@MicrochipNumber,
												@ItemName,
												@Brand,
												@ProductName,
												@IPT,
												@SourceLookupListItemID

		-- Remove these as they are done above
		DELETE	FROM @Overrides
		WHERE	AnyValue1 IN ('ProductName','ItemName')
		--AND		AnyValue2 IS NULL

		/* To exclude the tax for US clients we set this override */
		/* 2021-02-15 GPR/ALM added Canada (39) to list for FURKIN-258*/
		IF @ClientCountryID IN (233, 39) --= 233 -- USA
		BEGIN
			INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
			VALUES (2, 'RemoveTax', '1')
		END

		-- Insert into @PolicyOverrides
		INSERT INTO @PolicyOverrides (PetID, PolicySchemeID, RuleSetID, AnyID, AnyValue1, AnyValue2)
		SELECT @PetIndex, 0, @RuleSetID, AnyID, AnyValue1, AnyValue2
		FROM @Overrides

		PRINT '@PolicyOverrides Rowcount = ' + ISNULL(CONVERT(VARCHAR,@@RowCount),'NULL')

		-- Call function overrides passing in @Overrides
		-- Insert into @PolicyOverrides
		INSERT INTO @FunctionOverrides
		EXEC dbo._C600_PA_Policy_EvaluateRulesEngineFunction @CustomerID, @CaseID, @Overrides

		INSERT INTO @PolicyOverrides (PetID, PolicySchemeID, RuleSetID, AnyID, AnyValue1, AnyValue2)
		SELECT @PetIndex, 0, @RuleSetID, AnyID, AnyValue1, AnyValue2
		FROM @FunctionOverrides

		PRINT '@FunctionOverrides Rowcount = ' + ISNULL(CONVERT(VARCHAR,@@RowCount),'NULL')

		-- Delete from @Overrides and @FuctionOverrides
		DELETE FROM @Overrides
		DELETE FROM @FunctionOverrides

	-- End pet loop
	END

	-- Tidy and return distinct
	/*Delete empty overrides for which we have a populated value*/
	DELETE po1
	FROM @PolicyOverrides po1 
	INNER JOIN @PolicyOverrides po2 on po2.AnyID = po1.AnyID AND po2.AnyValue1 = po1.AnyValue1 AND (po2.PolicySchemeID = po1.PolicySchemeID OR po1.PolicySchemeID = 0)
	WHERE (po1.AnyValue2 = '' or po1.AnyValue2 is NULL)
	AND (po2.AnyValue2 <> '' or po2.AnyValue2 = po1.AnyValue2)
	
	SELECT	DISTINCT PetID, PolicySchemeID, RuleSetID, AnyID, AnyValue1, ISNULL(AnyValue2, '') AS AnyValue2
	FROM	@PolicyOverrides
	WHERE	RuleSetID is not NULL


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetCalculatedValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_GetCalculatedValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetCalculatedValues] TO [sp_executeall]
GO
