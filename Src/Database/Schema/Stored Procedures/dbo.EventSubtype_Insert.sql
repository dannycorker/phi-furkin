SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_Insert]
(

	@EventSubtypeID int   ,

	@EventSubtypeName varchar (50)  ,

	@EventSubtypeDescription varchar (250)  ,

	@ApplyEventMaxThreshold int   ,

	@Available bit   
)
AS


				
				INSERT INTO [dbo].[EventSubtype]
					(
					[EventSubtypeID]
					,[EventSubtypeName]
					,[EventSubtypeDescription]
					,[ApplyEventMaxThreshold]
					,[Available]
					)
				VALUES
					(
					@EventSubtypeID
					,@EventSubtypeName
					,@EventSubtypeDescription
					,@ApplyEventMaxThreshold
					,@Available
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Insert] TO [sp_executeall]
GO
