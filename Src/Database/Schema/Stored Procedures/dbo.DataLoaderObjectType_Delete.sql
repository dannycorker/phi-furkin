SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectType_Delete]
(

	@DataLoaderObjectTypeID int   
)
AS


				DELETE FROM [dbo].[DataLoaderObjectType] WITH (ROWLOCK) 
				WHERE
					[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Delete] TO [sp_executeall]
GO
