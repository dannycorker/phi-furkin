SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Check if a PortalUser Username is available
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser__CheckUsernameAvailability]
(
	@Username varchar(50)
)
AS

BEGIN

	SET NOCOUNT ON
		
	DECLARE @Result INT
	EXEC @Result = AquariusMaster.dbo.AQ_PortalUser__CheckEmailAvailability @Username
	RETURN @Result

END




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__CheckUsernameAvailability] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser__CheckUsernameAvailability] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__CheckUsernameAvailability] TO [sp_executeall]
GO
