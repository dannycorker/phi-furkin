SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-06-26
-- Description:	List UploadedFile details without the actual file content
--				for the given client id
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFile__GetByClientIDNoBlobs]
(
	@ClientID int   
)
AS
BEGIN

	SET ANSI_NULLS OFF
	
	SELECT
		[UploadedFileID],
		[ClientID],
		[FolderID],
		[UploadedFileName],
		[UploadedFileTitle],
		[UploadedFileDescription],
		CAST('' as varbinary) AS [UploadedFile],
		[UploadedFileGUID],
		[UploadedFileSize],
		[WhoCreated],
		[WhenCreated],
		[WhoModified],
		[WhenModified]
	FROM
		[dbo].[UploadedFile]
	WHERE
		[ClientID] = @ClientID
	
	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON


END




GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile__GetByClientIDNoBlobs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile__GetByClientIDNoBlobs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile__GetByClientIDNoBlobs] TO [sp_executeall]
GO
