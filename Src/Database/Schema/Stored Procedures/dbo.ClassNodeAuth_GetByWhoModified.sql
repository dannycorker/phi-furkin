SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClassNodeAuth table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_GetByWhoModified]
(

	@WhoModified int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClassNodeAuthID],
					[ClientID],
					[ClassNodeID],
					[ClientPersonnelAdminGroupID],
					[RightsLevel],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ClassNodeAuth] WITH (NOLOCK) 
				WHERE
					[WhoModified] = @WhoModified
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_GetByWhoModified] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_GetByWhoModified] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_GetByWhoModified] TO [sp_executeall]
GO
