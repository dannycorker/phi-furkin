SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReferenceValueSectionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueSectionTypes_Update]
(

	@ReferenceValueSectionTypeID int   ,

	@SectionTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReferenceValueSectionTypes]
				SET
					[SectionTypeName] = @SectionTypeName
				WHERE
[ReferenceValueSectionTypeID] = @ReferenceValueSectionTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueSectionTypes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Update] TO [sp_executeall]
GO
