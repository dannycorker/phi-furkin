SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2017-10-13
-- Description:	GetFailedOrOverduePaymentsByLeadID
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentSchedule__GetFailedOrPendingPaymentsByCustomerID]
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	EXEC dbo._C00_Logit 'PROC', 'PurchasedProductPaymentSchedule__GetFailedOrPendingPaymentsByCustomerID', '', '', NULL

	SELECT *
	FROM dbo.fn_Billing_GetFailedAndPendingPaymentsByCustomerID(@CustomerID) p
	LEFT JOIN dbo.PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = p.PaymentStatusID
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = p.PurchasedProductID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetFailedOrPendingPaymentsByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentSchedule__GetFailedOrPendingPaymentsByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetFailedOrPendingPaymentsByCustomerID] TO [sp_executeall]
GO
