SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-04-27
-- Description:	Call all Dashboard procs in turn
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard]
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Scheduler details */
	EXEC dbo.AIDashboard_SchedulerActivity @SummaryOnly

	/* Document zips */	
	EXEC dbo.AIDashboard_DocumentZips @SummaryOnly
	
	/* Dataloader */
	EXEC dbo.AIDashboard_Dataloader @SummaryOnly
	
	/* Disk free space on servers */
	EXEC dbo.AIDashboard_DiskFreeSpace @SummaryOnly
		
	/* DB free space */
	EXEC dbo.AIDashboard_DBFreeSpace @SummaryOnly
	
	/* DB Backup Status */
	EXEC dbo.AIDashboard_DBBackupStatus @SummaryOnly
	
	/* Imports */
	EXEC dbo.AIDashboard_Imports @SummaryOnly
	
	/* Identities */
	EXEC dbo.AIDashboard_Identities @SummaryOnly
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard] TO [sp_executehelper]
GO
