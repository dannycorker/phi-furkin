SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeAsyncEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAsyncEvent_Update]
(

	@EventTypeAsyncEventID int   ,

	@ClientID int   ,

	@SourceEventTypeID int   ,

	@AsyncEventTypeID int   ,

	@FollowupThreadNumber int   ,

	@AquariumOptionID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeAsyncEvent]
				SET
					[ClientID] = @ClientID
					,[SourceEventTypeID] = @SourceEventTypeID
					,[AsyncEventTypeID] = @AsyncEventTypeID
					,[FollowupThreadNumber] = @FollowupThreadNumber
					,[AquariumOptionID] = @AquariumOptionID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[EventTypeAsyncEventID] = @EventTypeAsyncEventID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAsyncEvent_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Update] TO [sp_executeall]
GO
