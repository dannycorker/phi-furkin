SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        Ian Slack
-- Create date:   2016-08-15
-- Description:   gets breadcrumbs for the id and level
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Breadcrumb_Get]
(
	@ClientID INT,
	@ItemID INT,
	@ItemType VARCHAR(200)
)
AS
BEGIN
      
      SET NOCOUNT ON;
      
      DECLARE 
			@ChangeSetID INT,
			@RuleSetID INT,
			@RuleID INT,
			@RuleParameterID INT
      
	IF @ItemType = 'ChangeSets'
	BEGIN
		SELECT @ChangeSetID = @ItemID
	END
	ELSE IF @ItemType = 'RuleSets'
	BEGIN
		SELECT @RuleSetID = @ItemID
	END
	ELSE IF @ItemType = 'Rules'
	BEGIN
		SELECT @RuleID = @ItemID
	END
	ELSE IF @ItemType = 'Outputs'
	BEGIN
		SELECT @RuleID = @ItemID
	END
	ELSE IF @ItemType = 'Parameters'
	BEGIN
		SELECT @RuleParameterID = @ItemID
	END
      
  	SELECT	
		@ChangeSetID = cs.ChangeSetID, 
		@RuleSetID = rs.RuleSetID,
		@RuleID = r.RuleID,
		@RuleParameterID = rp.RuleParameterID
	FROM dbo.RulesEngine_ChangeSets cs WITH (NOLOCK) 
	LEFT JOIN dbo.RulesEngine_RuleSets rs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID AND @ChangeSetID IS NULL
	LEFT JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID AND @RuleSetID IS NULL
	LEFT JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleID = r.RuleID AND @RuleID IS NULL
	
	WHERE 
		(rs.RuleSetID = @RuleSetID AND @RuleSetID IS NOT NULL)
		OR
		(r.RuleID = @RuleID AND @RuleID IS NOT NULL)
		OR
		(rp.RuleParameterID = @RuleParameterID AND @RuleParameterID IS NOT NULL)

	;WITH BreadCrumbs AS
	(
		SELECT	cs.ChangeSetID, rs.RuleSetID, r.RuleID, rp.RuleParameterID,
				cs.TagName CSName, rs.Name RSName, r.Name RName, rp.Name RPName
		FROM dbo.RulesEngine_ChangeSets cs WITH (NOLOCK) 
		LEFT JOIN dbo.RulesEngine_RuleSets rs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID AND rs.RuleSetID = @RuleSetID
		LEFT JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID AND r.RuleID = @RuleID
		LEFT JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleID = r.RuleID AND rp.RuleParameterID = @RuleParameterID
		WHERE	cs.ClientID = @ClientID
		AND		cs.ChangeSetID = @ChangeSetID
	),
	BreadCrumbOrder AS
	(
		SELECT	'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord 
		UNION
		SELECT	CSName AS Title, '#/ruleset/' + CAST(ChangeSetID AS VARCHAR) AS Path, 2 AS Ord  
		FROM	BreadCrumbs
		WHERE	ChangeSetID IS NOT NULL
		UNION
		SELECT	RSName AS Title, '#/ruleset/' + CAST(RuleSetID AS VARCHAR) + '/rules' AS Path, 3 AS Ord 
		FROM	BreadCrumbs
		WHERE	RuleSetID IS NOT NULL
		UNION
		SELECT	RName AS Title, '#/rule/' + CAST(RuleID AS VARCHAR) AS Path, 4 AS Ord 
		FROM	BreadCrumbs
		WHERE	RuleID IS NOT NULL
		UNION
		SELECT	RPName AS Title, '#/parameter/' + CAST(RuleParameterID AS VARCHAR) + '/options' AS Path, 5 AS Ord  
		FROM	BreadCrumbs
		WHERE	RuleParameterID IS NOT NULL
	)
	SELECT	* 
	FROM	BreadCrumbOrder
	ORDER BY Ord
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Breadcrumb_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Breadcrumb_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Breadcrumb_Get] TO [sp_executeall]
GO
