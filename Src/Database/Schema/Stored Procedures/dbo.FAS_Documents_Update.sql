SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FAS_Documents table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_Update]
(

	@FasDocumentID int   ,

	@FasCheckerDataID int   ,

	@DocumentBlob image   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FAS_Documents]
				SET
					[FasCheckerDataID] = @FasCheckerDataID
					,[DocumentBlob] = @DocumentBlob
				WHERE
[FasDocumentID] = @FasDocumentID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Update] TO [sp_executeall]
GO
