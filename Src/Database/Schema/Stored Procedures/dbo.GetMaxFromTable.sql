SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetMaxFromTable] @TableName varchar(100), @ColumnName varchar(100), @OptionalWhereClause varchar(1000) AS 

declare @SqlStatement varchar(2000)

BEGIN

	select @SqlStatement = 'SELECT IsNull(convert(char, MAX(' + @ColumnName + ')), ''0'') FROM ' + @TableName + ' (nolock) ' + @OptionalWhereClause

	print @SqlStatement
	exec(@SqlStatement)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetMaxFromTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMaxFromTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMaxFromTable] TO [sp_executeall]
GO
