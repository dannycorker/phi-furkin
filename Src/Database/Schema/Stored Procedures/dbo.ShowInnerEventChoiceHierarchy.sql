SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[ShowInnerEventChoiceHierarchy]
(
	@RootEventTypeID int,
	@EventHierarchyTreeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @EventTypeID int, @EventTypeName varchar(50), @Description varchar(50)

	DECLARE @MinNETI int, @Stop char(1)

	SET @EventTypeName = (SELECT EventTypeName FROM EventType WHERE EventTypeID = @RootEventTypeID)
	PRINT REPLICATE('-', @@NESTLEVEL * 2) + convert(varchar,@RootEventTypeID) + ' ' + @EventTypeName

	SET @MinNETI = IsNull((SELECT MAX(NextEventTypeID) FROM EventHierarchyTree WHERE EventHierarchyTreeID = @EventHierarchyTreeID AND EventTypeID = @RootEventTypeID), 0)

	SET @EventTypeID = (SELECT MIN(NextEventTypeID) FROM EventChoice WHERE EventTypeID = @RootEventTypeID AND NextEventTypeID > @MinNETI)

	SET @Stop = 'N'
	WHILE @EventTypeID IS NOT NULL AND @Stop = 'N'
	BEGIN
		INSERT INTO EventHierarchyTree (EventHierarchyTreeID, EventTypeID, NextEventTypeID) VALUES (@EventHierarchyTreeID, @RootEventTypeID, @EventTypeID)

		--PRINT 'Calling ' + convert(varchar,@EventTypeID) + ' From  ' + convert(varchar,@RootEventTypeID)
		EXEC ShowInnerEventChoiceHierarchy @EventTypeID, @EventHierarchyTreeID

		SET @EventTypeID = (SELECT MIN(NextEventTypeID) FROM EventChoice WHERE NextEventTypeID = @RootEventTypeID AND EventTypeID > @EventTypeID)

		IF EXISTS(SELECT * FROM EventHierarchyTree WHERE EventHierarchyTreeID = @EventHierarchyTreeID AND EventTypeID = @RootEventTypeID AND NextEventTypeID = @EventTypeID)
		BEGIN
			--PRINT REPLICATE('-', (@@NESTLEVEL * 2) + 2) + convert(varchar,@RootEventTypeID) + 'LOOPS BACK TO: ' + convert(varchar,@EventTypeID) + ' ' + @EventTypeName
			SET @Stop = 'Y'
		END
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ShowInnerEventChoiceHierarchy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ShowInnerEventChoiceHierarchy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ShowInnerEventChoiceHierarchy] TO [sp_executeall]
GO
