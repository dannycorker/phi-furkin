SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MasterQuestions table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_GetByMasterQuestionID]
(

	@MasterQuestionID int   
)
AS


				SELECT
					[MasterQuestionID],
					[ClientQuestionnaireID],
					[QuestionTypeID],
					[QuestionText],
					[QuestionOrder],
					[DefaultAnswerID],
					[Mandatory],
					[QuestionToolTip],
					[LinkedQuestionnaireMasterQuestionID],
					[Active],
					[MasterQuestionStatus],
					[TextboxHeight],
					[ClientID],
					[AnswerPosition],
					[DisplayAnswerAs],
					[NumberOfAnswersPerRow],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[QuestionPossibleAnswerSortOrder]
				FROM
					[dbo].[MasterQuestions] WITH (NOLOCK) 
				WHERE
										[MasterQuestionID] = @MasterQuestionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_GetByMasterQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_GetByMasterQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_GetByMasterQuestionID] TO [sp_executeall]
GO
