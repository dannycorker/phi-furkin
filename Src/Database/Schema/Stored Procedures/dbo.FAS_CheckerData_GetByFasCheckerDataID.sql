SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FAS_CheckerData table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_CheckerData_GetByFasCheckerDataID]
(

	@FasCheckerDataID int   
)
AS


				SELECT
					[FasCheckerDataID],
					[FasUniqueReferenceNumber],
					[TitleOfAgreement],
					[IsDebtorsNameOrAddressMissing],
					[IsCreditorsNameOrAddressMissing],
					[AgreementDebtorSignatureDate],
					[NameOfCreditor],
					[AmountOfCredit],
					[TotalCashPrice],
					[DurationOfAgreementInMonths],
					[AnnualPercentageRate],
					[RateOfInterest],
					[FixedOrVariableRateOfInterest],
					[TotalInterest],
					[TotalChargeFees],
					[TotalChargeForCredit],
					[FirstPayment],
					[ConstantRegularOrSubsequentPayment],
					[LastPayment],
					[DeferredOrBallonPayment],
					[TotalPayments],
					[AdvancePayment],
					[TotalAmountPayable],
					[Category]
				FROM
					[dbo].[FAS_CheckerData] WITH (NOLOCK) 
				WHERE
										[FasCheckerDataID] = @FasCheckerDataID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_GetByFasCheckerDataID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_CheckerData_GetByFasCheckerDataID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_GetByFasCheckerDataID] TO [sp_executeall]
GO
