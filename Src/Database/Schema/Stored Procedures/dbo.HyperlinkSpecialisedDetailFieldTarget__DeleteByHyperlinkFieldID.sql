SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the HyperlinkSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget__DeleteByHyperlinkFieldID]
(

	@HyperlinkDetailFieldID int   
)
AS


				DELETE FROM [dbo].[HyperlinkSpecialisedDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
			



GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget__DeleteByHyperlinkFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget__DeleteByHyperlinkFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget__DeleteByHyperlinkFieldID] TO [sp_executeall]
GO
