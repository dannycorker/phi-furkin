SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SiteMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SiteMap_Delete]
(

	@SiteMapID int   
)
AS


				DELETE FROM [dbo].[SiteMap] WITH (ROWLOCK) 
				WHERE
					[SiteMapID] = @SiteMapID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SiteMap_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Delete] TO [sp_executeall]
GO
