SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryTemplate table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_GetBySqlQueryTemplateID]
(

	@SqlQueryTemplateID int   
)
AS


				SELECT
					[SqlQueryTemplateID],
					[ClientID],
					[Name],
					[Description],
					[TemplatePath],
					[DataWorksheetNumber],
					[DataStartCell],
					[TemplateFormat],
					[FolderID],
					[CreatedBy],
					[CreatedOn],
					[ModifiedBy],
					[ModifiedOn],
					[Enabled]
				FROM
					[dbo].[SqlQueryTemplate] WITH (NOLOCK) 
				WHERE
										[SqlQueryTemplateID] = @SqlQueryTemplateID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_GetBySqlQueryTemplateID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_GetBySqlQueryTemplateID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_GetBySqlQueryTemplateID] TO [sp_executeall]
GO
