SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-05
-- Description:	Sql After Event - Change Pet Details
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_155247]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@TermsDate						DATE
			,@AdjustmentDate				DATE
			,@Count							INT
			,@ValueInt						INT
			,@DetailValueData				dbo.tvpDetailValueData
			,@EventComments					VARCHAR(2000)
			,@NextEventID					INT
			,@IdId							dbo.tvpIntInt
			,@WorkflowTaskID				INT
			,@ResourceListID				INT
			,@RowCount						INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

IF @EventTypeID IN (155247) -- begin - Change Pet Details
	BEGIN

		/*GPR 2019-05-31 to error when users attempt to apply this event on UPP migrated policies that have not been renewed on AQ*/
		/*PET CHANGE - IF Migrated Policy and a Renew Policy event does not exist then RAISERROR*/ 
		IF EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=@CaseID AND le.EventDeleted=0 AND le.EventTypeID = 158850 /*Migrated Policy Process Start*/
		AND NOT EXISTS (SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=@CaseID AND le.EventDeleted=0 AND le.EventTypeID = 150151 /*Renew Policy*/))
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>This policy is a UPP migrated Policy that has not been renewed on Aquarium. If a new premium has been received from UPP following an MTA please use the Begin - Update Pet From UPP MTA event to update Aquarium. If the customer wishes to make a change to their details please refer the customer to UPP directly to make this change.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END

		
		/*GPR 2019-12-02 SDLPC-25*/
		IF dbo.fnGetSimpleDvAsDate(175334,@MatterID) > dbo.fnGetSimpleDvAsDate(170037,@MatterID) /*Policy End Date*/
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Date entered is greater than Policy End Date. Please correct before continuing.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		/*2018-04-09 - SA - Update to raise error where Pet DoB less than 8 Weeks*/
		IF dbo.fnGetSimpleDvAsDate(175326,@CustomerID) >= DATEADD(WEEK, -8, dbo.fn_GetDate_Local())
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Date entered for Pet Date of Birth is less than 8 weeks.  Please correct before continuing</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END
		
		/*GPR 2018-10-31 for Migrated Policy*/
		SELECT @TermsDate = dbo._C600_GetFirstNonMigratedTermsDate(@MatterID)

		IF @AdjustmentDate < @TermsDate
		BEGIN	

			SELECT @ErrorMessage = '<font color="red"></br></br>The adjustment date is in a period of cover that is not known to Aquarium (Migrated Term). Please check the Adjustment Date.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1	

		END
		
		/*2018-06-06 - JL - Update to raise error if micro chip removed*/
		IF dbo.fnGetSimpleDvAsDate(170030,@MatterID) = '' AND EXISTS (SELECT * FROM DetailValueHistory l WITH ( NOLOCK ) 
																	  WHERE l.DetailFieldID = 170030
																	  and l.LeadID = @LeadID
																	  and ISNULL(l.FieldValue,'') <> '' ) 
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot remove a MicroChip number at MTA</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END
		
		IF ISNULL(dbo.fnGetSimpleDvAsDate(170030,@MatterID),'') <> '' 
		BEGIN 
		
			EXEC _C00_SimpleValueIntoField  177372,5144,@LeadID, @WhoCreated 
		
		END  
		
		/*2018-04-09 - SA - Update to raise error when adjustment date less than inception date*/
		IF dbo.fnGetSimpleDvAsDate(175442,@MatterID) < dbo.fnGetSimpleDvAsDate(170035,@MatterID) 
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Adjustment date is less than inception date. Please correct before continuing</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1)	
			RETURN -1
		END
		
		IF dbo.fnGetSimpleDvAsInt(170038,@MatterID) IN (43003,43834)
		BEGIN
		
			SELECT @ErrorMessage = '<BR><BR><font color="red">Error: You cannot perform an MTA on a policy that is not live </font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			
		END

		SELECT @EventComments = ''

		/*switch the changed details to their premanent place prior to re-rating*/
		EXEC _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated /*TempLeadID*/
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated /*TempMatterID*/
			
		INSERT @IdId ( ID1, ID2 )
		SELECT cdv.DetailFieldID, cdv.CustomerDetailValueID
		FROM TableDetailValues tdv WITH ( NOLOCK )
		INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = 177665 /*Premium Affecting Fields*/
		INNER JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.DetailFieldID = tdv.ValueInt
		INNER JOIN CustomerDetailValues cdv2 WITH ( NOLOCK ) on cdv2.CustomerID = cdv.CustomerID AND cdv2.DetailFieldID = 170144 /*Match on Affinity*/
		LEFT JOIN TableDetailValues tdvCoi WITH ( NOLOCK ) on tdvCoi.TableRowID = tr.TableRowID AND tdvCoi.DetailFieldID = 178667 /*COI Only*/
		INNER JOIN TableDetailValues tdvAffinity WITH ( NOLOCK ) on tdvAffinity.TableRowID = tr.TableRowID AND tdvAffinity.DetailFieldID = 180001 /*COI Only*/
		WHERE cdv.CustomerID = @CustomerID
		AND tdv.DetailFieldID = 177660 /*DetailFieldID*/
		AND cdv.DetailValue NOT IN ( '','0' )
		AND cdv.DetailFieldID NOT IN ( 175586 ) /*Brand Specific Breed Search*/
		AND ( tdvCoi.ValueInt = 0 or tdvCoi.TableDetailValueID is NULL )
		and	 ( 
				( tdv.ValueInt = 175328 /*Pet Breed*/ AND cdv.ValueInt <> dbo.fnGetSimpleDvAsInt(144272,@LeadID) /*Pet Type*/ )
			OR
				exists ( SELECT * 
						 FROM EventTypeHelperField hf WITH ( NOLOCK ) 
						 WHERE hf.EventTypeID = @EventTypeID 
			 			 AND hf.DetailFieldID = tdv.ValueInt )
			 )
		AND tdvAffinity.ValueInt = cdv2.ValueInt /*GPR 2018-04-13 Added Affinity to Table, updated 2018-04-16*/
		
		/*!! NO SQL HERE !!*/
		SELECT @RowCount = @@ROWCOUNT	 
		SELECT @EventComments += CONVERT(VARCHAR,@RowCount) + ' key fields updated.' + CHAR(13)+CHAR(10)
		
		SELECT @EventComments += CONVERT(VARCHAR,df.DetailFieldID ) + ' (' + df.FieldCaption + ')' + CHAR(13)+CHAR(10)
		FROM @IdId id
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = id.ID1

		SELECT @ResourceListID = dbo.fnGetSimpleDvAsInt(175328,@CustomerID) /*Pet Breed*/
		
		/*CPS 2017-09-27 remove excluded breeds*/
		IF dbo.fnGetSimpleDvAsInt(177897,@ResourceListID) = 1 /*Excluded Breed*/
		BEGIN
			SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  You cannot proceed because ' + dbo.fnGetSimpleDv(144270,@ResourceListID) /*Pet Breed*/ + ' is a restricted breed.</font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END
				
		/*CPS 2017-07-06 don't bother with the MTA step if you have not changed key fields*/
		IF (Select dbo.fn_C00_1273_GetCurrentSchemeFromMatter(@MatterID)) = 10276 /*CW 2018-04-30 OMF MTA*/ 
		BEGIN
			IF @RowCount > 0
			BEGIN
				EXEC _C600_UpdatePHOrPetDetails @LeadEventID = @LeadEventID, @UpdateType = 2 /*Pet*/
				SELECT @NextEventID = 158808 /* OMF Mid Term Adjustment Pet*/
			END
			ELSE
			BEGIN
				EXEC _C600_UpdatePHOrPetDetails @LeadEventID = @LeadEventID, @UpdateType = 2 /*Pet*/
				SELECT @EventComments += 'No MTA Required.' + CHAR(13)+CHAR(10)
				SELECT @NextEventID = 150156 /*No change in premium*/
			END
		END
		ELSE IF @RowCount > 0
		BEGIN
			SELECT @EventComments += 'Mid Term Adjustment Pet triggered' + CHAR(13)+CHAR(10)
			SELECT @NextEventID = 156867 /*.Mid Term Adjustment Pet*/
		END   
		ELSE
		BEGIN		
			EXEC _C600_UpdatePHOrPetDetails @LeadEventID = @LeadEventID, @UpdateType = 2 /*Pet*/
			SELECT @EventComments += 'No MTA Required.' + CHAR(13)+CHAR(10)
			SELECT @NextEventID = 150156 /*No change in premium*/
		END
		
		EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @NextEventID, @WhoCreated, 0

		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

		/*copy adjustment date to field to use for docs*/ /*NG 2020-12-09 for JIRA SDPRU-187*/
		EXEC _C600_CopyFieldToField 175442, 314353, @MatterID, @WhoCreated /*Adjustment or Cancellation Date => MTA Date*/

		/*UAH 07/11/2018 - As there is only 1 MTA Workflow group, I am adding SAE on all events which relate to	MTA's to remove workflow tasks once these MTA events have been applied*/
		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) where w.caseid = @caseID
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID, WorkflowGroupID, AutomatedTaskID, Priority, AssignedTo, AssignedDate, LeadID, CaseID, EventTypeID, FollowUp, Important, CreationDate, Escalated, EscalatedBy, EscalationReason, EscalationDate, Disabled, CompletedBy, CompletedOn, CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, 	@WhoCreated, dbo.fn_GetDate_Local(),	'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE CaseID = @CaseID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 
		AND WorkflowGroupID = 1239 

	END
	
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_155247] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_MidTermAdjustment_155247] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_155247] TO [sp_executeall]
GO
