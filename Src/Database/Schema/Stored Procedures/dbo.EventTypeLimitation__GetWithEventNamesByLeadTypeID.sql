SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:  ()
-- Purpose: Select records from the EventTypeLimitation table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation__GetWithEventNamesByLeadTypeID]
(
	@LeadTypeID int
)
AS
			SET ANSI_NULLS OFF
				
			SELECT
				[EventTypeLimitation].[EventTypeLimitationID],
				[EventTypeLimitation].[ClientID],
				[EventTypeLimitation].[LeadTypeID],
				[etFrom].[EventTypeName] AS FromEvent,
				[etTo].[EventTypeName] AS ToEvent,
				[EventTypeLimitation].[LimitationDays],
				[EventTypeLimitation].[Description]
			FROM
				[dbo].[EventTypeLimitation]
			INNER JOIN EventType AS etFrom ON EventTypeLimitation.EventTypeIDFrom = etFrom.EventTypeID
			INNER JOIN EventType AS etTo ON EventTypeLimitation.EventTypeIDTo = etTo.EventTypeID
			WHERE
				[EventTypeLimitation].[LeadTypeID] = @LeadTypeID
			ORDER BY
				[EventTypeLimitation].[Description]
			
			Select @@ROWCOUNT
			SET ANSI_NULLS ON






GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation__GetWithEventNamesByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation__GetWithEventNamesByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation__GetWithEventNamesByLeadTypeID] TO [sp_executeall]
GO
