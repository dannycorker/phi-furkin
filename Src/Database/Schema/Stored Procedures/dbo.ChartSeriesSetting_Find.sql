SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChartSeriesSetting table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartSeriesSettingID int   = null ,

	@ChartSeriesID int   = null ,

	@SettingName varchar (100)  = null ,

	@SettingValue varchar (MAX)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartSeriesSettingID]
	, [ChartSeriesID]
	, [SettingName]
	, [SettingValue]
	, [ClientID]
    FROM
	[dbo].[ChartSeriesSetting] WITH (NOLOCK) 
    WHERE 
	 ([ChartSeriesSettingID] = @ChartSeriesSettingID OR @ChartSeriesSettingID IS NULL)
	AND ([ChartSeriesID] = @ChartSeriesID OR @ChartSeriesID IS NULL)
	AND ([SettingName] = @SettingName OR @SettingName IS NULL)
	AND ([SettingValue] = @SettingValue OR @SettingValue IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartSeriesSettingID]
	, [ChartSeriesID]
	, [SettingName]
	, [SettingValue]
	, [ClientID]
    FROM
	[dbo].[ChartSeriesSetting] WITH (NOLOCK) 
    WHERE 
	 ([ChartSeriesSettingID] = @ChartSeriesSettingID AND @ChartSeriesSettingID is not null)
	OR ([ChartSeriesID] = @ChartSeriesID AND @ChartSeriesID is not null)
	OR ([SettingName] = @SettingName AND @SettingName is not null)
	OR ([SettingValue] = @SettingValue AND @SettingValue is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Find] TO [sp_executeall]
GO
