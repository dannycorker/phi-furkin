SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-08-17
-- Description:	Helpful details about an Rules Engine Rule
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Rule__Describe] 
	@RuleID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SET NOCOUNT ON;
	
	SELECT	 'Rules' [Rules]
			,r.* 
			,cp.UserName [WhoCreated]
	FROM RulesEngine_Rules r WITH ( NOLOCK ) 
	LEFT JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = r.WhoCreated
	WHERE r.RuleID = @RuleID
	ORDER BY r.RuleOrder

	SELECT 'Parameters' [Parameters]
		,rp.RuleID
		,rp.RuleParameterID
		,rp.Name
		,rp.ParameterTypeID
		,pt.Name [ParameterTypeName]
		,rp.Value
		,rp.DataTypeID
		,dt.Name [DataTypeName]
		,rp.WhenCreated
		,cp.UserName + ' (' + CONVERT(VARCHAR,cp.ClientPersonnelID) + ')' [WhoCreated]
		,rp.WhenModified
		,cpMod.UserName + ' (' + CONVERT(VARCHAR,cpMod.ClientPersonnelID) + ')' [WhoModified]
	FROM RulesEngine_RuleParameters rp WITH ( NOLOCK )
	 LEFT JOIN RulesEngine_ParameterTypes pt WITH ( NOLOCK ) on pt.ParameterTypeID = rp.ParameterTypeID
	 LEFT JOIN RulesEngine_DataTypes dt WITH ( NOLOCK ) on dt.TypeID = rp.DataTypeID
	 LEFT JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = rp.WhoCreated
	 LEFT JOIN ClientPersonnel cpMod WITH ( NOLOCK ) on cpMod.ClientPersonnelID = rp.WhoModified
	WHERE rp.RuleID = @RuleID


	SELECT	 rp.RuleParameterID, rp.Name, rp.Value
			,po.ParameterOptionID,po.Val1, po.Val2, op.OperatorText
			,'...' [...],rp.*
	FROM RulesEngine_RuleParameters rp WITH ( NOLOCK ) 
	INNER JOIN RulesEngine_ParameterOptions po WITH ( NOLOCK ) on po.RuleParameterID = rp.RuleParameterID
	 LEFT JOIN RulesEngine_Operators op WITH ( NOLOCK ) on op.OperatorID = po.OperatorID
	WHERE rp.RuleID = @RuleID

	SELECT	'Rule Outputs' [Outputs]
			,rp.RuleParameterID, ro.RuleOutputID, rp.Name, rp.Value, op.Name [Operator] , po.Val1 [Option1], po.Val2 [Option2], tr.Name [Transform], ro.Value [TransformValue]
			,'...' [...], ro.*
			,'...' [...], po.*
			,'...' [...], oc.*
	FROM RulesEngine_RuleParameters rp WITH ( NOLOCK ) 
	INNER JOIN RulesEngine_ParameterOptions po WITH ( NOLOCK ) on rp.RuleParameterID = po.RuleParameterID
	INNER JOIN RulesEngine_OutputCoordinates oc WITH ( NOLOCK ) on po.ParameterOptionID = oc.ParameterOptionID
	INNER JOIN RulesEngine_RuleOutputs ro WITH ( NOLOCK ) on ro.RuleOutputID = oc.RuleOutputID
	 LEFT JOIN RulesEngine_Transforms tr WITH ( NOLOCK ) on tr.TransformID = ro.TransformID
	 LEFT JOIN RulesEngine_Operators op WITH ( NOLOCK ) on op.OperatorID = po.OperatorID
	WHERE ro.RuleID = @RuleID
	ORDER BY ro.RuleOutputID, rp.RuleParameterID, po.OptionOrder
	
END








GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rule__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Rule__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rule__Describe] TO [sp_executeall]
GO
