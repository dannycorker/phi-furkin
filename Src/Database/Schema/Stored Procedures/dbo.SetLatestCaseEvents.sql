SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-24
-- Description:	Set latest in-proc LeadEvent ID etc on the Cases record
-- =============================================
CREATE PROCEDURE [dbo].[SetLatestCaseEvents] 
	@CaseID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LatestLeadEventID int, 
	@LatestInProcessLeadEventID int, 
	@LatestOutOfProcessLeadEventID int, 
	@LatestNonNoteLeadEventID int, 
	@LatestNoteLeadEventID int 
	
	/* In Process */
	SELECT @LatestInProcessLeadEventID = max(le.LeadEventID) 
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID 
	WHERE le.CaseID = @CaseID 
	AND	le.EventDeleted = 0 
	AND et.InProcess = 1
	
	/* Out Of Process */
	SELECT @LatestOutOfProcessLeadEventID = max(le.LeadEventID) 
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID 
	WHERE le.CaseID = @CaseID 
	AND	le.EventDeleted = 0 
	AND et.InProcess = 0
	
	/* Notes */
	SELECT @LatestNoteLeadEventID = max(le.LeadEventID) 
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.NoteType nt WITH (NOLOCK) on nt.NoteTypeID = le.NoteTypeID 
	WHERE le.CaseID = @CaseID 
	AND	le.EventDeleted = 0 
	
	/* Deduce LatestNonNote from InProcess v OutOfProcess */
	SELECT @LatestNonNoteLeadEventID = dbo.fnMaxOfInt(@LatestInProcessLeadEventID, @LatestOutOfProcessLeadEventID) 
	
	/* Deduce Latest from NonNote v Note */
	SELECT @LatestLeadEventID = dbo.fnMaxOfInt(@LatestNonNoteLeadEventID, @LatestNoteLeadEventID)  
	
	UPDATE dbo.Cases 
	SET LatestLeadEventID = @LatestLeadEventID, 
	LatestInProcessLeadEventID = @LatestInProcessLeadEventID, 
	LatestOutOfProcessLeadEventID = @LatestOutOfProcessLeadEventID, 
	LatestNonNoteLeadEventID = @LatestNonNoteLeadEventID, 
	LatestNoteLeadEventID = @LatestNoteLeadEventID 
	WHERE CaseID = @CaseID
	
	RETURN @@RowCount 
END





GO
GRANT VIEW DEFINITION ON  [dbo].[SetLatestCaseEvents] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SetLatestCaseEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SetLatestCaseEvents] TO [sp_executeall]
GO
