SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the OutcomeEquations table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeEquations_Delete]
(

	@OutcomeEquationID int   
)
AS


				DELETE FROM [dbo].[OutcomeEquations] WITH (ROWLOCK) 
				WHERE
					[OutcomeEquationID] = @OutcomeEquationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeEquations_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Delete] TO [sp_executeall]
GO
