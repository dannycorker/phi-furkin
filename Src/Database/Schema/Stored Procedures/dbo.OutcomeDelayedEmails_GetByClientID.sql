SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the OutcomeDelayedEmails table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeDelayedEmails_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[OutcomeDelayedEmailID],
					[CustomerID],
					[ClientQuestionnaireID],
					[DateOutcomeEmailSent],
					[ClientID]
				FROM
					[dbo].[OutcomeDelayedEmails] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeDelayedEmails_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_GetByClientID] TO [sp_executeall]
GO
