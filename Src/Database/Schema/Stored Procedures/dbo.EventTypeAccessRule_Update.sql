SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeAccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAccessRule_Update]
(

	@EventTypeAccessRuleID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@AccessRuleID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeAccessRule]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[AccessRuleID] = @AccessRuleID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[EventTypeAccessRuleID] = @EventTypeAccessRuleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRule_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_Update] TO [sp_executeall]
GO
