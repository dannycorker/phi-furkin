SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-01-26
-- Description:	CustomerHistory helper
-- =============================================
CREATE PROCEDURE [dbo].[ch] 
	@CustomerID int
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Show all revisions, latest first */
	SELECT ch.WhenChanged, cp.UserName, ch.ChangeAction, ch.ChangeNotes, ch.* 
	FROM dbo.CustomerHistory ch WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = ch.WhoChanged
	WHERE ch.CustomerID = @CustomerID 
	ORDER BY 1 DESC
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ch] TO [sp_executeall]
GO
