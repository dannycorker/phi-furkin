SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================		
-- Author:	Ian Slack		
-- Create date: 2014-09-24		
-- Description:	Save Quote		
-- Mods:
-- 2015-10-?? IS? Added actual save quote SQL
-- 2017-08-14 AHOD Updated to include Send Quote Email - Line 100
-- 2017-08-17 AHOD Updated to restrict Quote Emails		
-- 2017-12-14 GPR  Updated WhenExpires to use AffinityDetails QuoteLapse field
-- 2018-03-15 MAB  Added values for WhenCreated & WhenExpire for INSERT into _C600_SavedQuote to prevent NOT NULL constraint violation
-- 2018-03-15 MAB  Modified node definition for extraction of BrandID
-- 2018-09-06 UAH Temporarily disabled internal saved quote email - line 122
-- 2019-08-27 for JIRA BAULAG-64 | If there is no match claimed, check the DB for this customer and apply MultiPet if we find a probable match
-- 2020-01-08 PR Updated Field lengths to correspond with actual column sizes.
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-01-15 GPR/PR Added customer update for date of birth and mobile telephone number
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Policy_SaveQuote]		
(		
	@XmlRequest NVARCHAR(MAX), --XML,		
	@Email VARCHAR(255),		
	@SecretQuestionID VARCHAR(50),		
	@SecretQuestionValue VARCHAR(250),		
	@SavedQuoteKey VARCHAR(50) = NULL,
	@QuoteSessionID INT = NULL,
	@AggregatorID VARCHAR(50) = NULL,
	@BlockEmail	BIT = 0
)		
AS		
BEGIN

	DECLARE  @QuoteLapse		INT 		-- GPR 2017-12-14 PM28 Client600
			,@BrandID			INT	 		-- GPR 2017-12-14 PM28 Client600		
			,@ExpiryDate		DATETIME	-- MAB 2018-03-15
			--,@AggregatorDefault	VARCHAR(10) = 'true' -- GPR 2019-06-21,
			,@CustomerID		INT
			,@Address1			VARCHAR(100)
			,@ClientID			INT = dbo.fnGetPrimaryClientID()
			,@Postcode			VARCHAR(50)
			,@FirstName			VARCHAR(100)
			,@LastName			VARCHAR(100)			
			,@MobileTelephone	VARCHAR(50)		
			,@HomeTelephone		VARCHAR(50)	
			,@TitleID			INT
			,@DateOfBirth	DATE
			/*GPR 2021-02-15*/
			,@FeeTableRowID	INT
			,@EnrollmentFee NUMERIC(18,2)
			,@TaxTableRowID INT
			,@Tax	NUMERIC(18,2)
			,@Total NUMERIC(18,2)
			,@ProvinceLookupListItemID	INT
			,@PetIndex	INT
			,@StartDate	DATE
			,@County	VARCHAR(200)


	--removing domain limit on outbound emails NG 2017-09-13, ticket #45486
	SELECT @BlockEmail = 1
	
	SELECT @BlockEmail = 0 --WHERE (@Email like '%@aquarium-software.com%')
	
	DECLARE @XmlRequestIn XML = @XmlRequest
	--DECLARE @XMLData VARCHAR(MAX) = CAST(@XmlRequest AS VARCHAR(MAX))		
		
	--exec dbo._C00_LogIt 'Info', 'PA', '[_C600_PA_Policy_SaveQuote]', @XmlRequest, 0		
		
	EXEC [dbo]._C00_LogItXML @ClientID, @QuoteSessionID, '[_C600_PA_Policy_SaveQuote] Response', @XmlRequest	
	
	SELECT	TOP 1 @SavedQuoteKey = SavedQuoteKey 
	FROM	_C600_SavedQuote WITH (NOLOCK) 
	WHERE	(QuoteSessionID = @QuoteSessionID AND @QuoteSessionID > 0)
	ORDER BY SavedQuoteID DESC

	IF @SavedQuoteKey IS NULL		
	BEGIN		
		SELECT @SavedQuoteKey = NEWID()
	END
	
	/* 2018-03-15 Mark Beaumont	- calculate expiry date */
	SELECT @BrandID = @XmlRequestIn.value('(//BrandingId)[1]', 'INT') -- GPR 2017-12-14 PM28 Client600 | MAB 2018-03-15 use //BrandingId to work with all XML request types
	SELECT @QuoteLapse = quotelapse.DetailValue FROM ResourceListDetailValues quotelapse WITH (NOLOCK) WHERE quotelapse.ResourceListID = @BrandID AND quotelapse.DetailFieldID = 179717 -- GPR 2017-12-14 PM28 Client600	
	SELECT @ExpiryDate = dbo.fn_GetDate_Local() + ISNULL(NULLIF(@QuoteLapse,0),30)


	/*GPR 2019-06-21 for #57791*/
	--IF @AggregatorID IS NOT NULL
	--BEGIN
	
	--	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotSms/text())[1] with sql:variable("@AggregatorDefault")');
	--	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotEmail/text())[1] with sql:variable("@AggregatorDefault")');
	--	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotPhone/text())[1] with sql:variable("@AggregatorDefault")');
	--	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/DoNotPost/text())[1] with sql:variable("@AggregatorDefault")');
	
	--END

	
	SELECT		@Address1	= @XmlRequestIn.value('(//Address1)[1]' ,'VARCHAR(200)')
				,@Postcode	= @XmlRequestIn.value('(//Postcode)[1]' ,'VARCHAR(50)')
				,@FirstName	= @XmlRequestIn.value('(//FirstName)[1]','VARCHAR(100)')
				,@LastName	= @XmlRequestIn.value('(//LastName)[1]' ,'VARCHAR(100)')
				,@DateOfBirth = @XmlRequestIn.value('(//DateOfBirth)[1]', 'DATE')				
				,@MobileTelephone = @XmlRequestIn.value('(//MobilePhone)[1]', 'VARCHAR(50)')
				,@HomeTelephone = @XmlRequestIn.value('(//HomePhone)[1]', 'VARCHAR(50)')
				,@CustomerID = @XmlRequestIn.value('(//CustomerId)[1]', 'INT')
				,@TitleID = @XmlRequestIn.value('(//TitleId)[1]', 'INT')


	/*
	CPS 2019-08-27 for JIRA BAULAG-64
	If there is still no match claimed, check the DB for this customer and apply MultiPet if we find a probable match
	*/
	IF (@CustomerID = 0 OR @CustomerID is NULL)
	BEGIN
		
		SELECT TOP (1)
					@CustomerID = cu.CustomerID
		FROM dbo.fn_C600_GetPossibleCustomerMatchesForMultipet(@BrandID,@ClientID,@Postcode,@LastName,@FirstName,@Address1,@XmlRequest, '') cu

		IF @CustomerID <> 0
		BEGIN
			SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/CustomerInfo/CustomerId/text())[1] with sql:variable("@CustomerID")');
		END

		DECLARE @TempLog VARCHAR(2000) 
		SELECT @TempLog = ISNULL(CONVERT(VARCHAR,@BrandID),'NULL') + ',' + ISNULL(CONVERT(VARCHAR,@ClientID),'NULL') + ',' + ISNULL(@Postcode,'NULL') + ',' + ISNULL(@LastName,'NULL') + ',' + ISNULL(@FirstName,'NULL') + ',' + ISNULL(@Address1,'NULL') + ' | @CustomerID = ' + ISNULL(CONVERT(VARCHAR,@CustomerID),'NULL')
		
		EXEC _C00_LogIt 'Info', '_C600_PA_Policy_SaveQuote', 'Probable customer match search', @TempLog, 58552 /*Aquarium Automation*/
	END
	ELSE -- Update customer
	BEGIN

		if(@DateOfBirth<>'0001-01-01') --  empty date of birth
		BEGIN

			UPDATE Customers
			SET DateOfBirth = @DateOfBirth, 				
				MobileTelephone = ISNULL(@MobileTelephone,''),
				HomeTelephone = ISNULL(@HomeTelephone,''),
				TitleID = ISNULL(@TitleID,0)
			FROM Customers c 
			WHERE c.CustomerID = @CustomerID

		END
	END

	/*GPR / AMG 2019-09-23 Does this Customer have existing active Policies? for #59771*/			
			DECLARE @ActivePolicyCount INT = 0
			DECLARE @HasExistingActivePolicy BIT = 0

			SELECT @ActivePolicyCount = COUNT(*)
			FROM Lead l WITH (NOLOCK) 
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.LeadID = l.LeadID AND mdv.DetailFieldID = 170038 /*Policy Status*/
			INNER JOIN MatterDetailValues product WITH ( NOLOCK ) ON product.LeadID = l.LeadID AND product.DetailFieldID = 170034 /*Current Policy*/
			WHERE l.CustomerID = @CustomerID
			AND l.LeadTypeID = 1492 /*Policy Admin*/
			AND mdv.ValueInt IN ( 43002 /*Live*/, 74573 /*Underwriting*/ )
			AND product.ValueInt <> 151577 /*Buddies One Month Free*/

			IF @ActivePolicyCount > 0
			BEGIN
				SELECT @HasExistingActivePolicy = 1
			END

				SET @XmlRequestIn.modify('insert(<HasExistingActivePolicy>ActivePolicy</HasExistingActivePolicy>) as last into (/BuyPolicyRequest[1]/KeyValues[1])');
				SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/KeyValues/HasExistingActivePolicy/text())[1] with sql:variable("@HasExistingActivePolicy")');


	
	/*GPR 2021-02-15 for FURKIN-256*/
	/*GPR 2021-02-24 replaced inner <EnrollmentFee></EnrollmentFee> with <Fee></Fee>*/
	/*
	<PolicyValueEnrollmentFees>
          <EnrollmentFee>
            <FeeTableRowID>FeeTableRowID</FeeTableRowID>
            <Fee>EnrollmentFee</Fee>
            <TaxTableRowID>TaxTableRowID</TaxTableRowID>
            <Tax>Tax</Tax>
            <Total>Total</Total>
          </EnrollmentFee>
        </PolicyValueEnrollmentFees>
	*/
	SET @XmlRequestIn.modify('insert(<PolicyValueEnrollmentFees><EnrollmentFee></EnrollmentFee></PolicyValueEnrollmentFees>) as last into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues)[1]');
	SET @XmlRequestIn.modify('insert(<FeeTableRowID>FeeTableRowID</FeeTableRowID>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<Fee>EnrollmentFee</Fee>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/Fee)[1]'); /*GPR 2021-02-24 updated to Fee*/
	SET @XmlRequestIn.modify('insert(<TaxTableRowID>TaxTableRowID</TaxTableRowID>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<Tax>Tax</Tax>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	SET @XmlRequestIn.modify('insert(<Total>Total</Total>) into (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee)[1]');
	
	SELECT @County = @XmlRequestIn.value('(//County)[1]' ,'VARCHAR(200)')
	SELECT @ProvinceLookupListItemID = CASE @County 
					WHEN 'AB' THEN 61739
					WHEN 'BC' THEN 61740
					WHEN 'MB' THEN 61741
					WHEN 'NB' THEN 61742
					WHEN 'NL' THEN 61743
					WHEN 'NS' THEN 61744
					WHEN 'NT' THEN 61745
					WHEN 'NU' THEN 61746
					WHEN 'ON' THEN 61747
					WHEN 'PE' THEN 61748
					WHEN 'QC' THEN 61749
					WHEN 'SK' THEN 61750
					WHEN 'YT' THEN 61751
				END
	SELECT @PetIndex = 1
	SELECT @StartDate = ISNULL(@XmlRequestIn.value('(//StartDate)[1]' ,'DATE'), dbo.fn_GetDate_Local())

	SELECT @FeeTableRowID = FeeTableRowID, @EnrollmentFee = EnrollmentFee, @TaxTableRowID = TaxTableRowID, @Tax = Tax, @Total = Total
	FROM dbo.fn_C600_GetEnrollmentFee(@BrandID, @CustomerID, @ProvinceLookupListItemID, @PetIndex, @StartDate)

	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/FeeTableRowID/text())[1] with sql:variable("@FeeTableRowID")');
	SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Fee/text())[1] with sql:variable("@EnrollmentFee")'); /*GPR 2021-02-24 updated to Fee*/
	
	IF @County IS NOT NULL /*GPR 2021-02-15 County doesn't exist in the XML until the Customers page which is after the first Save opportunity. Only do the tax and total replacements once we have a County*/
	BEGIN
		SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/TaxTableRowID/text())[1] with sql:variable("@TaxTableRowID")');
		SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Tax/text())[1] with sql:variable("@Tax")');
		SET @XmlRequestIn.modify('replace value of (/BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValueEnrollmentFees/EnrollmentFee/Total/text())[1] with sql:variable("@Total")');
	END

	IF NOT EXISTS (SELECT SavedQuoteID FROM _C600_SavedQuote WITH (NOLOCK) WHERE SavedQuoteKey = @SavedQuoteKey)		
	BEGIN		
		SELECT	 @SecretQuestionID		= ISNULL(@SecretQuestionID,0)
				,@SecretQuestionValue	= ISNULL(@SecretQuestionValue,'')

		/* 2018-03-15 Mark Beaumont	- added values for WhenCreated & WhenExpire to prevent NOT NULL constraint violation */
		INSERT INTO _C600_SavedQuote (SavedQuoteXML, EmailAddress, SecretQuestionID, SecretQuestionValue, WhenCreated, WhenExpire, SavedQuoteKey, AggregatorID, QuoteSessionID)		
		VALUES (@XmlRequestIn, @Email, @SecretQuestionID, @SecretQuestionValue, dbo.fn_GetDate_Local(), @ExpiryDate, @SavedQuoteKey, @AggregatorID, @QuoteSessionID)		
	END	
	ELSE		
	BEGIN

		UPDATE _C600_SavedQuote		
		SET		
			SavedQuoteXML = ISNULL(@XmlRequestIn, SavedQuoteXML),		
			EmailAddress = ISNULL(@Email,EmailAddress),		
			SecretQuestionID = ISNULL(@SecretQuestionID,SecretQuestionID),		
			SecretQuestionValue = ISNULL(@SecretQuestionValue, SecretQuestionValue),		
			WhenCreated = dbo.fn_GetDate_Local(),	
			WhenExpire = @ExpiryDate		-- 2018-03-15 Mark Beaumont
		WHERE SavedQuoteKey = @SavedQuoteKey		
	END		
	
	-- Update the header quote session table
	;WITH CustomerData AS(
	SELECT	
		@XmlRequestIn.value('(//CustomerInfo/Postcode)[1]'	 , 'VARCHAR(50)' ) Postcode,
		@XmlRequestIn.value('(//CustomerInfo/County)[1]'	 , 'VARCHAR(200)') County,
		@XmlRequestIn.value('(//CustomerInfo/FirstName)[1]'	 , 'VARCHAR(100)') FirstName,
		@XmlRequestIn.value('(//CustomerInfo/LastName)[1]'	 , 'VARCHAR(100)') LastName,
		@XmlRequestIn.value('(//CustomerInfo/Email)[1]'		 , 'VARCHAR(255)') Email,
		@XmlRequestIn.value('(//CustomerInfo/HomePhone)[1]'	 , 'varchar(50)' ) HomePhone,
		@XmlRequestIn.value('(//CustomerInfo/MobilePhone)[1]', 'varchar(50)' ) Mobile
	)
	UPDATE qs
	SET 
		qs.FirstName = ISNULL(cd.FirstName, qs.FirstName),
		qs.LastName = ISNULL(cd.LastName,qs.LastName),
		qs.Email = ISNULL(cd.Email,qs.Email),
		qs.PostCode = ISNULL(cd.Postcode,qs.Postcode),
		qs.HomeTel = ISNULL(cd.HomePhone,qs.HomeTel),
		qs.MobileTel = ISNULL(cd.Mobile,qs.MobileTel),
		qs.QuoteCount = qs.QuoteCount + 1
	FROM _C600_QuoteSessions qs 
	INNER JOIN CustomerData cd ON qs.QuoteSessionID = @QuoteSessionID

	SELECT	TOP 1 SavedQuoteID, SavedQuoteKey, CAST(SavedQuoteXML AS NVARCHAR(MAX)) SavedQuoteXML, EmailAddress, SecretQuestionID, SecretQuestionValue, WhenCreated, WhenExpire, AggregatorID, QuoteSessionID, RetrievedDate
	FROM	_C600_SavedQuote WITH (NOLOCK)		
	WHERE	SavedQuoteKey = @SavedQuoteKey		
	ORDER BY SavedQuoteID DESC		

	--IF EXISTS (SELECT * FROM _C600_SavedQuote SQ WITH (NOLOCK) WHERE PetQuoteDefinitionXml is NOT NULL AND SQ.SavedQuoteKey =@SavedQuoteKey)
	--BEGIN
	--	/*AH 2017-08-14 Queue a saved quote email*/
	--	EXEC _C600_PA_Policy_SaveQuote_QueueEmail @SavedQuoteKey
	--END
	
	IF @ClientID NOT IN (604) /*GPR 2020-06-25 for SDAAG-79*/
	BEGIN
		/*GPR 2019-11-18*/
		EXEC _C600_PA_Policy_SaveQuote_QueueEmail @SavedQuoteKey

	END
		
END












GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_SaveQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuote] TO [sp_executeall]
GO
