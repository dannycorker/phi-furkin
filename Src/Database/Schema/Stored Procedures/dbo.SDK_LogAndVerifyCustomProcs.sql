SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-03-02
-- Description:	Log custom proc calls and return allowance to app
-- =============================================
CREATE PROCEDURE [dbo].[SDK_LogAndVerifyCustomProcs]
	@ClientPersonnelID INT,
	@ClientID INT,
	@ProcName VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @IsAllowed BIT = 0

	IF NOT EXISTS (
		SELECT *
		FROM SDKCustomProcSecurity s WITH (NOLOCK)
		WHERE s.ClientID = @ClientID
		AND s.ClientPersonnelID = @ClientPersonnelID
		AND s.ProcName = @ProcName
	)
	BEGIN
	
		INSERT INTO SDKCustomProcSecurity (ClientID, ClientPersonnelID, ProcName, IsAllowed)
		VALUES (@ClientID, @ClientPersonnelID, @ProcName, 1)
	
	END

	SELECT @IsAllowed = s.IsAllowed
	FROM SDKCustomProcSecurity s WITH (NOLOCK)
	WHERE s.ClientID = @ClientID
	AND s.ClientPersonnelID = @ClientPersonnelID
	AND s.ProcName = @ProcName
	
	SELECT @IsAllowed AS [IsProcRunAllowed]
	
	EXEC dbo._C00_LogIt 'SDK', 'CustomProcCheck', @ProcName, @IsAllowed, @ClientPersonnelID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SDK_LogAndVerifyCustomProcs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SDK_LogAndVerifyCustomProcs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SDK_LogAndVerifyCustomProcs] TO [sp_executeall]
GO
