SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Chris Townsend
-- Create date: 2008-11-03
-- Description:	Gets all table detail fields for a particular client
--				and LeadTypeID, with an optional filter for 
--				either Lead (1) Matter (2) or both.
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetLeadAndMatterTableDetailFields] 

	@LeadTypeID int = null, 
	@ClientID int,
	@LeadOrMatterFilter varchar (20) = '1,2'

AS
BEGIN
				
				SELECT df.*
				FROM
					dbo.fnDetailFieldsShared(@ClientID) df
				WHERE
					[ClientID] = @ClientID 
				AND	[LeadOrMatter] IN 
				(SELECT AnyID FROM dbo.fnTableOfIDsFromCSV (@LeadOrMatterFilter))
				AND TableDetailFieldPageID IS NOT NULL
				AND (@LeadTypeID IS NULL OR LeadTypeID = @LeadTypeID)
				AND	[Enabled] = 1
				ORDER BY 
					[FieldName]

				-- Select @@ROWCOUNT

END










GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetLeadAndMatterTableDetailFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetLeadAndMatterTableDetailFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetLeadAndMatterTableDetailFields] TO [sp_executeall]
GO
