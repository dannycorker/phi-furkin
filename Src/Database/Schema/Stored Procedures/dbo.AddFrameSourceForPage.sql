SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddFrameSourceForPage]

@ClientID int, 
@ClientQuestionnaireID int,
@PageNumber int,
@SourceUrl nvarchar(555),
@FrameType int

AS

insert into QuestionnaireFrameSources(ClientID, ClientQuestionnaireID, PageNumber, SourceUrl, FrameType)
values (@ClientID, @ClientQuestionnaireID, @PageNumber, @SourceUrl, @FrameType)


declare @QuestionnaireFrameSourceID int

set @QuestionnaireFrameSourceID = SCOPE_IDENTITY()

select QuestionnaireFrameSourceID
from QuestionnaireFrameSources
where QuestionnaireFrameSourceID = @QuestionnaireFrameSourceID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddFrameSourceForPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddFrameSourceForPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddFrameSourceForPage] TO [sp_executeall]
GO
