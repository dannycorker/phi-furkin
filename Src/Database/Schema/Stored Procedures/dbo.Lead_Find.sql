SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Lead table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadID int   = null ,

	@ClientID int   = null ,

	@LeadRef varchar (100)  = null ,

	@CustomerID int   = null ,

	@LeadTypeID int   = null ,

	@AquariumStatusID int   = null ,

	@ClientStatusID int   = null ,

	@BrandNew bit   = null ,

	@Assigned bit   = null ,

	@AssignedTo int   = null ,

	@AssignedBy int   = null ,

	@AssignedDate datetime   = null ,

	@RecalculateEquations bit   = null ,

	@Password varchar (65)  = null ,

	@Salt varchar (25)  = null ,

	@WhenCreated datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadID]
	, [ClientID]
	, [LeadRef]
	, [CustomerID]
	, [LeadTypeID]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [BrandNew]
	, [Assigned]
	, [AssignedTo]
	, [AssignedBy]
	, [AssignedDate]
	, [RecalculateEquations]
	, [Password]
	, [Salt]
	, [WhenCreated]
    FROM
	[dbo].[Lead] WITH (NOLOCK) 
    WHERE 
	 ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadRef] = @LeadRef OR @LeadRef IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID IS NULL)
	AND ([ClientStatusID] = @ClientStatusID OR @ClientStatusID IS NULL)
	AND ([BrandNew] = @BrandNew OR @BrandNew IS NULL)
	AND ([Assigned] = @Assigned OR @Assigned IS NULL)
	AND ([AssignedTo] = @AssignedTo OR @AssignedTo IS NULL)
	AND ([AssignedBy] = @AssignedBy OR @AssignedBy IS NULL)
	AND ([AssignedDate] = @AssignedDate OR @AssignedDate IS NULL)
	AND ([RecalculateEquations] = @RecalculateEquations OR @RecalculateEquations IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([Salt] = @Salt OR @Salt IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadID]
	, [ClientID]
	, [LeadRef]
	, [CustomerID]
	, [LeadTypeID]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [BrandNew]
	, [Assigned]
	, [AssignedTo]
	, [AssignedBy]
	, [AssignedDate]
	, [RecalculateEquations]
	, [Password]
	, [Salt]
	, [WhenCreated]
    FROM
	[dbo].[Lead] WITH (NOLOCK) 
    WHERE 
	 ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadRef] = @LeadRef AND @LeadRef is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([ClientStatusID] = @ClientStatusID AND @ClientStatusID is not null)
	OR ([BrandNew] = @BrandNew AND @BrandNew is not null)
	OR ([Assigned] = @Assigned AND @Assigned is not null)
	OR ([AssignedTo] = @AssignedTo AND @AssignedTo is not null)
	OR ([AssignedBy] = @AssignedBy AND @AssignedBy is not null)
	OR ([AssignedDate] = @AssignedDate AND @AssignedDate is not null)
	OR ([RecalculateEquations] = @RecalculateEquations AND @RecalculateEquations is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([Salt] = @Salt AND @Salt is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Find] TO [sp_executeall]
GO
