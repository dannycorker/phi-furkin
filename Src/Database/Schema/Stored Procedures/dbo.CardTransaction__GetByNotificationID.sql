SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-04-2018
-- Description:	Gets the card transaction from the world pay notification
-- Please note that this is a specific variant of this stored procedure for L and G:
--
-- Since order code might not be the cardtransaction id 
-- eg., if first payment via eckoh and merchantcode ends in MOTTO 

-- Modified By PR 20/04/2018 - since eckoh have now changed there mind and used our card transaction id
--							it now just uses the card transaction id, left the original sql just in case they change there mind again!
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__GetByNotificationID]
	@WorldPayNotificationID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	Declare @CardTransactionID INT

	SELECT @CardTransactionID = CardTransactionID FROM WorldPayNotification WITH (NOLOCK) 
	WHERE WorldPayNotificationID = @WorldPayNotificationID

	SELECT * FROM CardTransaction WITH (NOLOCK) 
	WHERE CardTransactionID=@CardTransactionID

 --   DECLARE @CardTransactionID_Notification INT
	--DECLARE @OrderCode VARCHAR(250)
	--DECLARE @MerchantCode VARCHAR(250)
	--DECLARE @CardHolder VARCHAR(200)

	--SELECT @CardTransactionID_Notification = CardTransactionID, 
	--	   @OrderCode = OrderCode,
	--	   @MerchantCode = NotificationXML.value('(/paymentService/@merchantCode)[1]', 'nvarchar(max)'),
	--	   @CardHolder = NotificationXML.value('(/paymentService/notify/orderStatusEvent/token/paymentInstrument/cardDetails/cardHolderName/node())[1]', 'nvarchar(max)') 
	--FROM WorldPayNotification WITH (NOLOCK) 
	--WHERE WorldPayNotificationID=@WorldPayNotificationID

	--SELECT @CardHolder = REPLACE(@CardHolder, char(9), '') -- remove TABs
	--SELECT @CardHolder = REPLACE(@CardHolder, ' ' , '')	   -- remove SPACES

	--IF (@MerchantCode = 'BUDDIESMOTO' OR @MerchantCode = 'LANDGPETMOTO') AND (@CardHolder='IVRUser')
	--BEGIN
		
	--	DECLARE @CardTransactionID INT
	--	SELECT @CardTransactionID = CardTransactionId -- look up card transaction id from eckoh logging
	--	FROM EckohLogging WITH (NOLOCK) WHERE EckohId = @OrderCode

	--	IF (@CardTransactionID>0)
	--	BEGIN
	--		SELECT * FROM CardTransaction WITH (NOLOCK) 
	--		WHERE CardTransactionID=@CardTransactionID
	--	END
	--	ELSE -- sanity check may not exist in eckoh logging since its not a first payment
	--	BEGIN
	--		SELECT * FROM CardTransaction WITH (NOLOCK) 
	--		WHERE CardTransactionID=@CardTransactionID_Notification
	--	END

	--END
	--ELSE -- not a 'motto' merchant code - so use notifications from card transaction identity
	--BEGIN

	--	SELECT * FROM CardTransaction WITH (NOLOCK) 
	--	WHERE CardTransactionID=@CardTransactionID_Notification

	--END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__GetByNotificationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__GetByNotificationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__GetByNotificationID] TO [sp_executeall]
GO
