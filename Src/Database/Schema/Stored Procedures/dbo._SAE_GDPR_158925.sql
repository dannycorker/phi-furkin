SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM SOFTWARE
-- Create date: 2020-02-03
-- Description:	Sql After Event
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_GDPR_158925]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	IF @EventTypeID IN (158925)
	BEGIN
		/*GPR 2019-09-20 GDPR CR 2.1.3*/
		IF EXISTS (SELECT TOP 1* FROM Customers cu WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON Cu.CustomerID = l.CustomerID
		INNER JOIN Cases cs WITH (NOLOCK)  ON cs.LeadID = l.LeadID 
		INNER JOIN LeadEvent le WITH (NOLOCK)  ON Le.CaseID = cs.CaseID
		WHERE le.EventTypeID = 158926 AND le.WhoCreated = @WhoCreated AND cu.CustomerID = @CustomerID)
		BEGIN

			SELECT @ErrorMessage = '<br><br><font color="red">Notice: Person who has inititated the GDPR Obfuscation Process cannot Approve the Obfuscation.</font></br><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END
		ELSE
		BEGIN
			EXEC _C600_GDPR_RightToErasure @CustomerID, @LeadEventID, @WhoCreated
		END
	END



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_GDPR_158925] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_GDPR_158925] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_GDPR_158925] TO [sp_executeall]
GO
