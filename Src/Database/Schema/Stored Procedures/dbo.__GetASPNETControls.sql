SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 22/09/2010
-- Description:	Gets ASP.NET field definitions for fields on a DetailFieldPage
-- =============================================
CREATE PROCEDURE [dbo].[__GetASPNETControls]
	
	@DetailFieldPageID int,
	@TextBoxSize varchar(6) = '178px',
	@ReadOnly bit = 0,
	@IncludeLabels bit = 0
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;



;WITH InnerSql AS 
(
      SELECT df.DetailFieldID as GroupID, 
      df.FieldOrder as GroupOrder, 
      dft.DetailFieldID as DetailFieldID, df.QuestionTypeID, 
      ISNULL(dft.FieldName, df.FieldName) as FieldName, ISNULL(dft.FieldCaption, df.FieldCaption) as FieldCaption,
      dft.FieldOrder as FieldOrder, 
      ROW_NUMBER() OVER(PARTITION BY df.DetailFieldID ORDER BY df.FieldOrder ASC) as rnFirst
      FROM dbo.DetailFields df WITH (NOLOCK) 
      LEFT JOIN dbo.DetailFields dft WITH (NOLOCK) ON dft.DetailFieldPageID = df.TableDetailFieldPageID AND dft.DisplayInTableView = 1
      WHERE --df.ClientID = 180 
      --AND df.LeadTypeID = 906 AND
      df.DetailFieldPageID = @DetailFieldPageID
),
FirstLast AS 
(
      SELECT i.GroupID, MAX(i.FieldOrder) as LastInGroup
      FROM InnerSql i 
      GROUP BY i.GroupID
)
SELECT i.GroupID, i.QuestionTypeID, i.FieldName, i.FieldCaption,
	CASE WHEN @IncludeLabels = 1 AND i.DetailFieldID IS NULL THEN '<asp:Label ID="lbl' + REPLACE(i.FieldCaption, ' ', '_') + '" runat="server" Text="' + i.FieldCaption + '" />' ELSE '' END AS Label,
	CASE WHEN i.DetailFieldID IS NULL THEN '' WHEN i.rnFirst = 1 THEN 
	'<asp:GridView ID="df_' + CONVERT(varchar,i.GroupID) + '" runat="server" AutoGenerateColumns="false" style="width: 100%">
	<Columns>' 
	ELSE '' END  +
--'<asp:whatever>' + i.FieldName + '</whatever>' as FieldWhatever,
CASE  
	/* 1 = Text, 7 = currency, 11 = Email */
	WHEN i.QuestionTypeID IN (1,7,11) AND @ReadOnly = 0 THEN '<asp:TextBox ID="df_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" CssClass="input" style="width: '+ @TextBoxSize + '" />' 
	WHEN i.QuestionTypeID IN (1,11) AND @ReadOnly = 1 THEN '<asp:Label ID="dv_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" Text="N/A" />' 
	/* equation */
	WHEN i.QuestionTypeID IN (10) THEN '<asp:Label ID="dv_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" Text="N/A" />' 
	/* 3 = CheckBox */
	WHEN i.QuestionTypeID = 3 AND @ReadOnly = 0 THEN  '<asp:CheckBox ID="df_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" />' 
	WHEN i.QuestionTypeID = 3 AND @ReadOnly = 1 THEN  '<asp:Label ID="dv_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" Text="No" />' 
	/* 4 = Dropdown */
	WHEN i.QuestionTypeID = 4 AND @ReadOnly = 0 THEN '<asp:DropDownList ID="df_' + CONVERT(varchar,i.GroupID)
	+ '" runat="server" />'
	WHEN i.QuestionTypeID = 4 AND @ReadOnly = 1 THEN '<asp:Label ID="dv_' + CONVERT(varchar,i.GroupID)
	+ '" runat="server" />'
	/* 5 = Date */
	WHEN i.QuestionTypeID = 5 AND @ReadOnly = 0 THEN  '<asp:TextBox ID="df_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" CssClass="input" style="width: '+ @TextBoxSize + '" />' 
	WHEN i.QuestionTypeID = 5 AND @ReadOnly = 1 THEN  '<asp:Label ID="dv_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" Text="N/A" />' 
	/* 8 = Integer, 9 = Float */
	WHEN i.QuestionTypeID IN (8,9) AND @ReadOnly = 0 THEN '<asp:TextBox ID="df_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" CssClass="input" />' 
	WHEN i.QuestionTypeID IN (8,9) AND @ReadOnly = 1 THEN '<asp:Label ID="dv_' + CONVERT(varchar,i.GroupID) 
	+ '" runat="server" Text="N/A" />' 
	/* 19 = Table */
	WHEN i.QuestionTypeID = 19 AND @ReadOnly = 0 THEN 
	'
	<asp:TemplateField HeaderText="<strong>' + i.FieldCaption + '</strong>" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
	<ItemTemplate>
		<asp:Label id="dv_' + CONVERT(varchar,i.DetailFieldID) + '" runat="server" />
        <asp:TextBox id="de_' + CONVERT(varchar,i.DetailFieldID) + '" runat="server" Visible="false" />
        </ItemTemplate>
	</asp:TemplateField>
	'
	WHEN i.QuestionTypeID = 19 AND @ReadOnly = 1 THEN 
	'
	<asp:TemplateField HeaderText="<strong>' + i.FieldCaption + '</strong>" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
	<ItemTemplate>
		<asp:Label id="df_' + CONVERT(varchar,i.DetailFieldID) + '" runat="server" />
        </ItemTemplate>
	</asp:TemplateField>
	'
	END + 
	
	CASE WHEN i.DetailFieldID IS NULL THEN '' WHEN i.FieldOrder = fl.LastInGroup THEN 
	CASE WHEN @ReadOnly = 0 THEN
	'
	<asp:TemplateField ItemStyle-Width="15%">
        <ItemTemplate>
            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="RowEdit" CommandArgument="<%# Container.DataItemIndex %>" /> 
            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="RowDelete" CommandArgument="<%# Container.DataItemIndex %>" /> 
            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="RowUpdate" CommandArgument="<%# Container.DataItemIndex %>" Visible="false" /> 
            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="RowCancel" CommandArgument="<%# Container.DataItemIndex %>" Visible="false" /> 
        </ItemTemplate>
    </asp:TemplateField>
	' 
	ELSE '' END + 
	'</Columns>
        <EmptyDataTemplate>
			<p>No records exist.</p>
		</EmptyDataTemplate>
    </asp:GridView>
	' ELSE '' END AS [Control]
FROM InnerSql i 
INNER JOIN FirstLast fl ON fl.GroupID = i.GroupID 
ORDER BY i.GroupOrder, i.FieldOrder


END




GO
GRANT VIEW DEFINITION ON  [dbo].[__GetASPNETControls] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetASPNETControls] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetASPNETControls] TO [sp_executeall]
GO
