SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderSectionLocaterKeyword table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword__GetByDataLoaderMapSectionIDForGrid]
(

	@DataLoaderMapSectionID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					dlslk.[DataLoaderSectionLocaterKeywordID],
					dlslk.[ClientID],
					dlslk.[DataLoaderMapID],
					dlslk.[DataLoaderObjectTypeID],
					dlslk.[DataLoaderMapSectionID],
					dlslk.[Keyword],
					dlkmt.[MatchTypeName],
					dlslk.[KeywordNotes],
					dlslk.[RelativeRow],
					dlslk.[AbsoluteCol],
					dlslk.[IsForSectionStart]
				FROM
					[dbo].[DataLoaderSectionLocaterKeyword] dlslk
				INNER JOIN DataLoaderKeywordMatchType dlkmt ON dlkmt.DataLoaderKeywordMatchTypeID = dlslk.DataLoaderKeywordMatchTypeID
				WHERE
					[DataLoaderMapSectionID] = @DataLoaderMapSectionID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword__GetByDataLoaderMapSectionIDForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword__GetByDataLoaderMapSectionIDForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword__GetByDataLoaderMapSectionIDForGrid] TO [sp_executeall]
GO
