SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Louis Bromilow (via CS)
-- Create date: 2012-07-23
-- Description:	Remove dodgy rtf from document type
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DocumentTypeRTFRepair]
	@DocumentTypeID int,
	@ClientID int,
	@ReadOnly bit = 1,
	@RtfStrings AS VARCHAR(MAX) = '[\rtlch][\f1]'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @T TABLE (id INT, [TEXT] NVARCHAR(MAX))
	DECLARE @RTFString AS VARCHAR(MAX),
			@Charrsid AS VARCHAR(MAX),
			@TableRow AS INT,
			@Corrupted AS VARCHAR(MAX),
			@Fixed AS VARCHAR(MAX),
			@String AS VARCHAR(MAX),
			@ModifiedString AS VARCHAR(MAX),
			@LogEntry varchar(500)

	DECLARE @Lots TABLE (Charrsid NVARCHAR(MAX),ROW INT,RESULT VARCHAR(MAX))
	
	IF NOT EXISTS (  SELECT * 
	                 FROM dbo.DocumentType dt WITH (NOLOCK) 
	                 WHERE dt.DocumentTypeID = @DocumentTypeID
	                 and dt.ClientID = @ClientID )
	BEGIN
		RAISERROR( 'FAIL: DocumentType is not for this Client', 16, 1 )
	END
	ELSE
	BEGIN

		SET @TableRow = 1

		SELECT @RTFString = d.Template FROM DocumentType d WITH (NOLOCK) WHERE d.DocumentTypeID = @DocumentTypeID

		SELECT d.Template AS [!!!Take a BACKUP!!!] FROM DocumentType d WITH (NOLOCK) WHERE d.DocumentTypeID = @DocumentTypeID 

		IF @DocumentTypeID is null
		BEGIN
			RAISERROR( 'FAIL!  @DocumentTypeID is null', 16, 1 )
		END
		ELSE
		BEGIN

			Select @LogEntry = 'Starting fix on DocID ' + convert(varchar,@DocumentTypeID)
			exec _C00_LogIt 'Info', 'Louis Doc Fix', 'Start Of Log', @LogEntry, 7301
			
			INSERT INTO @T VALUES (1, @RTFString)

			;WITH cte( [TEXT], rtf) /*Select all the fields from the document*/
			AS
			(
				SELECT 
					RIGHT([TEXT], LEN([TEXT]) - CHARINDEX(']', [TEXT], 0)),
					SUBSTRING([TEXT], CHARINDEX('[', [TEXT], 0) + 1, CHARINDEX(']', [TEXT], 0) - CHARINDEX('[', [TEXT], 0) - 1) 
				FROM @T
				WHERE CHARINDEX('[', [TEXT], 0) > 0
			    
				UNION ALL
				SELECT 
					RIGHT([TEXT], LEN([TEXT]) - CHARINDEX(']', [TEXT], 0)),
					SUBSTRING([TEXT], CHARINDEX('[', [TEXT], 0) + 1, CHARINDEX(']', [TEXT], 0) - CHARINDEX('[', [TEXT], 0) - 1) 
				FROM cte
				WHERE CHARINDEX('[', [TEXT], 0) > 0
				AND CHARINDEX(']', [TEXT], 0) > CHARINDEX('[', [TEXT], 0) - 1
			)

			INSERT @Lots (Charrsid,ROW)
			SELECT RIGHT(rtf,LEN(rtf) + 1 - CHARINDEX('[', rtf) - 1),ROW_NUMBER() OVER(ORDER BY TEXT) /*Attempt to fix bad strings*/
			FROM cte
			WHERE CHARINDEX('[', RIGHT(rtf,LEN(rtf) + 1 - CHARINDEX('[', rtf) - 1), 0) = 0 /*Strip out bad strings that contain [*/
			AND '['+ RIGHT(rtf,LEN(rtf) + 1 - CHARINDEX('[', rtf) - 1)+']' NOT IN (Select Target from FieldTarget)
			OPTION (MAXRECURSION 20000)

			UPDATE @Lots
			SET RESULT = 	

				Charrsid

			FROM @Lots

			SELECT * FROM @Lots
			
            /*Strip out fields identified as valid fields*/
            Delete From @Lots 
            Where  Replace(Replace(Replace(Replace(Replace(Charrsid,'!A:',''),'!',''),' ',''),CHAR(13),''),CHAR(10),'') in 
            (Select Replace(DetailFieldAlias,' ','') 
            from DetailFieldAlias d WITH (NOLOCK) 
            where d.ClientID = @ClientID
            group by DetailFieldAlias)
            
            Delete From @Lots 
            Where  Replace(Replace(Replace(Replace(Replace(Charrsid,'!A:',''),'!',''),' ',''),CHAR(13),''),CHAR(10),'') in 
            (Select Replace(df.FieldName+', '+dfa.DetailFieldAlias,' ','')
            from DetailFieldAlias dfa WITH (NOLOCK) 
            inner join DetailFields d WITH (NOLOCK) on d.DetailFieldID = dfa.DetailFieldID
            inner join DetailFields df WITH (NOLOCK) on  df.ResourceListDetailFieldPageID = d.DetailFieldPageID
            Where dfa.ClientID = @ClientID      
            and d.LeadOrMatter = 4
            group by df.FieldName+', '+dfa.DetailFieldAlias)
            
            Delete From @Lots
            Where Charrsid in
            (Select Replace(Replace(SubstitutionsVariable,'[!',''),']','') from Substitutions)

			SELECT * FROM @Lots
			

			WHILE @TableRow <= (SELECT COUNT(*) FROM @Lots) /*While you have not reached the end of the table*/
			BEGIN

				SELECT @String = l.Charrsid + ' '
					FROM @Lots l 
					WHERE l.Row = @TableRow 		

				SET @RtfStrings = '[\rtlch][\f1]' /*Crap you want to remove*/

				IF (PATINDEX ('%'+@RtfStrings+'%',@String + ' ') = 0 OR PATINDEX ('%'+@RtfStrings+'%',@String + ' ') IS NULL) /*Skip the Field if it is OK*/
				BEGIN 
					SELECT @Fixed = l.Charrsid
					FROM   @Lots l 
					WHERE l.Row = @TableRow
				END

				WHILE PATINDEX ('%'+@RtfStrings+'%',@String + ' ') > 0 /*Start Attempting to Fix the Field*/

				BEGIN

					IF PATINDEX('%'+@RtfStrings+'%', @String) < LEN(@String) AND PATINDEX('%'+@RtfStrings+'%', @String) > 0 /*It breaks if the value goes negative*/

					BEGIN

						SET @ModifiedString = 
						LEFT(@String,PATINDEX('%'+@RtfStrings+'%', @String) - 1)+  /*Get Everything to the left of the thing you want*/
						RIGHT(@String,LEN(@String) +1 - CHARINDEX(' ', @String,PATINDEX('%'+@RtfStrings+'%', @String))) /*Get everything to the right of the first space after what you want*/
						/*Add them togeather and ignore what was in the middle*/

						SET @String = @ModifiedString

						IF PATINDEX ('%'+@RtfStrings+'%',@String + ' ') <= 0 
							BEGIN SELECT @Fixed =
							RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@String,
								'  ',''),
								'{',''),
								'}',''),
								'\rtlch',''),
								'\fcs1',''),
								'\af1',''),
								'\afs20',''),
								'\ltrch',''),
								'\fcs0',''),
								'\f1',''),
								'\fs20',''),
								'\lang1024',''),
								'\langfe1024',''),
								'\noproof',''),
								'\langnp1024',''),
								'\langfenp1024',''),
								'\hic',''),
								'\pard',''),
								'\plain',''),
								'\itrpa',''),
								'\par',''),
								'\rt',''),
								'\r',''),
								'  ',''))				
						END
					END
				END

				SELECT @Corrupted = l.Charrsid
				FROM   @Lots l 
				WHERE l.Row = @TableRow
				
				SET @TableRow = @TableRow + 1
				

				IF @Corrupted <> @Fixed
				BEGIN
					SELECT @TableRow,'['+@Fixed+']' AS [Fixed],'['+@Corrupted+']' AS [Corrupted]

					IF @ReadOnly = 0
					BEGIN
						UPDATE DocumentType
						SET Template = REPLACE(Template,@Corrupted,@Fixed)
						WHERE DocumentTypeID = @DocumentTypeID
						AND @Corrupted not like '%PartnerFirstname%'
					END
				END

				Select @LogEntry = 'Loop ' + convert(varchar,@TableRow)
				exec _C00_LogIt 'Info', 'Louis Doc Fix', 'Running Loop', @LogEntry, 7301

			END
			
			exec _C00_LogIt 'Info', 'Louis Doc Fix', 'End Of Log', 'Finished', 7301
		END	
	END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DocumentTypeRTFRepair] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DocumentTypeRTFRepair] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DocumentTypeRTFRepair] TO [sp_executeall]
GO
