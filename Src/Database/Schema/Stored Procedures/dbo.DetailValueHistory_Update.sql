SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DetailValueHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_Update]
(

	@DetailValueHistoryID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@LeadOrMatter tinyint   ,

	@LeadID int   ,

	@MatterID int   ,

	@FieldValue varchar (2000)  ,

	@WhenSaved datetime   ,

	@ClientPersonnelID int   ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@CustomerID int   ,

	@CaseID int   ,

	@ContactID int   ,

	@ClientPersonnelDetailValueID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DetailValueHistory]
				SET
					[ClientID] = @ClientID
					,[DetailFieldID] = @DetailFieldID
					,[LeadOrMatter] = @LeadOrMatter
					,[LeadID] = @LeadID
					,[MatterID] = @MatterID
					,[FieldValue] = @FieldValue
					,[WhenSaved] = @WhenSaved
					,[ClientPersonnelID] = @ClientPersonnelID
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[CustomerID] = @CustomerID
					,[CaseID] = @CaseID
					,[ContactID] = @ContactID
					,[ClientPersonnelDetailValueID] = @ClientPersonnelDetailValueID
				WHERE
[DetailValueHistoryID] = @DetailValueHistoryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Update] TO [sp_executeall]
GO
