SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventSubtype table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_GetByEventSubtypeName]
(

	@EventSubtypeName varchar (50)  
)
AS


				SELECT
					[EventSubtypeID],
					[EventSubtypeName],
					[EventSubtypeDescription],
					[ApplyEventMaxThreshold],
					[Available]
				FROM
					[dbo].[EventSubtype] WITH (NOLOCK) 
				WHERE
										[EventSubtypeName] = @EventSubtypeName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_GetByEventSubtypeName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_GetByEventSubtypeName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_GetByEventSubtypeName] TO [sp_executeall]
GO
