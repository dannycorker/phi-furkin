SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetOutcomeCountsWithinDates] 

@ClientQuestionnaireID int,
@OutcomeID int,
@FromDate datetime,
@ToDate datetime

AS

SELECT     dbo.CustomerOutcomes.ClientQuestionnaireID, COUNT(dbo.CustomerOutcomes.CustomerID) AS CountOfCustomers, 
                      dbo.CustomerOutcomes.OutcomeID
FROM         dbo.CustomerOutcomes INNER JOIN
                      dbo.Customers ON dbo.CustomerOutcomes.CustomerID = dbo.Customers.CustomerID INNER JOIN
                      dbo.Outcomes ON dbo.CustomerOutcomes.OutcomeID = dbo.Outcomes.OutcomeID INNER JOIN
                      dbo.CustomerQuestionnaires ON dbo.Customers.CustomerID = dbo.CustomerQuestionnaires.CustomerID
WHERE     (dbo.CustomerQuestionnaires.SubmissionDate > @FromDate) AND 
                      (dbo.CustomerQuestionnaires.SubmissionDate < @ToDate)
GROUP BY dbo.CustomerOutcomes.ClientQuestionnaireID, dbo.CustomerOutcomes.OutcomeID
HAVING      (dbo.CustomerOutcomes.ClientQuestionnaireID = @ClientQuestionnaireID) AND (dbo.CustomerOutcomes.OutcomeID = @OutcomeID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCountsWithinDates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeCountsWithinDates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCountsWithinDates] TO [sp_executeall]
GO
