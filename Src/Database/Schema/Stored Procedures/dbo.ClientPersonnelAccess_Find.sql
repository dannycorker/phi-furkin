SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPersonnelAccess table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPersonnelAccessID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientPersonnelID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@AccessRuleID int   = null ,

	@SourceLeadEventID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@AccessLevel tinyint   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPersonnelAccessID]
	, [ClientID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [LeadID]
	, [CaseID]
	, [AccessRuleID]
	, [SourceLeadEventID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [AccessLevel]
    FROM
	[dbo].[ClientPersonnelAccess] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelAccessID] = @ClientPersonnelAccessID OR @ClientPersonnelAccessID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([AccessRuleID] = @AccessRuleID OR @AccessRuleID IS NULL)
	AND ([SourceLeadEventID] = @SourceLeadEventID OR @SourceLeadEventID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([AccessLevel] = @AccessLevel OR @AccessLevel IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPersonnelAccessID]
	, [ClientID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [LeadID]
	, [CaseID]
	, [AccessRuleID]
	, [SourceLeadEventID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [AccessLevel]
    FROM
	[dbo].[ClientPersonnelAccess] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelAccessID] = @ClientPersonnelAccessID AND @ClientPersonnelAccessID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([AccessRuleID] = @AccessRuleID AND @AccessRuleID is not null)
	OR ([SourceLeadEventID] = @SourceLeadEventID AND @SourceLeadEventID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([AccessLevel] = @AccessLevel AND @AccessLevel is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Find] TO [sp_executeall]
GO
