SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2017-11-09
-- Description:	Called from External quote and buy when card payment is selected. Used to create the card transaction record and a linked customer 
--				so we can track the transaction
-- Mods
-- 2020-01-13 CPS for JIRA LPC-356 | Removed hard-coded @ClientID 600 section that was encapsulating the second stored proc call.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_BuyPolicy_CreateCustomerandCardTransaction]
(
	@ClientID INT,
	@Title INT,
	@Firstname VARCHAR(2000), 
	@Lastname VARCHAR(2000), 
	@Email VARCHAR(2000), 
	@HomePhone VARCHAR(2000), 
	@Mobile VARCHAR(2000), 
	@Address1 VARCHAR(2000), 
	@Address2 VARCHAR(2000), 
	@TownCity VARCHAR(2000), 
	@County VARCHAR(2000),
	@Postcode VARCHAR(2000),
	@CustDoB Date ,
	@WhoCreated INT,
	@Amount DECIMAL (18,2),
	@CustomerID INT = 0,
	@BrandID INT, 
	@SessionID INT
)
AS
BEGIN

	EXEC _C600_BuyPolicy_CreateCustomerandCardTransaction 	@Title , @Firstname,@Lastname,@Email,@HomePhone,@Mobile , @Address1 , @Address2,@TownCity ,@County ,@Postcode,@CustDoB,@WhoCreated ,@Amount ,@CustomerID ,@BrandID, @SessionID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BuyPolicy_CreateCustomerandCardTransaction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BuyPolicy_CreateCustomerandCardTransaction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BuyPolicy_CreateCustomerandCardTransaction] TO [sp_executeall]
GO
