SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 21-Dec-2012
-- Description:	Proc to Create a duplidate a matter in an existing case
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateDuplicateMatterForCase]
(
	@MatterID int, 
	@WhoCreated int
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare	@myERROR int,
			@NewMatterID int,
			@RefLetter varchar(3),
			@MatterStatus int,
			@CustomerID int,
			@LeadTypeID int,
			@ClientID int ,
			@LeadID int,
			@CaseID int

	Select top 1 @CustomerID = Matter.CustomerID,
		@ClientID = Matter.ClientID,
		@MatterStatus = Matter.MatterStatus,
		@CaseID = Matter.CaseID
	From Matter WITH (NOLOCK)
	Where Matter.MatterID = @MatterID

	Select @LeadID = c.LeadID
	From Cases c WITH (NOLOCK)
	WHERE c.CaseID = @CaseID

	select @RefLetter = dbo.fnRefLetterFromCaseNum(1+COUNT(*)) 
	from matter WITH (NOLOCK)
	where leadid = @LeadID

	/* Create Matter */
	INSERT INTO Matter([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID])
	VALUES (@ClientID,'',@CustomerID,@LeadID,@MatterStatus,@RefLetter,0,@CaseID)

	Select @NewMatterID = SCOPE_IDENTITY()

	insert into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue) 
	SELECT mdv.ClientID,mdv.LeadID,@NewMatterID,mdv.DetailFieldID,mdv.DetailValue 
	FROM Cases ca WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON ca.LeadID=l.LeadID
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.LeadTypeID=l.LeadTypeID and df.Enabled=1 and df.LeadOrMatter=2
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID=df.DetailFieldID and mdv.MatterID=@MatterID
	WHERE ca.CaseID=@CaseID

	Return @NewMatterID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDuplicateMatterForCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateDuplicateMatterForCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDuplicateMatterForCase] TO [sp_executeall]
GO
