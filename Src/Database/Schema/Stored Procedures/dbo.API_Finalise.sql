SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_Finalise
-- =============================================
CREATE PROCEDURE [dbo].[API_Finalise]
	@QID UNIQUEIDENTIFIER
AS
BEGIN
	
	DECLARE @XmlOut NVARCHAR(MAX) =
	(
		SELECT	QueueID, Handled, [Status]
		FROM 
		(
			SELECT	QueueIdx, QueueID, HandleDT Handled, ISNULL(ErrorMessage, 'Root') [Status]
			FROM	API_Queue WITH (NOLOCK)
			WHERE	QueueID = @QID
			UNION ALL
			SELECT	QueueIdx, QueueID, HandleDT Handled, ISNULL(ErrorMessage, 'Success') [Status]
			FROM	API_Queue WITH (NOLOCK)
			WHERE	ParentQID = @QID
			
		) list
		ORDER BY QueueIdx
		FOR XML AUTO, ELEMENTS, ROOT('response')
	)
	 
	SELECT	@XmlOut = 
			REPLACE(
				REPLACE(@XmlOut,'<response>','<response xmlns:json=''http://james.newtonking.com/projects/json''>'), 
			'<list>', '<list json:Array=''true''>')

	SELECT @XmlOut = ISNULL(@XmlOut, '<response>'+CAST(@QID AS VARCHAR(50))+'</response>')

	UPDATE API_Queue
		SET HandleXml = @XmlOut,
			HandleDT = SYSDATETIME(),
			QueueByteSize = ISNULL(QueueByteSize, 0) + DATALENGTH(ISNULL(@XmlOut,''))
	WHERE	QueueID = @QID
	AND		HandleDT IS NULL
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Finalise] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Finalise] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Finalise] TO [sp_executeall]
GO
