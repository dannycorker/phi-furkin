SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FAS_CheckerData table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_CheckerData_Find]
(

	@SearchUsingOR bit   = null ,

	@FasCheckerDataID int   = null ,

	@FasUniqueReferenceNumber varchar (100)  = null ,

	@TitleOfAgreement varchar (512)  = null ,

	@IsDebtorsNameOrAddressMissing bit   = null ,

	@IsCreditorsNameOrAddressMissing bit   = null ,

	@AgreementDebtorSignatureDate datetime   = null ,

	@NameOfCreditor varchar (512)  = null ,

	@AmountOfCredit decimal (18, 0)  = null ,

	@TotalCashPrice decimal (18, 0)  = null ,

	@DurationOfAgreementInMonths int   = null ,

	@AnnualPercentageRate float   = null ,

	@RateOfInterest float   = null ,

	@FixedOrVariableRateOfInterest varchar (50)  = null ,

	@TotalInterest decimal (18, 0)  = null ,

	@TotalChargeFees decimal (18, 0)  = null ,

	@TotalChargeForCredit decimal (18, 0)  = null ,

	@FirstPayment decimal (18, 0)  = null ,

	@ConstantRegularOrSubsequentPayment decimal (18, 0)  = null ,

	@LastPayment decimal (18, 0)  = null ,

	@DeferredOrBallonPayment decimal (18, 0)  = null ,

	@TotalPayments decimal (18, 0)  = null ,

	@AdvancePayment decimal (18, 0)  = null ,

	@TotalAmountPayable decimal (18, 0)  = null ,

	@Category char (1)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FasCheckerDataID]
	, [FasUniqueReferenceNumber]
	, [TitleOfAgreement]
	, [IsDebtorsNameOrAddressMissing]
	, [IsCreditorsNameOrAddressMissing]
	, [AgreementDebtorSignatureDate]
	, [NameOfCreditor]
	, [AmountOfCredit]
	, [TotalCashPrice]
	, [DurationOfAgreementInMonths]
	, [AnnualPercentageRate]
	, [RateOfInterest]
	, [FixedOrVariableRateOfInterest]
	, [TotalInterest]
	, [TotalChargeFees]
	, [TotalChargeForCredit]
	, [FirstPayment]
	, [ConstantRegularOrSubsequentPayment]
	, [LastPayment]
	, [DeferredOrBallonPayment]
	, [TotalPayments]
	, [AdvancePayment]
	, [TotalAmountPayable]
	, [Category]
    FROM
	[dbo].[FAS_CheckerData] WITH (NOLOCK) 
    WHERE 
	 ([FasCheckerDataID] = @FasCheckerDataID OR @FasCheckerDataID IS NULL)
	AND ([FasUniqueReferenceNumber] = @FasUniqueReferenceNumber OR @FasUniqueReferenceNumber IS NULL)
	AND ([TitleOfAgreement] = @TitleOfAgreement OR @TitleOfAgreement IS NULL)
	AND ([IsDebtorsNameOrAddressMissing] = @IsDebtorsNameOrAddressMissing OR @IsDebtorsNameOrAddressMissing IS NULL)
	AND ([IsCreditorsNameOrAddressMissing] = @IsCreditorsNameOrAddressMissing OR @IsCreditorsNameOrAddressMissing IS NULL)
	AND ([AgreementDebtorSignatureDate] = @AgreementDebtorSignatureDate OR @AgreementDebtorSignatureDate IS NULL)
	AND ([NameOfCreditor] = @NameOfCreditor OR @NameOfCreditor IS NULL)
	AND ([AmountOfCredit] = @AmountOfCredit OR @AmountOfCredit IS NULL)
	AND ([TotalCashPrice] = @TotalCashPrice OR @TotalCashPrice IS NULL)
	AND ([DurationOfAgreementInMonths] = @DurationOfAgreementInMonths OR @DurationOfAgreementInMonths IS NULL)
	AND ([AnnualPercentageRate] = @AnnualPercentageRate OR @AnnualPercentageRate IS NULL)
	AND ([RateOfInterest] = @RateOfInterest OR @RateOfInterest IS NULL)
	AND ([FixedOrVariableRateOfInterest] = @FixedOrVariableRateOfInterest OR @FixedOrVariableRateOfInterest IS NULL)
	AND ([TotalInterest] = @TotalInterest OR @TotalInterest IS NULL)
	AND ([TotalChargeFees] = @TotalChargeFees OR @TotalChargeFees IS NULL)
	AND ([TotalChargeForCredit] = @TotalChargeForCredit OR @TotalChargeForCredit IS NULL)
	AND ([FirstPayment] = @FirstPayment OR @FirstPayment IS NULL)
	AND ([ConstantRegularOrSubsequentPayment] = @ConstantRegularOrSubsequentPayment OR @ConstantRegularOrSubsequentPayment IS NULL)
	AND ([LastPayment] = @LastPayment OR @LastPayment IS NULL)
	AND ([DeferredOrBallonPayment] = @DeferredOrBallonPayment OR @DeferredOrBallonPayment IS NULL)
	AND ([TotalPayments] = @TotalPayments OR @TotalPayments IS NULL)
	AND ([AdvancePayment] = @AdvancePayment OR @AdvancePayment IS NULL)
	AND ([TotalAmountPayable] = @TotalAmountPayable OR @TotalAmountPayable IS NULL)
	AND ([Category] = @Category OR @Category IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FasCheckerDataID]
	, [FasUniqueReferenceNumber]
	, [TitleOfAgreement]
	, [IsDebtorsNameOrAddressMissing]
	, [IsCreditorsNameOrAddressMissing]
	, [AgreementDebtorSignatureDate]
	, [NameOfCreditor]
	, [AmountOfCredit]
	, [TotalCashPrice]
	, [DurationOfAgreementInMonths]
	, [AnnualPercentageRate]
	, [RateOfInterest]
	, [FixedOrVariableRateOfInterest]
	, [TotalInterest]
	, [TotalChargeFees]
	, [TotalChargeForCredit]
	, [FirstPayment]
	, [ConstantRegularOrSubsequentPayment]
	, [LastPayment]
	, [DeferredOrBallonPayment]
	, [TotalPayments]
	, [AdvancePayment]
	, [TotalAmountPayable]
	, [Category]
    FROM
	[dbo].[FAS_CheckerData] WITH (NOLOCK) 
    WHERE 
	 ([FasCheckerDataID] = @FasCheckerDataID AND @FasCheckerDataID is not null)
	OR ([FasUniqueReferenceNumber] = @FasUniqueReferenceNumber AND @FasUniqueReferenceNumber is not null)
	OR ([TitleOfAgreement] = @TitleOfAgreement AND @TitleOfAgreement is not null)
	OR ([IsDebtorsNameOrAddressMissing] = @IsDebtorsNameOrAddressMissing AND @IsDebtorsNameOrAddressMissing is not null)
	OR ([IsCreditorsNameOrAddressMissing] = @IsCreditorsNameOrAddressMissing AND @IsCreditorsNameOrAddressMissing is not null)
	OR ([AgreementDebtorSignatureDate] = @AgreementDebtorSignatureDate AND @AgreementDebtorSignatureDate is not null)
	OR ([NameOfCreditor] = @NameOfCreditor AND @NameOfCreditor is not null)
	OR ([AmountOfCredit] = @AmountOfCredit AND @AmountOfCredit is not null)
	OR ([TotalCashPrice] = @TotalCashPrice AND @TotalCashPrice is not null)
	OR ([DurationOfAgreementInMonths] = @DurationOfAgreementInMonths AND @DurationOfAgreementInMonths is not null)
	OR ([AnnualPercentageRate] = @AnnualPercentageRate AND @AnnualPercentageRate is not null)
	OR ([RateOfInterest] = @RateOfInterest AND @RateOfInterest is not null)
	OR ([FixedOrVariableRateOfInterest] = @FixedOrVariableRateOfInterest AND @FixedOrVariableRateOfInterest is not null)
	OR ([TotalInterest] = @TotalInterest AND @TotalInterest is not null)
	OR ([TotalChargeFees] = @TotalChargeFees AND @TotalChargeFees is not null)
	OR ([TotalChargeForCredit] = @TotalChargeForCredit AND @TotalChargeForCredit is not null)
	OR ([FirstPayment] = @FirstPayment AND @FirstPayment is not null)
	OR ([ConstantRegularOrSubsequentPayment] = @ConstantRegularOrSubsequentPayment AND @ConstantRegularOrSubsequentPayment is not null)
	OR ([LastPayment] = @LastPayment AND @LastPayment is not null)
	OR ([DeferredOrBallonPayment] = @DeferredOrBallonPayment AND @DeferredOrBallonPayment is not null)
	OR ([TotalPayments] = @TotalPayments AND @TotalPayments is not null)
	OR ([AdvancePayment] = @AdvancePayment AND @AdvancePayment is not null)
	OR ([TotalAmountPayable] = @TotalAmountPayable AND @TotalAmountPayable is not null)
	OR ([Category] = @Category AND @Category is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_CheckerData_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Find] TO [sp_executeall]
GO
