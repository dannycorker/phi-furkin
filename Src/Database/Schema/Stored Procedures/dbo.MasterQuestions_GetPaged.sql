SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the MasterQuestions table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [MasterQuestionID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([MasterQuestionID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [MasterQuestionID]'
				SET @SQL = @SQL + ' FROM [dbo].[MasterQuestions] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[MasterQuestionID], O.[ClientQuestionnaireID], O.[QuestionTypeID], O.[QuestionText], O.[QuestionOrder], O.[DefaultAnswerID], O.[Mandatory], O.[QuestionToolTip], O.[LinkedQuestionnaireMasterQuestionID], O.[Active], O.[MasterQuestionStatus], O.[TextboxHeight], O.[ClientID], O.[AnswerPosition], O.[DisplayAnswerAs], O.[NumberOfAnswersPerRow], O.[SourceID], O.[WhoCreated], O.[WhenCreated], O.[WhoModified], O.[WhenModified], O.[QuestionPossibleAnswerSortOrder]
				FROM
				    [dbo].[MasterQuestions] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[MasterQuestionID] = PageIndex.[MasterQuestionID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[MasterQuestions] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_GetPaged] TO [sp_executeall]
GO
