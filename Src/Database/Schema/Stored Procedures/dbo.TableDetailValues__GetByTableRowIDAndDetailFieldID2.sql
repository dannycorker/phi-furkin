SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-06-29
-- Description:	Get a pair of TDVs optimised for the Sage Bridge app (eg Invoice number and Invoice amount).
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID2] 
	@TableRowID int,
	@ClientID int,
	@DetailFieldID1 int,
	@DetailFieldID2 int
AS
BEGIN
	SET NOCOUNT ON

	/*
		The same table can appear more than once on a display page.
		This is the case for Sage Invoice and Sage Payments.
		This proc is used to return the Invoice Number to our Sage Bridge app.
	*/
	SELECT
		[TableDetailValueID],
		[TableRowID],
		[ResourceListID],
		[DetailFieldID],
		[DetailValue],
		[LeadID],
		[MatterID],
		[ClientID],
		[EncryptedValue],
		[ErrorMsg],
		[ValueInt],
		[ValueMoney],
		[ValueDate],
		[ValueDateTime],
		[CustomerID],
		[CaseID],
		[ClientPersonnelID],
		[ContactID]		
	FROM
		[dbo].[TableDetailValues] WITH (NOLOCK)  
	WHERE
		  ( [TableRowID] = @TableRowID ) AND
	      (	[ClientID] = @ClientID ) AND
	      ( [DetailFieldID] = @DetailFieldID1 OR [DetailFieldID] = @DetailFieldID2 )
	ORDER BY [DetailFieldID], [DetailValue] 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndDetailFieldID2] TO [sp_executeall]
GO
