SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 11/07/2017
-- Description:	Gets document type version information
-- =============================================
CREATE PROCEDURE [dbo].[DocumentTypeVersion__GetVersionInformation]
	@DocumentTypeID INT
AS
BEGIN
		
	SET NOCOUNT ON;

    SELECT dtv.DocumentTypeVersionID, 
		   dtv.DocumentTypeID, 
		   dtv.VersionNumber, 
		   CONVERT(varchar(10), dtv.VersionNumber) + ', ' + LEFT(CONVERT(VARCHAR, dtv.CreatedOn, 100), 22) VersionName,
		   dtv.ActiveFromDate, 		   
		   dtv.CreatedOn, 
		   dtv.CreatedBy, 
		   dtv.WhoModified, 
		   dtv.WhenModified, 
		   dtv.Archived,
		   cpCreatedBy.FirstName CreatedByFirstName, 
		   cpCreatedBy.LastName CreatedByLastName, 
		   cpModifiedBy.FirstName ModifiedByFirstName, 
		   cpModifiedBy.LastName ModifiedByLastName
	FROM DocumentTypeVersion dtv WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cpCreatedBy WITH (NOLOCK) ON cpCreatedBy.ClientPersonnelID=dtv.CreatedBy
	INNER JOIN dbo.ClientPersonnel cpModifiedBy WITH (NOLOCK) ON cpModifiedBy.ClientPersonnelID=dtv.WhoModified
	WHERE dtv.DocumentTypeID=@DocumentTypeID
	ORDER BY VersionNumber DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__GetVersionInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion__GetVersionInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__GetVersionInformation] TO [sp_executeall]
GO
