SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CheckMateFieldMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@CheckMateFieldMappingID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@CheckMateFieldID int   = null ,

	@FieldID int   = null ,

	@ColumnFieldID int   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@CheckMateFieldGroupID int   = null ,

	@IsEnabled bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CheckMateFieldMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [CheckMateFieldID]
	, [FieldID]
	, [ColumnFieldID]
	, [DataLoaderObjectTypeID]
	, [CheckMateFieldGroupID]
	, [IsEnabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[CheckMateFieldMapping] WITH (NOLOCK) 
    WHERE 
	 ([CheckMateFieldMappingID] = @CheckMateFieldMappingID OR @CheckMateFieldMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([CheckMateFieldID] = @CheckMateFieldID OR @CheckMateFieldID IS NULL)
	AND ([FieldID] = @FieldID OR @FieldID IS NULL)
	AND ([ColumnFieldID] = @ColumnFieldID OR @ColumnFieldID IS NULL)
	AND ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([CheckMateFieldGroupID] = @CheckMateFieldGroupID OR @CheckMateFieldGroupID IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CheckMateFieldMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [CheckMateFieldID]
	, [FieldID]
	, [ColumnFieldID]
	, [DataLoaderObjectTypeID]
	, [CheckMateFieldGroupID]
	, [IsEnabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[CheckMateFieldMapping] WITH (NOLOCK) 
    WHERE 
	 ([CheckMateFieldMappingID] = @CheckMateFieldMappingID AND @CheckMateFieldMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([CheckMateFieldID] = @CheckMateFieldID AND @CheckMateFieldID is not null)
	OR ([FieldID] = @FieldID AND @FieldID is not null)
	OR ([ColumnFieldID] = @ColumnFieldID AND @ColumnFieldID is not null)
	OR ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([CheckMateFieldGroupID] = @CheckMateFieldGroupID AND @CheckMateFieldGroupID is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Find] TO [sp_executeall]
GO
