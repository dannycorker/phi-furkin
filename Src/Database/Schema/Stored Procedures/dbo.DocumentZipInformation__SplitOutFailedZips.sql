SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:      Jim Green
-- Create date: 2011-05-09
-- Description: When a DocumentZipInformation record has a status of complete, but contains one or more 
--              incomplete DocumentZip records, move the incomplete ones to a new header record 
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipInformation__SplitOutFailedZips]
	@DocumentZipInformationID INT,		/* The DocumentZipInformationID that contains the flawed DocumentZip record(s) */
	@ShowResults BIT = 1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NewDocumentZipInformationID INT

	/* Make a new copy of the header record */
	INSERT INTO dbo.DocumentZipInformation
		([ClientID]
		,[ServerMapPath]
		,[SecurePath]
		,[HostPath]
		,[WordTempDirectory]
		,[DocumentsPerZip]
		,[ZipPrefix]
		,[StatusID]
		,[TriggeredByAutomatedTaskID]
		,[TriggeredByDocumentTypeID]
		,[WhoStored]
		,[WhenStored]
		,[WhenCreated]
		,[EmailOnSuccess]
		,[EmailOnError])
	SELECT 
		ClientID,
		ServerMapPath,
		SecurePath,
		HostPath,
		WordTempDirectory,
		DocumentsPerZip,
		ZipPrefix,
		35,
		TriggeredByAutomatedTaskID,
		TriggeredByDocumentTypeID,
		WhoStored,
		dbo.fn_GetDate_Local(),
		NULL,
		EmailOnSuccess,
		EmailOnError
	FROM dbo.DocumentZipInformation dziOld
	WHERE dziOld.DocumentZipInformationID = @DocumentZipInformationID 

	SELECT @NewDocumentZipInformationID = SCOPE_IDENTITY()

	/* Move selected zips to the new header record */
	IF @NewDocumentZipInformationID > 0
	BEGIN
		/* 
			Deliberately ignore the DocumentZipFile table here:
			1) New records get created by the scheduler
			2) Old records are already confused enough, so we can't reliably edit or delete them
		*/
		UPDATE dbo.DocumentZip 
		SET DocumentZipInformationID = @NewDocumentZipInformationID, 
		StatusID = 35 
		WHERE DocumentZipInformationID = @DocumentZipInformationID 
		AND StatusID <> 31
		
		/* Show details of the old and new headers */
		IF @ShowResults = 1
		BEGIN
			EXEC dbo.DocumentZip__Describe @DocumentZipInformationID
			EXEC dbo.DocumentZip__Describe @NewDocumentZipInformationID
		END
	END
	ELSE
	BEGIN
		PRINT 'ERROR: Failed to create the new header record!'
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__SplitOutFailedZips] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__SplitOutFailedZips] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__SplitOutFailedZips] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__SplitOutFailedZips] TO [sp_executehelper]
GO
