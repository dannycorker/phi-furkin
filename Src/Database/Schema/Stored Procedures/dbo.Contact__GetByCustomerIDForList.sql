SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Contact table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact__GetByCustomerIDForList]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[Contact].[ContactID],
					[Contact].[ClientID],
					[Contact].[CustomerID],
					[Titles].[Title],
					[Contact].[Firstname],
					[Contact].[Middlename],
					[Contact].[Lastname],
					[Contact].[Fullname],
					[Contact].[EmailAddressWork],
					[Contact].[EmailAddressOther],
					[Contact].[DirectDial],
					[Contact].[MobilePhoneWork],
					[Contact].[MobilePhoneOther],
					[Contact].[Address1],
					[Contact].[Address2],
					[Contact].[Town],
					[Contact].[County],
					[Contact].[Postcode],
					[Contact].[Country],
					[CustomerOffice].[OfficeName],
					[Department].[DepartmentName],
					[Contact].[JobTitle],
					[Contact].[Notes]
				FROM
					[dbo].[Contact]
				INNER JOIN
					[Titles] ON [Contact].[TitleID] = [Titles].[TitleID]
				INNER JOIN
                    [Department] ON [Contact].[DepartmentID] = [Department].[DepartmentID]
				INNER JOIN
                    [CustomerOffice] ON [Contact].[OfficeID] = [CustomerOffice].[OfficeID]
				WHERE
					[Contact].[CustomerID] = @CustomerID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[Contact__GetByCustomerIDForList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact__GetByCustomerIDForList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact__GetByCustomerIDForList] TO [sp_executeall]
GO
