SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AquariumOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOptionType_Delete]
(

	@AquariumOptionTypeID int   
)
AS


				DELETE FROM [dbo].[AquariumOptionType] WITH (ROWLOCK) 
				WHERE
					[AquariumOptionTypeID] = @AquariumOptionTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOptionType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Delete] TO [sp_executeall]
GO
