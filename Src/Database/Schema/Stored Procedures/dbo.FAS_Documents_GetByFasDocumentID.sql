SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FAS_Documents table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_GetByFasDocumentID]
(

	@FasDocumentID int   
)
AS


				SELECT
					[FasDocumentID],
					[FasCheckerDataID],
					[DocumentBlob]
				FROM
					[dbo].[FAS_Documents] WITH (NOLOCK) 
				WHERE
										[FasDocumentID] = @FasDocumentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_GetByFasDocumentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_GetByFasDocumentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_GetByFasDocumentID] TO [sp_executeall]
GO
