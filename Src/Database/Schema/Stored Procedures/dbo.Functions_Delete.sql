SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Functions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_Delete]
(

	@FunctionID int   
)
AS


				DELETE FROM [dbo].[Functions] WITH (ROWLOCK) 
				WHERE
					[FunctionID] = @FunctionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Delete] TO [sp_executeall]
GO
