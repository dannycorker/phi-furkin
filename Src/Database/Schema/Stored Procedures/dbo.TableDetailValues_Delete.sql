SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the TableDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableDetailValues_Delete]
(

	@TableDetailValueID int   
)
AS


				DELETE FROM [dbo].[TableDetailValues] WITH (ROWLOCK) 
				WHERE
					[TableDetailValueID] = @TableDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Delete] TO [sp_executeall]
GO
