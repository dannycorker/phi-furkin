SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-06-06
-- Description:	Import DDIC Data to DDIC Processing Table
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_ImportDDIC]
	 @XMLRaw VARCHAR(MAX),
	 @FileName VARCHAR(100) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XML XML = @XMLRaw,
			@ClientID INT = dbo.fnGetPrimaryClientID()

	/*Take the xml data and import it to the DDIC Import table.*/
	INSERT INTO BACSDDICImport(ClientID, Reference, BACSProcessedDate, PaymentAmount, FailureCode, FileName, ImportStatusID)
	OUTPUT inserted.*
	SELECT	@ClientID,
			node.value('(SUReference)[1]', 'VARCHAR(2000)') AS SUReference,
			c.value('(DateOfDirectDebit)[1]', 'VARCHAR(2000)') AS DateOfDirectDebit,
			c.value('(Amount)[1]', 'VARCHAR(2000)') AS Amount,
			node.value('(ReasonCode)[1]', 'VARCHAR(2000)') AS ReasonCode,
			@FileName,
			1 AS [ImportStatusID]
	FROM @XML.nodes('//NewAdvices/DDICAdvice') AS mandateRequests (node)
	CROSS APPLY mandateRequests.node.nodes('DDCollections/DDCollection') t(c)
	OPTION (OPTIMIZE FOR ( @XML = NULL ))

	EXEC Billing__ReconcileDDIC @ClientID

	SELECT *
	FROM BACSDDICImport d
	WHERE d.FileName = @FileName

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportDDIC] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_ImportDDIC] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportDDIC] TO [sp_executeall]
GO
