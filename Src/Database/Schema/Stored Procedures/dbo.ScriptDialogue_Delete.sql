SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptDialogue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_Delete]
(

	@ScriptDialogueID int   
)
AS


				DELETE FROM [dbo].[ScriptDialogue] WITH (ROWLOCK) 
				WHERE
					[ScriptDialogueID] = @ScriptDialogueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Delete] TO [sp_executeall]
GO
