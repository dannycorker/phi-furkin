SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-20
-- Description:	Insert Fields from Event comments etc into MDV or LDV
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertClientIDIntoField]
(
	@LeadEventID int,
	@ClientIDFieldID int
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare	@myERROR int,
	@LeadID int,
	@LeadTypeID int,
	@ClientID int,
	@CaseID int,
	@LeadOrMatterClientID int

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int,
		LeadOrMatter int
	)

	INSERT @RequiredFields
	SELECT @ClientIDFieldID, ''

	Select
	@LeadID = Lead.LeadID,
	@LeadTypeID = Lead.LeadTypeID,
	@ClientID = Lead.ClientID,
	@CaseID = LeadEvent.CaseID
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	where leadevent.leadeventid = @LeadEventID 

	Select 
	@LeadOrMatterClientID = LeadOrMatter
	From DetailFields 
	Where DetailFieldID = @ClientIDFieldID

	Update @RequiredFields
	Set LeadOrMatter = @LeadOrMatterClientID
	Where DetailFieldID = @ClientIDFieldID

	/* New way of creating detail fields*/
	exec dbo._C00_CreateDetailFields @ClientIDFieldID,@LeadID

	if @LeadOrMatterClientID = 1
	Begin

		/* Insert Comments into Lead Feild*/
		Update LeadDetailValues 
		Set DetailValue = @ClientID
		Where LeadID = @LeadID
		And DetailFieldID = @ClientIDFieldID

	end

	if @LeadOrMatterClientID = 2
	Begin

		/* Insert Comments into Matter Feild*/
		Update MatterDetailValues 
		Set DetailValue = @ClientID
		From MatterDetailValues
		Inner Join Matter on Matter.MatterID = MatterDetailValues.MatterID and Matter.CaseID = @CaseID
		Where MatterDetailValues.DetailFieldID = @ClientIDFieldID

	END

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertClientIDIntoField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertClientIDIntoField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertClientIDIntoField] TO [sp_executeall]
GO
