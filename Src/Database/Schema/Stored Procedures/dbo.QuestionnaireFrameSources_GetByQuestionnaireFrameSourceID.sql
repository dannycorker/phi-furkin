SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireFrameSources table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_GetByQuestionnaireFrameSourceID]
(

	@QuestionnaireFrameSourceID int   
)
AS


				SELECT
					[QuestionnaireFrameSourceID],
					[ClientID],
					[ClientQuestionnaireID],
					[PageNumber],
					[SourceUrl],
					[FrameType]
				FROM
					[dbo].[QuestionnaireFrameSources] WITH (NOLOCK) 
				WHERE
										[QuestionnaireFrameSourceID] = @QuestionnaireFrameSourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_GetByQuestionnaireFrameSourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_GetByQuestionnaireFrameSourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_GetByQuestionnaireFrameSourceID] TO [sp_executeall]
GO
