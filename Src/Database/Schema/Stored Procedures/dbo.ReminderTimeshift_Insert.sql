SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReminderTimeshift table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReminderTimeshift_Insert]
(

	@ReminderTimeshiftID int    OUTPUT,

	@ReminderTimeshiftName varchar (50)  ,

	@TimeUnitsID int   ,

	@TimeUnitsQuantity int   ,

	@DisplayOrder int   ,

	@TimeUnitsQuantityInMinutes int   
)
AS


				
				INSERT INTO [dbo].[ReminderTimeshift]
					(
					[ReminderTimeshiftName]
					,[TimeUnitsID]
					,[TimeUnitsQuantity]
					,[DisplayOrder]
					,[TimeUnitsQuantityInMinutes]
					)
				VALUES
					(
					@ReminderTimeshiftName
					,@TimeUnitsID
					,@TimeUnitsQuantity
					,@DisplayOrder
					,@TimeUnitsQuantityInMinutes
					)
				-- Get the identity value
				SET @ReminderTimeshiftID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReminderTimeshift_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Insert] TO [sp_executeall]
GO
