SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TextToSpeak table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TextToSpeak_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[TextToSpeakID],
					[ClientQuestionnaireID],
					[ClientID],
					[PageNumber],
					[SpeakText],
					[IsShown]
				FROM
					[dbo].[TextToSpeak] WITH (NOLOCK) 
				WHERE
					[ClientQuestionnaireID] = @ClientQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TextToSpeak_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_GetByClientQuestionnaireID] TO [sp_executeall]
GO
