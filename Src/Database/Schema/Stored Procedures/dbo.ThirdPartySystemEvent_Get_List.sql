SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ThirdPartySystemEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_Get_List]

AS


				
				SELECT
					[ThirdPartySystemEventID],
					[ThirdPartySystemID],
					[ClientID],
					[SubClientID],
					[Name],
					[Description],
					[EventTypeID],
					[LeadTypeID],
					[ThirdPartySystemKey],
					[MessageName]
				FROM
					[dbo].[ThirdPartySystemEvent] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Get_List] TO [sp_executeall]
GO
