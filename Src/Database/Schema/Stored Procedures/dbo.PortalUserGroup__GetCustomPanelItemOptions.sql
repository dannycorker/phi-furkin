SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-12-03
-- Description:	Gets a list of custom panel requirements for a particular PortalUserGroup (Add, Hide, Rename)
--              Needs the individual PortalUserID too, so they can be directed to their own Lead/Matter when required.
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserGroup__GetCustomPanelItemOptions]
(
	@PortalUserGroupID int,
	@PortalUserID int,
	@ClientID int	
)
	
AS
BEGIN

	SET NOCOUNT ON

	DECLARE 
	@CustomerID int,
	@LeadID int,
	@CaseID int,
	@MatterID int 
	
	/* Special options for some Portal User Groups (eg hide this button, show that one instead) */
	IF EXISTS (SELECT * FROM dbo.PortalUserGroupMenuOption pugmo WITH (NOLOCK) WHERE pugmo.PortalUserGroupID = @PortalUserGroupID AND pugmo.ClientID = @ClientID) 
	BEGIN
		
		SELECT TOP 1 
		@CustomerID = m.CustomerID, 
		@LeadID = m.LeadID, 
		@CaseID = m.CaseID, 
		@MatterID = m.MatterID 
		FROM dbo.PortalUser pu WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = pu.CustomerID 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID 
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = ca.CaseID
		WHERE pu.PortalUserID = @PortalUserID 
		AND pu.ClientID = @ClientID 
		ORDER BY m.LeadID, m.CaseID, m.MatterID 
		
		/* Rebrand the ordinary "Your Leads" button */
		SELECT pugmo.PanelItemAction, 
		pugmo.PanelItemName, 
		pugmo.PanelItemCaption, 
		pugmo.PanelItemIcon, 
		REPLACE(REPLACE(REPLACE(REPLACE(pugmo.PanelItemURL, '@CustomerID', @CustomerID), '@LeadID', @LeadID), '@CaseID', @CaseID), '@MatterID', @MatterID) as [PanelItemURL]
		FROM dbo.PortalUserGroupMenuOption pugmo WITH (NOLOCK) 
		WHERE pugmo.PortalUserGroupID = @PortalUserGroupID 
		AND pugmo.ClientID = @ClientID
		
		/*SELECT 'REBRAND' AS [PanelItemAction], 'pnl4' AS [PanelItemName], 'Your Cases' AS [PanelItemCaption], '' AS [PanelItemIcon], '' AS [PanelItemURL]
		UNION ALL
		SELECT 'ADD' AS [PanelItemAction], 'pnlCustom1' AS [PanelItemName], 'All Cases & Commissions' AS [PanelItemCaption], 'barchart.gif' AS [PanelItemIcon], REPLACE(REPLACE(REPLACE(REPLACE('/DetailFieldView.aspx?type=2&mode=1&aid=@CaseID&lid=@LeadID&mid=@MatterID', '@CustomerID', @CustomerID), '@LeadID', @LeadID), '@CaseID', @CaseID), '@MatterID', @MatterID) AS [PanelItemURL]*/
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetCustomPanelItemOptions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup__GetCustomPanelItemOptions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetCustomPanelItemOptions] TO [sp_executeall]
GO
