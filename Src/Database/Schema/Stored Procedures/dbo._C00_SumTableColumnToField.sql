SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-05-23
-- Description:	SQL Afterevent for C00 For adding on non-working days to events
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SumTableColumnToField]
	@CaseID int,
	@ColumnFieldID int,
	@DetailFieldID int

AS
BEGIN
	SET NOCOUNT ON;

	Declare @LeadOrMatter int,
			@LeadID int,
			@MatterID int,
			@TestID int

	Select @LeadOrMatter = LeadOrMatter
	From DetailFields WITH (NOLOCK) 
	Where DetailFieldID = @DetailFieldID

	Select @LeadID = LeadID
	From Cases WITH (NOLOCK) 
	Where CaseID = @CaseID

	exec dbo._C00_CreateDetailFields @DetailFieldID, @LeadID

	If @LeadOrMatter = 1
	Begin

		UPDATE LeadDetailValues 
		SET DetailValue = 
		IsNull((SELECT SUM(tdv.ValueMoney)
				FROM TableDetailValues tdv WITH (NOLOCK) 
				WHERE tdv.DetailFieldID = @ColumnFieldID 
				AND tdv.LeadID = LeadDetailValues.LeadID 
			), 0)
		FROM LeadDetailValues 
		WHERE LeadDetailValues.DetailFieldID = @DetailFieldID
		AND LeadDetailValues.LeadID = @LeadID

	End

	If @LeadOrMatter = 2
	Begin

		Select top 1 @MatterID = MatterID, @TestID = 0
		From Matter WITH (NOLOCK) 
		Where CaseID = @CaseID
		Order By MatterID

		While (@MatterID IS NOT NULL and @MatterID <> @TestID)
		Begin

			UPDATE MatterDetailValues 
			SET DetailValue = 
			IsNull((SELECT SUM(tdv.ValueMoney)
					FROM TableDetailValues tdv WITH (NOLOCK) 
					WHERE tdv.DetailFieldID = @ColumnFieldID 
					AND tdv.MatterID = Matter.MatterID 
				), 0)
			FROM MatterDetailValues 
			Inner Join Matter On Matter.MatterID = MatterDetailValues.MatterID and Matter.MatterID = @MatterID
			WHERE MatterDetailValues.DetailFieldID = @DetailFieldID

			Select @TestID = @MatterID

			Select top 1 @MatterID = MatterID
			From Matter WITH (NOLOCK) 
			Where CaseID = @CaseID
			And MatterID > @MatterID
			Order By MatterID

		End

	End

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SumTableColumnToField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SumTableColumnToField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SumTableColumnToField] TO [sp_executeall]
GO
