SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Stephen Ainsworth	
-- Create date: 2017-07-25
-- Description:	Take in a Policy Number and return Pet and PH numbers
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_VetFone_GetPetAndPolicyHolderDetailsFromPolicyNumber]
(
	 @PolicyNumber VARCHAR (2000)
)
	
AS
BEGIN

	/*Find the policy from the policy number*/

	DECLARE  @Xml				XML
			,@PolicyAdminLeadID	INT
			,@ClientID			INT	= dbo.fnGetPrimaryClientID()
				
	SELECT @PolicyAdminLeadID = l.LeadID
	FROM Lead l WITH ( NOLOCK ) 
	WHERE l.LeadTypeID = 1492 /*PolicyAdmin*/
	AND l.LeadRef = @PolicyNumber

	DECLARE @PetDetails TABLE ( RecordType VARCHAR(10), PolicyNumber VARCHAR(20), PetName VARCHAR(20), PetSpecies VARCHAR(15), PetBreed VARCHAR(15), PetGender VARCHAR(1), PetDob VARCHAR(8) )
	DECLARE @Conditions TABLE ( id INT IDENTITY, PolicyNumber VARCHAR(2000), Condition VARCHAR(2000) )

	IF @PolicyAdminLeadID > 0
	BEGIN
		/*Add the pet details to a table field.  Originally need only one row, but potentially more to follow*/
		INSERT @PetDetails ( RecordType
							,PolicyNumber
							,PetName
							,PetSpecies
							,PetBreed
							,PetGender
							,PetDob
						   )

		--VALUES ( 'new', 'policyNo','petName','petSpecies','petBreed','M','20170801' )
		SELECT 'NEW',LEFT(l.LeadRef,20),LEFT(ldv2.DetailValue,20),LEFT(lli.ItemValue,15),LEFT(rldv1.DetailValue,15),LEFT(lli1.ItemValue,1),CONVERT(VARCHAR,ldv3.ValueDate,112)
		FROM Customers cu WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = cu.CustomerID
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID AND ldv.DetailFieldID = 144272 /*Pet Type Resource List*/
		INNER JOIN ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.ResourceListID = ldv.ValueInt 
		INNER JOIN ResourceListDetailValues rldv2 WITH (NOLOCK) ON rldv2.ResourceListID = ldv.ValueInt 
		INNER JOIN LookupListItems lli WITH (NOLOCK) ON rldv2.ValueInt = lli.LookupListItemID
		INNER JOIN LeadDetailValues ldv1 WITH (NOLOCK) ON ldv1.LeadID = l.LeadID AND ldv1.DetailFieldID = 144275 /*Pet Sex*/
		INNER JOIN LookupListItems lli1 WITH (NOLOCK) ON lli1.LookupListItemID = ldv1.ValueInt
		INNER JOIN LeadDetailValues ldv2 WITH (NOLOCK) ON ldv2.LeadID = l.LeadID AND ldv2.DetailFieldID = 144268 /*Pet Name*/
		INNER JOIN LeadDetailValues ldv3 WITH (NOLOCK) ON ldv3.LeadID = l.LeadID AND ldv3.DetailFieldID = 144274 /*Pet Date of Birth*/
		WHERE cu.Test = 0
		AND l.LeadRef = @PolicyNumber
		AND rldv1.DetailFieldID = 144270 /*Pet Type - Pet Breed*/
		AND rldv2.DetailFieldID = 144269 /*Pet Type - Pet Type*/
		
		INSERT @Conditions ( PolicyNumber, Condition )
		--VALUES ('policyNo','Limp'),('policyNo','Unstable'),('policyNo','Cancer'),('policyNo','Epillepsy')
			SELECT DISTINCT L1.leadref,rldv.DetailValue [Condition]		
		FROM Customers cu WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = cu.CustomerID
		INNER JOIN Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = cs.CaseID
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 144504 /*Ailment*/
		INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv.ValueInt AND rldv.DetailFieldID = 144340 /*1st Cause*/ 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON l.LeadID = ltr.ToLeadID	
		INNER JOIN Lead L1 WITH (NOLOCK) ON L1.LeadID = ltr.FromLeadID
		INNER JOIN Matter Matter1 WITH (NOLOCK) ON Matter1.LeadID = L1.LeadID
		WHERE cu.Test = 0
		AND l.LeadTypeID = 1490 /*Claim Type*/
		AND ltr.FromLeadTypeID = 1492 /*Policy Admin*/
		AND L1.LeadRef = @PolicyNumber

		/*Construct an XML package based on VetFone format*/
		SELECT @Xml = 
		(
			SELECT	 pd.RecordType		[RECORD/@type]
					,pd.PolicyNumber	[RECORD/@policynumber]
					,pd.PetName			[RECORD/PETNAME]
					,pd.PetSpecies		[RECORD/PETSPECIES]
					,pd.PetBreed		[RECORD/PETBREED]
					,pd.PetGender		[RECORD/PETGENDER]
					,pd.PetDob			[RECORD/PETDOB]	
				,(
						SELECT TOP 10 c.id [Condition/@id], c.Condition [Condition]
						FROM @Conditions c 
						FOR XML PATH (''), TYPE
					 ) [RECORD/CONDITION]
			FROM @PetDetails pd 
			FOR XML PATH ( '' ) 
		)
	END
	ELSE
	BEGIN
		SELECT @Xml = CAST('<record policynumber="' + ISNULL(@PolicyNumber,'NULL') + '" found="no" />' as XML)
	END

	IF @PolicyAdminLeadID is NULL
	BEGIN
		SELECT @PolicyAdminLeadID = 0
	END
	
	EXEC _C00_LogItXML @ClientID, @PolicyAdminLeadID, '_C600_VetFone_GetPetAndPolicyHolderDetailsFromPolicyNumber', @Xml
	
	SELECT @Xml

	RETURN

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetFone_GetPetAndPolicyHolderDetailsFromPolicyNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_VetFone_GetPetAndPolicyHolderDetailsFromPolicyNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetFone_GetPetAndPolicyHolderDetailsFromPolicyNumber] TO [sp_executeall]
GO
