SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeLimitation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation_Delete]
(

	@EventTypeLimitationID int   
)
AS


				DELETE FROM [dbo].[EventTypeLimitation] WITH (ROWLOCK) 
				WHERE
					[EventTypeLimitationID] = @EventTypeLimitationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Delete] TO [sp_executeall]
GO
