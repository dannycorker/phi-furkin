SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAttachment table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment__GetDetailsByEventTypeID]
(

	@EventTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AttachmentEventTypeID],
					CASE 
					WHEN at.[AttachmentEventTypeID] IS NULL THEN eg.EventGroupName ELSE et.EventTypeName END AS [EventTypeName],
					[All],
					[AttachmentEventGroupID]
				FROM
					[dbo].[EventTypeAttachment] at
				LEFT JOIN [dbo].[EventType] et WITH (NOLOCK) ON at.AttachmentEventTypeID = et.EventTypeID
				LEFT JOIN [dbo].[EventGroup] eg WITH (NOLOCK) ON at.AttachmentEventGroupID = eg.EventGroupID
				WHERE
					at.[EventTypeID] = @EventTypeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment__GetDetailsByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment__GetDetailsByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment__GetDetailsByEventTypeID] TO [sp_executeall]
GO
