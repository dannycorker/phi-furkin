SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-12-11
-- Description:	List all LeadEventThreadCompletion info for a Lead (/Case)
-- =============================================
CREATE PROCEDURE [dbo].[letc] 
	@LeadID int = NULL,
	@CaseID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	IF @LeadID IS NULL AND @CaseID IS NULL
	BEGIN 
		SELECT 'You must pass in LeadID and/or CaseID'
	END
	ELSE
	BEGIN
		IF @CaseID IS NOT NULL
		BEGIN
			SELECT letc.* 
			FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK) 
			WHERE letc.CaseID = @CaseID
			ORDER BY letc.LeadEventThreadCompletionID 

			SELECT le.* 
			FROM dbo.LeadEvent le WITH (NOLOCK) 
			WHERE le.CaseID = @CaseID 
			ORDER BY le.LeadEventID 
		END
		ELSE
		BEGIN
			SELECT letc.* 
			FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK) 
			INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.CaseID = letc.CaseID 
			WHERE ca.LeadID = @LeadID
			ORDER BY letc.CaseID, letc.LeadEventThreadCompletionID 

			SELECT le.* 
			FROM dbo.LeadEvent le WITH (NOLOCK) 
			WHERE le.LeadID = @LeadID 
			ORDER BY le.CaseID, le.LeadEventID 
		END
		
		PRINT 'SELECT letc.* FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK) ' + CASE WHEN @CaseID IS NULL THEN ' INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.CaseID = letc.CaseID ' ELSE ''END + ' WHERE '
		+ CASE 
			WHEN @CaseID > 0 THEN 'letc.CaseID = ' + cast(@CaseID as varchar) 
			ELSE 'ca.LeadID = ' + cast(@LeadID as varchar) 
		  END
		+
		' ORDER BY letc.CaseID, letc.LeadEventThreadCompletionID '

		PRINT 'SELECT le.* FROM dbo.LeadEvent le WITH (NOLOCK) WHERE '
		+ CASE 
			WHEN @CaseID > 0 THEN 'le.CaseID = ' + cast(@CaseID as varchar) 
			ELSE 'le.LeadID = ' + cast(@LeadID as varchar) 
		  END
		+
		' ORDER BY le.CaseID, le.LeadEventID '
	END
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[letc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[letc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[letc] TO [sp_executeall]
GO
