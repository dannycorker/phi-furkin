SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset Type List
-- =============================================
CREATE PROCEDURE [dbo].[GetAssetTypeList] 
	@ClientPersonnelID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		at.[AssetTypeID],
		at.[AssetSubTypeID],
		at.[AssetTypeName],
		at.[Note],
		at.[Enabled],
		at.[Deleted],
		at.[WhoCreated],
		at.[WhenCreated],
		at.[WhoModified],
		at.[WhenModified]
	FROM 
		[dbo].[AssetType] at WITH (NOLOCK),
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND at.[AssetTypeID] = a.[AssetTypeID]
	ORDER BY at.[AssetTypeID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetAssetTypeList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAssetTypeList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAssetTypeList] TO [sp_executeall]
GO
