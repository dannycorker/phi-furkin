SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskResult table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_GetByAutomatedTaskResultID]
(

	@AutomatedTaskResultID int   
)
AS


				SELECT
					[AutomatedTaskResultID],
					[TaskID],
					[ClientID],
					[RunDate],
					[Description],
					[RecordCount],
					[Complete]
				FROM
					[dbo].[AutomatedTaskResult] WITH (NOLOCK) 
				WHERE
										[AutomatedTaskResultID] = @AutomatedTaskResultID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_GetByAutomatedTaskResultID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_GetByAutomatedTaskResultID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_GetByAutomatedTaskResultID] TO [sp_executeall]
GO
