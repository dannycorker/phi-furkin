SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2015-01-21
-- Description:	Show a list of clients own report tables (iReporting)
-- =============================================
CREATE PROCEDURE [dbo].[ClientReportTableDefinition__GetByClientID] 
	@ClientID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT crtd.ClientReportTableDefinitionID, 
	crtd.ClientID, 
	crtd.TableName, 
	crtd.LeadTypeID, 
	crtd.TableSubtypeID, 
	crtd.WhoCreated, 
	crtd.WhenCreated, 
	crtd.WhoModified, 
	crtd.WhenModified, 
	cp.UserName AS WhoCreatedUserName, 
	lt.LeadTypeName, 
	ISNULL(dfs.DetailFieldSubTypeName, '') AS TableSubtypeName 
	FROM dbo.ClientReportTableDefinition crtd WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = crtd.WhoCreated 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = crtd.LeadTypeID 
	LEFT JOIN dbo.DetailFieldSubType dfs WITH (NOLOCK) ON dfs.DetailFieldSubTypeID = crtd.TableSubtypeID 
	WHERE crtd.ClientID = @ClientID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableDefinition__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientReportTableDefinition__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableDefinition__GetByClientID] TO [sp_executeall]
GO
