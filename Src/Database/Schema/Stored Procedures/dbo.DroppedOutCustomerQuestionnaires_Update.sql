SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DroppedOutCustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerQuestionnaires_Update]
(

	@CustomerQuestionnaireID int   ,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@SubmissionDate datetime   ,

	@TrackingID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DroppedOutCustomerQuestionnaires]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[CustomerID] = @CustomerID
					,[SubmissionDate] = @SubmissionDate
					,[TrackingID] = @TrackingID
					,[ClientID] = @ClientID
				WHERE
[CustomerQuestionnaireID] = @CustomerQuestionnaireID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerQuestionnaires_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Update] TO [sp_executeall]
GO
