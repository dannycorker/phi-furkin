SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2014
-- Description:	Find customers via identity
--				Does not include business customers
--				Ordered by full name ascending
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByIdentity]

	@ClientID INT,
	@ID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM Customers c WITH (NOLOCK) 
	WHERE c.CustomerID=@ID AND ClientID=@ClientID AND IsBusiness=0
	UNION
	SELECT c.* FROM Lead l WITH (NOLOCK) 
	INNER JOIN Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID AND IsBusiness=0
	WHERE l.leadid=@ID AND l.ClientID=@ClientID
	UNION
	SELECT c.* FROM Cases cs WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = cs.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID AND IsBusiness=0
	WHERE cs.CaseID=@ID AND cs.ClientID=@ClientID
	UNION
	SELECT c.* FROM Matter m WITH (NOLOCK) 
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID=m.CustomerID AND IsBusiness=0
	WHERE m.MatterID=@ID AND m.ClientID=@ClientID
	ORDER BY Fullname ASC
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByIdentity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByIdentity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByIdentity] TO [sp_executeall]
GO
