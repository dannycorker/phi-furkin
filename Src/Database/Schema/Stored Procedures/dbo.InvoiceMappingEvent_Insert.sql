SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the InvoiceMappingEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_Insert]
(

	@InvoiceMappingEventID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@EventTypeID int   ,

	@Type char (1)  
)
AS


				
				INSERT INTO [dbo].[InvoiceMappingEvent]
					(
					[ClientID]
					,[LeadTypeID]
					,[EventTypeID]
					,[Type]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@EventTypeID
					,@Type
					)
				-- Get the identity value
				SET @InvoiceMappingEventID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Insert] TO [sp_executeall]
GO
