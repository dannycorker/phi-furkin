SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Rating Policy - except MTA
-- Mods
--	2020-08-20 ALM | Updated for US Cancellation Logic for PPET-110
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Rating]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@RatingType					INT
			,@EventComments					VARCHAR(2000)
			,@PetNumber						INT
			,@AdjustmentDate				DATE
			,@InCoolingOffPeriod			INT
			,@VarcharDate					VARCHAR(10)
			,@PolicyDetailTableRowID		INT
			,@StartDate						DATE
			,@EndDate						DATE
			,@CurrentPremium				MONEY
			,@RenewalPremium				MONEY
			,@RenewalPremiumDifference		MONEY
			,@FirstRenewalActualCollectionDate	DATE
			,@CancellationReason			INT
			,@State							VARCHAR(10)
			,@CancellationType				INT
			,@DaysIntoPolicyYear			INT
			,@IsRenewal						BIT
			,@CoolingOffPeriod				INT
			,@CountryID						INT 

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			,@CountryID		= cu.CountryID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID
			INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */
			
		/* Rating Policy - except MTA */
	IF @EventTypeID IN (155224, -- Re-rate policy (tentative match)
						155232, -- Re-rate policy (existing account details)
						155226, -- Re-rate policy (new accounts details)
						155366, -- Re-rate policy (pet number)
						155218, -- Re-rate (PH does not match)
						155262, -- Calculate Refund (cancelling)
						155303) -- SIG Reinstate Policy
	BEGIN
		
		/*ALM 2021-04-07 FURKIN-410*/
		IF @EventTypeID = 155303 AND dbo.fnGetSimpleDvAsDate(170055,@MatterID) = dbo.fnGetSimpleDvAsDate(170036,@MatterID)
		BEGIN 
			SELECT @ErrorMessage = '<br><br><font color="red">Error: Reinstatement is not available as policy cancelled in cooling off period</font>'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END

		-- clear out adjustment fields
		IF @EventTypeID<>155278
		BEGIN
			EXEC _C00_SimpleValueIntoField 175379,'',@MatterID,@WhoCreated  -- value
			EXEC _C00_SimpleValueIntoField 175345,'',@MatterID,@WhoCreated  -- type
		END
		
		-- 1=monthly premium, 2=renewal, 3=MTA, 4=Quote, 5=Cancellation, 7=reinstatement 
	
		/*GPR 2019-10-18 for LPC-35*/
		-- clear out adjustment fields
		IF @EventTypeID<>155278 /*Re-rate policy (MTA formal)*/
		BEGIN
			EXEC _C00_SimpleValueIntoField 175379,'',@MatterID,@WhoCreated  -- value
			EXEC _C00_SimpleValueIntoField 175345,'',@MatterID,@WhoCreated  -- type
		END
		
		SELECT @RatingType = se.ThirdPartySystemKey
		FROM dbo.ThirdPartySystemEvent se WITH ( NOLOCK ) 
		WHERE se.EventTypeID = @EventTypeID
		
		SELECT @EventComments	+= 'RatingType: ' + ISNULL(CONVERT(VARCHAR,@RatingType),'NULL') 
								 + ' (' + CASE @RatingType 
											WHEN 1 THEN 'Monthly Premium' 
											WHEN 2 THEN 'Renewal'
											WHEN 3 THEN 'MTA' 
											WHEN 4 THEN 'Quote' 
											WHEN 5 THEN 'Cancellation' 
											--WHEN 6 THEN 'MTA' 
											WHEN 7 THEN 'Reinstatement'
											ELSE 'Unknown' 
										  END  + ')' + CHAR(13)+CHAR(10)
		
		-- use pet number over-ride if it has been set
		IF ISNULL(dbo.fnGetDv(175461,@CaseID),'') <> ''
		BEGIN
			
			SELECT @PetNumber=dbo.fnGetDv(175461,@CaseID) /*Pet Number Override*/
			EXEC dbo._C00_SimpleValueIntoField 175453, @PetNumber, @MatterID, @AqAutomation		

			-- clear pet number override
			EXEC dbo._C00_SimpleValueIntoField 175461, '', @MatterID, @AqAutomation	
					
		END
		
		IF @RatingType=5
		BEGIN
		
			SELECT @AdjustmentDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID),
					@CancellationReason = dbo.fnGetSimpleDvAsInt(170054,@MatterID),
					@StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)
			
			/*2020-08-20 ALM PPET-110*/
			IF @CountryID = 233 /*United States*/
			BEGIN 

				SELECT @State = cu.County 
				FROM Customers cu WITH (NOLOCK) 
				WHERE cu.CustomerID = @CustomerID 
			
				SELECT @CancellationType = 
				CASE 
					WHEN @CancellationReason IN (69907,69911,72031,72032,72033,72035,72037,72038,74421,74519,74521,74523,74525,75135,75136,75141) THEN 1 
					ELSE 2 
				END
			
				SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())
			
				SELECT @IsRenewal = 
					CASE 
						WHEN COUNT (*) > 1 THEN 1 
						ELSE 0 
					END 
				FROM TableRows tr WITH (NOLOCK) 
				WHERE tr.DetailFieldID = 170033
				AND tr.MatterID = @MatterID

				SELECT @CoolingOffPeriod = [dbo].[fn_C00_USCancellationCoolingOffPeriod] (@State, @CancellationType, @IsRenewal, @DaysIntoPolicyYear)

				-- handle cooling off period.  Make sure cancellation date is set to policy start date & cancellation reason is cooling off
				SELECT @InCoolingOffPeriod= CASE WHEN @CoolingOffPeriod IS NOT NULL THEN
											CASE WHEN DATEADD(DAY,@CoolingOffPeriod,pp.ProductPurchasedOnDate) >= @AdjustmentDate THEN 1 ELSE 0 END
											ELSE 0 END
				FROM PurchasedProduct pp WITH (NOLOCK) 
				LEFT JOIN BillingConfiguration bc WITH (NOLOCK) ON pp.ClientID=bc.ClientID
				INNER JOIN MatterDetailValues mpp WITH (NOLOCK) ON mpp.ValueInt=pp.PurchasedProductID AND mpp.DetailFieldID=177074
				WHERE mpp.MatterID=@MatterID

			END 
			ELSE 
			BEGIN
		
				-- handle cooling off period.  Make sure cancellation date is set to policy start date & cancellation reason is cooling off
				SELECT @InCoolingOffPeriod= CASE WHEN bc.CoolingOffPeriod IS NOT NULL THEN
											CASE WHEN DATEADD(DAY,bc.CoolingOffPeriod,pp.ProductPurchasedOnDate) >= @AdjustmentDate THEN 1 ELSE 0 END
											ELSE 0 END
				FROM PurchasedProduct pp WITH (NOLOCK) 
				LEFT JOIN BillingConfiguration bc WITH (NOLOCK) ON pp.ClientID=bc.ClientID
				INNER JOIN MatterDetailValues mpp WITH (NOLOCK) ON mpp.ValueInt=pp.PurchasedProductID AND mpp.DetailFieldID=177074
				WHERE mpp.MatterID=@MatterID
			
			END 

			IF @InCoolingOffPeriod = 1
			BEGIN
			
				SELECT @VarcharDate=dbo.fnGetDv(170036,@CaseID) /*Policy Start Date*/
				EXEC dbo._C00_SimpleValueIntoField 170055, @VarcharDate, @MatterID, @AqAutomation	-- cancellation date
				
				/*JEL Removed at Conor Lind's request*/
				--IF dbo.fnGetSimpleDvAsInt(170054,@MatterID) <> 72038
				--/*Don't overrite if customer has died*/ 			
				--BEGIN
				--	EXEC dbo._C00_SimpleValueIntoField 170054, 69907, @MatterID, @AqAutomation	-- cancellation reason
				--END
			END
			
			EXEC dbo._C600_PA_QueueCancellation @LeadEventID, @MatterID
			
			EXEC _C00_SimpleValueIntoField  170038, 74535,@MatterID,@WhoCreated
			
		END
			
		EXEC @PolicyDetailTableRowID = _C600_ReRatePolicy @LeadEventid, @MatterID, @RatingType, 0,  @AqAutomation, NULL
				
		-- set documentation fields   /*GPR updated to use dateadd for startdate for defect 1251*/
		--SELECT @StartDate=DATEADD(DAY,1,dbo.fnGetDvAsDate (175307,@CaseID))
		SELECT @StartDate= dbo.fnGetDvAsDate (175307,@CaseID)
		EXEC _C00_SimpleValueIntoField 176383,@StartDate,@MatterID,@AqAutomation
		SELECT @EndDate=DATEADD(DAY,-1,DATEADD(YEAR,1,@StartDate))
		EXEC _C00_SimpleValueIntoField 176384,@EndDate,@MatterID,@AqAutomation

		--assign the current premium for the current policy to its own variable - UAH 17/10/2018
		SELECT @CurrentPremium =  mdv.ValueMoney FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.DetailFieldID = 175337 and mdv.matterid = @MatterID

		--assign the renewal premium for the current policy to its own variable -  UAH 17/10/2018
		SELECT @RenewalPremium = mdv.ValueMoney FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.DetailFieldID = 175443 and mdv.matterid = @MatterID

		--calculate the price difference between the renewal premium and the current premium - UAH 17/10/2018
		SELECT @RenewalPremiumDifference = @RenewalPremium - @CurrentPremium

		--insert into document field 'Renewal Premium difference from last years price' the difference- UAH 17/10/2018
		EXEC _C00_SimpleValueIntoField 180160,@RenewalPremiumDifference,@MatterID,@AqAutomation
		
		-- ALWAYS KEEP THIS AT THE END OF THIS BLOCK - the policy needs to be live so it can be re-rated
		IF @EventTypeID IN ( 155262 ) /*Re-rate policy (cancelling)*/
		BEGIN
		
			-- update historical policy table converting live (or unset) status to cancelled
			EXEC dbo._C00_CreateDetailFields '145666', @LeadID
			UPDATE TableDetailValues 
			SET DetailValue=43003
			WHERE DetailFieldID=145666 AND MatterID=@MatterID AND ValueInt IN (0,43002)
			
		END	

		--Once the new schedule is generated, get the first collection date of the renewal schedule and stamp into document field 180215 -- UAH 31/10/2018
		SELECT @FirstRenewalActualCollectionDate = dbo.fn_C00_ReturnFirstCollectionDateOfRenewal(@CustomerID, @LeadID, @CaseID, @MatterID)
		EXEC dbo._C00_SimpleValueIntoField 180215, @FirstRenewalActualCollectionDate, @matterID
	
	END

	/*GPR 2020-02-10*/
	IF @EventTypeID = 155303
	BEGIN

		EXEC dbo._SAE_Reinstatement_155303 @LeadEventID

	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Rating] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Rating] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Rating] TO [sp_executeall]
GO
