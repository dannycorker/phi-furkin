SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-09-2015
-- Description:	Get a customer by its matterid
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByMatterID]
	@ClientID INT,
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CustomerID INT
	
	SELECT @CustomerID=CustomerID FROM Matter WITH (NOLOCK) 
	WHERE MatterID=@MatterID AND ClientID=@ClientID

	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE CustomerID=@CustomerID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByMatterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByMatterID] TO [sp_executeall]
GO
