SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jan-Willem Wilson
-- Create date: 2011-10-07
-- Description:	Describe all attributes of a sql query
-- =============================================
CREATE PROCEDURE [dbo].[SqlQuery__GetByClientIDIsTemplate]
(
	@ClientID int
)
AS
BEGIN
	
	SELECT
		[QueryID],
		[QueryTitle]
	FROM
		[dbo].[SqlQuery] WITH (NOLOCK)
	WHERE
		[ClientID] IN (@ClientID, 0)
		AND IsTemplate = 1
		AND IsDeleted = 0
		
	ORDER BY 
		[QueryTitle] ASC

	SELECT @@ROWCOUNT

END		




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetByClientIDIsTemplate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__GetByClientIDIsTemplate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetByClientIDIsTemplate] TO [sp_executeall]
GO
