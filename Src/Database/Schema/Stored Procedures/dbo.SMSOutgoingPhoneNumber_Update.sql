SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SMSOutgoingPhoneNumber table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_Update]
(

	@SMSOutgoingPhoneNumberID int   ,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@LeadTypeID int   ,

	@OutgoingPhoneNumber varchar (16)  ,

	@SMSSurveyStartEventTypeID int   ,

	@MMSLetterInEvent int   ,

	@SmsGatewayID int   ,

	@HasIntroductionText bit   ,

	@IntroductionTextFrom varchar (250)  ,

	@IntroductionText varchar (2000)  ,

	@DelayInSeconds int   ,

	@IntroductionTextEventTypeID int   ,

	@IntroductionTextDocumentTypeID int   ,

	@NoCLIRejectText varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SMSOutgoingPhoneNumber]
				SET
					[ClientID] = @ClientID
					,[SMSQuestionnaireID] = @SMSQuestionnaireID
					,[LeadTypeID] = @LeadTypeID
					,[OutgoingPhoneNumber] = @OutgoingPhoneNumber
					,[SMSSurveyStartEventTypeID] = @SMSSurveyStartEventTypeID
					,[MMSLetterInEvent] = @MMSLetterInEvent
					,[SmsGatewayID] = @SmsGatewayID
					,[HasIntroductionText] = @HasIntroductionText
					,[IntroductionTextFrom] = @IntroductionTextFrom
					,[IntroductionText] = @IntroductionText
					,[DelayInSeconds] = @DelayInSeconds
					,[IntroductionTextEventTypeID] = @IntroductionTextEventTypeID
					,[IntroductionTextDocumentTypeID] = @IntroductionTextDocumentTypeID
					,[NoCLIRejectText] = @NoCLIRejectText
				WHERE
[SMSOutgoingPhoneNumberID] = @SMSOutgoingPhoneNumberID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Update] TO [sp_executeall]
GO
