SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the WorkflowGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_Insert]
(

	@WorkflowGroupID int    OUTPUT,

	@Name varchar (100)  ,

	@Description varchar (255)  ,

	@ManagerID int   ,

	@ClientID int   ,

	@Enabled bit   ,

	@SortOrder varchar (4)  ,

	@AssignedLeadsOnly bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[WorkflowGroup]
					(
					[Name]
					,[Description]
					,[ManagerID]
					,[ClientID]
					,[Enabled]
					,[SortOrder]
					,[AssignedLeadsOnly]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@Name
					,@Description
					,@ManagerID
					,@ClientID
					,@Enabled
					,@SortOrder
					,@AssignedLeadsOnly
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @WorkflowGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Insert] TO [sp_executeall]
GO
