SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReferenceValueFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueFormat_Update]
(

	@ReferenceValueFormatID int   ,

	@ClientID int   ,

	@ReferenceValueName varchar (50)  ,

	@ReferenceValueDescription varchar (255)  ,

	@IntegerSectionSize int   ,

	@IntegerStartPoint int   ,

	@DateFormat varchar (50)  ,

	@YearFormat varchar (50)  ,

	@DelimiterID int   ,

	@Section1TypeID int   ,

	@Section1DetailFieldID int   ,

	@Section2TypeID int   ,

	@Section2DetailFieldID int   ,

	@Section3TypeID int   ,

	@Section3DetailFieldID int   ,

	@Section4TypeID int   ,

	@Section4DetailFieldID int   ,

	@Section5TypeID int   ,

	@Section5DetailFieldID int   ,

	@LeftPadIntegerSection bit   ,

	@IncludeBlankSections bit   ,

	@EnforceUniqueness bit   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReferenceValueFormat]
				SET
					[ClientID] = @ClientID
					,[ReferenceValueName] = @ReferenceValueName
					,[ReferenceValueDescription] = @ReferenceValueDescription
					,[IntegerSectionSize] = @IntegerSectionSize
					,[IntegerStartPoint] = @IntegerStartPoint
					,[DateFormat] = @DateFormat
					,[YearFormat] = @YearFormat
					,[DelimiterID] = @DelimiterID
					,[Section1TypeID] = @Section1TypeID
					,[Section1DetailFieldID] = @Section1DetailFieldID
					,[Section2TypeID] = @Section2TypeID
					,[Section2DetailFieldID] = @Section2DetailFieldID
					,[Section3TypeID] = @Section3TypeID
					,[Section3DetailFieldID] = @Section3DetailFieldID
					,[Section4TypeID] = @Section4TypeID
					,[Section4DetailFieldID] = @Section4DetailFieldID
					,[Section5TypeID] = @Section5TypeID
					,[Section5DetailFieldID] = @Section5DetailFieldID
					,[LeftPadIntegerSection] = @LeftPadIntegerSection
					,[IncludeBlankSections] = @IncludeBlankSections
					,[EnforceUniqueness] = @EnforceUniqueness
					,[SourceID] = @SourceID
				WHERE
[ReferenceValueFormatID] = @ReferenceValueFormatID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueFormat_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Update] TO [sp_executeall]
GO
