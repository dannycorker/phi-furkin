SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget_GetByDocumentDetailFieldTargetID]
(

	@DocumentDetailFieldTargetID int   
)
AS


				SELECT
					[DocumentDetailFieldTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[TemplateTypeID],
					[DetailFieldAlias],
					[Notes],
					[ExcelSheetLocation],
					[Format],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[DocumentDetailFieldTargetID] = @DocumentDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_GetByDocumentDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget_GetByDocumentDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_GetByDocumentDetailFieldTargetID] TO [sp_executeall]
GO
