SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-11-06
-- Description:	Get iDiary event and limitation list for Claim 4 Refunds.
--				Will probably turn this in to a generic one if there is more interest.
-- =============================================
CREATE PROCEDURE [dbo].[C156_GetEventsForIDiary] 
	
	@UserID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TopXRows INT = 500,
			@ClientID INT,
			@ClientPersonnelAdminGroupID INT
			
	/* CS using Claim4Refunds as a test case 2012-09-13 */
	SELECT @ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @UserID


	SELECT @ClientID = cp.ClientID
	FROM ClientPersonnel cp WITH (NOLOCK)
	WHERE cp.ClientPersonnelID = @UserID

	;	
	-- Get leads assigned to this user; these
	-- are the only ones we are going to show
	WITH LeadsAssignedToMe AS (
		SELECT LeadID, CustomerID  
		FROM Lead  WITH (NOLOCK)
		WHERE assignedto = @UserID 
		AND aquariumstatusid = 2 
	),
	-- Combine the sql clauses above into one big select statement:
	AllCases as 
	(
	SELECT TOP(@TopXRows)	c.LeadID, 
							l.CustomerID, 
							cu.Fullname, 
							c.CaseID, 
							c.CaseRef, 
							isnull(isnull(et.eventtypeid,nt.NoteTypeID),'') [EventTypeID], 
							isnull(isnull(et.eventtypename,nt.NoteTypeName),'') [EventTypeName], 
							le.LeadEventID, 
							le.WhenCreated, 
							DATEDIFF(day,dbo.fn_GetDate_Local(),dateadd(day,7,le.WhenCreated)) AS 'Followup', 
							dateadd(day,7,le.WhenCreated) AS 'Deadline', 
							'' AS [DaysRemaining], 
							CASE WHEN nt.NoteTypeID is not null THEN left(le.Comments,200) ELSE isnull(dbo.fnNextEventChoice(le.eventtypeid),'') END AS NextEvent 
							, ROW_NUMBER() OVER (PARTITION BY l.LeadID ORDER BY le.LeadEventID desc) [Row]
	FROM LeadsAssignedToMe l  WITH (NOLOCK)
	INNER JOIN Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
	INNER JOIN Customers cu WITH (NOLOCK) ON l.CustomerID = cu.CustomerID 
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadEventID = c.LatestLeadEventID
	LEFT JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND et.InProcess = 1 AND et.Enabled = 1
	LEFT JOIN dbo.LeadEvent nle WITH (NOLOCK) on nle.LeadEventID = c.LatestNoteLeadEventID
	LEFT JOIN NoteType nt WITH (NOLOCK) on nt.NoteTypeID = nle.NoteTypeID
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.CaseID 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 92321 and mdv.ValueMoney > 0.00 /*Total Amount Due*/
	WHERE c.AquariumStatusID = 2 
	AND (le.WhenFollowedUp IS NULL or le.NoteTypeID is not null)
	)
	SELECT * 
	FROM AllCases ac 
	WHERE ac.Row = 1
	ORDER BY ac.LeadEventID 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[C156_GetEventsForIDiary] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[C156_GetEventsForIDiary] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[C156_GetEventsForIDiary] TO [sp_executeall]
GO
