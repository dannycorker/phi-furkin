SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventChoice table through an index
-- MODIFIED	2014-07-25	SB	Limited event choice by client ID due to shared lead types
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice__GetExistingThreadsByEventType]
(
	@EventTypeID int,
	@ClientID INT = NULL
)
AS


				SELECT DISTINCT 				
					[ThreadNumber]
				FROM
					[dbo].[EventChoice]
				WHERE
					[EventTypeID] = @EventTypeID
				AND
					(ClientID = @ClientID OR @ClientID IS NULL)
				ORDER BY [ThreadNumber]
			Select @@ROWCOUNT
			
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetExistingThreadsByEventType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetExistingThreadsByEventType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetExistingThreadsByEventType] TO [sp_executeall]
GO
