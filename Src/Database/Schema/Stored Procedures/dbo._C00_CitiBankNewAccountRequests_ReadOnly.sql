SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-12-08
-- Description:	CitiBank New Account Requests for Cheshire East
--              JWG 2010-07-21 Restrict lastname to 25 characters max.
--              JWG 2011-04-28 Restrict Town to 25 chars and County to 15.
--				ACE 2012-01-24 Copied and made generic
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CitiBankNewAccountRequests_ReadOnly] 
(
	@ClientID INT,
	@JobNumber varchar(10),
	@EventTypeID INT, 
	@AgencyNameField INT,
	@CaseClosedStatusID int,
	@ProgramID VARCHAR(4),
	@PartnerID varchar(4),
	@OutputAsXML bit = 1,
	@EcountNumberFieldID int,
	@ParisNumberFieldIfNeeded int = NULL
)
AS
BEGIN
	
	/*
		This uses the FileExportEvent table to keep track of 
		which file each event was added to.
		
		This proc finds Paris Number events (57177) that have 
		been created and not followed up, adds any new ones to the FileExportEvent
		table, removes any that the users have subsequently deleted,
		and then creates the XML output ready to send to CitiBank. 
		
		If you need to resubmit any records (eg because the file 
		got lost or rejected) then please put something in the resubmission 
		notes and then blank out Filename and FileID.
		
		Use this sql, replacing XXX with your initials, 
		and nnnnn with the FileID to resubmit.
		
		UPDATE dbo.FileExportEvent 
		SET  ResubmissionNotes = IsNull(ResubmissionNotes, '') + 'Resubmitted by XXX on ' + convert(varchar, dbo.fn_GetDate_Local(), 126) + ' Filename was ' + Filename + ' FileID ' + cast(FileID as varchar) + ' ', 
		Filename = NULL, 
		FileID = NULL
		WHERE FileID = nnnnn
		
	*/
	
	SET NOCOUNT ON;

	DECLARE @XML VARCHAR(MAX),
	@CreationDate CHAR(10),
	@FileName VARCHAR(50),
	@RunNumber INT 
 
	DECLARE @FileExportEvent Table (FileExportEventID int identity (1,1), ClientID int, WhenCreated date, LeadEventID int, EventTypeID int, AutomatedTaskID int, Filename varchar(1000), FileID int, WhenFollowedup datetime, NextLeadEventID int, NextEventTypeID int, NextAutomatedTaskID int, SubmissionNotes varchar(2000), ResubmissionNotes varchar(2000))


	/* Grab a list of all Paris Number events that are ready for export. */
	INSERT INTO @FileExportEvent (ClientID, WhenCreated, LeadEventID, EventTypeID, AutomatedTaskID) 
	SELECT le.ClientID, dbo.fn_GetDate_Local(), le.LeadEventID, le.EventTypeID, @JobNumber  
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
	INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = le.CaseID AND (ca.ClientStatusID <> @CaseClosedStatusID OR ca.ClientStatusID IS NULL)
	WHERE le.EventTypeID = @EventTypeID 
	AND le.WhenFollowedUp IS NULL 
	AND le.EventDeleted = 0 
	AND le.ClientID = @ClientID 
	AND (C.Test = 0 OR C.Test IS NULL) 
	AND (DATALENGTH(c.Town) < 26 OR c.Town IS NULL) 
	AND (DATALENGTH(c.County) < 16 OR c.County IS NULL) 
	AND NOT EXISTS (
		SELECT * 
		FROM @FileExportEvent fee2 
		WHERE fee2.LeadEventID = le.LeadEventID 
		AND fee2.AutomatedTaskID = @JobNumber 
		AND [Filename] IS NOT NULL /* Allow resubmissions */
	)
	
	/* 
		Remove deleted events from the list to export, 
		but only if they haven't already been sent yet.
	*/
	DELETE fee 
	FROM @FileExportEvent fee 
	INNER JOIN dbo.LeadEvent le ON le.LeadEventID = fee.LeadEventID AND le.EventTypeID = fee.EventTypeID 
	WHERE fee.[Filename] IS NULL 
	AND le.EventDeleted = 1 
	
	/* 
		Select the inner XML from the customer details to be sent. 
		This is much simpler than it looks. 
		
		1) [request/@attribute] comes out as 
		
			<request attribute="Customer: 1234"></request>
			
		whereas
		
		2) [request/element] comes out as 
		
			<request>
				<element>Jim</element>
			</request>
			
		Easy as that.
	*/
	SELECT @XML = (
		SELECT 
		'Customer:' + CAST(CASE WHEN @ParisNumberFieldIfNeeded > 0 THEN mdv_pno.DetailValue ELSE c.customerid END AS VARCHAR) AS [request/@partneruserid], 
		'Lead:' + CAST(le.LeadID AS VARCHAR) + ' LeadEvent:' + CAST(le.LeadEventID AS VARCHAR) AS [request/createaccountrequest/@passthrough], 
		c.FirstName AS [request/createaccountrequest/universalregistration/basicreg/firstname], 
		c.MiddleName AS [request/createaccountrequest/universalregistration/basicreg/middlename], 
		REPLACE(LEFT(c.LastName + CASE WHEN mdv.DetailValue > '' THEN ' ' + REPLACE(REPLACE(mdv.DetailValue, ')', ''), '(', '') ELSE '' END, 25), '/', '') AS [request/createaccountrequest/universalregistration/basicreg/lastname], 
		'' AS [request/createaccountrequest/universalregistration/basicreg/suffixname], 
		c.EmailAddress AS [request/createaccountrequest/universalregistration/basicreg/email],
		'H' AS [request/createaccountrequest/universalregistration/address/@type],
		c.Address1 AS [request/createaccountrequest/universalregistration/address/address1],
		CASE 
			WHEN c.Town IS NULL THEN '' /* If Town (mandatory) is blank then we are going to stuff Address2 into the Town field below, */
			WHEN c.Town = '' THEN ''	/* so leave it blank up here to avoid duplication */
			ELSE c.Address2 
		END AS [request/createaccountrequest/universalregistration/address/address2],
		'Customer' + CAST(c.customerid AS VARCHAR) AS [request/createaccountrequest/universalregistration/address/attentionline],
		'Lead' + CAST(le.LeadID AS VARCHAR) AS [request/createaccountrequest/universalregistration/address/company],
		CASE 
			WHEN c.Town IS NULL THEN	/* If Town (mandatory) is blank then show Address2 here instead */  
				CASE					/* unless Address2 is also blank, in which case try a space */
					WHEN c.Address2 IS NULL THEN ' ' 
					WHEN c.Address2 = '' THEN ' ' 
					ELSE c.Address2 
				END 
			WHEN c.Town = '' THEN 
				CASE 
					WHEN c.Address2 IS NULL THEN ' ' 
					WHEN c.Address2 = '' THEN ' ' 
					ELSE c.Address2 
				END 
			ELSE c.Town 
		END AS [request/createaccountrequest/universalregistration/address/city],
		'' AS [request/createaccountrequest/universalregistration/address/state],
		c.PostCode AS [request/createaccountrequest/universalregistration/address/postal],
		COALESCE(c.County, '') AS [request/createaccountrequest/universalregistration/address/county],
		'GB' AS [request/createaccountrequest/universalregistration/address/country],
		'false' AS [request/createaccountrequest/onlineregistrationrequired]
		FROM @FileExportEvent fee 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.LeadEventID = fee.LeadEventID  
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID 
		INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
		LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.LeadID = l.LeadID AND mdv.DetailFieldID = @AgencyNameField 
		LEFT JOIN MatterDetailValues mdv_pno WITH (NOLOCK) ON mdv_pno.LeadID = l.LeadID and mdv_pno.DetailFieldID = @ParisNumberFieldIfNeeded
		WHERE fee.AutomatedTaskID = @JobNumber
		AND fee.[Filename] IS NULL 
		AND fee.EventTypeID = @EventTypeID  
		FOR XML PATH(''), ROOT('batch')
	)

	IF @OutputAsXML = 1
	BEGIN

		/* Do not select anything at all if there are no records today. */
		IF @XML IS NULL
		BEGIN
			SELECT NULL AS [CitiBank DataFile] 
			WHERE 0 = 1
		END
		ELSE
		BEGIN
			
			/* Wrap the inner XML with a standard CitiBank root node (or two) */
			SELECT @CreationDate = CONVERT(CHAR(10), dbo.fn_GetDate_Local(), 126)
			
			/* Use the WorkingDays table to get a unique run number for the file (as long as it runs once per day). */
			SELECT @RunNumber = w.WorkingDayID 
			FROM dbo.WorkingDays w (NOLOCK) 
			WHERE w.CharDate = @CreationDate 

			SELECT @FileName = 'CitiBank New Accounts ' + @CreationDate + ' ' + REPLACE(CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 108), ':', '') + '.xml'

			SELECT @XML = REPLACE(@XML, '<batch>', '<batch programid="' + @ProgramID + '" batchdescription="New Accounts Batch" passthrough="Job Number (AT) ' + CAST(@JobNumber AS VARCHAR) + '" batchpromotionid="0" settlementdate="' + @CreationDate + '">')

			SELECT @XML = '<requestfile xmlns="http://www.ecount.com/" partnerid="' + @PartnerID + '" filename="' + @FileName + '" passthrough="Run Number (ATR) ' + CAST(@RunNumber AS VARCHAR) + '" creationdate="' + @CreationDate + '">' + @XML + '</requestfile>'

			SELECT CAST(@XML AS XML) AS [CitiBank DataFile]

			/* Finally, update the FileExportEvent table to show which records went in this file. */		
			UPDATE @FileExportEvent 
			SET [Filename] = @FileName, 
			FileID = @RunNumber, 
			SubmissionNotes = 'Auto'  
			WHERE EventTypeID = EventTypeID 
			AND AutomatedTaskID = @JobNumber 
			AND [Filename] IS NULL
			
		END
	
	END
	ELSE
	BEGIN
	
		SELECT @CreationDate = CONVERT(CHAR(10), dbo.fn_GetDate_Local(), 126)
		
		/* Use the WorkingDays table to get a unique run number for the file (as long as it runs once per day). */
		SELECT @RunNumber = w.WorkingDayID 
		FROM dbo.WorkingDays w (NOLOCK) 
		WHERE w.CharDate = @CreationDate 

		SELECT REPLACE(CONVERT(varchar,CONVERT(date,dbo.fn_GetDate_Local())),'-',''), 
				CASE WHEN @ParisNumberFieldIfNeeded > 0 THEN mdv_pno.DetailValue ELSE c.customerid END, 
				mdv_ecount.DetailValue as [EcountID], 
				c.FirstName, 
				c.MiddleName, 
				c.LastName, 
				ti.Title, 
				c.DateOfBirth, 
				c.Address1, 
				c.Address2, 
				'' as [Attention Line], 
				'' AS [Company Name], 
				c.Town, 
				'' as [State],
				c.PostCode,
				c.County,
				'UK' as [Country],
				'+44' as [Phone Country Code],
				'' as [Phone Area Code],
				c.HomeTelephone as [Phone Number],
				'' as [Extension],
				'' as [Phone Type],
				c.EmailAddress,
				'' as [Location Code],
				'' as [Amount],
				'' as [Notification Indicator],
				'' as [Taxable Flag],
				'' as [Direct Claim Flag],
				'' as [Online Registration Required],
				'' as [Passthrough]
		FROM @FileExportEvent fee 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.LeadEventID = fee.LeadEventID  
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID 
		INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
		LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.LeadID = l.LeadID AND mdv.DetailFieldID = @AgencyNameField 
		LEFT JOIN MatterDetailValues mdv_ecount ON mdv_ecount.LeadID = l.LeadID and mdv_ecount.DetailFieldID = @EcountNumberFieldID
		INNER JOIN Titles ti WITH (NOLOCK) ON ti.TitleID = c.TitleID
		LEFT JOIN MatterDetailValues mdv_pno WITH (NOLOCK) ON mdv_pno.LeadID = l.LeadID and mdv_pno.DetailFieldID = @ParisNumberFieldIfNeeded
		WHERE fee.AutomatedTaskID = @JobNumber
		AND fee.[Filename] IS NULL 
		AND fee.EventTypeID = @EventTypeID  
		ORDER BY c.CustomerID	

		UPDATE @FileExportEvent 
		SET [Filename] = @FileName, 
		FileID = @RunNumber, 
		SubmissionNotes = 'Auto'  
		WHERE EventTypeID = EventTypeID 
		AND AutomatedTaskID = @JobNumber 
		AND [Filename] IS NULL
			
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CitiBankNewAccountRequests_ReadOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CitiBankNewAccountRequests_ReadOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CitiBankNewAccountRequests_ReadOnly] TO [sp_executeall]
GO
