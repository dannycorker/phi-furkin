SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDroppedOutCustomerQuestionnaire] @CustomerQuestionnaireID int 
AS
BEGIN
Delete From DroppedOutCustomerQuestionnaires where CustomerQuestionnaireID = @CustomerQuestionnaireID;
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDroppedOutCustomerQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteDroppedOutCustomerQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDroppedOutCustomerQuestionnaire] TO [sp_executeall]
GO
