SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack (#31922)
-- Create date: 2015-03-31
-- Description:	Gets a salt for a clientpersonnel, using ThirdPartySystemID
-- Updates:		2015-04-16	SB	Transform TP System ID to match on 0 for external apps that 'impersonate'
-- =============================================
CREATE PROCEDURE [dbo].[GetSaltFromEmailAddress] 
(
	@EmailAddress nvarchar(255),
	@ThirdPartySystemID INT = 0
)
AS
BEGIN

	IF @ThirdPartySystemID NOT IN (0, 11, 37)
    BEGIN
        SELECT @ThirdPartySystemID = 0
    END
	
	SELECT	TOP (1) cp.Salt 	FROM	dbo.ClientPersonnel cp WITH (NOLOCK) 	WHERE	cp.EmailAddress = @EmailAddress	AND		cp.ThirdPartySystemId = @ThirdPartySystemID	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[GetSaltFromEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSaltFromEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSaltFromEmailAddress] TO [sp_executeall]
GO
