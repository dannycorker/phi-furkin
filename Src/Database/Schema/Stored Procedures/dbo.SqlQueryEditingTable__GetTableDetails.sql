SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-08-25
-- Description:	Loads all the tables used by a query and gets the extra join information required
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingTable__GetTableDetails]
(
	@SqlQueryEditingID int   
)
AS


DECLARE @IsChildQuery BIT

SELECT @IsChildQuery = CASE WHEN ParentQueryID IS NULL THEN 0 ELSE 1 END
FROM dbo.SqlQueryEditing e WITH (NOLOCK) 
WHERE e.SqlQueryEditingID = @SqlQueryEditingID

IF @IsChildQuery = 1
BEGIN

	SELECT	0 AS TableID, t.SqlQueryTableName AS TableName, 
			CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END AS TableAlias, 
			t.JoinType, ISNULL(r.TableTo, '') AS FromTableName, JoinText
	FROM dbo.SqlQueryEditing e WITH (NOLOCK)
	INNER JOIN dbo.SqlQueryTables t WITH (NOLOCK) ON e.ParentQueryID = t.SqlQueryID  
	LEFT JOIN dbo.ReportTableRelationships r WITH (NOLOCK) ON t.JoinRTRID = r.ReportTableRelationshipID 
	WHERE SqlQueryEditingID = @SqlQueryEditingID
	ORDER BY t.SqlQueryTableID		
	
END
ELSE
BEGIN

	SELECT	t.SqlQueryEditingTableID AS TableID, t.SqlQueryTableName AS TableName, 
			CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END AS TableAlias, 
			t.JoinType, ISNULL(r.TableTo, '') AS FromTableName, JoinText
	FROM dbo.SqlQueryEditingTable t WITH (NOLOCK)
	LEFT JOIN dbo.ReportTableRelationships r WITH (NOLOCK) ON t.JoinRTRID = r.ReportTableRelationshipID 
	WHERE SqlQueryEditingID = @SqlQueryEditingID
	ORDER BY t.SqlQueryEditingTableID		
	
END
					




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetTableDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable__GetTableDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetTableDetails] TO [sp_executeall]
GO
