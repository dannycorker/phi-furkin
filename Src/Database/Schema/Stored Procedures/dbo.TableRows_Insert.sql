SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the TableRows table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_Insert]
(

	@TableRowID int    OUTPUT,

	@ClientID int   ,

	@LeadID int   ,

	@MatterID int   ,

	@DetailFieldID int   ,

	@DetailFieldPageID int   ,

	@DenyEdit bit   ,

	@DenyDelete bit   ,

	@CustomerID int   ,

	@CaseID int   ,

	@ClientPersonnelID int   ,

	@ContactID int   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[TableRows]
					(
					[ClientID]
					,[LeadID]
					,[MatterID]
					,[DetailFieldID]
					,[DetailFieldPageID]
					,[DenyEdit]
					,[DenyDelete]
					,[CustomerID]
					,[CaseID]
					,[ClientPersonnelID]
					,[ContactID]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@LeadID
					,@MatterID
					,@DetailFieldID
					,@DetailFieldPageID
					,@DenyEdit
					,@DenyDelete
					,@CustomerID
					,@CaseID
					,@ClientPersonnelID
					,@ContactID
					,@SourceID
					)
				-- Get the identity value
				SET @TableRowID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Insert] TO [sp_executeall]
GO
