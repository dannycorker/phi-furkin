SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-03-28
-- Description:	Keep debt management 
-- =============================================
CREATE PROCEDURE [dbo].[_Debt_Recon]
	
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @myERROR int

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int
	)

	INSERT @RequiredFields SELECT 8127 
	/*UNION ALL SELECT nnnm */

	DECLARE @Leads TABLE 
	(
		LeadID int
	);

	INSERT INTO @Leads (LeadID)
	SELECT DISTINCT LeadID 
	FROM Lead
	WHERE ClientID = 4
	and LeadTypeID=172

	DECLARE @RequiredFieldsMatter TABLE 
	(
		DetailFieldID int
	)

	INSERT @RequiredFieldsMatter SELECT 8199
	/*UNION ALL SELECT nnnm */

	DECLARE @Matter TABLE 
	(
		LeadID int,
		MatterID int
	);

	INSERT INTO @Matter (LeadID, MatterID)
	SELECT DISTINCT m.LeadID, m.MatterID 
	FROM Matter m 
	INNER JOIN Lead l ON l.LeadID = m.LeadID AND l.LeadTypeID = 172 
	WHERE m.ClientID = 4


	BEGIN TRAN

	/* Create all necessary LeadDetailValues records that don't already exist */
	INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
	SELECT 4,lam.LeadID,rf.DetailFieldID,'' 
	FROM @Leads lam 
	CROSS JOIN @RequiredFields rf 
	WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WHERE ldv.LeadID = lam.LeadID AND ldv.DetailFieldID = rf.DetailFieldID) 
	
	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/* Create all necessary MatterDetailValues records that don't already exist */
	INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
	SELECT 4,mam.LeadID,mam.MatterID,rfm.DetailFieldID,'' 
	FROM @Matter mam 
	CROSS JOIN @RequiredFieldsMatter rfm 
	WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WHERE mdv.MatterID = mam.MatterID AND mdv.DetailFieldID = rfm.DetailFieldID) 
	
	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR


	/* Cash Price */	 
	UPDATE LeadDetailValues 
	SET DetailValue = IsNull((
		SELECT sum(CONVERT(money, MatterDetailValues.DetailValue))
		FROM @Leads lam
		INNER JOIN MatterDetailValues ON MatterDetailValues.LeadID = lam.LeadID AND MatterDetailValues.DetailFieldID = 8119
	), 0)
	FROM @Leads lam
	WHERE LeadDetailValues.LeadID = lam.LeadID 
	AND LeadDetailValues.DetailFieldID = 8127

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/* Set Creditor Precentage*/
	UPDATE MatterDetailValues 
	SET DetailValue = IsNull((
select CONVERT(money, MatterDetailValues.DetailValue)/CONVERT(money, leaddetailvalues.DetailValue) 
from leaddetailvalues
inner join cases on cases.leadid = leaddetailvalues.leadid
inner join matter on matter.caseid = cases.caseid
inner join matterdetailvalues on matterdetailvalues.matterid = matter.matterid and matterdetailvalues.DetailFieldID = 8119
where matter.matterid = mam.MatterID AND LeadDetailValues.DetailFieldID = 8127
	), 0)
	FROM @Matter mam
	WHERE MatterDetailValues.MatterID = mam.MatterID 
	AND MatterDetailValues.DetailFieldID = 8199

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/* Creditor Monthly Repayment */
	UPDATE MatterDetailValues 
	SET DetailValue = IsNull((
select CONVERT(money, MatterDetailValues.DetailValue)*CONVERT(money, leaddetailvalues.DetailValue) 
from leaddetailvalues
inner join cases on cases.leadid = leaddetailvalues.leadid
inner join matter on matter.caseid = cases.caseid
inner join matterdetailvalues on matterdetailvalues.matterid = matter.matterid and matterdetailvalues.DetailFieldID = 8199
where matter.matterid = mam.MatterID AND LeadDetailValues.DetailFieldID = 8129
	), 0)
	FROM @Matter mam
	WHERE MatterDetailValues.MatterID = mam.MatterID 
	AND MatterDetailValues.DetailFieldID = 8120

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/* Set Payment due date */
update leaddetailvalues 
set detailvalue = dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), convert(int,luli.itemvalue), 11)
from leaddetailvalues 
inner join leaddetailvalues ldv2 on ldv2.leadid = leaddetailvalues.leadid and ldv2.detailfieldid = 8202
inner join lookuplistitems luli on luli.lookuplistitemid = ldv2.detailvalue 
where leaddetailvalues.detailfieldid = 8196
and ((leaddetailvalues.DetailValue is not null) and (datediff(dd, convert(datetime, leaddetailvalues.DetailValue), dbo.fn_GetDate_Local()) > 0) )

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	UPDATE MatterDetailValues 
	SET DetailValue = 
convert(varchar, IsNull((SELECT CONVERT(money, DetailValue)
FROM MatterDetailValues
WHERE MatterDetailValues.MatterID = mam.MatterID and (MatterDetailValues.DetailFieldID = 8119)), 0)
-
IsNull((SELECT SUM(CONVERT(money, tdv.DetailValue))
		FROM TableDetailValues tdv
		WHERE tdv.ClientID = 4  
		AND tdv.DetailFieldID = 8124 
		AND tdv.MatterID = MatterDetailValues.MatterID
	), 0))
	FROM @Matter mam
	WHERE MatterDetailValues.MatterID = mam.MatterID 
	AND MatterDetailValues.DetailFieldID = 8117

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	GOTO END_NOW

HANDLE_ERROR:
    ROLLBACK TRAN

END_NOW:
	SELECT @myERROR as 'Outcome'

	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_Debt_Recon] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_Debt_Recon] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_Debt_Recon] TO [sp_executeall]
GO
