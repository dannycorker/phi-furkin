SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSection_Delete]
(

	@ScriptSectionID int   
)
AS


				DELETE FROM [dbo].[ScriptSection] WITH (ROWLOCK) 
				WHERE
					[ScriptSectionID] = @ScriptSectionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Delete] TO [sp_executeall]
GO
