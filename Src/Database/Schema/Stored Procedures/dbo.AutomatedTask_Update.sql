SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTask_Update]
(

	@TaskID int   ,

	@ClientID int   ,

	@Taskname varchar (100)  ,

	@Description varchar (250)  ,

	@Enabled bit   ,

	@RunAtHour int   ,

	@RunAtMinute int   ,

	@RepeatTimeUnitsID int   ,

	@RepeatTimeQuantity int   ,

	@NextRunDateTime datetime   ,

	@WorkflowTask bit   ,

	@AlreadyRunning bit   ,

	@AutomatedTaskGroupID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@MaximumAllowableErrors int   ,

	@EventSubTypeThresholding bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AutomatedTask]
				SET
					[ClientID] = @ClientID
					,[Taskname] = @Taskname
					,[Description] = @Description
					,[Enabled] = @Enabled
					,[RunAtHour] = @RunAtHour
					,[RunAtMinute] = @RunAtMinute
					,[RepeatTimeUnitsID] = @RepeatTimeUnitsID
					,[RepeatTimeQuantity] = @RepeatTimeQuantity
					,[NextRunDateTime] = @NextRunDateTime
					,[WorkflowTask] = @WorkflowTask
					,[AlreadyRunning] = @AlreadyRunning
					,[AutomatedTaskGroupID] = @AutomatedTaskGroupID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[MaximumAllowableErrors] = @MaximumAllowableErrors
					,[EventSubTypeThresholding] = @EventSubTypeThresholding
				WHERE
[TaskID] = @TaskID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Update] TO [sp_executeall]
GO
