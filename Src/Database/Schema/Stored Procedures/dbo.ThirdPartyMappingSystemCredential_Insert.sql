SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyMappingSystemCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential_Insert]
(

	@ThirdPartyMappingSystemCredentialID int    OUTPUT,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@ConsumerKey varchar (500)  ,

	@ConsumerSecret varchar (500)  
)
AS


				
				INSERT INTO [dbo].[ThirdPartyMappingSystemCredential]
					(
					[ThirdPartySystemID]
					,[ClientID]
					,[ConsumerKey]
					,[ConsumerSecret]
					)
				VALUES
					(
					@ThirdPartySystemID
					,@ClientID
					,@ConsumerKey
					,@ConsumerSecret
					)
				-- Get the identity value
				SET @ThirdPartyMappingSystemCredentialID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Insert] TO [sp_executeall]
GO
