SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-07-02
-- Description:	Gets a single automated task queue record
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTaskQueue_GetByID]
	@AutomatedTaskQueueID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT q.AutomatedTaskQueueID, q.ClientID, q.Description, q.Enabled, q.MaxRecords, q.Notes
	FROM dbo.AutomatedTaskQueue q WITH (NOLOCK) 
	WHERE q.AutomatedTaskQueueID = @AutomatedTaskQueueID
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskQueue_GetByID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskQueue_GetByID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskQueue_GetByID] TO [sp_executeall]
GO
