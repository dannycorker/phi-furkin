SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==============================================================
-- Author:		Thomas Doyle
-- Create date: 2015-12-22
-- Description:	Disable SMS and Survey events for a given day and client
-- Outgoing phone number Key (as a last stop measure, these chars get replaced in the outgoing number): 
-- 0 = '`'
-- 7 = '|'
-- ==============================================================
CREATE PROCEDURE [dbo].[_C00_DisableSMS]
(	
	@ClientID			INT 
	,@EnableOrDisable	Varchar(2000)			/* 'Enable' or 'Disable'   */
	,@ToggleDate		DATE 
	,@MandatoryEvent	INT 	/*SC2: 155787 "NEVER EVER APPLY",SC1: "156151"*/  
)

AS
BEGIN

	IF @ToggleDate <> convert(date, dbo.fn_GetDate_Local(),120)
	BEGIN
		SELECT 'Not Today'
	END

	IF @EnableOrDisable = 'Disable' 
	AND convert(date, dbo.fn_GetDate_Local(),120) = @ToggleDate /*Today is the day*/
	AND @MandatoryEvent > 1		
	AND @ClientID > 1 					
	BEGIN

		/*Replace digits in the Outgoing phone number*/
		UPDATE smso
		SET smso.OutgoingPhoneNumber = REPLACE(REPLACE(smso.outgoingphonenumber,'0','`'),'7','|') 
		FROM  SMSOutgoingPhoneNumber smso WITH (NOLOCK) 
		WHERE smso.ClientID = @ClientID
		AND len(dbo.fnExtractNumber(smso.outgoingphonenumber)) = len(smso.outgoingphonenumber) /*return phone numbers consisting only of numbers*/ 
		AND len(smso.outgoingphonenumber) > 0 

		/*Disable sms and survey events, set sourceID to: 25252525 so you can find them later to re enable them*/
		UPDATE EventType  
		SET SourceID = 25252525, Enabled = 0 
		WHERE Enabled=1 
		and EventSubtypeID in (24,13) 
		and ClientID = @ClientID
		
		/*Change the Recipientsto field in the documentmanager,replace the word mobile with the word disabled*/
		UPDATE dt
		SET dt.RecipientsTo = REPLACE(DT.Recipientsto,'Mobile','Disabled')
		FROM EventType ET with(nolock) 
		Inner join DocumentType dt with(nolock) on dt.documenttypeid = et.documenttypeid and dt.clientid = @ClientID
		WHERE ET.clientid= @ClientID
		and et.EventSubtypeID  = 13 /*SMS event subtype*/
		and  dt.RecipientsTo in ('[!A:Mobile Number]','[!Mobile]','[!Name] (Customer) [[!Mobile]]') /*In the future remove this filter + add prefix and suffix of disabled to aliasfields, then replace with '' when you want to enable again*/
		
		/*Disable batch jobs that apply an sms event, set their sourceID to: 25252525 */
		UPDATE at
		SET AT.Enabled=0 ,at.SourceID = 25252525
		FROM AutomatedTask at 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON ATP.TaskID = at.TaskID
		INNER JOIN EventType et WITH (NOLOCK) on CAST(et.EventTypeID AS VARCHAR(2000)) =  ATP.ParamValue
		WHERE atp.ParamName = 'LEADEVENT_TO_ADD'
		and et.EventSubtypeID in (24,13) 
		and et.ClientID = @ClientID  
		and at.Enabled = 1
		
		/*Insert Into EventTypeMandatoryEvent*/
		INSERT INTO EventTypeMandatoryEvent (ClientID,LeadTypeID,ToEventTypeID,RequiredEventID)
		SELECT 
		et.ClientID
		,et.LeadTypeID
		,et.EventTypeID
		,@MandatoryEvent 
		FROM EventType et WITH (NOLOCK) 
		INNER JOIN EventSubtype ets WITH (NOLOCK) on ets.EventSubtypeID = et.EventSubtypeID and ets.EventSubtypeID in (24,13) /*SMS Out,SMS Survey Start*/
		WHERE et.ClientID = @ClientID 
		and et.Enabled = 0
		and et.EventTypeID not in (select sete.ToEventTypeID from EventTypeMandatoryEvent sete WITH (NOLOCK) WHERE sete.RequiredEventID =  @MandatoryEvent)
		
		SELECT 'Disabled SMS and batch jobs'

	END

	IF @EnableOrDisable = 'Enable' 
	AND convert(date, dbo.fn_GetDate_Local(),120) = @ToggleDate /*Today is the day*/
	AND @MandatoryEvent > 1		
	AND @ClientID > 1 					
	BEGIN
		
		/*Replace Digits in the Outgoing phone number*/		
		UPDATE smso
		SET smso.OutgoingPhoneNumber = REPLACE(REPLACE(smso.outgoingphonenumber,'`','0'),'|','7')
		FROM  SMSOutgoingPhoneNumber smso WITH (NOLOCK) 
		WHERE smso.ClientID = @ClientID
		AND len(dbo.fnExtractNumber(smso.outgoingphonenumber)) = len(smso.outgoingphonenumber) /*return phone numbers consisting only of numbers*/ 
		AND len(smso.outgoingphonenumber) > 0 
		
		/*Enable SMS Events, find them by the mandatory event you've added earlier*/
		UPDATE et 
		SET et.Enabled = 1,et.SourceID=1
		FROM EventType et 
		INNER JOIN EventTypeMandatoryEvent ete WITH (NOLOCK) on ete.ToEventTypeID=et.EventTypeID 
		WHERE et.Enabled= 0 and ete.RequiredEventID = @MandatoryEvent
		and EventSubtypeID in (24,13) 
		and et.ClientID = @ClientID
		and ete.ClientID = @ClientID
		
		/*Change the Recipientsto field in the documentmanager*/
		UPDATE dt
		SET dt.RecipientsTo = REPLACE(DT.Recipientsto,'Disabled','Mobile')
		FROM EventType ET with(nolock) 
		Inner join DocumentType dt with(nolock) on dt.documenttypeid = et.documenttypeid and dt.clientid = @ClientID
		WHERE ET.clientid= @ClientID
		and et.EventSubtypeID  = 13 /*SMS event subtype*/
		--and  dt.RecipientsTo in ('[!A:Disabled Number]','[!Disabled]','[!Name] (Customer) [[!Disabled]]') /*In the future remove this filter + add prefix and suffix of disabled to aliasfields, then replace with '' when you want to enable again*/

		/*Enable batch jobs that apply an sms event*/
		--UPDATE at
		--SET at.Enabled = 1 ,at.SourceID = 1
		--FROM AutomatedTask at 
		--INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON ATP.TaskID = at.TaskID
		--INNER JOIN EventType et WITH (NOLOCK) on CAST(et.EventTypeID AS VARCHAR(2000)) =  ATP.ParamValue
		--INNER JOIN EventTypeMandatoryEvent ete WITH (NOLOCK) on ete.ToEventTypeID=et.EventTypeID 
		--WHERE atp.ParamName = 'LEADEVENT_TO_ADD'
		--and et.EventSubtypeID in (24,13) 
		--and ete.RequiredEventID = @MandatoryEvent
		--and ete.ClientID= @ClientID
		--and et.ClientID = @ClientID  
		--and at.Enabled = 1
		--and at.ClientID = @ClientID

		/**/
		UPDATE at
		SET at.enabled = 1
		, at.NextRunDateTime = CASE WHEN  substring(convert(varchar,at.NextRunDateTime ,121),11,3) is not null 
		THEN CAST( left(convert(varchar,dbo.fn_GetDate_Local(),21),11) + substring(convert(varchar,at.NextRunDateTime ,121),12,2) + ':00:00.000' AS DATETIME)
		ELSE  CAST( left(convert(varchar,dbo.fn_GetDate_Local(),21),11) + '10:00:00.000'  AS DATETIME) END 
		FROM  AutomatedTask at
		INNER JOIN AutomatedTaskHistory ath WITH (NOLOCK) on ath.TaskID = at.TaskID
		WHERE at.ClientID = @ClientID
		and at.Enabled = 0
		and ath.Enabled = 1
		and not exists (select * from AutomatedTaskHistory sub_ath WITH (NOLOCK) WHERE sub_ath.TaskID=ath.TaskID and sub_ath.TaskHistoryID > ath.TaskHistoryID)
		and exists (select * from AutomatedTaskResult atr WITH (NOLOCK) WHERE atr.TaskID= ath.TaskID)
				
		/*Remove the mandatory event you've added for the client*/	
		DELETE FROM EventTypeMandatoryEvent
		WHERE RequiredEventID = @MandatoryEvent 
		and ClientID = @ClientID
		
		SELECT 'Enabled SMS and batch jobs'
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DisableSMS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DisableSMS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DisableSMS] TO [sp_executeall]
GO
