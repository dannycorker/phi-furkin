SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 22/10/2014
-- Description:	Finds a lead type with the given script name
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FindLeadTypeWithScriptName]

	@ClientID INT,
	@ScriptName Varchar(250)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM LeadType WITH (NOLOCK) 
	WHERE ClientID=@ClientID and LeadTypeName=@ScriptName
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindLeadTypeWithScriptName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindLeadTypeWithScriptName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindLeadTypeWithScriptName] TO [sp_executeall]
GO
