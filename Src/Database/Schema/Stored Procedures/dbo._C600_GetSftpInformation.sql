SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-12-21
-- Description:	Gets the SFTP information for payaway
-- AH Removed line 25 and 29 as not required for CP
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetSftpInformation]

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	
	SELECT cdvSftpServer.DetailValue SftpServer,
		   cdvSftpUserName.DetailValue SftpUserName,
		   cdvSftpPassword.DetailValue SftpPassword
		   /*cdvSftpCertificate.DetailValue SftpCertificate --AH Removed as not required for CP*/
	FROM ClientDetailValues cdvSftpServer WITH (NOLOCK) 
	Left JOIN dbo.ClientDetailValues cdvSftpUserName WITH (NOLOCK) ON cdvSftpUserName.DetailFieldID=177115 AND cdvSftpUserName.ClientID=@ClientID
	Left JOIN dbo.ClientDetailValues cdvSftpPassword WITH (NOLOCK) ON cdvSftpPassword.DetailFieldID=177116 AND cdvSftpPassword.ClientID=@ClientID
	/*Left JOIN dbo.ClientDetailValues cdvSftpCertificate WITH (NOLOCK) ON cdvSftpCertificate.DetailFieldID=177121 AND cdvSftpCertificate.ClientID=@ClientID*/
	WHERE cdvSftpServer.DetailFieldID=177172 AND cdvSftpServer.ClientID=@ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetSftpInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetSftpInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetSftpInformation] TO [sp_executeall]
GO
