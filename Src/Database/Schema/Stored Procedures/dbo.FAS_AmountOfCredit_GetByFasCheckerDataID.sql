SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FAS_AmountOfCredit table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_GetByFasCheckerDataID]
(

	@FasCheckerDataID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FasAmountOfCreditID],
					[FasCheckerDataID],
					[Description],
					[CostAmount]
				FROM
					[dbo].[FAS_AmountOfCredit] WITH (NOLOCK) 
				WHERE
					[FasCheckerDataID] = @FasCheckerDataID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_GetByFasCheckerDataID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_GetByFasCheckerDataID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_GetByFasCheckerDataID] TO [sp_executeall]
GO
