SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptDialogue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_Insert]
(

	@ScriptDialogueID int    OUTPUT,

	@ScriptSectionID int   ,

	@ClientID int   ,

	@Dialogue varchar (MAX)  ,

	@HtmlDialogue varchar (MAX)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@WhenModified datetime   ,

	@WhoModified int   
)
AS


				
				INSERT INTO [dbo].[ScriptDialogue]
					(
					[ScriptSectionID]
					,[ClientID]
					,[Dialogue]
					,[HtmlDialogue]
					,[WhenCreated]
					,[WhoCreated]
					,[WhenModified]
					,[WhoModified]
					)
				VALUES
					(
					@ScriptSectionID
					,@ClientID
					,@Dialogue
					,@HtmlDialogue
					,@WhenCreated
					,@WhoCreated
					,@WhenModified
					,@WhoModified
					)
				-- Get the identity value
				SET @ScriptDialogueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Insert] TO [sp_executeall]
GO
