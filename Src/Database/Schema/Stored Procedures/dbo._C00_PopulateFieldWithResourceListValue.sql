SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateFieldWithResourceListValue]
(
	@ToDetailFieldID int,
	@FromDetailFieldID int,
	@ResListValueFieldID int,
	@CaseID int
)


AS
BEGIN
	SET NOCOUNT ON;

	Declare	@LeadID int,
			@LeadOrMatterToField int,
			@LeadOrMatterFromField int,
			@DetailValue varchar(100) = '',
			@MatterID int,
			@TestID int

	Select @LeadOrMatterToField = LeadOrMatter
	From DetailFields WITH (NOLOCK)
	Where DetailFieldID = @ToDetailFieldID

	Select @LeadOrMatterFromField = LeadOrMatter
	From DetailFields WITH (NOLOCK)
	Where DetailFieldID = @FromDetailFieldID

	Select @LeadID = Cases.LeadID
	From Cases WITH (NOLOCK)
	Where CaseID = @CaseID

	Exec _C00_CreateDetailFields @ToDetailFieldID, @LeadID

	If @LeadOrMatterToField = 1
	Begin 

		If @LeadOrMatterFromField = 1
		Begin 

			Select @LeadID = Matter.LeadID
			From Matter WITH (NOLOCK)
			Where CaseID = @CaseID

			Select @DetailValue = rldv.DetailValue
			From LeadDetailValues ldv WITH (NOLOCK)
			Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = ldv.ValueInt and rldv.DetailFieldID = @ResListValueFieldID
			Where ldv.DetailFieldID = @FromDetailFieldID
			and ldv.LeadID = @LeadID


		End

		If @LeadOrMatterFromField = 2
		Begin 

			Select @LeadID = Matter.LeadID, @MatterID = Matter.MatterID
			From Matter WITH (NOLOCK)
			Where CaseID = @CaseID

			Select @DetailValue = rldv.DetailValue
			From MatterDetailValues mdv WITH (NOLOCK)
			Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = @ResListValueFieldID
			Where mdv.DetailFieldID = @FromDetailFieldID
			and mdv.MatterID = @MatterID
		End

		Update LeadDetailValues
		Set DetailValue = @DetailValue
		Where LeadDetailValues.LeadID = @LeadID
		and LeadDetailValues.DetailFieldID = @ToDetailFieldID

	END

	If @LeadOrMatterToField = 2
	Begin 

		Select @TestID = 0

		Select top 1 @MatterID = MatterID
		From Matter WITH (NOLOCK)
		Where CaseID = @CaseID
		Order By MatterID

		While (@MatterID > 0 and @MatterID > @TestID)
		Begin

			If @LeadOrMatterFromField = 1
			Begin 

				Select @LeadID = Matter.LeadID
				From Matter WITH (NOLOCK)
				Where CaseID = @CaseID

				Select @DetailValue = rldv.DetailValue
				From LeadDetailValues ldv WITH (NOLOCK)
				Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = ldv.ValueInt and rldv.DetailFieldID = @ResListValueFieldID
				Where ldv.DetailFieldID = @FromDetailFieldID
				and ldv.LeadID = @LeadID


			End

			If @LeadOrMatterFromField = 2
			Begin 

				Select @LeadID = Matter.LeadID
				From Matter WITH (NOLOCK)
				Where CaseID = @CaseID

				Select @DetailValue = rldv.DetailValue
				From MatterDetailValues mdv WITH (NOLOCK)
				Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = @ResListValueFieldID
				Where mdv.DetailFieldID = @FromDetailFieldID
				and mdv.MatterID = @MatterID

				--select @DetailValue

			End

			If @DetailValue IS NOT NULL
			Begin

				Update MatterDetailValues
				Set DetailValue = @DetailValue
				Where MatterDetailValues.MatterID = @MatterID
				and MatterDetailValues.DetailFieldID = @ToDetailFieldID

			End

			Select @TestID = @MatterID

			Select top 1 @MatterID = MatterID
			From Matter WITH (NOLOCK)
			Where CaseID = @CaseID
			And MatterID > @MatterID
			Order By MatterID

		End

	END

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateFieldWithResourceListValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateFieldWithResourceListValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateFieldWithResourceListValue] TO [sp_executeall]
GO
