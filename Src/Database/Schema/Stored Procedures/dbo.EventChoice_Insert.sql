SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice_Insert]
(

	@EventChoiceID int    OUTPUT,

	@EventTypeID int   ,

	@Description varchar (50)  ,

	@NextEventTypeID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ThreadNumber int   ,

	@EscalationEvent bit   ,

	@Field varchar (50)  ,

	@LogicalOperator varchar (50)  ,

	@Value1 varchar (50)  ,

	@Value2 varchar (50)  ,

	@SqlClauseForInclusion varchar (2000)  ,

	@Weighting int   ,

	@ValueTypeID int   
)
AS


				
				INSERT INTO [dbo].[EventChoice]
					(
					[EventTypeID]
					,[Description]
					,[NextEventTypeID]
					,[ClientID]
					,[LeadTypeID]
					,[ThreadNumber]
					,[EscalationEvent]
					,[Field]
					,[LogicalOperator]
					,[Value1]
					,[Value2]
					,[SqlClauseForInclusion]
					,[Weighting]
					,[ValueTypeID]
					)
				VALUES
					(
					@EventTypeID
					,@Description
					,@NextEventTypeID
					,@ClientID
					,@LeadTypeID
					,@ThreadNumber
					,@EscalationEvent
					,@Field
					,@LogicalOperator
					,@Value1
					,@Value2
					,@SqlClauseForInclusion
					,@Weighting
					,@ValueTypeID
					)
				-- Get the identity value
				SET @EventChoiceID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Insert] TO [sp_executeall]
GO
