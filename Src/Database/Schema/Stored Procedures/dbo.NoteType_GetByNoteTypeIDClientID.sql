SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the NoteType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType_GetByNoteTypeIDClientID]
(

	@NoteTypeID int   ,

	@ClientID int   
)
AS


				SELECT
					[NoteTypeID],
					[ClientID],
					[NoteTypeName],
					[NoteTypeDescription],
					[DefaultPriority],
					[AlertColour],
					[NormalColour],
					[Enabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID],
					[LeadTypeID],
					[IsShared]
				FROM
					dbo.fnNoteTypeShared(@ClientID)
				WHERE
										[NoteTypeID] = @NoteTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_GetByNoteTypeIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType_GetByNoteTypeIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_GetByNoteTypeIDClientID] TO [sp_executeall]
GO
