SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 4/8/2016
-- Description:	Updates a client payment gateway record
-- Modified By PR:01/09/2016 Added IpAuthentication
-- =============================================
CREATE PROCEDURE [dbo].[ClientPaymentGateway__Update]
	@ClientPaymentGatewayID INT, 
	@ClientID INT, 
	@PaymentGatewayID INT, 
	@Login VARCHAR(250), 
	@UserName VARCHAR(250), 
	@Password VARCHAR(250), 
	@MerchantName VARCHAR(250), 
	@AccountID VARCHAR(250), 
	@MerchantUrl VARCHAR(MAX), 
	@TransactionKey VARCHAR(500), 
	@TerminalID VARCHAR(250), 
	@ApplySecure3D INT, 
	@TestMode BIT,
	@IpAuthentication BIT
AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE ClientPaymentGateway
	SET ClientID=@ClientID, 
		PaymentGatewayID=@PaymentGatewayID,
		Login=@Login,
		UserName=@UserName,
		Password=@Password,
		MerchantName=@MerchantName,
		AccountID=@AccountID,
		MerchantUrl=@MerchantUrl,
		TransactionKey=@TransactionKey,
		TerminalID=@TerminalID,
		ApplySecure3D=@ApplySecure3D,		
		TestMode=@TestMode,
		IpAuthentication=@IpAuthentication
	WHERE ClientPaymentGatewayID=@ClientPaymentGatewayID    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPaymentGateway__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__Update] TO [sp_executeall]
GO
