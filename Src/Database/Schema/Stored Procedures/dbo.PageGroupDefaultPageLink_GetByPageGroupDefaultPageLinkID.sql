SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PageGroupDefaultPageLink table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroupDefaultPageLink_GetByPageGroupDefaultPageLinkID]
(

	@PageGroupDefaultPageLinkID int   
)
AS


				SELECT
					[PageGroupDefaultPageLinkID],
					[ClientID],
					[PageGroupID],
					[DetailFieldPageID],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[PageGroupDefaultPageLink] WITH (NOLOCK) 
				WHERE
										[PageGroupDefaultPageLinkID] = @PageGroupDefaultPageLinkID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_GetByPageGroupDefaultPageLinkID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroupDefaultPageLink_GetByPageGroupDefaultPageLinkID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_GetByPageGroupDefaultPageLinkID] TO [sp_executeall]
GO
