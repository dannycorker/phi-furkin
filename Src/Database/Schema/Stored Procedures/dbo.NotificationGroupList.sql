SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Notification Group List
-- =============================================
CREATE PROCEDURE [dbo].[NotificationGroupList] 
	@ClientPersonnelID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ng.[NotificationGroupID],
		ng.[ClientID],
		ng.[NotificationGroupName],
		ng.[Enabled],
		ng.[Deleted],
		ng.[WhoCreated],
		ng.[WhenCreated],
		ng.[WhoModified],
		ng.[WhenModified]
	FROM 
		[dbo].[NotificationGroup] ng WITH (NOLOCK),
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND ng.[NotificationGroupID] = a.[LocationID]
	ORDER BY ng.[NotificationGroupID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupList] TO [sp_executeall]
GO
