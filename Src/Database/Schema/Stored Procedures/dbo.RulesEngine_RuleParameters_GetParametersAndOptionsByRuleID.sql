SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-10-2014
-- Description:	Gets a list of parameters and the associated parameter options
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleParameters_GetParametersAndOptionsByRuleID]
(
	@ClientID INT,
	@RuleID INT	
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT rp.RuleParameterID, rp.Name, rp.RuleID, rp.ParameterTypeID, rp.Value, rp.Parent, po.ParameterOptionID,po.OperatorID,po.Val1, po.Val2, po.OptionOrder
	FROM RulesEngine_RuleParameters rp WITH (NOLOCK) 
	INNER JOIN RulesEngine_ParameterOptions po WITH (NOLOCK) ON rp.RuleParameterID=po.RuleParameterID
	WHERE rp.RuleID=@RuleID AND rp.ClientID=@ClientID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_GetParametersAndOptionsByRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleParameters_GetParametersAndOptionsByRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_GetParametersAndOptionsByRuleID] TO [sp_executeall]
GO
