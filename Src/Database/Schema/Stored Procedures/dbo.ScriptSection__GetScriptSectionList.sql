SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson 
-- Create date: 21-10-2014
-- Description:	Gets a set of script sections
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSection__GetScriptSectionList]
	@ScriptID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT ss.ScriptSectionID, ss.ScriptID, ss.ClientID, ss.SectionTitle, ss.SectionOrder 
	FROM ScriptSection ss WITH (NOLOCK) 	
	WHERE ss.ScriptID=@ScriptID
	ORDER BY ss.SectionOrder ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__GetScriptSectionList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection__GetScriptSectionList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__GetScriptSectionList] TO [sp_executeall]
GO
