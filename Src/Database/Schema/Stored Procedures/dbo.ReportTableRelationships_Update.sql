SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ReportTableRelationships table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTableRelationships_Update]
(

	@ReportTableRelationshipID int   ,

	@TableFrom varchar (50)  ,

	@TableTo varchar (50)  ,

	@TableJoin varchar (2000)  ,

	@Description varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ReportTableRelationships]
				SET
					[TableFrom] = @TableFrom
					,[TableTo] = @TableTo
					,[TableJoin] = @TableJoin
					,[Description] = @Description
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ReportTableRelationshipID] = @ReportTableRelationshipID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTableRelationships_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Update] TO [sp_executeall]
GO
