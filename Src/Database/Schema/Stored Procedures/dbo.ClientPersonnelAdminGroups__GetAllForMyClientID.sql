SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01 March 2007

-- Created By:  ()
-- Purpose: Select records from the ClientPersonnelAdminGroups table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAdminGroups__GetAllForMyClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					*
				FROM
					dbo.[ClientPersonnelAdminGroups]
				WHERE
					([ClientID] = @ClientID OR [ClientID] = 0)
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups__GetAllForMyClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAdminGroups__GetAllForMyClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups__GetAllForMyClientID] TO [sp_executeall]
GO
