SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the BillStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@BillStatusID int   = null ,

	@Name varchar (50)  = null ,

	@Description varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [BillStatusID]
	, [Name]
	, [Description]
    FROM
	[dbo].[BillStatus] WITH (NOLOCK) 
    WHERE 
	 ([BillStatusID] = @BillStatusID OR @BillStatusID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [BillStatusID]
	, [Name]
	, [Description]
    FROM
	[dbo].[BillStatus] WITH (NOLOCK) 
    WHERE 
	 ([BillStatusID] = @BillStatusID AND @BillStatusID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Find] TO [sp_executeall]
GO
