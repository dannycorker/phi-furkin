SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientQuestionnairesVisitedButNotComplete table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedButNotComplete_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientQuestionnairesVisitedButNotCompleteID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@VisitedDate datetime   = null ,

	@IPAddress varchar (50)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientQuestionnairesVisitedButNotCompleteID]
	, [ClientQuestionnaireID]
	, [VisitedDate]
	, [IPAddress]
	, [ClientID]
    FROM
	[dbo].[ClientQuestionnairesVisitedButNotComplete] WITH (NOLOCK) 
    WHERE 
	 ([ClientQuestionnairesVisitedButNotCompleteID] = @ClientQuestionnairesVisitedButNotCompleteID OR @ClientQuestionnairesVisitedButNotCompleteID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([VisitedDate] = @VisitedDate OR @VisitedDate IS NULL)
	AND ([IPAddress] = @IPAddress OR @IPAddress IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientQuestionnairesVisitedButNotCompleteID]
	, [ClientQuestionnaireID]
	, [VisitedDate]
	, [IPAddress]
	, [ClientID]
    FROM
	[dbo].[ClientQuestionnairesVisitedButNotComplete] WITH (NOLOCK) 
    WHERE 
	 ([ClientQuestionnairesVisitedButNotCompleteID] = @ClientQuestionnairesVisitedButNotCompleteID AND @ClientQuestionnairesVisitedButNotCompleteID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([VisitedDate] = @VisitedDate AND @VisitedDate is not null)
	OR ([IPAddress] = @IPAddress AND @IPAddress is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Find] TO [sp_executeall]
GO
