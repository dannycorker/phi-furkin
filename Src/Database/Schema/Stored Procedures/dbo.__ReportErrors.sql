SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-11-03
-- Description:	Report errors
-- =============================================
CREATE PROCEDURE [dbo].[__ReportErrors]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @SqlQuery varchar(max), @Title varchar(100), @Body varchar(max)

	Select @SqlQuery = 'SET NOCOUNT ON ' + sq.QueryText, @Title = sq.QueryTitle
	From SqlQuery sq with (nolock)
	where sq.QueryID = 14450

	declare @SchedulerSlow TABLE (NextRunDateTime datetime, TaskID int, ClientID int, TaskName varchar(100), AlreadyRunning bit, QueueID int, SchedulerID int, ServerName varchar(20), LockDateTime datetime)

	INSERT INTO @SchedulerSlow (NextRunDateTime, TaskID, ClientID, TaskName, AlreadyRunning, QueueID, SchedulerID, ServerName, LockDateTime)
	exec (@SqlQuery)

	IF @@ROWCOUNT > 0
	BEGIN

		SELECT @Body = ISNULL(@Body,'') + 'ClientID: <font color="red">' + CONVERT(varchar(100),s.ClientID) + '</font> Percentage: <font color="red">' + CONVERT(varchar,CONVERT(Numeric(18,2),COUNT(*)*100.00 / (SELECT COUNT(*) FROM @SchedulerSlow))) + '</font> Number Of Failures: <font color="red">' + CONVERT(varchar,COUNT(*)) + '</font><Br>
	'
		FROM @SchedulerSlow s
		GROUP BY s.ClientID

		EXEC msdb.dbo.sp_send_dbmail @profile_name='DB2Mail',
		@recipients='alex.elger@aquarium-software.com;jim.green@aquarium-software.com;cathal.sherry@aquarium-software.com',
		@subject=@Title,
		@body=@Body,
		@body_format = 'HTML',
		@from_address='jim.green@aquarium-software.com',
		@reply_to='jim.green@aquarium-software.com',
		@query=@SqlQuery,
		@attach_query_result_as_file=1,
		@query_attachment_filename='slow.csv',
		@query_result_separator='	',
		@exclude_query_output=1,
		@execute_query_database='Aquarius',
		@query_result_no_padding = 1,
		@query_result_width = 32767

		DELETE FROM @SchedulerSlow

	END	

	Select @SqlQuery = 'SET NOCOUNT ON ' + sq.QueryText, @Title = sq.QueryTitle
	From SqlQuery sq with (nolock)
	where sq.QueryID = 14449

	INSERT INTO @SchedulerSlow (NextRunDateTime, TaskID, ClientID, TaskName, AlreadyRunning, QueueID, SchedulerID, ServerName, LockDateTime)
	exec (@SqlQuery)

	IF @@ROWCOUNT > 0
	BEGIN

		SELECT @Body = ISNULL(@Body,'') + 'ClientID: <font color="red">' + CONVERT(varchar(100),s.ClientID) + '</font> Percentage: <font color="red">' + CONVERT(varchar,CONVERT(Numeric(18,2),COUNT(*)*100.00 / (SELECT COUNT(*) FROM @SchedulerSlow))) + '</font> Number Of Failures: <font color="red">' + CONVERT(varchar,COUNT(*)) + '</font>
	'
		FROM @SchedulerSlow s
		GROUP BY s.ClientID

		EXEC msdb.dbo.sp_send_dbmail @profile_name='DB2Mail',
		@recipients='alex.elger@aquarium-software.com;jim.green@aquarium-software.com;cathal.sherry@aquarium-software.com',
		@subject=@Title,
		@body=@Body,
		@body_format = 'HTML',
		@from_address='jim.green@aquarium-software.com',
		@reply_to='jim.green@aquarium-software.com',
		@query=@SqlQuery,
		@attach_query_result_as_file=1,
		@query_attachment_filename='slow.csv',
		@query_result_separator='	',
		@exclude_query_output=1,
		@execute_query_database='Aquarius',
		@query_result_no_padding = 1,
		@query_result_width = 32767

	END	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__ReportErrors] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__ReportErrors] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__ReportErrors] TO [sp_executeall]
GO
