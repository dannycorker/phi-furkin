SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerQuestionnaires table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerQuestionnaires_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SELECT
					[CustomerQuestionnaireID],
					[ClientQuestionnaireID],
					[CustomerID],
					[SubmissionDate],
					[TrackingID],
					[Referrer],
					[SearchTerms],
					[ClientID]
				FROM
					[dbo].[CustomerQuestionnaires] WITH (NOLOCK) 
				WHERE
										[ClientQuestionnaireID] = @ClientQuestionnaireID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerQuestionnaires_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_GetByClientQuestionnaireID] TO [sp_executeall]
GO
