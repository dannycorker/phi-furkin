SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the BillItem table through a foreign key with additional ClientPersonnel data joined in.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem__GetForGridByBillID]
(
	@BillID int   
)
AS

	SET ANSI_NULLS OFF

	SELECT 
		[BillItemID],
		bi.[ClientID],
		[BillID],
		[Description],
		cp.[Username],
		[WorkUnits],
		[Total]
	FROM
		[dbo].[BillItem] bi
	INNER JOIN 
		[dbo].[ClientPersonnel] cp on bi.[ClientPersonnelID] = cp.[ClientPersonnelID]
	WHERE
		bi.[BillID] = @BillID

	Select @@ROWCOUNT
	SET ANSI_NULLS ON






GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem__GetForGridByBillID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem__GetForGridByBillID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem__GetForGridByBillID] TO [sp_executeall]
GO
