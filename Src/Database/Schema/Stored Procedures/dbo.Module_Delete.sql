SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Module table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Module_Delete]
(

	@ModuleID int   
)
AS


				DELETE FROM [dbo].[Module] WITH (ROWLOCK) 
				WHERE
					[ModuleID] = @ModuleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Module_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Delete] TO [sp_executeall]
GO
