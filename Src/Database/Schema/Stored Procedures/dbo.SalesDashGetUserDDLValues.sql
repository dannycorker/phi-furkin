SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SalesDashGetUserDDLValues]
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT cp.*
	FROM ClientPersonnel cp WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues rldv_can_see WITH (NOLOCK) ON rldv_can_see.ValueInt = cp.ClientPersonnelID AND rldv_can_see.DetailFieldID = 299823
	INNER JOIN ResourceListDetailValues rldv_manager WITH (NOLOCK) ON rldv_manager.ResourceListID = rldv_can_see.ResourceListID AND rldv_manager.DetailFieldID = 299822
	WHERE (rldv_manager.ValueInt = @ClientPersonnelID OR rldv_can_see.ValueInt = @ClientPersonnelID)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SalesDashGetUserDDLValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SalesDashGetUserDDLValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SalesDashGetUserDDLValues] TO [sp_executeall]
GO
