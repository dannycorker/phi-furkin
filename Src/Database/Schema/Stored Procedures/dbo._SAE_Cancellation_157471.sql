SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Abort Cancellation
-- 2020-03-25 AAD for AAG-453 | Delete cancellation row from WrittenPremium table instead of Premium Detail History. Set 
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Cancellation_157471]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@ClaimLeadID					INT
			,@CancellationWrittenPremiumID	INT
			,@EventComments					VARCHAR(2000)
			,@EndDate						DATE
			,@ExistingWrittenPremiumID		INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

IF @EventTypeID = 157471  /*Abort Cancellation*/
	BEGIN 

		/* 2020-03-25 AAD - Removed section used to delete the new Premium Detail History row and set the old one back to Live */

		/* 2020-03-25 AAD AAG-453 - Identify and delete the new Cancellation row from the WrittenPremium table */

		SELECT TOP 1 @CancellationWrittenPremiumID = wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4 /* Cancellation */
		ORDER BY wp.WrittenPremiumID DESC

		DELETE FROM WrittenPremium
		WHERE MatterID = @MatterID
		AND WrittenPremiumID = @CancellationWrittenPremiumID

		--SELECT @TableRowID = tr.TableRowID
		--FROM TableRows tr WITH ( NOLOCK ) 
		--INNER JOIN TableDetailValues sttus WITH ( NOLOCK ) on tr.TableRowID = sttus.TableRowID and sttus.DetailFieldID = 175722
		--INNER JOIN TableDetailValues adjustty WITH ( NOLOCK ) on adjustty.TableRowID = tr.TableRowID and adjustty.DetailFieldID = 175398
		--	WHERE tr.MatterID = @MatterID
		--	AND tr.DetailFieldID = 	aq 1, 2, 175336
		--	AND sttus.ValueInt = 72326
		--	and adjustty.ValueInt = 74529
		--	AND NOT EXISTS (SELECT * FROM TableRows subtr WITH ( NOLOCK ) 
		--						INNER JOIN TableDetailValues sttussub WITH ( NOLOCK ) on subtr.TableRowID = sttussub.TableRowID and sttussub.DetailFieldID = 175722
		--						WHERE subtr.MatterID = @MatterID
		--						AND subtr.DetailFieldID = 175336
		--						AND sttussub.ValueInt = 72326
		--						AND subtr.TableRowID > tr.tablerowID)

		/*Remove the tablerow we have just made*/
		--EXEC TableRows_Delete @TableRowID 
		
		/*and reset the old one to live, possibly need to find a better way of finding this*/
		--SELECT @TableRowID = tr.TableRowID
		--FROM TableRows tr WITH ( NOLOCK ) 
		--	WHERE tr.MatterID = @MatterID
		--	AND tr.DetailFieldID = 	175336
		--	AND NOT EXISTS (SELECT * FROM TableRows subtr WITH ( NOLOCK ) 
		--						WHERE subtr.MatterID = @MatterID
		--						AND subtr.DetailFieldID = 175336
		--						AND subtr.TableRowID > tr.tablerowID)
		
		--/*Set to live*/		
		--EXEC _C00_SimpleValueIntoField 175722,72326,@TableRowID,@WhoCreated --changed from @MatterID to @TableRowID as it's a field on a table NG 2017-09-25

		SELECT @EventComments = 'Cancellation Aborted, ' + CAST(@CancellationWrittenPremiumID as VARCHAR (20))

		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		/* Set the policy from cancellation pending*/
		EXEC _C00_SimpleValueIntoField 170038,43002,@MatterID,@WhoCreated
		
		SELECT @ClaimLeadID = dbo.fn_C00_1272_GetClaimLeadFromPolicyLead(@LeadID)

		/* 2020-03-25 AAD AAG-453 - Reset WrittenPremium ValidTo to policy end date */

		SELECT @EndDate = dbo.fnGetSimpleDvAsDate(170037, @MatterID) /* Policy End Date aq 1, 2, 170037 */

		IF @EndDate IS NOT NULL
		BEGIN

			SELECT TOP 1 @ExistingWrittenPremiumID = wp.WrittenPremiumID
			FROM WrittenPremium wp WITH (NOLOCK)
			WHERE wp.MatterID = @MatterID
			ORDER BY wp.WrittenPremiumID 

			UPDATE WrittenPremium
			SET ValidTo = @EndDate
			WHERE WrittenPremiumID = @ExistingWrittenPremiumID

		END
	
	END 
	
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Cancellation_157471] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Cancellation_157471] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Cancellation_157471] TO [sp_executeall]
GO
