SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ObjectDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectDetailValues_Insert]
(

	@ObjectDetailValueID int    OUTPUT,

	@ClientID int   ,

	@SubClientID int   ,

	@ObjectID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   
)
AS


				
				INSERT INTO [dbo].[ObjectDetailValues]
					(
					[ClientID]
					,[SubClientID]
					,[ObjectID]
					,[DetailFieldID]
					,[DetailValue]
					,[ErrorMsg]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					)
				VALUES
					(
					@ClientID
					,@SubClientID
					,@ObjectID
					,@DetailFieldID
					,@DetailValue
					,@ErrorMsg
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					)
				-- Get the identity value
				SET @ObjectDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_Insert] TO [sp_executeall]
GO
