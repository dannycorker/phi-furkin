SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlFunction table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunction_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlFunctionID int   = null ,

	@SqlFunctionName varchar (50)  = null ,

	@FunctionAlias varchar (50)  = null ,

	@FunctionDescription varchar (250)  = null ,

	@RestrictedToDataTypeID int   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null ,

	@IsPreferred bit   = null ,

	@Example varchar (250)  = null ,

	@ExampleResult varchar (250)  = null ,

	@ResultDataTypeID int   = null ,

	@ExpressionMask varchar (250)  = null ,

	@IsAggregate bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlFunctionID]
	, [SqlFunctionName]
	, [FunctionAlias]
	, [FunctionDescription]
	, [RestrictedToDataTypeID]
	, [WhenCreated]
	, [WhenModified]
	, [IsPreferred]
	, [Example]
	, [ExampleResult]
	, [ResultDataTypeID]
	, [ExpressionMask]
	, [IsAggregate]
    FROM
	[dbo].[SqlFunction] WITH (NOLOCK) 
    WHERE 
	 ([SqlFunctionID] = @SqlFunctionID OR @SqlFunctionID IS NULL)
	AND ([SqlFunctionName] = @SqlFunctionName OR @SqlFunctionName IS NULL)
	AND ([FunctionAlias] = @FunctionAlias OR @FunctionAlias IS NULL)
	AND ([FunctionDescription] = @FunctionDescription OR @FunctionDescription IS NULL)
	AND ([RestrictedToDataTypeID] = @RestrictedToDataTypeID OR @RestrictedToDataTypeID IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([IsPreferred] = @IsPreferred OR @IsPreferred IS NULL)
	AND ([Example] = @Example OR @Example IS NULL)
	AND ([ExampleResult] = @ExampleResult OR @ExampleResult IS NULL)
	AND ([ResultDataTypeID] = @ResultDataTypeID OR @ResultDataTypeID IS NULL)
	AND ([ExpressionMask] = @ExpressionMask OR @ExpressionMask IS NULL)
	AND ([IsAggregate] = @IsAggregate OR @IsAggregate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlFunctionID]
	, [SqlFunctionName]
	, [FunctionAlias]
	, [FunctionDescription]
	, [RestrictedToDataTypeID]
	, [WhenCreated]
	, [WhenModified]
	, [IsPreferred]
	, [Example]
	, [ExampleResult]
	, [ResultDataTypeID]
	, [ExpressionMask]
	, [IsAggregate]
    FROM
	[dbo].[SqlFunction] WITH (NOLOCK) 
    WHERE 
	 ([SqlFunctionID] = @SqlFunctionID AND @SqlFunctionID is not null)
	OR ([SqlFunctionName] = @SqlFunctionName AND @SqlFunctionName is not null)
	OR ([FunctionAlias] = @FunctionAlias AND @FunctionAlias is not null)
	OR ([FunctionDescription] = @FunctionDescription AND @FunctionDescription is not null)
	OR ([RestrictedToDataTypeID] = @RestrictedToDataTypeID AND @RestrictedToDataTypeID is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([IsPreferred] = @IsPreferred AND @IsPreferred is not null)
	OR ([Example] = @Example AND @Example is not null)
	OR ([ExampleResult] = @ExampleResult AND @ExampleResult is not null)
	OR ([ResultDataTypeID] = @ResultDataTypeID AND @ResultDataTypeID is not null)
	OR ([ExpressionMask] = @ExpressionMask AND @ExpressionMask is not null)
	OR ([IsAggregate] = @IsAggregate AND @IsAggregate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunction_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Find] TO [sp_executeall]
GO
