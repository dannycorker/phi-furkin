SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIClaimStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_Delete]
(

	@RPIClaimStatusID int   
)
AS


				DELETE FROM [dbo].[RPIClaimStatus] WITH (ROWLOCK) 
				WHERE
					[RPIClaimStatusID] = @RPIClaimStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Delete] TO [sp_executeall]
GO
