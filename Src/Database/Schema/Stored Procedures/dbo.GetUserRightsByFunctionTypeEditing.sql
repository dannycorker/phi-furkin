SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-03
-- Description:	Get all EDITING rights for this User
--		These records only exist while the group is
--      actually being edited. Then they get deleted.
--              2008-07-16: Encrypted Detail Fields
-- =============================================
CREATE PROCEDURE [dbo].[GetUserRightsByFunctionTypeEditing]
	-- Add the parameters for the stored procedure here
	@GroupID int,
	@UserID int,
	@FunctionTypeID int,
	@LeadTypeID int,
	@OfficeID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- 1) Get the Control record for this function type
	SELECT ufc.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	ufc.UserFunctionControlEditingID as 'UniqueID',
	-1 as 'LeadTypeID'   
	FROM UserFunctionControlEditing ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID = @FunctionTypeID
	AND ufc.LeadTypeID IS NULL
	AND @LeadTypeID = -1

	UNION

	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	ufc.UserFunctionControlEditingID as 'UniqueID',
	ufc.LeadTypeID as 'LeadTypeID'     
	FROM UserFunctionControlEditing ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID 
	INNER JOIN LeadType lt ON lt.LeadTypeID = ufc.LeadTypeID 
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID = @FunctionTypeID
	AND ufc.LeadTypeID = lt.LeadTypeID
	AND ufc.LeadTypeID = @LeadTypeID

	UNION

	-- Very special case to show all the possible
	-- virtual lead types when the user clicks "Lead Types"
	SELECT ufc.FunctionTypeID as 'ID', 
	lt.LeadTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	4 as 'Source', 
	ufc.UserFunctionControlEditingID as 'UniqueID',
	ufc.LeadTypeID as 'LeadTypeID'    
	FROM UserFunctionControlEditing ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	INNER JOIN LeadType lt ON lt.LeadTypeID = ufc.LeadTypeID 
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID = 23
	AND @FunctionTypeID = 7
	AND ufc.LeadTypeID IS NOT NULL
	AND @LeadTypeID = -1

	UNION

    -- 2) Get the Control records for the direct descendants of this function type
	SELECT ft.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	ufc.UserFunctionControlEditingID as 'UniqueID',
	-1 as 'LeadTypeID'     
	FROM UserFunctionControlEditing ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND ufc.LeadTypeID IS NULL 
	AND @LeadTypeID = -1

	UNION

    -- Special case for virtual function type 23, representing individual lead types
	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	ufc.UserFunctionControlEditingID as 'UniqueID',
	ufc.LeadTypeID as 'LeadTypeID'     
	FROM UserFunctionControlEditing ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	INNER JOIN LeadType lt ON lt.LeadTypeID = ufc.LeadTypeID 
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND ufc.LeadTypeID = lt.LeadTypeID
	AND ufc.LeadTypeID = @LeadTypeID

	UNION

    -- 3) Get any dynamic rights records for this function type
	SELECT urd.ObjectID as 'ID', 
	f.FunctionDescription as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	3 as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',
	-1 as 'LeadTypeID'      
	FROM UserRightsDynamicEditing urd INNER JOIN Functions f ON f.FunctionID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID 
	AND urd.LeadTypeID IS NULL
	AND @LeadTypeID = -1 -- Only show these when general rights are requested, not when EventType rights by lead type (12, 13 and 25) are being requested!
	AND @FunctionTypeID <> 26 /* Exclude NoteTypes by Client */

	UNION

	-- Specific: Outcomes
	SELECT urd.ObjectID as 'ID', 
	o.OutcomeName COLLATE SQL_Latin1_General_CP1_CI_AS as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',
	-1 as 'LeadTypeID'     
	FROM UserRightsDynamicEditing urd INNER JOIN Outcomes o ON o.OutcomeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 9 -- 9=Outcomes 
	AND urd.LeadTypeID IS NULL

	UNION

	-- Specific: Lead Types by Office
	SELECT urd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID' 
	FROM UserRightsDynamicEditing urd INNER JOIN LeadType lt ON lt.LeadTypeID = urd.LeadTypeID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 11 -- 11=Lead Types by Office
	AND urd.LeadTypeID IS NOT NULL
	AND urd.ObjectID = @OfficeID 
    
	UNION

	-- Specific: Pages by Lead Type
	SELECT urd.ObjectID as 'ID', 
	dfp.PageName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',
	urd.LeadTypeID as 'LeadTypeID'     
	FROM UserRightsDynamicEditing urd INNER JOIN DetailFieldPages dfp ON dfp.DetailFieldPageID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 12 -- 12=Pages 
	AND urd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Event Types by Lead Type
	SELECT urd.ObjectID as 'ID', 
	et.EventTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',
	urd.LeadTypeID as 'LeadTypeID'     
	FROM UserRightsDynamicEditing urd INNER JOIN EventType et ON et.EventTypeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 13 -- 13=Events 
	AND urd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Encrypted Detail Fields by Lead Type
	SELECT urd.ObjectID as 'ID', 
	df.FieldName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',
	urd.LeadTypeID as 'LeadTypeID'     
	FROM UserRightsDynamicEditing urd INNER JOIN DetailFields df ON df.DetailFieldID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 25 -- 25=Encrypted Detail Fields 
	AND urd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Note Types for this Client
	SELECT urd.ObjectID as 'ID', 
	nt.NoteTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID' 
	FROM UserRightsDynamicEditing urd 
	INNER JOIN NoteType nt WITH (NOLOCK) ON nt.NoteTypeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 26 -- 26=Notes 
	AND @LeadTypeID = -1
	
	UNION

	-- Specific: Lead Type rights
	SELECT urd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicEditingID as 'UniqueID',
	urd.LeadTypeID as 'LeadTypeID'     
	FROM UserRightsDynamicEditing urd INNER JOIN LeadType lt ON lt.LeadTypeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 23 -- 23= Specific Lead Type
	AND urd.LeadTypeID = @LeadTypeID 

	ORDER BY 5, 2, 1

END























GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserRightsByFunctionTypeEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserRightsByFunctionTypeEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserRightsByFunctionTypeEditing] TO [sp_executeall]
GO
