SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-06
-- Description:	Auto-import files that have arrived by email from our clients.
-- =============================================
CREATE PROCEDURE [dbo].[DataLoader_ImportThisFile]
	@ClientID INT,
	@FilenameNoPath VARCHAR(2000),
	@MapIDToUse INT, 
	@FileFormatID INT,
	@ClientPersonnelID INT 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @NewFileID INT,
	@NewFilename VARCHAR(250),
	@ClientIDVarchar VARCHAR(10)
	
	DECLARE @OutputCommands TABLE (CommandText VARCHAR(500), CommandOrder TINYINT)
	
	/* Create a DataLoaderFile record */
	INSERT INTO dbo.DataLoaderFile (
		ClientID,
		DataLoaderMapID,
		FileStatusID,
		SourceFileNameAndPath,
		TargetFileName,
		TargetFileLocation,
		FileFormatID,
		ScheduledDateTime,
		UploadedBy
	)
	SELECT
		@ClientID,
		@MapIDToUse, 
		2,	/* Scheduled */
		'From Email',
		@FilenameNoPath, 
		'Inbox',
		@FileFormatID,  
		DATEADD(MINUTE, 1, dbo.fn_GetDate_Local()),
		@ClientPersonnelID
	
	
	/* Output the ID, to be used as the new filename (eg 16521.csv) */
	SELECT @NewFileID = SCOPE_IDENTITY()
	
	SELECT @NewFilename = CAST(@NewFileID AS VARCHAR(250)) + '.' + dlff.FileFormatName 
	FROM dbo.DataLoaderFileFormat dlff (NOLOCK) 
	WHERE dlff.DataLoaderFileFormatID = @FileFormatID 
	
	SELECT @ClientIDVarchar = CAST(@ClientID AS VARCHAR)
	
	INSERT INTO @OutputCommands (CommandText, CommandOrder)
	VALUES 
	 ('move "\\468000-File1\AqShared\SharedResources\DataLoader\AutoImport\Client_' + @ClientIDVarchar + '\' + @FilenameNoPath + '" "\\468000-File1\AqShared\SharedResources\DataLoader\Inbox\Client_' + @ClientIDVarchar + '\' + @NewFilename + '"', 10)
	
	/* 
		Final output for the resulting DOS script.
	*/
	SELECT oc.CommandText 
	FROM @OutputCommands oc
	ORDER BY oc.CommandOrder 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoader_ImportThisFile] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoader_ImportThisFile] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoader_ImportThisFile] TO [sp_executeall]
GO
