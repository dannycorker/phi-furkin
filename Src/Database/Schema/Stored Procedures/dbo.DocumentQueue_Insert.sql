SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentQueue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentQueue_Insert]
(

	@DocumentQueueID int    OUTPUT,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@DocumentTypeID int   ,

	@WhoCreated int   ,

	@WhenStored datetime   ,

	@WhenCreated datetime   ,

	@RequiresApproval bit   ,

	@ParsedDocumentTitle varchar (1000)  ,

	@BasedUponLeadEventID int   
)
AS


				
				INSERT INTO [dbo].[DocumentQueue]
					(
					[ClientID]
					,[CustomerID]
					,[LeadID]
					,[CaseID]
					,[DocumentTypeID]
					,[WhoCreated]
					,[WhenStored]
					,[WhenCreated]
					,[RequiresApproval]
					,[ParsedDocumentTitle]
					,[BasedUponLeadEventID]
					)
				VALUES
					(
					@ClientID
					,@CustomerID
					,@LeadID
					,@CaseID
					,@DocumentTypeID
					,@WhoCreated
					,@WhenStored
					,@WhenCreated
					,@RequiresApproval
					,@ParsedDocumentTitle
					,@BasedUponLeadEventID
					)
				-- Get the identity value
				SET @DocumentQueueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Insert] TO [sp_executeall]
GO
