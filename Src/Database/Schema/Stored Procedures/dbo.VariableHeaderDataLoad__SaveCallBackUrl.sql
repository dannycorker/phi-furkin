SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-06-2015
-- Description:	Saves the call back url for the variable header data load
-- =============================================
CREATE PROCEDURE [dbo].[VariableHeaderDataLoad__SaveCallBackUrl]

	@ClientID INT,
	@CallbackURL VARCHAR(2000)

AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @DetailFieldID INT
	SELECT @DetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4203 -- call back url field    
    
    IF @DetailFieldID>0
    BEGIN
		EXEC _C00_SimpleValueIntoField @DetailFieldID, @CallbackURL, @ClientID
    END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__SaveCallBackUrl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad__SaveCallBackUrl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__SaveCallBackUrl] TO [sp_executeall]
GO
