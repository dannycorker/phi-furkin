SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_Update]
(

	@EventTypeID int   ,

	@ClientID int   ,

	@EventTypeName varchar (50)  ,

	@EventTypeDescription varchar (250)  ,

	@Enabled bit   ,

	@UnitsOfEffort int   ,

	@FollowupTimeUnitsID int   ,

	@FollowupQuantity int   ,

	@AvailableManually bit   ,

	@StatusAfterEvent int   ,

	@AquariumEventAfterEvent int   ,

	@EventSubtypeID int   ,

	@DocumentTypeID int   ,

	@LeadTypeID int   ,

	@AllowCustomTimeUnits bit   ,

	@InProcess bit   ,

	@KeyEvent bit   ,

	@UseEventCosts bit   ,

	@UseEventUOEs bit   ,

	@UseEventDisbursements bit   ,

	@UseEventComments bit   ,

	@SignatureRequired bit   ,

	@SignatureOverride bit   ,

	@VisioX decimal (18, 8)  ,

	@VisioY decimal (18, 8)  ,

	@AquariumEventSubtypeID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@FollowupWorkingDaysOnly bit   ,

	@CalculateTableRows bit   ,

	@SourceID int   ,

	@SmsGatewayID int   ,

	@IsShared bit   ,

	@SocialFeedID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventType]
				SET
					[ClientID] = @ClientID
					,[EventTypeName] = @EventTypeName
					,[EventTypeDescription] = @EventTypeDescription
					,[Enabled] = @Enabled
					,[UnitsOfEffort] = @UnitsOfEffort
					,[FollowupTimeUnitsID] = @FollowupTimeUnitsID
					,[FollowupQuantity] = @FollowupQuantity
					,[AvailableManually] = @AvailableManually
					,[StatusAfterEvent] = @StatusAfterEvent
					,[AquariumEventAfterEvent] = @AquariumEventAfterEvent
					,[EventSubtypeID] = @EventSubtypeID
					,[DocumentTypeID] = @DocumentTypeID
					,[LeadTypeID] = @LeadTypeID
					,[AllowCustomTimeUnits] = @AllowCustomTimeUnits
					,[InProcess] = @InProcess
					,[KeyEvent] = @KeyEvent
					,[UseEventCosts] = @UseEventCosts
					,[UseEventUOEs] = @UseEventUOEs
					,[UseEventDisbursements] = @UseEventDisbursements
					,[UseEventComments] = @UseEventComments
					,[SignatureRequired] = @SignatureRequired
					,[SignatureOverride] = @SignatureOverride
					,[VisioX] = @VisioX
					,[VisioY] = @VisioY
					,[AquariumEventSubtypeID] = @AquariumEventSubtypeID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[FollowupWorkingDaysOnly] = @FollowupWorkingDaysOnly
					,[CalculateTableRows] = @CalculateTableRows
					,[SourceID] = @SourceID
					,[SmsGatewayID] = @SmsGatewayID
					,[IsShared] = @IsShared
					,[SocialFeedID] = @SocialFeedID
				WHERE
[EventTypeID] = @EventTypeID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Update] TO [sp_executeall]
GO
