SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-25
-- Description:	List all LeadDetailValues records for a record
-- =============================================
CREATE PROCEDURE [dbo].[ldv] 
	@LeadID int = NULL,
	@LeadTypeID int = NULL, 
	@DetailFieldID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	/* Option to show values for a particular field */
	IF @LeadID IS NULL AND @LeadTypeID IS NULL AND @DetailFieldID IS NOT NULL
	BEGIN
		SELECT TOP 100 ldv.* 
		FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
		WHERE ldv.DetailFieldID = @DetailFieldID
	END
	ELSE
	BEGIN
		
		/* Grab a sample lead for the lead type passed in */
		IF @LeadID IS NULL AND @LeadTypeID IS NOT NULL
		BEGIN
			SELECT TOP 1 @LeadID = LeadID 
			FROM dbo.Lead l (nolock) 
			WHERE l.LeadTypeID = @LeadTypeID 
			ORDER BY l.LeadID DESC
		END
		
		/* Show all existing values for this lead */
		IF @LeadID IS NOT NULL
		BEGIN
			SELECT ldv.*, df.DetailFieldID, df.FieldName, df.LeadOrMatter, df.QuestionTypeID, df.[Enabled] 
			FROM dbo.LeadDetailValues ldv (nolock) 
			INNER JOIN dbo.DetailFields df (nolock) on df.DetailFieldID = ldv.DetailFieldID 
			WHERE ldv.LeadID = @LeadID 
			AND (@DetailFieldID IS NULL OR df.DetailFieldID = @DetailFieldID)
			ORDER BY ldv.DetailFieldID, ldv.LeadDetailValueID  
		END

		/* Need lead type to show missing values in the next step */
		IF @LeadTypeID IS NULL
		BEGIN
			SELECT @LeadTypeID = l.LeadTypeID 
			FROM dbo.Lead l (nolock) 
			WHERE l.LeadID = @LeadID 
		END
		
		/* Show missing values for this lead */
		IF @LeadTypeID IS NOT NULL
		BEGIN
			
			SELECT df.DetailFieldID, df.FieldName, df.LeadOrMatter, df.QuestionTypeID, df.[Enabled] 
			FROM dbo.DetailFields df (nolock) 
			LEFT JOIN dbo.LeadDetailValues ldv (nolock) on df.DetailFieldID = ldv.DetailFieldID AND ldv.LeadID = @LeadID 
			WHERE df.LeadTypeID = @LeadTypeID 
			AND df.LeadOrMatter = 1 
			AND ldv.DetailFieldID IS NULL
			AND (@DetailFieldID IS NULL OR df.DetailFieldID = @DetailFieldID)
			ORDER BY df.DetailFieldID 
			
		END
		
	END
		
END






GO
GRANT VIEW DEFINITION ON  [dbo].[ldv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ldv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ldv] TO [sp_executeall]
GO
