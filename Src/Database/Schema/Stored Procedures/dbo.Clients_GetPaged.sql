SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the Clients table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ClientID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([ClientID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ClientID]'
				SET @SQL = @SQL + ' FROM [dbo].[Clients] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[ClientID], O.[CompanyName], O.[WebAddress], O.[IPAddress], O.[DefaultEmailAddress], O.[ClientTypeID], O.[AllowSMS], O.[SecurityCode], O.[LeadsBelongToOffices], O.[UseEventCosts], O.[UseEventUOEs], O.[UseEventDisbursements], O.[UseEventComments], O.[VerifyAddress], O.[UseTapi], O.[UseRPI], O.[UseGBAddress], O.[UsePinpoint], O.[UseSage], O.[UseSAS], O.[UseCreditCalculation], O.[FollowupWorkingDaysOnly], O.[AddLeadByQuestionnaire], O.[UseIncendia], O.[LanguageID], O.[CountryID], O.[UseMobileInterface], O.[UseGBValidation], O.[CurrencyID], O.[AllowSmsCommandProcessing]
				FROM
				    [dbo].[Clients] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[ClientID] = PageIndex.[ClientID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[Clients] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_GetPaged] TO [sp_executeall]
GO
