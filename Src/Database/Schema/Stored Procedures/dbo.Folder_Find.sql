SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Folder table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_Find]
(

	@SearchUsingOR bit   = null ,

	@FolderID int   = null ,

	@ClientID int   = null ,

	@FolderTypeID int   = null ,

	@FolderParentID int   = null ,

	@FolderName varchar (50)  = null ,

	@FolderDescription varchar (250)  = null ,

	@WhenCreated datetime   = null ,

	@WhoCreated int   = null ,

	@Personal bit   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FolderID]
	, [ClientID]
	, [FolderTypeID]
	, [FolderParentID]
	, [FolderName]
	, [FolderDescription]
	, [WhenCreated]
	, [WhoCreated]
	, [Personal]
	, [SourceID]
    FROM
	[dbo].[Folder] WITH (NOLOCK) 
    WHERE 
	 ([FolderID] = @FolderID OR @FolderID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FolderTypeID] = @FolderTypeID OR @FolderTypeID IS NULL)
	AND ([FolderParentID] = @FolderParentID OR @FolderParentID IS NULL)
	AND ([FolderName] = @FolderName OR @FolderName IS NULL)
	AND ([FolderDescription] = @FolderDescription OR @FolderDescription IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([Personal] = @Personal OR @Personal IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FolderID]
	, [ClientID]
	, [FolderTypeID]
	, [FolderParentID]
	, [FolderName]
	, [FolderDescription]
	, [WhenCreated]
	, [WhoCreated]
	, [Personal]
	, [SourceID]
    FROM
	[dbo].[Folder] WITH (NOLOCK) 
    WHERE 
	 ([FolderID] = @FolderID AND @FolderID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FolderTypeID] = @FolderTypeID AND @FolderTypeID is not null)
	OR ([FolderParentID] = @FolderParentID AND @FolderParentID is not null)
	OR ([FolderName] = @FolderName AND @FolderName is not null)
	OR ([FolderDescription] = @FolderDescription AND @FolderDescription is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([Personal] = @Personal AND @Personal is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Find] TO [sp_executeall]
GO
