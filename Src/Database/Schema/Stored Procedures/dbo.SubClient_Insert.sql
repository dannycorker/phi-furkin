SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SubClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubClient_Insert]
(

	@SubClientID int    OUTPUT,

	@ClientID int   ,

	@CompanyName varchar (100)  ,

	@ClientTypeID int   ,

	@LanguageID int   ,

	@CountryID int   ,

	@WhenCreated datetime   
)
AS


				
				INSERT INTO [dbo].[SubClient]
					(
					[ClientID]
					,[CompanyName]
					,[ClientTypeID]
					,[LanguageID]
					,[CountryID]
					,[WhenCreated]
					)
				VALUES
					(
					@ClientID
					,@CompanyName
					,@ClientTypeID
					,@LanguageID
					,@CountryID
					,@WhenCreated
					)
				-- Get the identity value
				SET @SubClientID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubClient_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Insert] TO [sp_executeall]
GO
