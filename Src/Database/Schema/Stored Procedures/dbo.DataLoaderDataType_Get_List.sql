SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderDataType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDataType_Get_List]

AS


				
				SELECT
					[DataLoaderDataTypeID],
					[DataTypeName],
					[DataTypeDescription]
				FROM
					[dbo].[DataLoaderDataType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDataType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Get_List] TO [sp_executeall]
GO
