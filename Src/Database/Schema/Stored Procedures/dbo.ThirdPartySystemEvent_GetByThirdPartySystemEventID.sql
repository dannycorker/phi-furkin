SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartySystemEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_GetByThirdPartySystemEventID]
(

	@ThirdPartySystemEventID int   
)
AS


				SELECT
					[ThirdPartySystemEventID],
					[ThirdPartySystemID],
					[ClientID],
					[SubClientID],
					[Name],
					[Description],
					[EventTypeID],
					[LeadTypeID],
					[ThirdPartySystemKey],
					[MessageName]
				FROM
					[dbo].[ThirdPartySystemEvent] WITH (NOLOCK) 
				WHERE
										[ThirdPartySystemEventID] = @ThirdPartySystemEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_GetByThirdPartySystemEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_GetByThirdPartySystemEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_GetByThirdPartySystemEventID] TO [sp_executeall]
GO
