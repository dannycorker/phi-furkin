SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TimeUnits table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TimeUnits_GetByTimeUnitsID]
(

	@TimeUnitsID int   
)
AS


				SELECT
					[TimeUnitsID],
					[TimeUnitsName],
					[SQLTimeUnits]
				FROM
					[dbo].[TimeUnits] WITH (NOLOCK) 
				WHERE
										[TimeUnitsID] = @TimeUnitsID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_GetByTimeUnitsID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimeUnits_GetByTimeUnitsID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_GetByTimeUnitsID] TO [sp_executeall]
GO
