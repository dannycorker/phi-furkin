SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_Delete]
(

	@AccessRuleID int   
)
AS


				DELETE FROM [dbo].[AccessRule] WITH (ROWLOCK) 
				WHERE
					[AccessRuleID] = @AccessRuleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Delete] TO [sp_executeall]
GO
