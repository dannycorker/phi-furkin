SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlFunction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunction_Delete]
(

	@SqlFunctionID int   
)
AS


				DELETE FROM [dbo].[SqlFunction] WITH (ROWLOCK) 
				WHERE
					[SqlFunctionID] = @SqlFunctionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunction_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Delete] TO [sp_executeall]
GO
