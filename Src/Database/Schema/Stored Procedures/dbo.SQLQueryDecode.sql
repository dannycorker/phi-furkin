SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-08-07
-- Description:	Pull out query text with decoded ObjectIDs for debugging
-- =============================================
CREATE PROCEDURE [dbo].[SQLQueryDecode]  
	@SQLQueryID int = 30448
AS
BEGIN

	DECLARE @QueryText varchar(8000),
			@ClientID int,
			@RebuiltQueryText varchar(8000) = '',
			@MyText varchar(200)

	DECLARE @SplitString TABLE (LineNum int, TextLine varchar(8000), TypeID int, ObjectID int, ToReplace varchar(200), ReplaceWith varchar(200) )
	
	DECLARE @MyOutput Table (Line int, LineText varchar(8000) )

	/*
	Find every instance of each type of ID, and add comments for the name of whatever that ID relates to
	*/

	/*
	Types to start with
	1 = DetailFieldID
	2 = EventTypeID
	3 = LeadTypeID
	*/

	SELECT @QueryText = sq.QueryText, @ClientID = sq.ClientID, @MyText = sq.QueryTitle
	FROM dbo.SqlQuery sq WITH (NOLOCK) 
	WHERE sq.QueryID = @SQLQueryID
	
	INSERT @MyOutput (Line,LineText)
	VALUES (-5,'aq 1,5,' + convert(varchar,@SQLQueryID) + ' /*' + @MyText + '*/')
	
	INSERT @MyOutput (Line,LineText) 
	SELECT -1, CASE t.N	WHEN 1 THEN  'aq 1,1,' + convert(varchar,at.TaskID) + ' /*' + at.Taskname + '*/'
						WHEN 2 THEN CASE  WHEN et.EventTypeID is null THEN '' ELSE 'aq 1,3,' + atp_le.ParamValue + ' /*' + et.EventTypeName + '*/' END
						ELSE '' END  
	FROM dbo.AutomatedTask at WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTaskParam atp_sq WITH (NOLOCK) on atp_sq.ParamName = 'SQL_QUERY' and atp_sq.TaskID = at.TaskID
	INNER JOIN dbo.AutomatedTaskParam atp_le WITH (NOLOCK) on atp_le.ParamName = 'LEADEVENT_TO_ADD' and atp_le.TaskID = at.TaskID 
	LEFT JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = atp_le.ParamValue
	INNER JOIN dbo.Tally t WITH (NOLOCK) on t.N < 3
	WHERE atp_sq.ParamValue = @SQLQueryID

	INSERT @SplitString (LineNum,TextLine)
	exec SplitString @SQLQueryID
	
	/*Tidy up the SplitString to make it easier to parse*/
	UPDATE ss
	SET TextLine = REPLACE(REPLACE(TextLine,'EventTypeID = (','EventTypeID = '),'))',')')
	FROM @SplitString ss 
	WHERE rtrim(ss.TextLine) like '%EventTypeID = (%))%'
	/*Tidy up the SplitString to make it easier to parse*/
	
	UPDATE ss
	SET TypeID = 1, 
		ObjectID = SUBSTRING(ss.TextLine,CHARINDEX('DetailFieldID = ',ss.TextLine,0)+16, CHARINDEX(')',ss.TextLine,CHARINDEX('DetailFieldID = ',ss.TextLine,0))-CHARINDEX('DetailFieldID = ',ss.TextLine,0)-16), 
		ToReplace = 'DetailFieldID = ' + SUBSTRING(ss.TextLine,CHARINDEX('DetailFieldID = ',ss.TextLine,0)+16, CHARINDEX(')',ss.TextLine,CHARINDEX('DetailFieldID = ',ss.TextLine,0))-CHARINDEX('DetailFieldID = ',ss.TextLine,0)-16)
	FROM @SplitString ss
	WHERE ss.TextLine like '%DetailFieldID = %)%'
	and ss.TextLine not like '%=%=%'
	and ss.TextLine not like '% OR %'
	
	UPDATE ss
	SET TypeID = 2, 
		ObjectID = SUBSTRING(ss.TextLine,CHARINDEX('EventTypeID = ',ss.TextLine,0)+14, CHARINDEX(')',ss.TextLine,CHARINDEX('EventTypeID = ',ss.TextLine,0))-CHARINDEX('EventTypeID = ',ss.TextLine,0)-14), 
		ToReplace = 'EventTypeID = ' + SUBSTRING(ss.TextLine,CHARINDEX('EventTypeID = ',ss.TextLine,0)+14, CHARINDEX(')',ss.TextLine,CHARINDEX('EventTypeID = ',ss.TextLine,0))-CHARINDEX('EventTypeID = ',ss.TextLine,0)-14)
	FROM @SplitString ss
	WHERE ss.TextLine like '%EventTypeID = %)%'
	and ss.TextLine not like '%.EventTypeID = %.EventTypeID%'
	and ss.TextLine not like '%=%=%'
	and ss.TextLine not like '% OR %'
	
	UPDATE ss
	SET TypeID = 3, 
		ObjectID = SUBSTRING(ss.TextLine,CHARINDEX('LeadTypeID = ',ss.TextLine,0)+13, CHARINDEX(')',ss.TextLine,CHARINDEX('LeadTypeID = ',ss.TextLine,0))-CHARINDEX('LeadTypeID = ',ss.TextLine,0)-13), 
		ToReplace = 'LeadTypeID = ' + SUBSTRING(ss.TextLine,CHARINDEX('LeadTypeID = ',ss.TextLine,0)+13, CHARINDEX(')',ss.TextLine,CHARINDEX('LeadTypeID = ',ss.TextLine,0))-CHARINDEX('LeadTypeID = ',ss.TextLine,0)-13)
	FROM @SplitString ss
	WHERE ss.TextLine like '%LeadTypeID = %)%'
	and ss.TextLine not like '%.LeadTypeID = %.LeadTypeID%'
	and ss.TextLine not like '%=%=%'
	and ss.TextLine not like '% OR %'

	UPDATE ss
	SET ReplaceWith = Replace(ss.ToReplace,ss.ObjectID, convert(varchar,ss.ObjectID) + ' /*' + df.FieldName + '*/ ')
	FROM @SplitString ss
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = ss.ObjectID and df.ClientID = @ClientID
	WHERE TypeID = 1

	UPDATE ss
	SET ReplaceWith = Replace(ss.ToReplace,ss.ObjectID, convert(varchar,ss.ObjectID) + ' /*' + et.EventTypeName + '*/ ')
	FROM @SplitString ss
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ss.ObjectID and et.ClientID = @ClientID
	WHERE TypeID = 2

	UPDATE ss
	SET ReplaceWith = Replace(ss.ToReplace,ss.ObjectID, convert(varchar,ss.ObjectID) + ' /*' + lt.LeadTypeName + '*/ ')
	FROM @SplitString ss
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) on lt.LeadTypeID = ss.ObjectID and lt.ClientID = @ClientID
	WHERE TypeID = 3

	SELECT @QueryText = REPLACE(@QueryText,ss.ToReplace,ss.ReplaceWith)
	FROM @SplitString ss
	WHERE ReplaceWith is not null
	
	SELECT @RebuiltQueryText = @RebuiltQueryText + replace(ss.TextLine,isnull(ss.ToReplace,''),isnull(ss.ReplaceWith,''))
	FROM @SplitString ss

	SELECT @RebuiltQueryText = replace(replace(replace(@RebuiltQueryText,'@ClientID', convert(varchar,@ClientID) + ' /*@ClientID in QueryText*/'),'@Yesterday''', convert(varchar(10),dbo.fn_GetDate_Local()-1,121) + ''' /*@Yesterday in QueryText*/ '),'@Today''', convert(varchar(10),dbo.fn_GetDate_Local(),121) + ''' /*@Today in QueryText*/ ')

	
	INSERT @MyOutput (Line,LineText)
	EXEC SplitString @InputStr = @RebuiltQueryText, @InputType = 'SQL', @UseOldVersion = 0
	
	SELECT mo.LineText
	FROM @MyOutput mo
	ORDER BY mo.Line
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SQLQueryDecode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SQLQueryDecode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SQLQueryDecode] TO [sp_executeall]
GO
