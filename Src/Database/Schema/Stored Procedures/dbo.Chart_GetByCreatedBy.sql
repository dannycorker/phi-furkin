SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Chart table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_GetByCreatedBy]
(

	@CreatedBy int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChartID],
					[QueryID],
					[ClientID],
					[ChartTitle],
					[ChartDescription],
					[ChartTypeID],
					[XAxisColumn],
					[CreatedBy],
					[CreatedOn],
					[LastEditedBy],
					[LastEditedOn]
				FROM
					[dbo].[Chart] WITH (NOLOCK) 
				WHERE
					[CreatedBy] = @CreatedBy
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_GetByCreatedBy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_GetByCreatedBy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_GetByCreatedBy] TO [sp_executeall]
GO
