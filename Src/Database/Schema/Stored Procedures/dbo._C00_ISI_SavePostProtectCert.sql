SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-04-17
-- Description:	Save check boxes from custom "view"
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISI_SavePostProtectCert]
	@ClientPersonnelID INT,
	@TableRowID INT,
	@Type INT,
	@Value BIT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	
	/*
		Just using clientID to make sure we are updating the correct
		clients data..
	*/
	SELECT @ClientID = cp.ClientID
	FROM ClientPersonnel cp WITH (NOLOCK)
	WHERE cp.ClientPersonnelID = @ClientPersonnelID

	/*
		This proc is to take the table row and what type (thrd party field)
		to update to true if 1 and false if 0.
	*/
	
	/*Log details of the save into the logs table*/
	INSERT INTO Logs (LogDateTime, TypeOfLogEntry, ClassName, MethodName, LogEntry, ClientPersonnelID)
	VALUES (dbo.fn_GetDate_Local(), 'Debug', '_C00_ISI_SavePostProtectCert', '', 'ClientPersonnelID:' + CONVERT(VARCHAR,@ClientPersonnelID) + ' TableRowID:' + CONVERT(VARCHAR,@TableRowID) + ' Value:' + CONVERT(VARCHAR,@Value) + ' Type:' + CONVERT(VARCHAR,@Type), NULL)

	UPDATE tdv
	SET DetailValue = CASE @Value WHEN 1 THEN 'true' ELSE 'false' END
	FROM TableDetailValues tdv
	INNER JOIN ThirdPartyFieldMapping tpm ON tpm.DetailFieldID = tdv.DetailFieldID and tpm.ThirdPartyFieldID = @Type
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_SavePostProtectCert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISI_SavePostProtectCert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_SavePostProtectCert] TO [sp_executeall]
GO
