SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-11-2012
-- Description:	Gets the automate work flow group by event type id
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedWorkflow__GetByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	SELECT et.EventTypeName, wfg.Name, etw.[EventTypeAutomatedWorkflowID], etw.[ClientID], etw.[EventTypeID], etw.[WorkflowGroupID], etw.[RunAsUserID], CASE WHEN etw.[FollowUp] = 1 THEN '1' ELSE '0' END AS [FollowUp], etw.[EventTypeToAdd], etw.[Priority], CASE WHEN etw.[Important] = 1 THEN '1' ELSE '0' END AS [Important] 
	FROM EventTypeAutomatedWorkflow etw WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = etw.EventTypeToAdd
	INNER JOIN dbo.WorkflowGroup wfg WITH (NOLOCK) ON wfg.WorkflowGroupID = etw.WorkflowGroupID
	WHERE etw.EventTypeID=@EventTypeID 
	AND etw.ClientID = @ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflow__GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedWorkflow__GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflow__GetByEventTypeID] TO [sp_executeall]
GO
