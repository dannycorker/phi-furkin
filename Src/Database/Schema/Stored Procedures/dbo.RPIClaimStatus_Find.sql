SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIClaimStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIClaimStatusID int   = null ,

	@MatterID int   = null ,

	@ClientID int   = null ,

	@ApplicationID varchar (150)  = null ,

	@ActivityEngineGuid varchar (250)  = null ,

	@PhaseCacheID varchar (250)  = null ,

	@PhaseCacheName varchar (250)  = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@RPIAsUser varchar (250)  = null ,

	@RPIUser varchar (250)  = null ,

	@RPIPass varchar (250)  = null ,

	@Notes varchar (500)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIClaimStatusID]
	, [MatterID]
	, [ClientID]
	, [ApplicationID]
	, [ActivityEngineGuid]
	, [PhaseCacheID]
	, [PhaseCacheName]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [RPIAsUser]
	, [RPIUser]
	, [RPIPass]
	, [Notes]
    FROM
	[dbo].[RPIClaimStatus] WITH (NOLOCK) 
    WHERE 
	 ([RPIClaimStatusID] = @RPIClaimStatusID OR @RPIClaimStatusID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ApplicationID] = @ApplicationID OR @ApplicationID IS NULL)
	AND ([ActivityEngineGuid] = @ActivityEngineGuid OR @ActivityEngineGuid IS NULL)
	AND ([PhaseCacheID] = @PhaseCacheID OR @PhaseCacheID IS NULL)
	AND ([PhaseCacheName] = @PhaseCacheName OR @PhaseCacheName IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([RPIAsUser] = @RPIAsUser OR @RPIAsUser IS NULL)
	AND ([RPIUser] = @RPIUser OR @RPIUser IS NULL)
	AND ([RPIPass] = @RPIPass OR @RPIPass IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIClaimStatusID]
	, [MatterID]
	, [ClientID]
	, [ApplicationID]
	, [ActivityEngineGuid]
	, [PhaseCacheID]
	, [PhaseCacheName]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [RPIAsUser]
	, [RPIUser]
	, [RPIPass]
	, [Notes]
    FROM
	[dbo].[RPIClaimStatus] WITH (NOLOCK) 
    WHERE 
	 ([RPIClaimStatusID] = @RPIClaimStatusID AND @RPIClaimStatusID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ApplicationID] = @ApplicationID AND @ApplicationID is not null)
	OR ([ActivityEngineGuid] = @ActivityEngineGuid AND @ActivityEngineGuid is not null)
	OR ([PhaseCacheID] = @PhaseCacheID AND @PhaseCacheID is not null)
	OR ([PhaseCacheName] = @PhaseCacheName AND @PhaseCacheName is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([RPIAsUser] = @RPIAsUser AND @RPIAsUser is not null)
	OR ([RPIUser] = @RPIUser AND @RPIUser is not null)
	OR ([RPIPass] = @RPIPass AND @RPIPass is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Find] TO [sp_executeall]
GO
