SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brusehtt
-- Create date: 2011-08-02
-- Description:	Gets the admin contact for a portal user
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__GetAdminContactForPortalUser]
(
	@PortalUserID INT
)	
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT cpAdmin.* 
	FROM dbo.PortalUser pu WITH (NOLOCK)
	INNER JOIN dbo.ClientPersonnel cpImpersonate WITH (NOLOCK) ON pu.ClientPersonnelID = cpImpersonate.ClientPersonnelID
	INNER JOIN dbo.ClientOffices o WITH (NOLOCK) ON cpImpersonate.ClientOfficeID = o.ClientOfficeID
	INNER JOIN dbo.ClientPersonnel cpAdmin WITH (NOLOCK) ON o.AdminClientPersonnelID = cpAdmin.ClientPersonnelID
	WHERE pu.PortalUserID = @PortalUserID 
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetAdminContactForPortalUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetAdminContactForPortalUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetAdminContactForPortalUser] TO [sp_executeall]
GO
