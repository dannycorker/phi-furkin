SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-29
-- Description:	Add XML log entries (for exports and imports)
-- Modified by PR on 19-03-2013 Added InvalidXML column to store invalid XML
-- SLACKY-PERF-20190621- Reduce Load on LogXML INSERTS
-- =============================================
CREATE PROCEDURE [dbo].[LogXML__AddEntry]
	@ClientID INT,
	@ContextID INT, 
	@ContextVarchar VARCHAR(250),
	@LogXMLEntry XML  = NULL,
	@InvalidXML VARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	-- SLACKY-PERF-20190621- Reduce Load on LogXML INSERTS
	--IF	
	--	@ContextVarchar LIKE '%_C%_PA_Policy_GetProducts%' OR
	--	@ContextVarchar LIKE '%_C%_PA_Policy_GetCalculatedValues%' OR
	--	@ContextVarchar LIKE '%_C%_PA_Policy_GetQuoteValues%' OR
	--	@ContextVarchar LIKE '%QuoteApi-GetQuoteValues%' OR
	--	@ContextVarchar LIKE '%_C%_PA_Policy_SaveQuote%' OR
	--	@ContextVarchar LIKE '%RulesEngine_Evaluate%'
	--BEGIN
	--	RETURN;
	--END

	/*
		ContextID is a SqlQueryID or any other 
		ContextVarchar is (eg) the name of the calling proc
		LogXMLEntry is literally the XML being sent or received
	*/
	INSERT dbo.LogXML (
		ClientID,
		LogDateTime,
		ContextID,
		ContextVarchar,
		LogXMLEntry,
		InvalidXML
	)
    VALUES (
		@ClientID,
		dbo.fn_GetDate_Local(),
		@ContextID, 
		@ContextVarchar,
		@LogXMLEntry,
		@InvalidXML
	)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LogXML__AddEntry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogXML__AddEntry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogXML__AddEntry] TO [sp_executeall]
GO
