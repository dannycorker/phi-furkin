SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the GroupFunctionControlEditing table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing_Find]
(

	@SearchUsingOR bit   = null ,

	@GroupFunctionControlEditingID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ModuleID int   = null ,

	@FunctionTypeID int   = null ,

	@HasDescendants bit   = null ,

	@RightID int   = null ,

	@LeadTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [GroupFunctionControlEditingID]
	, [ClientPersonnelAdminGroupID]
	, [ModuleID]
	, [FunctionTypeID]
	, [HasDescendants]
	, [RightID]
	, [LeadTypeID]
    FROM
	[dbo].[GroupFunctionControlEditing] WITH (NOLOCK) 
    WHERE 
	 ([GroupFunctionControlEditingID] = @GroupFunctionControlEditingID OR @GroupFunctionControlEditingID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ModuleID] = @ModuleID OR @ModuleID IS NULL)
	AND ([FunctionTypeID] = @FunctionTypeID OR @FunctionTypeID IS NULL)
	AND ([HasDescendants] = @HasDescendants OR @HasDescendants IS NULL)
	AND ([RightID] = @RightID OR @RightID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [GroupFunctionControlEditingID]
	, [ClientPersonnelAdminGroupID]
	, [ModuleID]
	, [FunctionTypeID]
	, [HasDescendants]
	, [RightID]
	, [LeadTypeID]
    FROM
	[dbo].[GroupFunctionControlEditing] WITH (NOLOCK) 
    WHERE 
	 ([GroupFunctionControlEditingID] = @GroupFunctionControlEditingID AND @GroupFunctionControlEditingID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ModuleID] = @ModuleID AND @ModuleID is not null)
	OR ([FunctionTypeID] = @FunctionTypeID AND @FunctionTypeID is not null)
	OR ([HasDescendants] = @HasDescendants AND @HasDescendants is not null)
	OR ([RightID] = @RightID AND @RightID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Find] TO [sp_executeall]
GO
