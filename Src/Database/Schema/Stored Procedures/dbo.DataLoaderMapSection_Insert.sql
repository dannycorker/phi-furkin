SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderMapSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_Insert]
(

	@DataLoaderMapSectionID int    OUTPUT,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderObjectTypeID int   ,

	@DataLoaderObjectActionID int   ,

	@DataLoaderSectionLocaterTypeID int   ,

	@DetailFieldSubTypeID tinyint   ,

	@HasAquariumID bit   ,

	@NumberOfFooterRowsToSkip int   ,

	@ImportIntoLeadManager bit   ,

	@IsFixedLengthSection bit   ,

	@FixedLengthNumberOfRows int   ,

	@IsMandatory bit   ,

	@IsMultipleAllowedWithinFile bit   ,

	@Notes varchar (2000)  ,

	@TableDetailFieldID int   ,

	@ResourceListDetailFieldID int   ,

	@CreatePaymentSchedule bit   
)
AS


				
				INSERT INTO [dbo].[DataLoaderMapSection]
					(
					[ClientID]
					,[DataLoaderMapID]
					,[DataLoaderObjectTypeID]
					,[DataLoaderObjectActionID]
					,[DataLoaderSectionLocaterTypeID]
					,[DetailFieldSubTypeID]
					,[HasAquariumID]
					,[NumberOfFooterRowsToSkip]
					,[ImportIntoLeadManager]
					,[IsFixedLengthSection]
					,[FixedLengthNumberOfRows]
					,[IsMandatory]
					,[IsMultipleAllowedWithinFile]
					,[Notes]
					,[TableDetailFieldID]
					,[ResourceListDetailFieldID]
					,[CreatePaymentSchedule]
					)
				VALUES
					(
					@ClientID
					,@DataLoaderMapID
					,@DataLoaderObjectTypeID
					,@DataLoaderObjectActionID
					,@DataLoaderSectionLocaterTypeID
					,@DetailFieldSubTypeID
					,@HasAquariumID
					,@NumberOfFooterRowsToSkip
					,@ImportIntoLeadManager
					,@IsFixedLengthSection
					,@FixedLengthNumberOfRows
					,@IsMandatory
					,@IsMultipleAllowedWithinFile
					,@Notes
					,@TableDetailFieldID
					,@ResourceListDetailFieldID
					,@CreatePaymentSchedule
					)
				-- Get the identity value
				SET @DataLoaderMapSectionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Insert] TO [sp_executeall]
GO
