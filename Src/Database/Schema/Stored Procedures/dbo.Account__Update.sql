SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-06-2016
-- Description:	Updates account information
-- =============================================
CREATE PROCEDURE [dbo].[Account__Update]
	@AccountID INT, 
	@ClientID INT, 
	@CustomerID INT, 
	@AccountHolderName VARCHAR(250) = null, 
	@FriendlyName VARCHAR(250) = null, 
	@AccountNumber VARCHAR(50) = null, 
	@Sortcode VARCHAR(20) = null, 
	@Reference VARCHAR(250) = null, 
	@CardToken VARCHAR(250) = null, 
	@MaskedCardNumber VARCHAR(24) = null, 
	@ExpiryMonth INT = null, 
	@ExpiryYear INT = null, 
	@ExpiryDate DATETIME = null, 
	@AccountTypeID INT = null,
	@Active BIT = null, 
	@WhoCreated INT, 
	@WhenCreated DATETIME, 
	@WhoModified INT, 
	@WhenModified DATETIME,
	@InstitutionNumber VARCHAR(100) = ''
AS
BEGIN
		
	SET NOCOUNT ON;

	UPDATE Account
	SET ClientID=@ClientID, 
		CustomerID=@CustomerID, 
		AccountHolderName=@AccountHolderName, 
		FriendlyName=@FriendlyName, 
		AccountNumber=@AccountNumber, 
		Sortcode=@Sortcode, 
		Reference=@Reference, 
		CardToken=@CardToken, 
		MaskedCardNumber=@MaskedCardNumber, 
		ExpiryMonth=@ExpiryMonth, 
		ExpiryYear=@ExpiryYear, 
		ExpiryDate=@ExpiryDate, 
		AccountTypeID=@AccountTypeID, 
		Active=@Active, 
		WhoCreated=@WhoCreated, 
		WhenCreated=@WhenCreated, 
		WhoModified=@WhoModified, 
		WhenModified=@WhenModified,
		InstitutionNumber = @InstitutionNumber
	WHERE AccountID=@AccountID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__Update] TO [sp_executeall]
GO
