SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CurrencyRate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_Get_List]

AS


				
				SELECT
					[CurrencyRateID],
					[CurrencyId],
					[CurrencyCode],
					[FromGBPRate],
					[ToGBPRate],
					[ConversionDate],
					[IsLatest]
				FROM
					[dbo].[CurrencyRate] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Get_List] TO [sp_executeall]
GO
