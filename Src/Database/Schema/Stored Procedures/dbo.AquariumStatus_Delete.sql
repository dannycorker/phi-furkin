SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AquariumStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumStatus_Delete]
(

	@AquariumStatusID int   
)
AS


				DELETE FROM [dbo].[AquariumStatus] WITH (ROWLOCK) 
				WHERE
					[AquariumStatusID] = @AquariumStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Delete] TO [sp_executeall]
GO
