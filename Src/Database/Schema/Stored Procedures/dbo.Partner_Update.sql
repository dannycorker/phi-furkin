SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Partner table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Partner_Update]
(

	@PartnerID int   ,

	@CustomerID int   ,

	@ClientID int   ,

	@UseCustomerAddress bit   ,

	@TitleID int   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@EmailAddress varchar (255)  ,

	@DayTimeTelephoneNumber varchar (50)  ,

	@HomeTelephone varchar (50)  ,

	@MobileTelephone varchar (50)  ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (200)  ,

	@PostCode varchar (50)  ,

	@Occupation varchar (100)  ,

	@Employer varchar (100)  ,

	@DateOfBirth datetime   ,

	@FullName varchar (201)   OUTPUT,

	@CountryID int   ,

	@WhenModified datetime   ,

	@WhoModified int   ,

	@Longitude numeric (25, 18)  ,

	@Latitude numeric (25, 18)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Partner]
				SET
					[CustomerID] = @CustomerID
					,[ClientID] = @ClientID
					,[UseCustomerAddress] = @UseCustomerAddress
					,[TitleID] = @TitleID
					,[FirstName] = @FirstName
					,[MiddleName] = @MiddleName
					,[LastName] = @LastName
					,[EmailAddress] = @EmailAddress
					,[DayTimeTelephoneNumber] = @DayTimeTelephoneNumber
					,[HomeTelephone] = @HomeTelephone
					,[MobileTelephone] = @MobileTelephone
					,[Address1] = @Address1
					,[Address2] = @Address2
					,[Town] = @Town
					,[County] = @County
					,[PostCode] = @PostCode
					,[Occupation] = @Occupation
					,[Employer] = @Employer
					,[DateOfBirth] = @DateOfBirth
					,[CountryID] = @CountryID
					,[WhenModified] = @WhenModified
					,[WhoModified] = @WhoModified
					,[Longitude] = @Longitude
					,[Latitude] = @Latitude
				WHERE
[PartnerID] = @PartnerID 
				
				
				-- Select computed columns into output parameters
				SELECT
 @FullName = [FullName]
				FROM
					[dbo].[Partner] WITH (NOLOCK) 
				WHERE
[PartnerID] = @PartnerID 
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Update] TO [sp_executeall]
GO
