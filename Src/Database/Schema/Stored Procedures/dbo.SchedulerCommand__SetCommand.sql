SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Set commands for Automated Task Schedulers. Stop all by default.
-- =============================================
CREATE PROCEDURE [dbo].[SchedulerCommand__SetCommand] 
	@SchedulerIdentifier varchar(50) = 'ALL', 
	@SchedulerCommand varchar(50) = 'STOP', 
	@IsEnabled bit = 1 
AS
BEGIN

	SET NOCOUNT ON;
	
	DELETE FROM dbo.SchedulerCommand 
	WHERE SchedulerIdentifier = @SchedulerIdentifier
	
	INSERT INTO dbo.SchedulerCommand (SchedulerIdentifier, SchedulerCommand, IsEnabled)
	VALUES (@SchedulerIdentifier, @SchedulerCommand, @IsEnabled)
	
	SELECT * 
	FROM dbo.SchedulerCommand
	
	SELECT 'Remember to bounce the service afterwards!' AS [Remember to bounce the service afterwards!]
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__SetCommand] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SchedulerCommand__SetCommand] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__SetCommand] TO [sp_executeall]
GO
