SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIEventType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventType_GetByRPIEventTypeID]
(

	@RPIEventTypeID int   
)
AS


				SELECT
					[RPIEventTypeID],
					[RPIEventTypeName],
					[RPIEventTypeDescription],
					[IsAquariumTriggered],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[RPIEventType] WITH (NOLOCK) 
				WHERE
										[RPIEventTypeID] = @RPIEventTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_GetByRPIEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventType_GetByRPIEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_GetByRPIEventTypeID] TO [sp_executeall]
GO
