SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-23
-- Description:	Gets a list of VetEnvoy accounts to poll
-- Modified:	2015-04-28	SB	Added a type flag for filtering of test and live
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_GetAccountsToPoll]
(
	@ClientID INT,
	@Type VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * 
	FROM dbo.VetEnvoy_Accounts WITH (NOLOCK) 
	WHERE ClientID = @ClientID
	AND Enabled = 1
	AND AccountType = @Type

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_GetAccountsToPoll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_GetAccountsToPoll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_GetAccountsToPoll] TO [sp_executeall]
GO
