SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TableDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableDetailValues_GetByClientIDDetailFieldID]
(

	@ClientID int   ,

	@DetailFieldID int   
)
AS


				SELECT
					[TableDetailValueID],
					[TableRowID],
					[ResourceListID],
					[DetailFieldID],
					[DetailValue],
					[LeadID],
					[MatterID],
					[ClientID],
					[EncryptedValue],
					[ErrorMsg],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID]
				FROM
					[dbo].[TableDetailValues] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [DetailFieldID] = @DetailFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_GetByClientIDDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_GetByClientIDDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_GetByClientIDDetailFieldID] TO [sp_executeall]
GO
