SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentTypeFolderLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink_Delete]
(

	@DocumentTypeID int   ,

	@FolderID int   
)
AS


				DELETE FROM [dbo].[DocumentTypeFolderLink] WITH (ROWLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID
					AND [FolderID] = @FolderID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_Delete] TO [sp_executeall]
GO
