SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadEventThreadCompletion table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_GetByClientIDCaseIDFromLeadEventIDToLeadEventID]
(

	@ClientID int   ,

	@CaseID int   ,

	@FromLeadEventID int   ,

	@ToLeadEventID int   
)
AS


				SELECT
					[LeadEventThreadCompletionID],
					[ClientID],
					[LeadID],
					[CaseID],
					[FromLeadEventID],
					[FromEventTypeID],
					[ThreadNumberRequired],
					[ToLeadEventID],
					[ToEventTypeID]
				FROM
					[dbo].[LeadEventThreadCompletion] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [CaseID] = @CaseID
					AND [FromLeadEventID] = @FromLeadEventID
					AND [ToLeadEventID] = @ToLeadEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_GetByClientIDCaseIDFromLeadEventIDToLeadEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_GetByClientIDCaseIDFromLeadEventIDToLeadEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_GetByClientIDCaseIDFromLeadEventIDToLeadEventID] TO [sp_executeall]
GO
