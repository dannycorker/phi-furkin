SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 31-11-2014
-- Description:	Gets a distinct list of tables via script id and client id
-- =============================================
CREATE PROCEDURE [dbo].[ScriptStandardTarget__GetDistinctStandardTableList]
	@ClientID INT,
	@ScriptID INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT Distinct ObjectName FROM ScriptStandardTarget WITH (NOLOCK) 
	WHERE ScriptID=@ScriptID and ClientID=@ClientID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget__GetDistinctStandardTableList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget__GetDistinctStandardTableList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget__GetDistinctStandardTableList] TO [sp_executeall]
GO
