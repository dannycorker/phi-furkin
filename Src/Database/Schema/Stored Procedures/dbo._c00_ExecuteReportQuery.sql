SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[_c00_ExecuteReportQuery]
(
	@QueryID int,
	@ClientID int,
	@SubClientID int,
	@ClientPersonnelID int,
	@ClientPersonnelAdminGroupID int,
	@PortalUserID int,
	@ParamValues dbo.tvpVarcharVarchar READONLY
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @QUERY_TEXT NVARCHAR(MAX)

	SELECT @QUERY_TEXT = QueryText FROM SqlQuery WITH (NOLOCK) 
	WHERE	QueryID = @QueryID
	
	SELECT @QUERY_TEXT = REPLACE(@QUERY_TEXT, AnyValue1, AnyValue2)
	FROM @ParamValues

	SELECT @QUERY_TEXT = 
	[dbo].[fnSubstituteSqlParams] 
	(
		@QUERY_TEXT, 
		@ClientPersonnelID,
		@ClientID,
		'',
		'',
		'', 
		ISNULL(@SubClientID, 0),
		@PortalUserID 
	)
		
	IF @QUERY_TEXT LIKE '%EXEC%' 
	BEGIN
		SELECT @QUERY_TEXT = @QUERY_TEXT +' '+ AnyValue2 + ','
		FROM @ParamValues
		
		SELECT @QUERY_TEXT = SUBSTRING(@QUERY_TEXT, 0, LEN(@QUERY_TEXT))
		
	END
	
	EXEC sp_executesql @QUERY_TEXT 

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_c00_ExecuteReportQuery] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_c00_ExecuteReportQuery] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_c00_ExecuteReportQuery] TO [sp_executeall]
GO
