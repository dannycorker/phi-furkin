SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-04-14
-- Description:	RPI initial "Add Claim" XML creation
-- 2011-03-21 JWG "Release 1" of the RPI portal makes a couple of changes to this "Stage 1" code.
-- 2011-05-06 JWG Comment out duplicate [DefendantDetails/CompanyDetails] section
-- 2013-08-06 JWG Minor changes for "Release 3" of the claims portal
-- =============================================
CREATE PROCEDURE [dbo].[RPI_GetXML_AddClaim]
	@LeadEventID int /* 9726177, @LeadTypeID int = 837, @MatterID int = 1135864, 11219327, @LeadTypeID int = 837, @MatterID int = 1219272 */
AS
BEGIN
	SET NOCOUNT ON;

	/* 
		Select for XML works like this: 
		
		1) [request/@attribute] comes out as 
		
			<request attribute="Customer: 1234"></request>
			
		
		2) [request/element] comes out as 
		
			<request>
				<element>Jim</element>
			</request>
		
		3) PATH and ROOT work like this:
		
			SELECT 'Value' as [Node/@Attribute]
			FOR XML PATH('X'), ROOT('Y')
			
			<Y>
			  <X>
				<Node Attribute="Value" />
			  </X>
			</Y>
		
		
		Ideally we would have one huge statement SELECT...FOR XML PATH(''), ROOT('MedicalDetails'), TYPE, ELEMENTS XSINIL 
		However, the pivot statement for 240+ fields would be horrendous, and probably impossible for repeating groups (witnesses ect)
		so it has been broken up into several nvarchar blocks instead. We must use nvarchar rather than varchar because
		"FOR XML" uses UTF-16 (2 byte) binary storage, and this doesn't mix well with varchar when SAX validation is used (as it is
		by the CRIF application at the RPI end of this process).
		
		So, use nvarchar throughout and it all works. Many fields have unusual spelling but please don't correct them as RPI have
		specified them like this and if you spell them correctly they will fail validation.
	*/
		
	DECLARE @XML nvarchar(max) = '',
	@XML_ApplicationData nvarchar(400) = '',
	@XML_CACDetails nvarchar(max) = '',
	@XML_MedicalDetails nvarchar(max) = '',
	@XML_Repairs nvarchar(max) = '',
	@XML_AccidentData nvarchar(max) = '',
	@XML_LiabilityFunding nvarchar(max) = '',
	@XML_OtherParties nvarchar(max) = ''
	
	DECLARE	@LiabilityFunding_ICName nvarchar(500) = '',
	@LiabilityFunding_ICAddress nvarchar(500) = '',
	@ClaimData_Signatory nvarchar(100) = 'S',
	@OrganisationID nvarchar(200) = 'CRIFCM',
	@OrganisationPath nvarchar(200) = '/CRIFCM',
	@OrganisationName nvarchar(200) = '',
	@OrganisationType nvarchar(1) = 'I',
	@SendOtherPartyDetails bit = 0,
	@OPCount int = 0,
	@OP1Responsible bit = NULL,
	@OP2Responsible bit = NULL,
	@OP3Responsible bit = NULL,
	@OP4Responsible bit = NULL,
	@OP5Responsible bit = NULL,
	@OP6Responsible bit = NULL,
	@OPLoopResponsible bit = NULL,
	@OPLoopID int = 0,
	@LiabilityFunding_IC_DetailFieldID int = NULL,
	@LiabilityFunding_IC_ColumnFieldID int = NULL

	DECLARE @OtherPartyList TABLE (
		TableRowID int, 
		IsResponsible bit
	)
	
	DECLARE	@ClientID int,
	@CustomerID int,
	@LeadID int,
	@CaseID int,
	@MatterID int,
	@TableRowID int,
	@IsTestCustomer int,
	@WhenCreated dateTime,
	@LeadTypeID int
	
	
	/* Allow us to investigate the XML even if the LeadEvent failed to create */
	/*IF @MatterID IS NOT NULL
	BEGIN
		SELECT TOP 1 
		@LeadID = l.LeadID,
		@LeadTypeID = l.LeadTypeID,
		@CustomerID = l.CustomerID,
		@IsTestCustomer = COALESCE(c.Test, 0), 
		@WhenCreated = l.WhenCreated
		FROM dbo.Matter m 
		INNER JOIN dbo.Lead l ON l.LeadID = m.LeadID 
		INNER JOIN dbo.Customers c ON c.CustomerID = l.CustomerID 
		WHERE m.MatterID = @MatterID 
	END
	ELSE
	BEGIN*/
		SELECT TOP 1 
		@ClientID = l.ClientID,
		@CustomerID = l.CustomerID,
		@LeadID = le.LeadID,
		@MatterID = m.MatterID, 
		@LeadTypeID = l.LeadTypeID,
		@IsTestCustomer = COALESCE(c.Test, 0), 
		@WhenCreated = l.WhenCreated
		FROM dbo.LeadEvent le 
		INNER JOIN dbo.Cases ca ON ca.CaseID = le.CaseID 
		INNER JOIN dbo.Lead l ON l.LeadID = ca.LeadID 
		INNER JOIN dbo.Matter m ON m.CaseID = ca.CaseID 
		INNER JOIN dbo.Customers c ON c.CustomerID = l.CustomerID 
		WHERE le.LeadEventID = @LeadEventID 
	/*END*/
	
	
	/* Exactly 1 ApplicationData section */
	SELECT TOP 1 @ClaimData_Signatory = mdv.DetailValue 
	FROM dbo.RPIField rf WITH (NOLOCK) 
	INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
	INNER JOIN dbo.vMatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = rfm.DetailFieldID AND mdv.MatterID = @MatterID 
	WHERE rf.RPIFieldID = 249
	AND rf.IsEnabled = 1

	/* 2013-08-06 JWG "Release 3" */
	/*SELECT @XML_ApplicationData = '<ApplicationData><ClaimDetails RetainedCopy="1" Signatory="' + CASE WHEN datalength(@ClaimData_Signatory) > 0 THEN SUBSTRING(@ClaimData_Signatory, 1, 1) ELSE 'S' END + '" /></ApplicationData>'*/	/* xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" */
	SELECT @XML_ApplicationData = '<ApplicationData><ClaimDetails RetainedCopy="1" Signatory="' + CASE WHEN datalength(@ClaimData_Signatory) > 0 THEN SUBSTRING(@ClaimData_Signatory, 1, 1) ELSE 'S' END + '" ClaimValue="1" /></ApplicationData>'	/* xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" */
	
	
	/* Exactly 1 Claim-And-Claimant Details section */
	SELECT @OrganisationName = rldv_id.DetailValue
	FROM dbo.RPIField rf WITH (NOLOCK) 
	INNER JOIN dbo.RPIFieldMapping rfm_id WITH (NOLOCK) ON rfm_id.RPIFieldID = rf.RPIFieldID AND rfm_id.LeadTypeID = @LeadTypeID 
	LEFT JOIN dbo.vMatterDetailValuesResource rldv_id WITH (NOLOCK) on rldv_id.MatterID = @MatterID AND rldv_id.DetailFieldID = rfm_id.ColumnDetailFieldID 
	WHERE rf.RPIFieldID = 193
	
	SELECT @OrganisationID = rldv_id.DetailValue
	FROM dbo.RPIField rf WITH (NOLOCK) 
	INNER JOIN dbo.RPIFieldMapping rfm_id WITH (NOLOCK) ON rfm_id.RPIFieldID = rf.RPIFieldID AND rfm_id.LeadTypeID = @LeadTypeID 
	LEFT JOIN dbo.vMatterDetailValuesResource rldv_id WITH (NOLOCK) on rldv_id.MatterID = @MatterID AND rldv_id.DetailFieldID = rfm_id.ColumnDetailFieldID 
	WHERE rf.RPIFieldID = 194
	
	SELECT @OrganisationPath = rldv_path.DetailValue
	FROM dbo.RPIField rf WITH (NOLOCK) 
	INNER JOIN dbo.RPIFieldMapping rfm_path WITH (NOLOCK) ON rfm_path.RPIFieldID = rf.RPIFieldID AND rfm_path.LeadTypeID = @LeadTypeID 
	LEFT JOIN dbo.vMatterDetailValuesResource rldv_path WITH (NOLOCK) on rldv_path.MatterID = @MatterID AND rldv_path.DetailFieldID = rfm_path.ColumnDetailFieldID 
	WHERE rf.RPIFieldID = 195
	
	SELECT @OrganisationType = rldv_id.DetailValue
	FROM dbo.RPIField rf WITH (NOLOCK) 
	INNER JOIN dbo.RPIFieldMapping rfm_id WITH (NOLOCK) ON rfm_id.RPIFieldID = rf.RPIFieldID AND rfm_id.LeadTypeID = @LeadTypeID 
	LEFT JOIN dbo.vMatterDetailValuesResource rldv_id WITH (NOLOCK) on rldv_id.MatterID = @MatterID AND rldv_id.DetailFieldID = rfm_id.ColumnDetailFieldID 
	WHERE rf.RPIFieldID = 196
	
	SELECT @XML_CACDetails = 
	(
		SELECT 
		p.[78] as [ClaimantRepresentative/CompanyDetails/@CompanyName], /*M*/ 
		p.[177] as [ClaimantRepresentative/CompanyDetails/@ContactName], /*M*/
		p.[241] as [ClaimantRepresentative/CompanyDetails/@ContactMiddleName], /*O*/
		p.[178] as [ClaimantRepresentative/CompanyDetails/@ContactSurname], /*M*/
		p.[180] as [ClaimantRepresentative/CompanyDetails/@TelephoneNumber], /*M*/
		p.[242] as [ClaimantRepresentative/CompanyDetails/@EmailAddress], /*O*/
		p.[179] as [ClaimantRepresentative/CompanyDetails/@ReferenceNumber], /*M*/
		COALESCE(p.[181], 'A') as [ClaimantRepresentative/CompanyDetails/Address/@AddressType], /*M*/
		p.[183] as [ClaimantRepresentative/CompanyDetails/Address/@HouseName], 
		p.[197] as [ClaimantRepresentative/CompanyDetails/Address/@HouseNumber], 
		p.[185] as [ClaimantRepresentative/CompanyDetails/Address/@Street1], 
		p.[198] as [ClaimantRepresentative/CompanyDetails/Address/@District], 
		p.[182] as [ClaimantRepresentative/CompanyDetails/Address/@City], 
		p.[199] as [ClaimantRepresentative/CompanyDetails/Address/@County], 
		COALESCE(p.[184], 'UK') as [ClaimantRepresentative/CompanyDetails/Address/@Country], 
		p.[200] as [ClaimantRepresentative/CompanyDetails/Address/@PostCode], 

		CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END as [DefendantDetails/@DefendantStatus], /*M*/ /*Take first letter from Personal, Business*/
		p.[76] as [DefendantDetails/@DefDriverName], /*O*/
		p.[187] as [DefendantDetails/@PolicyNumberReference], /*M*/
		/* 2013-08-06 JWG "Release 3" */
		/*p.[243] as [DefendantDetails/@ReferralSource],*/ /*O*/
		/* 2011-03-21 JWG "Release 1" */
		/*p.[244] as [DefendantDetails/@Comments],*/ 
		NULL as [DefendantDetails/@Comments], /*O*/
		CASE WHEN p.[79] = '' THEN NULL ELSE p.[79] END as [DefendantDetails/@DefedantAge], /*C - MIB only*/
		CASE WHEN p.[80] = '' THEN NULL ELSE p.[80] END as [DefendantDetails/@DefendandDescription], /*C - MIB only*/
		CASE WHEN p.[81] = '' THEN NULL ELSE p.[81] END as [DefendantDetails/@DefendantDetailsObtained], /*C - MIB only*/

		/* 2011-03-21 JWG "Release 1" */
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN p.[83] ELSE NULL END as [DefendantDetails/PersonalDetails/@Name], /*O*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN p.[82] ELSE NULL END as [DefendantDetails/PersonalDetails/@MiddleName], /*O*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN p.[84] ELSE NULL END as [DefendantDetails/PersonalDetails/@Surname], /*C - required if DefendantStatus=Personal*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN datalength(p.[85]) > 0 THEN SUBSTRING(p.[85], 1, 1) ELSE 'N' END ELSE NULL END as [DefendantDetails/PersonalDetails/@Sex], /*C - MIB only*/ /* M, F or N=Not known*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN COALESCE(p.[188], 'A') ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@AddressType], /*O*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[190] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@HouseName],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[201] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@HouseNumber],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[192] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@Street1],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[202] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@District],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[189] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@City],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[203] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@County],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN COALESCE(p.[191], 'UK') ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@Country],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'P' THEN CASE WHEN p.[204] > '' THEN p.[204] ELSE NULL END ELSE NULL END AS [DefendantDetails/PersonalDetails/Address/@PostCode],
/*
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[83] ELSE NULL END as [DefendantDetails/CompanyDetails/@Name], /*C - required if DefendantStatus=Business*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN COALESCE(p.[188], 'A') ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@AddressType], /*O*/
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[190] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@HouseName],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[201] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@HouseNumber],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[192] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@Street1],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[202] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@District],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[189] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@City],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[203] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@County],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN COALESCE(p.[191], 'UK') ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@Country],
		CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN CASE WHEN p.[204] > '' THEN p.[204] ELSE NULL END ELSE NULL END AS [DefendantDetails/CompanyDetails/Address/@PostCode],
*/
		p.[89] as [DefendantDetails/Vehicle/@VRN], /*M*/
		p.[87] as [DefendantDetails/Vehicle/@Make], /*C - MIB Only*/
		p.[88] as [DefendantDetails/Vehicle/@Model], /*C - MIB Only*/
		p.[86] as [DefendantDetails/Vehicle/@Color], /*C - MIB Only*/

		CASE WHEN datalength(@OrganisationType) > 0 THEN SUBSTRING(@OrganisationType, 1, 1) ELSE 'I' END as [DefendantDetails/InsurerInformation/@InsurerType], /*M*/ 
		@OrganisationName as [DefendantDetails/InsurerInformation/@InsurerName], /*M*/
		'1' as [DefendantDetails/InsurerInformation/@Selected], /*M*/
		@OrganisationID as [DefendantDetails/InsurerInformation/@InsurerOrganizationID], /*M*/
		@OrganisationPath as [DefendantDetails/InsurerInformation/@InsurerOrganizationPath], /*M*/

		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[205] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/@CompanyName], /*C - required if DefendantStatus=Business*/
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN COALESCE(p.[206], 'A') ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@AddressType], /*O*/
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[208] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@HouseName],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[211] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@HouseNumber],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[210] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@Street1],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[212] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@District],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[207] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@City],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[213] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@County],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN COALESCE(p.[209], 'UK') ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@Country],
		CASE WHEN p.[214] > '' THEN CASE WHEN CASE WHEN datalength(p.[186]) > 0 THEN SUBSTRING(p.[186], 1, 1) ELSE 'P' END = 'B' THEN p.[214] ELSE NULL END ELSE NULL END as [DefendantDetails/CompanyDetails/Address/@PostCode],
		
		p.[77] as [ClaimantDetails/@Occupation], /*M*/
		dbo.fnBoolFromString(p.[48]) as [ClaimantDetails/@ChildClaim], /*M*/
		CASE WHEN COALESCE(p.[51], '') > '' THEN p.[51] ELSE NULL END as [ClaimantDetails/@NationalInsuranceNumber], /*O*/
		CASE WHEN COALESCE(p.[51], '') > '' THEN NULL ELSE 
			CASE WHEN COALESCE(p.[50], '') > '' THEN p.[50] ELSE 'Not Known' END 
		END as [ClaimantDetails/@NINComment] , /*C - required if NINO not present */
		
		p.[216] as [ClaimantDetails/PersonalDetails/@Name], /*M*/
		p.[247] as [ClaimantDetails/PersonalDetails/@MiddleName], /*O*/
		p.[217] as [ClaimantDetails/PersonalDetails/@Surname], /*M*/
		p.[215] as [ClaimantDetails/PersonalDetails/@DateOfBirth], /*M*/ 
		CASE p.[218] WHEN 'Mr' THEN 1 WHEN 'Mrs' THEN 2 WHEN 'Ms' THEN 3 WHEN 'Miss' THEN 4 ELSE 5 END as [ClaimantDetails/PersonalDetails/@TitleType], /*M*/
		/*248*/CASE p.[218] WHEN 'Mr' THEN NULL WHEN 'Mrs' THEN NULL WHEN 'Ms' THEN NULL WHEN 'Miss' THEN NULL ELSE p.[248] END as [ClaimantDetails/PersonalDetails/@OtherTitle], /*C*/
		COALESCE(p.[219], 'A') as [ClaimantDetails/PersonalDetails/Address/@AddressType], /*M*/
		p.[221] as [ClaimantDetails/PersonalDetails/Address/@HouseName],
		p.[224] as [ClaimantDetails/PersonalDetails/Address/@HouseNumber],
		p.[223] as [ClaimantDetails/PersonalDetails/Address/@Street1],
		p.[225] as [ClaimantDetails/PersonalDetails/Address/@District],
		p.[220] as [ClaimantDetails/PersonalDetails/Address/@City],
		p.[226] as [ClaimantDetails/PersonalDetails/Address/@County],
		COALESCE(p.[222], 'UK') as [ClaimantDetails/PersonalDetails/Address/@Country],
		p.[227] as [ClaimantDetails/PersonalDetails/Address/@PostCode],

		p.[228] as [ClaimantDetails/Vehicle/@VRN] /*O*/

		FROM
		(
			SELECT rfm.RPIFieldID, mdv.DetailValue 
			FROM dbo.RPIField rf WITH (NOLOCK) 
			INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
			INNER JOIN dbo.vMatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = rfm.DetailFieldID AND mdv.MatterID = @MatterID 
			WHERE (rf.RPIFieldID BETWEEN 48 AND 51 OR rf.RPIFieldID BETWEEN 76 AND 89 OR rf.RPIFieldID BETWEEN 177 AND 228 OR rf.RPIFieldID BETWEEN 241 AND 244 OR rf.RPIFieldID BETWEEN 247 AND 248)
			AND (rf.RPIFieldID NOT IN (49))
			AND rf.IsEnabled = 1
		) s
		PIVOT
		(
			MIN(s.DetailValue)
			FOR s.RPIFieldID IN ([48], [50], [51], [76], [77], [78], [79], [80], [81], [82], [83], [84], [85], [86], [87], [88], [89], [177], [178], [179], [180], [181], [182], [183], [184], [185], [186], [187], [188], [189], [190], [191], [192], [197], [198], [199], [200], [201], [202], [203], [204], [205], [206], [207], [208], [209], [210], [211], [212], [213], [214], [215], [216], [217], [218], [219], [220], [221], [222], [223], [224], [225], [226], [227], [228], [241], [242], [243], [244], [247], [248])
		) p
		FOR XML PATH(''), ROOT('ClaimAndClaimantDetails') 
	)
	

	/* Exactly 1 Medical Details section */
	SELECT @XML_MedicalDetails = 
	(
		SELECT 
		dbo.fnBoolFromString(p.[124]) as [Injury/@SoftTissue], /*M*/
		dbo.fnBoolFromString(p.[116]) as [Injury/@BoneInjury], /*M*/
		dbo.fnBoolFromString(p.[128]) as [Injury/@Whiplash], /*M*/
		dbo.fnBoolFromString(p.[121]) as [Injury/@Other], /*M*/
		p.[118] as [Injury/@InjurySustainedDescription], /*M*/
		dbo.fnBoolFromString(p.[127]) as [Injury/@TimeOffRequired], /*M*/
		CASE dbo.fnBoolFromString(p.[127]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[125]) ELSE NULL END as [Injury/@StillOffWork], /*C - required if TimeOffRequired=1*/
		CASE WHEN p.[126] > '' THEN p.[126] ELSE NULL END as [Injury/@TimeOffPeriod], /*C - required if StillOffWork=0 (rather than empty)*/
		dbo.fnBoolFromString(p.[120]) as [Injury/@MedicalAttentionSeeking], /*M*/
		CASE WHEN p.[119] > '' THEN p.[119] ELSE NULL END as [Injury/@MedicalAttentionFirstDate], /*C*/ 
		dbo.fnBoolFromString(p.[117]) as [Injury/@HospitalAttendance], /*M*/
		CASE dbo.fnBoolFromString(p.[117]) WHEN 1 THEN dbo.fnBoolFromString(p.[122]) ELSE NULL END as [Injury/@OvernightDetention], /*C - required if HospitalAttendance=1*/
		CASE WHEN p.[123] > '' THEN p.[123] ELSE NULL END as [Injury/@DaysNumber], /*C - required if OvernightDetention=1*/
		
		CASE p.[229] WHEN 'NHS' THEN 0 WHEN 'Private' THEN 1 ELSE NULL END as [Hospital/@HospitalType], /*M if this section is present*/ 
		CASE WHEN p.[229] > '' THEN p.[230] ELSE NULL END as [Hospital/@HospitalName], /*M if this section is present*/
		CASE WHEN p.[229] > '' THEN p.[235] ELSE NULL END as [Hospital/@PostCode], /*M if this section is present*/
		CASE WHEN p.[229] > '' THEN p.[231] ELSE NULL END as [Hospital/HospitalAddress/@AddressLine1], /*O*/
		CASE WHEN p.[229] > '' THEN p.[232] ELSE NULL END as [Hospital/HospitalAddress/@AddressLine2], /*O*/
		CASE WHEN p.[229] > '' THEN p.[233] ELSE NULL END as [Hospital/HospitalAddress/@AddressLine3], /*O*/
		CASE WHEN p.[229] > '' THEN p.[234] ELSE NULL END as [Hospital/HospitalAddress/@AddressLine4], /*O*/
		
		CASE p.[133] WHEN 'Yes' THEN 0 WHEN 'No' THEN 1 ELSE 3 END as [Rehabilitation/@RehabilitationUndertaken], /*M*/
		p.[132] as [Rehabilitation/@RehabilitationTreatment], /*C - if RehabilitationUndertaken = 0*/
		dbo.fnBoolFromString(p.[131]) as [Rehabilitation/@RehabilitationNeeds], /*C - if RehabilitationUndertaken = 1 or 3*/
		p.[130] as [Rehabilitation/@RehabilitationDetails] /*C - if RehabilitationUndertaken = 1*/
		FROM
		(
			SELECT rfm.RPIFieldID, mdv.DetailValue 
			FROM dbo.RPIField rf WITH (NOLOCK) 
			INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
			INNER JOIN dbo.vMatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = rfm.DetailFieldID AND mdv.MatterID = @MatterID 
			WHERE (rf.RPIFieldID BETWEEN 116 AND 128 OR rf.RPIFieldID BETWEEN 130 AND 133 OR rf.RPIFieldID BETWEEN 229 AND 235) 
			AND rf.IsEnabled = 1
		) s
		PIVOT
		(
			MIN(s.DetailValue)
			FOR s.RPIFieldID IN ([116], [117], [118], [119], [120], [121], [122], [123], [124], [125], [126], [127], [128], [130], [131], [132], [133], [229], [230], [231], [232], [233], [234], [235])
		) p
		FOR XML PATH(''), ROOT('MedicalDetails')  
	)
	
	
	/* Exactly 1 Repairs section */
	SELECT @XML_Repairs = 
	(
		SELECT 
		dbo.fnBoolFromString(p.[65]) as [Repairs/@ClaiimingDamageOwnVehicle], /*M*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[69] WHEN 'Comprehensive' THEN 0 WHEN 'TPFT' THEN 1 WHEN 'TPO' THEN 2 WHEN 'Other' THEN 3 ELSE NULL END ELSE NULL END as [Repairs/@DetailsOfTheInsurance],
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN p.[70] ELSE NULL END as [Repairs/@OtherDetails], /*C - required if DetailsOfTheInsurance=Other*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN dbo.fnBoolFromString(p.[74]) ELSE NULL END as [Repairs/@ThroughClaimantInsurer], /*C - required if ClaiimingDamageOwnVehicle=No*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[73]) ELSE NULL END as [Repairs/@ThroughAlternatieCompany], /*C - required if ThroughClaimantInsurer=No*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[75] WHEN 'Yes' THEN 0 WHEN 'No' THEN 1 WHEN 'Don''t Know' THEN 2 ELSE NULL END ELSE NULL END as [Repairs/@TotalLoss], /*C - required if ClaiimingDamageOwnVehicle=No*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[72] WHEN 'Complete' THEN 0 WHEN 'Authorised' THEN 1 WHEN 'Not yet authorised' THEN 2 WHEN 'Not Known' THEN 3 ELSE NULL END ELSE NULL END as [Repairs/@RepairsPosition], /*C - required if TotalLoss=No*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[67]) ELSE NULL END as [Repairs/@DefendantInsInspection], /*C - required if TotalLoss=No*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN p.[68] ELSE NULL END as [Repairs/@Location], /*C - required if DefendantInsInspection=Yes*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN p.[66] ELSE NULL END as [Repairs/@ContactDetails], /*C - required if DefendantInsInspection=Yes*/ 
		
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[56] WHEN '' THEN NULL ELSE p.[56] END ELSE NULL END as [Repairs/AlternativeCompany/@CompanyName], /*O*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[56] WHEN '' THEN NULL ELSE REPLACE(REPLACE(p.[64], CHAR(10), ', '), CHAR(13), '') END ELSE NULL END as [Repairs/AlternativeCompany/@Address], /*O*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[56] WHEN '' THEN NULL ELSE p.[63] END ELSE NULL END as [Repairs/AlternativeCompany/@TelephoneNumber], /*O*/
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN CASE p.[56] WHEN '' THEN NULL ELSE p.[71] END ELSE NULL END as [Repairs/AlternativeCompany/@ReferenceNumber], /*O*/
		
		CASE dbo.fnBoolFromString(p.[65]) WHEN 1 THEN dbo.fnBoolFromString(p.[55]) ELSE NULL END as [AlternativeVehicleProvision/@ClaimantEntitled], /*M*/
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[53]) ELSE NULL END as [AlternativeVehicleProvision/@AVRequiredByCL], /*C - required if ClaimantEntitled=Yes*/
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[52]) ELSE NULL END as [AlternativeVehicleProvision/@AVProvided], /*C - required if ClaimantEntitled=Yes (guess)*/
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[52]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[58]) ELSE NULL END as [AlternativeVehicleProvision/@HireNeed], /*C - required if AVProvided=Yes*/
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) WHEN 1 THEN dbo.fnBoolFromStringN(p.[54]) ELSE NULL END as [AlternativeVehicleProvision/@AVRequiredByCR], /*M - this means “Do you require the defendant’s insurer to provide your client with an alternative vehicle?” */
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[54]) WHEN 1 THEN p.[258] ELSE NULL END as [AlternativeVehicleProvision/@VehicleType], /*C - required if ClaimantEntitled=Yes, AVRequiredByCR=Yes*/
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[54]) WHEN 1 THEN p.[259] ELSE NULL END as [AlternativeVehicleProvision/@ContactName], /*C - required if ClaimantEntitled=Yes, AVRequiredByCR=Yes*/
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[54]) WHEN 1 THEN p.[260] ELSE NULL END as [AlternativeVehicleProvision/@TelephoneNumber], /*C - required if ClaimantEntitled=Yes, AVRequiredByCR=Yes*/
		
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[52]) WHEN 1 THEN p.[60] ELSE NULL END as [AlternativeVehicleProvision/Provider/@ProviderName],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[52]) WHEN 1 THEN p.[61] ELSE NULL END as [AlternativeVehicleProvision/Provider/@ReferenceNumber],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[52]) WHEN 1 THEN REPLACE(REPLACE(p.[59], CHAR(10), ', '), CHAR(13), '') ELSE NULL END as [AlternativeVehicleProvision/Provider/@ProviderAddress],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[52]) WHEN 1 THEN p.[62] ELSE NULL END as [AlternativeVehicleProvision/Provider/@StartDate], /*M*/ 
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[55]) & dbo.fnBoolFromStringN(p.[52]) WHEN 1 THEN p.[57] ELSE NULL END as [AlternativeVehicleProvision/Provider/@EndDate], /*M*/ 
		
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[52]) WHEN 1 THEN p.[176] ELSE NULL END as [AlternativeVehicleProvision/Provider/Vehicle/@VRN],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[52]) WHEN 1 THEN p.[172] ELSE NULL END as [AlternativeVehicleProvision/Provider/Vehicle/@Color],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[52]) WHEN 1 THEN p.[173] ELSE NULL END as [AlternativeVehicleProvision/Provider/Vehicle/@EngineSize],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[52]) WHEN 1 THEN p.[174] ELSE NULL END as [AlternativeVehicleProvision/Provider/Vehicle/@Make],
		CASE dbo.fnBoolFromString(p.[65]) & dbo.fnBoolFromString(p.[52]) WHEN 1 THEN p.[175] ELSE NULL END as [AlternativeVehicleProvision/Provider/Vehicle/@Model]
		
		FROM
		(
			SELECT rfm.RPIFieldID, mdv.DetailValue 
			FROM dbo.RPIField rf WITH (NOLOCK) 
			INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
			INNER JOIN dbo.vMatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = rfm.DetailFieldID AND mdv.MatterID = @MatterID 
			WHERE (rf.RPIFieldID BETWEEN 52 AND 76 OR rf.RPIFieldID BETWEEN 172 AND 176 OR rf.RPIFieldID BETWEEN 258 AND 260) 
			AND (rf.RPIFieldID NOT IN (76))
			AND rf.IsEnabled = 1
		) s
		PIVOT
		(
			MIN(s.DetailValue)
			FOR s.RPIFieldID IN ([52], [53], [54], [55], [56], [57], [58], [59], [60], [61], [62], [63], [64], [65], [66], [67], [68], [69], [70], [71], [72], [73], [74], [75], [172], [173], [174], [175], [176], [258], [259], [260])
		) p
		FOR XML PATH(''), ROOT('RepairsAndAlternativeVehicleProvision')  
	)
	
	
	
	/* Exactly 1 AccidentData section */
	SELECT @XML_AccidentData = 
	(
		SELECT 
		CASE p.[17] 
			WHEN 'The Driver' THEN 0 
			WHEN 'The owner of the vehicle but not driving' THEN 1 
			WHEN 'A passenger in a vehicle owned by someone else' THEN 2 
			WHEN 'a passenger in or on a vehicle owned by someone else' THEN 2 
			WHEN 'A pedestrian' THEN 3
			WHEN 'A cyclist' THEN 4
			WHEN 'A motorcyclist' THEN 5
		ELSE 6 END as [AccidentDetails/@ClaimantType], /*M*/
		p.[26] as [AccidentDetails/@OtherType], /*C - required if ClaimantType=6*/
		p.[23] as [AccidentDetails/@OccupantsNumber], /*C - required if ClaimantType=0,1,2*/
		/* 2011-03-21 JWG "Release 1" */
		/*dbo.fnBoolFromString(p.[39]) as [AccidentDetails/@Seatbelt], */
		ISNULL(dbo.fnBoolFromStringN(p.[39]), 2) as [AccidentDetails/@Seatbelt], /*C - required if ClaimantType=0,1,2*/
		dbo.fnBoolFromString(p.[240]) as [AccidentDetails/@DriverIsDefendant], /*C - required if ClaimantType=2*/
		p.[3]  as [AccidentDetails/@AccidentDate], /*M*/ 
		p.[6]  as [AccidentDetails/@AccidentTime], /*M*/
		p.[5]  as [AccidentDetails/@AccidentLocation], /*M*/
		p.[4]  as [AccidentDetails/@AccidentDescription], /*M*/
		/* 2011-03-21 JWG "Release 1" */
		/*dbo.fnBoolFromString(p.[27]) as [AccidentDetails/@PoliceReported], */
		ISNULL(dbo.fnBoolFromStringN(p.[27]), 2) as [AccidentDetails/@PoliceReported], /*M*/
		
		CASE WHEN p.[240] LIKE 'N%' AND p.[19] > '' THEN p.[19] ELSE NULL END as [AccidentDetails/Driver/@Name], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[170] > '' THEN p.[170] ELSE NULL END as [AccidentDetails/Driver/@MiddleName], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[20] > '' THEN p.[20] ELSE NULL END as [AccidentDetails/Driver/@Surname], /*C - required if DriverIsDefendant=No*/ 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN COALESCE(p.[134], 'A') ELSE NULL END as [AccidentDetails/Driver/Address/@AddressType], /*C - required if DriverIsDefendant=No*/ 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[135] ELSE NULL END as [AccidentDetails/Driver/Address/@HouseName], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[136] ELSE NULL END as [AccidentDetails/Driver/Address/@HouseNumber], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[137] ELSE NULL END as [AccidentDetails/Driver/Address/@Street1], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[138] ELSE NULL END as [AccidentDetails/Driver/Address/@District], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[139] ELSE NULL END as [AccidentDetails/Driver/Address/@City], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[140] ELSE NULL END as [AccidentDetails/Driver/Address/@County], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN COALESCE(p.[141], 'UK') ELSE NULL END as [AccidentDetails/Driver/Address/@Country], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[142] > '' THEN p.[142] ELSE NULL END as [AccidentDetails/Driver/Address/@PostCode], 
		
		CASE WHEN p.[240] LIKE 'N%' AND p.[250] > '' THEN p.[250] ELSE NULL END as [AccidentDetails/Owner/@Name], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[251] > '' THEN p.[251] ELSE NULL END as [AccidentDetails/Owner/@MiddleName], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[252] > '' THEN p.[252] ELSE NULL END as [AccidentDetails/Owner/@Surname], /*C - required if DriverIsDefendant=No*/ 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN COALESCE(p.[143], 'A') ELSE NULL END as [AccidentDetails/Owner/Address/@AddressType], /*C - required if DriverIsDefendant=No*/ 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[144] ELSE NULL END as [AccidentDetails/Owner/Address/@HouseName], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[145] ELSE NULL END as [AccidentDetails/Owner/Address/@HouseNumber], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[146] ELSE NULL END as [AccidentDetails/Owner/Address/@Street1], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[147] ELSE NULL END as [AccidentDetails/Owner/Address/@District], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[148] ELSE NULL END as [AccidentDetails/Owner/Address/@City], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[149] ELSE NULL END as [AccidentDetails/Owner/Address/@County], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN COALESCE(p.[150], 'UK') ELSE NULL END as [AccidentDetails/Owner/Address/@Country], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[151] > '' THEN p.[151] ELSE NULL END as [AccidentDetails/Owner/Address/@PostCode], 
		
		CASE WHEN p.[240] LIKE 'N%' AND p.[253] > '' THEN p.[253] ELSE NULL END as [AccidentDetails/Vehicle/@Make], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[254] > '' THEN p.[254] ELSE NULL END as [AccidentDetails/Vehicle/@Model], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[255] > '' THEN p.[255] ELSE NULL END as [AccidentDetails/Vehicle/@VRN], /*C - required if DriverIsDefendant=No*/ 
		
		CASE WHEN p.[240] LIKE 'N%' AND p.[256] > '' THEN p.[256] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/@CompanyName], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[257] > '' THEN p.[257] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/@PolicyNumber], /*C - required if DriverIsDefendant=No*/
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN COALESCE(p.[161], 'A') ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@AddressType], /*C - required if DriverIsDefendant=No*/ 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[162] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@HouseName], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[163] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@HouseNumber], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[164] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@Street1], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[165] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@District], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[166] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@City], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[167] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@County], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN COALESCE(p.[168], 'UK') ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@Country], 
		CASE WHEN p.[240] LIKE 'N%' AND p.[169] > '' THEN p.[169] ELSE NULL END as [AccidentDetails/InsuranceCompanyInformatiion/Address/@PostCode], 
		
		dbo.fnBoolFromStringN(p.[43]) as [AccidentDetails/WeatherConditions/@Sun],
		dbo.fnBoolFromStringN(p.[28]) as [AccidentDetails/WeatherConditions/@Rain],
		dbo.fnBoolFromStringN(p.[40]) as [AccidentDetails/WeatherConditions/@Snow],
		dbo.fnBoolFromStringN(p.[22]) as [AccidentDetails/WeatherConditions/@Ice],
		dbo.fnBoolFromStringN(p.[21]) as [AccidentDetails/WeatherConditions/@Fog],
		dbo.fnBoolFromStringN(p.[24]) as [AccidentDetails/WeatherConditions/@Other],
		p.[25] as [AccidentDetails/WeatherConditions/@OtherDetails], /*C*/
		
		dbo.fnBoolFromStringN(p.[29]) as [AccidentDetails/RoadConditions/@Dry],
		dbo.fnBoolFromStringN(p.[36]) as [AccidentDetails/RoadConditions/@Wet],
		dbo.fnBoolFromStringN(p.[35]) as [AccidentDetails/RoadConditions/@Snow],
		dbo.fnBoolFromStringN(p.[30]) as [AccidentDetails/RoadConditions/@Ice],
		dbo.fnBoolFromStringN(p.[31]) as [AccidentDetails/RoadConditions/@Mud],
		dbo.fnBoolFromStringN(p.[32]) as [AccidentDetails/RoadConditions/@Oil],
		dbo.fnBoolFromStringN(p.[33]) as [AccidentDetails/RoadConditions/@Other],
		p.[34] as [AccidentDetails/RoadConditions/@OtherDetails], /*C*/
		
		dbo.fnBoolFromStringN(p.[45]) as [AccidentDetails/AccidentCircumstances/@VhclHitSideRoad],
		dbo.fnBoolFromStringN(p.[44]) as [AccidentDetails/AccidentCircumstances/@VhclHitInRear],
		dbo.fnBoolFromStringN(p.[46]) as [AccidentDetails/AccidentCircumstances/@VhclHitWhilstParked],
		dbo.fnBoolFromStringN(p.[1])  as [AccidentDetails/AccidentCircumstances/@AccidCarPark],
		dbo.fnBoolFromStringN(p.[7])  as [AccidentDetails/AccidentCircumstances/@AccidRoundabout],
		dbo.fnBoolFromStringN(p.[2])  as [AccidentDetails/AccidentCircumstances/@AccidChangingLanes],
		dbo.fnBoolFromStringN(p.[18]) as [AccidentDetails/AccidentCircumstances/@ConcertinaCollision],
		dbo.fnBoolFromStringN(p.[8])  as [AccidentDetails/AccidentCircumstances/@Other],

		CASE dbo.fnBoolFromString(p.[27]) WHEN 1 THEN p.[42] ELSE NULL END as [AccidentDetails/PoliceDetails/@StationName],
		CASE dbo.fnBoolFromString(p.[27]) WHEN 1 THEN REPLACE(REPLACE(p.[41], CHAR(10), ', '), CHAR(13), '') ELSE NULL END as [AccidentDetails/PoliceDetails/@StationAddress],
		CASE dbo.fnBoolFromString(p.[27]) WHEN 1 THEN p.[38] ELSE NULL END as [AccidentDetails/PoliceDetails/@ReportingOfficerName],
		CASE dbo.fnBoolFromString(p.[27]) WHEN 1 THEN p.[37] ELSE NULL END as [AccidentDetails/PoliceDetails/@ReferenceNumber],

		dbo.fnBoolFromString(p.[16]) as [BusCoach/@BussOrCoach],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN p.[11] ELSE NULL END as [BusCoach/@DriverName],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN p.[10] ELSE NULL END as [BusCoach/@DriverID],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN p.[9] ELSE NULL END  as [BusCoach/@DriverDescription],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN p.[15] ELSE NULL END as [BusCoach/@VehicleDescription],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN p.[13] ELSE NULL END as [BusCoach/@NumberOfPassengers],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN dbo.fnBoolFromString(p.[12]) ELSE NULL END as [BusCoach/@Evidence],
		CASE dbo.fnBoolFromString(p.[16]) WHEN 1 THEN p.[14] ELSE NULL END as [BusCoach/@Comments] 
		
		FROM
		(
			SELECT rfm.RPIFieldID, mdv.DetailValue 
			FROM dbo.RPIField rf WITH (NOLOCK) 
			INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
			INNER JOIN dbo.vMatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = rfm.DetailFieldID AND mdv.MatterID = @MatterID 
			WHERE (rf.RPIFieldID BETWEEN 1 AND 46 OR rf.RPIFieldID BETWEEN 134 AND 170 OR rf.RPIFieldID BETWEEN 250 AND 257 OR rf.RPIFieldID IN (240)) 
			AND rf.IsEnabled = 1
		) s
		PIVOT
		(
			MIN(s.DetailValue)
			FOR s.RPIFieldID IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21], [22], [23], [24], [25], [26], [27], [28], [29], [30], [31], [32], [33], [34], [35], [36], [37], [38], [39], [40], [41], [42], [43], [44], [45], [46], [134], [135], [136], [137], [138], [139], [140], [141], [142], [143], [144], [145], [146], [147], [148], [149], [150], [151], [152], [153], [154], [155], [156], [157], [158], [159], [160], [161], [162], [163], [164], [165], [166], [167], [168], [169], [170], [240], [250], [251], [252], [253], [254], [255], [256], [257])
		) p
		FOR XML PATH(''), ROOT('AccidentData')  
	)
	
	
	/* As many Other Party records as required */
	SELECT TOP 1 @SendOtherPartyDetails = dbo.fnBoolFromString(mdv_send.DetailValue) 
	FROM dbo.RPIField rf WITH (NOLOCK) 
	INNER JOIN dbo.RPIFieldMapping rfm_send WITH (NOLOCK) ON rfm_send.RPIFieldID = rf.RPIFieldID AND rfm_send.LeadTypeID = @LeadTypeID 
	INNER JOIN dbo.vMatterDetailValues mdv_send WITH (NOLOCK) on mdv_send.MatterID = @MatterID AND mdv_send.DetailFieldID = rfm_send.DetailFieldID 
	WHERE rf.RPIFieldID = 292 
	
	IF @SendOtherPartyDetails = 1 
	BEGIN

		/* 
			Populate a table variable with the rows required, and the responsibility flags which 
			are required later on in the Liability section.
			Limited to 6 records (by CRIF).
		*/
		INSERT @OtherPartyList (TableRowID, IsResponsible) 
		SELECT TOP (6) tdv.TableRowID, dbo.fnBoolFromString(CASE df.QuestionTypeID WHEN 4 THEN luli.ItemValue ELSE tdv.DetailValue END) 
		FROM dbo.RPIField rf WITH (NOLOCK) 
		INNER JOIN dbo.RPIFieldMapping rfm_ir WITH (NOLOCK) ON rfm_ir.RPIFieldID = rf.RPIFieldID AND rfm_ir.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.TableRows tr_ir WITH (NOLOCK) on tr_ir.DetailFieldID = rfm_ir.DetailFieldID AND tr_ir.MatterID = @MatterID
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr_ir.TableRowID AND tdv.DetailFieldID = rfm_ir.ColumnDetailFieldID
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID 
		LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv.ValueInt 
		WHERE rf.RPIFieldID = 129 
		ORDER BY tdv.TableRowID 

		SELECT @OPCount = @@ROWCOUNT
		
		/* Loop round this section once per table row */
		WHILE @OPCount > 0
		BEGIN
			
			/* 
				Process one TableRow at a time, setting the @OP_Responsible flags as we go
			*/
			SELECT TOP (1) 
			@TableRowID = opl.TableRowID, 
			@OPLoopResponsible = opl.IsResponsible, 
			@OPLoopID += 1, 
			@OPCount -= 1 
			FROM @OtherPartyList opl 
			ORDER BY opl.TableRowID  
			
			IF @OPLoopID = 1
				SET @OP1Responsible = @OPLoopResponsible
			ELSE IF @OPLoopID = 2
				SET @OP2Responsible = @OPLoopResponsible
			ELSE IF @OPLoopID = 3
				SET @OP3Responsible = @OPLoopResponsible
			ELSE IF @OPLoopID = 4
				SET @OP4Responsible = @OPLoopResponsible
			ELSE IF @OPLoopID = 5
				SET @OP5Responsible = @OPLoopResponsible
			ELSE IF @OPLoopID = 6
				SET @OP6Responsible = @OPLoopResponsible
			
			SELECT @XML_OtherParties += 
			(
				SELECT 
				CASE WHEN datalength(p.[261]) > 0 THEN SUBSTRING(p.[261], 1, 1) ELSE 'W' END as [OtherParty/@OPType],
				CASE WHEN datalength(p.[261]) > 0 THEN CASE SUBSTRING(p.[261], 1, 1) WHEN 'O' THEN p.[262] ELSE NULL END ELSE NULL END as [OtherParty/@OPOther],
				
				p.[263] as [OtherParty/PersonalDetails/@Name],
				p.[264] as [OtherParty/PersonalDetails/@MiddleName],
				p.[265] as [OtherParty/PersonalDetails/@Surname],
				CASE p.[266] WHEN 'Mr' THEN 1 WHEN 'Mrs' THEN 2 WHEN 'Ms' THEN 3 WHEN 'Miss' THEN 4 ELSE 5 END as [OtherParty/PersonalDetails/@TitleType],
				/*267*/CASE p.[266] WHEN 'Mr' THEN NULL WHEN 'Mrs' THEN NULL WHEN 'Ms' THEN NULL WHEN 'Miss' THEN NULL ELSE p.[266] END as [OtherParty/PersonalDetails/@OtherTitle],
				CASE WHEN p.[276] > '' THEN COALESCE(p.[268], 'A') ELSE NULL END as [OtherParty/PersonalDetails/Address/@AddressType], 
				CASE WHEN p.[276] > '' THEN p.[269] ELSE NULL END as [OtherParty/PersonalDetails/Address/@HouseName], 
				CASE WHEN p.[276] > '' THEN p.[270] ELSE NULL END as [OtherParty/PersonalDetails/Address/@HouseNumber], 
				CASE WHEN p.[276] > '' THEN p.[271] ELSE NULL END as [OtherParty/PersonalDetails/Address/@Street1], 
				CASE WHEN p.[276] > '' THEN p.[272] ELSE NULL END as [OtherParty/PersonalDetails/Address/@District], 
				CASE WHEN p.[276] > '' THEN p.[273] ELSE NULL END as [OtherParty/PersonalDetails/Address/@City], 
				CASE WHEN p.[276] > '' THEN p.[274] ELSE NULL END as [OtherParty/PersonalDetails/Address/@County], 
				CASE WHEN p.[276] > '' THEN COALESCE(p.[275], 'UK') ELSE NULL END as [OtherParty/PersonalDetails/Address/@Country], 
				CASE WHEN p.[276] > '' THEN p.[276] ELSE NULL END as [OtherParty/PersonalDetails/Address/@PostCode], 
				
				p.[277] as [OtherParty/VihicleInformation/@VRN],
				p.[278] as [OtherParty/VihicleInformation/@Make],
				p.[279]as [OtherParty/VihicleInformation/@Model],
				p.[280] as [OtherParty/VihicleInformation/@Color],
				
				p.[281] as [OtherParty/InsuranceCompanyInformatiion/@CompanyName],
				p.[282] as [OtherParty/InsuranceCompanyInformatiion/@PolicyNumber],
				CASE WHEN p.[291] > '' THEN COALESCE(p.[283], 'A') ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@AddressType], 
				CASE WHEN p.[291] > '' THEN p.[284] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@HouseName], 
				CASE WHEN p.[291] > '' THEN p.[285] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@HouseNumber], 
				CASE WHEN p.[291] > '' THEN p.[286] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@Street1], 
				CASE WHEN p.[291] > '' THEN p.[287] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@District], 
				CASE WHEN p.[291] > '' THEN p.[288] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@City], 
				CASE WHEN p.[291] > '' THEN p.[289] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@County], 
				CASE WHEN p.[291] > '' THEN COALESCE(p.[290], 'UK') ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@Country], 
				CASE WHEN p.[291] > '' THEN p.[291] ELSE NULL END as [OtherParty/InsuranceCompanyInformatiion/Address/@PostCode] 
				
				FROM
				(
					SELECT rfm.RPIFieldID, CASE df.QuestionTypeID WHEN 4 THEN luli.ItemValue ELSE tdv.DetailValue END AS [DetailValue] 
					FROM dbo.RPIField rf WITH (NOLOCK) 
					INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
					INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = rfm.ColumnDetailFieldID AND tdv.MatterID = @MatterID AND tdv.TableRowID = @TableRowID 
					INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID 
					LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv.ValueInt 
					WHERE (rf.RPIFieldID BETWEEN 261 AND 291)
					AND rf.IsEnabled = 1
				) s
				PIVOT
				(
					MIN(s.DetailValue)
					FOR s.RPIFieldID IN ([261], [262], [263], [264], [265], [266], [267], [268], [269], [270], [271], [272], [273], [274], [275], [276], [277], [278], [279], [280], [281], [282], [283], [284], [285], [286], [287], [288], [289], [290], [291])
				) p
				FOR XML PATH(''), ROOT('OtherPartyDetails')  

			)
			
			/* Remove this one from the list */
			DELETE @OtherPartyList 
			WHERE TableRowID = @TableRowID
			
		END
	END
	
	IF @XML_OtherParties <> ''
	BEGIN
		SELECT @XML_OtherParties = REPLACE(@XML_OtherParties, '</OtherPartyDetails><OtherPartyDetails>', '')
	END
	
	
	/* Exactly 1 LiabilityFunding section */
	
	/* JWG 2012-07-13 Relax the requirement for a resource list here. Allow free text field for ATE insurer name. */
	SELECT @LiabilityFunding_IC_DetailFieldID = rfm.DetailFieldID,
	@LiabilityFunding_IC_ColumnFieldID = rfm.ColumnDetailFieldID
	FROM dbo.RPIFieldMapping rfm WITH (NOLOCK) 
	WHERE rfm.RPIFieldID = 96 
	AND rfm.LeadTypeID = @LeadTypeID 
	
	IF @LiabilityFunding_IC_DetailFieldID IS NOT NULL
	BEGIN
		IF @LiabilityFunding_IC_ColumnFieldID IS NOT NULL
		BEGIN
			/* Resource List */
			SELECT @LiabilityFunding_ICName = rldv_name.DetailValue
			FROM dbo.vMatterDetailValuesResource rldv_name WITH (NOLOCK) 
			WHERE rldv_name.MatterID = @MatterID AND rldv_name.DetailFieldID = @LiabilityFunding_IC_ColumnFieldID 
		END
		ELSE
		BEGIN
			/* Other field types */
			SELECT @LiabilityFunding_ICName = mdv_name.DetailValue
			FROM dbo.vMatterDetailValues mdv_name WITH (NOLOCK) 
			WHERE mdv_name.MatterID = @MatterID 
			AND mdv_name.DetailFieldID = @LiabilityFunding_IC_DetailFieldID 
		END
	END
	
	/* JWG 2012-07-13 Ditto for insurer address. This does not have to come from a resource list now. */
	SELECT @LiabilityFunding_IC_DetailFieldID = rfm.DetailFieldID,
	@LiabilityFunding_IC_ColumnFieldID = rfm.ColumnDetailFieldID
	FROM dbo.RPIFieldMapping rfm WITH (NOLOCK) 
	WHERE rfm.RPIFieldID = 171 
	AND rfm.LeadTypeID = @LeadTypeID 
	
	IF @LiabilityFunding_IC_DetailFieldID IS NOT NULL
	BEGIN
		IF @LiabilityFunding_IC_ColumnFieldID IS NOT NULL
		BEGIN
			/* Resource List */
			SELECT @LiabilityFunding_ICAddress = rldv_address.DetailValue
			FROM dbo.vMatterDetailValuesResource rldv_address WITH (NOLOCK) 
			WHERE rldv_address.MatterID = @MatterID AND rldv_address.DetailFieldID = @LiabilityFunding_IC_ColumnFieldID 
		END
		ELSE
		BEGIN
			/* Other field types */
			SELECT @LiabilityFunding_ICAddress = mdv_address.DetailValue
			FROM dbo.vMatterDetailValues mdv_address WITH (NOLOCK) 
			WHERE mdv_address.MatterID = @MatterID 
			AND mdv_address.DetailFieldID = @LiabilityFunding_IC_DetailFieldID 
		END
	END	
	SELECT @LiabilityFunding_ICAddress = REPLACE(REPLACE(@LiabilityFunding_ICAddress, CHAR(10), ', '), CHAR(13), '') 
	
	SELECT @XML_LiabilityFunding = 
	(
		SELECT 
		p.[109] as [Liability/@DefendantResponsability], /*M*/
		CASE WHEN @OP1Responsible IS NULL THEN NULL ELSE @OP1Responsible END as [Liability/@OP1Responsability], /*C*/
		CASE WHEN @OP2Responsible IS NULL THEN NULL ELSE @OP2Responsible END as [Liability/@OP2Responsability], /*C*/
		CASE WHEN @OP3Responsible IS NULL THEN NULL ELSE @OP3Responsible END as [Liability/@OP3Responsability], /*C*/
		CASE WHEN @OP4Responsible IS NULL THEN NULL ELSE @OP4Responsible END as [Liability/@OP4Responsability], /*C*/
		CASE WHEN @OP5Responsible IS NULL THEN NULL ELSE @OP5Responsible END as [Liability/@OP5Responsability], /*C*/
		CASE WHEN @OP6Responsible IS NULL THEN NULL ELSE @OP6Responsible END as [Liability/@OP6Responsability], /*C*/
		dbo.fnBoolFromString(p.[94]) as [Funding/@FundingUndertaken], /*M*/
		CASE dbo.fnBoolFromString(p.[94]) WHEN 1 THEN dbo.fnBoolFromString(p.[106]) ELSE NULL END as [Funding/@Section58], /* C - FundingUndertaken=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[106]) WHEN 1 THEN p.[92] ELSE NULL END as [Funding/@ConditionalFeeDate], /* C - FundingUndertaken=1, Section58=1 */
		CASE dbo.fnBoolFromString(p.[94]) WHEN 1 THEN dbo.fnBoolFromString(p.[105]) ELSE NULL END as [Funding/@Section29], /* C - FundingUndertaken=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN COALESCE(@LiabilityFunding_ICName, '') ELSE NULL END as [Funding/@ICName], /* C - FundingUndertaken=1, Section29=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN COALESCE(@LiabilityFunding_ICAddress, '') ELSE NULL END as [Funding/@ICAddress], /* C - FundingUndertaken=1, Section29=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN p.[103] ELSE NULL END as [Funding/@PolicyNumber], /* C - FundingUndertaken=1, Section29=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN p.[102] ELSE NULL END as [Funding/@PolicyDate], /* C - FundingUndertaken=1, Section29=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN p.[97] ELSE NULL END as [Funding/@LevelOfCover], /* C - FundingUndertaken=1, Section29=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN dbo.fnBoolFromString(p.[104]) ELSE NULL END as [Funding/@PremiumsStaged], /* C - FundingUndertaken=1, Section29=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[105]) WHEN 1 THEN p.[95] ELSE NULL END as [Funding/@IncreasingPoint], /* C - FundingUndertaken=1, Section29=1, PremiumsStaged=1 */
		CASE dbo.fnBoolFromString(p.[94]) WHEN 1 THEN dbo.fnBoolFromString(p.[98]) ELSE NULL END as [Funding/@MembershipOrganisation], /* C - FundingUndertaken=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[98]) WHEN 1 THEN p.[99] ELSE NULL END as [Funding/@OrganizationName], /* C - FundingUndertaken=1, MembershipOrganisation=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[98]) WHEN 1 THEN p.[90] ELSE NULL END as [Funding/@AgreementDate], /* C - FundingUndertaken=1, MembershipOrganisation=1 */
		CASE dbo.fnBoolFromString(p.[94]) WHEN 1 THEN dbo.fnBoolFromString(p.[100]) ELSE NULL END as [Funding/@Ohter], /* C - FundingUndertaken=1 */
		CASE dbo.fnBoolFromString(p.[94]) & dbo.fnBoolFromString(p.[100]) WHEN 1 THEN p.[101] ELSE NULL END as [Funding/@OtherDetails], /* C - FundingUndertaken=1, Ohter=1 */
		CASE dbo.fnBoolFromString(p.[94]) WHEN 1 THEN CASE WHEN @OrganisationType LIKE 'M%' THEN dbo.fnBoolFromString(p.[93]) ELSE NULL END ELSE NULL END as [Funding/@ConsideredFreeLegalExpIns], /* C - FundingUndertaken=1, InsurerType=M */
		CASE dbo.fnBoolFromString(p.[94]) WHEN 1 THEN p.[91] ELSE NULL END as [Funding/@Comments] /* C - FundingUndertaken=1 */
		FROM
		(
			SELECT rfm.RPIFieldID, mdv.DetailValue 
			FROM dbo.RPIField rf WITH (NOLOCK) 
			INNER JOIN dbo.RPIFieldMapping rfm WITH (NOLOCK) ON rfm.RPIFieldID = rf.RPIFieldID AND rfm.LeadTypeID = @LeadTypeID 
			INNER JOIN dbo.vMatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = rfm.DetailFieldID AND mdv.MatterID = @MatterID 
			WHERE (rf.RPIFieldID BETWEEN 90 AND 106 OR rf.RPIFieldID IN (109)) 
			AND rf.RPIFieldID <> 96 
			AND rf.IsEnabled = 1 
		) s
		PIVOT
		(
			MIN(s.DetailValue)
			FOR s.RPIFieldID IN ([90], [91], [92], [93], [94], [95], [97], [98], [99], [100], [101], [102], [103], [104], [105], [106], [109])
		) p
		FOR XML PATH(''), ROOT('LiabilityFunding')  
	)
	
	
	/* Combine all the elements together. InterResolve server doesn't output nvarchar/XML as UTF-16 so exclude that element for them. */
	SELECT @XML = CASE @ClientID WHEN 74 THEN '' ELSE '<?xml version="1.0" encoding="UTF-16"?>' END + '<DocumentInput>' + @XML_ApplicationData + @XML_CACDetails + @XML_MedicalDetails + @XML_Repairs + @XML_AccidentData + @XML_OtherParties + @XML_LiabilityFunding + '</DocumentInput>'
	
    /* Do not select anything at all if there are no records today. */
	IF @XML IS NULL
	BEGIN
		SELECT NULL as [XML] 
		WHERE 0 = 1
	END
	ELSE
	BEGIN
		
		/* 
			Cast as XML ready for the app to transmit 
		*/
		IF @ClientID = 74
		BEGIN
			SELECT @XML as [XML]
		END
		ELSE
		BEGIN
			SELECT cast(@XML as XML) as [XML]
		END
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RPI_GetXML_AddClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPI_GetXML_AddClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPI_GetXML_AddClaim] TO [sp_executeall]
GO
