SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Patch table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Patch_GetByPatchID]
(

	@PatchID int   
)
AS


				SELECT
					[PatchID],
					[PatchDate],
					[PatchVersion]
				FROM
					[dbo].[Patch] WITH (NOLOCK) 
				WHERE
										[PatchID] = @PatchID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_GetByPatchID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Patch_GetByPatchID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_GetByPatchID] TO [sp_executeall]
GO
