SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetAllClientQuestionnaires]

AS

Select ClientQuestionnaireID, ClientID, QuestionnaireTitle, QuestionnaireDescription, QuestionsPerPage, QuestionnaireLogo, QuestionnaireLogoFileName, QuestionnaireIntroductionText, HideIntro, QuestionnaireFooterText, HideFooter, Published, LinkedQuestionnaireClientQuestionnaireID, QuestionnaireFooterIframe, QuestionnaireHeaderIframe, QuestionnaireFooterInternal, QuestionnaireHeaderInternal, QuestionnaireFooterIframeHeight, QuestionnaireHeaderIframeHeight, QuestionnaireFooterIframeWidth, QuestionnaireHeaderIframeWidth, FrameMode, LayoutCss, LayoutCssFileName, ImportDirectlyIntoLeadManager, RunAsClientPersonnelID, RememberAnswers
From ClientQuestionnaires




GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllClientQuestionnaires] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAllClientQuestionnaires] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllClientQuestionnaires] TO [sp_executeall]
GO
