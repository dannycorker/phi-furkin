SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionTypes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionTypes_GetByQuestionTypeID]
(

	@QuestionTypeID int   
)
AS


				SELECT
					[QuestionTypeID],
					[Name],
					[Description],
					[Multiselect],
					[EcatcherField]
				FROM
					[dbo].[QuestionTypes] WITH (NOLOCK) 
				WHERE
										[QuestionTypeID] = @QuestionTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_GetByQuestionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionTypes_GetByQuestionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_GetByQuestionTypeID] TO [sp_executeall]
GO
