SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClassNode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_Insert]
(

	@ClassNodeID int    OUTPUT,

	@ClientID int   ,

	@ParentClassNodeID int   ,

	@NodeType int   ,

	@Name varchar (255)  ,

	@ClassDescription varchar (255)  ,

	@NodeOrder int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Deleted bit   
)
AS


				
				INSERT INTO [dbo].[ClassNode]
					(
					[ClientID]
					,[ParentClassNodeID]
					,[NodeType]
					,[Name]
					,[ClassDescription]
					,[NodeOrder]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[Deleted]
					)
				VALUES
					(
					@ClientID
					,@ParentClassNodeID
					,@NodeType
					,@Name
					,@ClassDescription
					,@NodeOrder
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@Deleted
					)
				-- Get the identity value
				SET @ClassNodeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Insert] TO [sp_executeall]
GO
