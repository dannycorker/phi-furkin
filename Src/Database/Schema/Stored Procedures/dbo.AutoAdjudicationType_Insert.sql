SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AutoAdjudicationType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_Insert]
(

	@AutoAdjudicationTypeID int    OUTPUT,

	@AutoAdjudicationTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[AutoAdjudicationType]
					(
					[AutoAdjudicationTypeName]
					)
				VALUES
					(
					@AutoAdjudicationTypeName
					)
				-- Get the identity value
				SET @AutoAdjudicationTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Insert] TO [sp_executeall]
GO
