SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================		
-- Author:	Simon Brushett	
-- Create date: 2016-11-09		
-- Description:	Gets a list of campaign codes for the specified brands 		
-- 2016-11-15 IS 3-tier campaign codes
-- 2016-11-16 SB Fix
-- 2018-04-04 GPR removed MI1/MI2/ from select for C600 Defect 292
-- 2018-04-25 GPR added ValidatedDisplayText for C600 EQB
-- 2019-12-12 GPR removed ValidatedDisplayText for 603 and removed many legacy joins
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Search_CampaignCodes]		
(		
	@ClientId INT,
	@BrandIds dbo.tvpInt READONLY
)		
AS		
BEGIN		
	
	DECLARE @Now DATE = dbo.fn_GetDate_Local()
	
	SELECT b.AnyID AS BrandId, rdvCode.DetailValue AS DisplayText, rdvCode.DetailValue AS Code
	FROM dbo.Customers c WITH (NOLOCK) 
	INNER JOIN dbo.CustomerDetailValues cdvBrand WITH (NOLOCK)  ON c.CustomerID = cdvBrand.CustomerID AND cdvBrand.DetailFieldID = 170144 
	INNER JOIN @BrandIds b ON cdvBrand.ValueInt = b.AnyID
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	INNER JOIN dbo.TableRows r WITH (NOLOCK) ON l.LeadID = r.LeadID AND r.DetailFieldID = 177367
	INNER JOIN dbo.TableDetailValues tdvRl WITH (NOLOCK) ON r.TableRowID = tdvRl.TableRowID AND tdvRl.DetailFieldID = 177364
	INNER JOIN dbo.ResourceListDetailValues rdvCode WITH (NOLOCK) ON tdvRl.ResourceListID = rdvCode.ResourceListID AND rdvCode.DetailFieldID = 177363
	LEFT JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 177365  
	LEFT JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 177366 
	WHERE c.ClientID = @ClientId
	AND (tdvStart.ValueDate IS NOT NULL AND tdvStart.ValueDate <= @Now)	/*GPR 2019-12-12 changed to NOT NULL*/
	AND (tdvEnd.ValueDate IS NOT NULL AND tdvEnd.ValueDate >= @Now)		/*GPR 2019-12-12 changed to NOT NULL*/
	ORDER BY r.TableRowID ASC --rdvCode.DetailValue
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_CampaignCodes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_CampaignCodes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_CampaignCodes] TO [sp_executeall]
GO
