SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SubQueryLinkage table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SubQueryLinkageID],
					[ClientID],
					[SubQueryID],
					[LinksToTableName],
					[LinksToColumnName],
					[LinkageDataType],
					[ParamTableName],
					[ParamColumnName],
					[ParamColumnDataType],
					[ParamColumnHelperSql],
					[LeadTypeFilterSql]
				FROM
					[dbo].[SubQueryLinkage] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_GetByClientID] TO [sp_executeall]
GO
