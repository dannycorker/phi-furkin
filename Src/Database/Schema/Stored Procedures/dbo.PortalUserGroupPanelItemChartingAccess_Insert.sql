SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalUserGroupPanelItemChartingAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_Insert]
(

	@PortalUserGroupPanelItemChartingAccessID int    OUTPUT,

	@ClientID int   ,

	@PortalUserGroupID int   ,

	@PanelItemChartingID int   ,

	@HasAccess bit   
)
AS


				
				INSERT INTO [dbo].[PortalUserGroupPanelItemChartingAccess]
					(
					[ClientID]
					,[PortalUserGroupID]
					,[PanelItemChartingID]
					,[HasAccess]
					)
				VALUES
					(
					@ClientID
					,@PortalUserGroupID
					,@PanelItemChartingID
					,@HasAccess
					)
				-- Get the identity value
				SET @PortalUserGroupPanelItemChartingAccessID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Insert] TO [sp_executeall]
GO
