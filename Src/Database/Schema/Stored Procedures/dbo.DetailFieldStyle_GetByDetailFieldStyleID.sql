SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldStyle table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldStyle_GetByDetailFieldStyleID]
(

	@DetailFieldStyleID int   
)
AS


				SELECT
					[DetailFieldStyleID],
					[ClientID],
					[StyleName],
					[BackgroundColor],
					[ForegroundColor],
					[FontSize],
					[FontWeight],
					[Align],
					[Padding],
					[PaddingHorizontal],
					[TextDecoration],
					[FontStyle],
					[PaddingVertical],
					[Enabled]
				FROM
					[dbo].[DetailFieldStyle] WITH (NOLOCK) 
				WHERE
										[DetailFieldStyleID] = @DetailFieldStyleID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_GetByDetailFieldStyleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldStyle_GetByDetailFieldStyleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_GetByDetailFieldStyleID] TO [sp_executeall]
GO
