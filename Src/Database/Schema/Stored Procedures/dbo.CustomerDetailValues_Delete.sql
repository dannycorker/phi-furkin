SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomerDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerDetailValues_Delete]
(

	@CustomerDetailValueID int   
)
AS


				DELETE FROM [dbo].[CustomerDetailValues] WITH (ROWLOCK) 
				WHERE
					[CustomerDetailValueID] = @CustomerDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_Delete] TO [sp_executeall]
GO
