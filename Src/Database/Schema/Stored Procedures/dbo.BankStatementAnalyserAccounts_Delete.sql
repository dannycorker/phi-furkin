SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the BankStatementAnalyserAccounts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BankStatementAnalyserAccounts_Delete]
(

	@BankStatementAnalyserAccountID int   
)
AS


				DELETE FROM [dbo].[BankStatementAnalyserAccounts] WITH (ROWLOCK) 
				WHERE
					[BankStatementAnalyserAccountID] = @BankStatementAnalyserAccountID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStatementAnalyserAccounts_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Delete] TO [sp_executeall]
GO
