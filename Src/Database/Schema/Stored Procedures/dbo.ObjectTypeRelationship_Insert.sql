SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ObjectTypeRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_Insert]
(

	@ObjectTypeRelationshipID int    OUTPUT,

	@FromObjectTypeID int   ,

	@ToObjectTypeID int   ,

	@RelationshipTypeID int   ,

	@RelationshipName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ObjectTypeRelationship]
					(
					[FromObjectTypeID]
					,[ToObjectTypeID]
					,[RelationshipTypeID]
					,[RelationshipName]
					)
				VALUES
					(
					@FromObjectTypeID
					,@ToObjectTypeID
					,@RelationshipTypeID
					,@RelationshipName
					)
				-- Get the identity value
				SET @ObjectTypeRelationshipID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Insert] TO [sp_executeall]
GO
