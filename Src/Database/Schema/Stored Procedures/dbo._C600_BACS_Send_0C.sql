SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-07-13
-- Description:	Adds a 0C record to the Collections Mandate table if necessary (i.e. mandate not already withdrawn)
-- Mods
-- 2016-02-29 DCM Don't send 0C at this point, just set flag
-- 2017-10-12 JEL Don't worry about the mandate status for setting the cancellation flag to allow policies cancelled on day 1 to send cancellation records
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BACS_Send_0C] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50022344
	
	DECLARE 
			@AqUserID INT,
			@CaseID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@CollectionsMatterID INT,
			@TableRowID INT
			
	-- find appropriate collections matter for the matter passed in
	IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @CollectionsMatterID=@MatterID
		
	END
	ELSE
	BEGIN
	
		SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493

	END
	SELECT @CaseID=m.CaseID,
		@AqUserID=CASE WHEN @AqUserID IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @AqUserID END
	FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@CollectionsMatterID

	-- SEND 0C?
	/* 
	   We can't send the 0C to BACS until all other transactions related to the cancellation (refund or payment)
	   have completed.  Just set flag at this point and allow house-keeping to apply the 0C when
	   everything has cleared
	*/
	IF --dbo.fnGetDvAsInt(170101,@CaseID) IN (69917,69918) -- only if mandate status is pending or live
		NOT EXISTS ( -- and only if there are no other live policies being collected on this mandate
		SELECT * FROM MatterDetailValues pstat WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON pstat.MatterID=ltr.FromMatterID AND ltr.ToLeadTypeID=1493
		WHERE pstat.DetailFieldID=170038 AND pstat.ValueInt IN (43002)
		AND ltr.ToMatterID=@CollectionsMatterID
	 )
	BEGIN

		-- set cancel flag
		EXEC _C00_SimpleValueIntoField 176895, 5144, @CollectionsMatterID, @AqUserID 

		-- set next date for refunds
		EXEC _C600_SetNextPaymentDate @MatterID, 3
	
	END

	-- SEND UPDATED COLLECTIONS NOTIFICATION?
	IF dbo.fnGetDvAsInt(170101,@CaseID) IN (69917,69918) -- only if mandate status is pending or live
	 AND EXISTS ( -- and only if there are other live policies being collected on this mandate
		SELECT * FROM MatterDetailValues pstat WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON pstat.MatterID=ltr.FromMatterID AND ltr.ToLeadTypeID=1493
		WHERE pstat.DetailFieldID=170038 AND pstat.ValueInt IN (43002)
		AND ltr.ToMatterID=@CollectionsMatterID
	 )
	BEGIN

		-- ensure 10 days warning
		EXEC _C600_SetNextPaymentDate @MatterID, 9
		
		-- update doc fields for notification process
		EXEC _C600_Collections_RefreshDocumentationFields @CollectionsMatterID
		EXEC _C600_UpdateLetterTextField 175462, 'RECALCAFTERCANCEL', @CollectionsMatterID
		
		-- start notification process
		EXEC _C600_AddAutomatedEvent @CollectionsMatterID, 155439, -1,@AqUserID
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_Send_0C] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BACS_Send_0C] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_Send_0C] TO [sp_executeall]
GO
