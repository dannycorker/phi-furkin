SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeInstance table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeInstance_GetByWhoModified]
(

	@WhoModified int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChangeInstanceID],
					[ClientID],
					[ChangeDefinitionID],
					[ChangeTypeID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ChangeInstance] WITH (NOLOCK) 
				WHERE
					[WhoModified] = @WhoModified
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_GetByWhoModified] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeInstance_GetByWhoModified] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_GetByWhoModified] TO [sp_executeall]
GO
