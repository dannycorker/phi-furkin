SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_App_Verify
-- =============================================
CREATE PROCEDURE [dbo].[API_Authorised]
	@IdentID UNIQUEIDENTIFIER,
	@AppKey VARCHAR(50),
	@ObjectKey VARCHAR(50),
	@ByteSize INT
AS
BEGIN

	SELECT	app.AppKey
	FROM	API_App app WITH (NOLOCK) 
	INNER JOIN API_AppAccess aa WITH (NOLOCK) ON app.AppKey = aa.AppKey AND app.Enabled = 1 AND aa.Enabled = 1 
	INNER JOIN	API_Ident i WITH (NOLOCK) ON i.IdentID = @IdentID AND i.Enabled = 1
	WHERE	aa.ObjectKey = @ObjectKey
	AND		aa.MaxByteSize > @ByteSize
	AND		aa.AppKey = @AppKey

END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Authorised] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Authorised] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Authorised] TO [sp_executeall]
GO
