SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterPageChoice table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_GetByMatterIDDetailFieldPageID]
(

	@MatterID int   ,

	@DetailFieldPageID int   
)
AS


				SELECT
					[MatterPageChoiceID],
					[ClientID],
					[MatterID],
					[DetailFieldPageID],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[MatterPageChoice] WITH (NOLOCK) 
				WHERE
										[MatterID] = @MatterID
					AND [DetailFieldPageID] = @DetailFieldPageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_GetByMatterIDDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_GetByMatterIDDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_GetByMatterIDDetailFieldPageID] TO [sp_executeall]
GO
