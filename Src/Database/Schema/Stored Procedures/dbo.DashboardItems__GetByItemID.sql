SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-06
-- Description:	Get Group Items
-- =============================================
CREATE PROCEDURE [dbo].[DashboardItems__GetByItemID] 
	@ClientID INT,
	@DasboardItemID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM DashboardItems d WITH (NOLOCK)
	WHERE d.DashboardItemID = @DasboardItemID
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItems__GetByItemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItems__GetByItemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItems__GetByItemID] TO [sp_executeall]
GO
