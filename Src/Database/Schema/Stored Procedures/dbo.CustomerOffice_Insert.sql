SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomerOffice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_Insert]
(

	@OfficeID int    OUTPUT,

	@ClientID int   ,

	@CustomerID int   ,

	@OfficeName varchar (100)  ,

	@MainPhoneNumber varchar (100)  ,

	@FaxNumber varchar (100)  ,

	@Address1 varchar (100)  ,

	@Address2 varchar (100)  ,

	@Town varchar (100)  ,

	@County varchar (100)  ,

	@Postcode varchar (100)  ,

	@Country varchar (100)  ,

	@Notes varchar (255)  ,

	@CountryID int   
)
AS


				
				INSERT INTO [dbo].[CustomerOffice]
					(
					[ClientID]
					,[CustomerID]
					,[OfficeName]
					,[MainPhoneNumber]
					,[FaxNumber]
					,[Address1]
					,[Address2]
					,[Town]
					,[County]
					,[Postcode]
					,[Country]
					,[Notes]
					,[CountryID]
					)
				VALUES
					(
					@ClientID
					,@CustomerID
					,@OfficeName
					,@MainPhoneNumber
					,@FaxNumber
					,@Address1
					,@Address2
					,@Town
					,@County
					,@Postcode
					,@Country
					,@Notes
					,@CountryID
					)
				-- Get the identity value
				SET @OfficeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Insert] TO [sp_executeall]
GO
