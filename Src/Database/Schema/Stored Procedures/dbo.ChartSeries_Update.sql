SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChartSeries table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_Update]
(

	@ChartSeriesID int   ,

	@ChartSeriesTypeID int   ,

	@ChartSeriesOrder int   ,

	@ChartID int   ,

	@ClientID int   ,

	@PanelItemChartingID int   ,

	@ColumnName varchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChartSeries]
				SET
					[ChartSeriesTypeID] = @ChartSeriesTypeID
					,[ChartSeriesOrder] = @ChartSeriesOrder
					,[ChartID] = @ChartID
					,[ClientID] = @ClientID
					,[PanelItemChartingID] = @PanelItemChartingID
					,[ColumnName] = @ColumnName
				WHERE
[ChartSeriesID] = @ChartSeriesID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Update] TO [sp_executeall]
GO
