SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE PROCEDURE [dbo].[AddLinkedDetailField]
@LeadTypeIDTo int,
@LeadTypeIDFrom int,
@DetailFieldIDTo int,
@DetailFieldIDFrom int,
@DisplayOnPageID int,
@FieldOrder int,
@Enabled bit,
@History bit,
@LeadOrMatter int,
@FieldName nvarchar(100),
@LeadLinkedTo int,
@LeadLinkedFrom int,
@IncludeLinkedField bit, 
@ClientID int

as

insert into LinkedDetailFields (LeadTypeIDTo, LeadTypeIDFrom, DetailFieldIDTo, DetailFieldIDFrom,
DisplayOnPageID, FieldOrder, Enabled, History, LeadOrMatter, FieldName, LeadLinkedTo, LeadLinkedFrom, IncludeLinkedField, ClientID)
values (@LeadTypeIDTo, @LeadTypeIDFrom, @DetailFieldIDTo, @DetailFieldIDFrom,
@DisplayOnPageID, @FieldOrder, @Enabled, @History, @LeadOrMatter, @FieldName, @LeadLinkedTo, @LeadLinkedFrom, @IncludeLinkedField, @ClientID)

declare @LinkedDetailFieldID int

set @LinkedDetailFieldID = SCOPE_IDENTITY()

Select LinkedDetailFieldID from LinkedDetailFields where LinkedDetailFieldID = @LinkedDetailFieldID







GO
GRANT VIEW DEFINITION ON  [dbo].[AddLinkedDetailField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddLinkedDetailField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddLinkedDetailField] TO [sp_executeall]
GO
