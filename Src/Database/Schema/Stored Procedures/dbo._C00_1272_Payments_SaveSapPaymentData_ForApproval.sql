SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-11-02
-- Description:	Saves the SAP payment data ready for approval
-- Used by:		SAE
-- Modified:	2013-02-05 ROH Fix for Payee Reference 
--				2013-05-20 ROH Parameter for PaymentType (instead of inferring from sign of amount)
--							   Also error checking for payment details from policy/scheme			
-- ROH 2014-05-01 Write cashbook transactions for refunds and reversals
-- ROH 2014-08-27 DenyEdit and DenyDelete on payment rows
-- ROH 2014-09-11 Cashbook processing commented out (could be cient switchable in future)
-- ROH 2014-12-11 Use textual title rather than crazy old SAP code Zen#30120
-- ROH 2015-05-09 ContractCode and clientIdentifier now obsolete Zen#32931
-- TMD 2015-10-29 Populate Vets address ResourceLists Zen#34516
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Payments_SaveSapPaymentData_ForApproval] 
(
	@MatterID INT,
	@Payments dbo.tvpIntMoney READONLY,
	@PaymentType VARCHAR(1) = '0', -- 0=Payment, 1=Refund, 2=Cancellation
	@Debug BIT = 0
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	-- Verify the data first and throw so that event is not applied
	EXEC _C00_1272_Payments_VerifySapPaymentData @Payments, @MatterID, @PaymentType
	
	DECLARE @Amounts TABLE
	(
		ID INT IDENTITY,
		MatterID INT,
		TableRowIDs VARCHAR(2000),
		PayTo VARCHAR(2000),
		PayeeTitle VARCHAR(2000),
		PayeeFirstName VARCHAR(2000),
		PayeeLastName VARCHAR(2000),
		PayeeOrganisation VARCHAR(2000),
		PayeeAddress1 VARCHAR(2000),
		PayeeAddress2 VARCHAR(2000),
		PayeeAddress3 VARCHAR(2000),
		PayeeAddress4 VARCHAR(2000),
		PayeeAddress5 VARCHAR(2000),
		PayeePostcode VARCHAR(2000),
		PayeeCountry VARCHAR(2000),
		PayeeReference VARCHAR(2000),
		PayeeType VARCHAR(2000),
		PolicyHolderTitle VARCHAR(2000),
		PolicyHolderFirstName VARCHAR(2000),
		PolicyHolderLastName VARCHAR(2000),
		PolicyHolderOrganisation VARCHAR(2000),
		PolicyHolderAddress1 VARCHAR(2000),
		PolicyHolderAddress2 VARCHAR(2000),
		PolicyHolderAddress3 VARCHAR(2000),
		PolicyHolderAddress4 VARCHAR(2000),
		PolicyHolderAddress5 VARCHAR(2000),
		PolicyHolderPostcode VARCHAR(2000),
		PolicyHolderCountry VARCHAR(2000),
		PolicyHolderReference VARCHAR(2000),
		PolicyHolderType VARCHAR(2000),
		PolicyCode VARCHAR(2000),
		ClaimCode VARCHAR(2000),
		PaymentType VARCHAR(2000),
		PaymentAmount VARCHAR(2000),
		ClientIdentifier VARCHAR(2000),
		PaymentDate VARCHAR(2000),
		DateCreated VARCHAR(2000),
		PetName VARCHAR(2000),
		OriginalPaymentReference VARCHAR(2000),
		Approved VARCHAR(2000),
		Policy VARCHAR(2000),
		ClaimContract VARCHAR(2000),
		POLHExternalRef VARCHAR(2000),
		AffinityCode VARCHAR(2000),
		AffinityPartnerName VARCHAR(2000),
		ValidFromDate VARCHAR(2000)
		
	)
	
	Declare @ReferralVetResourceListID	INT
			,@ReferralVetFieldID		INT
			,@PayVetResourceListID		INT
			,@PayVetFieldID				INT

	INSERT @Amounts (MatterID, PayTo, PaymentAmount, Approved)
	SELECT @MatterID, p.ID, p.Value, ''
	FROM @Payments p
	
	-- Pay policy holder
	UPDATE @Amounts
	SET PayeeTitle = CASE WHEN c.TitleID = 0 THEN '' ELSE tit.Title END
						--CASE c.TitleID
						--	WHEN 1 THEN '0001' -- Mr
						--	WHEN 2 THEN '0002' -- Mrs
						--	WHEN 3 THEN 'Z001' -- Miss
						--	WHEN 6 THEN 'Z002' -- Ms
						--	WHEN 11 THEN 'Z003' -- Master
						--	WHEN 10 THEN 'Z004' -- Sir
						--	WHEN 4 THEN 'Z006' -- Dr
						--	WHEN 9 THEN 'Z007' -- Mr & Mrs
						--	WHEN 12 THEN 'Z010' -- Prof
						--	ELSE ''						
						--END
						, PayeeFirstName = c.FirstName, PayeeLastName = c.LastName, PayeeOrganisation = c.Fullname, PayeeAddress1 = c.Address1, PayeeAddress2 = c.Address2,
						PayeeAddress3 = c.Town, PayeeAddress4 = c.County, PayeeAddress5 = '', PayeePostcode = c.PostCode, PayeeCountry = 'GB', PayeeReference = 'PH'+CAST(m.MatterID as varchar(20)),
						PayeeType = 1
	FROM dbo.Matter m WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON m.LeadID = l.LeadID
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	LEFT JOIN Titles tit WITH (NOLOCK) on tit.TitleID =c.TitleID
	WHERE PayTo = 44231
	AND m.MatterID = @MatterID
	
	-- Pay Vet
	UPDATE @Amounts
	SET PayeeTitle = '', PayeeFirstName = '', PayeeLastName = '', PayeeOrganisation = rdvOrg.DetailValue, PayeeAddress1 = rdvAddress1.DetailValue, 
		PayeeAddress2 = rdvAddress2.DetailValue, PayeeAddress3 = rdvAddress3.DetailValue, PayeeAddress4 = rdvTown.DetailValue, PayeeAddress5 = rdvCounty.DetailValue, 
		PayeePostcode = rdvPostcode.DetailValue, PayeeCountry = ISNULL(c.Alpha2Code, 'GB'), PayeeReference = 'VT'+CAST(ldvVet.ValueInt as varchar(20)), PayeeType = 2
	FROM dbo.Lead l WITH (NOLOCK)
	LEFT JOIN dbo.LeadDetailValues ldvVet WITH (NOLOCK) ON l.LeadID = ldvVet.LeadID AND ldvVet.DetailFieldID = 146215
	LEFT JOIN dbo.ResourceListDetailValues rdvOrg WITH (NOLOCK) ON ldvVet.ValueInt = rdvOrg.ResourceListID AND rdvOrg.DetailFieldID = 144473
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON ldvVet.ValueInt = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress2 WITH (NOLOCK) ON ldvVet.ValueInt = rdvAddress2.ResourceListID AND rdvAddress2.DetailFieldID = 144476
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress3 WITH (NOLOCK) ON ldvVet.ValueInt = rdvAddress3.ResourceListID AND rdvAddress3.DetailFieldID = 144474
	LEFT JOIN dbo.ResourceListDetailValues rdvTown WITH (NOLOCK) ON ldvVet.ValueInt = rdvTown.ResourceListID AND rdvTown.DetailFieldID = 144477
	LEFT JOIN dbo.ResourceListDetailValues rdvCounty WITH (NOLOCK) ON ldvVet.ValueInt = rdvCounty.ResourceListID AND rdvCounty.DetailFieldID = 144478
	LEFT JOIN dbo.ResourceListDetailValues rdvPostcode WITH (NOLOCK) ON ldvVet.ValueInt = rdvPostcode.ResourceListID AND rdvPostcode.DetailFieldID = 144479
	LEFT JOIN dbo.ResourceListDetailValues rdvCountry WITH (NOLOCK) ON ldvVet.ValueInt = rdvCountry.ResourceListID AND rdvCountry.DetailFieldID = 158509
	LEFT JOIN dbo.LookupListItems llCountry WITH (NOLOCK) ON rdvCountry.ValueInt = llCountry.LookupListItemID
	LEFT JOIN dbo.Country c WITH (NOLOCK) ON llCountry.ItemValue = c.CountryName
	WHERE PayTo = 44232
	AND l.LeadID = @PolicyLeadID
	
	/*Ticket#34516*/
	SELECT TOP 1 @PayVetResourceListID = ldv.detailvalue ,@PayVetFieldID = 157804
	FROM LeadDetailValues ldv WITH (NOLOCK)	
	WHERE ldv.DetailFieldID = 146215
	AND ldv.LeadID = @PolicyLeadID	
	
	exec _C00_SimpleValueIntoField @PayVetFieldID,@PayVetResourceListID,@MatterID
	
	-- Pay referral vet
	UPDATE a
	SET PayeeTitle = '', PayeeFirstName = '', PayeeLastName = '', PayeeOrganisation = rdvOrg.DetailValue, PayeeAddress1 = rdvAddress1.DetailValue, 
		PayeeAddress2 = rdvAddress2.DetailValue, PayeeAddress3 = rdvAddress3.DetailValue, PayeeAddress4 = rdvTown.DetailValue, PayeeAddress5 = rdvCounty.DetailValue, 
		PayeePostcode = rdvPostcode.DetailValue, PayeeCountry = ISNULL(c.Alpha2Code, 'GB'), PayeeReference = 'RV'+CAST(mdvVet.ValueInt as varchar(20)), PayeeType = 2
	FROM @Amounts a
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON a.MatterID = m.MatterID
	LEFT JOIN dbo.MatterDetailValues mdvVet WITH (NOLOCK) ON m.MatterID = mdvVet.MatterID AND mdvVet.DetailFieldID = 146236
	LEFT JOIN dbo.ResourceListDetailValues rdvOrg WITH (NOLOCK) ON mdvVet.ValueInt = rdvOrg.ResourceListID AND rdvOrg.DetailFieldID = 144473
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress2 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress2.ResourceListID AND rdvAddress2.DetailFieldID = 144476
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress3 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress3.ResourceListID AND rdvAddress3.DetailFieldID = 144474
	LEFT JOIN dbo.ResourceListDetailValues rdvTown WITH (NOLOCK) ON mdvVet.ValueInt = rdvTown.ResourceListID AND rdvTown.DetailFieldID = 144477
	LEFT JOIN dbo.ResourceListDetailValues rdvCounty WITH (NOLOCK) ON mdvVet.ValueInt = rdvCounty.ResourceListID AND rdvCounty.DetailFieldID = 144478
	LEFT JOIN dbo.ResourceListDetailValues rdvPostcode WITH (NOLOCK) ON mdvVet.ValueInt = rdvPostcode.ResourceListID AND rdvPostcode.DetailFieldID = 144479
	LEFT JOIN dbo.ResourceListDetailValues rdvCountry WITH (NOLOCK) ON mdvVet.ValueInt = rdvCountry.ResourceListID AND rdvCountry.DetailFieldID = 158509
	LEFT JOIN dbo.LookupListItems llCountry WITH (NOLOCK) ON rdvCountry.ValueInt = llCountry.LookupListItemID
	LEFT JOIN dbo.Country c WITH (NOLOCK) ON llCountry.ItemValue = c.CountryName
	WHERE a.PayTo = 46135
	AND m.MatterID = @MatterID


	/*Ticket#34516*/
	SELECT TOP 1 @ReferralVetResourceListID = mdv.detailvalue ,@ReferralVetFieldID = 157805
	FROM MatterDetailValues mdv WITH (NOLOCK)	
	WHERE mdv.DetailFieldID = 146236 
	AND mdv.MatterID = @MatterID	

	-- Pay Out of Hours Vet /*SA - 2018-03-15*/
	UPDATE a
	SET PayeeTitle = '', PayeeFirstName = '', PayeeLastName = '', PayeeOrganisation = rdvOrg.DetailValue, PayeeAddress1 = rdvAddress1.DetailValue, 
		PayeeAddress2 = rdvAddress2.DetailValue, PayeeAddress3 = rdvAddress3.DetailValue, PayeeAddress4 = rdvTown.DetailValue, PayeeAddress5 = rdvCounty.DetailValue, 
		PayeePostcode = rdvPostcode.DetailValue, PayeeCountry = ISNULL(c.Alpha2Code, 'GB'), PayeeReference = 'RV'+CAST(mdvVet.ValueInt as varchar(20)), PayeeType = 2
	FROM @Amounts a
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON a.MatterID = m.MatterID
	LEFT JOIN dbo.MatterDetailValues mdvVet WITH (NOLOCK) ON m.MatterID = mdvVet.MatterID AND mdvVet.DetailFieldID =176944 /*Out of hours VET resource list*/
	LEFT JOIN dbo.ResourceListDetailValues rdvOrg WITH (NOLOCK) ON mdvVet.ValueInt = rdvOrg.ResourceListID AND rdvOrg.DetailFieldID =  144473
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress2 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress2.ResourceListID AND rdvAddress2.DetailFieldID = 144476
	LEFT JOIN dbo.ResourceListDetailValues rdvAddress3 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress3.ResourceListID AND rdvAddress3.DetailFieldID = 144474
	LEFT JOIN dbo.ResourceListDetailValues rdvTown WITH (NOLOCK) ON mdvVet.ValueInt = rdvTown.ResourceListID AND rdvTown.DetailFieldID = 144477
	LEFT JOIN dbo.ResourceListDetailValues rdvCounty WITH (NOLOCK) ON mdvVet.ValueInt = rdvCounty.ResourceListID AND rdvCounty.DetailFieldID = 144478
	LEFT JOIN dbo.ResourceListDetailValues rdvPostcode WITH (NOLOCK) ON mdvVet.ValueInt = rdvPostcode.ResourceListID AND rdvPostcode.DetailFieldID = 144479
	LEFT JOIN dbo.ResourceListDetailValues rdvCountry WITH (NOLOCK) ON mdvVet.ValueInt = rdvCountry.ResourceListID AND rdvCountry.DetailFieldID = 158509
	LEFT JOIN dbo.LookupListItems llCountry WITH (NOLOCK) ON rdvCountry.ValueInt = llCountry.LookupListItemID
	LEFT JOIN dbo.Country c WITH (NOLOCK) ON llCountry.ItemValue = c.CountryName
	WHERE a.PayTo = 76367 /*OHV*/
	AND m.MatterID = @MatterID




	
	exec _C00_SimpleValueIntoField @ReferralVetFieldID,@ReferralVetResourceListID,@MatterID
	
	-- Pay third party
	UPDATE a
	SET PayeeTitle = mdvTitle.DetailValue, PayeeFirstName = mdvFirst.DetailValue, PayeeLastName = mdvLast.DetailValue, 
		PayeeOrganisation = CASE WHEN mdvType.DetailValue = '2' THEN mdvOrganisation.DetailValue ELSE ISNULL(mdvFirst.DetailValue, '') + ' ' + ISNULL(mdvLast.DetailValue, '') END, 
		PayeeAddress1 = mdvAddress1.DetailValue, PayeeAddress2 = mdvAddress2.DetailValue, PayeeAddress3 = mdvAddress3.DetailValue, PayeeAddress4 = mdvAddress4.DetailValue, 
		PayeeAddress5 = mdvAddress5.DetailValue, PayeePostcode = mdvPostcode.DetailValue, PayeeCountry = 'GB'
		, PayeeReference = NULL --Gets set later as "TPnnnnnnnn" from TableRow
		, PayeeType = mdvType.DetailValue
	FROM @Amounts a
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON a.MatterID = m.MatterID
	LEFT JOIN dbo.MatterDetailValues mdvTitle WITH (NOLOCK) ON m.MatterID = mdvTitle.MatterID AND mdvTitle.DetailFieldID = 157873
	LEFT JOIN dbo.MatterDetailValues mdvFirst WITH (NOLOCK) ON m.MatterID = mdvFirst.MatterID AND mdvFirst.DetailFieldID = 157808
	LEFT JOIN dbo.MatterDetailValues mdvLast WITH (NOLOCK) ON m.MatterID = mdvLast.MatterID AND mdvLast.DetailFieldID = 157809
	LEFT JOIN dbo.MatterDetailValues mdvAddress1 WITH (NOLOCK) ON m.MatterID = mdvAddress1.MatterID AND mdvAddress1.DetailFieldID = 157810
	LEFT JOIN dbo.MatterDetailValues mdvAddress2 WITH (NOLOCK) ON m.MatterID = mdvAddress2.MatterID AND mdvAddress2.DetailFieldID = 157811
	LEFT JOIN dbo.MatterDetailValues mdvAddress3 WITH (NOLOCK) ON m.MatterID = mdvAddress3.MatterID AND mdvAddress3.DetailFieldID = 157812
	LEFT JOIN dbo.MatterDetailValues mdvAddress4 WITH (NOLOCK) ON m.MatterID = mdvAddress4.MatterID AND mdvAddress4.DetailFieldID = 157813
	LEFT JOIN dbo.MatterDetailValues mdvAddress5 WITH (NOLOCK) ON m.MatterID = mdvAddress5.MatterID AND mdvAddress5.DetailFieldID = 157814
	LEFT JOIN dbo.MatterDetailValues mdvPostcode WITH (NOLOCK) ON m.MatterID = mdvPostcode.MatterID AND mdvPostcode.DetailFieldID = 157815
	LEFT JOIN dbo.MatterDetailValues mdvType WITH (NOLOCK) ON m.MatterID = mdvType.MatterID AND mdvType.DetailFieldID = 157817
	LEFT JOIN dbo.MatterDetailValues mdvOrganisation WITH (NOLOCK) ON m.MatterID = mdvOrganisation.MatterID AND mdvOrganisation.DetailFieldID = 157818
	WHERE a.PayTo = 44233
	AND m.MatterID = @MatterID
	
	
	-- PolicyHolder Info
	UPDATE @Amounts
	SET PolicyHolderTitle =	c.TitleID, PolicyHolderFirstName = c.FirstName, PolicyHolderLastName = c.LastName, PolicyHolderOrganisation = c.Fullname, PolicyHolderAddress1 = c.Address1, 
							PolicyHolderAddress2 = c.Address2, PolicyHolderAddress3 = c.Town, PolicyHolderAddress4 = c.County, PolicyHolderAddress5 = '', 
							PolicyHolderPostcode = c.PostCode, PolicyHolderCountry = 'GB', PolicyHolderReference = c.CustomerID, PolicyHolderType = 1
	FROM dbo.Matter m WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON m.LeadID = l.LeadID
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	
	WHERE m.MatterID = @MatterID
	
	
	-- Find the correct scheme code based on the treatment dates
	DECLARE @SchemeRLID INT
	SELECT @SchemeRLID = dbo.fn_C00_1272_GetSchemeFromClaim(@MatterID)
	
	DECLARE @ClaimContract VARCHAR(2000),
			@ClientIdentifier VARCHAR(2000),
			@Affinity VARCHAR(2000)
	
	-- dcm 2015-02-03 temporary fix to get claims payments running. Using ISNULL on @ClaimContract & @ClientIdentifier
	SELECT @ClaimContract = '', @ClientIdentifier = '', @Affinity = rdvAffinity.DetailValue
	FROM dbo.ResourceList r WITH (NOLOCK) 
	--INNER to LEFT Zen#32931
	--INNER JOIN dbo.ResourceListDetailValues rdvContract WITH (NOLOCK) ON r.ResourceListID = rdvContract.ResourceListID AND rdvContract.DetailFieldID = 161714
	LEFT JOIN dbo.ResourceListDetailValues rdvContract WITH (NOLOCK) ON r.ResourceListID = rdvContract.ResourceListID AND rdvContract.DetailFieldID = 161714
	LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdvContract.ValueInt = ll.LookupListItemID
	LEFT JOIN dbo.ResourceListDetailValues rdvCompany WITH (NOLOCK) ON r.ResourceListID = rdvCompany.ResourceListID AND rdvCompany.DetailFieldID = 155931
	LEFT JOIN dbo.LookupListItems llCompany WITH (NOLOCK) ON rdvCompany.ValueInt = llCompany.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rdvAffinity WITH (NOLOCK) ON r.ResourceListID = rdvAffinity.ResourceListID AND rdvAffinity.DetailFieldID = 144314
	WHERE r.ResourceListID = @SchemeRLID
	
	DECLARE @TreatmentStart DATE,
			@PolicyStart DATE,
			@PolicyNumber VARCHAR(1000)
			
	SELECT @TreatmentStart = mdv.ValueDate 
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144366
	WHERE m.MatterID = @MatterID
	
	SELECT @PolicyStart = tdvStart.ValueDate, @PolicyNumber=tdvPolNum.DetailValue
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	LEFT JOIN dbo.TableDetailValues tdvPolNum WITH (NOLOCK) ON r.TableRowID = tdvPolNum.TableRowID AND tdvPolNum.DetailFieldID = 162641
	WHERE @TreatmentStart >= tdvStart.ValueDate
	AND @TreatmentStart <= tdvEnd.ValueDate
	
	-- Throw error if payment/policy details cannot be determined
	IF (@ClaimContract IS NULL OR @ClientIdentifier IS NULL OR @Affinity IS NULL OR @PolicyStart IS NULL)
	BEGIN
		DECLARE @ErrorMessage VARCHAR(2000)
		SELECT @ErrorMessage = '<br /><br /><font color="red">Payment or policy details cannot be determined.</font><br/><font color="blue">Please check if claim dates have been changed.</font>'
		RAISERROR(@ErrorMessage,16,1)
		RETURN
	END

	UPDATE @Amounts
	SET POLHExternalRef = p.ImportedPolicyRef, 
		AffinityCode = p.AffinityGroup
	FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID) p
	
	-- Split out this block as it is on the lead which could be claim or policy
	UPDATE @Amounts
	SET PolicyCode = l.LeadID, 
		ClaimCode = @MatterID, 
		PaymentType = @PaymentType,
		ClientIdentifier = @ClientIdentifier, 
		PaymentDate = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 23), 
		DateCreated = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120),
		PetName = ISNULL(ldvPet.DetailValue, ''),
		Policy = ISNULL(@PolicyNumber,l.LeadRef), 
		ClaimContract = @ClaimContract, 
		--POLHExternalRef = ldvPolRef.DetailValue, 
		--AffinityCode = ldvAffinity.DetailValue, 
		AffinityPartnerName = @Affinity, 
		ValidFromDate = CONVERT(VARCHAR, @PolicyStart, 103)
	FROM dbo.Lead l WITH (NOLOCK)
	LEFT JOIN dbo.LeadDetailValues ldvPet WITH (NOLOCK) ON l.LeadID = ldvPet.LeadID AND ldvPet.DetailFieldID = 144268
	WHERE l.LeadID = @PolicyLeadID
	
	
	/* Handle refunds and cancellations */
	-- Only ever one transaction per call
	IF @PaymentType IN ('1','2')
	BEGIN
	
		DECLARE @OriginalPaymentReference VARCHAR(2000),
				@TableRowID INT
		
		SELECT @TableRowID = ValueInt, @OriginalPaymentReference = DetailValue -- Default to the saved table row ID
		FROM dbo.MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID 
		AND DetailFieldID = 159559
		
		-- Check to see if the row has a SAP claim row key
		DECLARE @SapKey VARCHAR(2000) = NULL
		SELECT @SapKey = DetailValue
		FROM dbo.TableDetailValues WITH (NOLOCK) 
		WHERE TableRowID = @TableRowID 
		AND MatterID = @MatterID
		AND DetailFieldID = 162635
		
		IF @SapKey > ''
		BEGIN
			DECLARE @ClaimNumber VARCHAR(2000)
			SELECT @ClaimNumber = DetailValue
			FROM dbo.MatterDetailValues WITH (NOLOCK) 
			WHERE MatterID = @MatterID 
			AND DetailFieldID = 162628
			
			SELECT @OriginalPaymentReference = ISNULL(@ClaimNumber, '') + '-' + @SapKey
		END
		
		UPDATE @Amounts
		SET PaymentAmount = CAST(PaymentAmount AS MONEY) * -1,
			OriginalPaymentReference = @OriginalPaymentReference,
			Approved = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		--WHERE PaymentType = '1'
		
		
		/* NO CASHBOOK REQUIRED !! */ -- ROH 2014-09-11
		/*
			/* Write a Cashbook transaction for the reversal */
			DECLARE @Test BIT
			SELECT @Test = Test
			FROM Customers cu WITH (NOLOCK)
			INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CustomerID = cu.CustomerID
			WHERE m.MatterID = @MatterID
			
			IF @Test = 0
			BEGIN
			
				DECLARE 
					@CashbookBalance MONEY,
					@CashbookLeadID INT,
					@Comments VARCHAR(2000),
					@RefundAmount MONEY = 0,
					@CashbookTransactionRowID INT
				
				SELECT TOP 1 @CashbookLeadID = lCashbook.LeadID, @CashbookBalance = ISNULL(ldvBalance.ValueMoney,0)
				FROM Lead lCashbook 
				INNER JOIN dbo.LeadDetailValues ldvContract WITH (NOLOCK) on ldvContract.LeadID = lCashbook.LeadID AND ldvContract.DetailFieldID = 162718
				LEFT JOIN dbo.LeadDetailValues ldvBalance WITH (NOLOCK) on ldvBalance.LeadID = lCashbook.LeadID AND ldvBalance.DetailFieldID = 162707
				WHERE ldvContract.DetailValue = @ClaimContract
					
				-- Reduce cashbook balance
				SELECT @RefundAmount += a.PaymentAmount -- This will be a negative total
				FROM @Amounts a
				
				SELECT @CashbookBalance += @RefundAmount -- Increasing the balance
				
				SELECT @Comments = CASE @PaymentType WHEN '1' THEN 'REFUND ' WHEN '2' THEN 'REVERSAL ' END
					+ ' MatterID: ' + CONVERT(VARCHAR(25),@MatterID)
					+ ' Value: ' + CONVERT(VARCHAR(25),@RefundAmount)
							
				-- Write Cashbook Transaction		
				INSERT TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID)
				SELECT @ClientID, @CashbookLeadID, 162717 , 18191
				SELECT @CashbookTransactionRowID = SCOPE_IDENTITY()
				
				INSERT TableDetailValues (TableRowID, ClientID, LeadID, DetailFieldID, DetailValue)
				VALUES
				 (@CashbookTransactionRowID, @ClientID, @CashbookLeadID, 162711, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120))		--Transaction date
				 ,(@CashbookTransactionRowID, @ClientID, @CashbookLeadID, 162722
					, CASE @PaymentType WHEN '1' THEN '69698' WHEN '2' THEN '69697' END)								--Transaction type (refund/cancellation)
				,(@CashbookTransactionRowID, @ClientID, @CashbookLeadID, 162712, CONVERT(varchar(50), @RefundAmount))		--Transaction value
				,(@CashbookTransactionRowID, @ClientID, @CashbookLeadID, 162713, CONVERT(varchar(50), @CashbookBalance))	--New balance
				,(@CashbookTransactionRowID, @ClientID, @CashbookLeadID, 162716, @Comments)								--Comments
					
				-- Update current balance
				EXEC _C00_SimpleValueIntoField 162707, @CashbookBalance, @CashbookLeadID, NULL
			
			END
		*/
	
	END

	
	IF @Debug = 1
	BEGIN
	
		SELECT *
		FROM @Amounts
		
	END
	ELSE -- Not debug, so write the payment rows
	BEGIN
	
		
		-- Clear out the old rows that have not yet been neither approved nor sent
		DELETE r
		FROM dbo.TableRows r
		LEFT JOIN dbo.TableDetailValues tdvApproved WITH (NOLOCK) ON r.TableRowID = tdvApproved.TableRowID AND tdvApproved.DetailFieldID = 159407
		LEFT JOIN dbo.TableDetailValues tdvSent WITH (NOLOCK) ON r.TableRowID = tdvSent.TableRowID AND tdvSent.DetailFieldID = 154521
		WHERE r.DetailFieldID = 154485
		AND r.DetailFieldPageID = 17324
		AND r.MatterID = @MatterID
		AND r.ClientID = @ClientID
		AND tdvApproved.ValueDate IS NULL
		AND tdvSent.ValueDate IS NULL
	
		-- Insert the table rows
		DECLARE @Inserted TABLE
		(
			ID INT IDENTITY,
			TableRowID INT
		)	
		
		INSERT dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete)
		OUTPUT inserted.TableRowID INTO @Inserted
		SELECT @ClientID, @MatterID, 154485, 17324, 1, 1
		FROM @Amounts
		
		DECLARE @Fields TABLE
		(
			FieldID INT
		)
		INSERT @Fields (FieldID) VALUES
		(154486),	--Payee Title
		(154487),	--Payee First Name
		(154488),	--Payee Last Name
		(154490),	--Payee Organisation
		(154491),	--Payee Address 1
		(154492),	--Payee Address 2
		(154493),	--Payee Address 3
		(154494),	--Payee Address 4
		(154495),	--Payee Address 5
		(154496),	--Payee Postcode
		(154497),	--Payee Country
		(154498),	--Payee Reference
		(154499),	--Payee Type
		(154500),	--Policyholder Title
		(154501),	--Policyholder First Name
		(154502),	--Policyholder Last Name
		(154503),	--Policyholder Organisation
		(154504),	--Policyholder Address 1
		(154505),	--Policyholder Address 2
		(154506),	--Policyholder Address 3
		(154507),	--Policyholder Address 4
		(154508),	--Policyholder Address 5
		(154509),	--Policyholder Postcode
		(154510),	--Policyholder Country
		(154511),	--Policyholder Reference
		(154514),	--Policyholder Type
		(154512),	--Policy Code
		(154513),	--Claim Code
		(154517),	--Payment Type
		(154518),	--Payment Amount
		(154519),	--Client Identifier
		(154520),	--Payment Date
		(158479),	--Date Created
		(158534),	--TableRowIDs
		(159005),	--PetName
		(159407),	--Payment Approved
		(159408),	--Pay To
		(159479),	--Orig Pay Ref
		(161798),	--Policy
		(161799),	--ClaimContract
		(161800),	--POLHExternalRef
		(161801),	--AffinityCode
		(161802),	--AffinityPartnerName
		(161803) 	--ValidFromDate
		
		INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, MatterID, ClientID, DetailValue)
		SELECT i.TableRowID, f.FieldID, @MatterID, @ClientID,
			CASE
				WHEN f.FieldID = 154486 THEN d.PayeeTitle							--Payee Title
				WHEN f.FieldID = 154487 THEN d.PayeeFirstName						--Payee First Name
				WHEN f.FieldID = 154488 THEN d.PayeeLastName						--Payee Last Name
				WHEN f.FieldID = 154490 THEN d.PayeeOrganisation					--Payee Organisation
				WHEN f.FieldID = 154491 THEN d.PayeeAddress1						--Payee Address 1
				WHEN f.FieldID = 154492 THEN d.PayeeAddress2						--Payee Address 2
				WHEN f.FieldID = 154493 THEN d.PayeeAddress3						--Payee Address 3
				WHEN f.FieldID = 154494 THEN d.Payeeaddress4						--Payee Address 4
				WHEN f.FieldID = 154495 THEN d.PayeeAddress5						--Payee Address 5
				WHEN f.FieldID = 154496 THEN d.PayeePostcode						--Payee Postcode
				WHEN f.FieldID = 154497 THEN d.PayeeCountry							--Payee Country
--				WHEN f.FieldID = 154498 THEN d.PayeeReference						--Payee Reference
--				WHEN f.FieldID = 154498 THEN 'TMP'+CAST(i.TableRowID as varchar(20))--Payee Reference (TemporaryFix)
				WHEN f.FieldID = 154498 THEN  CASE  WHEN d.PayeeReference like 'PH' THEN 'PH' + CAST(i.TableRowID  as VARCHAR(50))
											  WHEN d.PayeeReference like 'VT' THEN 'VT' + CAST(i.TableRowID  as VARCHAR(50)) 
											  WHEN d.PayeeReference like 'TP' THEN 'TP' + CAST(i.TableRowID  as VARCHAR(50))
											  WHEN d.PayeeReference like 'RV' THEN 'RV' + CAST(i.TableRowID  as VARCHAR(50)) ELSE CAST(i.TableRowID  as VARCHAR(50)) END --Payee Reference (PermanentFix)
				WHEN f.FieldID = 154499 THEN d.PayeeType							--Payee Type
				WHEN f.FieldID = 154500 THEN d.PolicyHolderTitle					--Policyholder Title
				WHEN f.FieldID = 154501 THEN d.PolicyHolderFirstName				--Policyholder First Name
				WHEN f.FieldID = 154502 THEN d.PolicyHolderLastName					--Policyholder Last Name
				WHEN f.FieldID = 154503 THEN d.PolicyHolderOrganisation				--Policyholder Organisation
				WHEN f.FieldID = 154504 THEN d.PolicyHolderAddress1					--Policyholder Address 1
				WHEN f.FieldID = 154505 THEN d.PolicyHolderAddress2					--Policyholder Address 2
				WHEN f.FieldID = 154506 THEN d.PolicyHolderAddress3					--Policyholder Address 3
				WHEN f.FieldID = 154507 THEN d.PolicyHolderAddress4					--Policyholder Address 4
				WHEN f.FieldID = 154508 THEN d.PolicyHolderAddress5					--Policyholder Address 5
				WHEN f.FieldID = 154509 THEN d.PolicyHolderPostcode					--Policyholder Postcode
				WHEN f.FieldID = 154510 THEN d.PolicyHolderCountry					--Policyholder Country
				WHEN f.FieldID = 154511 THEN d.PolicyHolderReference				--Policyholder Reference
				WHEN f.FieldID = 154514 THEN d.PolicyHolderType						--Policyholder Type
				WHEN f.FieldID = 154512 THEN d.PolicyCode							--Policy Code
				WHEN f.FieldID = 154513 THEN d.ClaimCode							--Claim Code
				WHEN f.FieldID = 154517 THEN d.PaymentType							--Payment Type
				WHEN f.FieldID = 154518 THEN d.PaymentAmount						--Payment Amount
				WHEN f.FieldID = 154519 THEN d.ClientIdentifier						--Client Identifier
				WHEN f.FieldID = 154520 THEN d.PaymentDate							--Payment Date
				WHEN f.FieldID = 158479 THEN d.DateCreated							--Date Created
				WHEN f.FieldID = 158534 THEN d.TableRowIDs							--TableRowIDs
				WHEN f.FieldID = 159005 THEN d.PetName								--PetName
				WHEN f.FieldID = 159407 THEN d.Approved								--Payment Approved
				WHEN f.FieldID = 159408 THEN d.PayTo								--Pay To 
				WHEN f.FieldID = 159479 THEN d.OriginalPaymentReference				--Orig Pay Ref 
				WHEN f.FieldID = 161798 THEN d.Policy								--Policy
				WHEN f.FieldID = 161799 THEN d.ClaimContract						--ClaimContract
				WHEN f.FieldID = 161800 THEN d.POLHExternalRef						--POLHExternalRef
				WHEN f.FieldID = 161801 THEN d.AffinityCode							--AffinityCode
				WHEN f.FieldID = 161802 THEN d.AffinityPartnerName					--AffinityPartnerName
				WHEN f.FieldID = 161803 THEN d.ValidFromDate						--ValidFromDate
			END AS Value
		FROM @Amounts d
		INNER JOIN @Inserted i ON d.ID = i.ID
		CROSS JOIN @Fields f 

	END
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_SaveSapPaymentData_ForApproval] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Payments_SaveSapPaymentData_ForApproval] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_SaveSapPaymentData_ForApproval] TO [sp_executeall]
GO
