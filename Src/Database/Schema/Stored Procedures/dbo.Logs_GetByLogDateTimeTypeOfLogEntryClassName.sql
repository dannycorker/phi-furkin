SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Logs table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_GetByLogDateTimeTypeOfLogEntryClassName]
(

	@LogDateTime datetime   ,

	@TypeOfLogEntry varchar (6)  ,

	@ClassName varchar (512)  
)
AS


				SELECT
					[LogID],
					[LogDateTime],
					[TypeOfLogEntry],
					[ClassName],
					[MethodName],
					[LogEntry],
					[ClientPersonnelID]
				FROM
					[dbo].[Logs] WITH (NOLOCK) 
				WHERE
										[LogDateTime] = @LogDateTime
					AND [TypeOfLogEntry] = @TypeOfLogEntry
					AND [ClassName] = @ClassName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_GetByLogDateTimeTypeOfLogEntryClassName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_GetByLogDateTimeTypeOfLogEntryClassName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_GetByLogDateTimeTypeOfLogEntryClassName] TO [sp_executeall]
GO
