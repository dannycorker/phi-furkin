SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =================================================================================================
-- Author:		Cathal Sherry
-- Create date: 2013-11-13
-- Description:	Take details from CustomerMain and return a custom error message and message type
-- PR 2013-12-12 Changed the tvp to the type thats returned by the customer main page.
-- PR 2013-12-12 Added caption
-- PR 2014-02-14 No Message returned if the ClientID is one of the following:
--					Red Star Financial Management Ltd	205
--					Strathern Associates				148
--					Money Boomerang						42
--					Aquarium CRM						72
--
-- Notes:
--			MessageType:	Dictates the buttons that are shown in the dialogue
--							0 - shows just the cancel button
--							1 - shows the ok and cancel button
--			Message:		This is the message to display
--			Caption:		This is the title displayed at the top of the message box
-- =================================================================================================
CREATE PROCEDURE [dbo].[_C00_CustomerMainMessage] 
	@ClientID INT,
	@ValueData dbo.tvpDetailValueData READONLY,
	@TableData dbo.tvpTablePropertyValue READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Message		VARCHAR(MAX),
			@MessageType	INT,
			@Caption		VARCHAR(MAX)	
	

	
	IF @ClientID not in (4,2,205,148,42,72,324)
	BEGIN
	
		SELECT @Message = 'Hello World', @MessageType = 0, @Caption = 'Values have not been saved:'
		
		SELECT @Message [MESSAGE], @MessageType [MessageType], @Caption [Caption]
	
	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomerMainMessage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CustomerMainMessage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomerMainMessage] TO [sp_executeall]
GO
