SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CreditCalculationCredential table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_GetByCreditCalculationCredentialID]
(

	@CreditCalculationCredentialID int   
)
AS


				SELECT
					[CreditCalculationCredentialID],
					[ClientID],
					[ClientPersonnelID],
					[SecretKey]
				FROM
					[dbo].[CreditCalculationCredential] WITH (NOLOCK) 
				WHERE
										[CreditCalculationCredentialID] = @CreditCalculationCredentialID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_GetByCreditCalculationCredentialID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_GetByCreditCalculationCredentialID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_GetByCreditCalculationCredentialID] TO [sp_executeall]
GO
