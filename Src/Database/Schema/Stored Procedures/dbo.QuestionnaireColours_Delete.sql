SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuestionnaireColours table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_Delete]
(

	@QuestionnaireColourID int   
)
AS


				DELETE FROM [dbo].[QuestionnaireColours] WITH (ROWLOCK) 
				WHERE
					[QuestionnaireColourID] = @QuestionnaireColourID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Delete] TO [sp_executeall]
GO
