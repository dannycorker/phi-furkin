SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Update an Outcome from eCatcher
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[UpdateOutcome]
	@OutcomeID INT,
	@ClientQuestionnaireID INT,
	@OutcomeName NVARCHAR(50),
	@OutcomeDescription TEXT,
	@SmsWhenFound BIT,
	@MobileNumber NVARCHAR(50),
	@EmailCustomer BIT,
	@CustomersEmail TEXT,
	@ThankYouPage TEXT, 
	@EmailFromAddress NVARCHAR(255),
	@SmsToCustomer NVARCHAR(160),
	@CustomersEmailSubject NVARCHAR(500),
	@ClientID INT,
	@EmailClient BIT,
	@ClientEmail NVARCHAR(255), 
	@WhoEdited INT
AS
BEGIN

	UPDATE Outcomes
	SET ClientQuestionnaireID = @ClientQuestionnaireID,
	OutcomeName = @OutcomeName,
	OutcomeDescription = @OutcomeDescription,
	SmsWhenFound = @SmsWhenFound,
	MobileNumber = @MobileNumber,
	EmailCustomer = @EmailCustomer,
	CustomersEmail = @CustomersEmail,
	ThankYouPage = @ThankYouPage,
	EmailFromAddress = @EmailFromAddress,
	SmsToCustomer = @SmsToCustomer,
	CustomersEmailSubject = @CustomersEmailSubject,
	ClientID = @ClientID,
	EmailClient = @EmailClient,
	ClientEmail = @ClientEmail, 
	WhoModified = @WhoEdited, 
	WhenModified = dbo.fn_GetDate_Local()
	WHERE OutcomeID = @OutcomeID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcome] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateOutcome] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcome] TO [sp_executeall]
GO
