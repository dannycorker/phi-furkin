SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-06
-- Description:	Get DetailValueHistory for a field by any ID (CaseID, ContactID, MatterID etc) 
--				ACE 2012-01-25 Added Order by and Nolocked
-- =============================================
CREATE PROCEDURE [dbo].[DetailValueHistory__GetByAnyID]
(
	@AnyID int, 
	@DetailFieldSubtypeID int, 
	@DetailFieldID int 
)
AS
BEGIN

	SET ANSI_NULLS OFF
	
	IF @DetailFieldSubtypeID = 1
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[LeadID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
			
	ELSE IF @DetailFieldSubtypeID = 2
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[MatterID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
	
	ELSE IF @DetailFieldSubtypeID = 10
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[CustomerID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
	
	ELSE IF @DetailFieldSubtypeID = 11
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[CaseID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
	
	ELSE IF @DetailFieldSubtypeID = 12
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[ClientID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
	
	ELSE IF @DetailFieldSubtypeID = 13
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[ClientPersonnelID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
	
	ELSE IF @DetailFieldSubtypeID = 14
	
		SELECT
			[DetailValueHistoryID],
			[ClientID],
			[DetailFieldID],
			[LeadOrMatter],
			[LeadID],
			[MatterID],
			[FieldValue],
			[WhenSaved],
			[ClientPersonnelID],
			[EncryptedValue],
			[ValueInt],
			[ValueMoney],
			[ValueDate],
			[ValueDateTime],
			[CustomerID],
			[CaseID],
			[ContactID],
			[ClientPersonnelDetailValueID]
		FROM
			[dbo].[DetailValueHistory] WITH (NOLOCK)
		WHERE
			[ContactID] = @AnyID 
		AND
			[DetailFieldID] = @DetailFieldID
			ORDER BY DetailValueHistoryID DESC
	
	SELECT @@ROWCOUNT
	
	SET ANSI_NULLS ON


END




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory__GetByAnyID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory__GetByAnyID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory__GetByAnyID] TO [sp_executeall]
GO
