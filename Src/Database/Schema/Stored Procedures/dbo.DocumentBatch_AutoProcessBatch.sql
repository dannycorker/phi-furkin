SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ==============================================================
-- Author:		Jim Green
-- Create date: 2013-01-22
-- Description:	Auto process a document batch.
--              This procedure is typically called as soon as a 
--				batch has been marked as uploaded.
--				2013-01-25 - Jan Wilson
--				Updated to check that the @AutomationUserID
--				is zero rather than null
--
-- Parameters:  @DocumentBatchId
--              The document batch to process
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatch_AutoProcessBatch]
	@DocumentBatchId INT

/*
	Get a list of possible events that can be added automatically.
	Upload detail values first, then check for any missing mandatory fields or events.
	If all is well, add the event automatically and link it to the uploaded document,
	otherwise update the notes column to show why a user has to action it manually.
*/

AS
BEGIN

	DECLARE @AutomationUserID INT = 0,
	@LoopCounter INT = 0,
	@LoopMax INT,
	@ValueCounter INT,
	@ValueMax INT,
	@MissingFieldCount INT,
	@MissingEventCount INT,
	@DocumentBatchDetailID INT,
	@EventTypeID INT,
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@Notes VARCHAR(500),
	@DetailFieldID INT,
	@DetailValue VARCHAR(2000),
	@MatterID INT,
	@NotesMessage VARCHAR(2000)


	SELECT TOP 1 @AutomationUserID  = StandalonePropertyValueInt 
	FROM StandaloneProperty WITH (NOLOCK)
	WHERE StandalonePropertyName = 'DocumentAutomationUser'

	IF (@AutomationUserID = 0)
	BEGIN
		SET @AutomationUserID = 19889
	END
	
	DECLARE @PossibleAutomatedEvents TABLE (
		DocumentBatchDetailID INT NOT NULL,
		DocumentBatchID INT NOT NULL,
		LeadDocumentID INT NOT NULL,
		EventTypeID INT NOT NULL,
		ClientID INT NOT NULL,
		CustomerID INT NOT NULL,
		LeadID INT NOT NULL,
		CaseID INT NOT NULL,
		Notes VARCHAR(500) NULL,
		MatterID INT NOT NULL
	)

	DECLARE @ValueIntoField TABLE (
		DetailFieldID INT NOT NULL,
		DetailValue VARCHAR(2000) NOT NULL 
	)

	/*
		Look for EventGroup records starting with "DocumentBatch" in the name.
		All events belonging to those groups can be auto-added but only if all other criteria are met.
	*/
	INSERT INTO @PossibleAutomatedEvents (DocumentBatchDetailID, DocumentBatchID, LeadDocumentID, EventTypeID, ClientID, CustomerID, LeadID, CaseID, MatterID)
	SELECT dbd.DocumentBatchDetailID, dbd.DocumentBatchID, dbd.LeadDocumentID, dbd.EventTypeId, m.ClientID, m.CustomerID, m.LeadID, m.CaseID, dbd.MatterID
	FROM DocumentBatchDetail dbd 
	INNER JOIN dbo.Matter m ON m.MatterID = dbd.MatterID
	INNER JOIN dbo.EventType et ON et.EventTypeID = dbd.EventTypeId
	INNER JOIN dbo.EventGroupMember egm ON egm.EventTypeID = dbd.EventTypeId
	INNER JOIN dbo.EventGroup eg ON eg.EventGroupID = egm.EventGroupID
	WHERE eg.EventGroupName LIKE 'DocumentBatch%'
	AND dbd.DocumentBatchID = @DocumentBatchId
	ORDER BY dbd.DocumentBatchDetailID ASC 
	
	SELECT @LoopMax = @@ROWCOUNT

	DECLARE @lastDocumentBatchDetailId INT = -1
	
	/* Now verify each DocumentBatchDetail record in turn */
	WHILE @LoopCounter < @LoopMax
	BEGIN
		
		SELECT @LoopCounter += 1,
		@MissingFieldCount = 0,
		@MissingEventCount = 0,
		@Notes = '',
		@ValueCounter = 0,
		@ValueMax = 0
		
		SELECT TOP 1 
		@DocumentBatchDetailID = p.DocumentBatchDetailID,
		@EventTypeID = p.EventTypeID,
		@CaseID = p.CaseID, 
		@ClientID = p.ClientID,
		@LeadID = p.LeadID,
		@CustomerID = p.CustomerID,
		@MatterID = p.MatterID
		FROM @PossibleAutomatedEvents p
		WHERE p.DocumentBatchDetailID > @lastDocumentBatchDetailId
		ORDER BY p.DocumentBatchDetailID ASC
		
		/* The DocumentBatch XML can contain field/value pairs so add these first before checking for missing mandatory fields */
		INSERT @ValueIntoField(DetailFieldID, DetailValue)
		EXEC dbo.DocumentBatchDetail_GetFieldsAndValues @DocumentBatchDetailID
		
		SELECT @ValueMax = COUNT(*) 
		FROM @ValueIntoField
		
		WHILE @ValueCounter < @ValueMax
		BEGIN
			
			SELECT TOP 1 @ValueCounter += 1,
			@DetailFieldID = v.DetailFieldID,
			@DetailValue = v.DetailValue
			FROM @ValueIntoField v
			
			EXEC dbo._C00_ValueIntoField null, @DetailFieldID, @DetailValue, @MatterID, @AutomationUserID
		END
		
		/* Check for missing mandatory fields */
		EXEC @MissingFieldCount = dbo.GetMissingMandatoryFields @EventTypeID, @LeadID, @CaseID, @CustomerID, @ClientID

		SELECT
			@EventTypeID EventTypeId,
			@LeadID LeadId,
			@CaseID CaseId,
			@CustomerID CustomerId,
			@ClientID ClientId,
			@MissingFieldCount MissingFieldCount,
			@DocumentBatchDetailID DocumentBatchDetailId
		
		IF @MissingFieldCount > 0
		BEGIN
			SELECT @Notes = 'Missing mandatory field count = ' + CAST(@MissingFieldCount AS VARCHAR) + ' : event cannot be added yet.'
		END
		
		/* Check for missing mandatory events */
		SELECT
			@EventTypeID EventTypeId,
			@LeadID LeadId,
			@CaseID CaseId
		EXEC @MissingEventCount = dbo.GetMissingMandatoryEvents @EventTypeID, @LeadID, @CaseID
		
		IF @MissingEventCount > 0
		BEGIN
			SELECT @Notes += ' Missing mandatory event count = ' + CAST(@MissingEventCount AS VARCHAR) + ' : event cannot be added yet.'
		END
		
		IF @Notes > ''
		BEGIN
			UPDATE @PossibleAutomatedEvents 
			SET Notes = @Notes
			WHERE DocumentBatchDetailID = @DocumentBatchDetailID
		END
		
		DELETE @ValueIntoField
		
		SET @lastDocumentBatchDetailId = @DocumentBatchDetailID
	END

	DECLARE @LastDocumentBatchDetailToCreateId INT = -1
	DECLARE @NextDocumentBatchDetailToCreateId INT = -1
	
	WHILE @LastDocumentBatchDetailToCreateId IS NOT NULL
	BEGIN
		SET @NextDocumentBatchDetailToCreateId = 0
		
		SELECT TOP 1 @NextDocumentBatchDetailToCreateId = dbd.DocumentBatchDetailID
		FROM @PossibleAutomatedEvents dbd
		WHERE dbd.DocumentBatchDetailID > @LastDocumentBatchDetailToCreateId 
		ORDER BY dbd.DocumentBatchDetailID ASC
		
		-- If no document was found
		IF (@NextDocumentBatchDetailToCreateId = 0)
		BEGIN
			-- SET @LastDocumentBatchDetailToCreateId to force an exit of the while loop
			SET @LastDocumentBatchDetailToCreateId = NULL
		END
		ELSE
			BEGIN
				SET @LastDocumentBatchDetailToCreateId = @NextDocumentBatchDetailToCreateId
				
				BEGIN TRY
					INSERT dbo.LeadEvent([ClientID], [LeadID], [WhenCreated], [WhoCreated], [Cost], [Comments], [EventTypeID], [NoteTypeID], [FollowupDateTime], [WhenFollowedUp], [AquariumEventType], [NextEventID], [CaseID], [LeadDocumentID], [NotePriority], [DocumentQueueID], [EventDeleted], [WhoDeleted], [DeletionComments], [ContactID], [BaseCost], [DisbursementCost], [DisbursementDescription], [ChargeOutRate], [UnitsOfEffort], [CostEnteredManually], [IsOnHold], [HoldLeadEventID]) 
					SELECT TOP 1 dbd.[ClientID], dbd.[LeadID], dbo.fn_GetDate_Local(), @AutomationUserID, 0 AS [Cost], 'Auto-created by the Document Batch Uploader', dbd.[EventTypeID], NULL AS [NoteTypeID], dbo.fnAddTimeUnitsToDate(dbo.fn_GetDate_Local(), et.FollowupTimeUnitsID, et.FollowupQuantity), NULL AS [WhenFollowedUp], et.[AquariumEventAfterEvent], NULL AS [NextEventID], dbd.[CaseID], dbd.[LeadDocumentID], NULL AS [NotePriority], NULL AS [DocumentQueueID], 0 AS [EventDeleted], NULL AS [WhoDeleted], NULL AS [DeletionComments], NULL AS [ContactID], 0 AS [BaseCost], 0 AS [DisbursementCost], NULL AS [DisbursementDescription], NULL AS [ChargeOutRate], NULL AS [UnitsOfEffort], NULL AS [CostEnteredManually], NULL AS [IsOnHold], NULL AS [HoldLeadEventID] 
					FROM @PossibleAutomatedEvents dbd 
					INNER JOIN dbo.EventType et ON et.EventTypeID = dbd.EventTypeId
					WHERE dbd.Notes IS NULL
					AND dbd.DocumentBatchDetailID = @LastDocumentBatchDetailToCreateId

					/* Update the DocumentBatchDetail entries that failed to have a lead event created as they have 
					   incomplete information
					*/
					UPDATE DocumentBatchDetail
					SET Notes = PAE.Notes
					FROM DocumentBatchDetail dbd
					INNER JOIN @PossibleAutomatedEvents PAE ON PAE.DocumentBatchDetailId = DBD.DocumentBatchDetailID
					WHERE PAE.Notes IS NOT NULL AND PAE.DocumentBatchDetailID = @LastDocumentBatchDetailToCreateId
					
					/* Update the DocumentBatchDetail entries that passed */
					UPDATE DocumentBatchDetail
					SET Uploaded = 1
					FROM DocumentBatchDetail dbd
					INNER JOIN @PossibleAutomatedEvents PAE ON PAE.DocumentBatchDetailId = DBD.DocumentBatchDetailID
					WHERE PAE.Notes IS NULL AND PAE.DocumentBatchDetailID = @LastDocumentBatchDetailToCreateId
				END TRY
				
				BEGIN CATCH
					SET @NotesMessage = ERROR_MESSAGE()
					
					UPDATE DocumentBatchDetail
					SET Notes = @NotesMessage
					FROM DocumentBatchDetail dbd
					WHERE dbd.DocumentBatchDetailID = @NextDocumentBatchDetailToCreateId
							
				END CATCH
			END
	END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_AutoProcessBatch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_AutoProcessBatch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_AutoProcessBatch] TO [sp_executeall]
GO
