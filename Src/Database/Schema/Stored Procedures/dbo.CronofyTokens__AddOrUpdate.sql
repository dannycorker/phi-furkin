SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: Add/Update Cronofy Token
-- Description:	2015-07-17
-- =============================================
CREATE PROCEDURE [dbo].[CronofyTokens__AddOrUpdate]
	@ClientID INT,
	@ClientPersonnelID INT,
	@AccessToken VARCHAR(100),
	@TokenType VARCHAR(20),
	@ExpiresIn INT,
	@RefreshToken VARCHAR(100),
	@Scope VARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (
		SELECT *
		FROM CronofyTokens c WITH (NOLOCK)
		WHERE c.ClientPersonnelID = @ClientPersonnelID
		)
	BEGIN
	
		/*Update the record*/
		UPDATE CronofyTokens 
		SET AccessToken = @AccessToken, TokenType = @TokenType, ExpiresIn = @ExpiresIn, RefreshToken = @RefreshToken, Scope = @Scope, DateTimeUpdated = dbo.fn_GetDate_Local()
		WHERE ClientPersonnelID = @ClientPersonnelID
	
	END
	ELSE
	BEGIN
	
		/*Insert the record*/
		INSERT INTO CronofyTokens (ClientID, ClientPersonnelID, AccessToken, TokenType, ExpiresIn, RefreshToken, Scope, DateTimeUpdated)
		VALUES (@ClientID, @ClientPersonnelID, @AccessToken, @TokenType, @ExpiresIn, @RefreshToken, @Scope, dbo.fn_GetDate_Local())
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CronofyTokens__AddOrUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CronofyTokens__AddOrUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CronofyTokens__AddOrUpdate] TO [sp_executeall]
GO
