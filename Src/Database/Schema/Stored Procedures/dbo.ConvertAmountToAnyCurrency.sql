SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-12
-- Description:	Convert an amount from any currency to any currency.
-- =============================================
CREATE PROCEDURE [dbo].[ConvertAmountToAnyCurrency] 
	@AmountInFromCurrency decimal(18, 10),
	@ToCurrencyID int = 1,					/* Default to GBP */
	@FromCurrencyID int = 1,				/* Default to GBP */
	@ConversionDate datetime = null,		/* Leave this empty to get the latest rates */   
	@UseNextAvailableIfNotFound bit = 1		/* If user asks for data before 2011, show them the data from January 2011 by default */
AS
BEGIN
	SET NOCOUNT ON;

	/*
		1. Convert this to GBP from EUR
		2. Convert this to EUR from GBP
		3. Convert this to USD from EUR
		4. Convert this to GBP from GBP
		
		Do all of the above at:
		(a) the latest rate we have
		(b) the latest rate we had prior to datetime ddddddddtttttttttt
		
		Return suitable codes or messages if no rate found, or rate ridiculously out of date
	*/
	DECLARE 
	@AmountInToCurrency decimal(18, 10) = @AmountInFromCurrency,
	@FromRate decimal(18, 10), 
	@ToRate decimal(18, 10), 
	@FromCurrencyRateLastUpdated datetime,
	@ToCurrencyRateLastUpdated datetime,
	@ErrorMessages varchar(50) = '', 
	@NearestFromCurrencyRateID int, 
	@NearestToCurrencyRateID int 
	
	/* 
		If FromCurrency is not GBP then get the appropriate conversion rate to convert it to GBP now. 
		Example:  100 EUR on Jan 12th 2011 was worth 83.2200000000 GBP
	*/
	IF @FromCurrencyID <> 1
	BEGIN
		
		/* 
			Get the latest FROM rate by default when no particular datetime is specified. 
		*/
		IF @ConversionDate IS NULL
		BEGIN
		
			SELECT @FromRate = cr.ToGBPRate, @FromCurrencyRateLastUpdated = cr.ConversionDate 
			FROM dbo.CurrencyRate cr WITH (NOLOCK) 
			WHERE cr.CurrencyId = @FromCurrencyID 
			AND cr.IsLatest = 1 
			
		END
		ELSE
		BEGIN
	
			/* 
				If a particular datetime is specified, get the FROM rate just before that moment in time.
			*/
			SELECT TOP 1 @NearestFromCurrencyRateID = cr.CurrencyRateID 
			FROM dbo.CurrencyRate cr WITH (NOLOCK) 
			WHERE cr.CurrencyId = @FromCurrencyID 
			AND cr.ConversionDate <= @ConversionDate 
			ORDER BY cr.ConversionDate DESC 
			
			/* 
				If we have no FROM rate just before that moment in time, and if the flag is set to 
				use the nearest one available, look forwards in time from the specified datetime.
			*/
			IF @NearestFromCurrencyRateID IS NULL AND @UseNextAvailableIfNotFound = 1
			BEGIN
				SELECT TOP 1 @NearestFromCurrencyRateID = cr.CurrencyRateID 
				FROM dbo.CurrencyRate cr WITH (NOLOCK) 
				WHERE cr.CurrencyId = @FromCurrencyID 
				AND cr.ConversionDate >= @ConversionDate 
				ORDER BY cr.ConversionDate ASC 
			END
			
			/*
				If we have a nearest FROM rate after all that, get it now.
			*/
			IF @NearestFromCurrencyRateID IS NOT NULL
			BEGIN
				SELECT @FromRate = cr.ToGBPRate, @FromCurrencyRateLastUpdated = cr.ConversionDate 
				FROM dbo.CurrencyRate cr WITH (NOLOCK) 
				WHERE cr.CurrencyRateId = @NearestFromCurrencyRateID 
			END
			
		END
		
		/*
			If a suitable FROM rate was found, apply it now, otherwise store an error message.
		*/		
		IF @FromRate IS NOT NULL
		BEGIN
			SELECT @AmountInToCurrency = @AmountInFromCurrency * @FromRate
		END
		ELSE
		BEGIN
			SELECT @AmountInToCurrency = NULL, @FromRate = NULL, @FromCurrencyRateLastUpdated = NULL, @ErrorMessages = 'From Rate not found! '
		END
	END

	
	/* 
		If ToCurrency is not GBP then get the appropriate conversion rate to convert to it from GBP now. 
		Example:  83.2200000000 GBP on Jan 12th 2011 was worth 165.0257295023 USD
	*/
	IF @ToCurrencyID <> 1
	BEGIN
		
		/* 
			Get the latest TO rate by default when no particular datetime is specified. 
		*/
		IF @ConversionDate IS NULL
		BEGIN
		
			SELECT @ToRate = cr.FromGBPRate, @ToCurrencyRateLastUpdated = cr.ConversionDate 
			FROM dbo.CurrencyRate cr WITH (NOLOCK) 
			WHERE cr.CurrencyId = @ToCurrencyID 
			AND cr.IsLatest = 1 
			
		END
		ELSE
		BEGIN
	
			/* 
				If a particular datetime is specified, get the TO rate just before that moment in time.
			*/
			SELECT TOP 1 @NearestToCurrencyRateID = cr.CurrencyRateID 
			FROM dbo.CurrencyRate cr WITH (NOLOCK) 
			WHERE cr.CurrencyId = @ToCurrencyID 
			AND cr.ConversionDate <= @ConversionDate 
			ORDER BY cr.ConversionDate DESC 
			
			/* 
				If we have no TO rate just before that moment in time, and if the flag is set to 
				use the nearest one available, look forwards in time from the specified datetime.
			*/
			IF @NearestToCurrencyRateID IS NULL AND @UseNextAvailableIfNotFound = 1
			BEGIN
				SELECT TOP 1 @NearestToCurrencyRateID = cr.CurrencyRateID 
				FROM dbo.CurrencyRate cr WITH (NOLOCK) 
				WHERE cr.CurrencyId = @ToCurrencyID 
				AND cr.ConversionDate >= @ConversionDate 
				ORDER BY cr.ConversionDate ASC 
			END
			
			/*
				If we have a nearest TO rate after all that, get it now.
			*/
			IF @NearestToCurrencyRateID IS NOT NULL
			BEGIN
				SELECT @ToRate = cr.FromGBPRate, @ToCurrencyRateLastUpdated = cr.ConversionDate 
				FROM dbo.CurrencyRate cr WITH (NOLOCK) 
				WHERE cr.CurrencyRateId = @NearestToCurrencyRateID 
			END
			
		END
		
		/*
			If a suitable TO rate was found, apply it now, otherwise store an error message.
		*/		
		IF @ToRate IS NOT NULL
		BEGIN
			SELECT @AmountInToCurrency = @AmountInToCurrency * @ToRate
		END
		ELSE
		BEGIN
			SELECT @AmountInToCurrency = NULL, @ToRate = NULL, @ToCurrencyRateLastUpdated = NULL, @ErrorMessages += 'To Rate not found!'
		END
		
	END
	
	/*
		Select out the converted amount so the calling code can use it, along with some helpful information
	*/
	SELECT @AmountInToCurrency as AmountInToCurrency,
	@ErrorMessages as ErrorMessages,
	@FromCurrencyRateLastUpdated as FromCurrencyRateLastUpdated,
	@ToCurrencyRateLastUpdated as ToCurrencyRateLastUpdated
	
	IF @ErrorMessages = ''
	BEGIN
		RETURN 1
	END
	ELSE
	BEGIN
		RETURN -1
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ConvertAmountToAnyCurrency] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ConvertAmountToAnyCurrency] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ConvertAmountToAnyCurrency] TO [sp_executeall]
GO
