SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		 Alex Elger
-- Create date:  2011-06-17
-- Description:	 Create Fields for all leads, customers etc
-- CS 2016-03-01 Lock Case updates to the correct lead type
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateDetailValues]
@DetailFieldID int,
@DetailValue varchar(2000) = '',
@TopXRows int = '1000'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @ClientID int,
			@DetailFieldSubType int,
			@LeadTypeID int,
			@DetailFieldPageID int,
			@DetailFieldType int
			
	Select  @DetailFieldType = QuestionTypeID,
			@DetailFieldSubType = d.LeadOrMatter, 
			@ClientID = d.ClientID, 
			@LeadTypeID = d.LeadTypeID, 
			@DetailFieldPageID = d.DetailFieldPageID
	From DetailFields d with (nolock) 
	WHERE d.DetailFieldID = @DetailFieldID

	IF @DetailFieldSubType = 1
	BEGIN
	
		insert into LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
		select top (@TopXRows) @ClientID, l.LeadID, @DetailFieldID, @DetailValue
		from Lead l WITH (NOLOCK) 
		WHERE l.LeadTypeID = @LeadTypeID
		and l.ClientID = @ClientID 
		and not exists ( SELECT * 
		                 FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
		                 WHERE ldv.LeadID = l.LeadID 
		                 and ldv.DetailFieldID = @DetailFieldID 
		                 and ldv.ClientID = @ClientID 
		               )

		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END
	
	IF @DetailFieldSubType = 2
	BEGIN
	
		insert into MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		SELECT top (@TopXRows) @ClientID, l.LeadID, m.MatterID, @DetailFieldID, @DetailValue 
		from Lead l with (nolock)
		INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID
		where l.LeadTypeID = @LeadTypeID
		and l.ClientID = @ClientID 
		and not exists (
			select *
			from MatterDetailValues mdv with (nolock)
			where mdv.MatterID = m.MatterID
			and mdv.DetailFieldID = @DetailFieldID
			and mdv.ClientID = @ClientID
		)

		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END

	IF @DetailFieldSubType = 10 /*Customers*/
	BEGIN
	
		insert into CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		SELECT top (@TopXRows) @ClientID, c.CustomerID, @DetailFieldID, @DetailValue 
		from Customers c with (nolock)
		where c.ClientID = @ClientID
		and not exists (
			select *
			from CustomerDetailValues cdv with (nolock)
			where cdv.CustomerID = c.CustomerID
			and cdv.DetailFieldID = @DetailFieldID
			and cdv.ClientID = @ClientID
		)

		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END

	IF @DetailFieldSubType = 11 /*Cases*/
	BEGIN
	
		insert into CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue)
		SELECT top (@TopXRows) @ClientID, c.CaseID, @DetailFieldID, @DetailValue 
		from Cases c with (nolock)
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = c.LeadID AND l.LeadTypeID = @LeadTypeID /*CS 2016-03-01*/
		where c.ClientID = @ClientID
		and not exists (
			select *
			from CaseDetailValues cdv with (nolock)
			where cdv.CaseID = c.CaseID
			and cdv.DetailFieldID = @DetailFieldID
			and cdv.ClientID = @ClientID
		)

		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END

	IF @DetailFieldSubType IN (6,8) -- basic table
	BEGIN
	
		insert into TableDetailValues (TableRowID,DetailFieldID,DetailValue,LeadID,MatterID,ClientID,CustomerID) -- dcm added customerid 8/5/2014
		SELECT top (@TopXRows) tr.TableRowID,@DetailFieldID,@DetailValue,tr.LeadID,tr.MatterID,tr.ClientID,tr.CustomerID 
		FROM TableRows tr WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID=tr.DetailFieldID and df.TableDetailFieldPageID=@DetailFieldPageID  
		and not exists (
			select * 
			from TableDetailValues tdv1 WITH (NOLOCK) 
			WHERE tdv1.TableRowID=tr.TableRowID 
			and tdv1.DetailFieldID=@DetailFieldID 
		)

		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END

	IF @DetailFieldSubType IN (4) -- resource
	BEGIN
	
		UPDATE ResourceList
		SET DetailFieldPageID = @DetailFieldPageID
		FROM ResourceList
		INNER JOIN ResourceListDetailValues rldv ON rldv.ResourceListID = ResourceList.ResourceListID
		INNER JOIN DetailFields df ON df.DetailFieldID = rldv.DetailFieldID and df.DetailFieldPageID = @DetailFieldPageID
		WHERE ResourceList.DetailFieldPageID IS NULL
		
		insert into ResourceListDetailValues (ResourceListID, ClientID, LeadOrMatter, DetailFieldID, DetailValue)
		SELECT top (@TopXRows) rl.ResourceListID, rl.ClientID, 4 ,@DetailFieldID, @DetailValue
		FROM ResourceList rl WITH (NOLOCK)
		WHERE rl.DetailFieldPageID = @DetailFieldPageID
		and not exists (
			select * 
			from ResourceListDetailValues rldv1 WITH (NOLOCK) 
			WHERE rldv1.ResourceListID=rl.ResourceListID 
			and rldv1.DetailFieldID=@DetailFieldID 
		)

		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END
	
	IF @DetailFieldSubType IN (13) /*ClientPersonnel*/ /*CS 2012-04-26*/
	BEGIN
		
		insert into ClientPersonnelDetailValues (ClientPersonnelID, ClientID,DetailFieldID, DetailValue)
		SELECT top (@TopXRows) cp.ClientPersonnelID, @ClientID, @DetailFieldID, @DetailValue
		FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
		WHERE cp.ClientID = @ClientID
		and not exists ( SELECT * 
		                 FROM dbo.ClientPersonnelDetailValues cdv WITH (NOLOCK) 
		                 WHERE cdv.ClientPersonnelID = cp.ClientPersonnelID 
		                 and cdv.DetailFieldID = @DetailFieldID )
		
		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END

	IF @DetailFieldSubType IN (14) /*Contact*/ /*CS 2016-03-15*/
	BEGIN
		
		insert into ContactDetailValues (ContactID, ClientID,DetailFieldID, DetailValue)
		SELECT top (@TopXRows) co.ContactID, @ClientID, @DetailFieldID, @DetailValue
		FROM dbo.Contact co WITH (NOLOCK) 
		WHERE co.ClientID = @ClientID
		and not exists ( SELECT * 
		                 FROM dbo.ContactDetailValues cdv WITH (NOLOCK) 
		                 WHERE cdv.ContactID = co.ContactID
		                 and cdv.DetailFieldID = @DetailFieldID )
		
		Print  'Records inserted ' + CONVERT(VARCHAR,@@ROWCOUNT )

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDetailValues] TO [sp_executeall]
GO
