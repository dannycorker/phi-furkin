SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadDocument table
-- JWG 2012-07-12 Use all fields for LeadDocument again.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_Update]
(
	@LeadDocumentID INT OUTPUT,
	@ClientID INT,
	@LeadID INT,
	@DocumentTypeID INT,
	@LeadDocumentTitle VARCHAR(1000),
	@UploadDateTime DATETIME,
	@WhoUploaded INT,
	@DocumentBLOB IMAGE,
	@FileName VARCHAR(255),
	@EmailBLOB IMAGE,
	@DocumentFormat VARCHAR(24) ,
	@EmailFrom VARCHAR(512),
	@EmailTo VARCHAR(MAX),
	@CcList VARCHAR(MAX),
	@BccList VARCHAR(MAX),
	@ElectronicSignatureDocumentKey VARCHAR(50),
	@Encoding VARCHAR(50),
	@ContentFormat VARCHAR(50),
	@ZipFormat VARCHAR(50), 
	@DocumentBlobSize INT = NULL, 
	@EmailBlobSize INT = NULL, 
	@DocumentDatabaseID INT = NULL, 
	@WhenArchived DATETIME = NULL, 
	@DocumentTypeVersionID INT = NULL 
)
AS
BEGIN
	
	-- JWG 2012-07-12 Use all fields for LeadDocument again. Temp size calc until app sends these through.
	SELECT @DocumentBlobSize = DATALENGTH(@DocumentBLOB), @EmailBlobSize = DATALENGTH(@EmailBLOB)
	
	UPDATE
		[dbo].[LeadDocumentFS]
	SET
		[ClientID] = @ClientID
		,[LeadID] = @LeadID
		,[DocumentTypeID] = @DocumentTypeID
		,[LeadDocumentTitle] = @LeadDocumentTitle
		,[UploadDateTime] = @UploadDateTime
		,[WhoUploaded] = @WhoUploaded
		,[DocumentBLOB] = @DocumentBLOB
		,[FileName] = @FileName
		,[EmailBLOB] = @EmailBLOB
		,[DocumentFormat] = @DocumentFormat
		,[EmailFrom] = @EmailFrom
		,[EmailTo] = @EmailTo
		,[CcList] = @CcList
		,[BccList] = @BccList
		,[ElectronicSignatureDocumentKey] = @ElectronicSignatureDocumentKey
		,[Encoding] = @Encoding
		,[ContentFormat] = @ContentFormat
		,[ZipFormat] = @ZipFormat

	WHERE 
		[LeadDocumentID] = @LeadDocumentID 
		
	/* 
		JWG 2012-07-12 Use all fields for LeadDocument again.
	*/
	UPDATE
		[dbo].[LeadDocument]
	SET
		[ClientID] = @ClientID
		,[LeadID] = @LeadID
		,[DocumentTypeID] = @DocumentTypeID
		,[LeadDocumentTitle] = @LeadDocumentTitle
		,[UploadDateTime] = @UploadDateTime
		,[WhoUploaded] = @WhoUploaded
		--,[DocumentBLOB] = @DocumentBLOB
		,[FileName] = @FileName
		--,[EmailBLOB] = @EmailBLOB
		,[DocumentFormat] = @DocumentFormat
		,[EmailFrom] = @EmailFrom
		,[EmailTo] = @EmailTo
		,[CcList] = @CcList
		,[BccList] = @BccList
		,[ElectronicSignatureDocumentKey] = @ElectronicSignatureDocumentKey
		,[Encoding] = @Encoding
		,[ContentFormat] = @ContentFormat
		,[ZipFormat] = @ZipFormat
		,[DocumentBlobSize] = @DocumentBlobSize 
		,[EmailBlobSize] = @EmailBlobSize 
		,[DocumentDatabaseID] = @DocumentDatabaseID 
		,[WhenArchived] = @WhenArchived 
		,[DocumentTypeVersionID] = @DocumentTypeVersionID 
	WHERE 
		[LeadDocumentID] = @LeadDocumentID 

			
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Update] TO [sp_executeall]
GO
