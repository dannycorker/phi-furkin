SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-01-01
-- Description:	Returns some ClientPersonnel info to the main app based on the email address passed in
-- 2013-04-18 JWG Added @ThirdPartySystemID = 0 to cater for users who have the same emailaddress for (eg) the main app and Chorus HR
-- 2014-04-10 PR Added memorable word information
-- =============================================
CREATE PROCEDURE [dbo].[GetClientPersonViaEmailAddress] 
	@EmailAddress NVARCHAR(255), 
	@ThirdPartySystemID INT = 0
AS
BEGIN
SELECT 

	cp.ClientPersonnelID,
	cp.ClientID,
	cp.ClientOfficeID,
	cp.TitleID,
	cp.FirstName,
	cp.MiddleName,
	cp.LastName,
	cp.JobTitle,
	cp.Password,
	cp.MobileTelephone,
	cp.HomeTelephone,
	cp.OfficeTelephone,
	cp.OfficeTelephoneExtension,
	cp.EmailAddress,
	cp.Salt,
	cp.AttemptedLogins,
	cp.AccountDisabled,
	cp.MemorableWord,
	cp.MemorableWordSalt,
	cp.MemorableWordAttempts,
	cp.PendingActivation
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 	WHERE cp.EmailAddress = @EmailAddress 	AND cp.ThirdPartySystemID = @ThirdPartySystemID END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonViaEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientPersonViaEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonViaEmailAddress] TO [sp_executeall]
GO
