SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ValidationCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriterias_Delete]
(

	@ValidationCriteriaID int   
)
AS


				DELETE FROM [dbo].[ValidationCriterias] WITH (ROWLOCK) 
				WHERE
					[ValidationCriteriaID] = @ValidationCriteriaID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriterias_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Delete] TO [sp_executeall]
GO
