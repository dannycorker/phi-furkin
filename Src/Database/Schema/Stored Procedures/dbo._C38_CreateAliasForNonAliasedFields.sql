SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_CreateAliasForNonAliasedFields]
(
@LeadEventID int
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@ClientID int,
		@myERROR int,
		@NotDone varchar(2000)

	Select @ClientID = ldv.detailvalue
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	Inner join leaddetailvalues ldv on ldv.leadid = lead.leadid and ldv.detailfieldid = 6453
	where leadevent.leadeventid = @LeadEventID 


	insert into detailfieldalias ([ClientID],[LeadTypeID],[DetailFieldID],[DetailFieldAlias])
	select distinct Clientid, LeadTypeID, DetailFieldID, FieldName
	from DetailFields
	where clientid = @clientid 
	and leadormatter in (1,2)
	and Enabled = 1
	and not exists
	(
	select *
	from DetailFieldAlias
	Where (DetailFieldAlias.DetailFieldID = DetailFields.DetailFieldID or (DetailFieldAlias.DetailFieldAlias = DetailFields.FieldName and DetailFieldAlias.LeadTypeID = DetailFields.LeadTypeID))
--	or (DetailFieldAlias.DetailFieldAlias = DetailFields.FieldName and DetailFieldAlias.LeadTypeID = DetailFields.LeadTypeID)
	)
	And Not Exists(
	select df.fieldname, count(*)
	from detailfields df
	where df.clientid = @ClientID
	and df.fieldname = detailfields.fieldname
	and df.leadtypeid = detailfields.leadtypeid
	and df.leadormatter in (1,2)
	group by df.fieldname
	having count(*) >1
	)
UNION ALL
	select distinct Clientid, NULL, DetailFieldID, FieldName
	from DetailFields
	where clientid = @clientid 
	and leadormatter in (4)
	and Enabled = 1
	and not exists
	(
	select *
	from DetailFieldAlias
	Where (DetailFieldAlias.DetailFieldID = DetailFields.DetailFieldID or (DetailFieldAlias.DetailFieldAlias = DetailFields.FieldName and DetailFieldAlias.LeadTypeID = DetailFields.LeadTypeID))
	)
	And Not Exists(
	select df.fieldname, count(*)
	from detailfields df
	where df.clientid = @ClientID
	and df.fieldname = detailfields.fieldname
	and df.leadormatter in (4)
	group by df.fieldname
	having count(*) >1
	)

	Select @NotDone = 'Failed to alias the following fields: <Br>'

	Select @NotDone =  @NotDone + 'Field: ' +  df.fieldname + ' DetailFieldID: ' + convert(varchar,df.detailfieldid) + ' LeadTypeID: ' + convert(varchar,df.leadtypeid) + ' LeadOrMatter: ' + convert(varchar,df.leadormatter) + '<Br>' + char(13)
	from detailfields df
	where not exists(
	select *
	from detailfieldalias
	where detailfieldid = df.detailfieldid
	and leadormatter in (1,2,4)
	)
	and df.clientid = @ClientID
	and df.enabled = 1
	order by df.FieldName

	Update LeadEvent
	Set Comments = @NotDone
	Where LeadEventID = @LeadEventID


END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateAliasForNonAliasedFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_CreateAliasForNonAliasedFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateAliasForNonAliasedFields] TO [sp_executeall]
GO
