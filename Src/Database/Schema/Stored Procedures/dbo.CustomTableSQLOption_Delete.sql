SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomTableSQLOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomTableSQLOption_Delete]
(

	@CustomTableSQLOptionID int   
)
AS


				DELETE FROM [dbo].[CustomTableSQLOption] WITH (ROWLOCK) 
				WHERE
					[CustomTableSQLOptionID] = @CustomTableSQLOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQLOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Delete] TO [sp_executeall]
GO
