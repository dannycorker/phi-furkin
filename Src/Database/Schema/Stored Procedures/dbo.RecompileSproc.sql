SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2014-02-03
-- Description:	Mark one proc for recompilation, or list all known troublemakers if no name is passed in
-- =============================================
CREATE PROCEDURE [dbo].[RecompileSproc]
	@SprocName SYSNAME = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Statement VARCHAR(500)
	
	DECLARE @KnownTroublemakers TABLE (SprocName SYSNAME)

	IF @SprocName IS NULL
	BEGIN
		INSERT INTO @KnownTroublemakers (SprocName) VALUES 
		 ('DocumentZipInformation__GetByClientIDForGrid')
		,('DocumentQueue__EnhancedGetByClientLeadTypeDocTypeClientPersonnel')
		,('FieldParser_ParseTemplate')
		,('fnGetDVAsMoney')

		SELECT 'Pass in a proc name to mark just that one for recompilation' AS [Option]
		
		SELECT 'EXEC RecompileAllSprocs;' AS [Sledgehammer Approach]
		
		SELECT 'EXEC sp_recompile ' + k.SprocName + ';' AS [Known Troublemakers]
		FROM @KnownTroublemakers k 
		ORDER BY k.SprocName
	END
	ELSE
	BEGIN
		
		SELECT @Statement = 'EXEC sp_recompile ' + @SprocName + ';'
		
		EXEC(@Statement)
		
	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileSproc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RecompileSproc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileSproc] TO [sp_executeall]
GO
