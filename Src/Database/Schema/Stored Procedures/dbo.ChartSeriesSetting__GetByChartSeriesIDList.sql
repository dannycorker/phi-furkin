SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01 May 2008

-- Created By:  Chris Townsend
-- Purpose: Takes a list of ChartSeriesIDs and returns all ChartSeriesSettings 
			for those ChartSeries
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting__GetByChartSeriesIDList]
(
	@ChartSeriesIDList varchar(100)
)

AS

SELECT  css.[ChartSeriesSettingID]
		,css.[ChartSeriesID]
		,css.[SettingName]
		,css.[SettingValue]
		,css.[ClientID]

FROM dbo.fnTableOfIDsFromCSV (@ChartSeriesIDList) idList

INNER JOIN ChartSeriesSetting css ON css.ChartSeriesID = idList.AnyID






GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting__GetByChartSeriesIDList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting__GetByChartSeriesIDList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting__GetByChartSeriesIDList] TO [sp_executeall]
GO
