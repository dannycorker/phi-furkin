SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-01
-- Description:	Cancel all Group and User Rights editing from temp/work tables
-- =============================================
CREATE PROCEDURE [dbo].[CancelAllRightsForEditing]
AS
BEGIN
	SET NOCOUNT ON;

	DELETE UserRightsDynamicEditing 

	DELETE UserFunctionControlEditing

	DELETE GroupRightsDynamicEditing 

	DELETE GroupFunctionControlEditing
	
	DELETE GroupAndUserEditingControl

END








GO
GRANT VIEW DEFINITION ON  [dbo].[CancelAllRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CancelAllRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CancelAllRightsForEditing] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[CancelAllRightsForEditing] TO [sp_executehelper]
GO
