SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-10-01
-- Description:	Take an input string and location and create a series of helper fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateHeaderFieldsInBulkFromNarrative]
(
	 @Narrative				VARCHAR(MAX)
	,@StartOrder			INT 
	,@DetailFieldPageID		INT 
	,@DetailFieldStyleID	INT = NULL
	,@ClientPersonnelID		INT
	,@FieldPrefix			VARCHAR(20) = ''
	,@Debug					BIT = 1
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE	 @Delimiter		VARCHAR(2000) = CHAR(13)+CHAR(10)
			,@SubString		VARCHAR(MAX)
			,@Count			INT
			,@LeadTypeID	INT
			,@ClientID		INT
			,@LeadOrMatter	INT

	DECLARE  @OutputTable TABLE ( FieldOrder INT Identity, FieldCaption VARCHAR(100) )
	DECLARE  @CreatedFields dbo.tvpInt

	/*Pick up and validate all necessary values*/
	SELECT @ClientID = dp.ClientID, @LeadTypeID = dp.LeadTypeID, @LeadOrMatter = dp.LeadOrMatter, @DetailFieldStyleID = st.DetailFieldStyleID
	FROM DetailFieldPages dp WITH ( NOLOCK ) 
	INNER JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientID = dp.ClientID AND cp.ClientPersonnelID = @ClientPersonnelID
	LEFT JOIN DetailFieldStyle st WITH ( NOLOCK ) on st.ClientID = dp.ClientID AND st.DetailFieldStyleID = @DetailFieldStyleID
	WHERE dp.DetailFieldPageID = @DetailFieldPageID
	AND LEN(@Narrative) > 0
	AND @StartOrder > 0
	AND ( st.DetailFieldStyleID is not NULL or @DetailFieldStyleID is NULL )

	IF @@ROWCOUNT = 0
	BEGIN
		RAISERROR( 'Error: No records found when validating input', 16, 1 )
		PRINT 'SELECT *
	FROM DetailFieldPages dp WITH ( NOLOCK ) 
	INNER JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientID = dp.ClientID AND cp.ClientPersonnelID = ' + ISNULL(CONVERT(VARCHAR,@ClientPersonnelID),'NULL') + '
	LEFT JOIN DetailFieldStyle st WITH ( NOLOCK ) on st.ClientID = dp.ClientID AND st.DetailFieldStyleID = ' + ISNULL(CONVERT(VARCHAR,@DetailFieldStyleID),'NULL') + '
	WHERE dp.DetailFieldPageID = ' + ISNULL(CONVERT(VARCHAR,@DetailFieldPageID),'NULL') + '
	AND LEN('''+ ISNULL(@Narrative,'') + ''') > 0
	AND '+ ISNULL(CONVERT(VARCHAR,@StartOrder),'NULL') +' > 0
	AND ( st.DetailFieldStyleID is not NULL or '+ ISNULL(CONVERT(VARCHAR,@DetailFieldStyleID),'NULL') +' is NULL )'
		RETURN
	END

	WHILE LEN(@Narrative) > 0
	BEGIN
		SELECT @SubString = LEFT(@Narrative,dbo.fnMinOf(100,ISNULL(NULLIF(CHARINDEX(@Delimiter,@Narrative),0),LEN(@Narrative))))
		
		SELECT @Narrative = RIGHT(@Narrative,LEN(@Narrative)-LEN(@SubString))
		
		INSERT @OutputTable ( FieldCaption )
		VALUES ( RTRIM(LTRIM(REPLACE(@SubString,CHAR(10),''))) )
	END

	SELECT @Count = COUNT(*)
	FROM @OutputTable ot 

	IF @Debug = 1
	BEGIN
		SELECT * 
		FROM @OutputTable
	END
	ELSE
	BEGIN
		UPDATE df
		SET FieldOrder = df.FieldOrder + @StartOrder + @Count
		FROM DetailFields df WITH ( NOLOCK ) 
		WHERE df.DetailFieldPageID = @DetailFieldPageID 
		AND df.FieldOrder >= @StartOrder

		INSERT DetailFields ( ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, Hidden, WhoCreated, WhenCreated, DetailFieldStyleID )
			OUTPUT	inserted.DetailFieldID
			INTO	@CreatedFields (AnyID)
		SELECT @ClientID, @LeadOrMatter, LEFT(@FieldPrefix + o.FieldCaption,50), o.FieldCaption, 25, 0, 0, @LeadTypeID, 1, @DetailFieldPageID, o.FieldOrder+(@StartOrder-1), 0, 0, @ClientPersonnelID, CURRENT_TIMESTAMP, @DetailFieldStyleID
		FROM @OutputTable o

		SELECT * 
		FROM @CreatedFields cf 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = cf.AnyID
	END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateHeaderFieldsInBulkFromNarrative] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateHeaderFieldsInBulkFromNarrative] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateHeaderFieldsInBulkFromNarrative] TO [sp_executeall]
GO
