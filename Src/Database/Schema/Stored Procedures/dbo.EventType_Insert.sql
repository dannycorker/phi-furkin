SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_Insert]
(

	@EventTypeID int    OUTPUT,

	@ClientID int   ,

	@EventTypeName varchar (50)  ,

	@EventTypeDescription varchar (250)  ,

	@Enabled bit   ,

	@UnitsOfEffort int   ,

	@FollowupTimeUnitsID int   ,

	@FollowupQuantity int   ,

	@AvailableManually bit   ,

	@StatusAfterEvent int   ,

	@AquariumEventAfterEvent int   ,

	@EventSubtypeID int   ,

	@DocumentTypeID int   ,

	@LeadTypeID int   ,

	@AllowCustomTimeUnits bit   ,

	@InProcess bit   ,

	@KeyEvent bit   ,

	@UseEventCosts bit   ,

	@UseEventUOEs bit   ,

	@UseEventDisbursements bit   ,

	@UseEventComments bit   ,

	@SignatureRequired bit   ,

	@SignatureOverride bit   ,

	@VisioX decimal (18, 8)  ,

	@VisioY decimal (18, 8)  ,

	@AquariumEventSubtypeID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@FollowupWorkingDaysOnly bit   ,

	@CalculateTableRows bit   ,

	@SourceID int   ,

	@SmsGatewayID int   ,

	@IsShared bit   ,

	@SocialFeedID int   
)
AS


				
				INSERT INTO [dbo].[EventType]
					(
					[ClientID]
					,[EventTypeName]
					,[EventTypeDescription]
					,[Enabled]
					,[UnitsOfEffort]
					,[FollowupTimeUnitsID]
					,[FollowupQuantity]
					,[AvailableManually]
					,[StatusAfterEvent]
					,[AquariumEventAfterEvent]
					,[EventSubtypeID]
					,[DocumentTypeID]
					,[LeadTypeID]
					,[AllowCustomTimeUnits]
					,[InProcess]
					,[KeyEvent]
					,[UseEventCosts]
					,[UseEventUOEs]
					,[UseEventDisbursements]
					,[UseEventComments]
					,[SignatureRequired]
					,[SignatureOverride]
					,[VisioX]
					,[VisioY]
					,[AquariumEventSubtypeID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[FollowupWorkingDaysOnly]
					,[CalculateTableRows]
					,[SourceID]
					,[SmsGatewayID]
					,[IsShared]
					,[SocialFeedID]
					)
				VALUES
					(
					@ClientID
					,@EventTypeName
					,@EventTypeDescription
					,@Enabled
					,@UnitsOfEffort
					,@FollowupTimeUnitsID
					,@FollowupQuantity
					,@AvailableManually
					,@StatusAfterEvent
					,@AquariumEventAfterEvent
					,@EventSubtypeID
					,@DocumentTypeID
					,@LeadTypeID
					,@AllowCustomTimeUnits
					,@InProcess
					,@KeyEvent
					,@UseEventCosts
					,@UseEventUOEs
					,@UseEventDisbursements
					,@UseEventComments
					,@SignatureRequired
					,@SignatureOverride
					,@VisioX
					,@VisioY
					,@AquariumEventSubtypeID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@FollowupWorkingDaysOnly
					,@CalculateTableRows
					,@SourceID
					,@SmsGatewayID
					,@IsShared
					,@SocialFeedID
					)
				-- Get the identity value
				SET @EventTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Insert] TO [sp_executeall]
GO
