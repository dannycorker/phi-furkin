SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the UploadedFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_Get_List]

AS


				
				SELECT
					[UploadedFileID],
					[ClientID],
					[FolderID],
					[UploadedFileName],
					[UploadedFileTitle],
					[UploadedFileDescription],
					[UploadedFile],
					[UploadedFileGUID],
					[UploadedFileSize],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[UploadedFile] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Get_List] TO [sp_executeall]
GO
