SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestionGroupMember table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupMemberID]
(

	@SMSQuestionGroupMemberID int   
)
AS


				SELECT
					[SMSQuestionGroupMemberID],
					[ClientID],
					[SMSQuestionnaireID],
					[SMSQuestionID],
					[SMSQuestionGroupID]
				FROM
					[dbo].[SMSQuestionGroupMember] WITH (NOLOCK) 
				WHERE
										[SMSQuestionGroupMemberID] = @SMSQuestionGroupMemberID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupMemberID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupMemberID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupMemberID] TO [sp_executeall]
GO
