SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28/07/2015
-- Description:	Find customer via lead ref / matter ref
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByLeadRefAndMatterRef]
	@ClientID INT ,
	@Reference VARCHAR(100)
AS
BEGIN

	SELECT c.* FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID AND l.LeadRef LIKE '%' + @Reference + '%'
	WHERE C.ClientID=@ClientID
	UNION
	SELECT c.* FROM Customers c WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON m.CustomerID = c.CustomerID AND m.MatterRef LIKE '%' + @Reference + '%'
	WHERE C.ClientID=@ClientID
	ORDER BY Fullname ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByLeadRefAndMatterRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByLeadRefAndMatterRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByLeadRefAndMatterRef] TO [sp_executeall]
GO
