SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-12-07
-- Description:	Remove event subtype thresholding on a batch job
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RemoveBatchThresholding]
	
AS
BEGIN

	SET NOCOUNT ON	
	

	
	UPDATE AT
	SET at.EventSubTypeThresholding = 0
	FROM AutomatedTask at
	WHERE at.EventSubTypeThresholding = 1
	and (at.TaskID in (
	SELECT distinct ath.TaskID FROM  AutomatedTaskHistory ath WITH (NOLOCK) 
	INNER JOIN ClientPersonnel CP WITH (NOLOCK) ON CP.ClientPersonnelID = ATH.WhoModified 
	AND ((CP.EmailAddress like '%THOMAS.DOYLE%@AQUARIUM-SOFTWARE.COM%') or (cp.ClientPersonnelID = 21046 /*Jess*/))
	and cp.IsAquarium=1
	INNER JOIN AutomatedTask at WITH (NOLOCK) on at.TaskID=ath.TaskID and at.EventSubTypeThresholding=1
	WHERE ATH.clientid in (253,309)
	) and at.ClientID in (253,309)
	or at.WhoCreated = 21046
	
	 )

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RemoveBatchThresholding] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RemoveBatchThresholding] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RemoveBatchThresholding] TO [sp_executeall]
GO
