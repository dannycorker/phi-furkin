SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-08
-- Description:	Loop through all new imports attempting reconcile
-- Mods
--	2015-07-09 DCM tidy info added to reversal tablerows 
--	2015-07-13 DCM Add reversal row to lead level (itemised table)
--	2015-07-14 DCM Trigger follow up processing depending return code
--  2016-03-16 DCM Don't do any post-processing if mandate is live but an 0C is queued to be sent
--	2016-11-14 DCM add reversal row into OutgoingPayment table when failure is for claims
--	2016-12-21 DCM added cancelled policy flagging to ARUDD
--	2017-01-30 DCM updated duplicate testing to match on @ReasonCode or @OrigReasonCode so long as not in same import file
--	2017-05-31 AH updated CliendID and added BACS Notifications MatterID and updated function naming (line 168 & 169)
--  2017-06-25 JEL Changed insert of PPPS and CPS failure rows to updates. Sets PPPS and CPS to failed and uses new ContraCustomerLedgerID column to track
--  2017-09-07 JEL Added @Now as effective payment date rather than the origional attempt's date
--  2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
--  2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BACS_ReconcileImport] 
(
	@TableRowID				INT,
	@ReconciliationType		INT,	/* 1=Auto, 2=Manual */
	@AqAutomation			INT
)

AS
BEGIN

	SET NOCOUNT ON;

	/*
		To be run either after import or when manually applied using table row SAS.
		Using the fields imported from BACS files, reconcile to the appropriate customer/account, add entry to the appropriate history table
		(Collections or Mandate) and kick off further processing by adding the appropriate event.
	*/
	
	DECLARE
				@ClientID				INT = dbo.fnGetPrimaryClientID(),  -- 600 specific
				@ImportTableMatterID	INT = 233   -- 600 specific
	
	DECLARE		@CountryID				INT

	DECLARE		
				@AccName				VARCHAR(100),
				@AccNumber				VARCHAR(100),
				@AccountID				INT,
				@AccSortCode			VARCHAR(10),
				@AdviceType				VARCHAR(30),
				@AdviceTypeLULI			INT,
				@Amount					MONEY,
				@BACSFileDate			DATE,
				@CandidateRows			tvpIntInt,
				@CaseID					INT,
				@CustomerID				INT,
				@ClaimsMatterID			INT,
				@ClaimsCaseID			INT,
				@DuplicateTableRowID	INT,
				@EffectiveDate			DATE,
				@ErrorMessage			VARCHAR(500),
				@ImportFileID			VARCHAR(500),
				@LeadID					INT,
				@LogDate				VARCHAR(10),
				@LogEntry				VARCHAR(2000),
				@LeadEventID			INT,
				@MatchCount				INT = 0,
				@MatchTableRowID		INT,
				@MatterID				INT,
				@NewLedgerID			INT,
				@NewPremiumTableRowID   INT,
				@NewPaymentID			INT,
				@NewTableRowID			INT,
				@Now					DATETIME = dbo.fn_GetDate_Local(),
				@PolMatterID			INT,
				@PostProcessingRequired	INT,
				@PPPSID					INT,
				@PremiumTableRowID		INT,
				@PremiumTableRows dbo.tvpIntInt,
				@ReasonCode				VARCHAR(100),
				@ReasonCodeOrig			VARCHAR(100),
				@ReasonText				VARCHAR(500),
				@ReconcileVarcharDate	VARCHAR(10),
				@Reference				VARCHAR(100),
				@Today					VARCHAR(10),
				@TransactionCode		VARCHAR(20),
				@ValueInt				INT,
				@OutGoingPaymentID		INT,
				@NewLeadEventID			INT ,
				@NewCustomerPaymentScheduleID INT,
				@MatchPPPSID INT,
				@CPSID INT  
			
	
	DECLARE @ItemisedColHistory TABLE (TRID INT, LeadID INT, Done INT)	
	
	SELECT @AqAutomation=dbo.fn_C600_GetAqAutomationUser()
	
	/* Ensure all fields are present */
	
	EXEC _C00_CompleteTableRow @TableRowID,@ImportTableMatterID,1,1

	/*
	If this row has already been reconciled, say so and do no more work
	*/
	IF @ReconcileVarcharDate > ''
	BEGIN
		SELECT @LogEntry = 'Error.  This row has already reconciled.  Date: ' + @ReconcileVarcharDate
		
		/*Add the error message to the top of the Reconcile Outcome field*/
		SELECT @LogEntry = CASE WHEN tdv.DetailValue > '' THEN @LogEntry + CHAR(13) + CHAR(10) + tdv.DetailValue ELSE @LogEntry END
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.TableRowID = @TableRowID
		and tdv.DetailFieldID = 170219 /*Reconcile Outcome*/
		
		EXEC dbo._C00_SimpleValueIntoField 170219, @LogEntry, @TableRowID, @AqAutomation /*Update the Reconcile Outcome*/
		RETURN /*Leave this SP*/
	END

	/*If this row does not appear in the import table field, say so and do no more work*/
	IF NOT EXISTS ( SELECT * FROM TableRows tr WITH (NOLOCK) WHERE tr.ClientID = @ClientID and tr.DetailFieldID IN(170221,170224) /*New Imports, Unreconciled*/ and tr.TableRowID = @TableRowID )
	BEGIN
		SELECT @LogEntry = 'Error.  TableRowID ' + CONVERT(VARCHAR,@TableRowID) + ' does not appear in the imported payments table on MatterID ' + CONVERT(VARCHAR,@ImportTableMatterID)
		RAISERROR( @LogEntry, 16, 1 )
		RETURN /*Leave this SP*/
	END
	
	
	-- get all of the details in one hit
	SELECT TOP 1
			@AdviceType = adv.DetailValue,
			@EffectiveDate = ed.DetailValue,
			@BACSFileDate = bfd.DetailValue,
			@TransactionCode = tc.DetailValue,
			@Reference = ref.DetailValue,
			@MatterID = ISNULL(mref.MatterID,0),
			@AccName = an.DetailValue,
			@AccNumber = anum.DetailValue,
			@AccSortCode = acsc.DetailValue,
			@Amount = am.ValueMoney,
			@ReasonCodeOrig = rc.DetailValue,
			@AdviceTypeLULI = ISNULL(advli.LookupListItemID,0),
			@AccountID = ISNULL(a.AccountID,0),
			@CustomerID = ISNULL(a.CustomerID,0)
	FROM TableDetailValues adv WITH (NOLOCK) 
	INNER JOIN TableDetailValues ed WITH (NOLOCK) ON adv.TableRowID=ed.TableRowID AND ed.DetailFieldID=170209
	INNER JOIN TableDetailValues ref WITH (NOLOCK) ON adv.TableRowID=ref.TableRowID AND ref.DetailFieldID=170210
	INNER JOIN TableDetailValues an WITH (NOLOCK) ON adv.TableRowID=an.TableRowID AND an.DetailFieldID=170211
	INNER JOIN TableDetailValues anum WITH (NOLOCK) ON adv.TableRowID=anum.TableRowID AND anum.DetailFieldID=170212
	INNER JOIN TableDetailValues acsc WITH (NOLOCK) ON adv.TableRowID=acsc.TableRowID AND acsc.DetailFieldID=170213
	INNER JOIN TableDetailValues am WITH (NOLOCK) ON adv.TableRowID=am.TableRowID AND am.DetailFieldID=170214
	INNER JOIN TableDetailValues rc WITH (NOLOCK) ON adv.TableRowID=rc.TableRowID AND rc.DetailFieldID=170215
	INNER JOIN TableDetailValues tc WITH (NOLOCK) ON adv.TableRowID=tc.TableRowID AND tc.DetailFieldID=175406
	--INNER JOIN TableDetailValues fid WITH (NOLOCK) ON adv.TableRowID=fid.TableRowID AND fid.DetailFieldID=170206
	INNER JOIN TableDetailValues bfd WITH (NOLOCK) ON adv.TableRowID=bfd.TableRowID AND bfd.DetailFieldID=175405
	LEFT JOIN LookupListItems advli WITH (NOLOCK) ON advli.ItemValue = adv.DetailValue AND advli.LookupListID=4443
	LEFT JOIN MatterDetailValues mref WITH (NOLOCK) ON ref.DetailValue=mref.DetailValue AND mref.DetailFieldID=175460
	LEFT JOIN Account a WITH (NOLOCK) ON ref.DetailValue=a.Reference
	WHERE adv.DetailFieldID=170208 AND adv.TableRowID=@TableRowID

	-- get reason text
	SELECT @ReasonText=RIGHT(dbo.fn_C600_GetBACSReasonText(@AdviceTypeLULI,@ReasonCodeOrig),LEN(dbo.fn_C600_GetBACSReasonText(@AdviceTypeLULI,@ReasonCodeOrig))-CHARINDEX('|',dbo.fn_C600_GetBACSReasonText(@AdviceTypeLULI,@ReasonCodeOrig)))
	SELECT @ReasonCode=LEFT(dbo.fn_C600_GetBACSReasonText(@AdviceTypeLULI,@ReasonCodeOrig),CHARINDEX('|',dbo.fn_C600_GetBACSReasonText(@AdviceTypeLULI,@ReasonCodeOrig))-1)
	
	IF ISNULL(@MatterID,0) <> 0 
	BEGIN 
		-- store reason code for documentation
		EXEC dbo._C00_SimpleValueIntoField 170107, @ReasonText, @MatterID, @AqAutomation	
		EXEC dbo._C00_SimpleValueIntoField 170169, @ReasonCode, @MatterID, @AqAutomation	
		-- documentation paragraph
		SELECT @ErrorMessage=''
		SELECT @ErrorMessage=dbo.fn_C00_ReturnCorrespondingResourceColumn(170198, @ReasonCode, 175508)
		EXEC dbo._C00_SimpleValueIntoField 175476, @ErrorMessage, @MatterID, @AqAutomation	
	END

	/* check for potential duplicate import if the record is being auto-reconciled i.e. is a fresh new import */
	IF @ReconciliationType=1
	BEGIN
		
		SELECT @DuplicateTableRowID=tr.TableRowID 
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues ref WITH (NOLOCK) ON tr.TableRowID=ref.TableRowID and ref.DetailFieldID=170210 and ref.DetailValue=@Reference
		INNER JOIN TableDetailValues at WITH (NOLOCK) ON tr.TableRowID=at.TableRowID and at.DetailFieldID=170208 and at.DetailValue=@AdviceType
		INNER JOIN TableDetailValues anum WITH (NOLOCK) ON tr.TableRowID=anum.TableRowID and anum.DetailFieldID=170212 and anum.DetailValue=@AccNumber
		INNER JOIN TableDetailValues accsc WITH (NOLOCK) ON tr.TableRowID=accsc.TableRowID and accsc.DetailFieldID=170213 and accsc.DetailValue=@AccSortCode
		INNER JOIN TableDetailValues am WITH (NOLOCK) ON tr.TableRowID=am.TableRowID and am.DetailFieldID=170214 and am.ValueMoney=@Amount
		INNER JOIN TableDetailValues rc WITH (NOLOCK) ON tr.TableRowID=rc.TableRowID and rc.DetailFieldID=170215 and (rc.DetailValue=@ReasonCode OR rc.DetailValue=@ReasonCodeOrig)
		INNER JOIN TableDetailValues tc WITH (NOLOCK) ON tr.TableRowID=tc.TableRowID and tc.DetailFieldID=175406 and tc.DetailValue=@TransactionCode
		INNER JOIN TableDetailValues bfd WITH (NOLOCK) ON tr.TableRowID=bfd.TableRowID and bfd.DetailFieldID=175405 and bfd.ValueDate=@BACSFileDate
		--INNER JOIN TableDetailValues fid WITH (NOLOCK) ON tr.TableRowID=fid.TableRowID and fid.DetailFieldID=170206 and fid.ValueDate<>@ImportFileID
		WHERE tr.DetailFieldID <> 170221

		IF @DuplicateTableRowID IS NOT NULL
		BEGIN
		
			SELECT @AdviceType='',	-- don't attempt further matching
					@LogEntry='Potential duplicate record imported. See TableRowID: ' + CONVERT(VARCHAR(30),@DuplicateTableRowID),	-- add unreconciled comment
					@MatchCount=0,	-- move row to unreconciled table
					@LogDate=''     -- blank reconciled date
		
		END
		
	END

	-- reformat sort code
	SELECT @AccSortCode=REPLACE(@AccSortCode,'-','')

	/*GPR 2020-03-02*/
	SELECT @CountryID = cl.CountryID FROM Customers c WITH (NOLOCK)
			INNER JOIN Clients cl WITH (NOLOCK) ON cl.ClientID = c.ClientID
			WHERE c.CustomerID = @CustomerID


	IF @AdviceType IN ('ADDACS','AUDDIS') /* Imports related to mandate lodging ADDACS changes/failures from customer side; AUDDIS changes/failures from us */
	BEGIN
		
		/*GPR 2020-03-02 for AAG-202*/
		IF @CountryID <> 14 /*Australia*/
		BEGIN
			-- Get the most recent mandate lodgement request row 
			SELECT TOP 1 @MatchTableRowID=am.AccountMandateID, @MatchCount=1 
			FROM AccountMandate am WITH (NOLOCK)
			WHERE am.AccountID=@AccountID 
			AND am.MandateStatusID IN (1,2,3) -- pending, requested, accepted
			AND NOT EXISTS ( SELECT * FROM AccountMandate am2 WITH (NOLOCK) 
							 WHERE am2.AccountID=am.AccountID 
								AND am2.AccountMandateID>am.AccountMandateID 
								AND am2.MandateStatusID IN (4,5) ) -- rejected, cancelled
			ORDER BY am.AccountMandateID DESC
		END

		IF @MatchCount=1
		BEGIN

			-- determine post-processing required			
			SELECT @PostProcessingRequired=ppr.ValueInt 
			FROM ResourceListDetailValues typ WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues cod WITH (NOLOCK) ON typ.ResourceListID=cod.ResourceListID 
				AND cod.DetailFieldID=170198
				AND cod.DetailValue=@ReasonCode
			INNER JOIN ResourceListDetailValues ppr WITH (NOLOCK) ON typ.ResourceListID=ppr.ResourceListID
				AND ppr.DetailFieldID=175595
			INNER JOIN LookupListItems typli WITH (NOLOCK) ON typ.ValueInt=typli.LookupListItemID
			WHERE typ.DetailFieldID=170200 
			AND typli.ItemValue=@AdviceType
			
			-- if we are instructed just to update the account details, do so, otherwise fail the existing mandate
			IF @PostProcessingRequired=72211
			BEGIN
			
				EXEC dbo.AccountHistory__Backup @AccountID, WhoCreated
				
				UPDATE Account 
				SET FriendlyName=@AccName,
					AccountNumber=@AccNumber,
					Sortcode=@AccSortCode
				WHERE AccountID=@AccountID
			
			END
			ELSE
			BEGIN
				
				IF @CountryID <> 14 /*Australia*/
				BEGIN
					-- add failure record to the mandate history table
					INSERT INTO AccountMandate (ClientID, CustomerID, AccountID, MandateStatusID, DateRequested, FailureDate, FailureCode, WhoCreated, WhenCreated) VALUES
					(@ClientID, @CustomerID, @AccountID, 5, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(),@ReasonCode,@AqAutomation,dbo.fn_GetDate_Local())
				END
			END
						
			-- record new table rowID
			SELECT @LogEntry = 'Reconciled to AccountMandateID ' + ISNULL(CONVERT(VARCHAR,@MatchTableRowID),'null'),
								@LogDate=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)

			-- ...and MatterID
			EXEC dbo._C00_SimpleValueIntoField 170217, @MatterID, @TableRowID, @AqAutomation

		END
		 
		BEGIN
		
			SELECT @LogEntry = 'No Match', @LogDate=''
		
		END

	END

	ELSE IF @AdviceType='ARUDD' /* Imports related to collection requests */
	BEGIN

		-- Get the most recent payment request row that matches the amount and hasn't already had an ARUDD match 
		SELECT TOP 1 @MatchTableRowID=p.PaymentID, @MatchCount=1
		FROM Payment p WITH (NOLOCK)
		WHERE p.PaymentReference=@Reference 
		AND p.PaymentGross=@Amount
		AND p.PaymentStatusID IN (2,5,6) -- processed, retry, paid
		AND NOT EXISTS ( SELECT * FROM Payment p2 WITH (NOLOCK) 
						 WHERE p.CustomerPaymentScheduleID=p2.CustomerPaymentScheduleID 
							AND p2.PaymentStatusID = 4 
							AND p.PaymentID < p2.PaymentID )
		ORDER BY p.PaymentID DESC

		IF @MatchCount=1
		BEGIN
		
			-- insert a payment reversal record
			INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, CustomerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
			SELECT ClientID, CustomerID, PaymentDateTime, PaymentTypeID, CAST(dbo.fn_GetDate_Local() AS DATE), 'Failure notification', PaymentReference, -PaymentNet, -PaymentVAT, -PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, 4, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, @ReasonCode, @ReasonText, CustomerPaymentScheduleID, @AqAutomation, dbo.fn_GetDate_Local(), @AqAutomation, dbo.fn_GetDate_Local() 
			FROM Payment WITH (NOLOCK) WHERE PaymentID=@MatchTableRowID
			
			SELECT  @NewPaymentID = SCOPE_IDENTITY()
			
			-- insert a GL reversal record
			INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			SELECT ClientID, CustomerID, @Now, @ReasonCode, @ReasonText, CAST(dbo.fn_GetDate_Local() AS DATE), TransactionReference, 'Failure notification', -TransactionNet, -TransactionVAT, -TransactionGross, LeadEventID, ObjectID, ObjectTypeID, @NewPaymentID, OutgoingPaymentID, @AqAutomation, dbo.fn_GetDate_Local() 
			FROM CustomerLedger cl WITH (NOLOCK) 
			WHERE cl.PaymentID=@MatchTableRowID
			
			SELECT  @NewLedgerID = SCOPE_IDENTITY()
			
			-- reverse customerpaymentschedule, reverse payment status
			--INSERT INTO CustomerPaymentSchedule (ClientID,CustomerID,AccountID,ActualCollectionDate,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,WhoCreated,WhenCreated)
			--SELECT pp.ClientID,pp.CustomerID,pp.AccountID,ActualCollectionDate,pp.PaymentDate,-pp.PaymentNet,-pp.PaymentVAT,-pp.PaymentGross,4,@NewLedgerID,ReconciledDate,44412,pp.WhenCreated  
			--FROM CustomerPaymentSchedule pp
			--INNER JOIN Payment p WITH (NOLOCK) ON pp.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			--WHERE p.PaymentID=@MatchTableRowID
			
			SELECT @NewCustomerPaymentScheduleID = SCOPE_IDENTITY()
			
			UPDATE cps  
			SET PaymentStatusID=4, CustomerLedgerID=@NewLedgerID, ReconciledDate=dbo.fn_GetDate_Local()
			FROM CustomerPaymentSchedule cps
			INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE p.PaymentID=@MatchTableRowID
		
			--INSERT INTO PurchasedProductPaymentSchedule (ClientID,CustomerID,AccountID,PurchasedProductID,ActualCollectionDate,CoverFrom,CoverTo,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,CustomerPaymentScheduleID,WhoCreated,WhenCreated,PurchasedProductPaymentScheduleTypeID, PurchasedProductPaymentScheduleParentID)
			--SELECT pp.ClientID,pp.CustomerID,pp.AccountID,pp.PurchasedProductID,@Now,pp.CoverFrom,pp.CoverTo,@Now,-pp.PaymentNet, -pp.PaymentVAT, -pp.PaymentGross, 4,@NewLedgerID,@Now,@NewCustomerPaymentScheduleID,44412,@Now,1 ,pp.PurchasedProductPaymentScheduleParentID
			--FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
			--INNER JOIN Payment p WITH (NOLOCK) ON pp.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			--WHERE p.PaymentID=@MatchTableRowID
			
				
			-- purchasedproductpaymentschedule, reverse payment status
			UPDATE ppps  
			SET PaymentStatusID=4, ContraCustomerLedgerID=@NewLedgerID, ReconciledDate=dbo.fn_GetDate_Local()
			FROM PurchasedProductPaymentSchedule ppps
			INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE p.PaymentID=@MatchTableRowID
			
			-- if any of the payments reversed are for policies that have been cancelled flag this so that additional cancellation processing will be applied
			INSERT INTO @CandidateRows
			SELECT ppps.PurchasedProductPaymentScheduleID,0 
			FROM PurchasedProductPaymentSchedule ppps
			INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE p.PaymentID=@MatchTableRowID
			
			WHILE EXISTS ( SELECT * FROM @CandidateRows WHERE ID2=0 )
			BEGIN
			
				SELECT TOP 1 @PPPSID=ID1 FROM @CandidateRows WHERE ID2=0
				
				SELECT @ValueInt=CASE WHEN stat.ValueInt=43003 THEN 5144 ELSE 5145 END,
						@PolMatterID=mpp.MatterID
				FROM MatterDetailValues mpp WITH (NOLOCK) 
				INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON mpp.ValueInt=ppps.PurchasedProductID
				INNER JOIN MatterDetailValues stat WITH (NOLOCK) ON mpp.MatterID=stat.MatterID AND stat.DetailFieldID=170038
				WHERE mpp.DetailFieldID=177074
					AND ppps.PurchasedProductPaymentScheduleID=@PPPSID 
				
				EXEC dbo._C00_SimpleValueIntoField 177420, @ValueInt, @PolMatterID, @AqAutomation
				
				UPDATE @CandidateRows SET ID2=1 WHERE ID1=@PPPSID
			
			END
						
			-- record new table rowID
			SELECT @LogEntry = 'Reconciled to Payment ID ' + ISNULL(CONVERT(VARCHAR,@MatchTableRowID),'null'),
								@LogDate=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
			
			-- ...and CustomerID
			EXEC dbo._C00_SimpleValueIntoField 170216, @CustomerID, @TableRowID, @AqAutomation
			
			-- ...and the failed payment ID
			EXEC dbo._C00_SimpleValueIntoField 177370, @NewPaymentID, @MatterID, @AqAutomation


			-- determine post-processing required			
			SELECT @PostProcessingRequired=ppr.ValueInt 
			FROM ResourceListDetailValues typ WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues ppr WITH (NOLOCK) ON typ.ResourceListID=ppr.ResourceListID AND ppr.DetailFieldID=175595
			INNER JOIN ResourceListDetailValues rea WITH (NOLOCK) ON typ.ResourceListID=rea.ResourceListID
				AND rea.DetailFieldID=170199 AND rea.DetailValue=@ReasonCodeOrig
			INNER JOIN LookupListItems typli WITH (NOLOCK) ON typ.ValueInt=typli.LookupListItemID
			WHERE typ.DetailFieldID=170200 AND typli.ItemValue=@AdviceType
			
			-- if post-processing is empty, it means we don't understand the ReasonCode.  Set postprocessing to the 
			-- that defined for ARUDD unknown
			IF @PostProcessingRequired IS NULL
			BEGIN

				SELECT @PostProcessingRequired=ppr.ValueInt 
				FROM ResourceListDetailValues ppr WITH (NOLOCK) 
				WHERE ppr.DetailFieldID=175595 AND ppr.ResourceListID=145209			
			
			END
						
			/*Stamp the missed payment amount into a field on the claims leadtype to be deducted in the event of a claim*/
			SELECT @ClaimsMatterID = ltr.ToMatterID , @ClaimsCaseID = ltr.ToCaseID FROM Payment p WITH (NOLOCK) 
			INNER JOIN dbo.LeadTypeRelationship ltr WITH (NOLOCK) on ltr.FromMatterID = p.ObjectID
			Where  p.PaymentID = @MatchTableRowID 
			and p.ObjectTypeID = 2 
			
			 EXEC _C00_SimpleValueIntoField 177261,@Amount,@ClaimsMatterID,44412
			 EXEC _C00_SimpleValueIntoField 177262,@Reference,@ClaimsMatterID,44412
			 EXEC  	_C00_AddANote    @ClaimsCaseID,919,'Collections Failed Outstanding Premiums', 1,44412,1,0,@Now
		END
		ELSE 
		BEGIN
			
			/* Try to find a match in claims payments*/ 
			SELECT @MatchTableRowID = tr.TableRowID, @MatterID = tr.MatterID, @MatchCount=1  FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 154498 /*Payment Ref*/ 
			INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 175270 /*Account No.*/ 
			INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 154518 /*Amount*/ 
			Where tdv.DetailValue = @Reference
			and tdv1.DetailValue = @AccNumber
			and tdv2.DetailValue = @Amount 
			and tr.DetailFieldID = 154485
			
			-- add a reversal row into the OutgoingPayment table
			INSERT INTO OutgoingPayment (ClientID, CustomerID, LeadEventID, ClaimObjectTypeID, ClaimObjectID, PolicyObjectTypeID, PolicyObjectID, PayeeTitle, PayeeFirstName, PayeeLastName, PayeeOrganisation, PayeeAddress1, PayeeAddress2, PayeeAddress3, PayeeAddress4, PayeeAddress5, PayeePostcode, PayeeCountryID, PayeeReference, PolicyholderReference, PaymentTypeID, PaymentAmount, PaymentDate, PaymentRunDate, DateCreated, OriginalPaymentReferrence, ChequeNumber, AccountNumber, Sortcode, MaskedCardNumber, PaymentSourceTypeID, PaymentStatusID, PaymentFileName, FailureCode, FailureReason, ParentOutgoingPaymentID)
			SELECT ClientID, CustomerID, LeadEventID, ClaimObjectTypeID, ClaimObjectID, PolicyObjectTypeID, PolicyObjectID, PayeeTitle, PayeeFirstName, PayeeLastName, PayeeOrganisation, PayeeAddress1, PayeeAddress2, PayeeAddress3, PayeeAddress4, PayeeAddress5, PayeePostcode, PayeeCountryID, PayeeReference, PolicyholderReference, PaymentTypeID, PaymentAmount, PaymentDate, PaymentRunDate, DateCreated, OriginalPaymentReferrence, ChequeNumber, AccountNumber, Sortcode, MaskedCardNumber, PaymentSourceTypeID, 4, '', @ReasonCode, @ReasonText, ParentOutgoingPaymentID 
			FROM OutgoingPayment op WITH (NOLOCK) 
			WHERE op.ClaimObjectID=@MatchTableRowID
			
			SELECT @OutGoingPaymentID = SCOPE_IDENTITY()	
						
			-- record new table rowID
			SELECT @LogEntry = 'Reconciled to ClaimPaymentID ' + ISNULL(CONVERT(VARCHAR,@MatchTableRowID),'null'),
			@LogDate=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
			
			-- ...and MatterID
			EXEC dbo._C00_SimpleValueIntoField 170217, @MatterID, @TableRowID, @AqAutomation		
									
		END

		IF ISNULL(@MatchTableRowID,0) = 0 and ISNULL(@MatchCount,0) <> 1 
		BEGIN
		
			SELECT @LogEntry = 'No Match', @LogDate=''
		
		END

	END

	/* kick off post processing to deal with failure return */
	/* Note we do nothing further if the post-processing code is ignore (72214) */
	/* ... and do nothing if the mandate is about to be cancelled (an 0C queued to be sent) */
	SELECT @CaseID=CaseID FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID

	IF @MatchCount=1
		AND dbo.fnGetDvAsInt(176895,@CaseID) <> 5144 -- send 0C
	BEGIN
			
		-- update the matter field used by automation reports
		EXEC dbo._C00_SimpleValueIntoField 175596, @PostProcessingRequired, @MatterID, @AqAutomation
				
		-- open a mandate failed thread (but only if mandate is live or pending)
		IF @PostProcessingRequired IN (72207, -- Mandate cancel
										72209, -- Mandate cancel + record failed payment
										72215, -- Mandate chase
										72208, -- Mandate chase + record failed payment
										74413, -- Mandate chase or cancel
										74414) -- Mandate chase + record failed payment or cancel
			AND dbo.fnGetDvAsInt(170101,@CaseID) NOT IN (69919,69920) -- failed & cancelled			
		BEGIN
										
				-- Move this to Cancel with potential for manual intervention if this is the first payment (or first payment yet to be taken)
				-- on any of the policies covered by the failure on this account i.e. if for any policy this is policy year one and the count 
				-- of scheduled payments processed or paid is 0 ( this payment is already marked as failed )
				IF @PostProcessingRequired IN (74413, 74414) 
					AND EXISTS ( SELECT *
									FROM PurchasedProduct pp
									WHERE pp.AccountID=@AccountID
									AND EXISTS (
									SELECT COUNT(*) FROM PurchasedProductPaymentSchedule ppps WHERE ppps.PurchasedProductID=pp.PurchasedProductID 
									AND ppps.PaymentStatusID IN (2,6) -- processed, paid
									HAVING COUNT(*)=0
									AND NOT EXISTS ( 
										SELECT COUNT(*) FROM TableRows py WITH (NOLOCK) WHERE pp.ObjectID=py.MatterID AND py.DetailFieldID=170033
										HAVING COUNT(*) > 1
										) 
									) 
								)
				BEGIN
		
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 150098, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
				
				END
				ELSE
				BEGIN
				
					-- add the IP mandate failed event - follow up the send or resend mandate
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 150092, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON c.CaseID=le.CaseID AND le.EventDeleted = 0 AND le.EventTypeID IN (150089,150128) AND le.WhenFollowedUp IS NULL
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
								
				END
				
		END
		
		-- follow up the payment collection thread 
		IF @PostProcessingRequired IN (72210, -- Failed payment recollection
										72209, -- Mandate cancel + record failed payment
										72208, -- Mandate chase + record failed payment
										74414) -- Mandate chase + record failed payment or cancel
		BEGIN
		
			/*Get out the policy MatterID and check the status of the policy*/ 
			SELECT @ValueInt=stat.ValueInt,
			@PolMatterID=mpp.MatterID
			FROM MatterDetailValues mpp WITH (NOLOCK) 
			INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON mpp.ValueInt=ppps.PurchasedProductID
			INNER JOIN MatterDetailValues stat WITH (NOLOCK) ON mpp.MatterID=stat.MatterID AND stat.DetailFieldID=170038
			WHERE mpp.DetailFieldID=177074
			AND ppps.PurchasedProductPaymentScheduleID=@PPPSID 
		

		
			IF @AdviceType = 'ARUDD' and @PostProcessingRequired = 72210 AND @ValueInt NOT IN (43003,74535)
			BEGIN
			
			
				-- purchasedproductpaymentschedule, reverse payment status
				SELECT @MatchPPPSID = ppps.PurchasedProductPaymentScheduleID, @CPSID = ppps.CustomerPaymentScheduleID 
				FROM PurchasedProductPaymentSchedule ppps
				INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
				WHERE p.PaymentID=@MatchTableRowID
				
				IF EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule ppps Where ppps.PurchasedProductPaymentScheduleID = @PPPSID 
							and ppps.PurchasedProductPaymentScheduleParentID <> @PPPSID )
				BEGIN 
				
					-- Cancel
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 150098, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
				
				END  
				ELSE 
				BEGIN 
				
					-- RETRY 
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 155358, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
				
				END		
			
			END
			ELSE
			BEGIN	
				
				-- either add the payment chase event (not following up anything)
				IF NOT EXISTS ( SELECT * FROM MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID=170093 AND MatterID=@MatterID AND ValueInt=1 )
				BEGIN
				
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 150099, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
			
				END
				-- or if we are already in a chase process just send the further failure notification event
				ELSE
				BEGIN
				
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156728, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
				
				END

				EXEC dbo._C00_SimpleValueIntoField 170093, 1, @MatterID, @AqAutomation
			END
		END
		
		-- suspend mandate
		IF @PostProcessingRequired IN (74404, -- Suspend Mandate
										74405) -- Suspend mandate + collection
		BEGIN
		
				-- suspend the mandate
				INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
				SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156736, @AqAutomation, -1, 5, 0, 5
				FROM Cases c WITH (NOLOCK)
				INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
				INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
				INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
				WHERE m.MatterID=@MatterID

				-- suspend collection
				IF @PostProcessingRequired = 74405
				BEGIN
				
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156734, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
				
				END
		
		END
		
		
		-- resend existng mandate details 
		IF @PostProcessingRequired IN (74406) -- Resend Instruction
		BEGIN
		
				-- BEU do we need to reset mandate status?
				-- BEU set to to ensure 0N on first payment collection?
				
				-- add OOP resend event
				INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
				SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156740, @AqAutomation, -1, 5, 0, 5
				FROM Cases c WITH (NOLOCK)
				INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
				INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
				INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
				WHERE m.MatterID=@MatterID
				
				IF @AdviceType='ARUDD' -- we will have marked a payment as failed, so suspend collections & the payment will be requeued immediately
				BEGIN
					
					INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
					SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156734, @AqAutomation, -1, 5, 0, 5
					FROM Cases c WITH (NOLOCK)
					INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
					INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
					INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
					WHERE m.MatterID=@MatterID
				
				END
		
		END
		
		-- just amend the account details and continue
		IF @PostProcessingRequired IN (72211) -- Update account details & inform customer
		BEGIN
		
				-- add an IP Account details amended by BACS return event
				INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
				SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 155430, @AqAutomation, -1, 5, 0, 5
				FROM Cases c WITH (NOLOCK)
				INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
				INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
				INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
				WHERE m.MatterID=@MatterID
		
		END
		
	END
	ELSE 
	IF EXISTS (SELECT * FROM Cases c WITH (NOLOCK) 
			   INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = c.LeadID
			   Where c.CaseID = @CaseID 
			   and l.leadtypeID = 1490)
	BEGIN 
	
		EXEC _C00_SimpleValueIntoField 158540,'Payment Failed',@TableRowID,@AqAutomation
		
		SELECT @LeadEventID = c.LatestInprocessLeadEventID FROM Cases c WITH (NOLOCK) 
		Where c.CaseID = @CaseID 
		
		SELECT @CustomerID = m.CustomerID FROM Matter m WITH (NOLOCK) where m.matterID = @MatterID 
		
        EXEC @NewLeadEventID = _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID,158744,@AqAutomation,0
                
        SELECT @Amount = @Amount
                       
        INSERT INTO CustomerLedger (ClientID,CustomerID,FailureReason,EffectivePaymentDate,TransactionDate,TransactionReference,TransactionDescription,TransactionNet,TransactionVAT,TransactionGross,LeadEventID,ObjectID,ObjectTypeID,OutgoingPaymentID,WhoCreated,WhenCreated)
		VALUES (@ClientID,@CustomerID,'BACs Payment Failed',@Now,@Now,@Reference,'Claim Payment Reversal',0,0,@Amount,@NewLeadEventID,@MatterID,2,@OutGoingPaymentID,44412,@Now) 
	
	END 
		
	/* Move to reconciled or unreconciled page */
	UPDATE tr
	SET DetailFieldID = CASE WHEN @MatchCount = 0 THEN 170224 ELSE					-- unreconciled
							CASE WHEN @ReconciliationType=1 THEN 170223				-- reconciled
									ELSE 170222 END									-- manually reconciled
						END
		, DenyDelete  = CASE WHEN @MatchCount = 0 THEN 0 ELSE 1 END
		, DenyEdit    = CASE WHEN @MatchCount = 0 THEN 0 ELSE 1 END
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.TableRowID = @TableRowID
	
	EXEC dbo._C00_SimpleValueIntoField 170219, @LogEntry, @TableRowID, @AqAutomation /*Reconcile Outcome*/
	EXEC dbo._C00_SimpleValueIntoField 170218, @LogDate, @TableRowID, @AqAutomation /*Reconcile Date*/
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_ReconcileImport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BACS_ReconcileImport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_ReconcileImport] TO [sp_executeall]
GO
