SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-07-03
-- Description:	Create Delete LeadEvent Statement
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateDeleteLeadEventStatement]
@EventTypeID int,
@DeletionComments varchar(1000),
@CreatedFrom datetime,
@CreatedTo datetime = NULL,
@ClientPersonnelID int = -1
AS
BEGIN
	SET NOCOUNT ON;

	If @CreatedTo = NULL Select @CreatedTo = dbo.fn_GetDate_Local()

	select 'exec dbo.deleteleadevent ' + convert(varchar(10),leadeventid) + ',' +  Case @ClientPersonnelID When -1 then WhoCreated Else @ClientPersonnelID END + ', ' + @DeletionComments + ', 4'
	from leadevent 
	where eventtypeid = @EventTypeID 
	and whencreated Between @CreatedFrom and @CreatedTo
	and eventdeleted = 0 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDeleteLeadEventStatement] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateDeleteLeadEventStatement] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDeleteLeadEventStatement] TO [sp_executeall]
GO
