SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeSql table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_Get_List]

AS


				
				SELECT
					[EventTypeSqlID],
					[ClientID],
					[EventTypeID],
					[PostUpdateSql],
					[IsNativeSql]
				FROM
					[dbo].[EventTypeSql] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Get_List] TO [sp_executeall]
GO
