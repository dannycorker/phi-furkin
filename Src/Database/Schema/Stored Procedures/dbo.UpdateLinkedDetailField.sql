SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[UpdateLinkedDetailField]

@LinkedDetailFieldID int,
@LeadTypeIDTo int,
@LeadTypeIDFrom int,
@DetailFieldIDTo int,
@DetailFieldIDFrom int,
@DisplayOnPageID int,
@FieldOrder int,
@Enabled bit,
@History bit,
@LeadOrMatter int,
@FieldName nvarchar(100),
@LeadLinkedTo int,
@LeadLinkedFrom int,
@IncludeLinkedField bit,
@ClientID int = null

as

Update LinkedDetailFields
Set 
LeadTypeIDTo = @LeadTypeIDTo,
LeadTypeIDFrom = @LeadTypeIDFrom,
DetailFieldIDTo = @DetailFieldIDTo,
DetailFieldIDFrom = @DetailFieldIDFrom,
DisplayOnPageID = @DisplayOnPageID,
FieldOrder = @FieldOrder,
Enabled = @Enabled,
History = @History,
LeadOrMatter = @LeadOrMatter,
FieldName = @FieldName,
LeadLinkedTo = @LeadLinkedTo,
LeadLinkedFrom = @LeadLinkedFrom,
IncludeLinkedField = @IncludeLinkedField
Where LinkedDetailFieldID = @LinkedDetailFieldID






GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateLinkedDetailField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateLinkedDetailField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateLinkedDetailField] TO [sp_executeall]
GO
