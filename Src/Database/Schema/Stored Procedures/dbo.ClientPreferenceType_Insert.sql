SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPreferenceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreferenceType_Insert]
(

	@ClientPreferenceTypeID int    OUTPUT,

	@PreferenceTypeName varchar (100)  
)
AS


				
				INSERT INTO [dbo].[ClientPreferenceType]
					(
					[PreferenceTypeName]
					)
				VALUES
					(
					@PreferenceTypeName
					)
				-- Get the identity value
				SET @ClientPreferenceTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreferenceType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_Insert] TO [sp_executeall]
GO
