SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AssetSubType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetSubType_Delete]
(

	@AssetSubTypeID int   
)
AS


				DELETE FROM [dbo].[AssetSubType] WITH (ROWLOCK) 
				WHERE
					[AssetSubTypeID] = @AssetSubTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetSubType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Delete] TO [sp_executeall]
GO
