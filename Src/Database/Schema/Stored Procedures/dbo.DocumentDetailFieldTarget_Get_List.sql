SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DocumentDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget_Get_List]

AS


				
				SELECT
					[DocumentDetailFieldTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[TemplateTypeID],
					[DetailFieldAlias],
					[Notes],
					[ExcelSheetLocation],
					[Format],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentDetailFieldTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Get_List] TO [sp_executeall]
GO
