SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuestionnaireFrameSources table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_Get_List]

AS


				
				SELECT
					[QuestionnaireFrameSourceID],
					[ClientID],
					[ClientQuestionnaireID],
					[PageNumber],
					[SourceUrl],
					[FrameType]
				FROM
					[dbo].[QuestionnaireFrameSources] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Get_List] TO [sp_executeall]
GO
