SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-04
-- Description:	Complete purchase by bank
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthBank]
	@MatterID INT,
	@PaymentDay VARCHAR(2000),
	@PaymentMethodID VARCHAR(2000),
	@PaymentIntervalID VARCHAR(2000),
	@SortCode VARCHAR(2000),
	@AccountNumber VARCHAR(2000),
	@BankName VARCHAR(2000),
	@BankAccountName VARCHAR(2000)			
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	DECLARE @AutomationUserID INT = dbo.fn_C00_GetAutomationUser(@ClientID),
			@CustomerID INT

	SELECT @CustomerID = m.CustomerID
	FROM Matter m WITH (NOLOCK)
	WHERE m.MatterID = @MatterID

	IF EXISTS (
		SELECT *
		FROM CardTransaction ct WITH (NOLOCK)
		WHERE ct.CustomerID = @CustomerID
		AND ct.ObjectID IS NULL
		AND ct.WhenCreated > DATEADD(DD, -1, dbo.fn_GetDate_Local())
	)
	BEGIN

		DELETE ct
		FROM CardTransaction ct WITH (NOLOCK)
		WHERE ct.CustomerID = @CustomerID
		AND ct.ObjectID IS NULL
		AND ct.WhenCreated > DATEADD(DD, -1, dbo.fn_GetDate_Local())

	END

	/*
	2020-10-19 ACE - Looks like we have no idea whats going on with this.
	4 for monthly
	5 for annual
	7 for the payment interval to fortnightly
	*/

	SELECT @PaymentIntervalID = CASE @PaymentIntervalID
									WHEN '4' THEN '69943' /*Monthly*/
									WHEN '5' THEN '69944' /*Annually*/
									WHEN '7' THEN '293321' /*Fortnightly*/
									ELSE @PaymentIntervalID
								END

	EXEC dbo._C00_SimpleValueIntoField 170168, @PaymentDay, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170114, @PaymentMethodID, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170176, @PaymentIntervalID, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170105, @SortCode, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170106, @AccountNumber, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170170, @BankName, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170104, @BankAccountName, @MatterID, @AutomationUserID
	EXEC dbo._C00_SimpleValueIntoField 170168, @PaymentDay, @MatterID, @AutomationUserID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthBank] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthBank] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy_CompleteWIthBank] TO [sp_executeall]
GO
