SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-06-25
-- Description:	Returns all transforms for the rule outputs
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Transforms_GetAll]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.RulesEngine_Transforms WITH (NOLOCK) 
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Transforms_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Transforms_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Transforms_GetAll] TO [sp_executeall]
GO
