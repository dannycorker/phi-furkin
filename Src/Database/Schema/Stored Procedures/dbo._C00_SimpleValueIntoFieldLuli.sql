SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-01-29
-- Description:	Take a text value and insert the appropriate LookupListItem
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SimpleValueIntoFieldLuli]
(
	 @DetailFieldID INT
	,@DetailValue VARCHAR(2000)
	,@ObjectID INT
	,@ClientPersonnelID INT = NULL
	,@ValueIfNotMatched VARCHAR(2000) = ''
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @LeadOrMatter INT,
			@ClientID INT,
			@MaintainHistory BIT,
			@QuestionTypeID int,
			@LookupListItemID VARCHAR(2000)

	SELECT	@LeadOrMatter = LeadOrmatter,
			@ClientID = DetailFields.ClientID,
			@MaintainHistory = MaintainHistory,
			@QuestionTypeID = QuestionTypeID,
			@LookupListItemID = COALESCE(CONVERT(VARCHAR,li.LookupListItemID),@ValueIfNotMatched,'')
	FROM DetailFields WITH (NOLOCK) 
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = DetailFields.LookupListID AND li.ItemValue = @DetailValue
	WHERE DetailFieldID = @DetailFieldID
	

	IF @ClientPersonnelID IS NULL
	BEGIN
		SELECT TOP 1 @ClientPersonnelID=ClientPersonnelID FROM ClientPersonnel WITH (NOLOCK) 
		WHERE UserName LIKE 'Aquarium%Automation' AND ClientID=@ClientID
	END

	IF @LeadOrMatter = 1
	BEGIN
		IF EXISTS (SELECT * FROM dbo.LeadDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND LeadID = @ObjectID)
		BEGIN
			UPDATE dbo.LeadDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND LeadID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@LookupListItemID, ''))
		END
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, @ObjectID, NULL, @LookupListItemID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END

	END

	IF @LeadOrMatter = 2
	BEGIN
		IF EXISTS (SELECT * FROM dbo.MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND MatterID = @ObjectID)
		BEGIN
			UPDATE dbo.MatterDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND MatterID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue)
			SELECT @ClientID, @ObjectID, LeadID, @DetailFieldID, ISNULL(@LookupListItemID, '')
			FROM dbo.Matter
			WHERE MatterID = @ObjectID
		END
		
		IF @MaintainHistory = 1  AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, NULL, @ObjectID, @LookupListItemID, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END
	END

	IF @LeadOrMatter = 4
	BEGIN
		IF EXISTS (SELECT * FROM dbo.ResourceListDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ResourceListID = @ObjectID)
		BEGIN
			UPDATE dbo.ResourceListDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ResourceListID = @ObjectID
		END
		ELSE
		BEGIN 
			INSERT dbo.ResourcelistDetailValues (ResourceListID,ClientID,DetailFieldID,LeadOrMatter,DetailValue)
			SELECT @ObjectID,@ClientID,@DetailFieldID,@LeadOrMatter,ISNULL(@LookupListItemID, '')
			FROM dbo.ResourceList
			WHERE ResourceListID = @ObjectID
		END

	END

	IF @LeadOrMatter in(8,6)
	BEGIN
		IF EXISTS (SELECT * FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND TableRowID = @ObjectID)
		BEGIN
			UPDATE dbo.TableDetailValues
			SET DetailValue =		CASE @QuestionTypeID WHEN 14 THEN null ELSE @LookupListItemID END,
				ResourceListID =	CASE @QuestionTypeID WHEN 14 THEN @LookupListItemID ELSE NULL END 
			WHERE DetailFieldID = @DetailFieldID 
			AND TableRowID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.TableDetailValues (TableRowID,DetailFieldID,DetailValue,ResourceListID,MatterID,ClientID,CustomerID,CaseID,LeadID)
			SELECT	tr.TableRowID,@DetailFieldID,
					CASE @QuestionTypeID WHEN 14 THEN null ELSE @LookupListItemID END,
					CASE @QuestionTypeID WHEN 14 THEN @LookupListItemID ELSE NULL END,
					tr.MatterID,tr.ClientID,tr.CustomerID,tr.CaseID,tr.LeadID
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.TableRowID = @ObjectID
		END
	END

	IF @LeadOrMatter = 10
	BEGIN
		IF EXISTS (SELECT * FROM dbo.CustomerDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND CustomerID = @ObjectID)
		BEGIN
			UPDATE dbo.CustomerDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND CustomerID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@LookupListItemID, ''))
		END
	END
	
	IF @LeadOrMatter = 11
	BEGIN
		IF EXISTS (SELECT * FROM dbo.CaseDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND CaseID = @ObjectID)
		BEGIN
			UPDATE dbo.CaseDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND CaseID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@LookupListItemID, ''))
		END
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		IF EXISTS (SELECT * FROM dbo.ClientDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ClientID = @ObjectID)
		BEGIN
			UPDATE dbo.ClientDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ClientID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.ClientDetailValues (ClientID, DetailFieldID, DetailValue)
			VALUES(@ObjectID, @DetailFieldID, ISNULL(@LookupListItemID, ''))
		END
		
		IF @MaintainHistory = 1  AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, @LookupListItemID, dbo.fn_GetDate_Local(), @ClientPersonnelID)
			
		END
	END
	
	IF @LeadOrMatter = 13
	BEGIN
		IF EXISTS (SELECT * FROM dbo.ClientPersonnelDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ClientPersonnelID = @ObjectID)
		BEGIN
			UPDATE dbo.ClientPersonnelDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ClientPersonnelID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.ClientPersonnelDetailValues (ClientID, ClientPersonnelID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@LookupListItemID, ''))
		END
	END
	
	IF @LeadOrMatter = 14
	BEGIN
		IF EXISTS (SELECT * FROM dbo.ContactDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ContactID = @ObjectID)
		BEGIN
			UPDATE dbo.ContactDetailValues
			SET DetailValue = ISNULL(@LookupListItemID, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ContactID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.ContactDetailValues (ClientID, ContactID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@LookupListItemID, ''))
		END
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoFieldLuli] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SimpleValueIntoFieldLuli] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoFieldLuli] TO [sp_executeall]
GO
