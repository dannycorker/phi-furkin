SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CaseTransferMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_Delete]
(

	@CaseTransferMappingID int   
)
AS


				DELETE FROM [dbo].[CaseTransferMapping] WITH (ROWLOCK) 
				WHERE
					[CaseTransferMappingID] = @CaseTransferMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Delete] TO [sp_executeall]
GO
