SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditingCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_Insert]
(

	@SqlQueryEditingCriteriaID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryEditingTableID int   ,

	@SqlQueryEditingColumnID int   ,

	@CriteriaText varchar (1000)  ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  ,

	@CriteriaName varchar (250)  ,

	@SubQueryID int   ,

	@SubQueryLinkType varchar (50)  ,

	@ParamValue varchar (250)  ,

	@IsSecurityClause bit   ,

	@CriteriaSubstitutions varchar (2000)  ,

	@IsParameterizable bit   ,

	@Comparison varchar (50)  ,

	@IsJoinClause bit   
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditingCriteria]
					(
					[ClientID]
					,[SqlQueryEditingID]
					,[SqlQueryEditingTableID]
					,[SqlQueryEditingColumnID]
					,[CriteriaText]
					,[Criteria1]
					,[Criteria2]
					,[CriteriaName]
					,[SubQueryID]
					,[SubQueryLinkType]
					,[ParamValue]
					,[IsSecurityClause]
					,[CriteriaSubstitutions]
					,[IsParameterizable]
					,[Comparison]
					,[IsJoinClause]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryEditingID
					,@SqlQueryEditingTableID
					,@SqlQueryEditingColumnID
					,@CriteriaText
					,@Criteria1
					,@Criteria2
					,@CriteriaName
					,@SubQueryID
					,@SubQueryLinkType
					,@ParamValue
					,@IsSecurityClause
					,@CriteriaSubstitutions
					,@IsParameterizable
					,@Comparison
					,@IsJoinClause
					)
				-- Get the identity value
				SET @SqlQueryEditingCriteriaID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Insert] TO [sp_executeall]
GO
