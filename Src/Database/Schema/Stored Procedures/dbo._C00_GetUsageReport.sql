SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01/05/2019
-- Description:	Get Usage Report
--		51				Address Lookup
--		61, 55			SMS With Event
--		63				GBPortal
--		85				BankVal37
--		100				Email Out
--		101				Email In
--		102				Faxes sent
--		104				ID3 Identification matching
-- =============================================

CREATE PROCEDURE [dbo].[_C00_GetUsageReport]
	@ClientID INT,
	@IncludeBaseData BIT = 0,
	@From DateTime = NULL,
	@To DateTime = NULL
AS
BEGIN
                
    SET NOCOUNT ON;

    DECLARE @EmailOutCount INT
    DECLARE @EmailInCount INT      
    DECLARE @SMSOutCount INT
    DECLARE @AddressLookupCount INT
    DECLARE @BankValidationCount INT
    DECLARE @FaxOutCount INT
    DECLARE @EmailValidationCount INT
    DECLARE @ID3IdentificationMatchingCount INT

    IF(@From IS NULL) -- All from the begining of time
    BEGIN

		--Email OUT
		SELECT @EmailOutCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=100 AND ClientID = @ClientID
                                
		--Email IN
		SELECT @EmailInCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=101 AND ClientID = @ClientID
                                
		--SMS Sent
		SELECT @SMSOutCount = SUM((LEN(Notes) / 160) + 1) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=61 OR SourceAquariumOptionID=55 AND ClientID = @ClientID
                                
		--Address Lookup
		SELECT @AddressLookupCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=51 AND ClientID = @ClientID
                                
		--Bank Val
		SELECT @BankValidationCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=85 AND ClientID = @ClientID
                                
		--FAX Out 
		SELECT @FaxOutCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=102 AND ClientID = @ClientID
                                
		--Email Validation
		SELECT @EmailValidationCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=63 AND ClientID = @ClientID
                                
		--ID3 Identification Matching
		SELECT @ID3IdentificationMatchingCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=104 AND ClientID = @ClientID

    END
    ELSE
    BEGIN

        --Email OUT
        SELECT @EmailOutCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=100 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID = @ClientID
                                
        --Email IN
        SELECT @EmailInCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=101 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID = @ClientID
                                
        --SMS Sent
        SELECT @SMSOutCount = SUM((LEN(Notes) / 160) + 1) FROM ClientBillingDetail WITH (NOLOCK) WHERE ClientID = @ClientID AND (SourceAquariumOptionID=61 OR SourceAquariumOptionID=55) AND (WhenCreated>=@From AND WhenCreated<=@To)
                                
        --Address Lookup
        SELECT @AddressLookupCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=51 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --Bank Val
        SELECT @BankValidationCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=85 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --FAX Out 
        SELECT @FaxOutCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=102 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --Email Validation
        SELECT @EmailValidationCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=63 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --ID3 Identification Matching
        SELECT @ID3IdentificationMatchingCount = Count(*) FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=104 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID

    END


    SELECT  @EmailOutCount EmailOutCount,
            @EmailInCount EmailInCount,
            @SMSOutCount SMSOutCount,
            @AddressLookupCount AddressLookupCount,
            @BankValidationCount BankValidationCount,
            @FaxOutCount FaxOutCount,
            @EmailValidationCount EmailValidationCount,
            @ID3IdentificationMatchingCount ID3IdentificationMatchingCount

	if @IncludeBaseData = 1
	BEGIN

		--Email OUT
		SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=100 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID = @ClientID
                                
        --Email IN
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=101 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID = @ClientID
                                
        --SMS Sent
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE ClientID = @ClientID AND (SourceAquariumOptionID=61 OR SourceAquariumOptionID=55) AND (WhenCreated>=@From AND WhenCreated<=@To)
                                
        --Address Lookup
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=51 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --Bank Val
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=85 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --FAX Out 
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=102 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --Email Validation
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=63 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID
                                
        --ID3 Identification Matching
        SELECT * FROM ClientBillingDetail WITH (NOLOCK) WHERE SourceAquariumOptionID=104 AND WhenCreated>=@From AND WhenCreated<=@To AND ClientID=@ClientID


	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetUsageReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetUsageReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetUsageReport] TO [sp_executeall]
GO
