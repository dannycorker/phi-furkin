SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ActiveSession table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_GetByEmailAddress]
(

	@EmailAddress varchar (255)  
)
AS


				SELECT
					[SessionID],
					[EmailAddress]
				FROM
					[dbo].[ActiveSession] WITH (NOLOCK)
				WHERE
					[EmailAddress] = @EmailAddress
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetByEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_GetByEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetByEmailAddress] TO [sp_executeall]
GO
