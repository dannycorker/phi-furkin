SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadTypePageTitleSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Insert]
(

	@LeadTypePageTitleSpecialisedDetailFieldTargetID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@RowField int   ,

	@Notes varchar (250)  
)
AS


				
				INSERT INTO [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget]
					(
					[ClientID]
					,[LeadTypeID]
					,[Target]
					,[DetailFieldID]
					,[ColumnField]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[ColumnFieldAlias]
					,[RowField]
					,[Notes]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@Target
					,@DetailFieldID
					,@ColumnField
					,@TemplateTypeID
					,@DetailFieldAlias
					,@ColumnFieldAlias
					,@RowField
					,@Notes
					)
				-- Get the identity value
				SET @LeadTypePageTitleSpecialisedDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Insert] TO [sp_executeall]
GO
