SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 2010-01-11
-- Description:	List the aliases in use for a particular client, or all clients
-- Updates:		2010-01-13: Added Lead, Case, Matter and Customer display names
-- =============================================
CREATE PROCEDURE [dbo].[cla] 
	@ClientID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @ClientID IS NULL
	BEGIN
		
		SELECT c.ClientID, c.CompanyName, ISNULL(ab.LeadManagerAlias,'') AS [LeadManagerAlias], ISNULL(ab.LeadAlias,'') AS [LeadAlias], lt.LeadTypeID, lt.LeadTypeName, ISNULL(lt.LeadDisplayName,'') AS [LeadDisplayName], ISNULL(lt.CaseDisplayName,'') AS [CaseDisplayName], ISNULL(lt.MatterDisplayName,'') AS [MatterDisplayName], ISNULL(lt.CustomerDisplayName,'') AS [CustomerDisplayName]
		FROM Clients c
		LEFT JOIN ApplicationBranding ab on ab.ClientID = c.ClientID
		LEFT JOIN LeadType lt on lt.ClientID = c.ClientID
		ORDER BY CompanyName
	END
	ELSE
	BEGIN
		SELECT c.ClientID, c.CompanyName, ISNULL(ab.LeadManagerAlias,'') AS [LeadManagerAlias], ISNULL(ab.LeadAlias,'') AS [LeadAlias]
		FROM Clients c
		LEFT JOIN ApplicationBranding ab on ab.ClientID = c.ClientID
		WHERE ab.ClientID = @ClientID
		ORDER BY CompanyName
		
		SELECT lt.LeadTypeID, lt.LeadTypeName, lt.LeadDisplayName, lt.CaseDisplayName, lt.MatterDisplayName, lt.CustomerDisplayName
		FROM LeadType lt 
		WHERE lt.ClientID = @ClientID
		ORDER BY LeadTypeName
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[cla] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cla] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cla] TO [sp_executeall]
GO
