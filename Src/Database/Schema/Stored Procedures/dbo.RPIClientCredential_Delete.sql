SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIClientCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientCredential_Delete]
(

	@RPIClientCredentialID int   
)
AS


				DELETE FROM [dbo].[RPIClientCredential] WITH (ROWLOCK) 
				WHERE
					[RPIClientCredentialID] = @RPIClientCredentialID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientCredential_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Delete] TO [sp_executeall]
GO
