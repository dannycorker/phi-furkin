SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Inserts a record into the Document Zip Information table
-- Date: 13/02/2009
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentZipInformation__Insert]
(

	@ClientID int   ,

	@ServerMapPath varchar(2000)   ,

	@SecurePath varchar(2000)   ,

	@HostPath varchar (2000)  ,

	@WordTempDirectory varchar (2000)  ,

	@DocumentsPerZip int  ,

	@ZipPrefix varchar (1000)  ,
	
	@StatusID int,

	@TriggeredByAutomatedTaskID int = NULL,

	@TriggeredByDocumentTypeID int = NULL, 
	
	@WhoStored int = NULL, 
	
	@WhenStored datetime = NULL, 
	
	@WhenCreated datetime = NULL, 
	
	@EmailOnSuccess varchar(500) = NULL, 
	
	@EmailOnError varchar(500) = NULL

)
AS

	declare @DocumentZipInformationID int

					
				INSERT INTO [dbo].[DocumentZipInformation]
					(
					[ClientID]
					,[ServerMapPath]
					,[SecurePath]
					,[HostPath]
					,[WordTempDirectory]
					,[DocumentsPerZip]
					,[ZipPrefix]
					,[StatusID]
					,[TriggeredByAutomatedTaskID]
					,[TriggeredByDocumentTypeID]
					,[WhoStored] 
					,[WhenStored] 
					,[WhenCreated] 
					,[EmailOnSuccess] 
					,[EmailOnError] 
					)
				VALUES
					(
					@ClientID
					,@ServerMapPath
					,@SecurePath
					,@HostPath
					,@WordTempDirectory
					,@DocumentsPerZip
					,@ZipPrefix
					,@StatusID
					,@TriggeredByAutomatedTaskID
					,@TriggeredByDocumentTypeID
					,@WhoStored 
					,dbo.fn_GetDate_Local() 
					,@WhenCreated 
					,@EmailOnSuccess 
					,@EmailOnError 
					)
				
				-- Get the identity value
				SET @DocumentZipInformationID = SCOPE_IDENTITY()
									
Select DocumentZipInformationID from DocumentZipInformation where DocumentZipInformationID = @DocumentZipInformationID
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__Insert] TO [sp_executeall]
GO
