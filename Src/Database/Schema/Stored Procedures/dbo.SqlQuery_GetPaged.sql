SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the SqlQuery table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [QueryID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([QueryID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [QueryID]'
				SET @SQL = @SQL + ' FROM [dbo].[SqlQuery] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[QueryID], O.[ClientID], O.[QueryText], O.[QueryTitle], O.[AutorunOnline], O.[OnlineLimit], O.[BatchLimit], O.[SqlQueryTypeID], O.[FolderID], O.[IsEditable], O.[IsTemplate], O.[IsDeleted], O.[WhenCreated], O.[CreatedBy], O.[OwnedBy], O.[RunCount], O.[LastRundate], O.[LastRuntime], O.[LastRowcount], O.[MaxRuntime], O.[MaxRowcount], O.[AvgRuntime], O.[AvgRowcount], O.[Comments], O.[WhenModified], O.[ModifiedBy], O.[LeadTypeID], O.[ParentQueryID], O.[IsParent], O.[SqlQueryTemplateID], O.[OutputFormat], O.[ShowInCustomSearch], O.[OutputFileExtension], O.[OutputSeparatorCharmapID], O.[OutputEncapsulatorCharmapID], O.[SuppressHeaderRow], O.[LockAllTables], O.[SourceID]
				FROM
				    [dbo].[SqlQuery] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[QueryID] = PageIndex.[QueryID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[SqlQuery] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_GetPaged] TO [sp_executeall]
GO
