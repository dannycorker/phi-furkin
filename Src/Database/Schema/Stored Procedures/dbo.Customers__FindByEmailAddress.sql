SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2014
-- Description:	Find customers via email address exact match only 
--				Does not include business customers
--				Ordered by full name ascending
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByEmailAddress]

	@ClientID INT,
	@Email VARCHAR(255)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND EmailAddress like '%' + @Email + '%' AND IsBusiness=0
	ORDER BY Fullname ASC
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByEmailAddress] TO [sp_executeall]
GO
