SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 26/06/2015
-- Description:	Deletes the navigation criteria based upon the from step section ID
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria__DeleteByFromStepSectionID]
	@ClientID INT,
	@FromStepSectionID INT
AS
BEGIN
	
	
	SET NOCOUNT ON;

	DELETE FROM ScriptSectionNavigationCriteria
	WHERE ClientID=@ClientID AND FromStepSectionID = @FromStepSectionID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__DeleteByFromStepSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria__DeleteByFromStepSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__DeleteByFromStepSectionID] TO [sp_executeall]
GO
