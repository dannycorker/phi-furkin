SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PanelItemTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemTypes_Update]
(

	@PanelItemTypeID int   ,

	@PanelItemTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PanelItemTypes]
				SET
					[PanelItemTypeName] = @PanelItemTypeName
				WHERE
[PanelItemTypeID] = @PanelItemTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemTypes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemTypes_Update] TO [sp_executeall]
GO
