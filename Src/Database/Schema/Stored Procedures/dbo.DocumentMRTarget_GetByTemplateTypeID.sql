SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentMRTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentMRTarget_GetByTemplateTypeID]
(

	@TemplateTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentMRTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[ObjectName],
					[PropertyName],
					[Notes],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentMRTarget] WITH (NOLOCK) 
				WHERE
					[TemplateTypeID] = @TemplateTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_GetByTemplateTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
