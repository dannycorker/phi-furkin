SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-28
-- Description:	Get all function control records for this User
--              2008-07-16: Encrypted Detail Fields
-- =============================================
CREATE PROCEDURE [dbo].[GetUserRightsByFunctionType]
	@GroupID int,
	@UserID int,
	@FunctionTypeID int,
	@LeadTypeID int,
	@OfficeID int = null
AS
BEGIN
	SET NOCOUNT ON;

    -- 1) Get the Control record for this function type
	SELECT gfc.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	gfc.GroupFunctionControlID as 'UniqueID',  
	-1 as 'LeadTypeID'  
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID = @FunctionTypeID
	AND gfc.LeadTypeID IS NULL
	AND @LeadTypeID = -1
	and not exists (select * from userfunctioncontrol ufc where ufc.functiontypeid = gfc.functiontypeid and ufc.LeadTypeID IS NULL and @LeadTypeID = -1 and ufc.clientpersonnelid = @UserID)
	UNION
	SELECT ufc.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	ufc.UserFunctionControlID as 'UniqueID',  
	-1 as 'LeadTypeID'    
	FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID = @FunctionTypeID
	AND ufc.LeadTypeID IS NULL
	AND @LeadTypeID = -1

	UNION

	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	gfc.GroupFunctionControlID as 'UniqueID',  
	gfc.LeadTypeID as 'LeadTypeID'   
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID 
	INNER JOIN LeadType lt ON lt.LeadTypeID = gfc.LeadTypeID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID = @FunctionTypeID
	AND gfc.LeadTypeID = lt.LeadTypeID
	AND gfc.LeadTypeID = @LeadTypeID
	and not exists (select * from userfunctioncontrol ufc where ufc.functiontypeid = gfc.functiontypeid and ufc.LeadTypeID = gfc.LeadTypeID and ufc.LeadTypeID = @LeadTypeID and ufc.clientpersonnelid = @UserID)
	UNION
	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	1 as 'Source', 
	ufc.UserFunctionControlID as 'UniqueID',  
	ufc.LeadTypeID as 'LeadTypeID'   
	FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID 
	INNER JOIN LeadType lt ON lt.LeadTypeID = ufc.LeadTypeID 
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID = @FunctionTypeID
	AND ufc.LeadTypeID = lt.LeadTypeID
	AND ufc.LeadTypeID = @LeadTypeID

	UNION

	-- Very special case to show all the possible
	-- virtual lead types when the user clicks "Lead Types"
	SELECT gfc.FunctionTypeID as 'ID', 
	lt.LeadTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	4 as 'Source', 
	gfc.GroupFunctionControlID as 'UniqueID',  
	-1 as 'LeadTypeID'  
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID
	INNER JOIN LeadType lt ON lt.LeadTypeID = gfc.LeadTypeID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID = 23
	AND @FunctionTypeID = 7
	AND gfc.LeadTypeID IS NOT NULL
	AND @LeadTypeID = -1
	and not exists (select * from userfunctioncontrol ufc where ufc.functiontypeid = gfc.functiontypeid and ufc.LeadTypeID = gfc.LeadTypeID and ufc.clientpersonnelid = @UserID)
	UNION
	-- Very special case to show all the possible
	-- virtual lead types when the user clicks "Lead Types"
	SELECT ufc.FunctionTypeID as 'ID', 
	lt.LeadTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	4 as 'Source', 
	ufc.UserFunctionControlID as 'UniqueID',  
	-1 as 'LeadTypeID'  
	FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	INNER JOIN LeadType lt ON lt.LeadTypeID = ufc.LeadTypeID 
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID = 23
	AND @FunctionTypeID = 7
	AND ufc.LeadTypeID IS NOT NULL
	AND @LeadTypeID = -1


	UNION

    -- 2) Get the Control records for the direct descendants of this function type
	SELECT ft.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	gfc.GroupFunctionControlID as 'UniqueID',  
	-1 as 'LeadTypeID'   
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND gfc.LeadTypeID IS NULL 
	AND @LeadTypeID = -1
	and not exists (select * from userfunctioncontrol ufc where ufc.functiontypeid = gfc.functiontypeid and ufc.LeadTypeID IS NULL and ufc.clientpersonnelid = @UserID)
	UNION
	SELECT ft.FunctionTypeID as 'ID', 
	ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	ufc.UserFunctionControlID as 'UniqueID',  
	-1 as 'LeadTypeID'   
	FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND ufc.LeadTypeID IS NULL 
	AND @LeadTypeID = -1

	UNION

    -- Special case for virtual function type 23, representing individual lead types
	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	gfc.RightID as 'Rights', 
	gfc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	gfc.GroupFunctionControlID as 'UniqueID',  
	gfc.LeadTypeID as 'LeadTypeID'   
	FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID 
	INNER JOIN LeadType lt ON lt.LeadTypeID = gfc.LeadTypeID 
	WHERE gfc.ClientPersonnelAdminGroupID = @GroupID 
	AND gfc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND gfc.LeadTypeID = lt.LeadTypeID
	AND gfc.LeadTypeID = @LeadTypeID
	and not exists (select * from userfunctioncontrol ufc where ufc.functiontypeid = gfc.functiontypeid and ufc.LeadTypeID = gfc.LeadTypeID and ufc.clientpersonnelid = @UserID)
	UNION
    -- Special case for virtual function type 23, representing individual lead types
	SELECT ft.FunctionTypeID as 'ID', 
	lt.LeadTypeName + ' : ' + ft.FunctionTypeName as 'Name', 
	ufc.RightID as 'Rights', 
	ufc.HasDescendants as 'HasDescendants', 
	2 as 'Source', 
	ufc.UserFunctionControlID as 'UniqueID',  
	ufc.LeadTypeID as 'LeadTypeID'   
	FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID
	INNER JOIN LeadType lt ON lt.LeadTypeID = ufc.LeadTypeID 
	WHERE ufc.ClientPersonnelID = @UserID 
	AND ufc.FunctionTypeID IN (select FunctionTypeID from FunctionType where ParentFunctionTypeID = @FunctionTypeID)
	AND ufc.LeadTypeID = lt.LeadTypeID
	AND ufc.LeadTypeID = @LeadTypeID


	UNION

    -- 3) Get any dynamic rights records for this function type
	--    This section is for general things like "Can this user click the Save Customer button", 
    --    rather than for specific rights for Pages-By-Lead-Type etc.
	SELECT grd.ObjectID as 'ID', 
	f.FunctionName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	3 as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	-1 as 'LeadTypeID'    
	FROM GroupRightsDynamic grd INNER JOIN Functions f ON f.FunctionID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID 
	AND grd.LeadTypeID IS NULL
	AND @LeadTypeID = -1 -- Only show these when general rights are requested, not when EventType rights by lead type (12, 13 and 25) are being requested!
	AND @FunctionTypeID <> 26 /* Exclude NoteTypes by Client */
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid is null and @LeadTypeID = -1 and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	f.FunctionName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	3 as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	-1 as 'LeadTypeID'    
	FROM UserRightsDynamic urd INNER JOIN Functions f ON f.FunctionID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID 
	AND urd.LeadTypeID IS NULL
	AND @LeadTypeID = -1 -- Only show these when general rights are requested, not when EventType rights by lead type (12, 13 and 25) are being requested!
	AND @FunctionTypeID <> 26 /* Exclude NoteTypes by Client */

	UNION

	-- Specific: Outcomes
	SELECT grd.ObjectID as 'ID', 
	o.OutcomeName COLLATE SQL_Latin1_General_CP1_CI_AS as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	-1 as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN Outcomes o ON o.OutcomeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 9 -- 9=Outcomes 
	AND grd.LeadTypeID IS NULL
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid is null and grd.leadtypeid is null and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID) 
	UNION
	SELECT urd.ObjectID as 'ID', 
	o.OutcomeName COLLATE SQL_Latin1_General_CP1_CI_AS as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	-1 as 'LeadTypeID'   
	FROM UserRightsDynamic urd INNER JOIN Outcomes o ON o.OutcomeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 9 -- 9=Outcomes 
	AND urd.LeadTypeID IS NULL

	UNION

	-- Specific: Lead Types by Office
	SELECT grd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN LeadType lt ON lt.LeadTypeID = grd.LeadTypeID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 11 -- 11=Lead Types by Office
	AND grd.LeadTypeID IS NOT NULL
	AND grd.ObjectID = @OfficeID 
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid = grd.leadtypeid and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID'   
	FROM UserRightsDynamic urd INNER JOIN LeadType lt ON lt.LeadTypeID = urd.LeadTypeID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 11 -- 11=Lead Types by Office
	AND urd.LeadTypeID IS NOT NULL
	AND urd.ObjectID = @OfficeID 

	UNION

	-- Specific: Pages by Lead Type
	SELECT grd.ObjectID as 'ID', 
	dfp.PageName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN DetailFieldPages dfp ON dfp.DetailFieldPageID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 12 -- 12=Pages 
	AND grd.LeadTypeID = @LeadTypeID
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid = grd.leadtypeid and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	dfp.PageName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID'  
	FROM UserRightsDynamic urd INNER JOIN DetailFieldPages dfp ON dfp.DetailFieldPageID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 12 -- Pages 
	AND urd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Event Types by Lead Type
	SELECT grd.ObjectID as 'ID', 
	et.EventTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN EventType et ON et.EventTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 13 -- 13=Events 
	AND grd.LeadTypeID = @LeadTypeID
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid = grd.leadtypeid and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	et.EventTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID'    
	FROM UserRightsDynamic urd INNER JOIN EventType et ON et.EventTypeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 13 -- Events 
	AND urd.LeadTypeID = @LeadTypeID

	UNION

	-- Specific: Encrypted Detail Fields by Lead Type
	SELECT grd.ObjectID as 'ID', 
	df.FieldName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN DetailFields df ON df.DetailFieldID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 25 -- 25=Encrypted Detail Fields 
	AND grd.LeadTypeID = @LeadTypeID
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid = grd.leadtypeid and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	df.FieldName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID'    
	FROM UserRightsDynamic urd INNER JOIN DetailFields df ON df.DetailFieldID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 25 -- 25=Encrypted Detail Fields 
	AND urd.LeadTypeID = @LeadTypeID

	UNION
	
	-- Specific: Notes Types for this ClientID
	SELECT grd.ObjectID as 'ID', 
	nt.NoteTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	grd.GroupRightsDynamicID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN NoteType nt ON nt.NoteTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 26 -- 26=Notes 
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid = grd.leadtypeid and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	nt.NoteTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID'    
	FROM UserRightsDynamic urd INNER JOIN NoteType nt ON nt.NoteTypeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 26 -- Notes 

	UNION

	-- Specific: Lead Type Rights
	SELECT grd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	grd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', -- Virtual function type 23 (variable by lead type)
	grd.GroupRightsDynamicID as 'UniqueID',  
	grd.LeadTypeID as 'LeadTypeID'   
	FROM GroupRightsDynamic grd INNER JOIN LeadType lt ON lt.LeadTypeID = grd.ObjectID 
	WHERE grd.ClientPersonnelAdminGroupID = @GroupID 
	AND grd.FunctionTypeID = @FunctionTypeID
	AND grd.FunctionTypeID = 23 -- 23= Specific Lead Type
	AND grd.LeadTypeID = @LeadTypeID 
	and not exists (select * from userrightsdynamic urd where urd.functiontypeid = grd.functiontypeid and urd.leadtypeid = grd.leadtypeid and urd.objectid = grd.objectid and urd.clientpersonnelid = @UserID)
	UNION
	SELECT urd.ObjectID as 'ID', 
	lt.LeadTypeName as 'Name', 
	urd.RightID as 'Rights', 
	0 as 'HasDescendants', 
	@FunctionTypeID as 'Source', 
	urd.UserRightsDynamicID as 'UniqueID',  
	urd.LeadTypeID as 'LeadTypeID'   
	FROM UserRightsDynamic urd INNER JOIN LeadType lt ON lt.LeadTypeID = urd.ObjectID 
	WHERE urd.ClientPersonnelID = @UserID 
	AND urd.FunctionTypeID = @FunctionTypeID
	AND urd.FunctionTypeID = 23 -- 23= Specific Lead Type
	AND urd.LeadTypeID = @LeadTypeID 

	ORDER BY 5, 2, 1

END















GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserRightsByFunctionType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserRightsByFunctionType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserRightsByFunctionType] TO [sp_executeall]
GO
