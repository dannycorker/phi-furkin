SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-06-15
-- Description:	Saves multiple objects and their detail fields
-- TODO:		1. DONE - Need to make sure that object links are added in the same from and to direction as the relationship has been defined
--				2. Same with links removed - if you clear out a link on the to object the tvpObjectLink may have the ids the wrong way round
--				3. DONE We clear out existing one to one links when adding a new one.  Need to do the same on the back link of one to many
--				4. Prevent duplicate entries in the object link table
-- =============================================
CREATE PROCEDURE [dbo].[Object__Save]
(	
	@ClientID INT,
	@SubClientID INT = NULL,
	@Objects tvpObject READONLY,
	@LinksAdded tvpObjectLink READONLY,
	@LinksRemoved tvpObjectLink READONLY,
	@DetailValues tvpDetailValue READONLY,
	@WhoSaving INT,
	@ObjectsDelete tvpInt READONLY
)
AS
BEGIN
	
	-- Used to link the ids passed in to the new keys generated from the insert
	DECLARE @ObjectIDs TABLE
	(
		PassedID INT,
		ObjectID INT
	)	
	-- Insert the new objects
	INSERT dbo.[Objects] (ClientID, SubClientID, ObjectTypeID, Name, Details, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	OUTPUT inserted.SourceID, inserted.ObjectID INTO @ObjectIDs
	SELECT @ClientID, @SubClientID, ObjectTypeID, Name, Details, @WhoSaving, dbo.fn_GetDate_Local(), @WhoSaving, dbo.fn_GetDate_Local(), ObjectID
	FROM @Objects
	WHERE ObjectID < 1
	
	-- Update the object ids to the newly generated ones
	DECLARE @LinksAddedValid tvpObjectLink
	INSERT INTO @LinksAddedValid (FromObjectID, ToObjectID, ObjectTypeRelationshipID)
	SELECT FromObjectID, ToObjectID, ObjectTypeRelationshipID 
	FROM @LinksAdded a

	UPDATE a
	SET FromObjectID = o.ObjectID
	FROM @LinksAddedValid a
	INNER JOIN @ObjectIDs o ON a.FromObjectID = o.PassedID 
	WHERE FromObjectID < 1
	
	UPDATE a
	SET ToObjectID = o.ObjectID
	FROM @LinksAddedValid a
	INNER JOIN @ObjectIDs o ON a.ToObjectID = o.PassedID  
	WHERE ToObjectID < 1
	
	
	-- Now update the name and details for existing objects
	UPDATE o
	SET Name = obj.Name, Details = obj.Details
	FROM dbo.[Objects] o
	INNER JOIN @Objects obj ON o.ObjectID = obj.ObjectID 
	WHERE obj.ObjectID > 0
	AND o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID)
	
	
	
	-- Remove links between objects
	DELETE ol
	FROM dbo.ObjectLink ol
	INNER JOIN dbo.[Objects] fromObject WITH (NOLOCK) ON ol.FromObjectID = fromObject.ObjectID
	INNER JOIN dbo.[Objects] toObject WITH (NOLOCK) ON ol.ToObjectID = toObject.ObjectID
	INNER JOIN @LinksRemoved r ON ol.ObjectTypeRelationshipID = r.ObjectTypeRelationshipID AND ol.FromObjectID = r.FromObjectID AND ol.ToObjectID = r.ToObjectID 
	WHERE toObject.ClientID = @ClientID
	AND (@SubClientID IS NULL OR toObject.SubClientID = @SubClientID)
	AND fromObject.ClientID = @ClientID
	AND (@SubClientID IS NULL OR fromObject.SubClientID = @SubClientID)
	
	-- Remove links for one to one relationships for objects we are adding - prevents issues with multiple one to one links
	DELETE ol
	FROM dbo.ObjectLink ol
	INNER JOIN dbo.[Objects] fromObject WITH (NOLOCK) ON ol.FromObjectID = fromObject.ObjectID
	INNER JOIN dbo.[Objects] toObject WITH (NOLOCK) ON ol.ToObjectID = toObject.ObjectID
	INNER JOIN @LinksAddedValid a ON ol.ObjectTypeRelationshipID = a.ObjectTypeRelationshipID AND (ol.FromObjectID = a.FromObjectID OR ol.FromObjectID = a.ToObjectID)
	INNER JOIN dbo.ObjectTypeRelationship r WITH (NOLOCK) ON ol.ObjectTypeRelationshipID = r.ObjectTypeRelationshipID 
	WHERE toObject.ClientID = @ClientID
	AND (@SubClientID IS NULL OR toObject.SubClientID = @SubClientID)
	AND fromObject.ClientID = @ClientID
	AND (@SubClientID IS NULL OR fromObject.SubClientID = @SubClientID)
	AND r.RelationshipTypeID = 52
	
	
	-- Add new links between ojects
	-- The in statements are so we can force the links to go in the same direction as defined in the relationship
	INSERT dbo.ObjectLink (ObjectTypeRelationshipID, FromObjectID, ToObjectID)
	SELECT a.ObjectTypeRelationshipID, fromObject.ObjectID, toObject.ObjectID
	FROM @LinksAddedValid a
	INNER JOIN dbo.ObjectTypeRelationship r WITH (NOLOCK) ON a.ObjectTypeRelationshipID = r.ObjectTypeRelationshipID
	INNER JOIN dbo.Objects fromObject WITH (NOLOCK) ON r.FromObjectTypeID = fromObject.ObjectTypeID AND fromObject.ObjectID IN (a.FromObjectID, a.ToObjectID)
	INNER JOIN dbo.Objects toObject WITH (NOLOCK) ON r.ToObjectTypeID = toObject.ObjectTypeID AND toObject.ObjectID IN (a.FromObjectID, a.ToObjectID)
	WHERE toObject.ClientID = @ClientID
	AND (@SubClientID IS NULL OR toObject.SubClientID = @SubClientID)
	AND fromObject.ClientID = @ClientID
	AND (@SubClientID IS NULL OR fromObject.SubClientID = @SubClientID)
	
	
	-- This removes any duplicate back links for the one to many type to keep the relationships correct.  Similar to the SQL for one to one relationships above.
	;WITH InnerSql AS 
	(
	SELECT ol.ObjectLinkID, ROW_NUMBER() OVER(PARTITION BY ol.ToObjectID, ol.ObjectTypeRelationshipID ORDER BY ol.ObjectLinkID DESC) as rn 
	FROM dbo.ObjectLink ol
	INNER JOIN dbo.ObjectTypeRelationship r WITH (NOLOCK) ON ol.ObjectTypeRelationshipID = r.ObjectTypeRelationshipID 
	WHERE r.RelationshipTypeID = 53
	)
	DELETE ol
	FROM dbo.ObjectLink ol
	INNER JOIN InnerSql i ON ol.ObjectLinkID = i.ObjectLinkID
	WHERE rn > 1 

	
	-- Update existing detail values
	UPDATE v
	SET v.DetailValue = df.DetailValue
	FROM dbo.ObjectDetailValues v
	INNER JOIN @DetailValues df ON v.DetailFieldID = df.DetailFieldID AND v.ObjectID = df.AnyID
	WHERE df.AnyID > 0
	AND v.ClientID = @ClientID
	AND (@SubClientID IS NULL OR v.SubClientID = @SubClientID)
	
	-- Now insert the rest
	INSERT dbo.ObjectDetailValues (ClientID, SubClientID, ObjectID, DetailFieldID, DetailValue)
	SELECT @ClientID, @SubClientID, ISNULL(id.ObjectID, df.AnyID), df.DetailFieldID, df.DetailValue
	FROM @DetailValues df
	LEFT JOIN @ObjectIDs id ON df.AnyID = id.PassedID -- use the primary key created above if a df of an inserted object
	LEFT JOIN dbo.ObjectDetailValues v WITH (NOLOCK) ON v.DetailFieldID = df.DetailFieldID AND v.ObjectID = df.AnyID
	WHERE v.ObjectDetailValueID IS NULL
	
	
	-- Delete from object detail values...
	DELETE odv
	FROM dbo.ObjectDetailValues odv
	INNER JOIN @ObjectsDelete d ON odv.ObjectID = d.AnyID 
	INNER JOIN dbo.Objects o ON d.AnyID = o.ObjectID
	WHERE o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID) 
	
	-- and object links from and to...
	DELETE l
	FROM dbo.ObjectLink l
	INNER JOIN @ObjectsDelete d ON l.FromObjectID = d.AnyID 
	INNER JOIN dbo.Objects o ON d.AnyID = o.ObjectID
	WHERE o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID) 
	
	DELETE l
	FROM dbo.ObjectLink l
	INNER JOIN @ObjectsDelete d ON l.ToObjectID = d.AnyID 
	INNER JOIN dbo.Objects o ON d.AnyID = o.ObjectID
	WHERE o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID) 
	
	-- and clear out lead...
	UPDATE dv
	SET dv.DetailValue = ''
	FROM @ObjectsDelete d
	INNER JOIN dbo.Objects o ON d.AnyID = o.ObjectID 
	INNER JOIN dbo.DetailFields df ON o.ObjectTypeID = df.ObjectTypeID AND df.QuestionTypeID = 22 -- question type when linking an object to an AQ entity like lead
	INNER JOIN dbo.LeadDetailValues dv ON df.DetailFieldID = dv.DetailFieldID AND o.ObjectID = dv.ValueInt
	WHERE dv.ClientID = @ClientID
	AND o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID)
	-- matter...
	UPDATE dv
	SET dv.DetailValue = ''
	FROM @ObjectsDelete d
	INNER JOIN dbo.Objects o ON d.AnyID = o.ObjectID 
	INNER JOIN dbo.DetailFields df ON o.ObjectTypeID = df.ObjectTypeID AND df.QuestionTypeID = 22 
	INNER JOIN dbo.MatterDetailValues dv ON df.DetailFieldID = dv.DetailFieldID AND o.ObjectID = dv.ValueInt
	WHERE dv.ClientID = @ClientID
	AND o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID)
	-- and customer detail values...
	UPDATE dv
	SET dv.DetailValue = ''
	FROM @ObjectsDelete d
	INNER JOIN dbo.Objects o ON d.AnyID = o.ObjectID 
	INNER JOIN dbo.DetailFields df ON o.ObjectTypeID = df.ObjectTypeID AND df.QuestionTypeID = 22 
	INNER JOIN dbo.CustomerDetailValues dv ON df.DetailFieldID = dv.DetailFieldID AND o.ObjectID = dv.ValueInt
	WHERE dv.ClientID = @ClientID
	AND o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID)
	
	-- and finally remove the objects
	DELETE o
	FROM dbo.Objects o
	INNER JOIN @ObjectsDelete d ON o.ObjectID = d.AnyID 
	WHERE o.ClientID = @ClientID
	AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID) 	
	
	-- Return the new IDs so can load them into the saved objects 
	SELECT *
	FROM @ObjectIDs
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Object__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Object__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Object__Save] TO [sp_executeall]
GO
