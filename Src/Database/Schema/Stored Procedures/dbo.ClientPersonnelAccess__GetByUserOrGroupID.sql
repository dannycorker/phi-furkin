SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-05
-- Description:	Get existing access details so that the app can display and manage them
-- JWG 2014-01-31 #25493 Get the level of access too (0=none, 1=view, 4=full)
-- JWG 2014-05-13 #26507 LatestRecordsFirst 
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnelAccess__GetByUserOrGroupID]
	@ClientID int,
	@UserID int = null,
	@GroupID int = null,
	@LeadID int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LatestRecordsFirst BIT = 0

	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	WHERE cl.ClientID = @ClientID

	/*
		Show all existing access records for the user or group passed in, optionally filtered to a specific lead.
	*/
	IF @UserID > 0
	BEGIN
		/* ClientID included for belt-and-braces, but see groups below which is subtly different. */
		SELECT cpa.ClientPersonnelAccessID as [PortalUserCaseID], cpa.LeadID, cpa.CaseID, ISNULL(r.RightName, 'FULL') AS [AccessLevel]
		FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) 
		LEFT JOIN dbo.Rights r WITH (NOLOCK) ON r.RightID = cpa.AccessLevel
		WHERE cpa.ClientID = @ClientID 
		AND cpa.ClientPersonnelID = @UserID 
		AND (@LeadID IS NULL OR cpa.LeadID = @LeadID)
		ORDER BY 
			/*
				JWG 2014-05-13 #26507 LatestRecordsFirst
				The dynamic part is really the ASC/DESC rather than the column to order by, but this is the only way to do it at present.
			*/
			CASE @LatestRecordsFirst WHEN 1 THEN cpa.LeadID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN cpa.LeadID END ASC, 
			CASE @LatestRecordsFirst WHEN 1 THEN cpa.CaseID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN cpa.CaseID END ASC 
	END
	ELSE
	IF @GroupID > 0
	BEGIN
		/* 
			Some groups belong to client zero and are shared by all other clients,
			so make sure that the rule belongs to the current client only!
		*/
		SELECT cpa.ClientPersonnelAccessID as [PortalUserCaseID], cpa.LeadID, cpa.CaseID, ISNULL(r.RightName, 'FULL') AS [AccessLevel] 
		FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) 
		LEFT JOIN dbo.Rights r WITH (NOLOCK) ON r.RightID = cpa.AccessLevel
		WHERE cpa.ClientID = @ClientID 
		AND cpa.ClientPersonnelAdminGroupID = @GroupID 
		AND (@LeadID IS NULL OR cpa.LeadID = @LeadID)
		ORDER BY 
			/*
				JWG 2014-05-13 #26507 LatestRecordsFirst
				The dynamic part is really the ASC/DESC rather than the column to order by, but this is the only way to do it at present.
			*/
			CASE @LatestRecordsFirst WHEN 1 THEN cpa.LeadID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN cpa.LeadID END ASC, 
			CASE @LatestRecordsFirst WHEN 1 THEN cpa.CaseID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN cpa.CaseID END ASC
 	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess__GetByUserOrGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess__GetByUserOrGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess__GetByUserOrGroupID] TO [sp_executeall]
GO
