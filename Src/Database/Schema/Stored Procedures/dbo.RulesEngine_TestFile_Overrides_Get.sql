SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-09-01
-- Description:	Gets the base test file overrides based on rule set
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_TestFile_Overrides_Get]
(
	@ClientID INT,
	@RuleSetID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	;WITH Param_Hierarchy(ClientID, ParentRuleSetID, RuleSetID, ParentRuleID, RuleID, RuleParameterID, ParameterName, ParameterTypeID, ParameterValue, Parent, DataTypeID) 
	AS 
	(
		SELECT r.ClientID, r.RuleSetID, r.RuleSetID, r.RuleID, r.RuleID, p.RuleParameterID, p.Name, p.ParameterTypeID, p.Value, p.Parent, p.DataTypeID
		FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON r.RuleID = p.RuleID
		WHERE	r.ClientID = @ClientID
		
		UNION ALL
		
		SELECT r.ClientID,h.ParentRuleSetID, r.RuleSetID, h.RuleID, r.RuleID, p.RuleParameterID, p.Name, p.ParameterTypeID, p.Value, p.Parent, p.DataTypeID
		FROM Param_Hierarchy h
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON h.ParameterValue = r.RuleSetID 
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON r.RuleID = p.RuleID
		WHERE	h.ParameterTypeID = 3
		AND		r.ClientID = @ClientID
		
	)
	SELECT DISTINCT h.ClientID, MAX(h.RuleParameterID) RuleParameterID, h.ParameterName, h.ParameterTypeID, h.ParameterValue, h.Parent, h.DataTypeID
	FROM Param_Hierarchy h
	WHERE h.ParentRuleSetID = @RuleSetID
	AND h.ParameterTypeID != 3
	AND h.ParameterTypeID != 7
	AND (h.ParameterTypeID != 2 OR h.ParameterValue NOT IN ('All', 'Any'))
	GROUP BY h.ClientID, h.ParameterName, h.ParameterTypeID, h.ParameterValue, h.Parent, h.DataTypeID
	ORDER BY h.ParameterName
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Overrides_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_TestFile_Overrides_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Overrides_Get] TO [sp_executeall]
GO
