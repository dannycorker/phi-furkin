SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteEquation] @EquationID int
AS
Delete From Equations
Where EquationID = @EquationID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteEquation] TO [sp_executeall]
GO
