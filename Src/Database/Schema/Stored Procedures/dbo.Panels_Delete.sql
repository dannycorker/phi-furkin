SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Panels table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_Delete]
(

	@PanelID int   
)
AS


				DELETE FROM [dbo].[Panels] WITH (ROWLOCK) 
				WHERE
					[PanelID] = @PanelID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Delete] TO [sp_executeall]
GO
