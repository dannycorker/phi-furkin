SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_GetByNewCaseID]
(

	@NewCaseID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CaseTransferMappingID],
					[ClientRelationshipID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID],
					[NewClientID],
					[NewCustomerID],
					[NewLeadID],
					[NewCaseID],
					[NewMatterID],
					[CaseTransferStatusID],
					[NewLeadTypeID]
				FROM
					[dbo].[CaseTransferMapping] WITH (NOLOCK) 
				WHERE
					[NewCaseID] = @NewCaseID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByNewCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_GetByNewCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByNewCaseID] TO [sp_executeall]
GO
