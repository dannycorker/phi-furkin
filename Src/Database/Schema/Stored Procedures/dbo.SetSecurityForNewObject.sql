SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-15
-- Description:	Copy default Rights for new objects that the user has created (Event Type etc)
-- 2011-03-16 JWG New value of -1 for @Level means use the default RightID from the parent level
-- =============================================
CREATE PROCEDURE [dbo].[SetSecurityForNewObject]
	@FunctionTypeID int,
	@LeadTypeID int,
	@ObjectID int,
	@Level int,
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	/* 
		If an Outcome has just been added, then look for any Groups with a 
		GroupFunctionControl record of type 9 where HasDescendants = 1.  
		For each record found, add a new GroupRightsDynamic record for this new Outcome.
		Then do it all again for individual User rights.
	*/
	IF @FunctionTypeID = 9
	BEGIN

		/* 
			Level -1 means use default rights as set by the parent branch.
			Levels 0 to 4 mean "None" to "Full"
		*/
		/* Outcome Groups */
		INSERT INTO GroupRightsDynamic (
			ClientPersonnelAdminGroupID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			gfc.ClientPersonnelAdminGroupID,
			@FunctionTypeID, 
			NULL, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN gfc.RightID ELSE @Level END 
		FROM GroupFunctionControl gfc 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		WHERE gfc.FunctionTypeID = @FunctionTypeID 
		AND gfc.LeadTypeID IS NULL 
		AND @LeadTypeID = -1
		AND gfc.HasDescendants = 1 
		AND cpg.ClientID = @ClientID

		/* Outcome Users */
		INSERT INTO UserRightsDynamic (
			ClientPersonnelID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			ufc.ClientPersonnelID,
			@FunctionTypeID, 
			NULL, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN ufc.RightID ELSE @Level END
		FROM UserFunctionControl ufc 
		INNER JOIN ClientPersonnel cp ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE ufc.FunctionTypeID = @FunctionTypeID 
		AND ufc.LeadTypeID IS NULL 
		AND @LeadTypeID = -1
		AND ufc.HasDescendants = 1 
		AND cp.ClientID = @ClientID
		
	END
	
	/*
		If a new Page Type (12) or Event Type (13) has just been added, then look for 
		GroupFunctionControl records where the parent is Virtual Lead Type (23) for the same Lead Type,
		and where HasDescendants = 1.  For each record found, add a new GroupRightsDynamic record.
	*/
	
	IF @FunctionTypeID = 12 OR @FunctionTypeID = 13 OR @FunctionTypeID = 25
	BEGIN

		/* Page/Event/Encryption Groups */
		INSERT INTO GroupRightsDynamic (
			ClientPersonnelAdminGroupID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			gfc.ClientPersonnelAdminGroupID,
			@FunctionTypeID, 
			@LeadTypeID, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN gfc.RightID ELSE @Level END
		FROM GroupFunctionControl gfc 
		/*INNER JOIN FunctionType ft ON gfc.FunctionTypeID = ft.ParentFunctionTypeID 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		WHERE ft.FunctionTypeID = @FunctionTypeID */
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		WHERE gfc.FunctionTypeID = @FunctionTypeID 
		AND gfc.LeadTypeID = @LeadTypeID 
		AND gfc.HasDescendants = 1 
		AND cpg.ClientID = @ClientID

		/* Page/Event/Encryption Groups */
		INSERT INTO UserRightsDynamic (
			ClientPersonnelID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			ufc.ClientPersonnelID,
			@FunctionTypeID, 
			@LeadTypeID, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN ufc.RightID ELSE @Level END
		FROM UserFunctionControl ufc 
		/*INNER JOIN FunctionType ft ON ufc.FunctionTypeID = ft.ParentFunctionTypeID 
		INNER JOIN ClientPersonnel cp ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE ft.FunctionTypeID = @FunctionTypeID */
		INNER JOIN ClientPersonnel cp ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE ufc.FunctionTypeID = @FunctionTypeID 
		AND ufc.LeadTypeID = @LeadTypeID 
		AND ufc.HasDescendants = 1 
		AND cp.ClientID = @ClientID
		
	END
	/* Similar for Note Types but they don't have a LeadTypeID */
	IF @FunctionTypeID = 26
	BEGIN

		/* NoteTypes for Groups */
		INSERT INTO GroupRightsDynamic (
			ClientPersonnelAdminGroupID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			gfc.ClientPersonnelAdminGroupID,
			@FunctionTypeID, 
			NULL, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN gfc.RightID ELSE @Level END
		FROM GroupFunctionControl gfc 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		WHERE gfc.FunctionTypeID = @FunctionTypeID 
		AND gfc.HasDescendants = 1 
		AND cpg.ClientID = @ClientID

		/* NoteTypes for Users */
		INSERT INTO UserRightsDynamic (
			ClientPersonnelID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			ufc.ClientPersonnelID,
			@FunctionTypeID, 
			NULL, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN ufc.RightID ELSE @Level END
		FROM UserFunctionControl ufc 
		INNER JOIN ClientPersonnel cp ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE ufc.FunctionTypeID = @FunctionTypeID 
		AND ufc.HasDescendants = 1 
		AND cp.ClientID = @ClientID
		
	END

	/* If a new Lead Type has been created */
	IF @FunctionTypeID = 23
	BEGIN
	
		/*
			First, add this new Lead Type to the main Lead-Types category
			for each Group that has individual Lead-Types listed.
			Groups first
		*/
		INSERT INTO GroupFunctionControl (
			ClientPersonnelAdminGroupID,
			ModuleID,
			FunctionTypeID,
			HasDescendants,
			RightID,
			LeadTypeID)
		SELECT gfc.ClientPersonnelAdminGroupID,
			ModuleID,
			@FunctionTypeID, 
			0,
			CASE @Level WHEN -1 THEN gfc.RightID ELSE @Level END,
			@LeadTypeID 
		FROM GroupFunctionControl gfc 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		WHERE gfc.FunctionTypeID = 7
		AND gfc.HasDescendants = 1 
		AND cpg.ClientID = @ClientID
		
		/*
			Now add this new Lead Type to Lead-Types-By-Office
			for each Office that has any Lead-Type restrictions already
		*/
		INSERT INTO GroupRightsDynamic (
			ClientPersonnelAdminGroupID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			gfc.ClientPersonnelAdminGroupID,
			@FunctionTypeID, 
			@LeadTypeID, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN gfc.RightID ELSE @Level END
		FROM GroupFunctionControl gfc
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		WHERE gfc.FunctionTypeID = 11 
		AND gfc.HasDescendants = 1 
		AND cpg.ClientID = @ClientID

		/* Now do it all again for Users */
		INSERT INTO UserFunctionControl (
			ClientPersonnelID,
			ModuleID,
			FunctionTypeID,
			HasDescendants,
			RightID,
			LeadTypeID)
		SELECT ufc.ClientPersonnelID,
			ModuleID,
			@FunctionTypeID, 
			0,
			CASE @Level WHEN -1 THEN ufc.RightID ELSE @Level END,
			@LeadTypeID 
		FROM UserFunctionControl ufc 
		INNER JOIN ClientPersonnel cp ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE ufc.FunctionTypeID = 7
		AND ufc.HasDescendants = 1 
		AND cp.ClientID = @ClientID

		INSERT INTO UserRightsDynamic (
			ClientPersonnelID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			ufc.ClientPersonnelID,
			@FunctionTypeID, 
			@LeadTypeID, 
			@ObjectID, 
			CASE @Level WHEN -1 THEN ufc.RightID ELSE @Level END
		FROM UserFunctionControl ufc
		INNER JOIN ClientPersonnel cp ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE ufc.FunctionTypeID = 11 
		AND ufc.HasDescendants = 1 
		AND cp.ClientID = @ClientID

	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[SetSecurityForNewObject] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SetSecurityForNewObject] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SetSecurityForNewObject] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[SetSecurityForNewObject] TO [sp_executehelper]
GO
