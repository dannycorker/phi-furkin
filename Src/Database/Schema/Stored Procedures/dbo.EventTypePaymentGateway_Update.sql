SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypePaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_Update]
(

	@EventTypePaymentGatewayID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@ClientPaymentGatewayID int   ,

	@TransactionTypeID int   ,

	@CardMaskingMethodID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypePaymentGateway]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[ClientPaymentGatewayID] = @ClientPaymentGatewayID
					,[TransactionTypeID] = @TransactionTypeID
					,[CardMaskingMethodID] = @CardMaskingMethodID
				WHERE
[EventTypePaymentGatewayID] = @EventTypePaymentGatewayID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Update] TO [sp_executeall]
GO
