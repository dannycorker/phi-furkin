SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-03-09
-- Description:	Create Customer Ledger And Export
-- 2018-03-22 ACE Added support for card transaction
-- 2018-04-09 JEL Added names for XML and @ReceiptOrPAyment logic 
-- 2018-04-10 JEL wrapped event type application in IF so it doesnt error for cheques
-- 2018-04-30 JEL Changed candidate date filter to use Payment Date to be in line with new login (PaymentDate = Send File, ActualCollection = Money Collected(3 working days hence)
-- 2018-08-15 GPR Changes subquery so that we pick up a refund that takes place on a Policy where there has already been a refund(e.g. A Cancellation refund following an MTA refund) C600 Defect #445 / #892
-- 2019-03-04 JEL Added sub query for card refunds to ensure we wonly pick up true card transaction records, not data fixed records 
-- 2019-12-18 CPS for JIRA LPC-252 | Return CardTransactionID as CardTransactionID
-- 2020-01-08 CPS for JIRA LPC-251 | Update subquery conditions on Card Transaction insert otherwise we risk failing to create a card transaction
-- 2020-01-09 CPS for JIRA LPC-307 | Incorporate CardTransactionMap as OrderID so that we don't get conflicts between environments
-- 2020-02-25 GPR for JIRA AAG-201 | Remove AccountMandate check as not applicable for AAG / Australia (EFT)
-- 2020-02-28 GPR for JIRA AAG-201 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- 2020-03-10 NG  for Daily Error resolution - eventtype ID 156751 being added by AEQ to wrong lead type
-- 2020-03-30 GPR for JIRA AAG-571 | Corrected logic in Payment insert select
-- 2020-04-01 GPR for AAG-603 | altered insert for CardTransaction, added a.CardToken as the CustomerRefNum in place of NULL as CustomerRefNum
-- 2020-07-22 GPR added Account.Active check
-- 2021-02-04 CPS for JIRA FURKIN-184 | Bypass CardTransaction requirements for non-card accounts, and include Debug option for testing
-- 2021-03-19 CPS for JIRA FURKIN-129 | Output Institution Number
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreateLedgerAndExportPayments]
	@ClientID INT,
	@AccountTypeID INT,
	@ActualCollectionDate DATETIME = NULL,
	@RunAsUserID INT,
	@ClientAccountID INT,
	@PaymentTypeID INT,
	@ReceiptOrPayment INT, /*1 = Receive 2 = Pay*/
	@ClientPaymentGatewayID INT = NULL,
	@Debug BIT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FnGetDateLocal DATETIME = dbo.fn_GetDate_Local()

	IF ISNULL( @ActualCollectionDate, '') = ''
	BEGIN
	
		SELECT @ActualCollectionDate = @FnGetDateLocal 
	
	END


	/*
		1. Insert Payments from the customer payment schedule. Nothing more complicated than that
		2. Insert Customer Ledger records
		3. Update the customer payment schedule with the new Customer Ledger ID's, set the reconciled date and set the status to 2
		4. Update the purchased product payment schedules with the payment status 2, rec date and customer ledger id
		5. Execute this for every account that needs it... Account__SetDateAndAmountOfNextPayment
		5. Add event to all cases.
	*/
	
	DECLARE  @PaymentRunDate				 DATETIME = @FnGetDateLocal
			,@AccountID						 INT
			,@EventToAdd					 INT
			,@EffectivePaymentDate			 DATE
			,@DDCollectionProcessingInterval INT
			,@AccountUseID					 INT
			,@DatabaseName					 VARCHAR(250) = DB_NAME()
			,@ServerName					 VARCHAR(250) = 'TSTDB1'
			,@CountryID						 INT /*GPR 2020-02-28*/
	
	SELECT @CountryID = CountryID 
	FROM Clients WITH (NOLOCK) 
	WHERE ClientID = @ClientID
			
	SELECT @AccountUseID = c.AccountUseID 
	FROM ClientAccount c WITH (NOLOCK) 
	WHERE c.ClientAccountID = @ClientAccountID 
	
	CREATE TABLE #InsertedPayments (CustomerPaymentScheduleID INT, PaymentID INT)
	CREATE TABLE #InsertedCustomerLedger (CustomerLedgerID INT, PaymentID INT)
	CREATE TABLE #AccountsToSet (AccountID INT)
	CREATE TABLE #InsertedCardTransactionRows (CardTransactionID INT, OriginalCardTransactionID INT, CardTransactionMapID INT)
	CREATE TABLE #InsertedCardTransactionMaps (CardTransactionID INT, CardTransactionMapID INT)

	DECLARE @LogEntry VARCHAR(2000) = ' @ClientID = '				+ ISNULL(CONVERT(VARCHAR,@ClientID),'NULL')
									+ ',@AccountTypeID = '			+ ISNULL(CONVERT(VARCHAR,@AccountTypeID),'NULL')
									+ ',@ActualCollectionDate = '	+ ISNULL(CONVERT(VARCHAR,@ActualCollectionDate,121),'NULL')
									+ ',@RunAsUserID = '			+ ISNULL(CONVERT(VARCHAR,@RunAsUserID),'NULL')
									+ ',@ClientAccountID = '		+ ISNULL(CONVERT(VARCHAR,@ClientAccountID),'NULL')
									+ ',@PaymentTypeID = '			+ ISNULL(CONVERT(VARCHAR,@PaymentTypeID),'NULL')
									+ ',@ReceiptOrPayment = '		+ ISNULL(CONVERT(VARCHAR,@ReceiptOrPayment),'NULL')
									+ ',@ClientPaymentGatewayID = ' + ISNULL(CONVERT(VARCHAR,@ClientPaymentGatewayID),'NULL')

	EXEC _C00_LogIt  'Info','Billing__CreateLedgerAndExportPayments','Start of Procedure',@LogEntry,@RunAsUserID

	BEGIN TRY
		BEGIN TRAN

		/*Insert payment records*/
		INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, CustomerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		OUTPUT inserted.CustomerPaymentScheduleID, inserted.PaymentID INTO #InsertedPayments (CustomerPaymentScheduleID, PaymentID)
		SELECT cps.ClientID, cps.CustomerID, 
				cps.PaymentDate,
				@PaymentTypeID AS [PaymentTypeID], 
				NULL AS [DateReceived], 
				'' AS [PaymentDescription], 
				ISNULL(a.Reference,''), 
				cps.PaymentNet, 
				cps.PaymentVAT, 
				cps.PaymentGross, 
				NULL AS [PaymentCurrency], 
				NULL AS [RelatedOrderRef], 
				NULL AS [RelatedRequestDescription], 
				NULL AS [PayeeFullName], 
				NULL AS [MaskedAccountNumber], 
				NULL AS [AccountTypeDescription], 
				2 AS [PaymentStatusID], 
				NULL AS [DateReconciled], 
				NULL AS [ReconciliationOutcome], 
				cps.RelatedObjectID, 
				cps.RelatedObjectTypeID, 
				'' AS [Comments], 
				NULL AS [PaymentFileName], 
				NULL AS [FailureCode], 
				NULL AS [FailureReason], 
				cps.CustomerPaymentScheduleID, 
				@RunAsUserID AS [WhoCreated], 
				@PaymentRunDate AS [WhenCreated], 
				@RunAsUserID AS [WhoModified], 
				@PaymentRunDate AS [WhenModified]
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID = a.AccountID
		INNER JOIN Customers c with (NOLOCK) on c.CustomerID = a.CustomerID 
		WHERE cps.ClientID = @ClientID
		AND cps.PaymentDate <= @ActualCollectionDate
		AND a.AccountTypeID = @AccountTypeID
		AND cps.ClientAccountID = @ClientAccountID
		AND c.Test = 0 
		AND a.Active = 1
		/*JEL Add status filter*/ AND cps.PaymentStatusID IN (5,1) 		
		AND (
			(@ReceiptOrPayment = 1 AND cps.PaymentGross > 0.00) /*Make sure its an incoming payment request!*/
			OR
			(@ReceiptOrPayment = 2 AND cps.PaymentGross < 0.00) /*Make sure its an outgoing payment request!*/
			)
		/*GPR 2020-05-26, removed during demo - caused task to fail*/
		AND (/*GPR 2020-02-28 Added Country Check*/
			(/*@CountryID <> 14 /*Australia*/ AND*/ @ReceiptOrPayment = 1 AND @AccountTypeID = 1 /*Only need a mandate for Bank...*/		
			/*AND EXISTS (SELECT * FROM AccountMandate am WITH (NOLOCK)
						WHERE am.AccountID = a.AccountID
						AND am.MandateStatusID = 3)*/
			)
			OR (@ReceiptOrPayment = 2 AND @AccountTypeID IN (1,3,2))
			OR (@ReceiptOrPayment = 1 AND @AccountTypeID IN (2) 
				AND EXISTS (
								SELECT * 
								FROM Account aa WITH (NOLOCK) 
								WHERE a.AccountID = aa.AccountID 
								/*AND ISNULL(aa.CardToken,'') <> '' */
								AND aa.CardToken IS NOT NULL 
								AND aa.CardToken <> ''
								AND (aa.ExpiryDate > @FnGetDateLocal OR @ClientID IN (605, 607))
							)	
								
			)
		)
				
		--SELECT @@RowCount AS [Payment]

		/*3018-03-27 ACE Updated to use working days for the effective payment date*/
		SELECT @DDCollectionProcessingInterval = bc.DDCollectionProcessingInterval
		FROM BillingConfiguration bc WITH (NOLOCK)
		WHERE bc.ClientID = @ClientID

		SELECT @EffectivePaymentDate = dbo.fnAddWorkingDays(@PaymentRunDate, @DDCollectionProcessingInterval)

		/* Insert CustomerLedger Entries */
		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		OUTPUT inserted.CustomerLedgerID, inserted.PaymentID INTO #InsertedCustomerLedger (CustomerLedgerID, PaymentID)
		SELECT p.ClientID, p.CustomerID, @PaymentRunDate, 
			NULL AS [FailureCode], 
			NULL AS [FailureReason], 
			@FnGetDateLocal AS [TransactionDate], 
			p.PaymentReference, 
			p.PaymentDescription, 
			p.PaymentNet,
			p.PaymentVAT, 
			p.PaymentGross, 
			NULL AS [LeadEventID], 
			p.ObjectID AS [ObjectID], 
			p.ObjectTypeID, 
			p.PaymentID, 
			NULL AS [OutgoingPaymentID], 
			@RunAsUserID, 
			@PaymentRunDate AS [WhenCreated]
		FROM #InsertedPayments ip 
		INNER JOIN Payment p WITH (NOLOCK) ON p.PaymentID = ip.PaymentID
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = p.CustomerID /*GPR 2020-03-26*/
		WHERE c.Test = 0 /*GPR 2020-03-26*/
		AND c.ClientID = @ClientID

		-- 2021-02-04 CPS for JIRA Furkin-184 | Changed from AccountTypeID IN (1,2) as there's no need to insert CardTransaction records unless we're a Card account
		IF @AccountTypeID = 2 /*Card*/
		BEGIN
			
			IF @ReceiptOrPayment = 1 
			BEGIN 
			
				/*Ok, we have a card! Fill in the CT Table, for a collection, track it's own CTID as the origional CTID/sourceID */
				INSERT INTO CardTransaction (ClientID, CustomerID, ObjectID, ObjectTypeID, PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID, ClientPaymentGatewayID, PaymentTypeID, PreValidate, OrderID, ReferenceNumber, Description, Amount, CurrencyID, FirstName, LastName, CardName, Number, ExpiryMonth, ExpiryYear, Code, AuthCode, AvsCode, AvsIndicator, Address, Address2, City, StateProvince, Country, ZipPostal, BankAccountType, BankCode, BankName, Company, GatewayCustomerID, TransactionID, TransactionTypeID, Email, Fax, Phone, Partner, Certificate, ClientIP, Referrer, ErrorCode, ErrorMessage, WhoCreated, WhenCreated, CardTypeID, StatusMsg, CustomerRefNum, ParentID, Filename, AccountID, MerchantCode)
				OUTPUT inserted.CardTransactionID,inserted.CardTransactionID  INTO #InsertedCardTransactionRows ( CardTransactionID, OriginalCardTransactionID )
				SELECT cps.ClientID, cps.CustomerID, cps.RelatedObjectID, cps.RelatedObjectTypeID, NULL, 
						cps.CustomerPaymentScheduleID, 
						@ClientPaymentGatewayID, 
						@PaymentTypeID, 
						NULL AS [PreValidate], 
						NULL AS OrderID, 
						NULL AS ReferenceNumber, 
						'Payment request for Policy ' + ISNULL(CONVERT(VARCHAR,cps.RelatedObjectID), '') AS Description, 
						cps.PaymentGross, 
						NULL AS [Currency], 
						a.FirstName, 
						a.LastName, 
						NULL AS CardName, 
						NULL AS Number, 
						a.ExpiryMonth, 
						a.ExpiryYear, 
						NULL AS Code, 
						NULL AS AuthCode, 
						NULL AS AvsCode, 
						NULL AS AvsIndicator, 
						NULL AS Address, 
						NULL AS Address2, 
						NULL AS City, 
						NULL AS StateProvince, 
						NULL AS Country, 
						NULL AS ZipPostal, 
						NULL AS BankAccountType, 
						NULL AS BankCode, 
						a.Bankname, 
						NULL AS Company, 
						NULL AS GatewayCustomerID, 
						NULL AS TransactionID, 
						NULL AS TransactionTypeID, 
						NULL AS Email, 
						NULL AS Fax, 
						NULL AS Phone, 
						NULL AS Partner, 
						NULL AS Certificate, 
						NULL AS ClientIP,
						NULL AS Referrer, 
						NULL AS ErrorCode, 
						NULL AS ErrorMessage, 
						@RunAsUserID, 
						@FnGetDateLocal, 
						NULL AS [CardTypeID], 
						NULL AS [StatusMsg], 
						a.CardToken AS [CustomerRefNum], /*GPR 2020-04-01 for AAG-603 | The CardToken will be stored in the CustomerRefNum on the CT*/ 
						NULL AS [ParentID], 
						NULL AS [Filename], 
						a.AccountID,
						cpg.MerchantName
				FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
				INNER JOIN #InsertedPayments i ON i.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID 
				INNER JOIN ClientPaymentGateway cpg WITH (NOLOCK) on cpg.PaymentGatewayID = @ClientPaymentGatewayID
				INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = a.CustomerID /*GPR 2020-03-26*/
				WHERE c.Test = 0 /*GPR 2020-03-26*/
				AND c.ClientID = @ClientID 
				AND a.Active = 1

				IF @@ROWCOUNT = 0 AND EXISTS ( 
					SELECT * 
					FROM #InsertedPayments WITH (NOLOCK) 
				)
				BEGIN
					RAISERROR( 'Error:  Payment record(s) Inserted for Collection(s) but no valid Card Transaction Identified.', 16, 1 )
				END		
			
				---- 2020-01-09 CPS for JIRA LPC-307	| Incorporate CardTransactionMap as OrderID so that we don't get conflicts between environments
				--INSERT AquariusMaster.dbo.CardTransactionMap ( CardTransactionID, ClientID, DatabaseName, ServerName )
				--	OUTPUT inserted.CardTransactionID, inserted.CardTransactionMapID
				--	INTO #InsertedCardTransactionMaps ( CardTransactionID, CardTransactionMapID )
				--SELECT icr.CardTransactionID, @ClientID, @DatabaseName, @ServerName
				--FROM #InsertedCardTransactionRows icr

				UPDATE ct
				SET OrderID = map.CardTransactionID
				FROM #InsertedCardTransactionRows map WITH (NOLOCK) 
				INNER JOIN dbo.CardTransaction ct WITH (NOLOCK) ON ct.CardTransactionID = map.CardTransactionID
					
			END 
			ELSE 
			BEGIN 
				
				/*For a Worldpay refund however, the order ID (card transactionID usually) needs to be the card transactionID of the initial payment,
				We don't have a method to track this with its own data point at the moment, the rules are as follows
				Use an cardtransactionID as the orderID  for a refund IF 
				- The Account is the same
				- The amount is positive and greater than the refund amount
				- the transaction was a success
				- there is no later CT that meets these conditions */
			
				INSERT INTO CardTransaction (ClientID, CustomerID, ObjectID, ObjectTypeID, PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID, ClientPaymentGatewayID, PaymentTypeID, PreValidate, OrderID, ReferenceNumber, Description, Amount, CurrencyID, FirstName, LastName, CardName, Number, ExpiryMonth, ExpiryYear, Code, AuthCode, AvsCode, AvsIndicator, Address, Address2, City, StateProvince, Country, ZipPostal, BankAccountType, BankCode, BankName, Company, GatewayCustomerID, TransactionID, TransactionTypeID, Email, Fax, Phone, Partner, Certificate, ClientIP, Referrer, ErrorCode, ErrorMessage, WhoCreated, WhenCreated, CardTypeID, StatusMsg, CustomerRefNum, ParentID, MerchantCode, Filename, AccountID)
				OUTPUT inserted.CardTransactionID, inserted.OrderID  INTO #InsertedCardTransactionRows ( CardTransactionID, OriginalCardTransactionID )
				SELECT cps.ClientID, cps.CustomerID, cps.RelatedObjectID, cps.RelatedObjectTypeID, NULL, 
						cps.CustomerPaymentScheduleID, 
						@ClientPaymentGatewayID, 
						@PaymentTypeID, 
						NULL AS [PreValidate], 
						--oct.CardTransactionID AS OrderID, 
						'' AS OrderID, -- 2020-01-08 CPS for JIRA LAGCLAIM-310 | PolicyHistory Row
						NULL AS ReferenceNumber, 
						'Payment request for Policy ' + ISNULL(CONVERT(VARCHAR,cps.RelatedObjectID), '') AS Description, 
						/*2020-02-12 - ACE - Keep Amount -ve*/
						cps.PaymentGross, 
						NULL AS [Currency], 
						a.FirstName, 
						a.LastName, 
						NULL AS CardName, 
						NULL AS Number, 
						a.ExpiryMonth, 
						a.ExpiryYear, 
						NULL AS Code, 
						NULL AS AuthCode, 
						NULL AS AvsCode, 
						NULL AS AvsIndicator, 
						NULL AS Address, 
						NULL AS Address2, 
						NULL AS City, 
						NULL AS StateProvince, 
						NULL AS Country, 
						NULL AS ZipPostal, 
						NULL AS BankAccountType, 
						NULL AS BankCode, 
						a.Bankname, 
						NULL AS Company, 
						NULL AS GatewayCustomerID, 
						a.CardToken AS TransactionID, 
						NULL AS TransactionTypeID, 
						NULL AS Email, 
						NULL AS Fax, 
						NULL AS Phone, 
						NULL AS Partner, 
						NULL AS Certificate, 
						NULL AS ClientIP,
						NULL AS Referrer, 
						NULL AS ErrorCode, 
						NULL AS ErrorMessage, 
						@RunAsUserID, 
						@FnGetDateLocal, 
						NULL AS [CardTypeID], 
						NULL AS [StatusMsg], 
						a.CardToken AS [CustomerRefNum], /*GPR 2020-04-01 for AAG-603 | The CardToken will be stored in the CustomerRefNum on the CT*/ 
						'' AS [ParentID], 
						cpg.MerchantName, 
						NULL AS [Filename], 
						a.AccountID
				FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
				INNER JOIN #InsertedPayments i ON i.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID
				INNER JOIN ClientPaymentGateway cpg WITH (NOLOCK) on cpg.PaymentGatewayID = @ClientPaymentGatewayID
				--INNER JOIN AquariusMaster.dbo.CardTransactionMap ctm WITH (NOLOCK) on ctm.CardTransactionID = oct.CardTransactionID
				INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cps.CustomerID /*GPR 2020-03-26*/
				WHERE c.Test = 0 /*GPR 2020-03-26*/
				AND c.ClientID = @ClientID
				AND a.Active = 1
				--AND cps.PaymentStatusID <> 4 
				/*
				AND NOT EXISTS (SELECT * FROM CardTransaction sub_oct WITH (NOLOCK)
								INNER JOIN CustomerPaymentSchedule sub_cps WITH (NOLOCK) on sub_cps.CustomerPaymentScheduleID = sub_oct.CustomerPaymentScheduleID 
								WHERE sub_oct.AccountID = oct.AccountID 
								AND sub_oct.Amount > 0 
								AND sub_cps.PaymentGross > 0 /*GPR 2018-08-15 Defect 892 / 445*/
								AND sub_oct.Amount >= ABS(sub_cps.PaymentGross) -- 2019-12-18 CPS for JIRA LPC-252 | Make sure the logic here matches the details above otherwise we risk getting no results
								AND sub_oct.CardTransactionID > oct.CardTransactionID
								AND sub_cps.PaymentStatusID <> 4)
								*/
								-- 2020-01-08 CPS for JIRA LPC-251 | The conditions for sub_oct MUST match those for oct otherwise we risk failing to create a card transaction
								--AND EXISTS (SELECT top 1 w.CardTransactionID FROM WorldPayNotification w with (NOLOCK) 
								--WHERE w.CardTransactionID = sub_oct.CardTransactionID)
								
				--				)
				--AND EXISTS (SELECT top 1 w.CardTransactionID FROM WorldPayNotification w with (NOLOCK) 
				--			WHERE w.CardTransactionID = oct.CardTransactionID)
			
				IF @@ROWCOUNT = 0 AND EXISTS ( 
					SELECT * 
					FROM #InsertedPayments WITH (NOLOCK) 
				)
				BEGIN
					RAISERROR( 'Error:  Payment record(s) Inserted for refund(s) but no valid Card Transaction Identified.', 16, 1 )
				END

			END
		
			/*Make sure the ParentID is correct. 
			 For a collection that is it's own card transactionID 
			 For a refund, it has to be the CardTransactionID of a previous collection for this account of a greater amount*/ 
			UPDATE ct
			SET OrderID = i.CardTransactionID
			FROM CardTransaction ct 
			INNER JOIN #InsertedCardTransactionRows i on i.CardTransactionID = ct.CardTransactionID
			WHERE ct.OrderID = '' -- 2020-01-09 CPS for JIRA LPC-307 | Incorporate CardTransactionMap as OrderID so that we don't get conflicts between environments

		END
		
		--SELECT @@RowCount AS [Cl]

		/*Set the Customer Ledger ID Back onto the Customer Payment Schedule*/
		UPDATE cps
		SET CustomerLedgerID = icl.CustomerLedgerID, 
			ReconciledDate = @EffectivePaymentDate, 
			PaymentStatusID=2
		FROM CustomerPaymentSchedule cps 
		INNER JOIN #InsertedPayments ipay ON ipay.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID 
		INNER JOIN #InsertedCustomerLedger icl ON icl.PaymentID = ipay.PaymentID
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cps.CustomerID  /*GPR 2020-03-26*/
		WHERE c.Test = 0 /*GPR 2020-03-26*/
		AND c.ClientID = @ClientID

		/*Set the Purchased Product Payment Schedule, set the customer ledger id and rec dates*/
		UPDATE pps
		SET PaymentStatusID = 2, 
			ReconciledDate = cps.ReconciledDate, 
			CustomerLedgerID = cps.CustomerLedgerID
		FROM CustomerPaymentSchedule cps
		INNER JOIN #InsertedPayments ipay ON ipay.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID 
		INNER JOIN dbo.PurchasedProductPaymentSchedule pps on cps.CustomerPaymentScheduleID = pps.CustomerPaymentScheduleID
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cps.CustomerID  /*GPR 2020-03-26*/
		WHERE c.Test = 0 /*GPR 2020-03-26*/
		AND c.ClientID = @ClientID

		/*GPR 2020-04-21 copied over from C603*/
		/*JEL/GPR 2020-09-08 Updated*/
		IF @AccountTypeID = 2 
		BEGIN 
			/*2020-02-28 JEL For Card Payers, we need to write the CTID back to the payments table*/ 
			Update pp 
			SET CardTransactionID = ct.CardTransactionID 
			FROM #InsertedPayments p 
			INNER JOIN dbo.Payment pp WITH (NOLOCK) ON pp.PaymentID = p.PaymentID
			INNER JOIN CardTransaction ct WITH (NOLOCK) ON ct.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID

		END 


		INSERT INTO #AccountsToSet (AccountID)
		SELECT DISTINCT cps.AccountID
		FROM CustomerPaymentSchedule cps
		INNER JOIN #InsertedPayments ipay ON ipay.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cps.CustomerID  /*GPR 2020-03-30*/
		WHERE c.Test = 0 /*GPR 2020-03-30*/
		AND c.ClientID = @ClientID

		/*Set the next payment date and amounts of each of the accounts*/
		WHILE EXISTS (
			SELECT *
			FROM #AccountsToSet
		)
		BEGIN

			SELECT TOP 1 @AccountID = a.AccountID
			FROM #AccountsToSet a

			EXEC Account__SetDateAndAmountOfNextPayment @AccountID

			DELETE a 
			FROM #AccountsToSet a
			WHERE a.AccountID = @AccountID

		END
		
		IF @ReceiptOrPayment = 1 /*Premium Collection*/ 
		BEGIN 
			
			SELECT @EventToAdd = bec.EventToApply
			FROM BillingEventConfig bec WITH (NOLOCK)
			WHERE bec.ClientAccountID = @ClientAccountID
			AND bec.ClientID = @ClientID
			/*JEL changed from 1 to 2*/	AND bec.FileGenerationType = 1
		END 
		
		IF @ReceiptOrPayment = 2 and @AccountUseID = 1 and @AccountTypeID = 1/*Premium Refund*/ 
		BEGIN 
		
			SELECT @EventToAdd = bec.EventToApply
			FROM BillingEventConfig bec WITH (NOLOCK)
			WHERE bec.ClientAccountID = @ClientAccountID
			AND bec.ClientID = @ClientID
			/*JEL changed from 1 to 2*/	AND bec.FileGenerationType = 3
			
		END
		
		IF @ReceiptOrPayment = 2 and @AccountUseID = 2 and @AccountTypeID = 1/*BACs Claims Payment*/ 
		BEGIN 
		
			SELECT @EventToAdd = bec.EventToApply
			FROM BillingEventConfig bec WITH (NOLOCK)
			WHERE bec.ClientAccountID = @ClientAccountID
			AND bec.ClientID = @ClientID
			/*JEL changed from 1 to 2*/	AND bec.FileGenerationType = 5
			
		END
		
		IF ISNULL(@EventToAdd,0) <> 0 
		BEGIN 
		
			/*Finally add events to the event history*/
			INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount,EarliestRunDateTime, CommentsToApply)
			SELECT cps.ClientID, cps.CustomerID, m.LeadID, m.CaseID, ca.LatestNonNoteLeadEventID, le.EventTypeID, @FnGetDateLocal, @RunAsUserID, @EventToAdd, @RunAsUserID, -1, 5, 0, 5, DATEADD(SECOND,15,@FnGetDateLocal), 'Added by Billing__CreateLedgerAndExportPayments'
			FROM CustomerPaymentSchedule cps WITH (NOLOCK)
			INNER JOIN #InsertedPayments ipay ON ipay.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID 
			INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = cps.RelatedObjectID /*JEL join on the matter*/ 
			INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = m.CaseID
			INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = ca.LatestNonNoteLeadEventID
			INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cps.CustomerID  /*GPR 2020-03-26*/
			WHERE c.Test = 0 /*GPR 2020-03-26*/
			AND c.ClientID = @ClientID
			AND a.Active = 1

			PRINT @@Rowcount
		
		END
		
		IF @AccountTypeID = 1
		BEGIN
		
			EXEC dbo._C00_LogIt 'Info', 'Billing__CreateLedgerAndExportPayments', 'ClientAccountID 1', @ClientAccountID, @RunAsUserID /*GPR 2020-03-27*/

			/*Select the output out for the scheduler to pass onto the next stage*/
			SELECT i.CustomerPaymentScheduleID, 
				p.CustomerID, 
				NULL AS [LeadEventID], 
				a.ObjectID AS [ColMatterID], 
				cps.AccountID, 
				p.PaymentReference as [Reference], 
				CASE WHEN @ReceiptOrPayment = 1 THEN p.PaymentGross ELSE -p.PaymentGross END as [Amount],/*If refund we don't want a -*/ 
				p.PaymentID,
				ROW_NUMBER() OVER(PARTITION BY cps.AccountID ORDER BY p.PaymentDateTime ASC) as [RNO],
				a.AccountHolderName,
				a.AccountNumber,
				a.Sortcode as [SortCode],
				CASE WHEN @ReceiptOrPayment = 1 THEN '50' ELSE '13' END AS [TransactionCode], --'17' ELSE '99' END AS [TransactionCode], /*If this is a refund make sure wqe sent this transaction code*/ 
				ca.SUN, 
				ca.AccountNumber AS [ClientAccountNo], 
				ca.SortCode AS [ClientSortCode],
				cps.ClientAccountID,
				ca.UsersName,
				@ReceiptOrPayment as [ReceiptOrPayment], /*JEL Added Receipt or Payment Back in*/ 
				a.CardToken AS [CustomerRefNum], /*2020-09-01 ACE Smuggle the token as the CustRefNum for One Inc EFT*/
				CASE WHEN @ReceiptOrPayment = 1 THEN '' ELSE a.CardToken END AS [TransactionId], /*Pass the Card token (transaction ID on a 1 to 1 basis for refunds) as TransactionId*/
				a.Reference as [AccountRef],
				ct.CardTransactionID,
				a.InstitutionNumber -- 2021-03-19 CPS for JIRA FURKIN-129 | Output Institution Number
			INTO #BACSOutput
			FROM #InsertedPayments i
			INNER JOIN Payment p WITH (NOLOCK) ON p.PaymentID = i.PaymentID
			INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = i.CustomerPaymentScheduleID
			INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID
			INNER JOIN ClientAccount ca WITH (NOLOCK) ON ca.ClientAccountID = cps.ClientAccountID
			INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cps.CustomerID  /*GPR 2020-03-26*/
			 LEFT JOIN CardTransaction ct WITH (NOLOCK) ON ct.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID -- 2021-02-04 CPS for JIRA FURKIN-184 | Left Joined as we won't necessarily have CardTransaction records
			WHERE c.Test = 0 /*GPR 2020-03-26*/
			AND c.ClientID = @ClientID
			AND a.Active = 1
			
			--SELECT @@RowCount AS [Output]

			IF @ReceiptOrPayment = 1 /* JEL Only do this for collections*/ 
			AND @AccountTypeID = 1 /*ACE Only do this for Bank account on collections*/
			BEGIN

				/*Set the transaction code to 01 for new payments*/
				UPDATE b
				SET TransactionCode = '50'--'01'
				FROM #BACSOutput b
				WHERE NOT EXISTS (
					SELECT *
					FROM CustomerPaymentSchedule cps WITH (NOLOCK)
					WHERE cps.AccountID = b.AccountID
					AND cps.CustomerPaymentScheduleID < b.CustomerPaymentScheduleID
				)

			END


			SELECT *
			FROM #BACSOutput b
			
			IF @Debug = 1
			BEGIN
				SELECT '#InsertedPayments'				[#InsertedPayments]				, * FROM #InsertedPayments			  WITH (NOLOCK)
				SELECT '#InsertedCustomerLedger'		[#InsertedCustomerLedger]		, * FROM #InsertedCustomerLedger	  WITH (NOLOCK)
				SELECT '#AccountsToSet'					[#AccountsToSet]				, * FROM #AccountsToSet				  WITH (NOLOCK)
				SELECT '#InsertedCardTransactionRows'	[#InsertedCardTransactionRows]	, * FROM #InsertedCardTransactionRows WITH (NOLOCK)
				SELECT '#InsertedCardTransactionMaps'	[#InsertedCardTransactionMaps]	, * FROM #InsertedCardTransactionMaps WITH (NOLOCK)
			END

			DROP TABLE #AccountsToSet
			DROP TABLE #BACSOutput
			DROP TABLE #InsertedCustomerLedger
			DROP TABLE #InsertedPayments

		END
		ELSE
		BEGIN
			
			/*As we have a card we need to return the transactions to the script runner so it can itterate over each and send the requests.*/ 
				

			--SELECT ct.OrderID as [CardTransactionID], -- 2019-12-18 CPS for JIRA LPC-252  | WHY WHY WHY would you do this and not put something to explain why?
			--																				| Returning OrderID as CardTransactionID made refunds fail to reconcile with WorldPay :(
			--SELECT ct.CardTransactionID, 
			/*2020-02-12 - ACE - Make Amount +ve*/
			SELECT ct.OrderID [CardTransactionID],  -- 2020-01-09 CPS for JIRA LPC-307 | Testing the previous insanity
				   ct.ClientID, 
				   ct.CustomerID, 
				   ct.ObjectID, 
				   ct.ObjectTypeID, 
				   ct.PurchasedProductPaymentScheduleID, 
				   ct.CustomerPaymentScheduleID,
				   ct.ClientPaymentGatewayID,
				   ct.PaymentTypeID,
				   ct.PreValidate,
				   ct.OrderID,
				   ct.ReferenceNumber,
				   ct.Description,
				   ABS(ct.Amount) AS [Amount],
				   ct.CurrencyID,
				   ct.FirstName,
				   ct.LastName,
				   ct.CardName,
				   ct.Number,
				   ct.ExpiryMonth,
				   ct.ExpiryYear,
				   ct.Code,
				   ct.AuthCode,
				   ct.AvsCode,
				   ct.AvsIndicator,
				   ct.Address,
				   ct.Address2,
				   ct.City,
				   ct.StateProvince,
				   ct.country,
				   ct.ZipPostal,
				   ct.BankAccountType, 
				   ct.BankCode, 
				   ct.BankName, 
				   ct.Company,
				   ct.GatewayCustomerID,
				   ct.TransactionID,--CASE WHEN @ReceiptOrPayment = 1 THEN ct.TransactionID ELSE ct.CustomerRefNum END AS [TransactionID],
				   ct.TransactionTypeID, 
				   ct.Email,
				   ct.Fax,
				   ct.Phone,
				   ct.Partner, 
				   ct.Certificate,
				   ct.ClientIP, 
				   ct.Referrer, 
				   ct.ErrorCode,
				   ct.ErrorMessage,
				   ct.WhoCreated,
				   ct.WhenCreated,
				   ct.CardTypeID,
				   ct.StatusMsg,
				   ct.CustomerRefNum,
				   ct.ParentID,
				   ct.AccountID,
				   ct.OriginalAmount,
				   ct.MerchantCode,
				   ct.FileName,
				   @ReceiptOrPayment AS [ReceiptOrPayment],
				   a.Reference as [AccountRef]
			FROM #InsertedCardTransactionRows i
			INNER JOIN CardTransaction ct WITH (NOLOCK) ON ct.CardTransactionID = i.CardTransactionID 
			INNER JOIN Account a with (NOLOCK) ON a.AccountID = ct.AccountID 
			
			DROP TABLE #InsertedCardTransactionRows
		
		END

		IF @Debug = 1
		BEGIN
			ROLLBACK
		END
		ELSE
		BEGIN
			COMMIT
		END

	END TRY
	BEGIN CATCH
		
		ROLLBACK

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		
	END CATCH
	
	IF @Debug = 1
	BEGIN
		SELECT TOP 10 *
		FROM Logs l WITH (NOLOCK) 
		WHERE l.ClassName = 'Billing__CreateLedgerAndExportPayments'
		ORDER BY l.LogID DESC
	END

	EXEC dbo._C00_LogIt 'Info', 'Billing__CreateLedgerAndExportPayments', 'Proc Complete - ClientAccountID', @ClientAccountID, @RunAsUserID /*GPR 2020-03-27*/

END


GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreateLedgerAndExportPayments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreateLedgerAndExportPayments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreateLedgerAndExportPayments] TO [sp_executeall]
GO
