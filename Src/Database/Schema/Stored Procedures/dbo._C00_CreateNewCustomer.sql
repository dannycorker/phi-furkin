SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-28
-- Description:	Create a new customer and all the records below it
-- Modified by PR 30-01-2014 added the parameters @CreateLeadRegardless int = 0,
--												  @ReturnNewMatterID int = 0,
--												  @AddProcessStart int = 0
--												  @MobileTelephone VARCHAR(200) = NULL
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateNewCustomer]
(
	@ClientID INT,
	@LeadTypeID INT,
	@TitleID INT = 0,
	@FirstName VARCHAR(100) = '',
	@LastName VARCHAR(100) = '',
	@EmailAddress VARCHAR(255) = NULL,
	@IsBusiness BIT = 0,
	@BusinessName VARCHAR(200) = NULL, 
	@Address1 VARCHAR(200) = NULL, 
	@Address2 VARCHAR(200) = NULL, 
	@Town VARCHAR(200) = NULL, 
	@County VARCHAR(200) = NULL, 
	@PostCode VARCHAR(200) = NULL, 
	@SitePhone  VARCHAR(200) = NULL,
	@WhoCreated INT = NULL,
	@Website VARCHAR(200) = NULL, 
	@CountryID INT = 232,
	@CreateLeadRegardless INT = 0,
	@ReturnNewMatterID INT = 0,
	@AddProcessStart INT = 0,
	@MobileTelephone VARCHAR(200) = NULL
)

AS
BEGIN

	SET NOCOUNT ON;

	/* Instant fail if these are not present */
	IF @ClientID IS NULL OR @LeadTypeID IS NULL 
	BEGIN
		RETURN -1
	END

	DECLARE @NewCustomerID INT, @NewLeadID INT
	
	/* Create a skeleton new customer record */
	INSERT INTO dbo.Customers(
		ClientID,
		TitleID,
		FirstName,
		LastName,
		EmailAddress,
		IsBusiness,
		AquariumStatusID,
		CompanyName,
		Address1,
		Address2,
		Town,
		County,
		PostCode,
		CompanyTelephone, 
		Website, 
		CountryID,
		MobileTelephone
	)
	VALUES(
		@ClientID, 
		@TitleID,
		@FirstName,
		@LastName,
		@EmailAddress,
		@IsBusiness,
		2, 
		@BusinessName, 
		@Address1, 
		@Address2, 
		@Town, 
		@County, 
		@PostCode, 
		@SitePhone, 
		@Website, 
		@CountryID,
		@MobileTelephone
	)

	SELECT @NewCustomerID = SCOPE_IDENTITY()
	
	EXEC @NewLeadID = dbo._C00_CreateNewLeadForCust @NewCustomerID, @LeadTypeID, @ClientID, @WhoCreated, @CreateLeadRegardless, @ReturnNewMatterID, @AddProcessStart
	
	/* Check that all the sub-records worked, else fail the whole job. */
	IF @NewLeadID > 0
	BEGIN
		RETURN @NewCustomerID
	END
	ELSE
	BEGIN
		RETURN -1
	END


END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateNewCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewCustomer] TO [sp_executeall]
GO
