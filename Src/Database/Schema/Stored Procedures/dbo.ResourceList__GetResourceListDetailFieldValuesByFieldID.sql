SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldID]
	-- Add the parameters for the stored procedure here
	@ClientID INT,
	@FieldID INT
	
AS
BEGIN
	
	SELECT r.ResourceListID, r.DetailFieldID AS ResourceListDetailFieldID, r.ResourceListDetailValueID, 
			r.DetailValue AS ResourceListDetailValue, COALESCE(f.Encrypt, 0) AS ResourceListEncrypt, r.EncryptedValue AS ResourceListEncryptedValue,
			li.ItemValue AS ResourceListLookupListValue
	FROM 
		dbo.Detailfields p WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON p.ResourceListDetailFieldPageID = f.DetailFieldPageID
		INNER JOIN dbo.ResourceListDetailValues r WITH (NOLOCK) ON f.DetailFieldID = r.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND r.ValueInt = li.LookupListItemID
	WHERE p.DetailFieldID = @FieldID
	AND p.ClientID = @ClientID
	AND f.Enabled = 1
	ORDER BY r.ResourceListID, f.FieldOrder

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldID] TO [sp_executeall]
GO
