SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Panels table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_Find]
(

	@SearchUsingOR bit   = null ,

	@PanelID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelID int   = null ,

	@PanelName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PanelID]
	, [ClientID]
	, [ClientPersonnelID]
	, [PanelName]
    FROM
	[dbo].[Panels] WITH (NOLOCK) 
    WHERE 
	 ([PanelID] = @PanelID OR @PanelID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([PanelName] = @PanelName OR @PanelName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PanelID]
	, [ClientID]
	, [ClientPersonnelID]
	, [PanelName]
    FROM
	[dbo].[Panels] WITH (NOLOCK) 
    WHERE 
	 ([PanelID] = @PanelID AND @PanelID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([PanelName] = @PanelName AND @PanelName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Find] TO [sp_executeall]
GO
