SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the NotificationGroupMember table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroupMember_Find]
(

	@SearchUsingOR bit   = null ,

	@NotificationGroupMemberID int   = null ,

	@NotificationGroupID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [NotificationGroupMemberID]
	, [NotificationGroupID]
	, [ClientID]
	, [ClientPersonnelID]
    FROM
	[dbo].[NotificationGroupMember] WITH (NOLOCK) 
    WHERE 
	 ([NotificationGroupMemberID] = @NotificationGroupMemberID OR @NotificationGroupMemberID IS NULL)
	AND ([NotificationGroupID] = @NotificationGroupID OR @NotificationGroupID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [NotificationGroupMemberID]
	, [NotificationGroupID]
	, [ClientID]
	, [ClientPersonnelID]
    FROM
	[dbo].[NotificationGroupMember] WITH (NOLOCK) 
    WHERE 
	 ([NotificationGroupMemberID] = @NotificationGroupMemberID AND @NotificationGroupMemberID is not null)
	OR ([NotificationGroupID] = @NotificationGroupID AND @NotificationGroupID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupMember_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Find] TO [sp_executeall]
GO
