SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Department table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department_Get_List]

AS


				
				SELECT
					[DepartmentID],
					[ClientID],
					[CustomerID],
					[DepartmentName],
					[DepartmentDescription]
				FROM
					[dbo].[Department] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Get_List] TO [sp_executeall]
GO
