SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-03
-- Description:	Check email/mobile is valid & synchronise p/holder comms preferences
--
-- 2018-03-12 Mark Beaumont		Handle marketing preferences as well
-- 2019-10-14 CPS for Platform Startup | Don't do this.  Let's handle it with a heads-up display instead.  That task added to existing Jira item LPC-74
-- 2019-11-01 GPR replaced by HUD config
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CheckCommunicationPreferences] 
(
	@CustomerID INT
)

AS
BEGIN

	RETURN

	SET NOCOUNT ON;

	DECLARE @CaseID INT, 
			@CCEmail INT, 
			@CCSMS INT,
			@DocsBy INT,
			@Email VARCHAR(500),
			@EmailValid INT = 1,
			@Mobile VARCHAR(30), 
			@MobileValid INT = 1,
			@NoteMessage VARCHAR(2000) = '',
			/* 2018-03-12 Mark Beaumont - marketing preferences */
			@IsSMSMarketingTarget BIT,
			@IsEmailMarketingTarget BIT
			
	SELECT TOP 1 @CaseID=ca.CaseID FROM Lead l WITH (NOLOCK) 
	INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
	WHERE l.CustomerID=@CustomerID
	
	-- delete any existing warning messages
	EXEC _C600_DeleteANote @CaseID, 904
			
	SELECT @Email=ISNULL(EmailAddress,''),@Mobile=ISNULL(MobileTelephone,'') FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
	
	IF @Email <> '' AND @Email NOT LIKE '%@%'
	BEGIN
	
		SELECT @NoteMessage='P/holder''s email address is not valid. ' + CHAR(13) + CHAR(10), @EmailValid=0		
	
	END
	
	IF @Mobile <> '' AND NOT (@Mobile LIKE '[0][7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%' 
								AND @Mobile LIKE '%[0][7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
	BEGIN
	
		SELECT @NoteMessage=@NoteMessage+'P/holder''s mobile number is not valid. ' + CHAR(13) + CHAR(10), @MobileValid=0
	
	END
	
	
	SELECT TOP 1 @CaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.CustomerID=@CustomerID
	
	SELECT @DocsBy=ISNULL(dbo.fnGetDvAsInt ( 170257, @CaseID ),0)
	SELECT @CCEmail=ISNULL(dbo.fnGetDvAsInt ( 170259, @CaseID ),0)
	SELECT @CCSMS=ISNULL(dbo.fnGetDvAsInt ( 170260, @CaseID ),0)
	
	-- set defaults if the prefs aren't set
	IF @DocsBy=0 
	BEGIN
		SELECT @DocsBy=60788
		EXEC dbo._C00_SimpleValueIntoField 170257, @DocsBy, @CustomerID
	END
	IF @CCEmail=0
	BEGIN
		SELECT @CCEmail=5144
		EXEC dbo._C00_SimpleValueIntoField 170259, @CCEmail, @CustomerID
	END
	IF @CCSMS=0
	BEGIN
		SELECT @CCSMS=5144
		EXEC dbo._C00_SimpleValueIntoField 170260, @CCSMS, @CustomerID
	END
	
	IF ( @DocsBy=60789 OR @CCEmail=5144 ) AND ( @EmailValid=0 OR @Email='' )
	BEGIN
	
		SELECT @DocsBy=60788, @CCEmail=5145, @NoteMessage=@NoteMessage+'P/holder had a communications preference setting for email but does not have a valid email address. '  + CHAR(13) + CHAR(10)
		EXEC dbo._C00_SimpleValueIntoField 170257, @DocsBy, @CustomerID
		EXEC dbo._C00_SimpleValueIntoField 170259, @CCEmail, @CustomerID
	
	END
	
	IF @CCSMS=5144 AND ( @MobileValid=0 OR @Mobile='' )
	BEGIN
	
		SELECT @CCSMS=5145, @NoteMessage=@NoteMessage + 'P/holder had a communications preference setting for SMS but does not have a valid mobile number.' + CHAR(13) + CHAR(10)
		EXEC dbo._C00_SimpleValueIntoField 170260, @CCSMS, @CustomerID
	
	END
	
	/* 2018-03-12 Mark Beaumont - Add a message for any marketing email/mobile discrepancies but do not reset marketing flags 
		as the client would want to retain the marketing permission even though the target address/number is invalid. */
	SELECT		@IsSMSMarketingTarget = cdv_SMS.ValueInt,
				@IsEmailMarketingTarget = cdv_Email.ValueInt
	FROM		Customers c WITH (NOLOCK)
				INNER JOIN CustomerDetailValues cdv_SMS WITH (NOLOCK) ON c.CustomerID = cdv_SMS.CustomerID
					AND cdv_SMS.DetailFieldID = 177152		-- Customer allows Marketing by SMS	
				INNER JOIN CustomerDetailValues cdv_Email WITH (NOLOCK) ON c.CustomerID = cdv_Email.CustomerID
					AND cdv_Email.DetailFieldID = 177153	-- Customer allows Marketing by Email	
	WHERE		c.CustomerID = @CustomerID

	IF @IsSMSMarketingTarget = 1 AND ( @MobileValid = 0 OR @Mobile = '' )
		SELECT	@NoteMessage = @NoteMessage + 'P/holder has a marketing preference setting for SMS but does not have a valid mobile number.' + CHAR(13) + CHAR(10)
		
	IF @IsEmailMarketingTarget = 1 AND ( @EmailValid = 0 OR @Email = '' )
		SELECT	@NoteMessage = @NoteMessage + 'P/holder has a marketing preference setting for email but does not have a valid email address.'

	IF @NoteMessage <> ''
		EXEC _C600_AddANote @CaseID, 904, @NoteMessage

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CheckCommunicationPreferences] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CheckCommunicationPreferences] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CheckCommunicationPreferences] TO [sp_executeall]
GO
