SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-04-17
-- Description:	ISI New post-protected cert breakdown tables
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISI_ReturnPostProtectCert]
	@DetailFieldID	INT,
	@CustomerID		INT,
	@Secured		INT /*IE 5144 for yes and 5145 for no*/
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TableFieldID					INT,
			@SecuredFieldID					INT,
			@OrgNameFieldID					INT,
			@AmountDueFieldID				INT,
			@ProofFieldID					INT,
			@AcceptedField					INT,
			@MoveToRightsFieldID			INT,
			@ExcludeFromRepaymentFieldID	INT,
			@ClientID						INT 

	/*
		Using third party fields get all of the rows from all of the tables
		(there are 10 or so of them) and join them all into one for custom view.
	*/

	SELECT @ClientID = df.DetailFieldID
	FROM DetailFields df with (nolock) 
	WHERE df.DetailFieldID = @DetailFieldID

	/*Removed by CS 2015-03-24.  Use common function fn_C00_ISI_GetCreditorsForCustomer instead*/
	--SELECT tr.TableRowID, 
	--	--tdv_secured.DetailValue as [Secured], 
	--	CASE df.Lookup WHEN 1 THEN luli.ItemValue ELSE tdv_org.DetailValue END as [Name], 
	--	tdv_amt.ValueMoney as [Amount], 
	--	CASE WHEN tdv_proof.DetailValue = '' THEN 'false' ELSE tdv_proof.DetailValue END as [Proof], 
	--	CASE WHEN tdv_acc.DetailValue = '' THEN 'false' ELSE tdv_acc.DetailValue END as [Accepted],
	--	CASE WHEN tdv_voting.DetailValue = '' THEN 'false' ELSE tdv_voting.DetailValue END as [Voting],
	--	CASE WHEN tdv_exclude.DetailValue = '' THEN 'false' ELSE tdv_exclude.DetailValue END as [Exclude]
	--FROM ThirdPartyFieldMapping tpm_f WITH (NOLOCK)
	--INNER JOIN TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = tpm_f.DetailFieldID
	--CROSS APPLY ThirdPartyFieldMapping tpm_sec WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_secured WITH (NOLOCK) ON tdv_secured.TableRowID = tr.TableRowID and tdv_secured.DetailFieldID = tpm_sec.DetailFieldID AND tdv_secured.ValueInt = @Secured
	--CROSS APPLY ThirdPartyFieldMapping tpm_org WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_org WITH (NOLOCK) ON tdv_org.TableRowID = tr.TableRowID and tdv_org.DetailFieldID = tpm_org.DetailFieldID
	--INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv_org.DetailFieldID 
	--LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID AND luli.LookupListItemID = tdv_org.ValueInt
	--CROSS APPLY ThirdPartyFieldMapping tpm_amt WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_amt WITH (NOLOCK) ON tdv_amt.TableRowID = tr.TableRowID and tdv_amt.DetailFieldID = tpm_amt.DetailFieldID
	--CROSS APPLY ThirdPartyFieldMapping tpm_proof WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_proof WITH (NOLOCK) ON tdv_proof.TableRowID = tr.TableRowID and tdv_proof.DetailFieldID = tpm_proof.DetailFieldID
	--CROSS APPLY ThirdPartyFieldMapping tpm_acc WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_acc WITH (NOLOCK) ON tdv_acc.TableRowID = tr.TableRowID and tdv_acc.DetailFieldID = tpm_acc.DetailFieldID
	--CROSS APPLY ThirdPartyFieldMapping tpm_voting WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_voting WITH (NOLOCK) ON tdv_voting.TableRowID = tr.TableRowID and tdv_voting.DetailFieldID = tpm_voting.DetailFieldID
	--CROSS APPLY ThirdPartyFieldMapping tpm_exclude WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv_exclude WITH (NOLOCK) ON tdv_exclude.TableRowID = tr.TableRowID and tdv_exclude.DetailFieldID = tpm_exclude.DetailFieldID
	--WHERE tpm_f.ThirdPartyFieldID		= 848
	--and tpm_sec.ThirdPartyFieldID		= 849
	--and tpm_org.ThirdPartyFieldID		= 850
	--and tpm_amt.ThirdPartyFieldID		= 851
	--and tpm_proof.ThirdPartyFieldID		= 852
	--and tpm_acc.ThirdPartyFieldID		= 853
	--and tpm_voting.ThirdPartyFieldID	= 854
	--and tpm_exclude.ThirdPartyFieldID	= 855
	--AND tpm_f.LeadTypeID				= 0			
	--and tpm_sec.LeadTypeID				> 0		
	--and tpm_org.LeadTypeID				> 0		
	--and tpm_amt.LeadTypeID				> 0		
	--and tpm_proof.LeadTypeID			> 0		
	--and tpm_acc.LeadTypeID				> 0		
	--and tpm_voting.LeadTypeID			> 0	
	--and tpm_exclude.LeadTypeID			> 0		
	--AND tr.CustomerID = @CustomerID

	SELECT	 fn.TableRowID
			,fn.CreditorName													 [Name]
			,fn.Balance															 [Amount]
			,CASE WHEN fn.ProofOfDebt			= 1 THEN 'true' ELSE 'false' END [Proof]
			,CASE WHEN fn.DeemedAccepted		= 1 THEN 'true' ELSE 'false' END [Accepted]
			,CASE WHEN fn.MoveToVoting			= 1 THEN 'true' ELSE 'false' END [Voting]
			,CASE WHEN fn.ExcludeFromRepayments	= 1 THEN 'true' ELSE 'false' END [Exclude]
	FROM fn_C00_ISI_GetCreditorsForCustomer(@CustomerID) fn
	WHERE fn.IsSecure = CASE WHEN @Secured = 5144 THEN 1 ELSE 0 END 
	ORDER BY [Amount] DESC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_ReturnPostProtectCert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISI_ReturnPostProtectCert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_ReturnPostProtectCert] TO [sp_executeall]
GO
