SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuillConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationAquarium_Update]
(

	@QuillConfigurationAquariumID int   ,

	@Vendor varchar (50)  ,

	@APIKey uniqueidentifier   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuillConfigurationAquarium]
				SET
					[Vendor] = @Vendor
					,[APIKey] = @APIKey
				WHERE
[QuillConfigurationAquariumID] = @QuillConfigurationAquariumID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationAquarium_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Update] TO [sp_executeall]
GO
