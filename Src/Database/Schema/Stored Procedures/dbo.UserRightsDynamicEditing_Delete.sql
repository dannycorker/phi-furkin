SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UserRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamicEditing_Delete]
(

	@UserRightsDynamicEditingID int   
)
AS


				DELETE FROM [dbo].[UserRightsDynamicEditing] WITH (ROWLOCK) 
				WHERE
					[UserRightsDynamicEditingID] = @UserRightsDynamicEditingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamicEditing_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Delete] TO [sp_executeall]
GO
