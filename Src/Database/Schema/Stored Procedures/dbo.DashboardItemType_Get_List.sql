SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DashboardItemType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_Get_List]

AS


				
				SELECT
					[DashboardItemTypeID],
					[DashboardItemTypeName],
					[ClientID]
				FROM
					[dbo].[DashboardItemType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Get_List] TO [sp_executeall]
GO
