SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-08-15
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CustomViewRouting_Update]
(
@CustomViewRouteID int 
,@MapsToFieldID int 
)
AS

UPDATE top (1) [CustomViewRouting]
SET [MapsToFieldID] = @MapsToFieldID
WHERE customviewrouteid= @CustomViewRouteID
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomViewRouting_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CustomViewRouting_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomViewRouting_Update] TO [sp_executeall]
GO
