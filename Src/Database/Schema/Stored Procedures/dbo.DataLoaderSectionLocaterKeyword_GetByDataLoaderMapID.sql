SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderSectionLocaterKeyword table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapID]
(

	@DataLoaderMapID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderSectionLocaterKeywordID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderMapSectionID],
					[Keyword],
					[DataLoaderKeywordMatchTypeID],
					[KeywordNotes],
					[RelativeRow],
					[AbsoluteCol],
					[IsForSectionStart]
				FROM
					[dbo].[DataLoaderSectionLocaterKeyword] WITH (NOLOCK) 
				WHERE
					[DataLoaderMapID] = @DataLoaderMapID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapID] TO [sp_executeall]
GO
