SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16/08/2016
-- Description:	Gets The PremiumCalculationDetailValues
-- 2017-08-14 CPS order by Rule Sequence
-- =============================================
CREATE PROCEDURE [dbo].[PremiumCalculationDetailValue__GetByPremiumCalculationDetailID] 

	@PremiumCalculationDetailID INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
	WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationDetailID
	ORDER BY pcdv.RuleSequence
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumCalculationDetailValue__GetByPremiumCalculationDetailID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumCalculationDetailValue__GetByPremiumCalculationDetailID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumCalculationDetailValue__GetByPremiumCalculationDetailID] TO [sp_executeall]
GO
