SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-03-05
-- Description:	Keep Mark's demo up to date
-- =============================================
CREATE PROCEDURE [dbo].[_AquariumDemoUpdate]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DaysToAdd TABLE (
		LeadID int,
		DaysToAdd int
	)

	-- For each demo Lead you want to keep up to date, add an entry to this list
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171474, 10)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (170918, 9)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (170976, 8)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171191, 9)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (176378, 6)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171479, 5)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171483, 4)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171485, 3)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171490, 2)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (171492, 0)
	INSERT INTO @DaysToAdd (LeadID, DaysToAdd) VALUES (391841, 20)


	UPDATE LeadEvent
	SET LeadEvent.FollowUpDateTime = dateadd(dd, dta.DaysToAdd, dbo.fn_GetDate_Local())
	FROM @DaysToAdd dta
	WHERE LeadEvent.LeadID = dta.LeadID
	AND LeadEvent.LeadEventID = (
		select max(le2.LeadEventID)
		from leadevent le2
		where le2.leadid = dta.LeadID
		and le2.FollowUpDateTime IS NOT NULL
	)
	
	UPDATE LeadEvent
	SET LeadEvent.WhenCreated = dateadd(dd, dta.DaysToAdd+21-375, dbo.fn_GetDate_Local())
	FROM @DaysToAdd dta
	WHERE LeadEvent.LeadID = dta.LeadID
	and leadEvent.EventTypeID = 3229

	SELECT 'Success' as [Outcome]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_AquariumDemoUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_AquariumDemoUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_AquariumDemoUpdate] TO [sp_executeall]
GO
