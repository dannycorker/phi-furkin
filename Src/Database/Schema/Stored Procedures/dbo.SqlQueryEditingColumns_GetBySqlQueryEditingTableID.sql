SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingColumns table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingTableID]
(

	@SqlQueryEditingTableID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingColumnID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[CompleteOutputText],
					[ColumnAlias],
					[ShowColumn],
					[DisplayOrder],
					[SortOrder],
					[SortType],
					[IsAggregate],
					[ColumnDataType],
					[ManipulatedDataType],
					[ColumnNaturalName],
					[RealSqlQueryColumnID],
					[ComplexValue]
				FROM
					[dbo].[SqlQueryEditingColumns] WITH (NOLOCK) 
				WHERE
					[SqlQueryEditingTableID] = @SqlQueryEditingTableID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingTableID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingTableID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingTableID] TO [sp_executeall]
GO
