SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddClientAreaMailingList]

@Name nvarchar(200), 
@Email nvarchar(255),
@PostCode nvarchar(10),
@OutcomeID int,
@YellowPagesAreaCode nvarchar(10),
@ClientID int,
@ClientQuestionnaireID int,
@OnHold bit

as

insert into ClientAreaMailingLists (ClientAreaMailingLists.Name, Email, PostCode, OutcomeID, YellowPagesAreaCode, ClientID, ClientQuestionnaireID, OnHold)
values (@Name, @Email, @PostCode, @OutcomeID, @YellowPagesAreaCode, @ClientID, @ClientQuestionnaireID, @OnHold)


declare @ClientAreaMailingListID int

set @ClientAreaMailingListID = SCOPE_IDENTITY()

select ClientAreaMailingListID
from ClientAreaMailingLists
where ClientAreaMailingListID = @ClientAreaMailingListID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientAreaMailingList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientAreaMailingList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientAreaMailingList] TO [sp_executeall]
GO
