SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-08
-- Description:	Get first event type for a given lead type
-- =============================================
CREATE PROCEDURE [dbo].[GetFirstEventTypeByLeadType] 
	
	@LeadTypeID int 

AS
BEGIN
	SET NOCOUNT ON;

	select fet.eventtypeid, fet.eventtypename 
	from eventtype fet 
	where fet.leadtypeid = @LeadTypeID 
	and fet.eventsubtypeid = 10 
	and fet.enabled = 1 
	and fet.AvailableManually = 1
	order by fet.eventtypeid

END
















GO
GRANT VIEW DEFINITION ON  [dbo].[GetFirstEventTypeByLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetFirstEventTypeByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetFirstEventTypeByLeadType] TO [sp_executeall]
GO
