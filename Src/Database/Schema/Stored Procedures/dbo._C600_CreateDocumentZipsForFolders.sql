SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-22
-- Description:	Create DocumentZip records for any documents on the queue that are listed in the SIG FTP Folder Configuration.
--              A hack of Jim's original
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CreateDocumentZipsForFolders] 
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @CanBeAutoSent bit,
	@DocumentTypeName varchar(50),
	@DocsToZipCount int,
	@ValidControlRecords int,
	@DocumentZipInformationID int,
	@ServerMapPath varchar(2000), 
	@SecurePath char(1), 
	@HostPath varchar(250), 
	@WordTempDirectory varchar(250), 
	@ZipPrefix varchar(50), 
	@StatusID int,
	@ClientID int = dbo.fnGetPrimaryClientID(),
	@AutomatedTaskID int, 
	@DocumentTypeID int,
	@DocumentsPerZip int = 1,
	@LogEntry VARCHAR(2000),
	@Docs tvpIntInt


	INSERT INTO @Docs
	SELECT DISTINCT dt.ValueInt,0 FROM TableDetailValues dt WITH (NOLOCK) WHERE dt.ClientID=@ClientID AND dt.DetailFieldID=170146 AND dt.ValueInt IS NOT NULL

	WHILE EXISTS ( SELECT * FROM @Docs dt WHERE dt.ID2=0 )
	BEGIN
	
		SELECT TOP 1 @DocumentTypeID=dt.ID1 FROM @Docs dt WHERE dt.ID2=0

		--SELECT @LogEntry='Zipping DocumentTypeID - ' + CONVERT(VARCHAR(30),@DocumentTypeID)
		--EXEC _C00_LogIt 'Info', 'CreateDocumentZipsFromAutomatedTask', 'Collecting Test Data', @LogEntry, 2372 

		-- If documents need to be individually tailored before saving & printing,
		-- then they cannot be auto-queued. They must be completed manually, one at a time.
		SELECT @CanBeAutoSent = dt.CanBeAutoSent, 
		@DocumentTypeName = dt.DocumentTypeName 
		FROM dbo.DocumentType dt 
		WHERE dt.DocumentTypeID = @DocumentTypeID 

		IF @CanBeAutoSent = 1
		BEGIN

			-- Special processing should only take place for documents
			-- that have a control record set up to handle them.
			-- Regular processing (where controlRespondsToDocumentTypeID is 0)
			-- takes place under all circumstances unless a special control record exists
			-- with "Suppress all after me" set to true.
			SELECT @ValidControlRecords = COUNT(*) 
			FROM dbo.DocumentZipFtpControl con 
			WHERE con.ClientID IN (@ClientID) 
			AND con.Enabled = 1 
			AND ((con.SpecificDocumentTypesOnly = 0) OR EXISTS(SELECT * FROM dbo.DocumentZipFtpControlLink link WHERE link.ClientID = con.ClientID AND link.DocumentZipFtpControlID = con.DocumentZipFtpControlID AND link.Enabled = 1 AND link.DocumentTypeID = @DocumentTypeID))

			IF @ValidControlRecords > 0
			BEGIN
				SELECT @DocsToZipCount = COUNT(*) 
				FROM dbo.DocumentQueue dq 
				WHERE dq.ClientID = @ClientID
				AND dq.[WhenCreated] IS NULL
				AND dq.[DocumentTypeID] = @DocumentTypeID
				AND NOT EXISTS (
					SELECT * 
					FROM DocumentZip dz 
					WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
				)

				IF @DocsToZipCount > 0
				BEGIN

					SELECT @ServerMapPath = 'ClientImages\Client_' + convert(varchar, @ClientID) + '_Images\DocumentZips', 
					@SecurePath = 's', 
					@HostPath = 'www.aquarium-software.com', 
					@WordTempDirectory = 'WordTemp', 
					@ZipPrefix = @DocumentTypeName, 
					@StatusID = 35
					
					-- Insert a Zip Information header record for this batch of records
					INSERT INTO [dbo].[DocumentZipInformation] (ClientID, ServerMapPath, SecurePath, HostPath, WordTempDirectory, DocumentsPerZip, ZipPrefix, StatusID, TriggeredByDocumentTypeID)
					VALUES (@ClientID, @ServerMapPath, @SecurePath, @HostPath, @WordTempDirectory, @DocumentsPerZip, @ZipPrefix, @StatusID, @DocumentTypeID)

					SELECT @DocumentZipInformationID = SCOPE_IDENTITY()

					-- Insert the detail records
					INSERT INTO dbo.DocumentZip (DocumentZipInformationID, ClientID, DocumentQueueID, StatusID) 
					SELECT @DocumentZipInformationID, @ClientID, dq.DocumentQueueID, @StatusID
					FROM dbo.DocumentQueue dq 
					WHERE dq.ClientID = @ClientID
					AND dq.[WhenCreated] IS NULL
					AND dq.[DocumentTypeID] = @DocumentTypeID
					AND NOT EXISTS (
						SELECT * 
						FROM DocumentZip dz 
						WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
					)

					-- Now simply wait for the DocumentZips() method to fire in the batch scheduler
					-- app, which happens once every minute.  That will pick up these records, produce
					-- the documents, zip them, and then run any special bat/ftp commands as dictated
					-- by the DocumentZipFtpControl table for this client and document type.

				END --IF @DocsToZipCount > 0

			END --IF @ValidControlRecords > 0

		END --IF @CanBeAutoSent = 1
		
		UPDATE @Docs SET ID2=1 WHERE ID1=@DocumentTypeID
	
	END
	
	SELECT ''
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreateDocumentZipsForFolders] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CreateDocumentZipsForFolders] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreateDocumentZipsForFolders] TO [sp_executeall]
GO
