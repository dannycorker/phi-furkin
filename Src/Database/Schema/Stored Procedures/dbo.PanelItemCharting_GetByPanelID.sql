SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PanelItemCharting table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_GetByPanelID]
(

	@PanelID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PanelItemChartingID],
					[ClientID],
					[PanelItemChartingName],
					[PanelID],
					[CreatedBy],
					[ChartID]
				FROM
					[dbo].[PanelItemCharting] WITH (NOLOCK) 
				WHERE
					[PanelID] = @PanelID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_GetByPanelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_GetByPanelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_GetByPanelID] TO [sp_executeall]
GO
