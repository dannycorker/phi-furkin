SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DroppedOutCustomerAnswers table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerAnswers_GetByCustomerQuestionnaireID]
(

	@CustomerQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerAnswerID],
					[CustomerQuestionnaireID],
					[MasterQuestionID],
					[Answer],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[DroppedOutCustomerAnswers] WITH (NOLOCK) 
				WHERE
					[CustomerQuestionnaireID] = @CustomerQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_GetByCustomerQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerAnswers_GetByCustomerQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_GetByCustomerQuestionnaireID] TO [sp_executeall]
GO
