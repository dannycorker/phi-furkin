SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Ben Crinion
-- Purpose: Select the output format from the DocumentType Table for a lead document.
-- JWG 2012-07-12 Use LeadDocument again now that it has all the details back again.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument__GetOutputFormatByLeadDocumentID]
(
	@LeadDocumentID int   
)
AS
BEGIN

	SELECT dt.OutputFormat
	FROM [dbo].[DocumentType] dt WITH (NOLOCK) 
	INNER JOIN dbo.LeadDocument ld WITH (NOLOCK) on ld.DocumentTypeID = dt.DocumentTypeID
	WHERE ld.[LeadDocumentID] = @LeadDocumentID
	
	SELECT @@ROWCOUNT
END				




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__GetOutputFormatByLeadDocumentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument__GetOutputFormatByLeadDocumentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__GetOutputFormatByLeadDocumentID] TO [sp_executeall]
GO
