SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the RPIEventTypeMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventTypeMapping_Insert]
(

	@RPIEventTypeMappingID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@RPIEventTypeID int   ,

	@EventTypeID int   ,

	@IsEnabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[RPIEventTypeMapping]
					(
					[ClientID]
					,[LeadTypeID]
					,[RPIEventTypeID]
					,[EventTypeID]
					,[IsEnabled]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@RPIEventTypeID
					,@EventTypeID
					,@IsEnabled
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @RPIEventTypeMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventTypeMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_Insert] TO [sp_executeall]
GO
