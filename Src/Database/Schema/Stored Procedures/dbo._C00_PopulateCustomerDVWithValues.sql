SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Chris Townsend
-- Create date: 2010-12-17
-- Description:	Populate any number of CustomerDetailValues at once 
--				(Based on PopulateLDVWithValues)
--				LeadOrMatter 10 = Customer Detail Value
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateCustomerDVWithValues]
(
	@CustomerID int,
	@ClientPersonnelID int,
	@tvpIDValue tvpIDValue READONLY
)
AS
BEGIN
	SET NOCOUNT ON;
	
	
	
	/* Maintain history if enabled. Only do this for data that has changed, so do it first or it won't look like it has changed. */
	INSERT INTO DetailValueHistory(ClientID, LeadID, MatterID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue) 
	SELECT df.ClientID, @CustomerID, NULL, t.AnyID, 10, t.AnyValue, dbo.fn_GetDate_Local(), @ClientPersonnelID, NULL 
	FROM @tvpIDValue t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 10 AND df.MaintainHistory = 1 
	LEFT JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = t.AnyID AND cdv.CustomerID = @CustomerID
	WHERE t.AnyValue IS NOT NULL
	AND ((cdv.CustomerDetailValueID IS NULL) OR (cdv.DetailValue <> t.AnyValue)) 
	
	/* Update any values that already exist but have changed */
	UPDATE dbo.CustomerDetailValues 
	SET DetailValue = t.AnyValue
	FROM @tvpIDValue t 
	INNER JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = t.AnyID AND cdv.CustomerID = @CustomerID
	WHERE t.AnyValue IS NOT NULL
	AND cdv.DetailValue <> t.AnyValue
	
	/* Insert any values that don't exist already */
	INSERT INTO dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	SELECT df.ClientID, @CustomerID, t.AnyID, t.AnyValue
	FROM @tvpIDValue t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 10 
	WHERE t.AnyValue IS NOT NULL
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.CustomerDetailValues cdv WITH (NOLOCK) 
		WHERE cdv.CustomerID = @CustomerID
		AND cdv.DetailFieldID = t.AnyID 
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateCustomerDVWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateCustomerDVWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateCustomerDVWithValues] TO [sp_executeall]
GO
