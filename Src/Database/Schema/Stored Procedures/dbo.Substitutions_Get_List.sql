SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Substitutions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Substitutions_Get_List]

AS


				
				SELECT
					[SubstitutionsID],
					[SubstitutionsVariable],
					[SubstitutionsDisplayName],
					[SubstitutionsDescription]
				FROM
					[dbo].[Substitutions] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Substitutions_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Get_List] TO [sp_executeall]
GO
