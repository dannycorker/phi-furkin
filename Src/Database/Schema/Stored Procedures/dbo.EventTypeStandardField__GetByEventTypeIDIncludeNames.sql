SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-01
-- Description:	Gets a list of event type standard fields by the given event type id,
-- Includes the object and field names
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeStandardField__GetByEventTypeIDIncludeNames]
(

	@EventTypeID INT   
)
AS


	SET ANSI_NULLS ON
	
	SELECT t.ObjectTypeName ObjectName, f.FieldName, esf.* 
	FROM EventTypeStandardField esf WITH (NOLOCK) 
	INNER JOIN dbo.DataLoaderObjectField f WITH (NOLOCK) ON f.DataLoaderObjectFieldID = esf.DataLoaderObjectFieldID
	INNER JOIN dbo.DataLoaderObjectType t WITH (NOLOCK) ON t.DataLoaderObjectTypeID = f.DataLoaderObjectTypeID
	WHERE esf.EventTypeID = @EventTypeID
	
	SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField__GetByEventTypeIDIncludeNames] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField__GetByEventTypeIDIncludeNames] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField__GetByEventTypeIDIncludeNames] TO [sp_executeall]
GO
