SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Chris Townsend
-- Create date: 09 July 2007
-- Description:	Selects the column names, column data types 
--				and ordinal position (the order in which the columns were
--				created) from a given table
				
-- =============================================
CREATE PROCEDURE [dbo].[ReportBuilder_GetTableColumnNames]
@TableName varchar(150)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COLUMN_NAME AS [COLUMN_NAME], DATA_TYPE, ORDINAL_POSITION
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = @TableName

END


GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetTableColumnNames] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportBuilder_GetTableColumnNames] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetTableColumnNames] TO [sp_executeall]
GO
