SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice_Get_List]

AS


				
				SELECT
					[EventChoiceID],
					[EventTypeID],
					[Description],
					[NextEventTypeID],
					[ClientID],
					[LeadTypeID],
					[ThreadNumber],
					[EscalationEvent],
					[Field],
					[LogicalOperator],
					[Value1],
					[Value2],
					[SqlClauseForInclusion],
					[Weighting],
					[ValueTypeID]
				FROM
					[dbo].[EventChoice] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Get_List] TO [sp_executeall]
GO
