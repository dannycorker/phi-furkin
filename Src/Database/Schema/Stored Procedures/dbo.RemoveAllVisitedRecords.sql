SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveAllVisitedRecords] @ClientQuestionnaireID int
AS
BEGIN
Delete From ClientQuestionnairesVisited where ClientQuestionnaireID = @ClientQuestionnaireID;
Delete From ClientQuestionnairesVisitedAndCompleted where ClientQuestionnaireID = @ClientQuestionnaireID;
Delete From ClientQuestionnairesVisitedButNotComplete where ClientQuestionnaireID = @ClientQuestionnaireID;
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveAllVisitedRecords] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoveAllVisitedRecords] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveAllVisitedRecords] TO [sp_executeall]
GO
