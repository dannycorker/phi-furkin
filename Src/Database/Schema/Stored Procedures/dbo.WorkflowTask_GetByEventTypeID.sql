SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowTask table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTask_GetByEventTypeID]
(

	@EventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[WorkflowTaskID],
					[WorkflowGroupID],
					[AutomatedTaskID],
					[Priority],
					[AssignedTo],
					[AssignedDate],
					[LeadID],
					[CaseID],
					[EventTypeID],
					[ClientID],
					[FollowUp],
					[Important],
					[CreationDate],
					[Escalated],
					[EscalatedBy],
					[EscalationReason],
					[EscalationDate],
					[Disabled],
					[DisabledReason],
					[DisabledDate]
				FROM
					[dbo].[WorkflowTask] WITH (NOLOCK) 
				WHERE
					[EventTypeID] = @EventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetByEventTypeID] TO [sp_executeall]
GO
