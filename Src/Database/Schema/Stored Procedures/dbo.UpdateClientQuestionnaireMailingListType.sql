SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateClientQuestionnaireMailingListType]

@ClientQuestionnaireID int,
@MailingListType int

as

Update ClientQuestionnaires
SET MailingListType = @MailingListType
where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireMailingListType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientQuestionnaireMailingListType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireMailingListType] TO [sp_executeall]
GO
