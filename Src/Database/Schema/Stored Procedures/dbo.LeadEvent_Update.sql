SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_Update]
(

	@LeadEventID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@Cost money   ,

	@Comments varchar (2000)  ,

	@EventTypeID int   ,

	@NoteTypeID int   ,

	@FollowupDateTime datetime   ,

	@WhenFollowedUp datetime   ,

	@AquariumEventType int   ,

	@NextEventID int   ,

	@CaseID int   ,

	@LeadDocumentID int   ,

	@NotePriority int   ,

	@DocumentQueueID int   ,

	@EventDeleted bit   ,

	@WhoDeleted int   ,

	@DeletionComments varchar (2000)  ,

	@ContactID int   ,

	@BaseCost money   ,

	@DisbursementCost money   ,

	@DisbursementDescription varchar (200)  ,

	@ChargeOutRate money   ,

	@UnitsOfEffort int   ,

	@CostEnteredManually bit   ,

	@IsOnHold bit   ,

	@HoldLeadEventID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadEvent]
				SET
					[ClientID] = @ClientID
					,[LeadID] = @LeadID
					,[WhenCreated] = @WhenCreated
					,[WhoCreated] = @WhoCreated
					,[Cost] = @Cost
					,[Comments] = @Comments
					,[EventTypeID] = @EventTypeID
					,[NoteTypeID] = @NoteTypeID
					,[FollowupDateTime] = @FollowupDateTime
					,[WhenFollowedUp] = @WhenFollowedUp
					,[AquariumEventType] = @AquariumEventType
					,[NextEventID] = @NextEventID
					,[CaseID] = @CaseID
					,[LeadDocumentID] = @LeadDocumentID
					,[NotePriority] = @NotePriority
					,[DocumentQueueID] = @DocumentQueueID
					,[EventDeleted] = @EventDeleted
					,[WhoDeleted] = @WhoDeleted
					,[DeletionComments] = @DeletionComments
					,[ContactID] = @ContactID
					,[BaseCost] = @BaseCost
					,[DisbursementCost] = @DisbursementCost
					,[DisbursementDescription] = @DisbursementDescription
					,[ChargeOutRate] = @ChargeOutRate
					,[UnitsOfEffort] = @UnitsOfEffort
					,[CostEnteredManually] = @CostEnteredManually
					,[IsOnHold] = @IsOnHold
					,[HoldLeadEventID] = @HoldLeadEventID
				WHERE
[LeadEventID] = @LeadEventID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Update] TO [sp_executeall]
GO
