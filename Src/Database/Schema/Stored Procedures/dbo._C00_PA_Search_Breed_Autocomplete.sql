SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-05-15
-- Description:	Allow brand specific searching via autocomplete
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PA_Search_Breed_Autocomplete]
(
	@Filter VARCHAR(50),
	@Species INT,
	@LeadID INT
)
AS
BEGIN


	DECLARE @CustomerID INT,
			@BrandID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID()
			
	SELECT @CustomerID = CustomerID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	SELECT @BrandID = ValueInt
	FROM dbo.CustomerDetailValues WITH (NOLOCK) 
	WHERE CustomerID = @CustomerID
	AND DetailFieldID = 170144
	
	EXEC dbo._C600_PA_Search_Breed @ClientID, 0, @Filter, @Species, @BrandID
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PA_Search_Breed_Autocomplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PA_Search_Breed_Autocomplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PA_Search_Breed_Autocomplete] TO [sp_executeall]
GO
