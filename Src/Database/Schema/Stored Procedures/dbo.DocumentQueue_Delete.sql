SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentQueue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentQueue_Delete]
(

	@DocumentQueueID int   
)
AS


				DELETE FROM [dbo].[DocumentQueue] WITH (ROWLOCK) 
				WHERE
					[DocumentQueueID] = @DocumentQueueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Delete] TO [sp_executeall]
GO
