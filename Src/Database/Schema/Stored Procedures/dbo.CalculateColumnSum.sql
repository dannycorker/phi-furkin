SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2008-08-12
-- Description:	Calculates table column sum
-- JWG 2010-05-25 Shrink it down to 1 single result column
-- PR  2010-01-06 Added support for client, customer, case, client personnel and contact detail values
-- =============================================
CREATE PROCEDURE [dbo].[CalculateColumnSum]
	@identity int, 
	@detailFieldSubTypeID int, 
	@detailFieldID int, 	
	@columnDetailFieldID int,
	@clientID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @simpleClause varchar(max) = '',
	@query VARCHAR(MAX) = ''
	
	SELECT @simpleClause = CASE 
		WHEN @detailFieldSubTypeID = 1 THEN ' AND (tdv.LeadID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 2 THEN ' AND (tdv.MatterID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 10 THEN ' AND (tdv.CustomerID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 11 THEN ' AND (tdv.CaseID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 12 THEN 'AND (tdv.ClientID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 13 THEN ' AND (tdv.ClientPersonnelID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 14 THEN ' AND (tdv.ContactID = ' + Cast(@identity as VarChar) + ')'
	END
	
	/* 
		Add up all the valid money values for this column (identified by @columnDetailFieldID)
		The same table can be used more than once per page, so the DetailFieldID on the TableRows table
		(identified by @detailFieldID) is used to tell them apart.
		Lead tables have NULL in the MatterID column.
	*/
	SET @query = 'SELECT CONVERT(Numeric(18,2),IsNULL(SUM(tdv.ValueMoney), 0.00))  	
	FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
	INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
	WHERE (tdv.DetailFieldID = ' + Cast(@columnDetailFieldID as VarChar) + ')
	AND (tr.DetailFieldID = ' + Cast(@detailFieldID as VarChar) + ') ' + @simpleClause

	exec (@query)
	

/*	AND (tdv.ClientID = @clientID) 
	AND (tdv.LeadID = @leadID) 
	AND (tdv.MatterID = @matterID OR (tdv.MatterID is null AND @matterID is null) OR (tdv.MatterID = 0 AND @matterID is null)) */
	
	
	/* 
		If any rows were returned, then round to 2 decimal places and return,
		otherwise just return the default empty string.
	*/
	--IF @MoneySum IS NOT NULL
	--BEGIN
	--	SELECT @Result = convert(varchar(30), convert(decimal(18, 2), @MoneySum))
	--END
	
	--SELECT @Result AS [result]
	
	/*SELECT dbo.TableDetailValues.DetailFieldID AS Expr1, dbo.TableDetailValues.LeadID AS Expr2, dbo.TableDetailValues.MatterID AS Expr3, 
                      dbo.TableDetailValues.ClientID AS Expr4, SUM(CONVERT(Decimal(18, 2), dbo.TableDetailValues.ValueMoney)) AS result, 
                      dbo.TableRows.DetailFieldID
	FROM dbo.TableDetailValues INNER JOIN
                      dbo.TableRows ON dbo.TableDetailValues.TableRowID = dbo.TableRows.TableRowID
	WHERE (dbo.TableDetailValues.DetailFieldID = @columnDetailFieldID) AND (dbo.TableDetailValues.LeadID = @leadID) AND ((@matterID is null and dbo.TableDetailValues.MatterID is null) or ( dbo.TableDetailValues.MatterID = @matterID)) AND 
                      (dbo.TableDetailValues.ClientID = @clientID)
	GROUP BY dbo.TableDetailValues.DetailFieldID, dbo.TableDetailValues.LeadID, dbo.TableDetailValues.MatterID, dbo.TableDetailValues.ClientID, 
                      dbo.TableRows.DetailFieldID
	HAVING (dbo.TableRows.DetailFieldID = @detailFieldID)*/

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CalculateColumnSum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalculateColumnSum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalculateColumnSum] TO [sp_executeall]
GO
