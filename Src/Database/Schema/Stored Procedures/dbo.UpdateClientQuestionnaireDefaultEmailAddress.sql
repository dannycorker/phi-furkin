SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateClientQuestionnaireDefaultEmailAddress]

@ClientQuestionnaireID int,
@DefaultEmailAddress nvarchar(255)

as

Update ClientQuestionnaires
SET DefaultEmailAddress = @DefaultEmailAddress
where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireDefaultEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientQuestionnaireDefaultEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaireDefaultEmailAddress] TO [sp_executeall]
GO
