SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerInformationFieldDefinitions table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerInformationFieldDefinitionID],
					[ClientQuestionnaireID],
					[FieldAtBeginningOfQuestionnaire],
					[OrdinalPosition],
					[FieldName],
					[FieldLabel],
					[Mandatory],
					[VerifyPhoneNumber],
					[ClientID]
				FROM
					[dbo].[CustomerInformationFieldDefinitions] WITH (NOLOCK) 
				WHERE
					[ClientQuestionnaireID] = @ClientQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_GetByClientQuestionnaireID] TO [sp_executeall]
GO
