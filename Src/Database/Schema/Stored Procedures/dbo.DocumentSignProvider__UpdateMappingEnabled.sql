SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__UpdateMappingEnabled]
	@ClientID INT,
	@DSPMappingID INT,
	@Enabled BIT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	UPDATE DocumentSignProviderMapping
	SET [Enabled] = @Enabled
	WHERE	ClientID = @ClientID
	AND		DSPMappingID = @DSPMappingID
	
	SELECT @@ROWCOUNT RowChanged
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__UpdateMappingEnabled] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__UpdateMappingEnabled] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__UpdateMappingEnabled] TO [sp_executeall]
GO
