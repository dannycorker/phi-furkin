SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectType_Get_List]

AS


				
				SELECT
					[ObjectTypeID],
					[ClientID],
					[SubClientID],
					[ObjectTypeName],
					[ObjectTypeDescription],
					[Enabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ObjectType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Get_List] TO [sp_executeall]
GO
