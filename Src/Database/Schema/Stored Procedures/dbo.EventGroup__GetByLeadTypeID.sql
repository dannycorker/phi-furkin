SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-11-2012
-- Description:	Gets a list of event groups by client and lead type (-1 means all lead types)
-- =============================================
CREATE PROCEDURE [dbo].[EventGroup__GetByLeadTypeID]
	@ClientID INT,
	@LeadTypeID INT

AS
BEGIN

	SELECT * 
	FROM dbo.EventGroup eg WITH (NOLOCK) 
	WHERE eg.ClientID = @ClientID 
	AND (@LeadTypeID = -1 OR eg.LeadTypeID=@LeadTypeID) 
	ORDER BY eg.EventGroupName

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroup__GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__GetByLeadTypeID] TO [sp_executeall]
GO
