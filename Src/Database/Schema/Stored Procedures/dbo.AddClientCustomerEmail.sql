SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddClientCustomerEmail]

@ClientID int,
@ClientQuestionnaireID int,
@CustomerID int,
@ClientAreaMailingListID int,
@DateSent datetime

as

insert into ClientCustomerMails (ClientID, ClientQuestionnaireID, CustomerID, ClientAreaMailingListID,DateSent)
values (@ClientID, @ClientQuestionnaireID, @CustomerID, @ClientAreaMailingListID,@DateSent)



GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientCustomerEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientCustomerEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientCustomerEmail] TO [sp_executeall]
GO
