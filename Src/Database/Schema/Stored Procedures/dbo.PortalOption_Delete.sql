SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalOption_Delete]
(

	@PortalOptionID int   
)
AS


				DELETE FROM [dbo].[PortalOption] WITH (ROWLOCK) 
				WHERE
					[PortalOptionID] = @PortalOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Delete] TO [sp_executeall]
GO
