SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-21
-- Description:	Runs the claim calculations on a group of matters
-- Used by:		DF 147634, SAE			
-- UPDATE:		SB 2012-03-16 We are now using the approved date in place of the paid date.  The detail field id has been changed below from 144354 to 144362 but all the variables still say paid.
-- Modified:    RH 2013-01-18 Fetches ExcessType and ExcessCeiling for ExcessRows
-- Modified:	RH 2013-10-21 Fix to match correct policy year when appliying limits (Zen#23991)
-- Modified:    RH 2014-04-29 Get PolicyType from the PolicyMatter rather than assuming the current policy (eg upgrade/downgrade cover)
--				SB 2014-06-27 Added Breed column which now comes back from co-insurance calcs
--				SB 2014-07-02 Removed claim items where the date of loss does not match for reinstatement policies
--				SB 2014-10-09 Moved the reinstatement logic above into the function fn_C224_GetRelatedClaims so it is available elsewhere
--				SB 2015-06-22 Change to allow multiple levels of age driven coinsurance
--				SB 2015-10-22 Changed Non Financial Limit to just handle age... discussed with RH... session count e.g. hydro therapy is handled elsewhere.  Moved age limit from
--							  _C00_1272_Policy_SavePolicySections so that it can work with optional sections too
--				SB 2015-12-17 Need to handle getting multiple rows of policy age related excess back and making sure we have the right amount against the cross joined rows
--				JL 2016-06-28 Added CutOffAge to output CoInsurance Table.
--				JL 2016-08-17 Added condition limits, if not applicable should simply return no records.
--				JL 2016-11-15 Added ID = 1 filter to co ins cut off date
--				CPS 2017-06-15 Changed @Debug to INT so I can use @Debug = 2 to actually debug, given that the app uses @Debug = 1 to function
--				CPS 2017-06-15 Moved the limits section to after calculations
--				CPS 2017-06-22 Removed a restriction on excess calculation that prevented acknowledgement of excess being used up by claims whose total was below the total excess
--				CPS 2017-06-25 Rearranged to allow excess and coinsurance to calculate and compare
--				CPS 2017-07-02 Changes to allow Policy Limits and Condition Limits to override one another depending on available balance for each
--				CPS 2017-07-17 Changes to accomodate UserDeductions
--				JEL 2018-01-10 Removed decision on whether to apply Excess or Coinsurance as LNG do both 
--				JEL 2018-04-15 Made 12 month policies obey their application rule (treatment start of Date of Loss)
--				SA 2018-04-30 - /*SA - updated client personnel record to 58552 from 44412 for line 1645
--				DJO 2018-05-17 - Joined Co-In to relevant policy section *Defect 174*
--              ANW 2018-05-16 Fix issue where claim detail row not getting excess applied. Trello 372
--				JEL 2019-03-04 Added sub query to stop returning duplicate rows for Excess
--				CPS 2019-03-15 Prevent double-excess application for #54450
--				CPS 2019-04-15 Prevent missed excess for 12 month policies for #55779
--				JEL 2019-06-10 Changed initial function calls for a simple select statement 
--				IS: 2019-07-03 SLACKY-PERF - breakup the calculation and the save into two parts, 1-CALC&CACHE 2-SAVE
--				JEL/GPR 2019-07-23 altered filter for P3/P2 #58420.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs]
(
	@CaseID INT,
	@Debug INT = 0
)

AS
BEGIN
	SET NOCOUNT ON;


	--				IS: 2019-07-03 SLACKY-PERF - breakup the calculation and the save into two parts, 1-CALC&CACHE 2-SAVE
	IF ISNULL(@Debug,0) = 0
	BEGIN
		EXEC [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs_Save] @CaseID
		RETURN;
	END
	--				IS: 2019-07-03 SLACKY-PERF - breakup the calculation and the save into two parts, 1-CALC&CACHE 2-SAVE
	
	DECLARE  @MatterID	INT
			,@LeadID	INT
			,@ClientID	INT
			,@RowCount	INT
			,@RejectThisClaim INT 
			
	SELECT	@MatterID = MatterID, 
			@LeadID = LeadID,
			@ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	
	---- Find the correct policy
	--DECLARE @CurrentPolicyMatterID INT
	--SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	
	---- Check the policy type
	--DECLARE @PolicyType INT
	----SELECT @PolicyType = dbo.fn_C00_1272_GetPolicyTypeFromClaim(@MatterID)
	--SELECT @PolicyType = dbo.fn_C00_1272_GetPolicyTypeFromScheme(@CurrentPolicyMatterID)
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@LeadID)
	
	/*JEL 2019-06-10 replaced the above function calls with a simple select for speed improvments*/ 
	DECLARE @CurrentPolicyMatterID INT ,
			@PolicyType INT 

	/*FInd the current scheme matter*/ 
	SELECT @CurrentPolicyMatterID = schmdv.MatterID  , @PolicyType = rdvType.ValueInt 
	FROM LeadDetailValues ldv WITH ( NOLOCK ) 
	INNER JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = ldv.ValueInt AND rdv.ClientID = @ClientID  AND rdv.DetailFieldID = 144269 /*Pet Type (Species)*/
	INNER JOIN MatterDetailValues polmdv with (NOLOCK) on polmdv.LeadID = ldv.LeadID and polmdv.DetailFieldID = 170034 /*Policy scheme RL*/  and polmdv.ClientID = @ClientID 
	INNER JOIN MatterDetailValues schmdv with (NOLOCK) on schmdv.DetailFieldID = 145689 and schmdv.ValueINT = polmdv.valueINT /*Scheme RL*/  and schmdv.ClientID = @ClientID 
	INNER JOIN MatterDetailValues sp with (NOLOCK) on sp.MatterID = schmdv.MatterID and sp.DetailFieldID = 177283 and sp.ValueINT = rdv.ValueINT /*SPecides*/ and sp.ClientID = @ClientID 
	INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) on rdvType.ResourceListID = polmdv.ValueInt and rdvType.DetailFieldID = 144319 and rdvType.ClientID = @ClientID /*JEL/GPR 2019-07-03 #58420*/
	WHERE ldv.leadID = @PolicyLeadID 
	AND ldv.ClientID = @ClientID
	AND ldv.DetailFieldID = 144272 /*Pet Type*/	
	
	DECLARE @DateOfBirth DATE
	SELECT @DateOfBirth = ValueDate
	FROM dbo.LeadDetailValues ldvPetDOB WITH (NOLOCK) 
	WHERE ldvPetDOB.LeadID = @PolicyLeadID 
	AND ldvPetDOB.DetailFieldID = 144274
		
	-- Get the list of claim rows from all related matters
	CREATE TABLE #ClaimRows 
	(
		ID INT,
		CustomerID INT,
		LeadID INT,
		CaseID INT,
		MatterID INT,
		ParentID INT,
		TableRowID INT,
		ResourceListID INT,
		ParentResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		DateOfLoss DATE,
		TreatmentStart DATE,
		TreatmentEnd DATE,
		Claim MONEY,
		UserDeductions MONEY,
		Settle MONEY,
		Total MONEY,
		Paid DATE,
		OutsidePolicyCover MONEY,
		Excess MONEY,
		VoluntaryExcess MONEY,
		ExcessRebate MONEY,
		CoInsurance MONEY,
		Limit MONEY,
		TotalSettle MONEY,
		LimitRLID INT,
		AfterCoInsCutIn BIT,
		ClaimRowType VARCHAR(500)

	)
	
	;WITH ClaimRowData AS 
	(
	SELECT	ROW_NUMBER() OVER(ORDER BY CASE WHEN m.CaseID = @CaseID THEN 1 ELSE 0 END, ISNULL(tdvPaid.ValueDateTime, dbo.fn_GetDate_Local()), tdvStart.ValueDate, tdvSettle.ValueMoney DESC, r.TableRowID) AS ID, 
			tdvSettle.CustomerID, tdvSettle.LeadID, tdvSettle.CaseID, 
			tdvSettle.MatterID, m.ParentID, r.TableRowID, tdvRLID.ResourceListID, ISNULL(s.Out_ResourceListID, tdvRLID.ResourceListID) AS ParentResourceListID, 
			ISNULL(mdvDateOfLoss.ValueDate, tdvStart.ValueDate) AS DateOfLoss, tdvStart.ValueDate AS TreatmentStart, tdvEnd.ValueDate AS TreatmentEnd,  
			tdvClaim.ValueMoney AS Claim, tdvUserDeduct.ValueMoney AS UserDeductions, ISNULL(tdvSettle.ValueMoney, tdvClaim.ValueMoney) AS Settle, tdvTotal.ValueMoney AS Total, 
			tdvPaid.ValueDate AS Paid, tdvOutside.ValueMoney AS OutsidePolicyCover, tdvExcess.ValueMoney AS Excess, tdvVoluntaryExcess.ValueMoney AS VoluntaryExcess, 
			tdvExcessRebate.ValueMoney AS ExcessRebate, tdvCoInsurance.ValueMoney AS CoInsurance, tdvLimit.ValueMoney AS LIMIT, llSection.ItemValue AS SECTION, 
			llSubSection.ItemValue AS SubSection,
			CASE WHEN tdvClaimRowType.ValueInt = 50538 THEN 1 ELSE 0 END AS AfterCoInsCutIn, tdvClaimRowType.ValueInt AS ClaimRowType

	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m ON r.MatterID = m.MatterID -- get related condition claims or all if a reinstatement policy
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON m.CaseID = c.CaseID 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH ( NOLOCK )  ON tdvRLID.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH ( NOLOCK )  ON rdvSection.ValueInt = llSection.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH ( NOLOCK )  ON tdvRLID.ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH ( NOLOCK )  ON rdvSubSection.ValueInt = llSubSection.LookupListItemID
	LEFT JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID AND s.ResourceListID != s.Out_ResourceListID
	INNER JOIN dbo.TableDetailValues tdvClaim WITH (NOLOCK) ON r.TableRowID = tdvClaim.TableRowID AND tdvClaim.DetailFieldID = 144353
	INNER JOIN dbo.TableDetailValues tdvUserDeduct WITH (NOLOCK) ON r.TableRowID = tdvUserDeduct.TableRowID AND tdvUserDeduct.DetailFieldID = 146179
	INNER JOIN dbo.TableDetailValues tdvSettle WITH (NOLOCK) ON r.TableRowID = tdvSettle.TableRowID AND tdvSettle.DetailFieldID = 145678
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 144349 
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 144351
	LEFT JOIN dbo.TableDetailValues tdvClaimRowType WITH (NOLOCK) ON r.TableRowID = tdvClaimRowType.TableRowID AND tdvClaimRowType.DetailFieldID = 149778 /*Claim Row Type*/
	LEFT JOIN dbo.TableDetailValues tdvPaid WITH (NOLOCK) ON r.TableRowID = tdvPaid.TableRowID AND tdvPaid.DetailFieldID = 144362 -- 144354 we now use approved and not paid
	LEFT JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	LEFT JOIN dbo.TableDetailValues tdvOutside WITH (NOLOCK) ON r.TableRowID = tdvOutside.TableRowID AND tdvOutside.DetailFieldID = 147001
	LEFT JOIN dbo.TableDetailValues tdvExcess WITH (NOLOCK) ON r.TableRowID = tdvExcess.TableRowID AND tdvExcess.DetailFieldID = 146406
	LEFT JOIN dbo.TableDetailValues tdvVoluntaryExcess WITH (NOLOCK) ON r.TableRowID = tdvVoluntaryExcess.TableRowID AND tdvVoluntaryExcess.DetailFieldID = 158802
	LEFT JOIN dbo.TableDetailValues tdvExcessRebate WITH (NOLOCK) ON r.TableRowID = tdvExcessRebate.TableRowID AND tdvExcessRebate.DetailFieldID = 147434
	LEFT JOIN dbo.TableDetailValues tdvCoInsurance WITH (NOLOCK) ON r.TableRowID = tdvCoInsurance.TableRowID AND tdvCoInsurance.DetailFieldID = 146407
	LEFT JOIN dbo.TableDetailValues tdvLimit WITH (NOLOCK) ON r.TableRowID = tdvLimit.TableRowID AND tdvLimit.DetailFieldID = 146408
	LEFT JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892

	WHERE r.DetailFieldID = 144355
	AND r.DetailFieldPageID = 16157
	AND r.ClientID = @ClientID
	AND (c.ClientStatusID IS NULL OR c.ClientStatusID NOT IN (3819)) -- Exclude rejected claims
	AND (tdvPaid.ValueDate IS NOT NULL OR c.CaseID = @CaseID) -- All paid rows and the rows from this claim
	)
	
	
	INSERT #ClaimRows (ID, CustomerID, LeadID, CaseID, MatterID, ParentID, TableRowID, ResourceListID, ParentResourceListID, DateOfLoss, TreatmentStart, TreatmentEnd, Claim, UserDeductions, 
						Settle, Total, Paid, OutsidePolicyCover, Excess, VoluntaryExcess, ExcessRebate, CoInsurance, Limit, Section, SubSection, AfterCoInsCutIn, ClaimRowType)
	SELECT * 
	FROM ClaimRowData
	
	-- For reinstatement we get all paid claims and not just for this condition.  We need to know the parent ID for this claim so we can limit excess and rebate to just this condition
	DECLARE @ClaimParentID INT
	SELECT @ClaimParentID = ISNULL(ParentID, MatterID)
	FROM #ClaimRows
	WHERE MatterID = @MatterID
	
	-- Clear out any data for items that have not been paid
	UPDATE #ClaimRows
	SET --OutsidePolicyCover = NULL,
		Excess = NULL, 
		VoluntaryExcess = NULL,
		ExcessRebate = NULL,
		CoInsurance = NULL,
		Limit = NULL,
		Total = NULL

	WHERE Paid IS NULL
	OR MatterID = @MatterID
	
	
	-- Initalise the total settle amount for the matter
	;WITH InnerSql AS 
	(
		SELECT MatterID, SUM(Settle) AS TotalSettle
		FROM #ClaimRows
		GROUP BY MatterID
	)
	UPDATE c
	SET c.TotalSettle = i.TotalSettle
	FROM #ClaimRows c
	INNER JOIN InnerSql i ON c.MatterID = i.MatterID

	
	-- Get the policy cover dates.  
	DECLARE @PolicyCover TABLE
	(
		ID INT IDENTITY,
		PolicyType VARCHAR(50),
		StartDate DATE,
		EndDate DATE,
		InceptionDate DATE,
		SectionName VARCHAR(50),
		SectionRLID INT
		,SubSectionRLID	INT
	)
	
	-- Reinstatement will be the list of policy start and end dates that cover the treatment period		
	DECLARE @FirstTreatment DATE, @LastTreatment DATE
	SELECT @FirstTreatment = MIN(TreatmentStart), @LastTreatment = MAX(TreatmentEnd)
	FROM #ClaimRows
	WHERE CaseID = @CaseID
	
-- We need to loop round this for each section. Also add PolicyType for section into input and output  	
	DECLARE @Sections TABLE (RowNumber INT, ClaimMatterID INT, SectionName VARCHAR(50),SectionRLID INT, Processed BIT, SubSectionRlID INT) /*CPS 2017-10-19 added SubSectionRLID #44277*/
	INSERT INTO @Sections (RowNumber,ClaimMatterID,SectionName,SectionRLID,Processed,SubSectionRlID)
	Select ROW_NUMBER() OVER (ORDER BY c.MatterID DESC), c.MatterID, c.Section, c.ParentResourceListID,0,c.ResourceListID FROM #ClaimRows c 
	WHERE c.MatterID = @MatterID
	
	
	DECLARE @CurrentRow INT, @SectionRLID INT, @SectionName VARCHAR(50), @SubSectionRLID INT
	PRINT 'Section Loop1'
	WHILE EXISTS (SELECT * FROM @Sections s WHERE s.Processed = 0) 
	BEGIN 
	
		SELECT TOP 1 @CurrentRow = s.RowNumber, @SectionRLID = s.SectionRLID, @SectionName = s.SectionName, @SubSectionRLID = s.SubSectionRlID  FROM @Sections s
		WHERE s.Processed = 0 

		SELECT @PolicyType = tdv1.ValueInt FROM TableRows tr WITH ( NOLOCK )
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tdv.DetailFieldID = 144357 /*Policy Section*/
		INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 176935 /*Application Rule*/
		WHERE tr.MatterID = @CurrentPolicyMatterID 
		and tdv.ResourceListID = @SectionRLID		
		
		/*Max Ben*/ 
		IF @PolicyType = 76174
		BEGIN 

			SELECT @FirstTreatment = MIN(TreatmentStart), @LastTreatment = MAX(TreatmentEnd)
			FROM #ClaimRows
	
		END 

		DECLARE @TempPolicyCover TABLE 
		(	
		ID INT IDENTITY,
		PolicyType VARCHAR(50),
		StartDate DATE,
		EndDate DATE,
		InceptionDate DATE 
		)
		
		INSERT INTO @TempPolicyCover(PolicyType, StartDate, EndDate, InceptionDate)
		EXEC dbo._C00_1272_Policy_GetGroupedPolicyCover @MatterID, @LeadID, @CurrentPolicyMatterID, @PolicyType, @FirstTreatment, @LastTreatment
		
		INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate,SectionName,SectionRLID,SubSectionRLID)
		SELECT t.PolicyType,t.StartDate,t.EndDate,t.InceptionDate,@SectionName, @SectionRLID, @SubSectionRLID  FROM @TempPolicyCover t
				
		DELETE @TempPolicyCover 
		
		Update @Sections 
		SET Processed = 1
		Where RowNumber = @CurrentRow 
		
	END	
	
	Print 'Section Loop2'
	
	-- Get the list of policy section and sums insured
	DECLARE @PolicyLimits TABLE
	(
		ID INT IDENTITY,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		SumInsured MONEY,
		Balance MONEY,
		StartDate DATE,
		EndDate DATE,
		AllowedCount INT,
		--AllowedRemaining INT, See comment from 2015-10-22
		LimitTypeID INT,
		LimitType VARCHAR(2000)
	)
	PRINT '@CurrentPolicyMatterID'
	PRINT @CurrentPolicyMatterID

	PRINT '@MatterID'
	PRINT @MatterID

	INSERT @PolicyLimits (ResourceListID, Section, SubSection, SumInsured, AllowedCount, LimitTypeID, LimitType)
	EXEC dbo._C00_1272_Policy_GetGroupedPolicyLimits @MatterID, @CurrentPolicyMatterID,0
	
	-- We cross join with the policy cover data so we have a row for each policy year for reinstatement
	INSERT @PolicyLimits (ResourceListID, Section, SubSection, SumInsured, AllowedCount, LimitTypeID, LimitType, StartDate, EndDate)
	SELECT DISTINCT ResourceListID, Section, SubSection, SumInsured, AllowedCount, LimitTypeID, LimitType, c.StartDate, c.EndDate
	FROM @PolicyLimits
-- Change this cross join to an inner joining on section/subsection
--	CROSS JOIN @PolicyCover c
	INNER JOIN @PolicyCover c on c.SectionName = Section  --c.SectionRLID = ResourceListID
	-- And then delete the original rows
	DELETE @PolicyLimits
	WHERE StartDate IS NULL
	
	UPDATE @PolicyLimits
	SET Balance = SumInsured

			-- Initialise the balance 

	-- Now deduct the allowed clim items for the paid claims - See comment from 2015-10-22
	--;WITH ClaimYears AS 
	--(
	--	SELECT p.ID, 
	--	ROW_NUMBER() OVER(PARTITION BY p.ResourceListID, c.CaseID ORDER BY c.CaseID) as rn 
	--	FROM @PolicyLimits p
	--	INNER JOIN #ClaimRows c ON (c.ResourceListID = p.ResourceListID OR c.ParentResourceListID = p.ResourceListID) AND c.TreatmentStart >= p.StartDate AND c.TreatmentStart < p.EndDate
	--	WHERE p.AllowedCount > 0
	--	AND c.Paid IS NOT NULL 
	--	AND c.MatterID != @MatterID
	--)
	
	--UPDATE p
	--SET p.AllowedRemaining -= 1
	--FROM @PolicyLimits p
	--INNER JOIN ClaimYears y ON p.ID = y.ID
	--WHERE y.rn = 1
		
	Print 'Excess Loop1'
	
	DECLARE @PolicyCoverMultiYear TABLE
	(
		ID INT IDENTITY,
		PolicyType VARCHAR(50),
		StartDate DATE,
		EndDate DATE,
		InceptionDate DATE
	)
		

	INSERT @PolicyCoverMultiYear (PolicyType, StartDate, EndDate, InceptionDate)
	EXEC dbo._C00_1272_Policy_GetGroupedPolicyCover @MatterID, @LeadID, @CurrentPolicyMatterID, @PolicyType, @FirstTreatment, @LastTreatment, @ReturnMultipleYearsForExcess = 1				
		
	-- Get a list of the excesses that apply
	DECLARE @PolicyExcesses TABLE
	(
		ID INT IDENTITY,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		Excess MONEY,
		PercentageExcess DECIMAL(19,4),
		Postcode VARCHAR(50),
		Breed VARCHAR(2000),
		PetAge INT,
		PetType INT,
		ExcessType INT,
		ExcessCeiling MONEY,
		VoluntaryExcess MONEY,
		PolicyAge INT,
		ExcessDeducted MONEY,
		ExcessMatterID INT,
		ClaimPaid MONEY,
		StartDate DATE,
		EndDate DATE,
		InceptionDate DATE,
		ExcessApproachingCeiling MONEY,
		ExcessRule INT
	)
	INSERT @PolicyExcesses (ResourceListID, Section, SubSection, Excess, PercentageExcess, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, VoluntaryExcess, PolicyAge, ExcessRule)
	EXEC dbo._C00_1272_Policy_GetGroupedPolicyExcesses  @MatterID, @CurrentPolicyMatterID

	DECLARE @ToProcess INT = 1 
	
	PRINT 'Excess Loop1b'
		
	WHILE EXISTS (SELECT * FROM @PolicyExcesses where ID = @ToProcess) 
	BEGIN
	
	
			-- Check to see  if we need an excess each policy year like Sainsbury's
		DECLARE @ExcessEachYear BIT = 0
		
		SELECT @ExcessEachYear =  CASE p.ExcessRule WHEN 69459 THEN 1 ELSE 0 END FROM @PolicyExcesses p 
		Where p.ID = @ToProcess

	-- loop round this based on excess type. 	
		-- One excess per policy cover period
		IF @ExcessEachYear = 0
		BEGIN
			INSERT @PolicyExcesses (ResourceListID, Section, SubSection, Excess, PercentageExcess, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, VoluntaryExcess, PolicyAge, StartDate, EndDate, InceptionDate,ExcessRule)
			SELECT ResourceListID, Section, SubSection, Excess, PercentageExcess, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, VoluntaryExcess, PolicyAge, c.StartDate, c.EndDate, c.InceptionDate, e.ExcessRule
			FROM @PolicyExcesses e
			INNER JOIN @PolicyCover c on c.SectionRLID = e.ResourceListID 
			Where e.ID = @ToProcess
			and e.StartDate is NULL
		END
		ELSE -- If we need to have an excess deducted per year
		BEGIN
			INSERT @PolicyExcesses (ResourceListID, Section, SubSection, Excess, PercentageExcess, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, VoluntaryExcess, PolicyAge, StartDate, EndDate, InceptionDate, ExcessRule)
			SELECT e.ResourceListID, e.Section, e.SubSection, e.Excess, e.PercentageExcess, e.Postcode, e.Breed, e.PetAge, e.PetType, e.ExcessType, e.ExcessCeiling, e.VoluntaryExcess, e.PolicyAge, c.StartDate, c.EndDate, c.InceptionDate, e.ExcessRule
			FROM @PolicyExcesses e
			CROSS JOIN @PolicyCoverMultiYear c
			Where e.ID = @ToProcess
			and e.StartDate IS NULL 
		END
		
		SELECT @ToProcess += 1 
		PRINT @ToProcess 
		
	END
	
	PRINT 'EXCESS loop 2'
	
	-- And then delete the original rows
	DELETE @PolicyExcesses
	WHERE StartDate IS NULL
	
	-- Handle policy age
	-- Delete from @PolicyExcess where the policy age isn't null 
	--								and the age (year diff between inception and start + 1) is less than the excess policy age
	DELETE @PolicyExcesses
	WHERE PolicyAge IS NOT NULL
	AND ISNULL(DATEDIFF(YEAR, InceptionDate, StartDate), 0) + 1 < PolicyAge
	
	-- Now we will need to delete the cross applied rows where there is no policy age
	-- that match the policy rows where there is a policy age
	;WITH PolicyAgeRows AS 
	(
		SELECT * 
		FROM @PolicyExcesses
		WHERE PolicyAge IS NOT NULL
	), NoPolicyAgeRows AS
	(
		SELECT * 
		FROM @PolicyExcesses
		WHERE PolicyAge IS NULL
	)
	
	DELETE @PolicyExcesses
	WHERE ID IN
	(
		SELECT ID
		FROM NoPolicyAgeRows n
		WHERE EXISTS
		(
			SELECT *
			FROM PolicyAgeRows p
			WHERE n.ResourceListID = p.ResourceListID
			AND n.StartDate = p.StartDate
			AND n.EndDate = p.EndDate
		)
	)
	
	
	-- Initialise data
	UPDATE @PolicyExcesses
	SET ExcessDeducted = 0, ClaimPaid = 0, ExcessApproachingCeiling = 0
	
	
	-- Get details of any excess rebate that is to be applied
	DECLARE @ExcessRebate TABLE
	(
		Rebate MONEY,
		RebateAllocated MONEY,
		MatterID INT,
		StartDate DATE,
		EndDate DATE
	)
	INSERT @ExcessRebate (Rebate)
	EXEC dbo._C00_1272_Policy_GetPolicyExcessesRebate @MatterID, @CurrentPolicyMatterID
	

	
	-- We cross join with the policy cover data so we have a row for each policy year for reinstatement
	IF @ExcessEachYear = 0
	BEGIN
		INSERT @ExcessRebate (Rebate, RebateAllocated, MatterID, StartDate, EndDate)
		SELECT Rebate, RebateAllocated, MatterID, c.StartDate, c.EndDate
		FROM @ExcessRebate
		CROSS JOIN @PolicyCover c
	END
	ELSE -- If we need to have an excess rebate per year
	BEGIN
		INSERT @ExcessRebate (Rebate, RebateAllocated, MatterID, StartDate, EndDate)
		SELECT Rebate, RebateAllocated, MatterID, c.StartDate, c.EndDate
		FROM @ExcessRebate
		CROSS JOIN @PolicyCoverMultiYear c
	END
	
	-- And then delete the original rows
	DELETE @ExcessRebate
	WHERE StartDate IS NULL
	
	-- Initialise data
	--DECLARE @TotalRebate MONEY
	--SELECT @TotalRebate = SUM(ExcessRebate)
	--FROM #ClaimRows
	--WHERE Paid IS NOT NULL
	--AND (@PolicyType != 42933 OR MatterID = @ClaimParentID OR ParentID = @ClaimParentID) -- For reinstatement we limit to just for this condition group
	
	--UPDATE @ExcessRebate
	--SET RebateAllocated = @TotalRebate
	
	--DECLARE @FirstMatterID INT
	--SELECT @FirstMatterID = MatterID
	--FROM #ClaimRows
	--WHERE ID = 1
	
	--UPDATE @ExcessRebate
	--SET RebateAllocated = 0, MatterID = @FirstMatterID
	
	UPDATE @ExcessRebate
	SET RebateAllocated = 0		
	
	
	-- Get a list of the co-insurance limits that apply
	/*Changed By JL to return CutOffAge (age at which coins becomes applicable)*/ 
	DECLARE @PolicyCoInsurance TABLE
	(
		ID INT IDENTITY,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		PercentageExcess DECIMAL(19,4),
		DateCutOff DATE,
		CutOffAge INT,
		CoInsLinkedTo INT,
		MinThreshold MONEY,
		ClaimPaid MONEY,
		Breed VARCHAR(2000),
		DateTo DATE,
		IsMulti BIT
	)
	INSERT @PolicyCoInsurance (ResourceListID, SECTION, SubSection, PercentageExcess, DateCutOff, CoInsLinkedTo, MinThreshold, Breed)
	EXEC dbo._C00_1272_Policy_GetGroupedPolicyCoInsurance @MatterID, @CurrentPolicyMatterID
	
	Update p 
	SET CutOffAge = DATEDIFF(YEAR,@DateofBirth,p.DateCutOff)
	FROM @PolicyCoInsurance p
	WHERE 1=1 /*avoid "update with no where clause" warnings*/
	
	-- Initialise data
	UPDATE @PolicyCoInsurance
	SET ClaimPaid = 0
	
	-- This next block is now duplicated in the save policy sections so need to keep them in sync
	DECLARE @CoInsAnniversaryCount INT
	SELECT @CoInsAnniversaryCount = COUNT(*) 
	FROM @PolicyCoInsurance
	WHERE CoInsLinkedTo = 52783
	
	IF @CoInsAnniversaryCount > 0
	BEGIN
		
		-- Get the next renewal date
		DECLARE @Renewal DATE
		SELECT @Renewal = DATEADD(YEAR, 1, PolicyStart)
		FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
				
		-- If we have co-insurance rows linked to the policy anniversary then we need to update the Date Cut off to be the 
		-- earliest policy from date that is after the current cut off
		;WITH PolicyDates AS 
		(
			SELECT tdvFrom.ValueDate
			FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
			INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
		), CoInsPolicyDates AS
		(
			SELECT co.*, pd.ValueDate, ROW_NUMBER() OVER(PARTITION BY co.ID ORDER BY pd.ValueDate) as rn 
			FROM @PolicyCoInsurance co
			INNER JOIN PolicyDates pd ON pd.ValueDate >= co.DateCutOff
			WHERE co.CoInsLinkedTo = 52783		
		), EarliesPolicyDates AS 
		(
			SELECT * 
			FROM CoInsPolicyDates
			WHERE rn = 1
		)
		UPDATE ci
		SET ci.DateCutOff = ISNULL(pd.ValueDate, @Renewal) -- try and match with the policy history but roll back the the renewal date in the future
		--SET ci.DateCutOff = pd.ValueDate
		FROM @PolicyCoInsurance ci
		INNER JOIN EarliesPolicyDates pd ON ci.ID = pd.ID
		WHERE Not EXISTS (SELECT * FROM @PolicyCoinsurance c where c.DateCutOff > ci.DateCutOff and pd.ValueDate >= c.DateCutOff) 
		--Where ci.ID = 1   /*Added by JL as surly we only need to update the first valid date? the left join on earliespolicydates makes the above filter redundant*/  
		--INNER JOIN EarliesPolicyDates pd ON ci.ID = pd.ID
	
	END
	
	-- Now, as we may have multiple coinsurance rows for the same section with different cut in dates we need to set a to date and also the IsMulti flag
	;WITH OrderedCoIns AS 
	(
		SELECT *, 
		ROW_NUMBER() OVER(PARTITION BY c.ResourceListID ORDER BY c.DateCutOff) as rn 
		FROM @PolicyCoInsurance c
	)	
	
	UPDATE co
	SET DateTo = DATEADD(DAY, -1, o2.DateCutOff), 
		IsMulti =	CASE 
						WHEN o.rn > 1 THEN 1
						WHEN o2.rn > 1 THEN 1
						ELSE 0
					END
	FROM @PolicyCoInsurance co
	INNER JOIN OrderedCoIns o ON co.ID = o.ID
	LEFT JOIN OrderedCoIns o2 ON o.ResourceListID = o2.ResourceListID AND o.rn + 1 = o2.rn

	DECLARE  @Count	INT
			,@Index	INT
			
	SELECT @Count = COUNT(*), @Index = 0 
	FROM #ClaimRows
	
	IF @Debug > 1
	BEGIN
		SELECT 'Before Big Loop' [#ClaimRows], * FROM #ClaimRows
	END

	PRINT 'Big Loop Start1'
	-- Loop round all the claim rows... paid rows come first and then not paid in order
	WHILE @Index < @Count
	BEGIN
		
		SELECT @Index += 1

		IF @Debug > 1
		BEGIN
			SELECT '#ClaimRows' [Big Loop Start], @Index [@Index], * FROM #ClaimRows
		END
		
		DECLARE @Paid					DATE	= NULL
				,@Total					MONEY	= NULL
				,@DateOfOnset			DATE 	= NULL
				,@TreatmentStart		DATE 	= NULL 
				,@TreatmentEnd			DATE 	= NULL 
				,@ResourceListID		INT 	= NULL
				,@ParentResourceListID	INT 	= NULL
				,@ExcessPaid			MONEY 	= NULL
				,@RebatePaid			MONEY 	= NULL
				,@ClaimMatterID 		INT 	= NULL
				,@ParentID				INT 	= NULL
				,@Claim					MONEY 	= NULL
				,@Settle				MONEY 	= NULL
				,@UserDeductions		MONEY 	= NULL
				,@TotalSettle			MONEY 	= NULL
				,@AfterCoInsCutIn		BIT		= NULL
				,@CoInsPaid				MONEY	= NULL
				,@TableRowID			INT		= NULL
				
		SELECT	@Paid = c.Paid, 
				@Total = c.Settle, 
				@DateOfOnset = DateOfLoss,
				@TreatmentStart = TreatmentStart, 
				@TreatmentEnd = TreatmentEnd, 
				@ResourceListID = ResourceListID,
				@ParentResourceListID = ParentResourceListID,
				@ExcessPaid = ISNULL(Excess, 0),
				@RebatePaid = ISNULL(ExcessRebate, 0),
				@ClaimMatterID = MatterID,
				@ParentID = ParentID,
				@Claim = Claim,
				@Settle = Settle,
				@UserDeductions = UserDeductions,
				@TotalSettle = TotalSettle,
				@AfterCoInsCutIn = AfterCoInsCutIn,
				@CoInsPaid = ISNULL(CoInsurance, 0)
				,@TableRowID = c.TableRowID
		FROM #ClaimRows c
		WHERE c.ID = @Index
		
		-- If paid then we need to deduct the totals paid and excesses from the limits and excesses tables
		IF @Paid IS NOT NULL AND @ClaimMatterID != @MatterID
		BEGIN
		
			/*
				TODO: We might have to check the applied deductions as per the policy rules and write a value into a new adjustment column.
				This could be used to balance a refund / charge against the next payment.  Would need a way of marking this as having been settled or applying the deduction onto the row...
				maybe an adjustment column per deduction?
			
			*/			
				
			-- We keep track of all excess when not reinstatement... when reinstatement we limit to just this condition
			PRINT 'Ergh'
			IF @ClaimMatterID = @ClaimParentID 
				OR 
				@ParentID = @ClaimParentID -- @PolicyType != 42933 Removed by JL as no longer needed
			   -- and exists (SELECT * FROM @PolicyExcesses p where p.ResourceListID = @ResourceListID and p.ExcessRule in (69458 /*Per condition*/,74333 /*Per claim*/)) -- Added by JL, we only care about /*CPS 2017-06-22 this line killed following discussion with James.  Tested on CP LeadID 1020 and PHI*/
			BEGIN
				-- First we update the excess and claim paid columns of any rows that have paid (We are using the settle value here)
				UPDATE @PolicyExcesses
				SET ExcessDeducted += (@ExcessPaid * -1), 
					ClaimPaid += @Total,
					ExcessMatterID = CASE WHEN @ExcessPaid < 0 THEN @ClaimMatterID ELSE ExcessMatterID END, -- If there is an excess deduction then we keep track of the matter it was applied to
					ExcessApproachingCeiling += ((@ExcessPaid * -1) + (@CoInsPaid * -1)) -- Excess ceiling
				WHERE ResourceListID = @ParentResourceListID
				AND @TreatmentStart >= StartDate 
				AND @TreatmentStart < EndDate 
				
				UPDATE @ExcessRebate
				SET RebateAllocated += @RebatePaid,
					MatterID = @ClaimParentID
				WHERE @TreatmentStart >= StartDate 
				AND @TreatmentStart < EndDate
				
			END
			
			-- For paid claims we need to use the actual total column rather than the settled column for the balance
			SELECT	@Total = c.Total
			FROM #ClaimRows c
			WHERE c.ID = @Index
			
			/*JEL 2018-04-15 We need to make  sure a 12 month policy obeys it's application rule i.e. treatment or onset starts the 12 month*/
			IF @PolicyType = 76175 /*12 Month*/ AND 69456 = (SELECT ISNULL(Scheme12MonthCoverStart.ValueInt,0) -- Retrieve the "12 month cover start"  setting
												 FROM MatterDetailValues Scheme12MonthCoverStart WITH (NOLOCK)
											     WHERE Scheme12MonthCoverStart.MatterID = @CurrentPolicyMatterID
		                                         AND Scheme12MonthCoverStart.DetailFieldID =   162632 )
			BEGIN 
			
				-- And then deduct the totals of any rows that have paid
				UPDATE @PolicyLimits
				SET Balance -= @Total
				WHERE (ResourceListID = @ResourceListID OR ResourceListID = @ParentResourceListID)
				AND @DateOfOnset >= StartDate
				AND @DateOfOnset < EndDate
				
				--IF @Debug = 1 
				--BEGIN
		
				--SELECT 'Policy Limitshere' AS Data, * 
				--FROM @PolicyLimits
			
				--END
			
			END 
			ELSE 
			BEGIN 
			
				-- And then deduct the totals of any rows that have paid
				UPDATE @PolicyLimits
				SET Balance -= @Total
				WHERE (ResourceListID = @ResourceListID OR ResourceListID = @ParentResourceListID)
				AND @TreatmentStart >= StartDate
				AND @TreatmentStart < EndDate
			
			END

		END
		ELSE -- If not paid then we need to apply the correct deductions
		BEGIN
			
			-- Initialise total
			UPDATE #ClaimRows
			SET Total = @Total
			WHERE ID = @Index
			
			-- Move on straight away if this row is zero
			IF @Total = 0
			BEGIN
				CONTINUE
			END
			
			
			
			/* 
			No Policy cover 
			*/
			IF NOT EXISTS (	SELECT * 
							FROM @PolicyCover c)
			BEGIN
			
				UPDATE #ClaimRows
				SET OutsidePolicyCover = @Total * -1,
					Total = 0
				WHERE ID = @Index
				
				CONTINUE
			
			END



			
			/*
				Non financial limits
			*/
			-- First we need to check to make sure we have not used up all the policy limit counts
			DECLARE @AllowedCount INT = NULL,
					--@AllowedRemaining INT = NULL - See comment from 2015-10-22
					@LimitTypeID INT = NULL
					
			SELECT	@AllowedCount = AllowedCount,
					--@AllowedRemaining = AllowedRemaining - See comment from 2015-10-22
					@LimitTypeID = LimitTypeID
			FROM @PolicyLimits
			WHERE (ResourceListID = @ResourceListID)
			AND @TreatmentStart >= StartDate 
			AND @TreatmentStart < EndDate
			
			IF @AllowedCount > 0 AND @LimitTypeID = 72058 -- Age limit
			BEGIN
			
				IF DATEADD(YEAR, @AllowedCount, @DateofBirth) <= @DateOfOnset -- Pet too old
				BEGIN
				
					-- Update the claim row
					UPDATE #ClaimRows
					SET Limit = @Total * -1,
						Total = 0
					WHERE ID = @Index	
					
					-- Update the running total of this row
					SELECT @Total = 0
				
				END
					
			END
			
			
			IF @Total = 0
			BEGIN
				CONTINUE
			END
			
			
		
			/*
			Excess
			*/
			DECLARE @Excess MONEY = NULL, 
					@PercentageExcess DECIMAL(19,4) = NULL,
					@VoluntaryExcess MONEY = NULL,
					@ExcessDeducted MONEY = NULL,
					@ClaimPaid MONEY = NULL,
					@ExcessCalc DECIMAL (17,2) = NULL,
					@RebateCalc MONEY = NULL,
					@VoluntaryExcessApply MONEY = NULL,
					@ExcessMatterID INT = NULL,
					@MinThreshold MONEY = NULL,
					@ExcessCeiling MONEY = NULL,
					@ExcessApproachingCeiling MONEY = NULL,
					@ExcessRule INT = NULL
		
					
			SELECT @ParentResourceListID = ParentResourceListID
			FROM #ClaimRows	
			WHERE ID = @Index
			
			IF @Debug > 1
			BEGIN
				SELECT '@PolicyExcesses'	[Section]
						,@Index				[@Index]
						,@TreatmentStart	[@TreatmentStart]
						,@PolicyType		[@PolicyType]
						,* 
				FROM @PolicyExcesses

				SELECT	'SelectedRow'		[Section]
						,@Index				[@Index]
						,@TreatmentStart	[@TreatmentStart]
						,@PolicyType		[@PolicyType]
						,*
				FROM @PolicyExcesses
				--WHERE ResourceListID = @ParentResourceListID
				WHERE ResourceListID IN (@ParentResourceListID,@ResourceListID) /*CPS 2017-10-19 #44277*/
				/*
				CPS 2019-03-15 #54450 
				For 12 month policies, we need to capture the excess for each year in case we have multiple 12-month treatments to consider, 
				but when working out how much has been charged already, we need to make sure that we don't start charging excess once we cross a year boundary.
				*/
				AND (
						(
							@TreatmentStart >= StartDate 
						AND @TreatmentStart < EndDate
						AND @PolicyType <> 76175 /*12 Month*/
						)
						OR
						(	
							@TreatmentStart >= StartDate
						AND @PolicyType = 76175 /*12 Month*/ /*CPS 2019-03-25 changed from <> to = for #55947*/
						)
					)
				ORDER BY CASE WHEN ResourceListID = @ResourceListID THEN 0 ELSE 1 END /*CPS 2017-07-07*/
						,StartDate -- CPS 2019-04-15 #55779
						,ExcessDeducted DESC

			END
			
			--IF @ExcessEachYear = 0
			--BEGIN 
				-- Try and find the matching excess for this row's resource list
				SELECT	TOP 1
						@Excess = Excess, 
						@VoluntaryExcess = VoluntaryExcess,
						@PercentageExcess = PercentageExcess, 
						@ExcessDeducted = ExcessDeducted, /*Excess Already paid on this section*/
						@ClaimPaid = ClaimPaid + @Total,
						@ExcessMatterID = ExcessMatterID,
						@ExcessCeiling = ExcessCeiling,
						@ExcessApproachingCeiling = ExcessApproachingCeiling,
						@ExcessRule = ExcessRule
					
				FROM @PolicyExcesses
				--WHERE ResourceListID = @ParentResourceListID
				WHERE ResourceListID IN (@ParentResourceListID,@ResourceListID) /*CPS 2017-10-19 #44277*/
				/*
				CPS 2019-03-15 #54450 
				For 12 month policies, we need to capture the excess for each year in case we have multiple 12-month treatments to consider, 
				but when working out how much has been charged already, we need to make sure that we don't start charging excess once we cross a year boundary.
				*/
				AND (
						(
							@TreatmentStart >= StartDate 
						AND @TreatmentStart < EndDate
						AND @PolicyType <> 76175 /*12 Month*/
						)
						OR
						(	
							@TreatmentStart >= StartDate
						AND @PolicyType = 76175 /*12 Month*/ /*CPS 2019-03-25 changed from <> to = for #55947*/
						)
					)
				ORDER BY CASE WHEN ResourceListID = @ResourceListID THEN 0 ELSE 1 END /*CPS 2017-07-07*/
						,StartDate -- CPS 2019-04-15 #55779
						,ExcessDeducted DESC

			--END
			--ELSE /* If we have Excess per year then we want to make sure the excess is addative for each year split*/ 
			--BEGIN 
			
			--	-- Try and find the matching excess for this row's resource list
			--	SELECT	@Excess = Excess, 
			--			@VoluntaryExcess = VoluntaryExcess,
			--			@PercentageExcess = PercentageExcess, 
			--			@ExcessDeducted = ExcessDeducted, /*Excess Already paid on this section*/
			--			@ClaimPaid = ClaimPaid + @Total,
			--			@ExcessMatterID = ExcessMatterID,
			--			@ExcessCeiling = ExcessCeiling,
			--			@ExcessApproachingCeiling = ExcessApproachingCeiling,
			--			@ExcessRule = ExcessRule
					
			--	FROM @PolicyExcesses
			--	WHERE ResourceListID = @ParentResourceListID
			--	AND @TreatmentStart >= StartDate 
			--	AND @TreatmentStart < EndDate
			
			--	-- Add any later excess rows
			--	SELECT	@Excess = @Excess + Excess, 
			--			@VoluntaryExcess = @VoluntaryExcess +  VoluntaryExcess,
			--			@PercentageExcess = @PercentageExcess + PercentageExcess, 
			--			@ExcessDeducted = @ExcessDeducted + ExcessDeducted, /*Excess Already paid on this section*/
			--			@ClaimPaid = @ClaimPaid +  (ClaimPaid + @Total)					
			--	FROM @PolicyExcesses
			--	WHERE ResourceListID = @ParentResourceListID
			--	AND @TreatmentStart < StartDate 
			--	AND @TreatmentEnd < EndDate
				
				
				
			--END
			
			-- And we also need the min threshold amount for co-insurance here for an extra check
			SELECT	@MinThreshold = MinThreshold
			FROM @PolicyCoInsurance c
			WHERE ResourceListID = @ParentResourceListID
			-- DateCutOff has been manipulated for linked to policy anniversary so can just compare with threatment start or date of loss and optionaly filter on date to when multiple rows
			AND (
				c.IsMulti = 0
				OR
				(c.CoInsLinkedTo = 45241 AND @DateOfOnset >= c.DateCutOff AND (c.DateTo IS NULL OR @DateOfOnset <= c.DateTo))
				OR 
				(c.CoInsLinkedTo != 45241 AND @TreatmentStart >= c.DateCutOff AND (c.DateTo IS NULL OR @TreatmentStart <= c.DateTo))
				)
			
			DECLARE @ApplyExcess BIT = 0
			
			-- For Sainsury's we apply co-insurance instead of excess for claims where the total settle amount is above the threshold
			-- If there is no threshold or the total claim amount is not above the threshold carry on as normal
			IF @MinThreshold IS NULL OR @MinThreshold = 0 OR @TotalSettle <= @MinThreshold
			BEGIN
				
				IF @Debug > 1
				BEGIN
					SELECT 'CoIns Section' [Section],@Excess [@Excess], @PercentageExcess [@PercentageExcess], @ExcessDeducted [@ExcessDeducted], @ExcessRule [@ExcessRule], @ParentID [@ParentID], @ResourceListID [@ResourceListID], @ParentResourceListID [@ParentResourceListID]
				END
				-- If we need to apply an excess
				-- Either min contribution or 
				--(no excess applied yet, we are on the same matter as where excess already applied or we are on the parent row or we still have excess to pay (under excess claims 7.5))
				IF (@Excess > 0 AND @PercentageExcess > 0) OR (@Excess > 0 AND 
															  (@ExcessDeducted = 0 OR @ParentID IS NULL OR @Excess > @ExcessDeducted)
															  OR (@ExcessRule = 74333))
				BEGIN
				
					-- If we have both an excess and a percentage excess defined then we are on a miniumum contribution excess
					IF @Excess > 0 AND @PercentageExcess > 0
					BEGIN

						IF @Debug > 1
						BEGIN
							SELECT	 'Percentage Excess Section' [Section]
									,@Index			[@Index]
									,@ExcessCalc	[@ExcessCalc]
						END
					
						-- Initialise at the percentage excess
						SELECT @ExcessCalc = (@ClaimPaid * @PercentageExcess) / 100, @VoluntaryExcessApply = NULL
						IF @ExcessCalc < @Excess  -- ... but roll back to the standard flat rate excess if it is greater
						BEGIN
							SELECT @ExcessCalc = @Excess, @VoluntaryExcessApply = @VoluntaryExcess
						END
							
						-- Do we need to apply an excess
						IF @ExcessDeducted < @ExcessCalc
						BEGIN
							-- Deduct the excess already paid
							SELECT @ExcessCalc -= @ExcessDeducted						
							SELECT @ApplyExcess = 1
							
						END
					END 
					ELSE
					BEGIN

						IF @Debug > 1
						BEGIN
							SELECT	 'Full Excess Section Start' [Section]
									,@Index			[@Index]
									,@ExcessCalc	[@ExcessCalc]
						END

						-- Initialise on the full excess
						SELECT @ExcessCalc = @Excess, @VoluntaryExcessApply = @VoluntaryExcess
						
						IF @ExcessRule = 74333 /*Always apply excess if the section is per claim*/
						BEGIN 
							SELECT @ApplyExcess = 1 
						END
						ELSE 
						BEGIN
						
							IF @ExcessDeducted = 0
							BEGIN
								SELECT @ApplyExcess = 1
							END
							ELSE --IF @ExcessMatterID = @ClaimMatterID
							BEGIN
			

								-- Do we need to apply an excess
								IF @ExcessDeducted < @ExcessCalc
								BEGIN
									-- Deduct the excess already paid
									SELECT @ExcessCalc -= @ExcessDeducted						
									SELECT @ApplyExcess = 1

								END
							END
						END

						IF @Debug > 1
						BEGIN
							SELECT	 'Full Excess Section End' [Section]
									,@Index			[@Index]
									,@ExcessCalc	[@ExcessCalc]
						END
					END

					/*
					Excess Rebate
					*/
					
					DECLARE @Rebate MONEY = NULL,
							@RebateAllocated MONEY = NULL
					SELECT @Rebate = Rebate, @RebateAllocated = RebateAllocated
					FROM @ExcessRebate
					WHERE @TreatmentStart >= StartDate 
					AND @TreatmentStart < EndDate
					
					
					IF @Rebate > 0 AND @RebateAllocated < @Rebate
					BEGIN
					
						-- Only rebate up to the amount of excess deducted
						DECLARE @ExcessForRow MONEY = NULL
						SELECT @ExcessForRow = @ExcessCalc
					
						SELECT @RebateCalc = @Rebate - @RebateAllocated
						IF @RebateCalc > ISNULL(@ExcessForRow, 0)
						BEGIN
							SELECT @RebateCalc = ISNULL(@ExcessForRow, 0)					
						END
					
					END
					
					-- I moved this from inside the IF @ApplyExcess = 1 so I could use the final value in the excess ceiling stuff below
					-- Handle the excess remaining being higher than the claim row
					IF @ExcessCalc - ISNULL(@RebateCalc, 0) > @Total
					BEGIN
						SELECT @ExcessCalc = @Total	+ ISNULL(@RebateCalc, 0)				
					END
					
					-- Excess Ceiling
					-- I don't think this is needed here as the excess is £75 vs a £750 ceiling but I had already dadded this before I realised.  No harm done.
					IF @ExcessCeiling > 0
					BEGIN
					
						-- Are we at or over the ceiling now
						IF @ExcessApproachingCeiling >= @ExcessCeiling
						BEGIN
						
							-- Prevent an excess being applied
							SELECT @ExcessCalc = 0, @ApplyExcess = 0
						
						END
						ELSE
						BEGIN						
							-- If we are going to go over the ceiling then reduce appropriately
							IF @ExcessApproachingCeiling + @ExcessCalc > @ExcessCeiling
							BEGIN
								
								SELECT @ExcessCalc = (@ExcessCeiling - @ExcessApproachingCeiling)
								
							END
						
						END
					
					END
									
					IF @ApplyExcess = 1
					BEGIN						

						IF @Debug > 1
						BEGIN
							SELECT	 'Apply Excess Section' [Section]
									,@Index					[@Index]
									,@ExcessCalc			[@ExcessCalc]
									,@ExcessDeducted		[@ExcessDeducted]
									,@VoluntaryExcessApply	[@VoluntaryExcessApply]
						END
												
						-- Voluntary excess
						IF @VoluntaryExcessApply > 0
						BEGIN
						
							-- Update @ExcessDeducted to include this row
							SELECT @ExcessDeducted += @ExcessCalc
						
							IF @ExcessDeducted > (@Excess - @VoluntaryExcess)
							BEGIN
								SELECT @VoluntaryExcessApply = @ExcessDeducted - (@Excess - @VoluntaryExcess)
								IF @VoluntaryExcessApply > @ExcessCalc
								BEGIN
									SELECT @VoluntaryExcessApply = @ExcessCalc
								END
							END
							ELSE
							BEGIN
								SELECT @VoluntaryExcessApply = NULL
							END
							
							-- Not sure if this is needed now but no harm
							IF @VoluntaryExcessApply <= 0
							BEGIN
								SELECT @VoluntaryExcessApply = NULL
							END
							
						END

						-- And now the rebate stuff
						IF @RebateCalc > 0
						BEGIN
							-- Write back to the excess table
							UPDATE @ExcessRebate
							SET RebateAllocated += @RebateCalc,
								MatterID = @ClaimParentID -- Keep track of the parent
							WHERE @TreatmentStart >= StartDate 
							AND @TreatmentStart < EndDate
							
							-- Update the running total of this row (we add as excess rebate is positive)
							SELECT @Total += @RebateCalc
							
							-- And now the claim row
							UPDATE #ClaimRows
							SET ExcessRebate = @RebateCalc,
								Total = @Total
							WHERE ID = @Index
						END

					END
				
				END
				
				IF @Total = 0
				BEGIN
					CONTINUE
				END
				
			END -- End check for co-insurance max threshold 


			IF @Debug > 1
			BEGIN
				SELECT	 'Excess Section End'	[Section]
						,@Index					[@Index]
						,@ExcessCalc			[@ExcessCalc]
						,@ExcessDeducted		[@ExcessDeducted]
			END	
			
			/*
			Co-Insurance
			*/

			-- Try and find the matching co-insurance for this row's resource list
			DECLARE  @DateCutOff				DATE	= NULL
					,@CoInsLinkedTo				INT		= NULL
					,@CoInsuranceCalc			MONEY	= 0.00
					,@ExcessPaidForThisChain	MONEY
					,@TotalClaimedForThisChain	MONEY
					
			SELECT	TOP 1
					@PercentageExcess = PercentageExcess, 
					@DateCutOff = DateCutOff, 
					@CoInsLinkedTo = CoInsLinkedTo,
					@MinThreshold = MinThreshold,
					@ClaimPaid = ClaimPaid + @Total
			FROM @PolicyCoInsurance c
			WHERE ResourceListID IN ( @ParentResourceListID, @ResourceListID ) /*CPS 2017-07-07*/
			-- DateCutOff has been manipulated for linked to policy anniversary so can just compare with threatment start or date of loss and optionaly filter on date to when multiple rows
			AND (
				c.IsMulti = 0
				OR
				(c.CoInsLinkedTo = 45241 /*Date of Onset*/ AND @DateOfOnset >= c.DateCutOff AND (c.DateTo IS NULL OR @DateOfOnset <= c.DateTo))
				OR 
				(c.CoInsLinkedTo != 45241 /*Date of Onset*/ AND @TreatmentStart >= c.DateCutOff AND (c.DateTo IS NULL OR @TreatmentStart <= c.DateTo))
				)
			ORDER BY CASE WHEN ResourceListID = @ResourceListID THEN 0 ELSE 1 END /*CPS 2017-07-07*/

			SELECT	 @ExcessPaidForThisChain	= SUM(ISNULL(cr.Excess,0.00)) + SUM(ISNULL(cr.CoInsurance,0.00))
					,@TotalClaimedForThisChain	= SUM(cr.Claim + ISNULL(cr.UserDeductions,0.00)) /*CPS 2017-07-17*/
			FROM #ClaimRows cr 
			WHERE (
					cr.MatterID IN ( @MatterID,@ParentID )
					OR
					cr.ParentID IN ( @MatterID,@ParentID )
				  )
			
					PRINT 'ex1'
					PRINT @ExcessCalc 
					PRINT @ApplyExcess 
					PRINT @ExcessPaidForThisChain

			--END

			IF @Debug > 1
			BEGIN
				SELECT	 'Update Excess Section Start'	[Section]
						,@Index					[@Index]
						,@ExcessCalc			[@ExcessCalc]
						,@ExcessDeducted		[@ExcessDeducted]
						,@ExcessPaidForThisChain[@ExcessPaidForThisChain]
			END	
			-- ANW 16-MAY-18 Trello 372 - @ExcessPaidForThisChain needs comparing to the Policy amount (@Excess) not the calculated amount for this claim item (@ExcessCalc)
			IF ISNULL(ABS(@ExcessPaidForThisChain),0.00) < ISNULL(@Excess,0.00) OR @ApplyExcess = 1 
			BEGIN

				-- Write back to the excess table
				UPDATE @PolicyExcesses
				SET ExcessDeducted += ISNULL(@ExcessCalc,0.00),
					ClaimPaid += ISNULL(@Total,0.00),
					ExcessMatterID = @ClaimMatterID, -- If there is an excess deduction then we keep track of the matter it was applied to
					ExcessApproachingCeiling += ISNULL(@ExcessCalc,0.00) -- And keep track of the running total for the excess ceiling calcs
				--WHERE ResourceListID = @ResourceListID--@ParentResourceListID /*CPS 2017-10-19*/
				--AND @TreatmentStart >= StartDate 
				--AND @TreatmentStart < EndDate
				--PRINT CONVERT(VARCHAR,@@RowCount) + ' Excess Rows Updated'
				WHERE ResourceListID IN (@ParentResourceListID,@ResourceListID) /*CPS 2017-10-19 #44277*/
				/*
				CPS 2019-04-15 #55779
				Brought this section down from the start of the excess loop where it was changed as part of #54450
				*/
				AND (
						(
							@TreatmentStart >= StartDate 
						AND @TreatmentStart < EndDate
						AND @PolicyType <> 76175 /*12 Month*/
						)
						OR
						(	
							@TreatmentStart >= StartDate
						AND @PolicyType = 76175 /*12 Month*/ /*CPS 2019-03-25 changed from <> to = for #55947*/
						)
					)

				-- Update the running total of this row (we subtract as excess calc is positive)
				SELECT @Total -= ISNULL(@ExcessCalc,0.00)
				
				PRINT @Total
				
				-- And now the claim row
				UPDATE #ClaimRows
				SET Excess = ISNULL(@ExcessCalc,0.00) * -1,
					VoluntaryExcess = ISNULL(@VoluntaryExcessApply,0.00) * -1,
					Total = ISNULL(@Total,0.00)
				WHERE ID = @Index
				
				-- We also keep track of this in a local variable for co-insurance below
				SELECT @ExcessApproachingCeiling += @ExcessCalc

			END
				
			IF @Total = 0
			BEGIN
				CONTINUE
			END

	
	/*CPS 2017-06-15 Moved the whole limits section to here*/
			/*PHI need the ability to have limits on conditions as well as treatments, so new table on the shcemes lead type with conditions mapped against 
			policy parent sections (by the luli) with a limit against it. (sigh, AQ doesnt support a RL table with two RLs on it so we need to go manual. 
			find the RL from the limits that shares the parent section luli with the conditions table)
			If one of those conditions is used then update all limits where the condition limit is LOWER than the normal limit.*/    	
			
			PRINT 'Condition Start'
			DECLARE @ConditionID INT =    dbo.fngetsimpledvasINT(144504,@MatterID) /*Ailment*/
				
			/*sigh, AQ doesnt support a RL table with two RLs on it so we need to go manual. find the RL from the limits that shares the parent section luli with the conditions table.*/   	

			--------DECLARE @ConditionTable TABLE (ParentSection VARCHAR(100), Limit DECIMAL (18,2), PolicySectionResourceListID INT, Balance INT)
			--------INSERT INTO @ConditionTable (ParentSection,Limit,PolicySectionResourceListID,Balance) 
			--------/*CPS 2017-06-12 Use new config*/
			--------SELECT liSection.ItemValue, tdvLimit.ValueMoney, tdvSection.ResourceListID, tdvLimit.ValueMoney
			--------FROM TableRows tr WITH ( NOLOCK )
			--------INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 176984 /*Policy Sections*/
			--------INNER JOIN ResourceListDetailValues rdvSection WITH ( NOLOCK ) on rdvSection.ResourceListID = tdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
			--------INNER JOIN LookupListItems liSection WITH ( NOLOCK ) on liSection.LookupListItemID = rdvSection.ValueInt
			--------INNER JOIN TableDetailValues tdvLimit WITH ( NOLOCK ) on tdvLimit.TableRowID = tr.TableRowID AND tdvLimit.DetailFieldID = 176985 /*Limit*/
			--------WHERE tr.MatterID = @CurrentPolicyMatterID
			--------AND tr.DetailFieldID = 176986 /*Condition Limits*/
			
			----------SELECT l.ItemValue, tdv1.ValueMoney FROM TableRows tr WITH (NOLOCK) 
			----------INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 176984 /*Condition RL*/ 
			----------INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 176985 /*Limit*/ 
			----------INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID  and tdv2.DetailFieldID = 176983 /*Parent Section*/
			----------INNER JOIN dbo.LookupListItems l WITH (NOLOCK) on l.LookupListItemID = tdv2.ValueInt 
			----------WHERE tdv.ResourceListID = @ConditionID 
			----------and tr.MatterID = @CurrentPolicyMatterID
			
			--------IF EXISTS (SELECT top 1 * FROM @ConditionTable)
			--------BEGIN
			--------	/*All of the condition limits apply per policy year, ensure that this is obeyed by checking previous claims for this lead with the same condition*/  
				
			--------	DECLARE @LimitUsed DECIMAL (18,2)
			--------	SELECT @LimitUsed = ISNULL(SUM(sett.ValueMoney),0.00)  
			--------	FROM MatterDetailValues cond WITH (NOLOCK) 
			--------	INNER JOIN MatterDetailValues sett WITH (NOLOCK) on sett.MatterID = cond.MatterID 
			--------	INNER JOIN MatterDetailValues st WITH (NOLOCK) on st.MatterID = cond.MatterID
			--------	INNER JOIN @PolicyCover p on st.ValueDate BETWEEN p.StartDate and p.EndDate
			--------	Where cond.LeadID = @LeadID 
			--------	and cond.MatterID <> @MatterID 
			--------	and cond.DetailFieldID = 144504 /*Ailment*/
			--------	and cond.ValueInt = @ConditionID 
			--------	and sett.DetailFieldID = 148375-- total
			--------	and st.DetailFieldID = 144366 -- Treatment start
				
			--------	SELECT @LimitUsed += cr.Total /*CPS 2017-08-07*/
			--------	FROM #ClaimRows cr
			--------	WHERE cr.ParentResourceListID = @ParentResourceListID
			--------	AND cr.ID < @Index
			--------	AND cr.CaseID = @CaseID

			--------	UPDATE ct
			--------	SET Balance = ISNULL(Limit,0.00) - ISNULL(@LimitUsed,0.00)
			--------	FROM @ConditionTable ct 
			--------	WHERE ct.PolicySectionResourceListID = @ParentResourceListID
				
			--------	SELECT @RowCount = @@ROWCOUNT

			--------	--Update p 
			--------	--Set SumInsured = (c.Limit - @LimitUsed)
			--------	--FROM @PolicyLimits p 
			--------	--INNER JOIN @ConditionTable c on c.ParentSection = p.Section 
			--------	--Where c.Limit < SumInsured 
					
			--------	-- CPS 2017-06-29 this section appears to be preventing the Policy Limits from taking effect.
			--------	-- Initialise the balance 
			--------	--UPDATE @PolicyLimits
			--------	--SET Balance = SumInsured
			--------	--AllowedRemaining = AllowedCount - See comment from 2015-10-22
				
			--------	IF @Debug = 2
			--------	BEGIN
			--------		SELECT '@ConditionTable' [Section], @Index [@Index], @ConditionID [@ConditionID], @ParentResourceListID [@ParentResourceListID], @LimitUsed [@LimitUsed], @RowCount [@RowCount], ct.*
			--------		FROM @ConditionTable ct
					
			--------	END
				
			--------END
			
						/*sigh, AQ doesnt support a RL table with two RLs on it so we need to go manual. find the RL from the limits that shares the parent section luli with the conditions table.*/   	







/*
Limits usedto be
*/








			PRINT @Total 
			Print '54'
			PRINT @TotalSettle 
			PRINT @MinThreshold
			PRINT @ExcessCeiling
			PRINT @TotalClaimedForThisChain
			PRINT @ExcessPaidForThisChain

				-- Do we need to apply an excess
			-- We have an excess defined and 
			-- We are past the Co-Insurance Cut-in
			--(we are a new claim and the total settle amount is above the threshold) OR (we are a continuation and the cumulative claim amount has pushed over the threshold)
			-- This is for Sainsbury's but for everyone else MinThreshold will be 0 so it will work for everyone
			IF @PercentageExcess > 0 AND @AfterCoInsCutIn = 1 AND ((@ParentID IS NULL AND @TotalSettle > @MinThreshold) OR (@ParentID IS NOT NULL AND @ClaimPaid > @MinThreshold))
			BEGIN
				
				IF @MinThreshold > 0
				BEGIN
					IF @ParentID IS NULL -- If we have a minimum threshold we apply full percentage to new claims or for continuation on the amount above the threshold
					BEGIN
						SELECT @CoInsuranceCalc = (@Total * @PercentageExcess) / 100
					END
					ELSE
					BEGIN
						-- If we are just over the threshold then @ClaimPaid - @MinThreshold will be the amount over the threshold
						-- For the next row the amount for that row will be lower so that will be used instead
						SELECT @CoInsuranceCalc = (dbo.fnMinOf(@Total, @ClaimPaid - @MinThreshold) * @PercentageExcess) / 100				 
					END
				END
				ELSE 
				BEGIN
					-- If no threshold then we apply the full percentage
					SELECT @CoInsuranceCalc = ( (@Total * @PercentageExcess) / 100 ) --+ @ExcessPaidForThisChain
				END
				
				IF @Debug = 2
				BEGIN
					SELECT	 @TotalClaimedForThisChain	[@TotalClaimedForThisChain]
							,@CoInsuranceCalc			[@CoInsuranceCalc]
							,@PercentageExcess			[@PercentageExcess]
							,@ExcessPaidForThisChain	[@ExcessPaidForThisChain]
							,@MinThreshold				[@MinThreshold]
							,@TableRowID				[@TableRowID]
				END
				
				--SELECT @ExcessApproachingCeiling AS ExcessApproachingCeiling
				
				-- Excess Ceiling
				IF @ExcessCeiling > 0
				BEGIN
				
					-- Are we at or over the ceiling now
					IF @ExcessApproachingCeiling >= @ExcessCeiling
					BEGIN
					
						-- Prevent an excess being applied
						SELECT @CoInsuranceCalc = 0
					
					END
					ELSE
					BEGIN						
						-- If we are going to go over the ceiling then reduce appropriately
						IF @ExcessApproachingCeiling + @CoInsuranceCalc > @ExcessCeiling
						BEGIN
							
							SELECT @CoInsuranceCalc = (@ExcessCeiling - @ExcessApproachingCeiling)
							
						END
					
					END
				
				END

			END
			
			/*
			CPS 2017-06-22  DECISION
			Cardif apply EITHER Coinsurance OR Excess.
			Need to calculate both, and then decide which to apply so all actual updates moved down to here.			
			*/
			IF @Debug = 2
			BEGIN
				SELECT	 'Decision'			[Section]
						,@CoInsuranceCalc	[@CoInsuranceCalc]
						,@ExcessCalc		[@ExcessCalc]
						,@Index				[@Index]
						,@TableRowID		[@TableRowID]
						,@ExcessPaidForThisChain	[@ExcessPaidForThisChain]
			END
			
			--IF ISNULL(@CoInsuranceCalc,0.00) > ISNULL(@ExcessCalc,0.00)
			--BEGIN

				-- Update the running total of this row (we subtract as excess calc is positive)
				SELECT @Total -= @CoInsuranceCalc

				SELECT @Total = dbo.fnMaxOf(@Total,0.00)

				-- Write back to the claim row
				UPDATE #ClaimRows
				SET CoInsurance = @CoInsuranceCalc * -1,
					Total = @Total
				WHERE ID = @Index

				-- Write back to the excess table for the excess ceiling
				UPDATE @PolicyExcesses
				SET ExcessApproachingCeiling += @CoInsuranceCalc -- And keep track of the running total for the excess ceiling calcs
				WHERE ResourceListID = @ParentResourceListID
				AND @TreatmentStart >= StartDate 
				AND @TreatmentStart < EndDate

				-- Write back to the co-insurance table
				UPDATE c
				SET ClaimPaid += ISNULL(@Total,0.00)
				FROM @PolicyCoInsurance c
				WHERE ResourceListID = @ParentResourceListID
				-- DateCutOff has been manipulated for linked to policy anniversary so can just compare with threatment start or date of loss and optionaly filter on date to when multiple rows
				AND (
					c.IsMulti = 0
					OR
					(c.CoInsLinkedTo = 45241 AND @DateOfOnset >= c.DateCutOff AND (c.DateTo IS NULL OR @DateOfOnset <= c.DateTo))
					OR 
					(c.CoInsLinkedTo != 45241 AND @TreatmentStart >= c.DateCutOff AND (c.DateTo IS NULL OR @TreatmentStart <= c.DateTo))
					)
		
		
						DECLARE @ConditionTable TABLE (ParentSection VARCHAR(100), Limit DECIMAL (18,2)) 
			INSERT INTO @ConditionTable (ParentSection,Limit) 
			SELECT l.ItemValue, tdv1.ValueMoney FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 176984 /*Condition RL*/ 
			INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 176985 /*Limit*/ 
			INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID  and tdv2.DetailFieldID = 176983 /*Parent Section*/
			INNER JOIN dbo.LookupListItems l WITH (NOLOCK) on l.LookupListItemID = tdv2.ValueInt 
			WHERE tdv.ResourceListID = @ConditionID 
			and tr.MatterID = @CurrentPolicyMatterID 
			
			IF EXISTS (SELECT top 1 * FROM @ConditionTable)
			BEGIN
				/*All of the condition limits apply per policy year, ensure that this is obeyed by checking previous claims for this lead with the same condition*/  
				
				DECLARE @LimitUsed DECIMAL (18,2) 
				SELECT @LimitUsed = ISNULL(SUM(sett.ValueMoney),0.00)  FROM MatterDetailValues cond WITH (NOLOCK) 
				INNER JOIN dbo.MatterDetailValues sett WITH (NOLOCK) on sett.MatterID = cond.MatterID 
				INNER JOIN dbo.MatterDetailValues st WITH (NOLOCK) on st.MatterID = cond.MatterID
				INNER JOIN @PolicyCover p on st.ValueDate BETWEEN p.StartDate and p.EndDate
				Where cond.LeadID = @LeadID 
				and cond.MatterID <> @MatterID 
				and cond.DetailFieldID = 144504-- condition
				and cond.ValueInt = @ConditionID 
				and sett.DetailFieldID = 148375-- total
				and st.DetailFieldID = 144366 -- Treatment start
			
				Update p 
				Set SumInsured = (ISNULL(c.Limit,0) - ISNULL(@LimitUsed,0)) 
				FROM @PolicyLimits p 
				INNER JOIN @ConditionTable c on c.ParentSection = p.Section 
				Where c.Limit < SumInsured 


				PRINT 'Condition End'
								
				-- Initialise the balance 
				UPDATE @PolicyLimits
				SET Balance = SumInsured
				--AllowedRemaining = AllowedCount - See comment from 2015-10-22
			
			END
		
			IF @Debug = 2
			BEGIN
				SELECT '#ClaimRows' [Section], cr.*
				FROM #ClaimRows cr 
				WHERE cr.ID = @Index
				
				SELECT '@PolicyLimits' [Section], * 
				FROM @PolicyLimits pl
 			END
		
			----------/*
			----------Limit Reached
			----------*/
			
			------------ If we have settled on a figure that is greater than the lowest remaining balance of the policy section or parent section then...
			----------DECLARE  @LowestBalanceRowID	INT = NULL
			----------		,@LowestBalance			MONEY = NULL
			----------		,@LowestRLID			INT = NULL
			----------		,@ConditionBalance		MONEY = 0.00
			----------		,@PolicySectionBalance	MONEY = 0.00
			------------ Find the ID of the row we are interested in
			----------SELECT	@LowestBalanceRowID =	CASE 
			----------									WHEN p.Balance < ISNULL(p2.Balance, 0) AND p.Balance IS NOT NULL THEN p.ID 
			----------									WHEN p2.Balance IS NOT NULL THEN p2.ID
			----------									ELSE NULL 
			----------								 END
			----------		,@ConditionBalance = ct.Balance
			----------FROM #ClaimRows c
			------------LEFT JOIN @PolicyLimits p ON c.ResourceListID = p.ResourceListID
			------------LEFT JOIN @PolicyLimits p2 ON c.ParentResourceListID = p2.ResourceListID
			------------FIX for Zen#23991
			----------LEFT JOIN @PolicyLimits p ON c.ResourceListID = p.ResourceListID and c.TreatmentStart >= p.StartDate and c.TreatmentStart < p.EndDate
			----------LEFT JOIN @PolicyLimits p2 ON c.ParentResourceListID = p2.ResourceListID and c.TreatmentStart >= p2.StartDate and c.TreatmentStart < p2.EndDate
			----------OUTER APPLY @ConditionTable ct
			----------WHERE c.ID = @Index

			------------ Now get the data from that row
			----------SELECT	 @LowestRLID			= ResourceListID
			----------		--,@LowestBalance			= Balance
			----------		,@PolicySectionBalance	= Balance
			----------FROM @PolicyLimits
			----------WHERE ID = @LowestBalanceRowID
			
			----------/*CPS 2017-07-02 Identify which balance should come into effect*/
			----------SELECT @LowestBalance = CASE WHEN @PolicySectionBalance <= @ConditionBalance or @ConditionBalance is NULL 
			----------							 THEN @PolicySectionBalance 
			----------							 ELSE @ConditionBalance 
			----------						END
			
			----------IF @Debug = 2
			----------BEGIN
			----------	 SELECT CASE WHEN ISNULL(@PolicySectionBalance,0.00) <= ISNULL(@ConditionBalance,0.00) THEN @PolicySectionBalance ELSE @ConditionBalance END [LowestBalance Calc]
			----------	,@LowestBalance			[@LowestBalance]
			----------	,@ConditionBalance		[@ConditionBalance]
			----------	,@PolicySectionBalance	[@PolicySectionBalance]
				
			----------	SELECT '#ClaimRows' [#ClaimRows], * 
			----------	FROM #ClaimRows
				
			----------END
			
			------------ Handle SAP claims calculated against a policy limit that is too high
			----------IF @LowestBalance < 0
			----------BEGIN
			----------	SELECT @LowestBalance = 0
			----------END
			
			----------DECLARE @Limit MONEY = NULL			
			----------IF @Total > @LowestBalance
			----------BEGIN
			
			----------	SELECT @Limit = @Total - @LowestBalance	
				
			----------	-- Update the running total of this row (we add as limit reached is positive)
			----------	SELECT @Total -= @Limit
				
			----------	-- Update the claim row
			----------	UPDATE #ClaimRows
			----------	SET Limit = @Limit * -1,
			----------		Total = @Total,
			----------		LimitRLID = @LowestRLID
			----------	WHERE ID = @Index	
				
			----------	IF @Debug = 2
			----------	BEGIN
			----------		SELECT	 @Limit			[@Limit]
			----------				,@Total			[@Total]
			----------				,@LowestBalance	[@LowestBalance]
			----------	END
				
			----------END
			
			------------ Write back to the policy limits
			----------UPDATE p
			----------SET p.Balance -= @Total
			----------FROM @PolicyLimits p
			----------INNER JOIN #ClaimRows c ON p.ResourceListID = c.ResourceListID OR p.ResourceListID = c.ParentResourceListID
			----------WHERE c.ID = @Index
			----------AND @TreatmentStart >= StartDate 
			----------AND @TreatmentStart < EndDate
			
			
			/*
			Limit Reached
			*/
			
			-- If we have settled on a figure that is greater than the lowest remaining balance of the policy section or parent section then...
			DECLARE @LowestBalanceRowID INT = NULL,
					@LowestBalance MONEY = NULL,
					@LowestRLID INT = NULL
			-- Find the ID of the row we are interested in
			SELECT	@LowestBalanceRowID =	CASE 
												WHEN p.Balance < ISNULL(p2.Balance, 0) AND p.Balance IS NOT NULL THEN p.ID 
												WHEN p2.Balance IS NOT NULL THEN p2.ID
												ELSE NULL 
											 END
			FROM #ClaimRows c
			--LEFT JOIN @PolicyLimits p ON c.ResourceListID = p.ResourceListID
			--LEFT JOIN @PolicyLimits p2 ON c.ParentResourceListID = p2.ResourceListID
			--FIX for Zen#23991
			LEFT JOIN @PolicyLimits p ON c.ResourceListID = p.ResourceListID and c.TreatmentStart >= p.StartDate and c.TreatmentStart < p.EndDate
			LEFT JOIN @PolicyLimits p2 ON c.ParentResourceListID = p2.ResourceListID and c.TreatmentStart >= p2.StartDate and c.TreatmentStart < p2.EndDate
			WHERE c.ID = @Index
			

			-- Now get the data from that row
			SELECT	@LowestBalance = Balance,
					@LowestRLID = ResourceListID
			FROM @PolicyLimits
			WHERE ID = @LowestBalanceRowID
			
			-- Handle SAP claims calculated against a policy limit that is too high
			IF @LowestBalance < 0
			BEGIN
				SELECT @LowestBalance = 0
			END
			
			DECLARE @Limit MONEY = NULL			
			IF @Total > @LowestBalance
			BEGIN
			
				SELECT @Limit = @Total - @LowestBalance	
				
				-- Update the running total of this row (we add as limit reached is positive)
				SELECT @Total -= @Limit
				
				-- Update the claim row
				UPDATE #ClaimRows
				SET Limit = @Limit * -1,
					Total = @Total,
					LimitRLID = @LowestRLID
				WHERE ID = @Index	
					
			END
			
			/*JEL 2018-04-15 We need to make  sure a 12 month policy obeys it's application rule i.e. treatment or onset starts the 12 month*/
			IF @PolicyType = 76175 /*12 Month*/ AND 69456 = (SELECT ISNULL(Scheme12MonthCoverStart.ValueInt,0) -- Retrieve the "12 month cover start"  setting
												 FROM MatterDetailValues Scheme12MonthCoverStart WITH (NOLOCK)
											     WHERE Scheme12MonthCoverStart.MatterID = @CurrentPolicyMatterID
		                                         AND Scheme12MonthCoverStart.DetailFieldID =   162632 )
			BEGIN 
			
				-- Write back to the policy limits
				UPDATE p
				SET p.Balance -= @Total
				FROM @PolicyLimits p
				INNER JOIN #ClaimRows c ON p.ResourceListID = c.ResourceListID OR p.ResourceListID = c.ParentResourceListID
				WHERE c.ID = @Index
				AND @DateOfOnset >= StartDate 
				AND @DateOfOnset < EndDate
			
			
			END
			ELSE
			BEGIN
			
				-- Write back to the policy limits
				UPDATE p
				SET p.Balance -= @Total
				FROM @PolicyLimits p
				INNER JOIN #ClaimRows c ON p.ResourceListID = c.ResourceListID OR p.ResourceListID = c.ParentResourceListID
				WHERE c.ID = @Index
				AND @TreatmentStart >= StartDate 
				AND @TreatmentStart < EndDate
				
			END
			
			IF @Total = 0
			BEGIN
				CONTINUE
			END
		
		
		END

		IF @Debug > 1
		BEGIN
			SELECT '#ClaimRows' [Big Loop End], @Index [@Index], * FROM #ClaimRows
		END
	
	END

	IF @Debug > 1
	BEGIN
		SELECT 'After Big Loop' [#ClaimRows], * FROM #ClaimRows
	END

	
	/*2018-04-13 - SA - Taken from /*Not required for LandG*/ section as required for process*/


	--DECLARE @PolicyMatterID INT = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID),
	--				   @ValueINT INT

	--/*Write to PA lead to inform customer*/ 
	--SELECT @ValueInt = c.LatestInProcessLeadEventID from Cases c WITH (NOLOCK) 
	--INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.caseID 
	--where m.MatterID = @PolicyMatterID

	--/*Cancel policy*/
	--EXEC _C00_ApplyLeadEventByAutomatedEventQueue @ValueINT,156748,58552 ,-1 /*SA - updated client personnel record to 58552 from 44412 - 2018-04-30*/


	Print 'Big Loop End' 
	/*Not required for LandG*/
	--/*If there is a date in dead pet, we need to remove the outstnding balance from claims*/ 
	--DECLARE @PolicyMatterID INT = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID),
	--@PetDod DATE,
	--@OneOffPaymentGross DECIMAL (18,2),
	--@ThisPayment DECIMAL (18,2),
	--,
	--@DeductionText VARCHAR(100),
	--@PolLeadID INT
			
	--SELECT @PolLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID),
	--		 @PetDod = dbo.fnGetSimpleDv(144271,@PolLeadID)		
	
	--/*If we have a date of death and a customer pays monthly only*/
	--IF (ISNULL(@PetDod,'') <> '' and dbo.fnGetSimpleDvAsInt(170176,@PolicyMatterID) = 69943)
	--BEGIN
		
	--	EXEC @OneOffPaymentGross = _C600_DeadPet_GetOutstandingBalance @CaseID,NULL,@PetDod 

	--	IF ISNULL(@OneOffPaymentGross,0.00) <> 0.00
	--	BEGIN
			
	--		SELECT @DeductionText = 'We have deducted ' + CAST(@OneOffPaymentGross as VARCHAR(30)) + ' in repsect of unpaid premiums for this policy'
		
	--		/*Write to PA lead to inform customer*/ 
	--		SELECT @ValueInt = c.LatestInProcessLeadEventID from Cases c WITH (NOLOCK) 
	--		INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.caseID 
	--		where m.MatterID = @PolicyMatterID

	--		EXEC _C00_ApplyLeadEventByAutomatedEventQueue @ValueINT,156748,44412,-1
			
	--		DECLARE @IDToP INT,
	--				@AmountToReduce DECIMAL (18,2),
	--				@Totes DECIMAL (18,2)
					
					
	--		SELECT @Totes = SUM(Total) FROM #ClaimRows c 
	--		where c.CaseID = @CaseID	
			
	--		/*If the total of outstanding paymet is greater than the full amount then reduce to 0*/
	--		IF @OneOffPaymentGross > @Totes
	--		BEGIN
				
	--			Update c 
	--			SET  UserDeductions =  Total, Total = 0.00
	--			FROM #ClaimRows c 
	--			where c.CaseID = @CaseID 	
				
	--		END 
	--		ELSE 
	--		BEGIN /*Otherwise loop round the rows, highest first and deduct*/  
			
	--			DECLARE @Processed TABLE (ID INT, Processed BIT) 
	--			INSERT INTO @Processed (ID,Processed)
	--			SELECT c.ID, 0 FROM  #ClaimRows c 
	--			WHere c.CaseID = @CaseID 
						
	--			WHILE EXISTS (SELECT * FROM @Processed p where p.Processed = 0) and @OneOffPaymentGross > 0  
	--			BEGIN 
					
					
	--				SELECT top 1 @IDToP = c.ID ,@AmountToReduce = CASE WHEN c.Total >= @OneOffPaymentGross THEN @OneOffPaymentGross ELSE c.Total END
	--				FROM #ClaimRows c 
	--				INNER JOIN @Processed p on p.ID = c.ID 
	--				WHERE c.Total > 0
	--				and c.CaseID = @CaseID
	--				and p.Processed = 0
	--				ORDER BY c.Total DESC
	--				PRINT @IDToP
	--				SELECT @OneOffPaymentGross = @OneOffPaymentGross - @AmountToReduce

	--				UPDATE sr
	--				SET Total = sr.Total - @AmountToReduce, UserDeductions = ISNULL(UserDeductions,0.00) + @AmountToReduce
	--				FROM #ClaimRows sr 
	--				WHERE sr.ID = @IDToP
					
	--				Update @Processed 
	--				SET Processed = 1 
	--				Where ID = @IDToP

	--				SELECT @IDToP = NULL, @AmountToReduce = 0.00	
					
	--			END
	--		END

	--		IF @Debug = 0 
	--		BEGIN
				
	--			EXEC _C00_ApplyLeadEventByAutomatedEventQueue @ValueINT,156748,44412,-1
	--			SELECT @ValueInt = 0
				
	--			/*Make sure we don't go negative*/
				
				
	--			/*Now write the detail to the user deductions text so we know this was an outstanding prem*/ 
								
	--			DECLARE @NoteDate DATETIME = dbo.fn_GetDate_Local()
				
	--			EXEC _C00_AddANote  @CaseID,924,@DeductionText,0, 44412,0,0,@NoteDate
			
	--			EXEC TableRows_Insert   @ValueINT OUTPUT,384,Null,@MatterID,148349,16188,0,0,NULL,NULL,NULL,NULL,NULL
			
	--			EXEC _C00_SimpleValueIntoField  148348, @DeductionText, @ValueINT,44412
				
				
	--		END
	--		ELSE
	--		BEGIN 
				
	--			PRINT 'Robin...'
				
	--		END
	--	END
	--END
	IF @Debug >= 1
	BEGIN
	
		SELECT 'Claim Rows' AS Data, * 
		FROM #ClaimRows
		ORDER BY ID
		
		/*JEL 2019-03-04 Added sub query to stop returning duplicate rows for Excess*/ 
		SELECT 'Policy Cover' AS Data, *
		FROM @PolicyCover p 
		WHERE NOT EXISTS (SELECT * FROM @PolicyCover p2 
						  WHERE p2.StartDate = p.StartDate
						  AND p2.EndDate = p.EndDate 
						  AND p2.SectionRLID = p.SectionRLID
						  AND p2.SubSectionRLID = p.SubSectionRLID 
						  AND p2.ID > p.ID) 
		
		SELECT 'Policy Excess' AS Data, * 
		FROM @PolicyExcesses  p 
		WHERE NOT EXISTS (SELECT * FROM @PolicyExcesses p2 
						  WHERE p2.StartDate = p.StartDate
						  AND p2.EndDate = p.EndDate 
						  AND p2.Section = p.Section
						  AND p2.SubSection = p.SubSection 
						  AND p2.ID > p.ID) 
		
		SELECT 'Excess Rebate' AS Data, *
		FROM @ExcessRebate
		
		SELECT 'Co-Insurance' AS Data, c.*, ll.ItemValue AS LinkedTo
		FROM @PolicyCoInsurance c
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON c.CoInsLinkedTo = ll.LookupListItemID
		INNER JOIN @PolicyExcesses pe ON pe.ResourceListID = c.ResourceListID
		
		SELECT 'Policy Limits' AS Data, * 
		FROM @PolicyLimits
		
		/*SELECT DISTINCT 'User Deductions' AS Data
		FROM #ClaimRows c
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON c.MatterID = r.MatterID AND r.DetailFieldID = 147302
		INNER JOIN dbo.TableDetailValues tdvTableRowID WITH (NOLOCK) ON r.TableRowID = tdvTableRowID.TableRowID AND tdvTableRowID.DetailFieldID = 147299
		INNER JOIN dbo.TableDetailValues tdvReason WITH (NOLOCK) ON r.TableRowID = tdvReason.TableRowID AND tdvReason.DetailFieldID = 147300
		INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 147301
		WHERE c.TableRowID = 0*/

		--				IS: 2019-07-03 SLACKY-PERF - breakup the calculation and the save into two parts, 1-CALC&CACHE 2-SAVE
		DECLARE @WhenCreated_XML DATETIME = dbo.fn_GetDate_Local()
		DECLARE @XML_CALC VARCHAR(MAX) =
		(
			SELECT 
				CAST(
				( 
					SELECT 'Claim Rows' AS Data, * 
					FROM #ClaimRows cr
					ORDER BY ID
					FOR XML AUTO, ELEMENTS
				) AS XML) [ClaimRows],
				CAST(
				(		
					/*JEL 2019-03-04 Added sub query to stop returning duplicate rows for Excess*/ 
					SELECT 'Policy Cover' AS Data, *
					FROM @PolicyCover pc 
					WHERE NOT EXISTS (SELECT * FROM @PolicyCover p2 
									  WHERE p2.StartDate = pc.StartDate
									  AND p2.EndDate = pc.EndDate 
									  AND p2.SectionRLID = pc.SectionRLID
									  AND p2.SubSectionRLID = pc.SubSectionRLID 
									  AND p2.ID > pc.ID)
					FOR XML AUTO, ELEMENTS
				) AS XML) [PolicyCovers],
				CAST(
				(
					SELECT 'Policy Excess' AS Data, * 
					FROM @PolicyExcesses  pe 
					WHERE NOT EXISTS (SELECT * FROM @PolicyExcesses p2 
									  WHERE p2.StartDate = pe.StartDate
									  AND p2.EndDate = pe.EndDate 
									  AND p2.Section = pe.Section
									  AND p2.SubSection = pe.SubSection 
									  AND p2.ID > pe.ID)
					FOR XML AUTO, ELEMENTS
				) AS XML) [PolicyExcesses],
				CAST(
				(
					SELECT 'Excess Rebate' AS Data, *
					FROM @ExcessRebate er
					FOR XML AUTO, ELEMENTS
				) AS XML) [ExcessRebates],
				CAST(
				(
					SELECT 'Co-Insurance' AS Data, pci.*, ll.ItemValue AS LinkedTo
					FROM @PolicyCoInsurance pci
					LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON pci.CoInsLinkedTo = ll.LookupListItemID
					INNER JOIN @PolicyExcesses pe ON pe.ResourceListID = pci.ResourceListID
					FOR XML AUTO
				) AS XML) [PolicyCoInsurances],
				CAST(
				(
					SELECT 'Policy Limits' AS Data, * 
					FROM @PolicyLimits pl
					FOR XML AUTO, ELEMENTS
				) AS XML) [PolicyLimits],
				[CaseID],
				[WhenGen]
			FROM (
				SELECT @CaseID [CaseID], dbo.fn_GetDate_Local() [WhenGen]
			) [ClaimCalc]
			FOR XML AUTO, ELEMENTS
		)

		EXEC AsyncQueue__Insert 
			@ClientID =		@ClientID, 
			@WhenCreated =	@WhenCreated_XML, 
			@QueueTypeID =	@CaseID, 
			@Status =		0, 
			@Payload =		@XML_CALC, 
			@Outcome =		'', 
			@WhenCompleted = NULL
		--				IS: 2019-07-03 SLACKY-PERF - breakup the calculation and the save into two parts, 1-CALC&CACHE 2-SAVE
						
	END 
	ELSE
	BEGIN

		-- Need to insert new table rows where the table row ID is 0
		DECLARE @TableRowIDs TABLE
		(
			ID INT,
			TableRowID INT
		)
		INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
		OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowIDs
		SELECT @ClientID, MatterID, 144355, 16157, ID
		FROM #ClaimRows
		WHERE TableRowID = 0
		
		UPDATE c
		SET c.TableRowID = r.TableRowID
		FROM #ClaimRows c
		INNER JOIN @TableRowIDs r ON c.ID = r.ID
		WHERE c.TableRowID = 0
		
		
		
		-- And now the table detail values
		DECLARE @Fields TABLE
		(
			FieldID INT
		)
		INSERT @Fields(FieldID) VALUES
		(146406), 
		(146407), 
		(146408), 
		(144352),
		(144353), 
		(145678), 
		(148399),
		(147434),
		(158802),
		(146179),
		(179568),
		(179564)


	
		-- Update values that exists -- only not paid rows	
		UPDATE tdv
		SET DetailValue =
			CASE 
				WHEN tdv.DetailFieldID = 146406 THEN CAST(c.Excess AS VARCHAR)
				WHEN tdv.DetailFieldID = 146407 THEN CAST(c.CoInsurance AS VARCHAR)
				WHEN tdv.DetailFieldID = 146408 THEN CAST(c.Limit AS VARCHAR)
				WHEN tdv.DetailFieldID = 144352 THEN CAST(c.Total AS VARCHAR)
				WHEN tdv.DetailFieldID = 144353 THEN CAST(c.Claim AS VARCHAR)
				WHEN tdv.DetailFieldID = 145678 THEN CAST(c.Settle AS VARCHAR)
				WHEN tdv.DetailFieldID = 148399 THEN CAST(c.LimitRLID AS VARCHAR)
				WHEN tdv.DetailFieldID = 147434 THEN CAST(c.ExcessRebate AS VARCHAR)
				WHEN tdv.DetailFieldID = 158802 THEN CAST(c.VoluntaryExcess AS VARCHAR)
				WHEN tdv.DetailFieldID = 146179 THEN CAST(c.UserDeductions AS VARCHAR)
				WHEN tdv.DetailFieldID = 179568 THEN CAST(pl.Balance as VARCHAR) /*Remaining Limit*/
				WHEN tdv.DetailFieldID = 179564 THEN CAST(c.CoInsurance as VARCHAR) /*Coinsurance Percentage*/
			END
		FROM dbo.TableDetailValues tdv (nolock)
		INNER JOIN #ClaimRows c ON tdv.TableRowID = c.TableRowID AND tdv.MatterID = c.MatterID
		LEFT JOIN @PolicyLimits pl on pl.ResourceListID = c.ResourceListID
		WHERE tdv.ClientID = @ClientID
		AND tdv.DetailFieldID IN (SELECT * FROM @Fields)
		AND (c.Paid IS NULL OR c.MatterID = @MatterID) -- Update non paid rows or if recalculating current matter
		
		-- And insert those that don't
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT DISTINCT @ClientID, f.FieldID, c.CustomerID, c.LeadID, c.CaseID, c.MatterID, c.TableRowID, 
			CASE
				WHEN f.FieldID = 146406 THEN CAST(c.Excess AS VARCHAR)
				WHEN f.FieldID = 146407 THEN CAST(c.CoInsurance AS VARCHAR)
				WHEN f.FieldID = 146408 THEN CAST(c.Limit AS VARCHAR)
				WHEN f.FieldID = 144352 THEN CAST(c.Total AS VARCHAR)
				WHEN f.FieldID = 144353 THEN CAST(c.Claim AS VARCHAR)
				WHEN f.FieldID = 145678 THEN CAST(c.Settle AS VARCHAR)
				WHEN f.FieldID = 148399 THEN CAST(c.LimitRLID AS VARCHAR)
				WHEN f.FieldID = 147434 THEN CAST(c.ExcessRebate AS VARCHAR)
				WHEN f.FieldID = 158802 THEN CAST(c.VoluntaryExcess AS VARCHAR)
				WHEN f.FieldID = 146179 THEN CAST(c.UserDeductions AS VARCHAR)
				WHEN f.FieldID = 179568 THEN CAST(pl.Balance as VARCHAR) /*Remaining Limit*/
				WHEN f.FieldID = 179564 THEN CAST(c.CoInsurance as VARCHAR) /*Coinsurance Percentage*/
			END AS VALUE	
		FROM #ClaimRows c
		CROSS JOIN @Fields f 
		LEFT JOIN @PolicyLimits pl on pl.ResourceListID = c.ResourceListID
		WHERE NOT EXISTS (
			SELECT *
			FROM dbo.TableDetailValues tdv (nolock)
			WHERE tdv.TableRowID = c.TableRowID AND tdv.MatterID = c.MatterID AND tdv.DetailFieldID = f.FieldID
		)
		AND (c.Paid IS NULL OR c.MatterID = @MatterID) -- insert non paid rows or if recalculating current matter
		
		-- Finally insert any resource list values if they don't exist - for split rows
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, ResourceListID)
		SELECT @ClientID, 144350, c.CustomerID, c.LeadID, c.CaseID, c.MatterID, c.TableRowID, c.ResourceListID
		FROM #ClaimRows c
		WHERE NOT EXISTS (
			SELECT *
			FROM dbo.TableDetailValues tdv (nolock)
			WHERE tdv.TableRowID = c.TableRowID AND tdv.MatterID = c.MatterID AND tdv.DetailFieldID = 144350
		)
		AND (c.Paid IS NULL OR c.MatterID = @MatterID) -- insert non paid rows or if recalculating current matter
		
			
	END	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Calculations_RunGroupedClaimCalcs] TO [sp_executeall]
GO
