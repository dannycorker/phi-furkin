SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-10
-- Description:	Create a document batch entry

-- Parameters:  @BatchId
--              The document batch entry to create
--              2013-01-24
--              Updated the default Id to 239 (Pheonix)
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatch_Create]
	@Metadata VARCHAR(MAX),
	@ClientID INT = 239
AS
BEGIN
	DECLARE @DocumentBatchId INT
	DECLARE @BatchId INT
	DECLARE @BatchProductionDate DATETIME
	DECLARE @MetadataXml XML
	
	SET @MetadataXml = CAST(@Metadata AS XML)
	
	-- First extract the id of the batch
	SELECT
		@BatchId = Col.value('@Id', 'int'),
		@BatchProductionDate = Col.value('@BatchProductionDate', 'datetime')
	FROM
		@MetadataXml.nodes('/MFG/Batch[1]') AS Tab(Col)

	-- Check the the batch id could be located
	-- If the batch id could not be found then return 0
	IF (@BatchId IS NULL) 
	BEGIN
		SELECT -1
		RETURN
	END
	
	-- Create the DocumentBatch entry
	INSERT INTO DocumentBatch(BatchID, MetadataXml, ClientID, BatchProductionDate)
	VALUES (@BatchId, @MetadataXml, @ClientID, @BatchProductionDate)
	
	-- Return the id of the record just created
	SELECT SCOPE_IDENTITY()
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_Create] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_Create] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_Create] TO [sp_executeall]
GO
