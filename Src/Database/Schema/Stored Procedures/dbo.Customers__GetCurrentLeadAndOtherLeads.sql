SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2016-02-16
-- Description:	Get a single lead plus other leads and lead types for a customer
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetCurrentLeadAndOtherLeads]
(
	@CustomerID INT,
	@LeadID INT,
	@UserID INT
)
AS
BEGIN

	EXEC dbo.Lead__GetExpandedByLeadID @LeadID
	EXEC dbo.Customers__GetAllLeadTypesAndLeadIDByCustomerID @CustomerID, @UserID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetCurrentLeadAndOtherLeads] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetCurrentLeadAndOtherLeads] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetCurrentLeadAndOtherLeads] TO [sp_executeall]
GO
