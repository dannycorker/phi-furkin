SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateAvatarTextToSpeak] 
@TextToSpeakID int,
@ClientQuestionnaireID int,
@ClientID int,
@PageNumber int,
@SpeakText nvarchar(512),
@IsShown bit

as

Update TextToSpeak
SET  ClientQuestionnaireID = @ClientQuestionnaireID,
ClientID = @ClientID,
PageNumber = @PageNumber,
SpeakText = @SpeakText,
IsShown = @IsShown
where TextToSpeakID = @TextToSpeakID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateAvatarTextToSpeak] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateAvatarTextToSpeak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateAvatarTextToSpeak] TO [sp_executeall]
GO
