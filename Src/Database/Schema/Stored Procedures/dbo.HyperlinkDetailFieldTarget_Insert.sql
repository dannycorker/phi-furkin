SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the HyperlinkDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkDetailFieldTarget_Insert]
(

	@HyperlinkDetailFieldTargetID int    OUTPUT,

	@ClientID int   ,

	@HyperlinkDetailFieldID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@Notes varchar (250)  
)
AS


				
				INSERT INTO [dbo].[HyperlinkDetailFieldTarget]
					(
					[ClientID]
					,[HyperlinkDetailFieldID]
					,[Target]
					,[DetailFieldID]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[Notes]
					)
				VALUES
					(
					@ClientID
					,@HyperlinkDetailFieldID
					,@Target
					,@DetailFieldID
					,@TemplateTypeID
					,@DetailFieldAlias
					,@Notes
					)
				-- Get the identity value
				SET @HyperlinkDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_Insert] TO [sp_executeall]
GO
