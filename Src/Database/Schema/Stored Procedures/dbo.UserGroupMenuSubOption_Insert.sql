SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UserGroupMenuSubOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuSubOption_Insert]
(

	@UserGroupMenuSubOptionID int    OUTPUT,

	@UserGroupMenuOptionID int   ,

	@ClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@PanelItemName varchar (50)  ,

	@PanelItemCaption varchar (50)  ,

	@PanelItemURL varchar (250)  ,

	@ShowInFancyBox bit   ,

	@SubItemOrder int   
)
AS


				
				INSERT INTO [dbo].[UserGroupMenuSubOption]
					(
					[UserGroupMenuOptionID]
					,[ClientID]
					,[ClientPersonnelAdminGroupID]
					,[PanelItemName]
					,[PanelItemCaption]
					,[PanelItemURL]
					,[ShowInFancyBox]
					,[SubItemOrder]
					)
				VALUES
					(
					@UserGroupMenuOptionID
					,@ClientID
					,@ClientPersonnelAdminGroupID
					,@PanelItemName
					,@PanelItemCaption
					,@PanelItemURL
					,@ShowInFancyBox
					,@SubItemOrder
					)
				-- Get the identity value
				SET @UserGroupMenuSubOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Insert] TO [sp_executeall]
GO
