SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeAsyncEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAsyncEvent_Delete]
(

	@EventTypeAsyncEventID int   
)
AS


				DELETE FROM [dbo].[EventTypeAsyncEvent] WITH (ROWLOCK) 
				WHERE
					[EventTypeAsyncEventID] = @EventTypeAsyncEventID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAsyncEvent_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Delete] TO [sp_executeall]
GO
