SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-21
-- Description:	Get last successful sync date
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SageGetRuntimeParameters]
	@ClientID INT,
	@Area VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Message VARCHAR(1024) = 'ClientID: ' + CONVERT(VARCHAR,@ClientID) + ' Area: ' + @Area

	EXEC dbo._C00_LogIt 'SAGE50', '_C00_SageGetRuntimeParameters', '_C00_SageGetRuntimeParameters', @Message, 38867

	IF @ClientID = 361 
	BEGIN
	
		IF @Area IN ('Orders')
		BEGIN
	
			SELECT CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()-30,120) AS [LastSuccess]
		
		END
	
		IF @Area IN ('OrdersGDN')
		BEGIN
	
			SELECT CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()+1,120) AS [LastSuccess] /*Dont need this anymore*/
		
		END
	
		IF @Area IN ('StockTrans')
		BEGIN
	
			SELECT CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()-30,120) AS [LastSuccess]
		
		END
	
	END

	IF @ClientID = 361 AND @Area = 'CustomerUpdates'
	BEGIN
	
		SELECT CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()-30,120) AS [LastSuccess]
	
	END
	ELSE
	IF @ClientID = 361 AND @Area = 'SupplierUpdates'
	BEGIN
	
		SELECT '1990-01-01' AS [LastSuccess]
	
	END
	ELSE
	IF @ClientID = 361 AND @Area = 'Customers'
	BEGIN
	
		SELECT CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()-30,120) AS [LastSuccess]
	
	END
	ELSE
	IF @ClientID = 361 AND @Area = 'Invoices'
	BEGIN
	
		SELECT CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()-30,120) AS [LastSuccess]
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageGetRuntimeParameters] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SageGetRuntimeParameters] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageGetRuntimeParameters] TO [sp_executeall]
GO
