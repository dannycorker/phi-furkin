SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentQueue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentQueue_Update]
(

	@DocumentQueueID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@DocumentTypeID int   ,

	@WhoCreated int   ,

	@WhenStored datetime   ,

	@WhenCreated datetime   ,

	@RequiresApproval bit   ,

	@ParsedDocumentTitle varchar (1000)  ,

	@BasedUponLeadEventID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentQueue]
				SET
					[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[DocumentTypeID] = @DocumentTypeID
					,[WhoCreated] = @WhoCreated
					,[WhenStored] = @WhenStored
					,[WhenCreated] = @WhenCreated
					,[RequiresApproval] = @RequiresApproval
					,[ParsedDocumentTitle] = @ParsedDocumentTitle
					,[BasedUponLeadEventID] = @BasedUponLeadEventID
				WHERE
[DocumentQueueID] = @DocumentQueueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Update] TO [sp_executeall]
GO
