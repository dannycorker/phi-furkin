SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Module table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Module_Get_List]

AS


				
				SELECT
					[ModuleID],
					[ModuleName]
				FROM
					[dbo].[Module] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Module_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Get_List] TO [sp_executeall]
GO
