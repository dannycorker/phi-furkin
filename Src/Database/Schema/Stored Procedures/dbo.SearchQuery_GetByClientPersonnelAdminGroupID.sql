SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SearchQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_GetByClientPersonnelAdminGroupID]
(

	@ClientPersonnelAdminGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SearchQueryID],
					[ClientID],
					[SubClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[SearchQueryName]
				FROM
					[dbo].[SearchQuery] WITH (NOLOCK) 
				WHERE
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_GetByClientPersonnelAdminGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
