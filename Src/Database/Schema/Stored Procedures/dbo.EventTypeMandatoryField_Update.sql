SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeMandatoryField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField_Update]
(

	@EventTypeMandatoryFieldID int   ,

	@EventTypeID int   ,

	@DetailFieldID int   ,

	@LeadTypeID int   ,

	@ClientID int   ,

	@ForceEditableOverride bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeMandatoryField]
				SET
					[EventTypeID] = @EventTypeID
					,[DetailFieldID] = @DetailFieldID
					,[LeadTypeID] = @LeadTypeID
					,[ClientID] = @ClientID
					,[ForceEditableOverride] = @ForceEditableOverride
				WHERE
[EventTypeMandatoryFieldID] = @EventTypeMandatoryFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Update] TO [sp_executeall]
GO
