SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-03-24
-- Description:	Shreds the Claim XML into Aquarium Detail Field Values
-- =============================================
CREATE PROCEDURE [dbo].[IISSA_AddClaim]

	@ClientID INT, 
	@ClientPersonnelID INT, 
	@XMLVarchar VARCHAR(MAX) = NULL, 
	@XMLLogID INT = NULL, 
	@LeadDocumentID INT = NULL, 
	@Debug BIT = 0, 
	@LeadTypeID INT = 1262 /* Irish Insolvency System for C267 */

AS
BEGIN
	
	SET NOCOUNT ON;
	
	/*
		JWG 2014-03-24 Multi-pass approach to this new work.
		
		1) Quick demo.  This will use a sample of real field IDs for one client only.  
		Use a letter-in event against a dummy customer to create a document; 
		Use SAE to call this proc with the LeadDocumentID;
		Read the document blob and convert it into XML
		
		2) Full implementation.  This will map all fields using 3rd party mapping.
		Clients will call the web service, passing in the XML;
		The app will log the XML and then validate it against the XSD;
		The app will call this proc, passing in the XML and the log id
	
	*/
	
	/************************************************************************************************************************************************************/
	/*																																							*/
	/*																Variable Declarations																		*/
	/*																																							*/
	/************************************************************************************************************************************************************/
	DECLARE 
	
	/* General Details */
	@XML XML,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@LeadRef VARCHAR(100), 
	@LeadViewHistoryID INT,
	@TheDateTime DATETIME = dbo.fn_GetDate_Local(),
	@TheDateTimePlus1s DATETIME = DATEADD(SECOND, 1, dbo.fn_GetDate_Local()), 
	
	/* XML Personal */
	@Title VARCHAR(100),
	@TitleID INT = 0,
	@FirstName VARCHAR(100),
	@LastName VARCHAR(100),
	@DateOfBirth VARCHAR(100),
	@HomeTel VARCHAR(100),
	@MobileTel VARCHAR(100),
	@EmailAddress VARCHAR(100),
	@AddressLine1 VARCHAR(100),
	@AddressLine2 VARCHAR(100),
	@AddressLine3 VARCHAR(100),
	@Town VARCHAR(100),
	@County VARCHAR(100),
	@Postcode VARCHAR(100) = 'Ireland',
	@InterlockingLU VARCHAR(2000),
	@OwnRefNo VARCHAR(2000),
	@OwnDescription VARCHAR(2000),
	@CaseUNID VARCHAR(2000),
	@CaseCreated VARCHAR(2000),
	@CaseCompleted VARCHAR(2000),
	@CaseType VARCHAR(2000),
	@CaseDescription VARCHAR(2000),
	@PIPName VARCHAR(2000),
	@PIPRef VARCHAR(2000),
	@AppType VARCHAR(2000),
	@ApplicationTypeLU VARCHAR(2000) = '',
	@BirthCertName VARCHAR(2000),
	@Gender VARCHAR(2000),
	@GenderLU VARCHAR(2000),
	@PrevNames VARCHAR(2000),
	@PrevNamesLU VARCHAR(2000),
	@PPSN VARCHAR(2000),
	@Nationality VARCHAR(2000),	
	@BirthCountry VARCHAR(2000),	
	@BirthCountryLU VARCHAR(2000) = '',	
	@AQ_PIA_Q1 VARCHAR(2000),
	@AQ_PIA_Q2 VARCHAR(2000),
	@AQ_PIA_Q2A VARCHAR(2000),
	@AQ_PIA_Q3 VARCHAR(2000),
	@AQ_PIA_Q4 VARCHAR(2000),
	@AQ_PIA_Q5 VARCHAR(2000),
	@AQ_PIA_Q6 VARCHAR(2000),
	@INIT_Q1 VARCHAR(2000),
	@INIT_Q2 VARCHAR(2000),
	@INIT_Q3 VARCHAR(2000),
	@INIT_Q4 VARCHAR(2000),
	@INIT_Q5 VARCHAR(2000),
	@PersonalComment VARCHAR(2000), 
	@JurisdictionCounty VARCHAR(2000),
	@JurisdictionCourt VARCHAR(2000),
	@JurisdictionReason VARCHAR(2000),
	
	/* XML Employment */
	@EmploymentStatus VARCHAR(2000), 
	@EmploymentComment VARCHAR(2000), 
	@Occupation VARCHAR(2000),
	@EmployerName VARCHAR(2000),
	@LengthOfServiceRaw VARCHAR(2000), 
	@LengthOfServiceYears VARCHAR(2000) = '', 
	@LengthOfServiceMonths VARCHAR(2000) = '', 
	@EmployerAddressLine1 VARCHAR(100),
	@EmployerAddressLine2 VARCHAR(100),
	@EmployerAddressLine3 VARCHAR(100),
	@EmployerTown VARCHAR(100),
	@EmployerCounty VARCHAR(100),
	@EmployerCountry VARCHAR(100) = 'Ireland',
	
	/* XML RLE */
	@RLEOneOrTwoAdultsRaw VARCHAR(2000), 
	@RLEOneOrTwoAdultsLU VARCHAR(2000), 
	@RLEAnyDependentChildren VARCHAR(2000), 
	@RLEAnyDependentChildrenLU VARCHAR(2000), 
	@RLEDependentChildrenAges VARCHAR(2000), 
	@RLEOwnAMotor VARCHAR(2000), 
	@RLEOwnAMotorLU VARCHAR(2000), 
	@RLENeedAMotor VARCHAR(2000), 
	@RLENeedAMotorLU VARCHAR(2000), 
	@RLEAnyChildcareCosts VARCHAR(2000), 
	@RLEAnyChildcareCostsLU VARCHAR(2000), 
	@RLEChildcareCosts VARCHAR(2000), 
	@RLEMonthlyRent VARCHAR(2000), 
	@RLESpecialCircumstances VARCHAR(2000), 
	@RLESpecialCircumstancesAmount VARCHAR(2000), 
	@RLEComment VARCHAR(2000), 
	
	@EnoughVariablesAlready bit=1

	/* Tables for repeating XML nodes */
	DECLARE @InsertedTableRows TABLE (UNID INT, TableRowID INT)

	DECLARE @Fields TABLE (DetailFieldID INT)


	/************************************************************************************************************************************************************/
	/*																																							*/
	/*																Basic XML Parsing																			*/
	/*																																							*/
	/************************************************************************************************************************************************************/
	IF @XMLVarchar IS NOT NULL
	BEGIN
		/* Simply convert the varchar to XML so that we can query it */
		SELECT @XML = CAST(@XMLVarchar AS XML)
	END
	ELSE
	BEGIN
		IF @LeadDocumentID > 0 
		BEGIN
			/* Get the XML so that we can query it */
			SELECT @XML = CAST(CAST(v.DocumentBLOB AS VARCHAR(MAX)) AS XML) 
			FROM dbo.vLeadDocumentList v 
			WHERE v.LeadDocumentID = @LeadDocumentID 
		END
		ELSE
		BEGIN
			RAISERROR ('You must pass in the XML or the LeadDocumentID to read', 16, 1)
			RETURN
		END
	END
	
	/* Debug */
	IF @Debug = 1
	BEGIN
		SELECT @XML
	END
	

	SELECT 
	@LeadRef = n.c.value('(CaseDetails/CaseRefNo)[1]', 'varchar(2000)'), --CaseRefNo,
	@OwnRefNo = n.c.value('(CaseDetails/OwnRefNo)[1]', 'varchar(2000)'), --OwnRefNo,
	@CaseUNID = n.c.value('(CaseDetails/CaseUNID)[1]', 'varchar(2000)'), --CaseUNID,
	@PIPName = n.c.value('(CaseDetails/PIPName)[1]', 'varchar(2000)'), --PIPName,
	@PIPRef = n.c.value('(CaseDetails/PIPRef)[1]', 'varchar(2000)'), --PIPRef,
	@CaseType = n.c.value('(CaseDetails/CaseType)[1]', 'varchar(2000)'), --CaseType,
	@AppType = n.c.value('(CaseDetails/AppType)[1]', 'varchar(2000)'), --AppType,
	@CaseDescription = n.c.value('(CaseDetails/CaseDescription)[1]', 'varchar(2000)'), --CaseDescription,
	@OwnDescription = n.c.value('(AppForm/OwnDescription)[1]', 'varchar(2000)'), --OwnDescription,
	@AddressLine1 = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Line1)[1]', 'varchar(2000)'), 100), --Line1,
	@AddressLine2 = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Line2)[1]', 'varchar(2000)'), 100), --Line2,
	@AddressLine3 = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Line3)[1]', 'varchar(2000)'), 100), --Line3,
	@Town = LEFT(n.c.value('(AppForm/PPRAddress/Irish/Town)[1]', 'varchar(2000)'), 100), --Town,
	@County = LEFT(n.c.value('(AppForm/PPRAddress/Irish/County)[1]', 'varchar(2000)'), 100), --County,
	@AQ_PIA_Q1 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q1)[1]', 'varchar(2000)'), --AQ_PIA_Q1,
	@AQ_PIA_Q2 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q2)[1]', 'varchar(2000)'), --AQ_PIA_Q2,
	@AQ_PIA_Q2A = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q2A)[1]', 'varchar(2000)'), --AQ_PIA_Q2A,
	@AQ_PIA_Q3 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q3)[1]', 'varchar(2000)'), --AQ_PIA_Q3,
	@AQ_PIA_Q4 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q4)[1]', 'varchar(2000)'), --AQ_PIA_Q4,
	@AQ_PIA_Q5 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q5)[1]', 'varchar(2000)'), --AQ_PIA_Q5,
	@AQ_PIA_Q6 = n.c.value('(AppForm/ApplicationQuestions/ApplicationQuestions_PIA/AQ_PIA_Q6)[1]', 'varchar(2000)'), --AQ_PIA_Q6,
	@Title = LEFT(n.c.value('(AppForm/Debtor/Personal/Title)[1]', 'varchar(2000)'), 100), --Title,
	@FirstName = LEFT(n.c.value('(AppForm/Debtor/Personal/FirstName)[1]', 'varchar(2000)'), 100), --FirstName,
	@LastName = LEFT(n.c.value('(AppForm/Debtor/Personal/LastName)[1]', 'varchar(2000)'), 100), --LastName,
	@BirthCertName = n.c.value('(AppForm/Debtor/Personal/BirthCertName)[1]', 'varchar(2000)'), --BirthCertName,
	@PrevNames = n.c.value('(AppForm/Debtor/Personal/PreviouslyKnownNames)[1]', 'varchar(2000)'), --PreviouslyKnownNames,
	@PPSN = n.c.value('(AppForm/Debtor/Personal/PPSN)[1]', 'varchar(2000)'), --PPSN,
	@Gender = n.c.value('(AppForm/Debtor/Personal/Gender)[1]', 'varchar(2000)'), --Gender,
	@DateOfBirth = n.c.value('(AppForm/Debtor/Personal/BirthDate)[1]', 'varchar(2000)'), --BirthDate,
	@BirthCountry = n.c.value('(AppForm/Debtor/Personal/BirthCountry)[1]', 'varchar(2000)'), --BirthCountry,
	@Nationality = n.c.value('(AppForm/Debtor/Personal/Nationality)[1]', 'varchar(2000)'), --Nationality,
	--n.c.value('(AppForm/Debtor/Personal/MaritalStatus)[1]', 'varchar(2000)'), --MaritalStatus,
	@HomeTel = LEFT(n.c.value('(AppForm/Debtor/Personal/Phone)[1]', 'varchar(2000)'), 100), --Phone,
	@MobileTel = LEFT(n.c.value('(AppForm/Debtor/Personal/Mobile)[1]', 'varchar(2000)'), 100), --Mobile,
	@EmailAddress = LEFT(n.c.value('(AppForm/Debtor/Personal/Email)[1]', 'varchar(2000)'), 100), --Email,
	@PersonalComment = n.c.value('(AppForm/Debtor/Personal/Comment)[1]', 'varchar(2000)'), --Comment,
	@EmploymentStatus = n.c.value('(AppForm/Debtor/Employment/Employed/EmploymentStatus)[1]', 'varchar(2000)'), --EmploymentStatus,
	@Occupation = n.c.value('(AppForm/Debtor/Employment/Employed/Occupation)[1]', 'varchar(2000)'), --Occupation,
	@EmployerName = n.c.value('(AppForm/Debtor/Employment/Employed/EmployerName)[1]', 'varchar(2000)'), --EmployerName,
	@EmployerAddressLine1 = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Line1)[1]', 'varchar(2000)'), 100), --Line1,
	@EmployerAddressLine2 = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Line2)[1]', 'varchar(2000)'), 100), --Line2,
	@EmployerAddressLine3 = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Line3)[1]', 'varchar(2000)'), 100), --Line3,
	@EmployerTown = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/Town)[1]', 'varchar(2000)'), 100), --Town,
	@EmployerCounty = LEFT(n.c.value('(AppForm/Debtor/Employment/Employed/BusinessAddress/Irish/County)[1]', 'varchar(2000)'), 100), --County,
	@LengthOfServiceRaw = n.c.value('(AppForm/Debtor/Employment/Employed/LengthOfService)[1]', 'varchar(2000)'), --LengthOfService,
	@EmploymentComment = n.c.value('(AppForm/Debtor/Employment/Employed/Comment)[1]', 'varchar(2000)'), --Comment,
	@INIT_Q1 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q1)[1]', 'varchar(2000)'), --II_PIA_Q1 1. Has debtor received advice from a PIP?,
	@INIT_Q2 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q2)[1]', 'varchar(2000)'), --II_PIA_Q2 2. Has the PIP confirmed the advice in writing?,
	@INIT_Q3 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q3)[1]', 'varchar(2000)'), --II_PIA_Q3 3: Has the debtor instructed the PIP in writing to make a proposal?,
	@INIT_Q4 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q4)[1]', 'varchar(2000)'), --II_PIA_Q4 4: Does the debtor agree to receiving notices electronically from the ISI?,
	@INIT_Q5 = n.c.value('(AppForm/Debtor/InitialInfo/InitialInfo_PIA/II_PIA_Q5)[1]', 'varchar(2000)'), --II_PIA_Q5 5: Does the debtor agree to receiving notices electronically from the court?,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q1)[1]', 'varchar(2000)'), --IS_PIA_Q1,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q2)[1]', 'varchar(2000)'), --IS_PIA_Q2,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q3)[1]', 'varchar(2000)'), --IS_PIA_Q3,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q4)[1]', 'varchar(2000)'), --IS_PIA_Q4,
	--n.c.value('(AppForm/Debtor/InsolvencyStatus/InsolvencyStatus_PIA/IS_PIA_Q5)[1]', 'varchar(2000)'), --IS_PIA_Q5,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q1)[1]', 'varchar(2000)'), --PI_PIA_Q1,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q2)[1]', 'varchar(2000)'), --PI_PIA_Q2,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q3)[1]', 'varchar(2000)'), --PI_PIA_Q3,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q4)[1]', 'varchar(2000)'), --PI_PIA_Q4,
	--n.c.value('(AppForm/Debtor/PriorInsolvency/PriorInsolvency_PIA/PI_PIA_Q6)[1]', 'varchar(2000)'), --PI_PIA_Q6,
	--n.c.value('(AppForm/Debtor/OtherEligibilityCriteria/OC_Q1)[1]', 'varchar(2000)'), --OC_Q1,
	--n.c.value('(AppForm/Debtor/OtherEligibilityCriteria/OC_Q1A)[1]', 'varchar(2000)'), --OC_Q1A,
	--n.c.value('(AppForm/Debtor/OtherEligibilityCriteria/OC_Q2)[1]', 'varchar(2000)'), --OC_Q2,
	@RLEOneOrTwoAdultsRaw = n.c.value('(AppForm/RLE/Details/RLE_Details_Q1)[1]', 'varchar(2000)'), --RLE_Details_Q1, one or two adults?
	@RLEAnyDependentChildren = n.c.value('(AppForm/RLE/Details/RLE_Details_Q2)[1]', 'varchar(2000)'), --RLE_Details_Q2, dependent children Y/N & 2A child age category/ies
	--	@RLEDependentChildrenAges = ?,
	@RLEOwnAMotor = n.c.value('(AppForm/RLE/Details/RLE_Details_Q3)[1]', 'varchar(2000)'), --RLE_Details_Q3, own a motor vehicle & 3A do you require it?
	@RLENeedAMotor = n.c.value('(AppForm/RLE/Details/RLE_Details_Q3_1)[1]', 'varchar(2000)'), --RLE_Details_Q3, own a motor vehicle & 3A do you require it?
	@RLEAnyChildcareCosts = n.c.value('(AppForm/RLE/Details/RLE_Details_Q4)[1]', 'varchar(2000)'), --RLE_Details_Q4, any childcare costs & 4A what is the amount?
	@RLEChildcareCosts = n.c.value('(AppForm/RLE/Details/RLE_Details_Q4_1)[1]', 'varchar(2000)'), --RLE_Details_Q4, any childcare costs & 4A what is the amount?
	@RLEMonthlyRent = n.c.value('(AppForm/RLE/Details/RLE_Details_Q5)[1]', 'varchar(2000)'), --RLE_Details_Q5, monthly rent/mortgage amount
	@RLESpecialCircumstances = n.c.value('(AppForm/RLE/Details/RLE_Details_Q6)[1]', 'varchar(2000)'), --RLE_Details_Q6, special circumstance costs
	@RLESpecialCircumstancesAmount = n.c.value('(AppForm/RLE/Details/RLE_Details_Q6_1)[1]', 'varchar(2000)'), --RLE_Details_Q6, special circumstance costs
	@RLEComment = n.c.value('(AppForm/RLE/Details/Comment)[1]', 'varchar(2000)'), --Comment,
	
	@JurisdictionCounty = n.c.value('(AppForm/Jurisdiction/County)[1]', 'varchar(2000)'), --County,
	@JurisdictionCourt = n.c.value('(AppForm/Jurisdiction/Court)[1]', 'varchar(2000)'), --Court,
	@JurisdictionReason = n.c.value('(AppForm/Jurisdiction/Reason)[1]', 'varchar(2000)'), --Reason,
	@CaseCreated = n.c.value('(KeyDates/CaseCreated)[1]', 'varchar(2000)'), --CaseCreated,
	@CaseCompleted = n.c.value('(KeyDates/CaseCompleted)[1]', 'varchar(2000)') --CaseCompleted
	FROM @Xml.nodes('ISICase') n(c) 


	/************************************************************************************************************************************************************/
	/*																																							*/
	/*																Create Cust/Lead/Case/Matter																*/
	/*																																							*/
	/************************************************************************************************************************************************************/

	/* Set Aquarium TitleID */
	SELECT @TitleID = t.TitleID 
	FROM dbo.Titles t WITH (NOLOCK) 
	WHERE t.Title = @Title 
	
	/* Merge address lines if necessary */
	IF @AddressLine3 > ''
	BEGIN
		SELECT @AddressLine2 += CASE WHEN @AddressLine2 = '' THEN '' ELSE ', ' END + @AddressLine3
	END
	
	/* Format dates so that a COMPUTER can read them */
	/* TODO */
	SET @DateOfBirth = '1990-03-17'
	
	/* Create the new Customer, Lead, Case, Matter */
	EXEC @CustomerID = dbo._C00_CreateNewCustomerFull @ClientID, @LeadTypeID, @ClientPersonnelID, @TitleID, 0, @FirstName, '', @LastName, @EmailAddress, @MobileTel, NULL, @HomeTel, NULL, @MobileTel, NULL, '', NULL, '', NULL, @AddressLine1, @AddressLine2, @Town, @County, @Postcode, '', 0, NULL, 2, NULL, 0, '', @Occupation, @EmployerName, NULL, NULL, NULL, NULL, @DateOfBirth, NULL, NULL, NULL, NULL, NULL, @CaseUNID, NULL, @TheDateTime, NULL, NULL, NULL, ''
	
	IF @CustomerID > 0
	BEGIN
		
		/* Get all the other new IDs to send back to the app */
		SELECT @LeadID = m.LeadID, 
		@CaseID = m.CaseID, 
		@MatterID = m.MatterID 
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID 
		WHERE l.CustomerID = @CustomerID 
		
		/* Add the process start event as it isnt added by the _C00_Create Customer SP */
		EXEC dbo._C00_AddProcessStart @CaseID, @ClientPersonnelID 
		
		/*
			Populate the Aquarium fields at the right levels (Customer/Matter)
		*/
		UPDATE TOP (1) dbo.Lead 
		SET LeadRef = @LeadRef 
		WHERE LeadID = @LeadID 
		
		/********************************************************************************************************************************************************/
		/*																																						*/
		/*															Encode ISI strings into Aquarium LookupListIDs												*/
		/*																																						*/
		/********************************************************************************************************************************************************/
		SELECT @ApplicationTypeLU = CASE @AppType WHEN 'Individual' THEN '64170' WHEN 'Joint' THEN '64171' ELSE '' END 
		SELECT @InterlockingLU = CASE @AppType WHEN 'Joint' THEN '5144' ELSE '5145' END /* Yes No */
		SELECT @BirthCountryLU = CASE @BirthCountry WHEN 'Ireland' THEN '114667' ELSE '' END /* TODO */
		SELECT @PrevNamesLU = CASE @PrevNames WHEN '' THEN '5145' ELSE '5144' END /* No Yes */
		SELECT @GenderLU = CASE @Gender WHEN 'Male' THEN '64995' ELSE '64996' END /* Male Female */
		
		/* Example: "P1Y2M" which is wierd, so turn that into "1Y2" and then get chars before/after the "Y" as years and months respectively */
		SELECT @LengthOfServiceRaw = REPLACE(REPLACE(@LengthOfServiceRaw, 'P', ''), 'M', '')
		SELECT @LengthOfServiceYears = dbo.fnGetCharsBeforeOrAfterSeparator(@LengthOfServiceRaw, ',', 1, 1), 
		@LengthOfServiceMonths = dbo.fnGetCharsBeforeOrAfterSeparator(@LengthOfServiceRaw, ',', 1, 0)

		SELECT @RLEOneOrTwoAdultsLU = CASE WHEN @RLEOneOrTwoAdultsRaw = '1' THEN '64917' WHEN @RLEOneOrTwoAdultsRaw LIKE 'One%' THEN '64917' WHEN @RLEOneOrTwoAdultsRaw = '2' THEN '64918' WHEN @RLEOneOrTwoAdultsRaw LIKE 'Two%' THEN '64918' ELSE '' END 
		SELECT @RLEAnyDependentChildrenLU = CASE @RLEAnyDependentChildren WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		SELECT @RLEOwnAMotorLU = CASE @RLEOwnAMotor WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		SELECT @RLENeedAMotorLU = CASE @RLENeedAMotor WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		SELECT @RLEAnyChildcareCostsLU = CASE @RLEAnyChildcareCosts WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Yes/No */
		
		/* Create CustomerDetailValues */
		EXEC dbo._C00_SimpleValueIntoField 164078, @PIPName, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208324, @PIPRef, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163943, @BirthCertName, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 177101, @PPSN, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163946, @PPSN, @CustomerID, @ClientPersonnelID /* Yes, deliberate repeated use of @PPSN */
		EXEC dbo._C00_SimpleValueIntoField 163945, @GenderLU, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163948, @Nationality, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163947, @BirthCountryLU, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208327, @EmploymentStatus, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163944, @PrevNamesLU, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208774, @PrevNames, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163933, @AQ_PIA_Q1, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163934, @AQ_PIA_Q2, @CustomerID, @ClientPersonnelID
		--EXEC dbo._C00_SimpleValueIntoField TBA, @AQ_PIA_Q2A, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208818, @AQ_PIA_Q3, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208819, @AQ_PIA_Q4, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208820, @AQ_PIA_Q5, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 208821, @AQ_PIA_Q6, @CustomerID, @ClientPersonnelID
		
		EXEC dbo._C00_SimpleValueIntoField 163937, @INIT_Q1, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163938, @INIT_Q2, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163939, @INIT_Q3, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163940, @INIT_Q4, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163941, @INIT_Q5, @CustomerID, @ClientPersonnelID
		
		
		EXEC dbo._C00_SimpleValueIntoField 208775, @PersonalComment, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163956, @JurisdictionCounty, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163958, @JurisdictionCourt, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163957, @JurisdictionReason, @CustomerID, @ClientPersonnelID
		
		/* Employment */
		SET @EmployerCountry = 12851 /* Ireland */
		EXEC dbo._C00_SimpleValueIntoField 209111, @EmployerCountry, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 162628, @EmployerAddressLine1, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209107, @EmployerAddressLine2, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209108, @EmployerAddressLine3, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209109, @EmployerTown, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209110, @EmployerCounty, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 162629, @LengthOfServiceYears, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 163954, @LengthOfServiceMonths, @CustomerID, @ClientPersonnelID
		
		/* RLE */
		EXEC dbo._C00_SimpleValueIntoField 163451, @RLEOneOrTwoAdultsLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209114, @RLEAnyDependentChildrenLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209115, @RLEDependentChildrenAges, @CustomerID, @ClientPersonnelID /* TODO: not in XML, populate our table? LU? */
		EXEC dbo._C00_SimpleValueIntoField 209118, @RLEOwnAMotorLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209119, @RLENeedAMotorLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209120, @RLEAnyChildcareCostsLU, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209121, @RLEChildcareCosts, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209122, @RLEMonthlyRent, @CustomerID, @ClientPersonnelID
		--EXEC dbo._C00_SimpleValueIntoField , @RLESpecialCircumstances, @CustomerID, @ClientPersonnelID /* TODO: LU and new field required */
		EXEC dbo._C00_SimpleValueIntoField 163676, @RLESpecialCircumstancesAmount, @CustomerID, @ClientPersonnelID 
		EXEC dbo._C00_SimpleValueIntoField 209124, @RLEComment, @CustomerID, @ClientPersonnelID
		
		EXEC dbo._C00_SimpleValueIntoField 209278, @CaseCreated, @CustomerID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209279, @CaseCompleted, @CustomerID, @ClientPersonnelID
		
		
		/* Create MatterDetailValues */
		EXEC dbo._C00_SimpleValueIntoField 162357, @ApplicationTypeLU, @MatterID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209128, @InterlockingLU, @MatterID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209129, @OwnRefNo, @MatterID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 209130, @OwnDescription, @MatterID, @ClientPersonnelID
		--@CaseType
		--@CaseDescription
		
		/********************************************************************************************************************************************************/
		/*																																						*/
		/*															Populate tables from repeating nodes														*/
		/*																																						*/
		/********************************************************************************************************************************************************/
	
		/********************************************************************************************************************************************************/
		/*															RLE Dependent Children																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblChildren TABLE (
			UNID INT IDENTITY(1,1), 
			Category VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		/* Insert the childred into a table variable*/
		INSERT INTO @TblChildren (
			Category, 
			Comment
		)
		SELECT 
		n.c.value('(Category)[1]', 'varchar(2000)') AS Category,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//RLE/Child') n(c);

		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 209115, 18170, c.UNID 
			FROM @TblChildren c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblChildren c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(209116) /* Category */
				   ,(209117) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 209116 THEN c.Category /* TODO: LU? */
									WHEN 209117 THEN c.Comment
									ELSE ''
									END
			FROM @TblChildren c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields
		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset PrincipalPrivateResidence															*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetPPR TABLE (
			UNID INT IDENTITY(1,1), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetPPR (
			AddressLine1,
			AddressLine2,
			AddressLine3,
			AddressTown,
			AddressCounty,
			OriginalCost,
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			DebtorOwnership, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(AssetDetail/OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(AssetDetail/PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(AssetDetail/PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(AssetDetail/CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(AssetDetail/DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment 
		FROM @xml.nodes('//PFS/Asset/PrincipalPrivateResidence') n(c); 

		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208657, 21165, c.UNID 
			FROM @TblAssetPPR c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetPPR c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208646) /* Line 1 */
				   ,(208647) /* Line 2 */
				   ,(208648) /* Line 3 */
				   ,(208649) /* Town */
				   ,(208650) /* TODO: this is the ZIP field but should be County (we only have a ZIP field and Country, not County) */
				   ,(208651) /* Original Cost  */
				   ,(208652) /* Purchase Date Month */
				   ,(208653) /* Purchase Date Year */
				   ,(208654) /* Current Market Value */
				   ,(208655) /* Debtor Ownership (we have ownership % and interest value columns)*/
				   ,(209281) /* Asset Unique ID field missing */
				   ,(208656) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208646 THEN c.AddressLine1 
									WHEN 208647 THEN c.AddressLine2
									WHEN 208648 THEN c.AddressLine3
									WHEN 208649 THEN c.AddressTown
									WHEN 208650 THEN c.AddressCounty
									WHEN 208651 THEN c.OriginalCost
									WHEN 208652 THEN CAST(CAST(CASE WHEN c.PurchaseMonth BETWEEN 1 AND 12 THEN (116695 + c.PurchaseMonth) ELSE '' END AS VARCHAR) AS VARCHAR) /* Luli items 116696 to 116707 consecutively */
									WHEN 208653 THEN c.PurchaseYear
									WHEN 208654 THEN c.CurrentMarketValue
									WHEN 208655 THEN c.DebtorOwnership
									WHEN 209281 THEN c.AssetUniqueId
									WHEN 208656 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetPPR c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END

		
		/********************************************************************************************************************************************************/
		/*															PFS Asset InvestmentProperty																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetInvestmentProperty TABLE (
			UNID INT IDENTITY(1,1), 
			PType VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Title VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			MonthlyIncome VARCHAR(2000), 
			MonthlyExpenditure VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetInvestmentProperty (
			PType, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Title, 
			OriginalCost, 
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			DebtorOwnership, 
			MonthlyIncome, 
			MonthlyExpenditure, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Type)[1]', 'varchar(2000)') AS Type,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Title)[1]', 'varchar(2000)') AS Title,
		n.c.value('(AssetDetail/OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(AssetDetail/PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(AssetDetail/PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(AssetDetail/CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(AssetDetail/DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetDetail/MonthlyIncome)[1]', 'varchar(2000)') AS MonthlyIncome,
		n.c.value('(MonthlyExpenditure)[1]', 'varchar(2000)') AS MonthlyExpenditure,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/InvestmentProperty') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208675, 21165, c.UNID 
			FROM @TblAssetInvestmentProperty c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetInvestmentProperty c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208658) /* PType */
				   ,(208660) /* Line1 */
				   ,(208661) /* Line2 */
				   ,(208662) /* Line3 */
				   ,(208663) /* Town */
				   ,(208664) /* County */
				   ,(208665) /* Title */
				   ,(208666) /* OriginalCost */
				   ,(208667) /* PurchaseMonth */
				   ,(208668) /* PurchaseYear */
				   ,(208669) /* CurrentMarketValue */
				   ,(208670) /* DebtorOwnership */
				   ,(208671) /* MonthlyIncome */
				   ,(208672) /* MonthlyExpenditure */
				   ,(209282) /* Asset Unique ID field missing */
				   ,(208673) /* Comment */
			
			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208658 THEN c.PType
									WHEN 208660 THEN c.AddressLine1
									WHEN 208661 THEN c.AddressLine2
									WHEN 208662 THEN c.AddressLine3
									WHEN 208663 THEN c.AddressTown
									WHEN 208664 THEN c.AddressCounty
									WHEN 208665 THEN c.Title
									WHEN 208666 THEN c.OriginalCost
									WHEN 208667 THEN CAST(CASE WHEN c.PurchaseMonth BETWEEN 1 AND 12 THEN (116695 + c.PurchaseMonth) ELSE '' END AS VARCHAR) /* Luli items 116696 to 116707 consecutively */
									WHEN 208668 THEN c.PurchaseYear
									WHEN 208669 THEN c.CurrentMarketValue
									WHEN 208670 THEN c.DebtorOwnership
									WHEN 208671 THEN c.MonthlyIncome
									WHEN 208672 THEN c.MonthlyExpenditure
									WHEN 209282 THEN c.AssetUniqueId
									WHEN 208673 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetInvestmentProperty c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields
			
		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset PlantEquipmentTools																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetPlantEquipmentTools TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetPlantEquipmentTools (
			PDescription, 
			OriginalCost, 
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			DebtorOwnership, 
			IsAssetInTheState, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(InvestmentDetails/OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(InvestmentDetails/PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(InvestmentDetails/PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(InvestmentDetails/CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(InvestmentDetails/DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/PlantEquipmentTools') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208698, 21165, c.UNID 
			FROM @TblAssetPlantEquipmentTools c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetPlantEquipmentTools c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208689) /* PDescription */
				   ,(208690) /* OriginalCost */
				   ,(208691) /* PurchaseMonth */
				   ,(208692) /* PurchaseYear */
				   ,(208693) /* CurrentMarketValue */
				   ,(208694) /* DebtorOwnership */
				   ,(208696) /* IsAssetInTheState */
				   ,(209284) /* Asset Unique ID field missing */
				   ,(208697) /* Comment */
			
			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208689 THEN c.PDescription
									WHEN 208690 THEN c.OriginalCost
									WHEN 208691 THEN CAST(CASE WHEN c.PurchaseMonth BETWEEN 1 AND 12 THEN (116695 + c.PurchaseMonth) ELSE '' END AS VARCHAR) /* Luli items 116696 to 116707 consecutively */
									WHEN 208692 THEN c.PurchaseYear
									WHEN 208693 THEN c.CurrentMarketValue
									WHEN 208694 THEN c.DebtorOwnership
									WHEN 208696 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209284 THEN c.AssetUniqueId
									WHEN 208697 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetPlantEquipmentTools c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields
			
		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset Vehicle																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetVehicle TABLE (
			UNID INT IDENTITY(1,1), 
			Make VARCHAR(2000), 
			Model VARCHAR(2000), 
			Year VARCHAR(2000), 
			RegistrationNumber VARCHAR(2000), 
			Mileage VARCHAR(2000), 
			NeedForVehicle VARCHAR(2000), 
			OriginalCost VARCHAR(2000), 
			PurchaseMonth VARCHAR(2000), 
			PurchaseYear VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			SubjectToFinance VARCHAR(2000), 
			AdaptedForDisabledUse VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetVehicle (
			Make,
			Model,
			Year,
			RegistrationNumber,
			Mileage,
			NeedForVehicle,
			OriginalCost,
			PurchaseMonth, 
			PurchaseYear, 
			CurrentMarketValue, 
			SubjectToFinance, 
			AdaptedForDisabledUse, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Make)[1]', 'varchar(2000)') AS Make,
		n.c.value('(Model)[1]', 'varchar(2000)') AS Model,
		n.c.value('(Year)[1]', 'varchar(2000)') AS Year,
		n.c.value('(RegistrationNumber)[1]', 'varchar(2000)') AS RegistrationNumber,
		n.c.value('(Milage)[1]', 'varchar(2000)') AS Milage,
		n.c.value('(NeedForVehicle)[1]', 'varchar(2000)') AS NeedForVehicle,
		n.c.value('(OriginalCost)[1]', 'varchar(2000)') AS OriginalCost,
		n.c.value('(PurchaseMonth)[1]', 'varchar(2000)') AS PurchaseMonth,
		n.c.value('(PurchaseYear)[1]', 'varchar(2000)') AS PurchaseYear,
		n.c.value('(CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(SubjectToFinance)[1]', 'varchar(2000)') AS SubjectToFinance,
		n.c.value('(AdaptedForDisabledUse)[1]', 'varchar(2000)') AS AdaptedForDisabledUse,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/Vehicle') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208713, 21165, c.UNID 
			FROM @TblAssetVehicle c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetVehicle c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208699) /* Make */
				   ,(208700) /* Model */
				   ,(208701) /* Year YYYY */
				   ,(208702) /* Registration Number */
				   ,(208703) /* Milage (Kilometres) ("milage" in the XML") */
				   ,(208704) /* Need for vehicle */
				   ,(208705) /* Original Cost */
				   ,(208706) /* Purchase Date Month */
				   ,(208707) /* Purchase Date Year */
				   ,(208708) /* Current Market Value */
				   ,(208709) /* Subject to Finance */
				   ,(208710) /* Adapted for disabled use */
				   ,(208711) /* Is the asset located in the state */
				   ,(209285) /* Asset Unique ID field missing */
				   ,(208712) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208699 THEN c.Make 
									WHEN 208700 THEN c.Model
									WHEN 208701 THEN c.Year
									WHEN 208702 THEN c.RegistrationNumber
									WHEN 208703 THEN c.Mileage
									WHEN 208704 THEN c.NeedForVehicle
									WHEN 208705 THEN c.OriginalCost
									WHEN 208706 THEN CAST(CASE WHEN c.PurchaseMonth BETWEEN 1 AND 12 THEN (116695 + c.PurchaseMonth) ELSE '' END AS VARCHAR) /* Luli items 116696 to 116707 consecutively */
									WHEN 208707 THEN c.PurchaseYear
									WHEN 208708 THEN c.CurrentMarketValue
									WHEN 208709 THEN CASE c.SubjectToFinance WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208710 THEN CASE c.AdaptedForDisabledUse WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208711 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209285 THEN c.AssetUniqueId
									WHEN 208712 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetVehicle c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset StockInTrade																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetStockInTrade TABLE (
			UNID INT IDENTITY(1,1), 
			CurrentMarketValue VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetStockInTrade (
			CurrentMarketValue,
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(CurrentMarketValue)[1]', 'varchar(2000)') AS Make,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/StockInTrade') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208717, 21165, c.UNID 
			FROM @TblAssetStockInTrade c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetStockInTrade c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208714) /* Current Market Value */
				   ,(208715) /* Is the asset located in the state */
				   ,(209286) /* Asset Unique ID field missing */
				   ,(208716) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208714 THEN c.CurrentMarketValue
									WHEN 208715 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209286 THEN c.AssetUniqueId
									WHEN 208716 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetStockInTrade c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END

		
		/********************************************************************************************************************************************************/
		/*															PFS Asset MoneyOwedToYou																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetMoneyOwedToYou TABLE (
			UNID INT IDENTITY(1,1), 
			DebtorName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			BookValue VARCHAR(2000), 
			RealisableValue VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetMoneyOwedToYou (
			DebtorName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			BookValue, 
			RealisableValue, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(DebtorName)[1]', 'varchar(2000)') AS DebtorName,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(BookValue)[1]', 'varchar(2000)') AS BookValue,
		n.c.value('(RealisableValue)[1]', 'varchar(2000)') AS RealisableValue,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/MoneyOwedToYou') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208728, 21165, c.UNID 
			FROM @TblAssetMoneyOwedToYou c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetMoneyOwedToYou c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208718) /* DebtorName */
				   ,(208720) /* Line1 */
				   ,(208721) /* Line2 */
				   ,(208722) /* Line3 */
				   ,(208723) /* Town */
				   ,(208724) /* County */
				   ,(208725) /* BookValue */
				   ,(208726) /* RealisableValue */
				   ,(209287) /* Asset Unique ID field missing */
				   ,(208727) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208718 THEN c.DebtorName
									WHEN 208720 THEN c.AddressLine1
									WHEN 208721 THEN c.AddressLine2
									WHEN 208722 THEN c.AddressLine3
									WHEN 208723 THEN c.AddressTown
									WHEN 208724 THEN c.AddressCounty
									WHEN 208725 THEN c.BookValue
									WHEN 208726 THEN c.RealisableValue
									WHEN 209287 THEN c.AssetUniqueId
									WHEN 208727 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetMoneyOwedToYou c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
					
		/********************************************************************************************************************************************************/
		/*															PFS Asset BankAccount																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetBankAccount TABLE (
			UNID INT IDENTITY(1,1), 
			BankName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			IBANBIC VARCHAR(2000), 
			Balance VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetBankAccount (
			BankName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			AccountName, 
			AccountNumber, 
			IBANBIC, 
			Balance, 
			DebtorOwnership, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(BankName)[1]', 'varchar(2000)') AS BankName,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(IBANBIC)[1]', 'varchar(2000)') AS IBANBIC,
		n.c.value('(Balance)[1]', 'varchar(2000)') AS Balance,
		n.c.value('(DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/BankAccount') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208743, 21165, c.UNID 
			FROM @TblAssetBankAccount c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetBankAccount c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208729) /* BankName */
				   ,(208731) /* Line1 */
				   ,(208732) /* Line2 */
				   ,(208733) /* Line3 */
				   ,(208734) /* Town */
				   ,(208735) /* County */
				   ,(208736) /* AccountName */
				   ,(208737) /* AccountNumber */
				   ,(208738) /* IBANBIC */
				   ,(208739) /* Balance */
				   ,(208740) /* DebtorOwnership */
				   ,(209288) /* Asset Unique ID field missing */
				   ,(208742) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208729 THEN c.BankName
									WHEN 208731 THEN c.AddressLine1
									WHEN 208732 THEN c.AddressLine2
									WHEN 208733 THEN c.AddressLine3
									WHEN 208734 THEN c.AddressTown
									WHEN 208735 THEN c.AddressCounty
									WHEN 208736 THEN c.AccountName
									WHEN 208737 THEN c.AccountNumber
									WHEN 208738 THEN c.IBANBIC
									WHEN 208739 THEN c.Balance
									WHEN 208740 THEN c.DebtorOwnership
									WHEN 209288 THEN c.AssetUniqueId
									WHEN 208742 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetBankAccount c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset CreditUnionAccount																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetCreditUnionAccount TABLE (
			UNID INT IDENTITY(1,1), 
			CreditUnionName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			CurrentMarketValue VARCHAR(2000), 
			DebtorOwnership VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetCreditUnionAccount (
			CreditUnionName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			AccountName, 
			AccountNumber, 
			CurrentMarketValue, 
			DebtorOwnership, 
 			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(CreditUnionName)[1]', 'varchar(2000)') AS CreditUnionName,
		n.c.value('(Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(CurrentMarketValue)[1]', 'varchar(2000)') AS CurrentMarketValue,
		n.c.value('(DebtorOwnership)[1]', 'varchar(2000)') AS DebtorOwnership,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/CreditUnionAccount') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208757, 21165, c.UNID 
			FROM @TblAssetCreditUnionAccount c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetCreditUnionAccount c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208744) /* CreditUnionName */
				   ,(208746) /* Line1 */
				   ,(208747) /* Line2 */
				   ,(208748) /* Line3 */
				   ,(208749) /* Town */
				   ,(208750) /* County */
				   ,(208751) /* AccountName */
				   ,(208752) /* AccountNumber */
				   ,(208753) /* CurrentMarketValue */
				   ,(208754) /* DebtorOwnership */
				   ,(209289) /* Asset Unique ID field missing */
				   ,(208756) /* Comment */

			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208744 THEN c.CreditUnionName
									WHEN 208746 THEN c.AddressLine1
									WHEN 208747 THEN c.AddressLine2
									WHEN 208748 THEN c.AddressLine3
									WHEN 208749 THEN c.AddressTown
									WHEN 208750 THEN c.AddressCounty
									WHEN 208751 THEN c.AccountName
									WHEN 208752 THEN c.AccountNumber
									WHEN 208753 THEN c.CurrentMarketValue
									WHEN 208754 THEN c.DebtorOwnership
									WHEN 209289 THEN c.AssetUniqueId
									WHEN 208756 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetCreditUnionAccount c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset CashOnHand																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetCashOnHand TABLE (
			UNID INT IDENTITY(1,1), 
			Amount VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetCashOnHand (
			Amount,
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Amount)[1]', 'varchar(2000)') AS Amount,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/CashOnHand') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208761, 21165, c.UNID 
			FROM @TblAssetCashOnHand c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetCashOnHand c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208758) /* Amount */
				   ,(208759) /* Is the asset located in the state */
				   ,(209290) /* Asset Unique ID field missing */
				   ,(208760) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208758 THEN c.Amount
									WHEN 208759 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209290 THEN c.AssetUniqueId
									WHEN 208760 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetCashOnHand c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset ProspectiveAsset																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetProspectiveAsset TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			ReceiptEstimatedDate VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetProspectiveAsset (
			PDescription, 
			EstimatedValue, 
			ReceiptEstimatedDate, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(ReceiptEstimatedDate)[1]', 'varchar(2000)') AS ReceiptEstimatedDate,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/ProspectiveAsset') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208767, 21165, c.UNID 
			FROM @TblAssetProspectiveAsset c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetProspectiveAsset c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208762) /* PDescription */
				   ,(208763) /* EstimatedValue */
				   ,(208764) /* ReceiptEstimatedDate */
				   ,(208765) /* Is the asset located in the state */
				   ,(209291 ) /* Asset Unique ID field missing */
				   ,(208766) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208762 THEN c.PDescription
									WHEN 208763 THEN c.EstimatedValue
									WHEN 208764 THEN c.ReceiptEstimatedDate
									WHEN 208765 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209291 THEN c.AssetUniqueId
									WHEN 208766 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetProspectiveAsset c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset ContingentAsset																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetContingentAsset TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			ReceiptEstimatedDate VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetContingentAsset (
			PDescription, 
			EstimatedValue, 
			ReceiptEstimatedDate, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(ReceiptEstimatedDate)[1]', 'varchar(2000)') AS ReceiptEstimatedDate,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/ContingentAsset') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208768, 21165, c.UNID 
			FROM @TblAssetContingentAsset c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetContingentAsset c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208762) /* PDescription */
				   ,(208763) /* EstimatedValue */
				   ,(208764) /* ReceiptEstimatedDate */
				   ,(208765) /* Is the asset located in the state */
				   ,(209291 ) /* Asset Unique ID field missing */
				   ,(208766) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208762 THEN c.PDescription
									WHEN 208763 THEN c.EstimatedValue
									WHEN 208764 THEN c.ReceiptEstimatedDate
									WHEN 208765 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209291  THEN c.AssetUniqueId
									WHEN 208766 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetContingentAsset c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Asset Other																				*/
		/********************************************************************************************************************************************************/
		DECLARE @TblAssetOther TABLE (
			UNID INT IDENTITY(1,1), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			IsAssetInTheState VARCHAR(2000), 
			AssetUniqueId VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblAssetOther (
			PDescription, 
			EstimatedValue, 
			IsAssetInTheState, 
			AssetUniqueId, 
			Comment
		)
		SELECT 
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(IsAssetInTheState)[1]', 'varchar(2000)') AS IsAssetInTheState,
		n.c.value('(AssetUniqueId)[1]', 'varchar(2000)') AS AssetUniqueId,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Asset/Other') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208773, 21165, c.UNID 
			FROM @TblAssetOther c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblAssetOther c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208769) /* PDescription */
				   ,(208770) /* EstimatedValue */
				   ,(208771) /* Is the asset located in the state */
				   ,(209292) /* Asset Unique ID field missing */
				   ,(208772) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208769 THEN c.PDescription
									WHEN 208770 THEN c.EstimatedValue
									WHEN 208771 THEN CASE c.IsAssetInTheState WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 209292 THEN c.AssetUniqueId
									WHEN 208772 THEN c.Comment
									ELSE ''
									END
			FROM @TblAssetOther c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt PPRL = Principal Private Residence Lender											*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtPPRL TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			IBANBIC VARCHAR(2000), 
			MonthlyRepaymentContract VARCHAR(2000), 
			MonthlyRepaymentActual VARCHAR(2000), 
			RemainingTerm VARCHAR(2000), 
			OriginalAmountBorrowed VARCHAR(2000), 
			PurposeOfLoan VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			JointAndSeveralLiability VARCHAR(2000), 
			ArrearsIncludedInAmountDue VARCHAR(2000), 
			Restructured VARCHAR(2000), 
			CurrentInterestRate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtPPRL (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AccountName, 
			AccountNumber, 
			IBANBIC, 
			MonthlyRepaymentContract, 
			MonthlyRepaymentActual, 
			RemainingTerm, 
			OriginalAmountBorrowed, 
			PurposeOfLoan, 
			AmountDue, 
			JointAndSeveralLiability, 
			ArrearsIncludedInAmountDue, 
			Restructured, 
			CurrentInterestRate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(IBANBIC)[1]', 'varchar(2000)') AS IBANBIC,
		n.c.value('(MonthlyRepaymentContract)[1]', 'varchar(2000)') AS MonthlyRepaymentContract,
		n.c.value('(MonthlyRepaymentActual)[1]', 'varchar(2000)') AS MonthlyRepaymentActual,
		n.c.value('(RemainingTerm)[1]', 'varchar(2000)') AS RemainingTerm,
		n.c.value('(OriginalAmountBorrowed)[1]', 'varchar(2000)') AS OriginalAmountBorrowed,
		n.c.value('(PurposeOfLoan)[1]', 'varchar(2000)') AS PurposeOfLoan,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(JointAndSeveralLiability)[1]', 'varchar(2000)') AS JointAndSeveralLiability,
		n.c.value('(ArrearsIncludedInAmountDue)[1]', 'varchar(2000)') AS ArrearsIncludedInAmountDue,
		n.c.value('(Restructured)[1]', 'varchar(2000)') AS Restructured,
		n.c.value('(CurrentInterestRate)[1]', 'varchar(2000)') AS CurrentInterestRate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/PPRL') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208856, 21180, c.UNID 
			FROM @TblPFSDebtPPRL c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtPPRL c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208847) /* DebtSecured */
				   ,(208823) /* OrganisationName */
				   ,(208825) /* AddressLine1 */
				   ,(208826) /* AddressLine2 */
				   ,(208827) /* AddressLine3 */
				   ,(208828) /* AddressTown */
				   ,(208829) /* TODO: should be AddressCounty but says ZIP in Aquarium */
				   ,(208830) /* Email */
				   ,(208831) /* PhoneNumber */
				   ,(208832) /* Incurred6Month */
				   ,(208835) /* AccountName */
				   ,(208833) /* AccountNumber */
				   ,(208834) /* IBANBIC */
				   ,(208836) /* MonthlyRepaymentContract */
				   ,(208837) /* MonthlyRepaymentActual */
				   ,(208838) /* RemainingTerm */
				   ,(208839) /* OriginalAmountBorrowed */
				   ,(208840) /* PurposeOfLoan */
				   ,(208841) /* AmountDue */
				   ,(208842) /* JointAndSeveralLiability */
				   ,(208843) /* ArrearsIncludedInAmountDue */
				   ,(208844) /* Restructured */
				   ,(208845) /* CurrentInterestRate */
				   ,(208846) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208847 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208823 THEN c.OrganisationName
									WHEN 208825 THEN c.AddressLine1
									WHEN 208826 THEN c.AddressLine2
									WHEN 208827 THEN c.AddressLine3
									WHEN 208828 THEN c.AddressTown
									WHEN 208829 THEN c.AddressCounty
									WHEN 208830 THEN c.Email
									WHEN 208831 THEN c.PhoneNumber
									WHEN 208832 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208835 THEN c.AccountName
									WHEN 208833 THEN c.AccountNumber
									WHEN 208834 THEN c.IBANBIC
									WHEN 208836 THEN c.MonthlyRepaymentContract
									WHEN 208837 THEN c.MonthlyRepaymentActual
									WHEN 208838 THEN c.RemainingTerm
									WHEN 208839 THEN c.OriginalAmountBorrowed
									WHEN 208840 THEN c.PurposeOfLoan
									WHEN 208841 THEN c.AmountDue
									WHEN 208842 THEN CASE c.JointAndSeveralLiability WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208843 THEN CASE c.ArrearsIncludedInAmountDue WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208844 THEN CASE c.Restructured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208845 THEN c.CurrentInterestRate
									WHEN 208846 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtPPRL c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		

		/********************************************************************************************************************************************************/
		/*															PFS Debt FinancialInstitutions																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtFinancialInstitutions TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			IBANBIC VARCHAR(2000), 
			MonthlyRepaymentContract VARCHAR(2000), 
			MonthlyRepaymentActual VARCHAR(2000), 
			RemainingTerm VARCHAR(2000), 
			OriginalAmountBorrowed VARCHAR(2000), 
			PurposeOfLoan VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			JointAndSeveralLiability VARCHAR(2000), 
			ArrearsIncludedInAmountDue VARCHAR(2000), 
			Restructured VARCHAR(2000), 
			CurrentInterestRate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtFinancialInstitutions (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AccountName, 
			AccountNumber, 
			IBANBIC, 
			MonthlyRepaymentContract, 
			MonthlyRepaymentActual, 
			RemainingTerm, 
			OriginalAmountBorrowed, 
			PurposeOfLoan, 
			AmountDue, 
			JointAndSeveralLiability, 
			ArrearsIncludedInAmountDue, 
			Restructured, 
			CurrentInterestRate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(IBANBIC)[1]', 'varchar(2000)') AS IBANBIC,
		n.c.value('(MonthlyRepaymentContract)[1]', 'varchar(2000)') AS MonthlyRepaymentContract,
		n.c.value('(MonthlyRepaymentActual)[1]', 'varchar(2000)') AS MonthlyRepaymentActual,
		n.c.value('(RemainingTerm)[1]', 'varchar(2000)') AS RemainingTerm,
		n.c.value('(OriginalAmountBorrowed)[1]', 'varchar(2000)') AS OriginalAmountBorrowed,
		n.c.value('(PurposeOfLoan)[1]', 'varchar(2000)') AS PurposeOfLoan,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(JointAndSeveralLiability)[1]', 'varchar(2000)') AS JointAndSeveralLiability,
		n.c.value('(ArrearsIncludedInAmountDue)[1]', 'varchar(2000)') AS ArrearsIncludedInAmountDue,
		n.c.value('(Restructured)[1]', 'varchar(2000)') AS Restructured,
		n.c.value('(CurrentInterestRate)[1]', 'varchar(2000)') AS CurrentInterestRate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/FinancialInstitutions') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208886, 21180, c.UNID 
			FROM @TblPFSDebtFinancialInstitutions c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtFinancialInstitutions c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208882) /* DebtSecured */
				   ,(208857) /* OrganisationName */
				   ,(208859) /* AddressLine1 */
				   ,(208860) /* AddressLine2 */
				   ,(208861) /* AddressLine3 */
				   ,(208862) /* AddressTown */
				   ,(208863) /* AddressCounty */
				   ,(208864) /* Email */
				   ,(208865) /* PhoneNumber */
				   ,(208866) /* Incurred6Month */
				   ,(208869) /* AccountName */
				   ,(208867) /* AccountNumber */
				   ,(208868) /* IBANBIC */
				   ,(208872) /* MonthlyRepaymentContract */
				   ,(208873) /* MonthlyRepaymentActual */
				   ,(208874) /* RemainingTerm (months)*/
				   ,(208875) /* OriginalAmountBorrowed */
				   ,(208876) /* PurposeOfLoan */
				   ,(208877) /* AmountDue */
				   ,(208878) /* JointAndSeveralLiability */
				   ,(208879) /* ArrearsIncludedInAmountDue */
				   ,(208880) /* Restructured */
				   ,(208881) /* CurrentInterestRate */
				   --,() /* TODO: Comment field not present yet */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208882 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208857 THEN c.OrganisationName
									WHEN 208859 THEN c.AddressLine1
									WHEN 208860 THEN c.AddressLine2
									WHEN 208861 THEN c.AddressLine3
									WHEN 208862 THEN c.AddressTown
									WHEN 208863 THEN c.AddressCounty
									WHEN 208864 THEN c.Email
									WHEN 208865 THEN c.PhoneNumber
									WHEN 208866 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208869 THEN c.AccountName
									WHEN 208867 THEN c.AccountNumber
									WHEN 208868 THEN c.IBANBIC
									WHEN 208872 THEN c.MonthlyRepaymentContract
									WHEN 208873 THEN c.MonthlyRepaymentActual
									WHEN 208874 THEN c.RemainingTerm
									WHEN 208875 THEN c.OriginalAmountBorrowed
									WHEN 208876 THEN c.PurposeOfLoan
									WHEN 208877 THEN c.AmountDue
									WHEN 208878 THEN CASE c.JointAndSeveralLiability WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208879 THEN CASE c.ArrearsIncludedInAmountDue WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208880 THEN CASE c.Restructured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208881 THEN c.CurrentInterestRate
									--WHEN  THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtFinancialInstitutions c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END
		

		/********************************************************************************************************************************************************/
		/*															PFS Debt CreditUnion																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtCreditUnion TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AccountName VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			MonthlyRepaymentContract VARCHAR(2000), 
			MonthlyRepaymentActual VARCHAR(2000), 
			RemainingTerm VARCHAR(2000), 
			OriginalAmountBorrowed VARCHAR(2000), 
			PurposeOfLoan VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			JointAndSeveralLiability VARCHAR(2000), 
			ArrearsIncludedInAmountDue VARCHAR(2000), 
			Restructured VARCHAR(2000), 
			RestructuringDetails VARCHAR(2000), 
			CurrentInterestRate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtCreditUnion (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AccountName, 
			AccountNumber, 
			MonthlyRepaymentContract, 
			MonthlyRepaymentActual, 
			RemainingTerm, 
			OriginalAmountBorrowed, 
			PurposeOfLoan, 
			AmountDue, 
			JointAndSeveralLiability, 
			ArrearsIncludedInAmountDue, 
			Restructured, 
			RestructuringDetails, 
			CurrentInterestRate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AccountName)[1]', 'varchar(2000)') AS AccountName,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(MonthlyRepaymentContract)[1]', 'varchar(2000)') AS MonthlyRepaymentContract,
		n.c.value('(MonthlyRepaymentActual)[1]', 'varchar(2000)') AS MonthlyRepaymentActual,
		n.c.value('(RemainingTerm)[1]', 'varchar(2000)') AS RemainingTerm,
		n.c.value('(OriginalAmountBorrowed)[1]', 'varchar(2000)') AS OriginalAmountBorrowed,
		n.c.value('(PurposeOfLoan)[1]', 'varchar(2000)') AS PurposeOfLoan,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(JointAndSeveralLiability)[1]', 'varchar(2000)') AS JointAndSeveralLiability,
		n.c.value('(ArrearsIncludedInAmountDue)[1]', 'varchar(2000)') AS ArrearsIncludedInAmountDue,
		n.c.value('(Restructured)[1]', 'varchar(2000)') AS Restructured,
		n.c.value('(RestructuringDetails)[1]', 'varchar(2000)') AS RestructuringDetails,
		n.c.value('(CurrentInterestRate)[1]', 'varchar(2000)') AS CurrentInterestRate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/CreditUnion') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208887, 21180, c.UNID 
			FROM @TblPFSDebtCreditUnion c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtCreditUnion c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208882) /* DebtSecured */
				   ,(208857) /* OrganisationName */
				   ,(208859) /* AddressLine1 */
				   ,(208860) /* AddressLine2 */
				   ,(208861) /* AddressLine3 */
				   ,(208862) /* AddressTown */
				   ,(208863) /* AddressCounty */
				   ,(208864) /* Email */
				   ,(208865) /* PhoneNumber */
				   ,(208866) /* Incurred6Month */
				   ,(208869) /* AccountName */
				   ,(208867) /* AccountNumber */
				   ,(208872) /* MonthlyRepaymentContract */
				   ,(208873) /* MonthlyRepaymentActual */
				   ,(208874) /* RemainingTerm (months)*/
				   ,(208875) /* OriginalAmountBorrowed */
				   ,(208876) /* PurposeOfLoan */
				   ,(208877) /* AmountDue */
				   ,(208878) /* JointAndSeveralLiability */
				   ,(208879) /* ArrearsIncludedInAmountDue */
				   ,(208880) /* Restructured */
				   --,(2) /* RestructuringDetails */
				   ,(208881) /* CurrentInterestRate */
				   --,(2) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208882 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208857 THEN c.OrganisationName
									WHEN 208859 THEN c.AddressLine1
									WHEN 208860 THEN c.AddressLine2
									WHEN 208861 THEN c.AddressLine3
									WHEN 208862 THEN c.AddressTown
									WHEN 208863 THEN c.AddressCounty
									WHEN 208864 THEN c.Email
									WHEN 208865 THEN c.PhoneNumber
									WHEN 208866 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208869 THEN c.AccountName
									WHEN 208867 THEN c.AccountNumber
									WHEN 208872 THEN c.MonthlyRepaymentContract
									WHEN 208873 THEN c.MonthlyRepaymentActual
									WHEN 208874 THEN c.RemainingTerm
									WHEN 208875 THEN c.OriginalAmountBorrowed
									WHEN 208876 THEN c.PurposeOfLoan
									WHEN 208877 THEN c.AmountDue
									WHEN 208878 THEN CASE c.JointAndSeveralLiability WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208879 THEN CASE c.ArrearsIncludedInAmountDue WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208880 THEN CASE c.Restructured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									--WHEN 2 THEN c.RestructuringDetails
									WHEN 208881 THEN c.CurrentInterestRate
									--WHEN 2 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtCreditUnion c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		



		/********************************************************************************************************************************************************/
		/*															PFS Debt ExcludableRevenue																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtExcludableRevenue TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			OtherDetail VARCHAR(2000), 
			Permitted VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			PreferentialAmount VARCHAR(2000), 
			InstallmentArrangement VARCHAR(2000), 
			InstallmentAmount VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtExcludableRevenue (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			OtherDetail, 
			Permitted, 
			AmountDue, 
			PreferentialAmount, 
			InstallmentArrangement, 
			InstallmentAmount, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(OtherDetail)[1]', 'varchar(2000)') AS OtherDetail,
		n.c.value('(Permitted)[1]', 'varchar(2000)') AS Permitted,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(PreferentialAmount)[1]', 'varchar(2000)') AS PreferentialAmount,
		n.c.value('(InstallmentArrangement)[1]', 'varchar(2000)') AS InstallmentArrangement,
		n.c.value('(InstallmentAmount)[1]', 'varchar(2000)') AS InstallmentAmount,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/ExcludableRevenue') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208910, 21180, c.UNID 
			FROM @TblPFSDebtExcludableRevenue c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtExcludableRevenue c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208905) /* DebtSecured */
				   ,(208888) /* OrganisationName */
				   ,(208890) /* AddressLine1 */
				   ,(208891) /* AddressLine2 */
				   ,(208892) /* AddressLine3 */
				   ,(208893) /* AddressTown */
				   ,(208894) /* AddressCounty */
				   ,(208895) /* Email */
				   ,(208896) /* PhoneNumber */
				   ,(208897) /* Incurred6Month */
				   ,(208898) /* PType */
				   ,(208899) /* OtherDetail */
				   ,(208900) /* Permitted */
				   ,(208901) /* AmountDue */
				   ,(208902) /* PreferentialAmount */
				   ,(208903) /* InstallmentArrangement */
				   ,(208904) /* InstallmentAmount */
				   ,(208909) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208905 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208888 THEN c.OrganisationName
									WHEN 208890 THEN c.AddressLine1
									WHEN 208891 THEN c.AddressLine2
									WHEN 208892 THEN c.AddressLine3
									WHEN 208893 THEN c.AddressTown
									WHEN 208894 THEN c.AddressCounty
									WHEN 208895 THEN c.Email
									WHEN 208896 THEN c.PhoneNumber
									WHEN 208897 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208898 THEN c.PType
									WHEN 208899 THEN c.OtherDetail
									WHEN 208900 THEN CASE c.Permitted WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208901 THEN c.AmountDue
									WHEN 208902 THEN c.PreferentialAmount
									WHEN 208903 THEN CASE c.InstallmentArrangement WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208904 THEN c.InstallmentAmount
									WHEN 208909 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtExcludableRevenue c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt ExcludableNonRevenue																*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtExcludableNonRevenue TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			OtherDetail VARCHAR(2000), 
			Permitted VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			PreferentialAmount VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtExcludableNonRevenue (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			OtherDetail, 
			Permitted, 
			AmountDue, 
			PreferentialAmount, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(OtherDetail)[1]', 'varchar(2000)') AS OtherDetail,
		n.c.value('(Permitted)[1]', 'varchar(2000)') AS Permitted,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(PreferentialAmount)[1]', 'varchar(2000)') AS PreferentialAmount,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/ExcludableNonRevenue') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208911, 21180, c.UNID 
			FROM @TblPFSDebtExcludableNonRevenue c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtExcludableNonRevenue c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208927) /* DebtSecured */
				   ,(208912) /* OrganisationName */
				   ,(208914) /* AddressLine1 */
				   ,(208915) /* AddressLine2 */
				   ,(208916) /* AddressLine3 */
				   ,(208917) /* AddressTown */
				   ,(208918) /* AddressCounty */
				   ,(208919) /* Email */
				   ,(208920) /* PhoneNumber */
				   ,(208921) /* Incurred6Month */
				   ,(208922) /* PType */
				   ,(208923) /* OtherDetail */
				   ,(208924) /* Permitted */
				   ,(208925) /* AmountDue */
				   ,(208926) /* PreferentialAmount */
				   ,(208931) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208927 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208912 THEN c.OrganisationName
									WHEN 208914 THEN c.AddressLine1
									WHEN 208915 THEN c.AddressLine2
									WHEN 208916 THEN c.AddressLine3
									WHEN 208917 THEN c.AddressTown
									WHEN 208918 THEN c.AddressCounty
									WHEN 208919 THEN c.Email
									WHEN 208920 THEN c.PhoneNumber
									WHEN 208921 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208922 THEN c.PType
									WHEN 208923 THEN c.OtherDetail
									WHEN 208924 THEN CASE c.Permitted WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */
									WHEN 208925 THEN c.AmountDue
									WHEN 208926 THEN c.PreferentialAmount
									WHEN 208931 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtExcludableNonRevenue c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/********************************************************************************************************************************************************/
		/*															PFS Debt Employees																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtEmployees TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			FirstName VARCHAR(2000), 
			Surname VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			PreferentialAmount VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtEmployees (
			DebtSecured,  
			FirstName, 
			Surname, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			AmountDue, 
			PreferentialAmount, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Individual/FirstName)[1]', 'varchar(2000)') AS FirstName,
		n.c.value('(Creditor/Individual/Surname)[1]', 'varchar(2000)') AS Surname,
		n.c.value('(Creditor/Individual/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Individual/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Individual/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Individual/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Individual/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Individual/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Individual/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS Type,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(PreferentialAmount)[1]', 'varchar(2000)') AS PreferentialAmount,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Employees') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208967, 21180, c.UNID 
			FROM @TblPFSDebtEmployees c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtEmployees c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208962) /* DebtSecured */
				   ,(208949) /* FirstName */
				   ,(208948) /* Surname */
				   ,(208951) /* AddressLine1 */
				   ,(208952) /* AddressLine2 */
				   ,(208953) /* AddressLine3 */
				   ,(208954) /* AddressTown */
				   ,(208955) /* AddressCounty */
				   ,(208956) /* Email */
				   ,(208957) /* PhoneNumber */
				   ,(208958) /* Incurred6Month */
				   ,(208959) /* PType */
				   ,(208960) /* AmountDue */
				   ,(208961) /* PreferentialAmount */
				   ,(208931) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208962 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208949 THEN c.FirstName
									WHEN 208948 THEN c.Surname
									WHEN 208951 THEN c.AddressLine1
									WHEN 208952 THEN c.AddressLine2
									WHEN 208953 THEN c.AddressLine3
									WHEN 208954 THEN c.AddressTown
									WHEN 208955 THEN c.AddressCounty
									WHEN 208956 THEN c.Email
									WHEN 208957 THEN c.PhoneNumber
									WHEN 208958 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208959 THEN c.PType
									WHEN 208960 THEN c.AmountDue
									WHEN 208961 THEN c.PreferentialAmount
									WHEN 208931 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtEmployees c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/********************************************************************************************************************************************************/
		/*															PFS Debt Equipment																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtEquipment TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			AccountNumber VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtEquipment (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PType, 
			AccountNumber, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(AccountNumber)[1]', 'varchar(2000)') AS AccountNumber,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Equipment') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208989, 21180, c.UNID 
			FROM @TblPFSDebtEquipment c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtEquipment c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208984) /* DebtSecured */
				   ,(208968) /* OrganisationName */
				   ,(208973) /* AddressLine1 */
				   ,(208974) /* AddressLine2 */
				   ,(208975) /* AddressLine3 */
				   ,(208976) /* AddressTown */
				   ,(208977) /* TODO: AddressCounty but we only have a ZIP field to store it in */
				   ,(208978) /* Email */
				   ,(208979) /* PhoneNumber */
				   --,() /* Incurred6Month */
				   --,() /* PType */
				   ,(208982) /* AccountNumber */
				   ,(208983) /* AmountDue */
				   ,(208988) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208984 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208968 THEN c.OrganisationName
									WHEN 208973 THEN c.AddressLine1
									WHEN 208974 THEN c.AddressLine2
									WHEN 208975 THEN c.AddressLine3
									WHEN 208976 THEN c.AddressTown
									WHEN 208977 THEN c.AddressCounty
									WHEN 208978 THEN c.Email
									WHEN 208979 THEN c.PhoneNumber
									--WHEN  THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									--WHEN  THEN c.PType
									WHEN 208982 THEN c.AccountNumber
									WHEN 208983 THEN c.AmountDue
									WHEN 208988 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtEquipment c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/********************************************************************************************************************************************************/
		/*															PFS Debt TradeCreditors																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtTradeCreditors TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtTradeCreditors (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/TradeCreditors') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 209009, 21180, c.UNID 
			FROM @TblPFSDebtTradeCreditors c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtTradeCreditors c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208905) /* DebtSecured */
				   ,(208888) /* OrganisationName */
				   ,(208890) /* AddressLine1 */
				   ,(208891) /* AddressLine2 */
				   ,(208892) /* AddressLine3 */
				   ,(208893) /* AddressTown */
				   ,(208894) /* AddressCounty */
				   ,(208895) /* Email */
				   ,(208896) /* PhoneNumber */
				   ,(208897) /* Incurred6Month */
				   ,(208901) /* AmountDue */
				   ,(208909) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208905 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 208888 THEN c.OrganisationName
									WHEN 208890 THEN c.AddressLine1
									WHEN 208891 THEN c.AddressLine2
									WHEN 208892 THEN c.AddressLine3
									WHEN 208893 THEN c.AddressTown
									WHEN 208894 THEN c.AddressCounty
									WHEN 208895 THEN c.Email
									WHEN 208896 THEN c.PhoneNumber
									WHEN 208897 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 208901 THEN c.AmountDue
									WHEN 208909 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtTradeCreditors c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt ConnectedCreditors																	*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtConnectedCreditors TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			LoanPurpose VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Connection VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtConnectedCreditors (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			LoanPurpose, 
			AmountDue, 
			Connection, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(LoanPurpose)[1]', 'varchar(2000)') AS LoanPurpose,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Connection)[1]', 'varchar(2000)') AS Connection,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/ConnectedCreditors') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 209032, 21180, c.UNID 
			FROM @TblPFSDebtConnectedCreditors c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtConnectedCreditors c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(209027) /* DebtSecured */
				   ,(209010) /* OrganisationName */
				   ,(209015) /* AddressLine1 */
				   ,(209016) /* AddressLine2 */
				   ,(209017) /* AddressLine3 */
				   ,(209018) /* AddressTown */
				   ,(209019) /* AddressCounty */
				   ,(209021) /* Email */
				   ,(209022) /* PhoneNumber */
				   ,(209023) /* Incurred6Month */
				   ,(209024) /* LoanPurpose */
				   ,(209025) /* AmountDue */
				   ,(209026) /* Connection */
				   ,(209031) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 209027 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 209010 THEN c.OrganisationName
									WHEN 209015 THEN c.AddressLine1
									WHEN 209016 THEN c.AddressLine2
									WHEN 209017 THEN c.AddressLine3
									WHEN 209018 THEN c.AddressTown
									WHEN 209019 THEN c.AddressCounty
									WHEN 209021 THEN c.Email
									WHEN 209022 THEN c.PhoneNumber
									WHEN 209023 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 209024 THEN c.LoanPurpose
									WHEN 209025 THEN c.AmountDue
									WHEN 209026 THEN c.Connection
									WHEN 209031 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtConnectedCreditors c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Other																				*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtOther TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PDescription VARCHAR(2000), 
			DebtPurpose VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtOther (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PDescription, 
			DebtPurpose, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(DebtPurpose)[1]', 'varchar(2000)') AS DebtPurpose,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Other') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 209053, 21180, c.UNID 
			FROM @TblPFSDebtOther c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtOther c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(209048) /* DebtSecured */
				   ,(209033) /* OrganisationName */
				   ,(209037) /* AddressLine1 */
				   ,(209038) /* AddressLine2 */
				   ,(209039) /* AddressLine3 */
				   ,(209040) /* AddressTown */
				   ,(209041) /* AddressCounty */
				   ,(209042) /* Email */
				   ,(209043) /* PhoneNumber */
				   ,(209044) /* Incurred6Month */
				   ,(209045) /* PDescription */
				   ,(209046) /* DebtPurpose */
				   ,(209047) /* AmountDue */
				   ,(209052) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 209048 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 209033 THEN c.OrganisationName
									WHEN 209037 THEN c.AddressLine1
									WHEN 209038 THEN c.AddressLine2
									WHEN 209039 THEN c.AddressLine3
									WHEN 209040 THEN c.AddressTown
									WHEN 209041 THEN c.AddressCounty
									WHEN 209042 THEN c.Email
									WHEN 209043 THEN c.PhoneNumber
									WHEN 209044 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 209045 THEN c.PDescription
									WHEN 209046 THEN c.DebtPurpose
									WHEN 209047 THEN c.AmountDue
									WHEN 209052 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtOther c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Prospective																		*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtProspective TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			EstimatedPaymentDate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtProspective (
			DebtSecured,  
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PDescription, 
			EstimatedValue, 
			EstimatedPaymentDate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(EstimatedPaymentDate)[1]', 'varchar(2000)') AS EstimatedPaymentDate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Prospective') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 209075, 21180, c.UNID 
			FROM @TblPFSDebtProspective c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtProspective c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(209069) /* DebtSecured */
				   ,(209054) /* OrganisationName */
				   ,(209058) /* AddressLine1 */
				   ,(209059) /* AddressLine2 */
				   ,(209060) /* AddressLine3 */
				   ,(209061) /* AddressTown */
				   ,(209062) /* AddressCounty */
				   ,(209063) /* Email */
				   ,(209064) /* PhoneNumber */
				   ,(209065) /* Incurred6Month */
				   ,(209066) /* PDescription */
				   ,(209067) /* EstimatedValue */
				   ,(209068) /* EstimatedPaymentDate */
				   ,(209073) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 209069 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 209054 THEN c.OrganisationName
									WHEN 209058 THEN c.AddressLine1
									WHEN 209059 THEN c.AddressLine2
									WHEN 209060 THEN c.AddressLine3
									WHEN 209061 THEN c.AddressTown
									WHEN 209062 THEN c.AddressCounty
									WHEN 209063 THEN c.Email
									WHEN 209064 THEN c.PhoneNumber
									WHEN 209065 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 209066 THEN c.PDescription
									WHEN 209067 THEN c.EstimatedValue
									WHEN 209068 THEN c.EstimatedPaymentDate
									WHEN 209073 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtProspective c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Contingent																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtContingent TABLE (
			UNID INT IDENTITY(1,1), 
			DebtSecured VARCHAR(2000), 
			FirstName VARCHAR(2000), 
			Surname VARCHAR(2000), 
			AddressCountry VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PDescription VARCHAR(2000), 
			EstimatedValue VARCHAR(2000), 
			EstimatedPaymentDate VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtContingent (
			DebtSecured,  
			FirstName, 
			Surname, 
			AddressCountry, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			Incurred6Month, 
			PDescription, 
			EstimatedValue, 
			EstimatedPaymentDate, 
			Comment 
		)
		SELECT 
		n.c.value('(DebtSecured)[1]', 'varchar(2000)') AS DebtSecured,
		n.c.value('(Creditor/Individual/FirstName)[1]', 'varchar(2000)') AS FirstName,
		n.c.value('(Creditor/Individual/Surname)[1]', 'varchar(2000)') AS Surname,
		n.c.value('(Creditor/Individual/Address/NonIrish/Country)[1]', 'varchar(2000)') AS Country,
		n.c.value('(Creditor/Individual/Address/NonIrish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Individual/Address/NonIrish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Individual/Address/NonIrish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Individual/Address/NonIrish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Individual/Address/NonIrish/PostCode)[1]', 'varchar(2000)') AS PostCode,
		n.c.value('(Creditor/Individual/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Individual/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Incurred6Month)[1]', 'varchar(2000)') AS Incurred6Month,
		n.c.value('(Description)[1]', 'varchar(2000)') AS PDescription,
		n.c.value('(EstimatedValue)[1]', 'varchar(2000)') AS EstimatedValue,
		n.c.value('(EstimatedPaymentDate)[1]', 'varchar(2000)') AS EstimatedPaymentDate,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Contingent') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
			
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 209074, 21180, c.UNID 
			FROM @TblPFSDebtContingent c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtContingent c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(209069) /* DebtSecured */
				   ,(209057) /* FirstName */
				   ,(209056) /* Surname */
				   --,() /* AddressCountry */
				   ,(209058) /* AddressLine1 */
				   ,(209059) /* AddressLine2 */
				   ,(209060) /* AddressLine3 */
				   ,(209061) /* AddressTown */
				   ,(209062) /* AddressCounty */
				   ,(209063) /* Email */
				   ,(209064) /* PhoneNumber */
				   ,(209065) /* Incurred6Month */
				   ,(209066) /* PDescription */
				   ,(209067) /* EstimatedValue */
				   ,(209068) /* EstimatedPaymentDate */
				   ,(209073) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 209069 THEN CASE c.DebtSecured WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */  
									WHEN 209057 THEN c.FirstName
									WHEN 209056 THEN c.Surname
									--,() /* AddressCountry */
									WHEN 209058 THEN c.AddressLine1
									WHEN 209059 THEN c.AddressLine2
									WHEN 209060 THEN c.AddressLine3
									WHEN 209061 THEN c.AddressTown
									WHEN 209062 THEN c.AddressCounty
									WHEN 209063 THEN c.Email
									WHEN 209064 THEN c.PhoneNumber
									WHEN 209065 THEN CASE c.Incurred6Month WHEN 'Yes' THEN '5144' WHEN 'No' THEN '5145' ELSE '' END /* Aquarium Yes No */ 
									WHEN 209066 THEN c.PDescription
									WHEN 209067 THEN c.EstimatedValue
									WHEN 209068 THEN c.EstimatedPaymentDate
									WHEN 209073 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtContingent c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		
		
		
		/********************************************************************************************************************************************************/
		/*															PFS Debt Excluded																			*/
		/********************************************************************************************************************************************************/
		DECLARE @TblPFSDebtExcluded TABLE (
			UNID INT IDENTITY(1,1), 
			OrganisationName VARCHAR(2000), 
			AddressLine1 VARCHAR(2000), 
			AddressLine2 VARCHAR(2000), 
			AddressLine3 VARCHAR(2000), 
			AddressTown VARCHAR(2000), 
			AddressCounty VARCHAR(2000), 
			Email VARCHAR(2000), 
			PhoneNumber VARCHAR(2000), 
			Incurred6Month VARCHAR(2000), 
			PType VARCHAR(2000), 
			AmountDue VARCHAR(2000), 
			Comment VARCHAR(2000), 
			TableRowID INT
		)
		
		INSERT INTO @TblPFSDebtExcluded (
			OrganisationName, 
			AddressLine1, 
			AddressLine2, 
			AddressLine3, 
			AddressTown, 
			AddressCounty, 
			Email, 
			PhoneNumber, 
			PType, 
			AmountDue, 
			Comment 
		)
		SELECT 
		n.c.value('(Creditor/Organisation/OrganisationName)[1]', 'varchar(2000)') AS OrganisationName,
		n.c.value('(Creditor/Organisation/Address/Irish/Line1)[1]', 'varchar(2000)') AS Line1,
		n.c.value('(Creditor/Organisation/Address/Irish/Line2)[1]', 'varchar(2000)') AS Line2,
		n.c.value('(Creditor/Organisation/Address/Irish/Line3)[1]', 'varchar(2000)') AS Line3,
		n.c.value('(Creditor/Organisation/Address/Irish/Town)[1]', 'varchar(2000)') AS Town,
		n.c.value('(Creditor/Organisation/Address/Irish/County)[1]', 'varchar(2000)') AS County,
		n.c.value('(Creditor/Organisation/Email)[1]', 'varchar(2000)') AS Email,
		n.c.value('(Creditor/Organisation/PhoneNumber)[1]', 'varchar(2000)') AS PhoneNumber,
		n.c.value('(Type)[1]', 'varchar(2000)') AS PType,
		n.c.value('(AmountDue)[1]', 'varchar(2000)') AS AmountDue,
		n.c.value('(Comment)[1]', 'varchar(2000)') AS Comment
		FROM @xml.nodes('//PFS/Debt/Excluded') n(c); 
		
		IF @@ROWCOUNT > 0
		BEGIN
		
			/*
				Create the table rows and keep track of the rows to unids so we can update the 
				source table with the correct row id's
			*/
			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, SourceID) 
			OUTPUT inserted.SourceID, inserted.TableRowID INTO @InsertedTableRows 
			SELECT @ClientID, @CustomerID, 208947, 21180, c.UNID 
			FROM @TblPFSDebtExcluded c 

			/*Update source with newly created rows*/
			UPDATE c 
			SET TableRowID = t.TableRowID 
			FROM @TblPFSDebtExcluded c 
			INNER JOIN @InsertedTableRows t on t.UNID = c.UNID 

			/*Insert the fields required into the */
			INSERT INTO @Fields (DetailFieldID)
			VALUES	(208932) /* OrganisationName */
				   ,(208937) /* AddressLine1 */
				   ,(208938) /* AddressLine2 */
				   ,(208939) /* AddressLine3 */
				   ,(208940) /* AddressTown */
				   ,(208941) /* AddressCounty */
				   ,(208942) /* Email */
				   ,(208943) /* PhoneNumber */
				   ,(208944) /* PType */
				   ,(208945) /* AmountDue */
				   ,(208946) /* Comment */


			/*Insert all of the table detail values cross joining on the fields we inserted earlier*/
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			SELECT @ClientID, @CustomerID, c.TableRowID, f.DetailFieldID, CASE f.DetailFieldID 
									WHEN 208932 THEN c.OrganisationName
									WHEN 208937 THEN c.AddressLine1
									WHEN 208938 THEN c.AddressLine2
									WHEN 208939 THEN c.AddressLine3
									WHEN 208940 THEN c.AddressTown
									WHEN 208941 THEN c.AddressCounty
									WHEN 208942 THEN c.Email
									WHEN 208943 THEN c.PhoneNumber
									WHEN 208944 THEN c.PType
									WHEN 208945 THEN c.AmountDue
									WHEN 208946 THEN c.Comment
									ELSE ''
									END
			FROM @TblPFSDebtExcluded c 
			CROSS JOIN @Fields f 
			
			/* Clear out the work tables */
			DELETE @InsertedTableRows
			
			DELETE @Fields

		END		


		/* Income */
		
		/* Expenses */
		
		
		/********************************************************************************************************************************************************/
		/*																																						*/
		/*															End of data mappings																		*/
		/*																																						*/
		/********************************************************************************************************************************************************/
		
		
		
		/********************************************************************************************************************************************************/
		/*															Add a dashboard entry for the demo															*/
		/********************************************************************************************************************************************************/
		EXECUTE dbo.LeadViewHistory_Insert @LeadViewHistoryID OUTPUT, @ClientPersonnelID = @ClientPersonnelID, @ClientID = @ClientID, @LeadID = @LeadID, @WhenViewed = @TheDateTimePlus1s
		
	END
	
	/* Result should include the case id so that the calling service can log it */
    SELECT @CustomerID AS CustomerID, 
    @LeadID AS LeadID, 
    @CaseID AS CaseID, 
    @MatterID AS MatterID, 
	'' AS InfoMessage 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[IISSA_AddClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IISSA_AddClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IISSA_AddClaim] TO [sp_executeall]
GO
