SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2013-03-26
-- Description:	Test Maint Plan T-Sql - use this for anything you want
-- =============================================
CREATE PROCEDURE [dbo].[__JGTest]
	@SomeParam int = 2,
	@Body VARCHAR(2000) = 'Fail'
AS
BEGIN
	SET NOCOUNT ON;

	IF @SomeParam = 2 
	BEGIN
		INSERT INTO dbo.__JGTestTable (SomeValue, WhenCreated)
		VALUES (@Body, dbo.fn_GetDate_Local())
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[__JGTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__JGTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__JGTest] TO [sp_executeall]
GO
