SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-09
-- Description:	Returns new premium details for display
-- Mods:
--	2015-11-11 DCM replace adjustment metrics with originals rather than deleting				
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Adjustment_GetNewPremiumDetails]
(
	@DetailFieldID INT,
	@MatterID INT
)
AS
BEGIN

	SET NOCOUNT ON;

--declare
--	@DetailFieldID INT = 175479,
--	@MatterID INT = 50022146 


	DECLARE
			@AdjustmentAmount MONEY,
			@AdjustmentComment VARCHAR(500),
			@AdjustmentType VARCHAR(30),
			@AnnualPremium Money,
			@AqAutomation INT,
			@FirstMonthPremium MONEY,
			@OrigAdjustmentAmount VARCHAR(20),
			@OrigAdjustmentComment VARCHAR(500),
			@OrigAdjustmentDate VARCHAR(10),
			@OrigAdjustmentType VARCHAR(30),
			@OtherMonthPremium MONEY,
			@RatingType INT,
			@TableRowID INT,
			@Output XML

	-- 1=monthly premium, 2=renewal, 3=MTA, 4=Quote, 5=Cancellation 
	SELECT @RatingType=CASE @DetailFieldID 
			WHEN 175479 THEN 3
			WHEN 175480 THEN 4
			WHEN 175481 THEN 4
			WHEN 175482 THEN 4
			WHEN 175483 THEN 4
			WHEN 175484 THEN 4
			WHEN 175485 THEN 2
			WHEN 175486 THEN 5
			END
			
	-- get original adjustment date
	SELECT @AqAutomation=CASE WHEN @AqAutomation IS NULL 
				THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0) 
				ELSE @AqAutomation END
	FROM Matter m WITH (NOLOCK) 
	WHERE MatterID=@MatterID
			
	-- re-rate
	EXEC @TableRowID = _C600_ReRatePolicy 0, @MatterID, @RatingType, 1, @AqAutomation, @OutputData = @Output

	-- get the info
	SELECT @AnnualPremium = n.c.value('(DetailValue)[1]', 'MONEY')
	FROM @Output.nodes('//Data') as n(c)
	WHERE n.c.value('(DetailFieldID)[1]', 'INT') = 175349

	SELECT @FirstMonthPremium = n.c.value('(DetailValue)[1]', 'MONEY')
	FROM @Output.nodes('//Data') as n(c)
	WHERE n.c.value('(DetailFieldID)[1]', 'INT') = 175350

	SELECT @OtherMonthPremium = n.c.value('(DetailValue)[1]', 'MONEY')
	FROM @Output.nodes('//Data') as n(c)
	WHERE n.c.value('(DetailFieldID)[1]', 'INT') = 175351

	SELECT @AdjustmentAmount = n.c.value('(DetailValue)[1]', 'MONEY')
	FROM @Output.nodes('//Data') as n(c)
	WHERE n.c.value('(DetailFieldID)[1]', 'INT') = 175379

	SELECT	@AdjustmentType=adjtli.ItemValue,
			@AdjustmentComment=adjc.DetailValue 
	FROM MatterDetailValues adjv WITH (NOLOCK) 
	INNER JOIN MatterDetailValues adjt WITH (NOLOCK) ON adjv.MatterID=adjt.MatterID AND adjt.DetailFieldID=175345
	LEFT JOIN LookupListItems adjtli WITH (NOLOCK) ON adjt.ValueInt=adjtli.LookupListItemID
	INNER JOIN MatterDetailValues adjc WITH (NOLOCK) ON adjv.MatterID=adjc.MatterID AND adjc.DetailFieldID=175448
	WHERE adjv.MatterID=@MatterID 
	AND adjv.DetailFieldID=175379
	
	SELECT @AnnualPremium AS AnnualPremium,
			@FirstMonthPremium AS FirstMonth,
			@OtherMonthPremium AS OtherMonths,
			@AdjustmentAmount AS Adjustment,
			@AdjustmentType AS AdjustmentType,
			@AdjustmentComment AS Comments

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Adjustment_GetNewPremiumDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Adjustment_GetNewPremiumDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Adjustment_GetNewPremiumDetails] TO [sp_executeall]
GO
