SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-01-29
-- Description:	Delete all duplicate group rights and user rights for all objects. 
--              This includes a debug flag to show what will happen, without doing it.
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDuplicateRights] 

	@Debug bit = 0
	
AS
BEGIN
	SET NOCOUNT ON;

	IF @Debug = 1
	BEGIN
		/*
			1) Make sure that no-one is editing the group (or any users within it)
			The easiest way to do this is to ensure that the GroupAndUserEditingControl
			table is empty. Otherwise, you need to be sure that:
				i) no groups belonging to this client are being edited,
				AND that 
				ii) none of the Aquarium groups that any users within this client belong 
				to are being edited.
		*/
		SELECT * FROM dbo.GroupAndUserEditingControl

		/*
			Show duplicate GroupFunctionControl records
		*/
		SELECT * 
		FROM GroupFunctionControl gfc1 
		INNER JOIN GroupFunctionControl gfc2 ON gfc1.ClientPersonnelAdminGroupID = gfc2.ClientPersonnelAdminGroupID 
			AND gfc1.ModuleID = gfc2.ModuleID 
			AND gfc1.FunctionTypeID = gfc2.FunctionTypeID 
			AND ((gfc1.LeadTypeID IS NULL AND gfc2.LeadTypeID IS NULL) OR (gfc1.LeadTypeID = gfc2.LeadTypeID))
		WHERE gfc1.GroupFunctionControlID <> gfc2.GroupFunctionControlID
		ORDER BY 2,3,4,1

		SELECT * 
		FROM UserFunctionControl ufc1 
		INNER JOIN UserFunctionControl ufc2 ON ufc1.ClientPersonnelID = ufc2.ClientPersonnelID 
			AND ufc1.ModuleID = ufc2.ModuleID 
			AND ufc1.FunctionTypeID = ufc2.FunctionTypeID 
			AND ((ufc1.LeadTypeID IS NULL AND ufc2.LeadTypeID IS NULL) OR (ufc1.LeadTypeID = ufc2.LeadTypeID))
		WHERE ufc1.UserFunctionControlID <> ufc2.UserFunctionControlID
		ORDER BY 2,3,4,1

		SELECT * 
		FROM GroupRightsDynamic grd1 
		INNER JOIN GroupRightsDynamic grd2 ON grd1.ClientPersonnelAdminGroupID = grd2.ClientPersonnelAdminGroupID 
			AND grd1.FunctionTypeID = grd2.FunctionTypeID 
			AND ((grd1.LeadTypeID IS NULL AND grd2.LeadTypeID IS NULL) OR (grd1.LeadTypeID = grd2.LeadTypeID))
			AND grd1.ObjectID = grd2.ObjectID 
		WHERE grd1.GroupRightsDynamicID <> grd2.GroupRightsDynamicID
		ORDER BY 2,3,4,5,1

		SELECT * 
		FROM UserRightsDynamic urd1 
		INNER JOIN UserRightsDynamic urd2 ON urd1.ClientPersonnelID = urd2.ClientPersonnelID 
			AND urd1.FunctionTypeID = urd2.FunctionTypeID 
			AND ((urd1.LeadTypeID IS NULL AND urd2.LeadTypeID IS NULL) OR (urd1.LeadTypeID = urd2.LeadTypeID))
			AND urd1.ObjectID = urd2.ObjectID 
		WHERE urd1.UserRightsDynamicID <> urd2.UserRightsDynamicID
		ORDER BY 2,3,4,5,1

	END
	ELSE
	BEGIN

		/*
			Remove duplicates from all groups and users
		*/
		DELETE gfc1 
		FROM GroupFunctionControl gfc1 
		WHERE EXISTS (
			SELECT * 
			FROM GroupFunctionControl gfc2 
			WHERE gfc1.ClientPersonnelAdminGroupID = gfc2.ClientPersonnelAdminGroupID 
			AND gfc1.ModuleID = gfc2.ModuleID 
			AND gfc1.FunctionTypeID = gfc2.FunctionTypeID 
			AND ((gfc1.LeadTypeID IS NULL AND gfc2.LeadTypeID IS NULL) OR (gfc1.LeadTypeID = gfc2.LeadTypeID))
			AND gfc1.GroupFunctionControlID < gfc2.GroupFunctionControlID
		)

		DELETE ufc1 
		FROM UserFunctionControl ufc1 
		WHERE EXISTS (
			SELECT * 
			FROM UserFunctionControl ufc2 
			WHERE ufc1.ClientPersonnelID = ufc2.ClientPersonnelID 
			AND ufc1.ModuleID = ufc2.ModuleID 
			AND ufc1.FunctionTypeID = ufc2.FunctionTypeID 
			AND ((ufc1.LeadTypeID IS NULL AND ufc2.LeadTypeID IS NULL) OR (ufc1.LeadTypeID = ufc2.LeadTypeID))
			AND ufc1.UserFunctionControlID < ufc2.UserFunctionControlID
		)
		
		DELETE grd1 
		FROM GroupRightsDynamic grd1 
		WHERE EXISTS (
			SELECT * 
			FROM GroupRightsDynamic grd2 
			WHERE grd1.ClientPersonnelAdminGroupID = grd2.ClientPersonnelAdminGroupID 
			AND grd1.FunctionTypeID = grd2.FunctionTypeID 
			AND ((grd1.LeadTypeID IS NULL AND grd2.LeadTypeID IS NULL) OR (grd1.LeadTypeID = grd2.LeadTypeID))
			AND grd1.ObjectID = grd2.ObjectID 
			AND grd1.GroupRightsDynamicID < grd2.GroupRightsDynamicID
		)

		DELETE urd1 
		FROM UserRightsDynamic urd1 
		WHERE EXISTS (
			SELECT * 
			FROM UserRightsDynamic urd2 
			WHERE urd1.ClientPersonnelID = urd2.ClientPersonnelID 
			AND urd1.FunctionTypeID = urd2.FunctionTypeID 
			AND ((urd1.LeadTypeID IS NULL AND urd2.LeadTypeID IS NULL) OR (urd1.LeadTypeID = urd2.LeadTypeID))
			AND urd1.ObjectID = urd2.ObjectID 
			AND urd1.UserRightsDynamicID < urd2.UserRightsDynamicID
		)
	END
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDuplicateRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteDuplicateRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDuplicateRights] TO [sp_executeall]
GO
