SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==============================================================
-- Author:		Thomas Doyle
-- Create date: 2016-05-10
-- Description:	Insert all detail field aliases for a client matching the proc arguments
-- Doesn't currently handle the following scenario: you want to insert a field alias for 2 fields with the same name, a third field alias of the same name already exists.
-- ==============================================================
CREATE PROCEDURE [dbo].[_C00_DetailFieldAlias_Insert]
(	
	@ClientID INT  
	,@LeadTypeID INT  
	,@CreatedBy INT  = NULL /*null will do all createdby */
	,@Enabled	INT = NULL /*null will do all enabled&disabled*/
)

AS
BEGIN


	DECLARE @Holding TABLE (Clientid INT,LeadTypeID INT,DetailfieldID INT,AliasName VARCHAR(2000))
	INSERT INTO @Holding
	SELECT DISTINCT d.ClientID,d.LeadTypeID,d.DetailFieldID,dbo.fnStripCharacters(d.FieldName,'^0-9 A-Z ') 
	FROM  DetailFields d WITH (NOLOCK) 
	LEFT JOIN DetailFieldAlias da WITH (NOLOCK) ON d.DetailFieldID = da.DetailFieldID
	WHERE d.DetailFieldID IS NOT NULL
	AND da.DetailFieldID IS NULL
	AND d.ClientID = @ClientID 
	AND (d.LeadTypeID = @LeadTypeID OR @LeadTypeID  IS NULL)
	AND (@CreatedBy IS NULL OR d.WhoCreated= @CreatedBy)
	AND (@Enabled IS NULL OR d.Enabled = @Enabled)
	AND NOT EXISTS (SELECT * FROM DetailFieldAlias da WITH (NOLOCK) WHERE da.DetailFieldID = d.DetailFieldID)

	DECLARE @secondorder TABLE (clientid INT,leadtypeid INT,fieldid INT,ALIAS VARCHAR(200))		
	INSERT INTO @secondorder
	SELECT  H.Clientid,H.LeadTypeID,H.DetailfieldID,
	CASE ROW_NUMBER() OVER (PARTITION BY dbo.fnStripCharacters(h.AliasName,'^0-9 A-Z ') ORDER BY dbo.fnStripCharacters(h.AliasName,'^0-9 A-Z ') DESC)	
	WHEN 1 THEN h.AliasName
	ELSE h.AliasName + CAST(ROW_NUMBER() OVER (PARTITION BY dbo.fnStripCharacters(h.AliasName,'^0-9 A-Z ') ORDER BY dbo.fnStripCharacters(h.AliasName,'^0-9 A-Z ') DESC) AS VARCHAR(20)) +  CAST(ROW_NUMBER() OVER (PARTITION BY dbo.fnStripCharacters(h.AliasName,'^0-9 A-Z ') ORDER BY dbo.fnStripCharacters(h.AliasName,'^0-9 A-Z ') DESC) AS VARCHAR(20))
	END 
	FROM @Holding H 
	LEFT JOIN DetailFieldAlias DFA WITH (NOLOCK) ON DFA.LeadTypeID = H.LeadTypeID AND DFA.DetailFieldAlias = H.AliasName AND DFA.ClientID=H.Clientid

	UPDATE s
	SET s.ALIAS = s.ALIAS + '1'
	FROM  @secondorder s
	WHERE EXISTS (SELECT * FROM DetailFieldAlias dfa WITH (NOLOCK) WHERE dfa.ClientID=@ClientID AND dfa.LeadTypeID=s.leadtypeid AND s.ALIAS=dfa.DetailFieldAlias)

	INSERT INTO DetailFieldAlias (ClientID,LeadTypeID,DetailFieldID,DetailFieldAlias)
	SELECT * FROM  @secondorder
	

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DetailFieldAlias_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DetailFieldAlias_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DetailFieldAlias_Insert] TO [sp_executeall]
GO
