SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnelOptionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptionTypes_Delete]
(

	@ClientPersonnelOptionTypeID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnelOptionTypes] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptionTypes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Delete] TO [sp_executeall]
GO
