SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim
-- Create date: 2013-01-25
-- Description:	Get minimal EventType details for the EventGroupEdit page
-- SB  2014-07-10 Updated to use view which includes shared event types
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetForEventGroupID] 
	@EventGroupID INT,
	@ClientID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT et.EventTypeID, 
	et.EventTypeName, 
	lt.LeadTypeName,
	CASE WHEN et.Enabled = 1 THEN 'Y' ELSE '' END as [Enabled], 
	CASE WHEN et.InProcess = 1 THEN 'Y' ELSE '' END as [InProcess], 
	est.EventSubtypeName 
	FROM dbo.EventGroupMember egm WITH (NOLOCK) 
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et ON et.EventTypeID = egm.EventTypeID 
	INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = et.LeadTypeID
	WHERE egm.EventGroupID = @EventGroupID 
	AND egm.ClientID = @ClientID 
	ORDER BY et.EventTypeName, lt.LeadTypeName
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetForEventGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetForEventGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetForEventGroupID] TO [sp_executeall]
GO
