SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-03-16
-- Description:	Dump out all rule outputs for a rule in order
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleOutputs_ExportForRuleID]
(
	 @ClientID INT
	,@RuleID INT
)


AS
BEGIN

	SET NOCOUNT ON;

	;WITH Coordinates AS
	(
	SELECT p.RuleOutputID, p.p1 + ISNULL('|' + p.p2,'') + ISNULL('|' + p.p3,'') + ISNULL('|' + p.p4,'') + ISNULL('|' + p.p5,'') [CoordinateID]
	FROM		
		(	
		SELECT ro.RuleOutputID, 'p' + CONVERT(VARCHAR,np.ParamOrder) [ParamOrder], CONVERT(VARCHAR,oc.ParameterOptionID) [OutputCoordinateID]
		FROM RulesEngine_RuleOutputs ro WITH ( NOLOCK ) 
		INNER JOIN RulesEngine_OutputCoordinates oc WITH ( NOLOCK ) on oc.RuleOutputID = ro.RuleOutputID
		INNER JOIN RulesEngine_ParameterOptions po WITH ( NOLOCK ) on po.ParameterOptionID = oc.ParameterOptionID
		INNER JOIN (
					SELECT rp.RuleParameterID, rp.Name, ROW_NUMBER() OVER (ORDER BY rp.RuleParameterID) [ParamOrder]
					FROM RulesEngine_RuleParameters rp WITH ( NOLOCK ) 
					WHERE rp.RuleID = @RuleID
					) np on np.RuleParameterID = po.RuleParameterID
		INNER JOIN RulesEngine_Transforms tr WITH ( NOLOCK ) on tr.TransformID = ro.TransformID
		WHERE ro.RuleID = @RuleID
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([OutputCoordinateID])		
	FOR		
	[ParamOrder]
		IN ( [p1],[p2],[p3],[p4],[p5] )
	) AS p
	INNER JOIN RulesEngine_RuleOutputs ro WITH ( NOLOCK ) on ro.RuleOutputID = p.RuleOutputID
	)
	,
	Names AS
	(
	SELECT p.RuleOutputID, p.p1 + ISNULL(' ' + p.p2,'') + ISNULL(' ' + p.p3,'') + ISNULL(' ' + p.p4,'') + ISNULL(' ' + p.p5,'') [Name]
	FROM		
		(	
		SELECT ro.RuleOutputID, 'p' + CONVERT(VARCHAR,np.ParamOrder) [ParamOrder], np.Name + ' = ' + ISNULL(li.ItemValue,po.Val1) [Name]
		FROM RulesEngine_RuleOutputs ro WITH ( NOLOCK ) 
		INNER JOIN RulesEngine_OutputCoordinates oc WITH ( NOLOCK ) on oc.RuleOutputID = ro.RuleOutputID
		INNER JOIN RulesEngine_ParameterOptions po WITH ( NOLOCK ) on po.ParameterOptionID = oc.ParameterOptionID
		INNER JOIN (
					SELECT rp.RuleParameterID, rp.Name, rp.ParameterTypeID, rp.DataTypeID, ROW_NUMBER() OVER (ORDER BY rp.RuleParameterID) [ParamOrder]
					FROM RulesEngine_RuleParameters rp WITH ( NOLOCK ) 
					WHERE rp.RuleID = @RuleID
					) np on np.RuleParameterID = po.RuleParameterID
		INNER JOIN RulesEngine_Transforms tr WITH ( NOLOCK ) on tr.TransformID = ro.TransformID
		LEFT JOIN LookupListItems li WITH ( NOLOCK ) on CONVERT(VARCHAR,li.LookupListItemID) = po.Val1 AND np.DataTypeID = 6 /*LookupList*/
		WHERE ro.RuleID = @RuleID
		)  
		AS ToPivot		
	PIVOT		
	(		
		Max([Name])		
	FOR		
	[ParamOrder]
		IN ( [p1],[p2],[p3],[p4],[p5] )
	) AS p
	INNER JOIN RulesEngine_RuleOutputs ro WITH ( NOLOCK ) on ro.RuleOutputID = p.RuleOutputID
	)
	,Numbered AS
	(
	SELECT tr.Name [Transform], ro.Value, n.Name [Option], c.CoordinateID, ROW_NUMBER() OVER ( Partition By n.Name ORDER BY c.CoordinateID ) [RowNum]
	FROM RulesEngine_RuleOutputs ro WITH ( NOLOCK ) 
	INNER JOIN RulesEngine_Transforms tr WITH ( NOLOCK ) on tr.TransformID = ro.TransformID
	INNER JOIN Names n on n.RuleOutputID = ro.RuleOutputID
	INNER JOIN Coordinates c on c.RuleOutputID = ro.RuleOutputID
	WHERE ro.RuleID = @RuleID
	)

	SELECT n.Transform, n.Value, n.[Option], n.CoordinateID
	FROM Numbered n
	WHERE n.RowNum = 1
	ORDER BY n.[Option]

	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_ExportForRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleOutputs_ExportForRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_ExportForRuleID] TO [sp_executeall]
GO
