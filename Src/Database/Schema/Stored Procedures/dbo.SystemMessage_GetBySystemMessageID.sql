SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SystemMessage table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessage_GetBySystemMessageID]
(

	@SystemMessageID int   
)
AS


				SELECT
					[SystemMessageID],
					[ClientID],
					[SystemMessageName],
					[SystemMessageText],
					[Enabled]
				FROM
					[dbo].[SystemMessage] WITH (NOLOCK) 
				WHERE
										[SystemMessageID] = @SystemMessageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_GetBySystemMessageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessage_GetBySystemMessageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_GetBySystemMessageID] TO [sp_executeall]
GO
