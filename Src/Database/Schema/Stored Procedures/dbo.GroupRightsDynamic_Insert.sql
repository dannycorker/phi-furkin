SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the GroupRightsDynamic table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamic_Insert]
(

	@GroupRightsDynamicID int    OUTPUT,

	@ClientPersonnelAdminGroupID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				INSERT INTO [dbo].[GroupRightsDynamic]
					(
					[ClientPersonnelAdminGroupID]
					,[FunctionTypeID]
					,[LeadTypeID]
					,[ObjectID]
					,[RightID]
					)
				VALUES
					(
					@ClientPersonnelAdminGroupID
					,@FunctionTypeID
					,@LeadTypeID
					,@ObjectID
					,@RightID
					)
				-- Get the identity value
				SET @GroupRightsDynamicID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamic_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_Insert] TO [sp_executeall]
GO
