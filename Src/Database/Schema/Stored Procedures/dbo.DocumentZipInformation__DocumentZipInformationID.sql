SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By:	Aquarium Software (https://www.aquarium-software.com)
-- Author:		Ian SLack
-- Purpose:		TESTING PURPOSES ONLY - Gets a record from DocumentZipInformation table where the 
--				DocumentZipInformationID = @DocumentZipInformationID
-- Date:		2013-01-17
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[DocumentZipInformation__DocumentZipInformationID] 
(
	@DocumentZipInformationID int   
)
AS
BEGIN
	
	WITH d AS (
		SELECT dzi.[DocumentZipInformationID],
		dzi.[ClientID],
		dzi.[ServerMapPath],
		dzi.[SecurePath],
		dzi.[HostPath],
		dzi.[WordTempDirectory],
		dzi.[DocumentsPerZip],
		dzi.[ZipPrefix],
		dzi.[StatusID],
		IsNull(dzi.[TriggeredByAutomatedTaskID], 0) AS [TriggeredByAutomatedTaskID],
		IsNull(dzi.[TriggeredByDocumentTypeID], 0) AS [TriggeredByDocumentTypeID],
		dq.WhoCreated AS 'ClientPersonnelID', 
		row_number() over(partition by dzi.[DocumentZipInformationID] order by dq.WhoCreated) as rn, 
		IsNull(dzi.EmailOnSuccess, '') AS [EmailOnSuccess],
		IsNull(dzi.EmailOnError, '') AS [EmailOnError] 
		FROM [dbo].[DocumentZipInformation] dzi
		INNER JOIN DocumentZip dz ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID
		INNER JOIN DocumentQueue dq ON dq.DocumentQueueID = dz.DocumentQueueID
		WHERE dzi.[DocumentZipInformationID] = @DocumentZipInformationID
	) 
	SELECT * FROM d
	WHERE d.rn = 1 

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__DocumentZipInformationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__DocumentZipInformationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__DocumentZipInformationID] TO [sp_executeall]
GO
