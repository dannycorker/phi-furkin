SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-08-05
-- Description:	Generate RTF from SQL
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RTFFromSQL]
	@CustomerID INT = NULL,
	@LeadID INT, 
	@CaseID INT = NULL,
	@MatterID INT, 
	@TableDetailFieldID INT, 
	@ClientIDForDV INT = NULL, 
	@ClientPersonnelIDForDV INT = NULL, 
	@ContactIDForDV INT = NULL,
	@PrintSQL BIT = 0,
	@Header INT = 1 /*CS 2015-08-25 to bring in sync with Aquarius*/	
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ColWidth INT,
			@ColWidth2 INT,
			@CellWidths VARCHAR(MAX) = '',
			@NumberOfColumns INT,
			@body VARCHAR(MAX),
			@Columns VARCHAR(2000),
			@SQL VARCHAR(MAX),
			@BorderOn BIT,
			@SQLQueryID INT,
			@ClientID INT,
			@HtmlProperties	VARCHAR(200),
			@BorderWidth VARCHAR(MAX) = '10'

	/*Det the detail required from the custom sql table*/
	SELECT @ColWidth = c.RTFTwips, @Columns = c.HeaderColumns, @SQL = c.QueryText, @BorderOn = c.RTFBorderOn, @SQLQueryID = c.SQLQueryID, @ClientID = c.ClientID, @HtmlProperties = c.HTMLTableProperties
	FROM CustomTableSQL c
	WHERE c.DetailFieldID = @TableDetailFieldID
	
	IF @SQLQueryID > 0 
	BEGIN
	
		SELECT @SQL = sq.QueryText
		FROM SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @SQLQueryID
		AND sq.ClientID = @ClientID
	
	END
	
	/*2015-08-25 ACE Added border width*/
	SELECT @BorderWidth = ctso.OptionValue
	FROM CustomTableSQLOption ctso WITH (NOLOCK)
	WHERE ctso.DetailFieldID = @TableDetailFieldID
	AND ctso.OptionType = 'borderwidth'

	SELECT @SQL = REPLACE(@SQL, '@CustomerID', CONVERT(VARCHAR,ISNULL(@CustomerID,'')))
	SELECT @SQL = REPLACE(@SQL, '@LeadID', CONVERT(VARCHAR,ISNULL(@LeadID,'')))
	SELECT @SQL = REPLACE(@SQL, '@CaseID', CONVERT(VARCHAR,ISNULL(@CaseID,'')))
	SELECT @SQL = REPLACE(@SQL, '@MatterID', CONVERT(VARCHAR,ISNULL(@MatterID,'')))
	SELECT @SQL = REPLACE(@SQL, '@ClientID', CONVERT(VARCHAR,ISNULL(@ClientIDForDV,@ClientID))) /*Changed by CS 2014-11-04*/
	SELECT @SQL = REPLACE(@SQL, '@ClientPersonnelIDForDV', CONVERT(VARCHAR,ISNULL(@ClientPersonnelIDForDV,'')))
	SELECT @SQL = REPLACE(@SQL, '@ContactIDForDV', CONVERT(VARCHAR,ISNULL(@ContactIDForDV,'')))

	DECLARE @Table TABLE (TableRowID INT, XMLOutput VARCHAR(MAX), DenyEdit VARCHAR(MAX), DenyDelete VARCHAR(MAX))

	IF @HtmlProperties = 'RawText' /*CS 2015-07-14 Proof of Concept*/
	BEGIN
		INSERT @Table (TableRowID,XMLOutput,DenyEdit,DenyDelete)
		EXEC (@SQL)
		
		SELECT TOP(1) t.XMLOutput [MyHtml]
		FROM @Table t
		RETURN
	END


	/*We need to know how many columns there are so we can devide the twips by that number of columns*/
	SELECT @NumberOfColumns = COUNT(*)
	FROM dbo.fnSplitString (@Columns, ',')

	SELECT @ColWidth = @ColWidth/@NumberOfColumns

	SELECT @ColWidth2 = @ColWidth

	/*Start the RTF string off..*/
	SET @body = '{\trowd\trhdr\trgaph30\trleft0\trrh262 '

	/*
		Set the column widths as required
		2015-08-25 ACE Replaced brdrw10 with brdrw' + @BorderWidth + '
	*/
	SELECT @CellWidths = @CellWidths + CASE WHEN @BorderOn = 'true' 
					THEN '\clbrdrt\brdrs\brdrw' + @BorderWidth + '\clbrdrl\brdrs\brdrw' + @BorderWidth + '\clbrdrb\brdrs\brdrw' + @BorderWidth + '\clbrdrr\brdrs\brdrw' + @BorderWidth
					ELSE '' END  + '\cellx' +  CONVERT(VARCHAR(100),@ColWidth), @ColWidth = @ColWidth + @ColWidth2 + ' '
	FROM dbo.fnSplitString (@Columns, ',')

	/*Add the body and cell widths together*/
	SELECT @body = @body + @CellWidths

	IF @Header = 1 /*CS 2015-08-25*/
	BEGIN

		/*Add each of the headings into the rtf string*/
		SELECT @body = @body + '\pard\intbl\ql\b ' +  CONVERT(VARCHAR(100),RTRIM(LTRIM(Item))) + ' \b0\cell '
		FROM dbo.fnSplitString (@Columns, ',') f
		ORDER by f.ItemIndex
		
		/*Complete the header row off*/
		SELECT @body = @body + '\pard\intbl\row '
		
	END


	/*Add the outer select and inner select together and format as XML*/
	SELECT @SQL = 'SELECT CAST((' + @SQL + ' FOR XML RAW(''pard''), ELEMENTS
		) AS VARCHAR(MAX))'

	/*Replace @ClientID values*/
	SELECT @SQL = REPLACE(@SQL,'@ClientID',CONVERT(VARCHAR,@ClientID))

	/*Execute the sql stuffing the XML into a table variable so we can select it back out into a variable*/
	INSERT INTO @Table (XMLOutput)
	EXEC (@SQL)

	/*Get the body out of the table variable*/
	SELECT @body = @body + t.XMLOutput
	FROM @Table t
	
	SELECT @body = REPLACE(@body,'&amp;','&')

	/*CS 2015-08-11 replace RTF codes but DON'T use fnGetRTFEscapedValue because that will kill functional XML/RTF characters as well*/
	/*Need to include COLLATE Latin1_General_100_BIN in the REPLACE otherwise we start replacing things like the number 3 with the escape for superscript 3*/
	SELECT @body = REPLACE(@body COLLATE Latin1_General_100_BIN,cm.CharValue,cm.RTFEscapeCode)
	FROM CharMap cm WITH ( NOLOCK ) 
	WHERE cm.RTFEscapeCode <> ''
	AND cm.CharValue NOT IN ('<','>','_','/','\','}','{',' ','''')

	/*Replace the start of line xml with rtf*/
	SELECT @body = REPLACE(@body, '<pard>', ' \trowd\trgaph30\trleft0\trrh262 ' + @CellWidths)

	/*Replace the end of line xml with rtf*/
	SELECT @body = REPLACE(@body, '</pard>', ' \pard\intbl\row ')

	/*Replace the start of cell xml with rtf*/
	SELECT @body = REPLACE(@body, '<cell_replace>', ' \pard\intbl ')

	/*Replace the end of cell xml with rtf*/
	SELECT @body = REPLACE(@body, '</cell_replace>', ' \cell ')

	/*Complete the RTF off*/
	SELECT @body = @body + '}'

	/*Wipe out errant DenyEdit / DenyDelete flags.  CS 2014-11-04*/
	SELECT @body = REPLACE(REPLACE(@body,'<DenyDelete>true</DenyDelete>',''),'<DenyEdit>true</DenyEdit>','')

	/*Wipe out errant TableRowID flags.  CS 2014-11-04*/
	SELECT @body = REPLACE(@body,'<TableRowID>0</TableRowID>','')

	IF @PrintSQL = 1
	BEGIN
	
		PRINT @SQL
		PRINT @Columns
		PRINT @ClientID
	END

	/*Replace out pound signs for RTF pound code*/
	SELECT REPLACE(@body, '£', '\''a3') AS MyHtml

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RTFFromSQL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RTFFromSQL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RTFFromSQL] TO [sp_executeall]
GO
