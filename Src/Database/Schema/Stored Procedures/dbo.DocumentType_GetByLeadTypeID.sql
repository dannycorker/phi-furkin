SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT *
				FROM
					[dbo].[DocumentType]
				WHERE
					[LeadTypeID] = @LeadTypeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_GetByLeadTypeID] TO [sp_executehelper]
GO
