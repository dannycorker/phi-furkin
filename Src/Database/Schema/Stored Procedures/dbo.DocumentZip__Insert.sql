SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Inserts a record into the Document Zip table
-- Date: 13/02/2009
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentZip__Insert]
(
		
	@DocumentZipInformationID int  ,

	@ClientID int   ,

	@DocumentQueueID int  ,
	
	@StatusID int


)
AS


					
				INSERT INTO [dbo].[DocumentZip]
					(
					 [DocumentZipInformationID]
					,[ClientID]
					,[DocumentQueueID]
					,[StatusID]
					)
				VALUES
					(
					 @DocumentZipInformationID
					,@ClientID
					,@DocumentQueueID
					,@StatusID
					)
				
declare @DocumentZipID int;
				-- Get the identity value
				SET @DocumentZipID = SCOPE_IDENTITY()

Select DocumentZipID from DocumentZip where DocumentZipID = @DocumentZipID




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__Insert] TO [sp_executeall]
GO
