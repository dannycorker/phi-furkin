SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-06-26
-- Description:	Proc to Link a resourcelist to a lookuplist. Put this in the SAS
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ScriptResourceLink_LinkFields] 
(
		@ClientID					INT 
		,@CustORLeadORMatterID		INT
		,@DetailFieldToLink			INT /*This is the lookuplist field you will be able to add to the script*/
		,@ResourceListFieldID		INT	 
		,@ResourceListFieldColumnID	INT /*This is the column in the resource list that will be compared to the value in the */	
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@ResourceListIDtoInsert	INT
			,@LinkLookupListItemID		INT
			,@LinkLookupListID			INT /*ID Of the lookup list*/
			,@LookupListCountEnabled	INT 
			,@ResourceListCountEnabled	INT
			,@LinkLookupItemValue		VARCHAR(MAX)
			,@LogEntry				VARCHAR(2000)
			
	
/*Okay, now we have the new lookuplist we need to generate some SQL AFTER SAVE to update the resourcelist when a value has been selected in the lookuplist...i.e. Link the lookuplist to the resourcelist*/
SELECT @LinkLookupListItemID = dbo.fnGetSimpleDv(@DetailFieldToLink,@CustORLeadORMatterID) 

/*Get LookupListID*/
SELECT @LinkLookupListID = li.LookupListID 
FROM dbo.LookupListItems li WITH (NOLOCK) WHERE li.LookupListItemID=@LinkLookupListItemID

/*Count number of enabled items in lookuplist*/
SELECT @LookupListCountEnabled = COUNT(li.LookupListItemID)
FROM dbo.LookupListItems li WITH (NOLOCK)
WHERE li.LookupListID=@LinkLookupListID 
AND li.Enabled = 1

/*Count number of ResourceListItems in resourcelist*/
SELECT @ResourceListCountEnabled = COUNT(rldv.ResourceListID)
FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK) WHERE rldv.DetailFieldID=@ResourceListFieldColumnID

/*Check to see if number of items in lookuplist = ResourcelistItems, If not then add to the lookuplist anything you can find in the resource list that is not already in the lookuplist*/
IF @ResourceListCountEnabled <> @LookupListCountEnabled and @ResourceListCountEnabled is not null and @LookupListCountEnabled is not null
	BEGIN

		--/*Insert LookuplistItems into the lookuplist*/
		--INSERT INTO LookupListItems  ([LookupListID],[ItemValue],[ClientID],[Enabled],SortOrder,[SourceID])
		--SELECT @LinkLookupListID,rldvColumn.detailvalue,@ClientID,1,1,@ResourceListFieldColumnID
		--FROM ResourceListDetailValues rldvColumn 
		--WHERE rldvColumn.DetailFieldID = @ResourceListFieldColumnID	
		--AND @LinkLookupListID IS NOT NULL
		--AND rldvColumn.detailvalue not in (SELECT li.ItemValue 
		--FROM LookupListItems li WITH (NOLOCK) 
		--WHERE li.LookupListID=@LinkLookupListID and li.Enabled=1
		--)

		/*CS 2015-08-11 to confirm that this is the proc causing stupid numbers of lookup list items to be created*/
		SELECT @LogEntry = CONVERT(VARCHAR,@@ROWCOUNT)
		EXEC _C00_LogIt 'Info', '_C00_LOG', '_C00_ScriptResourceLink_LinkFields', @LogEntry, 3814 /*Cathal Sherry*/


	END
	
SELECT TOP 1 @LinkLookupItemValue = LLI.ItemValue
FROM LookupListItems LLI 
INNER JOIN LookupList ll WITH (NOLOCK) ON ll.LookupListID = LLI.LookupListID 
WHERE LLI.LookupListItemID = @LinkLookupListItemID 
AND ll.LookupListName LIKE 'scriptlink%'
AND lli.ClientID = @ClientID and LLI.ItemValue is not null 

SELECT TOP 1 @ResourceListIDtoInsert = RLDV.ResourceListID
FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK) 
WHERE rldv.DetailFieldID = @ResourceListFieldColumnID 
AND rldv.DetailValue = @LinkLookupItemValue and  RLDV.ResourceListID is not null

/*If resourcelist does not have a value or if resourcelistvalue does not match linked lookuplist value Then Update the resourcelist*/
IF dbo.fnGetSimpleDvAsInt(@ResourceListFieldID,@CustORLeadORMatterID) = 0 
OR (dbo.fnGetSimpleDvAsInt(@ResourceListFieldID,@CustORLeadORMatterID)  <> @ResourceListIDtoInsert AND @ResourceListIDtoInsert > 0 )
	BEGIN
		EXEC _C00_SimpleValueIntoField @ResourceListFieldID, @ResourceListIDtoInsert, @CustORLeadORMatterID
	END	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ScriptResourceLink_LinkFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ScriptResourceLink_LinkFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ScriptResourceLink_LinkFields] TO [sp_executeall]
GO
