SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2020-04-02
-- Description:	Card Transaction update with response for SecureCo / Team Members: James Lewis (JEL), Paul Richardson (PR), Gavin Reynolds (GPR)
-- 2020-04-20 GPR for AAG-679	| Added call to apply EventType
-- =============================================
CREATE PROCEDURE [dbo].[Billing_SecureCo_UpdateWithResponse]
	 @CardTransactionID			INT
	,@ErrorCode					VARCHAR (100)
	,@ErrorMessage				VARCHAR (500)
	,@TransactionID				VARCHAR (50)
	,@AuthCode					VARCHAR (250)
	,@StatusMsg					VARCHAR (250)

AS
BEGIN
  
    DECLARE	@JournalType				VARCHAR (100) 
	,@MaskedCardNumber			VARCHAR (200)
	,@ExpiryDate				DATE
	,@ExpiryMonth				VARCHAR (2)
	,@ExpiryYear				VARCHAR (4)
	,@Amount					DECIMAL (18,2)
	,@AccountID							INT
	,@PaymentID							INT
	,@CustomerPaymentScheduleID			INT
	,@CustomerLedgerID					INT
	,@RunAsUserID						INT = dbo.fn_C600_GetAqAutomationUser()
	,@EventToAdd						INT
	,@ClientID							INT = dbo.fnGetPrimaryClientID()
	,@CaseID							INT
	,@LeadEventID						INT
	,@EventTypeID						INT
	,@LeadID							INT
	,@DateForEvent						DATETIME = dbo.fn_GetDate_Local()
	,@CustomerID						INT
	,@MatterID							INT
	,@PolicyMatterID					INT
	,@Description						VARCHAR(200)
	,@LogEntry							VARCHAR (MAX)
	,@AmountNet							DECIMAL(18,2)
	,@AmountVat							DECIMAL (18,2)
	,@PurchasedProductID				INT
	,@PurchasedProductPaymentScheduleID INT 
	,@EventComments						VARCHAR(2000) = ''
	,@LogID								INT
	,@RowCount							INT
	,@LeadTypeID						INT = 1492 /*Policy Admin*/
	,@ContraCustomerLedgerID			INT


	DECLARE @FailedPPPS TABLE (PPPSID INT, PurchasedProductID INT)

	SELECT @LogEntry = CAST( ISNULL(@CardTransactionID,'ull')	as VARCHAR) + ' CT '  +
					  CAST( ISNULL(@ErrorCode,'ull')		as VARCHAR) + ' ec '  +		
					  CAST( ISNULL(@ErrorMessage,'ull')			as VARCHAR) + ' em '  +
					  CAST( ISNULL(@TransactionID,'ull')		as VARCHAR) + ' ti '  +	
					  CAST( ISNULL(@AuthCode,'ull')				as VARCHAR) + ' ac '  +
					  CAST( ISNULL(@StatusMsg,'ull')			as VARCHAR) + ' sm ' 


	EXEC _C00_LogIt    'Info', 'Billing_SecureCo_UpdateWithResponse','SecureCo',  @LogEntry, 58550

	/*Pick up other values from Card Transaction*/ 
	SELECT @CustomerID = c.CustomerID, @Amount = c.Amount, @PaymentID = p.PaymentID, @CustomerLedgerID = cl.CustomerLedgerID , @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID, @MatterID = c.ObjectID , @PurchasedProductID = c.ParentID, @AccountID = c.AccountID /*GPR 2020-03-04 - remove  use passed in StatusMsg*/-- ,@StatusMsg = c.StatusMsg
	FROM CardTransaction c WITH (NOLOCK)
	INNER JOIN Payment p with (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
	INNER JOIN CustomerLedger cl with (NOLOCK) on cl.PaymentID = p.PaymentID 
	WHERE c.CardTransactionID = @CardTransactionID  		

	/*Update the relevant schedule records if it worked*/ 
	IF @StatusMsg = 0 
	BEGIN 

		UPDATE PurchasedProductPaymentSchedule 
		Set PaymentStatusID = 6,CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
		From PurchasedProductPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE CustomerPaymentSchedule  
		Set PaymentStatusID = 6, CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
		From CustomerPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		SELECT @Description += LEFT(p.ProductDescription,CHARINDEX('(',p.ProductDescription,1)-1) + ' ' + pt.PurchasedProductPaymentScheduleTypeName + ' '
		FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
		INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.PurchasedProductID = pp.PurchasedProductID 
		INNER JOIN PurchasedProductPaymentScheduleType pt WITH (NOLOCK) on pt.PurchasedProductPaymentScheduleTypeID = pp.PurchasedProductPaymentScheduleTypeID
		WHERE pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
									
		UPDATE CustomerLedger 
		SET TransactionDescription = @Description, TransactionDate = dbo.fn_GetDate_Local()
		WHERE CustomerLedgerID = @CustomerLedgerID 

		UPDATE p 
		SET p.PaymentStatusID = 6,
				p.FailureReason = @ErrorMessage, 
				p.WhenModified = dbo.fn_GetDate_Local(), 
				p.WhoModified = @RunAsUserID
		FROM Payment p 
		WHERE p.PaymentID = @PaymentID

		Update CardTransaction 
		SET StatusMsg = @StatusMsg, TransactionID = @TransactionID
		Where CardTransactionID = @CardTransactionID 

	END 

	/*Update the relevant schedule records if it failed*/ 
	IF EXISTS (SELECT * FROM CardReturnProcessing crt WHERE crt.FailureCode = @StatusMsg and @StatusMsg <> 0) 
	BEGIN
				SELECT @EventToAdd = c.EventToApply, @RunAsUserID = c.AddEventAs, @ErrorMessage = c.FailureDescription
				FROM CardReturnProcessing c 
				INNER JOIN EventType et WITH (NOLOCK) on et.EventTypeID = c.EventToApply
				WHERE c.FailureCode IN ('-1', @ErrorCode) /*-1 is a fallback event...*/
				AND c.ClientID IN (0, @ClientID)
				--AND c.ReasonCodeType = @ClientPaymentGatewayID /*GPR 2020-04-02 removed as not required*/
				AND et.LeadTypeID = @LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
				ORDER BY c.ClientID DESC, c.FailureCode DESC

				Update CardTransaction 
				SET StatusMsg = @StatusMsg, TransactionID = @TransactionID, ErrorMessage = @ErrorMessage 
				Where CardTransactionID = @CardTransactionID 
				
				/*So not ok, this means we have a failure. Time to do the fail thing...*/
				UPDATE p 
				SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
						p.FailureReason = @ErrorMessage, 
						p.WhenModified = dbo.fn_GetDate_Local(), 
						p.WhoModified = @RunAsUserID
				FROM Payment p 
				WHERE p.PaymentID = @PaymentID

				INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
				SELECT p.ClientID, p.CustomerID, PaymentDateTime, p.FailureCode, p.FailureReason, CAST(dbo.fn_GetDate_Local() AS DATE), p.CardTransactionID, 'Failure notification ' + p.FailureReason, -p.PaymentNet, -p.PaymentVAT, -p.PaymentGross, NULL, NULL, NULL, @PaymentID, NULL, 58552 /*@AqAutomation*/, dbo.fn_GetDate_Local() 
				FROM Payment p WITH (NOLOCK) 
				WHERE p.PaymentID=@PaymentID
	            
				SELECT @ContraCustomerLedgerID = SCOPE_IDENTITY()

				-- Reverse customerpaymentschedule, reverse payment status
				UPDATE cps  
				SET PaymentStatusID = 4, ReconciledDate = dbo.fn_GetDate_Local(), CustomerLedgerID = @CustomerLedgerID 
				FROM CustomerPaymentSchedule cps WITH (NOLOCK)
				INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
				WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

				-- Purchasedproductpaymentschedule, reverse payment status
				UPDATE ppps  
				SET PaymentStatusID = 4, ReconciledDate = dbo.fn_GetDate_Local(), ContraCustomerLedgerID = @CustomerLedgerID, CustomerLedgerID = @CustomerLedgerID 
					OUTPUT inserted.PurchasedProductPaymentScheduleID, inserted.PurchasedProductID
					INTO @FailedPPPS (PPPSID, PurchasedProductID)
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
				INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
				WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

				/*GPR 2020-04-20 for AAG-679*/
				SELECT @LeadEventID = c.LatestLeadEventID FROM Lead l WITH (NOLOCK)
				INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
				WHERE l.LeadTypeID = @LeadTypeID
				AND l.CustomerID = @CustomerID

				/*GPR 2020-04-22*/
				EXEC _C00_LogIt   'Info', 'Billing_SecureCo_UpdateWithResponse EventToAdd','SecureCo',  @EventToAdd, 58550
				EXEC _C00_LogIt 'Info', 'Billing_SecureCo_UpdateWithResponse LeadEventID','SecureCo', @LeadEventID, 58550

				/*GPR 2020-04-21 added NULL check*/
				IF @LeadEventID IS NOT NULL AND @EventToAdd IS NOT NULL 
				BEGIN
					EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID, @EventToAdd, 58552 /*AQ Automation*/, -1
				END
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_SecureCo_UpdateWithResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing_SecureCo_UpdateWithResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_SecureCo_UpdateWithResponse] TO [sp_executeall]
GO
