SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientOfficeIPAddresses table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_Get_List]

AS


				
				SELECT
					[ClientOfficeIpAddressID],
					[ClientOfficeID],
					[ClientOfficeIPAddress],
					[ClientID],
					[Notes]
				FROM
					[dbo].[ClientOfficeIPAddresses] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Get_List] TO [sp_executeall]
GO
