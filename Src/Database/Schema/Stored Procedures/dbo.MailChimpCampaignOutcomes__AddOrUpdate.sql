SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-23
-- Description:	Add/Update campaign outcomes
-- =============================================
CREATE PROCEDURE [dbo].[MailChimpCampaignOutcomes__AddOrUpdate]
	@ClientID INT,
	@EmailAddress VARCHAR(200),
	@CampaignID VARCHAR(200),
	@Action VARCHAR(100),
	@ActivityCount INT,
	@UnSubReason VARCHAR(200) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (
		SELECT *
		FROM MailChimpCampaignOutcomes m WITH (NOLOCK)
		WHERE m.ClientID = @ClientID
		AND m.EmailAddress = @EmailAddress
		AND m.CampainID = @CampaignID
	)
	BEGIN
	
		UPDATE m
		SET Opened = CASE @Action WHEN 'open' THEN @ActivityCount ELSE Opened END,
			Clicked = CASE @Action WHEN 'click' THEN @ActivityCount ELSE Clicked END,
			Bounced = CASE @Action WHEN 'bounce' THEN @ActivityCount ELSE Bounced END,
			UnsubscribeReason = CASE WHEN @Action <> 'unsub' THEN m.UnsubscribeReason ELSE @UnSubReason END
		FROM MailChimpCampaignOutcomes m 
		WHERE m.ClientID = @ClientID
		AND m.EmailAddress = @EmailAddress
		AND m.CampainID = @CampaignID

	END
	ELSE
	BEGIN
	
		INSERT INTO MailChimpCampaignOutcomes (ClientID, EmailAddress, CampainID, Opened, Clicked, Bounced, UnsubscribeReason)
		VALUES (@ClientID, @EmailAddress, @CampaignID, 
					CASE @Action WHEN 'open' THEN @ActivityCount ELSE NULL END, 
					CASE @Action WHEN 'click' THEN @ActivityCount ELSE NULL END, 
					CASE @Action WHEN 'bounce' THEN @ActivityCount ELSE NULL END,
					@UnSubReason
					)
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimpCampaignOutcomes__AddOrUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MailChimpCampaignOutcomes__AddOrUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimpCampaignOutcomes__AddOrUpdate] TO [sp_executeall]
GO
