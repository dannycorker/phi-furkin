SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Script table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Script_Insert]
(

	@ScriptID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ScriptName varchar (250)  ,

	@ScriptDescription varchar (2000)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@WhenModified datetime   ,

	@WhoModified int   ,

	@FinishEventTypeID int   ,

	@Published bit   ,

	@Disabled bit   ,

	@ScriptParentID int   ,

	@StartEventTypeID int   ,

	@ShowNotes bit   ,

	@NumberOfNotesToShow int   ,

	@IntegrateWithDialler bit   ,

	@ApplyFinishEventTo int   ,

	@Redact bit   
)
AS


				
				INSERT INTO [dbo].[Script]
					(
					[ClientID]
					,[LeadTypeID]
					,[ScriptName]
					,[ScriptDescription]
					,[WhenCreated]
					,[WhoCreated]
					,[WhenModified]
					,[WhoModified]
					,[FinishEventTypeID]
					,[Published]
					,[Disabled]
					,[ScriptParentID]
					,[StartEventTypeID]
					,[ShowNotes]
					,[NumberOfNotesToShow]
					,[IntegrateWithDialler]
					,[ApplyFinishEventTo]
					,[Redact]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@ScriptName
					,@ScriptDescription
					,@WhenCreated
					,@WhoCreated
					,@WhenModified
					,@WhoModified
					,@FinishEventTypeID
					,@Published
					,@Disabled
					,@ScriptParentID
					,@StartEventTypeID
					,@ShowNotes
					,@NumberOfNotesToShow
					,@IntegrateWithDialler
					,@ApplyFinishEventTo
					,@Redact
					)
				-- Get the identity value
				SET @ScriptID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Insert] TO [sp_executeall]
GO
