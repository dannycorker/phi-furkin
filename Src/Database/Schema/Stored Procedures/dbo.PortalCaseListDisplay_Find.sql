SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalCaseListDisplay table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalCaseListDisplay_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalCaseListDisplayID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@Field1Caption varchar (50)  = null ,

	@Field1TypeID int   = null ,

	@Field1DetailFieldID int   = null ,

	@Field1ColumnDetailFieldID int   = null ,

	@Field2Caption varchar (50)  = null ,

	@Field2TypeID int   = null ,

	@Field2DetailFieldID int   = null ,

	@Field2ColumnDetailFieldID int   = null ,

	@Field3Caption varchar (50)  = null ,

	@Field3TypeID int   = null ,

	@Field3DetailFieldID int   = null ,

	@Field3ColumnDetailFieldID int   = null ,

	@Field4Caption varchar (50)  = null ,

	@Field4TypeID int   = null ,

	@Field4DetailFieldID int   = null ,

	@Field4ColumnDetailFieldID int   = null ,

	@Field5Caption varchar (50)  = null ,

	@Field5TypeID int   = null ,

	@Field5DetailFieldID int   = null ,

	@Field5ColumnDetailFieldID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@SqlQueryText varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalCaseListDisplayID]
	, [ClientID]
	, [LeadTypeID]
	, [Field1Caption]
	, [Field1TypeID]
	, [Field1DetailFieldID]
	, [Field1ColumnDetailFieldID]
	, [Field2Caption]
	, [Field2TypeID]
	, [Field2DetailFieldID]
	, [Field2ColumnDetailFieldID]
	, [Field3Caption]
	, [Field3TypeID]
	, [Field3DetailFieldID]
	, [Field3ColumnDetailFieldID]
	, [Field4Caption]
	, [Field4TypeID]
	, [Field4DetailFieldID]
	, [Field4ColumnDetailFieldID]
	, [Field5Caption]
	, [Field5TypeID]
	, [Field5DetailFieldID]
	, [Field5ColumnDetailFieldID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SqlQueryText]
    FROM
	[dbo].[PortalCaseListDisplay] WITH (NOLOCK) 
    WHERE 
	 ([PortalCaseListDisplayID] = @PortalCaseListDisplayID OR @PortalCaseListDisplayID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([Field1Caption] = @Field1Caption OR @Field1Caption IS NULL)
	AND ([Field1TypeID] = @Field1TypeID OR @Field1TypeID IS NULL)
	AND ([Field1DetailFieldID] = @Field1DetailFieldID OR @Field1DetailFieldID IS NULL)
	AND ([Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID OR @Field1ColumnDetailFieldID IS NULL)
	AND ([Field2Caption] = @Field2Caption OR @Field2Caption IS NULL)
	AND ([Field2TypeID] = @Field2TypeID OR @Field2TypeID IS NULL)
	AND ([Field2DetailFieldID] = @Field2DetailFieldID OR @Field2DetailFieldID IS NULL)
	AND ([Field2ColumnDetailFieldID] = @Field2ColumnDetailFieldID OR @Field2ColumnDetailFieldID IS NULL)
	AND ([Field3Caption] = @Field3Caption OR @Field3Caption IS NULL)
	AND ([Field3TypeID] = @Field3TypeID OR @Field3TypeID IS NULL)
	AND ([Field3DetailFieldID] = @Field3DetailFieldID OR @Field3DetailFieldID IS NULL)
	AND ([Field3ColumnDetailFieldID] = @Field3ColumnDetailFieldID OR @Field3ColumnDetailFieldID IS NULL)
	AND ([Field4Caption] = @Field4Caption OR @Field4Caption IS NULL)
	AND ([Field4TypeID] = @Field4TypeID OR @Field4TypeID IS NULL)
	AND ([Field4DetailFieldID] = @Field4DetailFieldID OR @Field4DetailFieldID IS NULL)
	AND ([Field4ColumnDetailFieldID] = @Field4ColumnDetailFieldID OR @Field4ColumnDetailFieldID IS NULL)
	AND ([Field5Caption] = @Field5Caption OR @Field5Caption IS NULL)
	AND ([Field5TypeID] = @Field5TypeID OR @Field5TypeID IS NULL)
	AND ([Field5DetailFieldID] = @Field5DetailFieldID OR @Field5DetailFieldID IS NULL)
	AND ([Field5ColumnDetailFieldID] = @Field5ColumnDetailFieldID OR @Field5ColumnDetailFieldID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([SqlQueryText] = @SqlQueryText OR @SqlQueryText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalCaseListDisplayID]
	, [ClientID]
	, [LeadTypeID]
	, [Field1Caption]
	, [Field1TypeID]
	, [Field1DetailFieldID]
	, [Field1ColumnDetailFieldID]
	, [Field2Caption]
	, [Field2TypeID]
	, [Field2DetailFieldID]
	, [Field2ColumnDetailFieldID]
	, [Field3Caption]
	, [Field3TypeID]
	, [Field3DetailFieldID]
	, [Field3ColumnDetailFieldID]
	, [Field4Caption]
	, [Field4TypeID]
	, [Field4DetailFieldID]
	, [Field4ColumnDetailFieldID]
	, [Field5Caption]
	, [Field5TypeID]
	, [Field5DetailFieldID]
	, [Field5ColumnDetailFieldID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SqlQueryText]
    FROM
	[dbo].[PortalCaseListDisplay] WITH (NOLOCK) 
    WHERE 
	 ([PortalCaseListDisplayID] = @PortalCaseListDisplayID AND @PortalCaseListDisplayID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([Field1Caption] = @Field1Caption AND @Field1Caption is not null)
	OR ([Field1TypeID] = @Field1TypeID AND @Field1TypeID is not null)
	OR ([Field1DetailFieldID] = @Field1DetailFieldID AND @Field1DetailFieldID is not null)
	OR ([Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID AND @Field1ColumnDetailFieldID is not null)
	OR ([Field2Caption] = @Field2Caption AND @Field2Caption is not null)
	OR ([Field2TypeID] = @Field2TypeID AND @Field2TypeID is not null)
	OR ([Field2DetailFieldID] = @Field2DetailFieldID AND @Field2DetailFieldID is not null)
	OR ([Field2ColumnDetailFieldID] = @Field2ColumnDetailFieldID AND @Field2ColumnDetailFieldID is not null)
	OR ([Field3Caption] = @Field3Caption AND @Field3Caption is not null)
	OR ([Field3TypeID] = @Field3TypeID AND @Field3TypeID is not null)
	OR ([Field3DetailFieldID] = @Field3DetailFieldID AND @Field3DetailFieldID is not null)
	OR ([Field3ColumnDetailFieldID] = @Field3ColumnDetailFieldID AND @Field3ColumnDetailFieldID is not null)
	OR ([Field4Caption] = @Field4Caption AND @Field4Caption is not null)
	OR ([Field4TypeID] = @Field4TypeID AND @Field4TypeID is not null)
	OR ([Field4DetailFieldID] = @Field4DetailFieldID AND @Field4DetailFieldID is not null)
	OR ([Field4ColumnDetailFieldID] = @Field4ColumnDetailFieldID AND @Field4ColumnDetailFieldID is not null)
	OR ([Field5Caption] = @Field5Caption AND @Field5Caption is not null)
	OR ([Field5TypeID] = @Field5TypeID AND @Field5TypeID is not null)
	OR ([Field5DetailFieldID] = @Field5DetailFieldID AND @Field5DetailFieldID is not null)
	OR ([Field5ColumnDetailFieldID] = @Field5ColumnDetailFieldID AND @Field5ColumnDetailFieldID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([SqlQueryText] = @SqlQueryText AND @SqlQueryText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalCaseListDisplay_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Find] TO [sp_executeall]
GO
