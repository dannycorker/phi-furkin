SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-01
-- Description:	Add email ids from Mailchimp
-- =============================================
CREATE PROCEDURE [dbo].[MailChimp__AddUniqueID]
	@ClientID INT,
	@EmailAddress VARCHAR(200),
	@UniqueID VARCHAR(200) = '',
	@MailchimpAccount VARCHAR(50) = ''
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ContactFieldID INT,
			@CustomerFieldID INT,
			@ClientPersonnelID INT

	DECLARE @DetailValueHistory TABLE (ClientID INT, DetailFieldID INT, LeadOrMatter INT, FieldValue VARCHAR(2000), WhenSaved DATETIME, CustomerID INT, ContactID INT, ClientPersonnelID INT)

	DECLARE @LocalDate DATETIME

	IF @ClientID = 361 AND @MailchimpAccount IN ('','a2ae3a8c57dfaf49d43bb7218760bd37-us4')
	BEGIN
	
		SELECT  @ContactFieldID = 300636,
				@CustomerFieldID = 300635,
				@ClientPersonnelID = 38887
	
	END
	ELSE
	IF @ClientID = 361 AND @MailchimpAccount IN ('e95ee0c7558b53436e923197b3ba75ec-us13')
	BEGIN
	
		SELECT  @ContactFieldID = 304986,
				@CustomerFieldID = 304984,
				@ClientPersonnelID = 38887
	
	END
	ELSE
	IF @ClientID = 253
	BEGIN

		SELECT  @CustomerFieldID = 301981,
				@ClientPersonnelID = 26154

	END
	
	IF @CustomerFieldID > 0
	BEGIN
	
		--INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		--SELECT @ClientID, c.CustomerID, @CustomerFieldID, ''
		--from Customers c with (nolock)
		--where c.ClientID = @ClientID
		--and not exists (
		--	select *
		--	from CustomerDetailValues cdv with (nolock)
		--	where cdv.CustomerID = c.CustomerID
		--	and cdv.DetailFieldID = @CustomerFieldID
		--	and cdv.ClientID = @ClientID
		--) 

		SELECT @LocalDate = dbo.fn_GetDate_Local()

		UPDATE cdv
		SET DetailValue = @UniqueID
			OUTPUT Inserted.ClientID, inserted.DetailFieldID, 10, inserted.DetailValue, @LocalDate, inserted.CustomerID, NULL, @ClientPersonnelID
			INTO @DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, CustomerID, ContactID, ClientPersonnelID)
		FROM CustomerDetailValues cdv
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = cdv.CustomerID AND c.EmailAddress = @EmailAddress
		WHERE cdv.DetailFieldID = @CustomerFieldID
		AND cdv.ClientID = @ClientID
	
	END
	
	IF @ContactFieldID > 0
	BEGIN
	
		--INSERT INTO ContactDetailValues (ClientID, ContactID, DetailFieldID, DetailValue)
		--SELECT @ClientID, c.ContactID, @CustomerFieldID, ''
		--from Contact c with (nolock)
		--where c.ClientID = @ClientID
		--and not exists (
		--	select *
		--	from ContactDetailValues cdv with (nolock)
		--	where cdv.ContactID = c.ContactID
		--	and cdv.DetailFieldID = @ContactFieldID
		--	and cdv.ClientID = @ClientID
		--) 

		SELECT @LocalDate = dbo.fn_GetDate_Local()

		UPDATE cdv
		SET DetailValue = @UniqueID
			OUTPUT Inserted.ClientID, inserted.DetailFieldID, 14, inserted.DetailValue, @LocalDate, NULL, inserted.ContactID, @ClientPersonnelID
			INTO @DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, CustomerID, ContactID, ClientPersonnelID)
		FROM ContactDetailValues cdv 
		INNER JOIN Contact c ON c.ContactID = cdv.ContactID AND (c.EmailAddressWork = @EmailAddress OR c.EmailAddressOther = @EmailAddress)
		WHERE cdv.DetailFieldID = @ContactFieldID
		AND cdv.ClientID = @ClientID
	
	END

	INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, CustomerID, ContactID, ClientPersonnelID)
	SELECT DISTINCT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, CustomerID, ContactID, ClientPersonnelID
	FROM @DetailValueHistory

END
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimp__AddUniqueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MailChimp__AddUniqueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MailChimp__AddUniqueID] TO [sp_executeall]
GO
