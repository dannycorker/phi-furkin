SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.UpdateClientOffice    Script Date: 08/09/2006 12:22:46 ******/

CREATE PROCEDURE [dbo].[UpdateClientOffice] 
@ClientOfficeID int,
@ClientID int,
@BuildingName nvarchar(50), 
@Address1 nvarchar(200),
@Address2 nvarchar(200), 
@Town nvarchar(200),
@County nvarchar(50),
@Country nvarchar(200),
@PostCode nvarchar(10),
@OfficeTelephone nvarchar(50),
@OfficeExt nvarchar(10)

as

Update ClientOffices 
SET  ClientID = @ClientID,
BuildingName = @BuildingName,
Address1=@Address1,
Address2=@Address2,
Town=@Town,
County=@County,
Country=@Country,
PostCode=@PostCode,
OfficeTelephone=@OfficeTelephone ,
OfficeTelephoneExtension=@OfficeExt
where ClientOfficeID = @ClientOfficeID





GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientOffice] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientOffice] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientOffice] TO [sp_executeall]
GO
