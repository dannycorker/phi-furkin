SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-20
-- Description:	Gets the sms "from" number if available
-- =============================================
CREATE PROCEDURE [dbo].[GetSMSFromNumber]		
	@ClientID INT,
	@LeadID INT,
	@CaseID INT,
	@EventTypeID INT = NULL
AS
BEGIN
	SET NOCOUNT ON
	
	/*
		One day we will have a database table of virtual mobile numbers courtesy of eSendex.com 
		that clients can use to send SMS messages.  The customer will then receive a message from 
		a specific mobile number depending on their lead type.  When they reply, eSendex will send 
		the details of their number and message back to an Aquarium page (currently AaranTest.aspx) 
		and we can move their process on automatically by applying the next event to their case.
		
		For now though, just use the only known values we have.
		
	*/
	
	DECLARE @LeadTypeID INT,
			@SMSQuestionID INT,
			@SMSQuestionnaireID INT,
			@Num VARCHAR(2000)
			
	SELECT @LeadTypeID = l.LeadTypeID 
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID 

	SELECT TOP 1 @SMSQuestionID = SMSQuestionID 
	FROM SMSQuestionEventType WITH (NOLOCK) 
	WHERE EventTypeID = @EventTypeID

	IF @SMSQuestionID IS NOT NULL
	BEGIN
				
		SELECT @SMSQuestionnaireID = SMSQuestionnaireID 
		FROM SMSQuestion WITH (NOLOCK) 
		WHERE SMSQuestionID = @SMSQuestionID	

		SELECT TOP 1 OutgoingPhoneNumber AS [SMSFromNumber]
		FROM SMSOutgoingPhoneNumber WITH (NOLOCK) 
		WHERE LeadTypeID = @LeadTypeID 
		AND SMSQuestionnaireID = @SMSQuestionnaireID
	
	END
	ELSE
	BEGIN
	
		IF EXISTS (
			SELECT *
			FROM ClientSmsAccountNumber c WITH (NOLOCK)
			WHERE c.ClientID = @ClientID
			/*
				2021-03-05 ACE There is a column (ResourceListID) that can be used for white labeling etc
			*/
		)
		BEGIN
		
			/*2021-03-05 ACE - FURKIN-156 - AS Per CS comment 2012!!! created a table..*/
			SELECT c.PhoneNumber AS [SMSFromNumber]
			FROM ClientSmsAccountNumber c WITH (NOLOCK)
			WHERE c.ClientID = @ClientID

		END
		ELSE
		IF @ClientID = 2
		BEGIN
			SELECT '07786202217'
		END
		ELSE
		IF @ClientID = 216
		BEGIN
			SELECT '07858739227'
		END
		ELSE
		/*If we get many more of these, we should probably set up some kind of mapping table?? CS 2012-02-15*/
		IF @ClientID = 225  /*Added by AE For C225 - 2012-04-16*/
		BEGIN
			SELECT rldv.DetailValue AS [SMSFromNumber]
			FROM MatterDetailValues mdv WITH (NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND m.CaseID = @CaseID
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv.ValueInt AND rldv.DetailFieldID = 144233
			WHERE mdv.DetailFieldID = 144122
		END
		ELSE
		IF @ClientID = 227 /* CS for support ticket #17160 */
		BEGIN
			SELECT 'Claim Eazy' [SMSFromNumber]
		END
		ELSE
		IF @ClientID = 237 /* JG for Jo Callahan */
		BEGIN
			SELECT '07860021910' [SMSFromNumber]
		END
		ELSE
		IF @ClientID=261 AND @LeadID=0 AND @CaseID=0
		BEGIN
			SELECT 'Phones 4U' [SMSFromNumber]	
		END	
		ELSE
		IF @ClientID=303  AND @LeadID=0 AND @CaseID=0
		BEGIN
			SELECT 'DialAPhone' [SMSFromNumber]	
		END
		ELSE
		IF @ClientID = 197  /* Added by CS for support ticket #11665 */
		BEGIN
			SELECT 
				CASE @LeadTypeID   
				WHEN 1029 THEN '08448588066'
				WHEN 1030 THEN '08448588066'
				WHEN 1034 THEN 'PPIADV LINE'
				END AS [SMSFromNumber]
		END	
		ELSE 
		IF @ClientID = 188 
		BEGIN
			DECLARE @CustomerID INT
			DECLARE @SubClientID INT
			SELECT @CustomerID=CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID
			SELECT @SubClientID = SubClientID FROM Customers WITH (NOLOCK) WHERE  CustomerID = @CustomerID
			SELECT CompanyName [SMSFromNumber] FROM SubClient WITH (NOLOCK) WHERE SubClientID=@SubClientID
				
		END	
		ELSE 
		IF @ClientID = 156
		BEGIN
			SELECT 'C4Refunds' [SMSFromNumber]
		END	
		ELSE 
		IF @ClientID = 310
		BEGIN
			IF @EventTypeID = 139158 /*INTRO sms*/
			BEGIN
				SELECT 'Essex PD' [SMSFromNumber]
			END
		END
		ELSE 
		IF @ClientID = 605 /*PRUDENT*/
		BEGIN
			SELECT '+18722446982' [SMSFromNumber]
		END	
		ELSE
		BEGIN

			SELECT 
				CASE @ClientID 
					WHEN 3 THEN '01619190580' 
					WHEN 183 THEN 'CardUpdate' 
					WHEN 166 THEN '01612287744'
					ELSE cl.CompanyName END 
				AS [SMSFromNumber] 
			FROM Clients cl WITH (NOLOCK) 
			WHERE cl.ClientID = @ClientID 

		END

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSMSFromNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSMSFromNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSMSFromNumber] TO [sp_executeall]
GO
