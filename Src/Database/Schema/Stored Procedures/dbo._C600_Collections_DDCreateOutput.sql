SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		James Lewis
-- Create date: 2017-06-13
-- Description:	Output DD file format print data in format required by local interface
-- 2017-06-08 CPS Added ISNULL()s
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_DDCreateOutput]
	@Payments dbo.tvpGenericPaymentData READONLY,
	--@TableRowID INT, 
	@ClientAccountID INT = NULL
AS
BEGIN
	
	
	DECLARE @CPAccountNo VARCHAR(MAX), --= dbo.fngetsimpleDV(177494,@TableRowID),
			@CPSortCode VARCHAR(MAX) ,--=  dbo.fngetsimpleDV(177495,@TableRowID), 
			@FreeFormat VARCHAR(MAX) = '    ',
			@CPUserName VARCHAR (MAX),-- = dbo.fngetsimpleDV(177497,@TableRowID),
			@Contents VARCHAR (MAX) = ''
			 
			--@CPRef VARCHAR (18)  = dbo.fngetsimpleDV(177498,@TableRowID) 
	
	SELECT @CPAccountNo = a.AccountNumber,
		   @CPSortCode = a.SortCode, 
		   @CPUserName = a.UsersName 
	FROM ClientAccount a WITH ( NOLOCK ) 
	WHERE a.ClientAccountID = @ClientAccountID 	

	SET NOCOUNT ON;

	SELECT   @Contents += 
	ISNULL(p.SortCode,'') +																			/*Destination Sort Code Number*/
	ISNULL(p.AccountNumber,'') +																	/*Destination Account Number*/
	'0' +																							/*Destination Type of Account Number*/
	ISNULL(p.BacsTransactionCode,'') +																/*Transaction Code*/
	ISNULL(@CPSortCode,'') +																		/*Originating Sort Code Number*/
	ISNULL(@CPAccountNo,'') +																			/*Originating Account Number*/																					
	ISNULL(@FreeFormat,'') +																		/*Free Format*/
	--REPLACE(CAST(ISNULL(p.Amount,0.00) AS VARCHAR),'.','') +	
	right(replicate('0', 11) + CAST(REPLACE(ISNULL(p.Amount,0.00),'.','') as VARCHAR(MAX)), 11) +	/*Amount (in pence)*/
	LEFT(CAST(ISNULL(@CPUserName,'') as VARCHAR(MAX)) + replicate(' ', 18), 18) +																			/*User’s Name*/															
	LEFT(CAST(ISNULL(p.Reference,'') as VARCHAR(MAX)) + replicate(' ', 18), 18) +																					/*User’s Reference*/ 
	LEFT(CAST(ISNULL(p.AccountName,'') as VARCHAR(MAX)) + replicate(' ', 18), 18)	/*Destination Account Name*/
	+ CHAR(10)-- + CHAR(10)																	
	FROM @Payments p
	
	--Select distinct  
 --        (
	--	SELECT STUFF(REPLACE((SELECT '#!' + LTRIM(RTRIM(  
	--	ISNULL(p.SortCode,'') +																			/*Destination Sort Code Number*/
	--	ISNULL(p.AccountNumber,'') +																	/*Destination Account Number*/
	--	'0' +																							/*Destination Type of Account Number*/
	--	ISNULL(p.BacsTransactionCode,'') +																/*Transaction Code*/
	--	ISNULL(@CPAccountNo,'') +																		/*Originating Sort Code Number*/
	--	ISNULL(@CPSortCode,'') +																		/*Originating Account Number*/																					
	--	ISNULL(@FreeFormat,'') +																		/*Free Format*/
	--	--REPLACE(CAST(ISNULL(p.Amount,0.00) AS VARCHAR),'.','') +	
	--	right(replicate('0', 11) + CAST(REPLACE(ISNULL(p.Amount,0.00),'.','') as VARCHAR(11)), 11) +	/*Amount (in pence)*/
	--	ISNULL(@CPUserName,'') +																		/*User’s Name*/															
	--	LEFT(CAST(ISNULL(p.Reference,'') as VARCHAR(18)) + replicate(' ', 18), 18) +																					/*User’s Reference*/ 
	--	LEFT(CAST(ISNULL(p.AccountName,'') as VARCHAR(18)) + replicate(' ', 18), 18)					/*Destination Account Name*/	
	--	--+ CHAR(10) + CHAR(13)
	--	)) AS 'data()' 																/*Horrid Hack to give correct line breaks*/	
	--	FROM @Payments p
	--	For XML PATH ('')),' #!',''), 1, 2, +'')
	--	)
 --    From @Payments 
	

	--SELECT   
	--ISNULL(p.SortCode,'') +																			/*Destination Sort Code Number*/
	--ISNULL(p.AccountNumber,'') +																	/*Destination Account Number*/
	--'0' +																							/*Destination Type of Account Number*/
	--ISNULL(p.BacsTransactionCode,'') +																/*Transaction Code*/
	--ISNULL(@CPAccountNo,'') +																		/*Originating Sort Code Number*/
	--ISNULL(@CPSortCode,'') +																		/*Originating Account Number*/																					
	--ISNULL(@FreeFormat,'') +																		/*Free Format*/
	----REPLACE(CAST(ISNULL(p.Amount,0.00) AS VARCHAR),'.','') +	
	--right(replicate('0', 11) + CAST(REPLACE(ISNULL(p.Amount,0.00),'.','') as VARCHAR(11)), 11) +	/*Amount (in pence)*/
	--ISNULL(@CPUserName,'') +																		/*User’s Name*/															
	--LEFT(CAST(ISNULL(p.Reference,'') as VARCHAR(18)) + replicate(' ', 18), 18) +																					/*User’s Reference*/ 
	--LEFT(CAST(ISNULL(p.AccountName,'') as VARCHAR(18)) + replicate(' ', 18), 18)	/*Destination Account Name*/
																		
	--FROM @Payments p
	
	SELECT @Contents = LEFT(@Contents,LEN(@Contents)-1) 
	
	SELECT @Contents

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_DDCreateOutput] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_DDCreateOutput] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_DDCreateOutput] TO [sp_executeall]
GO
