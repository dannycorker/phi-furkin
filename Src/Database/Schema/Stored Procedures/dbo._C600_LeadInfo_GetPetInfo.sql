SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-02-04
-- Description:	Gets the pet information to replace the normal lead info
-- Modified:	2014-11-12	SB	Added in collection lead ID to output
--				2014-11-20	SB	Bug fix for other pets
-- =============================================
CREATE PROCEDURE [dbo].[_C600_LeadInfo_GetPetInfo] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CustomerID INT,
			@ClaimLeadID INT,
			@PolicyLeadID INT,
			@CollectionsLeadID INT
			
	SELECT @CustomerID = CustomerID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	-- We could be passing in a policy lead or a claim lead so we need to get the right details first
	SELECT	@ClaimLeadID = dbo.fn_C00_1272_GetClaimLeadFromPolicyLead(@LeadID),
			@PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@LeadID),
			@CollectionsLeadID = dbo.fn_C00_1272_GetCollectionsLeadFromCustomer(@CustomerID)
			

	SELECT	l.LeadRef AS PolicyNo, ldvName.DetailValue AS Name, llSex.ItemValue AS Sex, llSpecies.ItemValue AS Species, rdvBreed.DetailValue AS Breed, 
			ISNULL(llColour.ItemValue, ldvColour.DetailValue) AS Colour, ldvDoB.ValueDate AS DateOfBirth, rdvVet.DetailValue AS Vet, rdvVetTel.DetailValue AS VetTel,
			@CustomerID AS CustomerID, @ClaimLeadID AS ClaimLeadID, @PolicyLeadID AS PolicyLeadID, @CollectionsLeadID AS CollectionsLeadID, ldvImage.DetailValue AS PetImage, ldvMicro.DetailValue as MicroChip
	FROM dbo.Lead l WITH (NOLOCK) 
	LEFT JOIN dbo.LeadDetailValues ldvName WITH (NOLOCK) ON l.LeadID = ldvName.LeadID AND ldvName.DetailFieldID = 144268
	LEFT JOIN dbo.LeadDetailValues ldvSex WITH (NOLOCK) ON l.LeadID = ldvSex.LeadID AND ldvSex.DetailFieldID = 144275
	LEFT JOIN dbo.LookupListItems llSex WITH (NOLOCK) ON ldvSex.ValueInt = llSex.LookupListItemID
	LEFT JOIN dbo.LeadDetailValues ldvBreed WITH (NOLOCK) ON l.LeadID = ldvBreed.LeadID AND ldvBreed.DetailFieldID = 144272
	LEFT JOIN dbo.ResourceListDetailValues rdvBreed WITH (NOLOCK) ON ldvBreed.ValueInt = rdvBreed.ResourceListID AND rdvBreed.DetailFieldID = 144270
	LEFT JOIN dbo.ResourceListDetailValues rdvSpecies WITH (NOLOCK) ON ldvBreed.ValueInt = rdvSpecies.ResourceListID AND rdvSpecies.DetailFieldID = 144269
	LEFT JOIN dbo.LookupListItems llSpecies WITH (NOLOCK) ON rdvSpecies.ValueInt = llSpecies.LookupListItemID
	LEFT JOIN dbo.LeadDetailValues ldvColour WITH (NOLOCK) ON l.LeadID = ldvColour.LeadID AND ldvColour.DetailFieldID = 144273
	LEFT JOIN dbo.LookupListItems llColour WITH (NOLOCK) ON ldvColour.ValueInt = llColour.LookupListItemID
	LEFT JOIN dbo.LeadDetailValues ldvDoB WITH (NOLOCK) ON l.LeadID = ldvDoB.LeadID AND ldvDoB.DetailFieldID = 144274
	LEFT JOIN dbo.LeadDetailValues ldvVet WITH (NOLOCK) ON l.LeadID = ldvVet.LeadID AND ldvVet.DetailFieldID = 146215
	LEFT JOIN dbo.ResourceListDetailValues rdvVet WITH (NOLOCK) ON ldvVet.ValueInt = rdvVet.ResourceListID AND rdvVet.DetailFieldID = 144473
	LEFT JOIN dbo.ResourceListDetailValues rdvVetTel WITH (NOLOCK) ON ldvVet.ValueInt = rdvVetTel.ResourceListID AND rdvVetTel.DetailFieldID = 144480
	LEFT JOIN dbo.LeadDetailValues ldvImage WITH (NOLOCK) ON l.LeadID = ldvImage.LeadID AND ldvImage.DetailFieldID = 175432
	LEFT JOIN dbo.LeadDetailValues ldvMicro WITH (NOLOCK) on l.LeadID = ldvMicro.LeadID and ldvMicro.DetailFieldID = 170030
	WHERE l.LeadID = @PolicyLeadID
	
	
	SELECT lOther.CustomerID, lOther.LeadID, ldvName.DetailValue + ' - ' + rldvPolicy.DetailValue  AS Name
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Lead lOther WITH (NOLOCK) ON l.CustomerID = lOther.CustomerID 
	INNER JOIN dbo.LeadDetailValues ldvName WITH (NOLOCK) ON lOther.LeadID = ldvName.LeadID AND ldvName.DetailFieldID = 144268
	INNER JOIN MatterDetailValues mdvPolicy WITH ( NOLOCK ) on lOther.LeadID = mdvPolicy.LeadID and mdvPolicy.DetailFieldID = 170034
	INNER JOIN ResourceListDetailValues rldvPolicy WITH ( NOLOCK ) on mdvPolicy.ValueInt = rldvPolicy.ResourceListID and rldvPolicy.DetailFieldID = 146200
	WHERE l.LeadID = @PolicyLeadID
	AND lOther.LeadID != @PolicyLeadID
	AND lOther.LeadTypeID = 1492
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_LeadInfo_GetPetInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_LeadInfo_GetPetInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_LeadInfo_GetPetInfo] TO [sp_executeall]
GO
