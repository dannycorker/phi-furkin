SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-28
-- Description:	Get Fields and things for showing on the custom search
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetCustomSearchFields]
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	/*Replace declared table with actual one later*/

	DECLARE @Table TABLE (DetailFieldID INT)

	IF @ClientID = 361
	BEGIN

		INSERT INTO @Table (DetailFieldID)
					VALUES (287644),
							(287354),
							(287355),
							(304985)
	END
	ELSE
	IF @ClientID = 356
	BEGIN
		INSERT INTO @Table   ( DetailFieldID )
					VALUES   ( 287256 )
							,( 287257 )
							,( 287258 )
							,( 287254 )
	END
	
	SELECT (
		SELECT (
			SELECT df.DetailFieldID, df.QuestionTypeID, df.FieldCaption, df.FieldOrder, (
				SELECT luli.ItemValue, luli.LookupListItemID
				FROM LookupListItems luli WITH (NOLOCK) 
				WHERE luli.LookupListID = df.LookupListID
				AND luli.Enabled = 1
				ORDER BY luli.ItemValue
				FOR XML PATH ('Values'), TYPE
			)
			FROM @Table t
			INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
			GROUP BY df.DetailFieldID, df.QuestionTypeID, df.FieldCaption, df.LookupListID, df.FieldOrder
			ORDER BY df.FieldOrder
			FOR XML PATH ('Field'), TYPE
		) 
		FOR XML PATH ('Fields'), TYPE 
	) AS [XML]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetCustomSearchFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetCustomSearchFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetCustomSearchFields] TO [sp_executeall]
GO
