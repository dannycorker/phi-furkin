SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-28
-- Description:	List all DetailValueHistory records for a lead or matter
-- =============================================
CREATE PROCEDURE [dbo].[dvh] 
	@LeadID int = NULL,
	@MatterID int = NULL, 
	@DetailFieldID int = NULL
	WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;

	/* Random selection if no params are passed in */
	IF @LeadID IS NULL AND @MatterID IS NULL AND @DetailFieldID IS NULL
	BEGIN
		SELECT TOP 100 dvh.* 
		FROM dbo.DetailValueHistory dvh WITH (NOLOCK) 
		ORDER BY dvh.DetailValueHistoryID DESC
	END
	ELSE
	BEGIN
		
		SELECT dvh.* 
		FROM dbo.DetailValueHistory dvh WITH (NOLOCK) 
		WHERE (dvh.LeadID = @LeadID OR @LeadID IS NULL)
		AND (dvh.MatterID = @MatterID OR @MatterID IS NULL)
		AND (dvh.DetailFieldID = @DetailFieldID OR @DetailFieldID IS NULL)
		ORDER BY dvh.LeadID, dvh.MatterID, dvh.DetailFieldID, dvh.DetailValueHistoryID 
		
	END
		
END







GO
GRANT VIEW DEFINITION ON  [dbo].[dvh] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dvh] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dvh] TO [sp_executeall]
GO
