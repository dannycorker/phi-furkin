SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-26
-- Description:	Take this Client38 user and create accounts in all clients
-- =============================================
CREATE PROCEDURE [dbo].[_C38_CopyUserToAllClients] 
	@Client38UserID int
AS
BEGIN
	SET NOCOUNT ON;

    /* 
		This proc assumes that "John Smith" has a user account within client 38
		
		It sets up an existing client 38 batch job with the right parameter to "Run As" the user passed in.
		
		Then it triggers the batch job, which uses SAE to call _C38_CreateUserInClient to do all the work.
    */
	UPDATE dbo.AutomatedTaskParam
	SET ParamValue = @Client38UserID
	WHERE TaskID = 13008				/* Create log on for all active clients */
	AND AutomatedTaskParamID = 1304820	/* RunAs user */
	AND ClientID = 38
    
    EXEC dbo.AutomatedTask__RunNow @TaskID = 13008
    
    PRINT 'Batch job 13008 has now been set to run asap.'
    PRINT 'Please do not trigger it again for another user until it has finished this one!'
    PRINT 'ATR 13008'
    
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CopyUserToAllClients] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_CopyUserToAllClients] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CopyUserToAllClients] TO [sp_executeall]
GO
