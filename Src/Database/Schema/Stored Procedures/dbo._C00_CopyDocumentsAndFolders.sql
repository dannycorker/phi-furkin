SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-07-17
-- Description:	Copy documents and folder structure from one leadtype to another
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyDocumentsAndFolders]
	@FromleadtypeID int,
	@ToleadtypeID int,
	@ToClientID int,
	@ToClientpersonnelID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Loopno int = 99,
			@RowsAdded int = 0


	DECLARE @FoldersToCopy TABLE (FolderID int, ParentFolderID int, FolderLevel int)

	DECLARE @Insertedfolders TABLE (NewFolderID int, OldfolderID int)

	DECLARE @InsertedDocuments TABLE (NewdocumentID int, OldDocumentID int)

	INSERT INTO @FoldersToCopy (FolderID, ParentFolderID, FolderLevel)
	SELECT distinct dtfl.FolderID, f.FolderParentID, 100
	FROM DocumentType dt WITH (NOLOCK)
	inner join documenttypefolderlink dtfl with (nolock) on dtfl.documenttypeid = dt.DocumentTypeID
	inner join Folder f with (nolock) on f.FolderID = dtfl.FolderID
	where dt.LeadTypeID = @FromleadtypeID
	and f.FolderID <> -1

	SELECT @RowsAdded = @@ROWCOUNT

	WHILE @RowsAdded > 0
	BEGIN

		insert into @FoldersToCopy (FolderID, ParentFolderID, FolderLevel)
		select distinct f.FolderID, f.FolderParentID, @Loopno
		FROM Folder f WITH (NOLOCK) 
		inner join @FoldersToCopy f2 on f2.ParentFolderID = f.FolderID
		where f2.FolderLevel = @Loopno + 1

		select @RowsAdded = @@ROWCOUNT

		select @Loopno = @Loopno - 1

	END

	select top 1 @Loopno = f.FolderLevel
	from @FoldersToCopy f
	order by FolderLevel asc

	INSERT INTO Folder (ClientID, FolderParentID, FolderName, FolderDescription, Personal, WhoCreated, WhenCreated, FolderTypeID)
	OUTPUT inserted.FolderID, inserted.FolderDescription INTO @Insertedfolders (NewFolderID, OldfolderID)
	SELECT @ToClientID, NULL, f1.FolderName, f.FolderID, Personal, @ToClientpersonnelID, dbo.fn_GetDate_Local(), FolderTypeID
	from @FoldersToCopy f
	inner join Folder f1  WITH (NOLOCK) on f1.FolderID = f.FolderID
	WHERE f.FolderLevel = @Loopno

	SELECT @RowsAdded = @@ROWCOUNT

	WHILE @RowsAdded > 0 
	BEGIN

		SELECT @Loopno = @Loopno + 1

		INSERT INTO Folder (ClientID, FolderParentID, FolderName, FolderDescription, Personal, WhoCreated, WhenCreated, FolderTypeID)
		OUTPUT inserted.FolderID, inserted.FolderDescription INTO @Insertedfolders (NewFolderID, OldfolderID)
		SELECT @ToClientID, i.NewFolderID, f1.FolderName, f.FolderID, Personal, @ToClientpersonnelID, dbo.fn_GetDate_Local(), FolderTypeID
		from @FoldersToCopy f
		inner join Folder f1  WITH (NOLOCK) on f1.FolderID = f.FolderID
		INNER JOIN @Insertedfolders i ON i.OldfolderID = f.ParentFolderID
		WHERE f.FolderLevel = @Loopno
		AND NOT EXISTS (
			SELECT * 
			FROM @Insertedfolders i
			WHERE i.OldfolderID = f.FolderID
		)
		
		SELECT @RowsAdded = @@ROWCOUNT

	END

	insert into DocumentType (ClientID, LeadTypeID, DocumentTypeName, DocumentTypeDescription, Header, Template, Footer, CanBeAutoSent, EmailSubject, EmailBodyText, InputFormat, OutputFormat, Enabled, RecipientsTo, RecipientsCC, RecipientsBCC, ReadOnlyTo, ReadOnlyCC, ReadOnlyBCC, SendToMultipleRecipients, MultipleRecipientDataSourceType, MultipleRecipientDataSourceID, SendToAllByDefault)
	output inserted.DocumentTypeID, inserted.DocumentTypeDescription into @InsertedDocuments (NewdocumentID, OldDocumentID)
	SELECT @ToClientID, @ToleadtypeID, DocumentTypeName, dt.DocumentTypeID, Header, Template, Footer, CanBeAutoSent, EmailSubject, EmailBodyText, InputFormat, OutputFormat, Enabled, RecipientsTo, RecipientsCC, RecipientsBCC, ReadOnlyTo, ReadOnlyCC, ReadOnlyBCC, SendToMultipleRecipients, MultipleRecipientDataSourceType, MultipleRecipientDataSourceID, SendToAllByDefault
	FROM DocumentType dt WITH (NOLOCK)
	where dt.LeadTypeID = @FromleadtypeID

	update DocumentType
	set DocumentTypeDescription = dt_from.DocumentTypeDescription
	FROM DocumentType 
	INNER JOIN @InsertedDocuments i on i.NewdocumentID = DocumentType.DocumentTypeID
	INNER JOIN DocumentType dt_from ON dt_from.DocumentTypeID = i.OldDocumentID

	insert into DocumentTypeFolderLink (DocumentTypeID, FolderID)
	SELECT i.NewdocumentID, i_f.NewFolderID
	FROM @InsertedDocuments i 
	INNER JOIN DocumentTypeFolderLink dtfl WITH (NOLOCK) ON dtfl.DocumentTypeID = i.OldDocumentID
	INNER JOIN @Insertedfolders i_f on i_f.OldfolderID = dtfl.FolderID

	SELECT i.OldDocumentID, i.NewdocumentID
	FROM @InsertedDocuments i

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyDocumentsAndFolders] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyDocumentsAndFolders] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyDocumentsAndFolders] TO [sp_executeall]
GO
