SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the OutcomeCriterias table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_GetByOutcomeID]
(

	@OutcomeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[OutcomeCriteriaID],
					[OutcomeID],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[OutcomeCriterias] WITH (NOLOCK) 
				WHERE
					[OutcomeID] = @OutcomeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_GetByOutcomeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_GetByOutcomeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_GetByOutcomeID] TO [sp_executeall]
GO
