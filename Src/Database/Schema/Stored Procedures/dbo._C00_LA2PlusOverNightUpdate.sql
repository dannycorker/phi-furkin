SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 23-Nov-2009
-- Description:	Proc to Run HSK for Client 144 Cheshire East
--				ACE 2012-01-24 Copied and Made Generic for LA2+
--				CS  2012-05-04 Allowed a different date adjustment for C226 on request of Krzys
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LA2PlusOverNightUpdate]
(
@ClientID int,
@LeadTypeID int,
@NextContributionDueDateFieldID int,
@DateofNextPaymenttoClientFieldID int,
@DateofPaymenttoClientFieldID int,
@PaymentImportLeadID int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int
	)

	DECLARE @NewMatterID int,
			@PaymentImportLeadTypeID int,
			@ContributionDateAdjustment int = -5

	INSERT @RequiredFields 
	SELECT @NextContributionDueDateFieldID 
	UNION SELECT @DateofNextPaymenttoClientFieldID
	
	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int 
	);
	
	IF @ClientID = 226 /*Added by CS 2012-05-11 on request of Krzys*/
	BEGIN
		Select @ContributionDateAdjustment = -4
	END
	
	INSERT INTO @LeadsAndMatters (LeadID, MatterID)
	SELECT DISTINCT m.LeadID, m.MatterID 
	FROM Matter m 
	INNER JOIN Lead l ON l.LeadID = m.LeadID AND l.LeadTypeID = @LeadTypeID 

	/* Create all necessary MatterDetailValues records that don't already exist */
	INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
	SELECT @ClientID,lam.LeadID,lam.MatterID,rf.DetailFieldID,''  /*@ClientID added by CS 2012-05-04*/
	FROM @LeadsAndMatters lam 
	CROSS JOIN @RequiredFields rf 
	WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WHERE mdv.MatterID = lam.MatterID AND mdv.DetailFieldID = rf.DetailFieldID) 

	/* If Date Of Month = 1 Create a new Matter */
	IF DATEPART(DD,dbo.fn_GetDate_Local())=1
	BEGIN
	
		SELECT @PaymentImportLeadTypeID = l.LeadTypeID
		FROM Lead l WITH (NOLOCK)
		WHERE l.LeadID = @PaymentImportLeadID 
	
		exec @NewMatterID = dbo._C00_CreateNewCaseForLead @PaymentImportLeadID, @PaymentImportLeadTypeID
		
		UPDATE Matter
		SET MatterRef = UPPER(LEFT(DATENAME(MM,dbo.fn_GetDate_Local()),3)) + '-' + convert(VARCHAR,DATEPART(YYYY,dbo.fn_GetDate_Local()))
		WHERE MatterID = @NewMatterID
	
	END

	/* Update Next Payment Due Date */
	update MatterDetailValues
	set DetailValue = mdv.ValueDate
	From MatterDetailValues
	Inner Join (
		select MatterID, MIN(ValueDate) as [ValueDate]
		from TableDetailValues
		where DetailFieldID = @DateofPaymenttoClientFieldID
		and ValueDate > dbo.fn_GetDate_Local()
		group by MatterID
	) as mdv on mdv.MatterID = MatterDetailValues.MatterID 
	Where MatterDetailValues.DetailFieldID = @DateofNextPaymenttoClientFieldID
	and (MatterDetailValues.ValueDate < dbo.fn_GetDate_Local()
	Or MatterDetailValues.ValueDate IS NULL)

	/* Update Next Contribution Due Date */
	update MatterDetailValues
	set DetailValue = mdv.ValueDate
	From MatterDetailValues
	Inner Join (
		select MatterID, dateadd(dd, @ContributionDateAdjustment,MIN(ValueDate)) as [ValueDate]
		from TableDetailValues
		where DetailFieldID = @DateofPaymenttoClientFieldID
		and ValueDate > dbo.fn_GetDate_Local()
		group by MatterID
	) as mdv on mdv.MatterID = MatterDetailValues.MatterID 
	Where MatterDetailValues.DetailFieldID = @NextContributionDueDateFieldID
	and (MatterDetailValues.ValueDate < dbo.fn_GetDate_Local()
	Or MatterDetailValues.ValueDate IS NULL)

	Select @@ROWCOUNT as [Number of Rows Updated]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LA2PlusOverNightUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LA2PlusOverNightUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LA2PlusOverNightUpdate] TO [sp_executeall]
GO
