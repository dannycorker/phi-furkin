SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_Cleanup
-- =============================================
CREATE PROCEDURE [dbo].[API_Cleanup]
	@QID UNIQUEIDENTIFIER
AS
BEGIN
	
	DELETE
	FROM API_Queue
	WHERE	
	(RecieveDT < (dbo.fn_GetDate_Local() -1))
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Cleanup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Cleanup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Cleanup] TO [sp_executeall]
GO
