SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the InvoiceMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMapping_GetByInvoiceMappingID]
(

	@InvoiceMappingID int   
)
AS


				SELECT
					[InvoiceMappingID],
					[ClientID],
					[LeadTypeID],
					[SourceInvoiceNumberField],
					[SourceTotalClaimAmountField],
					[SourceFeesToPostToSageNetField],
					[SourceFeesToPostToSageVatField],
					[InvoiceTypeCode],
					[CreditNoteTypeCode],
					[InvoiceNumberField],
					[DateField],
					[DatePostedField],
					[DetailsField],
					[SentToSageField],
					[NominalCodeField],
					[TaxCodeField],
					[InvoiceNetAmountField],
					[InvoiceTaxAmountField],
					[TypeField],
					[MatterIDField],
					[CaseNumberField],
					[CreditNoteNetAmountField],
					[CreditNoteTaxAmountField],
					[NominalCode],
					[TaxCode],
					[TableRowsDetailFieldID],
					[TableRowsDetailFieldPageID],
					[FeesInvoicedToSageField],
					[FeesPaidFromSageField],
					[FeesInvoicedOtherField],
					[FeesPaidOtherField],
					[CaseBalanceField],
					[SageCompanyName],
					[CostCentre]
				FROM
					[dbo].[InvoiceMapping] WITH (NOLOCK) 
				WHERE
										[InvoiceMappingID] = @InvoiceMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_GetByInvoiceMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMapping_GetByInvoiceMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_GetByInvoiceMappingID] TO [sp_executeall]
GO
