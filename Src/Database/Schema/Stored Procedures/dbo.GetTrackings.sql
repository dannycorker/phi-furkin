SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Object:  Stored Procedure dbo.GetTrackings    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetTrackings] 
	@ClientID int
AS

-- Get the most recent first by default
SELECT TrackingID, TrackingName,TrackingDescription,StartDate,EndDate,ClientQuestionnaireID,ClientID
FROM Tracking
WHERE ClientID = @ClientID 
ORDER BY TrackingID DESC






GO
GRANT VIEW DEFINITION ON  [dbo].[GetTrackings] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTrackings] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTrackings] TO [sp_executeall]
GO
