SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestionGroupMember table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupID]
(

	@SMSQuestionGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSQuestionGroupMemberID],
					[ClientID],
					[SMSQuestionnaireID],
					[SMSQuestionID],
					[SMSQuestionGroupID]
				FROM
					[dbo].[SMSQuestionGroupMember] WITH (NOLOCK) 
				WHERE
					[SMSQuestionGroupID] = @SMSQuestionGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_GetBySMSQuestionGroupID] TO [sp_executeall]
GO
