SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-12
-- Description:	Housekeeping for C600
-- Mods:
-- 2015-08-10 DCM Mark rows as paid on the day the report runs (edate from < to <=)
-- 2016-02-29 DCM Send 0C if all transactions cleared
-- 2016-12-14 DCM added _C600_HSK_CalculateChildCheckpoints
-- 2017-01-09 DCM updated billing system payment status update - only for BACS payments
-- 2017-08-21 CPS run user housekeeping
-- 2017-09-01 JEL changed HSK logic to set from the customerledger when transaction date was two (really three) days ago rather than CPS actual collection date
-- 2017-09-22 Added check for sort codes with xx-xx-xx format  
-- 2017-10-31 Changed logic to set payments as paid to working days not normal days 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_HSK]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @TableRowID INT,
			@AccName VARCHAR(100),
			@AccNumber VARCHAR(100),
			@AccSortCode VARCHAR(100),
			@AqUserID INT = 58552,  -- THIS IS A 600 ClientPersonnelID
			@BreedID VARCHAR(50),
			@BreedListID INT,
			@BreedName VARCHAR(250),
			@ClientID INT = dbo.fnGetPrimaryClientID(), 
			@CancelsToSend tvpINTINT,			
			@CustomerID INT,
			@Enabled INT,
			@EnabledRSID INT,
			@MatterID INT,
			@NameRSID INT,
			@New tvpIntVarchar,
			@PaymentDate VARCHAR(10),
			@PayRef VARCHAR(100),
			@PaymentStatus INT,
			@ResourceListID INT,
			@Species INT,
			@Today DATE = CAST(dbo.fn_GetDate_Local() as DATE) 

	DECLARE @EmptyTable TABLE (unid INT)
	
	DECLARE @PremiumRowsToUpdate TABLE ( TableRowID INT, PaymentDate VARCHAR(10), PaymentStatus INT, Done INT) 
	
	/* 1. Update Billing table entries from status pending to status paid  
	
		Find all rows where status is processed and the actual collection date date for their BACS collection is < today.
		Update these rows to status Paid 
	
	*/
	/*JEL - changed HSK logic to set from the customerledger when transaction date was two (really three) days ago rather than CPS actual collection date*/ 
						--UPDATE cps
						--SET PaymentStatusID=6 /*Paid*/
						--FROM CustomerPaymentSchedule cps
						--INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID
						--WHERE cps.PaymentStatusID=2 
						--	AND cps.ActualCollectionDate < DATEADD(DAY,-2,dbo.fn_GetDate_Local()) /*wait two days to mark it paid*/ 
						--	AND a.AccountTypeID=1 -- bank (card is 2)
						---- todo update after GOLIVE
						----FROM CustomerPaymentSchedule cps
						----INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
						----WHERE p.PaymentStatusID=2 AND cps.ActualCollectionDate < dbo.fn_GetDate_Local()
						
						--UPDATE ppps
						--SET PaymentStatusID=cps.PaymentStatusID
						--FROM PurchasedProductPaymentSchedule ppps
						--INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON  ppps.CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID
						--WHERE cps.PaymentStatusID IN(6,7) /*Paid,Free*/ AND ppps.PaymentStatusID NOT IN (6,7) /*Paid,Free*/
						
						--UPDATE cl
						--SET EffectivePaymentDate=cps.ActualCollectionDate
						--FROM CustomerLedger cl
						--INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON  cl.CustomerLedgerID=cps.CustomerLedgerID
						--WHERE cps.PaymentStatusID IN(6) /*Paid*/ AND cl.EffectivePaymentDate IS NULL
						
						--UPDATE p
						--SET DateReceived=cl.EffectivePaymentDate,
						--	PaymentStatusID=cps.PaymentStatusID
						--FROM Payment p
						--INNER JOIN CustomerLedger cl WITH (NOLOCK) ON p.PaymentID=cl.PaymentID
						--INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON  cl.CustomerLedgerID=cps.CustomerLedgerID
						--WHERE cps.PaymentStatusID IN (6,7) /*Paid,Free*/ AND cl.EffectivePaymentDate IS NOT NULL
		
	--UPDATE cl
	--SET EffectivePaymentDate=DATEADD(DAY,-1,dbo.fn_GetDate_Local()) 
	--FROM CustomerLedger cl
	--WHERE dbo.fnAddWorkingDays (CONVERT(DATE,cl.TransactionDate),3 ) = CAST(dbo.fn_GetDate_Local() as DATE)
	--AND cl.EffectivePaymentDate IS NULL
	
	--UPDATE cps
	--SET PaymentStatusID=6 /*Paid*/
	--FROM CustomerPaymentSchedule cps
	--INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID
	--INNER JOIN Customerledger cl WITH ( NOLOCK ) on cl.CustomerLedgerID = cps.CustomerLedgerID 
	--WHERE cps.PaymentStatusID=2 
	--AND CAST(cl.EffectivePaymentDate as DATE) = CAST(DATEADD(DAY,-1,dbo.fn_GetDate_Local()) as DATE)
	--AND a.AccountTypeID=1 -- bank (card is 2)
	---- todo update after GOLIVE
	----FROM CustomerPaymentSchedule cps
	----INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
	----WHERE p.PaymentStatusID=2 AND cps.ActualCollectionDate < dbo.fn_GetDate_Local()
	
	--UPDATE ppps
	--SET PaymentStatusID=cps.PaymentStatusID
	--FROM PurchasedProductPaymentSchedule ppps
	--INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON  ppps.CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID
	--WHERE cps.PaymentStatusID IN(6,7) /*Paid,Free*/ AND ppps.PaymentStatusID NOT IN (6,7) /*Paid,Free*/
	
	--UPDATE p
	--SET DateReceived=cl.EffectivePaymentDate,
	--	PaymentStatusID=cps.PaymentStatusID
	--FROM Payment p
	--INNER JOIN CustomerLedger cl WITH (NOLOCK) ON p.PaymentID=cl.PaymentID
	--INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON  cl.CustomerLedgerID=cps.CustomerLedgerID
	--WHERE cps.PaymentStatusID IN (6,7) /*Paid,Free*/ AND cl.EffectivePaymentDate IS NOT NULL
	
	
	----   **** BEU
	
	
	/*Check that none of our sort codes contain hypens*/
	UPDATE a 
	Set sortcode = REPLACE(a.sortcode,'-','')   
	FROM Account a WITH ( NOLOCK ) where a.SortCode like '%-%'
	
	INSERT INTO NoteTypeSQL (ClientID, NoteTypeID, PostUpdateSQL, WhoCreated, WhenCreated, WhoChanged, WhenChanged,InsertExisting)
	SELECT @ClientID, ntid.ValueInt, 'EXEC dbo._C600_SAN @LeadEventID', @AqUserID, dbo.fn_GetDate_Local(), @AqUserID, dbo.fn_GetDate_Local(), 1
	FROM ResourceListDetailValues ntid WITH (NOLOCK) 
	WHERE ntid.DetailFieldID=176936 
		AND NOT EXISTS ( SELECT * FROM NoteTypeSQL ex WITH (NOLOCK) WHERE ex.NoteTypeID=ntid.ValueInt AND ex.InsertExisting=1 )
	
	INSERT INTO NoteTypeSQL (ClientID, NoteTypeID, PostUpdateSQL, WhoCreated, WhenCreated, WhoChanged, WhenChanged,InsertExisting)
	SELECT @ClientID, ntid.ValueInt, 'EXEC dbo._C600_SANU @LeadEventID', @AqUserID, dbo.fn_GetDate_Local(), @AqUserID, dbo.fn_GetDate_Local(), 0
	FROM ResourceListDetailValues ntid WITH (NOLOCK) 
	WHERE ntid.DetailFieldID=176936 
		AND NOT EXISTS ( SELECT * FROM NoteTypeSQL ex WITH (NOLOCK) WHERE ex.NoteTypeID=ntid.ValueInt AND ex.InsertExisting=0 )
 	
	/* 
	
		4. Breed List RL synchronisation from Rules Engine
		
		Not very clever - we assume that the rulesets for a list of breed names and a list of enabled flags always have the
		same structure.  New breed name entries that are not listed in the 'enabled flag' ruleset are defaulted to enabled.
		Breed name resource list entries that are no longer in the Breed name ruleset are set to disabled.
		
		Processing is as follows:
		
			-- add any new entries setting enabled flag to true
			-- update all breed names for existing entries
			-- update all enabled flags for exssting entries
			-- disable any entries that no longer exist  
		
	*/
	
	DECLARE @Maps TABLE ( BreedListID INT, NameRSID INT, EnabledRSID INT, Done INT )
	
	DECLARE @BreedList TABLE ( BreedListID INT, Species INT, BreedName VARCHAR(500), Enabled INT, BreedID VARCHAR(50) )
	
	INSERT INTO @Maps ( BreedListID, NameRSID, EnabledRSID, Done )
	SELECT blID.ValueInt, nameRSID.ValueInt, enabledRSID.ValueInt, 0 
	FROM ResourceListDetailValues blID WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues enabledRSID WITH (NOLOCK) ON blID.ResourceListID=enabledRSID.ResourceListID AND enabledRSID.DetailFieldID=176960
	INNER JOIN ResourceListDetailValues nameRSID WITH (NOLOCK) ON blID.ResourceListID=nameRSID.ResourceListID AND nameRSID.DetailFieldID=176959
	WHERE blID.DetailFieldID=176958

	WHILE EXISTS ( SELECT * FROM @Maps WHERE Done=0 )  AND 1=2 /*CPS 2017-08-29 disable this*/
	BEGIN
	
		SELECT TOP 1 @NameRSID=m.NameRSID, @EnabledRSID=m.EnabledRSID, @BreedListID=m.BreedListID FROM @Maps m WHERE Done=0
	
		DELETE FROM @BreedList
		INSERT INTO @BreedList ( BreedListID, BreedName, BreedID, Enabled )
		SELECT @BreedListID, po.Val1, ro.Value, 5144
		FROM RulesEngine_RuleSets rs WITH (NOLOCK) 
		INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON rs.RuleSetID=r.RuleSetID
		INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON r.RuleID=rp.RuleID
		INNER JOIN RulesEngine_ParameterOptions po WITH (NOLOCK) ON rp.RuleParameterID=po.RuleParameterID
		INNER JOIN RulesEngine_OutputCoordinates oc WITH (NOLOCK) ON po.ParameterOptionID=oc.ParameterOptionID
		INNER JOIN RulesEngine_RuleOutputs ro WITH (NOLOCK) ON oc.RuleOutputID=ro.RuleOutputID
		--INNER JOIN RulesEngine_ParameterOptions po2 WITH (NOLOCK) ON 
		WHERE rs.RuleSetID=@NameRSID
		AND rp.Name='Pet: Breed'
		order by oc.RuleOutputID
		
		UPDATE bl
		SET Species=po.Val1
		FROM @BreedList bl
		INNER JOIN RulesEngine_RuleOutputs ro WITH (NOLOCK) ON bl.BreedID=ro.Value
		INNER JOIN RulesEngine_OutputCoordinates oc WITH (NOLOCK) ON oc.RuleOutputID=ro.RuleOutputID
		INNER JOIN RulesEngine_ParameterOptions po WITH (NOLOCK) ON po.ParameterOptionID=oc.ParameterOptionID
		INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID=po.RuleParameterID
		INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID=rp.RuleID
		WHERE r.RuleSetID=@NameRSID
		AND rp.Name='Pet: Species'
		
		UPDATE bl
		SET bl.Enabled=CASE WHEN ro.Value = 1 THEN 5144 ELSE 5145 END
		FROM RulesEngine_RuleSets rs WITH (NOLOCK) 
		INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON rs.RuleSetID=r.RuleSetID
		INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON r.RuleID=rp.RuleID
		INNER JOIN RulesEngine_ParameterOptions po WITH (NOLOCK) ON rp.RuleParameterID=po.RuleParameterID
		INNER JOIN RulesEngine_OutputCoordinates oc WITH (NOLOCK) ON po.ParameterOptionID=oc.ParameterOptionID
		INNER JOIN RulesEngine_RuleOutputs ro WITH (NOLOCK) ON oc.RuleOutputID=ro.RuleOutputID 
		INNER JOIN @BreedList bl ON CHARINDEX(bl.BreedID,po.Val1) > 0
		WHERE rs.RuleSetID=@EnabledRSID AND
		rp.name='Rule Set: Master Breed List'

		-- 1. Update existing
		UPDATE breedname
		SET DetailValue=bl.BreedName
		FROM ResourceListDetailValues breedID  
		INNER JOIN @BreedList bl ON BreedID.DetailValue=bl.BreedID
		INNER JOIN ResourceListDetailValues species ON breedID.ResourceListID=species.ResourceListID AND species.DetailFieldID=144269
		INNER JOIN ResourceListDetailValues breedname ON breedID.ResourceListID=breedname.ResourceListID AND breedname.DetailFieldID=144270
		WHERE breedid.DetailFieldID=170011
		AND species.ValueInt=bl.Species

		-- 2.  Add new
		INSERT INTO @New
		SELECT 0, bl.BreedID FROM @BreedList bl
		WHERE NOT EXISTS ( 
			SELECT * FROM ResourceListDetailValues breedID  
			INNER JOIN ResourceListDetailValues species ON breedID.ResourceListID=species.ResourceListID AND species.DetailFieldID=144269
			WHERE breedid.DetailFieldID=170011
			AND species.ValueInt=bl.Species 
			AND BreedID.DetailValue=bl.BreedID )

		WHILE EXISTS ( SELECT * FROM @New WHERE AnyID=0 )
		BEGIN
		
			SELECT TOP 1 @BreedID=AnyValue FROM @New WHERE AnyID=0

			SELECT @BreedListID=bl.BreedListID,
					@BreedName=bl.BreedName,
					@Enabled=bl.Enabled,
					@Species=bl.Species 
			FROM @BreedList bl WHERE bl.BreedID=@BreedID
			
			INSERT ResourceList (ClientID) VALUES (@ClientID)
			 	
			SELECT @ResourceListID = SCOPE_IDENTITY()
			
			INSERT INTO ResourceListDetailValues (ResourceListID,ClientID,DetailFieldID,LeadOrMatter,DetailValue) VALUES 
			(@ResourceListID,@ClientID,144269,4,CAST(@Species AS VARCHAR)),
			(@ResourceListID,@ClientID,144270,4,@BreedName),
			(@ResourceListID,@ClientID,170010,4,CAST(@BreedListID AS VARCHAR)),
			(@ResourceListID,@ClientID,170011,4,CAST(@BreedID AS VARCHAR)),
			(@ResourceListID,@ClientID,175584,4,CAST(@Enabled AS VARCHAR)),
			(@ResourceListID,@ClientID,175583,4,'')

			UPDATE @New SET AnyID=1 WHERE AnyValue=@BreedID 
		
		END

		-- 3. Disable not existing

		UPDATE enabl
		SET DetailValue=5145
		FROM ResourceListDetailValues breedID  
		INNER JOIN ResourceListDetailValues species ON breedID.ResourceListID=species.ResourceListID AND species.DetailFieldID=144269
		INNER JOIN ResourceListDetailValues enabl ON breedID.ResourceListID=enabl.ResourceListID AND enabl.DetailFieldID=175584
		WHERE breedid.DetailFieldID=170011
		AND NOT EXISTS ( SELECT * FROM @BreedList bl 
						 WHERE species.ValueInt=bl.Species 
							AND BreedID.DetailValue=bl.BreedID
						)		
		
		UPDATE @Maps SET Done=1 WHERE BreedListID=@BreedListID
	
	END

	/*

		5. Add child checkpoints for today's ratings

	*/
	
--	EXEC _C600_HSK_CalculateChildCheckpoints @Today

	/*
	
		6.  Make sure fields are not editable on page
		CPS 2017-10-31 for Nessa

	*/
	UPDATE df
	SET Editable = 0
	FROM dbo.DetailFields df WITH (NOLOCK) 
	INNER JOIN dbo.DetailFieldSubType dfst WITH (NOLOCK) on df.LeadOrMatter = dfst.DetailFieldSubTypeID
	INNER JOIN dbo.DetailFieldPages dfp WITH (NOLOCK) on df.DetailFieldPageID = dfp.DetailFieldPageID
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) on df.LeadTypeID = lt.LeadTypeID
	INNER JOIN dbo.QuestionTypes qt WITH (NOLOCK) on df.QuestionTypeID = qt.QuestionTypeID
	WHERE df.Editable = 1
	and df.LeadTypeID in (1492, 1490, 1493)
	and df.LeadOrMatter not in (4,6,8)
	and dfp.Enabled = 1
	and df.Enabled = 1
	and df.QuestionTypeID not in (25, 19, 16)	
	
	/*
	
		2017-08-21 CPS run user housekeeping
	
	*/
	--EXEC _C600_HSK_ClientPersonnel

	/*
		Trick to not output any results but still 
		allow the scheduler to run it without complaining
	*/
	SELECT *
	FROM @EmptyTable
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HSK] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_HSK] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HSK] TO [sp_executeall]
GO
