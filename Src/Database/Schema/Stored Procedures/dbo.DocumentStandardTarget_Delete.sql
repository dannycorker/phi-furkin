SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_Delete]
(

	@DocumentStandardTargetID int   
)
AS


				DELETE FROM [dbo].[DocumentStandardTarget] WITH (ROWLOCK) 
				WHERE
					[DocumentStandardTargetID] = @DocumentStandardTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Delete] TO [sp_executeall]
GO
