SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-11-11
-- Description:	Add records to the automated event queue
-- 2018-04-27 ACE Addeed comments to apply
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueueFull]
	@ClientID int,
	@LeadID int,
	@CaseID int,
	@LeadEventID int,
	@EventTypeID int,
	@AutomatedEventTypeID int,
	@ThreadToFollowUp int,
	@WhenCreated datetime,
	@WhoCreated int,
	@CommentsToApply VARCHAR(1500) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO dbo.AutomatedEventQueue (
		ClientID,
		CustomerID,
		LeadID,
		CaseID,
		FromLeadEventID,
		FromEventTypeID,
		WhenCreated,
		WhoCreated,
		AutomatedEventTypeID,
		RunAsUserID,
		ThreadToFollowUp,
		InternalPriority,
		BumpCount,
		ErrorCount,
		CommentsToApply
	)
	SELECT 
		@ClientID,
		l.CustomerID,
		@LeadID,
		@CaseID,
		@LeadEventID,
		@EventTypeID,
		@WhenCreated,
		@WhoCreated,
		@AutomatedEventTypeID,
		@WhoCreated,
		@ThreadToFollowUp,
		1,
		0 as BumpCount,
		0 as ErrorCount,
		@CommentsToApply
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueueFull] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueueFull] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueueFull] TO [sp_executeall]
GO
