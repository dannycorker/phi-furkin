SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CaseTransferStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferStatus_Insert]
(

	@CaseTransferStatusID int    OUTPUT,

	@Status varchar (60)  
)
AS


				
				INSERT INTO [dbo].[CaseTransferStatus]
					(
					[Status]
					)
				VALUES
					(
					@Status
					)
				-- Get the identity value
				SET @CaseTransferStatusID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Insert] TO [sp_executeall]
GO
