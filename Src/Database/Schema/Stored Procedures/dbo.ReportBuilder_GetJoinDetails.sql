SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Chris Townsend
-- Create date: 20th July 2007
-- Description:	Gets join details for a given table
-- =============================================
CREATE PROCEDURE [dbo].[ReportBuilder_GetJoinDetails]
	@TableName varchar(200) = null
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT rtr.ReportTableRelationshipID AS RTRID, rtr.TableTo AS JoinTable, rtr.TableJoin AS TableJoin, rtr.Description AS Description, rt.RequiresClientSecurity AS NewTableRequiresSecurity, rt.ShowAquariumValues
	FROM ReportTables rt 
	INNER JOIN ReportTableRelationships rtr ON rtr.TableFrom = rt.ReportTableName
	WHERE rt.ReportTableName = @TableName
	UNION
	SELECT rtr.ReportTableRelationshipID AS RTRID, rtr.TableFrom AS JoinTable, rtr.TableJoin AS TableJoin, rtr.Description AS Description, rt.RequiresClientSecurity AS NewTableRequiresSecurity, rt.ShowAquariumValues
	FROM ReportTables rt 
	INNER JOIN ReportTableRelationships rtr ON rtr.TableTo = rt.ReportTableName
	WHERE rt.ReportTableName = @TableName

END






GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetJoinDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportBuilder_GetJoinDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetJoinDetails] TO [sp_executeall]
GO
