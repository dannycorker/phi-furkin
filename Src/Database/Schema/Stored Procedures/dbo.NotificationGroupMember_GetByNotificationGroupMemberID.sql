SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the NotificationGroupMember table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroupMember_GetByNotificationGroupMemberID]
(

	@NotificationGroupMemberID int   
)
AS


				SELECT
					[NotificationGroupMemberID],
					[NotificationGroupID],
					[ClientID],
					[ClientPersonnelID]
				FROM
					[dbo].[NotificationGroupMember] WITH (NOLOCK) 
				WHERE
										[NotificationGroupMemberID] = @NotificationGroupMemberID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_GetByNotificationGroupMemberID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupMember_GetByNotificationGroupMemberID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_GetByNotificationGroupMemberID] TO [sp_executeall]
GO
