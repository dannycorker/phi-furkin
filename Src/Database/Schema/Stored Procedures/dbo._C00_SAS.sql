SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-04-30
-- Description:	SQL After Save For C00
-- 2020-01-13 CPS for JIRA LPC-356 | Removed hard-coded @ClientID 600 section that was encapsulating the second stored proc call.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SAS]
	@ClientID int,
	@ClientPersonnelID int,
	@CustomerID int = NULL,
	@LeadID int = NULL,
	@CaseID int = NULL, 
	@MatterID int = NULL,
	@TableRowID int = NULL,
	@ResourceListID int = NULL, 
	@ClientPersonnelIDForDV INT = NULL, 
	@ContactIDForDV INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	EXEC dbo._C600_SAS @ClientPersonnelID, @CustomerID, @LeadID, @CaseID, @MatterID, @TableRowID, @ResourceListID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SAS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAS] TO [sp_executeall]
GO
