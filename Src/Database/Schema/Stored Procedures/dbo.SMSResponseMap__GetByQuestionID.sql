SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 24/01/2014
-- Description:	Gets a list of Responses by the from sms question id#
-- Modified By PR 31-01-2014 added processing order for vodafone demo
-- =============================================
CREATE PROCEDURE [dbo].[SMSResponseMap__GetByQuestionID]
(
	@SMSQuestionID INT
	
)
AS


				SET ANSI_NULLS ON
				
				SELECT *
					
				FROM
					[dbo].[SMSResponseMap] WITH (NOLOCK) 
				WHERE
					[FromSMSQuestionID] = @SMSQuestionID 					
				ORDER BY ProcessingOrder ASC
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap__GetByQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByQuestionID] TO [sp_executeall]
GO
