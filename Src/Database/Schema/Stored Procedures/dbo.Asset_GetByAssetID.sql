SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Asset table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Asset_GetByAssetID]
(

	@AssetID int   
)
AS


				SELECT
					[AssetID],
					[ClientID],
					[AssetTypeID],
					[AssetSubTypeID],
					[LocationID],
					[AssetName],
					[Note],
					[SecureNote],
					[SecureUserName],
					[SecurePassword],
					[SecureOther],
					[Version],
					[ValidFrom],
					[ValidTo],
					[ReminderDue],
					[ReminderTimeUnitID],
					[ReminderTimeUnitQuantity],
					[ReminderNotificationGroupID],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Asset] WITH (NOLOCK) 
				WHERE
										[AssetID] = @AssetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_GetByAssetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset_GetByAssetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_GetByAssetID] TO [sp_executeall]
GO
