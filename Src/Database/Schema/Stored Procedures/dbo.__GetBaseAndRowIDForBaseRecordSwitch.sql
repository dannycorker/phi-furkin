SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 11-07-2012
-- Description:	Gets a base and row id for the given lead id so that inbound calls
--              can be associated with out bound calls
-- =============================================
CREATE PROCEDURE [dbo].[__GetBaseAndRowIDForBaseRecordSwitch]
	@ClientID INT,
	@LeadID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @RowID_DetailFieldID INT,
			@BaseID_DetailFieldID INT,
			@Deleted_DetailFieldID INT,
			@RowID_ThirdPartyFieldID INT = 101,
			@BaseID_ThirdPartyFieldID INT = 110,
			@Deleted_ThirdPartyFieldID INT = 108,
			@CallHistory_DetailFieldID INT,
			@LeadTypeID INT

	SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID
	
	SELECT @CallHistory_DetailFieldID=DetailFieldID, @RowID_DetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND leadtypeID=@LeadTypeID AND ThirdPartyFieldID=@RowID_ThirdPartyFieldID
	
	SELECT @BaseID_DetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND leadtypeID=@LeadTypeID AND ThirdPartyFieldID=@BaseID_ThirdPartyFieldID

	SELECT @Deleted_DetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND leadtypeID=@LeadTypeID AND ThirdPartyFieldID=@Deleted_ThirdPartyFieldID

	SELECT tdvRowID.ValueInt RowID, tdvBaseID.ValueInt BaseID FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRowID WITH (NOLOCK) ON tdvRowID.TableRowID = tr.TableRowID AND tdvRowID.DetailFieldID = @RowID_DetailFieldID AND tdvRowID.ValueInt > 0 
	INNER JOIN dbo.TableDetailValues tdvBaseID WITH (NOLOCK) ON tdvBaseID.TableRowID = tr.TableRowID AND tdvBaseID.DetailFieldID = @BaseID_DetailFieldID AND tdvBaseID.ValueInt > 0 
	INNER JOIN dbo.TableDetailValues tdvDeleted WITH (NOLOCK) ON tdvDeleted.TableRowID = tr.TableRowID AND tdvDeleted.DetailFieldID = @Deleted_DetailFieldID AND tdvDeleted.ValueInt <> 1 
	WHERE tr.DetailFieldID = @CallHistory_DetailFieldID AND tr.LeadID=@LeadID AND tr.ClientID = @ClientID


END




GO
GRANT VIEW DEFINITION ON  [dbo].[__GetBaseAndRowIDForBaseRecordSwitch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetBaseAndRowIDForBaseRecordSwitch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetBaseAndRowIDForBaseRecordSwitch] TO [sp_executeall]
GO
