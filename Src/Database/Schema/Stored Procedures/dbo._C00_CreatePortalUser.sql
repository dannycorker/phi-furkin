SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-11
-- Description:	Create a Portal User and use the CLR to generate a salt and password for it
-- UPDATES:		Simon Brushett, 2011-12-08, Changed to call into the CP_Insert_Helper proc as direct insert into CP no longer allowed
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreatePortalUser] 
	@ClientID int,
	@CustomerID int,
	@ImpersonateUserID int,
	@Username varchar(201),
	@Password varchar(65) = null,
	@LeadID int = null,
	@CaseID int = null,
	@ContinueIfUserExists bit = 1, 
	@PortalUserGroupID int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PortalUserID int,
	@EncryptedPassword varchar(65), 
	@Salt varchar(50), 
	@TestClientID int 

	DECLARE @Temp TABLE (
		EncryptedPassword varchar(65), 
		Salt varchar(250)
	)

	/* Check to see if there is already a Portal user with these details */
	SELECT @PortalUserID = p.PortalUserID, 
	@TestClientID = p.ClientID 
	FROM dbo.PortalUser p (nolock) 
	WHERE p.Username = @Username
	
	/* 
		1) If there is no such Portal user, then carry on.
		2) If this Portal user already exists, but it belongs to this client 
		   AND the @ContinueIfUserExists flag is true then carry on.
		3) Otherwise this is an error scenario, so do not do any more work at all.
	*/
	IF (@PortalUserID IS NULL) OR (@TestClientID = @ClientID AND @ContinueIfUserExists = 1)
	BEGIN
	
		IF (@PortalUserID IS NULL) 
		BEGIN
		
			/* Call the CLR application to generate a salt and encrypt our password */
			INSERT INTO @Temp
			EXEC CreatePassword @Password 

			/* Get the results from the temp table variables */
			SELECT TOP 1 @EncryptedPassword = t.EncryptedPassword, 
			@Salt = t.Salt 
			FROM @Temp t 

			/* If the CLR call worked then create the new Portal user */
			IF @EncryptedPassword IS NOT NULL AND @Salt IS NOT NULL
			BEGIN
			
				/* Create the new Portal user */
				EXEC @PortalUserID = PortalUser_Insert_Helper @ClientID, @CustomerID, @Username, @EncryptedPassword, @Salt, @ImpersonateUserID, 1, 0, @PortalUserGroupID

			END
		END
					
		/* 
			Only assign Lead/Case access if LeadID has been passed in, otherwise assume that it is not wanted.
			Only create one PortalUserCase record where CaseID is null,
			and create one PortalUserCase per CaseID. 
		*/
		IF @PortalUserID > 0 AND @LeadID > 0 AND NOT EXISTS (SELECT * FROM dbo.PortalUserCase puc (nolock) WHERE puc.PortalUserID = @PortalUserID AND LeadID = @LeadID AND (CaseID IS NULL OR CaseID = @CaseID))
		BEGIN
		
			/* Assign some or all cases for this lead to this new Portal User. */
			INSERT INTO dbo.PortalUserCase (PortalUserID, LeadID, CaseID)
			VALUES (@PortalUserID, @LeadID, @CaseID)
			
		END
				
	END

	Return @PortalUserID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreatePortalUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreatePortalUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreatePortalUser] TO [sp_executeall]
GO
