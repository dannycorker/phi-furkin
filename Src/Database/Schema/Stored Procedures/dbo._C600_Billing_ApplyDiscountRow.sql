SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-02
-- Description:	Add a discount line-item via the billing module
-- 2017-07-11 CPS create item with PurchasedProductPaymentScheduleTypeID 4 (Discount) and return to using StatusID 7 (Free)
-- 2017-07-14 CPS change ActualCollectionDate to NULL
-- 2017-07-16 CPS zero out discount rows, adjust PurchasedProduct to accomodate
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_ApplyDiscountRow]
(
 	 @MatterID		INT
	,@PaymentGross	DECIMAL(18,2) = 0.00
	,@PaymentTax 	DECIMAL(18,2) = 0.00
	,@PaymentNet 	DECIMAL(18,2) = 0.00
	,@PaymentDate	DATE
	,@LeadEventID	INT
	,@CoverFrom		DATE = NULL
	,@CoverTo		DATE = NULL
)
AS
BEGIN
PRINT OBJECT_NAME(@@ProcID)

	DECLARE  @ClientID							INT
			,@WhoCreated						INT
			,@WhenCreated						DATETIME
			,@PurchasedProductID				INT
			,@CustomerID						INT
			,@AccountID							INT
			,@PurchasedProductPaymentScheduleID INT
			,@EventComments						VARCHAR(2000) = ''


	UPDATE pp
	SET  ProductCostGross		-= @PaymentGross
		,ProductCostNet			-= @PaymentNet
		,ProductCostVAT			-= @PaymentTax
		,ProductAdditionalFee	+= @PaymentGross
	FROM PurchasedProduct pp WITH ( NOLOCK ) 
	WHERE pp.PurchasedProductID = @PurchasedProductID

	SELECT @EventComments += 'Product Gross Value reduced by ' + ISNULL(CONVERT(VARCHAR,@PaymentGross),'NULL') + CHAR(13)+CHAR(10)
			
	/*CPS 2017-07-16 Zero out Payment Schedule*/
	SELECT	 @PaymentGross	= 0.00
			,@PaymentTax	= 0.00
			,@PaymentNet	= 0.00

	SELECT	 @ClientID		= le.ClientID
			,@WhoCreated	= le.WhoCreated
			,@WhenCreated	= le.WhenCreated
			,@CustomerID	= l.CustomerID
	FROM LeadEvent le WITH ( NOLOCK )
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = le.LeadID 
	WHERE le.LeadEventID = @LeadEventID

	SELECT	 @PurchasedProductID	= pp.PurchasedProductID
			,@AccountID				= pp.AccountID
	FROM PurchasedProduct pp WITH ( NOLOCK ) 
	WHERE pp.PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(@MatterID,@WhenCreated)
	
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @EventComments = '<BR><BR><font color="red">Error:  No account config found for MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + '  PurchasedProductID ' + ISNULL(CONVERT(VARCHAR,dbo.fn_C600_GetPurchasedProductForMatter(@MatterID,@WhenCreated)),'NULL') + '</font>'
		RAISERROR( @EventComments, 16, 1 )
		RETURN
	END
	ELSE
	IF @AccountID is NULL
	BEGIN
		SELECT @EventComments = '<BR><BR><font color="red">Error:  No AccountID found for MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + 'PurchasedProductID ' + ISNULL(CONVERT(VARCHAR,dbo.fn_C600_GetPurchasedProductForMatter(@MatterID,@WhenCreated)),'NULL') + '</font>'
		RAISERROR( @EventComments, 16, 1 )
		RETURN
	END

	INSERT PurchasedProductPaymentSchedule ( ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, PurchasedProductPaymentScheduleTypeID, WhoCreated, WhenCreated )
	VALUES ( @ClientID, @CustomerID, @AccountID, @PurchasedProductID, NULL, @CoverFrom, @CoverTo, @PaymentDate, @PaymentNet, @PaymentTax, @PaymentGross, 7 /*New*/, 4 /*Discount*/, @WhoCreated, @WhenCreated )

	SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
	
	SELECT @EventComments += 'PurchasedProductID ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductID),'NULL') + '.  Discount Value £' + ISNULL(CONVERT(VARCHAR,@PaymentGross),'NULL') + ' scheduled for ' + ISNULL(CONVERT(VARCHAR,@PaymentDate,121),'NULL') + CHAR(13)+CHAR(10)
	EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	
	RETURN @PurchasedProductPaymentScheduleID
	
END












GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ApplyDiscountRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_ApplyDiscountRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ApplyDiscountRow] TO [sp_executeall]
GO
