SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Country table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_Get_List]

AS


				
				SELECT
					[CountryID],
					[CountryName],
					[CountryNamePlain],
					[Alpha2Code],
					[Alpha3Code],
					[NumericCode],
					[ISO3166Dash2Code],
					[WhenCreated],
					[WhenModified],
					[UsePostcode],
					[PostcodeRegex],
					[DiallingCode]
				FROM
					[dbo].[Country] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Get_List] TO [sp_executeall]
GO
