SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-03
-- Description:	Update the download details for a zip file
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipFile__UpdateDownloaded] 
	@DocumentZipFileID int, 
	@WhoDownloaded int, 
	@WhenDownloaded datetime = null 
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/* Assume the download datetime is right now unless told otherwise */
	UPDATE dbo.DocumentZipFile 
	SET WhoDownloaded = @WhoDownloaded, 
	WhenDownloaded = COALESCE(@WhenDownloaded, dbo.fn_GetDate_Local()) 
	WHERE DocumentZipFileID = @DocumentZipFileID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__UpdateDownloaded] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipFile__UpdateDownloaded] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__UpdateDownloaded] TO [sp_executeall]
GO
