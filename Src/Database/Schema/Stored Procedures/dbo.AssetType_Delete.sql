SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AssetType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_Delete]
(

	@AssetTypeID int   
)
AS


				DELETE FROM [dbo].[AssetType] WITH (ROWLOCK) 
				WHERE
					[AssetTypeID] = @AssetTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Delete] TO [sp_executeall]
GO
