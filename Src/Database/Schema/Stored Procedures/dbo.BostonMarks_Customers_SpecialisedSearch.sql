SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2014-02-06
-- Description:	Customised Search for BostonMarks Client 252 (Barnet Council) (searched Proxy cardholder fields)
-- =============================================
CREATE PROCEDURE [dbo].[BostonMarks_Customers_SpecialisedSearch] 
	@ClientID int,
	@SearchTerm varchar(MAX),
	@IdentifierID int = 157355,
	@StartRow INT = NULL,
	@EndRow INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT * FROM (
	SELECT ROW_NUMBER() OVER (order by Results.Fullname) as RowNumber, Results.* FROM (
		SELECT 
			'Primary' AS Type,
			'Customer' AS Match,
			mdv.DetailValue AS DetailValue,
			c.CustomerID,
			m.LeadID,
			m.CaseID,
			c.Fullname,
			c.Address1,
			c.Address2,
			c.Town,
			c.County,
			c.PostCode,
			c.HomeTelephone,
			c.EmailAddress,
			c.DateOfBirth
		FROM Customers c WITH (NOLOCK)
		LEFT JOIN Titles t WITH (NOLOCK) ON t.TitleID = c.TitleID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CustomerID = c.CustomerID
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @IdentifierID
		WHERE (t.Title LIKE '%' + @SearchTerm + '%'
		OR c.FirstName LIKE '%' + @SearchTerm + '%'
		OR c.LastName LIKE '%' + @SearchTerm + '%'
		OR c.FullName LIKE '%' + @SearchTerm + '%'
		OR c.DateOfBirth LIKE '%' + @SearchTerm + '%'
		OR c.Address1 LIKE '%' + @SearchTerm + '%'
		OR c.County LIKE '%' + @SearchTerm + '%'
		OR c.PostCode LIKE '%' + @SearchTerm + '%'
		OR c.HomeTelephone LIKE '%' + @SearchTerm + '%'
		OR c.EmailAddress LIKE '%' + @SearchTerm + '%')
		AND c.ClientID = @ClientID
		AND c.Test = 0

		UNION ALL

		SELECT
			'Primary' AS Type,
			'ParisNo' AS Match,
			mdv.DetailValue,
			c.CustomerID,
			m.LeadID,
			m.CaseID,
			c.Fullname,
			c.Address1,
			c.Address2,
			c.Town,
			c.County,
			c.PostCode,
			c.HomeTelephone,
			c.EmailAddress,
			c.DateOfBirth
		FROM Customers c WITH (NOLOCK)
		INNER JOIN Matter m WITH (NOLOCK) ON m.CustomerID = c.CustomerID
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @IdentifierID
		WHERE mdv.DetailValue LIKE '%' + @SearchTerm  + '%'
		AND c.ClientID = @ClientID
		AND c.Test = 0

		UNION ALL

		SELECT
			'Proxy' AS Type,
			--'DetailValue: ' + CAST(mdv.DetailFieldID AS varchar) AS Match,
			'Proxy<br />Cardholder' AS Match,
			mdv.DetailValue,
			c.CustomerID,
			m.LeadID,
			m.CaseID,
			c.Fullname,
			c.Address1,
			c.Address2,
			c.Town,
			c.County,
			c.PostCode,
			c.HomeTelephone,
			c.EmailAddress,
			c.DateOfBirth
		FROM Customers c WITH (NOLOCK)
		INNER JOIN Matter m WITH (NOLOCK) ON m.CustomerID = c.CustomerID
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID IN (162084, 161095, 161096, 161097, 161098, 161099, 161100, 161101)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = mdv.DetailFieldID
		WHERE mdv.DetailValue LIKE '%' + @SearchTerm  + '%'
		AND c.ClientID = @ClientID
		and mdv.DetailValue > ''
		AND c.Test = 0
		) AS Results
	)
	AS PagedResults
	WHERE (@StartRow IS NULL) OR (RowNumber Between @StartRow AND @EndRow)
	ORDER BY PagedResults.Fullname
END





GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_Customers_SpecialisedSearch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BostonMarks_Customers_SpecialisedSearch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_Customers_SpecialisedSearch] TO [sp_executeall]
GO
