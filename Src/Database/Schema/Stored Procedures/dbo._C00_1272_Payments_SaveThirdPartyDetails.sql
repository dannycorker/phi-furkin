SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-10-02
-- Description:	Saves the pay third party data
-- Used by:		df_157806
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Payments_SaveThirdPartyDetails] 
(
	@MatterID INT,
	@Type VARCHAR(2000),
	@Title VARCHAR(2000),
	@Organisation VARCHAR(2000),
	@FirstName VARCHAR(2000),
	@LastName VARCHAR(2000),
	@Address1 VARCHAR(2000),
	@Address2 VARCHAR(2000),
	@Address3 VARCHAR(2000),
	@Address4 VARCHAR(2000),
	@Address5 VARCHAR(2000),
	@Postcode VARCHAR(2000),
	@Country VARCHAR(2000)
)

AS
BEGIN
	SET NOCOUNT ON;
	
	EXEC _C00_SimpleValueIntoField 157817, @Type, @MatterID
	EXEC _C00_SimpleValueIntoField 157873, @Title, @MatterID
	EXEC _C00_SimpleValueIntoField 157818, @Organisation, @MatterID
	EXEC _C00_SimpleValueIntoField 157808, @FirstName, @MatterID
	EXEC _C00_SimpleValueIntoField 157809, @LastName, @MatterID
	EXEC _C00_SimpleValueIntoField 157810, @Address1, @MatterID
	EXEC _C00_SimpleValueIntoField 157811, @Address2, @MatterID
	EXEC _C00_SimpleValueIntoField 157812, @Address3, @MatterID
	EXEC _C00_SimpleValueIntoField 157813, @Address4, @MatterID
	EXEC _C00_SimpleValueIntoField 157814, @Address5, @MatterID
	EXEC _C00_SimpleValueIntoField 157815, @Postcode, @MatterID
	EXEC _C00_SimpleValueIntoField 157816, @Country, @MatterID
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_SaveThirdPartyDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Payments_SaveThirdPartyDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_SaveThirdPartyDetails] TO [sp_executeall]
GO
