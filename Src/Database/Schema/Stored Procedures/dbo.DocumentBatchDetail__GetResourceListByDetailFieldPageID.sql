SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ===================================================================
-- Author: Jan Wilson
-- Create date: 2013-03-05
-- Description:	Gets a pivoted resource list excluding hidden fields
-- ===================================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail__GetResourceListByDetailFieldPageID]
(	
	@ResourceListDetailFieldPageID int	
)
AS
	Declare @cols VARCHAR(MAX)=''
	Declare @fieldIdents VARCHAR(MAX)=''
	
	select @cols = @cols + ',[' + replace(replace(dbo.DetailFields.FieldCaption, '[', ''), ']', '') + ']',
	       @fieldIdents = @fieldIdents + ',' + CAST(DetailFieldID as varchar(50))
	from detailfields with (nolock) 
	where detailfieldpageid = @ResourceListDetailFieldPageID and enabled = 1 and Hidden = 0
	order by FieldOrder

	DECLARE @query VARCHAR(MAX)

	SELECT @cols=STUFF(@cols, 1, 1, ''), @fieldIdents=STUFF(@fieldIdents, 1, 1, '')
	
	SET @query = 'SELECT * From (
		SELECT rdv.ResourceListID, replace(replace(df.FieldCaption, ''['', ''''), '']'', '''') as FieldCaption, ISNULL(ll.ItemValue, rdv.DetailValue) as DetailValue
		FROM dbo.ResourceListDetailValues rdv with (nolock)
		INNER JOIN dbo.DetailFields df with (nolock) ON rdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID AND df.QuestionTypeID IN (2, 4)
		WHERE (df.DetailFieldID 
		IN (' + @fieldIdents + ')) ) src
	PIVOT (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt
	ORDER by pvt.ResourceListID
	'

	EXEC(@query)



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail__GetResourceListByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail__GetResourceListByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail__GetResourceListByDetailFieldPageID] TO [sp_executeall]
GO
