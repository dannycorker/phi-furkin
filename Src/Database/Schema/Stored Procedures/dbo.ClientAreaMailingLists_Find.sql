SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientAreaMailingLists table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientAreaMailingListID int   = null ,

	@Name varchar (200)  = null ,

	@Email varchar (255)  = null ,

	@PostCode varchar (10)  = null ,

	@OutcomeID int   = null ,

	@YellowPagesAreaCode varchar (10)  = null ,

	@ClientID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@OnHold bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientAreaMailingListID]
	, [Name]
	, [Email]
	, [PostCode]
	, [OutcomeID]
	, [YellowPagesAreaCode]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [OnHold]
    FROM
	[dbo].[ClientAreaMailingLists] WITH (NOLOCK) 
    WHERE 
	 ([ClientAreaMailingListID] = @ClientAreaMailingListID OR @ClientAreaMailingListID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Email] = @Email OR @Email IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
	AND ([OutcomeID] = @OutcomeID OR @OutcomeID IS NULL)
	AND ([YellowPagesAreaCode] = @YellowPagesAreaCode OR @YellowPagesAreaCode IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([OnHold] = @OnHold OR @OnHold IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientAreaMailingListID]
	, [Name]
	, [Email]
	, [PostCode]
	, [OutcomeID]
	, [YellowPagesAreaCode]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [OnHold]
    FROM
	[dbo].[ClientAreaMailingLists] WITH (NOLOCK) 
    WHERE 
	 ([ClientAreaMailingListID] = @ClientAreaMailingListID AND @ClientAreaMailingListID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Email] = @Email AND @Email is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	OR ([OutcomeID] = @OutcomeID AND @OutcomeID is not null)
	OR ([YellowPagesAreaCode] = @YellowPagesAreaCode AND @YellowPagesAreaCode is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([OnHold] = @OnHold AND @OnHold is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Find] TO [sp_executeall]
GO
