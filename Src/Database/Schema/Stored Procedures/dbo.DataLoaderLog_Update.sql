SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderLog table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderLog_Update]
(

	@DataLoaderLogID int   ,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderMapSectionID int   ,

	@DataLoaderFileID int   ,

	@DataLoaderFieldDefinitionID int   ,

	@DataLoaderObjectFieldID int   ,

	@DetailFieldID int   ,

	@ObjectName varchar (250)  ,

	@RowIndex int   ,

	@ColIndex int   ,

	@NodeName varchar (2000)  ,

	@Message varchar (2000)  ,

	@LogLevel int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderLog]
				SET
					[ClientID] = @ClientID
					,[DataLoaderMapID] = @DataLoaderMapID
					,[DataLoaderMapSectionID] = @DataLoaderMapSectionID
					,[DataLoaderFileID] = @DataLoaderFileID
					,[DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID
					,[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
					,[DetailFieldID] = @DetailFieldID
					,[ObjectName] = @ObjectName
					,[RowIndex] = @RowIndex
					,[ColIndex] = @ColIndex
					,[NodeName] = @NodeName
					,[Message] = @Message
					,[LogLevel] = @LogLevel
				WHERE
[DataLoaderLogID] = @DataLoaderLogID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderLog_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Update] TO [sp_executeall]
GO
