SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the IncomingPostEventValue table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEventValue_GetByIncomingPostEventValueID]
(

	@IncomingPostEventValueID int   
)
AS


				SELECT
					[IncomingPostEventValueID],
					[IncomingPostEventID],
					[PostKey],
					[PostValue],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID]
				FROM
					[dbo].[IncomingPostEventValue] WITH (NOLOCK) 
				WHERE
										[IncomingPostEventValueID] = @IncomingPostEventValueID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_GetByIncomingPostEventValueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEventValue_GetByIncomingPostEventValueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_GetByIncomingPostEventValueID] TO [sp_executeall]
GO
