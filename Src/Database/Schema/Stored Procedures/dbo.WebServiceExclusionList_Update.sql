SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the WebServiceExclusionList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WebServiceExclusionList_Update]
(

	@WebServiceExclusionListID int   ,

	@ClientPersonnelID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[WebServiceExclusionList]
				SET
					[ClientPersonnelID] = @ClientPersonnelID
				WHERE
[WebServiceExclusionListID] = @WebServiceExclusionListID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WebServiceExclusionList_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Update] TO [sp_executeall]
GO
