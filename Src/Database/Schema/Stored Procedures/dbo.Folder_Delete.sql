SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Folder table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_Delete]
(

	@FolderID int   
)
AS


				DELETE FROM [dbo].[Folder] WITH (ROWLOCK) 
				WHERE
					[FolderID] = @FolderID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Delete] TO [sp_executeall]
GO
