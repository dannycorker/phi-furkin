SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_SyncClientRelationshipChanges]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@AquariumClientID int,
		@ClientRelationshipID int,
		@TestID int,
		@ClientRelationshipName varchar(1000),
		@TableRowIDToUpdate int,
		@ReceivingClientID int,
		@OutgoingLeadTypeID int,
		@IncomingLeadTypeID int,
		@OutgoingEventTypeID int,
		@IncomingEventTypeID int,
		@Enabled int,
		@ClearLeadRefs int,
		@TestCount int,
		@NewTestCount int,
		@NumberOfUpdatesDone int

Declare @ClientRelationship TABLE (
	[BeforeOrAfter] varchar(100),
	[ClientRelationshipID] [int],
	[ClientRelationshipName] [varchar](2000),
	[OutgoingClientID] [int] NOT NULL,
	[ReceivingClientID] [int],
	[OutgoingLeadTypeID] [int],
	[IncomingLeadTypeID] [int],
	[OutgoingEventTypeID] [int],
	[IncomingEventTypeID] [int],
	[Enabled] [bit],
	[ClearLeadRefs] [bit]
)

	Insert Into @ClientRelationship(BeforeOrAfter,
	ClientRelationshipID,
	ClientRelationshipName,
	OutgoingClientID,
	ReceivingClientID,
	OutgoingLeadTypeID,
	IncomingLeadTypeID,
	OutgoingEventTypeID,
	IncomingEventTypeID,
	Enabled,
	ClearLeadRefs)


	Select 'Before', 
	tdv_ClientRelationshipID.DetailValue,
	tdv_RelationshipName.Detailvalue,
	ldv.DetailValue,
	tdv_ReceivingClientID.DetailValue,
	tdv_OutgoingLeadTypeID.DetailValue,
	tdv_IncomingLeadTypeID.DetailValue,
	tdv_OutgoingEventTypeID.DetailValue,
	tdv_IncomingEventTypeID.DetailValue,
	Case tdv_Enabled.DetailValue WHEN 'true' THEN 1 ELSE 0 END,
	Case tdv_ClearLeadRefs.DetailValue  WHEN 'true' THEN 1 ELSE 0 END
	From tabledetailvalues tdv_RelationshipName
	inner join TableDetailValues tdv_ClientRelationshipID ON tdv_ClientRelationshipID.TableRowID = tdv_RelationshipName.TableRowID and tdv_ClientRelationshipID.DetailFieldID = 36709
	inner join TableDetailValues tdv_ReceivingClientID ON tdv_ReceivingClientID.TableRowID = tdv_RelationshipName.TableRowID and tdv_ReceivingClientID.DetailFieldID = 36702
	inner join TableDetailValues tdv_OutgoingLeadTypeID ON tdv_OutgoingLeadTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_OutgoingLeadTypeID.DetailFieldID = 36703
	inner join TableDetailValues tdv_IncomingLeadTypeID ON tdv_IncomingLeadTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_IncomingLeadTypeID.DetailFieldID = 36704
	inner join TableDetailValues tdv_OutgoingEventTypeID ON tdv_OutgoingEventTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_OutgoingEventTypeID.DetailFieldID = 36705
	inner join TableDetailValues tdv_IncomingEventTypeID ON tdv_IncomingEventTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_IncomingEventTypeID.DetailFieldID = 36706
	inner join TableDetailValues tdv_Enabled ON tdv_Enabled.TableRowID = tdv_RelationshipName.TableRowID and tdv_Enabled.DetailFieldID = 36707
	inner join TableDetailValues tdv_ClearLeadRefs ON tdv_ClearLeadRefs.TableRowID = tdv_RelationshipName.TableRowID and tdv_ClearLeadRefs.DetailFieldID = 36708
	Inner Join LeadDetailValues ldv on ldv.LeadID = tdv_RelationshipName.leadid and ldv.DetailFieldID = 6453
	where tdv_RelationshipName.DetailFieldID = 36701
	and tdv_ReceivingClientID.DetailValue IS NOT NULL
	Order by convert(numeric,tdv_ClientRelationshipID.DetailValue)

Insert Into @ClientRelationship(BeforeOrAfter,ClientRelationshipID,ClientRelationshipName,OutgoingClientID,ReceivingClientID,OutgoingLeadTypeID,IncomingLeadTypeID,OutgoingEventTypeID,IncomingEventTypeID,Enabled,ClearLeadRefs)
Select 'Before', ClientRelationshipID,ClientRelationshipName,OutgoingClientID,ReceivingClientID,OutgoingLeadTypeID,IncomingLeadTypeID,OutgoingEventTypeID,IncomingEventTypeID,Enabled,ClearLeadRefs
From ClientRelationship
Inner Join LeadDetailValues ldv on ldv.DetailValue = ClientRelationship.OutgoingClientID and ldv.DetailFieldID = 6453

Select @NumberOfUpdatesDone = 0

select @TestCount = count(*)
from @ClientRelationship
Group By BeforeOrAfter, ClientRelationshipID,	ClientRelationshipName,	OutgoingClientID,	ReceivingClientID,	OutgoingLeadTypeID,	IncomingLeadTypeID,	OutgoingEventTypeID,	IncomingEventTypeID,	Enabled,	ClearLeadRefs
Having Count(*) <> 2 

If @TestCount > 0

	Begin

	select @ClientRelationshipID = 0, @TestID = 0

	select top 1 @ClientRelationshipID = ClientRelationshipID
	from @ClientRelationship
	Group By ClientRelationshipID,	ClientRelationshipName,	OutgoingClientID,	ReceivingClientID,	OutgoingLeadTypeID,	IncomingLeadTypeID,	OutgoingEventTypeID,	IncomingEventTypeID,	Enabled,	ClearLeadRefs
	Having Count(*) <> 2
	order by ClientRelationshipID

		while (@ClientRelationshipID > 0 AND @ClientRelationshipID > @TestID)
		begin

		Select	@ClientRelationshipName = ClientRelationshipName,
				@ReceivingClientID = ReceivingClientID,
				@OutgoingLeadTypeID = OutgoingLeadTypeID,
				@IncomingLeadTypeID = IncomingLeadTypeID,
				@OutgoingEventTypeID = OutgoingEventTypeID,
				@IncomingEventTypeID = IncomingEventTypeID,
				@Enabled = Enabled,
				@ClearLeadRefs = ClearLeadRefs
		From ClientRelationship
		Where ClientRelationshipID = @ClientRelationshipID

		Select @TableRowIDToUpdate = TableRowID
		From TableDetailValues 
		Where DetailFieldID = 36709
		And DetailValue = @ClientRelationshipID

		If @TableRowIDToUpdate IS NOT NULL
			Begin 

			Update TableDetailValues
			Set DetailValue = Case DetailFieldID 
					WHEN 36701 Then convert(varchar,@ClientRelationshipName) 
					WHEN 36702 Then convert(varchar,@ReceivingClientID) 
					WHEN 36703 Then convert(varchar,@OutgoingLeadTypeID) 
					WHEN 36704 Then convert(varchar,@IncomingLeadTypeID) 
					WHEN 36705 Then convert(varchar,@OutgoingEventTypeID) 
					WHEN 36706 Then convert(varchar,@IncomingEventTypeID) 
					WHEN 36707 Then Case @Enabled WHEN 1 THEN 'true' ELSE 'false' END 
					WHEN 36708 Then Case @ClearLeadRefs WHEN 1 THEN 'true' ELSE 'false' END ELSE '' END
			From TableDetailValues
			Where TablerowID = @TableRowIDToUpdate
			And DetailFieldID IN (36701,36702,36703,36704,36705,36706,36707,36708)
			END
		Select @NumberOfUpdatesDone = @NumberOfUpdatesDone + 1
		set @TestID = @ClientRelationshipID

		select top 1 @ClientRelationshipID = ClientRelationshipID
		from @ClientRelationship
		Group By ClientRelationshipID,	ClientRelationshipName,	OutgoingClientID,	ReceivingClientID,	OutgoingLeadTypeID,	IncomingLeadTypeID,	OutgoingEventTypeID,	IncomingEventTypeID,	Enabled,	ClearLeadRefs
		Having Count(*) <> 2
		and ClientRelationshipID > @ClientRelationshipID
		order by ClientRelationshipID

		END
		/* */

	END

	Insert Into @ClientRelationship(BeforeOrAfter,
	ClientRelationshipID,
	ClientRelationshipName,
	OutgoingClientID,
	ReceivingClientID,
	OutgoingLeadTypeID,
	IncomingLeadTypeID,
	OutgoingEventTypeID,
	IncomingEventTypeID,
	Enabled,
	ClearLeadRefs)


	Select 'After', 
	tdv_ClientRelationshipID.DetailValue,
	tdv_RelationshipName.Detailvalue,
	ldv.DetailValue,
	tdv_ReceivingClientID.DetailValue,
	tdv_OutgoingLeadTypeID.DetailValue,
	tdv_IncomingLeadTypeID.DetailValue,
	tdv_OutgoingEventTypeID.DetailValue,
	tdv_IncomingEventTypeID.DetailValue,
	Case tdv_Enabled.DetailValue WHEN 'true' THEN 1 ELSE 0 END,
	Case tdv_ClearLeadRefs.DetailValue  WHEN 'true' THEN 1 ELSE 0 END
	From tabledetailvalues tdv_RelationshipName
	inner join TableDetailValues tdv_ClientRelationshipID ON tdv_ClientRelationshipID.TableRowID = tdv_RelationshipName.TableRowID and tdv_ClientRelationshipID.DetailFieldID = 36709
	inner join TableDetailValues tdv_ReceivingClientID ON tdv_ReceivingClientID.TableRowID = tdv_RelationshipName.TableRowID and tdv_ReceivingClientID.DetailFieldID = 36702
	inner join TableDetailValues tdv_OutgoingLeadTypeID ON tdv_OutgoingLeadTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_OutgoingLeadTypeID.DetailFieldID = 36703
	inner join TableDetailValues tdv_IncomingLeadTypeID ON tdv_IncomingLeadTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_IncomingLeadTypeID.DetailFieldID = 36704
	inner join TableDetailValues tdv_OutgoingEventTypeID ON tdv_OutgoingEventTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_OutgoingEventTypeID.DetailFieldID = 36705
	inner join TableDetailValues tdv_IncomingEventTypeID ON tdv_IncomingEventTypeID.TableRowID = tdv_RelationshipName.TableRowID and tdv_IncomingEventTypeID.DetailFieldID = 36706
	inner join TableDetailValues tdv_Enabled ON tdv_Enabled.TableRowID = tdv_RelationshipName.TableRowID and tdv_Enabled.DetailFieldID = 36707
	inner join TableDetailValues tdv_ClearLeadRefs ON tdv_ClearLeadRefs.TableRowID = tdv_RelationshipName.TableRowID and tdv_ClearLeadRefs.DetailFieldID = 36708
	Inner Join LeadDetailValues ldv on ldv.LeadID = tdv_RelationshipName.leadid and ldv.DetailFieldID = 6453
	where tdv_RelationshipName.DetailFieldID = 36701
	and tdv_ReceivingClientID.DetailValue IS NOT NULL
	Order by convert(numeric,tdv_ClientRelationshipID.DetailValue)

	Insert Into @ClientRelationship(BeforeOrAfter,ClientRelationshipID,ClientRelationshipName,OutgoingClientID,ReceivingClientID,OutgoingLeadTypeID,IncomingLeadTypeID,OutgoingEventTypeID,IncomingEventTypeID,Enabled,ClearLeadRefs)
	Select 'After', ClientRelationshipID,ClientRelationshipName,OutgoingClientID,ReceivingClientID,OutgoingLeadTypeID,IncomingLeadTypeID,OutgoingEventTypeID,IncomingEventTypeID,Enabled,ClearLeadRefs
	From ClientRelationship
	Inner Join LeadDetailValues ldv on ldv.DetailValue = ClientRelationship.OutgoingClientID and ldv.DetailFieldID = 6453

--	select *
--	from @ClientRelationship
--	Group By BeforeOrAfter, ClientRelationshipID,	ClientRelationshipName,	OutgoingClientID,	ReceivingClientID,	OutgoingLeadTypeID,	IncomingLeadTypeID,	OutgoingEventTypeID,	IncomingEventTypeID,	Enabled,	ClearLeadRefs
--	Having Count(*) <> 2
--	order by ClientRelationshipID

--	Select @NewTestCount = @@RowCount 
--	If @NewTestCount <> @TestCount and @NewTestCount = 0 Select 'After', '', 'There are no mismatches'

--Select 'Number of Relationship Changes Posted to TDV = ' + convert(varchar,@NumberOfUpdatesDone)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_SyncClientRelationshipChanges] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_SyncClientRelationshipChanges] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_SyncClientRelationshipChanges] TO [sp_executeall]
GO
