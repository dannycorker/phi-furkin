SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the OutcomeCriterias table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_Find]
(

	@SearchUsingOR bit   = null ,

	@OutcomeCriteriaID int   = null ,

	@OutcomeID int   = null ,

	@QuestionPossibleAnswerID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OutcomeCriteriaID]
	, [OutcomeID]
	, [QuestionPossibleAnswerID]
	, [ClientID]
    FROM
	[dbo].[OutcomeCriterias] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeCriteriaID] = @OutcomeCriteriaID OR @OutcomeCriteriaID IS NULL)
	AND ([OutcomeID] = @OutcomeID OR @OutcomeID IS NULL)
	AND ([QuestionPossibleAnswerID] = @QuestionPossibleAnswerID OR @QuestionPossibleAnswerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OutcomeCriteriaID]
	, [OutcomeID]
	, [QuestionPossibleAnswerID]
	, [ClientID]
    FROM
	[dbo].[OutcomeCriterias] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeCriteriaID] = @OutcomeCriteriaID AND @OutcomeCriteriaID is not null)
	OR ([OutcomeID] = @OutcomeID AND @OutcomeID is not null)
	OR ([QuestionPossibleAnswerID] = @QuestionPossibleAnswerID AND @QuestionPossibleAnswerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Find] TO [sp_executeall]
GO
