SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderMap table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderMapID int   = null ,

	@ClientID int   = null ,

	@MapName varchar (250)  = null ,

	@MapDescription varchar (2000)  = null ,

	@WhenCreated datetime   = null ,

	@CreatedBy int   = null ,

	@LastUpdated datetime   = null ,

	@UpdatedBy int   = null ,

	@EnabledForUse bit   = null ,

	@Deleted bit   = null ,

	@LeadTypeID int   = null ,

	@UseDefaultLeadForCustomer bit   = null ,

	@UseDefaultCaseForLead bit   = null ,

	@UseDefaultMatterForCase bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderMapID]
	, [ClientID]
	, [MapName]
	, [MapDescription]
	, [WhenCreated]
	, [CreatedBy]
	, [LastUpdated]
	, [UpdatedBy]
	, [EnabledForUse]
	, [Deleted]
	, [LeadTypeID]
	, [UseDefaultLeadForCustomer]
	, [UseDefaultCaseForLead]
	, [UseDefaultMatterForCase]
    FROM
	[dbo].[DataLoaderMap] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderMapID] = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([MapName] = @MapName OR @MapName IS NULL)
	AND ([MapDescription] = @MapDescription OR @MapDescription IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([LastUpdated] = @LastUpdated OR @LastUpdated IS NULL)
	AND ([UpdatedBy] = @UpdatedBy OR @UpdatedBy IS NULL)
	AND ([EnabledForUse] = @EnabledForUse OR @EnabledForUse IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([UseDefaultLeadForCustomer] = @UseDefaultLeadForCustomer OR @UseDefaultLeadForCustomer IS NULL)
	AND ([UseDefaultCaseForLead] = @UseDefaultCaseForLead OR @UseDefaultCaseForLead IS NULL)
	AND ([UseDefaultMatterForCase] = @UseDefaultMatterForCase OR @UseDefaultMatterForCase IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderMapID]
	, [ClientID]
	, [MapName]
	, [MapDescription]
	, [WhenCreated]
	, [CreatedBy]
	, [LastUpdated]
	, [UpdatedBy]
	, [EnabledForUse]
	, [Deleted]
	, [LeadTypeID]
	, [UseDefaultLeadForCustomer]
	, [UseDefaultCaseForLead]
	, [UseDefaultMatterForCase]
    FROM
	[dbo].[DataLoaderMap] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderMapID] = @DataLoaderMapID AND @DataLoaderMapID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([MapName] = @MapName AND @MapName is not null)
	OR ([MapDescription] = @MapDescription AND @MapDescription is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([LastUpdated] = @LastUpdated AND @LastUpdated is not null)
	OR ([UpdatedBy] = @UpdatedBy AND @UpdatedBy is not null)
	OR ([EnabledForUse] = @EnabledForUse AND @EnabledForUse is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([UseDefaultLeadForCustomer] = @UseDefaultLeadForCustomer AND @UseDefaultLeadForCustomer is not null)
	OR ([UseDefaultCaseForLead] = @UseDefaultCaseForLead AND @UseDefaultCaseForLead is not null)
	OR ([UseDefaultMatterForCase] = @UseDefaultMatterForCase AND @UseDefaultMatterForCase is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Find] TO [sp_executeall]
GO
