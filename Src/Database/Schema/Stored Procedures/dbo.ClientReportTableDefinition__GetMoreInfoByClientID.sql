SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Richard Doyle
-- Create date: 2015-01-26
-- Description:	Show more information about a report table
-- =============================================
CREATE PROCEDURE [dbo].[ClientReportTableDefinition__GetMoreInfoByClientID] 
	@ClientID INT,
	@ClientReportTableDefinitionID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT crtd.ClientReportTableDefinitionID, 
	crtd.ClientID, 
	crtd.TableName, 
	crtd.LeadTypeID, 
	crtd.TableSubtypeID, 
	crtd.WhoCreated, 
	crtd.WhenCreated, 
	crtd.WhoModified, 
	crtd.WhenModified, 
	cp.UserName AS WhoCreatedUserName, 
	lt.LeadTypeName, 
	ISNULL(dfs.DetailFieldSubTypeName, '') AS TableSubtypeName 
	FROM dbo.ClientReportTableDefinition crtd WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = crtd.WhoCreated 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = crtd.LeadTypeID 
	LEFT JOIN dbo.DetailFieldSubType dfs WITH (NOLOCK) ON dfs.DetailFieldSubTypeID = crtd.TableSubtypeID 
	WHERE crtd.ClientID = @ClientID AND crtd.ClientReportTableDefinitionID = @ClientReportTableDefinitionID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableDefinition__GetMoreInfoByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientReportTableDefinition__GetMoreInfoByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableDefinition__GetMoreInfoByClientID] TO [sp_executeall]
GO
