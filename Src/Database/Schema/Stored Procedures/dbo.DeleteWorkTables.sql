SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-05
-- Description: Clear down all work tables at the end of the day
-- =============================================
CREATE PROCEDURE [dbo].[DeleteWorkTables] 
AS
BEGIN
	SET NOCOUNT ON;

	/* Let CascadeDeletes remove all work table entries that were using this ID */ 
    DELETE dbo.WorkSeed 
    WHERE WhenCreated IS NULL 
    
    DELETE dbo.WorkSeed 
    WHERE WhenCreated < dbo.fn_GetDate_Local() - 1 
    
	/* Let CascadeDeletes remove all WorkWins table entries that were using this ID */ 
    DELETE dbo.WorkWinSeed 
    WHERE WhenCreated < dbo.fn_GetDate_Local() - 1 
    
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteWorkTables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteWorkTables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteWorkTables] TO [sp_executeall]
GO
