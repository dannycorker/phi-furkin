SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Bill table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill_Update]
(

	@BillID int   ,

	@ClientID int   ,

	@Reference varchar (100)  ,

	@Description varchar (2000)  ,

	@CreatedDate datetime   ,

	@DiaryAppointmentID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@BillStatusID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Bill]
				SET
					[ClientID] = @ClientID
					,[Reference] = @Reference
					,[Description] = @Description
					,[CreatedDate] = @CreatedDate
					,[DiaryAppointmentID] = @DiaryAppointmentID
					,[CustomerID] = @CustomerID
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[BillStatusID] = @BillStatusID
				WHERE
[BillID] = @BillID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Update] TO [sp_executeall]
GO
