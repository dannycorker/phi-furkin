SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the NotificationGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroupMember_Update]
(

	@NotificationGroupMemberID int   ,

	@NotificationGroupID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[NotificationGroupMember]
				SET
					[NotificationGroupID] = @NotificationGroupID
					,[ClientID] = @ClientID
					,[ClientPersonnelID] = @ClientPersonnelID
				WHERE
[NotificationGroupMemberID] = @NotificationGroupMemberID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupMember_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Update] TO [sp_executeall]
GO
