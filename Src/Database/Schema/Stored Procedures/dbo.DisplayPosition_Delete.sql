SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DisplayPosition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DisplayPosition_Delete]
(

	@DisplayPositionNum smallint   
)
AS


				DELETE FROM [dbo].[DisplayPosition] WITH (ROWLOCK) 
				WHERE
					[DisplayPositionNum] = @DisplayPositionNum
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DisplayPosition_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Delete] TO [sp_executeall]
GO
