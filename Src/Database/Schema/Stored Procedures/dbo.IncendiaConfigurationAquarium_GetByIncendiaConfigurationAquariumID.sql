SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the IncendiaConfigurationAquarium table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationAquarium_GetByIncendiaConfigurationAquariumID]
(

	@IncendiaConfigurationAquariumID int   
)
AS


				SELECT
					[IncendiaConfigurationAquariumID],
					[AccountID]
				FROM
					[dbo].[IncendiaConfigurationAquarium] WITH (NOLOCK) 
				WHERE
										[IncendiaConfigurationAquariumID] = @IncendiaConfigurationAquariumID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_GetByIncendiaConfigurationAquariumID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationAquarium_GetByIncendiaConfigurationAquariumID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationAquarium_GetByIncendiaConfigurationAquariumID] TO [sp_executeall]
GO
