SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the WorkflowGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@WorkflowGroupID int   = null ,

	@Name varchar (100)  = null ,

	@Description varchar (255)  = null ,

	@ManagerID int   = null ,

	@ClientID int   = null ,

	@Enabled bit   = null ,

	@SortOrder varchar (4)  = null ,

	@AssignedLeadsOnly bit   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [WorkflowGroupID]
	, [Name]
	, [Description]
	, [ManagerID]
	, [ClientID]
	, [Enabled]
	, [SortOrder]
	, [AssignedLeadsOnly]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[WorkflowGroup] WITH (NOLOCK) 
    WHERE 
	 ([WorkflowGroupID] = @WorkflowGroupID OR @WorkflowGroupID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([ManagerID] = @ManagerID OR @ManagerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([SortOrder] = @SortOrder OR @SortOrder IS NULL)
	AND ([AssignedLeadsOnly] = @AssignedLeadsOnly OR @AssignedLeadsOnly IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [WorkflowGroupID]
	, [Name]
	, [Description]
	, [ManagerID]
	, [ClientID]
	, [Enabled]
	, [SortOrder]
	, [AssignedLeadsOnly]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[WorkflowGroup] WITH (NOLOCK) 
    WHERE 
	 ([WorkflowGroupID] = @WorkflowGroupID AND @WorkflowGroupID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([ManagerID] = @ManagerID AND @ManagerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([SortOrder] = @SortOrder AND @SortOrder is not null)
	OR ([AssignedLeadsOnly] = @AssignedLeadsOnly AND @AssignedLeadsOnly is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Find] TO [sp_executeall]
GO
