SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-10-20
-- Description:	Gets a list LeadDocuments without the blobs
-- JWG 2012-07-12 Use LeadDocument again now that it has all the details back again.
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocument__GetByLeadDocumentHeaders]
(
	@TvpIDs dbo.tvpInt READONLY
)
AS
BEGIN

	SELECT
		[LeadDocumentID],
		[ClientID],
		[LeadID],
		[DocumentTypeID],
		[LeadDocumentTitle],
		[UploadDateTime],
		[WhoUploaded],
		CAST('' as varbinary) AS [DocumentBLOB],
		[FileName],
		CAST('' as varbinary) AS [EmailBLOB],
		[DocumentFormat],
		[EmailFrom],
		[EmailTo],
		[CcList],
		[BccList],
		[ElectronicSignatureDocumentKey],
		[Encoding],
		[ContentFormat],
		[ZipFormat],
		[DocumentBlobSize], 
		[EmailBlobSize],
		[DocumentDatabaseID],
		[WhenArchived],
		[DocumentTypeVersionID]

	FROM
		dbo.LeadDocument ld WITH (NOLOCK)
	INNER JOIN @TvpIDs tvp ON ld.LeadDocumentID = tvp.AnyID

	SELECT @@ROWCOUNT
					
END	




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__GetByLeadDocumentHeaders] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument__GetByLeadDocumentHeaders] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__GetByLeadDocumentHeaders] TO [sp_executeall]
GO
