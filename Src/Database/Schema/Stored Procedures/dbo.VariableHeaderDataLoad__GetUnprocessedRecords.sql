SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-05-2015
-- Description:	Gets a list of unprocess records for every client
-- Modified 26/08/2015 By PR - Added ClientID for the Script 79 and 83 which now have AutomatedScriptParam's
-- =============================================
CREATE PROCEDURE [dbo].[VariableHeaderDataLoad__GetUnprocessedRecords]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM VariableHeaderDataLoad WITH (NOLOCK) 
	WHERE (Processed=0 OR Processed IS NULL) AND ClientID=@ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__GetUnprocessedRecords] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad__GetUnprocessedRecords] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__GetUnprocessedRecords] TO [sp_executeall]
GO
