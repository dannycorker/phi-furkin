SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date:	2019-11-26
-- Description:	De-queues a request in the Liberty Mediate API queue.
--
-- 2020-01-07 MAB Added header comment
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Mediate_DeQueue]
(
    @AppKey         [NVARCHAR](50),
    @Queue          [NVARCHAR](50)
) AS
BEGIN
 
	SET NOCOUNT ON

	DECLARE @RowCount INT

    UPDATE TOP(1)  [dbo].[_C00_Mediate_Queue]
    SET    [Action] = 'DeQueued'
    OUTPUT inserted.*
    WHERE	[AppKey] = @AppKey 
	AND		[Queue] = @Queue
	AND		[Flow] = 'Queue'
	AND		[Action] = 'Push'
    AND		ExceptionCount < 5

	SELECT @RowCount = @@ROWCOUNT

	IF @RowCount <> 0	
	BEGIN
		EXEC _C00_LogIt 'Info', '_C00_Mediate_DeQueue', @AppKey, @Queue, @RowCount
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_DeQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Mediate_DeQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_DeQueue] TO [sp_executeall]
GO
