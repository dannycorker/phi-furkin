SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-10-2014
-- Description:	Gets the last viewed lead for the given
--				client personnel identity
-- =============================================
CREATE PROCEDURE [dbo].[LeadViewHistory__GetLastViewedLead]
	@ClientPersonnelID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP 1 * FROM LeadViewHistory WITH (NOLOCK) 
	WHERE ClientPersonnelID=@ClientPersonnelID
	ORDER BY WhenViewed DESC

END


GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory__GetLastViewedLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory__GetLastViewedLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory__GetLastViewedLead] TO [sp_executeall]
GO
