SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-12-10
-- Description:	List all Portal Users
-- =============================================
CREATE PROCEDURE [dbo].[pu] 
	@ClientID int = NULL,
	@SortOrder tinyint = 1
AS
BEGIN
	SET NOCOUNT ON;

	SELECT pu.*, cl.CompanyName, c.Fullname 
	FROM dbo.PortalUser pu (nolock) 
	INNER JOIN dbo.Clients cl (nolock) on cl.ClientID = pu.ClientID 
	INNER JOIN dbo.Customers c (nolock) on c.CustomerID = pu.CustomerID 
	WHERE (pu.ClientID = @ClientID OR @ClientID IS NULL) 
	ORDER BY CASE @SortOrder WHEN 1 THEN right('0000000000' + cast(pu.PortalUserID as varchar), 10) WHEN 2 THEN pu.Username WHEN 3 THEN c.Fullname END 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[pu] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[pu] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[pu] TO [sp_executeall]
GO
