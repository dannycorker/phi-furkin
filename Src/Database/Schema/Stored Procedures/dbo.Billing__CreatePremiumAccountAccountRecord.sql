SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-20
-- Description:	Creates Account Record
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreatePremiumAccountAccountRecord]
(
	@ClientID INT,
	@ObjectID INT,
	@ObjectTypeID INT,
	@AccNumber VARCHAR(8) = NULL,
	@AccSortCode VARCHAR(6) = NULL,
	@AccountName VARCHAR(100),
	@ClientAccountID INT = NULL,
	@CustomerID INT,
	@WhoCreated INT,
	@AccountTypeID INT
	
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @AccountID INT

	/*Create a new account record in the billing system*/
	INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference,AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified, ObjectID, ObjectTypeID, ClientAccountID, AccountUseID)
	VALUES (@ClientID,
		   @CustomerID,
		   @AccountName,
		   'Premium Payment Account',
		   @AccNumber,
	       @AccSortCode,
		   'PR' + CAST(@ObjectID as VARCHAR),
		   @AccountTypeID,-- Bscs
		   1, -- Active
		   @WhoCreated,  -- who created 
		   dbo.fn_GetDate_Local(),
		   @WhoCreated, -- who modified
		   dbo.fn_GetDate_Local(),
		   @ObjectID,
		   @ObjectTypeID,
		   @ClientAccountID,
		   1) -- Account UseID (1= premium 2 = claims)
					   
	SELECT @AccountID = SCOPE_IDENTITY()
	
	SELECT @AccountID 

	RETURN @AccountID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePremiumAccountAccountRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreatePremiumAccountAccountRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePremiumAccountAccountRecord] TO [sp_executeall]
GO
