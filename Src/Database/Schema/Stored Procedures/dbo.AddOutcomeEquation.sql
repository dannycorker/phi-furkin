SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-31
-- Description:	Add an Outcome from eCatcher
-- =============================================
CREATE PROCEDURE [dbo].[AddOutcomeEquation]
	@OutcomeID  INT,
	@EquationID INT, 
	@ClientID INT
AS
BEGIN

	INSERT INTO OutcomeEquations (OutcomeID, EquationID, ClientID)
	VALUES (@OutcomeID, @EquationID, @ClientID)

	DECLARE @OutcomeEquationID INT

	SET @OutcomeEquationID = SCOPE_IDENTITY()

	SELECT OutcomeEquationID
	FROM OutcomeEquations
	WHERE OutcomeEquationID = @OutcomeEquationID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcomeEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddOutcomeEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcomeEquation] TO [sp_executeall]
GO
