SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeHelperField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeHelperField_Update]
(

	@EventTypeHelperFieldID int   ,

	@EventTypeID int   ,

	@DetailFieldID int   ,

	@LeadTypeID int   ,

	@ClientID int   ,

	@ForceEditableOverride bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeHelperField]
				SET
					[EventTypeID] = @EventTypeID
					,[DetailFieldID] = @DetailFieldID
					,[LeadTypeID] = @LeadTypeID
					,[ClientID] = @ClientID
					,[ForceEditableOverride] = @ForceEditableOverride
				WHERE
[EventTypeHelperFieldID] = @EventTypeHelperFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Update] TO [sp_executeall]
GO
