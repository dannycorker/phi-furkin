SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Someone a long time ago
-- Create date: Old old old
-- Description:	Updates a client personnel record in the days before the DAL
-- UPDATES:		Simon Brushett	2011-11-03	Added ForcePasswordChangeOn parameter
--				Simon Brushett	2012-01-17	Commented out line to update the email address so the trigger allows this proc to run.
-- =============================================

CREATE PROCEDURE [dbo].[UpdateClientPersonnel]
@ClientPersonnelID int,
@ClientID int,
@ClientOfficeID int,
@TitleID int,
@FirstName nvarchar(100),
@MiddleName nvarchar(100), 
@LastName nvarchar(100),
@JobTitle nvarchar(100),
@Password nvarchar(65), 
@MobileTelephone nvarchar(50), 
@EmailAddress nvarchar(255),
@HomeTelephone nvarchar(50),
@OfficeTelephone nvarchar(50),
@OfficeExt nvarchar(10),
@Salt nvarchar(25),
@AttemptedLogins int,
@AccountDisabled bit,
@ForcePasswordChangeOn DATETIME = NULL

as

Update ClientPersonnel 
SET ClientID=@ClientID, 
ClientOfficeID=@ClientOfficeID,
TitleID=@TitleID, 
FirstName=@FirstName,
MiddleName=@MiddleName, 
LastName=@LastName, 
JobTitle=@JobTitle, 
ClientPersonnel.Password=@Password, 
MobileTelephone=@MobileTelephone,
--EmailAddress=@EmailAddress, 
HomeTelephone=@HomeTelephone, 
OfficeTelephone=@OfficeTelephone, 
OfficeTelephoneExtension=@OfficeExt,
Salt = @Salt,
AttemptedLogins = @AttemptedLogins,
AccountDisabled = @AccountDisabled,
ForcePasswordChangeOn = ISNULL(@ForcePasswordChangeOn, ForcePasswordChangeOn)

WHERE ClientPersonnelID = @ClientPersonnelID




GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientPersonnel] TO [sp_executeall]
GO
