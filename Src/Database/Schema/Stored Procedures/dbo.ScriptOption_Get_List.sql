SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_Get_List]

AS


				
				SELECT
					[ScriptOptionID],
					[ClientID],
					[ClientPersonnelID],
					[ScriptEditorTheme],
					[ShowLineNumbers],
					[CollapseTopPanel],
					[CollapseLeftPanel],
					[CollapseRightPanel]
				FROM
					[dbo].[ScriptOption] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Get_List] TO [sp_executeall]
GO
