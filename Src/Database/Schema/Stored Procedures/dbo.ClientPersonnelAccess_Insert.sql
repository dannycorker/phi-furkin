SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnelAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_Insert]
(

	@ClientPersonnelAccessID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@AccessRuleID int   ,

	@SourceLeadEventID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@AccessLevel tinyint   
)
AS


				
				INSERT INTO [dbo].[ClientPersonnelAccess]
					(
					[ClientID]
					,[ClientPersonnelAdminGroupID]
					,[ClientPersonnelID]
					,[LeadID]
					,[CaseID]
					,[AccessRuleID]
					,[SourceLeadEventID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[AccessLevel]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelAdminGroupID
					,@ClientPersonnelID
					,@LeadID
					,@CaseID
					,@AccessRuleID
					,@SourceLeadEventID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@AccessLevel
					)
				-- Get the identity value
				SET @ClientPersonnelAccessID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Insert] TO [sp_executeall]
GO
