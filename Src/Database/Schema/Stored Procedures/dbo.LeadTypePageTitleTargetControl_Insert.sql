SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadTypePageTitleTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleTargetControl_Insert]
(

	@LeadTypePageTitleTargetControlID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@LastParsed datetime   
)
AS


				
				INSERT INTO [dbo].[LeadTypePageTitleTargetControl]
					(
					[ClientID]
					,[LeadTypeID]
					,[LastParsed]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@LastParsed
					)
				-- Get the identity value
				SET @LeadTypePageTitleTargetControlID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleTargetControl_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Insert] TO [sp_executeall]
GO
