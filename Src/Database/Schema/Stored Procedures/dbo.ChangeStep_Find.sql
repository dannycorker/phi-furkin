SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChangeStep table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStep_Find]
(

	@SearchUsingOR bit   = null ,

	@ChangeStepID int   = null ,

	@ClientID int   = null ,

	@ChangeInstanceID int   = null ,

	@StepTypeID int   = null ,

	@ChangeDocumentID int   = null ,

	@Comment varchar (255)  = null ,

	@ExternalUrl varchar (255)  = null ,

	@ChangeStepStateID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChangeStepID]
	, [ClientID]
	, [ChangeInstanceID]
	, [StepTypeID]
	, [ChangeDocumentID]
	, [Comment]
	, [ExternalUrl]
	, [ChangeStepStateID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ChangeStep] WITH (NOLOCK) 
    WHERE 
	 ([ChangeStepID] = @ChangeStepID OR @ChangeStepID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ChangeInstanceID] = @ChangeInstanceID OR @ChangeInstanceID IS NULL)
	AND ([StepTypeID] = @StepTypeID OR @StepTypeID IS NULL)
	AND ([ChangeDocumentID] = @ChangeDocumentID OR @ChangeDocumentID IS NULL)
	AND ([Comment] = @Comment OR @Comment IS NULL)
	AND ([ExternalUrl] = @ExternalUrl OR @ExternalUrl IS NULL)
	AND ([ChangeStepStateID] = @ChangeStepStateID OR @ChangeStepStateID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChangeStepID]
	, [ClientID]
	, [ChangeInstanceID]
	, [StepTypeID]
	, [ChangeDocumentID]
	, [Comment]
	, [ExternalUrl]
	, [ChangeStepStateID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ChangeStep] WITH (NOLOCK) 
    WHERE 
	 ([ChangeStepID] = @ChangeStepID AND @ChangeStepID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ChangeInstanceID] = @ChangeInstanceID AND @ChangeInstanceID is not null)
	OR ([StepTypeID] = @StepTypeID AND @StepTypeID is not null)
	OR ([ChangeDocumentID] = @ChangeDocumentID AND @ChangeDocumentID is not null)
	OR ([Comment] = @Comment AND @Comment is not null)
	OR ([ExternalUrl] = @ExternalUrl AND @ExternalUrl is not null)
	OR ([ChangeStepStateID] = @ChangeStepStateID AND @ChangeStepStateID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStep_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Find] TO [sp_executeall]
GO
