SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Contact table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact_Insert]
(

	@ContactID int    OUTPUT,

	@ClientID int   ,

	@CustomerID int   ,

	@TitleID int   ,

	@Firstname varchar (100)  ,

	@Middlename varchar (100)  ,

	@Lastname varchar (100)  ,

	@Fullname varchar (201)   OUTPUT,

	@EmailAddressWork varchar (100)  ,

	@EmailAddressOther varchar (100)  ,

	@DirectDial varchar (100)  ,

	@MobilePhoneWork varchar (100)  ,

	@MobilePhoneOther varchar (100)  ,

	@Address1 varchar (100)  ,

	@Address2 varchar (100)  ,

	@Town varchar (100)  ,

	@County varchar (100)  ,

	@Postcode varchar (100)  ,

	@Country varchar (100)  ,

	@OfficeID int   ,

	@DepartmentID int   ,

	@JobTitle varchar (100)  ,

	@Notes varchar (255)  ,

	@CountryID int   ,

	@LanguageID int   ,

	@WhenModified datetime   ,

	@WhoModified int   ,

	@Longitude numeric (25, 18)  ,

	@Latitude numeric (25, 18)  
)
AS


				
				INSERT INTO [dbo].[Contact]
					(
					[ClientID]
					,[CustomerID]
					,[TitleID]
					,[Firstname]
					,[Middlename]
					,[Lastname]
					,[EmailAddressWork]
					,[EmailAddressOther]
					,[DirectDial]
					,[MobilePhoneWork]
					,[MobilePhoneOther]
					,[Address1]
					,[Address2]
					,[Town]
					,[County]
					,[Postcode]
					,[Country]
					,[OfficeID]
					,[DepartmentID]
					,[JobTitle]
					,[Notes]
					,[CountryID]
					,[LanguageID]
					,[WhenModified]
					,[WhoModified]
					,[Longitude]
					,[Latitude]
					)
				VALUES
					(
					@ClientID
					,@CustomerID
					,@TitleID
					,@Firstname
					,@Middlename
					,@Lastname
					,@EmailAddressWork
					,@EmailAddressOther
					,@DirectDial
					,@MobilePhoneWork
					,@MobilePhoneOther
					,@Address1
					,@Address2
					,@Town
					,@County
					,@Postcode
					,@Country
					,@OfficeID
					,@DepartmentID
					,@JobTitle
					,@Notes
					,@CountryID
					,@LanguageID
					,@WhenModified
					,@WhoModified
					,@Longitude
					,@Latitude
					)
				-- Get the identity value
				SET @ContactID = SCOPE_IDENTITY()
									
				-- Select computed columns into output parameters
				SELECT
 @Fullname = [Fullname]
				FROM
					[dbo].[Contact] WITH (NOLOCK) 
				WHERE
[ContactID] = @ContactID 
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Insert] TO [sp_executeall]
GO
