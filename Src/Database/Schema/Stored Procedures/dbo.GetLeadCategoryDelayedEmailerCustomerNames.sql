SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE  [dbo].[GetLeadCategoryDelayedEmailerCustomerNames]

@ClientQuestionnaireID int

 AS

SELECT     dbo.OutcomeDelayedEmails.ClientQuestionnaireID, dbo.OutcomeDelayedEmails.DateOutcomeEmailSent, dbo.Customers.Fullname, 
                      dbo.OutcomeDelayedEmails.OutcomeDelayedEmailID, dbo.OutcomeDelayedEmails.CustomerID, Customers.ClientID 
FROM         dbo.Customers INNER JOIN
                      dbo.OutcomeDelayedEmails ON dbo.Customers.CustomerID = dbo.OutcomeDelayedEmails.CustomerID
WHERE     (dbo.OutcomeDelayedEmails.ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadCategoryDelayedEmailerCustomerNames] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadCategoryDelayedEmailerCustomerNames] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadCategoryDelayedEmailerCustomerNames] TO [sp_executeall]
GO
