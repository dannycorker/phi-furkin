SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SearchQueryValue table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_Find]
(

	@SearchUsingOR bit   = null ,

	@SearchQueryValueID int   = null ,

	@ClientID int   = null ,

	@SubClientID int   = null ,

	@SearchQueryID int   = null ,

	@SearchFieldID int   = null ,

	@SearchFieldOperatorID int   = null ,

	@FieldValue varchar (2000)  = null ,

	@FieldValueID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SearchQueryValueID]
	, [ClientID]
	, [SubClientID]
	, [SearchQueryID]
	, [SearchFieldID]
	, [SearchFieldOperatorID]
	, [FieldValue]
	, [FieldValueID]
    FROM
	[dbo].[SearchQueryValue] WITH (NOLOCK) 
    WHERE 
	 ([SearchQueryValueID] = @SearchQueryValueID OR @SearchQueryValueID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([SearchQueryID] = @SearchQueryID OR @SearchQueryID IS NULL)
	AND ([SearchFieldID] = @SearchFieldID OR @SearchFieldID IS NULL)
	AND ([SearchFieldOperatorID] = @SearchFieldOperatorID OR @SearchFieldOperatorID IS NULL)
	AND ([FieldValue] = @FieldValue OR @FieldValue IS NULL)
	AND ([FieldValueID] = @FieldValueID OR @FieldValueID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SearchQueryValueID]
	, [ClientID]
	, [SubClientID]
	, [SearchQueryID]
	, [SearchFieldID]
	, [SearchFieldOperatorID]
	, [FieldValue]
	, [FieldValueID]
    FROM
	[dbo].[SearchQueryValue] WITH (NOLOCK) 
    WHERE 
	 ([SearchQueryValueID] = @SearchQueryValueID AND @SearchQueryValueID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([SearchQueryID] = @SearchQueryID AND @SearchQueryID is not null)
	OR ([SearchFieldID] = @SearchFieldID AND @SearchFieldID is not null)
	OR ([SearchFieldOperatorID] = @SearchFieldOperatorID AND @SearchFieldOperatorID is not null)
	OR ([FieldValue] = @FieldValue AND @FieldValue is not null)
	OR ([FieldValueID] = @FieldValueID AND @FieldValueID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Find] TO [sp_executeall]
GO
