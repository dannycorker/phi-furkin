SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-01-21
-- Description:	Receive instruction from the app on which tables & columns to create
-- =============================================
CREATE PROCEDURE [dbo].[ClientReportTableBuilder__InitialInstruction] 
	@WhoRequested INT,
	@ClientID INT,
	@LeadTypeID INT = -1,
	@TableName VARCHAR(1024)= '',
	@TableSubtypeID INT = 0,
	@ActionType VARCHAR(10) = '',
	@Rows INT = 0,
	@DetailFieldPageID INT = 0,
	@Nolock BIT = 1,
	@LinkToiReporting BIT = 1
AS
BEGIN
	SET NOCOUNT ON
	
	/* A place to store all the instructions (in case "Whole Database" is selected in the app) */
	DECLARE @ActionList TABLE (
		LeadTypeID INT,
		TableName VARCHAR(1024),
		TableSubtypeID INT,
		DetailFieldPageID INT /* Use zero to create all pages for this LeadType */
	)
	
	/*
		Put all instructions in here and do them all at once 
		LeadTypeID -1 means model the whole client, all fields for all lead types.  All resources and tables too. 
		Add "_RL" to table names for Resource Lists and "_T" for Table pages
		
		If "Whole Database" is selected, we may need to clean up tables that have changed name, so just look at the tables
		that already exist for Archive and Drop commands, rather than what the current list of tables would be if created right now.
	*/
	IF @ActionType IN ('ARCHIVE', 'DROP')
	BEGIN
		IF @LeadTypeID = -1 BEGIN
			/*
				List tables to Archive or Drop report tables that already exist.
			*/
			INSERT INTO @ActionList (LeadTypeID, TableName, TableSubtypeID, DetailFieldPageID) 
			SELECT crtd.LeadTypeID, crtd.TableName, crtd.TableSubtypeID, crtd.TableDetailFieldPageID 
			FROM dbo.ClientReportTableDefinition crtd 
			INNER JOIN INFORMATION_SCHEMA.TABLES ist ON ist.TABLE_NAME = crtd.TableName /* Make sure the table did create successfully and is still there */
			WHERE crtd.ClientID = @ClientID 
		END
		ELSE
		BEGIN
			/* Just handle the one table specified by the app */
			INSERT INTO @ActionList (LeadTypeID, TableName, TableSubtypeID, DetailFieldPageID) 
			SELECT @LeadTypeID, @TableName, @TableSubtypeID, @DetailFieldPageID 
		END
	END
	ELSE
	BEGIN
		/*
			Create whole database, drop 1 existing table, recreate 1 or all existing tables...
		*/
		INSERT INTO @ActionList (LeadTypeID, TableName, TableSubtypeID, DetailFieldPageID) 
		SELECT lt.LeadTypeID, '_C' + CAST(@ClientID AS VARCHAR) + '_Report_' + dbo.fnGetValidSqlName(CASE WHEN @LeadTypeID > -1 AND @DetailFieldPageID > 0 AND @TableName > '' THEN @TableName ELSE dfp.PageName END, '') + CASE WHEN dfp.LeadOrMatter = 4 THEN '_RL' WHEN dfp.LeadOrMatter IN (6, 8) THEN '_T' ELSE '' END, dfp.LeadOrMatter, dfp.DetailFieldPageID
		FROM dbo.LeadType lt 
		INNER JOIN dbo.DetailFieldPages dfp ON dfp.LeadTypeID = lt.LeadTypeID 
		WHERE lt.ClientID IN (0, @ClientID) 
		AND (@LeadTypeID = -1 OR lt.LeadTypeID = @LeadTypeID) 
		AND lt.Enabled = 1 
		AND (@DetailFieldPageID = 0 OR dfp.DetailFieldPageID = @DetailFieldPageID) 
		AND dfp.Enabled = 1 
		AND dfp.ClientID = @ClientID /* LeadTypes may be shared, eg CustomerDetailValues, but pages are not shared */ 
		AND (@TableSubtypeID = 0 OR dfp.LeadOrMatter = @TableSubtypeID) 
	END
	
	/* Remove entries that will fail because the table already exists */
	IF @ActionType = 'CREATE'
	BEGIN
		DELETE @ActionList 
		FROM @ActionList a 
		INNER JOIN INFORMATION_SCHEMA.TABLES ist ON ist.TABLE_NAME = a.TableName 
	END
	
	
	/*
		Now issue commands for all the ActionList entries, based on the other parameters passed in.
		
		These will be picked up by ClientReportTableBuilder__ExecuteInstructions when Sql Server Agent next triggers it.
	*/
	
	
	/* @ActionType REFRESH means drop and create */
	IF @ActionType IN ('DROP')
	BEGIN
		INSERT INTO dbo.ClientReportTableBuilder 
		(
			ClientID,
			TableName,
			LeadTypeID,
			TableSubtypeID,
			ActionType,
			WhoRequested,
			WhenRequested,
			UserNotes
		)
		SELECT
		
			@ClientID,
			a.TableName,
			a.LeadTypeID,
			a.TableSubtypeID,
			@ActionType,
			@WhoRequested,
			dbo.fn_GetDate_Local(),
			'Drop ' + @TableName 
		FROM @ActionList a
	END
	

	/* @ActionType REFRESH means drop and create */
	IF @ActionType IN ('ARCHIVE', 'RECREATE')
	BEGIN
		INSERT INTO dbo.ClientReportTableBuilder 
		(
			ClientID,
			TableName,
			LeadTypeID,
			TableSubtypeID,
			ActionType,
			WhoRequested,
			WhenRequested,
			UserNotes
		)
		SELECT
		
			@ClientID,
			a.TableName,
			a.LeadTypeID,
			a.TableSubtypeID,
			'ARCHIVE',
			@WhoRequested,
			dbo.fn_GetDate_Local(),
			'Archive ' + a.TableName + CASE @ActionType WHEN 'RECREATE' THEN ' to refresh it ' ELSE '' END 
		FROM @ActionList a
	END

	IF @ActionType IN ('CREATE', 'RECREATE')
	BEGIN
		INSERT INTO dbo.ClientReportTableBuilder 
		(
			ClientID,
			TableName,
			LeadTypeID,
			TableSubtypeID,
			TableDetailFieldPageID,
			ActionType,
			MaxRecords,
			LockType,
			WhoRequested,
			WhenRequested,
			UserNotes
		)
		SELECT
		
			@ClientID,
			a.TableName,
			a.LeadTypeID,
			a.TableSubtypeID,
			a.DetailFieldPageID,
			'CREATE' + CASE WHEN TableSubtypeID = 4 THEN '_RL' ELSE '' END,
			CASE WHEN @Rows > 0 THEN @Rows ELSE NULL END, 
			CASE WHEN @Nolock = 1 THEN 'NOLOCK' ELSE 'TABLOCKX' END, 
			@WhoRequested,
			dbo.fn_GetDate_Local(),
			'Create ' + a.TableName + CASE @ActionType WHEN 'RECREATE' THEN ' to refresh it ' ELSE '' END 
		FROM @ActionList a
		
	
		;WITH FieldNames AS 
		(
			SELECT dbo.fnGetValidSqlName(df.FieldName, '') as NameOnly,
			df.ClientID, 
			df.DetailFieldID, 
			df.LeadOrMatter,
			a.LeadTypeID,
			a.TableName,
			a.DetailFieldPageID,
			ROW_NUMBER() OVER(PARTITION BY dbo.fnGetValidSqlName(df.FieldName, '') ORDER BY df.DetailFieldID ) as rn 
			FROM @ActionList a 
			INNER JOIN dbo.DetailFields df ON df.DetailFieldPageID = a.DetailFieldPageID
			WHERE df.QuestionTypeID < 15 /* Avoid Tables and other strange columns */
			AND a.TableSubtypeID <> 4    /* ResourceList columns are created separately */
			AND df.Enabled = 1

		),
		Renames AS 
		(
			/* Duplicate field names (when stripped of invalid sql characters), get the DetailFieldID tagged on at the end in order to make them unique */
			SELECT NameOnly + CASE WHEN rn > 1 THEN '_' + CAST(f.DetailFieldID AS VARCHAR) ELSE '' END AS UniqueName,
			f.ClientID, 
			f.DetailFieldID, 
			f.LeadOrMatter,
			f.LeadTypeID,
			f.TableName,
			f.DetailFieldPageID
			FROM FieldNames f 
		)
		INSERT INTO dbo.ClientReportTableBuilder 
		(
			ClientID,
			TableName,
			ColumnName,
			LeadTypeID,
			TableSubtypeID,
			DetailFieldID,
			TableDetailFieldPageID,
			ActionType,
			MaxRecords,
			LockType,
			WhoRequested,
			WhenRequested,
			UserNotes
		)
		SELECT @ClientID,
			r.TableName,
			r.UniqueName,
			r.LeadTypeID,
			r.LeadOrMatter,
			r.DetailFieldID,
			r.DetailFieldPageID,
			'ADD',
			CASE WHEN @Rows > 0 THEN @Rows ELSE NULL END, 
			CASE WHEN @Nolock = 1 THEN 'NOLOCK' ELSE 'TABLOCKX' END, 
			@WhoRequested,
			dbo.fn_GetDate_Local(),
			'Add ' + r.UniqueName + ' to ' + @TableName
		FROM Renames r 
		ORDER BY r.UniqueName 
		
		;WITH FieldNames AS 
		(
			SELECT dbo.fnGetValidSqlName(df.FieldName, '') as NameOnly,
			df.ClientID, 
			df.DetailFieldID, 
			df.LeadOrMatter,
			a.LeadTypeID,
			a.TableName,
			a.DetailFieldPageID,
			ROW_NUMBER() OVER(PARTITION BY dbo.fnGetValidSqlName(df.FieldName, '') ORDER BY df.DetailFieldID ) as rn 
			FROM @ActionList a 
			INNER JOIN dbo.DetailFields df ON df.DetailFieldPageID = a.DetailFieldPageID
			WHERE df.QuestionTypeID < 15 /* Avoid Tables and other strange columns */
			AND a.TableSubtypeID <> 4    /* ResourceList columns are created separately */
			AND df.Enabled = 1
		),
		Renames AS 
		(
			/* Duplicate field names (when stripped of invalid sql characters), get the DetailFieldID tagged on at the end in order to make them unique */
			SELECT NameOnly + CASE WHEN rn > 1 THEN '_' + CAST(f.DetailFieldID AS VARCHAR) ELSE '' END AS UniqueName,
			f.ClientID, 
			f.DetailFieldID, 
			f.LeadOrMatter,
			f.LeadTypeID,
			f.TableName,
			f.DetailFieldPageID
			FROM FieldNames f 
		)
		INSERT INTO dbo.ClientReportTableBuilder 
		(
			ClientID,
			TableName,
			ColumnName,
			LeadTypeID,
			TableSubtypeID,
			DetailFieldID,
			TableDetailFieldPageID,
			ActionType,
			MaxRecords,
			LockType,
			WhoRequested,
			WhenRequested,
			UserNotes
		)
		SELECT @ClientID,
			r.TableName,
			r.UniqueName,
			r.LeadTypeID,
			r.LeadOrMatter,
			r.DetailFieldID,
			r.DetailFieldPageID,
			'POPULATE',
			CASE WHEN @Rows > 0 THEN @Rows ELSE NULL END, 
			CASE WHEN @Nolock = 1 THEN 'NOLOCK' ELSE 'TABLOCKX' END, 
			@WhoRequested,
			dbo.fn_GetDate_Local(),
			'Populate ' + r.UniqueName + ' with data '
		FROM Renames r 
		ORDER BY r.UniqueName 
		
		/* 
			Very important: only link the tables to iReporting after all the tables have been created and the columns have been added.
			We can then create extra links to the new _RL tables to decode ResourceList fields.
		*/
		IF @LinkToiReporting = 1
		BEGIN
			INSERT INTO dbo.ClientReportTableBuilder 
			(
				ClientID,
				TableName,
				LeadTypeID,
				TableSubtypeID,
				ActionType,
				WhoRequested,
				WhenRequested,
				UserNotes
			)
			SELECT
				@ClientID,
				a.TableName,
				a.LeadTypeID,
				a.TableSubtypeID,
				'LINK',
				@WhoRequested,
				dbo.fn_GetDate_Local(),
				'Link ' + a.TableName + ' to iReporting '
			FROM @ActionList a 
			
		END
		
	END /* Create or Recreate */

	
	/* Return success */
	RETURN 1
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableBuilder__InitialInstruction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientReportTableBuilder__InitialInstruction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableBuilder__InitialInstruction] TO [sp_executeall]
GO
