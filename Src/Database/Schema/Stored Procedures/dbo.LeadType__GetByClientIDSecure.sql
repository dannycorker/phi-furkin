SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By:  Jim Green
-- Purpose: Select Lead Type records, as long as the user has access to them
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadType__GetByClientIDSecure]
(
	@ClientID int,
	@UserID int
)
AS
	
	--SELECT @IncludeDisabledLeadTypes = ISNULL(@IncludeDisabledLeadTypes, 1)

	SET ANSI_NULLS OFF
	
	SELECT lt.*				
	FROM [dbo].[LeadType] lt WITH (NOLOCK)
	INNER JOIN dbo.fnLeadTypeSecure(@UserID) f ON lt.LeadTypeID = f.LeadTypeID
	WHERE [ClientID] = @ClientID	
	ORDER BY [LeadTypeName]
	
	Select @@ROWCOUNT
	SET ANSI_NULLS ON
			


GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType__GetByClientIDSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDSecure] TO [sp_executeall]
GO
