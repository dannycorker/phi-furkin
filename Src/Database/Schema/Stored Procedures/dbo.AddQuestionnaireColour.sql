SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddQuestionnaireColour    Script Date: 08/09/2006 12:22:41 ******/
CREATE PROCEDURE [dbo].[AddQuestionnaireColour]

@ClientQuestionnaireID int,
@Page nvarchar(50),
@Header nvarchar(50),
@Intro nvarchar(50),
@Body nvarchar(50),
@Footer nvarchar(50), 
@ClientID int

AS

insert into QuestionnaireColours (ClientQuestionnaireID, Page, Header, Intro, Body, Footer, ClientID)
values (@ClientQuestionnaireID, @Page, @Header, @Intro, @Body, @Footer, @ClientID)

declare @QuestionnaireColourID int

set @QuestionnaireColourID = SCOPE_IDENTITY()

select QuestionnaireColourID from QuestionnaireColours where QuestionnaireColourID = @QuestionnaireColourID





GO
GRANT VIEW DEFINITION ON  [dbo].[AddQuestionnaireColour] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddQuestionnaireColour] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddQuestionnaireColour] TO [sp_executeall]
GO
