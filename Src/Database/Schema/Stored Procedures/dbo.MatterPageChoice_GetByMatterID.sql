SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterPageChoice table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_GetByMatterID]
(

	@MatterID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[MatterPageChoiceID],
					[ClientID],
					[MatterID],
					[DetailFieldPageID],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[MatterPageChoice] WITH (NOLOCK) 
				WHERE
					[MatterID] = @MatterID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_GetByMatterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_GetByMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_GetByMatterID] TO [sp_executeall]
GO
