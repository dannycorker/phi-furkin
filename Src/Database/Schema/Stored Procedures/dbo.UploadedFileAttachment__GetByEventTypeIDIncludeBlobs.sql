SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2012
-- Description:	Gets an UploadedFileAttachment record by the given event type id
-- 2015-08-04 ACE Added CaseID and ClientPersonnelID into the accepted params #33696
-- 2020-09-09 ACE Updated to use functionality from AQ Alpha
-- 2020-09-09 NG  Updated to match AQ Alpha
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFileAttachment__GetByEventTypeIDIncludeBlobs]
	
	@EventTypeID INT,
	@CaseID INT = NULL,
	@ClientPersonnelID INT = NULL

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @OverrideQueryID INT
			,@OverrideQueryText VARCHAR(MAX)

	/*ALM/GPR 2020-09-14 for PPET*/
	DECLARE @MatterID INT = (SELECT m.MatterID
							FROM Matter m WITH (NOLOCK)
							WHERE m.CaseID = @CaseID)

	SELECT @OverrideQueryID = o.QueryID
	FROM Overrides o WITH (NOLOCK) 
	WHERE o.EventTypeID = @EventTypeID
	AND o.OverrideTypeID = 7 /*Uploaded File Attachment*/

	IF @OverrideQueryID is not NULL
	BEGIN

		SELECT @OverrideQueryText = REPLACE(REPLACE(REPLACE(REPLACE(sq.QueryText,'@CaseID',ISNULL(CONVERT(VARCHAR(MAX),@CaseID),'')),'@CurrentUser',ISNULL(CONVERT(VARCHAR(MAX),@ClientPersonnelID),'')),'@EventTypeID',ISNULL(CONVERT(VARCHAR,@EventTypeID),'')),'@MatterID',ISNULL(CONVERT(VARCHAR,@MatterID),''))
		FROM SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @OverrideQueryID

		PRINT @OverrideQueryText
		EXEC (@OverrideQueryText)

	END
    ELSE
    BEGIN

        SELECT ufa.*, uf.UploadedFileTitle, uf.UploadedFileName, uf.UploadedFile
        FROM UploadedFileAttachment ufa WITH (NOLOCK)
        INNER JOIN dbo.UploadedFile uf WITH (NOLOCK) ON uf.UploadedFileID = ufa.UploadedFileID
        WHERE EventTypeID=@EventTypeID

    END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__GetByEventTypeIDIncludeBlobs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFileAttachment__GetByEventTypeIDIncludeBlobs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__GetByEventTypeIDIncludeBlobs] TO [sp_executeall]
GO
