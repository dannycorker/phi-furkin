SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Currency table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Currency_Insert]
(

	@CurrencyID int   ,

	@CurrencyCode char (3)  ,

	@CurrencyName varchar (100)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@MajorNamePlural varchar (50)  ,

	@MajorNameSingular varchar (50)  ,

	@MinorNamePlural varchar (50)  ,

	@MinorNameSingular varchar (50)  ,

	@CurrencySymbol nvarchar (2)  
)
AS


				
				INSERT INTO [dbo].[Currency]
					(
					[CurrencyID]
					,[CurrencyCode]
					,[CurrencyName]
					,[WhenCreated]
					,[WhenModified]
					,[MajorNamePlural]
					,[MajorNameSingular]
					,[MinorNamePlural]
					,[MinorNameSingular]
					,[CurrencySymbol]
					)
				VALUES
					(
					@CurrencyID
					,@CurrencyCode
					,@CurrencyName
					,@WhenCreated
					,@WhenModified
					,@MajorNamePlural
					,@MajorNameSingular
					,@MinorNamePlural
					,@MinorNameSingular
					,@CurrencySymbol
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Currency_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Insert] TO [sp_executeall]
GO
