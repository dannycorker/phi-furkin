SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-12-03
-- Description:	Gets a list of custom panel requirements for a particular PortalUserGroup (Add, Hide, Rename)
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserGroup__GetCustomPanelItems]
(
	@PortalUserGroupID int,
	@ClientID int	
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	/* Dummy code for now */				
	SELECT p.PanelID, p.PanelName, pic.*, 1 AS [Accessible]
	FROM dbo.PanelItemCharting pic WITH (NOLOCK)
	INNER JOIN dbo.Panels p WITH (NOLOCK) ON pic.PanelID = p.PanelID
	INNER JOIN dbo.PortalUserGroupPanelItemChartingAccess pugpica WITH (NOLOCK) ON pugpica.PanelItemChartingID = pic.PanelItemChartingID
		AND pugpica.PortalUserGroupID = @PortalUserGroupID
		AND pugpica.HasAccess = 1
	WHERE pic.ClientID = @ClientID
	ORDER BY p.PanelID

END



GRANT EXEC ON [dbo].[PortalUserGroup__GetCustomPanelItems] TO sp_executeall




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetCustomPanelItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup__GetCustomPanelItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetCustomPanelItems] TO [sp_executeall]
GO
