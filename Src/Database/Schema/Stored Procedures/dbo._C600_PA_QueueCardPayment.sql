SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/10/2016
-- Description:	Queues a card transaction 
-- MODS
-- JL 2016-12-05 Changed TransactionID to customer ref
-- DCM 2016-12-19 Change settlement date to be today 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_QueueCardPayment]
	@ClientID INT = NULL,
	@MatterID INT,
	@LeadEventID INT

AS
BEGIN
	
	SET NOCOUNT ON;

	IF @ClientID is NULL
	BEGIN
		SELECT @ClientID = dbo.fnGetPrimaryClientID()
	END
	
	-- Inserts a payment record with the customer payment schedule ID

	DECLARE @Payments TABLE ( PaymentScheduleID INT, MatterID INT, PaymentMethod VARCHAR(30), Done INT )

	DECLARE 
			@AccountID INT,
			@DetailValue VARCHAR(200), 
			@LeadTypeID INT,
			@ObjectID INT,
			@PaymentScheduleID INT,
			@PaymentTypeID VARCHAR(30),
			@SettlementDate DATE,
			@CaseID INT,
			@Now DATE = dbo.fn_GetDate_Local(),
			@Amount DECIMAL (18,2) 

	
	SELECT @LeadTypeID=l.LeadTypeID, @ClientID=l.ClientID , @CaseID = m.CaseID
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=m.LeadID
	WHERE m.MatterID = @MatterID
	
	SELECT @SettlementDate=CAST(dbo.fn_GetDate_Local() AS DATE)
	
	INSERT INTO @Payments ( PaymentScheduleID, MatterID, PaymentMethod, Done )
	SELECT cps.CustomerPaymentScheduleID, m.MatterID, paymeth.DetailValue, 0
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
	INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID
	LEFT JOIN MatterDetailValues suspended WITH (NOLOCK) ON m.MatterID=suspended.MatterID AND suspended.DetailFieldID=175275
	INNER JOIN MatterDetailValues paymeth WITH (NOLOCK) ON m.MatterID=paymeth.MatterID AND paymeth.DetailFieldID=170115
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToMatterID=m.MatterID AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1493
	INNER JOIN Matter pam WITH (NOLOCK) ON ltr.FromMatterID=pam.MatterID
	INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON pam.MatterID=pstat.MatterID AND pstat.DetailFieldID=170038
	INNER JOIN Cases pac WITH (NOLOCK) ON pam.CaseID=pac.CaseID
	INNER JOIN MatterDetailValues aid WITH (NOLOCK) ON m.MatterID=aid.MatterID AND aid.DetailFieldID=176973
	INNER JOIN Account a WITH (NOLOCK) ON aid.ValueInt=a.AccountID
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON a.AccountID=cps.AccountID
	WHERE  c.Test=0 AND c.ClientID=@ClientID
	-- and not cancelled
	AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=pac.CaseID AND le.EventTypeID=150145 AND le.WhenFollowedUp IS NULL AND le.EventDeleted=0 )
	-- and we are doing collections for this customer
	AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID=ca.CaseID AND le.EventDeleted=0 AND le.EventTypeID=155198 )
	-- and collections are not on hold
	AND ( suspended.ValueInt <> 5144 OR suspended.ValueInt IS NULL )
	-- and policy status is live
	AND pstat.ValueInt=43002
	-- and payment schedule entry has not already been queued in the GL
	AND cps.CustomerLedgerID IS NULL
	-- and payment date is before or the same as settlement day
	AND cps.ActualCollectionDate <= @SettlementDate
	-- and just for this matter
	AND m.MatterID = @MatterID
	-- and payment value is not zero
	AND (cps.PaymentGross>0)
		  
	SELECT TOP 1 @MatterID=p.MatterID, 
				 @PaymentScheduleID=p.PaymentScheduleID,
				 @PaymentTypeID=p.PaymentMethod
	FROM @Payments p 
	WHERE p.Done=0
	ORDER BY p.PaymentScheduleID

	UPDATE LeadEvent Set Comments = Comments 
										+ CASE WHEN Comments='' THEN '' ELSE ' ' END 
										+ 'Collecting CustomerPaymentScheduleID: ' 
										+  CAST(@PaymentScheduleID AS VARCHAR)
	WHERE LeadEventID=@LeadEventID
		
	-- move data into card transaction fields
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4453)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PaymentScheduleID, @MatterID  -- (card transaction) customer payment schedule ID
	
	SELECT @Amount=pps.PaymentGross
	FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) WHERE pps.CustomerPaymentScheduleID=@PaymentScheduleID  -- Amount
	EXEC _C00_SimpleValueIntoField 177225,@Amount,@MatterID
	
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4455)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PaymentTypeID, @MatterID  -- (card transaction) payment type ID (69931:CreditCard, 74337:DebitCard)

	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4506)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '2', @MatterID  -- (card transaction) object type ID : matter 

	DECLARE @MatterID_DV VARCHAR(2000)
	SELECT @MatterID_DV = CAST(@MatterID AS VARCHAR)
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4505)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @MatterID_DV, @MatterID  -- (card transaction) object ID

	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4484) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '2', @MatterID  -- (card transaction) transaction type ID : 2 for authorise

	DECLARE @AuthCode VARCHAR(2000)
	SELECT @AuthCode=DetailValue FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@MatterID AND DetailFieldID=170191 -- credit card token detail field

	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4524) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AuthCode, @MatterID  -- Customer Reference number
	
	DECLARE @LeadEventID_DV VARCHAR(2000)
	SELECT @LeadEventID_DV = CAST(@LeadEventID AS VARCHAR)
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4457) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @LeadEventID_DV, @MatterID  -- (card transaction) order ID : set to the lead event ID
    
    DECLARE @ExpiryMonth INT, @ExpiryYear INT, @ExpiryMonth_DV VARCHAR(2000), @ExpiryYear_DV VARCHAR(2000)
    SELECT @AccountID=AccountID FROM CustomerPaymentSchedule WITH (NOLOCK) WHERE CustomerPaymentScheduleID=@PaymentScheduleID
    SELECT @ExpiryMonth=ExpiryMonth, @ExpiryYear=ExpiryYear FROM Account WITH (NOLOCK) WHERE AccountID=@AccountID
    
    SELECT @ExpiryMonth_DV = CAST(@ExpiryMonth AS VARCHAR)
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4466) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @ExpiryMonth_DV, @MatterID  -- (card transaction) expiry month

	SELECT @ExpiryYear_DV = CAST(@ExpiryYear AS VARCHAR)
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4467) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @ExpiryYear_DV, @MatterID  -- (card transaction) expiry year
    
    DECLARE @CardName VARCHAR(2000)
	SELECT @CardName=DetailValue FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@MatterID AND DetailFieldID=170192 -- credit card holder name detail field
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4464) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @CardName, @MatterID  -- (card transaction) card name

	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4472) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- (card transaction) address
	
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4473) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- (card transaction) address 2	
	
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4474) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- (card transaction) city
	
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4475) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- (card transaction) state provice
	
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4477) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- (card transaction) zip postal

	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4476) 
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- (card transaction) country
	
	

	/*Now make sure we have marked the payment as pending*/ 
	IF ISNULL(@PaymentScheduleID,0) > 0 
	BEGIN 
	
		UPDATE PurchasedProductPaymentSchedule 
		Set PaymentStatusID = 2
		From PurchasedProductPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @PaymentScheduleID
		
		UPDATE CustomerPaymentSchedule  
		Set PaymentStatusID = 2
		From CustomerPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @PaymentScheduleID
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueCardPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_QueueCardPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueCardPayment] TO [sp_executeall]
GO
