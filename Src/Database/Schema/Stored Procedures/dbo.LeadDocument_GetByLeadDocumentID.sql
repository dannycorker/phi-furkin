SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDocument table through an index
-- JWG 2012-07-12 Use vLeadDocumentList to cater for archived documents.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_GetByLeadDocumentID]
(
	@LeadDocumentID INT   
)
AS
BEGIN

	SELECT
		[LeadDocumentID],
		[ClientID],
		[LeadID],
		[DocumentTypeID],
		[LeadDocumentTitle],
		[UploadDateTime],
		[WhoUploaded],
		COALESCE([DocumentBLOB], CAST(0x AS VARBINARY)),
		[FileName],
		[EmailBLOB],
		[DocumentFormat],
		[EmailFrom],
		[EmailTo],
		[CcList],
		[BccList],
		[ElectronicSignatureDocumentKey],
		[Encoding],
		[ContentFormat],
		[ZipFormat],
		[DocumentBlobSize], 
		[EmailBlobSize],
		[DocumentDatabaseID],
		[WhenArchived],
		[DocumentTypeVersionID]
	FROM
		dbo.vLeadDocumentList WITH (NOLOCK) 
	WHERE
		[LeadDocumentID] = @LeadDocumentID
	
	SELECT @@ROWCOUNT
					
END		
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadDocumentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_GetByLeadDocumentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadDocumentID] TO [sp_executeall]
GO
