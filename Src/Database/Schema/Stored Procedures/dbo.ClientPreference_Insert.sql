SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPreference table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreference_Insert]
(

	@ClientPreferenceID int    OUTPUT,

	@ClientID int   ,

	@ClientPreferenceTypeID int   ,

	@PreferenceValue varchar (255)  
)
AS


				
				INSERT INTO [dbo].[ClientPreference]
					(
					[ClientID]
					,[ClientPreferenceTypeID]
					,[PreferenceValue]
					)
				VALUES
					(
					@ClientID
					,@ClientPreferenceTypeID
					,@PreferenceValue
					)
				-- Get the identity value
				SET @ClientPreferenceID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreference_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreference_Insert] TO [sp_executeall]
GO
