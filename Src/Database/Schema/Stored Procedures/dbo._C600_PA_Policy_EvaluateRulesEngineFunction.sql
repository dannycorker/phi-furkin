SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:          Simon Brushett
-- Create date:		2018-04-10
-- Description:     Evaluates all the functions
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_EvaluateRulesEngineFunction]
(
	@CustomerID INT,
	@CaseID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
AS
BEGIN

	-- TODO: Need a nicer way of working out what needs to be evaluated
	DECLARE @Functions TABLE
	(
		ID INT IDENTITY,
		ParameterTypeID INT,
		FunctionText VARCHAR(2000),
		DataTypeID INT,
		Value VARCHAR(2000)
	)
	INSERT @Functions (ParameterTypeID, FunctionText, DataTypeID)
	SELECT ParameterTypeID, Value, DataTypeID
	FROM dbo.RulesEngine_RuleParameters_PreSet
	WHERE ParameterTypeID = 4
	AND Value != 'fn_C00_1273_RulesEngine_PetCount'

		
	-- Call all the functions!
	DECLARE @FunctionCount INT,
			@FunctionIndex INT = 0

	SELECT @FunctionCount = COUNT(*) 
	FROM @Functions

	WHILE @FunctionIndex < @FunctionCount 
	BEGIN
		SELECT @FunctionIndex += 1

		DECLARE @ParameterTypeID INT,
				@FunctionText VARCHAR(2000),
				@DataTypeID INT
		SELECT	@ParameterTypeID = ParameterTypeID,
				@FunctionText = FunctionText,
				@DataTypeID = DataTypeID
		FROM @Functions
		WHERE ID = @FunctionIndex

		DECLARE @Sql NVARCHAR(2000),
				@Params NVARCHAR(MAX),
				@Value VARCHAR(2000)

		SELECT	@Sql = 'SELECT @ValueOUT = dbo.' + @FunctionText + '(@CustomerID, 0, @CaseID, 0, @Overrides)',
				@Params = N'@ValueOUT VARCHAR(2000) OUTPUT, @CustomerID INT, @CaseID INT, @Overrides dbo.tvpIntVarcharVarchar READONLY'	


		IF @DataTypeID = 5 -- DateTime
		BEGIN

			SELECT @Sql = 'SELECT @ValueOUT = CONVERT(VARCHAR, ' + @FunctionText + ', 120)'

		END
		
		/*CPS 2018-01-31 pull the function value from overrides if provided*/
		IF EXISTS ( SELECT * FROM @Overrides ovr WHERE ovr.AnyValue1 = @FunctionText AND ovr.AnyValue2 <> '' )
		BEGIN
			PRINT 'Value "' + ISNULL(CONVERT(VARCHAR,@FunctionText),'NULL') + '" already exists for function ' + ISNULL(CONVERT(VARCHAR,@FunctionText),'NULL')

			SELECT @Value = ovr.AnyValue2
			FROM @Overrides ovr 
			WHERE ovr.AnyValue1 = @FunctionText 
			AND ovr.AnyValue2 <> ''
		END
		ELSE
		IF EXISTS ( SELECT *
					FROM sys.sysobjects so WITH ( NOLOCK ) 
					WHERE so.type = 'FN'
					AND so.name = @FunctionText )
		BEGIN
			PRINT 'Executing function ' + ISNULL(CONVERT(VARCHAR,@FunctionText),'NULL')
			EXEC sp_executesql @Sql, @Params, @ValueOUT = @Value OUTPUT, @CustomerID = @CustomerID, @CaseID = @CaseID, @Overrides = @Overrides
		END
		ELSE
		BEGIN
			PRINT 'No match for function ' + ISNULL(CONVERT(VARCHAR,@FunctionText),'NULL')
			SELECT @Value = ''
		END

		UPDATE @Functions
		SET Value = @Value
		WHERE ID = @FunctionIndex

		PRINT ' Function:  ' + @Sql + '  Value: ' + ISNULL(@Value,'NULL')

	END
		
	SELECT ParameterTypeID AS AnyID, FunctionText AS AnyValue1, Value AS AnyValue2
	FROM @Functions f	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_EvaluateRulesEngineFunction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_EvaluateRulesEngineFunction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_EvaluateRulesEngineFunction] TO [sp_executeall]
GO
