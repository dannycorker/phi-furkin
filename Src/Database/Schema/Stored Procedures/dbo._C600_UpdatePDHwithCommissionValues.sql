SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2018-08-30
-- Description:	Update PremiumDetailHistory row with Commission Values (called from CalculatePremiumForPeriod)
--	2018-09-05	GPR changed fieldID assigned to @AdjustmentDate
-- 2019-10-21 CPS for JIRA LPC-37	| Allowed for leap years by collecting the @DaysInYear from the PurchasedProduct table
-- 2019-10-21 CPS for JIRA LPC-37	| Include @LeadEventID to allow us to show our working
-- 2019-10-24 CPS for JIRA LPC-37	| Store raw premium split
-- 2020-01-21 CPS for JIRA LPC-395	| Removed additive commission calc and changed daily value calculation to avoid rounding errors
-- =============================================
CREATE PROCEDURE [dbo].[_C600_UpdatePDHwithCommissionValues]
(
	 @MatterID		INT
	,@TableRowID	INT
	,@PolEnd		DATE
	,@LeadEventID	INT = NULL
)
AS
BEGIN

	DECLARE  @WhoCreated					INT
			,@PremiumCalculationDetailID	INT
			,@AnnualCommissionValue			MONEY
			,@TranCommissionValue			MONEY
			,@AdjustmentDate				DATE
			,@DaysRemaining					INT
			,@DaysInYear					INT
			,@PurchasedProductID			INT
			,@EventComments					VARCHAR(2000) = '| Commission Calculation (' + OBJECT_NAME(@@ProcID) + ') |' + CHAR(13)+CHAR(10)
			,@CustomerID					INT /*GPR 2020-01-12*/
			,@AffinityID					INT /*GPR 2020-01-12*/
			,@CommissionPercentageValue		INT /*GPR 2020-01-12*/
			,@GrossDiscountedPremium		MONEY /*GPR 2020-01-12*/
			,@RawPremium					DECIMAL(18,2)
	
	SELECT @CustomerID = CustomerID 
	FROM Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	SELECT @AffinityID = ValueInt 
	FROM CustomerDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID = 170144 
	AND CustomerID = @CustomerID
	
	SELECT @CommissionPercentageValue = ValueInt 
	FROM ResourceListDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID = 313814 
	AND ResourceListID = @AffinityID
		
	SELECT TOP (1)	 @DaysInYear = DATEDIFF(DAY,pp.ValidFrom,pp.ValidTo) + 1
					,@PurchasedProductID = pp.PurchasedProductID
	FROM PurchasedProduct pp WITH (NOLOCK) 
	WHERE pp.ObjectID = @MatterID
	AND @PolEnd between pp.ValidFrom AND pp.ValidTo
	ORDER BY pp.PurchasedProductID DESC

	IF @@ROWCOUNT = 0 or @PolEnd is NULL
	BEGIN
		SELECT	 @PolEnd = DATEADD(DAY,-1,DATEADD(YEAR,1,mdv.ValueDate))
				,@DaysInYear = DATEDIFF(DAY,mdv.ValueDate,DATEADD(DAY,-1,DATEADD(YEAR,1,mdv.ValueDate)) /*Policy Start Date*/) + 1
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.MatterID = @MatterID
		AND mdv.DetailFieldID = 170036 /*Policy Start Date*/
	END

	SELECT @EventComments += ' ' + ISNULL(CONVERT(VARCHAR,@DaysInYear),'NULL') + ' days for PurchasedProductID ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductID),'NULL') + ', ending '+ ISNULL(CONVERT(VARCHAR,@PolEnd,103),'NULL') + + CHAR(13)+CHAR(10)
	
	SELECT @PremiumCalculationDetailID = tdv.ValueInt 
	FROM TableDetailValues tdv 
	WHERE tdv.TableRowID = @TableRowID 
	AND tdv.DetailFieldID = 175709 /*PremiumCalculationID*/
		
	SELECT @EventComments += ' ' + 'PremiumCalculationID '	+ ISNULL(CONVERT(VARCHAR,@PremiumCalculationDetailID),'NULL') + CHAR(13)+CHAR(10)
						  +  ' ' + 'TableRowID '			+ ISNULL(CONVERT(VARCHAR,@TableRowID)				 ,'NULL') + CHAR(13)+CHAR(10)

	--Get the commission value
	--SELECT TOP 1 @AnnualCommissionValue = pcdv.RuleInputOutputDelta FROM PremiumCalculationDetailValues pcdv WITH ( NOLOCK )
	--WHERE pcdv.RuleCheckPoint = 'Commission'
	--AND pcdv.PremiumCalculationDetailID = @PremiumCalculationDetailID
	--ORDER BY pcdv.RuleSequence
	
	/*GPR 2020-01-12 GrossDiscountedPremium*/
	SELECT @GrossDiscountedPremium = dbo.fnGetSimpleDvAsMoney(177899,@TableRowID) /*Annual Discounted Premium*/

	SELECT @AnnualCommissionValue /*Broker Commission Value*/ = (@GrossDiscountedPremium / 100) * (100 - @CommissionPercentageValue /*UW Commission Value*/)
		
	SELECT @EventComments += ' ' + ' Annual Commission £'	+ ISNULL(CONVERT(VARCHAR,@AnnualCommissionValue),'NULL') + CHAR(13)+CHAR(10)

	--Insert the commission value
	EXEC _C00_SimpleValueIntoField 180170, @AnnualCommissionValue, @TableRowID, @WhoCreated /*Premium Split Commission*/
		
	--Get the count of days between adjustyment date and policy end date		
	SELECT @AdjustmentDate = dbo.fnGetSimpleDvAsDate(175396,@TableRowID) /*Adjustment Date*/
		
	IF @AdjustmentDate IS NULL /*then it's probably an MTA or Cancellation*/
	BEGIN
		SELECT @AdjustmentDate = dbo.fnGetSimpleDvAsDate(175442,@MatterID) /*Adjustment Date*/
	END
	
	SELECT @DaysRemaining = DATEDIFF(DAY,@AdjustmentDate, @PolEnd)+1

	SELECT @EventComments += ' ' + ISNULL(CONVERT(VARCHAR,@DaysRemaining),'NULL') + ' days remaining between ' + ISNULL(CONVERT(VARCHAR,@AdjustmentDate,103),'NULL') + ' and ' + ISNULL(CONVERT(VARCHAR,@PolEnd,103),'NULL') + CHAR(13)+CHAR(10)
		
	--Calculate the commission value for the period
	--SELECT @TranCommissionValue	= (((@AnnualCommissionValue)/@DaysInYear)*@DaysRemaining)
	-- 2020-01-22 CPS for JIRA LPC-395 | logic changed to avoid rounding errors
	SELECT @TranCommissionValue	= (@AnnualCommissionValue)* ( CAST(@DaysRemaining as MONEY) / CAST(@DaysInYear as MONEY) )

	SELECT @EventComments += ' Commission value for this period: £' + ISNULL(CONVERT(VARCHAR,@TranCommissionValue),'NULL')
		
	EXEC _C00_SimpleValueIntoField 180171, @TranCommissionValue, @TableRowID, @WhoCreated /*Adjustment Commission*/

	-- 2019-10-24 CPS for JIRA LPC-37	| Store raw premium split
	-- 2020-01-21 CPS for JIRA LPC-395	| Removed Commission
	SELECT  @RawPremium = ISNULL(tdv.ValueMoney,0.00) - ( ISNULL(ipt.ValueMoney,0.00) + ISNULL(tdvAdmin.ValueMoney,0.00) )
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 177899 /*Annual Discounted Premium*/
	 LEFT JOIN dbo.TableDetailValues tdvAdmin WITH (NOLOCK) on tdvAdmin.TableRowID = tr.TableRowID AND tdvAdmin.DetailFieldID = 313819 /*Annual Split Admin Fee*/
	INNER JOIN dbo.TableDetailValues ipt WITH (NOLOCK) on ipt.TableRowID = tr.TableRowID AND ipt.DetailFieldID = 175375 /*Annual Split IPT*/
	WHERE tr.TableRowID = @TableRowID

	EXEC _C00_SimpleValueIntoField 180307, @RawPremium, @TableRowID, @WhoCreated /*Annual Split Raw Premium*/

	IF @LeadEventID is not NULL
	BEGIN
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_UpdatePDHwithCommissionValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_UpdatePDHwithCommissionValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_UpdatePDHwithCommissionValues] TO [sp_executeall]
GO
