SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuestionPossibleAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_Insert]
(

	@QuestionPossibleAnswerID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@MasterQuestionID int   ,

	@AnswerText varchar (2000)  ,

	@Branch int   ,

	@LinkedQuestionnaireQuestionPossibleAnswerID int   ,

	@ClientID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[QuestionPossibleAnswers]
					(
					[ClientQuestionnaireID]
					,[MasterQuestionID]
					,[AnswerText]
					,[Branch]
					,[LinkedQuestionnaireQuestionPossibleAnswerID]
					,[ClientID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@MasterQuestionID
					,@AnswerText
					,@Branch
					,@LinkedQuestionnaireQuestionPossibleAnswerID
					,@ClientID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @QuestionPossibleAnswerID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Insert] TO [sp_executeall]
GO
