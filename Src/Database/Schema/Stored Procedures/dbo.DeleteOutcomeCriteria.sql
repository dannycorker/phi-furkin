SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteOutcomeCriteria    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[DeleteOutcomeCriteria] @OutcomeCriteriaID int
AS
Delete From OutcomeCriterias
Where OutcomeCriteriaID = @OutcomeCriteriaID





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeCriteria] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteOutcomeCriteria] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeCriteria] TO [sp_executeall]
GO
