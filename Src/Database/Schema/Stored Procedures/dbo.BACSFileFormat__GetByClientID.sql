SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 02-08-2016
-- Description:	Gets a list of file formats supported by the given client
-- =============================================
CREATE PROCEDURE [dbo].[BACSFileFormat__GetByClientID]
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;
	
	SELECT * FROM BACSFileFormat WITH (NOLOCK) WHERE ClientID=@ClientID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BACSFileFormat__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__GetByClientID] TO [sp_executeall]
GO
