SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderObjectAction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectAction_Update]
(

	@DataLoaderObjectActionID int   ,

	@OriginalDataLoaderObjectActionID int   ,

	@DataLoaderObjectActionName varchar (250)  ,

	@DataLoaderObjectActionDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderObjectAction]
				SET
					[DataLoaderObjectActionID] = @DataLoaderObjectActionID
					,[DataLoaderObjectActionName] = @DataLoaderObjectActionName
					,[DataLoaderObjectActionDescription] = @DataLoaderObjectActionDescription
				WHERE
[DataLoaderObjectActionID] = @OriginalDataLoaderObjectActionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectAction_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Update] TO [sp_executeall]
GO
