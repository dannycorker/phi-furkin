SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Gets a notification by id
-- =============================================
CREATE PROCEDURE [dbo].[Notification__GetByNotificationID]

	@NotificationID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	
			n.[NotificationID],
			n.[ClientID],
			n.[SubClientID],
			n.[ThirdPartySystemID],
			n.[Message],
			n.[DateMessageCreated],
			n.[MessageCreatedBy],
			n.[NotificationTypeID]
	FROM [NOTIFICATION] n WITH (NOLOCK) 
	WHERE n.NotificationID = @NotificationID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__GetByNotificationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Notification__GetByNotificationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__GetByNotificationID] TO [sp_executeall]
GO
