SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-29
-- Description:	Get Group/User Editing in progress mode
-- JWG 2009-01-08 Allow multiple Users to be edited simultaneously,
--     as long as the group is not being edited. Do not allow a 
--     Group to be edited if any Users (or the Group itself) are
--     being edited already though.
-- =============================================
CREATE PROCEDURE [dbo].[GroupEditingGetInProgress]
	@GroupIDToEdit int, 
	@UserIDToEdit int = NULL 
AS
BEGIN

	SET NOCOUNT ON;

	-- Scenarios that the app must cater for:

	-- 1) USERS:

	-- 1a)  This user belongs to your client and you want to edit a user:
	-- 1a1) The group is being edited by you - get lost
	-- 1a2) The group is being edited by someone else - get lost
	-- 1a3) The user is being edited by you - offer the option to resume or cancel the edit.
	-- 1a4) The user is being edited by someone else - get lost
	-- 1a5) The user is not being edited by anyone but at least one other user from the group is being edited - proceed
	-- 1a6) The user is not being edited by anyone - proceed
	--
	-- 1b)  This user belongs to a different client (eg client zero) and you want to edit the user - FAIL - NOT ALLOWED


	-- 2) GROUPS:

	-- 2a)  This group belongs to your client and you want to edit the group:
	-- 2a1) The group is being edited by you - offer the option to resume or cancel the edit.
	-- 2a2) The group is being edited by someone else - get lost
	-- 2a3) The group is not being edited by anyone but at least one user from the group is being edited - get lost
	-- 2a4) The group is not being edited by anyone and nor are any of the users - proceed
	--
	-- 2b)  This group belongs to a different client (eg client zero) and you want to edit the group - FAIL - NOT ALLOWED


	IF @UserIDToEdit > 0 
	BEGIN

		-- Get the first record where the group is being edited OR this user is being edited.
		-- Only one of these conditions is possible, so there can only be zero or 1 record in
		-- the result set.  Adding "SELECT TOP 1 ..." would guarantee this, but that would
		-- also mask any errors if this logic is wrong.
		SELECT e.ClientPersonnelAdminGroupID, 
		e.ClientPersonnelID, 
		e.WhoIsEditing, 
		convert(varchar(19), e.EditStartedAt, 121) as [EditStartedAt], 
		cp.UserName as [NameOfEditor], 
		cp.ClientID
		FROM GroupAndUserEditingControl e 
		INNER JOIN ClientPersonnel cp ON e.WhoIsEditing = cp.ClientPersonnelID 
		WHERE (e.ClientPersonnelAdminGroupID = @GroupIDToEdit)
		AND (
			(e.ClientPersonnelID IS NULL)         -- The Group is being edited
			OR								      -- OR
			(e.ClientPersonnelID = @UserIDToEdit) -- The exact user that we want to edit is being edited
		)
	END
	ELSE
	BEGIN
		/*
			The group can only be edited if it is unused and so are all of its users.
			That means that there must not be any records whatsoever in GroupAndUserEditingControl
			for this ClientPersonnelAdminGroupID.

			This is a bit trickier than the User section above as there could be multiple User edits
			going on, and any one of them prevents a Group edit, but which do we report on?  We only
			want to show ClientPersonnel names from this ClientID, and something like "other admin user(s)"
			for other clients.  Let the app handle all of this.
		*/
		SELECT e.ClientPersonnelAdminGroupID, 
		e.ClientPersonnelID, 
		e.WhoIsEditing, 
		convert(varchar(19), e.EditStartedAt, 121) as [EditStartedAt], 
		cp.UserName as [NameOfEditor], 
		cp.ClientID
		FROM GroupAndUserEditingControl e 
		INNER JOIN ClientPersonnel cp ON e.WhoIsEditing = cp.ClientPersonnelID 
		WHERE e.ClientPersonnelAdminGroupID = @GroupIDToEdit 
		ORDER BY cp.ClientID, cp.UserName 
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[GroupEditingGetInProgress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupEditingGetInProgress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupEditingGetInProgress] TO [sp_executeall]
GO
