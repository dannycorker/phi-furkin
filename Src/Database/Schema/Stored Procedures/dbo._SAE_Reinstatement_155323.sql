SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Apply reinstatement
-- Mods
-- 2020-08-20 ALM | Updated for US Cancellation Logic for PPET-110
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Reinstatement_155323]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PurchasedProductID			INT
			,@PurchasedProductPaymentScheduleID	INT
			,@CancellationDate				DATE
			,@VarcharDate					VARCHAR(10)
			,@TableRowID					INT
			,@ValueDate						DATE
			,@AccountID						INT
			,@StartDate						DATE
			,@EndDate						DATE
			,@VarcharDateTime				VARCHAR(19)
			,@ValueInt						INT
			,@Adjustment					DECIMAL(18,2)
			,@PremiumGross					DECIMAL (18,2)
			,@NewWritten					DECIMAL (18,2)
			,@ColMatterID					INT
			,@OneVisionID					INT
			,@State							VARCHAR(10)
			,@CancellationReason			INT
			,@CancellationType				INT
			,@DaysIntoPolicyYear			INT
			,@IsRenewal						BIT
			,@CoolingOffPeriod				INT
			,@CountryID						INT
			,@WellnessOneVisionID			INT
			,@ExamFeeOneVisionID			INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			,@CountryID		= cu.CountryID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID 
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


/* Apply Re-instatement */
	IF @EventTypeID IN (155323)
	BEGIN
		
		-- Update the policy end dates
		SELECT @VarcharDate=dbo.fnGetDv (170055, @CaseID) -- cancellation date
		SELECT @TableRowID=dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow (@CaseID, @VarcharDate)
		SELECT @VarcharDate=CONVERT(VARCHAR(10),DATEADD(DAY,-1,DATEADD(YEAR,1,dbo.fnGetDvAsDate(170036,@CaseID))),120) -- new end date /*CPS 2017-10-30 subtract a day*/
		EXEC _C00_SimpleValueIntoField 170037,@VarcharDate,@MatterID,@AqAutomation
		EXEC dbo._C00_SimpleValueIntoField 145664, @VarcharDate, @TableRowID, @AqAutomation		
		EXEC dbo._C00_SimpleValueIntoField 145666, '43002', @TableRowID, @AqAutomation	-- update to live from cancelled	
		EXEC dbo._C00_SimpleValueIntoField 170038, '43002', @MatterID, @AqAutomation	-- update to live from cancelled	
		
		

		/*GPR 2020-09-18 for PPET-405*/
		IF @EventTypeID IN (150145)/*Policy Cancelled*/
		BEGIN
		
			SELECT @TableRowID = (
					SELECT TOP(1) tr.TableRowID 
					FROM TableRows tr WITH (NOLOCK) 
					WHERE tr.MatterID = @MatterID 
					AND tr.DetailFieldID = 170033 
					ORDER BY tr.TableRowID DESC
			) /*Policy History*/

			EXEC [dbo].[_C600_UpdateWellnessHistory_AdjustEndDate] @TableRowID
		END


		SELECT @VarcharDate=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
		
		-- clear any notes 
		EXEC _C600_DeleteANote @CaseID, 907 -- Policy cancelled
		EXEC _C600_DeleteANote @CaseID, 908 -- No refund due

		-- clear cancellation date
		EXEC _C00_SimpleValueIntoField 170055,'',@MatterID,@AqAutomation
 						
 		-- apply the reinstaement in the billing system
		EXEC _C00_SimpleValueIntoField 177377,0,@MatterID,@WhoCreated
		
		-- get the reinstatment table row
		SELECT TOP 1 @TableRowID=tr.TableRowID FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues adt WITH (NOLOCK) ON tr.TableRowID=adt.TableRowID AND adt.DetailFieldID=175398 
		WHERE tr.DetailFieldID=175336
			AND adt.ValueInt=74531
			AND adt.MatterID=@MatterID
		ORDER BY tr.TableRowID DESC
		
		SELECT @ValueDate=dbo.fnGetSimpleDvAsDate(175442,@MatterID)--, @ValueMoney=dbo.fnGetDv(177404,@CaseID)
					
		/*#43064 2017-06-30 ACE Set ppid or this fails!*/
		SELECT TOP 1 @PurchasedProductID = pp.PurchasedProductID, @AccountID = pp.AccountID
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.ObjectID = @MatterID
		ORDER BY pp.PurchasedProductID DESC

		EXEC dbo._C00_SimpleValueIntoField 177031, @PurchasedProductID, @MatterID, @WhoCreated	-- Purchased Product ID

		/*#55222 JEL 2019-02-13 Cancellation will have modified the Valid To date of the policy, set this back to be correct before trying to work out the adjustment*/ 
		Update PurchasedProduct 
		SET ValidTo = DATEADD(YEAR,1,DATEADD(DAY,-1,ValidFrom))
		Where PurchasedProductID = @PurchasedProductID
	
		

		/*2020-09-23 GPR for AM - Reinstate Wellness Row*/
		SELECT @TableRowID = (
					SELECT TOP(1) tr.TableRowID 
					FROM TableRows tr WITH (NOLOCK) 
					WHERE tr.MatterID = @MatterID 
					AND tr.DetailFieldID = 170033 
					ORDER BY tr.TableRowID DESC
			) /*Policy History*/

			EXEC [dbo].[_C600_UpdateWellnessHistory_AdjustEndDate] @TableRowID
		EXEC [dbo].[_C600_UpdateWellnessHistory_AdjustEndDate] @TableRowID

		/*Work out the new written premium and update table*/ 
		
		/*2020-08-20 ALM PPET-110*/
		IF @CountryID = 233 /*UnitedStates*/
		BEGIN 

			SELECT @State = cu.County 
			FROM Customers cu WITH (NOLOCK) 
			WHERE cu.CustomerID = @CustomerID 

			SELECT @CancellationReason = dbo.fnGetSimpleDvAsInt(170054,@MatterID),
					@StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)

			SELECT @CancellationType = 
			CASE 
				WHEN @CancellationReason IN (69907,69911,72031,72032,72033,72035,72037,72038,74421,74519,74521,74523,74525,75135,75136,75141) THEN 1 
				ELSE 2 
			END

			SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())

			SELECT @IsRenewal = 
			CASE 
				WHEN COUNT (*) > 1 THEN 1 
				ELSE 0 
			END 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID

			SELECT @CoolingOffPeriod = [dbo].[fn_C00_USCancellationCoolingOffPeriod] (@State, @CancellationType, @IsRenewal, @DaysIntoPolicyYear)
		
			/*If Adjustment date is in cooling off then work out new WP from inception*/
			SELECT @ValueDate = CASE WHEN @CoolingOffPeriod IS NOT NULL THEN
								CASE WHEN DATEADD(DAY,@CoolingOffPeriod,@StartDate) >= CAST(dbo.fn_GetDate_Local() AS DATE) THEN @StartDate ELSE @ValueDate END
								ELSE p.ValidTo END
			FROM PurchasedProduct p WITH ( NOLOCK )
			WHERE p.PurchasedProductID = @PurchasedProductID

		END
		ELSE 
		BEGIN 

			SELECT @StartDate = p.ValidFrom, @EndDate = p.ValidTo
			FROM PurchasedProduct p WITH ( NOLOCK )
			INNER JOIN BillingConfiguration bc WITH ( NOLOCK ) on bc.ClientID = p.ClientID 
			WHERE p.PurchasedProductID = @PurchasedProductID 		

			/*If Adjustment date is in cooling off then work out new WP from inception*/
			SELECT @ValueDate = CASE WHEN bc.CoolingOffPeriod IS NOT NULL THEN
								CASE WHEN DATEADD(DAY,bc.CoolingOffPeriod,@StartDate) >= CAST(dbo.fn_GetDate_Local() AS DATE) THEN @StartDate ELSE @ValueDate END
								ELSE p.ValidTo END
			FROM PurchasedProduct p WITH ( NOLOCK )
			INNER JOIN BillingConfiguration bc WITH ( NOLOCK ) on bc.ClientID = p.ClientID 
			WHERE p.PurchasedProductID = @PurchasedProductID 
					
		END
		
		EXEC dbo._C00_SimpleValueIntoField 175396, @ValueDate, @TableRowID, @WhoCreated -- adjustment date	
		--EXEC dbo._C00_SimpleValueIntoField 175397, @ValueMoney, @TableRowID, @WhoCreated -- adjustment amount
		SELECT @VarcharDateTime=CONVERT(VARCHAR(19),dbo.fn_GetDate_Local(),121)
		EXEC dbo._C00_SimpleValueIntoField 175400, @VarcharDateTime, @TableRowID, @WhoCreated	-- adjustment application date			  
		
		/*Now get the cancellation table row so we can see the last WP value*/
		SELECT TOP 1 @ValueInt=tr.TableRowID, @Adjustment = am.ValueMoney FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues adt WITH (NOLOCK) ON tr.TableRowID=adt.TableRowID AND adt.DetailFieldID= 175398 
		INNER JOIN TabledetailValues am WITH ( NOLOCK ) on am.tableRowID = tr.TableRowID and am.detailfieldID = 175397
		WHERE tr.DetailFieldID=175336
		AND adt.ValueInt=74529
		AND adt.MatterID=@MatterID
		ORDER BY tr.TableRowID DESC
		 
		SELECT @PremiumGross = dbo.fnGetSimpleDvAsMoney(177899,@ValueInt) /*Annual Amount*/ 
		SELECT @Adjustment = @Adjustment * -1
		SELECT @NewWritten = ISNULL(@PremiumGross,0) + ISNULL(@Adjustment,0) 
		
		EXEC _C00_SimpleValueIntoField 177899,@NewWritten,@TableRowID,@WhoCreated 
		EXEC _C00_SimpleValueIntoField 175397,@Adjustment,@TableRowID, @WhoCreated 
		
		SELECT @ColMatterID=ltr.ToMatterID 
		FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
		
		IF EXISTS (SELECT * FROM Account a WITH ( NOLOCK ) where a.AccountID = @AccountID and a.AccountTypeID = 1)
		BEGIN
		
			EXEC _C00_SimpleValueIntoField 176895,5145,@ColMatterID,58552
			
			INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
			SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156740, @AqAutomation, -1, 5, 0, 5
			FROM Cases c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
			INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
			WHERE m.MatterID=@ColMatterID
			
		END

		/*2020-07-28 ALM AAG-970*/
		SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

		IF @OneVisionID IS NOT NULL 
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Policy', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
		END

		SELECT @WellnessOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness'

		IF @WellnessOneVisionID IS NOT NULL 
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID
		END

		SELECT @ExamFeeOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'

		IF @ExamFeeOneVisionID IS NOT NULL 
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID
		END

	END	



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Reinstatement_155323] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Reinstatement_155323] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Reinstatement_155323] TO [sp_executeall]
GO
