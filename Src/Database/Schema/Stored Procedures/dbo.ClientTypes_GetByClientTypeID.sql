SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientTypes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientTypes_GetByClientTypeID]
(

	@ClientTypeID int   
)
AS


				SELECT
					[ClientTypeID],
					[ClientTypeName]
				FROM
					[dbo].[ClientTypes] WITH (NOLOCK) 
				WHERE
										[ClientTypeID] = @ClientTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_GetByClientTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientTypes_GetByClientTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_GetByClientTypeID] TO [sp_executeall]
GO
