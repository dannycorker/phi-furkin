SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the InvoiceMappingEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_GetByLeadTypeIDEventTypeIDType]
(

	@LeadTypeID int   ,

	@EventTypeID int   ,

	@Type char (1)  
)
AS


				SELECT
					[InvoiceMappingEventID],
					[ClientID],
					[LeadTypeID],
					[EventTypeID],
					[Type]
				FROM
					[dbo].[InvoiceMappingEvent] WITH (NOLOCK) 
				WHERE
										[LeadTypeID] = @LeadTypeID
					AND [EventTypeID] = @EventTypeID
					AND [Type] = @Type
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_GetByLeadTypeIDEventTypeIDType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_GetByLeadTypeIDEventTypeIDType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_GetByLeadTypeIDEventTypeIDType] TO [sp_executeall]
GO
