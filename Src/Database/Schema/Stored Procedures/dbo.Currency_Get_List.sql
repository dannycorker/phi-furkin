SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Currency table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Currency_Get_List]

AS


				
				SELECT
					[CurrencyID],
					[CurrencyCode],
					[CurrencyName],
					[WhenCreated],
					[WhenModified],
					[MajorNamePlural],
					[MajorNameSingular],
					[MinorNamePlural],
					[MinorNameSingular],
					[CurrencySymbol]
				FROM
					[dbo].[Currency] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Currency_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Get_List] TO [sp_executeall]
GO
