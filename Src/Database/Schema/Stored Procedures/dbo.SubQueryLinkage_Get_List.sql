SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SubQueryLinkage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_Get_List]

AS


				
				SELECT
					[SubQueryLinkageID],
					[ClientID],
					[SubQueryID],
					[LinksToTableName],
					[LinksToColumnName],
					[LinkageDataType],
					[ParamTableName],
					[ParamColumnName],
					[ParamColumnDataType],
					[ParamColumnHelperSql],
					[LeadTypeFilterSql]
				FROM
					[dbo].[SubQueryLinkage] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Get_List] TO [sp_executeall]
GO
