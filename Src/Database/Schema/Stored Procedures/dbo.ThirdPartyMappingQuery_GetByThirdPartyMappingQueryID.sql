SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingQuery table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_GetByThirdPartyMappingQueryID]
(

	@ThirdPartyMappingQueryID int   
)
AS


				SELECT
					[ThirdPartyMappingQueryID],
					[ClientID],
					[ThirdPartySystemID],
					[ThirdPartyFieldGroupID],
					[QueryID],
					[Method],
					[ActionURI],
					[SchemaName],
					[BaseIndex]
				FROM
					[dbo].[ThirdPartyMappingQuery] WITH (NOLOCK) 
				WHERE
										[ThirdPartyMappingQueryID] = @ThirdPartyMappingQueryID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_GetByThirdPartyMappingQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_GetByThirdPartyMappingQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_GetByThirdPartyMappingQueryID] TO [sp_executeall]
GO
