SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_GetByManagerID]
(

	@ManagerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[WorkflowGroupID],
					[Name],
					[Description],
					[ManagerID],
					[ClientID],
					[Enabled],
					[SortOrder],
					[AssignedLeadsOnly],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[WorkflowGroup] WITH (NOLOCK) 
				WHERE
					[ManagerID] = @ManagerID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_GetByManagerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_GetByManagerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_GetByManagerID] TO [sp_executeall]
GO
