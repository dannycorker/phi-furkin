SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-02
-- Description:	Add Asset History
-- =============================================

CREATE PROCEDURE [dbo].[AssetHistory_Add]
(
	@ClientPersonnelID int,
	@AssetID int,
	@ClientID int,
	@AssetTypeID int,
	@AssetSubTypeID int,
	@LocationID int,
	@AssetName varchar (255),
	@Note varchar (255),
	@SecureNote varchar (255),
	@SecureUserName varchar (255),
	@SecurePassword varchar (255),
	@SecureOther varchar (255),
	@Version varchar (255),
	@ValidFrom datetime,
	@ValidTo datetime,
	@ReminderDue datetime,
	@ReminderTimeUnitID int,
	@ReminderTimeUnitQuantity int,
	@ReminderNotificationGroupID int,
	@Enabled bit,
	@Deleted bit,
	@WhoCreated int,
	@WhenCreated datetime,
	@WhoModified int,
	@WhenModified datetime
)
AS
BEGIN			
	SET NOCOUNT ON;
	INSERT INTO [dbo].[AssetHistory]
		(
		[AssetID]
		,[ClientID]
		,[AssetTypeID]
		,[AssetSubTypeID]
		,[LocationID]
		,[AssetName]
		,[Note]
		,[SecureNote]
		,[SecureUserName]
		,[SecurePassword]
		,[SecureOther]
		,[Version]
		,[ValidFrom]
		,[ValidTo]
		,[ReminderDue]
		,[ReminderTimeUnitID]
		,[ReminderTimeUnitQuantity]
		,[ReminderNotificationGroupID]
		,[Enabled]
		,[Deleted]
		,[WhoCreated]
		,[WhenCreated]
		,[WhoModified]
		,[WhenModified]
	)
	VALUES
		(
		@AssetID
		,@ClientID
		,@AssetTypeID
		,@AssetSubTypeID
		,@LocationID
		,@AssetName
		,@Note
		,@SecureNote
		,@SecureUserName
		,@SecurePassword
		,@SecureOther
		,@Version
		,@ValidFrom
		,@ValidTo
		,@ReminderDue
		,@ReminderTimeUnitID
		,@ReminderTimeUnitQuantity
		,@ReminderNotificationGroupID
		,@Enabled
		,@Deleted
		,@ClientPersonnelID
		,dbo.fn_GetDate_Local()
		,@ClientPersonnelID
		,dbo.fn_GetDate_Local()
	)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Add] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory_Add] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Add] TO [sp_executeall]
GO
