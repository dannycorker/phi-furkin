SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_Insert]
(

	@SqlQueryCriteriaSetMemberID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryCriteriaSetID int   ,

	@SqlQueryColumnID int   ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  
)
AS


				
				INSERT INTO [dbo].[SqlQueryCriteriaSetMember]
					(
					[ClientID]
					,[SqlQueryID]
					,[SqlQueryCriteriaSetID]
					,[SqlQueryColumnID]
					,[Criteria1]
					,[Criteria2]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@SqlQueryCriteriaSetID
					,@SqlQueryColumnID
					,@Criteria1
					,@Criteria2
					)
				-- Get the identity value
				SET @SqlQueryCriteriaSetMemberID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Insert] TO [sp_executeall]
GO
