SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskResult table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_GetByRunDateClientIDTaskID]
(

	@RunDate datetime   ,

	@ClientID int   ,

	@TaskID int   
)
AS


				SELECT
					[AutomatedTaskResultID],
					[TaskID],
					[ClientID],
					[RunDate],
					[Description],
					[RecordCount],
					[Complete]
				FROM
					[dbo].[AutomatedTaskResult] WITH (NOLOCK) 
				WHERE
										[RunDate] = @RunDate
					AND [ClientID] = @ClientID
					AND [TaskID] = @TaskID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_GetByRunDateClientIDTaskID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_GetByRunDateClientIDTaskID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_GetByRunDateClientIDTaskID] TO [sp_executeall]
GO
