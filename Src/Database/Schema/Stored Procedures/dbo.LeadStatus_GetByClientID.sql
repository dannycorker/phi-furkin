SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[StatusID],
					[ClientID],
					[StatusName],
					[StatusDescription],
					[SourceID],
					[LeadTypeID],
					[IsShared]
				FROM
					dbo.fnLeadStatusShared(@ClientID)
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_GetByClientID] TO [sp_executeall]
GO
