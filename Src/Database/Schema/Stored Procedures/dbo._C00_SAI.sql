SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-04-08
-- Description:	SQL After Incoming Post Event For C00
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SAI]
	@ClientID int = null,	
	@CustomerID int = NULL,
	@LeadID int = NULL,
	@CaseID int = NULL, 
	@MatterID int = NULL,	
	@IncomingPostEventID int
AS
BEGIN
	SET NOCOUNT ON;

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAI] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SAI] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAI] TO [sp_executeall]
GO
