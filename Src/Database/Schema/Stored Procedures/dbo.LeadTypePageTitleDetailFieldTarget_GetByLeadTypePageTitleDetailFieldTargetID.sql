SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitleDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleDetailFieldTarget_GetByLeadTypePageTitleDetailFieldTargetID]
(

	@LeadTypePageTitleDetailFieldTargetID int   
)
AS


				SELECT
					[LeadTypePageTitleDetailFieldTargetID],
					[ClientID],
					[LeadTypeID],
					[Target],
					[DetailFieldID],
					[TemplateTypeID],
					[DetailFieldAlias],
					[Notes]
				FROM
					[dbo].[LeadTypePageTitleDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[LeadTypePageTitleDetailFieldTargetID] = @LeadTypePageTitleDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_GetByLeadTypePageTitleDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleDetailFieldTarget_GetByLeadTypePageTitleDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_GetByLeadTypePageTitleDetailFieldTargetID] TO [sp_executeall]
GO
