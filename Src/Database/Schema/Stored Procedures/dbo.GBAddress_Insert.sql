SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the GBAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GBAddress_Insert]
(

	@GBAddressID int    OUTPUT,

	@ClientID int   ,

	@WebServiceURL varchar (500)  
)
AS


				
				INSERT INTO [dbo].[GBAddress]
					(
					[ClientID]
					,[WebServiceURL]
					)
				VALUES
					(
					@ClientID
					,@WebServiceURL
					)
				-- Get the identity value
				SET @GBAddressID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GBAddress_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Insert] TO [sp_executeall]
GO
