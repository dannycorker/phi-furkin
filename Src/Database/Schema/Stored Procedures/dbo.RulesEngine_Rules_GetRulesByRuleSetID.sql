SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:        Simon Brushett
-- Create date: 2013-05-30
-- Description:   Returns all rule sets for a specific rule set

-- Modified By:   Austin Davies
-- Modified:      2014-06-03
-- Description:   Added RuleOrder column to be returned
-- 2016-06-07 IS Pass RuleSetID, RuleCheckpoint, RuleValidFrom, RuleValidTo to the UI
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Rules_GetRulesByRuleSetID]
(
      @ClientID INT,
      @RuleSetID INT,
      @RuleID INT = NULL
)


AS
BEGIN
      
      SET NOCOUNT ON;

      SELECT      r.RuleID, r.RuleSetID, r.Name, r.WhenCreated, cpCreate.UserName AS WhoCreated, r.WhenModified, cpMod.UserName AS WhoModified, r.Description,
      r.RuleOrder, r.RuleCheckpoint, CONVERT(CHAR(10),r.RuleValidFrom,120) RuleValidFrom, CONVERT(CHAR(10),r.RuleValidTo,120) RuleValidTo
      FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
      INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON r.WhoCreated = cpCreate.ClientPersonnelID
      INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON r.WhoModified = cpMod.ClientPersonnelID
      WHERE r.ClientID = @ClientID
      AND r.RuleSetID = @RuleSetID
      AND (@RuleID IS NULL OR r.RuleID = @RuleID)
      ORDER BY r.RuleOrder
      
      
      SELECT 'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord
      UNION ALL
      SELECT r.Name AS Title, '#/ruleset/' + CAST(r.RuleSetID AS VARCHAR) + '/rules' AS Path, 2 AS Ord
      FROM dbo.RulesEngine_RuleSets r WITH (NOLOCK) 
      WHERE r.RuleSetID = @RuleSetID
      ORDER BY Ord
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_GetRulesByRuleSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Rules_GetRulesByRuleSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_GetRulesByRuleSetID] TO [sp_executeall]
GO
