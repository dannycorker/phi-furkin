SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeStandardField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_Delete]
(

	@EventTypeStandardFieldID int   
)
AS


				DELETE FROM [dbo].[EventTypeStandardField] WITH (ROWLOCK) 
				WHERE
					[EventTypeStandardFieldID] = @EventTypeStandardFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Delete] TO [sp_executeall]
GO
