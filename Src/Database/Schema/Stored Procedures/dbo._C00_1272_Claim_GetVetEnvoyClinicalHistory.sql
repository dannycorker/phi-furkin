SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-12-04
-- Description:	Gets the VE clinical history for a claim
-- Used by:		DF 162697
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetVetEnvoyClinicalHistory] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT tdvDate.ValueDate AS Date, tdvTime.DetailValue AS Time, tdvBy.DetailValue AS EnteredBy, tdvEntry.DetailValue AS TextEntry
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvDate WITH (NOLOCK) ON r.TableRowID = tdvDate.TableRowID AND tdvDate.DetailFieldID = 162666
	INNER JOIN dbo.TableDetailValues tdvTime WITH (NOLOCK) ON r.TableRowID = tdvTime.TableRowID AND tdvTime.DetailFieldID = 162667
	INNER JOIN dbo.TableDetailValues tdvBy WITH (NOLOCK) ON r.TableRowID = tdvBy.TableRowID AND tdvBy.DetailFieldID = 162668
	INNER JOIN dbo.TableDetailValues tdvEntry WITH (NOLOCK) ON r.TableRowID = tdvEntry.TableRowID AND tdvEntry.DetailFieldID = 162669
	WHERE r.MatterID = @MatterID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetVetEnvoyClinicalHistory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetVetEnvoyClinicalHistory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetVetEnvoyClinicalHistory] TO [sp_executeall]
GO
