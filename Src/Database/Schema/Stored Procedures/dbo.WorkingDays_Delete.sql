SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the WorkingDays table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_Delete]
(

	@WorkingDayID int   
)
AS


				DELETE FROM [dbo].[WorkingDays] WITH (ROWLOCK) 
				WHERE
					[WorkingDayID] = @WorkingDayID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Delete] TO [sp_executeall]
GO
