SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PageGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroup_Update]
(

	@PageGroupID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@PageGroupName varchar (100)  ,

	@PageGroupDescription varchar (255)  ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PageGroup]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[PageGroupName] = @PageGroupName
					,[PageGroupDescription] = @PageGroupDescription
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[PageGroupID] = @PageGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Update] TO [sp_executeall]
GO
