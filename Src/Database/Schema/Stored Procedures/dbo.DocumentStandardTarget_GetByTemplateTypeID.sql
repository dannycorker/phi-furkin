SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentStandardTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_GetByTemplateTypeID]
(

	@TemplateTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentStandardTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes],
					[IsSpecial],
					[ExcelSheetLocation],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentStandardTarget] WITH (NOLOCK) 
				WHERE
					[TemplateTypeID] = @TemplateTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByTemplateTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
