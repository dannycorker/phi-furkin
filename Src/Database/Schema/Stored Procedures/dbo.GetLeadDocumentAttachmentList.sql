SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-04
-- Description:	Build attachment list for this Case
-- Edits:		CT, 1:14, 08/10/2007 - changed so it selects items with either a documentformat or 
--				a filename, rather than ones without either one or the other.
-- JWG 2012-07-12 Use LeadDocument again now that it knows about DocumentBlobSize.
-- IS 2014-05-14 Emit blob size to show on the attachment listing
-- =============================================
CREATE PROCEDURE [dbo].[GetLeadDocumentAttachmentList]
	@LeadID int,
	@CaseID int,
	@ClientPersonnelID INT = NULL
AS
BEGIN
	SET NOCOUNT ON
	
	/*
		OLD comment:
		At first glance this looks insane, but the purpose is to return details 
		of documents that are not blank or null. Asking the database this question
		in a WHERE clause makes it table scan LeadDocument for some reason, whereas
		selecting CASE WHEN null THEN 0 ELSE 1 END as [IsBlobNull] works instantly.
	*/
	DECLARE @le TABLE (
		LeadEventID int, 
		DocumentTypeID int, 
		DocumentFormat varchar(24), 
		FileName varchar(255), 
		LeadDocumentTitle varchar(1000), 
		IsNewBlobNull bit, 
		IsNewNameBlank bit, 
		IsNewFormatBlank bit,
		DocumentBlobSizeMB DECIMAL(18,4)
	)
	
	-- IS 2014-05-14: because rtfs are compressed we need to inflate the value
	-- the average is 8
	DECLARE @RTFBlobSizeMultiplier INT = 8

	/* Populate table variable with candidate document details */
	INSERT @le (
		LeadEventID, 
		DocumentTypeID, 
		DocumentFormat, 
		FileName, 
		LeadDocumentTitle,
		IsNewBlobNull, 
		IsNewNameBlank, 
		IsNewFormatBlank,
		DocumentBlobSizeMB
	) 
	SELECT le.LeadEventID, 
	ld.DocumentTypeID, 
	ld.DocumentFormat, 
	ld.FileName, 
	ld.LeadDocumentTitle, 
	CASE WHEN ld.DocumentBLOBSize > 0 THEN 0 ELSE 1 END as IsNewBlobNull,
	CASE WHEN ld.[FileName] > ' ' THEN 0 ELSE 1 END as IsNewNameBlank,
	CASE WHEN ld.DocumentFormat > ' ' THEN 0 ELSE 1 END as IsNewFormatBlank,
	CASE 
		WHEN ld.DocumentFormat = 'RTF' THEN ISNULL(ld.DocumentBlobSize, 1024) * @RTFBlobSizeMultiplier 
		ELSE ISNULL(ld.DocumentBlobSize, 1024) 
	END / 1024000.0  DocumentBlobSizeMB
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.LeadDocument ld WITH (NOLOCK) ON ld.LeadDocumentID = le.LeadDocumentID
	WHERE le.LeadID = @LeadID 
	AND le.EventDeleted = 0 

	/* Remove any that have no blob details */
	DELETE @le 
	WHERE IsNewBlobNull = 1 

	/* Add notes */
	INSERT @le (
		LeadEventID, 
		DocumentTypeID, 
		DocumentFormat, 
		FileName, 
		LeadDocumentTitle,
		IsNewBlobNull, 
		IsNewNameBlank, 
		IsNewFormatBlank
	) 
	SELECT le.LeadEventID, 
	nt.NoteTypeID, 
	'NOTE', 
	nt.NoteTypeName, 
	nt.NoteTypeDescription, 
	0 as IsNewBlobNull,
	0 as IsNewNameBlank,
	0 as IsNewFormatBlank
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.NoteType nt WITH (NOLOCK) ON nt.NoteTypeID = le.NoteTypeID
	WHERE le.LeadID = @LeadID 
	AND le.EventDeleted = 0 

	/* Select remaining details back to the app (CustomersDocumentOut)*/
	SELECT le.LeadEventID,
	le.LeadID, 
	le.LeadDocumentID, 
	local_le.DocumentFormat, 
	dt.OutputFormat, 
	local_le.LeadDocumentTitle, 
	local_le.[FileName], 
	le.CaseID, 
	ca.CaseRef, 
	le.WhenCreated, 
	le.Comments, 
	et.EventTypeName, 
	0 as IsBlobNull,
	local_le.DocumentBlobSizeMB
	FROM @le local_le
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.LeadEventID = local_le.LeadEventID 
	INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.CaseID = le.CaseID
	LEFT JOIN dbo.DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = local_le.DocumentTypeID
	LEFT JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
	LEFT JOIN dbo.LeadDocument ld WITH (NOLOCK) ON le.LeadDocumentID = ld.LeadDocumentID
	WHERE 
	(
		(IsNewNameBlank = 0) 
		OR 
		(IsNewFormatBlank = 0) 
		OR 
		(dt.OutputFormat = 'SMS')
	)

	ORDER BY le.WhenCreated desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadDocumentAttachmentList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadDocumentAttachmentList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadDocumentAttachmentList] TO [sp_executeall]
GO
