SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeMandatoryField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeMandatoryFieldID int   = null ,

	@EventTypeID int   = null ,

	@DetailFieldID int   = null ,

	@LeadTypeID int   = null ,

	@ClientID int   = null ,

	@ForceEditableOverride bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeMandatoryFieldID]
	, [EventTypeID]
	, [DetailFieldID]
	, [LeadTypeID]
	, [ClientID]
	, [ForceEditableOverride]
    FROM
	[dbo].[EventTypeMandatoryField] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeMandatoryFieldID] = @EventTypeMandatoryFieldID OR @EventTypeMandatoryFieldID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ForceEditableOverride] = @ForceEditableOverride OR @ForceEditableOverride IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeMandatoryFieldID]
	, [EventTypeID]
	, [DetailFieldID]
	, [LeadTypeID]
	, [ClientID]
	, [ForceEditableOverride]
    FROM
	[dbo].[EventTypeMandatoryField] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeMandatoryFieldID] = @EventTypeMandatoryFieldID AND @EventTypeMandatoryFieldID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ForceEditableOverride] = @ForceEditableOverride AND @ForceEditableOverride is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Find] TO [sp_executeall]
GO
