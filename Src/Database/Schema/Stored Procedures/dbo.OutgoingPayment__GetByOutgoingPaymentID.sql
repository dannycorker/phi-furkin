SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-07-2016
-- Description:	Gets an outgoing payment via its identity
-- =============================================
CREATE PROCEDURE [dbo].[OutgoingPayment__GetByOutgoingPaymentID]

	@OutgoingPaymentID INT,
	@CustomerID INT,
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT op.*, pt.PaymentTypeName, ps.SourceType FROM OutgoingPayment op WITH (NOLOCK) 
	LEFT JOIN PaymentTypes pt WITH (NOLOCK) ON pt.PaymentTypeID = op.PaymentTypeID
	LEFT JOIN PaymentSourceType ps WITH (NOLOCK) ON ps.PaymentSourceTypeID=op.PaymentSourceTypeID
	WHERE OutgoingPaymentID=@OutgoingPaymentID AND CustomerID=@CustomerID AND op.ClientID=@ClientID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[OutgoingPayment__GetByOutgoingPaymentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutgoingPayment__GetByOutgoingPaymentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutgoingPayment__GetByOutgoingPaymentID] TO [sp_executeall]
GO
