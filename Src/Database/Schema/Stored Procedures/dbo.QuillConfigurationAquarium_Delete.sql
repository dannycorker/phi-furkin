SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuillConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationAquarium_Delete]
(

	@QuillConfigurationAquariumID int   
)
AS


				DELETE FROM [dbo].[QuillConfigurationAquarium] WITH (ROWLOCK) 
				WHERE
					[QuillConfigurationAquariumID] = @QuillConfigurationAquariumID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationAquarium_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Delete] TO [sp_executeall]
GO
