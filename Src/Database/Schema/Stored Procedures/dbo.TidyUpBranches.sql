SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[TidyUpBranches] 
@MasterQuestionID int

as

Update QuestionPossibleAnswers
SET  Branch = 0
where Branch=@MasterQuestionID



GO
GRANT VIEW DEFINITION ON  [dbo].[TidyUpBranches] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TidyUpBranches] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TidyUpBranches] TO [sp_executeall]
GO
