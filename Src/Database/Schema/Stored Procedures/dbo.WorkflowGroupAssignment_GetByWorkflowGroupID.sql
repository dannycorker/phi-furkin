SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowGroupAssignment table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupID]
(

	@WorkflowGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[WorkflowGroupAssignmentID],
					[WorkflowGroupID],
					[ClientPersonnelID],
					[Priority],
					[ClientID]
				FROM
					[dbo].[WorkflowGroupAssignment] WITH (NOLOCK) 
				WHERE
					[WorkflowGroupID] = @WorkflowGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_GetByWorkflowGroupID] TO [sp_executeall]
GO
