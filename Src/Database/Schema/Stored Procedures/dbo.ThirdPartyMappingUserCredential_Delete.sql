SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyMappingUserCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential_Delete]
(

	@ThirdPartyMappingUserCredentialID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyMappingUserCredential] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyMappingUserCredentialID] = @ThirdPartyMappingUserCredentialID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Delete] TO [sp_executeall]
GO
