SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE PROCEDURE [dbo].[GetLinkedDetailFieldByFieldName]
@LeadTypeIDTo int,
@LeadTypeIDFrom int,
@FieldName nvarchar(100)

as

SELECT LinkedDetailFieldID, LeadTypeIDTo, LeadTypeIDFrom, DetailFieldIDTo, DetailFieldIDFrom, DisplayOnPageID, 
FieldOrder, Enabled, History, LeadOrMatter, FieldName, LeadLinkedTo, LeadLinkedFrom, IncludeLinkedField, ClientID 
FROM dbo.LinkedDetailFields
WHERE (LeadTypeIDTo = @LeadTypeIDTo) AND (LeadTypeIDFrom = @LeadTypeIDFrom) AND (FieldName = @FieldName)








GO
GRANT VIEW DEFINITION ON  [dbo].[GetLinkedDetailFieldByFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLinkedDetailFieldByFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLinkedDetailFieldByFieldName] TO [sp_executeall]
GO
