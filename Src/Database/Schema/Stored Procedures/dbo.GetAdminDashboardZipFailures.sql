SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-10
-- Description:	Admin dashboard account ZipFailures
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminDashboardZipFailures]
	@ClientID int
AS
BEGIN

	SET NOCOUNT ON;
	WITH dzi AS 
	(
		SELECT dzi.DocumentZipInformationID, dzi.WhenStored, dzi.DocumentsPerZip, dzi.ZipPrefix
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		WHERE dzi.ClientID = @ClientID 
		AND dzi.StatusID IN (31, 32, 34) 
	),
	dzi_total AS 
	(
		SELECT dz.DocumentZipInformationID, COUNT(*) AS TotalCount
		FROM dzi 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		GROUP BY dz.DocumentZipInformationID
	),
	dzi_fails AS 
	(
		SELECT dz.DocumentZipInformationID, COUNT(*) AS FailCount
		FROM dzi 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		AND dz.StatusID = 34 
		GROUP BY dz.DocumentZipInformationID
	),
	dzi_badtemplates AS 
	(
		SELECT dz.DocumentZipInformationID, COUNT(*) AS BadTemplateCount
		FROM dzi 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		AND dz.StatusID = 32
		GROUP BY dz.DocumentZipInformationID
	),
	dzi_done AS 
	(
		SELECT dz.DocumentZipInformationID, COUNT(*) AS DoneCount
		FROM dzi 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		AND dz.StatusID = 31
		GROUP BY dz.DocumentZipInformationID
	),
	dzi_todo AS 
	(
		SELECT dz.DocumentZipInformationID, COUNT(*) AS TodoCount
		FROM dzi 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
		AND dz.StatusID = 35
		GROUP BY dz.DocumentZipInformationID
	)
	SELECT dzi.DocumentZipInformationID,  dzi.DocumentsPerZip, dzi.ZipPrefix, dzi.WhenStored,
	COALESCE(dzi_fails.FailCount, 0) AS FailCount, 
	COALESCE(dzi_badtemplates.BadTemplateCount, 0) AS BadTemplateCount, 
	COALESCE(dzi_done.DoneCount, 0) AS DoneCount, 
	COALESCE(dzi_todo.TodoCount, 0) AS TodoCount/*, 
	ISNULL(dzi_total.TotalCount, 0) - ISNULL(dzi_done.DoneCount, 0) AS [Checksum]  */ 
	FROM dzi 
	LEFT JOIN dzi_fails ON dzi_fails.DocumentZipInformationID = dzi.DocumentZipInformationID 
	LEFT JOIN dzi_badtemplates ON dzi_badtemplates.DocumentZipInformationID = dzi.DocumentZipInformationID 
	LEFT JOIN dzi_done ON dzi_done.DocumentZipInformationID = dzi.DocumentZipInformationID 
	LEFT JOIN dzi_total ON dzi_total.DocumentZipInformationID = dzi.DocumentZipInformationID 
	LEFT JOIN dzi_todo ON dzi_todo.DocumentZipInformationID = dzi.DocumentZipInformationID 
	WHERE ISNULL(dzi_done.DoneCount, 0) <> ISNULL(dzi_total.TotalCount, 0)
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardZipFailures] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAdminDashboardZipFailures] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardZipFailures] TO [sp_executeall]
GO
