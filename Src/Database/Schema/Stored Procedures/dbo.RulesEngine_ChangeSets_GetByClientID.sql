SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-11
-- Description:	Returns all changesets for a client
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ChangeSets_GetByClientID]
(
	@ClientID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	cs.ChangeSetID, cs.ParentChangeSetID, cs.ClientID, cs.TagName, CONVERT(CHAR(10), cs.ValidFrom, 120) ValidFrom, CONVERT(CHAR(10), cs.ValidTo, 120) ValidTo, 
		cs.WhenCreated, cs.WhoCreated WhoCreatedID, cs.WhenModified, cs.WhoModified WhoModifiedID, 
		cpCreate.UserName WhoCreated, cpMod.UserName WhoModified,
		(	
			SELECT TOP 1 csi.ChangeSetID
			FROM RulesEngine_ChangeSets csi WITH (NOLOCK) 
			WHERE csi.ParentChangeSetID = cs.ChangeSetID
			AND		csi.ClientID = cs.ClientID
		) ChildChangeSetID
	FROM dbo.RulesEngine_ChangeSets cs WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON cs.WhoCreated = cpCreate.ClientPersonnelID
	LEFT JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON cs.WhoModified = cpMod.ClientPersonnelID
	WHERE cs.ClientID = @ClientID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ChangeSets_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_GetByClientID] TO [sp_executeall]
GO
