SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteria table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SELECT
					[SqlQueryEditingCriteriaID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[SqlQueryEditingColumnID],
					[CriteriaText],
					[Criteria1],
					[Criteria2],
					[CriteriaName],
					[SubQueryID],
					[SubQueryLinkType],
					[ParamValue],
					[IsSecurityClause],
					[CriteriaSubstitutions],
					[IsParameterizable],
					[Comparison],
					[IsJoinClause]
				FROM
					[dbo].[SqlQueryEditingCriteria] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingID] = @SqlQueryEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingID] TO [sp_executeall]
GO
