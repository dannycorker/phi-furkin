SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CheckMateField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateField_Find]
(

	@SearchUsingOR bit   = null ,

	@CheckMateFieldID int   = null ,

	@CheckMateFieldName varchar (50)  = null ,

	@CheckMateFieldDescription varchar (250)  = null ,

	@QuestionTypeID int   = null ,

	@LookupListID int   = null ,

	@IsEnabled bit   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CheckMateFieldID]
	, [CheckMateFieldName]
	, [CheckMateFieldDescription]
	, [QuestionTypeID]
	, [LookupListID]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[CheckMateField] WITH (NOLOCK) 
    WHERE 
	 ([CheckMateFieldID] = @CheckMateFieldID OR @CheckMateFieldID IS NULL)
	AND ([CheckMateFieldName] = @CheckMateFieldName OR @CheckMateFieldName IS NULL)
	AND ([CheckMateFieldDescription] = @CheckMateFieldDescription OR @CheckMateFieldDescription IS NULL)
	AND ([QuestionTypeID] = @QuestionTypeID OR @QuestionTypeID IS NULL)
	AND ([LookupListID] = @LookupListID OR @LookupListID IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CheckMateFieldID]
	, [CheckMateFieldName]
	, [CheckMateFieldDescription]
	, [QuestionTypeID]
	, [LookupListID]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[CheckMateField] WITH (NOLOCK) 
    WHERE 
	 ([CheckMateFieldID] = @CheckMateFieldID AND @CheckMateFieldID is not null)
	OR ([CheckMateFieldName] = @CheckMateFieldName AND @CheckMateFieldName is not null)
	OR ([CheckMateFieldDescription] = @CheckMateFieldDescription AND @CheckMateFieldDescription is not null)
	OR ([QuestionTypeID] = @QuestionTypeID AND @QuestionTypeID is not null)
	OR ([LookupListID] = @LookupListID AND @LookupListID is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Find] TO [sp_executeall]
GO
