SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventChoice table through an index
-- MODIFIED	2014-07-25	SB	Limited event choice by client ID and used event type function due to shared lead types
-- Modified to bring back auto aducication
-- 2015-03-19 ACE Updated to include Auto Adjudication
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice__GetGridByEventTypeID]
(
	@EventTypeID int,
	@ClientID INT = NULL
)
AS


				SELECT
					[EventChoiceID],
					[Description],
					[NextEventTypeID],
					[EventTypeName],				
					[ThreadNumber],
					[EscalationEvent],
					[Weighting],
					c.SqlClauseForInclusion,
					CONVERT(bit,CASE WHEN ISNULL(c.SqlClauseForInclusion,'') = '' THEN 0 ELSE 1 END) AS [HasAutoAducication]
				FROM
					[dbo].[EventChoice] c
				INNER JOIN 
					dbo.fnEventTypeShared(@ClientID) t on t.EventTypeID = c.NextEventTypeID
				WHERE
					c.[EventTypeID] = @EventTypeID
				AND
					(c.ClientID = @ClientID OR @ClientID IS NULL)
				ORDER BY isnull(Weighting, 9999) asc

			Select @@ROWCOUNT
					
			


GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetGridByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetGridByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetGridByEventTypeID] TO [sp_executeall]
GO
