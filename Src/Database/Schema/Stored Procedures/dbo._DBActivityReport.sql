SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-07-20
-- Description:	Show any activity that might be affecting the app or DB
-- =============================================
CREATE PROCEDURE [dbo].[_DBActivityReport] 
	@StartDateTime DATETIME = NULL, 
	@WhatCountsAsSlowSeconds INT = 60
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Default to the start of the current day */
	IF @StartDateTime IS NULL
	BEGIN
		SELECT @StartDateTime = dbo.fnDateOnly(dbo.fn_GetDate_Local())
	END
	
	/* Show any Excel exports currently in progress from the web app (not scheduler) */
	SELECT 'ExcelRunning' AS ExcelRunning, DATEDIFF(SECOND, l.LogDateTime, dbo.fn_GetDate_Local()) AS RunTime, *
	FROM dbo.Logs l WITH (NOLOCK) 
	WHERE l.LogDateTime > @StartDateTime
	AND l.ClassName = 'ExcelHelper'
	AND l.MethodName = 'WriteToResponse Start' 
	AND NOT EXISTS (
		SELECT *
		FROM dbo.Logs l WITH (NOLOCK) 
		WHERE l.LogDateTime > @StartDateTime
		AND l.ClassName = 'ExcelHelper'
		AND l.MethodName = 'WriteToResponse End' 
	)
	
	/* Show any completed Excel exports from the web app (not scheduler) that we would class as slow */
	;WITH InnerSql AS 
	(
		SELECT *, 
		ROW_NUMBER() OVER(PARTITION BY l.LogEntry ORDER BY l.LogID) as rn 
		FROM dbo.Logs l WITH (NOLOCK) 
		WHERE l.LogDateTime > @StartDateTime 
		AND l.ClassName = 'ExcelHelper'
		AND l.MethodName LIKE 'WriteToResponse%'
	),
	StartAndEnd AS 
	(
		SELECT i.*, i2.MethodName AS FinishNote, i2.LogDateTime AS FinishTime, DATEDIFF(SECOND, i.LogDateTime, i2.LogDateTime) AS NumberOfSeconds 
		FROM InnerSql i 
		CROSS JOIN InnerSql i2 
		WHERE i.LogEntry = i2.LogEntry 
		AND (i2.rn % 2 = 0) 
		AND (i.rn = i2.rn - 1)
		AND i.MethodName = 'WriteToResponse Start'
		AND i2.MethodName = 'WriteToResponse End'
	)
	SELECT 'ExcelComplete' AS ExcelComplete, s.* 
	FROM StartAndEnd s 
	WHERE s.NumberOfSeconds > @WhatCountsAsSlowSeconds 
	ORDER BY s.LogDateTime DESC
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_DBActivityReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_DBActivityReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_DBActivityReport] TO [sp_executeall]
GO
