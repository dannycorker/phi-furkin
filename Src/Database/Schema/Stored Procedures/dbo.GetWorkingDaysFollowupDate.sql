SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 28th June 2010
-- Description:	Returns a working-day follow up date
-- =============================================
CREATE PROCEDURE [dbo].[GetWorkingDaysFollowupDate] 
(
	@NumberOfFollowUpDays int = -1,
	@CurrentDate datetime = null,
	@FollowUpDate datetime
)
AS
BEGIN
	SET NOCOUNT ON;

-- Default CurrentDate to now if it's not specified
SET @CurrentDate = COALESCE(@CurrentDate, dbo.fn_GetDate_Local())

-- If the number of days isn't set, work it out from the Current and FollowUp dates
IF @NumberOfFollowUpDays = -1
BEGIN
	SET @NumberOfFollowUpDays = DATEDIFF(day, @CurrentDate, @FollowUpDate);
END

/*
Assuming we've got a value for the days, select that many records
from the WorkingDays table and get the last one as the working-days FollowUp date
*/
IF @NumberOfFollowUpDays > -1 AND @NumberOfFollowUpDays IS NOT NULL
BEGIN
	;WITH ApplicableWorkingDays AS
	(
	SELECT TOP (@NumberOfFollowUpDays) *
	FROM WorkingDays wd WITH (NOLOCK) 
	WHERE wd.IsWorkDay = 1
	and wd.CharDate > CONVERT(VARCHAR(10), @CurrentDate, 120) 
	ORDER BY wd.date
	)

	SELECT TOP 1 *
	FROM ApplicableWorkingDays WITH (NOLOCK) 
	ORDER BY ApplicableWorkingDays.Date desc
END
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetWorkingDaysFollowupDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetWorkingDaysFollowupDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetWorkingDaysFollowupDate] TO [sp_executeall]
GO
