SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-12-2012
-- Description:	Gets the standard field options by client id
-- =============================================
CREATE PROCEDURE [dbo].[StandardFieldOption__GetByClientID]

	@ClientID INT

AS
BEGIN

	SELECT * FROM StandardFieldOption WITH (NOLOCK) 
	WHERE ClientID=@ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[StandardFieldOption__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StandardFieldOption__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StandardFieldOption__GetByClientID] TO [sp_executeall]
GO
