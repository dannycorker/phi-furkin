SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-08-20
-- Description:	Initial Data Load For L&G
-- JEL 2018-09-18 Added current policy so it works with EQB matching (join to current policy needed to exclude multi pet 
-- AHOD 2019-01-21 Added AquariumStatusID = 2 to first Customer Update Block to set customers to Accepted
-- GPR 2019-06-03 Added filter to exclude Policies with instance of Renew or Expire events from update to Status or EndDate
-- PL  2019-09-24 Added filter to only update policies status and end dates on cancellation #59634
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ImportData]
	@ImportToBrandID INT = 153389, /*Default to L&G */
	@ObfuscateEmails BIT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	/*
		-1. Thin down the data by getting the last set of values for each policy..
		0. Update source data with ID's if this is an update..
		1. Load customer data (deduplicated)
		2. Update the customer data with un-deduplicated data (rest of address, first name etc)
		3. Create Leads (policy) for each pet
		4. Create policy cases (one for each lead created)
		5. Create required matters for each case (one per case)
		6. Populate the required fields at lead level and matter level. 
		7. Save off the originating policy number for later use.

	*/

	CREATE TABLE #ImportedCustomers (CustomerID INT, LastName VARCHAR(100), PostCode VARCHAR(100), DateOfBirth DATE)

	CREATE TABLE #ImportedLeads (AquariumCustomerID INT, PAleadID INT, PolicyID VARCHAR(2000))

	CREATE TABLE #ImportedPolicyCases (LeadID INT, CaseID INT)

	CREATE TABLE #ImportedPolicyMatters(CaseID INT, AquariumMatterID INT)

	CREATE TABLE #InsertedObjects (TableName VARCHAR(100), RowCounter INT)

	CREATE TABLE #FieldHistory (CustomerID INT, LeadID INT, MatterID INT, DetailFieldID INT, DetailValue VARCHAR(2000), LeadORMatter INT)

	/*2018-11-28 ACE moved the null check out of the ordering or we risk going backwards...*/
	;
	WITH OrderedImports AS (
		SELECT *, ROW_NUMBER() OVER (PARTITION BY c.[Column 1] ORDER BY c.[Column 3] DESC) AS [Rn]
		FROM _C600_UPP_FILE_CATCHUP c
		--WHERE AquariumCustomerID IS NULL 
	)

	SELECT *
	INTO #LatestImports
	FROM OrderedImports o
	WHERE o.Rn = 1
	AND o.AquariumCustomerID IS NULL

	/*
		Note the idents for update later..
	*/
	UPDATE c
	SET AquariumCustomerID = m.CustomerID, PALeadID = m.LeadID, PACaseID = m.CaseID, PAMatterID = m.MatterID
	FROM #LatestImports c
	INNER JOIN MatterDetailValues mdv_policyID WITH (NOLOCK) ON mdv_policyID.DetailFieldID = 180168
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv_policyID.MatterID 
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID AND cu.Test = 0
	WHERE mdv_policyID.DetailValue = c.[Column 1]

	UPDATE c
	SET AquariumCustomerID = i.CustomerID
	FROM Customers i
	INNER JOIN #LatestImports c ON c.[Column 41] COLLATE Latin1_General_CI_AS = i.LastName 
		AND c.[Column 48] COLLATE Latin1_General_CI_AS = i.PostCode 
		AND c.[Column 42] COLLATE Latin1_General_CI_AS = i.DateOfBirth
	WHERE i.Test = 0

	/*Create the customers that havent been updated yet*/
	INSERT INTO Customers (ClientID, TitleID, LastName, PostCode, DateOfBirth, ChangeSource, IsBusiness)
	OUTPUT inserted.CustomerID, inserted.LastName, inserted.PostCode, inserted.DateOfBirth INTO #ImportedCustomers (CustomerID, LastName, PostCode, DateOfBirth)
	SELECT DISTINCT @ClientID, 0, [Column 41], [Column 48], [Column 42], 'LegacyImport', 0
	FROM #LatestImports c
	WHERE c.AquariumCustomerID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('Customers', @@ROWCOUNT)

	PRINT 'Start CustomerID Update'

	UPDATE c
	SET AquariumCustomerID = i.CustomerID
	FROM #ImportedCustomers i
	INNER JOIN #LatestImports c ON c.[Column 41] COLLATE Latin1_General_CI_AS = i.LastName 
		AND c.[Column 48] COLLATE Latin1_General_CI_AS = i.PostCode 
		AND c.[Column 42] COLLATE Latin1_General_CI_AS = i.DateOfBirth

	PRINT 'CustomerID Update Complete'

	/*2018-09-05 ACE Concatenate Address1 & 2 into our Address1*/
	UPDATE c
	SET FirstName = REPLACE(i.[Column 40],CHAR(0),''), 
		Address1 = REPLACE(i.[Column 43],CHAR(0),'') + CASE WHEN ISNULL(REPLACE(i.[Column 43],CHAR(0),''),'') <> '' AND ISNULL(REPLACE(i.[Column 44],CHAR(0),''),'') <> '' THEN ', ' ELSE '' END + REPLACE(i.[Column 44],CHAR(0),''), 
		Address2 = REPLACE(i.[Column 45],CHAR(0),''), 
		Town = REPLACE(i.[Column 46],CHAR(0),''), 
		County = REPLACE(i.[Column 47],CHAR(0),''), 
		EmailAddress = CASE WHEn @ObfuscateEmails = 1 THEN 'mark.colonnese@aquarium-software.com' ELSE i.[Column 52] END, 
		TitleID = t.TitleID,
		AquariumStatusID = 2 /*AHOD 2019-01-21 Updated to set customers to Accepted*/
	FROM Customers c
	INNER JOIN #LatestImports i ON i.AquariumCustomerID = c.CustomerID
	LEFT JOIN Titles t ON t.Title = REPLACE(i.[Column 39], '.', '')
	WHERE 
	(ISNULL(FirstName, '') <> ISNULL(i.[Column 40], '')
	OR ISNULL(Address1, '') <> ISNULL(i.[Column 43] + CASE WHEN ISNULL(i.[Column 43],'') <> '' AND ISNULL(i.[Column 44],'') <> '' THEN ', ' ELSE '' END + i.[Column 44], '')
	OR ISNULL(Address2, '') <> ISNULL(i.[Column 44] ,'')
	OR ISNULL(Town,'') <> ISNULL(i.[Column 45] ,'')
	OR ISNULL(County,'') <> ISNULL(i.[Column 46] ,'')
	OR ISNULL(EmailAddress,'') <> ISNULL(CASE WHEN @ObfuscateEmails = 1 THEN 'mark.colonnese@aquarium-software.com' ELSE i.[Column 52] END, '')
	OR DateOfBirth <> i.[Column 42]
	OR ISNULL(LastName, '') <> ISNULL(i.[Column 41], '')
	OR ISNULL(PostCode, '') <> ISNULL(i.[Column 48], '')
	)
	AND NOT EXISTS (
		SELECT *
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadID = l.LeadID AND le.EventDeleted = 0 AND le.EventTypeID = 155196 /*Create linked leads and complete set-up*/
		WHERE l.CustomerID = c.CustomerID
	)

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('CustomerUpdates', @@ROWCOUNT)

	INSERT INTO Lead (ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
	OUTPUT inserted.CustomerID, inserted.LeadID, inserted.LeadRef INTO #ImportedLeads (AquariumCustomerID, PAleadID, PolicyID)
	SELECT DISTINCT @ClientID, c.[Column 1], c.AquariumCustomerID, 1492, 2, NULL, 0, 0, NULL, NULL, NULL, 1, NULL, NULL, dbo.fn_GetDate_Local()
	FROM #LatestImports c
	WHERE c.PALeadID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('Lead', @@ROWCOUNT)

	PRINT 'Lead insert Complete'

	UPDATE c
	SET PALeadID = i.PALeadID
	FROM #LatestImports c
	INNER JOIN #ImportedLeads i ON i.PolicyID = c.[Column 1] COLLATE Latin1_General_CI_AS 

	INSERT INTO Cases (LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
	OUTPUT inserted.LeadID, inserted.CaseID INTO #ImportedPolicyCases(LeadID, CaseID)
	SELECT c.PALeadID, @ClientID, 1, c.[Column 1] AS [PolicyNo], NULL AS [Status], 2, NULL, NULL, NULL, NULL, NULL, NULL, 44412, CONVERT(DATE,c.[Column 19]), 44412, dbo.fn_GetDate_Local(), NULL
	FROM #ImportedLeads i
	INNER JOIN #LatestImports c ON c.PALeadID = i.PAleadID
	WHERE c.PACaseID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('Cases', @@ROWCOUNT)

	PRINT 'Case insert Complete'

	UPDATE c
	SET PACaseID = i.CaseID
	FROM #LatestImports c
	INNER JOIN #ImportedPolicyCases i ON i.LeadID = c.PALeadID

	INSERT INTO Matter (ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	OUTPUT inserted.CaseID, inserted.MatterID INTO #ImportedPolicyMatters(CaseID, AquariumMatterID)
	SELECT @ClientID, c.[Column 1], c.AquariumCustomerID, c.PALeadID, 1, dbo.fnRefLetterFromCaseNum(ROW_NUMBER() OVER (Partition BY c.PALeadID ORDER BY c.PALeadID)), 0, c.PACaseID, 44412, CONVERT(DATE,c.[Column 19]), 44412, dbo.fn_GetDate_Local(), NULL
	FROM #LatestImports c
	WHERE c.PAMatterID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('Matter', @@ROWCOUNT)

	UPDATE c
	SET PAMatterID = i.AquariumMatterID
	FROM #LatestImports c
	INNER JOIN #ImportedPolicyMatters i ON i.CaseID = c.PACaseID

	PRINT 'Matter insert Complete'

	/*Brand to import to*/
	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.AquariumCustomerID, 170144, @ImportToBrandID
	FROM #LatestImports i
	WHERE i.AquariumCustomerID > 0
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = i.AquariumCustomerID
		AND cdv.DetailFieldID = 170144
		AND cdv.ValueInt = @ImportToBrandID
	)

	/*Pet name*/
	INSERT INTO LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.PALeadID, 144268, i.[Column 14]
	FROM #LatestImports i
	WHERE i.PALeadID > 0
	AND NOT EXISTS (
		SELECT *
		FROM LeadDetailValues ldv WITH (NOLOCK)
		WHERE ldv.LeadID = i.PALeadID
		AND ldv.DetailFieldID = 144268
		AND ldv.DetailValue = i.[Column 14]
	)

	/*Policy_End_Date*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	OUTPUT inserted.LeadID, inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (LeadID, MatterID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.PALeadID, i.PAMatterID, 170037, i.[Column 21]
	FROM #LatestImports i
	WHERE i.PAMatterID > 0
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = i.PAMatterID
		AND mdv.DetailFieldID = 170037
		AND mdv.DetailValue = i.[Column 21]
	)
	AND NOT EXISTS ( /*GPR 2019-03-13 ZD #55697*/ /*GPR 2019-06-03 introduced Expiry event to IN list*/
		SELECT *
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.CaseID = c.CaseID AND le.EventDeleted = 0 AND le.EventTypeID IN (150151 /*Renew Policy*/, 150150 /*Policy Expired*/)
		WHERE l.LeadID = i.PALeadID
	)
	AND i.[Column 4] = 'Cancelled' /*PL 2019-09-24 ZD #59634*/

	/*Current Policy*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	OUTPUT inserted.LeadID, inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (LeadID, MatterID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.PALeadID, i.PAMatterID, 170034,0
	FROM #LatestImports i
	WHERE i.PAMatterID > 0
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = i.PAMatterID
		AND mdv.DetailFieldID = 170034
	)


	/*Policy Number*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	OUTPUT inserted.LeadID, inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (LeadID, MatterID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.PALeadID, i.PAMatterID, 180168, i.[Column 1]
	FROM #LatestImports i
	WHERE i.PAMatterID > 0
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = i.PAMatterID
		AND mdv.DetailFieldID = 180168
		AND mdv.DetailValue = i.[Column 1]
	)

	/*Outcome*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	OUTPUT inserted.LeadID, inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (LeadID, MatterID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.PALeadID, i.PAMatterID, 180169, i.[Column 4] /*Outcome?*/
	FROM #LatestImports i
	WHERE i.PAMatterID > 0
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = i.PAMatterID
		AND mdv.DetailFieldID = 180169
		AND mdv.DetailValue = i.[Column 4]
	)

	/*Sub Status*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	OUTPUT inserted.LeadID, inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (LeadID, MatterID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT DISTINCT @ClientID, i.PALeadID, i.PAMatterID, 170038, 
		CASE i.[Column 4] /*Outcome?*/
			WHEN 'New Business' THEN '43002' /*Live*/
			WHEN 'MTA' THEN '43002' /*Live*/
			WHEN 'Renewal' THEN '43002' /*Live*/
			WHEN 'Cancelled' THEN '43003' /*Cancelled*/
			WHEN 'Lapsed' THEN '43004' /*Lapsed*/
			ELSE '43003' /*Default to Cancelled...*/
		END
	FROM #LatestImports i
	WHERE i.PAMatterID > 0
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = i.PAMatterID
		AND mdv.DetailFieldID = 170038
		AND mdv.DetailValue = 
			CASE i.[Column 4] /*Outcome?*/
				WHEN 'New Business' THEN '43002' /*Live*/
				WHEN 'MTA' THEN '43002' /*Live*/
				WHEN 'Renewal' THEN '43002' /*Live*/
				WHEN 'Cancelled' THEN '43003' /*Cancelled*/
				WHEN 'Lapsed' THEN '43004' /*Lapsed*/
				ELSE '43003' /*Default to Cancelled...*/
			END
	)
	AND NOT EXISTS ( /*GPR 2019-03-13 ZD #55697*/ /*GPR 2019-06-03 introduced Expiry event to IN list*/
		SELECT *
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.CaseID = c.CaseID AND le.EventDeleted = 0 AND le.EventTypeID IN (150151 /*Renew Policy*/, 150150 /*Policy Expired*/)
		WHERE l.LeadID = i.PALeadID
	)
	AND i.[Column 4] = 'Cancelled' /*PL 2019-09-24 ZD #59634*/
	

	INSERT INTO DetailValueHistory (ClientID, CustomerID, LeadID, MatterID, LeadOrMatter, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
	SELECT @ClientID, f.CustomerID, f.LeadID, f.MatterID, f.LeadORMatter, f.DetailFieldID, f.DetailValue, 58552, dbo.fn_GetDate_Local()
	FROM #FieldHistory f

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('DetailValueHistory', @@ROWCOUNT)

	UPDATE c
	SET AquariumCustomerID = l.AquariumCustomerID, PALeadID = l.PALeadID, PACaseID = l.PACaseID, PAMatterID = l.PAMatterID, DateImported = dbo.fn_GetDate_Local()
	FROM _C600_UPP_FILE_CATCHUP c
	INNER JOIN #LatestImports l ON l.UNID = c.UNID
	WHERE l.PAMatterID > 0

	INSERT INTO #InsertedObjects (TableName, RowCounter)
	VALUES ('_C600_UPP_FILE_CATCHUP', @@ROWCOUNT)

	SELECT *
	FROM #InsertedObjects

	SELECT *
	FROM #LatestImports

	DROP TABLE #InsertedObjects
	DROP TABLE #LatestImports

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ImportData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportData] TO [sp_executeall]
GO
