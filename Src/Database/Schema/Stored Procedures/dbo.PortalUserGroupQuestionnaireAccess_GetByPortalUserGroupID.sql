SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserGroupQuestionnaireAccess table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupID]
(

	@PortalUserGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalUserGroupQuestionnaireAccessID],
					[ClientID],
					[PortalUserGroupID],
					[ClientQuestionnaireID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupQuestionnaireAccess] WITH (NOLOCK) 
				WHERE
					[PortalUserGroupID] = @PortalUserGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_GetByPortalUserGroupID] TO [sp_executeall]
GO
