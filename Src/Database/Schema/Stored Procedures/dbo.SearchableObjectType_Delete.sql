SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SearchableObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchableObjectType_Delete]
(

	@SearchableObjectTypeID int   
)
AS


				DELETE FROM [dbo].[SearchableObjectType] WITH (ROWLOCK) 
				WHERE
					[SearchableObjectTypeID] = @SearchableObjectTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchableObjectType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Delete] TO [sp_executeall]
GO
