SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUserCase table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserCase_Update]
(

	@PortalUserCaseID INT   ,

	@PortalUserID INT   ,

	@LeadID INT   ,

	@CaseID INT   ,

	@ClientID INT = NULL   
)
AS
BEGIN
	
	-- Modify the updatable columns
	UPDATE
		[dbo].[PortalUserCase]
	SET
		[PortalUserID] = @PortalUserID
		,[LeadID] = @LeadID
		,[CaseID] = @CaseID
		,[ClientID] = @ClientID
	WHERE
		[PortalUserCaseID] = @PortalUserCaseID 
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Update] TO [sp_executeall]
GO
