SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentGateway_Get_List]

AS


				
				SELECT
					[PaymentGatewayID],
					[Gateway],
					[Name],
					[Description],
					[Url]
				FROM
					[dbo].[PaymentGateway] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Get_List] TO [sp_executeall]
GO
