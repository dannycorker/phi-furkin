SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSeries table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_GetByChartSeriesID]
(

	@ChartSeriesID int   
)
AS


				SELECT
					[ChartSeriesID],
					[ChartSeriesTypeID],
					[ChartSeriesOrder],
					[ChartID],
					[ClientID],
					[PanelItemChartingID],
					[ColumnName]
				FROM
					[dbo].[ChartSeries] WITH (NOLOCK) 
				WHERE
										[ChartSeriesID] = @ChartSeriesID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_GetByChartSeriesID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_GetByChartSeriesID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_GetByChartSeriesID] TO [sp_executeall]
GO
