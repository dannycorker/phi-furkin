SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_AlterFreeTextFieldToPhoneNumber]
(
@LeadEventID int
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@ClientID int,
	@myERROR int,
	@LeadID int

	Select @ClientID = ldv.detailvalue
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	Inner join leaddetailvalues ldv on ldv.leadid = lead.leadid and ldv.detailfieldid = 6453
	where leadevent.leadeventid = @LeadEventID 

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int
	)

	INSERT @RequiredFields SELECT 6453 

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int 
	);

	INSERT INTO @LeadsAndMatters (LeadID, MatterID)
	SELECT DISTINCT m.LeadID, m.MatterID 
	FROM Matter m 
	INNER JOIN Lead l ON l.LeadID = m.LeadID AND l.LeadTypeID = 135 
	WHERE m.ClientID = 38

Begin Tran

	update detailfields
	set QuestiontypeID = 12, EquationText = '', FieldSize = NULL, ValidationCriteriaFieldTypeID = 9, ValidationCriteriaID = 1, MinimumValue = '', Regex = '^((((?:\+|00)\d{1,3}\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|(((?:\+|00)\d{1,3}\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|(((?:\+|00)\d{1,3}\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$', ErrorMessage = '', DefaultFilter = '', ColumnEquationText = '', Hidden = 0, LastReferenceInteger = 0, ReferenceValueFormatID = 0, Encrypt = 0, ShowCharacters = 0, NumberofCharactersToShow = 0
	where fieldname like '%Phone%'
	and clientid = @ClientID

	Select @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	update detailfields
	set QuestiontypeID = 18, EquationText = '', FieldSize = NULL, ValidationCriteriaFieldTypeID = 9, ValidationCriteriaID = 1, MinimumValue = '', Regex = '^((((?:\+|00)\d{1,3}\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|(((?:\+|00)\d{1,3}\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|(((?:\+|00)\d{1,3}\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$', ErrorMessage = '', DefaultFilter = '', ColumnEquationText = '', Hidden = 0, LastReferenceInteger = 0, ReferenceValueFormatID = 0, Encrypt = 0, ShowCharacters = 0, NumberofCharactersToShow = 0
	where fieldname like '%fax%'
	and clientid = @ClientID

	Select @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	GOTO END_NOW

HANDLE_ERROR:
    ROLLBACK TRAN

END_NOW:


END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_AlterFreeTextFieldToPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_AlterFreeTextFieldToPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_AlterFreeTextFieldToPhoneNumber] TO [sp_executeall]
GO
