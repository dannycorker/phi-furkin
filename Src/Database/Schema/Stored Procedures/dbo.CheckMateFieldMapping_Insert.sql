SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CheckMateFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_Insert]
(

	@CheckMateFieldMappingID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@CheckMateFieldID int   ,

	@FieldID int   ,

	@ColumnFieldID int   ,

	@DataLoaderObjectTypeID int   ,

	@CheckMateFieldGroupID int   ,

	@IsEnabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[CheckMateFieldMapping]
					(
					[ClientID]
					,[LeadTypeID]
					,[CheckMateFieldID]
					,[FieldID]
					,[ColumnFieldID]
					,[DataLoaderObjectTypeID]
					,[CheckMateFieldGroupID]
					,[IsEnabled]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@CheckMateFieldID
					,@FieldID
					,@ColumnFieldID
					,@DataLoaderObjectTypeID
					,@CheckMateFieldGroupID
					,@IsEnabled
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @CheckMateFieldMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Insert] TO [sp_executeall]
GO
