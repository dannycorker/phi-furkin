SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Expanded proc to show LeadEvent details from foreign key tables
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent__GetExpandedByLeadEventID]
(

	@LeadEventID int   
)
AS


				SELECT le.*,
					CASE WHEN et.EventTypeName IS NOT NULL THEN 'Action' ELSE 'Note' END AS EventOrNote,
					et.EventTypeName as EventTypeName,					
					CASE WHEN le.NotePriority = '1' THEN nt.AlertColour ELSE nt.NormalColour END AS NoteColour,
					nt.AlertColour as AlertColour,
					est.EventSubtypeName as EventSubtypeName,
					aest.AquariumEventSubtypeName as AquariumEventSubtypeName,
					cpWhoCreated.UserName as WhoCreatedName,
					cpWhoDeleted.UserName as WhoDeletedName
				FROM [dbo].[LeadEvent] le WITH (NOLOCK)
				LEFT JOIN [dbo].[EventType] et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
				LEFT JOIN [dbo].[NoteType] nt WITH (NOLOCK) ON nt.NoteTypeID = le.NoteTypeID
				LEFT JOIN [dbo].[EventSubtype] est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID
				LEFT JOIN [dbo].[AquariumEventSubtype] aest WITH (NOLOCK) ON aest.AquariumEventSubtypeID = et.AquariumEventSubtypeID
				INNER JOIN [dbo].[ClientPersonnel] cpWhoCreated WITH (NOLOCK) ON cpWhoCreated.ClientPersonnelID = le.WhoCreated
				LEFT JOIN [dbo].[ClientPersonnel] cpWhoDeleted WITH (NOLOCK) ON cpWhoDeleted.ClientPersonnelID = le.WhoDeleted
				WHERE [LeadEventID] = @LeadEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetExpandedByLeadEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__GetExpandedByLeadEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetExpandedByLeadEventID] TO [sp_executeall]
GO
