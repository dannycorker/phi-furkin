SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brusehtt
-- Create date: 2011-08-03
-- Description:	Gets the admin contact for a client personnel user
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__GetAdminContactForClientPersonnelUser]
(
	@ClientPersonnelID INT
)	
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT cpAdmin.* 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientOffices o WITH (NOLOCK) ON cp.ClientOfficeID = o.ClientOfficeID
	INNER JOIN dbo.ClientPersonnel cpAdmin WITH (NOLOCK) ON o.AdminClientPersonnelID = cpAdmin.ClientPersonnelID
	WHERE cp.ClientPersonnelID = @ClientPersonnelID 
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetAdminContactForClientPersonnelUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetAdminContactForClientPersonnelUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetAdminContactForClientPersonnelUser] TO [sp_executeall]
GO
