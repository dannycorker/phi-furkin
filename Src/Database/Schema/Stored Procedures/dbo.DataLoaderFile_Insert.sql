SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFile_Insert]
(

	@DataLoaderFileID int    OUTPUT,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@FileStatusID int   ,

	@SourceFileNameAndPath varchar (2000)  ,

	@TargetFileName varchar (2000)  ,

	@TargetFileLocation varchar (2000)  ,

	@FileFormatID int   ,

	@ScheduledDateTime datetime   ,

	@DataLoadedDateTime datetime   ,

	@UploadedBy int   ,

	@RowsInFile int   
)
AS


				
				INSERT INTO [dbo].[DataLoaderFile]
					(
					[ClientID]
					,[DataLoaderMapID]
					,[FileStatusID]
					,[SourceFileNameAndPath]
					,[TargetFileName]
					,[TargetFileLocation]
					,[FileFormatID]
					,[ScheduledDateTime]
					,[DataLoadedDateTime]
					,[UploadedBy]
					,[RowsInFile]
					)
				VALUES
					(
					@ClientID
					,@DataLoaderMapID
					,@FileStatusID
					,@SourceFileNameAndPath
					,@TargetFileName
					,@TargetFileLocation
					,@FileFormatID
					,@ScheduledDateTime
					,@DataLoadedDateTime
					,@UploadedBy
					,@RowsInFile
					)
				-- Get the identity value
				SET @DataLoaderFileID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Insert] TO [sp_executeall]
GO
