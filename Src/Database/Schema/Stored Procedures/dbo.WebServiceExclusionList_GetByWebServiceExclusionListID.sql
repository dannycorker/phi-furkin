SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WebServiceExclusionList table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WebServiceExclusionList_GetByWebServiceExclusionListID]
(

	@WebServiceExclusionListID int   
)
AS


				SELECT
					[WebServiceExclusionListID],
					[ClientPersonnelID]
				FROM
					[dbo].[WebServiceExclusionList] WITH (NOLOCK) 
				WHERE
										[WebServiceExclusionListID] = @WebServiceExclusionListID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_GetByWebServiceExclusionListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WebServiceExclusionList_GetByWebServiceExclusionListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_GetByWebServiceExclusionListID] TO [sp_executeall]
GO
