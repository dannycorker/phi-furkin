SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the HyperlinkStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget_Insert]
(

	@HyperlinkStandardTargetID int    OUTPUT,

	@ClientID int   ,

	@HyperlinkDetailFieldID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@TemplateTypeID int   ,

	@Notes varchar (250)  ,

	@IsSpecial bit   
)
AS


				
				INSERT INTO [dbo].[HyperlinkStandardTarget]
					(
					[ClientID]
					,[HyperlinkDetailFieldID]
					,[Target]
					,[ObjectName]
					,[PropertyName]
					,[TemplateTypeID]
					,[Notes]
					,[IsSpecial]
					)
				VALUES
					(
					@ClientID
					,@HyperlinkDetailFieldID
					,@Target
					,@ObjectName
					,@PropertyName
					,@TemplateTypeID
					,@Notes
					,@IsSpecial
					)
				-- Get the identity value
				SET @HyperlinkStandardTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Insert] TO [sp_executeall]
GO
