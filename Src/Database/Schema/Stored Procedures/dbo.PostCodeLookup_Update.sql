SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PostCodeLookup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PostCodeLookup_Update]
(

	@PostCodeLookupID int   ,

	@PostCode varchar (10)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PostCodeLookup]
				SET
					[PostCode] = @PostCode
				WHERE
[PostCodeLookupID] = @PostCodeLookupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PostCodeLookup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Update] TO [sp_executeall]
GO
