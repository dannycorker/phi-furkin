SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DiaryAppointment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_Insert]
(

	@DiaryAppointmentID int    OUTPUT,

	@ClientID int   ,

	@DiaryAppointmentTitle varchar (250)  ,

	@DiaryAppointmentText varchar (2000)  ,

	@CreatedBy int   ,

	@DueDate datetime   ,

	@EndDate datetime   ,

	@AllDayEvent bit   ,

	@Completed bit   ,

	@LeadID int   ,

	@CaseID int   ,

	@CustomerID int   ,

	@Version int   ,

	@ExportVersion int   ,

	@RecurrenceInfo varchar (MAX)  ,

	@DiaryAppointmentEventType int   ,

	@ResourceInfo varchar (MAX)  ,

	@TempReminderTimeshiftID int   ,

	@StatusID int   ,

	@LabelID int   
)
AS


				
				INSERT INTO [dbo].[DiaryAppointment]
					(
					[ClientID]
					,[DiaryAppointmentTitle]
					,[DiaryAppointmentText]
					,[CreatedBy]
					,[DueDate]
					,[EndDate]
					,[AllDayEvent]
					,[Completed]
					,[LeadID]
					,[CaseID]
					,[CustomerID]
					,[Version]
					,[ExportVersion]
					,[RecurrenceInfo]
					,[DiaryAppointmentEventType]
					,[ResourceInfo]
					,[TempReminderTimeshiftID]
					,[StatusID]
					,[LabelID]
					)
				VALUES
					(
					@ClientID
					,@DiaryAppointmentTitle
					,@DiaryAppointmentText
					,@CreatedBy
					,@DueDate
					,@EndDate
					,@AllDayEvent
					,@Completed
					,@LeadID
					,@CaseID
					,@CustomerID
					,@Version
					,@ExportVersion
					,@RecurrenceInfo
					,@DiaryAppointmentEventType
					,@ResourceInfo
					,@TempReminderTimeshiftID
					,@StatusID
					,@LabelID
					)
				-- Get the identity value
				SET @DiaryAppointmentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Insert] TO [sp_executeall]
GO
