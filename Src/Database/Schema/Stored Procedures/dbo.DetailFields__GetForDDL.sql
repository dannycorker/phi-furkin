SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 23-11-07
-- Description:	Gets a list of DetailFieldIDs and a concat of Pagename + : + Fieldname
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetForDDL]
	-- Add the parameters for the stored procedure here
	@LeadTypeID int,
	@ClientID int,
	@LeadOrMatter int,
	@IncludeDisabledFields bit = 0
AS
	SET NOCOUNT ON

	SELECT
		df.[DetailFieldID],
		[DetailFieldPages].[PageName] + ': ' + df.[FieldCaption] AS FieldName
	FROM
		dbo.fnDetailFieldsShared(@ClientID) df
	INNER JOIN DetailFieldPages ON df.DetailFieldPageID = DetailFieldPages.DetailFieldPageID
	WHERE
		((@IncludeDisabledFields = 0 AND df.[Enabled] = 1) OR @IncludeDisabledFields = 1)
		AND df.[LeadOrMatter] = @LeadOrMatter
		AND df.[LeadTypeID] = @LeadTypeID
		AND df.[ClientID] = @ClientID
	ORDER BY [DetailFieldPages].[PageName], df.[FieldCaption]










GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForDDL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetForDDL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForDDL] TO [sp_executeall]
GO
