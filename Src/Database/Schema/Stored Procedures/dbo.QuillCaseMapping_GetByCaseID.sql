SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillCaseMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_GetByCaseID]
(

	@CaseID int   
)
AS


				SELECT
					[QuillCaseMappingID],
					[ClientID],
					[CaseID],
					[QuillCaseCode]
				FROM
					[dbo].[QuillCaseMapping] WITH (NOLOCK) 
				WHERE
										[CaseID] = @CaseID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_GetByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_GetByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_GetByCaseID] TO [sp_executeall]
GO
