SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2013-03-07
-- Description:	Remove an email address from all aspects of a batch job
-- =============================================
CREATE PROCEDURE [dbo].[EmailRemoval] 
	@Email VARCHAR(200), 
	@ClientIDList VARCHAR(20) = NULL, 
	@ReadOnly BIT = 1, 
	@ReplaceWithEmailAddress VARCHAR(200) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientIDs TABLE (ClientID INT NOT NULL)
	
	/* 
		Users can pass in a csv of clients. 
		Decode them into a table for easy matching throughout the proc.
	*/
	IF @ClientIDList > ''
	BEGIN
		INSERT INTO @ClientIDs(ClientID) 
		SELECT f.AnyID
		FROM dbo.fnTableOfIDsFromCSV(@ClientIDList) f
	END
	ELSE
	BEGIN
		/* Just in case the user passed in an empty string */
		SET @ClientIDList = NULL
	END
	
	/* If we are replacing one address with another, just run one select/update statement and return */
	IF @ReplaceWithEmailAddress > ''
	BEGIN
		/* Show all potential matches (remove all spaces) */
		SELECT 'All Matches' AS [All Matches], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue, REPLACE(atp.ParamValue, @Email, @ReplaceWithEmailAddress) 
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE '%'+@Email+'%'

		/* Update if applicable */
		IF @ReadOnly = 0
		BEGIN
			UPDATE dbo.AutomatedTaskParam 
			SET ParamValue = REPLACE(ParamValue, @Email, @ReplaceWithEmailAddress) 
			WHERE (@ClientIDList IS NULL OR ClientID IN (SELECT ClientID FROM @ClientIDs))
			AND (ParamName LIKE 'EMAIL_TO' OR ParamName LIKE 'EMAIL_%CC' OR ParamName = 'ALERT_ON_ERROR_EMAIL')
			AND ParamValue LIKE '%'+@Email+'%'
			
			/* Show all new matches */
			SELECT 'All New Matches' AS [All New Matches], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue 
			FROM AutomatedTask a WITH (NOLOCK) 
			INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
			AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
			AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
			AND atp.ParamValue LIKE '%'+@ReplaceWithEmailAddress+'%'
		END
		
		RETURN
	END
	
	/* 
		ReadOnly runs select statements without updating the data 
	*/
	IF @ReadOnly = 1
	BEGIN
		
		SELECT 'Please see Messages' AS [Messages]
		
		PRINT '
		Please bear in mind that these are just select statements, so the results on screen may be slightly misleading.
		
		The same param may appear in multiple result sets here, whereas when the update statements are run, they will not.
		
		Finally, you may have to run this more than once to remove stubborn,stubborn,stubborn, addresses!
		'
		
		/* Show all potential matches (remove all spaces) */
		SELECT 'All Matches' AS [All Matches], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue,REPLACE(atp.ParamValue,' ','') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND REPLACE(atp.ParamValue,' ','') LIKE '%'+@Email+'%'

		/* Match the email address with leading and trailing commas */
		SELECT ',Match,' AS [comma Match comma], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue,REPLACE(REPLACE(atp.ParamValue,' ',''),','+@Email+',',',') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND REPLACE(atp.ParamValue,' ','') LIKE '%,'+@Email+',%'

		/* Match the email address with a trailing comma */
		SELECT 'Match,' AS [Match comma], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue,REPLACE(REPLACE(atp.ParamValue,' ',''),@Email + ',','') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND REPLACE(atp.ParamValue,' ','') LIKE @Email + ',%'

		/* Params that end with a comma and then this email address */
		SELECT ',Match' AS [comma Match], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue,REPLACE(REPLACE(atp.ParamValue,' ',''),','+@Email,'') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND REPLACE(atp.ParamValue,' ','') LIKE '%,'+ @Email

		/* Match exactly this email address */
		SELECT 'Match' AS [Exact Match], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue,REPLACE(REPLACE(atp.ParamValue,' ',''),@Email,'') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND REPLACE(atp.ParamValue,' ','') = @Email

		/* Check for double commas */
		SELECT ',,' AS [Double comma], a.ClientID, a.TaskID, a.TaskName, a.Enabled, atp.ParamName, atp.ParamValue,REPLACE(atp.ParamValue,',,',',')
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE '%,,%'
		
		/* 
			Check for tasks that have daft sets of params.
			This shouldn't show up any results.
		*/
		/* Check for CC where TO is blank */
		SELECT 'CC but no TO' AS [CC but no TO], * 
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		INNER JOIN AutomatedTaskParam atpcc WITH (NOLOCK) ON atpcc.TaskID = a.TaskID 
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND atp.ParamName = 'EMAIL_TO'  
		AND atp.ParamValue = ''
		AND atpcc.ParamName = 'EMAIL_CC' 
		AND atpcc.ParamValue <> ''
		AND a.Enabled = 1
						
		/* Check for CC = TO */
		SELECT 'CC = TO' AS [CC same as TO], * 
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		INNER JOIN AutomatedTaskParam atpcc WITH (NOLOCK) ON atpcc.TaskID = a.TaskID 
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND atp.ParamName = 'EMAIL_TO'  
		AND atpcc.ParamName = 'EMAIL_CC' 
		AND atp.ParamValue = atpcc.ParamValue
		AND a.Enabled = 1
		
		/* Check for CC blank, TO blank, SEND_RESULTS true/yes */
		SELECT 'No recipients' AS [No recipients], * 
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		INNER JOIN AutomatedTaskParam atpcc WITH (NOLOCK) ON atpcc.TaskID = a.TaskID 
		INNER JOIN AutomatedTaskParam atpSend WITH (NOLOCK) ON atpSend.TaskID = a.TaskID 
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND atp.ParamName = 'EMAIL_TO'  
		AND atp.ParamValue = ''
		AND atpcc.ParamName = 'EMAIL_CC' 
		AND atpcc.ParamValue = ''
		AND atpSend.ParamName = 'SEND_RESULTS' 
		AND atpSend.ParamValue <> 'No'
		AND a.Enabled = 1
		
	END
	ELSE
	BEGIN
	
		/* Remove all spaces in email addresses to make matching more accurate */
		UPDATE AutomatedTaskParam
		SET ParamValue = REPLACE(atp.ParamValue,' ','') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE '%'+@Email+'%'

		/* Params that contain this email address with leading and trailing commas. Replace with a single comma. */
		/* ,test, */
		UPDATE AutomatedTaskParam
		SET ParamValue = REPLACE(atp.ParamValue,','+@Email+',',',') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE '%,'+@Email+',%'

		/* Params that start with this email address and then a comma. Remove both. */
		/* test, */
		UPDATE AutomatedTaskParam
		SET ParamValue = REPLACE(atp.ParamValue,@Email + ',','') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE @Email + ',%'

		/* Params that end with a comma and then this email address. Remove both. */
		/* test, */
		UPDATE AutomatedTaskParam
		SET ParamValue = REPLACE(atp.ParamValue,','+@Email,'') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE '%,'+ @Email

		/* Params that are exactly equal to this email address. Remove. */
		/* test */
		UPDATE AutomatedTaskParam
		SET ParamValue = REPLACE(atp.ParamValue,@Email,'') FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue = @Email

		/* Params that now contain double commas. Replace with a single comma. */
		/* ,, */
		UPDATE AutomatedTaskParam
		SET ParamValue = REPLACE(atp.ParamValue,',,',',')
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND (atp.ParamName LIKE 'EMAIL_TO' OR atp.ParamName LIKE 'EMAIL_%CC' OR atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		AND atp.ParamValue LIKE '%,,%'
		
		/* 
			Check for tasks that now have daft sets of params and fix them.
		*/
		/* Check for CC where TO is blank and put CC value in the TO param as well */
		UPDATE atp
		SET atp.ParamValue = atpcc.ParamValue
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		INNER JOIN AutomatedTaskParam atpcc WITH (NOLOCK) ON atpcc.TaskID = a.TaskID 
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND atp.ParamName = 'EMAIL_TO'  
		AND atp.ParamValue = ''
		AND atpcc.ParamName = 'EMAIL_CC' 
		AND atpcc.ParamValue <> ''
		AND a.Enabled = 1
						
		/* Check for CC = TO and remove CC if they are a perfect match */
		UPDATE atpcc
		SET atpcc.ParamValue = ''
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		INNER JOIN AutomatedTaskParam atpcc WITH (NOLOCK) ON atpcc.TaskID = a.TaskID 
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND atp.ParamName = 'EMAIL_TO'  
		AND atpcc.ParamName = 'EMAIL_CC' 
		AND atp.ParamValue = atpcc.ParamValue
		AND a.Enabled = 1
		
		/* Check for CC blank, TO blank, SEND_RESULTS true/yes and stop sending the results */
		UPDATE atpSend
		SET atpSend.ParamValue = 'No'
		FROM AutomatedTask a WITH (NOLOCK) 
		INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
		INNER JOIN AutomatedTaskParam atpcc WITH (NOLOCK) ON atpcc.TaskID = a.TaskID 
		INNER JOIN AutomatedTaskParam atpSend WITH (NOLOCK) ON atpSend.TaskID = a.TaskID 
		AND (@ClientIDList IS NULL OR a.ClientID IN (SELECT ClientID FROM @ClientIDs))
		AND atp.ParamName = 'EMAIL_TO'  
		AND atp.ParamValue = ''
		AND atpcc.ParamName = 'EMAIL_CC' 
		AND atpcc.ParamValue = ''
		AND atpSend.ParamName = 'SEND_RESULTS' 
		AND atpSend.ParamValue <> 'No'
		AND a.Enabled = 1
		
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[EmailRemoval] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EmailRemoval] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EmailRemoval] TO [sp_executeall]
GO
