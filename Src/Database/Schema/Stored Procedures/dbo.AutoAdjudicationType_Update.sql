SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AutoAdjudicationType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_Update]
(

	@AutoAdjudicationTypeID int   ,

	@AutoAdjudicationTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AutoAdjudicationType]
				SET
					[AutoAdjudicationTypeName] = @AutoAdjudicationTypeName
				WHERE
[AutoAdjudicationTypeID] = @AutoAdjudicationTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Update] TO [sp_executeall]
GO
