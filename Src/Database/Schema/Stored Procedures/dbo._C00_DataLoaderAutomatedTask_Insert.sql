SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-09-03
-- Description:	Inserts into the DataloaderAutomatedTask Table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DataLoaderAutomatedTask_Insert] 
	@ClientID INT,
	@DataloaderMapID INT,
	@AutomatedTaskID INT,
	@DelaySeconds INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO DataLoaderAutomatedTask (ClientID,DataloaderMapID,AutomatedTaskID,DelaySeconds)
	VALUES (@ClientID,@DataloaderMapID,@AutomatedTaskID,@DelaySeconds)
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DataLoaderAutomatedTask_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DataLoaderAutomatedTask_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DataLoaderAutomatedTask_Insert] TO [sp_executeall]
GO
