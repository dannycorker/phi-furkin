SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ResourceListDetailValues table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_GetByResourceListID]
(

	@ResourceListID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ResourceListDetailValueID],
					[ResourceListID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime]
				FROM
					[dbo].[ResourceListDetailValues] WITH (NOLOCK) 
				WHERE
					[ResourceListID] = @ResourceListID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_GetByResourceListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_GetByResourceListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_GetByResourceListID] TO [sp_executeall]
GO
