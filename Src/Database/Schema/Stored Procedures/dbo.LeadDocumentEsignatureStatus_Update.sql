SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadDocumentEsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_Update]
(

	@LeadDocumentEsignatureStatusID int   ,

	@ClientID int   ,

	@LeadDocumentID int   ,

	@ElectronicSignatureDocumentKey varchar (50)  ,

	@DocumentName varchar (255)  ,

	@SentTo varchar (255)  ,

	@DateSent datetime   ,

	@EsignatureStatusID int   ,

	@StatusCheckedDate datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadDocumentEsignatureStatus]
				SET
					[ClientID] = @ClientID
					,[LeadDocumentID] = @LeadDocumentID
					,[ElectronicSignatureDocumentKey] = @ElectronicSignatureDocumentKey
					,[DocumentName] = @DocumentName
					,[SentTo] = @SentTo
					,[DateSent] = @DateSent
					,[EsignatureStatusID] = @EsignatureStatusID
					,[StatusCheckedDate] = @StatusCheckedDate
				WHERE
[LeadDocumentEsignatureStatusID] = @LeadDocumentEsignatureStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Update] TO [sp_executeall]
GO
