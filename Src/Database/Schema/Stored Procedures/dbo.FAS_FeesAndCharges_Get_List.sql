SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FAS_FeesAndCharges table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_FeesAndCharges_Get_List]

AS


				
				SELECT
					[FasFeesAndChargesID],
					[FasCheckerDataID],
					[Description],
					[Amount],
					[WhenPaid]
				FROM
					[dbo].[FAS_FeesAndCharges] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_FeesAndCharges_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Get_List] TO [sp_executeall]
GO
