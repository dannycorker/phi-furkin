SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-06-24
-- Description:	Created in absence of DAL proc
-- =============================================
CREATE PROCEDURE [dbo].[StandardFieldOption_Insert]
	@ClientID int,
	@TableName varchar(250),
	@ColumnName varchar(250),
	@ColumnCaption varchar(250),
	@Mandatory bit
           
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [StandardFieldOption]
           ([ClientID]
           ,[TableName]
           ,[ColumnName]
           ,[ColumnCaption]
           ,[Mandatory])
     VALUES
           (@ClientID,
           @TableName,
           @ColumnName,
           @ColumnCaption,
           @Mandatory)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[StandardFieldOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StandardFieldOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StandardFieldOption_Insert] TO [sp_executeall]
GO
