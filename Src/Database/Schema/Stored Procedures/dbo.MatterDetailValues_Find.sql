SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the MatterDetailValues table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_Find]
(

	@SearchUsingOR BIT   = NULL ,

	@MatterDetailValueID INT   = NULL ,

	@ClientID INT   = NULL ,

	@LeadID INT   = NULL ,

	@MatterID INT   = NULL ,

	@DetailFieldID INT   = NULL ,

	@DetailValue VARCHAR (2000)  = NULL ,

	@ErrorMsg VARCHAR (1000)  = NULL ,

	@OriginalDetailValueID INT   = NULL ,

	@OriginalLeadID INT   = NULL ,

	@EncryptedValue VARCHAR (3000)  = NULL ,

	@ValueInt INT   = NULL ,

	@ValueMoney MONEY   = NULL ,

	@ValueDate DATE   = NULL ,

	@ValueDateTime DATETIME2   = NULL, 

	@SourceID INT   = NULL 
)
AS
BEGIN

	
	IF ISNULL(@SearchUsingOR, 0) <> 1
	BEGIN
		IF (@ClientID IS NOT NULL AND @LeadID IS NOT NULL AND @MatterID IS NOT NULL AND @DetailFieldID IS NOT NULL)
		BEGIN
			SELECT
				[MatterDetailValueID]
				, [ClientID]
				, [LeadID]
				, [MatterID]
				, [DetailFieldID]
				, [DetailValue]
				, [ErrorMsg]
				, [OriginalDetailValueID]
				, [OriginalLeadID]
				, [EncryptedValue]
				, [ValueInt]
				, [ValueMoney]
				, [ValueDate]
				, [ValueDateTime]
				, [SourceID]
			FROM
				[dbo].[MatterDetailValues] (NOLOCK) 
			WHERE 
				([ClientID] = @ClientID)
				AND ([LeadID] = @LeadID)
				AND ([MatterID] = @MatterID)
				AND ([DetailFieldID] = @DetailFieldID)
			
		END
		ELSE
		BEGIN
			SELECT
				[MatterDetailValueID]
				, [ClientID]
				, [LeadID]
				, [MatterID]
				, [DetailFieldID]
				, [DetailValue]
				, [ErrorMsg]
				, [OriginalDetailValueID]
				, [OriginalLeadID]
				, [EncryptedValue]
				, [ValueInt]
				, [ValueMoney]
				, [ValueDate]
				, [ValueDateTime]
				, [SourceID]
			FROM
				[dbo].[MatterDetailValues] (NOLOCK) 
			WHERE 
				([MatterDetailValueID] = @MatterDetailValueID OR @MatterDetailValueID IS NULL)
				AND ([ClientID] = @ClientID OR @ClientID IS NULL)
				AND ([LeadID] = @LeadID OR @LeadID IS NULL)
				AND ([MatterID] = @MatterID OR @MatterID IS NULL)
				AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
				AND ([DetailValue] = @DetailValue OR @DetailValue IS NULL)
				--AND ([ErrorMsg] = @ErrorMsg OR @ErrorMsg is null)
				--AND ([OriginalDetailValueID] = @OriginalDetailValueID OR @OriginalDetailValueID is null)
				--AND ([OriginalLeadID] = @OriginalLeadID OR @OriginalLeadID is null)
				--AND ([EncryptedValue] = @EncryptedValue OR @EncryptedValue is null)
				--AND ([ValueInt] = @ValueInt OR @ValueInt is null)
				--AND ([ValueMoney] = @ValueMoney OR @ValueMoney is null)
				--AND ([ValueDate] = @ValueDate OR @ValueDate is null)
				--AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime is null)
		END					
	END
	ELSE
	BEGIN
		SELECT
			[MatterDetailValueID]
			, [ClientID]
			, [LeadID]
			, [MatterID]
			, [DetailFieldID]
			, [DetailValue]
			, [ErrorMsg]
			, [OriginalDetailValueID]
			, [OriginalLeadID]
			, [EncryptedValue]
			, [ValueInt]
			, [ValueMoney]
			, [ValueDate]
			, [ValueDateTime]
			, [SourceID]
		FROM
			[dbo].[MatterDetailValues] (NOLOCK) 
		WHERE 
			([MatterDetailValueID] = @MatterDetailValueID AND @MatterDetailValueID IS NOT NULL)
			OR ([ClientID] = @ClientID AND @ClientID IS NOT NULL)
			OR ([LeadID] = @LeadID AND @LeadID IS NOT NULL)
			OR ([MatterID] = @MatterID AND @MatterID IS NOT NULL)
			OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID IS NOT NULL)
			OR ([DetailValue] = @DetailValue AND @DetailValue IS NOT NULL)
			--OR ([ErrorMsg] = @ErrorMsg AND @ErrorMsg is not null)
			--OR ([OriginalDetailValueID] = @OriginalDetailValueID AND @OriginalDetailValueID is not null)
			--OR ([OriginalLeadID] = @OriginalLeadID AND @OriginalLeadID is not null)
			--OR ([EncryptedValue] = @EncryptedValue AND @EncryptedValue is not null)
			--OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
			--OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
			--OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
			--OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	END
	
	SELECT @@ROWCOUNT
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Find] TO [sp_executeall]
GO
