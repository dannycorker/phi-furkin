SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectDetailValues_GetByDetailFieldID]
(

	@DetailFieldID int   
)
AS


				SELECT
					[ObjectDetailValueID],
					[ClientID],
					[SubClientID],
					[ObjectID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime]
				FROM
					[dbo].[ObjectDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectDetailValues_GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_GetByDetailFieldID] TO [sp_executeall]
GO
