SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 21-05-20130
-- Description:	Gets the sage pay gateway vendor name for the given
-- LeadTypeID and ClientID
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__GetVendorName]
	@ClientID INT,
	@LeadTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TableDetailFieldID INT
	DECLARE @VendorLeadTypeIDColumnDetailFieldID INT
	DECLARE @VendorNameColumnDetailFieldID INT

	SELECT @TableDetailFieldID=DetailFieldID, @VendorLeadTypeIDColumnDetailFieldID=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=499

	SELECT @VendorNameColumnDetailFieldID=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=500

	SELECT 
			tdvVendorLeadTypeID.TableRowID, tdvVendorLeadTypeID.DetailValue VendorLeadTypeID,  
			tdvVendorName.DetailValue VendorName
	FROM TableDetailValues tdvVendorLeadTypeID WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdvVendorName WITH (NOLOCK) ON 
				tdvVendorName.TableRowID = tdvVendorLeadTypeID.TableRowID AND 
				tdvVendorName.DetailFieldID=@VendorNameColumnDetailFieldID
	WHERE 
			tdvVendorLeadTypeID.DetailFieldID=@VendorLeadTypeIDColumnDetailFieldID AND 
			tdvVendorLeadTypeID.ClientID=@ClientID AND
			tdvVendorLeadTypeID.ValueInt = @LeadTypeID
    
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetVendorName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__GetVendorName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetVendorName] TO [sp_executeall]
GO
