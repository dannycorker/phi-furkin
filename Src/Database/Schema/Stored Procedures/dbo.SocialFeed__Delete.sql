SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 09-11-2015
-- Description:	Deletes a social feed record
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__Delete]
	@ClientID INT,
	@SocialFeedID INT

AS
BEGIN
		
	SET NOCOUNT ON;

    DELETE FROM SocialFeed
    WHERE SocialFeedID=@SocialFeedID AND ClientID=@ClientID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__Delete] TO [sp_executeall]
GO
