SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the WorkflowTask table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTask_Find]
(

	@SearchUsingOR bit   = null ,

	@WorkflowTaskID int   = null ,

	@WorkflowGroupID int   = null ,

	@AutomatedTaskID int   = null ,

	@Priority int   = null ,

	@AssignedTo int   = null ,

	@AssignedDate datetime   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@EventTypeID int   = null ,

	@ClientID int   = null ,

	@FollowUp bit   = null ,

	@Important bit   = null ,

	@CreationDate datetime   = null ,

	@Escalated bit   = null ,

	@EscalatedBy int   = null ,

	@EscalationReason varchar (255)  = null ,

	@EscalationDate datetime   = null ,

	@Disabled bit   = null ,

	@DisabledReason varchar (255)  = null ,

	@DisabledDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [WorkflowTaskID]
	, [WorkflowGroupID]
	, [AutomatedTaskID]
	, [Priority]
	, [AssignedTo]
	, [AssignedDate]
	, [LeadID]
	, [CaseID]
	, [EventTypeID]
	, [ClientID]
	, [FollowUp]
	, [Important]
	, [CreationDate]
	, [Escalated]
	, [EscalatedBy]
	, [EscalationReason]
	, [EscalationDate]
	, [Disabled]
	, [DisabledReason]
	, [DisabledDate]
    FROM
	[dbo].[WorkflowTask] WITH (NOLOCK) 
    WHERE 
	 ([WorkflowTaskID] = @WorkflowTaskID OR @WorkflowTaskID IS NULL)
	AND ([WorkflowGroupID] = @WorkflowGroupID OR @WorkflowGroupID IS NULL)
	AND ([AutomatedTaskID] = @AutomatedTaskID OR @AutomatedTaskID IS NULL)
	AND ([Priority] = @Priority OR @Priority IS NULL)
	AND ([AssignedTo] = @AssignedTo OR @AssignedTo IS NULL)
	AND ([AssignedDate] = @AssignedDate OR @AssignedDate IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FollowUp] = @FollowUp OR @FollowUp IS NULL)
	AND ([Important] = @Important OR @Important IS NULL)
	AND ([CreationDate] = @CreationDate OR @CreationDate IS NULL)
	AND ([Escalated] = @Escalated OR @Escalated IS NULL)
	AND ([EscalatedBy] = @EscalatedBy OR @EscalatedBy IS NULL)
	AND ([EscalationReason] = @EscalationReason OR @EscalationReason IS NULL)
	AND ([EscalationDate] = @EscalationDate OR @EscalationDate IS NULL)
	AND ([Disabled] = @Disabled OR @Disabled IS NULL)
	AND ([DisabledReason] = @DisabledReason OR @DisabledReason IS NULL)
	AND ([DisabledDate] = @DisabledDate OR @DisabledDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [WorkflowTaskID]
	, [WorkflowGroupID]
	, [AutomatedTaskID]
	, [Priority]
	, [AssignedTo]
	, [AssignedDate]
	, [LeadID]
	, [CaseID]
	, [EventTypeID]
	, [ClientID]
	, [FollowUp]
	, [Important]
	, [CreationDate]
	, [Escalated]
	, [EscalatedBy]
	, [EscalationReason]
	, [EscalationDate]
	, [Disabled]
	, [DisabledReason]
	, [DisabledDate]
    FROM
	[dbo].[WorkflowTask] WITH (NOLOCK) 
    WHERE 
	 ([WorkflowTaskID] = @WorkflowTaskID AND @WorkflowTaskID is not null)
	OR ([WorkflowGroupID] = @WorkflowGroupID AND @WorkflowGroupID is not null)
	OR ([AutomatedTaskID] = @AutomatedTaskID AND @AutomatedTaskID is not null)
	OR ([Priority] = @Priority AND @Priority is not null)
	OR ([AssignedTo] = @AssignedTo AND @AssignedTo is not null)
	OR ([AssignedDate] = @AssignedDate AND @AssignedDate is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FollowUp] = @FollowUp AND @FollowUp is not null)
	OR ([Important] = @Important AND @Important is not null)
	OR ([CreationDate] = @CreationDate AND @CreationDate is not null)
	OR ([Escalated] = @Escalated AND @Escalated is not null)
	OR ([EscalatedBy] = @EscalatedBy AND @EscalatedBy is not null)
	OR ([EscalationReason] = @EscalationReason AND @EscalationReason is not null)
	OR ([EscalationDate] = @EscalationDate AND @EscalationDate is not null)
	OR ([Disabled] = @Disabled AND @Disabled is not null)
	OR ([DisabledReason] = @DisabledReason AND @DisabledReason is not null)
	OR ([DisabledDate] = @DisabledDate AND @DisabledDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Find] TO [sp_executeall]
GO
