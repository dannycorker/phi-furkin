SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryCriteria table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteria_GetBySqlQueryCriteriaID]
(

	@SqlQueryCriteriaID int   
)
AS


				SELECT
					[SqlQueryCriteriaID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryTableID],
					[SqlQueryColumnID],
					[CriteriaText],
					[Criteria1],
					[Criteria2],
					[CriteriaName],
					[SubQueryID],
					[SubQueryLinkType],
					[ParamValue],
					[TempTableID],
					[TempColumnID],
					[TempCriteriaID],
					[IsSecurityClause],
					[CriteriaSubstitutions],
					[IsParameterizable],
					[Comparison],
					[IsJoinClause],
					[SourceID]
				FROM
					[dbo].[SqlQueryCriteria] WITH (NOLOCK) 
				WHERE
										[SqlQueryCriteriaID] = @SqlQueryCriteriaID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_GetBySqlQueryCriteriaID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteria_GetBySqlQueryCriteriaID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_GetBySqlQueryCriteriaID] TO [sp_executeall]
GO
