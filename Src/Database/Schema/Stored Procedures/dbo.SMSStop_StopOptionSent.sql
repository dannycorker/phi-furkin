SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-09
-- Description:	Log SMS opt out offer has been sent to a mobile number
-- =============================================

CREATE PROCEDURE [dbo].[SMSStop_StopOptionSent]
(
	  @LeadID		int
	, @MobileNumber	varchar(50)	
)

AS
BEGIN
	SET NOCOUNT ON;

	
/* 	Client wide opt out		= any leadtype on that client will not be able to send an SMS to this mobile
	Lead Type specific opt  = only this leadtype will not be able to send an SMS to this mobile				*/

	DECLARE @isClientWideOptOut	bit = 1
	, @LeadTypeID			int
	, @CustomerID			int
	, @ClientID				int
	, @StopSMS				bit = 0 
	, @OptOutRequestSent	bit = 1 
	, @SMSStopID			int
	
	SELECT @LeadTypeID	= l.LeadTypeID
	, @CustomerID		= l.CustomerID
	, @ClientID			= l.ClientID
	FROM Lead l with(nolock) 
	WHERE l.LeadID = @LeadID	

	IF @isClientWideOptOut = 1 
	BEGIN

		SELECT @SMSStopID = s.SMSStopID
		FROM SMSStop s with(nolock)
		WHERE s.MobileNumber = @MobileNumber
		and s.ClientID = @ClientID

	END
	ELSE IF @isClientWideOptOut = 0  /* LeadType specific opt out */
	BEGIN

		SELECT @SMSStopID = s.SMSStopID
		FROM SMSStop s with(nolock)
		WHERE s.MobileNumber = @MobileNumber
		and s.ClientID = @ClientID
		and s.LeadTypeID = @LeadTypeID	

	END

	IF @SMSStopID > 0 /* Have already sent an opt out */
	BEGIN
	
		UPDATE TOP (1) s
		SET s.Updated = dbo.fn_GetDate_Local()
		, s.OptOutRequestSent = @OptOutRequestSent
		FROM SMSStop s 
		WHERE s.SMSStopID = @SMSStopID
	
	END
	ELSE 
	BEGIN /* First opt out offer */

		INSERT INTO SMSStop (ClientID, CustomerID, LeadTypeID, MobileNumber, StopSMS, OptOutRequestSent, Updated, Created)
		VALUES (@ClientID, @CustomerID, @LeadTypeID, @MobileNumber, @StopSMS, @OptOutRequestSent, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())

	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_StopOptionSent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStop_StopOptionSent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_StopOptionSent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_StopOptionSent] TO [sp_executehelper]
GO
