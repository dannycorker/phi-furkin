SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2016-01-12
-- Description:	For BACs fails we retry the payment either by cheque or Bacs. We do this by rejoining the process at the point payment method is picked,
--				but that means we miss the row creation (as obviously this is handled in about 5 different procs over several events, cheers Rob). So we now have a cheeky little proc to get this done
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CreatePaymentRowForRetry]
	
	@CaseID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
		
	DECLARE @EventTypeID INT = 0,
			@MatterID    INT

	SELECT top 1 @EventTypeID = le.EventTypeID, @MatterID = m.MatterID
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = le.CaseID 
	where le.CaseID = @CaseID 
	and le.EventTypeID IN (123460, 123461, 121863, 123462)
	ORDER BY le.LeadEventID desc 
		
	-- Update the payee and payee type field
	IF @EventTypeID IN (123460, 123461, 121863, 123462,157254)
	BEGIN
		
		DECLARE @PayToData dbo.tvpIntMoney
		INSERT @PayToData(ID, VALUE)
		SELECT	CASE @EventTypeID
					WHEN 123460 THEN 44233 -- Pay TP
					WHEN 121863 THEN 44232 -- Pay VT
					WHEN 123462 THEN 46135 -- Pay RV
					WHEN 157254 THEN 76367 -- Pay RV (Out of Hours) - /*SA - 2018-03-15*/
				END, SUM(ValueMoney) 
		FROM dbo.TableDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 144352
		
		-- Set Potential Fraud Indicator if paying third party
		IF @EventTypeID = 123461
		BEGIN
			EXEC _C00_SimpleValueIntoField 144494, 'true', @MatterID, 44412
		END
				
		EXEC dbo._C00_1272_Payments_SaveSapPaymentData_ForApproval  @MatterID, @PayToData, '0' 
		
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreatePaymentRowForRetry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CreatePaymentRowForRetry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreatePaymentRowForRetry] TO [sp_executeall]
GO
