SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-08-17
-- Description:	Look up a client-level table to return the appropriate config value for this database
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetDatabaseSpecificConfigValue]
(
	@ConfigDataName VARCHAR(2000) = ''
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientLevelConfigTableID	INT
	DECLARE @DbName						VARCHAR(2000) = DB_NAME()
	DECLARE @ReturnValue				VARCHAR(2000)

	SELECT TOP 1 @ReturnValue = tdvDatabase.DetailValue
	FROM TableRows tr WITH ( NOLOCK )
	INNER JOIN TableDetailValues tdvDataname WITH ( NOLOCK ) on tdvDataname.TableRowID = tr.TableRowID AND tdvDataname.DetailFieldID = 178653 /*Config Data Name*/
	INNER JOIN TableDetailValues tdvDatabase WITH ( NOLOCK ) on tdvDatabase.TableRowID = tr.TableRowID AND tdvDatabase.DetailFieldID <> 178653 /*Config Data Name*/
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdvDatabase.DetailFieldID 
	WHERE tr.DetailFieldID = @ClientLevelConfigTableID
	AND df.FieldCaption = @DbName

	RETURN @ReturnValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetDatabaseSpecificConfigValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetDatabaseSpecificConfigValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetDatabaseSpecificConfigValue] TO [sp_executeall]
GO
