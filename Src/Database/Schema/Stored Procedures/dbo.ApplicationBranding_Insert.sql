SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ApplicationBranding table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ApplicationBranding_Insert]
(

	@ApplicationBrandingID int    OUTPUT,

	@ClientID int   ,

	@FileName varchar (50)  ,

	@VerticalSize int   ,

	@HorizontalSize int   ,

	@LeadManagerAlias varchar (50)  ,

	@LeadAlias varchar (50)  ,

	@LeadDetailsTabImageFileName varchar (255)  
)
AS


				
				INSERT INTO [dbo].[ApplicationBranding]
					(
					[ClientID]
					,[FileName]
					,[VerticalSize]
					,[HorizontalSize]
					,[LeadManagerAlias]
					,[LeadAlias]
					,[LeadDetailsTabImageFileName]
					)
				VALUES
					(
					@ClientID
					,@FileName
					,@VerticalSize
					,@HorizontalSize
					,@LeadManagerAlias
					,@LeadAlias
					,@LeadDetailsTabImageFileName
					)
				-- Get the identity value
				SET @ApplicationBrandingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ApplicationBranding_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Insert] TO [sp_executeall]
GO
