SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnelFavourite table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite_Delete]
(

	@ClientPersonnelFavouriteID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnelFavourite] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelFavouriteID] = @ClientPersonnelFavouriteID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Delete] TO [sp_executeall]
GO
