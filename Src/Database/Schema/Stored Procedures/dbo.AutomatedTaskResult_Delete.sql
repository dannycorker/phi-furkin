SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AutomatedTaskResult table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_Delete]
(

	@AutomatedTaskResultID int   
)
AS


				DELETE FROM [dbo].[AutomatedTaskResult] WITH (ROWLOCK) 
				WHERE
					[AutomatedTaskResultID] = @AutomatedTaskResultID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Delete] TO [sp_executeall]
GO
