SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_Delete]
(

	@ThirdPartyFieldMappingID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyFieldMapping] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyFieldMappingID] = @ThirdPartyFieldMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Delete] TO [sp_executeall]
GO
