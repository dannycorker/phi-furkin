SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Patch table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Patch_Delete]
(

	@PatchID int   
)
AS


				DELETE FROM [dbo].[Patch] WITH (ROWLOCK) 
				WHERE
					[PatchID] = @PatchID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Patch_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Delete] TO [sp_executeall]
GO
