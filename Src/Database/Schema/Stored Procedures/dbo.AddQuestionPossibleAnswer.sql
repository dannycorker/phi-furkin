SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Add QuestionPossibleAnswer from eCatcher
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[AddQuestionPossibleAnswer]
	@ClientQuestionnaireID INT,
	@MasterQuestionID INT,
	@AnswerText TEXT,
	@Branch INT, 
	@ClientID INT, 
	@WhoEdited INT  

AS
BEGIN

	INSERT INTO QuestionPossibleAnswers (ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch, ClientID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@ClientQuestionnaireID, @MasterQuestionID, @AnswerText, @Branch, @ClientID, @WhoEdited, dbo.fn_GetDate_Local(), @WhoEdited, dbo.fn_GetDate_Local())

	DECLARE @QuestionPossibleAnswerID INT

	SET @QuestionPossibleAnswerID = SCOPE_IDENTITY()

	SELECT QuestionPossibleAnswerID
	FROM QuestionPossibleAnswers
	WHERE QuestionPossibleAnswerID = @QuestionPossibleAnswerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AddQuestionPossibleAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddQuestionPossibleAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddQuestionPossibleAnswer] TO [sp_executeall]
GO
