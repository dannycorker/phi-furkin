SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 23/06/2016
-- Description:	Payment__Insert
-- =============================================
CREATE PROCEDURE [dbo].[Payment__Insert]
	@ClientID INT, 
	@CustomerID INT, 
	@PaymentDateTime DATETIME, 
	@PaymentTypeID INT, 
	@DateReceived DATETIME, 
	@PaymentDescription VARCHAR(2000), 
	@PaymentReference VARCHAR(2000), 
	@PaymentNet NUMERIC(18,2) = NULL, 
	@PaymentVAT NUMERIC(18,2) = NULL, 
	@PaymentGross NUMERIC(18,2), 
	@PaymentCurrency INT = NULL, 
	@RelatedOrderRef VARCHAR(2000) = NULL, 
	@RelatedRequestDescription VARCHAR(2000) = NULL, 
	@PayeeFullName VARCHAR(2000) = NULL, 
	@MaskedAccountNumber VARCHAR(2000) = NULL, 
	@AccountTypeDescription VARCHAR(2000) = NULL, 
	@PaymentStatusID INT = NULL,  
	@DateReconciled DATETIME = NULL, 
	@ReconciliationOutcome VARCHAR(2000) = NULL, 
	@ObjectID INT = NULL, 
	@ObjectTypeID INT = NULL, 
	@Comments VARCHAR(2000) = NULL, 
	@PaymentFileName VARCHAR(200) = NULL, 
	@FailureCode VARCHAR(50) = NULL, 
	@FailureReason VARCHAR(500) = NULL, 
	@WhoCreated INT, 
	@WhenCreated DATETIME = NULL, 
	@WhoModified INT = NULL, 
	@WhenModified DATETIME = NULL
AS
BEGIN

	SET NOCOUNT ON;

	IF @WhenCreated IS NULL
	BEGIN
		SELECT @WhenCreated = dbo.fn_GetDate_Local()
	END
	
	IF @WhoModified IS NULL
	BEGIN
		SELECT @WhoModified = @WhoCreated
	END
	
	IF @WhenModified IS NULL
	BEGIN
		SELECT @WhenModified = dbo.fn_GetDate_Local()
	END
	
	INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@ClientID, @CustomerID, @PaymentDateTime, @PaymentTypeID, @DateReceived, @PaymentDescription, @PaymentReference, @PaymentNet, @PaymentVAT, @PaymentGross, @PaymentCurrency, @RelatedOrderRef, @RelatedRequestDescription, @PayeeFullName, @MaskedAccountNumber, @AccountTypeDescription, @PaymentStatusID, @DateReconciled, @ReconciliationOutcome, @ObjectID, @ObjectTypeID, @Comments, @PaymentFileName, @FailureCode, @FailureReason, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Payment__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__Insert] TO [sp_executeall]
GO
