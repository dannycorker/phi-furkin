SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-10-26
-- Description:	Add to async queue
-- =============================================
CREATE PROCEDURE [dbo].[AsyncQueue__Insert]
	@ClientID INT, 
	@WhenCreated DATETIME, 
	@QueueTypeID INT, 
	@Status INT, 
	@Payload VARCHAR(MAX), 
	@Outcome VARCHAR(MAX), 
	@WhenCompleted DATETIME = NULL
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO AsyncQueue (ClientID, WhenCreated, QueueTypeID, Status, Payload, Outcome, WhenCompleted)
	VALUES (@ClientID, @WhenCreated, @QueueTypeID, @Status, @Payload, @Outcome, @WhenCompleted)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AsyncQueue__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AsyncQueue__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AsyncQueue__Insert] TO [sp_executeall]
GO
