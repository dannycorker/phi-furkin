SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-11-08
-- Description:	Decode the MatterListDisplay for a Client
-- =============================================
CREATE PROCEDURE [dbo].[mld]
(
	@ClientID int
)
	
AS
BEGIN

	SET NOCOUNT ON

	SELECT	mld.MatterListDisplayID,
			lt.LeadTypeID, 
			lt.LeadTypeName, 
			mld.MatterRef, 
			mld.MatterStatus, 
			mld.MatterID, 
			mld.Field1,
			df1.FieldName,
			mld.Field1ColumnDetailFieldID [Column1],
			cf1.FieldName,
			mld.Field2,
			df2.FieldName,
			mld.Field1ColumnDetailFieldID [Column2],
			cf2.FieldName,
			mld.Field3,
			df3.FieldName,
			mld.Field1ColumnDetailFieldID [Column3],
			cf3.FieldName,
			mld.Field4,
			df4.FieldName,
			mld.Field1ColumnDetailFieldID [Column4],
			cf4.FieldName
	FROM MatterListDisplay mld WITH (NOLOCK) 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) on lt.LeadTypeID = mld.LeadTypeID
	LEFT JOIN DetailFields df1 WITH (NOLOCK) on df1.DetailFieldID = mld.Field1
	LEFT JOIN DetailFields cf1 WITH (NOLOCK) on cf1.DetailFieldID = mld.Field1ColumnDetailFieldID
	LEFT JOIN DetailFields df2 WITH (NOLOCK) on df2.DetailFieldID = mld.Field2
	LEFT JOIN DetailFields cf2 WITH (NOLOCK) on cf2.DetailFieldID = mld.Field2ColumnDetailFieldID
	LEFT JOIN DetailFields df3 WITH (NOLOCK) on df3.DetailFieldID = mld.Field3
	LEFT JOIN DetailFields cf3 WITH (NOLOCK) on cf3.DetailFieldID = mld.Field3ColumnDetailFieldID
	LEFT JOIN DetailFields df4 WITH (NOLOCK) on df4.DetailFieldID = mld.Field4
	LEFT JOIN DetailFields cf4 WITH (NOLOCK) on cf4.DetailFieldID = mld.Field4ColumnDetailFieldID
	WHERE mld.ClientID = @ClientID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[mld] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[mld] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[mld] TO [sp_executeall]
GO
