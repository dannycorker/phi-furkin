SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_GetByCaseID]
(

	@CaseID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CaseTransferMappingID],
					[ClientRelationshipID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID],
					[NewClientID],
					[NewCustomerID],
					[NewLeadID],
					[NewCaseID],
					[NewMatterID],
					[CaseTransferStatusID],
					[NewLeadTypeID]
				FROM
					[dbo].[CaseTransferMapping] WITH (NOLOCK) 
				WHERE
					[CaseID] = @CaseID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_GetByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_GetByCaseID] TO [sp_executeall]
GO
