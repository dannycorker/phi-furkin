SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-10-2014
-- Description:	Returns a rule based upon the rule set id and the client id
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Rules_GetRulesByRuleSetIDAndClientID]
(
	@ClientID INT,
	@RuleSetID INT	
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * FROM RulesEngine_Rules r WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND RuleSetID=@RuleSetID
	ORDER BY r.RuleOrder
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_GetRulesByRuleSetIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Rules_GetRulesByRuleSetIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_GetRulesByRuleSetIDAndClientID] TO [sp_executeall]
GO
