SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Messages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_Insert]
(

	@MessageID int    OUTPUT,

	@ClientPersonnelIDTo int   ,

	@ClientPersonnelIDFrom int   ,

	@DateSent datetime   ,

	@DateRead datetime   ,

	@Status int   ,

	@Subject varchar (255)  ,

	@MessageText varchar (1000)  ,

	@DateReplied datetime   ,

	@DateForwarded datetime   ,

	@PreviousMessageID int   ,

	@ClientID int   ,

	@PortalUserIDTo int   ,

	@PortalUserIDFrom int   
)
AS


				
				INSERT INTO [dbo].[Messages]
					(
					[ClientPersonnelIDTo]
					,[ClientPersonnelIDFrom]
					,[DateSent]
					,[DateRead]
					,[Status]
					,[Subject]
					,[MessageText]
					,[DateReplied]
					,[DateForwarded]
					,[PreviousMessageID]
					,[ClientID]
					,[PortalUserIDTo]
					,[PortalUserIDFrom]
					)
				VALUES
					(
					@ClientPersonnelIDTo
					,@ClientPersonnelIDFrom
					,@DateSent
					,@DateRead
					,@Status
					,@Subject
					,@MessageText
					,@DateReplied
					,@DateForwarded
					,@PreviousMessageID
					,@ClientID
					,@PortalUserIDTo
					,@PortalUserIDFrom
					)
				-- Get the identity value
				SET @MessageID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Insert] TO [sp_executeall]
GO
