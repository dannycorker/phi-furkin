SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeAutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAutomatedTask_Delete]
(

	@EventTypeAutomatedTaskID int   
)
AS


				DELETE FROM [dbo].[EventTypeAutomatedTask] WITH (ROWLOCK) 
				WHERE
					[EventTypeAutomatedTaskID] = @EventTypeAutomatedTaskID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Delete] TO [sp_executeall]
GO
