SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChartSeriesSetting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting_Update]
(

	@ChartSeriesSettingID int   ,

	@ChartSeriesID int   ,

	@SettingName varchar (100)  ,

	@SettingValue varchar (MAX)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChartSeriesSetting]
				SET
					[ChartSeriesID] = @ChartSeriesID
					,[SettingName] = @SettingName
					,[SettingValue] = @SettingValue
					,[ClientID] = @ClientID
				WHERE
[ChartSeriesSettingID] = @ChartSeriesSettingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Update] TO [sp_executeall]
GO
