SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Advance date to allow linked policy
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_155442]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@WaitingDays					INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


/* Advance date to allow linked policy */
	IF @EventTypeID IN (155442)
	BEGIN
	
		SELECT @WaitingDays=ValueInt FROM ClientDetailValues WITH (NOLOCK) WHERE ClientID=@ClientID AND DetailFieldID=170228
		IF dbo.fnGetDvAsDate(175608,@CaseID) < ( SELECT CAST(DATEADD(DAY,@WaitingDays,dbo.fn_GetDate_Local()) AS DATE) )
		BEGIN
		
			SELECT @ErrorMessage = ISNULL(@ErrorMessage,'') + '<br /><br /><font color="red">Advanced date is less than 10 days away.</font><br/><br/><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
		
		END
		ELSE
		BEGIN
		
			-- advance date next collection
			EXEC _C600_SetNextPaymentDate @MatterID, 7
			
			-- apply notification event
			EXEC _C600_AddAutomatedEvent @MatterID, 155435, -1, @AqAutomation
			
		END
		
	END	



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155442] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_155442] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155442] TO [sp_executeall]
GO
