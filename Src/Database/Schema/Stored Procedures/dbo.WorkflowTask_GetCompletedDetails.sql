SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-02-05
-- Description:	Show completed task details
-- =============================================
CREATE PROCEDURE [dbo].[WorkflowTask_GetCompletedDetails]

	@ClientPersonnelID int 

AS
BEGIN
	
	DECLARE @CompleteDay INT = 0,
			@CompleteHour INT = 0,
			@OutputText VARCHAR(200)

	SELECT @CompleteDay = COUNT(*)
	FROM WorkflowTaskCompleted w WITH (NOLOCK)
	WHERE w.CompletedBy = @ClientPersonnelID
	AND w.CompletedOn > CONVERT(DATE,dbo.fn_GetDate_Local())
	
	SELECT @CompleteHour = COUNT(*)
	FROM WorkflowTaskCompleted w WITH (NOLOCK)
	WHERE w.CompletedBy = @ClientPersonnelID
	AND w.CompletedOn > DATEADD(HH, -1, dbo.fn_GetDate_Local())

	SELECT @OutputText = 'Tasks Completed Today: ' + CONVERT(VARCHAR,@CompleteDay) + '<Br>Tasks Completed In The Hour: ' + CONVERT(VARCHAR,@CompleteHour) 
	
	SELECT @OutputText AS [OutputText]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetCompletedDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_GetCompletedDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetCompletedDetails] TO [sp_executeall]
GO
