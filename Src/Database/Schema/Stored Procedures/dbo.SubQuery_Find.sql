SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SubQuery table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQuery_Find]
(

	@SearchUsingOR bit   = null ,

	@SubQueryID int   = null ,

	@ClientID int   = null ,

	@SubQueryName varchar (50)  = null ,

	@SubQueryDesc varchar (500)  = null ,

	@SubQueryText varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SubQueryID]
	, [ClientID]
	, [SubQueryName]
	, [SubQueryDesc]
	, [SubQueryText]
    FROM
	[dbo].[SubQuery] WITH (NOLOCK) 
    WHERE 
	 ([SubQueryID] = @SubQueryID OR @SubQueryID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubQueryName] = @SubQueryName OR @SubQueryName IS NULL)
	AND ([SubQueryDesc] = @SubQueryDesc OR @SubQueryDesc IS NULL)
	AND ([SubQueryText] = @SubQueryText OR @SubQueryText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SubQueryID]
	, [ClientID]
	, [SubQueryName]
	, [SubQueryDesc]
	, [SubQueryText]
    FROM
	[dbo].[SubQuery] WITH (NOLOCK) 
    WHERE 
	 ([SubQueryID] = @SubQueryID AND @SubQueryID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubQueryName] = @SubQueryName AND @SubQueryName is not null)
	OR ([SubQueryDesc] = @SubQueryDesc AND @SubQueryDesc is not null)
	OR ([SubQueryText] = @SubQueryText AND @SubQueryText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Find] TO [sp_executeall]
GO
