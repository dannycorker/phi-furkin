SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-12-13
-- Description:	Get all the associated identitys for the given customer
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetAllIdentitys]
	
	@CustomerID INT,
	@ClientID INT

AS
BEGIN

	SELECT c.CustomerID, l.LeadID, l.LeadTypeID, 
			cases.CaseID, 
			cases.CaseNum, 
			m.MatterID, 
			m.RefLetter, 
			'Status' as [Col0Name],
			ISNULL(ls.StatusName, '') as [Col0Value],
			ISNULL(df1.FieldCaption,'') as [Col1Name], 
			ISNULL(dbo.fnGetMatterOrResValue (m.MatterID, mdl.Field1, mdl.Field2ColumnDetailFieldID), '') as [Col1Value],
			ISNULL(df2.FieldCaption,'') as [Col2Name], 
			ISNULL(dbo.fnGetMatterOrResValue (m.MatterID, mdl.Field2, mdl.Field2ColumnDetailFieldID), '') as [Col2Value],
			ISNULL(df3.FieldCaption,'') as [Col3Name], 
			ISNULL(dbo.fnGetMatterOrResValue (m.MatterID, mdl.Field3, mdl.Field3ColumnDetailFieldID), '') as [Col3Value],
			ISNULL(df4.FieldCaption,'') as [Col4Name],
			ISNULL(dbo.fnGetMatterOrResValue (m.MatterID, mdl.Field4, mdl.Field4ColumnDetailFieldID), '') as [Col4Value]
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	LEFT JOIN MatterListDisplay mdl WITH (NOLOCK) ON mdl.LeadTypeID = l.LeadTypeID
	LEFT JOIN DetailFields df1 WITH (NOLOCK) ON df1.DetailFieldID = COALESCE(mdl.Field1ColumnDetailFieldID, mdl.Field1)
	LEFT JOIN DetailFields df2 WITH (NOLOCK) ON df2.DetailFieldID = COALESCE(mdl.Field2ColumnDetailFieldID, mdl.Field2)
	LEFT JOIN DetailFields df3 WITH (NOLOCK) ON df3.DetailFieldID = COALESCE(mdl.Field3ColumnDetailFieldID, mdl.Field3)
	LEFT JOIN DetailFields df4 WITH (NOLOCK) ON df4.DetailFieldID = COALESCE(mdl.Field4ColumnDetailFieldID, mdl.Field4)
	INNER JOIN dbo.Cases cases WITH (NOLOCK) ON cases.LeadID = l.LeadID
	LEFT JOIN LeadStatus ls WITH (NOLOCK) ON ls.StatusID = cases.ClientStatusID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = cases.CaseID
	WHERE c.CustomerID=@CustomerID 
	AND c.ClientID=@ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetAllIdentitys] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetAllIdentitys] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetAllIdentitys] TO [sp_executeall]
GO
