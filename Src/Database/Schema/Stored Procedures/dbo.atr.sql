SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-09-29
-- Description:	List all AutomatedTaskResults for a task
--				CS 2012-10-17 added @IncludeZeroCounts
-- =============================================
CREATE PROCEDURE [dbo].[atr] 
	@TaskID INT = NULL, 
	@ClientID INT = NULL, 
	@MaxRows INT = 500,
	@IncludeZeroCounts BIT = 1
	--@AgeInDays int = 1, 
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @TaskID IS NULL AND @ClientID IS NOT NULL
	BEGIN
		SELECT TOP (@MaxRows) atr.*
		FROM AutomatedTaskResult atr WITH (NOLOCK) 
		WHERE atr.ClientID = @ClientID
		AND (@IncludeZeroCounts = 1 OR atr.RecordCount <> 0)
		ORDER BY atr.AutomatedTaskResultID DESC
	END
	ELSE
	BEGIN
		SELECT TOP (@MaxRows) atr.*
		FROM AutomatedTaskResult atr WITH (NOLOCK) 
		WHERE atr.TaskID = @TaskID
		AND (@IncludeZeroCounts = 1 OR atr.RecordCount <> 0)
		ORDER BY atr.AutomatedTaskResultID DESC
	END

	
	/*IF @ClientID IS NULL
	BEGIN
		SELECT TOP (@MaxRows) atr.* 
		FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
		WHERE atr.RunDate > dbo.fn_GetDate_Local() - @AgeInDays
		ORDER BY atr.AutomatedTaskResultID DESC 
	END
	ELSE
	BEGIN
		IF @TaskID IS NULL
		BEGIN
			SELECT TOP (@MaxRows) atr.* 
			FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
			WHERE atr.RunDate > dbo.fn_GetDate_Local() - @AgeInDays
			AND atr.ClientID = @ClientID 
			ORDER BY atr.AutomatedTaskResultID DESC 
		END
		ELSE
		BEGIN
			SELECT TOP (@MaxRows) atr.* 
			FROM dbo.AutomatedTaskResult atr WITH (NOLOCK) 
			WHERE atr.RunDate > dbo.fn_GetDate_Local() - @AgeInDays
			AND atr.ClientID = @ClientID 
			AND atr.TaskID = @TaskID 
			ORDER BY atr.AutomatedTaskResultID DESC 
		END
	END*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[atr] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[atr] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[atr] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[atr] TO [sp_executehelper]
GO
