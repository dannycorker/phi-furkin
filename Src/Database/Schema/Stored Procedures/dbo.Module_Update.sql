SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Module table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Module_Update]
(

	@ModuleID int   ,

	@OriginalModuleID int   ,

	@ModuleName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Module]
				SET
					[ModuleID] = @ModuleID
					,[ModuleName] = @ModuleName
				WHERE
[ModuleID] = @OriginalModuleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Module_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Update] TO [sp_executeall]
GO
