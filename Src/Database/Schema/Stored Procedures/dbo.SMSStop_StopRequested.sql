SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-09
-- Description:	Log STOP request from customer
-- =============================================

CREATE PROCEDURE [dbo].[SMSStop_StopRequested]
(
	  @SurveyNumber varchar(50)
	, @MobileNumber	varchar(50)	
	, @ClientID int = null
	, @LeadTypeID int = null
)

AS
BEGIN
	SET NOCOUNT ON;

	
/* 	Client wide opt out		= any leadtype on that client will not be able to send an SMS to this mobile
	Lead Type specific opt  = only this leadtype will not be able to send an SMS to this mobile				*/

	DECLARE @isClientWideOptOut	bit = 1
	, @CustomerID			int
	, @StopSMS				bit = 1
	, @OptOutRequestSent	bit = 0
	, @SMSStopID			int
	, @UpdateCount			int = 0
	
	IF ISNULL(@LeadTypeID, 0) = 0  or ISNULL(@ClientID, 0) = 0 
	BEGIN

		SELECT @LeadTypeID	= so.LeadTypeID	
		, @ClientID			= so.ClientID	
		FROM SMSOutgoingPhoneNumber so with(nolock) 
		WHERE so.OutgoingPhoneNumber = @SurveyNumber

	END
	
	SELECT @isClientWideOptOUT = case S.isLeadTypeSpecific when 0 then 1 else 0 end
	FROM smsstopconfig s with(nolock) 
	WHERE s.clientid = @ClientID

	IF ISNULL(@CustomerID, 0) = 0 
	BEGIN

		SELECT TOP 1 @CustomerID = c.CustomerID
		FROM Customers c with(nolock) 
		WHERE c.MobileTelephone = @MobileNumber
		AND c.ClientID = @ClientID

	END

	IF @isClientWideOptOut = 1 
	BEGIN

		UPDATE s
		SET s.StopSMS = @StopSMS
		, s.Updated = dbo.fn_GetDate_Local()
		FROM SMSStop s
		WHERE s.MobileNumber = @MobileNumber
		AND s.ClientID = @ClientID
		
		SELECT @UpdateCount	= @@ROWCOUNT

	END
	ELSE IF @isClientWideOptOut = 0  /* LeadType specific opt out */
	BEGIN

		UPDATE s
		SET s.StopSMS = @StopSMS
			, s.Updated = dbo.fn_GetDate_Local()
		FROM SMSStop s
		WHERE s.MobileNumber = @MobileNumber
		AND s.LeadTypeID = @LeadTypeID
		AND s.ClientID = @ClientID

		SELECT @UpdateCount	= @@ROWCOUNT

	END
	
	IF @UpdateCount = 0 
	AND ISNULL(@ClientID, 0) != 0 
	BEGIN 

		INSERT INTO SMSStop (ClientID, CustomerID, LeadTypeID, MobileNumber, StopSMS, OptOutRequestSent, Updated, Created)
		VALUES (@ClientID, ISNULL(@CustomerID, 0) , ISNULL(@LeadTypeID, 0) , @MobileNumber, @StopSMS, @OptOutRequestSent, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())

	END
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_StopRequested] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStop_StopRequested] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_StopRequested] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_StopRequested] TO [sp_executehelper]
GO
