SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-24
-- Description:	Copy all Sql Query tables/columns/criteria etc to editing tables
-- JWG 2012-08-14 Added new columns Comparison & IsJoinClause
-- JW  2013-05-08 Added new column ComplexValue - used to support CASE function
-- =============================================
CREATE PROCEDURE [dbo].[CopySqlQueryForEditing]
	@SqlQueryID int,
	@SqlQueryEditingID int
AS
BEGIN
	SET NOCOUNT ON;

	-- Copy Tables
	INSERT INTO SqlQueryEditingTable
	(ClientID, SqlQueryEditingID, SqlQueryTableName, TableAlias, TableDisplayOrder,
	JoinType, JoinText, JoinTableID, JoinRTRID, RealSqlQueryTableID)
	SELECT
	ClientID, @SqlQueryEditingID, SqlQueryTableName, TableAlias, TableDisplayOrder,
	JoinType, JoinText, JoinTableID, JoinRTRID, SqlQueryTableID
	FROM SqlQueryTables 
	WHERE SqlQueryID = @SqlQueryID 
	ORDER BY SqlQueryTableID
	
	-- Copy Columns
	-- This uses LEFT JOINs because you can have columns without tables
	-- (only if this is a CHILD/SUMMARY report).
	INSERT INTO SqlQueryEditingColumns
	(ClientID, SqlQueryEditingID, SqlQueryEditingTableID, CompleteOutputText, ColumnAlias,
	ShowColumn, DisplayOrder, SortOrder, SortType, IsAggregate, ColumnDataType,
	ManipulatedDataType, ColumnNaturalName, RealSqlQueryColumnID, ComplexValue)
	SELECT
	sqc.ClientID, @SqlQueryEditingID, sqet.SqlQueryEditingTableID, sqc.CompleteOutputText, sqc.ColumnAlias, 
	sqc.ShowColumn, sqc.DisplayOrder, sqc.SortOrder, sqc.SortType, sqc.IsAggregate, sqc.ColumnDataType, 
	sqc.ManipulatedDataType, sqc.ColumnNaturalName, sqc.SqlQueryColumnID, sqc.ComplexValue
	FROM SqlQueryColumns sqc
	LEFT JOIN SqlQueryEditingTable sqet ON sqet.RealSqlQueryTableID = sqc.SqlQueryTableID AND sqet.SqlQueryEditingID = @SqlQueryEditingID
	WHERE SqlQueryID = @SqlQueryID 
	ORDER BY SqlQueryTableID, SqlQueryColumnID

	-- Copy Criteria
	-- This uses LEFT JOINs because you can have criteria without columns
	-- (only for security criteria).
	INSERT INTO SqlQueryEditingCriteria
	(ClientID, SqlQueryEditingID, SqlQueryEditingTableID, SqlQueryEditingColumnID,
	CriteriaText, Criteria1, Criteria2, CriteriaName, SubQueryID, SubQueryLinkType,
	ParamValue, IsSecurityClause, CriteriaSubstitutions, IsParameterizable, Comparison, IsJoinClause)
	SELECT
	sqc.ClientID, @SqlQueryEditingID, sqet.SqlQueryEditingTableID, sqco.SqlQueryEditingColumnID,
	sqc.CriteriaText, sqc.Criteria1, sqc.Criteria2, sqc.CriteriaName, sqc.SubQueryID, sqc.SubQueryLinkType,
	sqc.ParamValue, sqc.IsSecurityClause, sqc.CriteriaSubstitutions, sqc.IsParameterizable, sqc.Comparison, sqc.IsJoinClause
	FROM SqlQueryCriteria sqc
	LEFT JOIN SqlQueryEditingTable sqet ON sqet.RealSqlQueryTableID = sqc.SqlQueryTableID AND sqet.SqlQueryEditingID = @SqlQueryEditingID
	LEFT JOIN SqlQueryEditingColumns sqco ON sqco.RealSqlQueryColumnID = sqc.SqlQueryColumnID AND sqco.SqlQueryEditingID = @SqlQueryEditingID
	WHERE SqlQueryID = @SqlQueryID 
	ORDER BY SqlQueryTableID, SqlQueryColumnID

	-- Copy Group By / Having
	-- This uses INNER JOINs because you can't have group by without columns
	INSERT INTO SqlQueryEditingGrouping
	(ClientID, SqlQueryEditingID, GroupByClause, HavingClause,
	HavingColumnID, HavingCriteria1, HavingCriteria2)
	SELECT
	sqg.ClientID, @SqlQueryEditingID, sqg.GroupByClause, sqg.HavingClause,
	sqc.SqlQueryEditingColumnID, sqg.HavingCriteria1, sqg.HavingCriteria2
	FROM SqlQueryGrouping sqg
	INNER JOIN SqlQueryEditingColumns sqc ON sqc.RealSqlQueryColumnID = sqg.HavingColumnID AND sqc.SqlQueryEditingID = @SqlQueryEditingID
	WHERE SqlQueryID = @SqlQueryID 

END








GO
GRANT VIEW DEFINITION ON  [dbo].[CopySqlQueryForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CopySqlQueryForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CopySqlQueryForEditing] TO [sp_executeall]
GO
