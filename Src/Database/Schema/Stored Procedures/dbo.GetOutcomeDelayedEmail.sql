SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetOutcomeDelayedEmail]
@OutcomeDelayedEmailID int
AS
Select OutcomeDelayedEmailID, CustomerID, ClientQuestionnaireID, DateOutcomeEmailSent, ClientID 
From OutcomeDelayedEmails
Where OutcomeDelayedEmailID = @OutcomeDelayedEmailID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeDelayedEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeDelayedEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeDelayedEmail] TO [sp_executeall]
GO
