SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:  Jim Green
-- Purpose: Select records from the DetailFieldPages that this user is allowed to see
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages__GetAllByLeadOrMatterSecure]
(

	@LeadTypeID int,
	@LeadOrMatter int,
	@UserID int  
)
AS


				SET ANSI_NULLS OFF
				
				DECLARE @ClientID INT
				SELECT @ClientID = ClientID
				FROM dbo.LeadType WITH (NOLOCK) 
				WHERE LeadTypeID = @LeadTypeID
				
				
				SELECT DISTINCT dfp.*
				FROM
					dbo.fnDetailFieldPagesShared(@ClientID) dfp 
				INNER JOIN fnDetailFieldPagesSecure(@UserID, @LeadTypeID) f ON dfp.DetailFieldPageID = f.objectid
				WHERE
					[LeadTypeID] = @LeadTypeID
				AND Enabled = 1
				AND LeadOrMatter = @LeadOrMatter
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByLeadOrMatterSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetAllByLeadOrMatterSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByLeadOrMatterSecure] TO [sp_executeall]
GO
