SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
	Jim Green 2008-01-15

	This proc looks for a specific LeadEvent and checks to see if
	the current user is allowed to delete it.  
	Deletes are logical, not physical. Retain any linked documents too.
	If the LeadEvent followed up a previous InProcess event then the followup flags get cleared on the old event.
	Finally, if the LeadEvent being deleted affected the LeadStatus or AquariumStatus when it was added, then they get reset too.
	Change the status of the Cases record, rather than the lead.

	JWG 2010-02-05 Open ALL previous events if this one followed up multiple previous events.
	
	JWG 2010-11-30 Attempt to reduce deadlocks.
	
	ACE 2013-09-03 Added the functionality to remove NextEventID on multi threads where the next event isnt the one you are 
	deleteing but on another thread
	
	TDN 2015-08-26 Remove from automated EventQueue any Event that has been triggered by this one (FromLeadEventID)
	but hasn't been removed yet. #34114
*/

CREATE PROCEDURE [dbo].[DeleteLeadEvent] 
	@LeadEventID int, 
	@UserID int, 
	@DeletionComments varchar(2000), 
	@FullRights tinyint = 0, 
	@ShowComments bit = 0 
AS 
BEGIN
	
	SET NOCOUNT ON

	DECLARE @DeleteTest int, 
	@DocumentQueueID int, 
	@LeadID int, 
	@CaseID int, 
	@EventTypeID int, 
	@StatusID int, 
	@AquariumStatusID int, 
	@NextEventID int, 
	@PrevLeadEventID int, 
	@PrevEventTypeID int, 
	@PrevStatusID int, 
	@PrevAquariumStatusID int,  
	@LeadDocumentID int,
	@LeadCaseCount int
	
	DECLARE @LeadeventsToUnfollowUp TABLE (FromLeadEventID int)

	/* Check if this event can be deleted by anyone at all, let alone the current user. */
	SELECT @DeleteTest = dbo.fnCanLeadEventBeDeleted(@LeadEventID, @UserID)

	/* If this event cannot be deleted at all, by anyone, then return an error. */
	IF @DeleteTest < 0
	BEGIN
		IF @ShowComments = 1
		BEGIN 
			SELECT 'This Event cannot be deleted by any user'
		END
		RETURN -1
	END

	/* If this event can only be deleted by a super-user, then make sure this user is one. */
	IF @DeleteTest = 0
	BEGIN
		IF @FullRights < 4
		BEGIN
			IF @ShowComments = 1
			BEGIN 
				SELECT 'This Event cannot be deleted by this user'
			END
			RETURN -1
		END
	END


	/* Get details for this LeadEvent */
	SELECT TOP 1 @LeadID = le.LeadID, 
	@CaseID = le.CaseID, 
	@NextEventID = le.NextEventID, 
	@EventTypeID = le.EventTypeID, 
	@StatusID = et.StatusAfterEvent, 
	@AquariumStatusID = aet.AquariumStatusAfterEvent, 
	@LeadDocumentID = le.LeadDocumentID,
	@DocumentQueueID = le.DocumentQueueID  
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	LEFT JOIN dbo.EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID 
	LEFT JOIN dbo.AquariumEventType aet WITH (NOLOCK) on et.AquariumEventAfterEvent = aet.AquariumEventTypeID 
	WHERE le.LeadEventID = @LeadEventID  

	IF @ShowComments = 1
	BEGIN 
		SELECT 'LeadID is ' + convert(varchar, @LeadID)
	END
	
	IF @NextEventID > 0 AND @NextEventID <> @LeadEventID
	BEGIN
		IF @ShowComments = 1
		BEGIN 
			SELECT 'NextEvent ' + convert(varchar, @NextEventID) + ' has already followed up this event, which is InProcess and cannot be deleted'
		END
		RETURN -1
	END

	/*
		2013-09-03 ACE
		Updated to remove incorrectly left leadevent followups
	*/
	INSERT INTO @LeadeventsToUnfollowUp (FromLeadEventID)
	SELECT letc.FromLeadEventID
	FROM LeadEventThreadCompletion letc WITH (NOLOCK)
	WHERE letc.ToLeadEventID = @LeadEventID

	/* Count the number of Cases for this Lead. Less work is required if there is only one. 
	Commented out by ACE as the statement isnt actually doing anything...
	--SELECT count(*) 
	--FROM dbo.Cases ca WITH (NOLOCK) 
	--WHERE ca.LeadID = @LeadID 
	*/
	
	/* Get details for any one LeadEvent that was followed up by this one */
	SELECT TOP 1 @PrevLeadEventID = le.LeadEventID, 
	@PrevEventTypeID = le.EventTypeID, 
	@PrevStatusID = et.StatusAfterEvent, 
	@PrevAquariumStatusID = aet.AquariumStatusAfterEvent
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID 
	LEFT JOIN dbo.AquariumEventType aet WITH (NOLOCK) on et.AquariumEventAfterEvent = aet.AquariumEventTypeID 
	WHERE le.NextEventID = @LeadEventID  

	IF @ShowComments = 1
	BEGIN 
		SELECT 'EventTypeID is ' + IsNull(convert(varchar, @EventTypeID),'NULL') + ' and LeadStatus is ' + IsNull(convert(varchar, @StatusID),'NULL') + ' and AquariumStatus is ' + IsNull(convert(varchar, @AquariumStatusID),'NULL') 
	
		SELECT CASE IsNull(@PrevLeadEventID, 0) WHEN 0 THEN 'No Previous Event' ELSE 'PrevLeadEventID is ' + IsNull(convert(varchar, @PrevLeadEventID),'NULL') + ' and PrevEventTypeID is ' + IsNull(convert(varchar, @PrevEventTypeID),'NULL') + ' and PrevLeadStatus is ' + IsNull(convert(varchar, @PrevStatusID),'NULL') + ' and PrevAquariumStatus is ' + IsNull(convert(varchar, @PrevAquariumStatusID),'NULL')  END
	END
	
	
	/* Set the client-defined status of the Case and Lead back to the previous values (LOA sent, Case Won And Paid etc). */
	IF @StatusID > 0
	BEGIN
		IF @ShowComments = 1
		BEGIN 
			SELECT 'Case Status may need repair'
		END
		
		IF IsNull(@PrevStatusID, 0) = 0
		BEGIN
			IF @ShowComments = 1
			BEGIN 
				SELECT 'Reverting Case to previous status'
			END
			 
			SELECT @PrevStatusID = et.StatusAfterEvent 
			FROM dbo.LeadEvent le WITH (NOLOCK) 
			INNER JOIN dbo.EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID 
			WHERE le.CaseID = @CaseID  /* CASEID */
			AND le.LeadEventID = (
				SELECT max(le2.LeadEventID) 
				FROM dbo.LeadEvent le2 WITH (NOLOCK) 
				INNER JOIN dbo.EventType et2 WITH (NOLOCK) on le2.EventTypeID = et2.EventTypeID 
				WHERE le2.CaseID = @CaseID  /* CASEID */ 
				AND le2.EventDeleted = 0 
				AND le2.LeadEventID <> @LeadEventID 
				AND et2.StatusAfterEvent IS NOT NULL 
			)
	
		END
		
		/*
			Previous status may even be NULL now, which is legitimate.
		*/
		UPDATE dbo.Cases 
		SET ClientStatusID = @PrevStatusID 
		WHERE CaseID = @CaseID
	
		/* 
			If the Lead has multiple Cases, there is no easy way to tell what the status should revert back to.
			Find the latest non-deleted LeadEvent on any Case that set the status and use that one. 
		*/
		IF @ShowComments = 1
		BEGIN 
			SELECT 'Reverting Lead to previous status'
		END
		
		IF @LeadCaseCount > 1
		BEGIN
			SELECT @PrevStatusID = et.StatusAfterEvent 
			FROM dbo.LeadEvent le WITH (NOLOCK) 
			INNER JOIN dbo.EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID 
			WHERE le.LeadID = @LeadID /* LEADID here, rather than CASEID above */ 
			AND le.LeadEventID = (
				SELECT max(le2.LeadEventID) 
				FROM dbo.LeadEvent le2 WITH (NOLOCK) 
				INNER JOIN dbo.EventType et2 WITH (NOLOCK) on le2.EventTypeID = et2.EventTypeID 
				WHERE le2.LeadID = @LeadID /* LEADID here, rather than CASEID above */
				AND le2.EventDeleted = 0 
				AND le2.LeadEventID <> @LeadEventID 
				AND et2.StatusAfterEvent IS NOT NULL 
			)
		END
		
		UPDATE dbo.Lead 
		SET	ClientStatusID = @PrevStatusID 
		WHERE LeadID = @LeadID 

	END


	/* Same all over again for Aquarium Status (Closed, Accepted etc) */ 
	IF @AquariumStatusID > 0
	BEGIN
		IF @ShowComments = 1
		BEGIN 
			SELECT 'Aquarium Status may need repair'
		END
		
		IF IsNull(@PrevAquariumStatusID, 0) = 0
		BEGIN
			IF @ShowComments = 1
			BEGIN 
				SELECT 'Repairing Lead and Case Aquarium status'
			END
				 
			SELECT TOP 1 @PrevAquariumStatusID = aet.AquariumStatusAfterEvent 
			FROM dbo.LeadEvent le WITH (NOLOCK) 
			INNER JOIN dbo.EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID 
			INNER JOIN dbo.AquariumEventType aet WITH (NOLOCK) on et.AquariumEventAfterEvent = aet.AquariumEventTypeID 
			WHERE le.CaseID = @CaseID  /* CASEID */ 
			AND le.LeadEventID = (
			
				SELECT max(le2.LeadEventID) 
				FROM dbo.LeadEvent le2 WITH (NOLOCK) 
				INNER JOIN dbo.EventType et2 WITH (NOLOCK) on le2.EventTypeID = et2.EventTypeID 
				INNER JOIN dbo.AquariumEventType aet2 WITH (NOLOCK) on et2.AquariumEventAfterEvent = aet2.AquariumEventTypeID 
				WHERE le2.CaseID = @CaseID  /* CASEID */
				AND le2.LeadEventID < @LeadEventID 
				AND aet2.AquariumStatusAfterEvent IS NOT NULL 
			
			)
	
		END
	
		UPDATE dbo.Cases 
		SET AquariumStatusID = ISNULL(@PrevAquariumStatusID, 2) 
		WHERE CaseID = @CaseID 

		/* 
			If the Lead has multiple Cases, there is no easy way to tell what the status should revert back to.
			Find the latest non-deleted LeadEvent on any Case that set the status and use that one. 
		*/
		IF @ShowComments = 1
		BEGIN 
			SELECT 'Reverting Lead to previous status'
		END
		
		IF @LeadCaseCount > 1
		BEGIN
			SELECT TOP 1 @PrevAquariumStatusID = aet.AquariumStatusAfterEvent 
			FROM dbo.LeadEvent le WITH (NOLOCK) 
			INNER JOIN dbo.EventType et WITH (NOLOCK) on le.EventTypeID = et.EventTypeID 
			INNER JOIN dbo.AquariumEventType aet WITH (NOLOCK) on et.AquariumEventAfterEvent = aet.AquariumEventTypeID 
			WHERE le.LeadID = @LeadID /* LEADID here, rather than CASEID above */ 
			AND le.LeadEventID = (
			
				SELECT max(le2.LeadEventID) 
				FROM dbo.LeadEvent le2 WITH (NOLOCK) 
				INNER JOIN dbo.EventType et2 WITH (NOLOCK) on le2.EventTypeID = et2.EventTypeID 
				INNER JOIN dbo.AquariumEventType aet2 WITH (NOLOCK) on et2.AquariumEventAfterEvent = aet2.AquariumEventTypeID 
				WHERE le2.LeadID = @LeadID /* LEADID here, rather than CASEID above */
				AND le2.LeadEventID < @LeadEventID 
				AND aet2.AquariumStatusAfterEvent IS NOT NULL 
			
			)
		END

		/* 
			Set concrete Aquarium status on the Lead record if it has changed. 
			Only set it to closed (4) if all the other Cases are closed as well.
		*/
		/* Previous Aquarium status can never be NULL */
		IF @PrevAquariumStatusID IS NULL
		BEGIN
			SET @PrevAquariumStatusID = 2
		END

		UPDATE dbo.Lead 
		SET	AquariumStatusID = @PrevAquariumStatusID 
		WHERE LeadID = @LeadID 
		AND AquariumStatusID <> @PrevAquariumStatusID 
		AND (
				(@PrevAquariumStatusID <> 4) 
			OR
				(NOT EXISTS (SELECT * FROM dbo.Cases ca WITH (NOLOCK) WHERE ca.LeadID = @LeadID AND ca.AquariumStatusID <> 4))
			)
		
	
	END


	/* 
		Now we have all the details we need, mark the event as deleted. 
		This update fires trgiu_LeadEvent on the LeadEvent table.
	*/
	UPDATE dbo.LeadEvent 
	SET EventDeleted = 1, 
	WhoDeleted = @UserID, 
	DeletionComments = @DeletionComments, 
	DocumentQueueID = NULL, 
	WhenFollowedUp = NULL,  /* For OOP events, these will need to be cleared */
	NextEventID = NULL		/* out by the trigger, so this saves a step.     */
	WHERE LeadEventID = @LeadEventID 


	/* Make sure this record doesn't come back into the list of next events required. */
	DELETE dbo.LeadEventThreadCompletion 
	WHERE FromLeadEventID = @LeadEventID

	/*
		JWG 2009-07-10 
		Remove associated WorkFlow entries that demand
		the followups of an event of this type for this case.
	*/
	DELETE dbo.WorkFlowTask  
	WHERE CaseID = @CaseID 
	AND EventTypeID = @EventTypeID 
	AND FollowUp = 1 


	/*
		JWG 2009-09-30 
		Remove any associated DocumentQueue entries
		linked to this event (if there aren't any others also linked to it).
	*/
	IF NOT EXISTS (
		SELECT * 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.DocumentQueueID = @DocumentQueueID 
		AND le.EventDeleted = 0
	)
	BEGIN

		DELETE dbo.DocumentQueue 
		WHERE DocumentQueueID = @DocumentQueueID

	END


	/* Re-open all LeadEvents that were followedup by this one. */
	WHILE @PrevLeadEventID > 0
	BEGIN
		UPDATE dbo.LeadEvent
		SET WhenFollowedUp = NULL, 
		NextEventID = NULL 
		WHERE LeadEventID = @PrevLeadEventID

		/*
			Cater for older cases where 1 LETC record was added for the latest in-proc event at Release 2.3
			and that leadevent has just been deleted, leaving nothing in LETC for the previous event.
			This means it would never leave the list of Threads-In-Progress for this case.
		*/
		IF NOT EXISTS (
			SELECT * 
			FROM dbo.LeadEventThreadCompletion WITH (NOLOCK) 
			WHERE FromLeadEventID = @PrevLeadEventID
		)
		BEGIN
			INSERT INTO LeadEventThreadCompletion (ClientID, LeadID, CaseID, FromEventTypeID, FromLeadEventID, ThreadNumberRequired, ToEventTypeID, ToLeadEventID)
			SELECT le.ClientID, le.LeadID, le.CaseID, le.EventTypeID, le.LeadEventID, ec.ThreadNumber, NULL, NULL  
			FROM LeadEvent le WITH (NOLOCK)
			INNER JOIN EventChoice ec WITH (NOLOCK)on ec.EventTypeID = le.EventTypeID
			WHERE le.LeadEventID = @PrevLeadEventID
		END

		SET @PrevLeadEventID = NULL

		SELECT TOP 1 @PrevLeadEventID = LeadEvent.LeadEventID 
		FROM dbo.LeadEvent WITH (NOLOCK) 
		WHERE LeadEvent.NextEventID = @LeadEventID 

	END

	/*
		TDN 2015-08-26 Remove from automated EventQueue any Event that has been triggered by this one (FromLeadEventID)
		but hasn't been removed yet. #34114
		Do that by setting the ErrorMessage to "Trigger Event Deleted" and Error date to current date.
	*/	
	UPDATE dbo.AutomatedEventQueue
		SET ErrorMessage = 'Trigger Event has been deleted',
			ErrorDateTime = dbo.fn_GetDate_Local()
	WHERE automatedEventQueueID in
		(SELECT aeq.AutomatedEventQueueID
			FROM AutomatedEventQueue AS aeq
			WHERE FromLeadEventID = @LeadEventID
			AND aeq.SuccessDateTime IS NULL
			AND aeq.LockDateTime IS NULL
			AND aeq.ErrorMessage IS NULL)
			

	/*
		ACE 2013-09-03 Last bit.. Remove the NextEventID from any events that arent set correctly still.
	*/
	UPDATE LeadEvent
	SET NextEventID = NULL, WhenFollowedUp = NULL
	FROM LeadEvent 
	INNER JOIN @LeadeventsToUnfollowUp l ON l.FromLeadEventID = LeadEvent.LeadEventID

	/* Keep latest event IDs in step. The LeadEvent trigger only does this for fresh inserts, not for updates */
	EXEC dbo.SetLatestCaseEvents @CaseID


	/* Return success (ie zero errors) */
	RETURN 0
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteLeadEvent] TO [sp_executeall]
GO
