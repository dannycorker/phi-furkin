SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DroppedOutCustomers table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_GetByCountryID]
(

	@CountryID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerID],
					[ClientID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[HomeTelephone],
					[MobileTelephone],
					[CompanyTelephone],
					[WorksTelephone],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[HasDownloaded],
					[DownloadedOn],
					[AquariumStatusID],
					[ClientStatusID],
					[Test],
					[CompanyName],
					[Occupation],
					[Employer],
					[ReminderEmailSentDate],
					[ReminderSmsSentDate],
					[CountryID]
				FROM
					[dbo].[DroppedOutCustomers] WITH (NOLOCK) 
				WHERE
					[CountryID] = @CountryID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_GetByCountryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_GetByCountryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_GetByCountryID] TO [sp_executeall]
GO
