SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderObjectAction table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionID]
(

	@DataLoaderObjectActionID int   
)
AS


				SELECT
					[DataLoaderObjectActionID],
					[DataLoaderObjectActionName],
					[DataLoaderObjectActionDescription]
				FROM
					[dbo].[DataLoaderObjectAction] WITH (NOLOCK) 
				WHERE
										[DataLoaderObjectActionID] = @DataLoaderObjectActionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_GetByDataLoaderObjectActionID] TO [sp_executeall]
GO
