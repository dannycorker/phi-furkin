SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSection_Update]
(

	@ScriptSectionID int   ,

	@ScriptID int   ,

	@ClientID int   ,

	@SectionTitle varchar (250)  ,

	@SectionOrder int   ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@WhenModified datetime   ,

	@WhoModified int   ,

	@SourceID int   ,

	@NextEventTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptSection]
				SET
					[ScriptID] = @ScriptID
					,[ClientID] = @ClientID
					,[SectionTitle] = @SectionTitle
					,[SectionOrder] = @SectionOrder
					,[WhenCreated] = @WhenCreated
					,[WhoCreated] = @WhoCreated
					,[WhenModified] = @WhenModified
					,[WhoModified] = @WhoModified
					,[SourceID] = @SourceID
					,[NextEventTypeID] = @NextEventTypeID
				WHERE
[ScriptSectionID] = @ScriptSectionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Update] TO [sp_executeall]
GO
