SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-09-09
-- Description:	Insert Date to mdv or ldv
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertDateToField]
(
	@LeadEventID int,
	@DateField int
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare	@myERROR int,
	@LeadID int,
	@LeadTypeID int,
	@ClientID int,
	@MatterID int,
	@LeadOrMatterFieldDate varchar(100),
	@Comments varchar(2000)

	exec dbo._C00_CreateDetailFields @DateField,@LeadID

	Select
	@LeadID = Lead.LeadID,
	@LeadTypeID = Lead.LeadTypeID,
	@ClientID = Lead.ClientID,
	@MatterID = Matter.MatterID
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	inner join Matter on Matter.LeadID = Lead.LeadID
	where leadevent.leadeventid = @LeadEventID 

	Select 
	@LeadOrMatterFieldDate = LeadOrMatter
	From DetailFields 
	Where DetailFieldID = @DateField


	if @LeadOrMatterFieldDate = 1
	Begin

		/* Insert date into Lead Feild*/
		Update LeadDetailValues 
		Set DetailValue = convert(varchar, dbo.fn_GetDate_Local(), 120)
		Where LeadID = @LeadID
		And DetailFieldID = @DateField

	end

	if @LeadOrMatterFieldDate = 2
	Begin

		/* Insert Date into Matter Feild*/
		Update MatterDetailValues 
		Set DetailValue = convert(varchar, dbo.fn_GetDate_Local(), 120)
		Where MatterID = @MatterID
		And DetailFieldID = @DateField

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertDateToField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertDateToField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertDateToField] TO [sp_executeall]
GO
