SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-11-11
-- Description:	Get the semafone config that is suppied
-- with every request to semafone
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Semafone_GetConfig]
(
	@ClientPersonnelID INT = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF @ClientPersonnelID IN (44393,44407,44404,44399)
	BEGIN
	
		-- test config
		SELECT	
			5						AccountId,	
			'2'						ClientId,	
			'Password'				Password,	
			'dev1'					Principle,	
			'https://45.79.187.92'	SemafoneURL,	
			'A'						TenantId,
			'Semafone123'			Secret,			
			'f6cfceb7-08c7-42ae-b300-7adddcc4c8b9' ClientReference,		
			'203533'				MerchantReference,	
			'GetTokenPan'			TransactionType,	
			'000001'				CustomerBin,		
			'true'					PPResponse,		
			'false'					ReturnBin,
			'GB'					CustomerCountryCode
	
	END
	ELSE
	BEGIN
		-- live config
		SELECT	
			315325					AccountId,	
			'1'						ClientId,	
			'JUklhWuETlR3qlJR34Mc'	Password,	
			'AQU'					Principle,	
			'https://192.168.80.40'	SemafoneURL,	
			'AQUARIUM'				TenantId,
			'PetHealth123'			Secret,			
			'f6cfceb7-08c7-42ae-b300-7adddcc4c8b9' ClientReference,		
			'256972'				MerchantReference,	
			'GetTokenPan'			TransactionType,	
			'000001'				CustomerBin,		
			'true'					PPResponse,		
			'false'					ReturnBin,
			'GB'					CustomerCountryCode
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Semafone_GetConfig] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Semafone_GetConfig] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Semafone_GetConfig] TO [sp_executeall]
GO
