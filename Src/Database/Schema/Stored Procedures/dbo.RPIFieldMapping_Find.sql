SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the RPIFieldMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@RPIFieldMappingID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@RPIFieldID int   = null ,

	@DetailFieldID int   = null ,

	@IsEnabled bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@ColumnDetailFieldID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RPIFieldMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [RPIFieldID]
	, [DetailFieldID]
	, [IsEnabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [ColumnDetailFieldID]
    FROM
	[dbo].[RPIFieldMapping] WITH (NOLOCK) 
    WHERE 
	 ([RPIFieldMappingID] = @RPIFieldMappingID OR @RPIFieldMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([RPIFieldID] = @RPIFieldID OR @RPIFieldID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([ColumnDetailFieldID] = @ColumnDetailFieldID OR @ColumnDetailFieldID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RPIFieldMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [RPIFieldID]
	, [DetailFieldID]
	, [IsEnabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [ColumnDetailFieldID]
    FROM
	[dbo].[RPIFieldMapping] WITH (NOLOCK) 
    WHERE 
	 ([RPIFieldMappingID] = @RPIFieldMappingID AND @RPIFieldMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([RPIFieldID] = @RPIFieldID AND @RPIFieldID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([ColumnDetailFieldID] = @ColumnDetailFieldID AND @ColumnDetailFieldID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Find] TO [sp_executeall]
GO
