SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Messages table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_Find]
(

	@SearchUsingOR bit   = null ,

	@MessageID int   = null ,

	@ClientPersonnelIDTo int   = null ,

	@ClientPersonnelIDFrom int   = null ,

	@DateSent datetime   = null ,

	@DateRead datetime   = null ,

	@Status int   = null ,

	@Subject varchar (255)  = null ,

	@MessageText varchar (1000)  = null ,

	@DateReplied datetime   = null ,

	@DateForwarded datetime   = null ,

	@PreviousMessageID int   = null ,

	@ClientID int   = null ,

	@PortalUserIDTo int   = null ,

	@PortalUserIDFrom int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MessageID]
	, [ClientPersonnelIDTo]
	, [ClientPersonnelIDFrom]
	, [DateSent]
	, [DateRead]
	, [Status]
	, [Subject]
	, [MessageText]
	, [DateReplied]
	, [DateForwarded]
	, [PreviousMessageID]
	, [ClientID]
	, [PortalUserIDTo]
	, [PortalUserIDFrom]
    FROM
	[dbo].[Messages] WITH (NOLOCK) 
    WHERE 
	 ([MessageID] = @MessageID OR @MessageID IS NULL)
	AND ([ClientPersonnelIDTo] = @ClientPersonnelIDTo OR @ClientPersonnelIDTo IS NULL)
	AND ([ClientPersonnelIDFrom] = @ClientPersonnelIDFrom OR @ClientPersonnelIDFrom IS NULL)
	AND ([DateSent] = @DateSent OR @DateSent IS NULL)
	AND ([DateRead] = @DateRead OR @DateRead IS NULL)
	AND ([Status] = @Status OR @Status IS NULL)
	AND ([Subject] = @Subject OR @Subject IS NULL)
	AND ([MessageText] = @MessageText OR @MessageText IS NULL)
	AND ([DateReplied] = @DateReplied OR @DateReplied IS NULL)
	AND ([DateForwarded] = @DateForwarded OR @DateForwarded IS NULL)
	AND ([PreviousMessageID] = @PreviousMessageID OR @PreviousMessageID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PortalUserIDTo] = @PortalUserIDTo OR @PortalUserIDTo IS NULL)
	AND ([PortalUserIDFrom] = @PortalUserIDFrom OR @PortalUserIDFrom IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MessageID]
	, [ClientPersonnelIDTo]
	, [ClientPersonnelIDFrom]
	, [DateSent]
	, [DateRead]
	, [Status]
	, [Subject]
	, [MessageText]
	, [DateReplied]
	, [DateForwarded]
	, [PreviousMessageID]
	, [ClientID]
	, [PortalUserIDTo]
	, [PortalUserIDFrom]
    FROM
	[dbo].[Messages] WITH (NOLOCK) 
    WHERE 
	 ([MessageID] = @MessageID AND @MessageID is not null)
	OR ([ClientPersonnelIDTo] = @ClientPersonnelIDTo AND @ClientPersonnelIDTo is not null)
	OR ([ClientPersonnelIDFrom] = @ClientPersonnelIDFrom AND @ClientPersonnelIDFrom is not null)
	OR ([DateSent] = @DateSent AND @DateSent is not null)
	OR ([DateRead] = @DateRead AND @DateRead is not null)
	OR ([Status] = @Status AND @Status is not null)
	OR ([Subject] = @Subject AND @Subject is not null)
	OR ([MessageText] = @MessageText AND @MessageText is not null)
	OR ([DateReplied] = @DateReplied AND @DateReplied is not null)
	OR ([DateForwarded] = @DateForwarded AND @DateForwarded is not null)
	OR ([PreviousMessageID] = @PreviousMessageID AND @PreviousMessageID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PortalUserIDTo] = @PortalUserIDTo AND @PortalUserIDTo is not null)
	OR ([PortalUserIDFrom] = @PortalUserIDFrom AND @PortalUserIDFrom is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Find] TO [sp_executeall]
GO
