SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventSubtype table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_Find]
(

	@SearchUsingOR bit   = null ,

	@EventSubtypeID int   = null ,

	@EventSubtypeName varchar (50)  = null ,

	@EventSubtypeDescription varchar (250)  = null ,

	@ApplyEventMaxThreshold int   = null ,

	@Available bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventSubtypeID]
	, [EventSubtypeName]
	, [EventSubtypeDescription]
	, [ApplyEventMaxThreshold]
	, [Available]
    FROM
	[dbo].[EventSubtype] WITH (NOLOCK) 
    WHERE 
	 ([EventSubtypeID] = @EventSubtypeID OR @EventSubtypeID IS NULL)
	AND ([EventSubtypeName] = @EventSubtypeName OR @EventSubtypeName IS NULL)
	AND ([EventSubtypeDescription] = @EventSubtypeDescription OR @EventSubtypeDescription IS NULL)
	AND ([ApplyEventMaxThreshold] = @ApplyEventMaxThreshold OR @ApplyEventMaxThreshold IS NULL)
	AND ([Available] = @Available OR @Available IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventSubtypeID]
	, [EventSubtypeName]
	, [EventSubtypeDescription]
	, [ApplyEventMaxThreshold]
	, [Available]
    FROM
	[dbo].[EventSubtype] WITH (NOLOCK) 
    WHERE 
	 ([EventSubtypeID] = @EventSubtypeID AND @EventSubtypeID is not null)
	OR ([EventSubtypeName] = @EventSubtypeName AND @EventSubtypeName is not null)
	OR ([EventSubtypeDescription] = @EventSubtypeDescription AND @EventSubtypeDescription is not null)
	OR ([ApplyEventMaxThreshold] = @ApplyEventMaxThreshold AND @ApplyEventMaxThreshold is not null)
	OR ([Available] = @Available AND @Available is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Find] TO [sp_executeall]
GO
