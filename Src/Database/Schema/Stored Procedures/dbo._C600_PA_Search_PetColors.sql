SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:           Angel Petrov
-- Create date: 2015-01-28
-- Description:      Get all the base pet colors
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_PetColors]
(
       @ClientId INT,
       @ClientPersonnelId INT
)
AS
BEGIN

       SELECT LookupListItemID Value, ItemValue Text
       FROM   LookupListItems WITH (NOLOCK) 
       WHERE  LookupListID = 5007
       AND Enabled = 1 

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_PetColors] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_PetColors] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_PetColors] TO [sp_executeall]
GO
