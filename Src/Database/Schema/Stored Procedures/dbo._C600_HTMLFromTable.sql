SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-10-24
-- Description:	Specialised HTMLFromTable for C600
-- =============================================
CREATE PROCEDURE [dbo].[_C600_HTMLFromTable]
(	
	@TableDetailFieldID INT,
	@WhereClause VARCHAR(MAX),
	@LeadID INT,
	@CaseID INT,
	@MatterID INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @Html VARCHAR(MAX) = '', 
			@Items dbo.tvpVarchar

	-- Itemised missing information
	IF @TableDetailFieldID IN (170273, 170274)
	BEGIN

		INSERT @Items
		SELECT 
			CASE 
				WHEN df.DetailFieldID = 170125 THEN mdvOtherVet.DetailValue
				WHEN df.DetailFieldID = 170126 THEN mdvOtherPH.DetailValue
				WHEN df.ErrorMessage > '' THEN df.ErrorMessage 
				ELSE df.FieldName
			END
		FROM 
			Matter m WITH (NOLOCK)
			INNER JOIN dbo.MatterDetailValues mdvDDL WITH (NOLOCK) ON mdvDDL.MatterID = m.MatterID
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = mdvDDL.DetailFieldID AND df.DetailFieldPageID = 16158 AND df.LookupListID = 4203 
			LEFT JOIN dbo.MatterDetailValues mdvOtherVet WITH (NOLOCK) ON mdvOtherVet.MatterID = m.MatterID and mdvOtherVet.DetailFieldID = 170271
			LEFT JOIN dbo.MatterDetailValues mdvOtherPH WITH (NOLOCK) ON mdvOtherPH.MatterID = m.MatterID and mdvOtherPH.DetailFieldID = 170272
		WHERE 
			m.MatterID = @MatterID
			AND mdvDDL.ValueInt = 63964 -- "Missing"
			AND (
					(@TableDetailFieldID = 170273 AND df.FieldName NOT LIKE '%Policyholder%')	-- Vet list, exclude PH items
				OR
					(@TableDetailFieldID = 170274 AND df.FieldName LIKE '%Policyholder%')		-- PH list, only PH items
				)
		ORDER BY df.FieldOrder		

		SELECT @Html = dbo.fn_C00_HTMLBulletList(@Items)
	
	END
	ELSE
	BEGIN
		RETURN
	END
	
	PRINT @HTML
	--exec _C00_LogIt  'TEST', 'C235', '_C321_HTMLFromTable', @html, 32465
	SELECT @Html AS MyHtml
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HTMLFromTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_HTMLFromTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HTMLFromTable] TO [sp_executeall]
GO
