SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-11-11
-- Description:	Copy Lookups from one client to another
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyLookupList]
@LookupListID int,
@ToClientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Lookups TABLE(LookupListID int)

	INSERT INTO LookupList (LookupListName, LookupListDescription, ClientID, Enabled, SortOptionID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	output inserted.LookupListID INTO @Lookups
	SELECT LookupListName, LookupListDescription, @ToClientID, Enabled, SortOptionID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified
	FROM LookupList l WITH (NOLOCK)
	WHERE l.LookupListID = @LookupListID
	
	INSERT INTO LookupListItems (LookupListID, ItemValue, ClientID, Enabled, SortOrder, ValueInt, ValueMoney, ValueDate, ValueDateTime, SourceID)
	SELECT l.LookupListID, ItemValue, @ToClientID, Enabled, SortOrder, ValueInt, ValueMoney, ValueDate, ValueDateTime, luli.LookupListItemID
	FROM LookupListItems luli WITH (NOLOCK)
	CROSS APPLY @Lookups l 
	WHERE luli.LookupListID = @LookupListID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyLookupList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyLookupList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyLookupList] TO [sp_executeall]
GO
