SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-17
-- Description:	Gets a list of Portal Users and counts their Cases
-- =============================================
CREATE PROCEDURE  [dbo].[PortalUserCase__GetUsersAndCounts]
	@ClientID int, 
	@CustomerID int = null
AS
BEGIN
	SET NOCOUNT ON;

    -- Select all PortalUser details for the chosen Client
	SELECT		PortalUser.PortalUserID, 
				Customers.Fullname, 
				ClientPersonnel.Username as [Impersonates], 
				Count(PortalUserCase.LeadID) as [CaseCount] 
	FROM        PortalUser WITH (NOLOCK) 
	INNER JOIN	Customers WITH (NOLOCK) ON PortalUser.CustomerID = Customers.CustomerID 
	INNER JOIN	ClientPersonnel WITH (NOLOCK) ON ClientPersonnel.ClientPersonnelID = PortalUser.ClientPersonnelID 
	LEFT JOIN   PortalUserCase WITH (NOLOCK) ON PortalUserCase.PortalUserID = PortalUser.PortalUserID 
	WHERE		PortalUser.ClientID = @ClientID 
	AND         (@CustomerID IS NULL OR @CustomerID = PortalUser.CustomerID)
	AND			ClientPersonnel.AccountDisabled = 0
	GROUP BY PortalUser.PortalUserID, Customers.Fullname, ClientPersonnel.Username 
END









GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetUsersAndCounts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__GetUsersAndCounts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetUsersAndCounts] TO [sp_executeall]
GO
