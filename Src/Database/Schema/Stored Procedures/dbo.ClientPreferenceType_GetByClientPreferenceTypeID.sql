SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPreferenceType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPreferenceType_GetByClientPreferenceTypeID]
(

	@ClientPreferenceTypeID int   
)
AS


				SELECT
					[ClientPreferenceTypeID],
					[PreferenceTypeName]
				FROM
					[dbo].[ClientPreferenceType] WITH (NOLOCK) 
				WHERE
										[ClientPreferenceTypeID] = @ClientPreferenceTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_GetByClientPreferenceTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPreferenceType_GetByClientPreferenceTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPreferenceType_GetByClientPreferenceTypeID] TO [sp_executeall]
GO
