SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2019-11-19
-- Description:	Take an XML package representing a claim and create the matter and case records within this database
--
-- 2020-01-07 CPS for JIRA LAGCLAIM-311 | Update to use new GUID method
-- 2020-01-09 MAB Validate input data for LAGCLAIM-290
-- 2020-01-10 MAB corrected filter for resource list updates, removed redundant mapping table column DecodeLookupListID, 
--					reformatted mapping table definition and population for clarity.
-- 2020-01-13 MAB refactored code - removed validation to its own proc to prevent code duplication in the receive procs
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-01-14 MAB refactored to call _C00_BulkValidateAndSaveDetailValuesFromXml to eliminate duplicate code in the "receive" procedures
-- 2020-01-20 MAB for Jira LAGCLAIM-365 | Implement receiver for nested tables (i.e. handle deductions which are nested within claims)
-- 2020-01-22 MAB added configurable error message separator character and raising of error message
-- 2020-01-24 MAB removed redundant retrieval of claim lead from LeadTypeRelationship table
-- =============================================
CREATE PROCEDURE [dbo].[_C600_API_Receive_Claim]
(
	 @XmlPayload			XML
	,@HttpMethod			VARCHAR(50)
	,@ObjectID				UNIQUEIDENTIFIER
	,@ParentObjectID		UNIQUEIDENTIFIER
	,@RunAsUserID			INT
	,@JsonResponse			NVARCHAR(MAX) OUTPUT
	,@ShowNewClaim			BIT = 0
)
AS
BEGIN

	/*
	- Configure mapping of input XML data to AQ detail fields
	- Make sure we have enough information to proceed including a mapped CustomerID and PetID
	- Look for existing claims and determine whether we are inserting or updating
	
	INSERT
	- Create new Case and Matter record, using existing variables
	- Collect and return the new MatterID

	UPDATE
	- Locate and update existing Matter
	*/

	DECLARE	 @PolicyID				INT
			,@RefLetter				VARCHAR(3)
			,@CaseNum				INT = 1
			,@NewCustomerID			INT
			,@NewLeadID				INT
			,@NewCaseID				INT
			,@NewMatterID			INT
			,@ClientID				INT				= dbo.fnGetPrimaryClientID()
			,@DateTimeCompleted		DATETIME2		= CURRENT_TIMESTAMP
			,@ErrorMessage			VARCHAR(2000)	= ''
			,@ClaimItemsXml			XML
			,@PaymentsXml			XML
			,@DeductionsXml			XML
			,@ObjectIDVchr			VARCHAR(40)		= CONVERT(VARCHAR(40), @ObjectID)
			,@HttpMethodID			INT
			,@ErrorMessageSeparator	VARCHAR(3)		= ' | '

	DECLARE	@LeadFieldMapping tvpXmlToDetailFieldMap			/*XML node to Lead Detail Field Mapping*/
	DECLARE	@MatterFieldMapping tvpXmlToDetailFieldMap			/*XML node to Matter Detail Field Mapping*/
	DECLARE	@ClaimTableFieldMapping tvpXmlToDetailFieldMap		/*XML node to Claim AQ Table Mapping*/
	DECLARE @DeductionTableFieldMapping tvpXmlToDetailFieldMap	/*XML node to Deduction AQ Table Mapping*/
	DECLARE	@PaymentTableFieldMapping tvpXmlToDetailFieldMap	/*XML node to Payment AQ Table Mapping*/

	/*
		XML to Detail Field Mapping Configuration Data
		----------------------------------------------
	*/

	/*LEAD Detail Field Mapping*/
	INSERT @LeadFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 144271, 			'dateOfDeath'			,	'DATE'	, 		NULL	, 							NULL	)	/*Date Pet Deceased or Lost*/
		,( 146215, 			'practiceName'			,	'TEXT'	, 		144473	, 							NULL	)	/*Current Vet*/
		-- The following entries are commented out as they are part of the Vet resource list which is already looked up via the practiceName
		-- entry above (which should be unique). If they were retained, then the lookup on postcode could result in an incorrect resource 
		-- list ID (as multiple vets may have the same post code). 
		--,( 146215, 			'address1'				,	'TEXT'	, 		144475	, 							NULL	)	/*Current Vet*/
		--,( 146215, 			'address2'				,	'TEXT'	, 		144476	,							NULL	)	/*Current Vet*/
		--,( 146215, 			'town'					,	'TEXT'	, 		144477	, 							NULL	)	/*Current Vet*/
		--,( 146215, 			'postcode'				,	'TEXT'	, 		144479	, 							NULL	)	/*Current Vet*/

	/*MATTER Detail Field Mapping*/
	INSERT @MatterFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 144332, 			'fnolDescription'		,	'TEXT'	, 		NULL	, 							NULL	)	/*FNOL Description of loss*/
		,( 144892, 			'dateOfLoss'			,	'DATE'	, 		NULL	, 							NULL	)	/*Date of Loss*/
		,( 144366, 			'treatmentStartDate'	,	'DATE'	, 		NULL	, 							NULL	)	/*Treatment Start*/
		,( 145674, 			'treatmentEndDate'		,	'DATE'	, 		NULL	, 							NULL	)	/*Treatment End*/
		,( 149850, 			'amount'				,	'MONEY'	, 		NULL	,	 						NULL	)	/*Total Claim Amount*/
		,( 144504, 			'ailmentId'				,	'RESOURCE',		NULL	, 							NULL	)	/*Ailment*/
		-- The following entries are commented out as they are part of the Ailment resource list and none of them are unique. As such they
		-- cannot be used in isolation to reliably lookup the resource list ID. Until a solution is found, the only means of identifying
		-- the Ailment will be to provide the resource list ID in the id field above.
		--,( 144504, 			'firstCause'			,	'TEXT'	, 		144340	, 							NULL	)	/*Ailment*/
		--,( 144504, 			'secondCause'			,	'TEXT'	, 		144341	, 							NULL	)	/*Ailment*/
		--,( 144504, 			'conditionType'			,	'INT'	, 		162655	, 							NULL	)	/*Ailment*/
		,( 144483, 			'claimType'				,	'TEXT'	, 		NULL	, 							44356	)	/*Claim Type: default=New*/

	/*CLAIM ITEMS Table Detail Field Mapping*/
	INSERT @ClaimTableFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 144349,			'treatmentStartDate'	,	'DATE'	,		NULL	,							NULL	)	/*Treat Start*/
		,( 144351, 			'treatmentEndDate'		,	'DATE'	,		NULL	,							NULL	)	/*Treatment End*/
		,( 144350, 			'policySection'			,	'RESOURCE',		NULL	,							NULL	)	/*Policy Section*/
		,( 144353, 			'amount'				,	'MONEY'	,		NULL	,							NULL	)	/*Claim*/
		,( 146179, 			'totalUserDeductions'	,	'MONEY'	,		NULL	,							NULL	)	/*User Deductions*/
		,( 145678, 			'settle'				,	'MONEY'	,		NULL	,							NULL	)	/*Settle*/
		,( 147001, 			'noCover'				,	'MONEY'	,		NULL	,							NULL	)	/*No Cover*/
		,( 146406, 			'excess'				,	'MONEY'	,		NULL	,							NULL	)	/*Excess*/
		,( 158802, 			'voluntaryExcess'		,	'MONEY'	,		NULL	,							NULL	)	/*Vol Excess*/
		,( 147434, 			'rebate'				,	'MONEY'	,		NULL	,							NULL	)	/*Rebate*/
		,( 146407, 			'coInsurance'			,	'MONEY'	,		NULL	,							NULL	)	/*Co Ins*/
		,( 146408, 			'limit'					,	'MONEY'	,		NULL	,							NULL	)	/*Limit*/
		,( 144352, 			'total'					,	'MONEY'	,		NULL	,							NULL	)	/*Total*/
		,( 147602, 			'payTo'					,	'TEXT'	,		NULL	,							NULL	)	/*Pay To*/
		,( 148399, 			'limitReached'			,	'MONEY'	,		NULL	,							NULL	)	/*Limit Reached RLID*/
		,( 149778, 			'claimRowType'			,	'TEXT'	,		NULL	,							NULL	)	/*Claim Row Type*/
		,( 144362, 			'approved'				,	'DATE'	,		NULL	,							NULL	)	/*Approved*/
		,( 159290, 			'alreadyPaid'			,	'MONEY'	,		NULL	,							NULL	)	/*Already Paid*/
		,( 0,				'sourceId'				,	'SOURCE',		NULL	,							NULL	)	/*Not a detail field: the original TableRowID of the claim in the source system*/

	/*DEDUCTIONS Table Detail Field Mapping - this is nested within the claim table - it is linked to its parent claim table row via parentClaimRowId*/
	INSERT @DeductionTableFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 147299,			'parentClaimRowId'		,	'PARENT',		NULL	,							NULL	)	/*The TableRowID of the parent claim.*/
		,( 147300,			'deductionReason'		,	'TEXT'	,		NULL	,							NULL	)	/*Deduction Reason*/
		,( 147301,			'deductionAmount'		,	'MONEY'	,		NULL	,							NULL	)	/*Deduction Amount*/
		,( 148432,			'itemNotCovered'		,	'TEXT'	,		NULL	,							NULL	)	/*Item Not Covered*/

	/*PAYMENT Table Detail Field Mapping*/
	INSERT @PaymentTableFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 154486, 			'payeeTitle'			,	'TEXT'	,		NULL	,							NULL	)	/*Payee Title*/
		,( 154487, 			'payeeFirstName'		,	'TEXT'	,		NULL	, 							NULL	)	/*Payee First Name*/
		,( 154488, 			'payeeLastName'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Last Name*/
		,( 154490, 			'payeeOrganisation'		,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Organisation*/
		,( 154491, 			'payeeAddress1'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Address 1*/
		,( 154492, 			'payeeAddress2'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Address 2*/
		,( 154493, 			'payeeAddress3'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Address 3*/
		,( 154494, 			'payeeAddress4'			,	'TEXT'	,		NULL	,							NULL	)	/*Payee Address 4*/
		,( 154495, 			'payeeAddress5'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Address 5*/
		,( 154496, 			'payeePostcode'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Postcode*/
		,( 154497, 			'payeeCountry'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Country*/
		,( 154498, 			'payeeReference'		,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Reference*/
		,( 154499, 			'payeeType'				,	'TEXT'	,		NULL	, 							NULL	)	/*Payee Type*/
		,( 154500, 			'policyholderTitle'		,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Title*/
		,( 154501, 			'policyholderFirstName'	,	'TEXT'	,		NULL	,							NULL	)	/*Policyholder First Name*/
		,( 154502, 			'policyholderLastName'	,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Last Name*/
		,( 154503, 			'policyholderOrganisation',	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Organisation*/
		,( 154504, 			'policyholderAddress1'	,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Address 1*/
		,( 154505, 			'policyholderAddress2'	,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Address 2*/
		,( 154506, 			'policyholderAddress3'	,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Address 3*/
		,( 154509, 			'policyholderPostcode'	,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Postcode*/
		,( 154510, 			'policyholderCountry'	,	'TEXT'	,		NULL	,							NULL	)	/*Policyholder Country*/
		,( 154511, 			'policyholderReference'	,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Reference*/
		,( 154512, 			'policyCode'			,	'TEXT'	,		NULL	, 							NULL	)	/*Policy Code*/
		,( 154513, 			'claimCode'				,	'TEXT'	,		NULL	, 							NULL	)	/*Claim Code*/
		,( 154514, 			'policyholderType'		,	'TEXT'	,		NULL	, 							NULL	)	/*Policyholder Type*/
		,( 154518,			'paymentAmount'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payment Amount*/
		,( 154517, 			'paymentType'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payment Type*/
		,( 154520, 			'paymentDate'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payment Date*/
		,( 158479, 			'dateCreated'			,	'TEXT'	,		NULL	,							NULL	)	/*Date Created*/
		,( 159005, 			'petName'				,	'TEXT'	,		NULL	, 							NULL	)	/*Pet Name*/
		,( 159407, 			'paymentApproved'		,	'TEXT'	,		NULL	, 							NULL	)	/*Payment Approved*/
		,( 159408, 			'payTo'					,	'TEXT'	,		NULL	, 							NULL	)	/*Pay To*/
		,( 161802, 			'affinityPartnerName'	,	'TEXT'	,		NULL	, 							NULL	)	/*Affinity Partner Name*/
		,( 170232, 			'paymentMethod'			,	'TEXT'	,		NULL	, 							NULL	)	/*Payment method*/
		,( 175270, 			'accountNumber'			,	'TEXT'	,		NULL	, 							NULL	)	/*Account Number*/
		,( 175271, 			'sortCode'				,	'TEXT'	,		NULL	,							NULL	)	/*Sort Code*/

	/*
		Match Claim to its Parent AQ Objects or Create New 
		--------------------------------------------------
	*/

	/*Pick up the Customer ID and policy admin Lead ID from GUIDs. The customer and pet will have come across in a previous transaction.*/
	SELECT	@PolicyID = ldv.LeadID
			,@NewCustomerID = l.CustomerID
	FROM LeadDetailValues ldv WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = ldv.LeadID
	WHERE ldv.DetailFieldID = 313787 /*API Transfer Mapping GUID*/
	AND ldv.DetailValue = CONVERT(VARCHAR(40), @ParentObjectID)
	
	IF @PolicyID is NULL
	BEGIN
		SELECT @ErrorMessage += 'No mapped Policy Admin LeadID or CustomerID found for ParentObjectID ' + ISNULL(CONVERT(VARCHAR(40), @ParentObjectID),'NULL') + @ErrorMessageSeparator
	END

	IF @HttpMethod = 'PUT'	/*= SQL Update*/
	BEGIN 
		/*Get the existing claim matter ID and claim lead ID by matching the mapping GUID that was saved when the matter was created*/
		SELECT TOP 1 @NewMatterID = mdv.MatterID
					,@NewLeadID	  = mdv.LeadID
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.DetailFieldID = 313796 /*API Transfer Mapping GUID*/
		AND mdv.DetailValue = @ObjectIDVchr

		IF @NewMatterID is NULL
		BEGIN
			SELECT @ErrorMessage += 'No mapped Claim MatterID found for ObjectID ' + ISNULL(CONVERT(VARCHAR(40), @ObjectID),'NULL') + @ErrorMessageSeparator
		END
	END

	/*If a validation error occurred, raise it - it will be caught in the calling procedure _C00_Mediate_Pop*/
	IF @ErrorMessage > ''
	BEGIN
		SELECT @ErrorMessage = SUBSTRING(@ErrorMessage, 1, LEN(@ErrorMessage)-(LEN(@ErrorMessageSeparator)-1)) /*Remove the final error separator*/

		RAISERROR (@ErrorMessage, 16, 1)

		RETURN
	END

	IF @HttpMethod = 'PUT'	/*= SQL Update*/
	BEGIN
		/*Get the existing case for the claim matter*/
		SELECT @NewCaseID = m.CaseID
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @NewMatterID

		/*Record when changes were made and who made them.*/
		UPDATE m
		SET  WhenModified	= @DateTimeCompleted
			,WhoModified	= @RunAsUserID
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @NewMatterID
	END
	ELSE	/*HTTP Method = POST = SQL Insert*/
	BEGIN
		/*Get the next case number*/
		SELECT TOP 1 @CaseNum = c.CaseNum + 1
		FROM Cases c WITH (NOLOCK) 
		WHERE c.LeadID = @NewLeadID
		ORDER BY c.CaseNum DESC
		
		/*Create a new Case*/
		INSERT INTO Cases (LeadID,ClientID,CaseNum,CaseRef,ClientStatusID,AquariumStatusID,DefaultContactID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@NewLeadID,@ClientID,@CaseNum,'Case ' + CONVERT(VARCHAR,(@CaseNum + 1)),NULL,2,NULL, @RunAsUserID, @DateTimeCompleted, @RunAsUserID, @DateTimeCompleted)

		SELECT @NewCaseID = SCOPE_IDENTITY()

		/*Get the case reference for the newly created case*/
		SELECT @RefLetter = dbo.fnRefLetterFromCaseNum(1+COUNT(*)) 
		FROM Matter WITH (NOLOCK)
		WHERE LeadID = @NewLeadID

		/*Create a new Matter*/
		INSERT INTO Matter([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID], WhoCreated, WhenCreated, WhoModified, WhenModified)
		VALUES (@ClientID,'',@NewCustomerID,@NewLeadID,1,@RefLetter,0,@NewCaseID, @RunAsUserID, @DateTimeCompleted, @RunAsUserID, @DateTimeCompleted)

		SELECT @NewMatterID = SCOPE_IDENTITY()

		/*Save the mapping GUID in the new claim so it can be matched for future updates.*/
		EXEC dbo._C00_SimpleValueIntoField 313796/*API Transfer Mapping GUID*/, @ObjectIDVchr, @NewMatterID, @RunAsUserID
	END

	/*Save the last HTTP method that was applied to the claim matter*/
	SELECT @HttpMethodID = lli.LookupListItemID
	FROM LookupListItems lli WITH (NOLOCK)
	WHERE lli.LookupListID = 11071 /*HTTP Method*/
	AND lli.ItemValue = @HttpMethod

	EXEC dbo._C00_SimpleValueIntoField 313797/*API Transfer Method*/, @HttpMethodID, @NewMatterID, @RunAsUserID

	/*
		Save Data to Detail Fields
		--------------------------
	*/

	/*Validate and save the Lead detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @XmlPayload
		,@XmlToDetailFieldMap	= @LeadFieldMapping
		,@ObjectID				= @NewLeadID
		,@ObjectTypeID			= 1 /*Lead*/
		,@TableDetailFieldID	= NULL
		,@RunAsUserID			= @RunAsUserID

	/*Validate and save the Matter detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @XmlPayload
		,@XmlToDetailFieldMap	= @MatterFieldMapping
		,@ObjectID				= @NewMatterID
		,@ObjectTypeID			= 2 /*Matter*/
		,@TableDetailFieldID	= NULL
		,@RunAsUserID			= @RunAsUserID

	/*
		Validate and save table detail values. The following rules apply:
			1) the source XML should contain data for all existing table rows and any new ones
			2) the XML should be in the order in which rows are to be inserted into the AQ table
			3) just the relevant table data should be extracted from the XML source data
			4) the table data should be restructured using the following generic attributes:
					<tablerows>
						<tablerow>row1_data</tablerow>
						<tablerow>row2_data</tablerow>
						...
					</tablerows>
	*/

	/* Extract, restructure and sort the source Claim table data*/
	SET @ClaimItemsXml = (
		SELECT ClaimItem.query('./*')
		FROM @XmlPayload.nodes('(//claimItems/ClaimItem)') claimItems(ClaimItem)
		ORDER BY ClaimItem.value('(treatmentStartDate)[1]','DATE')
		FOR XML RAW ('tablerow'), ROOT ('tablerows')
	)

	/*Validate and save the Claim table detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @ClaimItemsXml
		,@XmlToDetailFieldMap	= @ClaimTableFieldMapping
		,@ObjectID				= @NewMatterID
		,@ObjectTypeID			= 2 /*Matter*/
		,@TableDetailFieldID	= 144355 /*Claim Details Data*/
		,@RunAsUserID			= @RunAsUserID

	/* Extract, restructure and sort the source Deduction table data*/
	SET @DeductionsXml = (
		SELECT Deduction.query('./*')
		FROM @XmlPayload.nodes('(//userDeductions/UserDeduction)') deductions(Deduction)
		ORDER BY Deduction.value('(parentClaimRowId)[1]','INT')
			,Deduction.value('(parentClaimRowId)[1]','INT')
			,Deduction.value('(deductionReasonId)[1]','INT')
		FOR XML RAW ('tablerow'), ROOT ('tablerows')
	)

	/*Validate and Save the Deduction table detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @DeductionsXml
		,@XmlToDetailFieldMap	= @DeductionTableFieldMapping
		,@ObjectID				= @NewMatterID
		,@ObjectTypeID			= 2 /*Matter*/
		,@TableDetailFieldID	= 147302 /*Claim Detail Deductions*/
		,@RunAsUserID			= @RunAsUserID

	/* Extract, restructure and sort the source Payment table data*/
	SET @PaymentsXml = (
		SELECT Payment.query('./*')
		FROM @XmlPayload.nodes('(//payments/Payment)') payments(Payment)
		ORDER BY Payment.value('(paymentDate)[1]','DATE')
		FOR XML RAW ('tablerow'), ROOT ('tablerows')
	)

	/*Validate and save the Payments table detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @PaymentsXml
		,@XmlToDetailFieldMap	= @PaymentTableFieldMapping
		,@ObjectID				= @NewMatterID
		,@ObjectTypeID			= 2 /*Matter*/
		,@TableDetailFieldID	= 154485 /*Payment Data*/
		,@RunAsUserID			= @RunAsUserID

	/*
		Return Response, Debug Display (if required) and New AQ Object ID
		-----------------------------------------------------------------
	*/

	/*Assemble response and return it*/
	SELECT @JsonResponse = ISNULL(dbo.fn_C600_Harvest_Claim_JSON(@NewMatterID,0), '{}')

	/*Return all the saved claim data*/
	IF @ShowNewClaim = 1
	BEGIN
		/*Lead detail fields*/
		SELECT	'Lead DF'
				,l.CustomerID		[CustomerID]
				,l.LeadID			[LeadID]
				,ldv.DetailFieldID	[DetailFieldID]
				,ldv.DetailValue	[DetailValue]
		FROM Lead l WITH (NOLOCK) 
		CROSS APPLY @LeadFieldMapping lfm
		LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID 
		AND ldv.DetailFieldID = lfm.DetailFieldID
		WHERE l.LeadID = @NewLeadID
		ORDER BY ldv.DetailFieldID

		/*Matter detail fields*/
		SELECT	 'Matter DF'
				,m.CustomerID		[CustomerID]
				,m.LeadID			[LeadID]
				,m.MatterID			[MatterID]
				,mdv.DetailFieldID	[DetailFieldID]
				,mdv.DetailValue	[DetailValue]
		FROM Matter m WITH (NOLOCK) 
		CROSS APPLY @MatterFieldMapping mfm
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID 
		AND mdv.DetailFieldID = mfm.DetailFieldID
		WHERE m.MatterID = @NewMatterID
		ORDER BY mdv.DetailFieldID

		/*Claim table detail fields*/
		SELECT	 'Claim table DF'
				,m.CustomerID		[MappedCustomerID]
				,m.LeadID			[MappedParentID]
				,m.MatterID			[MappedClaimID]
				,tdv.TableRowID		[TableRowID]
				,tdv.DetailFieldID	[DetailFieldID]
				,tdv.DetailValue	[DetailValue]
				,tdv.ResourceListID	[ResourceListID]
				,tr.SourceID		[TableRows.SourceID]
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID
			AND tr.DetailFieldID = 144355 /*Claim Details Data*/
		CROSS APPLY @ClaimTableFieldMapping ctfm
		LEFT JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			AND tdv.DetailFieldID = ctfm.DetailFieldID
		WHERE m.MatterID = @NewMatterID
		ORDER BY tdv.TableRowID
			,tdv.DetailFieldID

		/*Deductions table detail fields*/
		SELECT	 'Deductions  table DF'
				,m.CustomerID		[MappedCustomerID]
				,m.LeadID			[MappedParentID]
				,m.MatterID			[MappedClaimID]
				,tdv.TableRowID		[TableRowID]
				,tdv.DetailFieldID	[DetailFieldID]
				,tdv.DetailValue	[DetailValue]
				,tdv.ResourceListID	[ResourceListID]
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID
			AND tr.DetailFieldID = 147302 /*Claim Detail Deductions*/
		CROSS APPLY @DeductionTableFieldMapping dtfm
		LEFT JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			AND tdv.DetailFieldID = dtfm.DetailFieldID
		WHERE m.MatterID = @NewMatterID
		ORDER BY tdv.TableRowID
			,tdv.DetailFieldID

		/*Payment table detail fields*/
		SELECT	 'Payment table DF'
				,m.CustomerID		[CustomerID]
				,m.LeadID			[LeadID]
				,m.MatterID			[MatterID]
				,tdv.TableRowID		[TableRowID]
				,tdv.DetailFieldID	[DetailFieldID]
				,tdv.DetailValue	[DetailValue]
				,tdv.ResourceListID	[ResourceListID]
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID
			AND tr.DetailFieldID = 154485 /*Payment Data*/
		CROSS APPLY @PaymentTableFieldMapping ptfm
		LEFT JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			AND tdv.DetailFieldID = ptfm.DetailFieldID
		WHERE m.MatterID = @NewMatterID
		ORDER BY tdv.TableRowID
			,tdv.DetailFieldID

	END

	RETURN @NewMatterID

END














GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Claim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_API_Receive_Claim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Claim] TO [sp_executeall]
GO
