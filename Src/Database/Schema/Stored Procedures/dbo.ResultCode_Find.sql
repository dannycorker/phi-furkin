SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ResultCode table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResultCode_Find]
(

	@SearchUsingOR bit   = null ,

	@ResultCodeID varchar (4)  = null ,

	@Description varchar (200)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ResultCodeID]
	, [Description]
    FROM
	[dbo].[ResultCode] WITH (NOLOCK) 
    WHERE 
	 ([ResultCodeID] = @ResultCodeID OR @ResultCodeID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ResultCodeID]
	, [Description]
    FROM
	[dbo].[ResultCode] WITH (NOLOCK) 
    WHERE 
	 ([ResultCodeID] = @ResultCodeID AND @ResultCodeID is not null)
	OR ([Description] = @Description AND @Description is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResultCode_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Find] TO [sp_executeall]
GO
