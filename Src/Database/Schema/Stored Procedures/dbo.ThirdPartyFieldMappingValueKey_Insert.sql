SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyFieldMappingValueKey table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_Insert]
(

	@ThirdPartyFieldMappingValueKeyID int    OUTPUT,

	@ClientID int   ,

	@FieldMappingID int   ,

	@DetailValue varchar (2000)  ,

	@LookupListItemID int   ,

	@ThirdPartyKey varchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ThirdPartyFieldMappingValueKey]
					(
					[ClientID]
					,[FieldMappingID]
					,[DetailValue]
					,[LookupListItemID]
					,[ThirdPartyKey]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@FieldMappingID
					,@DetailValue
					,@LookupListItemID
					,@ThirdPartyKey
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ThirdPartyFieldMappingValueKeyID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Insert] TO [sp_executeall]
GO
