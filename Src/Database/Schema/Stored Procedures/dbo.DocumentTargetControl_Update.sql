SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_Update]
(

	@DocumentTargetControlID int   ,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@LastParsed datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentTargetControl]
				SET
					[ClientID] = @ClientID
					,[DocumentTypeID] = @DocumentTypeID
					,[LastParsed] = @LastParsed
				WHERE
[DocumentTargetControlID] = @DocumentTargetControlID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Update] TO [sp_executeall]
GO
