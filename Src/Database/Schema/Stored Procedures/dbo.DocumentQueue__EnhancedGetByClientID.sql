SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By:  (Ben)
-- Purpose: Select records from the DocumentQueue table through a foreign key
-- JWG 2009-07-02 Make it work. And speed it up. Added DISTINCT to cater for scenario where the Case is 
--                split while a document is in the queue, so 2 LeadEvent records
--                now point to the same DocumentQueueID.
-- AMG 2011-02-08 Added LeadType Filter
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DocumentQueue__EnhancedGetByClientID]
(
	@ClientID int,
	@LeadTypeID int = NULL
)
AS
BEGIN
	
	/**/
	SELECT DISTINCT 
	dq.[DocumentQueueID], 
	dq.[ClientID], 
	dq.[LeadID], 
	dq.[CaseID], 
	l.[LeadTypeID], 
	dt.[DocumentTypeID], 
	dt.[DocumentTypeName], 
	et.[EventTypeName], 
	cp.[UserName], 
	dq.[WhenStored], 
	dq.[WhenCreated], 
	dq.[RequiresApproval]
	FROM		DocumentQueue dq (nolock)
	INNER JOIN  LeadEvent le (nolock) on le.DocumentQueueID = dq.DocumentQueueId
	INNER JOIN	Lead l (nolock) on dq.LeadID = l.LeadID
	INNER JOIN	DocumentType dt (nolock) on dq.DocumentTypeID = dt.DocumentTypeID
	INNER JOIN	EventType et (nolock) on le.EventTypeID = et.EventTypeID
	INNER JOIN	ClientPersonnel cp (nolock) on dq.WhoCreated = cp.ClientPersonnelID 
	LEFT JOIN   DocumentZip dz (nolock) on dz.[DocumentQueueID] = dq.[DocumentQueueID] 
	WHERE dq.ClientID = @ClientID 
	AND dq.[WhenCreated] IS NULL
	AND dz.[DocumentQueueID] IS NULL 
	--AND (@ClientID <> 57 OR (@ClientID = 57 AND dt.DocumentTypeID <> 14258))
	AND (@LeadTypeID IS NULL OR l.LeadTypeID = @LeadTypeID)
	ORDER BY dq.[DocumentQueueID] asc


/*
	SELECT		dq.[DocumentQueueID]
				,dq.[ClientID]
				,dq.[LeadID]
				,dq.[CaseID]
				,l.[LeadTypeID]
				,dt.[DocumentTypeID]
				,dt.[DocumentTypeName]
				,et.[EventTypeName]
				,cp.[UserName]
				,dq.[WhenStored]
				,dq.[WhenCreated]
				,dq.[RequiresApproval]
	FROM		LeadEvent le
	LEFT JOIN	DocumentQueue dq on le.DocumentQueueID = dq.DocumentQueueId
	LEFT JOIN	Lead l		 on dq.LeadID = l.LeadID
	LEFT JOIN	DocumentType dt on dq.DocumentTypeID = dt.DocumentTypeID
	LEFT JOIN	EventType et on le.EventTypeID = et.EventTypeID
	LEFT JOIN	ClientPersonnel cp on dq.WhoCreated = cp.ClientPersonnelID
	WHERE dq.ClientID = @ClientID
	AND dq.[WhenCreated] is null
	AND NOT EXISTS (
		SELECT * 
		FROM DocumentZip dz 
		WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
	)
	ORDER BY dq.[DocumentQueueID] asc
*/
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue__EnhancedGetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue__EnhancedGetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue__EnhancedGetByClientID] TO [sp_executeall]
GO
