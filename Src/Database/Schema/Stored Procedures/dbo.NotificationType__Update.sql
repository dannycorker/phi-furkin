SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Updates a notification type record
-- =============================================
CREATE PROCEDURE [dbo].[NotificationType__Update]

	@NotificationTypeID INT, 
	@ClientID INT,
	@SubClientID INT,
	@Name VARCHAR(50),
	@BackgroundColour VARCHAR(20),
	@ForeColour VARCHAR(20)

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [NotificationType]
	SET [ClientID] = @ClientID,
		[SubClientID] = @SubClientID,
		[Name] = @Name,
		[BackgroundColour] = @BackgroundColour,
		[ForeColour] = @ForeColour
	WHERE NotificationTypeID = @NotificationTypeID
 
END





GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationType__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__Update] TO [sp_executeall]
GO
