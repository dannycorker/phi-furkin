SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget_Update]
(

	@DocumentSpecialisedDetailFieldTargetID int   ,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@RowField int   ,

	@Notes varchar (250)  ,

	@ExcelSheetLocation varchar (50)  ,

	@Format varchar (2000)  ,

	@DocumentTypeVersionID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentSpecialisedDetailFieldTarget]
				SET
					[ClientID] = @ClientID
					,[DocumentTypeID] = @DocumentTypeID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[ColumnField] = @ColumnField
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[ColumnFieldAlias] = @ColumnFieldAlias
					,[RowField] = @RowField
					,[Notes] = @Notes
					,[ExcelSheetLocation] = @ExcelSheetLocation
					,[Format] = @Format
					,[DocumentTypeVersionID] = @DocumentTypeVersionID
				WHERE
[DocumentSpecialisedDetailFieldTargetID] = @DocumentSpecialisedDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Update] TO [sp_executeall]
GO
