SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM (Gavin Reynolds / Alexandra Maguire)
-- Create date: 2020-09-15
-- Description:	Sql After Event - Endorse Policy
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_158999]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@TermsDate						DATE
			,@AdjustmentDate				DATE
			,@Count							INT
			,@ValueInt						INT
			,@DetailValueData				dbo.tvpDetailValueData
			,@EventComments					VARCHAR(2000)
			,@NextEventID					INT
			,@IdId							dbo.tvpIntInt
			,@WorkflowTaskID				INT
			,@ResourceListID				INT
			,@RowCount						INT
			,@PolicyOneVisionID				INT
			,@WellnessOneVisionID			INT
			,@ExamFeeOneVisionID			INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	IF @EventTypeID IN (158999) -- begin - Endorse Policy
	BEGIN
		
		DECLARE @ClaimsPaid INT

		SELECT @ClaimsPaid = ISNULL((SELECT COUNT(*)
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = cps.PaymentStatusID
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID AND AccountUseID = 2
		INNER JOIN OneVisionClaimPayment claimdata WITH (NOLOCK) ON claimdata.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		INNER JOIN OneVisionClaim visionclaim WITH (NOLOCK) ON visionclaim.OneVisionClaimID = claimdata.OneVisionClaimID
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.ObjectID = @MatterID
		WHERE cps.ClientID = @ClientID
		AND visionclaim.PAMatterID = pp.ObjectID
		AND cps.ActualCollectionDate BETWEEN pp.ValidFrom AND pp.ValidTo),0)

		IF  @ClaimsPaid >= 1
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>Endorse Policy not permitted as Paid Claims exist for this Policy.</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		SELECT @EventComments = ''
		
		SET @NextEventID = 159000 -- Mid Term Adjustment Policy 		
		
		EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @NextEventID, @WhoCreated, 0

		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

		/*copy adjustment date to field to use for docs*/ /*NG 2020-12-09 for JIRA SDPRU-187*/
		EXEC _C600_CopyFieldToField 175442, 314353, @MatterID, @WhoCreated /*Adjustment or Cancellation Date => MTA Date*/

		/*UAH 07/11/2018 - As there is only 1 MTA Workflow group, I am adding SAE on all events which relate to	MTA's to remove workflow tasks once these MTA events have been applied*/
		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) where w.caseid = @caseID
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID, WorkflowGroupID, AutomatedTaskID, Priority, AssignedTo, AssignedDate, LeadID, CaseID, EventTypeID, FollowUp, Important, CreationDate, Escalated, EscalatedBy, EscalationReason, EscalationDate, Disabled, CompletedBy, CompletedOn, CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, 	@WhoCreated, dbo.fn_GetDate_Local(),	'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE CaseID = @CaseID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 
		AND WorkflowGroupID = 1239 

		/*2020-10-16 ALM*/
		SELECT @PolicyOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

		SELECT @WellnessOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness'

		IF @WellnessOneVisionID IS NULL AND dbo.fnGetSimpleDvAsInt(314244, @LeadID) > 0 
		BEGIN  

			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			SELECT ov.VisionCustomerID, ov.VisionPolicyID, ov.VisionPetID, NULL, ov.CustomerID, ov.PALeadID, ov.PAMatterID, ov.HistoricalPolicyTableRow, ov.VisionAddressID, 'Wellness'
			FROM OneVision ov 
			WHERE ov.OneVisionID = @PolicyOneVisionID

		END 

		SELECT @ExamFeeOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'

		IF @ExamFeeOneVisionID IS NULL AND dbo.fnGetSimpleDvAsInt(314245, @LeadID) = 5144
		BEGIN  

			INSERT INTO OneVision ([VisionCustomerID], [VisionPolicyID], [VisionPetID], [VisionPolicyTermID], [CustomerID], [PALeadID], [PAMatterID], [HistoricalPolicyTableRow], [VisionAddressID], [PolicyTermType])
			SELECT ov.VisionCustomerID, ov.VisionPolicyID, ov.VisionPetID, NULL, ov.CustomerID, ov.PALeadID, ov.PAMatterID, ov.HistoricalPolicyTableRow, ov.VisionAddressID, 'ExamFee'
			FROM OneVision ov 
			WHERE ov.OneVisionID = @PolicyOneVisionID

		END 

	END
	
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END

GO
