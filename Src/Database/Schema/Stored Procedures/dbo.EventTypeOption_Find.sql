SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeOption_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeOptionID int   = null ,

	@ClientID int   = null ,

	@EventTypeID int   = null ,

	@AquariumOptionID int   = null ,

	@OptionValue varchar (50)  = null ,

	@ValueInt int   = null ,

	@ValueMoney money   = null ,

	@ValueDate date   = null ,

	@ValueDateTime datetime2   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeOptionID]
	, [ClientID]
	, [EventTypeID]
	, [AquariumOptionID]
	, [OptionValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
    FROM
	[dbo].[EventTypeOption] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeOptionID] = @EventTypeOptionID OR @EventTypeOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([AquariumOptionID] = @AquariumOptionID OR @AquariumOptionID IS NULL)
	AND ([OptionValue] = @OptionValue OR @OptionValue IS NULL)
	AND ([ValueInt] = @ValueInt OR @ValueInt IS NULL)
	AND ([ValueMoney] = @ValueMoney OR @ValueMoney IS NULL)
	AND ([ValueDate] = @ValueDate OR @ValueDate IS NULL)
	AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeOptionID]
	, [ClientID]
	, [EventTypeID]
	, [AquariumOptionID]
	, [OptionValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
    FROM
	[dbo].[EventTypeOption] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeOptionID] = @EventTypeOptionID AND @EventTypeOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([AquariumOptionID] = @AquariumOptionID AND @AquariumOptionID is not null)
	OR ([OptionValue] = @OptionValue AND @OptionValue is not null)
	OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
	OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
	OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
	OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Find] TO [sp_executeall]
GO
