SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Messages table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_GetByPortalUserIDFrom]
(

	@PortalUserIDFrom int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[MessageID],
					[ClientPersonnelIDTo],
					[ClientPersonnelIDFrom],
					[DateSent],
					[DateRead],
					[Status],
					[Subject],
					[MessageText],
					[DateReplied],
					[DateForwarded],
					[PreviousMessageID],
					[ClientID],
					[PortalUserIDTo],
					[PortalUserIDFrom]
				FROM
					[dbo].[Messages] WITH (NOLOCK) 
				WHERE
					[PortalUserIDFrom] = @PortalUserIDFrom
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_GetByPortalUserIDFrom] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_GetByPortalUserIDFrom] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_GetByPortalUserIDFrom] TO [sp_executeall]
GO
