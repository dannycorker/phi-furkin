SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- ALTER date: 2009-08-20
-- Description:	List all AutomatedTasks for a client.
-- JWG 2013-02-28 Only show tasks for clients who are still in the current database.
-- =============================================
CREATE PROCEDURE [dbo].[at] 
	@ClientID int = NULL, 
	@ClientPersonnelID int = NULL, 
	@EmailAddress varchar(250) = NULL 
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ServerName sysname, @DatabaseName sysname

	SELECT @ServerName = CAST(SERVERPROPERTY('Servername') AS sysname), @DatabaseName = DB_NAME()

	SELECT @ServerName AS [Current Server], @DatabaseName AS [Current Database]
	
	/*
		Show any tasks that are running right now.
		This includes any that claim to be running but aren't any
		longer if the scheduler crashed or we shut it down.
	*/
	SELECT 'Already Running' as [Running Now], at.*, ati.* 
	FROM dbo.AutomatedTask at WITH (NOLOCK) 
	INNER JOIN [AquariusMaster].dbo.ClientDatabase cd WITH (NOLOCK) ON cd.ClientID = at.ClientID AND cd.ServerName = @ServerName AND cd.DatabaseName = @DatabaseName
	LEFT JOIN dbo.AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID 
	WHERE at.AlreadyRunning = 1

	/*
		Show any tasks that have been allocated to a particular
		task scheduler and are about to run.
	*/
	SELECT 'Locked to a Scheduler' as [Locked In], ati.* , 
	'exec dbo.AutomatedTask__ClearLockedTasks ' + CONVERT(varchar,ati.QueueID)+ ',' + CONVERT(varchar,ati.SchedulerID) + ',''' + ati.ServerName + ''',' + CONVERT(varchar,ati.TaskID) /*Added by CS 2011-09-07*/
	FROM dbo.AutomatedTaskInfo ati WITH (NOLOCK) 
	WHERE ati.LockDateTime IS NOT NULL

	/* Show tasks for this client */
	IF @ClientID IS NOT NULL 
	BEGIN

		/* Show tasks that affect this user in any way */
		IF @ClientPersonnelID IS NOT NULL
		BEGIN
		
			/* Get task details, with all params, where this ClientPersonnel shows up in any of the params whatsoever. */
			SELECT atp.ParamName, atp.ParamValue, atp.AutomatedTaskParamID, at.* 
			FROM dbo.AutomatedTask at WITH (NOLOCK) 
			INNER JOIN dbo.AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = at.TaskID 
			WHERE at.TaskID IN (
			
				SELECT atp.TaskID 
				FROM dbo.AutomatedTaskParam atp WITH (NOLOCK) 
				WHERE atp.ClientID = @ClientID 
				AND atp.ParamName = 'RUN_AS_USERID' 
				AND atp.ParamValue = @ClientPersonnelID
				
				UNION
				
				SELECT atp.TaskID 
				FROM dbo.AutomatedTaskParam atp WITH (NOLOCK)
				WHERE atp.ClientID = @ClientID 
				AND atp.ParamName IN (
					'EMAIL_FROM',
					'EMAIL_TO',
					'EMAIL_CC',
					'EMAIL_BCC',
					'ALERT_ON_ERROR_EMAIL'
				)
				AND atp.ParamValue LIKE @EmailAddress
			)
			ORDER BY at.Taskname, atp.ParamName 
		END
		ELSE
		BEGIN
			/* Just show a list of all tasks for this client */
			IF @ClientID IS NOT NULL
			BEGIN
				SELECT at.* 
				FROM dbo.AutomatedTask at WITH (NOLOCK)
				WHERE at.ClientID = @ClientID 
				AND at.[Enabled] = 1 
				AND at.NextRunDateTime IS NOT NULL
				ORDER BY at.NextRunDateTime 
			END
		END
	END
	
	/* Show a quick count of all waiting tasks for all clients */
	SELECT COUNT(*) AS [Total Waiting Tasks], dbo.fn_GetDate_Local() [Database DateTime]
	FROM dbo.AutomatedTask at WITH (NOLOCK) 
	WHERE at.NextRunDateTime < dbo.fn_GetDate_Local()
	AND at.Enabled = 1
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[at] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[at] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[at] TO [sp_executeall]
GO
