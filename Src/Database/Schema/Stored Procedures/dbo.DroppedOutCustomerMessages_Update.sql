SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DroppedOutCustomerMessages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerMessages_Update]
(

	@DroppedOutCustomerMessageID int   ,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@EmailFromAddress varchar (255)  ,

	@EmailSubject varchar (500)  ,

	@Email varchar (MAX)  ,

	@SmsMessage varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DroppedOutCustomerMessages]
				SET
					[ClientID] = @ClientID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[EmailFromAddress] = @EmailFromAddress
					,[EmailSubject] = @EmailSubject
					,[Email] = @Email
					,[SmsMessage] = @SmsMessage
				WHERE
[DroppedOutCustomerMessageID] = @DroppedOutCustomerMessageID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerMessages_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Update] TO [sp_executeall]
GO
