SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIClientPersonnelCredential table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientPersonnelCredential_GetByRPIClientPersonnelCredentialID]
(

	@RPIClientPersonnelCredentialID int   
)
AS


				SELECT
					[RPIClientPersonnelCredentialID],
					[ClientID],
					[ClientPersonnelID],
					[UserName],
					[Password],
					[AsUser],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[RPIClientPersonnelCredential] WITH (NOLOCK) 
				WHERE
										[RPIClientPersonnelCredentialID] = @RPIClientPersonnelCredentialID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_GetByRPIClientPersonnelCredentialID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientPersonnelCredential_GetByRPIClientPersonnelCredentialID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_GetByRPIClientPersonnelCredentialID] TO [sp_executeall]
GO
