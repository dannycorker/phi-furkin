SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-09-15
-- Description:	Calculates and then stores composite commission and discount checkpoint values 
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Premium_StoreDerivedCalculationValues] 
(
	@ClientID INT,
	@PremiumCalculationID INT	
)

AS
BEGIN

	SET NOCOUNT ON;

--declare 	
--	@ClientID INT = 327,
--	@PremiumCalculationID INT = 10072	
	
	
	/* 
	
		Aggregate checkpoint values where an aggregation is defined in the MI Report Checkpoint Aggregation RL 
		Method 1: checkpoints to be aggregated are adjacent - we provide input, out and delta values
		Some rules:
			* checkpoints to be aggregated must be for adjacent rules (this is not policed)
			* the input value is taken from the first checkpoint in the aggregation series
			* the output values are taken from the last checkpoint in the aggregation series
			* the delta values are calculated from the new input and output values
		Method 2: checkpoints to be aggregated are not (necessarlity) adjacent. We provide just delta values
		
		Methods are exclusive. Method 2 is current live method.
	
	*/
	
	DECLARE @CheckpointAggregation TABLE (
								ID INT IDENTITY(1,1),
								PremiumCalculationID INT,
								RuleCheckpointPrefix VARCHAR(100),
								RuleCheckpoint VARCHAR(100),
								RuleInput VARCHAR(100),
								RuleOutput VARCHAR(100),
								RuleDelta VARCHAR(100),
								RuleSequence INT
								)
	---- METHOD 1
	--INSERT INTO @CheckpointAggregation
	--SELECT pcdv.PremiumCalculationDetailID,rcp.DetailValue,vcol.DetailValue,NULL,NULL,NULL,MIN(pcdv.RuleSequence) 
	--FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK)
	--INNER JOIN ResourceListDetailValues rcp WITH (NOLOCK) ON pcdv.RuleCheckpoint LIKE rcp.DetailValue + '%' AND rcp.DetailFieldID=175723
	--INNER JOIN ResourceListDetailValues vcol WITH (NOLOCK) ON rcp.ResourceListID=vcol.ResourceListID AND vcol.DetailFieldID=175725
	--INNER JOIN ResourceListDetailValues vname WITH (NOLOCK) ON rcp.ResourceListID=vname.ResourceListID AND vname.DetailFieldID=175724
	--WHERE pcdv.PremiumCalculationDetailID=@PremiumCalculationID
	--	AND vname.DetailValue='AlphaRisk'
	--GROUP BY pcdv.PremiumCalculationDetailID,rcp.DetailValue,vcol.DetailValue
	---- min
	--UPDATE ca
	--SET RuleInput=pcdv.RuleInput
	--FROM @CheckpointAggregation ca
	--INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pcdv.PremiumCalculationDetailID=ca.PremiumCalculationID
	--	AND pcdv.RuleCheckpoint LIKE ca.RuleCheckpointPrefix + '%'
	--WHERE NOT EXISTS ( SELECT * FROM PremiumCalculationDetailValues pcdv2 WITH (NOLOCK) 
	--						WHERE pcdv2.PremiumCalculationDetailID=pcdv.PremiumCalculationDetailID 
	--							AND pcdv2.RuleCheckpoint=pcdv.RuleCheckpoint
	--							AND pcdv2.RuleSequence<pcdv.RuleSequence
	--				 )
	---- max
	--UPDATE ca
	--SET RuleOutput=pcdv.RuleOutput
	--FROM @CheckpointAggregation ca
	--INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pcdv.PremiumCalculationDetailID=ca.PremiumCalculationID
	--	AND pcdv.RuleCheckpoint LIKE ca.RuleCheckpointPrefix + '%'
	--WHERE NOT EXISTS ( SELECT * FROM PremiumCalculationDetailValues pcdv2 WITH (NOLOCK) 
	--						WHERE pcdv2.PremiumCalculationDetailID=pcdv.PremiumCalculationDetailID 
	--							AND pcdv2.RuleCheckpoint=pcdv.RuleCheckpoint
	--							AND pcdv2.RuleSequence>pcdv.RuleSequence
	--				 )
	---- Delta
	--UPDATE @CheckpointAggregation SET RuleDelta = CAST(CAST(RuleOutput AS FLOAT)-CAST(RuleInput AS FLOAT) AS VARCHAR)

	-- METHOD 2
	INSERT INTO @CheckpointAggregation
	SELECT pcdv.PremiumCalculationDetailID,rcp.DetailValue,vcol.DetailValue,0,0,CAST(SUM(CAST(pcdv.RuleInputOutputDelta AS FLOAT)) AS VARCHAR),1 
	FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues rcp WITH (NOLOCK) ON pcdv.RuleCheckpoint LIKE rcp.DetailValue + '%' AND rcp.DetailFieldID=175723
	INNER JOIN ResourceListDetailValues vcol WITH (NOLOCK) ON rcp.ResourceListID=vcol.ResourceListID AND vcol.DetailFieldID=175725
	INNER JOIN ResourceListDetailValues vname WITH (NOLOCK) ON rcp.ResourceListID=vname.ResourceListID AND vname.DetailFieldID=175724
	WHERE pcdv.PremiumCalculationDetailID=@PremiumCalculationID
		AND vname.DetailValue='AlphaRisk'
	GROUP BY pcdv.PremiumCalculationDetailID,rcp.DetailValue,vcol.DetailValue

	-- now insert a new PCDV row for each of the aggregates found
	DECLARE @ID INT,
			@Delta VARCHAR(100),
			@RuleCheckpoint VARCHAR(100),
			@RuleInput VARCHAR(100),
			@RuleOutput VARCHAR(100),
			@ValueFirstMonth VARCHAR(100),
			@ValueOtherMonth VARCHAR(100),
			@DeltaFirstMonth VARCHAR(100),
			@DeltaOtherMonth VARCHAR(100)
			
	WHILE EXISTS ( SELECT * FROM @CheckpointAggregation ca WHERE ca.RuleSequence>0 )
	BEGIN
	
		SELECT @ID=ID,
				@RuleInput=RuleInput,
				@RuleOutput=RuleOutput,
				@PremiumCalculationID=PremiumCalculationID,
				@RuleCheckpoint=RuleCheckpoint,
				@Delta=RuleDelta
		FROM @CheckpointAggregation ca WHERE ca.RuleSequence>0
		
		/* Do not round at this stage */  
		--SELECT	@ValueFirstMonth = CAST(FirstMonthly AS VARCHAR),
		--		@ValueOtherMonth = CAST(RecurringMonthly AS VARCHAR)
		--FROM dbo.fn_C00_1273_SplitPremium(@RuleOutput)
				
		--SELECT	@DeltaFirstMonth = CAST(FirstMonthly AS VARCHAR),
		--		@DeltaOtherMonth = CAST(RecurringMonthly AS VARCHAR)
		--FROM dbo.fn_C00_1273_SplitPremium(CAST(@Delta AS FLOAT))
			
		SELECT	@ValueFirstMonth = FirstMonthly,
				@ValueOtherMonth = RecurringMonthly
		FROM dbo.fn_C00_1273_StorePremiumCalcSplitPremium(@RuleOutput)
				
		SELECT	@DeltaFirstMonth = FirstMonthly,
				@DeltaOtherMonth = RecurringMonthly
		FROM dbo.fn_C00_1273_StorePremiumCalcSplitPremium(@Delta)

		INSERT INTO dbo.PremiumCalculationDetailValues (ClientID,PremiumCalculationDetailID,RuleSequence,RuleID,RuleInput,RuleCheckpoint,RuleTransform,RuleTransformValue,RuleOutput,RuleOutputFirstMonth,RuleOutputOtherMonth,RuleInputOutputDelta,RuleInputOutputDeltaFirstMonth,RuleInputOutputDeltaOtherMonth) VALUES
		(@ClientID,@PremiumCalculationID,0,-2,@RuleInput,@RuleCheckpoint,'','',@RuleOutput,@ValueFirstMonth,@ValueOtherMonth,@Delta,@DeltaFirstMonth,@DeltaOtherMonth)
		
		UPDATE @CheckpointAggregation SET RuleSequence=0 WHERE ID=@ID
	
	END	 
	
	/* 
	
		Commission values may be aggregated.  We need to insert a PCDV row for each of the split commission columns 
		and also for the split required by the view (which may be a partial aggregation of the full split).
		
	*/
	
	DECLARE @Commission TABLE (
								ID INT IDENTITY(1,1),
								PremiumCalculationID INT,
								RuleCheckpoint VARCHAR(100),
								RuleDelta VARCHAR(100),
								RuleDeltaFirstMonth VARCHAR(100),
								RuleDeltaOtherMonth VARCHAR(100),
								Done INT
								)
	INSERT INTO @Commission
	SELECT pcdv.PremiumCalculationDetailID,rcp.DetailValue,pcdv.RuleInputOutputDelta,pcdv.RuleInputOutputDeltaFirstMonth,pcdv.RuleInputOutputDeltaOtherMonth,0 
	FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues rcp WITH (NOLOCK) ON rcp.DetailValue = pcdv.RuleCheckpoint AND rcp.DetailFieldID=175731
	INNER JOIN ResourceListDetailValues vn WITH (NOLOCK) ON rcp.ResourceListID = vn.ResourceListID AND vn.DetailFieldID=175728
	WHERE pcdv.PremiumCalculationDetailID=@PremiumCalculationID
		AND vn.DetailValue='AlphaRisk'
		AND NOT EXISTS ( SELECT * FROM ResourceListDetailValues rcp2 WITH (NOLOCK) 
						 WHERE rcp2.ResourceListID>rcp.ResourceListID AND rcp2.DetailValue=rcp.DetailValue AND rcp2.DetailFieldID=rcp.DetailFieldID )  

	DECLARE @CommissionCheckpoint VARCHAR(100)
	SELECT TOP 1 @CommissionCheckpoint=c.RuleCheckpoint FROM @Commission c

	DECLARE @CommissionSplits TABLE (
									RuleCheckpoint VARCHAR(100),
									ComType VARCHAR(100),
									ComPercent FLOAT,
									TakesRemainder INT,
									IsViewSet INT,
									Done INT
									)
	-- Individual Set of Commissions
	INSERT INTO @CommissionSplits
	SELECT rcp.DetailValue,ct.DetailValue,cp.ValueMoney,tr.ValueInt,0,0 
	FROM ResourceListDetailValues rcp WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues cp WITH (NOLOCK) ON rcp.ResourceListID=cp.ResourceListID AND cp.DetailFieldID=175727
	INNER JOIN ResourceListDetailValues tr WITH (NOLOCK) ON rcp.ResourceListID=tr.ResourceListID AND tr.DetailFieldID=175730
	INNER JOIN ResourceListDetailValues ct WITH (NOLOCK) ON rcp.ResourceListID=ct.ResourceListID AND ct.DetailFieldID=175726
	INNER JOIN ResourceListDetailValues vn WITH (NOLOCK) ON rcp.ResourceListID=vn.ResourceListID AND vn.DetailFieldID=175728
	WHERE rcp.DetailFieldID=175731 
		AND vn.DetailValue='AlphaRisk'
		AND rcp.DetailValue=@CommissionCheckpoint
	
	-- View Set of Commissions (may be some or even complete aggregation of individuals)
	INSERT INTO @CommissionSplits
	SELECT rcp.DetailValue,vc.DetailValue,SUM(cp.ValueMoney),5145,1,0
	FROM @CommissionSplits cs
	INNER JOIN ResourceListDetailValues rcp WITH (NOLOCK) ON cs.RuleCheckpoint=rcp.DetailValue AND rcp.DetailFieldID=175731
	INNER JOIN ResourceListDetailValues vc WITH (NOLOCK) ON rcp.ResourceListID=vc.ResourceListID AND vc.DetailFieldID=175729
	INNER JOIN ResourceListDetailValues vn WITH (NOLOCK) ON rcp.ResourceListID=vn.ResourceListID AND vn.DetailFieldID=175728
	INNER JOIN ResourceListDetailValues cp WITH (NOLOCK) ON rcp.ResourceListID=cp.ResourceListID AND cp.DetailFieldID=175727
	INNER JOIN ResourceListDetailValues tr WITH (NOLOCK) ON rcp.ResourceListID=tr.ResourceListID AND tr.DetailFieldID=175730
	INNER JOIN ResourceListDetailValues ct WITH (NOLOCK) ON rcp.ResourceListID=ct.ResourceListID AND ct.DetailFieldID=175726
		AND ct.DetailValue=cs.ComType
	WHERE vn.DetailValue='AlphaRisk'
	GROUP BY rcp.DetailValue,vc.DetailValue
		
	UPDATE cs
	SET TakesRemainder=5144
	FROM @CommissionSplits cs
	WHERE EXISTS ( SELECT * FROM ResourceListDetailValues vc WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues tr WITH (NOLOCK) ON vc.ResourceListID=tr.ResourceListID AND tr.DetailFieldID=175730
				INNER JOIN ResourceListDetailValues vn WITH (NOLOCK) ON vc.ResourceListID=vn.ResourceListID AND vn.DetailFieldID=175728
				WHERE vc.DetailValue=cs.ComType 
					AND vc.DetailFieldID=175729
					AND vn.DetailValue='AlphaRisk'
					AND tr.ValueInt=5144
				)

	---- now insert a new PremiumCalculationDetailValues row for each of the splits found

	DECLARE @CumulativeCom FLOAT,
			@CumulativeComFirst FLOAT,
			@CumulativeComOther FLOAT,
			@ComPercent FLOAT,
			@ComType VARCHAR(100),
			@ComDelta FLOAT,
			@ComFirst FLOAT,
			@ComOther FLOAT,
			@IsViewSet INT,
			@PercentTotal FLOAT
				
	WHILE EXISTS ( SELECT * FROM @Commission ca WHERE ca.Done=0 )
	BEGIN
	
		SELECT @ID=ID,
				@Delta=RuleDelta,
				@DeltaFirstMonth=RuleDeltaFirstMonth,
				@DeltaOtherMonth=RuleDeltaOtherMonth,
				@PremiumCalculationID=PremiumCalculationID,
				@RuleCheckpoint=RuleCheckpoint
		FROM @Commission ca WHERE ca.Done=0
		
		-- Commission Split ruleID is defined as RuleID -1
		IF NOT EXISTS ( SELECT * FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
						WHERE pcdv.PremiumCalculationDetailID=@PremiumCalculationID 
							AND pcdv.RuleID=-1)
		BEGIN

			UPDATE @CommissionSplits SET Done=0
			SELECT @IsViewSet=0	
			
			WHILE @IsViewSet < 2
			BEGIN		

				SELECT @CumulativeCom=0, @CumulativeComFirst=0, @CumulativeComOther=0, @PercentTotal=SUM(ComPercent)
				FROM @CommissionSplits WHERE IsViewSet=@IsViewSet
				
				WHILE EXISTS ( SELECT * FROM @CommissionSplits cs WHERE Done=0 AND cs.TakesRemainder=5145 AND cs.IsViewSet=@IsViewSet )
				BEGIN
				
					SELECT @ComPercent=cs.ComPercent,@ComType=cs.ComType 
					FROM @CommissionSplits cs WHERE Done=0 AND cs.TakesRemainder=5145 AND cs.IsViewSet=@IsViewSet

					SELECT @ComDelta=CAST(@Delta AS FLOAT)*(@ComPercent/@PercentTotal)
					SELECT @CumulativeCom=@CumulativeCom+@ComDelta
					
					SELECT @ComFirst=CAST(@DeltaFirstMonth AS FLOAT)*(@ComPercent/@PercentTotal)
					SELECT @CumulativeComFirst=@CumulativeComFirst+@ComFirst
					
					SELECT @ComOther=CAST(@DeltaOtherMonth AS FLOAT)*(@ComPercent/@PercentTotal)
					SELECT @CumulativeComOther=@CumulativeComOther+@ComOther

					INSERT INTO PremiumCalculationDetailValues (ClientID,PremiumCalculationDetailID,RuleSequence,RuleID,RuleInput,RuleCheckpoint,RuleTransform,RuleTransformValue,RuleOutput,RuleOutputFirstMonth,RuleOutputOtherMonth,RuleInputOutputDelta,RuleInputOutputDeltaFirstMonth,RuleInputOutputDeltaOtherMonth) VALUES
					(@ClientID,@PremiumCalculationID,0,-1,0,@ComType,'','',0,0,0,@ComDelta,@ComFirst,@ComOther)

					UPDATE @CommissionSplits SET Done=1 WHERE ComType=@ComType
					
				END 
				
				SELECT @ComType=cs.ComType, 
						@ComDelta=@Delta-@CumulativeCom, 
						@ComFirst=@DeltaFirstMonth-@CumulativeComFirst,
						@ComOther=@DeltaOtherMonth-@CumulativeComOther
				FROM @CommissionSplits cs WHERE Done=0 AND cs.TakesRemainder=5144 AND cs.IsViewSet=@IsViewSet
				
				INSERT INTO dbo.PremiumCalculationDetailValues (ClientID,PremiumCalculationDetailID,RuleSequence,RuleID,RuleInput,RuleCheckpoint,RuleTransform,RuleTransformValue,RuleOutput,RuleOutputFirstMonth,RuleOutputOtherMonth,RuleInputOutputDelta,RuleInputOutputDeltaFirstMonth,RuleInputOutputDeltaOtherMonth) VALUES
				(@ClientID,@PremiumCalculationID,0,-1,0,@ComType,'','',0,0,0,@ComDelta,@ComFirst,@ComOther)

				SELECT @IsViewSet=@IsViewSet+1

			END
			
		END
		
		UPDATE @Commission SET Done=1 WHERE ID=@ID
	
	END	
	
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Premium_StoreDerivedCalculationValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Premium_StoreDerivedCalculationValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Premium_StoreDerivedCalculationValues] TO [sp_executeall]
GO
