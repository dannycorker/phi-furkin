SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIFieldMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_GetByRPIFieldMappingID]
(

	@RPIFieldMappingID int   
)
AS


				SELECT
					[RPIFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[RPIFieldID],
					[DetailFieldID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ColumnDetailFieldID]
				FROM
					[dbo].[RPIFieldMapping] WITH (NOLOCK) 
				WHERE
										[RPIFieldMappingID] = @RPIFieldMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_GetByRPIFieldMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_GetByRPIFieldMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_GetByRPIFieldMappingID] TO [sp_executeall]
GO
