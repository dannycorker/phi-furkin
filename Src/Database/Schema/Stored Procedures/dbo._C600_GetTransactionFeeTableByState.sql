SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 09/08/2016
-- Description:	Creates a CardTransaction Record from ThirdPartyFields
--				Gets the Amount from the thirdparty field, this can be overwritten by the PurchasedProductPaymentSchedule amount if the id has been set,
--				this in turn can be overwritten by the amount in the CustomerProductPaymentSchedule if the id has been set.
--	Modified By PR On 23/08/2016 : Obfuscated Card Number In Detail Field.
--	Modified By PR On 25/10/2016 : removed un-necessary logging select
--  Modified By PR On 28/10/2016 : added customer ref num
--  Modified By PR On 02/11/2016 : added card type id
--  Modified By DCM On 09/01/2017 : made when created full datetime
--  Modified By DCM On 09/01/2017 : added creating customer ledger entry
-- 2019-10-07 CPS for JIRA LPC-28  | Store CustomerPaymentScheduleID if one exists
-- 2019-12-19 CPS for JIRA LPC-248 | Added CardTransactionPolicy update to allow the CC Payer batch to complete following arrears collection
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetTransactionFeeTableByState]

	@StateCode VARCHAR(10)

AS
BEGIN

	SET NOCOUNT ON;

	--/*Select the Fee based on the values passed in*/
	SELECT r.DetailValue AS [StateCode], l.ItemValue AS [PaymentType], l1.ItemValue as [PaymentFrequency], CAST(r3.ValueMoney AS DECIMAL (18,2)) as [TransactionFee]
	----SELECT r.DetailValue AS [StateCode], r1.ValueInt AS [PaymentType], r2.ValueInt as [PaymentFrequency], r3.ValueMoney as [TransactionFee]
	FROM ResourceListDetailValues r WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues r1 WITH (NOLOCK) ON r.ResourceListID = r1.ResourceListID
	INNER JOIN dbo.LookupListItems l WITH (NOLOCK) ON l.LookupListItemID = r1.ValueInt
	INNER JOIN ResourceListDetailValues r2 WITH (NOLOCK) ON r.ResourceListID = r2.ResourceListID
	INNER JOIN dbo.LookupListItems l1 WITH (NOLOCK) ON l1.LookupListItemID = r2.ValueInt
	INNER JOIN ResourceListDetailValues r3 WITH (NOLOCK) ON r.ResourceListID = r3.ResourceListID
	WHERE r.DetailFieldID = 313947
	AND r1.DetailFieldID = 313948
	AND r2.DetailFieldID = 313949
	AND r3.DetailFieldID = 313950
	AND r.DetailValue = @StateCode

	--DECLARE @Table TABLE (StateCode VARCHAR(3), PaymentType VARCHAR(20), PaymentFrequency Varchar (20), TransactionFee Decimal(18,2)) 
	--INSERT INTO @Table
	--(
	--    StateCode,
	--    PaymentType,
	--    PaymentFrequency,
	--    TransactionFee
	--)
	--VALUES
	--(   @StateCode,  -- StateCode - varchar(3)
	--    'Credit Card',  -- PaymentType - varchar(10)
	--    'Monthly',  -- PaymentFrequency - varchar(10)
	--    2.00 -- TransactionFee - decimal(18, 2)
	--    ),
	--		(   @StateCode,  -- StateCode - varchar(3)
	--    'Credit Card',  -- PaymentType - varchar(10)
	--    'Annually',  -- PaymentFrequency - varchar(10)
	--    10.00 -- TransactionFee - decimal(18, 2)
	--    ),
	--		(   @StateCode,  -- StateCode - varchar(3)
	--    'Direct Debit',  -- PaymentType - varchar(10)
	--    'Monthly',  -- PaymentFrequency - varchar(10)
	--    0.00 -- TransactionFee - decimal(18, 2)
	--    ),
	--		(   @StateCode,  -- StateCode - varchar(3)
	--    'Direct Debit',  -- PaymentType - varchar(10)
	--    'Annually',  -- PaymentFrequency - varchar(10)
	--    0.00 -- TransactionFee - decimal(18, 2)
	--    )

		--SELECT * FROM @Table
	
END


GO
