SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Customers table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_GetByDefaultContactID]
(

	@DefaultContactID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[CustomerID],
					[ClientID],
					[TitleID],
					[IsBusiness],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[DayTimeTelephoneNumberVerifiedAndValid],
					[HomeTelephone],
					[HomeTelephoneVerifiedAndValid],
					[MobileTelephone],
					[MobileTelephoneVerifiedAndValid],
					[CompanyTelephone],
					[CompanyTelephoneVerifiedAndValid],
					[WorksTelephone],
					[WorksTelephoneVerifiedAndValid],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[Website],
					[HasDownloaded],
					[DownloadedOn],
					[AquariumStatusID],
					[ClientStatusID],
					[Test],
					[CompanyName],
					[Occupation],
					[Employer],
					[Fullname],
					[PhoneNumbersVerifiedOn],
					[DoNotEmail],
					[DoNotSellToThirdParty],
					[AgreedToTermsAndConditions],
					[DateOfBirth],
					[DefaultContactID],
					[DefaultOfficeID],
					[AddressVerified]
				FROM
					[dbo].[Customers]
				WHERE
					[DefaultContactID] = @DefaultContactID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByDefaultContactID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_GetByDefaultContactID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByDefaultContactID] TO [sp_executeall]
GO
