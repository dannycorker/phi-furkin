SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-07-22
-- Description:	Import eCatcher into Customer table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISI_CreditorImport]
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT,
			@ClientQuestionnaireID INT,
			@CustomerQuestionnaireID INT,
			@TableRowID INT,
			@NewTableRowID INT,
			@TableDetailFieldPageID INT,
			@TableDetailFieldID INT,
			@CreditorNameFieldID INT,
			@SecuredDebtTypeFieldID INT,
			@MonthlyRepaymentsDueFieldID INT,
			@MonthlyRepaymentsBeingMadeFieldID INT,
			@RemainingTermInMonthsFieldID INT,
			@CurrentBalanceFieldID INT,
			@TotalArrearsFieldID INT,
			@IsTheDebtSecuredFieldID INT,
			@HasTheDebtRestructuredFieldID INT,
			@CreditorNumber INT = 2,
			@Message VARCHAR(2000)
	
	SELECT @ClientID = c.ClientID
	FROM Customers c WITH (NOLOCK)
	WHERE c.CustomerID = @CustomerID 
	
	SELECT @ClientQuestionnaireID = cq.ClientQuestionnaireID, @CustomerQuestionnaireID = cq.CustomerQuestionnaireID
	FROM CustomerQuestionnaires cq
	WHERE cq.CustomerID = @CustomerID

	SELECT @TableRowID = tdv.TableRowID
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.DetailFieldID = 220468
	AND tdv.ValueInt = @ClientID
	
	SELECT @TableDetailFieldPageID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220469

	SELECT @TableDetailFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220481

	SELECT @CreditorNameFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220470

	SELECT @SecuredDebtTypeFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220471

	SELECT @MonthlyRepaymentsDueFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220472

	SELECT @MonthlyRepaymentsBeingMadeFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220473

	SELECT @RemainingTermInMonthsFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220474

	SELECT @CurrentBalanceFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220475

	SELECT @TotalArrearsFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220476

	SELECT @IsTheDebtSecuredFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220477

	SELECT @HasTheDebtRestructuredFieldID = tdv.ValueInt
	FROM TableDetailValues tdv WITH (NOLOCK)
	WHERE tdv.TableRowID = @TableRowID
	AND tdv.DetailFieldID = 220478

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
	VALUES (@ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID, 0, 0)
	
	SELECT @NewTableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @NewTableRowID, CASE tdv_all.DetailFielDID 
					WHEN 220458 THEN @CreditorNameFieldID /*Creditor Name*/
					WHEN 220459 THEN @SecuredDebtTypeFieldID /*Select Debt Type*/
					WHEN 220460 THEN @MonthlyRepaymentsDueFieldID /*Monthly repayments due*/
					WHEN 220461 THEN @MonthlyRepaymentsBeingMadeFieldID /*Monthly repayments being made*/
					WHEN 220462 THEN @RemainingTermInMonthsFieldID /*Remaining term in months*/
					WHEN 220463 THEN @CurrentBalanceFieldID /*Current Balance*/
					WHEN 220464 THEN @TotalArrearsFieldID /*Total Arrears*/
					WHEN 220465 THEN @IsTheDebtSecuredFieldID /*Is the debt secured*/
					WHEN 220466 THEN @HasTheDebtRestructuredFieldID /*Has the debt restructured*/
					END, ISNULL(cpa.AnswerText,ca.Answer)
	FROM TableDetailValues tdv_cqid WITH (NOLOCK)
	inner join TableDetailValues tdv_credNo WITH (NOLOCK) ON tdv_credNo.TableRowID = tdv_cqid.TableRowID AND tdv_credNo.DetailFieldID = 220457 and tdv_credNo.ValueInt = 1
	inner join TableDetailValues tdv_all with (nolock) on tdv_all.TableRowID = tdv_cqid.TableRowID AND tdv_all.DetailFieldID NOT IN (220456,220457,220480)
	inner join CustomerAnswers ca WITH (NOLOCK) ON ca.MasterQuestionID = tdv_all.ValueInt AND ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
	LEFT JOIN QuestionPossibleAnswers cpa WITH (NOLOCK) ON cpa.QuestionPossibleAnswerID = ca.QuestionPossibleAnswerID
	WHERE tdv_cqid.DetailFieldID = 220456
	AND tdv_cqid.ValueInt = @ClientQuestionnaireID
	
	UPDATE tdv
	SET DetailValue = luli.LookupListItemID
	FROM TableDetailValues tdv
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID
	INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.ItemValue = tdv.DetailValue AND luli.LookupListID = df.LookupListID
	WHERE tdv.TableRowID = @NewTableRowID

	WHILE (SELECT cpa.AnswerText FROM TableDetailValues tdv_cqid WITH (NOLOCK)
		inner join TableDetailValues tdv_credNo WITH (NOLOCK) ON tdv_credNo.TableRowID = tdv_cqid.TableRowID AND tdv_credNo.DetailFieldID = 220457 and tdv_credNo.ValueInt = @CreditorNumber
		inner join TableDetailValues tdv_all with (nolock) on tdv_all.TableRowID = tdv_cqid.TableRowID AND tdv_all.DetailFieldID IN (220480)
		inner join CustomerAnswers ca WITH (NOLOCK) ON ca.MasterQuestionID = tdv_all.ValueInt AND ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
		INNER JOIN QuestionPossibleAnswers cpa WITH (NOLOCK) ON cpa.QuestionPossibleAnswerID = ca.QuestionPossibleAnswerID
		WHERE tdv_cqid.DetailFieldID = 220456
		AND tdv_cqid.ValueInt = @ClientQuestionnaireID
		) = 'Yes'
	BEGIN
	
		INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
		VALUES (@ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID, 0, 0)
		
		SELECT @NewTableRowID = SCOPE_IDENTITY()
		
		INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
		SELECT @ClientID, @CustomerID, @NewTableRowID, CASE tdv_all.DetailFielDID 
						WHEN 220458 THEN @CreditorNameFieldID /*Creditor Name*/
						WHEN 220459 THEN @SecuredDebtTypeFieldID /*Select Debt Type*/
						WHEN 220460 THEN @MonthlyRepaymentsDueFieldID /*Monthly repayments due*/
						WHEN 220461 THEN @MonthlyRepaymentsBeingMadeFieldID /*Monthly repayments being made*/
						WHEN 220462 THEN @RemainingTermInMonthsFieldID /*Remaining term in months*/
						WHEN 220463 THEN @CurrentBalanceFieldID /*Current Balance*/
						WHEN 220464 THEN @TotalArrearsFieldID /*Total Arrears*/
						WHEN 220465 THEN @IsTheDebtSecuredFieldID /*Is the debt secured*/
						WHEN 220466 THEN @HasTheDebtRestructuredFieldID /*Has the debt restructured*/
						END, ISNULL(cpa.AnswerText,ca.Answer)
		FROM TableDetailValues tdv_cqid WITH (NOLOCK)
		inner join TableDetailValues tdv_credNo WITH (NOLOCK) ON tdv_credNo.TableRowID = tdv_cqid.TableRowID AND tdv_credNo.DetailFieldID = 220457 and tdv_credNo.ValueInt = @CreditorNumber
		inner join TableDetailValues tdv_all with (nolock) on tdv_all.TableRowID = tdv_cqid.TableRowID AND tdv_all.DetailFieldID NOT IN (220456,220457,220480)
		inner join CustomerAnswers ca WITH (NOLOCK) ON ca.MasterQuestionID = tdv_all.ValueInt AND ca.CustomerQuestionnaireID = @CustomerQuestionnaireID
		LEFT JOIN QuestionPossibleAnswers cpa WITH (NOLOCK) ON cpa.QuestionPossibleAnswerID = ca.QuestionPossibleAnswerID
		WHERE tdv_cqid.DetailFieldID = 220456
		AND tdv_cqid.ValueInt = @ClientQuestionnaireID
		
		UPDATE tdv
		SET DetailValue = luli.LookupListItemID
		FROM TableDetailValues tdv
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.ItemValue = tdv.DetailValue AND luli.LookupListID = df.LookupListID
		WHERE tdv.TableRowID = @NewTableRowID

		SELECT @CreditorNumber = @CreditorNumber + 1
		
	END
	
	Print 'Creditors done..' + CONVERT(VARCHAR,@CreditorNumber-1)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_CreditorImport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISI_CreditorImport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_CreditorImport] TO [sp_executeall]
GO
