SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 27-07-2016
-- Description:	saves a billing configuration record, either insert in id is 0 or update if not
-- =============================================
CREATE PROCEDURE [dbo].[BillingConfiguration__Save]
	
	@BillingConfigurationID INT,
	@ClientID INT, 
	@RemainderUpFront BIT, 
	@RegularPaymentWait INT, 
	@OneOffAdjustmentWait INT

AS
BEGIN
		
	SET NOCOUNT ON;

	IF @BillingConfigurationID>0
	BEGIN
		EXEC BillingConfiguration__Update @BillingConfigurationID, @ClientID, @RemainderUpFront, @RegularPaymentWait, @OneOffAdjustmentWait
	END
	ELSE -- no id so create record
	BEGIN
		EXEC BillingConfiguration__Insert @BillingConfigurationID OUTPUT, @ClientID, @RemainderUpFront, @RegularPaymentWait, @OneOffAdjustmentWait
	END
	
	SELECT @BillingConfigurationID AS BillingConfigurationID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillingConfiguration__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__Save] TO [sp_executeall]
GO
