SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - begin - Change Customer Phone or Email
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_158291]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@WorkflowTaskID				INT
			,@OneVisionID					INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */
			
	IF @EventTYpeID IN (158291) /*begin - Change Customer Phone or Email*/ 
	BEGIN 

		DECLARE @CustDOB DATE, @Mobile VARCHAR(100), @ValidMobile VARCHAR(3)

			SELECT @CustDOB = cu.DateOfBirth, @Mobile = cu.MobileTelephone
			FROM Customers cu WITH (NOLOCK)
			WHERE Cu.CustomerID = @CustomerID

		/*2018-04-09 - SA - Update to raise error error against customers who are below the age of 18*/
		IF DATEDIFF(Year,@CustDOB,dbo.fn_GetDate_Local()) < 18
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Customer is below the age of 18. Please check date of birth entered </br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		/*GPR 2019-12-02 SDLPC-25*/
		IF dbo.fnGetSimpleDvAsDate(175334,@MatterID) > dbo.fnGetSimpleDvAsDate(170037,@MatterID) /*Policy End Date*/
		BEGIN 
			SELECT @ErrorMessage = '<font color="red"></br></br>Date entered is greater than Policy End Date. Please correct before continuing.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END
		
		/*ALM 2020-07-01 AAG-967*/
		SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

		IF @OneVisionID IS NOT NULL 
		BEGIN
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Customer', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID,  @OneVisionID = @OneVisionID
		END

		/*SA - 2018-04-05 - added proc to update required fields following update to event ID 158291 = change customer details*/
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 1, 1


		/*GPR 2020-04-14 update the LeadEvent comments to provide the Current and Previous Customer Details upon 'begin - Change Customer Phone, DoB or Email' for AAG-640*/
		DECLARE @CurrentDoB VARCHAR(11), @CurrentEmail VARCHAR(200), @CurrentHomeTel VARCHAR(20), @CurrentMobileTel VARCHAR(20), @CurrentDayTel VARCHAR(20),
		@HistoricDoB VARCHAR(11), @HistoricEmail VARCHAR(200), @HistoricHomeTel VARCHAR(20), @HistoricMobileTel VARCHAR(20), @HistoricDayTel VARCHAR(20)
		
		SELECT @CurrentDoB = cu.DateOfBirth, @CurrentEmail = cu.EmailAddress, @CurrentHomeTel = cu.HomeTelephone, @CurrentMobileTel = cu.MobileTelephone, @CurrentDayTel = cu.DayTimeTelephoneNumber
		FROM Customers cu WITH (NOLOCK)
		WHERE cu.CustomerID = @CustomerID

		-- GPR 2020-04-14 - As the latest historical row will be the current details we must order to take the second to latest row.
		;WITH History AS
		(
			SELECT ch.CustomerHistoryID, ch.DateOfBirth, ch.EmailAddress, ch.HomeTelephone, ch.MobileTelephone, ch.DayTimeTelephoneNumber, ROW_NUMBER() OVER(ORDER BY CustomerHistoryID DESC) AS 'RowNum'
			FROM CustomerHistory ch WITH (NOLOCK)
			WHERE ch.CustomerID = @CustomerID
		)

		SELECT @HistoricDoB = ch.DateOfBirth, @HistoricEmail = ch.EmailAddress, @HistoricHomeTel = ch.HomeTelephone, @HistoricMobileTel = ch.MobileTelephone, @HistoricDayTel = ch.DayTimeTelephoneNumber
		FROM History ch WITH (NOLOCK)
		WHERE ch.RowNum = 2 -- The second to latest row

		UPDATE LeadEvent
		SET Comments =
		'Customer Details Updated.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) +
		' Current Details: ' + CHAR(13)+CHAR(10) +
		' DOB: ' + @CurrentDoB + CHAR(13)+CHAR(10) +
		' Email: ' + @CurrentEmail + CHAR(13)+CHAR(10) +
		' Home Phone: ' + @CurrentHomeTel + CHAR(13)+CHAR(10) +
		' Mobile Phone: ' + @CurrentMobileTel + CHAR(13)+CHAR(10) +
		' DayTime Phone: ' + @CurrentDayTel + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)+
		' Previous Details: ' + CHAR(13)+CHAR(10) +
		' DOB: ' + ISNULL(@HistoricDoB,'') + CHAR(13)+CHAR(10) +
		' Email: ' + ISNULL(@HistoricEmail,'') + CHAR(13)+CHAR(10) +
		' Home Phone: ' + ISNULL(@HistoricHomeTel,'') + CHAR(13)+CHAR(10) +
		' Mobile Phone: ' + ISNULL(@HistoricMobileTel,'') + CHAR(13)+CHAR(10) +
		' DayTime Phone: ' + ISNULL(@HistoricDayTel,'')
		FROM LeadEvent LeadEvent WITH (NOLOCK)
		WHERE LeadEventID=@LeadEventID
		

		/*UAH 07/11/2018 - As there is only 1 MTA Workflow group, I am adding SAE on all events which relate to 
		MTA's to remove workflow tasks once these MTA events have been applied*/

		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) where w.caseid = @caseID
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, @WhoCreated,dbo.fn_GetDate_Local(),'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE CaseID = @CaseID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 
		AND WorkflowGroupID = 1239

	END
	
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_158291] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_MidTermAdjustment_158291] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_158291] TO [sp_executeall]
GO
