SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-08-23
-- Description:	This SP will take a TableRowID and MatterID 
--				then take the table values and copy them 
--				to matter level
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyTableValuesToMatter]
@ClientID int,
@Fields dbo.tvpIntInt READONLY,
@TableRowID INT,
@AnyID int,
@DetailFieldSubType int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadID int

	/*
		This SP will take a tvpIDValueInt (Using ID1 for the table fields
		and ID2 as the matter ones), it will then:
			1. Create any values as empty
			2. Copy the values from the row into the matter values
	*/
	
	IF @DetailFieldSubType = 1
	BEGIN

		IF EXISTS (SELECT * FROM Lead l WITH (NOLOCK) WHERE l.LeadID = @AnyID AND l.ClientID = @ClientID)
		BEGIN
		
			/*Add empty values where required*/
			INSERT INTO LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
			SELECT @ClientID, @AnyID, f.ID2, ''
			FROM @Fields f
			WHERE NOT EXISTS (
				SELECT *
				FROM LeadDetailValues ldv WITH (NOLOCK)
				WHERE ldv.LeadID = @AnyID
				AND ldv.DetailFieldID = f.ID2
			)
			
			UPDATE LeadDetailValues
			SET DetailValue = tdv.DetailValue
			FROM LeadDetailValues 
			INNER JOIN @Fields f on f.ID2 = LeadDetailValues.DetailFieldID
			INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = f.ID1 and tdv.TableRowID = @TableRowID AND tdv.ClientID = @ClientID
			WHERE LeadDetailValues.LeadID = @AnyID
			
		END
	
	END

	IF @DetailFieldSubType = 2
	BEGIN
	
		SELECT @LeadID = m.LeadID
		FROM Matter m WITH (NOLOCK)
		WHERE m.MatterID = @AnyID
		AND m.ClientID = @ClientID
		
		IF @LeadID > 0
		BEGIN
		
			/*Add empty values where required*/
			INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
			SELECT @ClientID, @LeadID, @AnyID, f.ID2, ''
			FROM @Fields f
			WHERE NOT EXISTS (
				SELECT *
				FROM MatterDetailValues mdv WITH (NOLOCK)
				WHERE mdv.MatterID = @AnyID
				AND mdv.DetailFieldID = f.ID2
			)
			
			UPDATE MatterDetailValues
			SET DetailValue = tdv.DetailValue
			FROM MatterDetailValues 
			INNER JOIN @Fields f on f.ID2 = MatterDetailValues.DetailFieldID
			INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = f.ID1 and tdv.TableRowID = @TableRowID AND tdv.ClientID = @ClientID
			WHERE MatterDetailValues.MatterID = @AnyID
			
		END
	
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyTableValuesToMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyTableValuesToMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyTableValuesToMatter] TO [sp_executeall]
GO
