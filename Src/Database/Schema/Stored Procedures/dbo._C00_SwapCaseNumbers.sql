SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-09-23
-- Description:	Switch Case Numbers
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SwapCaseNumbers]
@LeadID int,
@Case1Number int, 
@Case2Number int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Case1CaseID int, @Case2CaseID int

	SELECT @Case1CaseID =  c.CaseID
	FROM Cases c with (NOLOCK)
	WHERE c.LeadID = @LeadID
	AND c.CaseNum = @Case1Number
		
	SELECT @Case2CaseID =  c.CaseID
	FROM Cases c with (NOLOCK)
	WHERE c.LeadID = @LeadID
	AND c.CaseNum = @Case2Number

	UPDATE Cases
	SET CaseNum = @Case2Number
	WHERE CaseID = @Case1CaseID

	UPDATE Cases
	SET CaseNum = @Case1Number
	WHERE CaseID = @Case2CaseID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SwapCaseNumbers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SwapCaseNumbers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SwapCaseNumbers] TO [sp_executeall]
GO
