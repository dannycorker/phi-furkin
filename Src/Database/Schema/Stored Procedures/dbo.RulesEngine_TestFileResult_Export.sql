SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-19
-- Description:	exports test file result items
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_TestFileResult_Export]
(
	@ClientID INT,
	@TestFileID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @OutputText VARCHAR(MAX) ='',
			@DynamicParamNames VARCHAR(MAX) = ''
	
	SELECT	@DynamicParamNames += ',"'+ rp.Name +'"'
	FROM	RulesEngine_TestFileItemOverride tfo WITH (NOLOCK) 
	INNER JOIN	RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = tfo.RuleParameterID
	WHERE	TestFileItemID IN 
	(
		SELECT	TOP 1 TestFileItemID 
		FROM	RulesEngine_TestFileItem WITH (NOLOCK) 
		WHERE	TestFileID = @TestFileID
	)
	ORDER BY tfo.RuleParameterID

	
	CREATE TABLE #DynamicParamValues (TestFileItemID INT, DynamicParamValues VARCHAR(2000))
	INSERT INTO #DynamicParamValues(TestFileItemID,DynamicParamValues)
	SELECT 
		tfi.TestFileItemID,
		ISNULL((    
			SELECT '","' + ISNULL(tfo.ParameterValue,'') AS [text()]
			FROM RulesEngine_TestFileItemOverride tfo WITH (NOLOCK) 
			WHERE tfo.TestFileItemID = tfi.TestFileItemID
			ORDER BY tfo.RuleParameterID
			FOR XML PATH('')
		)+'","', '","')
	FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
	WHERE	tfi.TestFileID = @TestFileID
	AND		tfi.ClientID = @ClientID
	
	SELECT '"Input","CustomerID","LeadID","CaseID","MatterID","EvaluateByEffectiveDate","EvaluateByChangeSetID"'+@DynamicParamNames+',"TestFileID","TestFileItemID","Processed","ChangeSet","RuleSet","RuleID","RuleName","RuleCheckpoint","InputValue","Transform","Value","OutputValue","ErrorMessage"' HeaderText
		
	SELECT
		tfi.Input,
		tfi.CustomerID,
		tfi.LeadID,
		tfi.CaseID,
		tfi.MatterID,
		tfi.EvaluateByEffectiveDate,
		tfi.EvaluateByChangeSetID,
		dpv.DynamicParamValues,
		tfi.TestFileID,
		tfir.TestFileItemID,
		tfir.WhenProcessed,
		cs.TagName,
		rs.Name,
		tfir.RuleID,
		tfir.RuleName,
		tfir.RuleCheckpoint,
		tfir.InputValue,
		tfir.Transform,
		tfir.Value,
		tfir.OutputValue,
		tfir.ErrorMessage
	FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
	INNER JOIN #DynamicParamValues dpv ON tfi.TestFileItemID = dpv.TestFileItemID
	INNER JOIN RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfi.TestFileItemID = tfir.TestFileItemID
	LEFT JOIN RulesEngine_Rules r WITH (NOLOCK)  ON r.RuleID = tfir.RuleID
	LEFT JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID
	LEFT JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID
	WHERE	tfi.TestFileID = @TestFileID
	AND		tfi.ClientID = @ClientID
	ORDER BY tfi.TestFileItemID, tfir.TestFileItemResultID DESC
	
				
	--DECLARE @DynamicParamValues TABLE (TestFileItemID INT, DynamicParamValues VARCHAR(2000))
	--INSERT INTO @DynamicParamValues(TestFileItemID,DynamicParamValues)
	--SELECT 
	--	tfi.TestFileItemID,
	--	ISNULL((    
	--		SELECT '","' + ISNULL(tfo.ParameterValue,'') AS [text()]
	--		FROM RulesEngine_TestFileItemOverride tfo WITH (NOLOCK) 
	--		WHERE tfo.TestFileItemID = tfi.TestFileItemID
	--		ORDER BY tfo.RuleParameterID
	--		FOR XML PATH('')
	--	)+'","', '","')
	--FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
	--WHERE	tfi.TestFileID = @TestFileID
	--AND		tfi.ClientID = @ClientID
	
	--SELECT '"Input","CustomerID","LeadID","CaseID","MatterID","EvaluateByEffectiveDate","EvaluateByChangeSetID"'+@DynamicParamNames+',"TestFileID","TestFileItemID","Processed","ChangeSet","RuleSet","RuleID","RuleName","RuleCheckpoint","InputValue","Transform","Value","OutputValue","ErrorMessage"' HeaderText
		
	--SELECT
	--	tfi.Input,
	--	tfi.CustomerID,
	--	tfi.LeadID,
	--	tfi.CaseID,
	--	tfi.MatterID,
	--	tfi.EvaluateByEffectiveDate,
	--	tfi.EvaluateByChangeSetID,
	--	dpv.DynamicParamValues,
	--	tfi.TestFileID,
	--	tfir.TestFileItemID,
	--	tfir.WhenProcessed,
	--	cs.TagName,
	--	rs.Name,
	--	tfir.RuleID,
	--	tfir.RuleName,
	--	tfir.RuleCheckpoint,
	--	tfir.InputValue,
	--	tfir.Transform,
	--	tfir.Value,
	--	tfir.OutputValue,
	--	tfir.ErrorMessage
	--FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
	--INNER JOIN @DynamicParamValues dpv ON tfi.TestFileItemID = dpv.TestFileItemID
	--INNER JOIN RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfi.TestFileItemID = tfir.TestFileItemID
	--LEFT JOIN RulesEngine_Rules r WITH (NOLOCK)  ON r.RuleID = tfir.RuleID
	--LEFT JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID
	--LEFT JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID
	--WHERE	tfi.TestFileID = @TestFileID
	--AND		tfi.ClientID = @ClientID
	--ORDER BY tfi.TestFileItemID, tfir.TestFileItemResultID DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFileResult_Export] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_TestFileResult_Export] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFileResult_Export] TO [sp_executeall]
GO
