SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-24
-- Description:	Allow Jump-To events to follow up the existing event. Without this, the
--				LeadEvent trigger will simply undo the followup details.
-- =============================================
CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_JumpToEvent]
	@CaseID int,
	@FromLeadEventID int,
	@ToLeadEventID int,
	@ToEventTypeID int,
	@IndividualThreadNumber int = null
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE LeadEventThreadCompletion 
	SET ToLeadEventID = @ToLeadEventID,
	ToEventTypeID = @ToEventTypeID
	WHERE CaseID = @CaseID 
	AND FromLeadEventID = @FromLeadEventID 
	AND (@IndividualThreadNumber IS NULL OR ThreadNumberRequired = @IndividualThreadNumber)
END



GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_JumpToEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_JumpToEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_JumpToEvent] TO [sp_executeall]
GO
