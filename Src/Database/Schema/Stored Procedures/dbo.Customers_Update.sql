SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Customers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_Update]
(

	@CustomerID int   ,

	@ClientID int   ,

	@TitleID int   ,

	@IsBusiness bit   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@EmailAddress varchar (255)  ,

	@DayTimeTelephoneNumber varchar (50)  ,

	@DayTimeTelephoneNumberVerifiedAndValid bit   ,

	@HomeTelephone varchar (50)  ,

	@HomeTelephoneVerifiedAndValid bit   ,

	@MobileTelephone varchar (50)  ,

	@MobileTelephoneVerifiedAndValid bit   ,

	@CompanyTelephone varchar (50)  ,

	@CompanyTelephoneVerifiedAndValid bit   ,

	@WorksTelephone varchar (50)  ,

	@WorksTelephoneVerifiedAndValid bit   ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (200)  ,

	@PostCode varchar (50)  ,

	@Website varchar (200)  ,

	@HasDownloaded bit   ,

	@DownloadedOn datetime   ,

	@AquariumStatusID int   ,

	@ClientStatusID int   ,

	@Test bit   ,

	@CompanyName varchar (100)  ,

	@Occupation varchar (100)  ,

	@Employer varchar (100)  ,

	@Fullname varchar (201)   OUTPUT,

	@PhoneNumbersVerifiedOn datetime   ,

	@DoNotEmail bit   ,

	@DoNotSellToThirdParty bit   ,

	@AgreedToTermsAndConditions bit   ,

	@DateOfBirth datetime   ,

	@DefaultContactID int   ,

	@DefaultOfficeID int   ,

	@AddressVerified bit   ,

	@CountryID int   ,

	@SubClientID int   ,

	@CustomerRef varchar (100)  ,

	@WhoChanged int   ,

	@WhenChanged datetime   ,

	@ChangeSource varchar (200)  ,

	@EmailAddressVerifiedAndValid bit   ,

	@EmailAddressVerifiedOn datetime   ,

	@Comments varchar (MAX)  ,

	@AllowSmsCommandProcessing bit   ,

	@LanguageID int   ,

	@Longitude numeric (25, 18)  ,

	@Latitude numeric (25, 18)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Customers]
				SET
					[ClientID] = @ClientID
					,[TitleID] = @TitleID
					,[IsBusiness] = @IsBusiness
					,[FirstName] = @FirstName
					,[MiddleName] = @MiddleName
					,[LastName] = @LastName
					,[EmailAddress] = @EmailAddress
					,[DayTimeTelephoneNumber] = @DayTimeTelephoneNumber
					,[DayTimeTelephoneNumberVerifiedAndValid] = @DayTimeTelephoneNumberVerifiedAndValid
					,[HomeTelephone] = @HomeTelephone
					,[HomeTelephoneVerifiedAndValid] = @HomeTelephoneVerifiedAndValid
					,[MobileTelephone] = @MobileTelephone
					,[MobileTelephoneVerifiedAndValid] = @MobileTelephoneVerifiedAndValid
					,[CompanyTelephone] = @CompanyTelephone
					,[CompanyTelephoneVerifiedAndValid] = @CompanyTelephoneVerifiedAndValid
					,[WorksTelephone] = @WorksTelephone
					,[WorksTelephoneVerifiedAndValid] = @WorksTelephoneVerifiedAndValid
					,[Address1] = @Address1
					,[Address2] = @Address2
					,[Town] = @Town
					,[County] = @County
					,[PostCode] = @PostCode
					,[Website] = @Website
					,[HasDownloaded] = @HasDownloaded
					,[DownloadedOn] = @DownloadedOn
					,[AquariumStatusID] = @AquariumStatusID
					,[ClientStatusID] = @ClientStatusID
					,[Test] = @Test
					,[CompanyName] = @CompanyName
					,[Occupation] = @Occupation
					,[Employer] = @Employer
					,[PhoneNumbersVerifiedOn] = @PhoneNumbersVerifiedOn
					,[DoNotEmail] = @DoNotEmail
					,[DoNotSellToThirdParty] = @DoNotSellToThirdParty
					,[AgreedToTermsAndConditions] = @AgreedToTermsAndConditions
					,[DateOfBirth] = @DateOfBirth
					,[DefaultContactID] = @DefaultContactID
					,[DefaultOfficeID] = @DefaultOfficeID
					,[AddressVerified] = @AddressVerified
					,[CountryID] = @CountryID
					,[SubClientID] = @SubClientID
					,[CustomerRef] = @CustomerRef
					,[WhoChanged] = @WhoChanged
					,[WhenChanged] = @WhenChanged
					,[ChangeSource] = @ChangeSource
					,[EmailAddressVerifiedAndValid] = @EmailAddressVerifiedAndValid
					,[EmailAddressVerifiedOn] = @EmailAddressVerifiedOn
					,[Comments] = @Comments
					,[AllowSmsCommandProcessing] = @AllowSmsCommandProcessing
					,[LanguageID] = @LanguageID
					,[Longitude] = @Longitude
					,[Latitude] = @Latitude
				WHERE
[CustomerID] = @CustomerID 
				
				
				-- Select computed columns into output parameters
				SELECT
 @Fullname = [Fullname]
				FROM
					[dbo].[Customers] WITH (NOLOCK) 
				WHERE
[CustomerID] = @CustomerID 
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Update] TO [sp_executeall]
GO
