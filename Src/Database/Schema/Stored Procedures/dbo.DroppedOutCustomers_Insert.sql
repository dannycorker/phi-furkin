SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DroppedOutCustomers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_Insert]
(

	@CustomerID int    OUTPUT,

	@ClientID int   ,

	@TitleID int   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@EmailAddress varchar (255)  ,

	@DayTimeTelephoneNumber varchar (50)  ,

	@HomeTelephone varchar (50)  ,

	@MobileTelephone varchar (50)  ,

	@CompanyTelephone varchar (50)  ,

	@WorksTelephone varchar (50)  ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (200)  ,

	@PostCode varchar (10)  ,

	@HasDownloaded bit   ,

	@DownloadedOn datetime   ,

	@AquariumStatusID int   ,

	@ClientStatusID int   ,

	@Test bit   ,

	@CompanyName varchar (100)  ,

	@Occupation varchar (100)  ,

	@Employer varchar (100)  ,

	@ReminderEmailSentDate datetime   ,

	@ReminderSmsSentDate datetime   ,

	@CountryID int   
)
AS


				
				INSERT INTO [dbo].[DroppedOutCustomers]
					(
					[ClientID]
					,[TitleID]
					,[FirstName]
					,[MiddleName]
					,[LastName]
					,[EmailAddress]
					,[DayTimeTelephoneNumber]
					,[HomeTelephone]
					,[MobileTelephone]
					,[CompanyTelephone]
					,[WorksTelephone]
					,[Address1]
					,[Address2]
					,[Town]
					,[County]
					,[PostCode]
					,[HasDownloaded]
					,[DownloadedOn]
					,[AquariumStatusID]
					,[ClientStatusID]
					,[Test]
					,[CompanyName]
					,[Occupation]
					,[Employer]
					,[ReminderEmailSentDate]
					,[ReminderSmsSentDate]
					,[CountryID]
					)
				VALUES
					(
					@ClientID
					,@TitleID
					,@FirstName
					,@MiddleName
					,@LastName
					,@EmailAddress
					,@DayTimeTelephoneNumber
					,@HomeTelephone
					,@MobileTelephone
					,@CompanyTelephone
					,@WorksTelephone
					,@Address1
					,@Address2
					,@Town
					,@County
					,@PostCode
					,@HasDownloaded
					,@DownloadedOn
					,@AquariumStatusID
					,@ClientStatusID
					,@Test
					,@CompanyName
					,@Occupation
					,@Employer
					,@ReminderEmailSentDate
					,@ReminderSmsSentDate
					,@CountryID
					)
				-- Get the identity value
				SET @CustomerID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Insert] TO [sp_executeall]
GO
