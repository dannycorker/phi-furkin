SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChartPalette table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartPalette_Delete]
(

	@ChartPaletteID int   
)
AS


				DELETE FROM [dbo].[ChartPalette] WITH (ROWLOCK) 
				WHERE
					[ChartPaletteID] = @ChartPaletteID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartPalette_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Delete] TO [sp_executeall]
GO
