SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnel table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel__GetNameByClientPersonnelID]
(
	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[UserName]
				FROM
					[dbo].[ClientPersonnel]
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID

				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetNameByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetNameByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetNameByClientPersonnelID] TO [sp_executeall]
GO
