SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-02-18
-- Description:	Get all DocumentZipFtpCommand records for a control record
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipFtpCommand__GetByControlIDAndType]
	@ControlID int,
	@LineType char(3)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT com.LineText 
	FROM dbo.DocumentZipFtpCommand com 
	WHERE com.DocumentZipFtpControlID = @ControlID 
	AND com.LineType = @LineType 
	AND com.Enabled = 1 
	ORDER BY com.LineOrder 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFtpCommand__GetByControlIDAndType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipFtpCommand__GetByControlIDAndType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFtpCommand__GetByControlIDAndType] TO [sp_executeall]
GO
