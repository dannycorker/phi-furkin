SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyFieldGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyFieldGroupID int   = null ,

	@ThirdPartySystemID int   = null ,

	@GroupName varchar (50)  = null ,

	@GroupDescription varchar (250)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyFieldGroupID]
	, [ThirdPartySystemID]
	, [GroupName]
	, [GroupDescription]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyFieldGroup] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID OR @ThirdPartyFieldGroupID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([GroupName] = @GroupName OR @GroupName IS NULL)
	AND ([GroupDescription] = @GroupDescription OR @GroupDescription IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyFieldGroupID]
	, [ThirdPartySystemID]
	, [GroupName]
	, [GroupDescription]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyFieldGroup] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID AND @ThirdPartyFieldGroupID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([GroupName] = @GroupName AND @GroupName is not null)
	OR ([GroupDescription] = @GroupDescription AND @GroupDescription is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Find] TO [sp_executeall]
GO
