SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-05-2015
-- Description:	Updates the process flag
-- =============================================
CREATE PROCEDURE [dbo].[VariableHeaderDataLoad__UpdateProcessedFlag]
(
	@VariableHeaderDataLoadID INT,
	@ClientID INT,
	@Processed BIT	
)
AS
BEGIN
	
	
	SET NOCOUNT ON;
	
	UPDATE
		[dbo].[VariableHeaderDataLoad]
	SET
		[Processed] = @Processed
	WHERE
		[VariableHeaderDataLoadID] = @VariableHeaderDataLoadID AND
		[ClientID] = @ClientID
		
END
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__UpdateProcessedFlag] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad__UpdateProcessedFlag] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__UpdateProcessedFlag] TO [sp_executeall]
GO
