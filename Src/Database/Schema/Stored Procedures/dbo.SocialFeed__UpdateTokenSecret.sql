SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richaredson
-- Create date: 09-11-2015
-- Description:	Inserts a SocialFeed record
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__UpdateTokenSecret]

	@SocialFeedID INT,		
	@Token VARCHAR(1000), 
	@Secret VARCHAR(1000), 
	@WhenModified DATETIME, 
	@WhoModified INT
	
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE SocialFeed
	SET Token=@Token, [Secret]=@Secret, WhenModified=@WhenModified, WhoModified=@WhoModified
	WHERE SocialFeedID=@SocialFeedID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__UpdateTokenSecret] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__UpdateTokenSecret] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__UpdateTokenSecret] TO [sp_executeall]
GO
