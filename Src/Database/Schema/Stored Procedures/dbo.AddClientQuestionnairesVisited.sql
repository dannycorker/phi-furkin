SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddClientQuestionnairesVisited]

@ClientQuestionnaireID int,
@VisitedDate datetime,
@IPAddress nvarchar(50),
@ClientID int

As

insert ClientQuestionnairesVisited (ClientQuestionnaireID, VisitedDate, IPAddress, ClientID)
values (@ClientQuestionnaireID, @VisitedDate, @IPAddress, @ClientID)

declare @ClientQuestionnairesVisitedID int

set @ClientQuestionnairesVisitedID = SCOPE_IDENTITY()

select ClientQuestionnairesVisitedID from ClientQuestionnairesVisited where ClientQuestionnairesVisitedID = @ClientQuestionnairesVisitedID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnairesVisited] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientQuestionnairesVisited] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnairesVisited] TO [sp_executeall]
GO
