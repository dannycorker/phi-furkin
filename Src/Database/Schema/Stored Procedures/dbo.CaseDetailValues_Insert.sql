SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CaseDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseDetailValues_Insert]
(

	@CaseDetailValueID int    OUTPUT,

	@ClientID int   ,

	@CaseID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[CaseDetailValues]
					(
					[ClientID]
					,[CaseID]
					,[DetailFieldID]
					,[DetailValue]
					,[ErrorMsg]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@CaseID
					,@DetailFieldID
					,@DetailValue
					,@ErrorMsg
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@SourceID
					)
				-- Get the identity value
				SET @CaseDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_Insert] TO [sp_executeall]
GO
