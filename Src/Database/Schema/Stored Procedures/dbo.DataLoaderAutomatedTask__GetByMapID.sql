SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-06-08
-- Description:	Get Tasks For DL Map
-- =============================================
CREATE PROCEDURE [dbo].[DataLoaderAutomatedTask__GetByMapID]
	@ClientID INT,
	@MapID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM DataLoaderAutomatedTask d WITH (NOLOCK)
	WHERE d.ClientID = @ClientID
	AND d.DataloaderMapID = @MapID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderAutomatedTask__GetByMapID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderAutomatedTask__GetByMapID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderAutomatedTask__GetByMapID] TO [sp_executeall]
GO
