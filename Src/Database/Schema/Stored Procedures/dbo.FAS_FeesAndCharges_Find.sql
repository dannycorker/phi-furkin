SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FAS_FeesAndCharges table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_FeesAndCharges_Find]
(

	@SearchUsingOR bit   = null ,

	@FasFeesAndChargesID int   = null ,

	@FasCheckerDataID int   = null ,

	@Description varchar (512)  = null ,

	@Amount decimal (18, 0)  = null ,

	@WhenPaid varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FasFeesAndChargesID]
	, [FasCheckerDataID]
	, [Description]
	, [Amount]
	, [WhenPaid]
    FROM
	[dbo].[FAS_FeesAndCharges] WITH (NOLOCK) 
    WHERE 
	 ([FasFeesAndChargesID] = @FasFeesAndChargesID OR @FasFeesAndChargesID IS NULL)
	AND ([FasCheckerDataID] = @FasCheckerDataID OR @FasCheckerDataID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Amount] = @Amount OR @Amount IS NULL)
	AND ([WhenPaid] = @WhenPaid OR @WhenPaid IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FasFeesAndChargesID]
	, [FasCheckerDataID]
	, [Description]
	, [Amount]
	, [WhenPaid]
    FROM
	[dbo].[FAS_FeesAndCharges] WITH (NOLOCK) 
    WHERE 
	 ([FasFeesAndChargesID] = @FasFeesAndChargesID AND @FasFeesAndChargesID is not null)
	OR ([FasCheckerDataID] = @FasCheckerDataID AND @FasCheckerDataID is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Amount] = @Amount AND @Amount is not null)
	OR ([WhenPaid] = @WhenPaid AND @WhenPaid is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_FeesAndCharges_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Find] TO [sp_executeall]
GO
