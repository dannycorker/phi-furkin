SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DashboardItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItem_Delete]
(

	@DashboardItemID int   
)
AS


				DELETE FROM [dbo].[DashboardItem] WITH (ROWLOCK) 
				WHERE
					[DashboardItemID] = @DashboardItemID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItem_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Delete] TO [sp_executeall]
GO
