SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 25/11/2019
-- Description:	Gets the Application Response
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplicationResponse__GetByPremiumFunderRequestID]
	@PremiumFunderRequestID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM PremiumFunderApplicationResponse WITH (NOLOCK) 
	WHERE PremiumFunderRequestID = @PremiumFunderRequestID ORDER BY 1 DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse__GetByPremiumFunderRequestID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplicationResponse__GetByPremiumFunderRequestID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse__GetByPremiumFunderRequestID] TO [sp_executeall]
GO
