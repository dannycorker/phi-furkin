SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetAllClientPersonnel] 

AS

SELECT 

ClientPersonnelID,
ClientID,
ClientOfficeID,
TitleID,
FirstName,
MiddleName,
LastName,
JobTitle,
ClientPersonnel.Password,
MobileTelephone,
HomeTelephone,
OfficeTelephone,
OfficeTelephoneExtension,
EmailAddress,
Salt,
AttemptedLogins,
AccountDisabled

FROM dbo.ClientPersonnel



GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAllClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllClientPersonnel] TO [sp_executeall]
GO
