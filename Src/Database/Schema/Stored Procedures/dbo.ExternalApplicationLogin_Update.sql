SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ExternalApplicationLogin table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_Update]
(

	@ExternalApplicationLoginID int   ,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@UserName varchar (255)  ,

	@Password varchar (65)  ,

	@Salt varchar (50)  ,

	@ClientPersonnelID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@AttemptedLogins int   ,

	@AccountDisabled bit   ,

	@CustomerID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ExternalApplicationLogin]
				SET
					[ThirdPartySystemID] = @ThirdPartySystemID
					,[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[UserName] = @UserName
					,[Password] = @Password
					,[Salt] = @Salt
					,[ClientPersonnelID] = @ClientPersonnelID
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[AttemptedLogins] = @AttemptedLogins
					,[AccountDisabled] = @AccountDisabled
					,[CustomerID] = @CustomerID
				WHERE
[ExternalApplicationLoginID] = @ExternalApplicationLoginID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Update] TO [sp_executeall]
GO
