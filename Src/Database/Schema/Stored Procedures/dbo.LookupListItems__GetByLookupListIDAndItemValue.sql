SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2009-12-08
-- Description:	Gets a list of matching LookupListItems for ImportLead.cs
-- Modified:	2014-07-25	SB	Added function for shared lead types
-- Modified:	2016-04-06	PR	Only return enabled lookup list items Zen desk ticket 37552
-- =============================================
CREATE PROCEDURE [dbo].[LookupListItems__GetByLookupListIDAndItemValue] 
	@LookupListID int,
	@ItemValue nvarchar(50),
	@ClientID INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT * FROM dbo.fnLookupListItemsShared(@ClientID)
	WHERE [LookupListID] = @LookupListID AND
		  [ItemValue] LIKE  @ItemValue AND 
		  [Enabled] = 1
END

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems__GetByLookupListIDAndItemValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems__GetByLookupListIDAndItemValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems__GetByLookupListIDAndItemValue] TO [sp_executeall]
GO
