SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-15
-- Description:	Downstream calculation for MTAs calculated using the C# rater
-- 2019-10-13 GPR created to 603 for LPC-35
-- 2019-11-06 GPR updated to use GetSimple in place of GetDV
-- 2019-11-25 CPS extension of above	| Replaced @CaseID with the appropriate object ID for each of Gav's fnGetSimpleDv calls.  No need for @CaseID in this sp anymore.
--										| dbo.fnGetDv always uses @CaseID, dbo.fnGetSimpleDv requires you to pass in @LeadID for a lead-level field, @MatterID for matter-level etc
-- 2020-02-05 CPS for JIRA AAG-91		| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 NG  for AAG-512    | Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RulesEngine_ReratePolicy_Evaluated]
(
	 @LeadEventID		INT
	,@CalculationTypeID	INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	 @AdjustmentDate							DATE
			,@MatterID									INT
			,@EndDate									DATE
			,@StartDate									DATE
			,@WrittenPremiumID	INT
			,@RatingDate								DATE
			,@MTARequired								BIT
			,@WhoCreated								INT
			,@CustomerID								INT
			,@LeadID									INT
			,@EventTypeID								INT
			,@NextEventTypeID							INT
			,@VarcharDate								VARCHAR(10)
			,@ValueMoney								DECIMAL(18,2)
			,@PremiumCalculationID						INT

	SELECT	 @MatterID			= m.MatterID
			,@AdjustmentDate	= le.WhenCreated
			,@WhoCreated		= le.WhoCreated
			,@CustomerID		= m.CustomerID
			,@LeadID			= m.LeadID
			,@EventTypeID		= le.EventTypeID
	FROM dbo.LeadEvent le WITH ( NOLOCK ) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
	WHERE le.LeadEventID = @LeadEventID

	IF @CalculationTypeID = 9 -- MTA Quote.  Taken from SAE.
	BEGIN
		SELECT @RatingDate=dbo.fnGetSimpleDvAsDate(170036,@MatterID) /*Policy Start Date*/
		SELECT @AdjustmentDate=ISNULL(dbo.fnGetSimpleDvAsDate (175442,@MatterID),DATEADD(DAY,-1,dbo.fn_GetDate_Local())) /*Adjustment Date*/

		IF @AdjustmentDate<@RatingDate SELECT @AdjustmentDate=@RatingDate
	
		-- if MTA or cancellation update the adjustment/renewal fields 
		SELECT @StartDate=dbo.fnGetSimpleDvAsDate (170036,@MatterID) /*Policy Start Date*/
		SELECT @EndDate=dbo.fnGetSimpleDvAsDate (170037,@MatterID)	 /*Policy End Date*/
		
		-- calculate the value of adjustment
		EXEC dbo._C00_1273_Policy_CalculateAdjustments @MatterID,@StartDate,@EndDate,@AdjustmentDate,@CalculationTypeID,0,@LeadEventID

		-- update postcode and excess details
		EXEC dbo._C600_PA_SetPostcodeGroupAndExcessForMatter @MatterID, @LeadEventID /*Added for ticket #45278 SW*/

		-- has there been a change in premium
		SELECT @MTARequired = CASE WHEN 70021=dbo.fnGetSimpleDvAsMoney(175345,@MatterID) THEN 0 ELSE 1 END -- adjustment type = no change

		-- ensure breed for page title is set
		EXEC dbo._C600_SAS @WhoCreated, @CustomerID, @LeadID

		-- if one or more of the changes could affect the policy premium
		-- start the MTA process or re-start the renewal process as appropriate 
		SELECT @NextEventTypeID = CASE	WHEN @MTARequired = 0 THEN 155352 /*Mid-term Adjustment - no change to premium*/
										--WHEN @EventTypeID = 156670 /*Re-rate policy (MTA PH)*/ THEN 156671 /*Mid term adjustment PH continue*/
										--ELSE 150154 /*Re-rate policy (MTA Pet)*/
								  END

		IF @NextEventTypeID <> 0
		BEGIN
			EXEC dbo._C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID = @LeadEventID, @AutomatedEventTypeID = @NextEventTypeID, @WhoCreated = @WhoCreated, @ThreadToFollowUp = 1
		END
	END
	ELSE
	IF @CalculationTypeID = 3 -- MTA.  Taken from SAE.
	BEGIN

		SELECT TOP 1 @WrittenPremiumID = wp.WrittenPremiumID
					,@PremiumCalculationID = wp.PremiumCalculationID
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 3 /*MTA*/
		ORDER BY wp.WrittenPremiumID DESC
			
		IF @WrittenPremiumID <> 0
		BEGIN

			UPDATE wp
			SET  ValidFrom  = dbo.fnGetSimpleDvAsDate(175442,@MatterID) /*Adjustment Date*/
				,AdjustmentValueGross  = dbo.fnGetSimpleDvAsMoney(177404,@MatterID) /*Adjustment Amount for remaining policy term*/
				,AdjustmentRequestDate = CAST(dbo.fn_GetDate_Local() as DATE)
			FROM WrittenPremium wp WITH (NOLOCK) 
			WHERE wp.WrittenPremiumID = @WrittenPremiumID

		END
			
		EXEC dbo._C00_SimpleValueIntoField 177022, 'MTA'				, @MatterID, @WhoCreated /*Description*/
		EXEC dbo._C00_SimpleValueIntoField 177027, @VarcharDate			, @MatterID, @WhoCreated /*Notification Date*/
		EXEC dbo._C00_SimpleValueIntoField 177111, @PremiumCalculationID, @MatterID, @WhoCreated /*Premium Calculation Detail ID*/
		EXEC dbo._C00_SimpleValueIntoField 177377, '0'					, @MatterID, @WhoCreated /*MTA_AdjustmentValueOnly*/
	END

	RETURN @WrittenPremiumID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RulesEngine_ReratePolicy_Evaluated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RulesEngine_ReratePolicy_Evaluated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RulesEngine_ReratePolicy_Evaluated] TO [sp_executeall]
GO
