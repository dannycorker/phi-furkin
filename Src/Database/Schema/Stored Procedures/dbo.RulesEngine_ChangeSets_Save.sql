SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-11
-- Description:	saves change set details
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ChangeSets_Save]
(
	@ChangeSetID INT, 
	@ClientID INT, 
	@TagName VARCHAR(250), 
	@ValidFrom DATETIME, 
	@ValidTo DATETIME, 
	@UserID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF @ChangeSetID > 0
	BEGIN
	
		UPDATE dbo.RulesEngine_ChangeSets
		SET TagName = @TagName,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID,
			ValidFrom = @ValidFrom,
			ValidTo = @ValidTo
		WHERE ChangeSetID = @ChangeSetID
		AND		ClientID = @ClientID
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ChangeSets_Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_Save] TO [sp_executeall]
GO
