SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptDialogue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_Update]
(

	@ScriptDialogueID int   ,

	@ScriptSectionID int   ,

	@ClientID int   ,

	@Dialogue varchar (MAX)  ,

	@HtmlDialogue varchar (MAX)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@WhenModified datetime   ,

	@WhoModified int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptDialogue]
				SET
					[ScriptSectionID] = @ScriptSectionID
					,[ClientID] = @ClientID
					,[Dialogue] = @Dialogue
					,[HtmlDialogue] = @HtmlDialogue
					,[WhenCreated] = @WhenCreated
					,[WhoCreated] = @WhoCreated
					,[WhenModified] = @WhenModified
					,[WhoModified] = @WhoModified
				WHERE
[ScriptDialogueID] = @ScriptDialogueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Update] TO [sp_executeall]
GO
