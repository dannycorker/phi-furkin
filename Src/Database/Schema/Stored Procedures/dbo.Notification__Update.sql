SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Updates a notification record
-- =============================================
CREATE PROCEDURE [dbo].[Notification__Update]

   @NotificationID INT,
   @ClientID INT,
   @SubClientID INT,
   @ThirdPartySystemID INT,
   @Message VARCHAR,
   @DateMessageCreated DATETIME,
   @MessageCreatedBy INT,
   @NotificationTypeID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	UPDATE [Notification]
    SET [ClientID] = @ClientID,
		[SubClientID] = @SubClientID,
		[ThirdPartySystemID] = @ThirdPartySystemID,
		[Message] = @Message,
		[DateMessageCreated] = @DateMessageCreated,
		[MessageCreatedBy] = @MessageCreatedBy
	WHERE NotificationID = @NotificationID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Notification__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__Update] TO [sp_executeall]
GO
