SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01/03/2016
-- Description:	Gets a Script rule via the script rule ID
-- =============================================
CREATE PROCEDURE [dbo].[ScriptRule__GetByScriptRuleID] 
	@ClientID INT,
	@ScriptRuleID INT
AS
BEGIN	
	
	SET NOCOUNT ON;

	SELECT g.*, df.QuestionTypeID, df.FieldName, df.LookupListID FROM ScriptRuleGroup g WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = g.DetailFieldID
	WHERE g.ScriptRuleID=@ScriptRuleID AND g.ClientID=@ClientID
	ORDER BY g.ScriptRuleGroupID ASC    
	
	SELECT distinct GroupID FROM ScriptRuleGroup WITH (NOLOCK) 
	WHERE ScriptRuleID=@ScriptRuleID AND ClientID=@ClientID
	ORDER BY GroupID ASC
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRule__GetByScriptRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptRule__GetByScriptRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRule__GetByScriptRuleID] TO [sp_executeall]
GO
