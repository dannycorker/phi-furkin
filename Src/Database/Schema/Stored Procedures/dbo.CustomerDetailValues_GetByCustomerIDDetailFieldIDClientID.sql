SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerDetailValues_GetByCustomerIDDetailFieldIDClientID]
(

	@CustomerID int   ,

	@DetailFieldID int   ,

	@ClientID int   
)
AS


				SELECT
					[CustomerDetailValueID],
					[ClientID],
					[CustomerID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[CustomerDetailValues] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
					AND [DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_GetByCustomerIDDetailFieldIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerDetailValues_GetByCustomerIDDetailFieldIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_GetByCustomerIDDetailFieldIDClientID] TO [sp_executeall]
GO
