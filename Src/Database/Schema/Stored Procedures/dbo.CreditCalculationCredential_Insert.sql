SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CreditCalculationCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_Insert]
(

	@CreditCalculationCredentialID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@SecretKey varchar (32)  
)
AS


				
				INSERT INTO [dbo].[CreditCalculationCredential]
					(
					[ClientID]
					,[ClientPersonnelID]
					,[SecretKey]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelID
					,@SecretKey
					)
				-- Get the identity value
				SET @CreditCalculationCredentialID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Insert] TO [sp_executeall]
GO
