SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2020-05-13
-- Description:	Import EFT Failures
-- 2020-05-15 GPR added FailureFileType and AccountID to INSERT
-- 2020-05-19 NG  added fields for event processing
-- =============================================
	CREATE PROCEDURE [dbo].[_C600_ImportEFTResponse]
	@FileName VARCHAR(2000),
	@RecordType VARCHAR(2000),
	@SequenceNumber VARCHAR(2000),
    @BsbNumber VARCHAR(2000),
    @AccountNumber VARCHAR(2000),
    @Indicator VARCHAR(2000),
    @TransactionCode VARCHAR(2000),
    @Amount VARCHAR(2000),
    @TitleOfAccount VARCHAR(2000),
    @LodgementReference VARCHAR(2000),
    @TraceBsbNumber VARCHAR(2000),
    @TraceAccountNumber VARCHAR(2000),
    @NameOfRemitter VARCHAR(2000),
    --@AmountOfWithholdingTax VARCHAR(2000),
    @StatusCode VARCHAR(2000),
    @StatusText VARCHAR(2000)
	
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @BACSImportLeadID INT = 218,   -- UPDATE NEEDED specific
			@BACSImportMatterID INT = 233, -- UPDATE NEEDED specific
			@ClientID INT = dbo.fnGetPrimaryClientID(), -- 600 specific    
			@AccountID INT,
			@RunAsUserID INT,
			@MatterID INT -- to update required fields for processing        

	/*Script gets file from SFTP site then runs this proc for each row*/
	/*Insert into BACSPaymentFailures*/

	/*GPR 2020-05-15*/
	SELECT	@AccountID = a.AccountID
			,@MatterID = a.ObjectID FROM Account a
									WHERE a.Reference = @LodgementReference
									AND a.Active = 1
									AND a.ClientID = @ClientID

	INSERT INTO BACSPaymentFailures 
	(ClientID, FileName, FailureFileType, RowNum, AccountNumber, SortCode, Name, PaymentAmount,BACsProcessedDate, FailureReason,
	 FailureCode, Reference, AccountID, ImportedDate, ImportStatusID)
	 SELECT 
	 @ClientID,@Filename, @RecordType, 1 /*TempValue*/,@AccountNumber, @BsbNumber, @NameOfRemitter, @Amount, dbo.fn_GetDate_Local(), @StatusText,
	 @StatusCode, @LodgementReference, @AccountID, dbo.fn_GetDate_Local(), 1 /*GPR 2020-05-15 added ImportStatusID*/
	
	EXEC Billing__ReconcilePaymentFailures @ClientID

	SELECT @RunAsUserID	= dbo.fn_C00_GetAutomationUser(@ClientID)
	EXEC dbo._C00_SimpleValueIntoField 170107, @StatusText, @MatterID, @RunAsUserID   /*NG added - Latest failure reason*/
    EXEC dbo._C00_SimpleValueIntoField 170169, @StatusCode, @MatterID, @RunAsUserID   /*NG added - Mandate Failure Code*/
	EXEC dbo._C00_LogIt 'Info', '_C600_ImportEFTResponse', 'Proc Complete', '', @RunAsUserID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportEFTResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ImportEFTResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportEFTResponse] TO [sp_executeall]
GO
