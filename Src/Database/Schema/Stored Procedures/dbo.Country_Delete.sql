SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Country table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_Delete]
(

	@CountryID int   
)
AS


				DELETE FROM [dbo].[Country] WITH (ROWLOCK) 
				WHERE
					[CountryID] = @CountryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Delete] TO [sp_executeall]
GO
