SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.RegisterNewClient    Script Date: 08/09/2006 12:22:49 ******/

CREATE PROCEDURE [dbo].[RegisterNewClient] 
@CompanyName nvarchar(100), 
@WebAddress nvarchar(255),
@IPAddress nvarchar(50),
@BuildingName nvarchar(50), 
@Address1 nvarchar(200),
@Address2 nvarchar(200), 
@Town nvarchar(200),
@County nvarchar(50),
@Country nvarchar(200),
@PostCode nvarchar(10),
@OfficeTelephone nvarchar(50),
@OfficeExt nvarchar(10),
@TitleID int,
@FirstName nvarchar(100),
@MiddleName nvarchar(100), 
@LastName nvarchar(100),
@JobTitle nvarchar(100),
@Password nvarchar(65), 
@MobileTelephone nvarchar(50), 
@EmailAddress nvarchar(255),
@EncryptedPassword nvarchar(10), 
@Salt nvarchar(25)
as

Insert into Clients (CompanyName, WebAddress, IPAddress) values (@CompanyName, @WebAddress, @IPAddress)

declare @newClientID int

set @newClientID = SCOPE_IDENTITY()

insert into ClientOffices (ClientID, BuildingName, Address1, Address2, Town, County, Country, PostCode, OfficeTelephone, OfficeTelephoneExtension)
 values (@newClientID, @BuildingName, @address1,@Address2, @Town, @County, @Country, @PostCode, @OfficeTelephone, @OfficeExt)

declare @newClientOfficeID int

set @newClientOfficeID = SCOPE_IDENTITY()

insert into ClientPersonnel (ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, ClientPersonnel.Password, MobileTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, Salt) 
values (@newClientID, @newClientOfficeID,@TitleID, @FirstName, @MiddleName, @LastName, @JobTitle, @EncryptedPassword, @MobileTelephone, @OfficeTelephone, @OfficeExt, @EmailAddress, @Salt)

Select ClientID from Clients Where ClientID = @newClientID;



GO
GRANT VIEW DEFINITION ON  [dbo].[RegisterNewClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RegisterNewClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegisterNewClient] TO [sp_executeall]
GO
