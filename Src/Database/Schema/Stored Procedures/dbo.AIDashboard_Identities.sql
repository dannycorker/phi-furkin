SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-06-01
-- Description:	AI Dashboard - Check for identities that are reaching their maximum values
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard_Identities]
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SummaryOnly = 1 
	BEGIN
		
		IF EXISTS (SELECT * FROM sys.identity_columns WHERE last_value > 2000000000)
		BEGIN
			SELECT 'Red' AS [Status]
		END
		ELSE IF EXISTS (SELECT * FROM sys.identity_columns WHERE last_value > 1000000000)
		BEGIN
			SELECT 'Amber' AS [Status]
		END
		ELSE
		BEGIN
			SELECT 'Green' AS [Status]
		END
		
	END
	ELSE
	BEGIN
		
		SELECT OBJECT_NAME(OBJECT_ID) AS [Table], 
		name AS [Identity Column], 
		CASE DATALENGTH(CAST(last_value AS VARCHAR))
			WHEN 9 THEN STUFF(STUFF(CAST(last_value AS VARCHAR), 4, 0, ','), 8, 0, ',')
			WHEN 10 THEN STUFF(STUFF(STUFF(CAST(last_value AS VARCHAR), 2, 0, ','), 6, 0, ','), 10, 0, ',')
			ELSE last_value
		END AS [Last Value],
		'EXEC dbo.rc ' + OBJECT_NAME(OBJECT_ID) as [Quick Count],
		CASE 
			WHEN last_value > 2000000000 THEN 'Red'
			WHEN last_value > 1000000000 THEN 'Amber'
			WHEN last_value > 500000000 THEN 'Yellow'
			ELSE 'Green' 
		END AS [Status]
		FROM sys.identity_columns 
		WHERE last_value > 100000000
		ORDER BY last_value DESC 
		
	END
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_Identities] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard_Identities] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_Identities] TO [sp_executeall]
GO
