SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the NotificationGroupMember table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroupMember_GetByNotificationGroupID]
(

	@NotificationGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[NotificationGroupMemberID],
					[NotificationGroupID],
					[ClientID],
					[ClientPersonnelID]
				FROM
					[dbo].[NotificationGroupMember] WITH (NOLOCK) 
				WHERE
					[NotificationGroupID] = @NotificationGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_GetByNotificationGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupMember_GetByNotificationGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_GetByNotificationGroupID] TO [sp_executeall]
GO
