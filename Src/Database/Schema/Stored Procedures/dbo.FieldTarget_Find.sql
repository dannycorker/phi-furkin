SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FieldTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@FieldTargetID int   = null ,

	@Target varchar (250)  = null ,

	@ObjectName varchar (50)  = null ,

	@PropertyName varchar (50)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FieldTargetID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[FieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([FieldTargetID] = @FieldTargetID OR @FieldTargetID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FieldTargetID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[FieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([FieldTargetID] = @FieldTargetID AND @FieldTargetID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Find] TO [sp_executeall]
GO
