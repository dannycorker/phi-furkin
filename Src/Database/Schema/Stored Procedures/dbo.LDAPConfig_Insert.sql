SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LDAPConfig table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LDAPConfig_Insert]
(

	@LDAPConfigID int    OUTPUT,

	@ClientID int   ,

	@QueryString varchar (2000)  ,

	@QueryAccountName varchar (2000)  ,

	@QueryAccountPassword varchar (2000)  ,

	@Enabled bit   ,

	@Domain varchar (50)  
)
AS


				
				INSERT INTO [dbo].[LDAPConfig]
					(
					[ClientID]
					,[QueryString]
					,[QueryAccountName]
					,[QueryAccountPassword]
					,[Enabled]
					,[Domain]
					)
				VALUES
					(
					@ClientID
					,@QueryString
					,@QueryAccountName
					,@QueryAccountPassword
					,@Enabled
					,@Domain
					)
				-- Get the identity value
				SET @LDAPConfigID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Insert] TO [sp_executeall]
GO
