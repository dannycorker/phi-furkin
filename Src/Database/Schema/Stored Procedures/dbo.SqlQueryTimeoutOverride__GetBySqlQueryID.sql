SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-10-2013
-- Description:	Gets the Timeout for the given SqlQuery
-- =============================================
CREATE PROCEDURE [dbo].[SqlQueryTimeoutOverride__GetBySqlQueryID]

	@SqlQueryID INT,
	@ClientID INT

AS
BEGIN
	
	SELECT * FROM SqlQueryTimeoutOverride WITH (NOLOCK) 
	WHERE SqlQueryID=@SqlQueryID AND ClientID=@ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTimeoutOverride__GetBySqlQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTimeoutOverride__GetBySqlQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTimeoutOverride__GetBySqlQueryID] TO [sp_executeall]
GO
