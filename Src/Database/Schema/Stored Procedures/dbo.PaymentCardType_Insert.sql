SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PaymentCardType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentCardType_Insert]
(

	@PaymentCardTypeID int    OUTPUT,

	@PaymentCardType varchar (50)  ,

	@DisplayOrder int   
)
AS


				
				INSERT INTO [dbo].[PaymentCardType]
					(
					[PaymentCardType]
					,[DisplayOrder]
					)
				VALUES
					(
					@PaymentCardType
					,@DisplayOrder
					)
				-- Get the identity value
				SET @PaymentCardTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentCardType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Insert] TO [sp_executeall]
GO
