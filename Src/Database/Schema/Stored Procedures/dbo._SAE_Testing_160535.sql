SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2021-04-09
-- Description:	Sql After Event - TESTING: Braintree
-- 2021-04-09 CPS for JIRA FURKIN-479 | Find the next collection for this policy and update the PaymentGross with a BrainTree failure code
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Testing_160535]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	PRINT OBJECT_NAME(@@ProcID)

	DECLARE /* Standard */
			 @EventTypeID					INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@EventComments					VARCHAR(2000) = ''
			,@ErrorMessage					VARCHAR(2000) = ''
			,@BraintreeErrorCode			INT
			,@NextCustomerPaymentScheduleID	INT

/* ===================================================================================================================== */

	/*Select and assign Aquarium IDs using the LeadEventID*/
	SELECT TOP (1) 
		 @EventTypeID	= le.EventTypeID
		,@WhoCreated	= le.WhoCreated
		,@MatterID		= m.MatterID
		,@WhenCreated	= le.WhenCreated
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
	INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	IF @EventTypeID = 160535 /*TESTING - Prepare Braintree Failure*/
	BEGIN
		SELECT @BraintreeErrorCode = dbo.fnGetSimpleDvAsInt(315905,@MatterID) /*Braintree Error Code*/

		SELECT TOP (1)
				 @NextCustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN dbo.PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = ppps.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND ppps.ActualCollectionDate > @WhenCreated
		AND ppps.PaymentStatusID = 1 /*New*/
		
		IF @@RowCount = 0 
		BEGIN
			SELECT @ErrorMessage = '<br><br><font color="red">Error: no "new" PPPS records found for MatterID ' + CONVERT(VARCHAR,@MatterID) + ' with a collection date after ' + ISNULL(CONVERT(VARCHAR(10),@WhenCreated,121),'NULL') + '</font>'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END
		ELSE
		BEGIN
			UPDATE cps
			SET  PaymentGross = @BraintreeErrorCode
				,WhenModified = @WhenCreated
				,WhoModified  = @WhoCreated
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			WHERE cps.CustomerPaymentScheduleID = @NextCustomerPaymentScheduleID

			UPDATE ppps
			SET	 PaymentGross = @BraintreeErrorCode
				,WhenModified = @WhenCreated
				,WhoModified  = @WhoCreated
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
			WHERE ppps.CustomerPaymentScheduleID = @NextCustomerPaymentScheduleID

			SELECT @EventComments += CONVERT(VARCHAR,@@RowCount) + ' PPPS records set to ' + ISNULL(CONVERT(VARCHAR,@BraintreeErrorCode),'NULL') + ' for CustomerPaymentScheduleID ' + ISNULL(CONVERT(VARCHAR,@NextCustomerPaymentScheduleID),'NULL')

			EXEC _C00_SimpleValueIntoField 315905, '', @MatterID, @WhoCreated /*Braintree Error Code*/

			EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		END
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END

GO
GRANT EXECUTE ON  [dbo].[_SAE_Testing_160535] TO [sp_executeall]
GO
