SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-25
-- Description:	Get Third Party Fields for system
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartySystemFieldMapping__GetGridForEditing]
	@ClientID INT,
	@ClientPersonnelID INT,
	@ThirdPartySystemID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT t.ThirdPartyFieldMappingID, tg.GroupName, t.ThirdPartyFieldID, df.DetailFieldID, df.FieldName, df_column.FieldName AS [Column]--, dfp.PageName, dfp_table.PageCaption AS [Table Page Name]--, t.*
	FROM [ThirdPartyFieldMapping] t WITH (NOLOCK) 
	INNER JOIN ThirdPartyFieldGroup tg WITH (NOLOCK) ON tg.ThirdPartyFieldGroupID = t.ThirdPartyFieldGroupID
	INNER JOIN ThirdPartySystem tps WITH (NOLOCK) ON tps.ThirdPartySystemId = tg.ThirdPartySystemID
	INNER JOIN DetailFields df WITH (NOLOCK) On df.DetailFieldID = t.DetailFieldID
	LEFT JOIN DetailFields df_column WITH (NOLOCK) On df_column.DetailFieldID = t.ColumnFieldID
	INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON dfp.DetailFieldPageID = df.DetailFieldPageID
	WHERE t.clientid = @ClientID
	AND tps.ThirdPartySystemId = @ThirdPartySystemID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemFieldMapping__GetGridForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemFieldMapping__GetGridForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemFieldMapping__GetGridForEditing] TO [sp_executeall]
GO
