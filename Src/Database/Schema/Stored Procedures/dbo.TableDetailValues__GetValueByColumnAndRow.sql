SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-08-12
-- Description:	TableDetailValues__GetValueByColumnAndRow
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__GetValueByColumnAndRow]
	@LeadID int,
	@MatterID int = 0,
	@TableDetailFieldID int,
	@Column int,
	@Row int, 
	@CustomerID int = null, 
	@CaseID int = null, 
	@ClientIDForDV int = null, 
	@ClientPersonnelIDForDV int = null, 
	@ContactIDForDV int = null 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TableRowID int, 
			@DetailFieldID int,
			@TableDetailFieldPageID int,
			@Value varchar(2000) = '',
			@FieldType int,
			@LookupListID int,
			@QuestionTypeID int
			
	Select @TableDetailFieldPageID = TableDetailFieldPageID
	from detailfields
	where detailfieldid = @TableDetailFieldID

	; WITH Fields as (	
		/* Sort by all the resource list fields within the table first, then the rest of the table fields */
		Select CASE df.QuestionTypeID WHEN 14 THEN df1.DetailFieldID ELSE  df.DetailFieldID END as DetailFieldID, 
		ROW_NUMBER() OVER (order by IsNull(df1.FieldOrder, 9999), df.FieldOrder) as FieldOrder, CASE df.QuestionTypeID WHEN 14 THEN df.QuestionTypeID ELSE ISNULL(df1.QuestionTypeID, df.QuestiontypeID) END as QuestionTypeID
		from detailfields df with (nolock) 
		Left Join DetailFields df1 on df1.DetailFieldPageID = df.ResourceListDetailFieldPageID and df1.LeadOrMatter = 4
		where df.detailfieldpageid = @TableDetailFieldPageID
		and df.enabled = 1
	)

	Select @DetailFieldID = f.DetailFieldID, @QuestionTypeID = f.QuestionTypeID
	From Fields f
	Where f.FieldOrder = @Column
	
	Select @FieldType = df.QuestionTypeID
	From DetailFields df 
	Where df.DetailFieldID = @DetailFieldID
	
	/*
		JWG 2011-01-13 Cater for all the other XyzDetailValues tables as well
	*/
	IF @ContactIDForDV > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.ContactID = @ContactIDForDV
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	ELSE
	IF @ClientPersonnelIDForDV > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.ClientPersonnelID = @ClientPersonnelIDForDV
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	ELSE
	IF @ClientIDForDV > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.ClientID = @ClientIDForDV
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	ELSE
	IF @CaseID > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.CaseID = @CaseID
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	ELSE
	IF @CustomerID > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.CustomerID = @CustomerID
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	ELSE
	IF @MatterID > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.MatterID = @MatterID
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	ELSE
	IF @LeadID > 0
	BEGIN
		;WITH RealTableRows AS 
		(
			SELECT tr.TableRowID, (ROW_NUMBER() OVER (ORDER BY tr.TableRowID)) AS RowOrder
			FROM dbo.TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = @TableDetailFieldID 
			AND tr.LeadID = @LeadID
		)
		SELECT @TableRowID = rt.TableRowID
		FROM RealTableRows rt
		WHERE rt.RowOrder = @Row
	END
	
	IF @QuestionTypeID = 14
	BEGIN

		Select @Value = tdv.ResourceListID
		From TableDetailValues tdv WITH (NOLOCK)
		Where tdv.TableRowID = @TableRowID
		and tdv.ResourceListID IS NOT NULL

		Select @Value = CASE @FieldType 
			WHEN 2 THEN rldv.DetailValue
			WHEN 4 THEN rldv.DetailValue
			WHEN 5 THEN CONVERT(Varchar(10), rldv.ValueDate, 103) 
			ELSE rldv.DetailValue END
		From ResourceListDetailValues rldv WITH (NOLOCK)
		where rldv.DetailFieldID = @DetailFieldID
		and rldv.ResourceListID = CONVERT(int,@Value)
	
	END
	ELSE
	BEGIN
	
		Select @Value = CASE @FieldType 
			WHEN 2 THEN tdv.DetailValue
			WHEN 4 THEN tdv.DetailValue
			WHEN 5 THEN CONVERT(Varchar(10), tdv.ValueDate, 103) 
			ELSE tdv.DetailValue END
		From TableDetailValues tdv WITH (NOLOCK)
		Where tdv.TableRowID = @TableRowID
		and tdv.DetailFieldID = @DetailFieldID
	
	END
	
	IF @FieldType IN (2,4)	
	BEGIN
	
		Select @Value = luli.ItemValue
		From LookupListItems luli WITH (NOLOCK)
		Where luli.LookupListItemID = @Value
		
	END
	
	Select @Value as Value

END




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetValueByColumnAndRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetValueByColumnAndRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetValueByColumnAndRow] TO [sp_executeall]
GO
