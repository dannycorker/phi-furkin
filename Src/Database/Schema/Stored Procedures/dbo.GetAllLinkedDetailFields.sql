SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE PROCEDURE [dbo].[GetAllLinkedDetailFields]
@LeadTypeIDTo int,
@LeadTypeIDFrom int

as

SELECT LinkedDetailFieldID, LeadTypeIDTo, LeadTypeIDFrom, DetailFieldIDTo, DetailFieldIDFrom, DisplayOnPageID, 
FieldOrder, Enabled, History, LeadOrMatter, FieldName, LeadLinkedTo, LeadLinkedFrom, IncludeLinkedField, ClientID
FROM dbo.LinkedDetailFields
WHERE (LeadTypeIDTo = @LeadTypeIDTo) AND (LeadTypeIDFrom = @LeadTypeIDFrom)








GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllLinkedDetailFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAllLinkedDetailFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllLinkedDetailFields] TO [sp_executeall]
GO
