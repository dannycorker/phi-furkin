SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the NoteType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType_Get_List]

AS


				
				SELECT
					[NoteTypeID],
					[ClientID],
					[NoteTypeName],
					[NoteTypeDescription],
					[DefaultPriority],
					[AlertColour],
					[NormalColour],
					[Enabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID],
					[LeadTypeID],
					[IsShared]
				FROM
					[dbo].[NoteType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Get_List] TO [sp_executeall]
GO
