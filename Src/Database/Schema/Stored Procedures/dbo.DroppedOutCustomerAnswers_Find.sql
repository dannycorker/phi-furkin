SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DroppedOutCustomerAnswers table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerAnswers_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomerAnswerID int   = null ,

	@CustomerQuestionnaireID int   = null ,

	@MasterQuestionID int   = null ,

	@Answer varchar (MAX)  = null ,

	@QuestionPossibleAnswerID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomerAnswerID]
	, [CustomerQuestionnaireID]
	, [MasterQuestionID]
	, [Answer]
	, [QuestionPossibleAnswerID]
	, [ClientID]
    FROM
	[dbo].[DroppedOutCustomerAnswers] WITH (NOLOCK) 
    WHERE 
	 ([CustomerAnswerID] = @CustomerAnswerID OR @CustomerAnswerID IS NULL)
	AND ([CustomerQuestionnaireID] = @CustomerQuestionnaireID OR @CustomerQuestionnaireID IS NULL)
	AND ([MasterQuestionID] = @MasterQuestionID OR @MasterQuestionID IS NULL)
	AND ([Answer] = @Answer OR @Answer IS NULL)
	AND ([QuestionPossibleAnswerID] = @QuestionPossibleAnswerID OR @QuestionPossibleAnswerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomerAnswerID]
	, [CustomerQuestionnaireID]
	, [MasterQuestionID]
	, [Answer]
	, [QuestionPossibleAnswerID]
	, [ClientID]
    FROM
	[dbo].[DroppedOutCustomerAnswers] WITH (NOLOCK) 
    WHERE 
	 ([CustomerAnswerID] = @CustomerAnswerID AND @CustomerAnswerID is not null)
	OR ([CustomerQuestionnaireID] = @CustomerQuestionnaireID AND @CustomerQuestionnaireID is not null)
	OR ([MasterQuestionID] = @MasterQuestionID AND @MasterQuestionID is not null)
	OR ([Answer] = @Answer AND @Answer is not null)
	OR ([QuestionPossibleAnswerID] = @QuestionPossibleAnswerID AND @QuestionPossibleAnswerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerAnswers_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_Find] TO [sp_executeall]
GO
