SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_Delete]
(

	@EventTypeID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[EventType] WITH (ROWLOCK) 
				WHERE
					[EventTypeID] = @EventTypeID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_Delete] TO [sp_executeall]
GO
