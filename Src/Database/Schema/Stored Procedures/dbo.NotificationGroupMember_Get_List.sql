SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the NotificationGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroupMember_Get_List]

AS


				
				SELECT
					[NotificationGroupMemberID],
					[NotificationGroupID],
					[ClientID],
					[ClientPersonnelID]
				FROM
					[dbo].[NotificationGroupMember] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupMember_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Get_List] TO [sp_executeall]
GO
