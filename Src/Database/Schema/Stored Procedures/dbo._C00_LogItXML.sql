SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-03-24
-- Description:	Common routine for adding entries to the LogXMK table
-- SLACKY-PERF-20190621- Reduce Load on LogXML INSERTS
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LogItXML]
	@ClientID INT, 
	@ContextID INT, 
	@ContextVarchar VARCHAR(250), 
	@LogXMLEntry XML, 
	@InvalidXML VARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	---- SLACKY-PERF-20190621- Reduce Load on LogXML INSERTS
	--IF	
	--	@ContextVarchar LIKE '%_C%_PA_Policy_GetProducts%' OR
	--	@ContextVarchar LIKE '%_C%_PA_Policy_GetCalculatedValues%' OR
	--	@ContextVarchar LIKE '%_C%_PA_Policy_GetQuoteValues%' OR
	--	@ContextVarchar LIKE '%QuoteApi-GetQuoteValues%' OR
	--	@ContextVarchar LIKE '%_C%_PA_Policy_SaveQuote%' OR
	--	@ContextVarchar LIKE '%RulesEngine_Evaluate%'
	--BEGIN
	--	RETURN;
	--END

	INSERT INTO [dbo].[LogXML] (ClientID, LogDateTime, ContextID, ContextVarchar, LogXMLEntry, InvalidXML)
	VALUES (
		@ClientID,
		dbo.fn_GetDate_Local(), 
		@ContextID, 
		@ContextVarchar, 
		@LogXMLEntry, 
		@InvalidXML
	)
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LogItXML] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LogItXML] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LogItXML] TO [sp_executeall]
GO
