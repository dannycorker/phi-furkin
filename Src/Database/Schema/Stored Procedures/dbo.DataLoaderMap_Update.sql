SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap_Update]
(

	@DataLoaderMapID int   ,

	@ClientID int   ,

	@MapName varchar (250)  ,

	@MapDescription varchar (2000)  ,

	@WhenCreated datetime   ,

	@CreatedBy int   ,

	@LastUpdated datetime   ,

	@UpdatedBy int   ,

	@EnabledForUse bit   ,

	@Deleted bit   ,

	@LeadTypeID int   ,

	@UseDefaultLeadForCustomer bit   ,

	@UseDefaultCaseForLead bit   ,

	@UseDefaultMatterForCase bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderMap]
				SET
					[ClientID] = @ClientID
					,[MapName] = @MapName
					,[MapDescription] = @MapDescription
					,[WhenCreated] = @WhenCreated
					,[CreatedBy] = @CreatedBy
					,[LastUpdated] = @LastUpdated
					,[UpdatedBy] = @UpdatedBy
					,[EnabledForUse] = @EnabledForUse
					,[Deleted] = @Deleted
					,[LeadTypeID] = @LeadTypeID
					,[UseDefaultLeadForCustomer] = @UseDefaultLeadForCustomer
					,[UseDefaultCaseForLead] = @UseDefaultCaseForLead
					,[UseDefaultMatterForCase] = @UseDefaultMatterForCase
				WHERE
[DataLoaderMapID] = @DataLoaderMapID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Update] TO [sp_executeall]
GO
