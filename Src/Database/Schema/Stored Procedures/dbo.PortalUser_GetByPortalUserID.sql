SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUser table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser_GetByPortalUserID]
(

	@PortalUserID int   
)
AS


				SELECT
					[PortalUserID],
					[ClientID],
					[CustomerID],
					[Username],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[Enabled],
					[AttemptedLogins],
					[PortalUserGroupID]
				FROM
					[dbo].[PortalUser] WITH (NOLOCK) 
				WHERE
										[PortalUserID] = @PortalUserID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByPortalUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_GetByPortalUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByPortalUserID] TO [sp_executeall]
GO
