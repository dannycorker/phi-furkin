SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CheckMateFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldGroup_Get_List]

AS


				
				SELECT
					[CheckMateFieldGroupID],
					[GroupName],
					[GroupDescription]
				FROM
					[dbo].[CheckMateFieldGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Get_List] TO [sp_executeall]
GO
