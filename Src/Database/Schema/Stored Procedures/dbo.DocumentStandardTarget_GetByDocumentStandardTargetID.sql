SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentStandardTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_GetByDocumentStandardTargetID]
(

	@DocumentStandardTargetID int   
)
AS


				SELECT
					[DocumentStandardTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes],
					[IsSpecial],
					[ExcelSheetLocation],
					[DocumentTypeVersionID]
				FROM
					[dbo].[DocumentStandardTarget] WITH (NOLOCK) 
				WHERE
										[DocumentStandardTargetID] = @DocumentStandardTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByDocumentStandardTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_GetByDocumentStandardTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_GetByDocumentStandardTargetID] TO [sp_executeall]
GO
