SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/****** Object:  Stored Procedure dbo.LeadTypeLinks_GetByClientIDFull    Script Date: 02/11/2006 14:08:50 ******/

/*
----------------------------------------------------------------------------------------------------
-- Date Created: 02 November 2006

-- Created By:  Jim Green
-- Purpose: Select LeadTypeLinks records, joined to all description tables
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_GetByClientIDFull]
(

	@ClientID int   
)
AS

SELECT
	0 AS LeadTypeLinkID,
	cq.ClientID,
	o.OutcomeID,
	0 AS LeadTypeID,
	'' AS LeadTypeName,
	o.OutcomeName,
	cq.QuestionnaireTitle
FROM
	Outcomes o,
	ClientQuestionnaires cq
WHERE            
	o.ClientQuestionnaireID = cq.ClientQuestionnaireID
AND	cq.ClientID = @ClientID 
AND	NOT EXISTS (select * from LeadTypeLink ltl WHERE ltl.OutcomeID = o.OutcomeID)

UNION 

SELECT
	ltl.LeadTypeLinkID,
	cq.ClientID,
	o.OutcomeID,
	ltl.LeadTypeID,
	lt.LeadTypeName,
	o.OutcomeName,
	cq.QuestionnaireTitle
FROM
	Outcomes o,
	ClientQuestionnaires cq,
	LeadTypeLink ltl,
	LeadType lt
WHERE            
	o.ClientQuestionnaireID = cq.ClientQuestionnaireID
AND	o.OutcomeID = ltl.OutcomeID
AND	ltl.LeadTypeID = lt.LeadTypeID
AND	cq.ClientID = @ClientID

ORDER BY 
	7, 6, 5



GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_GetByClientIDFull] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_GetByClientIDFull] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_GetByClientIDFull] TO [sp_executeall]
GO
