SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderMapSection table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderMapSectionID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderObjectActionID],
					[DataLoaderSectionLocaterTypeID],
					[DetailFieldSubTypeID],
					[HasAquariumID],
					[NumberOfFooterRowsToSkip],
					[ImportIntoLeadManager],
					[IsFixedLengthSection],
					[FixedLengthNumberOfRows],
					[IsMandatory],
					[IsMultipleAllowedWithinFile],
					[Notes],
					[TableDetailFieldID],
					[ResourceListDetailFieldID],
					[CreatePaymentSchedule]
				FROM
					[dbo].[DataLoaderMapSection] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_GetByClientID] TO [sp_executeall]
GO
