SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 04-Dec-2015
-- Description:	Return a scripting URL for the next valid task for a user
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Scripting_GetNextWorkflowTask]
(
	@UserID INT,
	@ScriptID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ReturnURL VARCHAR (100),
			@CaseID VARCHAR (100)
	DECLARE @RON Table (WorkflowTaskID INT ,CustomerID INT , LeadID INT, CaseID VARCHAR(50)) 
	
	INSERT @RON (WorkflowTaskID,CustomerID,LeadID,CaseID)
	EXEC WorkflowTask_GetATaskForThisUser @UserID 
	
	SELECT top 1  @CaseID = CAST(r.CaseID as VARCHAR(20))
	FROM @RON r 
		
	SELECT @ReturnURL = 'https://scripting.aquarium-software.com/default.aspx?sid=' + CAST(@ScriptID as VARCHAR(20)) + '&cs=' + @CaseID 
		
	SELECT @ReturnURL 
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Scripting_GetNextWorkflowTask] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Scripting_GetNextWorkflowTask] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Scripting_GetNextWorkflowTask] TO [sp_executeall]
GO
