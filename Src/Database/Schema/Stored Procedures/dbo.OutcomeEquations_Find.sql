SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the OutcomeEquations table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeEquations_Find]
(

	@SearchUsingOR bit   = null ,

	@OutcomeEquationID int   = null ,

	@OutcomeID int   = null ,

	@EquationID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OutcomeEquationID]
	, [OutcomeID]
	, [EquationID]
	, [ClientID]
    FROM
	[dbo].[OutcomeEquations] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeEquationID] = @OutcomeEquationID OR @OutcomeEquationID IS NULL)
	AND ([OutcomeID] = @OutcomeID OR @OutcomeID IS NULL)
	AND ([EquationID] = @EquationID OR @EquationID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OutcomeEquationID]
	, [OutcomeID]
	, [EquationID]
	, [ClientID]
    FROM
	[dbo].[OutcomeEquations] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeEquationID] = @OutcomeEquationID AND @OutcomeEquationID is not null)
	OR ([OutcomeID] = @OutcomeID AND @OutcomeID is not null)
	OR ([EquationID] = @EquationID AND @EquationID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeEquations_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Find] TO [sp_executeall]
GO
