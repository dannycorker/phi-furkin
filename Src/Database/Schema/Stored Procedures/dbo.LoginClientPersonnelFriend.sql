SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
	JWG 2008-04-09 Already logged in, but switching to a different user
*/

CREATE PROCEDURE [dbo].[LoginClientPersonnelFriend] 
	@UserID int
as
begin
	select	ClientID, 			ClientOfficeID,			FirstName, 			MiddleName, 			LastName, 			ClientPersonnelID, 			EmailAddress,			AttemptedLogins,			AccountDisabled	from ClientPersonnel	where ClientPersonnelID = @UserIDend






GO
GRANT VIEW DEFINITION ON  [dbo].[LoginClientPersonnelFriend] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LoginClientPersonnelFriend] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LoginClientPersonnelFriend] TO [sp_executeall]
GO
