SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-01-2014
-- Description:	Adds the Sms Command to the history table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertSmsCommandIntoHistoryTable] 

	@ClientID INT,
	@ClientPersonnelID INT = NULL,
	@CustomerID INT = NULL,
	@IssuedCommand VARCHAR(50),
	@CommandRecognised BIT,
	@ParameterValues VARCHAR(2000),
	@Success BIT

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @DateTimeTheCommandWasIssuedDetailField INT    
	DECLARE @CommandDetailField INT    
	DECLARE @CommandRecognisedDetailField INT    
	DECLARE @ParametersDetailField INT    
	DECLARE @SuccessDetailField INT    
	DECLARE @LeadTypeID INT
	
	DECLARE @TableDetailFieldID INT
	DECLARE @PageID INT
	DECLARE @TableRowID INT
	
	IF @ClientPersonnelID IS NOT NULL AND @ClientPersonnelID>0
	BEGIN
		SELECT @LeadTypeID = 2
	END
	ELSE IF @CustomerID IS NOT NULL AND @CustomerID>0
	BEGIN
		SELECT @LeadTypeID = 0
	END

	SELECT @TableDetailFieldID=DetailFieldID, @DateTimeTheCommandWasIssuedDetailField=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=795
	
	SELECT @PageID=DetailFieldPageID FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID=@TableDetailFieldID
	
	SELECT @CommandDetailField=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=796

	SELECT @CommandRecognisedDetailField=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=797

	SELECT @ParametersDetailField=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=798

	SELECT @SuccessDetailField=ColumnFieldID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=799

	DECLARE @SuccessInfo VARCHAR(2000)
	IF @Success=1
	BEGIN
		SELECT @SuccessInfo='true'
	END
	ELSE
	BEGIN
		SELECT @SuccessInfo='false'
	END
	
	DECLARE @CommandRecognisedInfo VARCHAR(2000)
	IF @CommandRecognised=1
	BEGIN
		SELECT @CommandRecognisedInfo='true'
	END
	ELSE
	BEGIN
		SELECT @CommandRecognisedInfo='false'
	END

	IF @ClientPersonnelID IS NOT NULL AND @ClientPersonnelID>0
	BEGIN	
		INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ClientPersonnelID)
		VALUES (@ClientID, @TableDetailFieldID,@PageID,1,1,@ClientPersonnelID)	
		SELECT @TableRowID = SCOPE_IDENTITY()	

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientPersonnelID, ClientID)		
		VALUES (@TableRowID, @DateTimeTheCommandWasIssuedDetailField, CONVERT(VARCHAR(20), dbo.fn_GetDate_Local(),120), @ClientPersonnelID, @ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientPersonnelID, ClientID)		
		VALUES (@TableRowID, @CommandDetailField, @IssuedCommand,@ClientPersonnelID,@ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientPersonnelID, ClientID)		
		VALUES (@TableRowID, @CommandRecognisedDetailField, @CommandRecognisedInfo,@ClientPersonnelID,@ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientPersonnelID, ClientID)		
		VALUES (@TableRowID, @ParametersDetailField, @ParameterValues,@ClientPersonnelID,@ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, ClientPersonnelID, ClientID)		
		VALUES (@TableRowID, @SuccessDetailField, @SuccessInfo, @ClientPersonnelID, @ClientID)
	END
	ELSE IF @CustomerID IS NOT NULL AND @CustomerID>0	
	BEGIN
	
		INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID)
		VALUES (@ClientID, @TableDetailFieldID, @PageID,1,1,@CustomerID)
		SELECT @TableRowID = SCOPE_IDENTITY()
		
		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID)		
		VALUES (@TableRowID, @DateTimeTheCommandWasIssuedDetailField, CONVERT(VARCHAR(20), dbo.fn_GetDate_Local(),120), @CustomerID, @ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID)		
		VALUES (@TableRowID, @CommandDetailField, @IssuedCommand,@CustomerID,@ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID)		
		VALUES (@TableRowID, @CommandRecognisedDetailField, @CommandRecognisedInfo,@CustomerID,@ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID)		
		VALUES (@TableRowID, @ParametersDetailField, @ParameterValues,@CustomerID,@ClientID)

		INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID)		
		VALUES (@TableRowID, @SuccessDetailField, @SuccessInfo, @CustomerID, @ClientID)
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertSmsCommandIntoHistoryTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertSmsCommandIntoHistoryTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertSmsCommandIntoHistoryTable] TO [sp_executeall]
GO
