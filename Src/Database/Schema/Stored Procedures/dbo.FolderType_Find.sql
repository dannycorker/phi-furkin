SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FolderType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FolderType_Find]
(

	@SearchUsingOR bit   = null ,

	@FolderTypeID int   = null ,

	@FolderTypeName varchar (50)  = null ,

	@FolderTypeDescription varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FolderTypeID]
	, [FolderTypeName]
	, [FolderTypeDescription]
    FROM
	[dbo].[FolderType] WITH (NOLOCK) 
    WHERE 
	 ([FolderTypeID] = @FolderTypeID OR @FolderTypeID IS NULL)
	AND ([FolderTypeName] = @FolderTypeName OR @FolderTypeName IS NULL)
	AND ([FolderTypeDescription] = @FolderTypeDescription OR @FolderTypeDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FolderTypeID]
	, [FolderTypeName]
	, [FolderTypeDescription]
    FROM
	[dbo].[FolderType] WITH (NOLOCK) 
    WHERE 
	 ([FolderTypeID] = @FolderTypeID AND @FolderTypeID is not null)
	OR ([FolderTypeName] = @FolderTypeName AND @FolderTypeName is not null)
	OR ([FolderTypeDescription] = @FolderTypeDescription AND @FolderTypeDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FolderType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Find] TO [sp_executeall]
GO
