SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadTypePageTitleSpecialisedDetailFieldTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadTypePageTitleSpecialisedDetailFieldTargetID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@Target varchar (250)  = null ,

	@DetailFieldID int   = null ,

	@ColumnField int   = null ,

	@TemplateTypeID int   = null ,

	@DetailFieldAlias varchar (500)  = null ,

	@ColumnFieldAlias varchar (500)  = null ,

	@RowField int   = null ,

	@Notes varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadTypePageTitleSpecialisedDetailFieldTargetID]
	, [ClientID]
	, [LeadTypeID]
	, [Target]
	, [DetailFieldID]
	, [ColumnField]
	, [TemplateTypeID]
	, [DetailFieldAlias]
	, [ColumnFieldAlias]
	, [RowField]
	, [Notes]
    FROM
	[dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypePageTitleSpecialisedDetailFieldTargetID] = @LeadTypePageTitleSpecialisedDetailFieldTargetID OR @LeadTypePageTitleSpecialisedDetailFieldTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ColumnField] = @ColumnField OR @ColumnField IS NULL)
	AND ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([DetailFieldAlias] = @DetailFieldAlias OR @DetailFieldAlias IS NULL)
	AND ([ColumnFieldAlias] = @ColumnFieldAlias OR @ColumnFieldAlias IS NULL)
	AND ([RowField] = @RowField OR @RowField IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadTypePageTitleSpecialisedDetailFieldTargetID]
	, [ClientID]
	, [LeadTypeID]
	, [Target]
	, [DetailFieldID]
	, [ColumnField]
	, [TemplateTypeID]
	, [DetailFieldAlias]
	, [ColumnFieldAlias]
	, [RowField]
	, [Notes]
    FROM
	[dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypePageTitleSpecialisedDetailFieldTargetID] = @LeadTypePageTitleSpecialisedDetailFieldTargetID AND @LeadTypePageTitleSpecialisedDetailFieldTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ColumnField] = @ColumnField AND @ColumnField is not null)
	OR ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([DetailFieldAlias] = @DetailFieldAlias AND @DetailFieldAlias is not null)
	OR ([ColumnFieldAlias] = @ColumnFieldAlias AND @ColumnFieldAlias is not null)
	OR ([RowField] = @RowField AND @RowField is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Find] TO [sp_executeall]
GO
