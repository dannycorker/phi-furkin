SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AssetType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_Find]
(

	@SearchUsingOR bit   = null ,

	@AssetTypeID int   = null ,

	@AssetSubTypeID int   = null ,

	@AssetTypeName varchar (255)  = null ,

	@Note varchar (255)  = null ,

	@Enabled bit   = null ,

	@Deleted bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AssetTypeID]
	, [AssetSubTypeID]
	, [AssetTypeName]
	, [Note]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[AssetType] WITH (NOLOCK) 
    WHERE 
	 ([AssetTypeID] = @AssetTypeID OR @AssetTypeID IS NULL)
	AND ([AssetSubTypeID] = @AssetSubTypeID OR @AssetSubTypeID IS NULL)
	AND ([AssetTypeName] = @AssetTypeName OR @AssetTypeName IS NULL)
	AND ([Note] = @Note OR @Note IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AssetTypeID]
	, [AssetSubTypeID]
	, [AssetTypeName]
	, [Note]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[AssetType] WITH (NOLOCK) 
    WHERE 
	 ([AssetTypeID] = @AssetTypeID AND @AssetTypeID is not null)
	OR ([AssetSubTypeID] = @AssetSubTypeID AND @AssetSubTypeID is not null)
	OR ([AssetTypeName] = @AssetTypeName AND @AssetTypeName is not null)
	OR ([Note] = @Note AND @Note is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Find] TO [sp_executeall]
GO
