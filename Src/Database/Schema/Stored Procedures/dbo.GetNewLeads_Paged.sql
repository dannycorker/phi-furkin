SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetNewLeads_Paged]
(
	@WhereClause varchar (2000),
	@OrderBy varchar (2000),
	@PageIndex int,
	@PageSize int
)
AS
	BEGIN
		DECLARE @PageLowerBound int
		DECLARE @PageUpperBound int
		DECLARE @RowsToReturn int
		DECLARE @TotalRows int
		-- First set the rowcount
		SET @RowsToReturn = @PageSize * (@PageIndex + 1)
		--SET ROWCOUNT @RowsToReturn
		-- Set the page bounds
		SET @PageLowerBound = @PageSize * @PageIndex
		SET @PageUpperBound = @PageLowerBound + @PageSize
		-- Create a temp table to store the select results
		CREATE TABLE #PageIndex
		(
			[IndexId] int IDENTITY (1, 1) NOT NULL,
		 	[CustomerID] int 
		)
		-- Insert into temp table
		DECLARE @SQL as nvarchar(3500)
		SET @SQL = 'INSERT INTO #PageIndex (CustomerID)'
		SET @SQL = @SQL + 'SELECT DISTINCT dbo.[Customers].[CustomerID]'
		SET @SQL = @SQL + ' FROM dbo.[Customers]'
		SET @SQL = @SQL + ' INNER JOIN dbo.[CustomerQuestionnaires] ON dbo.[Customers].[CustomerID] = dbo.[CustomerQuestionnaires].[CustomerID]'
		SET @SQL = @SQL + ' INNER JOIN dbo.[ClientQuestionnaires] ON dbo.[CustomerQuestionnaires].[ClientQuestionnaireID] = dbo.[ClientQuestionnaires].[ClientQuestionnaireID]'
		IF LEN(@WhereClause) > 0
		BEGIN
			SET @SQL = @SQL + ' WHERE dbo.[Customers].[AquariumStatusID] = 1 AND ' + @WhereClause
		END
		IF LEN(@OrderBy) > 0
		BEGIN
			SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
		END
		-- Populate the temp table
		exec sp_executesql @SQL
		-- Tell the calling program how many rows are available in total
		SET @TotalRows = @@ROWCOUNT
		-- Set RowCount after TotalRows is determined
		SET ROWCOUNT @RowsToReturn
		-- Return paged results
		SELECT
			@TotalRows as TotalRows,
			CUS.CustomerID,
			CUS.Fullname,
			CUS.EmailAddress,
			CUS.DayTimeTelephoneNumber,
			CUS.Town,
			CUS.County, 
	                   	CUQ.CustomerQuestionnaireID,
			CUQ.ClientQuestionnaireID,
			CLQ.QuestionnaireTitle, 
	                   	CUS.AquariumStatusID,
			CUS.ClientID
		FROM		Customers CUS
		INNER JOIN	CustomerQuestionnaires CUQ ON CUS.CustomerID = CUQ.CustomerID
		INNER JOIN	ClientQuestionnaires CLQ ON CUQ.ClientQuestionnaireID = CLQ.ClientQuestionnaireID
		INNER JOIN	#PageIndex PageIndex ON CUS.[CustomerID] = PageIndex.[CustomerID]
		 WHERE	PageIndex.IndexID > @PageLowerBound
		AND		PageIndex.IndexID <= @PageUpperBound
		ORDER BY	PageIndex.IndexID
	END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetNewLeads_Paged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetNewLeads_Paged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetNewLeads_Paged] TO [sp_executeall]
GO
