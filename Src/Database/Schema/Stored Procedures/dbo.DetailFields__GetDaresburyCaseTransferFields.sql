SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Ben Crinion
-- Create date: 24th April 2008
-- Description:	Gets ...
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetDaresburyCaseTransferFields]
	@InboundLeadTypeID int,
	@OutboundLeadTypeID int
AS
BEGIN
		
DECLARE @SharedFields TABLE(
	[O_DetailFieldAliasID] [int] NOT NULL,
	[O_ClientID] [int] NOT NULL,
	[O_LeadTypeID] [int] NULL,
	[O_DetailFieldID] [int] NOT NULL,
	[O_DetailFieldAlias] [varchar](500),
	[I_DetailFieldAliasID] [int]  NOT NULL,
	[I_ClientID] [int] NOT NULL,
	[I_LeadTypeID] [int] NULL,
	[I_DetailFieldID] [int] NOT NULL,
	[I_DetailFieldAlias] [varchar](500)
)

insert into @SharedFields
SELECT		snd.*,recv.*
FROM		DetailFieldAlias snd 
INNER JOIN	DetailFieldAlias recv on snd.DetailFieldAlias = recv.DetailFieldAlias and snd.Leadtypeid = @OutboundLeadTypeID
INNER JOIN  DetailFields df on df.DetailFieldID = recv.DetailFieldID
WHERE 
	recv.leadtypeid = @InboundLeadTypeID
	AND df.LeadOrMatter = 2

SELECT * FROM @SharedFields

SELECT		df.* 
FROM		DetailFields df
INNER JOIN	@SharedFields on O_DetailFieldID = df.DetailFieldID
WHERE		NOT EXISTS (SELECT * FROM CaseTransferRestrictedField rf WHERE rf.DetailFieldID = O_DetailFieldID)


SELECT		df.*
FROM		DetailFields df
INNER JOIN	@SharedFields on I_DetailFieldID = df.DetailFieldID
AND NOT EXISTS (SELECT * FROM CaseTransferRestrictedField rf WHERE rf.DetailFieldID = I_DetailFieldID)


END









GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetDaresburyCaseTransferFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetDaresburyCaseTransferFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetDaresburyCaseTransferFields] TO [sp_executeall]
GO
