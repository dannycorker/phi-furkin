SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetLeadsToAssign] AS

SELECT     	Lead.LeadID,
		Customers.CustomerID,
		Customers.Fullname, LeadType.LeadTypeID, LeadType.LeadTypeDescription, 
                      CustomerQuestionnaires.SubmissionDate, Customers.ClientID, Lead.AquariumStatusID
FROM         Customers INNER JOIN
                      CustomerQuestionnaires ON Customers.CustomerID = CustomerQuestionnaires.CustomerID INNER JOIN
                      Lead ON Customers.CustomerID = Lead.CustomerID INNER JOIN
                      LeadType ON Lead.LeadTypeID = LeadType.LeadTypeID
WHERE     (Customers.ClientID = 3) AND (Lead.AquariumStatusID = 2)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadsToAssign] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadsToAssign] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadsToAssign] TO [sp_executeall]
GO
