SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderSectionLocaterType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterType_Delete]
(

	@DataLoaderSectionLocaterTypeID int   
)
AS


				DELETE FROM [dbo].[DataLoaderSectionLocaterType] WITH (ROWLOCK) 
				WHERE
					[DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Delete] TO [sp_executeall]
GO
