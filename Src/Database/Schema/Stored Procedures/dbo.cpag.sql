SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-28
-- Description:	List all ClientPersonnelAdminGroups for a client
-- =============================================
CREATE PROCEDURE [dbo].[cpag] 
	@ClientID int = NULL,
	@GroupID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	IF @ClientID IS NULL
	BEGIN
		SELECT * 
		FROM dbo.ClientPersonnelAdminGroups cpag (nolock) 
		WHERE (@GroupID IS NULL OR @GroupID = cpag.ClientPersonnelAdminGroupID)
		ORDER BY cpag.GroupName ASC 
	END
	ELSE
	BEGIN
		SELECT * 
		FROM dbo.ClientPersonnelAdminGroups cpag (nolock) 
		WHERE cpag.ClientID = @ClientID 
		ORDER BY cpag.GroupName ASC 
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[cpag] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cpag] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cpag] TO [sp_executeall]
GO
