SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-03-03
-- Description:	Populate the custom part routing table
-- =============================================
CREATE PROCEDURE [dbo].[CustomPartRouting_Insert]
	@ClientID INT,
	@LeadTypeID INT,
	@PartID INT,
	@MapsToLeadTypeID INT,
	@MapsToClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO CustomPartRouting (ClientID, LeadTypeID, PartID, MapsToLeadTypeID, MapsToClientID)
	VALUES (@ClientID, @LeadTypeID, @PartID, @MapsToLeadTypeID, @MapsToClientID)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomPartRouting_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomPartRouting_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomPartRouting_Insert] TO [sp_executeall]
GO
