SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-06-2014
-- Description:	Gets a translation record
-- =============================================
CREATE PROCEDURE [dbo].[Translation__GetByIdentityAndFieldID]

	@ClientID INT, 
	@ID INT,	
	@DetailFieldID INT,
	@LanguageID INT = NULL -- NULL OR 1 is default English

AS
BEGIN

	SET NOCOUNT ON;

		IF @LanguageID IS NULL
		BEGIN
			SELECT @LanguageID = 1
		END

		IF @DetailFieldID IS NULL OR @DetailFieldID = 0 OR @DetailFieldID = -1
		BEGIN

			SELECT * 
			FROM 
				TRANSLATION t WITH (NOLOCK) 
			WHERE 
				t.ClientID = @ClientID AND 
				t.ID = @ID AND
				t.DetailFieldID IS NULL AND
				t.LanguageID = @LanguageID

		END
		ELSE
		BEGIN

			SELECT * 
			FROM 
				TRANSLATION t WITH (NOLOCK) 
			WHERE 
				t.ClientID = @ClientID AND 
				t.ID = @ID AND
				t.DetailFieldID = @DetailFieldID AND
				t.LanguageID = @LanguageID

		END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Translation__GetByIdentityAndFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Translation__GetByIdentityAndFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Translation__GetByIdentityAndFieldID] TO [sp_executeall]
GO
