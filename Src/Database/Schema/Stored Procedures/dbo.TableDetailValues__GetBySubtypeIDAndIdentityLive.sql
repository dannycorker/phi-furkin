SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-12
-- Description:	Gets a list of table detail values based upon subtype id and the given identity
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityLive]
(
	@tableRowID INT, 
	@identity INT, 
	@detailFieldSubTypeID INT, 
	@detailFieldID INT, 		
	@clientID INT
)

AS

	--SELECT * FROM TableDetailValues WHERE ClientID = @ClientID

	DECLARE @simpleClause VARCHAR(MAX) = '',
	@query VARCHAR(MAX) = ''
	
	SELECT @simpleClause = CASE 
		WHEN @detailFieldSubTypeID = 1 THEN ' AND (td.LeadID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 2 THEN ' AND (td.MatterID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 10 THEN ' AND (td.CustomerID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 11 THEN ' AND (td.CaseID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 12 THEN 'AND (td.ClientID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 13 THEN ' AND (td.ClientPersonnelID = ' + CAST(@identity AS VARCHAR) + ')'
		WHEN @detailFieldSubTypeID = 14 THEN ' AND (td.ContactID = ' + CAST(@identity AS VARCHAR) + ')'
	END

    SET @query = 'SELECT * 
    FROM
	[dbo].[TableDetailValues] td WITH (NOLOCK) 
    WHERE 	 
	([TableRowID] = ' + CAST(@TableRowID AS VARCHAR) + ')	
	AND ([DetailFieldID] = ' + CAST(@DetailFieldID AS VARCHAR) + ')	
	AND ([ClientID] = ' + CAST(@ClientID AS VARCHAR) + ') ' + @simpleClause

	EXEC (@query)
						
	SELECT @@ROWCOUNT		





GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityLive] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityLive] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetBySubtypeIDAndIdentityLive] TO [sp_executeall]
GO
