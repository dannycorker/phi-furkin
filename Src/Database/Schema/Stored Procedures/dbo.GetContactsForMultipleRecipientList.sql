SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Contact table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetContactsForMultipleRecipientList]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[Titles].[Title] as 'Title',
					[Contact].[ContactID],
					[Contact].[Fullname] as 'Full Name',
					[Contact].[Notes]
				FROM
					[dbo].[Contact] WITH (NOLOCK)
				INNER JOIN
					[Titles] WITH (NOLOCK) ON [Contact].[TitleID] = [Titles].[TitleID]
				--INNER JOIN
                --    [Department] ON [Contact].[DepartmentID] = [Department].[DepartmentID]
				--INNER JOIN
                --    [CustomerOffice] ON [Contact].[OfficeID] = [CustomerOffice].[OfficeID]
				WHERE
					[Contact].[CustomerID] = @CustomerID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			







GO
GRANT VIEW DEFINITION ON  [dbo].[GetContactsForMultipleRecipientList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetContactsForMultipleRecipientList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetContactsForMultipleRecipientList] TO [sp_executeall]
GO
