SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-30
-- Description:	Copy all User Rights etc to temp/work table
-- JWG Compare ISNULL(LeadTypeID) in UserFunctionControl as well as UserRightsDynamic #19315
-- =============================================
CREATE PROCEDURE [dbo].[CommitUserRightsForEditing]
	@GroupID INT,
	@UserID INT 
AS
BEGIN
	
	SET NOCOUNT ON;

	BEGIN TRAN

	DELETE UserRightsDynamic 
	WHERE ClientPersonnelID = @UserID

	DELETE UserFunctionControl
	WHERE ClientPersonnelID = @UserID

	INSERT UserFunctionControl (ClientPersonnelID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)
	SELECT ClientPersonnelID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID
	FROM UserFunctionControlEditing
	WHERE ClientPersonnelID = @UserID
	AND NOT EXISTS(
		SELECT * 
		FROM GroupFunctionControl
		WHERE UserFunctionControlEditing.FunctionTypeID = GroupFunctionControl.FunctionTypeID
		AND UserFunctionControlEditing.ModuleID = GroupFunctionControl.ModuleID 
		AND UserFunctionControlEditing.FunctionTypeID = GroupFunctionControl.FunctionTypeID
		AND UserFunctionControlEditing.RightID = GroupFunctionControl.RightID
		AND UserFunctionControlEditing.HasDescendants = GroupFunctionControl.HasDescendants
		AND ISNULL(UserFunctionControlEditing.LeadTypeID, -1) = ISNULL(GroupFunctionControl.LeadTypeID, -1) 
		AND GroupFunctionControl.ClientPersonnelAdminGroupID = @GroupID
		AND UserFunctionControlEditing.ClientPersonnelID = @UserID
	)

	INSERT UserRightsDynamic (ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
	SELECT ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID
	FROM UserRightsDynamicEditing
	WHERE ClientPersonnelID = @UserID
	AND NOT EXISTS(
		SELECT * 
		FROM GroupRightsDynamic
		WHERE UserRightsDynamicEditing.FunctionTypeID = GroupRightsDynamic.FunctionTypeID
		AND UserRightsDynamicEditing.ObjectID = GroupRightsDynamic.ObjectID
		AND UserRightsDynamicEditing.RightID = GroupRightsDynamic.RightID
		AND ISNULL(UserRightsDynamicEditing.LeadTypeID, -1) = ISNULL(GroupRightsDynamic.LeadTypeID, -1)
		AND GroupRightsDynamic.ClientPersonnelAdminGroupID = @GroupID
		AND UserRightsDynamicEditing.ClientPersonnelID = @UserID
	)

	DELETE UserRightsDynamicEditing 
	WHERE ClientPersonnelID = @UserID

	DELETE UserFunctionControlEditing
	WHERE ClientPersonnelID = @UserID

	COMMIT

END










GO
GRANT VIEW DEFINITION ON  [dbo].[CommitUserRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CommitUserRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CommitUserRightsForEditing] TO [sp_executeall]
GO
