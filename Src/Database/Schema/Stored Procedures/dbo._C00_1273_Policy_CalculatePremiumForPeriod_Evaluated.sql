SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-06
-- Description:	Called from fast rater to calc monthly premium
-- 2019-10-13 GPR created to 603 for LPC-35
-- 2019-10-14 GPR removed 433 specific premium split: Insurer premium vs TPL Insurer premium
-- 2019-10-15 CPS for JIRA LPC-35  | Removed _C600_PA_Policy_SaveCoverPeriodRiskData as this is now held in QuotePetProduct
-- 2019-10-15 CPS for JIRA LPC-35  | Use fn_C600_GetDatabaseSpecificConfigValue to decide whether or not to save the QuotePetProduct XML
-- 2019-10-15 CPS for JIRA LPC-37  | Removed Cover Period Sequence Number
-- 2019-10-21 CPS for JIRA LPC-37  | Brought _C600_UpdatePDHwithCommissionValues call over from _C00_1273_Policy_CalculatePremiumForPeriod
-- 2019-11-22 CPS for JIRA SDLPC-8 | Included PH MTA 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with select from LeadEvent
-- 2020-01-21 CPS for JIRA LPC-395 | Include annualised Admin Fee
-- 2020-02-04 ACE Cleaned up the format and swapped history table for fat table.
-- 2020-02-13 CPS for JIRA AAG-106 | Completefd the above
-- 2020-02-24 GPR | Added ISNULL to 0.00 for 'AnnualDiscountCommissionSacrifice' and 'AnnualDiscountPremiumAffecting'
-- 2020-03-09 GPR | Added evaluation of amount to deduct from Underwriter / Broker commission when a discount is applied to a Policy at New Business or Renewal - for AAG-136 / AAG-292
-- 2020-03-18 NG  for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- 2020-03-24 GPR | Added AnnualLocalTaxRate, AnnualPriceForRiskLocalTax, AdjustmentValueLocalTax to INSERT for AAG-507
-- 2020-03-24 AAD for AAG-457 | Ensure correct values are inserted into WrittenPremium for cancellations
-- 2020-04-23 GPR | Added assignment of AdjustmentAmountNet and Tax for cancellation adjustment
-- 2020-04-24 GPR | Removed setting of adjustment values for Cancellations in this proc, update of WrittenPremium for cancellations will occur in SAE post-paymentschedule update
-- 2020-06-17 AAD | When finding @ToDate at renewal, +1 year -1 day instead of just -1 day
-- 2020-08-20 ALM | Updated for US Cancellation Logic for PPET-110
-- 2020-08-26 SB  | Handle the rate being < 1 for US and > 1 as default
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod_Evaluated]
(
	 @LeadEventID		INT
	,@RuleSetID			INT
	,@EvaluatedXml		XML
	,@PassedOverrides	dbo.tvpIntVarcharVarchar READONLY
	,@CalculateTypeID	INT = NULL
	,@DisplayOnly		BIT = 0
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @CustomerID						INT
			,@LeadID							INT
			,@CaseID							INT
			,@MatterID							INT
			,@WhoCreated						INT
			,@EventComments						VARCHAR(2000) = ''
			,@ClientID							INT
			,@ProcName							VARCHAR(2000) = OBJECT_NAME(@@ProcID)
			,@QuotePetProductID					INT
			,@FromDate							DATE
			,@AdjustmentType					INT
			,@OldwpID							INT
			--,@TableRowID						INT
			,@PetName							VARCHAR(2000)
			,@PremiumCalculationID				INT
			,@AnnualPremium						DECIMAL(18, 2)
			,@FirstMonthly						DECIMAL(18, 2)
			,@RecurringMonthly					DECIMAL(18, 2)
			,@Discount							DECIMAL(18, 2)=0
			,@Commission						DECIMAL(18, 2) = 0
			,@DiscountGrossNet					DECIMAL(18, 2)
			,@IPT								DECIMAL(18, 2)
			,@EventName							VARCHAR(250) = 'Unknown'
			,@EventTypeID						INT
			,@AdjustmentAmount					DECIMAL(18,2)
			,@OriginalAdjustment				DECIMAL(18,2)
			,@Status							INT
			,@PDHTableRowID						INT
			,@ToDate							DATE
			,@VarcharDate						VARCHAR(10)
			,@InceptionDate						DATE
			,@LatestRowStatusLookupListItemID	INT
			,@ProductID							INT
			,@Net								DECIMAL (18,2) 
			,@GrossNet							DECIMAL (18,2) 
			,@PAFIfBeforeIPT					DECIMAL (18,2)
			,@PAFBeforeIPTGrossNet				DECIMAL (18,2)
			,@IPTGrossNet						DECIMAL (18,2)
			,@PAFIfAfterIPT						DECIMAL (18,2)
			,@GrossGross						DECIMAL (18,2)
			,@PremiumLessDiscount				DECIMAL (18,2)
			,@CustomerDoB						DATE
			,@AdjustmentIPT						DECIMAL(18,2)
			,@UseLatestLiveRow					BIT
			,@CloseLatestLiveRow				BIT
			,@UpdateCurrentAndPreviousPremium	BIT
			,@ErrorMessage						VARCHAR(2000)
			,@DF175396							VARCHAR(2000)
			,@DF175397							VARCHAR(2000)
			,@DF175400							VARCHAR(2000)
			,@DF180295							VARCHAR(2000)
			,@DF180171							VARCHAR(2000)
			,@DF175347							VARCHAR(2000)
			,@OutputData						XML
			,@AffinityID						INT
			,@DaysInYear						INT
			,@PurchasedProductID				INT = 0
			,@AnnualCommissionValue				MONEY
			,@TranCommissionValue				MONEY
			,@CommissionPercentageValue			DECIMAL(18,2) /*GPR 2020-01-12*/ -- 2020-02-12 CPS for JIRA AAG-106 | This could also be a decimal value.  Changed from INT to DECIMAL(18,2)
			,@GrossDiscountedPremium			MONEY /*GPR 2020-01-12*/
			,@DaysRemaining						INT
			,@RawPremium						NUMERIC(18,2)
			,@AdjustmentTypeID					INT
			,@PolEnd							DATE
			,@WhenCreated						DATETIME
			,@WrittenPremiumID					INT
			,@DaysUsed							INT
			,@StartDate							DATE
			,@EndDate							DATE
			,@DailyPremium						DECIMAL(18,4)
			,@UsedPremium						DECIMAL(18,4)
			,@AdminFee							DECIMAL(18,2)
			,@IPTRate							DECIMAL(18,4)
			,@BrokerDiscountAccountability		DECIMAL(18,2)
			,@UnderwriterDiscountAccountability DECIMAL(18,2)
			,@LocalTaxRate						DECIMAL(18,4)
			,@LocalTax							DECIMAL(18,2)
			,@CancellationReason				INT
			,@State								VARCHAR(10)
			,@CancellationType					INT
			,@DaysIntoPolicyYear				INT
			,@IsRenewal							BIT
			,@CoolingOffPeriod					INT
			,@CountryID							INT
			,@County							VARCHAR(100)
			,@BrandID							INT
			,@ProvinceLookupListItemID			INT
			,@PetIndex							INT
			,@AdjustmentAdminFee				DECIMAL(18,4) = 0.00 /*GPR 2021-03-01  for FURKIN-324*/
			,@StateCode							VARCHAR(50)
			,@AdjustmentCommission				NUMERIC(18,2)
			,@ContextID							INT = 1
	DECLARE @TableValuesToInsert TABLE (DetailFieldID INT, DetailValue VARCHAR(1000))

	DECLARE @Policies TABLE (LeadID INT, PolicyNumber INT)

	SELECT	 @CustomerID	= m.CustomerID
			,@LeadID		= m.LeadID
			,@CaseID		= m.CaseID
			,@MatterID		= m.MatterID
			,@WhoCreated	= le.WhoCreated
			,@ClientID		= le.ClientID
			,@EventName		= CASE WHEN et.EventTypeID IN (155196,158931) THEN 'New Policy' ELSE et.EventTypeName END
			,@EventTypeID	= et.EventTypeID
			,@AdjustmentType = CASE et.EventTypeID 
								WHEN 155196 THEN 74527 -- Create linked leads and complete set-up
								WHEN 158931 THEN 74527 -- New Business - Calculate Premium
								WHEN 155235 THEN 74528 -- Renewal
								WHEN 155262 THEN 74529 -- cancelled from PA
								WHEN 155374 THEN 74529 -- cancelled from collections
								WHEN 156748 THEN 74529 -- cancelled from claims
								WHEN 155278 THEN 74532
								WHEN 155303 THEN 74531 
								WHEN 156670 THEN 74532 -- Mid Term Adjustment | 2019-11-22 CPS for JIRA SDLPC-8 | Included Policy Holder MTA
								ELSE 0 
							   END
			,@WhenCreated	= le.WhenCreated
			,@CountryID		= cu.CountryID
			,@County		= cu.County
	FROM dbo.LeadEvent le WITH ( NOLOCK ) 
	INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON le.EventTypeID=et.EventTypeID
	INNER JOIN dbo.Customers cu WITH (NOLOCK) ON Cu.CustomerID = m.CustomerID
	WHERE le.LeadEventID = @LeadEventID

	SELECT @EventComments = '|' + @ProcName + '|' + CHAR(13)+CHAR(10)

	SELECT	 @StartDate	= dbo.fnGetSimpleDvAsDate(170036,@MatterID) /*Policy Start Date*/
			,@EndDate	= dbo.fnGetSimpleDvAsDate(170037,@MatterID) /*Policy End Date*/


	/*
	This is where the database rater would have called into RulesEngine_Evaluate_RuleSet.
	With the C# based rater we've already done that work, so just save off the checkpoints and proceed as normal.
	*/
	--	INSERT @PremiumData
	--	EXEC @QuotePetProductID = _C00_1273_Policy_CalculatePremium    @CaseID, @StartDate, @TermsDate, NULL, @Overrides, @FromDate, NULL, @LeadEventID	 -- CPS 2018-06-05 return the QuotePetProductID and pass the LeadEventiD

	IF @CalculateTypeID = 5 /*Cancellation*/
	BEGIN
		SELECT TOP 1 @PremiumCalculationID = wp.PremiumCalculationID
					,@QuotePetProductID = wp.QuotePetProductID
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		ORDER BY WrittenPremiumID DESC
	END
	ELSE
	IF @CalculateTypeID = 7 /*Reinstatement*/
	BEGIN
		SELECT TOP 1 @PremiumCalculationID = wp.PremiumCalculationID
					,@QuotePetProductID = wp.QuotePetProductID
					,@PurchasedProductID = wp.PurchasedProductID /*GPR/ALM 2020-12-10 for SDPRU-189*/
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4
		ORDER BY WrittenPremiumID DESC
	END
	ELSE
	BEGIN
		/*Create a PremiumCalculationDetail record to store this calculation*/
		INSERT dbo.PremiumCalculationDetail(ClientID, ProductCostBreakdown, WhoCreated, WhenCreated, LeadEventID)
		VALUES (@ClientID,@EvaluatedXml,@WhoCreated,dbo.fn_GetDate_Local(),@LeadEventID)
		/*!!NO SQL HERE!!*/
		SELECT @PremiumCalculationID = SCOPE_IDENTITY()
	END

	/*Save the calculation steps*/
	EXEC dbo._C00_1273_Premium_StoreCalculationValues @ClientID, @EvaluatedXml, @PremiumCalculationID

	/*CPS 2018-07-06 created a CalculationType table to track this config properly*/
	SELECT	 @UseLatestLiveRow					= ct.UseLatestLiveRow
			,@CloseLatestLiveRow				= ct.CloseLatestLiveRow
			,@LatestRowStatusLookupListItemID	= ct.LatestLiveRowStatusID
			,@UpdateCurrentAndPreviousPremium	= ct.UpdateCurrentAndPreviousPremium
	FROM dbo.CalculationType ct WITH ( NOLOCK )
	WHERE ct.CalculationTypeID = @CalculateTypeID

	/*GPR 2020-11-18*/
	DECLARE @MultipetRel DECIMAL(18,2)
	IF @CalculateTypeID IN (2/*Renewal*/,8/*New Policy*/)
	BEGIN

			/*GPR/ALM 2020-11-25 for SDPRU-160 - altered logic and now use RuleTransformValue in place of RuleOutput*/
			SELECT TOP 1 @MultipetRel = ISNULL(NULLIF(pcdv.RuleTransformValue,''),1)
			FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
			WHERE pcdv.RuleCheckpoint = 'Multiple Pet'
			AND  pcdv.PremiumCalculationDetailID = @PremiumCalculationID

			IF @MultipetRel < 1
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 179923, 5144, @MatterID,@WhoCreated	-- 	MultiPet Discount For Period = Yes
				EXEC dbo._C00_SimpleValueIntoField 179924, @MultipetRel, @MatterID,@WhoCreated	-- 	MultiPet Relativity
			END
			ELSE
			BEGIN	
				EXEC dbo._C00_SimpleValueIntoField 179923, 5145, @MatterID,@WhoCreated	-- 	MultiPet Discount For Period = No	
				EXEC dbo._C00_SimpleValueIntoField 179924, @MultipetRel, @MatterID,@WhoCreated	-- 	MultiPet Relativity
			END

	END

	IF @CalculateTypeID NOT IN ( 5 /*Cancellation*/,7 /*Reinstatement*/ )
	BEGIN
		SELECT	 @PetName	= dbo.fnGetSimpleDv(144268,@LeadID) /*Pet Name*/
				,@ProductID = dbo.fnGetSimpleDvAsInt(170034,@MatterID) /*Current Policy*/

		SELECT @CustomerDoB = cu.DateOfBirth
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID

		/*Save the calculation data to MI quote tables*/
		EXEC @QuotePetProductID = dbo.Quote_SaveQuoteSingleCompleteSet	 @QuoteSessionID			= NULL
																		,@TestFileItemID			= NULL
																		,@MatterID					= @MatterID
																		,@WhoCreated				= @WhoCreated
																		,@SourceID					= @LeadEventID
																		,@ChangeNotes				= @ProcName
																		,@ProductID					= @ProductID
																		,@PetName					= @PetName
																		,@Overrides					= @PassedOverrides
																		,@RuleSetID					= @RuleSetID
																		,@RulesEngineOutcome		= @AnnualPremium
																		,@SaveCheckpoints			= 1
																		,@QuoteXml					= NULL
																		,@ResponseXml				= @EvaluatedXml
																		,@ParentQuotePetProductID	= NULL
																		,@LeadEventID				= @LeadEventID
																		,@CustomerDoB				= @CustomerDoB
																		,@ExistingCustomerID		= @CustomerID
	END

	SELECT 	 
		 @AnnualPremium			= fn.AnnualPremium
		,@FirstMonthly			= fn.FirstMonthly
		,@RecurringMonthly		= fn.RecurringMonthly
		,@Net					= fn.Net
		,@Commission			= fn.Commission
		,@GrossNet				= fn.GrossNet
		,@Discount				= fn.Discount
		,@DiscountGrossNet		= fn.DiscountGrossNet
		,@PAFIfBeforeIPT		= fn.PAFIfBeforeIPT
		,@PAFBeforeIPTGrossNet	= fn.PAFBeforeIPTGrossNet
		,@IPT					= fn.IPT
		,@IPTGrossNet			= fn.IPTGrossNet
		,@PAFIfAfterIPT			= fn.PAFIfAfterIPT
		,@GrossGross			= fn.GrossGross
		,@PremiumCalculationID	= fn.PremiumCalculationID
		,@RuleSetID				= fn.RuleSetID
		,@PremiumLessDiscount	= fn.PremiumLessDiscount
		,@LocalTax				= fn.LocalTax
	FROM dbo.fn_RulesEngine_PremiumCalculationDataBreakdown(@PremiumCalculationID) fn

	/*ALM 2020-11-13 SDPRU-144*/
	IF EXISTS (SELECT TOP 1 pp.ProductAdditionalFee FROM PurchasedProduct pp WITH (NOLOCK)
				WHERE pp.ObjectID = @MatterID
				AND pp.ProductAdditionalFee > 0.00
				ORDER BY 1 DESC) 
	BEGIN 
		
		SET @RecurringMonthly = @RecurringMonthly + 2.00

	END

	EXEC _C00_LogIt 'NationalTax',@ProcName,'NationalTax',@IPT,58550
	EXEC _C00_LogIt 'LocalTax',@ProcName,'LocalTax',@LocalTax,58550

	IF dbo.fn_C600_GetDatabaseSpecificConfigValue('SaveQuotePetProductXML') = 'true'	
	BEGIN

		UPDATE qpp
		SET EvaluatedXml = @EvaluatedXml
		FROM dbo.QuotePetProduct qpp WITH ( NOLOCK )
		WHERE qpp.QuotePetProductID = @QuotePetProductID

	END
	
	/*Raise an error if any mandatory checkpoints are not matched*/
	SELECT @ErrorMessage = ISNULL(@ErrorMessage,'Error during premium calculation.' + CHAR(13)+CHAR(10)) 
						 + CHAR(13)+CHAR(10) + fn.ErrorMessage
	FROM dbo.fn_C600_RulesEngineMandatoryCheckpointCheck(@PremiumCalculationID) fn
	
	IF @ErrorMessage <> '' AND @CaseID <> 0
	BEGIN

		EXEC dbo._C00_AddANote @CaseID, 945 /*Premium Calculation Error Identified*/, @ErrorMessage, 0, @WhoCreated, 0, 1
		EXEC dbo._C00_SimpleValueIntoField 179785, 'true', @MatterID, @WhoCreated /*MTA Calculation Error Identified*/
		EXEC dbo._C00_SimpleValueIntoField 177911, 76361 /*Calculation Error*/, @MatterID, @WhoCreated /*Select reason for aborting MTA*/

	END
	
	SELECT TOP (1) @OldwpID = wp.WrittenPremiumID
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.MatterID = @MatterID
	AND wp.AdjustmentTypeID <> 9 /*MTA Quote*/
	AND wp.Active = 1
	ORDER BY wp.WrittenPremiumID DESC

	SELECT	 @EventComments += 'Previous WrittenPremiumID ' + ISNULL(CONVERT(VARCHAR,@OldwpID),'NULL') + CHAR(13)+CHAR(10)

	EXEC _C00_LogIt 'AAG-28',@ProcName,'@CalculateTypeID',@CalculateTypeID,0

	/* 
		Setting 'from' and 'to' dates:
			First row in the table: From = premium start date & To = blank
			A quote: From = premium start date & To = blank
			An adjustment:  To (on last row) = adjustment date - 1 day; From (on new row) = adjustment date
			A renewal:  To (on last row) = renewal date - 1 day; From (on new row) = renewal date
			Cancellation: To (on last row of table) = cancellation date; no new row
			Re-instatement: blank out To on last row of table (handled elsewhere)
	*/

	IF @EventTypeID IN (155196,158931) /*Create linked leads and complete set-up*/ /*New Business - Calculate Premium*/
	BEGIN

		EXEC dbo._C00_SimpleValueIntoField 178144, @RuleSetID, @MatterID, @WhoCreated /*New Business RuleSetID*/

	END

	IF @OldwpID IS NULL -- first row
		OR @CalculateTypeID IN (4,8) -- quote, new policy
	BEGIN
		
		SELECT @FromDate=dbo.fn_C00_1273_GetPremiumCalculationStartDate (@CaseID,dbo.fnGetDvAsDate(170036,@CaseID)),
				@Status=CASE WHEN @CalculateTypeID=8 THEN 72326 /*Live*/
													 ELSE 72329 /*Other*/
													 END

	END
	ELSE 
	IF @CalculateTypeID IN (2) -- Renewal
	BEGIN

		SELECT @FromDate=dbo.fnGetDv (175307,@CaseID) /*Policy Renewal Date*/
		SELECT @ToDate=CONVERT(VARCHAR(10),DATEADD(YEAR,1,DATEADD(DAY,-1,@FromDate)),120), /* 2020-06-17 AAD - +1 year, -1 day instead of just -1 day */
				@Status=72329 /*Other*/

	END
	ELSE 
	IF @CalculateTypeID IN (3,9) -- MTA or pre MTA
	BEGIN

		/*GPR/ALM 2020-12-10 for MTA observed when testing SDPRU-189*/
		SELECT TOP(1) @PurchasedProductID = wp.PurchasedProductID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		ORDER BY 1 DESC

		SELECT	 @ToDate	= @EndDate
				,@FromDate	= dbo.fnGetDv (175442,@CaseID) /*Adjustment Date*/
				,@Status	= CASE WHEN @CalculateTypeID=3	
									THEN 72326 /*Live*/
									ELSE 74514 /*MTA*/
							  END

		-- if we are applying the MTA, mark the pre MTA row as not applied
		IF @CalculateTypeID=3
		BEGIN

			UPDATE dbo.MatterDetailValues
			SET DetailValue=72328 /*Not applied*/
			WHERE DetailFieldID=175722 /*Status*/
			AND MatterID=@MatterID AND ValueInt=74514

		END
		
	END
	ELSE 
	IF @CalculateTypeID = 5 -- cancellation  -- CPS 2018-05-14 updated to use billing config on advice of JEL
	BEGIN

		SELECT @InceptionDate = dbo.fnGetSimpleDvAsDate(170035,@MatterID),
				@FromDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID),
				@CancellationReason = dbo.fnGetSimpleDvAsInt(170054,@MatterID),
				@StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)
		
		/*2020-08-20 ALM PPET-110*/
		IF @CountryID = 233 /*United States*/
		BEGIN

			SELECT @State = cu.County 
			FROM Customers cu WITH (NOLOCK) 
			WHERE cu.CustomerID = @CustomerID 

			SELECT @CancellationType = 
			CASE 
				WHEN @CancellationReason IN (69907,69911,72031,72032,72033,72035,72037,72038,74421,74519,74521,74523,74525,75135,75136,75141) THEN 1 
				ELSE 2 
			END

			SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())

			SELECT @IsRenewal = 
				CASE 
					WHEN COUNT (*) > 1 THEN 1 
					ELSE 0 
				END 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID

			SELECT @CoolingOffPeriod = [dbo].[fn_C00_USCancellationCoolingOffPeriod] (@State, @CancellationType, @IsRenewal, @DaysIntoPolicyYear)

			SELECT @ToDate= 
				CASE 
					WHEN @InceptionDate > ISNULL(@FromDate,dbo.fn_GetDate_Local()) THEN @InceptionDate 
					WHEN DATEDIFF(DAY,@InceptionDate,ISNULL(@FromDate,dbo.fn_GetDate_Local()) ) <= @CoolingOffPeriod THEN @InceptionDate
					ELSE @FromDate 
				END 
		END
		ELSE
		BEGIN
			
			/*Original logic*/
			SELECT @ToDate= CASE 
				WHEN @InceptionDate > ISNULL(@FromDate,dbo.fn_GetDate_Local()) THEN @InceptionDate 
				WHEN DATEDIFF(DAY,@InceptionDate,ISNULL(@FromDate,dbo.fn_GetDate_Local()) ) <= b.CoolingOffPeriod THEN @InceptionDate
				ELSE @FromDate END 
			FROM BillingConfiguration b WITH (NOLOCK) 
			WHERE b.ClientID = @ClientID

		END
			
			SELECT	@Status = 72326 /*Live*/

	END
	ELSE 
	IF @CalculateTypeID = 7 -- reinstatement
	BEGIN

		DECLARE @AdjustmentLocalTax DECIMAL(18,4)
		DECLARE @AdjustmentNationalTax DECIMAL(18,4)

		-- 2020-02-16 CPS for JIRA AAG-110 | Pick up dates from the latest cancellation row
		SELECT TOP (1)	 @FromDate	 = wp.ValidFrom
						--,@ToDate	 = --wp.ValidTo /*GPR 2020-04-27 replaced with a below select of the EndDate for this Policy*/
						,@OldwpID = wp.WrittenPremiumID
						,@AdjustmentLocalTax = wp.AdjustmentValueLocalTax /*GPR 2020-04-27*/
						,@AdjustmentNationalTax = wp.AdjustmentValueNationalTax /*GPR 2020-04-27*/
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4 /*Cancellation*/
		ORDER BY wp.WrittenPremiumID DESC

		/*GPR 2020-04-27 pick up the RenewalDate as the ToDate for AAG-695*/
		SELECT @ToDate = dbo.fnGetSimpleDvAsDate(175307, @MatterID)

	END

	-- Update To date in existing row
	IF @CloseLatestLiveRow = 1 AND @OldwpID IS NOT NULL
	BEGIN

		/*ALM 2021-03-10 FURKIN-384*/
		-- 2020-02-13 CPS for JIRA AAG-106 | Change from SET ValidTo = @ToDate
		IF @FromDate = @InceptionDate /**/
		BEGIN 
			UPDATE wp
			SET ValidTo = @FromDate
			FROM WrittenPremium wp
			WHERE wp.WrittenPremiumID = @OldwpID
		END
		ELSE 
		BEGIN
			UPDATE wp
			SET ValidTo = DATEADD(DAY,-1,@FromDate)
			FROM WrittenPremium wp
			WHERE wp.WrittenPremiumID = @OldwpID
		END

	END
	-- Update Status in existing row
	IF @LatestRowStatusLookupListItemID <> 0 AND @OldwpID IS NOT NULL
	BEGIN

		UPDATE wp
		SET Active = @LatestRowStatusLookupListItemID
		FROM WrittenPremium wp
		WHERE wp.WrittenPremiumID = @OldwpID

	END

	/*Save the premium calculation id to the matter*/
	EXEC dbo._C00_SimpleValueIntoField  176175, @PremiumCalculationID, @MatterID /*Latest Premium Calculation ID*/, @WhoCreated

	-- get matching policy history row id
	SELECT @PDHTableRowID = dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow (@CaseID, @FromDate)

	IF @PDHTableRowID IS NULL 
	BEGIN

		-- @FromDate is outside of the range of any historical policy entry, so default to the latest.
		SELECT TOP (1) @PDHTableRowID = tr.TableRowID 
		FROM dbo.TableRows tr WITH (NOLOCK) 
		WHERE tr.DetailFieldID = 170033 /*Historical Policy*/
		AND MatterID = @MatterID
		ORDER BY tr.TableRowID DESC
		
		-- or give up 
		SELECT @PDHTableRowID = ISNULL(@PDHTableRowID,0)

	END

	SELECT TOP (1) @IPTRate = CAST(pcdv.RuleTransformValue AS DECIMAL(18,4))
	FROM dbo.PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
	WHERE pcdv.RuleCheckpoint IN ('IPT', 'GST', 'TaxLevel1') --='IPT' /*GPR 2020-03-04 updated to include GST*/ /*GPR 2020-08-05 added State Tax*/ /*GPR 2020-09-19 altered State to TaxLevel1*/
	AND  pcdv.PremiumCalculationDetailID = @PremiumCalculationID
	AND  ISNUMERIC(pcdv.RuleTransformValue) = 1
	ORDER BY pcdv.RuleSequence

	-- TPet default and for AAG / Aus clients the transform is the scaler e.g. 1.015 for 15%
	IF @IPTRate > 1
	BEGIN
		SELECT @IptRate = @IptRate - 1
	END
	IF @IPT = 0 
	BEGIN		
		SELECT @IPTRate = 0
	END
	SELECT @IptRate = @IptRate * 100 /*GPR 2020-03-25*/

	/*GPR 2020-03-04 | LocalTaxRate added for AAG-516*/
	SELECT TOP (1) @LocalTaxRate = CAST(pcdv.RuleTransformValue AS DECIMAL(18,4))
	FROM dbo.PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
	WHERE pcdv.RuleCheckpoint IN ('Stamp Duty', 'TaxLevel2') /*GPR 2020-08-05 added Municipal Tax*/ /*GPR 2020-09-19 altered Municipal to TaxLevel2*/
	AND  pcdv.PremiumCalculationDetailID = @PremiumCalculationID
	AND  ISNUMERIC(pcdv.RuleTransformValue) = 1
	ORDER BY pcdv.RuleSequence

	-- TPet default and for AAG / Aus clients the transform is the scaler e.g. 1.015 for 15% so we need to -1
	-- Prudent and other US as 0.015
	IF @LocalTaxRate > 1
	BEGIN
		SELECT @LocalTaxRate = @LocalTaxRate - 1
	END
	IF @LocalTax = 0 
	BEGIN		
		SELECT @LocalTaxRate = 0
	END
	SELECT @LocalTaxRate = @LocalTaxRate * 100 /*GPR 2020-03-25*/

	IF @ClientID <> 607
	BEGIN
	SELECT TOP (1) @AdminFee = CAST(pcdv.RuleTransformValue AS DECIMAL(18,4))
	FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
	WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationID
	AND pcdv.RuleCheckpoint = 'Setup Fee'
	AND ISNUMERIC(pcdv.RuleTransformValue) = 1
	END
	ELSE 
	BEGIN 

	/*GPR/ALM 2021-02-25 FURKIN-270*/
	DECLARE @NewBusiness BIT, @PolicyYear INT
	SELECT @PolicyYear = COUNT(tr.TableRowID) 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.DetailFieldID = 170033 /*Historical Policy*/ 
	AND tr.MatterID = @MatterID

	IF @PolicyYear > 1
	BEGIN
		SELECT @NewBusiness = 0
	END
	ELSE
	BEGIN
		SELECT @NewBusiness = 1
	END

	IF @NewBusiness = 1 AND @CalculateTypeID <> 2
	BEGIN

		/*ALM 2021-04-05 FURKIN-342*/
		SELECT @StateCode = canada.StateCode
		FROM Customers c WITH (NOLOCK)
		JOIN UnitedStates canada WITH (NOLOCK) ON canada.StateName = c.County
		WHERE c.CustomerID = @CustomerID

		/*ALM 2021-02-22 FURKIN-252*/
		SELECT @BrandID = dbo.fnGetSimpleDvAsInt (170144, @CustomerID)

		SELECT @ProvinceLookupListItemID = CASE @StateCode 
						WHEN 'AB' THEN 61739
						WHEN 'BC' THEN 61740
						WHEN 'MB' THEN 61741
						WHEN 'NB' THEN 61742
						WHEN 'NL' THEN 61743
						WHEN 'NS' THEN 61744
						WHEN 'NT' THEN 61745
						WHEN 'NU' THEN 61746
						WHEN 'ON' THEN 61747
						WHEN 'PE' THEN 61748
						WHEN 'QC' THEN 61749
						WHEN 'SK' THEN 61750
						WHEN 'YT' THEN 61751
					END

		/*ALM 2021-03-10 FURKIN-386*/
		/*Work out pet number*/
		INSERT INTO @Policies (LeadID, PolicyNumber)
		SELECT l.LeadID, ROW_NUMBER() OVER (PARTITION BY (SELECT 1) ORDER BY l.LeadID)
		FROM Lead l WITH (NOLOCK)
		WHERE l.CustomerID = @CustomerID
		AND l.leadTypeID = 1492

		SELECT @PetIndex = p.PolicyNumber
		FROM @Policies p
		WHERE p.LeadID = @LeadID

		SELECT @AdminFee = Total /*Enrollment Fee + Tax*/
		FROM dbo.fn_C600_GetEnrollmentFee(@BrandID, @CustomerID, @ProvinceLookupListItemID, @PetIndex, @StartDate)

	END
	END

	SELECT @DaysUsed	 = DATEDIFF(DD,@StartDate, @ToDate)
	SELECT @DailyPremium = ISNULL(@DiscountGrossNet,@AnnualPremium) / ( DATEDIFF(DAY,@StartDate,@EndDate)+1 )

	SELECT @DailyPremium = ISNULL(@DiscountGrossNet,@AnnualPremium) / ( DATEDIFF(DAY,@StartDate,@EndDate)+1 )
	
	/*GPR 2020-04-23*/
	--DECLARE @DailyPremiumNet DECIMAL(18,2), @UsedPremiumNet DECIMAL(18,2) /*GPR 2020-04-23*/
	--SELECT @DailyPremiumNet = ISNULL(@GrossNet,@Net)  / ( DATEDIFF(DAY,@StartDate,@EndDate)+1 )

	SELECT @UsedPremium  = @DailyPremium * @DaysUsed

	--/*GPR 2020-04-23*/
	--SELECT @UsedPremiumNet = @DailyPremiumNet * @DaysUsed

	---------------------
	--Table insert was here
	---------------------
	
		IF @CalculateTypeID = 5
		BEGIN
			SELECT TOP (1) @PurchasedProductID = pp.PurchasedProductID
			FROM PurchasedProduct pp WITH (NOLOCK)
			WHERE pp.ObjectID = @MatterID
			AND ISNULL(@FromDate, dbo.fn_GetDate_Local()) BETWEEN pp.ValidFrom AND pp.ValidTo -- GPR 2020-04-22 added ISNULL to dbo.fn_GetDate_Local()
			ORDER BY pp.PurchasedProductID DESC	

			DECLARE @TransactionFee DECIMAL(18,2), @AdjustmentNet DECIMAL(18,2)

			SELECT @OriginalAdjustment = SUM(PaymentGross), @TransactionFee = SUM(TransactionFee), @AdjustmentNet =SUM(PaymentNet) /*GPR 2020-12-14 remove TransactionFee from AdjustmentValueGross for SDPRU-192*/
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
			WHERE ppps.PurchasedProductID = @PurchasedProductID
			AND ppps.CustomerID = @CustomerID
			AND ppps.PaymentStatusID IN (2, 6) /* Processed, Paid */

			SELECT @AdjustmentAmount = ((ISNULL(@OriginalAdjustment, 0.00)-@UsedPremium) - @TransactionFee) * -1.00

			EXEC [dbo].[_C00_LogIt] 'Info', 'Cancellation', 'OriginalAdjusment', @OriginalAdjustment, 58550
			EXEC [dbo].[_C00_LogIt] 'Info', 'Cancellation', 'UsedPremium', @UsedPremium, 58550
			EXEC [dbo].[_C00_LogIt] 'Info', 'Cancellation', 'TransactionFee', @TransactionFee, 58550
			EXEC [dbo].[_C00_LogIt] 'Info', 'Cancellation', 'AdjustmentAmount', @AdjustmentAmount, 58550

			EXEC dbo._C00_SimpleValueIntoField 177404, @AdjustmentAmount, @MatterID, @WhoCreated /*Adjustment Amount for remaining policy term*/
		END

		IF @CalculateTypeID IN (2,8) -- renewal, new policy
		BEGIN

			SELECT @AdjustmentAmount = ISNULL(@DiscountGrossNet,@AnnualPremium)
			SELECT @AdjustmentIPT	 = ISNULL(@AdjustmentAmount-(@AdjustmentAmount/NULLIF(@IptRate,0)),0.00)

			SELECT @DF175396 = CONVERT(VARCHAR(10),@FromDate,120),
					@DF175397 = CONVERT(VARCHAR,@AdjustmentAmount),
					@DF175400 = CONVERT(VARCHAR(19),dbo.fn_GetDate_Local(),121),
					@DF180295 = CONVERT(VARCHAR,@AdjustmentIPT)
		END
		
		IF @CalculateTypeID NOT IN (2,8,5) -- renewal, new policy /*GPR 2020-04-24 made this a NOT IN in place of ELSE to add in Cancellation*/
		BEGIN -- 2019-10-21 CPS for JIRA LPC-37 | Populate adjustment fields

			EXEC _C00_1273_Policy_CalculateAdjustments @MatterID, @StartDate, @EndDate, @FromDate, @CalculateTypeID, 0, @LeadEventID
			
			SELECT	 @AdjustmentAmount  = fn.Period2Gross
					,@AdjustmentIPT		= fn.Period2Tax
			FROM dbo.fn_C600_Billing_GetProRataResults(@StartDate,@EndDate,@FromDate,1,@PremiumLessDiscount,0.00,@IPT) fn

			EXEC _C00_LogIt 'Info', 'AAG-Gavin', 'fn_C600_Billing_GetProRataResults AdjustmentAmount', @AdjustmentAmount, 58550

			SELECT	 @Commission	  = fn.Period2Gross
			FROM dbo.fn_C600_Billing_GetProRataResults(@StartDate,@EndDate,@FromDate,1,@Commission,0.00,0.00) fn
		
		END

	IF @UpdateCurrentAndPreviousPremium = 1 AND @DisplayOnly=0 -- quote, MTA & reinstatement; update current & previous premium fields
	BEGIN

		EXEC dbo._C00_CreateDetailFields '175337,175338,175339,175340,175341,175342,175711,175712', @LeadID
		
		-- copy current to previous
		EXEC dbo._C600_CopyFieldToField 175337,175340,@MatterID,@WhoCreated /*Current/New Premium - Annual => Previous Premium - Annual*/
		EXEC dbo._C600_CopyFieldToField 175338,175341,@MatterID,@WhoCreated /*Current/New Premium - First Month => Previous Premium - First Month*/
		EXEC dbo._C600_CopyFieldToField 175339,175342,@MatterID,@WhoCreated /*Current/New Premium - Other Month => Previous Premium - Other Month*/
		EXEC dbo._C600_CopyFieldToField 175711,175712,@MatterID,@WhoCreated /*Current Premium/New - PremiumCalculationID => Previous Premium - PremiumCalculationID*/
		
		-- update current
		EXEC dbo._C00_SimpleValueIntoField 175337, @DiscountGrossNet	, @MatterID,@WhoCreated /*Current/New Premium - Annual*/
		EXEC dbo._C00_SimpleValueIntoField 175338, @FirstMonthly		, @MatterID,@WhoCreated /*Current/New Premium - First Month*/
		EXEC dbo._C00_SimpleValueIntoField 175339, @RecurringMonthly	, @MatterID,@WhoCreated /*Current/New Premium - Other Month*/
		EXEC dbo._C00_SimpleValueIntoField 175711, @PremiumCalculationID, @MatterID,@WhoCreated /*Current Premium/New - PremiumCalculationID*/
		EXEC dbo._C00_SimpleValueIntoField 177898, @DiscountGrossNet	, @MatterID,@WhoCreated /*Current Premium - After Discount*/

	END
	ELSE
	IF @CalculateTypeID IN (2) -- renewal;  update renewal fields (don't update any for cancellation (5) )
	BEGIN

		EXEC dbo._C00_CreateDetailFields '175443,175444,175445', @LeadID

		EXEC dbo._C00_SimpleValueIntoField 175443, @DiscountGrossNet, @MatterID ,@WhoCreated /*Renewal Premium - Annual*/
		EXEC dbo._C00_SimpleValueIntoField 175444, @FirstMonthly	, @MatterID ,@WhoCreated /*Renewal Premium - First Month*/
		EXEC dbo._C00_SimpleValueIntoField 175445, @RecurringMonthly, @MatterID ,@WhoCreated /*Renewal Premium - Other Month*/

	END

	/*GPR 2020-04-08 Pre-MTA*/
	IF @CalculateTypeID IN (9) /*9 = Pre-MTA*/
	BEGIN

		SELECT TOP 1 @PurchasedProductID=pp.ValueInt
		FROM Matter m WITH (NOLOCK)
		INNER JOIN MatterDetailValues pp WITH (NOLOCK) ON m.MatterID=pp.MatterID AND pp.DetailFieldID=177074 /*Purchased Policy ID*/
		WHERE m.MatterID=@MatterID
	
	END

	-- 2019-10-21 CPS for JIRA LPC-37 | Brought over from _C00_1273_Policy_CalculatePremiumForPeriod
	IF @CalculateTypeID IN (2,3,5,6,8)
	BEGIN

		SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID) /*Affinity Details*/

		-- 2020-02-12 CPS for JIRA AAG-106 | This could also be a decimal value.  Changed from INT to DECIMAL(18,2)
		SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsMoney(313814,@AffinityID) /*Underwriter Commission Percentage Value*/

		SELECT TOP (1)	 @DaysInYear = DATEDIFF(DAY,pp.ValidFrom,pp.ValidTo) + 1
						,@PurchasedProductID = pp.PurchasedProductID
						,@PolEnd = pp.ValidTo
		FROM PurchasedProduct pp WITH (NOLOCK) 
		WHERE pp.ObjectID = @MatterID
		AND ISNULL(@DF175347,dbo.fn_GetDate_Local()) BETWEEN pp.ValidFrom AND pp.ValidTo /*GPR 2020-04-08 added ISNULL to dbo.fn_GetDate_Local() for JIRA AAG-634*/
		ORDER BY pp.PurchasedProductID DESC

		EXEC _C00_LogIt 'Info', 'AAG-Gavin', 'Cover Date', @DF175347, 58550
		EXEC _C00_LogIt 'Info', 'AAG-Gavin', 'PurchasedProductID', @PurchasedProductID, 58550

		IF @@ROWCOUNT = 0 or @PolEnd is NULL
		BEGIN

			SELECT @DaysInYear = DATEDIFF(DAY,mdv.ValueDate,DATEADD(DAY,-1,DATEADD(YEAR,1,mdv.ValueDate)) /*Policy Start Date*/) + 1
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			WHERE mdv.MatterID = @MatterID
			AND mdv.DetailFieldID = 170036 /*Policy Start Date*/

		END


		/*GPR 2020-04-21 added assignment of @CommissionPercentageValue for AAG-686*/
		SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID) /*Affinity Details*/
		SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsMoney(313814,@AffinityID) /*Underwriter Commission Percentage Value*/  
		SELECT @AnnualCommissionValue /*Broker Commission Value*/ = (@DiscountGrossNet / 100) * (100 - @CommissionPercentageValue /*UW Commission Value*/)

		SELECT @DaysRemaining = DATEDIFF(DAY,@Df175396, @DF175347)+1

		SELECT @TranCommissionValue	= (@AnnualCommissionValue)* ( CAST(@DaysRemaining as MONEY) / CAST(@DaysInYear as MONEY) )

		SELECT @RawPremium = ISNULL(@DiscountGrossNet,0.00) - ( ISNULL(@IPT,0.00)) /*+ ISNULL(tdvAdmin.ValueMoney,0.00) )*/ /*2020-12-03 ACE this vlaue isnt being set...*/

	END	

	/*GPR 2020-04-21 added assignment of @CommissionPercentageValue for AAG-686*/
	SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID) /*Affinity Details*/
	SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsMoney(313814,@AffinityID) /*Underwriter Commission Percentage Value*/  
	SELECT @AdjustmentCommission = (@AdjustmentNet * (100 - @CommissionPercentageValue)) * ( CAST(DATEDIFF(DD,@StartDate, @ToDate) as MONEY) / CAST(@DaysInYear as MONEY) )

	IF @DaysInYear IS NULL
	BEGIN

		SELECT TOP (1)	 @DaysInYear = DATEDIFF(DAY,pp.ValidFrom,pp.ValidTo) + 1
		FROM PurchasedProduct pp WITH (NOLOCK) 
		WHERE pp.ObjectID = @MatterID
		AND ISNULL(@DF175347,dbo.fn_GetDate_Local()) BETWEEN pp.ValidFrom AND pp.ValidTo /*GPR 2020-04-08 added ISNULL to dbo.fn_GetDate_Local() for JIRA AAG-634*/
		ORDER BY pp.PurchasedProductID DESC

	END

	INSERT INTO @TableValuesToInsert (DetailFieldID, DetailValue)
	VALUES  (175446,CONVERT(VARCHAR(16),dbo.fn_GetDate_Local(),120)),  -- date time of rating
			(175346,CONVERT(VARCHAR(10),@FromDate,120)),
			(175347,''),
			(175348,@EventName),
			(175722,CAST(@Status				AS VARCHAR(30))),	/*Status*/
			(175349,CAST(@AnnualPremium			AS VARCHAR(30))),	/*Annual*/
			(175350,CAST(@FirstMonthly			AS VARCHAR(30))),	/*First Month*/
			(175351,CAST(@RecurringMonthly		AS VARCHAR(30))),	/*Other Month*/
			(175375,CAST(@IPT					AS VARCHAR(30))),	/*IPT*/
			(180292,CAST(@IPTRate				AS VARCHAR(30))),
			(175709,CAST(@PremiumCalculationID	AS VARCHAR(30))),	/*PremiumCalculationID*/
			(175398,CAST(@AdjustmentType		AS VARCHAR(30))),	/*Adjustment Type*/
			(175376,CAST(@EventTypeID			AS VARCHAR(30))),	/*Event Type ID*/
			(175377,CAST(@PDHTableRowID			AS VARCHAR(30))),	/*Historic Pol TRID*/
			(177899,CAST(@DiscountGrossNet		AS VARCHAR(30))),	/*Premium less Discount*/
			(175373,CAST(@Discount				AS VARCHAR(30))),	/*Discount*/
			(177903,CAST(CASE WHEN @AdjustmentType = 74527 /*New*/ THEN @AdminFee ELSE 0.00 END AS VARCHAR(20))), /*Adjustment Admin Fee*/
			(179799,CAST(@Commission			AS VARCHAR(20))),
			(175396, @Df175396),  -- adjustment date
			(175397, @DF175397),   -- adjustment amount
			(175400, @DF175400), -- adjustment application date
			(175347, @DF175347), --313869 Cover To
			(180170, CAST(@AnnualCommissionValue AS VARCHAR(100))),
			(180171, CAST(@TranCommissionValue	 AS VARCHAR(100))),
			(180307, CAST(@RawPremium			 AS VARCHAR(100))),
			(180291, CAST(@QuotePetProductID	 AS VARCHAR(20))),
			(313819, CAST(@AdminFee				 AS VARCHAR(30))),
			(180295, CAST(@DF180295				 AS VARCHAR(100))),
			(313869, CAST(@LocalTax				 AS VARCHAR(100))),	/*Annual Local Tax		- AAG-516*/
			(313870, CAST(@LocalTaxRate			 AS VARCHAR(100))),	/*Annual Local Tax Rate	- AAG-516*/
			(-1, CAST(@CalculateTypeID				AS VARCHAR(100))),
			(-2, CAST(@DaysInYear					AS VARCHAR(100))),
			(-3, CAST(@AdjustmentCommission			AS VARCHAR(100))),
			(-4, CAST(@CommissionPercentageValue	AS VARCHAR(100))),
			(-5, CAST(@AffinityID					AS VARCHAR(100))),
			(-6, CAST(DATEDIFF(DD,@StartDate, @ToDate) as VARCHAR(100))),
			(-7, CAST(@StartDate as VARCHAR(100))),
			(-8, CAST(@ToDate as VARCHAR(100)))

	IF @DisplayOnly = 0
	BEGIN

		/*
			1 = Monthly, What is monthly?!?
			2 = renewal, 
			3 = MTA, 
			4 = Quote (during customer matching), 
			5 = cancellation, 
			6 = annual premium, 
			7 = reinstatement, 
			8 = new policy, 
			9 = pre MTA
		*/

		-- 2020-02-12 CPS for JIRA AAG-106 | Let's tidy this up.  Ideal world, make the values consistent across procedures.
		SELECT @AdjustmentTypeID = CASE @CalculateTypeID 
				WHEN 1 THEN 1
				WHEN 2 THEN 3
				WHEN 3 THEN 2
				WHEN 4 THEN 1
				WHEN 5 THEN 4 -- Cancellation
				WHEN 6 THEN 1
				WHEN 7 THEN 6
				WHEN 8 THEN 1
				WHEN 9 THEN 9
				END
		
		EXEC _C00_LogIt 'AAG-GAVIN',@ProcName,'Type',@CalculateTypeID,58550
				
		-- 2020-02-12 CPS for JIRA AAG-106 | If there is no end date for this row, use the natural end date of the policy.
		IF @ToDate is NULL
		BEGIN
			SELECT @ToDate = DATEADD(YEAR,1,DATEADD(DAY,-1,@StartDate))
		END

		/*GPR 2020-04-21 for AAG-686*/
		SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID) /*Affinity Details*/
		SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsMoney(313814,@AffinityID) /*Underwriter Commission Percentage Value*/  
		SELECT @AnnualCommissionValue /*Broker Commission Value*/ = (@DiscountGrossNet / 100) * (100 - @CommissionPercentageValue /*UW Commission Value*/)
		EXEC _C00_LogIt 'Info',@ProcName,'Broker',@AnnualCommissionValue,58550



		EXEC _C00_LogIt 'AAG-GAVIN',@ProcName,'Type',@AdjustmentTypeID,58550
		EXEC _C00_LogIt 'AAG-GAVIN',@ProcName,'Local',@AdjustmentLocalTax,58550
		EXEC _C00_LogIt 'AAG-GAVIN',@ProcName,'National',@AdjustmentNationalTax,58550

		/*GPR 2021-03-10 for FURKIN-385*/
		IF @FromDate = @StartDate AND @AdjustmentTypeID = 4/*Cancellation to Inception*/
		BEGIN
			SELECT @AdjustmentAdminFee = @AdminFee * - 1
		END
		
		INSERT INTO WrittenPremium (ClientID, CustomerID, LeadID, MatterID, PurchasedProductID, RatingDateTime, 
				PremiumCalculationID, ValidFrom, ValidTo, AdjustmentTypeID, Active,
				AdjustmentRequestDate, 
				AnnualPriceForRiskGross,
				AnnualPriceForRiskNET,
				AnnualPriceForRiskNationalTax, /*@IPT*/
				AnnualAdminFee,
				AnnualDiscountCommissionSacrifice,
				AnnualDiscountPremiumAffecting,
				AnnualNationalTaxRate, /*@IPTRate*/
				AdjustmentValueGross,
				AdjustmentValueNET,
				AdjustmentValueNationalTax,
				AdjustmentAdminFee, 
				AdjustmentDiscountCommissionSacrifice,
				AdjustmentDiscountCommissionPremium,
				AdjustmentBrokerCommission, 
				AdjustmentUnderwriterCommission,
				AdjustmentSourcecommission,
				CancellationReason,
				AdditionalDetailXML, 
				Comments,
				WhoCreated, WhenCreated, WhoChanged, WhenChanged, SourceID, QuotePetProductID,
				AnnualLocalTaxRate, AnnualPriceForRiskLocalTax, AdjustmentValueLocalTax,
				ProvinceState
				) /*GPR Added for AAG-516*/

		SELECT   @ClientID,@CustomerID,@LeadID,@MatterID,@PurchasedProductID,dbo.fn_GetDate_Local() /*RatingDateTime*/
				,@PremiumCalculationID, @FromDate /*ValidFrom*/, @ToDate /*ValidTo*/, @AdjustmentTypeID, 1 /*Active*/ 
				,@WhenCreated /*AdjustmentRequestDate*/ 
				,ISNULL(@AnnualPremium,0.00) 
				,ISNULL(@Net,0.00)			 
				,ISNULL(@IPT,0.00)			 
				,ISNULL(@AdminFee,0.00)
				,ISNULL(@Discount,0.00)					 /*AnnualDiscountCommissionSacrifice*/		-- 2020-02-12 CPS for JIRA AAG-106 | Definitely wrong, but let's get the procedure working first
				,ISNULL(@Discount,0.00)					 /*AnnualDiscountPremiumAffecting*/ 
				,@IPTRate					 
				,CASE
					WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN ISNULL(@AdjustmentAmount,0.00) /* 2020-03-24 AAD AAG-457 - Write correct adjustment amount for cancellations */
					WHEN @AdjustmentTypeID = 6 /*Reinstatement*/ THEN adj.AdjustmentValueGross * -1
					ELSE adj.AdjustmentValueGross
				 END
				,CASE
					WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN ISNULL(@AdjustmentNet,0.00) /*GPR 2020-12-15*/ /* 2020-03-24 AAD AAG-457 - Write 0.00 for cancellations */
					WHEN @AdjustmentTypeID = 6 /*Reinstatement*/ THEN adj.AdjustmentValueNet * -1
					ELSE adj.AdjustmentValueNet
				 END
				,CASE
					WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN 0.00 /* 2020-03-24 AAD AAG-457 - Write 0.00 for cancellations */
					WHEN @AdjustmentTypeID = 6 /*Reinstatement*/ THEN @AdjustmentNationalTax * -1
					ELSE adj.AdjustmentValueNationalTax
				 END
				,CASE /*GPR 2021-03-01 for FURKIN-324*/
					WHEN @AdjustmentTypeID = 1 /*New*/ THEN ISNULL(@AdminFee,0.00)
					ELSE ISNULL(@AdjustmentAdminFee,0.00)	/*AdjustmentAdminFee (0.00)*/
				 END
				,CASE
					WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN 0.00 /* 2020-03-24 AAD AAG-457 - Write 0.00 for cancellations */
					WHEN @AdjustmentTypeID = 6 /*Reinstatement*/ THEN adj.AdjustmentDiscountCommissionSacrifice * -1
					ELSE adj.AdjustmentDiscountCommissionSacrifice
				 END
				,CASE
					WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN 0.00 /* 2020-03-24 AAD AAG-457 - Write 0.00 for cancellations */
					WHEN @AdjustmentTypeID = 6 /*Reinstatement*/ THEN adj.AdjustmentDiscountCommissionPremium * -1
					ELSE adj.AdjustmentDiscountCommissionPremium
				 END
				, ISNULL(CASE WHEN @ClientID = 607 THEN CONVERT(NUMERIC(18,2),(adj.AdjustmentValueNet * ((100.00 - @CommissionPercentageValue)/100.00)))
				ELSE
					CASE 
						WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN 0.00 /*GPR 2020-04-24*/ ELSE ISNULL(@AnnualCommissionValue,0.00)	
					END 
				END, 0.00) /*AdjustmentBrokerCommission*/ 
				, CASE WHEN @ClientID = 607 THEN CONVERT(NUMERIC(18,2),adj.AdjustmentValueNet) - CONVERT(NUMERIC(18,2),(adj.AdjustmentValueNet * ((100.00 - @CommissionPercentageValue)/100.00)))
				ELSE
					CASE 
						WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN 0.00 /*GPR 2020-04-24*/ 
						ELSE CASE ISNULL(@AnnualCommissionValue,0.00) WHEN 0.00 THEN 0.00 ELSE ISNULL(@Net,0.00)-ISNULL(@AnnualCommissionValue,0.00) END 
					END 
				END /*AdjustmentUnderwriterCommission*/ 
				,ISNULL(@TranCommissionValue,0.00)					  /*AdjustmentSourceCommission*/-- 2020-02-12 CPS for JIRA AAG-106 | Unclear.  Aggregator Commission?
				,''							 /*CancellationReason*/						-- 2020-02-12 CPS for JIRA AAG-106 | Added by the cancellation process
				,NULL						 /*AdditionalDetailXML*/ 
				,'Added by ' + @ProcName	 /*Comments*/ 
				,@WhoCreated, dbo.fn_GetDate_Local() /*WhenCreated*/, @WhoCreated	/*WhoChanged*/, dbo.fn_GetDate_Local() /*WhenChanged*/, @LeadEventID /*SourceID*/, @QuotePetProductID
				,ISNULL(@LocalTaxRate,0.00)	/*AnnualLocalTaxRate - AAG-516*/
				,ISNULL(@LocalTax,0.00)		/*AnnualPriceForRiskLocalTax - AAG-516*/
				,CASE /*GPR 2020-04-23*/
					WHEN @AdjustmentTypeID = 4 /* Cancellation */ THEN 0.00 /* 2020-03-24 AAD AAG-457 - Write 0.00 for cancellations */
					WHEN @AdjustmentTypeID = 6 /*Reinstatement*/ THEN @AdjustmentLocalTax
					ELSE adj.AdjustmentValueLocalTax
				 END		/*AdjustmentValueLocalTax - AAG-516*/
				 ,@County
		FROM Matter m WITH (NOLOCK) 
		LEFT JOIN WrittenPremium wp WITH (NOLOCK) ON wp.WrittenPremiumID = @OldwpID
		OUTER APPLY dbo.fn_C600_GetWrittenPremiumAdjustmentValues(	 m.MatterID				-- MatterID
																	,@FromDate				-- CoverPeriodStartDate
																	,@ToDate				-- CoverPeriodEndDate
																	,@AdjustmentTypeID		-- AdjustmentTypeID
																	,@AnnualPremium			-- NewAnnualPremiumGross
																	,@Net					-- NewAnnualPremiumNet
																	,@Discount				-- NewAnnualDiscountCommissionSacrifice
																	,@Discount				-- NewAnnualDiscountPremiumAffecting
																	,@IPT					-- NewAnnualPremiumNationalTax
																	,@LocalTax				-- NewAnnualPremiumLocalTax 	/*2020-03-24 GPR for AAG-516*/
																 ) adj
		WHERE m.MatterID = @MatterID

		SELECT @WrittenPremiumID = SCOPE_IDENTITY()

		EXEC _C00_LogIt 'Info',@ProcName,'AdjustmentAmount',@AdjustmentAmount,58550
		EXEC _C00_LogIt 'NationalTax',@ProcName,'NationalTax',@IPT,58550

		/*GPR 2020-03-09 for AAG-136 / AAG-292*/
		IF @CalculateTypeID IN (2 /*Renewal*/, 8 /*New Policy*/) /*GPR 2020-03-11 updated to use @CalculateTypeID in place of @AdjustmentTypeID*/
		BEGIN

			SELECT @UnderwriterDiscountAccountability = dbo.fn_GetDiscountSplitForWrittenPremium(@WrittenPremiumID, 'Underwriter')
			SELECT @BrokerDiscountAccountability = (dbo.fn_GetDiscountSplitForWrittenPremium(@WrittenPremiumID, 'Agency') + dbo.fn_GetDiscountSplitForWrittenPremium(@WrittenPremiumID, 'Distributor'))

			/*GPR 2020-03-11 - Testing START*/
				--DECLARE @UWCommission DECIMAL(18,2)
				--DECLARE @BrokerCommission DECIMAL(18,2)
			
				--SELECT @BrokerCommission = AdjustmentBrokerCommission, @UWCommission = AdjustmentUnderwriterCommission
				--FROM PurchasedProductPaymentScheduleDetail
				--WHERE PurchasedProductPaymentScheduleDetailID = @PurchasedProductPaymentScheduleDetailID
			
				--EXEC _C00_LogIt 'Info', 'AAG-338', 'Original UW Commission', @UWCommission, 58550
				--EXEC _C00_LogIt 'Info', 'AAG-338', 'Original Broker Commission', @BrokerCommission, 58550			
				--EXEC _C00_LogIt 'Info', 'AAG-338', 'UW Discount Split', @UnderwriterDiscountAccountability, 58550
				--EXEC _C00_LogIt 'Info', 'AAG-338', 'Broker Discount Split', @BrokerDiscountAccountability, 58550
			/*GPR 2020-03-11 - Testing END*/


			UPDATE WrittenPremium
			SET AdjustmentBrokerCommission = AdjustmentBrokerCommission - ISNULL(@BrokerDiscountAccountability, 0.00), AdjustmentUnderwriterCommission = AdjustmentUnderwriterCommission - ISNULL(@UnderwriterDiscountAccountability, 0.00)
			WHERE WrittenPremiumID = @WrittenPremiumID
			

			/*GPR 2020-03-11 - Testing START*/
				--SELECT @BrokerCommission = AdjustmentBrokerCommission, @UWCommission = AdjustmentUnderwriterCommission
				--FROM PurchasedProductPaymentScheduleDetail
				--WHERE PurchasedProductPaymentScheduleDetailID = @PurchasedProductPaymentScheduleDetailID

				--EXEC _C00_LogIt 'Info', 'AAG-338', 'Updated UW Commission', @UWCommission, 58550
				--EXEC _C00_LogIt 'Info', 'AAG-338', 'Updated Broker Commission', @BrokerCommission, 58550	
			/*GPR 2020-03-11 - Testing END*/

		END


		SELECT @EventComments += 'Cancellation ScheduleDetailID ' + ISNULL(CONVERT(VARCHAR,@OldwpID),'NULL') + CHAR(13)+CHAR(10)

	END

	SELECT @OutputData = (
		SELECT *
		FROM @TableValuesToInsert i
		FOR XML PATH ('Data')
	)

	IF @MatterID > 0
	BEGIN

		SELECT @ContextID = @MatterID

	END

	EXEC _C00_LogItXML @ClientID, @ContextID, '_C00_1273_Policy_CalculatePremiumForPeriod_Evaluated', @OutputData, NULL

	SELECT @EventComments += '  WrittenPremium ' + ISNULL(CONVERT(VARCHAR,@WrittenPremiumID),'NULL') + CHAR(13)+CHAR(10)
						  +  '  QuotePetProductID ' + ISNULL(CONVERT(VARCHAR,@QuotePetProductID),'NULL') + CHAR(13)+CHAR(10)
	
	EXEC dbo._C00_SetLeadEventComments  @LeadEventID, @EventComments, 1
	
	RETURN @WrittenPremiumID
		
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod_Evaluated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod_Evaluated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremiumForPeriod_Evaluated] TO [sp_executeall]
GO
