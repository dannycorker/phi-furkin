SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_GetByThirdPartySystemID]
(

	@ThirdPartySystemID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyFieldGroupID],
					[ThirdPartySystemID],
					[GroupName],
					[GroupDescription],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldGroup] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemID] = @ThirdPartySystemID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_GetByThirdPartySystemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_GetByThirdPartySystemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_GetByThirdPartySystemID] TO [sp_executeall]
GO
