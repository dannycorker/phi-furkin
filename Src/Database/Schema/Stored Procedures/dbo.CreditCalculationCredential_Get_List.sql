SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CreditCalculationCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_Get_List]

AS


				
				SELECT
					[CreditCalculationCredentialID],
					[ClientID],
					[ClientPersonnelID],
					[SecretKey]
				FROM
					[dbo].[CreditCalculationCredential] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Get_List] TO [sp_executeall]
GO
