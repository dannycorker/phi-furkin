SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CustomerToTest]
(
@LeadEventID int
)


AS
BEGIN
	SET NOCOUNT ON;

	Declare @LeadID int

	Select @LeadID = LeadID
	From LeadEvent
	Where LeadEventID = @LeadEventID

	Update Customers
	Set Test = 1
	From Customers
	Inner Join Lead l on l.CustomerID = Customers.CustomerID and l.LeadID = @LeadID

	Update Lead
	Set Assigned = 0, AssignedTo = NULL, AssignedBy = NULL, AssignedDate = NULL
	Where LeadID = @LeadID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomerToTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CustomerToTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomerToTest] TO [sp_executeall]
GO
