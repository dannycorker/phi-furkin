SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 18 September 2007

-- Created By:  ()
-- Purpose: Updates a record in the DocumentTypeFolderLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType__MoveToFolder]
(

	@DocumentTypeID int,
	@OldFolderID int,
	@NewFolderID int
)
AS


		UPDATE DocumentTypeFolderLink
		SET FolderID = @NewFolderID
		WHERE
			[DocumentTypeID] = @DocumentTypeID
			AND [FolderID] = @OldFolderID
			
	
		-- if we are trashing a document then disable it.
		IF (@NewFolderID = -1)
		BEGIN
			exec [DocumentType__DisableDocumentType] @DocumentTypeID
		END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__MoveToFolder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__MoveToFolder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__MoveToFolder] TO [sp_executeall]
GO
