SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptInputType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptInputType_Delete]
(

	@ScriptInputTypeID int   
)
AS


				DELETE FROM [dbo].[ScriptInputType] WITH (ROWLOCK) 
				WHERE
					[ScriptInputTypeID] = @ScriptInputTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptInputType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Delete] TO [sp_executeall]
GO
