SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SMSQuestion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_Update]
(

	@SMSQuestionID int   ,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@Question varchar (960)  ,

	@QuestionOrder int   ,

	@DocumentTypeID int   ,

	@Enabled bit   ,

	@Timeout int   ,

	@Repeat int   ,

	@RepeatPrefix varchar (2000)  ,

	@DetailFieldToSave int   ,

	@EvaluateResponse bit   ,

	@VoiceAction int   ,

	@RedirectTo varchar (50)  ,

	@GatherNumDigits int   ,

	@RecordingMaxLength int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SMSQuestion]
				SET
					[ClientID] = @ClientID
					,[SMSQuestionnaireID] = @SMSQuestionnaireID
					,[Question] = @Question
					,[QuestionOrder] = @QuestionOrder
					,[DocumentTypeID] = @DocumentTypeID
					,[Enabled] = @Enabled
					,[Timeout] = @Timeout
					,[Repeat] = @Repeat
					,[RepeatPrefix] = @RepeatPrefix
					,[DetailFieldToSave] = @DetailFieldToSave
					,[EvaluateResponse] = @EvaluateResponse
					,[VoiceAction] = @VoiceAction
					,[RedirectTo] = @RedirectTo
					,[GatherNumDigits] = @GatherNumDigits
					,[RecordingMaxLength] = @RecordingMaxLength
				WHERE
[SMSQuestionID] = @SMSQuestionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Update] TO [sp_executeall]
GO
