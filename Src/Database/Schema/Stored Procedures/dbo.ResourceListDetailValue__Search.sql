SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 02-02-2016
-- Description:	Resource List Detail Value Search
-- =============================================
CREATE PROCEDURE [dbo].[ResourceListDetailValue__Search]

	@ClientID INT,
	@ResourceListDetailFieldID INT,
	@SearchTarget VARCHAR(2000)

AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @ResourceListDetailFieldPageID INT
	
	SELECT @ResourceListDetailFieldPageID = df.ResourceListDetailFieldPageID 
	FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID=@ResourceListDetailFieldID

	SELECT * FROM ResourceListDetailValues rldv WITH (NOLOCK) 
	WHERE rldv.DetailFieldID in (SELECT DetailFieldID FROM DetailFields WITH (NOLOCK) WHERE DetailFieldPageID=@ResourceListDetailFieldPageID) 
	      AND rldv.DetailValue like '%' + @SearchTarget + '%'
	      AND rldv.ClientID = @ClientID
    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValue__Search] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValue__Search] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValue__Search] TO [sp_executeall]
GO
