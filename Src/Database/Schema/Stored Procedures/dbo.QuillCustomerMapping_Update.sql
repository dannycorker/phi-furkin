SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuillCustomerMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCustomerMapping_Update]
(

	@QuillCustomerMappingID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@QuillClientCode varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuillCustomerMapping]
				SET
					[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[QuillClientCode] = @QuillClientCode
				WHERE
[QuillCustomerMappingID] = @QuillCustomerMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCustomerMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Update] TO [sp_executeall]
GO
