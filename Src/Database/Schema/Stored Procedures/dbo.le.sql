SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-25
-- Description:	List all LeadEvents for a record
-- =============================================
CREATE PROCEDURE [dbo].[le] 
	@LeadID int = NULL,
	@CaseID int = NULL,
	@EventTypeID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @CaseID IS NOT NULL
	BEGIN
	
		/* Show all values for this exact Case */
		SELECT 
		le.[LeadEventID]
		,le.[ClientID]
		,le.[LeadID]
		,le.[CaseID]
		,le.[WhenCreated]
		,le.[WhoCreated]
		,et.EventTypeName
		,le.[Comments]
		,le.[EventTypeID]
		,et.EventSubtypeID
		,le.[NoteTypeID]
		,le.[FollowupDateTime]
		,le.[WhenFollowedUp]
		,le.[AquariumEventType]
		,le.[NextEventID]
		,le.[Cost]
		,le.[LeadDocumentID]
		,le.[NotePriority]
		,le.[DocumentQueueID]
		,le.[EventDeleted]
		,le.[WhoDeleted]
		,le.[DeletionComments]
		,le.[ContactID]
		,le.[BaseCost]
		,le.[DisbursementCost]
		,le.[DisbursementDescription]
		,le.[ChargeOutRate]
		,le.[UnitsOfEffort]
		,le.[CostEnteredManually]
		,le.[IsOnHold]
		,le.[HoldLeadEventID] 
		
		FROM dbo.LeadEvent le (nolock) 
		INNER JOIN dbo.EventType et (nolock) on et.EventTypeID = le.EventTypeID 
		WHERE le.CaseID = @CaseID 
		AND (le.EventTypeID = @EventTypeID OR @EventTypeID IS NULL)
		ORDER BY le.LeadEventID 

	END
	ELSE
	BEGIN
		IF @LeadID IS NOT NULL
		BEGIN
		
			/* Show all values for this Lead */
			SELECT 
			le.[LeadEventID]
			,le.[ClientID]
			,le.[LeadID]
			,le.[CaseID]
			,le.[WhenCreated]
			,le.[WhoCreated]
			,et.EventTypeName
			,le.[Comments]
			,le.[EventTypeID]
			,et.EventSubtypeID
			,le.[NoteTypeID]
			,le.[FollowupDateTime]
			,le.[WhenFollowedUp]
			,le.[AquariumEventType]
			,le.[NextEventID]
			,le.[Cost]
			,le.[LeadDocumentID]
			,le.[NotePriority]
			,le.[DocumentQueueID]
			,le.[EventDeleted]
			,le.[WhoDeleted]
			,le.[DeletionComments]
			,le.[ContactID]
			,le.[BaseCost]
			,le.[DisbursementCost]
			,le.[DisbursementDescription]
			,le.[ChargeOutRate]
			,le.[UnitsOfEffort]
			,le.[CostEnteredManually]
			,le.[IsOnHold]
			,le.[HoldLeadEventID] 
			FROM dbo.LeadEvent le (nolock) 
			INNER JOIN dbo.EventType et (nolock) on et.EventTypeID = le.EventTypeID 
			WHERE le.LeadID = @LeadID 
			AND (le.EventTypeID = @EventTypeID OR @EventTypeID IS NULL)
			ORDER BY le.CaseID, le.LeadEventID 

		END
		ELSE
		BEGIN
		
			/* Show all events of this type */
			SELECT 
			le.[LeadEventID]
			,le.[ClientID]
			,le.[LeadID]
			,le.[CaseID]
			,le.[WhenCreated]
			,le.[WhoCreated]
			,et.EventTypeName
			,le.[Comments]
			,le.[EventTypeID]
			,et.EventSubtypeID
			,le.[NoteTypeID]
			,le.[FollowupDateTime]
			,le.[WhenFollowedUp]
			,le.[AquariumEventType]
			,le.[NextEventID]
			,le.[Cost]
			,le.[LeadDocumentID]
			,le.[NotePriority]
			,le.[DocumentQueueID]
			,le.[EventDeleted]
			,le.[WhoDeleted]
			,le.[DeletionComments]
			,le.[ContactID]
			,le.[BaseCost]
			,le.[DisbursementCost]
			,le.[DisbursementDescription]
			,le.[ChargeOutRate]
			,le.[UnitsOfEffort]
			,le.[CostEnteredManually]
			,le.[IsOnHold]
			,le.[HoldLeadEventID] 
			FROM dbo.LeadEvent le (nolock) 
			INNER JOIN dbo.EventType et (nolock) on et.EventTypeID = le.EventTypeID 
			WHERE le.EventTypeID = @EventTypeID 
			ORDER BY le.WhenCreated DESC

		END
		
	END
		
END







GO
GRANT VIEW DEFINITION ON  [dbo].[le] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[le] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[le] TO [sp_executeall]
GO
