SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Chris Williams
-- Create date: 2016-11-07
-- Description:	Prepares none standard fields for PHI documents
-- 2017-08-21 CPS added filters to prevent NULL DetailFieldIDs
-- 2018-03-27 MAB calculate "Covered Until Date" separately for One Month Free policies
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with variable
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PrepDocumentFields] 
(
	@MatterID INT, 
	@WhoCreated as INT = NULL
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50022656,
--	@DateAdjustmentType INT = 4
	
	DECLARE @Policyinceptiondate as DATE,
			@EffectiveDate as DATE,
			@LeadID as INT,
			@PolicyStartDate as DATE,
			@PetDOB as DATE,
			@AgeInMonths as INT,
			@PetAge as VARCHAR(25),
			@PolicyEffectiveDate as DATE,
			@AccountNo as Varchar(25),
			@SortNo as Varchar(25),
			@PurchaseProductID INT,
			@IntervalID INT,
			@AnnualPrem DECIMAL (18,2),
			@AnnualDate DATE, 
	        @ColMatterID INT,
			@BillingID INT,
			@ClientID	INT
	
	/*Get the policy admin leadID */
	SELECT	 @LeadID = m.LeadID 
			,@ClientID = m.ClientID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID = @MatterID
			
	/*Get the Billing ID for account details*/
	SELECT @ColMatterID = l.ToMatterID 
	FROM LeadTypeRelationship l WITH (NOLOCK)
	Where l.FromMatterID = @MatterID 
	and l.FromLeadTypeID = 1492
	--and l.ToLeadTypeID = 1490 -- Claims
    and l.ToLeadTypeID = 1493 -- Collections 
	 
    Select @BillingID = dbo.fnGetSimpleDv(176973,@ColMatterID)
	  
	/*Effective Date*/
	/*First deal with the date accident and illness claims become valid*/
	
	Select @Policyinceptiondate = dbo.fnGetSimpleDvAsDate(170035, @MatterID), /*Grab policy inception date*/
		   @EffectiveDate = DATEADD (Day,15,@Policyinceptiondate) /*Add 15 days to inception date to create effective date*/

	/*Stamp Effective Date to field */
	EXEC dbo._C00_SimpleValueIntoField 177328,@EffectiveDate,@MatterID,@WhoCreated
		
	/* Generate Pet Age In numbers + words */

	SELECT @PetDOB = dbo.fnGetSimpleDvAsDate(144274, @LeadID), 
		   @AgeInMonths = DATEDIFF(MONTH,@PetDOB, DATEADD(DAY, 1-DATEPART(DAY,@PetDOB), CONVERT(DATE,@Policyinceptiondate))) /*Handles whether the pet is one month younger than calculated when taking days into account*/

	SELECT @PetAge = CONVERT(VARCHAR, @AgeInMonths / 12) + CASE /*Age in months / 12 gives age in years */
			WHEN (@AgeInMonths / 12 = 1)
				THEN ' years '
			ELSE ' years '
			END + CONVERT(VARCHAR, @AgeInMonths % 12) + CASE /*Age in months modulo 12 gives age in months after years */
			WHEN (@AgeInMonths % 12 = 1)
				THEN ' month'
			ELSE ' months'
			END

	/*Write Pet Age to Field */
	EXEC dbo._C00_SimpleValueIntoField 177327,@PetAge,@LeadID,@WhoCreated

	/*- Period of Insurance – the end date of the policy should be 1 day before the anniversary, so 14/10/2016 – 13/10/2016 */
	Select @PolicyStartDate = dbo.fnGetSimpleDvAsDate(170036, @MatterID)
	
	/* 2018-03-27 Mark Beaumont : calculate "Covered Until Date" separately for One Month Free policies. */
	DECLARE	@IsOneMonthFreePolicy BIT
	SELECT @IsOneMonthFreePolicy = CASE WHEN dbo.fnGetSimpleDvAsInt(170034,  @MatterID) = 151577 THEN 1 ELSE 0 END	/* Is Current Policy = One Month Free? */
	
	IF @IsOneMonthFreePolicy = 1
		SELECT @PolicyEffectiveDate = DATEADD(DAY,-1,DATEADD(MONTH,1,@PolicyStartDate)) /* Calculates date policy is effective to, adds one month minus one day*/
	ELSE
		SELECT @PolicyEffectiveDate = DATEADD(DAY,-1,DATEADD(YEAR,1,@PolicyStartDate)) /* Calculates date policy is effective to, adds one year minus one day*/

	EXEC DBO._C00_SimpleValueIntoField 177287,@PolicyEffectiveDate,@MatterID,@WhoCreated


	/*Mask Payment Details*/
	SELECT @AccountNo = ISNULL(a.AccountNumber,''),  /*Account Number*/
		   @SortNo = ISNULL(a.Sortcode,'')
	FROM  Account a WITH (NOLOCK)
	WHERE a.AccountID = @BillingID; /*Account Sort Code*/ 
	  
	SELECT @AccountNo = REPLICATE('*',LEN(@AccountNo)-4) + RIGHT(@AccountNo,4), /*Masks all but the last four characters of the account number with '*' */
		   @SortNo = REPLICATE('*',LEN(@SortNo)-4) + RIGHT(@SortNo,4); /*Masks all but the last four characters of the sort code with '*' */

	EXEC DBO._C00_SimpleValueIntoField 177356,@AccountNo,@MatterID,@WhoCreated
	EXEC DBO._C00_SimpleValueIntoField 177357,@SortNo,@MatterID,@WhoCreated

	/*Payment Summary*/
	SELECT @PurchaseProductID = dbo.fnGetSimpleDvAsInt(177074,@MatterID),
		   @IntervalID = dbo.fnGetSimpleDvAsInt(170176,@MatterID)
	/* ACE LeadID is set at the top!,
           @LeadID = m.leadID 
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID = @MatterID*/
      
    IF @IntervalID = 69943 /*Monthly*/
    BEGIN 
         
         DECLARE @PaymentSchedule TABLE (ID INT IDENTITY (1,1) ,PaymentAmount DECIMAL(18,2), PaymentDate DATE, AmountDetailFieldID INT, DateDetailfieldID INT)
         INSERT INTO @PaymentSchedule (PaymentAmount,PaymentDate) 
         SELECT p.PaymentGross,p.PaymentDate  FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
         where p.PurchasedProductID = @PurchaseProductID 
         order by p.PaymentDate asc

         Update @PaymentSchedule 
         SET AmountDetailFieldID =     CASE ID WHEN 1 THEN 177341
                                                   WHEN 2 THEN 177342
                                                   WHEN 3 THEN 177343
                                                   WHEN 4 THEN 177344
                                                   WHEN 5 THEN 177345
                                                   WHEN 6 THEN 177346
                                                   WHEN 7 THEN 177347
                                                   WHEN 8 THEN 177348
                                                   WHEN 9 THEN 177349
                                                   WHEN 10 THEN 177350
                                                   WHEN 11 THEN 177351
                                                   WHEN 12 THEN 177353
                                                   END,
         DateDetailfieldID = CASE ID WHEN 1 THEN 177329
                                                   WHEN 2 THEN 177330
                                                   WHEN 3 THEN 177331
                                                   WHEN 4 THEN 177332
                                                   WHEN 5 THEN 177333
                                                   WHEN 6 THEN 177334
                                                   WHEN 7 THEN 177335
                                                   WHEN 8 THEN 177336
                                                   WHEN 9 THEN 177337
                                                   WHEN 10 THEN 177338
                                                   WHEN 11 THEN 177339
                                                   WHEN 12 THEN 177340
                                                   END
         /*Write the dates*/ 
         INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue) 
         SELECT @ClientID,@LeadID,@MatterID,p.DateDetailfieldID, p.PaymentDate
         FROM @PaymentSchedule p 
         WHERE p.DateDetailFieldID is not NULL /*CPS 2017-08-21*/

         /*Write the amount*/ 
         INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue) 
         SELECT @ClientID,@LeadID,@MatterID,p.AmountDetailFieldID, ISNULL(p.PaymentAmount,0.00) /*GPR 2020-10-05 ISNULL to 0.00 for CreateLinkedLeads*/
         FROM @PaymentSchedule p 
         WHERE p.AmountDetailFieldID is not NULL /*CPS 2017-08-21*/
         
         
     END
     ELSE /* IF @IntervalID <> 69943 'Monthly'  */
     BEGIN /*Annual*/  
         
         /*Only need to worry about the earliest payment*/
         SELECT @AnnualPrem = p.PaymentGross , @AnnualDate = p.PaymentDate FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
         where p.PurchasedProductID = @PurchaseProductID 
         and NOT EXISTS (SELECT * FROM PurchasedPRoductPaymentSchedule pp WITH (NOLOCK) where pp.PurchasedProductID = p.PurchasedProductID
                                 and pp.PaymentDate > p.PaymentDate)
                                 
         EXEC _C00_SimpleValueIntoField  177354,@AnnualPrem,@MatterID,@WhoCreated
         EXEC _C00_SimpleValueIntoField  177355,@AnnualDate,@MatterID,@WhoCreated
         
     END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PrepDocumentFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PrepDocumentFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PrepDocumentFields] TO [sp_executeall]
GO
