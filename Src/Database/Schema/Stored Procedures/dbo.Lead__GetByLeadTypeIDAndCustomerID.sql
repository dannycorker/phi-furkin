SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 3/11/2014
-- Description:	Gets a lead based upon the customer ID and the leadtype id
-- =============================================
CREATE PROCEDURE [dbo].[Lead__GetByLeadTypeIDAndCustomerID]
(
	@LeadTypeID int,
	@CustomerID int,
	@ClientID int   
)
AS
	SELECT * FROM [dbo].[Lead] WITH (NOLOCK) 					
	WHERE					
		[LeadTypeID] = @LeadTypeID AND 
		[CustomerID] = @CustomerID AND 
		[ClientID] = @ClientID
		
	SELECT @@ROWCOUNT
					
			


GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetByLeadTypeIDAndCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__GetByLeadTypeIDAndCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetByLeadTypeIDAndCustomerID] TO [sp_executeall]
GO
