SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-02
-- Description:	Update the Delete field on an 
--				Asset from Asset table
-- =============================================
CREATE PROCEDURE [dbo].[DeleteAsset] 
	@ClientPersonnelID INT = NULL,
	@AssetID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE a
	SET
		[Deleted] = 1
	FROM [dbo].[Asset] AS a
	INNER JOIN [ClientPersonnel] AS cp
		ON cp.[ClientID] = a.ClientID
	WHERE
		[AssetID] = @AssetID
		AND @ClientPersonnelID = cp.ClientPersonnelID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteAsset] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteAsset] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteAsset] TO [sp_executeall]
GO
