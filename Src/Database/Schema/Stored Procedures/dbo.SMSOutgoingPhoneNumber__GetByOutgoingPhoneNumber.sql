SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-05-2013
-- Description: Gets a list of outgoing sms phone numbers by outgoing phone number
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__GetByOutgoingPhoneNumber]
(
	@OutgoingPhoneNumber varchar(16)
)
AS

	SET ANSI_NULLS ON
	
	SELECT *
	FROM
		[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
	WHERE
		OutgoingPhoneNumber = @OutgoingPhoneNumber 
	
	SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__GetByOutgoingPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__GetByOutgoingPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__GetByOutgoingPhoneNumber] TO [sp_executeall]
GO
