SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-18
-- Description:	Imports items for bulk evaluations
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_TestFile_Import_Bulk]
(
	@ClientID INT, 
	@UserID INT, 
	@RuleSetID INT, 
	@ImportXml XML,
	@FileName VARCHAR(250)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	exec _C00_LogItXML @ClientID, @RuleSetID, 'RulesEngine_TestFile_Import', @ImportXml
	
	DECLARE @NewTestFileID INT = 0,
			@RowsImported INT = 0
	
	INSERT INTO dbo.RulesEngine_TestFile (ClientID, ChangeSetID, RuleSetID, FileName, WhoCreated, SourceID)
	SELECT rs.ClientID, rs.ChangeSetID, rs.RuleSetID, FileName, @UserID WhoCreated, TestFileSourceID
	FROM (
		SELECT	
			NULLIF(T.c.value('ClientID[1]','VARCHAR(250)'),'') AS ClientID,
			NULLIF(T.c.value('RuleSetID[1]','VARCHAR(250)'), '') AS RuleSetID,
			NULLIF(T.c.value('FileName[1]','VARCHAR(250)'), '') AS FileName,
			T.c.value('UID[1]','VARCHAR(250)') AS TestFileSourceID
		FROM	@ImportXML.nodes('/TestFile') T(c)
	) row
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = row.RuleSetID
	WHERE rs.ClientID = @ClientID
	
	SELECT @NewTestFileID = SCOPE_IDENTITY()
	
	INSERT INTO dbo.RulesEngine_TestFileItem (TestFileID, ClientID, Input, CustomerID, LeadID, CaseID, MatterID, EvaluateByEffectiveDate, EvaluateByChangeSetID, SourceID)
	SELECT @NewTestFileID, @ClientID, Input, CustomerID, LeadID, CaseID, MatterID, EvaluateByEffectiveDate, EvaluateByChangeSetID, TestFileItemSourceID
	FROM (
		SELECT	
			NULLIF(T.c.value('Input[1]','VARCHAR(250)'),'') AS Input,
			NULLIF(T.c.value('CustomerID[1]','VARCHAR(250)'), '') AS CustomerID,
			NULLIF(T.c.value('LeadID[1]','VARCHAR(250)'), '') AS LeadID,
			NULLIF(T.c.value('CaseID[1]','VARCHAR(250)'), '') AS CaseID,
			NULLIF(T.c.value('MatterID[1]','VARCHAR(250)'), '') AS MatterID,
			NULLIF(T.c.value('EvaluateByEffectiveDate[1]','VARCHAR(250)'), '') AS EvaluateByEffectiveDate,
			NULLIF(T.c.value('EvaluateByChangeSetID[1]','VARCHAR(250)'), '') AS EvaluateByChangeSetID,
			T.c.value('UID[1]','VARCHAR(250)') AS TestFileItemSourceID,
			T.c.value('../../UID[1]','VARCHAR(250)') AS TestFileSourceID
		FROM	@ImportXML.nodes('/TestFile/TestFileItems/TestFileItem') T(c)
	) row
	OPTION (OPTIMIZE FOR ( @ImportXML = NULL ))
	
	SELECT @RowsImported = @@ROWCOUNT

	INSERT INTO dbo.RulesEngine_TestFileItemOverride (TestFileItemID, ClientID, RuleParameterID, ParameterValue)
	SELECT tfi.TestFileItemID, tfi.ClientID, rp.RuleParameterID, row.ParameterValue
	FROM (
		SELECT
			NULLIF(T.c.value('RuleParameterID[1]','VARCHAR(250)'),'') AS RuleParameterID,
			NULLIF(T.c.value('ParameterName[1]','VARCHAR(250)'),'') AS ParameterName,
			NULLIF(T.c.value('ParameterTypeID[1]','VARCHAR(250)'),'') AS ParameterTypeID,
			NULLIF(T.c.value('ParameterValue[1]','VARCHAR(250)'),'') AS ParameterValue,
			NULLIF(T.c.value('DataTypeID[1]','VARCHAR(250)'),'') AS DataTypeID,
			NULLIF(T.c.value('../../UID[1]','VARCHAR(250)'),'') AS TestFileItemSourceID
		FROM	@ImportXML.nodes('/TestFile/TestFileItems/TestFileItem/Overrides/TestFileItemOverride') T(c)
	) row
	INNER JOIN RulesEngine_TestFileItem tfi WITH (NOLOCK) ON tfi.SourceID = row.TestFileItemSourceID
	INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = row.RuleParameterID
	WHERE tfi.ClientID = @ClientID
	ORDER BY tfi.TestFileItemID, rp.RuleParameterID
	OPTION (OPTIMIZE FOR ( @ImportXML = NULL ))
	
	DECLARE @ParsedCSV VARCHAR(MAX) = '"TestFileID","RowsImported"'+CHAR(13)+CHAR(10)
	SELECT @ParsedCSV += '"'+CAST(@NewTestFileID AS VARCHAR(50))+'","'+CAST(@RowsImported AS VARCHAR(50))+'"'+CHAR(13)+CHAR(10)
	
	SELECT @NewTestFileID ImportID, @ParsedCSV ResultCSV, @RowsImported TotalRows, 0 ErrorCount, @RowsImported SuccessCount

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Import_Bulk] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_TestFile_Import_Bulk] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Import_Bulk] TO [sp_executeall]
GO
