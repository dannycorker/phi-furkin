SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-07-17
-- Description:	Get enabled lookup values in the correct order
-- Modified:	2014-07-25	SB	Added function for shared lead types
-- JWG 2015-02-05 #30924 New option to get the currently selected item as well, even if disabled now.
-- =============================================
CREATE PROCEDURE [dbo].[LookupListItems__GetEnabledItemsSorted]
	@LookupListID INT,
	@ClientID INT,
	@IncludeDisabled BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SortOptionID INT
	
	SELECT @SortOptionID = ll.SortOptionID FROM LookupList ll WITH (NOLOCK) WHERE LookupListID=@LookupListID
	
	IF @SortOptionID = 21 -- Ascending
	BEGIN
		SELECT * FROM dbo.fnLookupListItemsShared(@ClientID) luli
		WHERE luli.LookupListID = @LookupListID 
		AND (luli.Enabled = 1 OR @IncludeDisabled = 1)
		ORDER BY ItemValue ASC
	END	
	ELSE IF @SortOptionID = 22 -- Descending
	BEGIN
		SELECT * FROM dbo.fnLookupListItemsShared(@ClientID) luli
		WHERE luli.LookupListID = @LookupListID 
		AND (luli.Enabled = 1 OR @IncludeDisabled = 1)
		ORDER BY ItemValue DESC		
	END
	ELSE 
	BEGIN
		SELECT * FROM dbo.fnLookupListItemsShared(@ClientID) luli
		WHERE luli.LookupListID = @LookupListID 
		AND (luli.Enabled = 1 OR @IncludeDisabled = 1)
		ORDER BY SortOrder ASC
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems__GetEnabledItemsSorted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems__GetEnabledItemsSorted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems__GetEnabledItemsSorted] TO [sp_executeall]
GO
