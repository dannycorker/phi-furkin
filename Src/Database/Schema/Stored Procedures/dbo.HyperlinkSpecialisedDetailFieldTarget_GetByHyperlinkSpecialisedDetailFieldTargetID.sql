SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the HyperlinkSpecialisedDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkSpecialisedDetailFieldTargetID]
(

	@HyperlinkSpecialisedDetailFieldTargetID int   
)
AS


				SELECT
					[HyperlinkSpecialisedDetailFieldTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[RowField],
					[Notes]
				FROM
					[dbo].[HyperlinkSpecialisedDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[HyperlinkSpecialisedDetailFieldTargetID] = @HyperlinkSpecialisedDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkSpecialisedDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkSpecialisedDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkSpecialisedDetailFieldTargetID] TO [sp_executeall]
GO
