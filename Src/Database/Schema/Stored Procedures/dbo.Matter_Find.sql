SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Matter table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_Find]
(

	@SearchUsingOR bit   = null ,

	@MatterID int   = null ,

	@ClientID int   = null ,

	@MatterRef varchar (100)  = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@MatterStatus tinyint   = null ,

	@RefLetter varchar (3)  = null ,

	@BrandNew bit   = null ,

	@CaseID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime2   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime2   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MatterID]
	, [ClientID]
	, [MatterRef]
	, [CustomerID]
	, [LeadID]
	, [MatterStatus]
	, [RefLetter]
	, [BrandNew]
	, [CaseID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
    FROM
	[dbo].[Matter] WITH (NOLOCK) 
    WHERE 
	 ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([MatterRef] = @MatterRef OR @MatterRef IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([MatterStatus] = @MatterStatus OR @MatterStatus IS NULL)
	AND ([RefLetter] = @RefLetter OR @RefLetter IS NULL)
	AND ([BrandNew] = @BrandNew OR @BrandNew IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MatterID]
	, [ClientID]
	, [MatterRef]
	, [CustomerID]
	, [LeadID]
	, [MatterStatus]
	, [RefLetter]
	, [BrandNew]
	, [CaseID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
    FROM
	[dbo].[Matter] WITH (NOLOCK) 
    WHERE 
	 ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([MatterRef] = @MatterRef AND @MatterRef is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([MatterStatus] = @MatterStatus AND @MatterStatus is not null)
	OR ([RefLetter] = @RefLetter AND @RefLetter is not null)
	OR ([BrandNew] = @BrandNew AND @BrandNew is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Find] TO [sp_executeall]
GO
