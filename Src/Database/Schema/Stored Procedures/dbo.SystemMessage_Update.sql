SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SystemMessage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessage_Update]
(

	@SystemMessageID int   ,

	@ClientID int   ,

	@SystemMessageName varchar (200)  ,

	@SystemMessageText varchar (MAX)  ,

	@Enabled bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SystemMessage]
				SET
					[ClientID] = @ClientID
					,[SystemMessageName] = @SystemMessageName
					,[SystemMessageText] = @SystemMessageText
					,[Enabled] = @Enabled
				WHERE
[SystemMessageID] = @SystemMessageID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessage_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Update] TO [sp_executeall]
GO
