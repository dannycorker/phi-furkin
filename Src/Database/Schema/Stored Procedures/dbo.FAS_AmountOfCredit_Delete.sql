SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FAS_AmountOfCredit table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_Delete]
(

	@FasAmountOfCreditID int   
)
AS


				DELETE FROM [dbo].[FAS_AmountOfCredit] WITH (ROWLOCK) 
				WHERE
					[FasAmountOfCreditID] = @FasAmountOfCreditID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Delete] TO [sp_executeall]
GO
