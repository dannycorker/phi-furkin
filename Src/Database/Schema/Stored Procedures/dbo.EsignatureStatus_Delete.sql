SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureStatus_Delete]
(

	@EsignatureStatusID int   
)
AS


				DELETE FROM [dbo].[EsignatureStatus] WITH (ROWLOCK) 
				WHERE
					[EsignatureStatusID] = @EsignatureStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Delete] TO [sp_executeall]
GO
