SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-01-08
-- Description:	Morph an out of process event into an in process one
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MorphLeadEvent]
@LeadEventID INT,
@NewEventTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE LeadEvent
	SET EventTypeID = @NewEventTypeID, WhenFollowedUp = NULL, NextEventID = NULL, Comments = LEFT(Comments + ' This event has been morphed',2000)
	WHERE LeadEventID = @LeadEventID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MorphLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MorphLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MorphLeadEvent] TO [sp_executeall]
GO
