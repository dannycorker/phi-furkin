SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 04-02-2013
-- Description:	Gets a list of the Un suppressed call list rows for a lead
-- =============================================
CREATE PROCEDURE [dbo].[__GetUnSuppressedCallListRows]

	@ClientID INT,
	@LeadID INT,
	@CaseID INT

AS
BEGIN

	DECLARE @TableRowID INT,
			@DetailFieldPageID INT,
			@DetailFieldID INT,
			@354FieldID INT, -- RowID
			@355FieldID INT, -- ListID			
			@357FieldID INT, -- CaseID						
			@363FieldID INT, -- Suppressed
			@LeadTypeID INT,
			@CustomerID INT
	
	SELECT @CustomerID=CustomerID, @LeadTypeID = LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID	
	
	SELECT @354FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 354
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @355FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 355
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @357FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 357
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @363FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 363
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID	
	
	SELECT @DetailFieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 354
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT  tdvRowID.DetailValue RowID,
			tdvListID.DetailValue ListID,
			tdvCaseID.DetailValue CaseID,
			tdvSuppressed.DetailValue Suppressed
	FROM TableDetailValues tdvRowID WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdvListID WITH (NOLOCK) ON tdvListID.TableRowID = tdvRowID.TableRowID AND tdvListID.DetailFieldID = @355FieldID
	INNER JOIN TableDetailValues tdvCaseID WITH (NOLOCK) ON tdvCaseID.TableRowID = tdvRowID.TableRowID AND tdvCaseID.DetailFieldID = @357FieldID AND tdvCaseID.ValueInt=@CaseID
	INNER JOIN TableDetailValues tdvSuppressed WITH (NOLOCK) ON tdvSuppressed.TableRowID = tdvRowID.TableRowID AND tdvSuppressed.DetailFieldID = @363FieldID AND tdvSuppressed.DetailValue='false'
	WHERE tdvRowID.DetailFieldID = @354FieldID AND tdvRowID.ClientID=@ClientID AND tdvRowID.CustomerID=@CustomerID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__GetUnSuppressedCallListRows] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetUnSuppressedCallListRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetUnSuppressedCallListRows] TO [sp_executeall]
GO
