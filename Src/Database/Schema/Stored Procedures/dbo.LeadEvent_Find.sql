SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadEvent table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadEventID int   = null ,

	@ClientID int   = null ,

	@LeadID int   = null ,

	@WhenCreated datetime   = null ,

	@WhoCreated int   = null ,

	@Cost money   = null ,

	@Comments varchar (2000)  = null ,

	@EventTypeID int   = null ,

	@NoteTypeID int   = null ,

	@FollowupDateTime datetime   = null ,

	@WhenFollowedUp datetime   = null ,

	@AquariumEventType int   = null ,

	@NextEventID int   = null ,

	@CaseID int   = null ,

	@LeadDocumentID int   = null ,

	@NotePriority int   = null ,

	@DocumentQueueID int   = null ,

	@EventDeleted bit   = null ,

	@WhoDeleted int   = null ,

	@DeletionComments varchar (2000)  = null ,

	@ContactID int   = null ,

	@BaseCost money   = null ,

	@DisbursementCost money   = null ,

	@DisbursementDescription varchar (200)  = null ,

	@ChargeOutRate money   = null ,

	@UnitsOfEffort int   = null ,

	@CostEnteredManually bit   = null ,

	@IsOnHold bit   = null ,

	@HoldLeadEventID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadEventID]
	, [ClientID]
	, [LeadID]
	, [WhenCreated]
	, [WhoCreated]
	, [Cost]
	, [Comments]
	, [EventTypeID]
	, [NoteTypeID]
	, [FollowupDateTime]
	, [WhenFollowedUp]
	, [AquariumEventType]
	, [NextEventID]
	, [CaseID]
	, [LeadDocumentID]
	, [NotePriority]
	, [DocumentQueueID]
	, [EventDeleted]
	, [WhoDeleted]
	, [DeletionComments]
	, [ContactID]
	, [BaseCost]
	, [DisbursementCost]
	, [DisbursementDescription]
	, [ChargeOutRate]
	, [UnitsOfEffort]
	, [CostEnteredManually]
	, [IsOnHold]
	, [HoldLeadEventID]
    FROM
	[dbo].[LeadEvent] WITH (NOLOCK) 
    WHERE 
	 ([LeadEventID] = @LeadEventID OR @LeadEventID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([Cost] = @Cost OR @Cost IS NULL)
	AND ([Comments] = @Comments OR @Comments IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([NoteTypeID] = @NoteTypeID OR @NoteTypeID IS NULL)
	AND ([FollowupDateTime] = @FollowupDateTime OR @FollowupDateTime IS NULL)
	AND ([WhenFollowedUp] = @WhenFollowedUp OR @WhenFollowedUp IS NULL)
	AND ([AquariumEventType] = @AquariumEventType OR @AquariumEventType IS NULL)
	AND ([NextEventID] = @NextEventID OR @NextEventID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([LeadDocumentID] = @LeadDocumentID OR @LeadDocumentID IS NULL)
	AND ([NotePriority] = @NotePriority OR @NotePriority IS NULL)
	AND ([DocumentQueueID] = @DocumentQueueID OR @DocumentQueueID IS NULL)
	AND ([EventDeleted] = @EventDeleted OR @EventDeleted IS NULL)
	AND ([WhoDeleted] = @WhoDeleted OR @WhoDeleted IS NULL)
	AND ([DeletionComments] = @DeletionComments OR @DeletionComments IS NULL)
	AND ([ContactID] = @ContactID OR @ContactID IS NULL)
	AND ([BaseCost] = @BaseCost OR @BaseCost IS NULL)
	AND ([DisbursementCost] = @DisbursementCost OR @DisbursementCost IS NULL)
	AND ([DisbursementDescription] = @DisbursementDescription OR @DisbursementDescription IS NULL)
	AND ([ChargeOutRate] = @ChargeOutRate OR @ChargeOutRate IS NULL)
	AND ([UnitsOfEffort] = @UnitsOfEffort OR @UnitsOfEffort IS NULL)
	AND ([CostEnteredManually] = @CostEnteredManually OR @CostEnteredManually IS NULL)
	AND ([IsOnHold] = @IsOnHold OR @IsOnHold IS NULL)
	AND ([HoldLeadEventID] = @HoldLeadEventID OR @HoldLeadEventID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadEventID]
	, [ClientID]
	, [LeadID]
	, [WhenCreated]
	, [WhoCreated]
	, [Cost]
	, [Comments]
	, [EventTypeID]
	, [NoteTypeID]
	, [FollowupDateTime]
	, [WhenFollowedUp]
	, [AquariumEventType]
	, [NextEventID]
	, [CaseID]
	, [LeadDocumentID]
	, [NotePriority]
	, [DocumentQueueID]
	, [EventDeleted]
	, [WhoDeleted]
	, [DeletionComments]
	, [ContactID]
	, [BaseCost]
	, [DisbursementCost]
	, [DisbursementDescription]
	, [ChargeOutRate]
	, [UnitsOfEffort]
	, [CostEnteredManually]
	, [IsOnHold]
	, [HoldLeadEventID]
    FROM
	[dbo].[LeadEvent] WITH (NOLOCK) 
    WHERE 
	 ([LeadEventID] = @LeadEventID AND @LeadEventID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([Cost] = @Cost AND @Cost is not null)
	OR ([Comments] = @Comments AND @Comments is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([NoteTypeID] = @NoteTypeID AND @NoteTypeID is not null)
	OR ([FollowupDateTime] = @FollowupDateTime AND @FollowupDateTime is not null)
	OR ([WhenFollowedUp] = @WhenFollowedUp AND @WhenFollowedUp is not null)
	OR ([AquariumEventType] = @AquariumEventType AND @AquariumEventType is not null)
	OR ([NextEventID] = @NextEventID AND @NextEventID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([LeadDocumentID] = @LeadDocumentID AND @LeadDocumentID is not null)
	OR ([NotePriority] = @NotePriority AND @NotePriority is not null)
	OR ([DocumentQueueID] = @DocumentQueueID AND @DocumentQueueID is not null)
	OR ([EventDeleted] = @EventDeleted AND @EventDeleted is not null)
	OR ([WhoDeleted] = @WhoDeleted AND @WhoDeleted is not null)
	OR ([DeletionComments] = @DeletionComments AND @DeletionComments is not null)
	OR ([ContactID] = @ContactID AND @ContactID is not null)
	OR ([BaseCost] = @BaseCost AND @BaseCost is not null)
	OR ([DisbursementCost] = @DisbursementCost AND @DisbursementCost is not null)
	OR ([DisbursementDescription] = @DisbursementDescription AND @DisbursementDescription is not null)
	OR ([ChargeOutRate] = @ChargeOutRate AND @ChargeOutRate is not null)
	OR ([UnitsOfEffort] = @UnitsOfEffort AND @UnitsOfEffort is not null)
	OR ([CostEnteredManually] = @CostEnteredManually AND @CostEnteredManually is not null)
	OR ([IsOnHold] = @IsOnHold AND @IsOnHold is not null)
	OR ([HoldLeadEventID] = @HoldLeadEventID AND @HoldLeadEventID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Find] TO [sp_executeall]
GO
