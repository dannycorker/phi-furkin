SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-11-2012
-- Description:	EventGroup Insert
-- JWG 2013-01-28 Added EventGroupMember records
-- =============================================
CREATE PROCEDURE [dbo].[EventGroup__Insert]

	@ClientID INT,
	@EventGroupName VARCHAR(50), 
	@EventGroupDescription VARCHAR(2000), 
	@WhoCreated INT, 
	@WhenCreated DATETIME, 
	@WhoModified INT, 
	@WhenModified DATETIME, 
	@ChangeNotes VARCHAR(2000),
	@MembersToAdd TvpIntInt READONLY, 
	@SourceID INT = NULL, 
	@LeadTypeID INT = NULL 

AS
BEGIN

	DECLARE @EventGroupID INT

	INSERT INTO EventGroup 
		(ClientID, EventGroupName, EventGroupDescription, WhoCreated, WhenCreated, WhoModified, WhenModified, ChangeNotes, SourceID, LeadTypeID)
	VALUES 
		(@ClientID, @EventGroupName, @EventGroupDescription, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified, @ChangeNotes, @SourceID, @LeadTypeID)

	/* Get the identity value */
	SELECT @EventGroupID = SCOPE_IDENTITY()
	
	/* Add the EventGroupMember records now (if any were passed in) */
	IF EXISTS (SELECT * FROM @MembersToAdd)
	BEGIN
	
		INSERT INTO dbo.EventGroupMember (ClientID, EventGroupID, EventGroupMemberDescription, EventTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		SELECT @ClientID, @EventGroupID, '', m.ID2, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified
		FROM @MembersToAdd m
		
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroup__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__Insert] TO [sp_executeall]
GO
