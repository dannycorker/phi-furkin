SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- ALTER date:  2020-09-14
-- Description:	Gets the pivoted policy sections for the user to select
-- Used by:		Prudent Pet
-- UPDATES:		GPR 2020-10-12 Prudent policies are created to start tomorrow, added DATEADD(DAY,-1 to FromDate.
--				GPR 2020-10-22 Added FileID from GeneralDocStore
--				GPR 2020-10-30 Added UploadedFileName and UploadedFileTitle to output
--				GPR 2020-10-30 Added SubType 20 (Case Summary) to filter
-- =============================================
CREATE PROCEDURE [dbo].[_C605_GetDocumentListByPet] 
(
	@MatterID INT
)
WITH RECOMPILE
AS
BEGIN
	
	
	SET NOCOUNT ON;

/* Test with MatterID 215612 */
	/*GPR 2020-10-23*/
	;WITH PolicyTerms (TermID,MatterID,FromDate,ToDate,rn) AS 
	(
	SELECT pp.PurchasedProductID, m.MatterID,pp.ValidFrom, pp.ValidTo,  ROW_NUMBER() OVER(PARTITION BY m.MatterID,pp.ObjectID ORDER BY pp.PurchasedProductID) FROM Matter m WITH (NOLOCK) 
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.ObjectID = m.MatterID 
	WHERE m.MatterID = @MatterID  
	)
	SELECT DISTINCT
	ld.LeadDocumentID AS [DocumentID],
	ld.LeadDocumentTitle AS [File Title],
	et.EventTypeName AS [Event Type Name],
	et.EventTypeID AS [EventTypeID],
	le.WhenCreated AS [Date Time of Event],
	uf.UploadedFileID,
	uf.UploadedFileName,
	uf.UploadedFileTitle,
	p.rn					
	FROM PolicyTerms p
	INNER JOIN  Matter M WITH (NOLOCK) ON m.MatterID = p.MatterID
	INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = m.CaseID 
	INNER JOIN LeadEvent le WITH (NOLOCK) ON le.CaseID = c.CaseID 
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID and et.EventSubtypeID IN (4,6,20) 
	INNER JOIN LeadDocument ld WITH (NOLOCK) ON ld.LeadDocumentID = le.LeadDocumentID
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 314232
	INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.CustomerID = m.CustomerID AND cdv.DetailFieldID = 314016 /*State Variance*/
	INNER JOIN ResourceListDetailValues svrldv WITH (NOLOCK) ON svrldv.ResourceListID = mdv.ValueInt AND svrldv.DetailFieldID IN (314228, 314229, 314230, 314231)
	INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) ON rdv.ResourceListID = cdv.ValueInt AND rdv.DetailFieldID = svrldv.ValueInt
	OUTER APPLY dbo.fnTableOfIDsFromCSVDistinct(rdv.DetailValue) fn
	LEFT JOIN UploadedFileAttachment ufa WITH (NOLOCK) ON ufa.UploadedFileID = fn.AnyID AND ufa.EventTypeID = et.EventTypeID
	LEFT JOIN UploadedFile uf WITH (NOLOCK) ON uf.UploadedFileID = ufa.UploadedFileID
	WHERE m.MatterID = @MatterID  
	AND le.WhenCreated BETWEEN DATEADD(DAY,-1,p.FromDate) AND p.ToDate /*GPR 2020-10-12*/
	ORDER BY p.rn


END
GO
