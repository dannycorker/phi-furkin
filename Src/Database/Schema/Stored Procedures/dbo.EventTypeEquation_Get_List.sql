SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeEquation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeEquation_Get_List]

AS


				
				SELECT
					[EventTypeEquationID],
					[ClientID],
					[EventTypeID],
					[DetailFieldID]
				FROM
					[dbo].[EventTypeEquation] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Get_List] TO [sp_executeall]
GO
