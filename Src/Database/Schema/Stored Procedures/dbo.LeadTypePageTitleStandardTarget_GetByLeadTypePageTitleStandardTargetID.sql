SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitleStandardTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypePageTitleStandardTargetID]
(

	@LeadTypePageTitleStandardTargetID int   
)
AS


				SELECT
					[LeadTypePageTitleStandardTargetID],
					[ClientID],
					[LeadTypeID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes]
				FROM
					[dbo].[LeadTypePageTitleStandardTarget] WITH (NOLOCK) 
				WHERE
										[LeadTypePageTitleStandardTargetID] = @LeadTypePageTitleStandardTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypePageTitleStandardTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypePageTitleStandardTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypePageTitleStandardTargetID] TO [sp_executeall]
GO
