SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-21
-- Description:	Writes the refund response error to the refund table
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteRefundError]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT,
	@RefundError VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ColumnDetailFieldID INT
	
	SELECT @ColumnDetailFieldID = ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=617

	DECLARE @TableDetailValueID INT
	SELECT @TableDetailValueID=TableDetailValueID FROM TableDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID = @ColumnDetailFieldID AND ClientID=@ClientID AND CustomerID=@CustomerID AND TableRowID=@TableRowID

	IF @TableDetailValueID IS NULL
	BEGIN
	
		INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
		SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																		WHEN 617 THEN @RefundError
																		ELSE ''
																		END
		FROM ThirdPartyFieldMapping tpm
		WHERE tpm.ClientID = @ClientID
		AND tpm.LeadTypeID = 0
		AND tpm.ThirdPartyFieldID IN (617)
	
	END
	ELSE
	BEGIN
		
		UPDATE TableDetailValues
		SET DetailValue = @RefundError
		WHERE TableDetailValueID = @TableDetailValueID
		
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRefundError] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteRefundError] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRefundError] TO [sp_executeall]
GO
