SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - Policy on hold - Tentative customer match
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_155200]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@UseExistingAccount INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


	/* Policy on hold - Tentative customer match */
	IF @EventTypeID IN (155200)
	BEGIN
			
		-- set breed for page title
		EXEC _C600_SAS @WhoCreated, @CustomerID, @LeadID
					
		-- Add banner note (delete any exisitng first)
		SELECT @UseExistingAccount = ISNULL(dbo.fnGetDvAsInt(175274,@CaseID),0)
		SELECT @ErrorMessage='PH believes they already have a policy. Please search and if you can find a match enter the ' + 
							'ID of the existing Policy Holder when applying the next action' 
							+ CASE WHEN @UseExistingAccount=5144 THEN
							' They also want to use their existing account details. Please check if the details supplied match' + 
							' account details on the existing policy holder and if they do enter the Collection Matter ID for ' + 
							'the existing account details when applying the next action.'
							ELSE
							'.'
							END

		EXEC dbo._C00_SimpleValueIntoField 175360, '', @CustomerID
		
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155200] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_155200] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155200] TO [sp_executeall]
GO
