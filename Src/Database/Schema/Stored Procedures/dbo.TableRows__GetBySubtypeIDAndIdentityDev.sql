SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-12
-- Description:	Gets a list of tablerows based upon subtype id and the given identity
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetBySubtypeIDAndIdentityDev]
(
	@identity int, 
	@detailFieldSubTypeID int, 
	@detailFieldID int, 	
	@detailFieldPageID int,
	@clientID int
)

AS

	SELECT * FROM TableRows WHERE ClientID = @ClientID

	/*declare @simpleClause varchar(max) = '',
	@query VARCHAR(MAX) = ''
	
	SELECT @simpleClause = CASE 
		WHEN @detailFieldSubTypeID = 1 THEN ' AND (tr.LeadID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 2 THEN ' AND (tr.MatterID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 10 THEN ' AND (tr.CustomerID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 11 THEN ' AND (tr.CaseID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 12 THEN 'AND (tr.ClientID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 13 THEN ' AND (tr.ClientPersonnelID = ' + Cast(@identity as VarChar) + ')'
		WHEN @detailFieldSubTypeID = 14 THEN ' AND (tr.ContactID = ' + Cast(@identity as VarChar) + ')'
	END

	
    SET @query = 'SELECT * FROM [dbo].[TableRows] tr WITH (NOLOCK)     
    WHERE (DetailFieldID = ' + Cast(@detailFieldID as VarChar) + ') AND    
    ([DetailFieldPageID] = ' + Cast(@detailFieldPageID as VarChar) + ') AND
    ([ClientID] = ' + Cast(@ClientID as VarChar) + ') ' + @simpleClause					

	exec (@query)*/
						
	Select @@ROWCOUNT			





GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetBySubtypeIDAndIdentityDev] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetBySubtypeIDAndIdentityDev] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetBySubtypeIDAndIdentityDev] TO [sp_executeall]
GO
