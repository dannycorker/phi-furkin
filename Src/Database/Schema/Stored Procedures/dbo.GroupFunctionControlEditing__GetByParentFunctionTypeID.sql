SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*
----------------------------------------------------------------------------------------------------

-- Created By: Jim Green
-- Purpose: Select records from the GroupFunctionControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing__GetByParentFunctionTypeID]
(
	@ClientPersonnelAdminGroupID int,
	@ParentFunctionTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					*
				FROM
					[dbo].[GroupFunctionControlEditing]
				WHERE
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
				AND [FunctionTypeID] IN (SELECT FunctionTypeID FROM FunctionType WHERE ParentFunctionTypeID = @ParentFunctionTypeID)
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			









GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing__GetByParentFunctionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing__GetByParentFunctionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing__GetByParentFunctionTypeID] TO [sp_executeall]
GO
