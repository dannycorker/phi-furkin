SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LogLevel table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LogLevel_Find]
(

	@SearchUsingOR bit   = null ,

	@LogLevelID int   = null ,

	@LogLevelName varchar (250)  = null ,

	@LogLevelDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LogLevelID]
	, [LogLevelName]
	, [LogLevelDescription]
    FROM
	[dbo].[LogLevel] WITH (NOLOCK) 
    WHERE 
	 ([LogLevelID] = @LogLevelID OR @LogLevelID IS NULL)
	AND ([LogLevelName] = @LogLevelName OR @LogLevelName IS NULL)
	AND ([LogLevelDescription] = @LogLevelDescription OR @LogLevelDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LogLevelID]
	, [LogLevelName]
	, [LogLevelDescription]
    FROM
	[dbo].[LogLevel] WITH (NOLOCK) 
    WHERE 
	 ([LogLevelID] = @LogLevelID AND @LogLevelID is not null)
	OR ([LogLevelName] = @LogLevelName AND @LogLevelName is not null)
	OR ([LogLevelDescription] = @LogLevelDescription AND @LogLevelDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogLevel_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Find] TO [sp_executeall]
GO
