SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserFunctionControlEditing table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing_GetByRightID]
(

	@RightID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[UserFunctionControlEditingID],
					[ClientPersonnelID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[UserFunctionControlEditing] WITH (NOLOCK) 
				WHERE
					[RightID] = @RightID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_GetByRightID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing_GetByRightID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_GetByRightID] TO [sp_executeall]
GO
