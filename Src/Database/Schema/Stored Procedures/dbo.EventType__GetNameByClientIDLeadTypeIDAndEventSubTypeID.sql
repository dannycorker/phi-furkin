SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01-04-2008
-- Created By:    Ben Crinion
-- Purpose:		  Get EventTypeName and EventTypeID for binding to a DropDownList.
-- JWG 2012-02-07 Exclude disabled events
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetNameByClientIDLeadTypeIDAndEventSubTypeID] 
(
	@ClientID int,
	@LeadTypeID int = null,
	@EventSubTypeID int = null
)
AS
BEGIN
	SET ANSI_NULLS OFF

	IF @ClientID = 145
	BEGIN

		SELECT
			[EventTypeID],
			[EventTypeName]
		FROM
			dbo.fnEventTypeShared(@ClientID) et
		WHERE
			ClientID = @ClientID
			AND (@LeadTypeID  IS NULL OR LeadTypeID = @LeadTypeID)
			AND (@EventSubTypeID IS NULL OR EventSubTypeID = @EventSubTypeID) 
			--AND et.Enabled = 1 /* JWG 2012-02-07 */
		ORDER BY EventTypeName

	END
	ELSE
	BEGIN
	
		SELECT
			[EventTypeID],
			[EventTypeName]
		FROM
			dbo.fnEventTypeShared(@ClientID) et
		WHERE
			ClientID = @ClientID
			AND (@LeadTypeID  IS NULL OR LeadTypeID = @LeadTypeID)
			AND (@EventSubTypeID IS NULL OR EventSubTypeID = @EventSubTypeID) 
			AND et.Enabled = 1 /* JWG 2012-02-07 */
		ORDER BY EventTypeName

	END

	SET ANSI_NULLS ON
END






GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetNameByClientIDLeadTypeIDAndEventSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetNameByClientIDLeadTypeIDAndEventSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetNameByClientIDLeadTypeIDAndEventSubTypeID] TO [sp_executeall]
GO
