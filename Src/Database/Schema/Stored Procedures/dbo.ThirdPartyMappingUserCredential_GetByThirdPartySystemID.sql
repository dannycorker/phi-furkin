SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingUserCredential table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential_GetByThirdPartySystemID]
(

	@ThirdPartySystemID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyMappingUserCredentialID],
					[ThirdPartySystemID],
					[ClientID],
					[UserID],
					[UserName],
					[Password],
					[Salt],
					[Domain],
					[AttemptedLogins],
					[AccountDisabled],
					[ThirdPartyClientID],
					[LDAPConfigID],
					[ImpersonateThirdPartySystemID]
				FROM
					[dbo].[ThirdPartyMappingUserCredential] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemID] = @ThirdPartySystemID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_GetByThirdPartySystemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential_GetByThirdPartySystemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_GetByThirdPartySystemID] TO [sp_executeall]
GO
