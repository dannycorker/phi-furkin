SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-05-19
-- Description:	Undelete data. IE Move Customer
--				/Lead/Case/Matter to ClientID 20
-- =============================================
CREATE PROCEDURE [dbo].[UndeleteDataForDPA]
	@ObjectID INT,
	@ObjectLevel INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT,
			@TestStatus INT,
			@CustomerIDToBeRestored INT = NULL, 
			@LeadIDToBeRestored INT = NULL, 
			@CaseIDToBeRestored INT = NULL, 
			@MatterIDToBeRestored INT = NULL,
			@CustomerIDToRestoreTo INT,
			@LeadIDToRestoredTo INT,
			@CaseIDToRestoreTo INT,
			@LastRowCount INT,
			@LeadAndCaseIDs tvpIntInt,
			@LogXmlEntry VARCHAR(MAX),
			@ContextInfo VARBINARY(100)

	DECLARE @LogOutput TABLE (UNID INT IDENTITY (1,1), LogArea VARCHAR(50), LogResult INT)

	IF @ObjectLevel = 10
	BEGIN
		SELECT @CustomerIDToBeRestored = @ObjectID
	END

	IF @ObjectLevel = 1
	BEGIN
		SELECT @LeadIDToBeRestored = @ObjectID
	END

	IF @ObjectLevel = 11
	BEGIN
		SELECT @CaseIDToBeRestored = @ObjectID
	END

	IF @ObjectLevel = 2
	BEGIN
		SELECT @MatterIDToBeRestored = @ObjectID
	END

	/*
		So, someone has soft deleted something in error.
		We will need to restore the Customer/Lead/Case/Matter
		The original Customer/Lead/Case/Matter ID are kept in ArchivedObjects
	*/
	IF @CustomerIDToBeRestored > 0
	BEGIN
	
		/*
			As the whole customer is to be restored we must move everything back..
			Get the original client ID for the move back.
		*/
		SELECT @ClientID = a.ClientID
		FROM ArchivedObjects a WITH (NOLOCK)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		
		/*
			Here we should determine if the customer was set to test before we un-set it. We will use CustomerHistory to determine this.
		*/
		
		SELECT TOP (1) @TestStatus = ch.Test
		FROM CustomerHistory ch WITH (NOLOCK) 
		WHERE ch.CustomerID = @CustomerIDToBeRestored
		AND ch.ChangeNotes NOT LIKE '%SoftDelete For DPA%'
		ORDER BY ch.CustomerHistoryID DESC
		
		UPDATE Customers
		SET ClientID = @ClientID, Test = @TestStatus
		FROM Customers
		WHERE Customers.CustomerID = @CustomerIDToBeRestored
		AND Customers.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Customers', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE IncomingPostEvent 
		SET ClientID = @ClientID, CustomerID = a.CustomerID, LeadID = a.LeadID
		FROM IncomingPostEvent 
		INNER JOIN ArchivedObjects a ON (a.LeadID = IncomingPostEvent.LeadID OR IncomingPostEvent.CaseID = a.CaseID OR IncomingPostEvent.CustomerID = a.CustomerID OR a.MatterID = IncomingPostEvent.MatterID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND IncomingPostEvent.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('IncomingPostEvent', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE IncomingPostEventValue 
		SET ClientID = @ClientID, CustomerID = a.CustomerID, LeadID = a.LeadID
		FROM IncomingPostEventValue 
		INNER JOIN ArchivedObjects a ON (a.LeadID = IncomingPostEventValue.LeadID OR IncomingPostEventValue.CaseID = a.CaseID OR IncomingPostEventValue.CustomerID = a.CustomerID OR a.MatterID = IncomingPostEventValue.MatterID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND IncomingPostEventValue.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('IncomingPostEventValue', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerQuestionnaires 
		SET ClientID = @ClientID
		FROM CustomerQuestionnaires
		WHERE CustomerQuestionnaires.CustomerID = @CustomerIDToBeRestored
		AND CustomerQuestionnaires.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerQuestionnaires', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE WorkflowTask
		SET ClientID = @ClientID, CaseID = a.CaseID, LeadID = a.LeadID
		FROM WorkflowTask
		INNER JOIN ArchivedObjects a ON (a.LeadID = WorkflowTask.LeadID or a.CaseID = WorkflowTask.CaseID) 
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND WorkflowTask.ClientID = 20
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('WorkflowTask', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE Cases
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM Cases 
		INNER JOIN ArchivedObjects a ON (a.LeadID = Cases.LeadID OR a.LeadID = Cases.LeadID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND Cases.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Cases', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableRows
		SET ClientID = @ClientID
		FROM TableRows 
		WHERE CustomerID = @CustomerIDToBeRestored
		AND TableRows.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableRows (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableRows
		SET ClientID = a.ClientID
		FROM TableRows 
		INNER JOIN ArchivedObjects a ON (a.LeadID = TableRows.LeadID AND (TableRows.MatterID = 0 OR TableRows.MatterID IS NULL))
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableRows.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableRows (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableRows
		SET ClientID = a.ClientID
		FROM TableRows 
		INNER JOIN ArchivedObjects a ON a.CaseID = TableRows.CaseID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableRows.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableRows (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableRows
		SET ClientID = a.ClientID, LeadID = a.LeadID
		FROM TableRows 
		INNER JOIN ArchivedObjects a ON a.MatterID = TableRows.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableRows.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableRows (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE Department
		SET ClientID = @ClientID
		WHERE CustomerID = @CustomerIDToBeRestored
		AND Department.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Department', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DroppedOutCustomers
		SET ClientID = @ClientID
		WHERE CustomerID = @CustomerIDToBeRestored
		AND DroppedOutCustomers.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DroppedOutCustomers', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE Bill
		SET ClientID = @ClientID, LeadID = a.LeadID, CustomerID = a.CustomerID
		FROM Bill
		INNER JOIN ArchivedObjects a ON (Bill.CustomerID = a.CustomerID OR a.LeadID = Bill.LeadID OR a.CaseID = Bill.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND Bill.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Bill', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE WorkflowTaskCompleted
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM WorkflowTaskCompleted
		INNER JOIN ArchivedObjects a ON (a.LeadID = WorkflowTaskCompleted.LeadID OR WorkflowTaskCompleted.CaseID = a.CaseID) 
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND WorkflowTaskCompleted.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('WorkflowTaskCompleted', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValuesHistory
		SET ClientID = @ClientID 
		FROM TableDetailValuesHistory
		WHERE CustomerID = @CustomerIDToBeRestored
		AND TableDetailValuesHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValuesHistory (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValuesHistory
		SET ClientID = @ClientID
		FROM TableDetailValuesHistory
		INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.LeadID = a.LeadID AND (TableDetailValuesHistory.MatterID IS NULL OR TableDetailValuesHistory.MatterID = 0))
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableDetailValuesHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValuesHistory (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValuesHistory
		SET ClientID = @ClientID
		FROM TableDetailValuesHistory
		INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.CaseID = a.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableDetailValuesHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValuesHistory (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValuesHistory
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM TableDetailValuesHistory
		INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.MatterID = a.MatterID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableDetailValuesHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValuesHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValues
		SET ClientID = @ClientID
		FROM TableDetailValues 
		WHERE CustomerID = @CustomerIDToBeRestored
		AND TableDetailValues.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValues (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValues
		SET ClientID = @ClientID
		FROM TableDetailValues
		INNER JOIN ArchivedObjects a ON (TableDetailValues.LeadID = a.LeadID AND (TableDetailValues.MatterID = 0 OR TableDetailValues.MatterID IS NULL))
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableDetailValues.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValues (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValues
		SET ClientID = @ClientID
		FROM TableDetailValues
		INNER JOIN ArchivedObjects a ON (TableDetailValues.CaseID = a.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableDetailValues.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValues (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE TableDetailValues
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM TableDetailValues
		INNER JOIN ArchivedObjects a ON (TableDetailValues.MatterID = a.MatterID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND TableDetailValues.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('TableDetailValues (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE RPIClaimStatus
		SET ClientID = @ClientID
		FROM RPIClaimStatus
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON RPIClaimStatus.MatterID = a.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND RPIClaimStatus.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('RPIClaimStatus', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE QuillCustomerMapping
		SET ClientID = @ClientID
		FROM QuillCustomerMapping
		WHERE CustomerID = @CustomerIDToBeRestored
		AND QuillCustomerMapping.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('QuillCustomerMapping', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE QuillCaseMapping
		SET ClientID = @ClientID
		FROM QuillCaseMapping
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON QuillCaseMapping.CaseID = a.CaseID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND QuillCaseMapping.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('QuillCaseMapping', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE OutcomeDelayedEmails
		SET ClientID = @ClientID
		FROM OutcomeDelayedEmails 
		WHERE CustomerID = @CustomerIDToBeRestored
		AND OutcomeDelayedEmails.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('OutcomeDelayedEmails', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE PartnerMatchKey
		SET ClientID = @ClientID
		FROM PartnerMatchKey 
		INNER JOIN Partner p ON p.PartnerID = PartnerMatchKey.PartnerID
		WHERE p.CustomerID = @CustomerIDToBeRestored
		AND PartnerMatchKey.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('PartnerMatchKey', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE Partner
		SET ClientID = @ClientID
		FROM Partner 
		WHERE CustomerID = @CustomerIDToBeRestored
		AND Partner.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Partner', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE PortalUser
		SET ClientID = @ClientID, Enabled = 1
		FROM PortalUser
		WHERE CustomerID = @CustomerIDToBeRestored
		AND PortalUser.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('PortalUser', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE MatterPageChoice 
		SET ClientID = @ClientID
		FROM MatterPageChoice
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = MatterPageChoice.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND MatterPageChoice.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('MatterPageChoice', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update Matterdetailvalues.*/
		UPDATE mdv
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM MatterDetailValues mdv
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = mdv.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND mdv.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('MatterDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE Matter
		SET ClientID = @ClientID, Matter.CaseID = a.CaseID, Matter.LeadID = a.LeadID, CustomerID = a.CustomerID
		FROM Matter
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = Matter.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND Matter.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Matter', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadViewHistoryArchive
		SET ClientID = @ClientID
		FROM LeadViewHistoryArchive
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadViewHistoryArchive.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadViewHistoryArchive.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadViewHistoryArchive', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadViewHistory
		SET ClientID = @ClientID
		FROM LeadViewHistory
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadViewHistory.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadViewHistory.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadViewHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadDetailValues
		SET ClientID = @ClientID
		FROM LeadDetailValues
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadDetailValues.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadDetailValues.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadAssignment 
		SET ClientID = @ClientID
		FROM LeadAssignment
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadAssignment.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadAssignment.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadAssignment', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE HskLeadEventToDelete
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM HskLeadEventToDelete
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = HskLeadEventToDelete.LeadID OR a.CaseID = HskLeadEventToDelete.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND HskLeadEventToDelete.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('HskLeadEventToDelete', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		-- Set context info to allow access to table
		SELECT @ContextInfo = CAST('DPADelete' AS VARBINARY)
		SET CONTEXT_INFO @ContextInfo

		UPDATE LeadEvent 
		SET ClientID = @ClientID, LeadID = a.LeadID 
		FROM LeadEvent 
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = LeadEvent.LeadID OR a.CaseID = LeadEvent.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadEvent.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		SET CONTEXT_INFO 0x0

		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadEvent', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE FileExportEvent
		SET ClientID = @ClientID
		FROM FileExportEvent
		INNER JOIN LeadEvent le ON le.LeadEventID = FileExportEvent.LeadEventID
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND FileExportEvent.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('FileExportEvent', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadDocumentFS
		SET ClientID = @ClientID, LeadID = a.LeadID 
		FROM LeadDocumentFS
		INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentFS.LeadDocumentID
		INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadDocumentFS.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadDocumentFS', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadDocumentEsignatureStatus 
		SET ClientID = @ClientID
		FROM LeadDocumentEsignatureStatus 
		INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentEsignatureStatus.LeadDocumentID
		INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadDocumentEsignatureStatus.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadDocumentEsignatureStatus', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE LeadEventThreadCompletion
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM LeadEventThreadCompletion
		INNER JOIN LeadEvent le ON le.LeadEventID = LeadEventThreadCompletion.FromLeadEventID
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadEventThreadCompletion.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadEventThreadCompletion', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DroppedOutCustomerAnswers
		SET ClientID = @ClientID
		FROM DroppedOutCustomerAnswers
		INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = DroppedOutCustomerAnswers.CustomerQuestionnaireID
		WHERE cq.CustomerID = @CustomerIDToBeRestored
		AND DroppedOutCustomerAnswers.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DroppedOutCustomerAnswers', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DroppedOutCustomerQuestionnaires
		SET ClientID = @ClientID
		FROM DroppedOutCustomerQuestionnaires
		INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = DroppedOutCustomerQuestionnaires.CustomerQuestionnaireID
		WHERE cq.CustomerID = @CustomerIDToBeRestored
		AND DroppedOutCustomerQuestionnaires.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DroppedOutCustomerQuestionnaires', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DocumentBatchDetail
		SET ClientID = @ClientID
		FROM DocumentBatchDetail
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DocumentBatchDetail.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND DocumentBatchDetail.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DocumentBatchDetail', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DiaryAppointment 
		SET ClientID = @ClientID, LeadID = a.LeadID
		FROM DiaryAppointment 
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = DiaryAppointment.LeadID or a.CaseID = DiaryAppointment.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND DiaryAppointment.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DiaryAppointment', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DiaryReminder
		SET ClientID = @ClientID
		FROM DiaryReminder
		INNER JOIN DiaryAppointment d on d.DiaryAppointmentID = DiaryReminder.DiaryAppointmentID
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = d.LeadID 
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND DiaryReminder.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DiaryReminder', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailValueHistory
		SET ClientID = @ClientID
		FROM DetailValueHistory
		WHERE CustomerID = @CustomerIDToBeRestored
		AND DetailValueHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailValueHistory
		SET ClientID = @ClientID
		FROM DetailValueHistory
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = DetailValueHistory.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND DetailValueHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailValueHistory
		SET ClientID = @ClientID
		FROM DetailValueHistory
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = DetailValueHistory.CaseID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND DetailValueHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailValueHistory
		SET ClientID = @ClientID
		FROM DetailValueHistory
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailValueHistory.MatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND DetailValueHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailFieldLock 
		SET ClientID = @ClientID
		FROM DetailFieldLock
		WHERE DetailFieldLock.LeadOrMatterID = @CustomerIDToBeRestored
		AND LeadOrMatter = 10
		AND DetailFieldLock.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailFieldLock 
		SET ClientID = @ClientID
		FROM DetailFieldLock
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = DetailFieldLock.LeadOrMatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadOrMatter = 1
		AND DetailFieldLock.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailFieldLock 
		SET ClientID = @ClientID
		FROM DetailFieldLock
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = DetailFieldLock.LeadOrMatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadOrMatter = 11
		AND DetailFieldLock.ClientID = 20
			
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE DetailFieldLock 
		SET ClientID = @ClientID
		FROM DetailFieldLock
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailFieldLock.LeadOrMatterID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND LeadOrMatter = 2
		AND DetailFieldLock.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerOutcomes
		SET ClientID = @ClientID
		FROM CustomerOutcomes 
		WHERE CustomerID = @CustomerIDToBeRestored
		AND CustomerOutcomes.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerOutcomes', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerOffice
		SET ClientID = @ClientID
		FROM CustomerOffice
		WHERE CustomerID = @CustomerIDToBeRestored
		AND CustomerOffice.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerOffice', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerMatchKey
		SET ClientID = @ClientID
		FROM CustomerMatchKey
		WHERE CustomerID = @CustomerIDToBeRestored
		AND CustomerMatchKey.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerMatchKey', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerHistory
		SET ClientID = @ClientID, ChangeNotes = 'Undelete For DPA'
		FROM CustomerHistory
		WHERE CustomerID = @CustomerIDToBeRestored
		AND CustomerHistory.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerDetailValues
		SET ClientID = @ClientID
		FROM CustomerDetailValues 
		WHERE CustomerID = @CustomerIDToBeRestored
		AND CustomerDetailValues.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE CustomerAnswers
		SET ClientID = @ClientID
		FROM CustomerAnswers
		INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = CustomerAnswers.CustomerQuestionnaireID
		WHERE cq.CustomerID = @CustomerIDToBeRestored
		AND CustomerAnswers.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerAnswers', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE ContactDetailValues
		SET ClientID = @ClientID
		FROM ContactDetailValues
		INNER JOIN Contact c on c.ContactID = ContactDetailValues.ContactID 
		WHERE c.CustomerID = @CustomerIDToBeRestored
		AND ContactDetailValues.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ContactDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE col
		SET ClientID = @ClientID
		FROM ClientOfficeLead col
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = col.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND col.ClientID = 20
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientOfficeLead', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE cpf
		SET ClientID = @ClientID
		FROM ClientPersonnelFavourite cpf
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = cpf.LeadID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND cpf.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientPersonnelFavourite', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE ClientCustomerMails
		SET ClientID = @ClientID
		FROM ClientCustomerMails
		WHERE CustomerID = @CustomerIDToBeRestored
		AND ClientCustomerMails.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientCustomerMails', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE ctm
		SET ClientID = @ClientID, CustomerID = a.CustomerID, LeadID = a.LeadID, CaseID = a.CaseID
		FROM CaseTransferMapping ctm
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.CustomerID = ctm.CustomerID OR ctm.LeadID = a.LeadID OR ctm.CaseID = a.CaseID OR ctm.MatterID = a.MatterID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND ctm.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CaseTransferMapping', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE cdv
		SET cdv.ClientID = @ClientID /*You would never need to update the CaseID here..*/
		FROM CaseDetailValues cdv WITH (NOLOCK)
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = cdv.CaseID
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND cdv.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CaseDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE aeq
		SET ClientID = @ClientID, CaseID = a.CaseID, LeadID = a.LeadID, CustomerID = a.CustomerID
		FROM AutomatedEventQueue aeq
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (aeq.CustomerID = a.CustomerID OR aeq.LeadID = a.LeadID OR aeq.CaseID = a.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND aeq.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('AutomatedEventQueue', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE puc
		SET ClientID = @ClientID, CaseID = a.CaseID
		FROM PortalUserCase puc
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (puc.LeadID = a.LeadID OR puc.CaseID = a.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND puc.ClientID = 20
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('PortalUserCase', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE cpa
		SET ClientID = @ClientID, LeadID = a.LeadID, CaseID = a.CaseID
		FROM ClientPersonnelAccess cpa
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (cpa.LeadID = a.LeadID OR cpa.CaseID = a.CaseID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND cpa.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientPersonnelAccess', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE l
		SET ClientID = @ClientID
		FROM Lead l 
		INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (l.LeadID = a.LeadID)
		WHERE a.CustomerID = @CustomerIDToBeRestored
		AND l.ClientID = 20

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Lead', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		UPDATE a
		SET DateRestored = dbo.fn_GetDate_Local(), WhoRestored = SUSER_NAME(), Status = 2
		FROM ArchivedObjects a 
		WHERE a.CustomerID = @CustomerIDToBeRestored

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ArchivedObjects', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL
		
		INSERT INTO @LeadAndCaseIDs (ID1, ID2)
		SELECT a.LeadID, a.CaseID
		FROM ArchivedObjects a WITH (NOLOCK) 
		WHERE a.CustomerID = @CustomerIDToBeRestored
		
		--EXEC LeadDocument__UnDelete @LeadAndCaseIDs, @ClientID

	END
	ELSE
	IF @LeadIDToBeRestored > 0 
	BEGIN
	
		/*
			Ok, here we need to move the Lead, Case and Matter(s)
		*/
		SELECT @ClientID = a.ClientID, @CustomerIDToRestoreTo = a.CustomerID
		FROM ArchivedObjects a WITH (NOLOCK)
		WHERE a.LeadID = @LeadIDToBeRestored
		
		/*
			Here we should determine if the customer was set to test before we un-set it. We will use CustomerHistory to determine this.
		*/
		
		IF (SELECT c.ClientID FROM Customers c WITH (NOLOCK) WHERE c.CustomerID = @CustomerIDToRestoreTo) = 20
		BEGIN 
		
			/*So, it looks like the Customer has not been restored, you cant restore a lead to a customer that hasnt been restored.*/
			RAISERROR( 'You can not restore a lead to a customer that is still on client 20', 16, 1 )
			RETURN /*Leave this SP*/
		
		END
		ELSE
		BEGIN

			UPDATE IncomingPostEvent 
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID
			FROM IncomingPostEvent 
			INNER JOIN ArchivedObjects a ON (a.LeadID = IncomingPostEvent.LeadID OR IncomingPostEvent.CaseID = a.CaseID OR a.MatterID = IncomingPostEvent.MatterID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND IncomingPostEvent.ClientID = 20
			
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('IncomingPostEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE IncomingPostEventValue 
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID
			FROM IncomingPostEventValue 
			INNER JOIN ArchivedObjects a ON (a.CustomerID = IncomingPostEventValue.CustomerID OR a.LeadID = IncomingPostEventValue.LeadID OR IncomingPostEventValue.CaseID = a.CaseID OR a.MatterID = IncomingPostEventValue.MatterID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND IncomingPostEventValue.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('IncomingPostEventValue', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE WorkflowTask
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM WorkflowTask
			INNER JOIN ArchivedObjects a ON (a.LeadID = WorkflowTask.LeadID or a.CaseID = WorkflowTask.CaseID) 
			WHERE a.LeadID = @LeadIDToBeRestored
			AND WorkflowTask.ClientID = 20
		
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('WorkflowTask', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Cases
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM Cases 
			INNER JOIN ArchivedObjects a ON (a.LeadID = Cases.LeadID OR a.LeadID = Cases.LeadID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND Cases.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Cases', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableRows
			SET ClientID = a.ClientID
			FROM TableRows 
			INNER JOIN ArchivedObjects a ON (a.LeadID = TableRows.LeadID AND (TableRows.MatterID = 0 OR TableRows.MatterID IS NULL))
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableRows.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableRows (Lead)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableRows
			SET ClientID = a.ClientID
			FROM TableRows 
			INNER JOIN ArchivedObjects a ON a.CaseID = TableRows.CaseID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableRows.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableRows (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableRows
			SET ClientID = a.ClientID, LeadID = a.LeadID
			FROM TableRows 
			INNER JOIN ArchivedObjects a ON a.MatterID = TableRows.MatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableRows.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableRows (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Bill
			SET ClientID = @ClientID, LeadID = a.LeadID, CustomerID = @CustomerIDToRestoreTo
			FROM Bill
			INNER JOIN ArchivedObjects a ON (a.LeadID = Bill.LeadID OR a.CaseID = Bill.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND Bill.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Bill', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE WorkflowTaskCompleted
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM WorkflowTaskCompleted
			INNER JOIN ArchivedObjects a ON (a.LeadID = WorkflowTaskCompleted.LeadID OR WorkflowTaskCompleted.CaseID = a.CaseID) 
			WHERE a.LeadID = @LeadIDToBeRestored
			AND WorkflowTaskCompleted.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('WorkflowTaskCompleted', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValuesHistory
			SET ClientID = @ClientID
			FROM TableDetailValuesHistory
			INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.LeadID = a.LeadID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableDetailValuesHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValuesHistory (Lead)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValuesHistory
			SET ClientID = @ClientID
			FROM TableDetailValuesHistory
			INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.CaseID = a.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableDetailValuesHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValuesHistory (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValuesHistory
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM TableDetailValuesHistory
			INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.MatterID = a.MatterID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableDetailValuesHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValuesHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValues
			SET ClientID = @ClientID
			FROM TableDetailValues
			INNER JOIN ArchivedObjects a ON (TableDetailValues.LeadID = a.LeadID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableDetailValues.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValues (Lead)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValues
			SET ClientID = @ClientID
			FROM TableDetailValues
			INNER JOIN ArchivedObjects a ON (TableDetailValues.CaseID = a.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableDetailValues.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValues (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValues
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM TableDetailValues
			INNER JOIN ArchivedObjects a ON (TableDetailValues.MatterID = a.MatterID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND TableDetailValues.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValues (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE RPIClaimStatus
			SET ClientID = @ClientID
			FROM RPIClaimStatus
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON RPIClaimStatus.MatterID = a.MatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND RPIClaimStatus.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('RPIClaimStatus', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE QuillCaseMapping
			SET ClientID = @ClientID
			FROM QuillCaseMapping
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON QuillCaseMapping.CaseID = a.CaseID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND QuillCaseMapping.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('QuillCaseMapping', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE MatterPageChoice 
			SET ClientID = @ClientID
			FROM MatterPageChoice
			INNER JOIN Matter m ON m.MatterID = MatterPageChoice.MatterID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = m.MatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND MatterPageChoice.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('MatterPageChoice', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			/*Update Matterdetailvalues.*/
			UPDATE mdv
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM MatterDetailValues mdv
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = mdv.MatterID
			WHERE mdv.ClientID = @ClientID
			AND mdv.ClientID = 20
			
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('MatterDetailValues', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Matter
			SET ClientID = @ClientID, Matter.CaseID = a.CaseID, Matter.LeadID = a.LeadID, CustomerID = @CustomerIDToRestoreTo
			FROM Matter
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = Matter.MatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND Matter.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Matter', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadViewHistoryArchive
			SET ClientID = @ClientID
			FROM LeadViewHistoryArchive
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadViewHistoryArchive.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadViewHistoryArchive.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadViewHistoryArchive', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadViewHistory
			SET ClientID = @ClientID
			FROM LeadViewHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadViewHistory.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadViewHistory.ClientID = 20
			
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadViewHistory', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadDetailValues
			SET ClientID = @ClientID
			FROM LeadDetailValues
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadDetailValues.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadDetailValues.ClientID = 20
			
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadDetailValues', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadAssignment 
			SET ClientID = @ClientID
			FROM LeadAssignment
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = LeadAssignment.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadAssignment.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadAssignment', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE HskLeadEventToDelete
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM HskLeadEventToDelete
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = HskLeadEventToDelete.LeadID OR a.CaseID = HskLeadEventToDelete.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND HskLeadEventToDelete.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('HskLeadEventToDelete', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			-- Set context info to allow access to table
			SELECT @ContextInfo = CAST('DPADelete' AS VARBINARY)
			SET CONTEXT_INFO @ContextInfo

			UPDATE LeadEvent 
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM LeadEvent 
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = LeadEvent.LeadID OR a.CaseID = LeadEvent.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadEvent.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			SET CONTEXT_INFO 0x0

			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE FileExportEvent
			SET ClientID = @ClientID
			FROM FileExportEvent
			INNER JOIN LeadEvent le ON le.LeadEventID = FileExportEvent.LeadEventID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND FileExportEvent.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('FileExportEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadDocumentFS
			SET ClientID = @ClientID, LeadID = a.LeadID 
			FROM LeadDocumentFS
			INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentFS.LeadDocumentID
			INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadDocumentFS.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadDocumentFS', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadDocumentEsignatureStatus 
			SET ClientID = @ClientID
			FROM LeadDocumentEsignatureStatus 
			INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentEsignatureStatus.LeadDocumentID
			INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadDocumentEsignatureStatus.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadDocumentEsignatureStatus', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadEventThreadCompletion
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM LeadEventThreadCompletion
			INNER JOIN LeadEvent le ON le.LeadEventID = LeadEventThreadCompletion.FromLeadEventID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadEventThreadCompletion.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadEventThreadCompletion', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DocumentBatchDetail
			SET ClientID = @ClientID
			FROM DocumentBatchDetail
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DocumentBatchDetail.MatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND DocumentBatchDetail.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DocumentBatchDetail', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DiaryAppointment 
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM DiaryAppointment 
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = DiaryAppointment.LeadID or a.CaseID = DiaryAppointment.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND DiaryAppointment.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DiaryAppointment', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DiaryReminder
			SET ClientID = @ClientID
			FROM DiaryReminder
			INNER JOIN DiaryAppointment d on d.DiaryAppointmentID = DiaryReminder.DiaryAppointmentID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = d.LeadID 
			WHERE a.LeadID = @LeadIDToBeRestored
			AND DiaryReminder.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DiaryReminder', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailValueHistory
			SET ClientID = @ClientID
			FROM DetailValueHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = DetailValueHistory.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND DetailValueHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailValueHistory (Lead)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailValueHistory
			SET ClientID = @ClientID
			FROM DetailValueHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = DetailValueHistory.CaseID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND DetailValueHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailValueHistory (Case)', CONVERT(VARCHAR,@LastRowCount))

			SELECT @LastRowCount = NULL

			UPDATE DetailValueHistory
			SET ClientID = @ClientID
			FROM DetailValueHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailValueHistory.MatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND DetailValueHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailValueHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailFieldLock 
			SET ClientID = @ClientID
			FROM DetailFieldLock
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = DetailFieldLock.LeadOrMatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadOrMatter = 1
			AND DetailFieldLock.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailFieldLock', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailFieldLock 
			SET ClientID = @ClientID
			FROM DetailFieldLock
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = DetailFieldLock.LeadOrMatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadOrMatter = 11
			AND DetailFieldLock.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailFieldLock', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailFieldLock 
			SET ClientID = @ClientID
			FROM DetailFieldLock
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailFieldLock.LeadOrMatterID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND LeadOrMatter = 2
			AND DetailFieldLock.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailFieldLock', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE col
			SET ClientID = @ClientID
			FROM ClientOfficeLead col
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = col.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND col.ClientID = 20
		
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ClientOfficeLead', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE cpf
			SET ClientID = @ClientID
			FROM ClientPersonnelFavourite cpf
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = cpf.LeadID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND cpf.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ClientPersonnelFavourite', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE ctm
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID, CaseID = a.CaseID
			FROM CaseTransferMapping ctm
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.CustomerID = ctm.CustomerID OR ctm.LeadID = a.LeadID OR ctm.CaseID = a.CaseID OR ctm.MatterID = a.MatterID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND ctm.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('CaseTransferMapping', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE cdv
			SET cdv.ClientID = @ClientID /*You would never need to update the CaseID here..*/
			FROM CaseDetailValues cdv WITH (NOLOCK)
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = cdv.CaseID
			WHERE a.LeadID = @LeadIDToBeRestored
			AND cdv.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('CaseDetailValues', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE aeq
			SET ClientID = @ClientID, CaseID = a.CaseID, LeadID = a.LeadID, CustomerID = @CustomerIDToRestoreTo
			FROM AutomatedEventQueue aeq
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (aeq.CustomerID = a.CustomerID OR aeq.LeadID = a.LeadID OR aeq.CaseID = a.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND aeq.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('AutomatedEventQueue', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE puc
			SET ClientID = @ClientID, CaseID = a.CaseID
			FROM PortalUserCase puc
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (puc.LeadID = a.LeadID OR puc.CaseID = a.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND puc.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('PortalUserCase', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE cpa
			SET ClientID = @ClientID, LeadID = a.LeadID, CaseID = a.CaseID
			FROM ClientPersonnelAccess cpa
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (cpa.LeadID = a.LeadID OR cpa.CaseID = a.CaseID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND cpa.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ClientPersonnelAccess', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE l
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo
			FROM Lead l 
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (l.LeadID = a.LeadID)
			WHERE a.LeadID = @LeadIDToBeRestored
			AND l.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Lead', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE a
			SET DateRestored = dbo.fn_GetDate_Local(), WhoRestored = SUSER_NAME(), Status = 2
			FROM ArchivedObjects a 
			WHERE a.LeadID = @LeadIDToBeRestored

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ArchivedObjects', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			INSERT INTO @LeadAndCaseIDs (ID1, ID2)
			SELECT a.LeadID, a.CaseID
			FROM ArchivedObjects a WITH (NOLOCK) 
			WHERE a.LeadID = @LeadIDToBeRestored
			
			--EXEC LeadDocument__UnDelete @LeadAndCaseIDs, @ClientID

		END
	
	END
	IF @CaseIDToBeRestored > 0 
	BEGIN

		/*
			Ok, here we need to move the Lead, Case and Matter(s)
		*/
		SELECT @ClientID = a.ClientID, @CustomerIDToRestoreTo = a.CustomerID, @LeadIDToRestoredTo = a.LeadID
		FROM ArchivedObjects a WITH (NOLOCK)
		WHERE a.CaseID = @CaseIDToBeRestored
		
		/*
			Here we should determine if the customer was set to test before we un-set it. We will use CustomerHistory to determine this.
		*/
		
		IF (SELECT l.ClientID FROM Lead l WITH (NOLOCK) WHERE l.LeadID = @LeadIDToRestoredTo) = 20
		BEGIN 
		
			/*So, it looks like the Lead has not been restored, you cant restore a Case to a Lead that hasnt been restored.*/
			RAISERROR( 'You can not restore a Case to a Lead that is still on Client 20', 16, 1 )
			RETURN /*Leave this SP*/
		
		END
		ELSE
		BEGIN

			UPDATE IncomingPostEvent 
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID
			FROM IncomingPostEvent 
			INNER JOIN ArchivedObjects a ON (a.LeadID = IncomingPostEvent.LeadID OR IncomingPostEvent.CaseID = a.CaseID OR a.MatterID = IncomingPostEvent.MatterID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND IncomingPostEvent.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('IncomingPostEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE IncomingPostEventValue 
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID
			FROM IncomingPostEventValue 
			INNER JOIN ArchivedObjects a ON (a.CustomerID = IncomingPostEventValue.CustomerID OR a.LeadID = IncomingPostEventValue.LeadID OR IncomingPostEventValue.CaseID = a.CaseID OR a.MatterID = IncomingPostEventValue.MatterID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND IncomingPostEventValue.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('IncomingPostEventValue', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE WorkflowTask
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM WorkflowTask
			INNER JOIN ArchivedObjects a ON (a.LeadID = WorkflowTask.LeadID or a.CaseID = WorkflowTask.CaseID) 
			WHERE a.CaseID = @CaseIDToBeRestored
			AND WorkflowTask.ClientID = 20
		
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('WorkflowTask', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Cases
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM Cases 
			INNER JOIN ArchivedObjects a ON (a.LeadID = Cases.LeadID OR a.CaseID = Cases.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND Cases.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Cases', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableRows
			SET ClientID = a.ClientID
			FROM TableRows 
			INNER JOIN ArchivedObjects a ON a.CaseID = TableRows.CaseID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND TableRows.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableRows (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableRows
			SET ClientID = a.ClientID, LeadID = a.LeadID
			FROM TableRows 
			INNER JOIN ArchivedObjects a ON a.MatterID = TableRows.MatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND TableRows.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableRows (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Bill
			SET ClientID = @ClientID, LeadID = a.LeadID, CustomerID = @CustomerIDToRestoreTo
			FROM Bill
			INNER JOIN ArchivedObjects a ON (a.LeadID = Bill.LeadID OR a.CaseID = Bill.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND Bill.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Bill', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE WorkflowTaskCompleted
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM WorkflowTaskCompleted
			INNER JOIN ArchivedObjects a ON (a.LeadID = WorkflowTaskCompleted.LeadID OR WorkflowTaskCompleted.CaseID = a.CaseID) 
			WHERE a.CaseID = @CaseIDToBeRestored
			AND WorkflowTaskCompleted.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('WorkflowTaskCompleted', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValuesHistory
			SET ClientID = @ClientID
			FROM TableDetailValuesHistory
			INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.CaseID = a.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND TableDetailValuesHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValuesHistory (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValuesHistory
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM TableDetailValuesHistory
			INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.MatterID = a.MatterID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND TableDetailValuesHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValuesHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValues
			SET ClientID = @ClientID
			FROM TableDetailValues
			INNER JOIN ArchivedObjects a ON (TableDetailValues.CaseID = a.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND TableDetailValues.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValues (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValues
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM TableDetailValues
			INNER JOIN ArchivedObjects a ON (TableDetailValues.MatterID = a.MatterID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND TableDetailValues.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValues (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE RPIClaimStatus
			SET ClientID = @ClientID
			FROM RPIClaimStatus
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON RPIClaimStatus.MatterID = a.MatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND RPIClaimStatus.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('RPIClaimStatus', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE QuillCaseMapping
			SET ClientID = @ClientID
			FROM QuillCaseMapping
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON QuillCaseMapping.CaseID = a.CaseID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND QuillCaseMapping.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('QuillCaseMapping', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE MatterPageChoice 
			SET ClientID = @ClientID
			FROM MatterPageChoice
			INNER JOIN Matter m ON m.MatterID = MatterPageChoice.MatterID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = m.MatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND MatterPageChoice.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('MatterPageChoice', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			/*Update Matterdetailvalues.*/
			UPDATE mdv
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM MatterDetailValues mdv
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = mdv.MatterID
			WHERE mdv.ClientID = @ClientID
			AND mdv.ClientID = 20
			
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('MatterDetailValues', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Matter
			SET ClientID = @ClientID, Matter.CaseID = a.CaseID, Matter.LeadID = a.LeadID, CustomerID = @CustomerIDToRestoreTo
			FROM Matter
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = Matter.MatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND Matter.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Matter', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE HskLeadEventToDelete
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM HskLeadEventToDelete
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = HskLeadEventToDelete.LeadID OR a.CaseID = HskLeadEventToDelete.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND HskLeadEventToDelete.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('HskLeadEventToDelete', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			-- Set context info to allow access to table
			SELECT @ContextInfo = CAST('DPADelete' AS VARBINARY)
			SET CONTEXT_INFO @ContextInfo

			UPDATE LeadEvent 
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM LeadEvent 
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = LeadEvent.LeadID OR a.CaseID = LeadEvent.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND LeadEvent.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			SET CONTEXT_INFO 0x0

			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE FileExportEvent
			SET ClientID = @ClientID
			FROM FileExportEvent
			INNER JOIN LeadEvent le ON le.LeadEventID = FileExportEvent.LeadEventID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND FileExportEvent.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('FileExportEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadDocumentFS
			SET ClientID = @ClientID, LeadID = a.LeadID 
			FROM LeadDocumentFS
			INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentFS.LeadDocumentID
			INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND LeadDocumentFS.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadDocumentFS', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadDocumentEsignatureStatus 
			SET ClientID = @ClientID
			FROM LeadDocumentEsignatureStatus 
			INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentEsignatureStatus.LeadDocumentID
			INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND LeadDocumentEsignatureStatus.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadDocumentEsignatureStatus', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE LeadEventThreadCompletion
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM LeadEventThreadCompletion
			INNER JOIN LeadEvent le ON le.LeadEventID = LeadEventThreadCompletion.FromLeadEventID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = le.LeadID OR a.CaseID = le.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND LeadEventThreadCompletion.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('LeadEventThreadCompletion', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DocumentBatchDetail
			SET ClientID = @ClientID
			FROM DocumentBatchDetail
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DocumentBatchDetail.MatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND DocumentBatchDetail.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DocumentBatchDetail', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DiaryAppointment 
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM DiaryAppointment 
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.LeadID = DiaryAppointment.LeadID or a.CaseID = DiaryAppointment.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND DiaryAppointment.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DiaryAppointment', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DiaryReminder
			SET ClientID = @ClientID
			FROM DiaryReminder
			INNER JOIN DiaryAppointment d on d.DiaryAppointmentID = DiaryReminder.DiaryAppointmentID
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.LeadID = d.LeadID 
			WHERE a.CaseID = @CaseIDToBeRestored
			AND DiaryReminder.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DiaryReminder', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailValueHistory
			SET ClientID = @ClientID
			FROM DetailValueHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = DetailValueHistory.CaseID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND DetailValueHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailValueHistory (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailValueHistory
			SET ClientID = @ClientID
			FROM DetailValueHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailValueHistory.MatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND DetailValueHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailValueHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailFieldLock 
			SET ClientID = @ClientID
			FROM DetailFieldLock
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = DetailFieldLock.LeadOrMatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND LeadOrMatter = 11
			AND DetailFieldLock.ClientID = 20
				
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailFieldLock (Case)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailFieldLock 
			SET ClientID = @ClientID
			FROM DetailFieldLock
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailFieldLock.LeadOrMatterID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND LeadOrMatter = 2
			AND DetailFieldLock.ClientID = 20
		
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailFieldLock (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE ctm
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID, CaseID = a.CaseID
			FROM CaseTransferMapping ctm
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.CustomerID = ctm.CustomerID OR ctm.LeadID = a.LeadID OR ctm.CaseID = a.CaseID OR ctm.MatterID = a.MatterID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND ctm.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('CaseTransferMapping', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE cdv
			SET cdv.ClientID = @ClientID /*You would never need to update the CaseID here..*/
			FROM CaseDetailValues cdv WITH (NOLOCK)
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.CaseID = cdv.CaseID
			WHERE a.CaseID = @CaseIDToBeRestored
			AND cdv.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('CaseDetailValues', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE aeq
			SET ClientID = @ClientID, CaseID = a.CaseID, LeadID = a.LeadID, CustomerID = @CustomerIDToRestoreTo
			FROM AutomatedEventQueue aeq
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (aeq.CustomerID = a.CustomerID OR aeq.LeadID = a.LeadID OR aeq.CaseID = a.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND aeq.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('AutomatedEventQueue', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE puc
			SET ClientID = @ClientID, CaseID = a.CaseID
			FROM PortalUserCase puc
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (puc.LeadID = a.LeadID OR puc.CaseID = a.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND puc.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('PortalUserCase', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE cpa
			SET ClientID = @ClientID, LeadID = a.LeadID, CaseID = a.CaseID
			FROM ClientPersonnelAccess cpa
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (cpa.LeadID = a.LeadID OR cpa.CaseID = a.CaseID)
			WHERE a.CaseID = @CaseIDToBeRestored
			AND cpa.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ClientPersonnelAccess', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE a
			SET DateRestored = dbo.fn_GetDate_Local(), WhoRestored = SUSER_NAME(), Status = 2
			FROM ArchivedObjects a 
			WHERE a.CaseID = @CaseIDToBeRestored

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ArchivedObjects', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			INSERT INTO @LeadAndCaseIDs (ID1, ID2)
			SELECT a.LeadID, a.CaseID
			FROM ArchivedObjects a WITH (NOLOCK) 
			WHERE a.CaseID = @CaseIDToBeRestored
			
			--EXEC LeadDocument__UnDelete @LeadAndCaseIDs, @ClientID

		END

	END
	ELSE
	IF @MatterIDToBeRestored > 0 
	BEGIN

		/*
			Ok, here we need to move the Matter
		*/
		SELECT @ClientID = a.ClientID, @CustomerIDToRestoreTo = a.CustomerID, @LeadIDToRestoredTo = a.LeadID, @CaseIDToRestoreTo = a.CaseID
		FROM ArchivedObjects a WITH (NOLOCK)
		WHERE a.MatterID = @MatterIDToBeRestored
		
		/*
			Here we should determine if the customer was set to test before we un-set it. We will use CustomerHistory to determine this.
		*/
		
		IF (SELECT c.ClientID FROM Cases c WITH (NOLOCK) WHERE c.CaseID = @CaseIDToRestoreTo) = 20
		BEGIN 
		
			/*So, it looks like the Case has not been restored, you cant restore a Matter to a Case that hasnt been restored.*/
			RAISERROR( 'You can not restore a Case to a Lead that is still on Client 20', 16, 1 )
			RETURN /*Leave this SP*/
		
		END
		ELSE
		BEGIN
		
			/*Do work here*/
			UPDATE IncomingPostEvent 
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID, CaseID = a.CaseID
			FROM IncomingPostEvent 
			INNER JOIN ArchivedObjects a ON (a.LeadID = IncomingPostEvent.LeadID OR IncomingPostEvent.CaseID = a.CaseID OR a.MatterID = IncomingPostEvent.MatterID)
			WHERE a.MatterID = @MatterIDToBeRestored
			AND IncomingPostEvent.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('IncomingPostEvent', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE IncomingPostEventValue 
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID, CaseID = a.CaseID
			FROM IncomingPostEventValue 
			INNER JOIN ArchivedObjects a ON (a.CustomerID = IncomingPostEventValue.CustomerID OR a.LeadID = IncomingPostEventValue.LeadID OR IncomingPostEventValue.CaseID = a.CaseID OR a.MatterID = IncomingPostEventValue.MatterID)
			WHERE a.MatterID = @MatterIDToBeRestored
			AND IncomingPostEventValue.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('IncomingPostEventValue', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableRows
			SET ClientID = a.ClientID, LeadID = a.LeadID
			FROM TableRows 
			INNER JOIN ArchivedObjects a ON a.MatterID = TableRows.MatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND TableRows.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableRows (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValuesHistory
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM TableDetailValuesHistory
			INNER JOIN ArchivedObjects a ON (TableDetailValuesHistory.MatterID = a.MatterID)
			WHERE a.MatterID = @MatterIDToBeRestored
			AND TableDetailValuesHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValuesHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE TableDetailValues
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM TableDetailValues
			INNER JOIN ArchivedObjects a ON (TableDetailValues.MatterID = a.MatterID)
			WHERE a.MatterID = @MatterIDToBeRestored
			AND TableDetailValues.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('TableDetailValues (Matter)', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE RPIClaimStatus
			SET ClientID = @ClientID
			FROM RPIClaimStatus
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON RPIClaimStatus.MatterID = a.MatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND RPIClaimStatus.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('RPIClaimStatus', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE MatterPageChoice 
			SET ClientID = @ClientID
			FROM MatterPageChoice
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = MatterPageChoice.MatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND MatterPageChoice.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('MatterPageChoice', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			/*Update Matterdetailvalues.*/
			UPDATE mdv
			SET ClientID = @ClientID, LeadID = a.LeadID
			FROM MatterDetailValues mdv
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = mdv.MatterID
			WHERE mdv.ClientID = @ClientID
			AND mdv.ClientID = 20
			
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('MatterDetailValues', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE Matter
			SET ClientID = @ClientID, Matter.CaseID = a.CaseID, Matter.LeadID = a.LeadID, CustomerID = a.CustomerID
			FROM Matter
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = Matter.MatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND Matter.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Matter', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DocumentBatchDetail
			SET ClientID = @ClientID
			FROM DocumentBatchDetail
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DocumentBatchDetail.MatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND DocumentBatchDetail.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DocumentBatchDetail', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailValueHistory
			SET ClientID = @ClientID
			FROM DetailValueHistory
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailValueHistory.MatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND DetailValueHistory.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailValueHistory', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE DetailFieldLock 
			SET ClientID = @ClientID
			FROM DetailFieldLock
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON a.MatterID = DetailFieldLock.LeadOrMatterID
			WHERE a.MatterID = @MatterIDToBeRestored
			AND LeadOrMatter = 2
			AND DetailFieldLock.ClientID = 20
		
			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('DetailFieldLock', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE ctm
			SET ClientID = @ClientID, CustomerID = @CustomerIDToRestoreTo, LeadID = a.LeadID, CaseID = a.CaseID
			FROM CaseTransferMapping ctm
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (a.CustomerID = ctm.CustomerID OR ctm.LeadID = a.LeadID OR ctm.CaseID = a.CaseID OR ctm.MatterID = a.MatterID)
			WHERE a.MatterID = @MatterIDToBeRestored
			AND ctm.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('CaseTransferMapping', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE cpa
			SET ClientID = @ClientID, LeadID = a.LeadID, CaseID = a.CaseID
			FROM ClientPersonnelAccess cpa
			INNER JOIN ArchivedObjects a WITH (NOLOCK) ON (cpa.LeadID = a.LeadID OR cpa.CaseID = a.CaseID)
			WHERE a.MatterID = @MatterIDToBeRestored
			AND cpa.ClientID = 20

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ClientPersonnelAccess', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

			UPDATE a
			SET DateRestored = dbo.fn_GetDate_Local(), WhoRestored = SUSER_NAME(), Status = 2
			FROM ArchivedObjects a 
			WHERE a.MatterID = @MatterIDToBeRestored

			SELECT @LastRowCount = @@ROWCOUNT
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('ArchivedObjects', CONVERT(VARCHAR,@LastRowCount))
			
			SELECT @LastRowCount = NULL

		END
		
	END
	
	SELECT *
	FROM @LogOutput

	SELECT @LogXmlEntry = 
	(
		SELECT 	CONVERT(VARCHAR,@ObjectID) AS [ObjectID],
				CONVERT(VARCHAR,@ObjectLevel) AS [ObjectLevel],
				CONVERT(VARCHAR,dbo.fn_GetDate_Local(),120) [DateTime],
				SUSER_NAME() AS [UsersName], 
					CAST ( ( SELECT * FROM @LogOutput FOR XML RAW ('CaseList') ) as XML )
		FOR XML RAW ('Values')
	)

	INSERT INTO LogXML(ClientID, LogDateTime, ContextID, ContextVarchar, LogXMLEntry)
	VALUES (20, dbo.fn_GetDate_Local(), 0, 'DPADelete', CAST(@LogXmlEntry AS XML))

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UndeleteDataForDPA] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UndeleteDataForDPA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UndeleteDataForDPA] TO [sp_executeall]
GO
