SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientAreaMailingLists table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_Insert]
(

	@ClientAreaMailingListID int    OUTPUT,

	@Name varchar (200)  ,

	@Email varchar (255)  ,

	@PostCode varchar (10)  ,

	@OutcomeID int   ,

	@YellowPagesAreaCode varchar (10)  ,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@OnHold bit   
)
AS


				
				INSERT INTO [dbo].[ClientAreaMailingLists]
					(
					[Name]
					,[Email]
					,[PostCode]
					,[OutcomeID]
					,[YellowPagesAreaCode]
					,[ClientID]
					,[ClientQuestionnaireID]
					,[OnHold]
					)
				VALUES
					(
					@Name
					,@Email
					,@PostCode
					,@OutcomeID
					,@YellowPagesAreaCode
					,@ClientID
					,@ClientQuestionnaireID
					,@OnHold
					)
				-- Get the identity value
				SET @ClientAreaMailingListID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Insert] TO [sp_executeall]
GO
