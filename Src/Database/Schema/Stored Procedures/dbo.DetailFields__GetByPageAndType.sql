SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-11
-- Description:	Get a list of equations (by page) that
--              need to be recalculated when a lead/matter is saved
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetByPageAndType] 
	@DetailFieldPageID int, 
	@QuestionTypeID int = 10, 
	@Enabled bit = 1,
	@ClientID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	df.[DetailFieldID],
	df.[ClientID],
	df.[LeadOrMatter],
	df.[FieldName],
	df.[FieldCaption],
	df.[QuestionTypeID],
	df.[Required],
	df.[Lookup],
	df.[LookupListID],
	df.[LeadTypeID],
	df.[Enabled],
	df.[DetailFieldPageID],
	df.[FieldOrder],
	df.[MaintainHistory],
	df.[EquationText],
	df.[MasterQuestionID],
	df.[FieldSize],
	df.[LinkedDetailFieldID],
	df.[ValidationCriteriaFieldTypeID],
	df.[ValidationCriteriaID],
	df.[MinimumValue],
	df.[MaximumValue],
	df.[RegEx],
	df.[ErrorMessage],
	df.[ResourceListDetailFieldPageID],
	df.[TableDetailFieldPageID],
	df.[DefaultFilter],
	df.[ColumnEquationText],
	df.[Editable],
	df.[Hidden],
	df.[LastReferenceInteger],
	df.[ReferenceValueFormatID],
	df.[Encrypt],
	df.[ShowCharacters],
	df.[NumberOfCharactersToShow],
	df.[TableEditMode],
	df.[DisplayInTableView]
	FROM dbo.fnDetailFieldsShared(@ClientID) df 
	WHERE df.DetailFieldPageID = @DetailFieldPageID 
	AND df.QuestionTypeID = @QuestionTypeID
	AND df.[Enabled] = @Enabled
	ORDER BY df.FieldOrder 

	SELECT @@ROWCOUNT


END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByPageAndType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByPageAndType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByPageAndType] TO [sp_executeall]
GO
