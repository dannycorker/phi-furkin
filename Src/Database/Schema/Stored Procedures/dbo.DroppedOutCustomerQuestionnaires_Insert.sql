SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DroppedOutCustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerQuestionnaires_Insert]
(

	@CustomerQuestionnaireID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@SubmissionDate datetime   ,

	@TrackingID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[DroppedOutCustomerQuestionnaires]
					(
					[ClientQuestionnaireID]
					,[CustomerID]
					,[SubmissionDate]
					,[TrackingID]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@CustomerID
					,@SubmissionDate
					,@TrackingID
					,@ClientID
					)
				-- Get the identity value
				SET @CustomerQuestionnaireID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerQuestionnaires_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_Insert] TO [sp_executeall]
GO
