SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingUserCredential table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential__GetByThirdPartySystemIDAndUserID]
(

	@ThirdPartySystemID int,
	@UserID int
	
)
AS

	SELECT *
	FROM
		[dbo].[ThirdPartyMappingUserCredential]
	WHERE
		[ThirdPartySystemID] = @ThirdPartySystemID AND
		[UserID] = @UserID
		
	SELECT @@ROWCOUNT


GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetByThirdPartySystemIDAndUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential__GetByThirdPartySystemIDAndUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetByThirdPartySystemIDAndUserID] TO [sp_executeall]
GO
