SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_Delete]
(

	@ClientPersonnelID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnel] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Delete] TO [sp_executeall]
GO
