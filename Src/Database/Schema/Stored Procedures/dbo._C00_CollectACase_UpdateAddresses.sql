SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-09-2012
-- Description:	Updates an appointment address
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CollectACase_UpdateAddresses]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT,
	@Address1 VARCHAR(2000),
	@Address2 VARCHAR(2000),
	@Town VARCHAR(2000),
	@County VARCHAR(2000),
	@Postcode VARCHAR(2000)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @FieldAddress1 INT,
		@FieldAddress2 INT,
		@FieldTown INT,
		@FieldCounty INT,
		@FieldPostcode INT,
		@PageFieldID INT,
		@PageID INT
		
		
	SELECT @FieldAddress1=tpfm.ColumnFieldID, @PageFieldID = tpfm.DetailFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=339
	SELECT @PageID=d.DetailFieldPageID FROM DetailFields d WITH (NOLOCK) WHERE d.DetailFieldID = @PageFieldID		
			
	SELECT @FieldAddress2=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=340
	
	SELECT @FieldTown=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=341
	
	SELECT @FieldCounty=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=342
	
	SELECT @FieldPostcode=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=343
	
	UPDATE TableDetailValues SET DetailValue = @Address1
	WHERE TableRowID = @TableRowID AND DetailFieldID = @FieldAddress1 AND CustomerID=@CustomerID AND ClientID=@ClientID
	UPDATE TableDetailValues SET DetailValue = @Address2
	WHERE TableRowID = @TableRowID AND DetailFieldID = @FieldAddress2 AND CustomerID=@CustomerID AND ClientID=@ClientID
	
	UPDATE TableDetailValues SET DetailValue = @Town
	WHERE TableRowID = @TableRowID AND DetailFieldID = @FieldTown AND CustomerID=@CustomerID AND ClientID=@ClientID

	UPDATE TableDetailValues SET DetailValue = @County
	WHERE TableRowID = @TableRowID AND DetailFieldID = @FieldCounty AND CustomerID=@CustomerID AND ClientID=@ClientID
	
	UPDATE TableDetailValues SET DetailValue = @Postcode
	WHERE TableRowID = @TableRowID AND DetailFieldID = @FieldPostcode AND CustomerID=@CustomerID AND ClientID=@ClientID
				
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CollectACase_UpdateAddresses] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CollectACase_UpdateAddresses] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CollectACase_UpdateAddresses] TO [sp_executeall]
GO
