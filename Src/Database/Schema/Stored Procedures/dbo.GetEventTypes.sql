SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE [dbo].[GetEventTypes]

-- New security checks by UserID and LeadTypeID (Jim Green, 2007-10-16)
@ClientID int,
@EventSubtypeID int,
@LeadTypeID int,
@UserID int

 AS

-- Only return Event Types to the web service, that the end user
-- will be able to edit (not just view).
SELECT     ClientID, EventSubtypeID, EventTypeDescription,EventTypeID
FROM         dbo.EventType et 
INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) f ON et.EventTypeID = f.objectid
WHERE     (EventSubtypeID = @EventSubtypeID) 
AND (et.Enabled = 1) 
AND (et.AvailableManually = 1) 
AND (ClientID = @ClientID) 
AND f.RightID > 1 

-- Nearly certain that this never gets called.






GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventTypes] TO [sp_executeall]
GO
