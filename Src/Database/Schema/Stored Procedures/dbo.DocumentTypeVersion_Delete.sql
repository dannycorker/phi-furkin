SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentTypeVersion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeVersion_Delete]
(

	@DocumentTypeVersionID int   
)
AS


				DELETE FROM [dbo].[DocumentTypeVersion] WITH (ROWLOCK) 
				WHERE
					[DocumentTypeVersionID] = @DocumentTypeVersionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion_Delete] TO [sp_executeall]
GO
