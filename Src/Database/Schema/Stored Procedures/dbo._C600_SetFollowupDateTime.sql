SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-08-03
-- Description:	Sets followup time according to RL setting
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SetFollowupDateTime] 
(
	@EventTypeID INT,
	@LeadEventID INT,
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50022656,
--	@DateAdjustmentType INT = 4
	
	DECLARE 
			@ClientID INT,
			@IterationDF INT,
			@TimeQuant INT,
			@TimeUnit INT
			 

	SELECT @ClientID=ClientID FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID			
	
	-- initial select from the follow up date/time configuration RL
	SELECT TOP 1 @TimeUnit=turl.DetailValue, @TimeQuant=tqrl.ValueInt, @IterationDF=idfrl.ValueInt 
	FROM ResourceListDetailValues clrl WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues etrl WITH (NOLOCK) ON clrl.ResourceListID=etrl.ResourceListID
				AND etrl.DetailFieldID=170095 AND etrl.ValueInt=@EventTypeID
	INNER JOIN ResourceListDetailValues turl WITH (NOLOCK) ON turl.ResourceListID=clrl.ResourceListID and turl.DetailFieldID=170096
	INNER JOIN ResourceListDetailValues tqrl WITH (NOLOCK) ON tqrl.ResourceListID=clrl.ResourceListID and tqrl.DetailFieldID=170097
	INNER JOIN ResourceListDetailValues irl WITH (NOLOCK) ON irl.ResourceListID=clrl.ResourceListID and irl.DetailFieldID=170098
	INNER JOIN ResourceListDetailValues idfrl WITH (NOLOCK) ON idfrl.ResourceListID=clrl.ResourceListID and idfrl.DetailFieldID=170108
	WHERE clrl.DetailFieldID=170094 AND clrl.ValueInt=@ClientID

	-- If the follow up date/time depends on the number of iterations ( usually = number of times event has been applied ),
	-- first look up the number of iterations and then select again from the RL
	IF @IterationDF > 0 
	BEGIN
	
		SELECT TOP 1 @TimeUnit=turl.ValueInt, @TimeQuant=tqrl.ValueInt, @IterationDF=idfrl.ValueInt 
		FROM ResourceListDetailValues clrl WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues etrl WITH (NOLOCK) ON clrl.ResourceListID=etrl.ResourceListID
					AND etrl.DetailFieldID=170095 AND etrl.ValueInt=@EventTypeID
		INNER JOIN ResourceListDetailValues turl WITH (NOLOCK) ON turl.ResourceListID=clrl.ResourceListID and turl.DetailFieldID=170096
		INNER JOIN ResourceListDetailValues tqrl WITH (NOLOCK) ON tqrl.ResourceListID=clrl.ResourceListID and tqrl.DetailFieldID=170097
		INNER JOIN ResourceListDetailValues irl WITH (NOLOCK) ON irl.ResourceListID=clrl.ResourceListID and irl.DetailFieldID=170098
		INNER JOIN ResourceListDetailValues idfrl WITH (NOLOCK) ON idfrl.ResourceListID=clrl.ResourceListID and idfrl.DetailFieldID=170108
		INNER JOIN MatterDetailValues imdv WITH (NOLOCK) ON clrl.ValueInt=imdv.ClientID 
			AND idfrl.ValueInt=imdv.DetailFieldID 
			AND irl.ValueInt=imdv.ValueInt
			AND imdv.MatterID=@MatterID
		WHERE clrl.DetailFieldID=170094
			
	END

	UPDATE LeadEvent SET FollowupDateTime=CASE @TimeUnit  
		WHEN 69912 THEN DATEADD(MINUTE,@TimeQuant,dbo.fn_GetDate_Local())
		WHEN 69913 THEN DATEADD(HOUR,@TimeQuant,dbo.fn_GetDate_Local())
		WHEN 69914 THEN DATEADD(DAY,@TimeQuant,dbo.fn_GetDate_Local())
		WHEN 69915 THEN DATEADD(WEEK,@TimeQuant,dbo.fn_GetDate_Local())
		WHEN 69916 THEN DATEADD(MONTH,@TimeQuant,dbo.fn_GetDate_Local())
		END
	WHERE LeadEventID=@LeadEventID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetFollowupDateTime] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SetFollowupDateTime] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetFollowupDateTime] TO [sp_executeall]
GO
