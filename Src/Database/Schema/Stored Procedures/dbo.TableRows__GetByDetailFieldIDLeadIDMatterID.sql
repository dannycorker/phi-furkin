SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 18-06-2009
-- Description:	Get Table Rows and Pivot
-- 2010-11-17 JWG Remove all clauses that we can do without now. Use TableDetailFieldPageID instead of DetailFieldID IN (lists). Only pivot output IF required.
-- 2010-12-12 ACE Added CaseID and CustomerID where clauses
-- 2011-08-01 SB Added field to return the look up list name so filtering works correctly
-- 2011-08-08 PR Moved the Basic field Columns to after the resource list fields.
--			  Fields have to be returned in field order with the resource fields before the basic fields
--            Any other order will break the document parser.
-- 2011-10-28 ACE Replaced ", tr.DenyEdit, tr.DenyDelete" with ", tr.DenyEdit as [DenyDelete], tr.DenyDelete as [DenyEdit]"
--			  Temporaraly as the app was showing the buttons the wrong way around
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID] 
	@DetailFieldID INT, 
	@LeadID INT = 0, 
	@MatterID INT = 0,
	@MatterIDList VARCHAR(MAX) = NULL,
	@PrintOnly BIT = 0,
	@PivotOutput BIT = 1,
	@DetailFieldSubtypeID INT = NULL,
	@CustomerID INT = NULL,
	@CaseID INT = NULL,
	@ClientID INT = NULL,
	@ClientPersonnelID INT = NULL,
	@ContactID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

--declare
--	@DetailFieldID INT = 208203, 
--	@LeadID INT = 0, 
--	@MatterID INT = NULL,
--	@MatterIDList VARCHAR(MAX) = NULL,
--	@PrintOnly BIT = 1,
--	@PivotOutput BIT = 1,
--	@DetailFieldSubtypeID INT = NULL,
--	@CustomerID INT = 2573044 ,
--	@CaseID INT = NULL,
--	@ClientID INT = 308,
--	@ClientPersonnelID INT = NULL,
--	@ContactID INT = NULL

	DECLARE @cols VARCHAR(MAX),
			@fieldIdents VARCHAR(MAX),
			@rlfieldIdents VARCHAR(MAX),
			@query VARCHAR(MAX) = '',
			@rlQuery VARCHAR(MAX) = '',
			@llQuery VARCHAR(MAX) = '',
			@rlllQuery VARCHAR(MAX) = '',
			@TableDetailFieldPageID INT,
			@basicQuery VARCHAR(MAX),
			@ResourceTable INT,
			@ResourceListDetailFieldPageID INT,
			@ResourceListDetailFieldID INT,
			@pvtStatement VARCHAR(MAX) = '',
			@queryheader VARCHAR(MAX) = '',
			@LeadOrMatterClause VARCHAR(MAX),
			@LookupListColumns INT,
			@DFPString varchar(100),
			@DFString varchar(100)

	IF @MatterID > 0
	BEGIN
		SET @LeadOrMatterClause = 'MatterID = ' + CAST(@MatterID AS VARCHAR)
	END
	ELSE
	IF @CaseID > 0
	BEGIN
		SET @LeadOrMatterClause = 'CaseID = ' + CAST(@CaseID AS VARCHAR)
	END
	ELSE
	IF @LeadID > 0
	BEGIN
		SET @LeadOrMatterClause = 'LeadID = ' + CAST(@LeadID AS VARCHAR)
	END
	ELSE
	IF @CustomerID > 0
	BEGIN
		SET @LeadOrMatterClause = 'CustomerID = ' + CAST(@CustomerID AS VARCHAR)
	END
	ELSE
	IF @ClientPersonnelID > 0
	BEGIN
		SET @LeadOrMatterClause = 'ClientPersonnelID = ' + CAST(@ClientPersonnelID AS VARCHAR)
	END
	ELSE
	IF @ContactID > 0
	BEGIN
		SET @LeadOrMatterClause = 'ContactID = ' + CAST(@ContactID AS VARCHAR)
	END
	ELSE
	IF @MatterIDList IS NOT NULL
	BEGIN
		SET @LeadOrMatterClause = 'MatterID IN (' + @MatterIDList + ') '	
	END
	ELSE
	IF @ClientID > 0
	BEGIN
		SET @LeadOrMatterClause = 'ClientID = ' + CAST(@ClientID AS VARCHAR)
	END
/* 
	IF @MatterID > 0
	BEGIN
		SET @LeadOrMatterClause = 'MatterID = ' + CAST(@MatterID as varchar)
	END
	ELSE
	BEGIN
		IF @MatterIDList IS NULL
		BEGIN
			SET @LeadOrMatterClause = 'LeadID = ' + CAST(@LeadID as varchar)
		END
		ELSE
		BEGIN	
			SET @LeadOrMatterClause = 'MatterID IN (' + @MatterIDList + ') '
		END
	END
*/	
	SELECT @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID

	IF @DetailFieldID = 166164
	BEGIN

		/*Quadra Customer table overview for the matter payment table (yes one and only) for that customer*/
		SELECT TOP 1 @LeadOrMatterClause = 'MatterID = ' + CAST(m.MatterID AS VARCHAR), @TableDetailFieldPageID = 12821, @DetailFieldID = 115616
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID and l.LeadTypeID = 887
		WHERE m.CustomerID = @CustomerID
		ORDER BY m.MatterID DESC

	END
	
	IF EXISTS(SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.QuestionTypeID = 14) 
	BEGIN
		SET @ResourceTable = 1
	END
	
	IF EXISTS(SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.QuestionTypeID IN (2, 4)) 
	BEGIN
		SET @LookupListColumns = 1
	END

	-- Client 210 Storage & Removal Tables DCM 24/05/2012
	IF @DetailFieldID IN (151304,  -- Details
							151309, -- Subcontract work
							151306 -- Contents cleaning 
						)
	BEGIN
	
		SELECT TOP 1 @LeadOrMatterClause = 'MatterID = ' + CAST(mc.MatterID AS VARCHAR)
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
		INNER JOIN Lead lc WITH (NOLOCK) ON l.CustomerID=lc.CustomerID AND lc.LeadTypeID=1113
		INNER JOIN Matter mc WITH (NOLOCK) ON lc.LeadID=mc.LeadID
		WHERE m.MatterID=@MatterID
		ORDER BY mc.MatterID
			
		SET @DetailFieldID = CASE @DetailFieldID
					WHEN 151304 THEN 138789	
					WHEN 151309 THEN 138798	
					WHEN 151306 THEN 138791	
					END	
			
	END
		   
	SET @basicQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, ISNULL(tdv.DetailValue, '''') as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + CAST(@TableDetailFieldPageID AS VARCHAR) + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
		WHERE tdv.' + @LeadOrMatterClause 

		

	/* This seems to be done below now
	IF @PivotOutput = 1 
	BEGIN
		SELECT @pvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'
	END	*/

	IF @ResourceTable = 1
	BEGIN

		SELECT @ResourceListDetailFieldPageID = ResourceListDetailFieldPageID, @ResourceListDetailFieldID = DetailFieldID
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
		AND df.QuestionTypeID = 14

		SELECT @cols = COALESCE(@cols + ',[' + FieldCaption + ']', '[' + FieldCaption + ']'),
			   @rlfieldIdents = COALESCE(@rlfieldIdents + ',' + CAST(DetailFieldID AS VARCHAR(50)), CAST(DetailFieldID AS VARCHAR(50)))
		FROM dbo.DetailFields df WITH (NOLOCK)		
		WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
		AND df.Enabled = 1
		ORDER BY FieldOrder

		SET @rlQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, rldv.DetailValue as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID AND df.DetailFieldID IN (' + @rlfieldIdents + ')
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.ResourceListID = rldv.ResourceListID AND tdv.DetailFieldID = ' + CAST(@ResourceListDetailFieldID AS VARCHAR) + ' AND tdv.' + @LeadOrMatterClause + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) 
		
	END 
	
	SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + ']', '[' + df.FieldCaption + ']'),
		   @fieldIdents = COALESCE(@fieldIdents + ',' + CAST(df.DetailFieldID AS VARCHAR(50)), CAST(df.DetailFieldID AS VARCHAR(50)))
	FROM dbo.DetailFields df WITH (NOLOCK)  
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
	AND df.Enabled = 1
	AND df.QuestionTypeID <> 14
	ORDER BY df.FieldOrder 
	
	IF @ResourceTable = 1
	BEGIN
		-- Now handle any lookup lists in resource list
		SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + '__NAME]', '[' + df.FieldCaption + '__NAME]')
		FROM dbo.DetailFields df WITH (NOLOCK)		
		WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
		AND df.Enabled = 1
		AND df.QuestionTypeID IN (2, 4)
		ORDER BY df.FieldOrder 
	
		SELECT @rlllQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption + ''__NAME'' AS FieldCaption, ISNULL(ll.ItemValue, '''') as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID AND df.DetailFieldID IN (' + @rlfieldIdents + ')
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.ResourceListID = rldv.ResourceListID AND tdv.DetailFieldID = ' + CAST(@ResourceListDetailFieldID AS VARCHAR) + ' AND tdv.' + @LeadOrMatterClause + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rldv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID
		AND df.QuestionTypeID IN (2, 4)'

	END
	
	IF @LookupListColumns = 1
	BEGIN
	
		SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + '__NAME]', '[' + df.FieldCaption + '__NAME]')
		FROM dbo.DetailFields df WITH (NOLOCK)  
		WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
		AND df.Enabled = 1
		AND df.QuestionTypeID IN (2, 4)
		ORDER BY df.FieldOrder 
	
		SELECT @llQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption + ''__NAME'' AS FieldCaption, ISNULL(ll.ItemValue, '''') as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + CAST(@TableDetailFieldPageID AS VARCHAR) + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON tdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID
		WHERE tdv.' + @LeadOrMatterClause + '
		AND df.QuestionTypeID IN (2, 4)'
	END
	
	
	IF @PivotOutput = 1 
	BEGIN
		SET @queryheader = 'SELECT pvt.TableRowID, ' + @cols + ', pvt.DenyEdit, pvt.DenyDelete FROM ('
	END
	
	IF @rlQuery != ''
	BEGIN
		SELECT @query = @rlQuery
	END
	
	IF @BasicQuery != ''
	BEGIN
		IF @query != ''
		BEGIN
			SELECT @query = @query + '
			 UNION 
			'
		END
		SELECT @query = @query + @BasicQuery	
	END
	
	IF @llQuery != ''
	BEGIN
		IF @query != ''
		BEGIN
			SELECT @query = @query + '
			 UNION 
			'
		END
		SELECT @query = @query + @llQuery	
	END
	
	IF @rlllQuery != ''
	BEGIN
		IF @query != ''
		BEGIN
			SELECT @query = @query + '
			 UNION 
			'
		END
		SELECT @query = @query + @rlllQuery	
	END
	
	
	IF @PivotOutput = 1 BEGIN
		SELECT @PvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'	
	END
	

	SELECT @query = @queryheader + @query + @PvtStatement

	IF @DetailFieldID = 133225
	BEGIN
	
		SELECT @query = @query + ' order by pvt.date desc, pvt.TableRowID DESC'
	
	END

	IF @DetailFieldID = 133498
	BEGIN
	
		SELECT @query = @query + ' order by pvt.[Date From File] desc'
	
	END

	IF @DetailFieldID = 93167
	BEGIN
	
		SELECT @query = @query + ' order by pvt.[Date of Payment to Client] desc'
	
	END

	IF @DetailFieldID IN (113069, 113127, 133233)
	BEGIN
	
		SELECT @query = @query + ' order by pvt.TableRowID DESC'
	
	END
	
	IF @DetailFieldID = 136594 /*C211 - Payments Overview*/
	BEGIN
	
		SELECT @query = 'select ROW_NUMBER() OVER (ORDER BY tdv.ValueDate) as [TableRowID], tdv.ValueDate as [Payment Date], tdv_payee.DetailValue as [Payee], CONVERT(numeric(18,2),SUM(tdv_amount.ValueMoney)) as [Payment Amount], tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
FROM TableDetailValues tdv WITH (NOLOCK)
INNER JOIN TableDetailValues tdv_payee WITH (NOLOCK) on tdv_payee.TableRowID = tdv.TableRowID and tdv_payee.DetailFieldID = 136557
INNER JOIN TableDetailValues tdv_amount WITH (NOLOCK) on tdv_amount.TableRowID = tdv.TableRowID and tdv_amount.DetailFieldID = 136561
INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv.TableRowID
WHERE tdv.MatterID = ' + CONVERT(VARCHAR(100),@MatterID) + '
and tdv.DetailFieldID = 136556
group by tdv.ValueDate, tdv_payee.DetailValue, tr.DenyEdit, tr.DenyDelete'
	
	END
	
	IF @DetailFieldID = 167666 /*C173 - Payments Overview*/
	BEGIN
	
		SELECT @query = 'select ROW_NUMBER() OVER (ORDER BY tdv.ValueDate) as [TableRowID], tdv.ValueDate as [Payment Date], tdv_payee.DetailValue as [Payee], CONVERT(numeric(18,2),SUM(tdv_amount.ValueMoney)) as [Payment Amount], tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
FROM TableDetailValues tdv WITH (NOLOCK)
INNER JOIN TableDetailValues tdv_payee WITH (NOLOCK) on tdv_payee.TableRowID = tdv.TableRowID and tdv_payee.DetailFieldID = 167646
INNER JOIN TableDetailValues tdv_amount WITH (NOLOCK) on tdv_amount.TableRowID = tdv.TableRowID and tdv_amount.DetailFieldID = 167649
INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv.TableRowID
WHERE tdv.MatterID = ' + CONVERT(VARCHAR(100),@MatterID) + '
and tdv.DetailFieldID = 167645
group by tdv.ValueDate, tdv_payee.DetailValue, tr.DenyEdit, tr.DenyDelete'
	
	END
	
	IF @DetailFieldID = 162593 /*C230 - Red Star Event History*/
	BEGIN
	
		SELECT @query = '
		SELECT	le.LeadEventID [TableRowID],
				ISNULL(''Note '' + convert(varchar,nt.NoteTypeID), convert(varchar,et.EventTypeID)) [Red Star EventTypeID],
				ISNULL(nt.NoteTypeName,et.EventTypeName) [Red Star EventTypeName],
				le.Comments [Red Star Comments],
				le.WhenCreated [Red Star WhenCreated],
				''true'' [DenyEdit],
				''true'' [DenyDelete]
		FROM LeadEvent le WITH (NOLOCK) 
		LEFT JOIN EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID
		LEFT JOIN dbo.NoteType nt WITH (NOLOCK) on nt.NoteTypeID = le.NoteTypeID
		INNER JOIN CaseTransferMapping tm WITH (NOLOCK) on tm.NewClientID = 205 and tm.NewCaseID = le.CaseID and tm.CaseID = ' + convert(varchar,@CaseID) + '
		WHERE le.EventDeleted = 0'
	
	END

	IF @DetailFieldID = 161610
	BEGIN
	
		/*IC Phoenix Customer/Case Oveview with Joint Cases*/
	
		SELECT @query = 'select ROW_NUMBER() OVER (ORDER BY (SELECT (1))) as [TableRowID], 
	cu.Fullname + ISNULL('' and '' + p.FullName,'''') as [Customer Names], 
	ISNULL(luli_joint.ItemValue,'''') as [Joint Single],
	l.LeadID as [Lead ID],
	m.MatterID as [Matter ID],
	m.CaseID as [Case ID],
	ls.StatusName as [Status],
	mdv_substatus.DetailValue as [Sub Status],
	rldv_ven.DetailValue as [Vendor],
	mdv_ac.DetailValue as [Account Number],
	''<a title="Link-->" href="https://www.aquarium-software.com/customersleaddetails2.aspx?cid='' + CONVERT(VARCHAR,cu.CustomerID) + ''&lid='' + CONVERT(VARCHAR,l.LeadID) + ''&aid='' + CONVERT(VARCHAR,c.CaseID) + ''" target="_blank">View</a>'' as [Link To Case],
	''true'' as [DenyEdit], 
	''true'' as [DenyDelete]
FROM Lead l WITH (NOLOCK)
INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
INNER JOIN LeadStatus ls WITH (NOLOCK) ON ls.StatusID = c.ClientStatusID
INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = l.CustomerID
LEFT JOIN Partner p WITH (NOLOCK) ON p.CustomerID = cu.CustomerID
INNER JOIN MatterDetailValues mdv_joint WITH (NOLOCK) ON mdv_joint.MatterID = m.MatterID AND mdv_joint.DetailFieldID = 160088
LEFT JOIN LookupListItems luli_joint WITH (NOLOCK) ON luli_joint.LookupListItemID = mdv_joint.ValueInt
INNER JOIN MatterDetailValues mdv_substatus WITH (NOLOCK) ON mdv_substatus.MatterID = m.MatterID and mdv_substatus.DetailFieldID = 160162
INNER JOIN MatterDetailValues mdv_ven WITH (NOLOCK) ON mdv_ven.MatterID = m.MatterID and mdv_ven.DetailFieldID = 160086
LEFT JOIN ResourceListDetailValues rldv_ven WITH (NOLOCK) ON rldv_ven.ResourceListID = mdv_ven.ValueInt and rldv_ven.DetailFieldID = 160075
INNER JOIN MatterDetailValues mdv_ac WITH (NOLOCK) ON mdv_ac.MatterID = m.MatterID and mdv_ac.DetailFieldID = 160087
WHERE l.CustomerID = ' + CONVERT(VARCHAR(100),@CustomerID) + '
UNION 

select ROW_NUMBER() OVER (ORDER BY (SELECT (1))) as [TableRowID], 
	cu2.Fullname + ISNULL('' and '' + p.FullName,'''') as [Customer Names], 
	ISNULL(luli_joint.ItemValue,'''') as [Joint Single],
	l.LeadID as [Lead ID],
	m.MatterID as [Matter ID],
	m.CaseID as [Case ID],
	ls.StatusName as [Status],
	mdv_substatus.DetailValue as [Sub Status],
	rldv_ven.DetailValue as [Vendor],
	mdv_ac.DetailValue as [Account Number],
	''<a title="Link-->" href="https://www.aquarium-software.com/customersleaddetails2.aspx?cid='' + CONVERT(VARCHAR,cu2.CustomerID) + ''&lid='' + CONVERT(VARCHAR,l.LeadID) + ''&aid='' + CONVERT(VARCHAR,c.CaseID) + ''" target="_blank">View</a>'' as [Link To Case],
	''true'' as [DenyEdit], 
	''true'' as [DenyDelete]
FROM CustomerDetailvalues cdv WITH (NOLOCK)
INNER JOIN Customers cu2 WITH (NOLOCK) ON cu2.CustomerID = cdv.ValueInt
LEFT JOIN Partner p WITH (NOLOCK) ON p.CustomerID = cu2.CustomerID
INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = cu2.CustomerID
INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
INNER JOIN LeadStatus ls WITH (NOLOCK) ON ls.StatusID = c.ClientStatusID
INNER JOIN MatterDetailValues mdv_joint WITH (NOLOCK) ON mdv_joint.MatterID = m.MatterID AND mdv_joint.DetailFieldID = 160088
LEFT JOIN LookupListItems luli_joint WITH (NOLOCK) ON luli_joint.LookupListItemID = mdv_joint.ValueInt
INNER JOIN MatterDetailValues mdv_substatus WITH (NOLOCK) ON mdv_substatus.MatterID = m.MatterID and mdv_substatus.DetailFieldID = 160162
INNER JOIN MatterDetailValues mdv_ven WITH (NOLOCK) ON mdv_ven.MatterID = m.MatterID and mdv_ven.DetailFieldID = 160086
LEFT JOIN ResourceListDetailValues rldv_ven WITH (NOLOCK) ON rldv_ven.ResourceListID = mdv_ven.ValueInt and rldv_ven.DetailFieldID = 160075
INNER JOIN MatterDetailValues mdv_ac WITH (NOLOCK) ON mdv_ac.MatterID = m.MatterID and mdv_ac.DetailFieldID = 160087
WHERE cdv.DetailFieldID = 161600
AND cdv.CustomerID = ' + CONVERT(VARCHAR(100),@CustomerID)
	
	END

	IF @DetailFieldID = 143016  -- Client 210 Moisture tables. DCM 14/12/2011
	BEGIN
	
		SELECT @query = 'SELECT ROW_NUMBER() over (order by tdv.DetailValue, m.MatterID) as [TableRowID],
		m.MatterRef as [Room], 
		tdv.DetailValue as [Visit Date],
		tdvrt.DetailValue as [Room Temp],
		tdvrh.DetailValue as [Room RH],
		tdvwr.DetailValue as[Walls Search REL],
		tdvww.DetailValue as [Walls WME],
		tdvfe.DetailValue as [Floor ERH],
		tdvfm.DetailValue as [Floor Moisture Reading],
		tdvcm.DetailValue as [Ceiling Moisture Reading],
		10 as [10],
		1 as [DenyEdit], 
		1 as [DenyDelete] 
		FROM TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID=tdv.TableRowID
		INNER JOIN Matter m WITH (NOLOCK) ON tr.MatterID = m.MatterID and m.RefLetter <> ''A''
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID
		INNER JOIN Matter ma WITH (NOLOCK) ON ma.LeadID=l.LeadID and ma.MatterID=' + CONVERT(VARCHAR(100),@MatterID) 
		+ ' INNER JOIN TableDetailValues tdvrt WITH (NOLOCK) ON tr.TableRowID=tdvrt.TableRowID and tdvrt.DetailFieldID=136615
		INNER JOIN TableDetailValues tdvrh WITH (NOLOCK) ON tr.TableRowID=tdvrh.TableRowID and tdvrh.DetailFieldID=136616
		INNER JOIN TableDetailValues tdvwr WITH (NOLOCK) ON tr.TableRowID=tdvwr.TableRowID and tdvwr.DetailFieldID=136619
		INNER JOIN TableDetailValues tdvww WITH (NOLOCK) ON tr.TableRowID=tdvww.TableRowID and tdvww.DetailFieldID=136618
		INNER JOIN TableDetailValues tdvfe WITH (NOLOCK) ON tr.TableRowID=tdvfe.TableRowID and tdvfe.DetailFieldID=136621
		INNER JOIN TableDetailValues tdvfm WITH (NOLOCK) ON tr.TableRowID=tdvfm.TableRowID and tdvfm.DetailFieldID=136623
		INNER JOIN TableDetailValues tdvcm WITH (NOLOCK) ON tr.TableRowID=tdvcm.TableRowID and tdvcm.DetailFieldID=136625
		WHERE tdv.DetailFieldID = 136614'

	END	
	
	IF @DetailFieldID = 143029  -- Client 210 Room Dimension table. DCM 14/12/2011
	BEGIN
	
		SELECT @query = 'SELECT ROW_NUMBER() over (order by m.MatterID) as [TableRowID],
		m.MatterID as [Room],
		mdv.DetailValue as [Height],
		mdvm2.DetailValue as [Floor Area], 
		mdvm3.DetailValue as [Volume],
		sum(tdvl.ValueMoney) as [Length],
		sum(tdvw.ValueMoney) as [Width],
		7 as [7],
		8 as [8],
		9 as [9],
		10 as [10],
		1 as [DenyEdit], 
		1 as [DenyDelete] 
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		INNER JOIN Matter m WITH (NOLOCK) ON mdv.MatterID = m.MatterID and m.RefLetter <> ''A''
		INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
		INNER JOIN Matter ma WITH (NOLOCK) ON ma.LeadID=l.LeadID and ma.MatterID=' + CONVERT(VARCHAR(100),@MatterID) 
		+ ' INNER JOIN MatterDetailValues mdvm2 WITH (NOLOCK) ON mdvm2.MatterID=m.MatterID and mdvm2.DetailFieldID=135226 -- area
		INNER JOIN MatterDetailValues mdvm3 WITH (NOLOCK) ON mdvm3.MatterID=m.MatterID and mdvm3.DetailFieldID=135396 -- volume
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID=m.MatterID and tr.DetailFieldID=135988
		INNER JOIN TableDetailValues tdvl WITH (NOLOCK) ON tdvl.TableRowID=tr.TableRowID and tdvl.DetailFieldID=135990 -- length
		INNER JOIN TableDetailValues tdvw WITH (NOLOCK) ON tdvw.TableRowID=tr.TableRowID and tdvw.DetailFieldID=135991 -- width
		WHERE mdv.DetailFieldID=135397 -- height
		group by m.MatterID,mdv.DetailValue,mdvm2.DetailValue,mdvm3.DetailValue'

	END
	
	IF @DetailFieldID = 143181  -- Client 210 Strip Out tables. DCM 19/12/2011
	BEGIN
	
		SELECT @query = 'SELECT ROW_NUMBER() OVER (ORDER BY m.MatterRef,tdv.DetailValue) AS [TableRowID],
		m.MatterRef AS [Room], 
		CASE tdv.DetailValue WHEN 35545 THEN tdviio.DetailValue ELSE lulitdv.ItemValue END AS [Item],
		tdvdes.DetailValue AS [Description],
		4 AS [4],
		5 AS [5],
		6 AS [6],
		7 AS [7],
		8 AS [8],
		9 AS [9],
		10 AS [10],
		1 AS [DenyEdit], 
		1 AS [DenyDelete] 
		FROM TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID=tdv.TableRowID
		INNER JOIN Matter m WITH (NOLOCK) ON tr.MatterID = m.MatterID AND m.RefLetter <> ''A''
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID
		INNER JOIN Matter ma WITH (NOLOCK) ON ma.LeadID=l.LeadID AND ma.MatterID=' + CONVERT(VARCHAR(100),@MatterID) 
		+ ' INNER JOIN TableDetailValues tdviio WITH (NOLOCK) ON tr.TableRowID=tdviio.TableRowID AND tdviio.DetailFieldID=135965
		INNER JOIN TableDetailValues tdvdes WITH (NOLOCK) ON tr.TableRowID=tdvdes.TableRowID AND tdvdes.DetailFieldID=135966
		INNER JOIN LookupListItems lulitdv WITH (NOLOCK) ON tdv.ValueInt=lulitdv.LookupListItemID and lulitdv.LookupListID=3334
		WHERE tdv.DetailFieldID = 135964'

	END
	
	IF @DetailFieldID in (152991,  -- Client 233 Customer Level DM Accounts Summary AE June 2010
						  153343,  -- Client 223 Customer Level DM Accounts Summary DCM 17/07/2012
						  157106)  -- Client 3 DCM 25/9/2012
						  --165632)  -- Client 239 ACE 2013-03-18
	BEGIN
	
		select @DFPString=case @DetailFieldID
							when 152991 then '17147'
							when 153343 then '17169'
							when 157106 then '17638'
							when 165632 then '18496'
							end
	
		select @DFString=case @DetailFieldID
							when 152991 then '152991,152990'
							when 153343 then '153343,153253'
							when 157106 then '157106,156995'
							when 165632 then '165631,165632'
							end
							
		SELECT @query = 'SELECT pvt.TableRowID, [Date],[Type],[Credit],[Debit],[Reference],[Type__NAME],[Creditor Name], pvt.DenyEdit, pvt.DenyDelete FROM (SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, ISNULL(tdv.DetailValue, '''') as DetailValue, CASE WHEN tr.MatterID > 1 THEN ''True'' ELSE tr.DenyEdit END as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN Matter m on (m.CustomerID = tdv.CustomerID OR m.MatterID = tdv.MatterID) and m.CustomerID = ' + CONVERT(VARCHAR(100),@CustomerID) + '
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + @DFPString + ' 
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID IN (' + @DFString + ')
			 UNION 
			SELECT tdv.TableRowID as [TableRowID], df.FieldCaption + ''__NAME'' AS FieldCaption, ISNULL(ll.ItemValue, '''') as DetailValue, CASE WHEN tr.MatterID > 1 THEN ''True'' ELSE tr.DenyEdit END as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN Matter m on (m.CustomerID = tdv.CustomerID OR m.MatterID = tdv.MatterID) and m.CustomerID = ' + CONVERT(VARCHAR(100),@CustomerID) + '
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + @DFPString + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID IN (' + @DFString + ')
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON tdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID
		AND df.QuestionTypeID IN (2, 4)) as src pivot (MAX(DetailValue) for FieldCaption in ([Date],[Type],[Credit],[Debit],[Reference],[Type__NAME],[Creditor Name]))	as pvt
'

	END
	
	IF @DetailFieldID in (
							--152991,  -- Client 233 Customer Level DM Accounts Summary AE June 2010
						 -- 153343,  -- Client 223 Customer Level DM Accounts Summary DCM 17/07/2012
						 -- 157106,  -- Client 3 DCM 25/9/2012
						  165632)  -- Client 239 ACE 2013-03-18
	BEGIN
	
		select @DFPString=case @DetailFieldID
							when 152991 then '17147'
							when 153343 then '17169'
							when 157106 then '17638'
							when 165632 then '18496'
							end
	
		select @DFString=case @DetailFieldID
							when 152991 then '152991,152990'
							when 153343 then '153343,153253'
							when 157106 then '157106,156995'
							when 165632 then '165631,165632'
							end
							
		SELECT @query = 'SELECT pvt.TableRowID, [Date],[Type],[Credit],[Debit],[Payment Reference],[Type__NAME],[Creditor Name],[Relating MatterID],[Payment Bounce TableRowID],[Number of payments], pvt.DenyEdit, pvt.DenyDelete FROM (SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, ISNULL(tdv.DetailValue, '''') as DetailValue, CASE WHEN tr.MatterID > 1 THEN ''True'' ELSE tr.DenyEdit END as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN Matter m on (m.CustomerID = tdv.CustomerID OR m.MatterID = tdv.MatterID) and m.CustomerID = ' + CONVERT(VARCHAR(100),@CustomerID) + '
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + @DFPString + ' 
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID IN (' + @DFString + ')
			 UNION 
			SELECT tdv.TableRowID as [TableRowID], df.FieldCaption + ''__NAME'' AS FieldCaption, ISNULL(ll.ItemValue, '''') as DetailValue, CASE WHEN tr.MatterID > 1 THEN ''True'' ELSE tr.DenyEdit END as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN Matter m on (m.CustomerID = tdv.CustomerID OR m.MatterID = tdv.MatterID) and m.CustomerID = ' + CONVERT(VARCHAR(100),@CustomerID) + '
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + @DFPString + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID IN (' + @DFString + ')
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON tdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID
		AND df.QuestionTypeID IN (2, 4)) as src pivot (MAX(DetailValue) for FieldCaption in ([Date],[Type],[Credit],[Debit],[Payment Reference],[Type__NAME],[Creditor Name],[Relating MatterID],[Payment Bounce TableRowID],[Number of payments]))	as pvt
'

	END

	IF @DetailFieldID = 166137 /*Client 243 - PPI Summary*/
	BEGIN
		SELECT @query = 'SELECT	0 [TableRowID],m.MatterID,
		rdv.DetailValue [Creditor Name], 
		mdv_offer.DetailValue [Offer Amount], 
		ISNULL(li_funds.ItemValue,''--Not Selected--'') [Refund Method],
		ISNULL(mdv_refund.DetailValue,''--Action Date Blank--'') [Refund Received],
		mdv_fee.DetailValue [First Step Fee],
		CASE mdv_pp.ValueInt WHEN 75642 THEN ''Payment Plan'' WHEN 75643 THEN ''Full'' ELSE ''--Not Selected--'' END  [Paid in Full or Payment Plan],
		''true'' [DenyEdit], ''true'' [DenyDelete]
FROM Matter m WITH (NOLOCK) 
LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 161843
LEFT JOIN ResourceListDetailValues rdv WITH (NOLOCK) on rdv.ResourceListID = mdv.ValueInt and rdv.DetailFieldID = 162172
LEFT JOIN MatterDetailValues mdv_offer WITH (NOLOCK) on mdv_offer.MatterID = m.MatterID and mdv_offer.DetailFieldID = 161946
LEFT JOIN MatterDetailValues mdv_fee WITH (NOLOCK) on mdv_fee.MatterID = m.MatterID and mdv_fee.DetailFieldID = 161982 and mdv_fee.DetailValue > ''''
LEFT JOIN MatterDetailValues mdv_pp WITH (NOLOCK) on mdv_pp.MatterID = m.MatterID and mdv_pp.DetailFieldID = 166271 
LEFT JOIN MatterDetailValues mdv_funds WITH (NOLOCK) on mdv_funds.MatterID = m.MatterID and mdv_funds.DetailFieldID = 166279
LEFT JOIN LookupListItems li_funds WITH (NOLOCK) on li_funds.LookupListItemID = mdv_funds.ValueInt
LEFT JOIN MatterDetailValues mdv_refund WITH (NOLOCK) on mdv_refund.MatterID = m.MatterID and mdv_refund.DetailFieldID = 166280
INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = m.CustomerID
INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID and l.LeadTypeID = 1367
WHERE l.CustomerID = ' + convert(varchar,@CustomerID) 
	END

	IF @DetailFieldID in(168675,168674) /*Client 42 - Case Invoice Summary*/
	BEGIN
		SELECT @query = 'SELECT *,
		''true'' [DenyEdit], ''true'' [DenyDelete]
FROM		
    (	
	SELECT tr.TableRowID, df.FieldCaption, CASE WHEN (LEFT(ino.DetailValue,2) = ''CN'' and df.DetailFieldID <> 166338) THEN ''<font color="LightCoral">'' + ISNULL(tdv.DetailValue,'''') + ''</font>'' ELSE ISNULL(tdv.DetailValue,'''') END  [DetailValue]
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = tr.MatterID
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
	INNER JOIN TableDetailValues ino WITH (NOLOCK) on ino.TableRowID = tr.TableRowID and ino.DetailFieldID = 139188 /*Invoice number*/
	WHERE tr.DetailFieldID = 168506
	' + CASE @DetailFieldID WHEN 168674 THEN 'and m.CaseID = ' + convert(varchar,@CaseID) ELSE 'and m.LeadID = ' + convert(varchar,@LeadID) END +
	')
    AS ToPivot		
PIVOT		
(		
    Max([DetailValue])		
FOR		
[FieldCaption] 
    IN ( [Invoice Number],[Date Raised],[Paid So Far],[Outstanding Amount],[Latest Payment Date],[Paid in Full],[Invoice Details],[Invoice Net],[Invoice Vat],[Invoice Total],[Matter ID],[Creditor],[PPI Win Amount])
) AS Pivoted
ORDER BY [TableRowID]
'
	END

	IF @DetailFieldID = 168730 /*Case Payment Summary*/
	BEGIN
		SELECT @query = 'SELECT *,
				''true'' [DenyEdit], ''true'' [DenyDelete]
		FROM		
			(	
			SELECT tr.TableRowID, df.FieldCaption, ISNULL(tdv.DetailValue,'''') [DetailValue]
			FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = tr.MatterID
			INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = tdv.DetailFieldID
			WHERE tr.DetailFieldID = 168507
			' + CASE @DetailFieldID WHEN 168730 THEN 'and m.CaseID = ' + convert(varchar,@CaseID) ELSE 'and m.LeadID = ' + convert(varchar,@LeadID) END +
			')
			AS ToPivot		
		PIVOT		
		(		
			Max([DetailValue])		
		FOR		
		[FieldCaption] 
			IN ( [Invoice Number],[Date Paid],[Amount],[Matter ID],[Payment Taken By])
		) AS Pivoted
		ORDER BY [TableRowID]
		'
	END
	
	IF @DetailFieldID = 162489 /*Threshold 2 - 50% Secured debt vote required*/
	BEGIN
		SELECT @query = 'SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ''true'' [DenyEdit], ''true'' [DenyDelete]
						FROM TableRows tr WITH (NOLOCK) 
						INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 162847 /*Secured Lender Name*/
						LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
						LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID = 162850
						LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID = 163578 
						WHERE tr.DetailFieldID = 162313 /*Enter Secured Creditor Details*/
						and tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + ' ORDER BY [Voting Rights] DESC'
	END
	
	IF @DetailFieldID = 162492 /*Threshold 3 - 50% Unsecured debt vote required*/
	BEGIN
		SELECT @query = 'SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ''true'' [DenyEdit], ''true'' [DenyDelete]
						FROM TableRows tr WITH (NOLOCK) 
						INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 162841 /*Secured Lender Name*/
						LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
						LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID = 162844
						LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID = 163579 
						WHERE tr.DetailFieldID = 162315 /*Enter Secured Creditor Details*/
						and tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + ' ORDER BY [Voting Rights] DESC'
	END	
	
	IF @DetailFieldID = 162485 /*Threshold 1 - 65% Vote Required:*/
	BEGIN
		SELECT @query = 
			';With Total as 
			(
			SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ROW_NUMBER() OVER (PARTITION BY tr.TableRowID ORDER BY tr.TableRowID desc) [Row]
			FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID IN(162847,162841) /*Secured Lender Name*/
			LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
			LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID IN(162850, 162844)
			LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID IN(163578, 163579)
			WHERE tr.DetailFieldID in(162313, 162315)
			and tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + '
			),
			PreAdjustment as
			(
			SELECT t.TableRowID, t.Creditor, t.[Total Debt Vote], ( t.[Total Debt Vote] / ( SELECT SUM(t2.[Total Debt Vote]) FROM total t2 WHERE t2.Row = 1 ) ) * 100 [Voting Rights], ROW_NUMBER() OVER (ORDER BY ( t.[Total Debt Vote] / (SELECT SUM(t2.[Total Debt Vote]) FROM total t2 WHERE t2.Row = 1 )) desc) [Row]
			FROM Total t 
			WHERE t.Row = 1
			)
			SELECT pa.TableRowID, pa.Creditor, pa.[Total Debt Vote] , pa.[Voting Rights] + CASE pa.Row WHEN 1 THEN 100.00 - ( SELECT SUM(pa2.[Voting Rights]) FROM PreAdjustment pa2 ) ELSE 0.00 END [Voting Rights], ''true'' [DenyEdit], ''true'' [DenyDelete]
			FROM PreAdjustment pa ORDER BY [Voting Rights] DESC'
	END

	IF @DetailFieldID = 182378 /*Threshold 2 - 50% Secured debt vote required*/
	BEGIN
		SELECT @query = 'SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ''true'' [DenyEdit], ''true'' [DenyDelete]
						FROM TableRows tr WITH (NOLOCK) 
						INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 182621 /*Secured Lender Name*/
						LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
						LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID = 182624
						LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID = 182630 
						WHERE tr.DetailFieldID = 181987 /*Enter Secured Creditor Details*/
						and tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + ' ORDER BY [Voting Rights] DESC'
	END

	IF @DetailFieldID = 182381 /*Threshold 3 - 50% Unsecured debt vote required*/
	BEGIN
		SELECT @query = 'SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ''true'' [DenyEdit], ''true'' [DenyDelete]
						FROM TableRows tr WITH (NOLOCK) 
						INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 182597 /*Secured Lender Name*/
						LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
						LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID = 182599
						LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID = 182606 
						WHERE tr.DetailFieldID = 181989 /*Enter Secured Creditor Details*/
						and tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + ' ORDER BY [Voting Rights] DESC'
	END	
	
	IF @DetailFieldID = 182375 /*Threshold 1 - 65% Vote Required:*/
	BEGIN
		SELECT @query = 
			';With Total as 
			(
			SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ROW_NUMBER() OVER (PARTITION BY tr.TableRowID ORDER BY tr.TableRowID desc) [Row]
			FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID IN(182621,182597) /*Secured Lender Name*/
			LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
			LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID IN(182624, 182599)
			LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID IN(182630, 182606)
			WHERE tr.DetailFieldID in(181987, 181989)
			and tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + '
			),
			PreAdjustment as
			(
			SELECT t.TableRowID, t.Creditor, t.[Total Debt Vote], ( t.[Total Debt Vote] / ( SELECT SUM(t2.[Total Debt Vote]) FROM total t2 WHERE t2.Row = 1 ) ) * 100 [Voting Rights], ROW_NUMBER() OVER (ORDER BY ( t.[Total Debt Vote] / (SELECT SUM(t2.[Total Debt Vote]) FROM total t2 WHERE t2.Row = 1 )) desc) [Row]
			FROM Total t 
			WHERE t.Row = 1
			)
			SELECT pa.TableRowID, pa.Creditor, pa.[Total Debt Vote] , pa.[Voting Rights] + CASE pa.Row WHEN 1 THEN 100.00 - ( SELECT SUM(pa2.[Voting Rights]) FROM PreAdjustment pa2 ) ELSE 0.00 END [Voting Rights], ''true'' [DenyEdit], ''true'' [DenyDelete]
			FROM PreAdjustment pa ORDER BY [Voting Rights] DESC'
	END
	
	--IF @DetailFieldID = 162684 /*C267 - Repayment breakdown between unsecured creditors*/
	--BEGIN
	--	SELECT @query = 'SELECT * FROM dbo.fn_C267_GetUnsecuredRepaymentBreakdownByCustomerID(' + CONVERT(VARCHAR,@CustomerID) + ')'
	--END

	--IF EXISTS (SELECT * FROM ThirdPartyFieldMapping fm WITH (NOLOCK) WHERE fm.ThirdPartyFieldID = 666 and fm.DetailFieldID = @DetailFieldID)
	--BEGIN
	--	SELECT @query = 'SELECT * FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(' + CONVERT(VARCHAR,@CustomerID) + ')'
	--END

	IF @DetailFieldID = 197558 /*C252 - LBB - Login History*/ /*CS 2013-12-17 need to move this into TableRowsClientOption*/
	BEGIN
		SELECT @query = 'SELECT cp.ClientPersonnelID [TableRowID], lh.LoginDateTime [Login Date], cp.UserName, lh.IsSuccess [Success], lh.Notes, ''true'' [DenyEdit], ''true'' [DenyDelete]
		FROM LoginHistory lh WITH (NOLOCK) 
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = lh.ClientPersonnelID --and cp.IsAquarium = 0
		WHERE lh.ClientID = 252
		ORDER BY lh.LoginDateTime DESC'
	END

	IF @DetailFieldID = 197562 /*C252 - LBB - Lead View History*/ /*CS 2013-12-17 need to move this into TableRowsClientOption*/
	BEGIN
		SELECT @query = 
		'SELECT vh.LeadViewHistoryID [TableRowID], vh.WhenViewed [View Date], cp.UserName [Username], vh.LeadID, ''true'' [DenyEdit], ''true'' [DenyDelete]
		FROM LeadViewHistory vh WITH (NOLOCK) 
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = vh.ClientPersonnelID
		WHERE vh.ClientID = 252
			UNION
		SELECT vh.LeadViewHistoryID [TableRowID], vh.WhenViewed [View Date], cp.UserName [Username], vh.LeadID , ''true'' [DenyEdit], ''true'' [DenyDelete]
		FROM LeadViewHistoryArchive vh WITH (NOLOCK) 
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = vh.ClientPersonnelID
		WHERE vh.ClientID = 252
		ORDER BY [View Date] DESC'
	END
	 
	IF @DetailFieldID = 207216 /*File Import History*/ 
	BEGIN
		SELECT @query = 
		'SELECT 0 [TableRowID], df.DataLoaderFileID [FileID], df.TargetFileName [FileName], df.DataLoadedDateTime [Date Loaded], df.TargetFileLocation [Status], ''true'' [DenyEdit], ''true'' [DenyDelete]
		FROM DataLoaderFile df WITH (NOLOCK) 
		WHERE df.DataLoaderMapID IN(26)
		ORDER BY df.DataLoaderFileID DESC'
	END

	IF @DetailFieldID = 164249 /* C273 Payment Record.  Amended DCM 12/05/2014*/
	BEGIN
		SELECT @Query = '
		SELECT tr.TableRowID, tdv_date.ValueDate [Date], tdv_meth.ValueInt [Method of Payment], tdv_type.ValueInt [Payment Type], tdv_cred.DetailValue [Credit], tdv_debi.DetailValue [Debit], tdv_pref.DetailValue [Payment Reference],  tdv_cov.DetailValue [Monthly Payment Covered], li_meth.ItemValue [Method of Payment__NAME], li_type.ItemValue [Payment Type__NAME], NULL [DenyEdit], NULL [DenyDelete]
		FROM TableRows tr WITH (NOLOCK) 
		LEFT JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 164243
		LEFT JOIN TableDetailValues tdv_meth WITH (NOLOCK) on tdv_meth.TableRowID = tr.TableRowID and tdv_meth.DetailFieldID = 164244
		LEFT JOIN LookupListItems li_meth WITH (NOLOCK) on li_meth.LookupListItemID = tdv_meth.ValueInt and li_meth.ClientID = 273
		LEFT JOIN TableDetailValues tdv_type WITH (NOLOCK) on tdv_type.TableRowID = tr.TableRowID and tdv_type.DetailFieldID = 164245
		LEFT JOIN LookupListItems li_type WITH (NOLOCK) on li_type.LookupListItemID = tdv_type.ValueInt and li_type.ClientID = 273
		LEFT JOIN TableDetailValues tdv_cred WITH (NOLOCK) on tdv_cred.TableRowID = tr.TableRowID and tdv_cred.DetailFieldID = 164246
		LEFT JOIN TableDetailValues tdv_debi WITH (NOLOCK) on tdv_debi.TableRowID = tr.TableRowID and tdv_debi.DetailFieldID = 164247
		LEFT JOIN TableDetailValues tdv_pref WITH (NOLOCK) on tdv_pref.TableRowID = tr.TableRowID and tdv_pref.DetailFieldID = 164248
		LEFT JOIN TableDetailValues tdv_cov WITH (NOLOCK) on tdv_cov.TableRowID = tr.TableRowID and tdv_cov.DetailFieldID = 212915
		WHERE tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) + '
		and tr.DetailFieldID = 164249 /*Payment Record*/
			UNION
		SELECT tr.TableRowID, tdv_date.ValueDate [Date], 0, 65100, tdv_cred.DetailValue, tdv_debi.DetailValue, rlcred.DetailValue + CASE tdv_pref.DetailValue WHEN '''' THEN '''' ELSE '' - '' END + tdv_pref.DetailValue, '''', '''',''CREDITOR PAYMENT'', ''true'', ''true''
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN MatterDetailValues mdvcred WITH (NOLOCK) ON m.MatterID=mdvcred.MatterID and mdvcred.DetailFieldID=204474
		INNER JOIN ResourceListDetailValues rlcred WITH (NOLOCK) ON mdvcred.ValueInt=rlcred.ResourceListID and rlcred.DetailFieldID=206008
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = m.MatterID and tr.DetailFieldID = 206036 /*Individual Creditor Account Breakdown*/
		LEFT JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 206044
		LEFT JOIN TableDetailValues tdv_cred WITH (NOLOCK) on tdv_cred.TableRowID = tr.TableRowID and tdv_cred.DetailFieldID = 206045
		LEFT JOIN TableDetailValues tdv_debi WITH (NOLOCK) on tdv_debi.TableRowID = tr.TableRowID and tdv_debi.DetailFieldID = 206046
		LEFT JOIN TableDetailValues tdv_pref WITH (NOLOCK) on tdv_pref.TableRowID = tr.TableRowID and tdv_pref.DetailFieldID = 206048
		WHERE m.CustomerID = ' + CONVERT(VARCHAR,@CustomerID)
	END

	IF EXISTS (
		SELECT *
		FROM CustomTableSQL c
		WHERE c.DetailFieldID = @DetailFieldID
	)
	BEGIN
	
		IF ISNULL(@MatterID,0) = 0
		BEGIN
		
			SELECT TOP 1 @MatterID = CONVERT(VARCHAR(100),RTRIM(LTRIM(Item)))
			FROM dbo.fnSplitString (@MatterIDList, ',') f

		END
	
		EXEC dbo._C00_TableFromSQL @CustomerID, @LeadID, @CaseID, @MatterID, @DetailFieldID, @ClientID, @ClientPersonnelID, @ContactID, @PrintOnly

		RETURN

	END
	
	IF @PrintOnly = 1
	BEGIN
		PRINT @query
	END
	ELSE
	BEGIN
	
		EXECUTE(@query)	 
		
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID] TO [sp_executeall]
GO
