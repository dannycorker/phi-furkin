SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jan Wilson
-- Create date: 2013-03-07
-- Description:	Gets all EventTypes that user has access to
-- =============================================
CREATE PROCEDURE [dbo].[DocumentBatch_GetAllowedEventTypes]
(
	@LeadTypeID int,
	@EventSubtypeID int,
	@UserID int
)
AS
	SET ANSI_NULLS OFF
	
	IF @LeadTypeID = -1
	BEGIN
		SET @LeadTypeID = null
	END

	

	SELECT
		[EventTypeID]
		,et.[EventSubtypeID]
		,et.[EventTypeDescription]
		,et.[EventTypeName]  
		,lt.[LeadTypeName]
	FROM
		dbo.[EventType] et WITH (NOLOCK)
		INNER JOIN fnEventTypeSecure(@UserID, null) f ON et.EventTypeID = f.objectid
		INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = et.LeadTypeID
	WHERE
		[EventSubTypeID] = @EventSubtypeID
	AND (et.[LeadTypeID] = @LeadTypeID OR @LeadTypeID is null)
	AND et.[Enabled] = 1 
	ORDER BY lt.LeadTypeName, EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON














GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_GetAllowedEventTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_GetAllowedEventTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_GetAllowedEventTypes] TO [sp_executeall]
GO
