SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-19
-- Description:	Gets a list of sms stop config text for a given client
-- =============================================
CREATE PROCEDURE [dbo].[SMSStopConfigText_GetByClientID]
	
	@ClientID INT

AS
BEGIN
	
	declare @isLeadTypeSpecific int = 1

	SELECT TOP 1 @isLeadTypeSpecific = cast(s.isLeadTypeSpecific  as int) 
	FROM SMSStopConfig s with(nolock) 
	WHERE s.ClientID = 4-- @ClientID

	
	IF @isLeadTypeSpecific = 1 
	BEGIN

		SELECT s.SMSStopConfigTextID, s.ClientID, s.OutgoingPhoneNumber, s.SMSTextToAppend, s.LeadTypeID
		FROM SMSStopConfigText s WITH (NOLOCK) 	
		WHERE s.Clientid = @ClientID 

	END
	ELSE 
	BEGIN
		
		SELECT top 1 s.SMSStopConfigTextID, s.ClientID, s.OutgoingPhoneNumber, s.SMSTextToAppend, s.LeadTypeID
		FROM SMSStopConfigText s WITH (NOLOCK) 	
		WHERE s.Clientid = @ClientID 
		order by 1 desc

	END 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStopConfigText_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_GetByClientID] TO [sp_executehelper]
GO
