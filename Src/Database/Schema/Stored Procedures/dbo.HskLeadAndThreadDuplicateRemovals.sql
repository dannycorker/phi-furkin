SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-05-30
-- Description:	LeadEvent/Thread duplicate removal proc
-- JWG 2010-05-14 Speeded up some of the older sections.
-- ACE 2011-01-19 Added ClientHSKOptons
-- JWG 2011-04-26 More speedups for sections 4, 7 & 10
-- =============================================
CREATE PROCEDURE [dbo].[HskLeadAndThreadDuplicateRemovals]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LastRowCount INT, 
	@Debug BIT = 1, 
	@AdminUserID INT = 2795,
	@LogMessage VARCHAR(1024)


	/* 1. Set LeadEventThreadCompletion records to complete where a later, completed, one exists */
	/* Do this for all clients */
	UPDATE letc1 
	SET letc1.ToLeadEventID = letc2.ToLeadEventID, letc1.ToEventTypeID = letc2.ToEventTypeID
	FROM dbo.LeadEventThreadCompletion letc1  
	INNER JOIN dbo.LeadEventThreadCompletion letc2 ON letc2.CaseID = letc1.CaseID 
		AND letc2.FromEventTypeID = letc1.FromEventTypeID 
		AND letc2.ThreadNumberRequired = letc1.ThreadNumberRequired 
	WHERE letc2.LeadEventThreadCompletionID > letc1.LeadEventThreadCompletionID 
	AND letc1.ToLeadEventID IS NULL 
	AND letc2.ToLeadEventID IS NOT NULL 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 1.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/* 2. Delete misleading, completed, LeadEventThreadCompletion records where a later incomplete one exists */
	/* This is very similar to the update above, but this time the later LETC record is blank. */
	/* Do this for all clients */
	DELETE letc1 
	FROM dbo.LeadEventThreadCompletion letc1 
	INNER JOIN dbo.LeadEventThreadCompletion letc2 ON letc2.CaseID = letc1.CaseID 
		AND letc2.FromLeadEventID = letc1.FromLeadEventID 
		AND letc2.ThreadNumberRequired = letc1.ThreadNumberRequired 
	WHERE letc2.LeadEventThreadCompletionID > letc1.LeadEventThreadCompletionID 
	AND letc1.ToLeadEventID IS NOT NULL
	AND letc2.ToLeadEventID IS NULL

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 2.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/* 3. Set LeadEvent records to followed-up where they have been */
	/* Do this for all clients */
	/* JWG 2010-05-14 The update is only required if no open threads exist. */
	UPDATE le 
	SET WhenFollowedUp = le2.WhenCreated, NextEventID = le2.LeadEventID 
	FROM dbo.LeadEvent le 
	INNER JOIN dbo.LeadEventThreadCompletion letc ON letc.CaseID = le.CaseID 
		AND letc.FromLeadEventID = le.LeadEventID 
		AND letc.ToLeadEventID IS NOT NULL 
	INNER JOIN dbo.LeadEvent le2 ON le2.LeadEventID = letc.ToLeadEventID AND le2.EventDeleted = 0 
	WHERE le.WhenFollowedUp IS NULL 
	AND le.EventDeleted = 0 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.LeadEventThreadCompletion letc2 
		WHERE letc.CaseID = le.CaseID 
		AND letc2.FromLeadEventID = le.LeadEventID 
		AND letc2.ToLeadEventID IS NULL 
	)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 3.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		Use this sql if you need to list all clients that don't have any multi-threading.
		If there is a client that you want to exclude from housekeeping, they will not
		(or at least should not) appear in the list.
		
		SELECT c.ClientID 
		FROM dbo.Clients c WITH (NOLOCK) 
		EXCEPT 
		SELECT DISTINCT ClientID 
		FROM dbo.EventChoice ec WITH (NOLOCK) 
		WHERE ThreadNumber > 1 

		And excluded Lead Types are:
		294 = UCA ("Unenforceable Credit Agreements") for Brunel Franklin. Added by JG 2009-02-06 for AOH
	*/
	
	
	/* 
		4. Find duplicate events which are piled up one after the other and mark all but the latest as deleted duplicates.  
		Look for all un-followed-up events but only compare them against new events that have been added since the previous midnight.
	*/
	UPDATE le1 
	SET EventDeleted = 1, WhoDeleted = @AdminUserID, DeletionComments = 'Duplicate deleted by Aquarium ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
	FROM dbo.LeadEvent le1
	INNER JOIN dbo.EventType et ON et.EventTypeID = le1.EventTypeID 
	INNER JOIN dbo.ClientHSKOptions chsk ON chsk.ClientID = le1.ClientID AND chsk.IncludeLETCCleanUp = 1
	INNER JOIN dbo.LeadEvent le2 WITH (NOLOCK) ON le2.CaseID = le1.CaseID 
		AND le2.EventTypeID = le1.EventTypeID 
		AND le2.WhenFollowedUp IS NULL
		AND le2.EventDeleted = 0
		AND le2.LeadEventID > le1.LeadEventID 
		AND le2.WhenCreated > dateadd(hh, -25, dbo.fn_GetDate_Local()) 
		/*
		AND le2.WhenCreated > dbo.fnDateOnly(dbo.fn_GetDate_Local() - 1) 
		The optimiser generates a bad plan because it doesn't work out the value of this function. 
		It therefore decides to tablescan LETC instead, which takes about 5 minutes rather than 20 seconds.
		Using dateadd(hh, -25, dbo.fn_GetDate_Local()) might include a few more events to check each day, but well worth it.
		*/
	WHERE le1.WhenFollowedUp IS NULL
	AND le1.EventDeleted = 0 
	AND et.LeadTypeID <> 294 
	
	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 4.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/* 
		5. Look for orphaned records in the LeadEventThreadCompletion table
		Tidy up the LeadEventThreadCompletion table, now that all the unwanted events
		have been marked as logically-deleted and will never, ever need following up.
	*/
	DELETE letc 
	FROM dbo.LeadEventThreadCompletion letc 
	INNER JOIN dbo.ClientHSKOptions chsk ON chsk.ClientID = letc.ClientID AND chsk.IncludeLETCCleanUp = 1
	INNER JOIN dbo.LeadEvent le ON le.LeadEventID = letc.FromLeadEventID 
	INNER JOIN dbo.EventType et ON et.EventTypeID = le.EventTypeID 
	WHERE le.EventDeleted = 1
	AND letc.ToLeadEventID IS NULL
	AND et.LeadTypeID <> 294 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 5.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		6. Show where one case has two LETC records on the go, from the same leadevent
		Delete the unwanted duplicate LETC record
	*/
	DELETE
	FROM LeadEventThreadCompletion 
	WHERE LeadEventThreadCompletionID IN
	(
		SELECT LeadEventThreadCompletionID 
		FROM dbo.LeadEventThreadCompletion l1 
		INNER JOIN dbo.Lead l ON l.LeadID = l1.LeadID 
		INNER JOIN dbo.ClientHSKOptions chsk ON chsk.ClientID = l1.ClientID AND chsk.IncludeLETCCleanUp = 1
		WHERE l1.ToLeadEventID IS NULL
		AND l.LeadTypeID NOT IN (294) 
		AND EXISTS (
			SELECT * 
			FROM LeadEventThreadCompletion l2 
			WHERE l2.CaseID = l1.CaseID 
			AND l2.FromLeadEventID = l1.FromLeadEventID 
			AND l2.ThreadNumberRequired = l1.ThreadNumberRequired
			AND l2.ToLeadEventID IS NULL
			AND l2.LeadEventThreadCompletionID > l1.LeadEventThreadCompletionID 
		)
	)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 6.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		7. Check for any "satisfied" events that are not marked as followed up:
		Loop through all "satisfied" events that are not marked as followed up, and mark them as followed up.
	*/
	DECLARE @fups TABLE (
		LeadID INT, 
		CaseID INT, 
		FeTID INT, 
		ToTID INT, 
		LeadEventID INT, 
		NextEventID INT,
		ClientID INT
	)

	INSERT @fups (LeadID, CaseID, FeTID, ToTID, LeadEventID, NextEventID, ClientID)
	SELECT le1.LeadID, le1.CaseID, le1.EventTypeID, le2.EventTypeID, le1.LeadEventID, le2.LeadEventID, le1.ClientID 
	FROM dbo.LeadEvent le1 WITH (NOLOCK) 
	INNER JOIN dbo.EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = le1.EventTypeID AND ec.LeadTypeID <> 294 
	INNER JOIN dbo.LeadEvent le2 WITH (NOLOCK) ON le2.CaseID = le1.CaseID 
		AND le2.EventDeleted = 0 
		AND le2.EventTypeID = ec.NextEventTypeID 
		AND le2.WhenCreated > dateadd(hh, -25, dbo.fn_GetDate_Local()) 
		/*
		AND le2.WhenCreated > dbo.fnDateOnly(dbo.fn_GetDate_Local() - 1) 
		Same optimiser issue. 
		*/
	INNER JOIN dbo.LeadEventThreadCompletion letc1 WITH (NOLOCK) ON letc1.CaseID = le1.CaseID 
		AND letc1.FromLeadEventID = le1.LeadEventID 
		AND letc1.ToLeadEventID IS NULL 
	WHERE le1.WhenFollowedUp IS NULL 
	AND le1.EventDeleted = 0
	AND le2.LeadEventID > le1.LeadEventID 

	DELETE @fups 
	FROM @fups f 
	WHERE NOT EXISTS (
		SELECT * 
		FROM dbo.ClientHSKOptions chsk 
		WHERE chsk.ClientID = f.ClientID 
		AND chsk.IncludeLETCCleanUp = 1 
	)

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 7.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		8. If any are found then sort them out
		Sort out LeadEventThreadCompletion first...
	*/
	UPDATE dbo.LeadEventThreadCompletion 
	SET ToLeadEventID = f.NextEventID, ToEventTypeID = f.ToTID
	FROM dbo.LeadEventThreadCompletion letc
	INNER JOIN @fups f ON letc.LeadID = f.LeadID AND letc.CaseID = f.CaseID AND letc.FromLeadEventID = f.LeadEventID AND letc.FromEventTypeID = f.FeTID 
	INNER JOIN dbo.ClientHSKOptions chsk ON chsk.ClientID = letc.ClientID AND chsk.IncludeLETCCleanUp = 1
	WHERE letc.ToLeadEventID IS NULL

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 8.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		9. Now we can mark the LeadEvent records as followed up
	*/
	UPDATE dbo.LeadEvent 
	SET WhenFollowedUp = dbo.fn_GetDate_Local(), NextEventID = f.NextEventID 
	FROM @fups f 
	INNER JOIN dbo.LeadEvent le ON le.LeadEventID = f.LeadEventID 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 9.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		10. SAME FOR JUMP-TOs caused/created by doc scanning
		Mark all but the latest as followed-up:
	*/
	DECLARE @fups2 TABLE (
		LeadID INT, 
		CaseID INT, 
		FeTID INT, 
		ToTID INT, 
		LeadEventID INT, 
		NextEventID INT,
		ClientID INT
	)

	INSERT @fups2 (LeadID, CaseID, FeTID, ToTID, LeadEventID, NextEventID, ClientID)
	SELECT le1.LeadID, le1.CaseID, le1.EventTypeID, le2.EventTypeID, le1.LeadEventID, le2.LeadEventID, le1.ClientID 
	FROM dbo.LeadEvent le1 WITH (NOLOCK) 
	INNER JOIN dbo.LeadEventThreadCompletion letc1 WITH (NOLOCK) ON letc1.CaseID = le1.CaseID AND letc1.FromLeadEventID = le1.LeadEventID AND letc1.ToLeadEventID IS NULL 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le1.EventTypeID AND et.LeadTypeID <> 294
	INNER JOIN dbo.LeadEvent le2 WITH (NOLOCK) ON le2.CaseID = le1.CaseID 
		AND le2.EventDeleted = 0 
		AND le2.WhenFollowedUp IS NULL 
		AND le2.LeadEventID > le1.LeadEventID 
		AND le2.WhenCreated > dateadd(hh, -25, dbo.fn_GetDate_Local()) 
		/*
		AND le2.WhenCreated > dbo.fnDateOnly(dbo.fn_GetDate_Local() - 1) 
		Same optimiser issue. 
		*/
	WHERE le1.WhenFollowedUp IS NULL 
	AND le1.EventDeleted = 0

	DELETE @fups2 
	FROM @fups2 f 
	WHERE NOT EXISTS (
		SELECT * 
		FROM dbo.ClientHSKOptions chsk 
		WHERE chsk.ClientID = f.ClientID 
		AND chsk.IncludeLETCCleanUp = 1 
	)


	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 10.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		11. If any are found then sort them out
		Sort out LeadEventThreadCompletion first...
	*/
	UPDATE dbo.LeadEventThreadCompletion 
	SET ToLeadEventID = f.NextEventID, ToEventTypeID = f.ToTID
	FROM dbo.LeadEventThreadCompletion letc
	INNER JOIN @fups2 f ON letc.LeadID = f.LeadID AND letc.CaseID = f.CaseID AND letc.FromLeadEventID = f.LeadEventID AND letc.FromEventTypeID = f.FeTID 
	INNER JOIN dbo.ClientHSKOptions chsk ON chsk.ClientID = letc.ClientID AND chsk.IncludeLETCCleanUp = 1
	WHERE letc.ToLeadEventID IS NULL

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 11.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		12. Now we can mark the LeadEvent records as followed up
	*/
	UPDATE dbo.LeadEvent 
	SET WhenFollowedUp = dbo.fn_GetDate_Local(), NextEventID = f.NextEventID 
	FROM @fups2 f 
	INNER JOIN dbo.LeadEvent le ON le.LeadEventID = f.LeadEventID 
	
	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 12.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		13. Check for any "unsatisfied" process start events that are not on the first case.
		For some clients this is fine, but if posting out packs (eg BF) then it makes no sense.
		JG 2008-12-02 New special case for BF where leadtypeid 294 DOES want dup start events,
		              because each case needs its own agreement to be signed etc.
	*/
	DECLARE @DupStarts TABLE (
		dsCaseID INT,
		dsLeadEventID INT,
		dsEventTypeID INT
	)

	INSERT @DupStarts
	SELECT le.CaseID, le.LeadEventID, le.EventTypeID
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.Cases ca WITH (NOLOCK) ON le.CaseID = ca.CaseID
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID AND et.EventSubtypeID = 10 
	WHERE le.ClientID = 3 
	AND le.WhenFollowedUp IS NULL 
	AND le.EventDeleted = 0 
	AND et.LeadTypeID <> 294 -- JG 2008-12-02
	AND ca.CaseNum > 1 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 13.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END

	
	/*
		14. Set the duplicate event to completed in the LETC table first, ready for step 15
	*/
	UPDATE dbo.LeadEventThreadCompletion
	SET ToLeadEventID = FromLeadEventID, ToEventTypeID = ToEventTypeID
	FROM dbo.LeadEventThreadCompletion letc 
	INNER JOIN @DupStarts ds ON ds.dsCaseID = letc.CaseID AND ds.dsLeadEventID = letc.FromLeadEventID
	WHERE letc.ToLeadEventID IS NULL

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 14.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		15. Now the event can be set to complete.
	*/
	UPDATE dbo.LeadEvent 
	SET WhenFollowedUp = dbo.fn_GetDate_Local(), NextEventID = LeadEventID 
	FROM dbo.LeadEvent le 
	INNER JOIN @DupStarts ds ON ds.dsLeadEventID = le.LeadEventID

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 15.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


	/*
		16. Deleted events no longer followup the previous event.
	*/
	UPDATE le1 
	SET WhenFollowedUp = NULL, NextEventID = NULL
	FROM dbo.LeadEvent le1 
	INNER JOIN dbo.LeadEvent le2 ON le2.LeadEventID = le1.NextEventID
	WHERE le2.EventDeleted = 1 
	AND le1.WhenFollowedUp IS NOT NULL 
	AND le1.EventDeleted = 0 

	SELECT @LastRowCount = @@ROWCOUNT

	IF @Debug = 1
	BEGIN
		SELECT @LogMessage = CONVERT(VARCHAR, @LastRowCount) + ' done in Step 16.'
		EXEC dbo._C00_LogIt 'Debug', 'AquariumHousekeeping', 'HskLeadAndThreadDuplicateRemovals', @LogMessage, @AdminUserID 
	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[HskLeadAndThreadDuplicateRemovals] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HskLeadAndThreadDuplicateRemovals] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HskLeadAndThreadDuplicateRemovals] TO [sp_executeall]
GO
