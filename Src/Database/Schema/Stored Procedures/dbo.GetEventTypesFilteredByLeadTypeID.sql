SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
MODIFIED	2014-07-25	SB	Changed to use function for shared lead types
*/


CREATE PROCEDURE [dbo].[GetEventTypesFilteredByLeadTypeID]

-- New security checks by UserID (Jim Green, 2007-10-16)
@ClientID int,
@EventSubtypeID int,
@LeadTypeID int,
@UserID int

 AS

-- Only return Event Types to the web service, that the end user
-- will be able to edit (not just view).
SELECT     ClientID, EventSubtypeID, EventTypeDescription,EventTypeID
FROM         dbo.fnEventTypeShared(@ClientID) et 
INNER JOIN dbo.fnEventTypeSecure(@UserID, @LeadTypeID) f ON et.EventTypeID = f.objectid
WHERE     (EventSubtypeID = @EventSubtypeID) AND (ClientID = @ClientID) AND (LeadTypeID=@LeadTypeID) AND (Enabled=1) AND f.RightID > 1





GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventTypesFilteredByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventTypesFilteredByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventTypesFilteredByLeadTypeID] TO [sp_executeall]
GO
