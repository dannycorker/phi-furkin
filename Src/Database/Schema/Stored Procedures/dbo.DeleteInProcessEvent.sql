SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[DeleteInProcessEvent] 
	@LeadEventID int, 
	@ClientPersonnelID int,
	@ClientID int
AS 
BEGIN

	IF SYSTEM_USER = 'LouisBromilow' AND @ClientID = (SELECT Top 1 ClientID FROM ClientPersonnel cp WITH (NOLOCK) where cp.ClientPersonnelID = @ClientPersonnelID)
	BEGIN
		
		DECLARE     @MostRecentX AS VARCHAR(20),
					@EarliestX AS VARCHAR(20), 
					@EventTo AS VARCHAR(20),
					@EventFrom AS VARCHAR(20),
					@CurrentCase AS VARCHAR(20),
					@FromET AS VARCHAR(20),
					@FromLE AS VARCHAR(20),
					@ToET AS VARCHAR(20),
					@ToLE AS VARCHAR(20),
					@DeleteFrom AS VARCHAR(20),
					@DeleteTo AS VARCHAR(20),
					@WhenCreated as date,
					@Counter int = 1, 
					@Comments varchar(max) = 'Deleted in Bulk by Aquarium'

		DECLARE @LeadEvents as Table(LeadEventID int, Row int)

		SELECT      @MostRecentX =  MAX(le.LeadEventID), /*Latest value you want to delete*/
					@EarliestX = MIN(le.LeadEventID), /*Earliest value you want to delete*/
					@CurrentCase = le.CaseID /*The Case you want to delete from*/


		FROM dbo.LeadEventThreadCompletion let WITH (NOLOCK) 
		INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = let.FromEventTypeID 
		LEFT JOIN dbo.EventType et2 WITH (NOLOCK) ON et2.EventTypeID = let.ToEventTypeID 
		LEFT JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.LeadEventID = let.FromLeadEventID 
		WHERE let.FromLeadEventID = @LeadEventID
		AND let.ToLeadEventID IS NOT NULL 
		GROUP BY le.CaseID

		--Select @MostRecentX,@EarliestX,@CurrentCase

		/*Get the latest value before the earliest value you want to delete - This is the new 'from'*/
		SELECT      @EventFrom = MAX(let.LeadEventThreadCompletionID), @DeleteFrom = MAX(let.FromLeadEventID)

		FROM dbo.LeadEventThreadCompletion let WITH (NOLOCK) 
		WHERE let.CaseID = @CurrentCase AND let.FromLeadEventID < @EarliestX
		GROUP BY let.CaseID

		/*Get the earliest value before the latest value you want to delete - This is the new 'to'*/
		SELECT      @EventTo = MIN(let.LeadEventThreadCompletionID), @DeleteTo = MIN(let.FromLeadEventID)

		FROM dbo.LeadEventThreadCompletion let WITH (NOLOCK) 
		WHERE let.CaseID = @CurrentCase AND let.FromLeadEventID > @MostRecentX
		GROUP BY let.CaseID

		/*The EventTypeID and LeadEventID are used in the LeadEvent Update Statement Later*/
		SELECT @FromET = let.FromEventTypeID, @FromLE = let.FromLeadEventID 
		FROM dbo.LeadEventThreadCompletion let WITH (NOLOCK) 
		WHERE let.CaseID = @CurrentCase 
		AND let.LeadEventThreadCompletionID = @EventFrom

		/*The EventTypeID and LeadEventID are used in the LeadEvent Update Statement Later*/
		SELECT @ToET = let.FromEventTypeID, @ToLE = let.FromLeadEventID 
		FROM dbo.LeadEventThreadCompletion let WITH (NOLOCK) 
		WHERE let.CaseID = @CurrentCase 
		AND let.LeadEventThreadCompletionID = @EventTo

		SELECT let.ToEventTypeID, let.ToLeadEventID ,@EventTo
		FROM dbo.LeadEventThreadCompletion let WITH (NOLOCK) 
		WHERE let.CaseID = @CurrentCase 
		AND let.LeadEventThreadCompletionID = @EventTo

		--Select* from LeadEventThreadCompletion let WITH (NOLOCK) where let.LeadEventThreadCompletionID = 75253184

		/*View the ID's Delete when make Proc*/
		SELECT      @CurrentCase AS [CurrentCase],
					@MostRecentX AS [MostRecentX],
					@EarliestX AS [EarliestX],
					@EventFrom AS [EventFrom],
					@EventTo AS [EventTo],
					@FromET AS [FROM EVENT],            
					@FromLE AS [FROM LeadEvent],
					@ToET AS [TO EVENT],            
					@ToLE AS [TO LeadEvent],
					@DeleteFrom AS [FROM DLE],
					@DeleteTo AS [TO DLE]

		/*Delete the LeadEventThreadCompletion and LeadEvent Records*/
		Delete 
		--SELECT LeadEventThreadCompletionID 
		FROM LeadEventThreadCompletion 
		WHERE LeadEventThreadCompletionID BETWEEN @EventFrom AND @EventTo
		AND CaseID = @CurrentCase
		AND LeadEventThreadCompletionID <> @EventFrom
		AND LeadEventThreadCompletionID <> @EventTo

		Insert @LeadEvents(LeadEventID,Row)
		SELECT LeadEventID, ROW_NUMBER()over(order by LeadEventID desc) 
		FROM LeadEvent
		INNER JOIN EventType WITH (NOLOCK) ON EventType.EventTypeID = Leadevent.EventTypeID
		WHERE LeadEvent.LeadEventID BETWEEN @DeleteFrom AND @DeleteTo
		AND CaseID = @CurrentCase
		AND LeadEvent.LeadEventID not in(@DeleteFrom,@DeleteTo)
		AND LeadEvent.EventDeleted = 0
		AND EventType.InProcess = 1

		/*Stitching*/
		Update let
		Set ToEventTypeID = @ToET, ToLeadEventID = @ToLE
		From LeadEventThreadCompletion let
		Where let.LeadEventThreadCompletionID = @EventFrom

		Select @WhenCreated = Le.WhenCreated
		From LeadEvent le WITH (NOLOCK) 
		Where le.LeadEventID = @ToLE

		Update Le
		Set NextEventID = @ToLE, WhenFollowedUp = @WhenCreated
		From LeadEvent le
		Where le.LeadEventID = @FromLE

		Update LeadEvent
		Set NextEventID = null, WhenFollowedUp = NULL
		Where LeadEventID = @LeadEventID

		exec DeleteLeadEvent @LeadEventID,@ClientPersonnelID, 'Louis Bromilow', 4, 1

	END
	ELSE
	BEGIN
	
		Select 'Please talk directly to Louis Bromilow before attempting to use this stored procedure'
	
	END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteInProcessEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteInProcessEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteInProcessEvent] TO [sp_executeall]
GO
