SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeFieldCompletion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_GetByDeleteOptionID]
(

	@DeleteOptionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeFieldCompletionID],
					[ClientID],
					[EventTypeID],
					[DetailFieldID],
					[InsertOptionID],
					[DeleteOptionID]
				FROM
					[dbo].[EventTypeFieldCompletion] WITH (NOLOCK) 
				WHERE
					[DeleteOptionID] = @DeleteOptionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_GetByDeleteOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_GetByDeleteOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_GetByDeleteOptionID] TO [sp_executeall]
GO
