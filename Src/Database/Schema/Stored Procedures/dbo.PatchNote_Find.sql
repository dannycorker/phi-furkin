SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PatchNote table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PatchNote_Find]
(

	@SearchUsingOR bit   = null ,

	@PatchNoteID int   = null ,

	@PatchID int   = null ,

	@Location varchar (50)  = null ,

	@PatchNoteText varchar (200)  = null ,

	@AdminOnly bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PatchNoteID]
	, [PatchID]
	, [Location]
	, [PatchNoteText]
	, [AdminOnly]
    FROM
	[dbo].[PatchNote] WITH (NOLOCK) 
    WHERE 
	 ([PatchNoteID] = @PatchNoteID OR @PatchNoteID IS NULL)
	AND ([PatchID] = @PatchID OR @PatchID IS NULL)
	AND ([Location] = @Location OR @Location IS NULL)
	AND ([PatchNoteText] = @PatchNoteText OR @PatchNoteText IS NULL)
	AND ([AdminOnly] = @AdminOnly OR @AdminOnly IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PatchNoteID]
	, [PatchID]
	, [Location]
	, [PatchNoteText]
	, [AdminOnly]
    FROM
	[dbo].[PatchNote] WITH (NOLOCK) 
    WHERE 
	 ([PatchNoteID] = @PatchNoteID AND @PatchNoteID is not null)
	OR ([PatchID] = @PatchID AND @PatchID is not null)
	OR ([Location] = @Location AND @Location is not null)
	OR ([PatchNoteText] = @PatchNoteText AND @PatchNoteText is not null)
	OR ([AdminOnly] = @AdminOnly AND @AdminOnly is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PatchNote_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Find] TO [sp_executeall]
GO
