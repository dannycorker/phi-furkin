SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-04-06
-- Description:	Create a User and use the CLR to generate a salt and password for it
-- UPDATE:		Simon Brushett
--				2011-12-08
--				Changed to call into the CP_Insert_Helper proc as direct insert into CP no longer allowed
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateAquariumUser] 
	@ClientID int,
	@EmailAddress varchar(201),
	@FirstName varchar(100),
	@LastName varchar(100),
	@JobTitle varchar(50),
	@ChargeOutRate money,
	@ClientOfficeID int,
	@TitleID int,
	@Password varchar(65) = null,
	@ClientPersonnelAdminGroupID int = null,
	@ContinueIfUserExists bit = 1 

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @UserID int,
	@EncryptedPassword varchar(65), 
	@Salt varchar(50), 
	@TestClientID int 

	DECLARE @Temp TABLE (
		EncryptedPassword varchar(65), 
		Salt varchar(250)
	)

	/* Check to see if there is already a user with these details */
	SELECT @UserID = cp.ClientPersonnelID, 
			@TestClientID = cp.ClientID 
	FROM dbo.ClientPersonnel cp (nolock) 
	WHERE cp.EmailAddress = @EmailAddress
	
	/* 
		1) If there is no such Portal user, then carry on.
		2) If this Portal user already exists, but it belongs to this client 
		   AND the @ContinueIfUserExists flag is true then carry on.
		3) Otherwise this is an error scenario, so do not do any more work at all.
	*/
	IF (@UserID IS NULL) OR (@TestClientID = @ClientID AND @ContinueIfUserExists = 1)
	BEGIN
	
		/* Call the CLR application to generate a salt and encrypt our password */
		INSERT INTO @Temp
		EXEC CreatePassword @Password 

		/* Get the results from the temp table variables */
		SELECT TOP 1 @EncryptedPassword = t.EncryptedPassword, 
		@Salt = t.Salt 
		FROM @Temp t 

		/* If the CLR call worked then create the new Portal user */
		IF @EncryptedPassword IS NOT NULL AND @Salt IS NOT NULL
		BEGIN
		
			EXEC @UserID = ClientPersonnel_Insert_Helper @ClientID, @FirstName, @LastName, @EmailAddress, @EncryptedPassword, @Salt, @ClientOfficeID, @TitleID, NULL, @JobTitle, @ClientPersonnelAdminGroupID, @ChargeOutRate = @ChargeOutRate

		END
		
		Return @UserID 

	END
	ELSE
	BEGIN
	
		RETURN NULL
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateAquariumUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateAquariumUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateAquariumUser] TO [sp_executeall]
GO
