SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldGroup_Update]
(

	@RPIFieldGroupID int   ,

	@GroupName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIFieldGroup]
				SET
					[GroupName] = @GroupName
				WHERE
[RPIFieldGroupID] = @RPIFieldGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Update] TO [sp_executeall]
GO
