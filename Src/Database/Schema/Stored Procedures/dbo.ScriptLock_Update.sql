SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptLock table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_Update]
(

	@ScriptLockID int   ,

	@ScriptID int   ,

	@ClientID int   ,

	@WhoIsEditing int   ,

	@EditStartedAt datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptLock]
				SET
					[ScriptID] = @ScriptID
					,[ClientID] = @ClientID
					,[WhoIsEditing] = @WhoIsEditing
					,[EditStartedAt] = @EditStartedAt
				WHERE
[ScriptLockID] = @ScriptLockID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Update] TO [sp_executeall]
GO
