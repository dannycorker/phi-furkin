SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PatchNote table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PatchNote_Insert]
(

	@PatchNoteID int    OUTPUT,

	@PatchID int   ,

	@Location varchar (50)  ,

	@PatchNoteText varchar (200)  ,

	@AdminOnly bit   
)
AS


				
				INSERT INTO [dbo].[PatchNote]
					(
					[PatchID]
					,[Location]
					,[PatchNoteText]
					,[AdminOnly]
					)
				VALUES
					(
					@PatchID
					,@Location
					,@PatchNoteText
					,@AdminOnly
					)
				-- Get the identity value
				SET @PatchNoteID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PatchNote_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Insert] TO [sp_executeall]
GO
