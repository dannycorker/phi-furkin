SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[GetLoginAttemptsFromEmailAddress] @emailAddress nvarchar(255)

AS

Select AttemptedLogins from ClientPersonnel WITH (NOLOCK)Where EmailAddress = @EmailAddress


GO
GRANT VIEW DEFINITION ON  [dbo].[GetLoginAttemptsFromEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLoginAttemptsFromEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLoginAttemptsFromEmailAddress] TO [sp_executeall]
GO
