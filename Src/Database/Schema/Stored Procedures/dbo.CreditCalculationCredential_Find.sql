SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CreditCalculationCredential table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_Find]
(

	@SearchUsingOR bit   = null ,

	@CreditCalculationCredentialID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelID int   = null ,

	@SecretKey varchar (32)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CreditCalculationCredentialID]
	, [ClientID]
	, [ClientPersonnelID]
	, [SecretKey]
    FROM
	[dbo].[CreditCalculationCredential] WITH (NOLOCK) 
    WHERE 
	 ([CreditCalculationCredentialID] = @CreditCalculationCredentialID OR @CreditCalculationCredentialID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([SecretKey] = @SecretKey OR @SecretKey IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CreditCalculationCredentialID]
	, [ClientID]
	, [ClientPersonnelID]
	, [SecretKey]
    FROM
	[dbo].[CreditCalculationCredential] WITH (NOLOCK) 
    WHERE 
	 ([CreditCalculationCredentialID] = @CreditCalculationCredentialID AND @CreditCalculationCredentialID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([SecretKey] = @SecretKey AND @SecretKey is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_Find] TO [sp_executeall]
GO
