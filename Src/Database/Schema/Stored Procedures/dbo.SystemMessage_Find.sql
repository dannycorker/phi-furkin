SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SystemMessage table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessage_Find]
(

	@SearchUsingOR bit   = null ,

	@SystemMessageID int   = null ,

	@ClientID int   = null ,

	@SystemMessageName varchar (200)  = null ,

	@SystemMessageText varchar (MAX)  = null ,

	@Enabled bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SystemMessageID]
	, [ClientID]
	, [SystemMessageName]
	, [SystemMessageText]
	, [Enabled]
    FROM
	[dbo].[SystemMessage] WITH (NOLOCK) 
    WHERE 
	 ([SystemMessageID] = @SystemMessageID OR @SystemMessageID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SystemMessageName] = @SystemMessageName OR @SystemMessageName IS NULL)
	AND ([SystemMessageText] = @SystemMessageText OR @SystemMessageText IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SystemMessageID]
	, [ClientID]
	, [SystemMessageName]
	, [SystemMessageText]
	, [Enabled]
    FROM
	[dbo].[SystemMessage] WITH (NOLOCK) 
    WHERE 
	 ([SystemMessageID] = @SystemMessageID AND @SystemMessageID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SystemMessageName] = @SystemMessageName AND @SystemMessageName is not null)
	OR ([SystemMessageText] = @SystemMessageText AND @SystemMessageText is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessage_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Find] TO [sp_executeall]
GO
