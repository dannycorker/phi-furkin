SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitleSpecialisedDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_GetByLeadTypePageTitleSpecialisedDetailFieldTargetID]
(

	@LeadTypePageTitleSpecialisedDetailFieldTargetID int   
)
AS


				SELECT
					[LeadTypePageTitleSpecialisedDetailFieldTargetID],
					[ClientID],
					[LeadTypeID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[RowField],
					[Notes]
				FROM
					[dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[LeadTypePageTitleSpecialisedDetailFieldTargetID] = @LeadTypePageTitleSpecialisedDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_GetByLeadTypePageTitleSpecialisedDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_GetByLeadTypePageTitleSpecialisedDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_GetByLeadTypePageTitleSpecialisedDetailFieldTargetID] TO [sp_executeall]
GO
