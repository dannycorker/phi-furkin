SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- ==========================================================================================================
-- Author:		Simon Brushett
-- Create date: 2011-08-30
-- Description:	Saves newly selected columns and removes columns that have been unselected
--
-- Updated:     09-12-2011
-- By:          Jan Wilson
-- Description: Now takes account of preferred columns and whether a column needs to be shown.  @AutoAdding 
--              parameter added to ensure that the shown column is only set when performing the Add Table 
--              function
-- ==========================================================================================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__AddRemoveTableColumns]
(
	@SqlQueryEditingID INT,
	@ColumnsToAdd dbo.tvpIntVarchar READONLY,
	@ColumnsToRemove dbo.tvpIntVarchar READONLY,
	@AutoAdding BIT = NULL
)
AS

DECLARE @ClientID INT,
		@IsChildQuery BIT

SELECT @ClientID = e.ClientID, @IsChildQuery = CASE WHEN ParentQueryID IS NULL THEN 0 ELSE 1 END
FROM dbo.SqlQueryEditing e WITH (NOLOCK) 
WHERE e.SqlQueryEditingID = @SqlQueryEditingID

IF @IsChildQuery = 1
BEGIN

	INSERT INTO dbo.SqlQueryEditingColumns
	(ClientID, SqlQueryEditingID, SqlQueryEditingTableID, CompleteOutputText, ShowColumn, DisplayOrder, IsAggregate, ColumnDataType, ManipulatedDataType, ColumnNaturalName, RealSqlQueryColumnID)
	SELECT	DISTINCT @ClientID, @SqlQueryEditingID, NULL, 
			CASE WHEN c.ColumnAlias > '' THEN c.ColumnAlias ELSE c.ColumnNaturalName END, 1, 9999, 0, c.ManipulatedDataType, c.ManipulatedDataType, 
			CASE WHEN c.ColumnAlias > '' THEN c.ColumnAlias ELSE c.ColumnNaturalName END, 0
	FROM dbo.SqlQueryTables t WITH (NOLOCK)
	INNER JOIN dbo.SqlQueryColumns c WITH (NOLOCK) ON t.SqlQueryTableID = c.SqlQueryTableID
	INNER JOIN dbo.SqlQueryEditing e WITH (NOLOCK) ON c.SqlQueryID = e.ParentQueryID AND t.SqlQueryID = e.ParentQueryID  
	INNER JOIN @ColumnsToAdd a ON CASE WHEN c.ColumnAlias > '' THEN c.ColumnAlias ELSE c.ColumnNaturalName END = a.AnyValue 
	WHERE c.ShowColumn = 1
	AND e.SqlQueryEditingID = @SqlQueryEditingID

	
	DELETE cr
	FROM dbo.SqlQueryEditingCriteria cr
	INNER JOIN dbo.SqlQueryEditingColumns c WITH (NOLOCK) ON cr.SqlQueryEditingColumnID = c.SqlQueryEditingColumnID 
	INNER JOIN @ColumnsToRemove r ON c.SqlQueryEditingColumnID = r.AnyValue
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID

	DELETE c
	FROM dbo.SqlQueryEditingColumns c
	INNER JOIN @ColumnsToRemove r ON c.SqlQueryEditingColumnID = r.AnyValue
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID

END
ELSE
BEGIN

	-- Add new columns into the editing tables	
	-- We insert new columns with a high order as they will be reset below
	INSERT INTO dbo.SqlQueryEditingColumns
	(ClientID, SqlQueryEditingID, SqlQueryEditingTableID, CompleteOutputText, ShowColumn, DisplayOrder, IsAggregate, ColumnDataType, ManipulatedDataType, ColumnNaturalName, RealSqlQueryColumnID)
	SELECT 
		@ClientID,										-- ClientId
		@SqlQueryEditingID,								-- SqlQueryEditingId
		t.SqlQueryEditingTableID,						-- SqlQueryEditingTableId
		CASE 
			WHEN t.TableAlias > '' THEN t.TableAlias 
			ELSE t.SqlQueryTableName 
		END + '.' + i.COLUMN_NAME,						-- CompleteOutputText
		CASE
			WHEN @AutoAdding = 1 THEN ISNULL(pc.ShowColumn, 1)
			ELSE 1
		END,											-- ShowColumn
		9999,											-- DisplayOrder
		0,												-- IsAggregate
		ISNULL(m.SqlQueryReportType, 'Text'),			-- ColumnDataType
		ISNULL(m.SqlQueryReportType, 'Text'),			-- ManipulatedDataType
		i.COLUMN_NAME,									-- ColumnNaturalName
		0												-- RealSqlQueryColumnID
	FROM
		INFORMATION_SCHEMA.COLUMNS i WITH (NOLOCK) 
		INNER JOIN @ColumnsToAdd a 
			ON i.COLUMN_NAME = a.AnyValue 
		INNER JOIN dbo.SqlQueryEditingTable t WITH (NOLOCK)
			ON a.AnyID = t.SqlQueryEditingTableID 
			AND i.TABLE_NAME = t.SqlQueryTableName
		INNER JOIN dbo.ReportTables rt WITH (NOLOCK)
			ON rt.ReportTableName = i.TABLE_NAME
		LEFT JOIN dbo.SqlQueryPreferredColumns pc
			ON rt.ReportTableID = pc.ReportTableID
			AND pc.ReportColumnName = i.COLUMN_NAME
		LEFT JOIN dbo.SqlQueryColumnTypeMapping m WITH (NOLOCK)
			ON i.DATA_TYPE = m.SqlQueryType
			
	
	-- Delete old columns and any criteria that depend on these columns
	DELETE cr
	FROM dbo.SqlQueryEditingCriteria cr
	INNER JOIN dbo.SqlQueryEditingColumns c WITH (NOLOCK) ON cr.SqlQueryEditingColumnID = c.SqlQueryEditingColumnID 
	INNER JOIN @ColumnsToRemove r ON c.SqlQueryEditingColumnID = r.AnyValue
	INNER JOIN dbo.SqlQueryEditingTable t WITH (NOLOCK) ON c.SqlQueryEditingTableID = t.SqlQueryEditingTableID AND r.AnyID = t.SqlQueryEditingTableID
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID

	DELETE c
	FROM dbo.SqlQueryEditingColumns c
	INNER JOIN @ColumnsToRemove r ON c.SqlQueryEditingColumnID = r.AnyValue
	INNER JOIN dbo.SqlQueryEditingTable t WITH (NOLOCK) ON c.SqlQueryEditingTableID = t.SqlQueryEditingTableID AND r.AnyID = t.SqlQueryEditingTableID
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID
	

END

-- Now reset the display order
;WITH InnerSql AS 
(
SELECT SqlQueryEditingColumnID, ROW_NUMBER() OVER(ORDER BY DisplayOrder, SqlQueryEditingColumnID) as rn 
FROM dbo.SqlQueryEditingColumns WITH (NOLOCK) 
WHERE SqlQueryEditingID = @SqlQueryEditingID
)

UPDATE c
SET c.DisplayOrder = i.rn
FROM dbo.SqlQueryEditingColumns c
INNER JOIN InnerSql i ON c.SqlQueryEditingColumnID = i.SqlQueryEditingColumnID






GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__AddRemoveTableColumns] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__AddRemoveTableColumns] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__AddRemoveTableColumns] TO [sp_executeall]
GO
