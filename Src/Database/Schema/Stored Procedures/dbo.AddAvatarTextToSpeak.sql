SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AddAvatarTextToSpeak]
@ClientQuestionnaireID int,
@ClientID int,
@PageNumber int,
@SpeakText nvarchar(512),
@IsShown bit

as

insert into TextToSpeak(ClientQuestionnaireID, ClientID, PageNumber, SpeakText, IsShown)
values (@ClientQuestionnaireID, @ClientID, @PageNumber, @SpeakText, @IsShown)


declare @TextToSpeakID int

set @TextToSpeakID = SCOPE_IDENTITY()

Select TextToSpeakID from TextToSpeak where TextToSpeakID = @TextToSpeakID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddAvatarTextToSpeak] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddAvatarTextToSpeak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddAvatarTextToSpeak] TO [sp_executeall]
GO
