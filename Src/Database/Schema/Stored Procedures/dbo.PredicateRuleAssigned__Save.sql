SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Predicate Rule Assigned Save (Insert/Update)
-- =============================================
CREATE PROCEDURE [dbo].[PredicateRuleAssigned__Save] 
	@PredicateRuleAssignedID INT, 
	@LeadTypeID INT, 
	@EventTypeID INT, 
	@DocumentTypeID INT, 
	@PredicateRuleID INT, 
	@RuleOrder INT,
	@OverrideDocumentTypeID INT, 
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	IF (@PredicateRuleAssignedID = 0)
	BEGIN
		INSERT INTO PredicateRuleAssigned (LeadTypeID, EventTypeID, DocumentTypeID, PredicateRuleID, RuleOrder, OverrideDocumentTypeID, ClientID)
		VALUES (@LeadTypeID, @EventTypeID, @DocumentTypeID, @PredicateRuleID, @RuleOrder, @OverrideDocumentTypeID, @ClientID)
		
		SELECT @PredicateRuleAssignedID = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE PredicateRuleAssigned
		SET
			PredicateRuleID = @PredicateRuleID, 
			RuleOrder = @RuleOrder, 
			OverrideDocumentTypeID = @OverrideDocumentTypeID
		WHERE
			PredicateRuleAssignedID = @PredicateRuleAssignedID
		AND ClientID = @ClientID
	END

	SELECT	pra.*, et.EventTypeName, dt.DocumentTypeName, pr.RuleName, dto.DocumentTypeName OverrideDocumentName
	FROM	PredicateRuleAssigned pra WITH (NOLOCK)
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = pra.LeadTypeID
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = pra.EventTypeID
	INNER JOIN DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = pra.DocumentTypeID
	INNER JOIN PredicateRule pr WITH (NOLOCK) ON pr.PredicateRuleID = pra.PredicateRuleID
	INNER JOIN DocumentType dto WITH (NOLOCK) ON dto.DocumentTypeID = pra.OverrideDocumentTypeID
	WHERE	pra.PredicateRuleAssignedID = @PredicateRuleAssignedID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRuleAssigned__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PredicateRuleAssigned__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRuleAssigned__Save] TO [sp_executeall]
GO
