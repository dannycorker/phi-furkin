SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-06-15
-- Description:	Add payments to accounts when not rejected
-- =============================================
CREATE PROCEDURE [dbo].[_C38_DailyUserCount]
	
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @myERROR int

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int
	);

	DECLARE @TableRows TABLE (
		TableRowID int
	)
	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int
	)

	INSERT @RequiredFields 
	SELECT 6452
	UNION SELECT 27300

	INSERT INTO @LeadsAndMatters (LeadID)
	SELECT LeadID
	From Lead
	Where ClientID = 38

	INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
	SELECT 38,lam.LeadID,rf.DetailFieldID,'' 
	FROM @LeadsAndMatters lam 
	CROSS JOIN @RequiredFields rf 
	WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WHERE ldv.LeadID = lam.LeadID AND ldv.DetailFieldID = rf.DetailFieldID) 


	BEGIN TRAN

/* Add History for Change of user Numbers */
INSERT INTO [dbo].[DetailValueHistory]([ClientID],[DetailFieldID],[LeadOrMatter],[LeadID],[MatterID],[FieldValue],[WhenSaved],[ClientPersonnelID],[EncryptedValue])
Select 38,6452,1,UsrCount.leadid,NULL,UsrCount.CountOfusers,dbo.fn_GetDate_Local(),315,''
From leadDetailValues
Inner Join (select count(*) as [CountOfusers], ldv.leadid as LeadID
		From leaddetailvalues ldv 
		inner Join ClientPersonnel on ClientPersonnel.ClientID = ldv.DetailValue 
		Where ClientPersonnel.EmailAddress not like '%aquarium%'
		and ClientPersonnel.UserName NOT IN ('Alex Elger', 'Alex Ash', 'Hiren Ghandi', 'Jim Green', 'Ed Shropshire', 'Mark Colonnese', 'Mark Mansfield', 'Edward Shropshire', 'Alexandra Ash', 'Data Recovery', 'hiren gandhi')
		and ClientPersonnel.AccountDisabled = 0
		and ldv.DetailFieldID = 6453
		Group by  ldv.leadid) as UsrCount on UsrCount.LeadID = LeadDetailValues.LeadID
Where LeadDetailValues.DetailFieldID = 6452
And case isnumeric(LeadDetailValues.DetailValue) when 1 then convert(numeric,LeadDetailValues.DetailValue) else 0 end <> UsrCount.CountOfusers


/* Update current number of Users*/ 
Update LeadDetailValues
Set DetailValue = UsrCount.CountOfusers
From leadDetailValues
Inner Join (select count(*) as [CountOfusers], ldv.leadid as LeadID
		From leaddetailvalues ldv 
		inner Join ClientPersonnel on ClientPersonnel.ClientID = ldv.DetailValue 
		Where ClientPersonnel.EmailAddress not like '%aquarium%'
		and ClientPersonnel.UserName NOT IN ('Alex Elger', 'Alex Ash', 'Hiren Ghandi', 'Jim Green', 'Ed Shropshire', 'Mark Colonnese', 'Mark Mansfield', 'Edward Shropshire', 'Alexandra Ash', 'Data Recovery', 'hiren gandhi')
		and ClientPersonnel.AccountDisabled = 0
		and ldv.DetailFieldID = 6453
		Group by  ldv.leadid) as UsrCount on UsrCount.LeadID = LeadDetailValues.LeadID
Where LeadDetailValues.DetailFieldID = 6452
And case isnumeric(LeadDetailValues.DetailValue) when 1 then convert(numeric,LeadDetailValues.DetailValue) else 0 end <> UsrCount.CountOfusers

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/*	Update Next Payment Date uing Working Days*/
update leaddetailvalues 
set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), convert(int,luli.itemvalue), 5), 126)
from leaddetailvalues 
inner join leaddetailvalues ldv2 on ldv2.leadid = leaddetailvalues.leadid and ldv2.detailfieldid = 27299
inner join lookuplistitems luli on luli.lookuplistitemid = ldv2.detailvalue 
where leaddetailvalues.detailfieldid = 27300
and ((leaddetailvalues.DetailValue is not null) and (datediff(dd, convert(datetime, leaddetailvalues.DetailValue), dbo.fn_GetDate_Local()) > 0) )

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	Select 'Success' as [Outcome]

	GOTO END_NOW

HANDLE_ERROR:
    ROLLBACK TRAN
	SELECT 'Faulure' as [Outcome]

END_NOW:
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_DailyUserCount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_DailyUserCount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_DailyUserCount] TO [sp_executeall]
GO
