SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- Modified By PR 16/03/2016 Added other statuses
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetQueuedItems]
	@ClientID INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	dspq.OutLeadEventID
	FROM	DocumentSignProviderQueue dspq WITH (NOLOCK) 
	WHERE	dspq.ClientID = @ClientID 
	AND		dspq.DSPEnvelopeStatus IN ('Completed', 'Declined', 'Voided')
	--AND		DATEADD(MINUTE, -30, dbo.fn_GetDate_Local()) > dspq.WhenStatus
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetQueuedItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetQueuedItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetQueuedItems] TO [sp_executeall]
GO
