SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Post back status change to parent syystem
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PostBackStatus]
(
	@LeadEventID INT
)


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@LeadID INT,
		@EventTypeID INT,
		@CaseID INT,
		@OldCaseID INT,
		@OldLeadID INT,
		@StatusDetailFieldID INT,
		@LeadStatusName VARCHAR(250),
		@LeadOrMatter INT,
		@CompanyName VARCHAR(250),
		@OldClientPersonnelID INT,
		@OldClientID INT,
		@StatusID INT,
		@ClientID INT

	SELECT
	@LeadID = Lead.LeadID,
	@EventTypeID = LeadEvent.EventTypeID,
	@CaseID = LeadEvent.CaseID,
	@OldCaseID = ctm.CaseID,
	@OldLeadID = ctm.LeadID,
	@StatusDetailFieldID = dfa.DetailFieldID,
	@LeadStatusName = leadstatus.statusname,
	@LeadOrMatter = df.LeadOrMatter,
	@CompanyName = Clients.CompanyName,
	@OldClientID = oldlead.ClientID,
	@StatusID = Cases.ClientStatusID,
	@ClientID = Cases.ClientID
	FROM dbo.Customers WITH (NOLOCK) 
	INNER JOIN dbo.Lead WITH (NOLOCK) ON Lead.CustomerID = Customers.CustomerID
	INNER JOIN dbo.LeadEvent WITH (NOLOCK) ON Lead.LeadID = LeadEvent.LeadID
	INNER JOIN dbo.Cases WITH (NOLOCK) ON Cases.CaseID = LeadEvent.Caseid
	INNER JOIN dbo.LeadStatus WITH (NOLOCK) ON LeadStatus.StatusID = Cases.ClientStatusID
	INNER JOIN dbo.CaseTransferMapping ctm WITH (NOLOCK) ON ctm.NewCaseID = LeadEvent.CaseID
	INNER JOIN dbo.Lead oldlead WITH (NOLOCK) ON oldlead.LeadID = ctm.LeadID
	INNER JOIN dbo.DetailFieldAlias dfa WITH (NOLOCK) ON dfa.LeadTypeID = oldlead.LeadTypeID AND dfa.detailfieldalias = 'Destination Status'
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfa.DetailFieldID
	INNER JOIN dbo.Clients WITH (NOLOCK) ON Clients.ClientID = Lead.ClientID
	WHERE dbo.LeadEvent.LeadEventID = @LeadEventID 

	IF @ClientID = 57 AND @OldClientID <> 112 AND @StatusID NOT IN (322,944,860,316,323,315,467,636,958,519,959) RETURN

	--Select @ClientID, @OldClientID, @StatusID

	SELECT @OldClientPersonnelID = ClientPersonnel.ClientPersonnelID
	FROM ClientPersonnel
	WHERE ClientID = @OldClientID
	AND FirstName = 'Alex' AND LastName = 'Elger'

	IF @StatusDetailFieldID IS NOT NULL
	BEGIN

		EXEC dbo._C00_CreateDetailFields @StatusDetailFieldID, @OldLeadID

		IF @LeadOrMatter = 1
		BEGIN

			UPDATE LeadDetailValues
			SET DetailValue = @LeadStatusName
			FROM LeadDetailValues
			WHERE DetailFieldID = @StatusDetailFieldID
			AND LeadID = @OldLeadID

			INSERT INTO DetailValueHistory(ClientID, DetaiLFieldID,LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT LeadDetailValues.ClientID, @StatusDetailFieldID, 1, LeadID, NULL, @CompanyName + ' - ' + @LeadStatusName, dbo.fn_GetDate_Local(), @OldClientPersonnelID
			FROM LeadDetailValues
			WHERE LeadDetailValues.DetailFieldID = @StatusDetailFieldID

		END

		--select @OldLeadID

		IF @LeadOrMatter = 2
		BEGIN

			UPDATE MatterdetailValues
			SET DetailValue = @LeadStatusName
			FROM MatterDetailValues
			INNER JOIN Matter ON Matter.MatterID = MatterDetailValues.MatterID AND Matter.CaseID = @OldCaseID
			WHERE DetailFieldID = @StatusDetailFieldID

			INSERT INTO DetailValueHistory(ClientID, DetaiLFieldID,LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT Matter.ClientID, MatterDetailValues.DetailFieldID, 2, Matter.LeadID, Matter.MatterID, @CompanyName + ' - ' + @LeadStatusName, dbo.fn_GetDate_Local(), @OldClientPersonnelID
			FROM MatterDetailValues
			INNER JOIN Matter ON Matter.MatterID = MatterDetailValues.MatterID AND Matter.CaseID = @OldCaseID
			WHERE MatterDetailValues.DetailFieldID = @StatusDetailFieldID


		END


	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PostBackStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PostBackStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PostBackStatus] TO [sp_executeall]
GO
