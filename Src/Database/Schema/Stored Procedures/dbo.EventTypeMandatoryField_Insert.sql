SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeMandatoryField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField_Insert]
(

	@EventTypeMandatoryFieldID int    OUTPUT,

	@EventTypeID int   ,

	@DetailFieldID int   ,

	@LeadTypeID int   ,

	@ClientID int   ,

	@ForceEditableOverride bit   
)
AS


				
				INSERT INTO [dbo].[EventTypeMandatoryField]
					(
					[EventTypeID]
					,[DetailFieldID]
					,[LeadTypeID]
					,[ClientID]
					,[ForceEditableOverride]
					)
				VALUES
					(
					@EventTypeID
					,@DetailFieldID
					,@LeadTypeID
					,@ClientID
					,@ForceEditableOverride
					)
				-- Get the identity value
				SET @EventTypeMandatoryFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Insert] TO [sp_executeall]
GO
