SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-10-20
-- Description:	Equations for Field
-- =============================================
CREATE PROCEDURE [dbo].[eqn]
@DetailFieldID int,
@LeadID int= NULL,
@MatterID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Fields TABLE(DetailFieldID int)

	declare @EquationTextRaw varchar(2000), 
			@EquationTextField varchar(2000), 
			@EquationTextValue varchar(2000), 
			@TestID int = 0 

	insert into @Fields
	select df.DetailFieldID
	from DetailFields df_e WITH (NOLOCK) 
	inner join DetailFields df WITH (NOLOCK) on df.ClientID = df_e.ClientID and df_e.EquationText LIKE '%Q' + convert(varchar,df.DetailFieldID) + '%'
	where df_e.EquationText <> ''
	and df_e.DetailFieldID = @DetailFieldID

	select @EquationTextRaw = df_e.EquationText, @EquationTextField = df_e.EquationText, @EquationTextValue = df_e.EquationText
	from DetailFields df_e WITH (NOLOCK) 
	where df_e.DetailFieldID = @DetailFieldID

	select TOP 1 @DetailFieldID = f.DetailFieldID
	From @Fields f
	Order BY f.DetailFieldID

	WHILE @DetailFieldID > @TestID
	BEGIN

		select @EquationTextField = replace(@EquationTextField, 'Q' + convert(varchar,df_e.DetailFieldID), df_e.FieldName)
		from DetailFields df_e WITH (NOLOCK) 
		where df_e.DetailFieldID = @DetailFieldID
		
		IF @MatterID IS NULL
		BEGIN
		
			select @EquationTextValue = replace(@EquationTextValue, 'Q' + convert(varchar,ldv.DetailFieldID), ldv.DetailValue)
			from LeadDetailValues ldv WITH (NOLOCK) 
			where ldv.DetailFieldID = @DetailFieldID
			and ldv.LeadID = @LeadID
		
		END
		ELSE
		BEGIN

			select @EquationTextValue = replace(@EquationTextValue, 'Q' + convert(varchar,mdv.DetailFieldID), mdv.DetailValue)
			from MatterDetailValues mdv WITH (NOLOCK) 
			where mdv.DetailFieldID = @DetailFieldID
			and mdv.MatterID = @MatterID
		
		END
		
		Select @TestID = @DetailFieldID

		select TOP 1 @DetailFieldID = f.DetailFieldID
		From @Fields f
		Where f.DetailFieldID > @DetailFieldID
		Order BY f.DetailFieldID

	END

	select @EquationTextRaw as [Original Equation], @EquationTextField as [English Equation], @EquationTextValue as [Equation Text With Values]

END




GO
GRANT VIEW DEFINITION ON  [dbo].[eqn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[eqn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[eqn] TO [sp_executeall]
GO
