SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReminderTimeshift table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReminderTimeshift_GetByReminderTimeshiftID]
(

	@ReminderTimeshiftID int   
)
AS


				SELECT
					[ReminderTimeshiftID],
					[ReminderTimeshiftName],
					[TimeUnitsID],
					[TimeUnitsQuantity],
					[DisplayOrder],
					[TimeUnitsQuantityInMinutes]
				FROM
					[dbo].[ReminderTimeshift] WITH (NOLOCK) 
				WHERE
										[ReminderTimeshiftID] = @ReminderTimeshiftID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_GetByReminderTimeshiftID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReminderTimeshift_GetByReminderTimeshiftID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_GetByReminderTimeshiftID] TO [sp_executeall]
GO
