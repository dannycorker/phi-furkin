SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Dave Morgan
-- Create date:	  29/11/2016	
-- Description:   Change payment interval
-- 2017-11-01 CPS call PurchasedProductHistory__Backup with @LeadEventID
-- =============================================
CREATE PROCEDURE [dbo].[ChangePaymentInterval__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT = NULL
AS
BEGIN


--declare
--	@ClientID INT = 384,
--	@CaseID INT = 7313,
--	@LeadEventID INT = 86494

	/*

		annual to monthly
		-----------------

			1. find the prorata amount left to pay
			2. add a ledger record to reverse that pro-rata amount
			3. find the remaining months available to pay 
			4. build a schedule for the remaining months using the regular payment amount
			5. find the total remaining to pay from the new schedule
			6. add an adjustment row on the first payment date for any difference between (1) and (5)

		monthly to annual
		-----------------
		
			1. find the total outstanding payment (unpaid months)
			2. remove the unpaid months from the schedule
			3. add a single row for (1) 

		Note in both cases we ignore past, but ongoing payment failures & assume these will be handled by the current overdue
		collections process, unless this is annual to monthly & no payment has been made (either because it hasn't been collected yet
		or it failed.
			
	*/
	

	SET NOCOUNT ON;
	  
	DECLARE @Success BIT
      
	-- Get the third party fields
 	-- Gets the customer, lead, matter identities and the leadtype
	DECLARE @CustomerID INT, 
			@LeadID INT, 
			@MatterID INT, 
			@LeadTypeID INT, 
			@WhoCreated INT,
			@StateCode VARCHAR(100),
			@PaymentTypeLuli INT,
			@PaymentMethod INT,
			@TransactionFee NUMERIC(18,2)

	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID, @LeadID = m.LeadID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID=@CaseID
	
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID 
	FROM Lead l WITH (NOLOCK) 
	WHERE l.LeadID=@LeadID

	SELECT @WhoCreated = le.WhoCreated 
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID=@LeadEventID

	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	--4413	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN
		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
		SELECT @AccountID=AccountID FROM PurchasedProduct WITH (NOLOCK) WHERE PurchasedProductID=@PurchasedProductID
	END	
	
	--4401	AdjustmentDate	
	DECLARE @AdjustmentDate_DV VARCHAR(2000), @IsDateTime_AdjustmentDate BIT, @AdjustmentDate DATETIME
	SELECT @AdjustmentDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4401)		
	SELECT @IsDateTime_AdjustmentDate = dbo.fnIsDateTime(@AdjustmentDate_DV)		
	IF(@IsDateTime_AdjustmentDate=1)
	BEGIN
		SELECT @AdjustmentDate = CAST(@AdjustmentDate_DV AS DATETIME)
	END
	
	--4523 Next payment date override
	DECLARE @OverrideDate_DV VARCHAR(2000), @IsDateTime_OverrideDate BIT, @OverrideDate DATETIME
	SELECT @OverrideDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4523)		
	SELECT @IsDateTime_OverrideDate = dbo.fnIsDateTime(@OverrideDate_DV)		
	IF(@IsDateTime_OverrideDate=1)
	BEGIN
		SELECT @OverrideDate = CAST(@OverrideDate_DV AS DATETIME)
	END	

	--4525	PaymentIntervalChangeID
	DECLARE @PaymentIntervalChangeID_DV VARCHAR(2000), @IsInt_PaymentIntervalChangeID BIT, @PaymentIntervalChangeID INT, @ItemID_PaymentIntervalChangeID INT, @PaymentIntervalChangeID_ItemValue VARCHAR(500)
	SELECT @PaymentIntervalChangeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4525)	
	SELECT @IsInt_PaymentIntervalChangeID = dbo.fnIsInt(@PaymentIntervalChangeID_DV)		
	IF(@IsInt_PaymentIntervalChangeID=1)
	BEGIN
		SELECT @ItemID_PaymentIntervalChangeID = CAST(@PaymentIntervalChangeID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual mid term adjustment calculation method
		SELECT @PaymentIntervalChangeID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_PaymentIntervalChangeID
		SELECT @PaymentIntervalChangeID = pf.PaymentFrequencyID FROM PaymentFrequency pf WITH (NOLOCK) WHERE pf.FrequencyName = @PaymentIntervalChangeID_ItemValue
	END		

	IF @PaymentIntervalChangeID IN (4, -- monthly
									5) -- annually
	BEGIN

		BEGIN TRY  
	      

		DECLARE @PPPSSummary TABLE (
				FailedNotYetRetried MONEY,
				FailedNotYetRetriedVAT MONEY,
				GroupedRetries MONEY,
				GroupedRetriesVAT MONEY,
				IndividualRetries MONEY,
				IndividualRetriesVAT MONEY,
				OriginalUnpaid MONEY,
				OriginalUnpaidVAT MONEY,
				Paid MONEY,
				PaidVAT MONEY,
				TotalAnnualPremium MONEY,
				TotalAnnualPremiumVAT MONEY,
				TotalDue MONEY,
				TotalDueVAT MONEY,
				TotalOverdue MONEY,
				TotalOverdueVAT MONEY )

			DECLARE @AnnualPremium MONEY,
					@AnnualPremiumVAT MONEY,
					@PayRefund INT,
					@FromDate DATE,
					@FutureDue MONEY,
					@FutureDueVAT MONEY,
					@PastDue MONEY,
					@PastDueVAT MONEY,
					@MonthsFutureDue MONEY,
					@MonthsFutureDueVAT MONEY,
					@FirstMonthAdjustment MONEY,
					@FirstMonthAdjustmentVAT MONEY,
					@MonthCoverFrom DATE,
					@MonthCoverTo DATE,
					@PaymentDate DATE,
					@PurchasedProductPaymentScheduleID INT,
					@ActualCollectionDate DATE,
					@CoverFrom DATE,
					@CoverTo DATE,
					@Paid MONEY,
					@PaidVAT MONEY

			-- performs backup of payment schedules if required
			IF @LeadEventID IS NOT NULL
			BEGIN
				EXEC dbo.PurchasedProductHistory__Backup @PurchasedProductID, @WhoCreated, @LeadEventID
				EXEC PurchasedProductPaymentScheduleHistory__Backup @PurchasedProductID, @WhoCreated, 0  -- don't delete the current purchased product schedule, we're just going to update the uncollected rows
				EXEC CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 0 -- don't delete the current schedule, we're just going to update the uncollected rows         
	        
				UPDATE PurchasedProduct 
				SET PaymentFrequencyID=@PaymentIntervalChangeID,
					WhenModified=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) 
				WHERE PurchasedProductID = @PurchasedProductID			
			END
	 
			SELECT      @ClientID = product.ClientID,
						@AccountID = product.AccountID, 
						@CoverFrom = product.ValidFrom,
						@FromDate = product.ValidFrom,
						@CoverTo = product.ValidTo
			FROM PurchasedProduct product WITH (NOLOCK)
			WHERE product.PurchasedProductID = @PurchasedProductID

			IF @PaymentIntervalChangeID = 4 -- from annually to monthly
			BEGIN

				--1. find the prorata amount left to pay

				INSERT INTO @PPPSSummary
				EXEC dbo._C600_PurchasedProductBillingSummary @PurchasedProductID
				
				SELECT @AnnualPremium=s.TotalAnnualPremium, 
						@AnnualPremiumVAT=s.TotalAnnualPremiumVAT,
						@Paid=s.Paid,
						@PaidVAT=s.PaidVAT
				FROM @PPPSSummary s 

				-- delete existing unpaid rows 
				DELETE FROM PurchasedProductPaymentSchedule
				WHERE PurchasedProductID=@PurchasedProductID
				AND PaymentStatusID=1
				
				--build a schedule
				/*NB PPET-73 Transaction fee is worked out in the following proc*/
				EXEC dbo.PurchasedProduct__CreatePaymentSchedule @PurchasedProductID
				
				-- set monthly rows to ignore if they are before the change date 
				DELETE FROM PurchasedProductPaymentSchedule
				WHERE PurchasedProductID=@PurchasedProductID
					AND CoverTo<@AdjustmentDate
					AND PaymentStatusID=1
			
				-- calculate any first month adjustment
				SELECT @MonthsFutureDue=SUM(s.PaymentGross), @MonthsFutureDueVAT=SUM(s.PaymentVAT) 
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductID=@PurchasedProductID
					AND s.PaymentStatusID=1
					AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID
					
				SELECT @FirstMonthAdjustment=(@AnnualPremium - @Paid) - @MonthsFutureDue
				SELECT @FirstMonthAdjustmentVAT=(@AnnualPremiumVAT - @PaidVAT)- @MonthsFutureDueVAT
				
				-- get dates
				SELECT @PurchasedProductPaymentScheduleID=MIN(s.PurchasedProductPaymentScheduleID) 
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductID=@PurchasedProductID
					AND s.PaymentStatusID=1
					AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID

				SELECT @MonthCoverFrom=s.CoverFrom, @MonthCoverTo=s.CoverTo, @PaymentDate=s.PaymentDate, @ActualCollectionDate=s.ActualCollectionDate
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID
								
				-- add any first month adjustment row
				IF @FirstMonthAdjustment <> 0
				BEGIN
									
					INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID)
					VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @ActualCollectionDate, @MonthCoverFrom, @MonthCoverTo, @PaymentDate, @FirstMonthAdjustment-@FirstMonthAdjustmentVAT, @FirstMonthAdjustmentVAT, @FirstMonthAdjustment, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(),2)
	
					SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
					
					UPDATE PurchasedProductPaymentSchedule 
					SET PurchasedProductPaymentScheduleParentID=PurchasedProductPaymentScheduleID 
					WHERE PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID
				
				END
			
			END

			IF @PaymentIntervalChangeID = 5 -- from monthly to annually
			BEGIN

				-- find the prorata amount left to pay
				INSERT INTO @PPPSSummary
				EXEC dbo._C384_PurchasedProductBillingSummary @PurchasedProductID

				SELECT @FutureDue=s.TotalDue-s.TotalOverdue, 
						@FutureDueVAT=s.TotalDueVAT-s.TotalOverdueVAT
				FROM @PPPSSummary s 

				-- get dates
				SELECT @PurchasedProductPaymentScheduleID=MIN(s.PurchasedProductPaymentScheduleID) 
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductID=@PurchasedProductID
					AND @AdjustmentDate<=s.CoverTo
					AND s.PaymentStatusID=1
					AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID

				SELECT @CoverFrom=s.CoverFrom, @PaymentDate=s.PaymentDate, @ActualCollectionDate=s.ActualCollectionDate
				FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
				WHERE s.PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID

				-- delete any uncollected months entries
				DELETE FROM PurchasedProductPaymentSchedule
				WHERE PurchasedProductID=@PurchasedProductID
					AND @AdjustmentDate<=CoverTo
					AND PaymentStatusID=1
					AND PurchasedProductPaymentScheduleID=PurchasedProductPaymentScheduleParentID
				
				-- add new row for the remaining annual payment
				IF @FutureDue <> 0
				BEGIN
				
					SELECT @StateCode = c.County
					FROM Customers c WITH (NOLOCK)
					WHERE c.CustomerID = @CustomerID

					SELECT @PaymentTypeLuli = 69931 /*Credit Card*/

					SELECT @PaymentMethod = 69944 /*Annually*/

					SELECT @TransactionFee = dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod)

					INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID, TransactionFee)
					VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @ActualCollectionDate, @CoverFrom, @CoverTo, @PaymentDate, @FutureDue-@FutureDueVAT, @FutureDueVAT, @FutureDue, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(), 1, @TransactionFee)
	
					SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
					
					UPDATE PurchasedProductPaymentSchedule 
					SET PurchasedProductPaymentScheduleParentID=PurchasedProductPaymentScheduleID 
					WHERE PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID
				
				END

			END
			
			--rebuild customer payment schedule 
			EXEC dbo.Billing__RebuildCustomerPaymentSchedule @AccountID, @FromDate, @WhoCreated     

			SET @Success = 1
	      
		END TRY  
		BEGIN CATCH  

			UPDATE PurchasedProduct
			SET PaymentScheduleSuccessfullyCreated=0, PaymentScheduleCreatedOn=NULL
			WHERE PurchasedProductID=@PurchasedProductID
		    
			SET @Success = 0
		    
		END CATCH  
	
	END
	ELSE
	BEGIN
	
		-- we don't handle change to the payment interval supplied
		SET @Success = 0
	
	END		  
		  
	SELECT @Success Success
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangePaymentInterval__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangePaymentInterval__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangePaymentInterval__CreateFromThirdPartyFields] TO [sp_executeall]
GO
