SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Chart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_Insert]
(

	@ChartID int    OUTPUT,

	@QueryID int   ,

	@ClientID int   ,

	@ChartTitle varchar (150)  ,

	@ChartDescription varchar (MAX)  ,

	@ChartTypeID int   ,

	@XAxisColumn varchar (100)  ,

	@CreatedBy int   ,

	@CreatedOn datetime   ,

	@LastEditedBy int   ,

	@LastEditedOn datetime   
)
AS


				
				INSERT INTO [dbo].[Chart]
					(
					[QueryID]
					,[ClientID]
					,[ChartTitle]
					,[ChartDescription]
					,[ChartTypeID]
					,[XAxisColumn]
					,[CreatedBy]
					,[CreatedOn]
					,[LastEditedBy]
					,[LastEditedOn]
					)
				VALUES
					(
					@QueryID
					,@ClientID
					,@ChartTitle
					,@ChartDescription
					,@ChartTypeID
					,@XAxisColumn
					,@CreatedBy
					,@CreatedOn
					,@LastEditedBy
					,@LastEditedOn
					)
				-- Get the identity value
				SET @ChartID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Insert] TO [sp_executeall]
GO
