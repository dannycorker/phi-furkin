SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-11-02
-- Description:	Save Field Value By ThirdPartyID, LeadTypeID and Object
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SimpleValueIntoThirdPartyField]
	@ThirdPartyFieldID INT,
	@DetailValue VARCHAR(2000),
	@ObjectID INT,
	@ClientPersonnelID INT,
	@LeadTypeID INT,
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @DetailFieldID INT
	
	SELECT @DetailFieldID = tpm.DetailFieldID
	FROM ThirdPartyFieldMapping tpm WITH (NOLOCK)
	WHERE tpm.ThirdPartyFieldID = @ThirdPartyFieldID
	AND tpm.LeadTypeID = @LeadTypeID
	AND tpm.ClientID = @ClientID

	IF @DetailFieldID > 0
	BEGIN
	
		EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @DetailValue, @ObjectID, @ClientPersonnelID
	
	END
	ELSE BEGIN
	
		RAISERROR('Could not locate DetailFieldID',16,100)
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoThirdPartyField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SimpleValueIntoThirdPartyField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoThirdPartyField] TO [sp_executeall]
GO
