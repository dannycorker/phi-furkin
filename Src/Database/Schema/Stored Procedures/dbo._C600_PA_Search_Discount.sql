SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get all the base discounts
-- Mods
--	2015-10-29 DCM Added 'UNION' to include interrogation of rules engine
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_Discount]
(
	@ClientId INT,
	@ClientPersonnelId INT,
	@Filter VARCHAR(50)
)
AS
BEGIN

	DECLARE @DATA TABLE (ResourceListID INT, 
						[Code] VARCHAR(100),
						[Description] VARCHAR(500),
						[Value] MONEY,
						[IsPercentage] BIT,
						[Amount] MONEY,
						[DurationMonths] MONEY,
						[OfferCodeRule] VARCHAR(100)
						) 
	
	-- OLD METHOD -- in scheme tables
	INSERT INTO @DATA
	SELECT  
		ResourceListID [DiscountId], 
		[170057] [Code],
		[170058] [Description],
		CAST([170059] AS MONEY) [Value],
		CAST([170060] AS BIT)	[IsPercentage],
		CAST([170059] AS MONEY) [Amount],
		CAST([170061] AS MONEY) [DurationMonths],
		[170062] [OfferCodeRule]
	FROM
	(
		SELECT	rldv.ResourceListID, df.DetailFieldID,
			CASE QuestionTypeID
			WHEN 4 THEN lli.ItemValue
			WHEN 3 THEN CAST(rldv.ValueInt AS VARCHAR(50))
			WHEN 8 THEN CAST(rldv.ValueInt AS VARCHAR(50))
			WHEN 9 THEN CAST(rldv.ValueMoney AS VARCHAR(50))
			ELSE rldv.DetailValue
			END AS DetailValue
		FROM	DetailFields df WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID
		LEFT JOIN LookupListItems lli WITH (NOLOCK) ON lli.LookupListID = df.LookupListID AND lli.LookupListItemID = rldv.ValueInt
		WHERE	df.DetailFieldPageID = 19015
	) AS s
	PIVOT
	(
		MAX(DetailValue)
		FOR DetailFieldID IN ([170057],[170058],[170059],[170060],[170061],[170062])
	) AS p
	WHERE p.[170057] LIKE '%'+@Filter+'%'
	
	-- NEW METHOD -- In rules engine
	DECLARE @BrandID INT = 2000184
			
	DECLARE @AllMarketingCodesForAffinity TABLE ( SchemeID INT, MCStartDate DATE, MCEndDate DATE )

	INSERT INTO @AllMarketingCodesForAffinity 
	SELECT affsc.ResourceListID [SchemeID], CAST(po2.Val1 AS DATE) [MCStartDate], CAST(po2.Val2 AS DATE) [MCEndDate] 
	FROM RulesEngine_ParameterOptions po WITH (NOLOCK)
	INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON po.RuleParameterID=rp.RuleParameterID
	INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON rp.RuleID=r.RuleID
	INNER JOIN RulesEngine_RuleParameters rp2 WITH (NOLOCK) ON rp2.RuleID=r.RuleID AND rp2.RuleParameterPreSetID IN (5)
	INNER JOIN RulesEngine_ParameterOptions po2 WITH (NOLOCK) ON rp2.RuleParameterID=po2.RuleParameterID
	INNER JOIN TableDetailValues tdvrs WITH (NOLOCK) ON tdvrs.ValueInt=r.RuleSetID AND tdvrs.DetailFieldID=170202 -- scheme using this ruleset
	INNER JOIN MatterDetailValues ps WITH (NOLOCK) ON tdvrs.MatterID=ps.MatterID AND ps.DetailFieldID=145689 -- policy scheme
	INNER JOIN ResourceListDetailValues affsc WITH (NOLOCK) ON ps.ValueInt=affsc.ResourceListID AND affsc.DetailFieldID=175269 -- scheme affinity (short code)
	INNER JOIN ResourceListDetailValues affaffsc WITH (NOLOCK) ON affsc.ValueInt=affaffsc.ValueInt AND affaffsc.DetailFieldID=170127 -- affinity (short code)
	WHERE po.Val1=@Filter
	AND affaffsc.ResourceListID=@BrandID

	INSERT INTO @DATA
	SELECT vmc.SchemeID, @Filter, '',0,0,0,0,'' 
	FROM @AllMarketingCodesForAffinity vmc
	WHERE dbo.fn_GetDate_Local() BETWEEN vmc.MCStartDate AND vmc.MCEndDate
	
	SELECT * FROM @DATA	
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_Discount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_Discount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_Discount] TO [sp_executeall]
GO
