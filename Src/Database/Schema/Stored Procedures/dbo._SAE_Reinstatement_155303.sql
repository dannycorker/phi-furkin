SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Reinstatement
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Reinstatement_155303]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@ValueDate						DATE
			,@ValueInt						INT
			,@VarcharDate					VARCHAR(10)
			,@PurchasedProductID			INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


/* begin Reinstate policy */
	/* NB Keep this block below re-rating block so that new row is added to the premium detail history table 
	   before the cancellation date is cleared */
	   
	/* ****TODO**** 
	  check that re-instatement opens up a collection thread on the case (i.e. collect recurring payment leadevent available to follow up)
	  because these may have been closed down on cancellation.  Depending on hopw cancellation is applied
	
	*/ 
	IF @EventTypeID IN (155303)
	BEGIN
		
		SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(170055,@MatterID) /*Cancellation Date*/ 
		SELECT @ValueInt = dbo.fnGetSimpleDvAsInt (178289,@WhoCreated)
		
		IF DATEDIFF(DAY,@ValueDate,dbo.fn_GetDate_Local()) > @ValueINT
		BEGIN 
			
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event, date of cancellation is too far in the past. Please pass to manager</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			
		END
		ELSE 
		IF EXISTS (SELECT * FROM LeadDetailValues ldv WITH ( NOLOCK ) where ldv.DetailFieldID = 177473 and ldv.DetailValue = 'true' and ldv.LeadID = @LeadID) /*If policy is still in underwriting*/
		BEGIN 
		
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event, the policy never exited underwriting process. Policy cannot be reinstated</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		
		END 
		ELSE 		
		BEGIN 
				
			-- morph to in-process event ( i.e. 'begin - Reinstate policy' to 'Reinstate policy'
			UPDATE LeadEvent
			SET EventTypeID = 156786, WhenFollowedUp = NULL, NextEventID = NULL
			WHERE LeadEventID = @LeadEventID
						
			-- update adjustment fields (blank for all except date which equals getdate
			EXEC _C00_SimpleValueIntoField 175379,'',@MatterID,@WhoCreated  -- value
			EXEC _C00_SimpleValueIntoField 175345,'',@MatterID,@WhoCreated  -- type
			EXEC _C00_SimpleValueIntoField 177387,'',@MatterID,@WhoCreated  -- adjustments
			SELECT @VarcharDate =CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
			EXEC _C00_SimpleValueIntoField 175442,@VarcharDate,@MatterID,@WhoCreated  -- adjustments

			/*
				#43064 2017-06-15 ACE (TFO)
				Grab the last PPID for this matter and stick it into the Purchased Product ID field on the Billing system helper fields page
			*/
			SELECT TOP 1 @PurchasedProductID = pp.PurchasedProductID
			FROM PurchasedProduct pp WITH (NOLOCK)
			WHERE pp.ObjectID = @MatterID
			ORDER BY pp.PurchasedProductID DESC
			
			EXEC _C00_SimpleValueIntoField 177031,@PurchasedProductID,@MatterID,@WhoCreated  -- Purchased Product ID


			-- add calculation event
			INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
			SELECT TOP(1) @ClientID, @CustomerID, @LeadID, @CaseID, @LeadEventID, @EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156888, @AqAutomation, 1, 5, 0, 5
		END
	END



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Reinstatement_155303] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Reinstatement_155303] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Reinstatement_155303] TO [sp_executeall]
GO
