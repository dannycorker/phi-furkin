SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-16
-- Description:	 Backfill workflow task for a particular event
-- =============================================
CREATE PROCEDURE [dbo].[_C00_BackFillWorkflowTask]
@EventTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ClientID int
	
	SELECT @ClientID = e.ClientID
	FROM EventType e WITH (NOLOCK)
	WHERE e.EventTypeID = @EventTypeID

	/*Batch 5235 is a client 0 batch (required for the workflow task table)*/
	INSERT INTO WorkflowTask (WorkflowGroupID, AutomatedTaskID, Priority, AssignedTo, AssignedDate, LeadID, CaseID, EventTypeID, ClientID, FollowUp, Important, CreationDate, Escalated, EscalatedBy, EscalationReason, EscalationDate, Disabled, DisabledReason, DisabledDate)
	SELECT WorkflowGroupID, 5235, e.Priority, NULL, NULL, le.LeadID, le.CaseID, CASE e.FollowUp WHEN 0 THEN @EventTypeID ELSE e.EventTypeToAdd END, e.ClientID, e.FollowUp, e.Important, dbo.fn_GetDate_Local(), 0, NULL, NULL, NULL, 0, NULL, NULL
	FROM EventTypeAutomatedWorkflow e WITH (NOLOCK)
	INNER JOIN Cases ca WITH (NOLOCK) ON ca.ClientID = @ClientID
	INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = ca.LatestInProcessLeadEventID and le.EventTypeID = e.EventTypeID 
	WHERE e.EventTypeID = @EventTypeID
	--AND NOT EXISTS (
	--	SELECT * 
	--	FROM WorkflowTask w WITH (NOLOCK)
	--	WHERE w.CaseID = ca.CaseID 
	--	and w.AutomatedTaskID = 5235
	--	and w.EventTypeID = @EventTypeID
	--)

	SELECT @@ROWCOUNT

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BackFillWorkflowTask] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BackFillWorkflowTask] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BackFillWorkflowTask] TO [sp_executeall]
GO
