SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the InvoiceMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMapping_Get_List]

AS


				
				SELECT
					[InvoiceMappingID],
					[ClientID],
					[LeadTypeID],
					[SourceInvoiceNumberField],
					[SourceTotalClaimAmountField],
					[SourceFeesToPostToSageNetField],
					[SourceFeesToPostToSageVatField],
					[InvoiceTypeCode],
					[CreditNoteTypeCode],
					[InvoiceNumberField],
					[DateField],
					[DatePostedField],
					[DetailsField],
					[SentToSageField],
					[NominalCodeField],
					[TaxCodeField],
					[InvoiceNetAmountField],
					[InvoiceTaxAmountField],
					[TypeField],
					[MatterIDField],
					[CaseNumberField],
					[CreditNoteNetAmountField],
					[CreditNoteTaxAmountField],
					[NominalCode],
					[TaxCode],
					[TableRowsDetailFieldID],
					[TableRowsDetailFieldPageID],
					[FeesInvoicedToSageField],
					[FeesPaidFromSageField],
					[FeesInvoicedOtherField],
					[FeesPaidOtherField],
					[CaseBalanceField],
					[SageCompanyName],
					[CostCentre]
				FROM
					[dbo].[InvoiceMapping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMapping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Get_List] TO [sp_executeall]
GO
