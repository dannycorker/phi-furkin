SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the YellowPagesAreaCodes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[YellowPagesAreaCodes_GetByYellowPagesAreaCodeID]
(

	@YellowPagesAreaCodeID int   
)
AS


				SELECT
					[YellowPagesAreaCodeID],
					[YellowPagesAreaCode],
					[PostCode]
				FROM
					[dbo].[YellowPagesAreaCodes] WITH (NOLOCK) 
				WHERE
										[YellowPagesAreaCodeID] = @YellowPagesAreaCodeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_GetByYellowPagesAreaCodeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[YellowPagesAreaCodes_GetByYellowPagesAreaCodeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_GetByYellowPagesAreaCodeID] TO [sp_executeall]
GO
