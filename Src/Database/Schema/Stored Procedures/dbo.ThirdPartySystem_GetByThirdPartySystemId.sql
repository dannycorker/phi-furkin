SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartySystem table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystem_GetByThirdPartySystemId]
(

	@ThirdPartySystemId int   
)
AS


				SELECT
					[ThirdPartySystemId],
					[SystemName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartySystem] WITH (NOLOCK) 
				WHERE
										[ThirdPartySystemId] = @ThirdPartySystemId
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_GetByThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem_GetByThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_GetByThirdPartySystemId] TO [sp_executeall]
GO
