SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeEquation table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeEquation_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeEquationID int   = null ,

	@ClientID int   = null ,

	@EventTypeID int   = null ,

	@DetailFieldID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeEquationID]
	, [ClientID]
	, [EventTypeID]
	, [DetailFieldID]
    FROM
	[dbo].[EventTypeEquation] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeEquationID] = @EventTypeEquationID OR @EventTypeEquationID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeEquationID]
	, [ClientID]
	, [EventTypeID]
	, [DetailFieldID]
    FROM
	[dbo].[EventTypeEquation] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeEquationID] = @EventTypeEquationID AND @EventTypeEquationID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Find] TO [sp_executeall]
GO
