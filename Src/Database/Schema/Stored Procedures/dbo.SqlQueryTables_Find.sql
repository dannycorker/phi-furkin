SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryTables table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryTableID int   = null ,

	@ClientID int   = null ,

	@SqlQueryID int   = null ,

	@SqlQueryTableName varchar (50)  = null ,

	@TableAlias varchar (50)  = null ,

	@TableDisplayOrder int   = null ,

	@JoinType varchar (25)  = null ,

	@JoinText varchar (500)  = null ,

	@JoinTableID int   = null ,

	@JoinRTRID int   = null ,

	@TempTableID int   = null ,

	@TempJoinTableID int   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryTableID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryTableName]
	, [TableAlias]
	, [TableDisplayOrder]
	, [JoinType]
	, [JoinText]
	, [JoinTableID]
	, [JoinRTRID]
	, [TempTableID]
	, [TempJoinTableID]
	, [SourceID]
    FROM
	[dbo].[SqlQueryTables] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryTableID] = @SqlQueryTableID OR @SqlQueryTableID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryID] = @SqlQueryID OR @SqlQueryID IS NULL)
	AND ([SqlQueryTableName] = @SqlQueryTableName OR @SqlQueryTableName IS NULL)
	AND ([TableAlias] = @TableAlias OR @TableAlias IS NULL)
	AND ([TableDisplayOrder] = @TableDisplayOrder OR @TableDisplayOrder IS NULL)
	AND ([JoinType] = @JoinType OR @JoinType IS NULL)
	AND ([JoinText] = @JoinText OR @JoinText IS NULL)
	AND ([JoinTableID] = @JoinTableID OR @JoinTableID IS NULL)
	AND ([JoinRTRID] = @JoinRTRID OR @JoinRTRID IS NULL)
	AND ([TempTableID] = @TempTableID OR @TempTableID IS NULL)
	AND ([TempJoinTableID] = @TempJoinTableID OR @TempJoinTableID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryTableID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryTableName]
	, [TableAlias]
	, [TableDisplayOrder]
	, [JoinType]
	, [JoinText]
	, [JoinTableID]
	, [JoinRTRID]
	, [TempTableID]
	, [TempJoinTableID]
	, [SourceID]
    FROM
	[dbo].[SqlQueryTables] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryTableID] = @SqlQueryTableID AND @SqlQueryTableID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryID] = @SqlQueryID AND @SqlQueryID is not null)
	OR ([SqlQueryTableName] = @SqlQueryTableName AND @SqlQueryTableName is not null)
	OR ([TableAlias] = @TableAlias AND @TableAlias is not null)
	OR ([TableDisplayOrder] = @TableDisplayOrder AND @TableDisplayOrder is not null)
	OR ([JoinType] = @JoinType AND @JoinType is not null)
	OR ([JoinText] = @JoinText AND @JoinText is not null)
	OR ([JoinTableID] = @JoinTableID AND @JoinTableID is not null)
	OR ([JoinRTRID] = @JoinRTRID AND @JoinRTRID is not null)
	OR ([TempTableID] = @TempTableID AND @TempTableID is not null)
	OR ([TempJoinTableID] = @TempJoinTableID AND @TempJoinTableID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Find] TO [sp_executeall]
GO
