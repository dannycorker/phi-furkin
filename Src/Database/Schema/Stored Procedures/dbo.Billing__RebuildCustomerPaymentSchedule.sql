SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgaan
-- Create date: 2016-09-08
-- Description:	Recreates the Customer Payment Schedule for Payment Status = New records 
--				for all records which have a paymentdate of today or in the future 
-- 2016-09-16 DCM Include both new & retry records from PPPS
-- 2016-09-16 DCM Set PaymentStatusID on new records
-- 2016-10-19 DCM include ActualCollectionDate when building new CPS
-- 2016-11-02 DCM reset new payment date & amount on all customer accounts
-- 2016-11-09 DCM added CPS backup
-- 2016-11-23 DCM added accountID match to update PPPS
-- 2016-12-09 DCM updated filter on rebuild schedule to PaymentStatusID IN (1,2,5) AND CustomerLedgerID IS NULL from PaymentStatusID IN (1,5)
-- 2017-02-22 DCM allow NULL FromDate to mean rebuild all relevant entries
-- 2017-06-27 JEL Added check in to make sure we are not loosing track of any payments during rebuild
-- 2018-03-23 JEL Added consideration for Account Expiry. We will send the cancellation request on the mandate after this date
-- 2018-11-13 JEL Changed AccountID logic to prefer the CT account than the CPS account
-- 2019-01-31 JEL Changed logic to check from date is earlier enough from PPPS to CPS as this proc deals with customer level no PPPS
-- 2020-02-23 CPS for JIRA AAG-XXX | Following a rebuild, the PurchasedProducts for this account should all have gross value = SUM of non-retry PPPS records
-- =============================================
CREATE PROCEDURE [dbo].[Billing__RebuildCustomerPaymentSchedule]
	@AccountID INT,
	@FromDate DATE,
	@WhoCreated INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE 
			@AllAccounts tvpIntInt,
			@ClientID INT,
			@CustomerID INT,
			@SetDateAccountID INT,
			@CollectionsMatterID INT,
			@LastPaymentOnAccount DATE 
				
			
		
	SELECT @ClientID=ClientID, 
			@CustomerID=CustomerID
	FROM Account WITH (NOLOCK) WHERE AccountID=@AccountID
		
	SELECT @CollectionsMatterID = a.ObjectID FROM Account a WITH ( NOLOCK ) 
	where a.AccountID = @AccountID
		
		
		
	/*Check we are not about to loose track of a payment because we've reconciled too early*/ 
	IF EXISTS (SELECT * FROM CustomerPaymentSchedule p WITH (NOLOCK) 
				WHERE p.PaymentDate < @FromDate 
				AND p.PaymentStatusID IN (1,2,5)
				AND p.CustomerLedgerID IS NULL
				AND p.CustomerID = @CustomerID) 
	BEGIN
		
		SELECT TOP 1 @FromDate = p.PaymentDate 
		FROM CustomerPaymentSchedule p WITH (NOLOCK) 
		WHERE p.PaymentDate < @FromDate 
		AND p.PaymentStatusID IN (1,2,5)
		AND p.CustomerLedgerID IS NULL
		AND p.CustomerID = @CustomerID
		ORDER BY p.PaymentDate ASC
		
	END 
				    
		
	-- remove none reconciled customer payment schedule records
	EXEC CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 1

	-- Insert the new customer payment schedule 
	INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, WhoCreated, WhenCreated, PaymentStatusID, ActualCollectionDate, ClientAccountID, RelatedObjectID,RelatedObjectTypeID )
	SELECT @ClientID, @CustomerID, ppps.AccountID, PaymentDate, SUM(PaymentNet) AS PaymentNet, SUM(PaymentVAT) AS PaymentVAT, SUM(PaymentGross) AS PaymentGross, @WhoCreated, dbo.fn_GetDate_Local(), 1, ActualCollectionDate, ppps.ClientAccountID, a.ObjectID, 2
	FROM dbo.PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
	INNER JOIN Account a WITH ( NOLOCK ) on a.AccountID = ppps.AccountID 
	WHERE (PaymentDate >= @FromDate OR @FromDate IS NULL) AND ppps.CustomerID=@CustomerID AND PaymentStatusID IN (1,2,5) AND CustomerLedgerID IS NULL 
	--WHERE PaymentDate >= @FromDate AND CustomerID=@CustomerID AND PaymentStatusID IN (1,5)
	GROUP BY PaymentDate, ppps.AccountID, ActualCollectionDate, ppps.ClientAccountID, a.ObjectID

	-- update the PurchasedProductPaymentSchedule for rows where the CustomerPaymentSchedule has been rebuild
	UPDATE PurchasedProductPaymentSchedule
	SET PurchasedProductPaymentSchedule.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	INNER JOIN dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK) ON pps.PaymentDate = cps.PaymentDate AND pps.CustomerID=cps.CustomerID
	WHERE (pps.PaymentDate >= @FromDate OR @FromDate IS NULL) AND pps.CustomerID=@CustomerID AND pps.PaymentStatusID IN (1,5) AND cps.PaymentStatusID=1 AND pps.AccountID=cps.AccountID
		
	/*JEL As this is called anytime the Payment Schedule is called we can use the last Actual Collection 
	Date adjusted 10 days forward to account for failures as the account expiry. */
	SELECT @LastPaymentOnAccount = dbo.fnAddWorkingDays(cps.ActualCollectionDate,10) 
	FROM CustomerPaymentSchedule cps WITH ( NOLOCK ) 
	WHERE cps.AccountID = @AccountID
	AND NOT EXISTS (SELECT * FROM CustomerPaymentSchedule sub_cps WITH ( NOLOCK ) 
					WHERE sub_cps.AccountID = @AccountID 
					AND sub_cps.ActualCollectionDate > cps.ActualCollectionDate) 
		
	SELECT @LastPaymentOnAccount = ISNULL(@LastPaymentOnAccount,dbo.fn_GetDate_Local()) 
		
	UPDATE Account 
	SET ExpiryDate = @LastPaymentOnAccount
	WHERE AccountID = @AccountID
		
	-- reset new payment date and amount on all customer accounts
	INSERT INTO @AllAccounts
	SELECT a2.AccountID,0 
	FROM Account a1 WITH (NOLOCK) 
	INNER JOIN Account a2 WITH (NOLOCK) ON a1.CustomerID=a2.CustomerID
	WHERE a1.AccountID=@AccountID

	WHILE EXISTS ( SELECT * FROM @AllAccounts WHERE ID2=0 )
	BEGIN
		
		SELECT @SetDateAccountID=ID1 FROM @AllAccounts WHERE ID2=0

		EXEC Account__SetDateAndAmountOfNextPayment @SetDateAccountID
			
		UPDATE @AllAccounts SET ID2=1 WHERE ID1=@SetDateAccountID
			
	END	

	---- 2020-02-23 CPS for JIRA AAG-XXX | Following a rebuild, the PurchasedProducts for this account should all have gross value = SUM of non-retry PPPS records
	--;WITH TotalPpps AS
	--(
	--SELECT	 pp.PurchasedProductID
	--		,SUM(ppps.PaymentNet  ) [PaymentNet]
	--		,SUM(ppps.PaymentVAT  ) [PaymentVAT]
	--		,SUM(ppps.PaymentGross) [PaymentGross]
	--FROM PurchasedProduct pp WITH (NOLOCK) 
	--INNER JOIN dbo.PurchasedProductPaymentSchedule ppps WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
	--WHERE pp.AccountID = @AccountID
	--AND ppps.PurchasedProductPaymentScheduleTypeID NOT IN ( 3 /*Scheduled Retry*/ )
	--GROUP BY pp.PurchasedProductID
	--)
	--UPDATE pp
	--SET  ProductCostGross	= ppps.PaymentGross
	--	,ProductCostVAT		= ppps.PaymentVAT
	--	,ProductCostNet		= ppps.PaymentNet
	--FROM PurchasedProduct pp WITH (NOLOCK)
	--INNER JOIN TotalPpps ppps on ppps.PurchasedProductID = pp.PurchasedProductID
	--WHERE pp.AccountID = @AccountID
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__RebuildCustomerPaymentSchedule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__RebuildCustomerPaymentSchedule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__RebuildCustomerPaymentSchedule] TO [sp_executeall]
GO
