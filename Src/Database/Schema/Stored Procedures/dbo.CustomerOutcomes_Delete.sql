SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomerOutcomes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOutcomes_Delete]
(

	@CustomerOutcomeID int   
)
AS


				DELETE FROM [dbo].[CustomerOutcomes] WITH (ROWLOCK) 
				WHERE
					[CustomerOutcomeID] = @CustomerOutcomeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOutcomes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Delete] TO [sp_executeall]
GO
