SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomerAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_Delete]
(

	@CustomerAnswerID int   
)
AS


				DELETE FROM [dbo].[CustomerAnswers] WITH (ROWLOCK) 
				WHERE
					[CustomerAnswerID] = @CustomerAnswerID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_Delete] TO [sp_executeall]
GO
