SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUser table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalUserID],
					[ClientID],
					[CustomerID],
					[Username],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[Enabled],
					[AttemptedLogins],
					[PortalUserGroupID]
				FROM
					[dbo].[PortalUser] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_GetByClientID] TO [sp_executeall]
GO
