SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIClientPersonnelCredential table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientPersonnelCredential_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[RPIClientPersonnelCredentialID],
					[ClientID],
					[ClientPersonnelID],
					[UserName],
					[Password],
					[AsUser],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[RPIClientPersonnelCredential] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientPersonnelCredential_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_GetByClientID] TO [sp_executeall]
GO
