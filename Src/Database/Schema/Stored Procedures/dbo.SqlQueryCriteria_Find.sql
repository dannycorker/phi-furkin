SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryCriteria table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteria_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryCriteriaID int   = null ,

	@ClientID int   = null ,

	@SqlQueryID int   = null ,

	@SqlQueryTableID int   = null ,

	@SqlQueryColumnID int   = null ,

	@CriteriaText varchar (1000)  = null ,

	@Criteria1 varchar (250)  = null ,

	@Criteria2 varchar (250)  = null ,

	@CriteriaName varchar (250)  = null ,

	@SubQueryID int   = null ,

	@SubQueryLinkType varchar (50)  = null ,

	@ParamValue varchar (250)  = null ,

	@TempTableID int   = null ,

	@TempColumnID int   = null ,

	@TempCriteriaID int   = null ,

	@IsSecurityClause bit   = null ,

	@CriteriaSubstitutions varchar (2000)  = null ,

	@IsParameterizable bit   = null ,

	@Comparison varchar (50)  = null ,

	@IsJoinClause bit   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryCriteriaID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryTableID]
	, [SqlQueryColumnID]
	, [CriteriaText]
	, [Criteria1]
	, [Criteria2]
	, [CriteriaName]
	, [SubQueryID]
	, [SubQueryLinkType]
	, [ParamValue]
	, [TempTableID]
	, [TempColumnID]
	, [TempCriteriaID]
	, [IsSecurityClause]
	, [CriteriaSubstitutions]
	, [IsParameterizable]
	, [Comparison]
	, [IsJoinClause]
	, [SourceID]
    FROM
	[dbo].[SqlQueryCriteria] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryCriteriaID] = @SqlQueryCriteriaID OR @SqlQueryCriteriaID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryID] = @SqlQueryID OR @SqlQueryID IS NULL)
	AND ([SqlQueryTableID] = @SqlQueryTableID OR @SqlQueryTableID IS NULL)
	AND ([SqlQueryColumnID] = @SqlQueryColumnID OR @SqlQueryColumnID IS NULL)
	AND ([CriteriaText] = @CriteriaText OR @CriteriaText IS NULL)
	AND ([Criteria1] = @Criteria1 OR @Criteria1 IS NULL)
	AND ([Criteria2] = @Criteria2 OR @Criteria2 IS NULL)
	AND ([CriteriaName] = @CriteriaName OR @CriteriaName IS NULL)
	AND ([SubQueryID] = @SubQueryID OR @SubQueryID IS NULL)
	AND ([SubQueryLinkType] = @SubQueryLinkType OR @SubQueryLinkType IS NULL)
	AND ([ParamValue] = @ParamValue OR @ParamValue IS NULL)
	AND ([TempTableID] = @TempTableID OR @TempTableID IS NULL)
	AND ([TempColumnID] = @TempColumnID OR @TempColumnID IS NULL)
	AND ([TempCriteriaID] = @TempCriteriaID OR @TempCriteriaID IS NULL)
	AND ([IsSecurityClause] = @IsSecurityClause OR @IsSecurityClause IS NULL)
	AND ([CriteriaSubstitutions] = @CriteriaSubstitutions OR @CriteriaSubstitutions IS NULL)
	AND ([IsParameterizable] = @IsParameterizable OR @IsParameterizable IS NULL)
	AND ([Comparison] = @Comparison OR @Comparison IS NULL)
	AND ([IsJoinClause] = @IsJoinClause OR @IsJoinClause IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryCriteriaID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryTableID]
	, [SqlQueryColumnID]
	, [CriteriaText]
	, [Criteria1]
	, [Criteria2]
	, [CriteriaName]
	, [SubQueryID]
	, [SubQueryLinkType]
	, [ParamValue]
	, [TempTableID]
	, [TempColumnID]
	, [TempCriteriaID]
	, [IsSecurityClause]
	, [CriteriaSubstitutions]
	, [IsParameterizable]
	, [Comparison]
	, [IsJoinClause]
	, [SourceID]
    FROM
	[dbo].[SqlQueryCriteria] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryCriteriaID] = @SqlQueryCriteriaID AND @SqlQueryCriteriaID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryID] = @SqlQueryID AND @SqlQueryID is not null)
	OR ([SqlQueryTableID] = @SqlQueryTableID AND @SqlQueryTableID is not null)
	OR ([SqlQueryColumnID] = @SqlQueryColumnID AND @SqlQueryColumnID is not null)
	OR ([CriteriaText] = @CriteriaText AND @CriteriaText is not null)
	OR ([Criteria1] = @Criteria1 AND @Criteria1 is not null)
	OR ([Criteria2] = @Criteria2 AND @Criteria2 is not null)
	OR ([CriteriaName] = @CriteriaName AND @CriteriaName is not null)
	OR ([SubQueryID] = @SubQueryID AND @SubQueryID is not null)
	OR ([SubQueryLinkType] = @SubQueryLinkType AND @SubQueryLinkType is not null)
	OR ([ParamValue] = @ParamValue AND @ParamValue is not null)
	OR ([TempTableID] = @TempTableID AND @TempTableID is not null)
	OR ([TempColumnID] = @TempColumnID AND @TempColumnID is not null)
	OR ([TempCriteriaID] = @TempCriteriaID AND @TempCriteriaID is not null)
	OR ([IsSecurityClause] = @IsSecurityClause AND @IsSecurityClause is not null)
	OR ([CriteriaSubstitutions] = @CriteriaSubstitutions AND @CriteriaSubstitutions is not null)
	OR ([IsParameterizable] = @IsParameterizable AND @IsParameterizable is not null)
	OR ([Comparison] = @Comparison AND @Comparison is not null)
	OR ([IsJoinClause] = @IsJoinClause AND @IsJoinClause is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteria_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Find] TO [sp_executeall]
GO
