SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2010-04-20
-- Description:	List all Cases fields for a Case or Lead
-- =============================================
CREATE PROCEDURE [dbo].[ca] 
	@CaseID int = NULL,
	@LeadID int = NULL
	WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;

	/* Grab a sample lead if only LeadType is specified */
	IF @CaseID IS NULL AND @LeadID IS NULL
	BEGIN
		SELECT 'Please enter CaseID or LeadID' as [Info]
	END
	ELSE
	BEGIN
		SELECT c.Fullname, ca.*, l.*, lt.* 
		FROM dbo.Cases ca WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = ca.LeadID  
		INNER JOIN dbo.LeadType lt WITH (NOLOCK) on lt.LeadTypeID = l.LeadTypeID 
		INNER JOIN dbo.Customers c WITH (NOLOCK) on c.CustomerID = l.CustomerID 
		WHERE (ca.CaseID = @CaseID OR @CaseID IS NULL)
		AND (ca.LeadID = @LeadID OR @LeadID IS NULL)
		
		SELECT 'Matters for this Case',* 
		FROM dbo.Matter m WITH (NOLOCK) 
		WHERE m.CaseID = @CaseID 
		or m.LeadID = @LeadID 
	END

END








GO
GRANT VIEW DEFINITION ON  [dbo].[ca] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ca] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ca] TO [sp_executeall]
GO
