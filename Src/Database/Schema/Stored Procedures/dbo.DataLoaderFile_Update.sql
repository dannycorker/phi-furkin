SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFile_Update]
(

	@DataLoaderFileID int   ,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@FileStatusID int   ,

	@SourceFileNameAndPath varchar (2000)  ,

	@TargetFileName varchar (2000)  ,

	@TargetFileLocation varchar (2000)  ,

	@FileFormatID int   ,

	@ScheduledDateTime datetime   ,

	@DataLoadedDateTime datetime   ,

	@UploadedBy int   ,

	@RowsInFile int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderFile]
				SET
					[ClientID] = @ClientID
					,[DataLoaderMapID] = @DataLoaderMapID
					,[FileStatusID] = @FileStatusID
					,[SourceFileNameAndPath] = @SourceFileNameAndPath
					,[TargetFileName] = @TargetFileName
					,[TargetFileLocation] = @TargetFileLocation
					,[FileFormatID] = @FileFormatID
					,[ScheduledDateTime] = @ScheduledDateTime
					,[DataLoadedDateTime] = @DataLoadedDateTime
					,[UploadedBy] = @UploadedBy
					,[RowsInFile] = @RowsInFile
				WHERE
[DataLoaderFileID] = @DataLoaderFileID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Update] TO [sp_executeall]
GO
