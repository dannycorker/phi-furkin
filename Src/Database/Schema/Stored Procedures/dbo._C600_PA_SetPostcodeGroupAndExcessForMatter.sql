SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-08-22
-- Description:	Take a Policy MatterID and set its postcode group and excess fields
-- MODS
-- JEL 2017-10-23 Add tracking for whether the excess has changed or not
-- JEL 2018-04-15 Capture and store TPL excess
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_SetPostcodeGroupAndExcessForMatter]
(
	 @PolicyMatterID	INT
	,@LeadEventID		INT = NULL
)
AS
BEGIN

	DECLARE  @PostcodeGroup		VARCHAR(10)
			,@Postcode			VARCHAR(10)
			,@ErrorMessage		VARCHAR(2000)
			,@WhoCreated		INT = 58552 /*Aquarium Automation*/
			,@PolicyLeadID		INT
			,@PreviousExcess	DECIMAL (18,2)
			,@PreviousCoPay     INT 
			,@CoPayPercent		INT

	SELECT @WhoCreated = le.WhoCreated
	FROM LeadEvent le WITH ( NOLOCK ) 
	WHERE le.LeadEventID = @LeadEventID
	
	/*First write the previous excess amount so the user can see if theres a change*/
	SELECT @PreviousExcess = ISNULL(dbo.fnGetSimpleDvAsMoney(175503,@PolicyMatterID),0)
	EXEC _C00_SimpleValueIntoField 179562,@PreviousExcess,@PolicyMatterID,@WhoCreated
	
	SELECT @PreviousCoPay = ISNULL(dbo.fnGetSimpleDvAsMoney(175504,@PolicyMatterID),0)
	EXEC _C00_SimpleValueIntoField 179566,@PreviousCoPay,@PolicyMatterID,@WhoCreated

	SELECT @Postcode = cu.PostCode, @PolicyLeadID = l.LeadID
	FROM Customers cu WITH ( NOLOCK ) 
	INNER JOIN Matter m WITH (NOLOCK) on m.CustomerID = cu.CustomerID
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID AND l.LeadTypeID = 1492 /*Policy Admin*/
	WHERE m.MatterID = @PolicyMatterID 
	/*!! NO SQL HERE !!*/
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No policy admin match found for MatterID ' + ISNULL(CONVERT(VARCHAR,@PolicyMatterID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	EXEC _C600_PA_GetPostcodeGroupFromPostcode @PostCode, 8426, @PostcodeGroup OUTPUT
	EXEC dbo._C00_SimpleValueIntoField 177489, @PostcodeGroup, @PolicyMatterID, @WhoCreated
	
	
	DECLARE	 @CurrentSchemeMatterID		INT
			,@BreedResourceListID		INT
			,@SpeciesLookupListItemID	INT
			,@PetDateOfBirth			DATE
			,@PolicyStartDate			DATE
			,@VoluntaryExcess			DECIMAL(18,2) = 0.00
			,@CompulsoryExcess			DECIMAL(18,2)
			,@TPLExcess					DECIMAL(18,2)
			,@SchemeResourceListID		INT
	
	SELECT	 @SchemeResourceListID	= dbo.fnGetSimpleDvAsInt (170034,@PolicyMatterID)	/*Current Policy*/
			,@PolicyStartDate		= dbo.fnGetSimpleDvAsDate(170036,@PolicyMatterID)	/*Policy Start Date*/
			,@BreedResourceListID	= dbo.fnGetSimpleDvAsInt (144272,@PolicyLeadID)		/*Pet Type (breed)*/
			,@PetDateOfBirth		= dbo.fnGetSimpleDvAsDate(144274,@PolicyLeadID)		/*Pet Date of Birth*/
			
	SELECT	 @SpeciesLookupListItemID = dbo.fnGetSimpleDvAsInt(144269,@BreedResourceListID) /*Pet Type (species)*/
	
	SELECT @CurrentSchemeMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies( @SchemeResourceListID
																				,@PolicyStartDate
																				,@SpeciesLookupListItemID)
	/*Pick Up Excess for Vet Fees RL*/
	SELECT @CompulsoryExcess = fn.Excess
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcessWithData( @CurrentSchemeMatterID
														,@PostcodeGroup
														,@BreedResourceListID
														,@PetDateOfBirth
														,@PolicyStartDate
														,@SpeciesLookupListItemID
														,@VoluntaryExcess
														,@PolicyStartDate
														,@PolicyStartDate
														,0,999) fn
	WHERE fn.Out_ResourceListID = 148140
	
	/*Pick Up Excess for Third Party Liability RL*/	
	SELECT @TPLExcess = fn.Excess
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcessWithData( @CurrentSchemeMatterID
														,@PostcodeGroup
														,@BreedResourceListID
														,@PetDateOfBirth
														,@PolicyStartDate
														,@SpeciesLookupListItemID
														,@VoluntaryExcess
														,@PolicyStartDate
														,@PolicyStartDate
														,0,999) fn
	WHERE fn.Out_ResourceListID = 148149

	EXEC dbo._C00_SimpleValueIntoField 175503, @CompulsoryExcess, @PolicyMatterID, @WhoCreated /*Compulsory excess*/
	EXEC dbo._C00_SimpleValueIntoField 170051, @TPLExcess, @PolicyMatterID, @WhoCreated /*Compulsory excess*/
	
	--SELECT @CoPayPercent = MAX(fn.ExcessPercentage) FROM dbo.fn_C00_1272_Policy_GetPolicyCoInsurance(@PolicyLeadID,@CurrentSchemeMatterID) fn
	
	--EXEC dbo._C00_SimpleValueIntoField 175504, @CoPayPercent, @PolicyMatterID, @WhoCreated /*Compulsory excess*/
	
	/*Write whether the Excess amount has changed since last document field update*/ 
	IF @PreviousExcess <> @CompulsoryExcess 
	BEGIN 
		EXEC dbo._C00_SimpleValueIntoField 179563, 5144, @PolicyMatterID, @WhoCreated /*Compulsory excess*/
	END 
	ELSE
	BEGIN
		EXEC dbo._C00_SimpleValueIntoField 179563, 5145, @PolicyMatterID, @WhoCreated /*Compulsory excess*/	
	END
	
	IF @PreviousCoPay <> @CoPayPercent 
	BEGIN 
		EXEC dbo._C00_SimpleValueIntoField 179567, 5144, @PolicyMatterID, @WhoCreated /*Compulsory excess*/
	END 
	ELSE
	BEGIN
		EXEC dbo._C00_SimpleValueIntoField 179567, 5145, @PolicyMatterID, @WhoCreated /*Compulsory excess*/	
	END
	

	EXEC _C00_LogIt 'Info', '[_C600_PA_SetPostcodeGroupAndExcessForMatter]', '@MatterID', @PolicyMatterID, @WhoCreated

	

	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_SetPostcodeGroupAndExcessForMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_SetPostcodeGroupAndExcessForMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_SetPostcodeGroupAndExcessForMatter] TO [sp_executeall]
GO
