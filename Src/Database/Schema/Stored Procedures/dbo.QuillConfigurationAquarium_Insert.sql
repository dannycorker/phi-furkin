SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuillConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationAquarium_Insert]
(

	@QuillConfigurationAquariumID int    OUTPUT,

	@Vendor varchar (50)  ,

	@APIKey uniqueidentifier   
)
AS


				
				INSERT INTO [dbo].[QuillConfigurationAquarium]
					(
					[Vendor]
					,[APIKey]
					)
				VALUES
					(
					@Vendor
					,@APIKey
					)
				-- Get the identity value
				SET @QuillConfigurationAquariumID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationAquarium_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Insert] TO [sp_executeall]
GO
