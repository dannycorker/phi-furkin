SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 2010-07-08
-- Description:	Gets a list of Portal User Groups and whether or not they have access to a particular PanelItemCharting record
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserGroup__GetAccessByPanelItemChartingID]
(
	@PanelItemChartingID int,
	@ClientID int	
)
	
AS

SET ANSI_NULLS OFF

	SELECT		pugpica.PortalUserGroupPanelItemChartingAccessID, 
				pug.PortalUserGroupID, 
				pug.GroupName, 
				COALESCE (pugpica.HasAccess, 0) AS 'Accessible' 
	FROM		dbo.PortalUserGroup pug WITH (NOLOCK)
	
	LEFT JOIN	dbo.PortalUserGroupPanelItemChartingAccess pugpica WITH (NOLOCK) 
				ON pugpica.PortalUserGroupID = pug.PortalUserGroupID
				AND (pugpica.PanelItemChartingID = @PanelItemChartingID OR pugpica.PanelItemChartingID IS NULL)
				
	WHERE		pug.ClientID = @ClientID


SELECT @@ROWCOUNT
SET ANSI_NULLS ON




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetAccessByPanelItemChartingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup__GetAccessByPanelItemChartingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup__GetAccessByPanelItemChartingID] TO [sp_executeall]
GO
