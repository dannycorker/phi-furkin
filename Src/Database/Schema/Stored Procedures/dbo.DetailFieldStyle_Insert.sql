SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailFieldStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldStyle_Insert]
(

	@DetailFieldStyleID int    OUTPUT,

	@ClientID int   ,

	@StyleName nvarchar (50)  ,

	@BackgroundColor nvarchar (50)  ,

	@ForegroundColor nvarchar (50)  ,

	@FontSize nvarchar (50)  ,

	@FontWeight nvarchar (50)  ,

	@Align nvarchar (50)  ,

	@Padding nvarchar (50)  ,

	@PaddingHorizontal nvarchar (5)  ,

	@TextDecoration nvarchar (50)  ,

	@FontStyle nvarchar (50)  ,

	@PaddingVertical nvarchar (5)  ,

	@Enabled bit   
)
AS


				
				INSERT INTO [dbo].[DetailFieldStyle]
					(
					[ClientID]
					,[StyleName]
					,[BackgroundColor]
					,[ForegroundColor]
					,[FontSize]
					,[FontWeight]
					,[Align]
					,[Padding]
					,[PaddingHorizontal]
					,[TextDecoration]
					,[FontStyle]
					,[PaddingVertical]
					,[Enabled]
					)
				VALUES
					(
					@ClientID
					,@StyleName
					,@BackgroundColor
					,@ForegroundColor
					,@FontSize
					,@FontWeight
					,@Align
					,@Padding
					,@PaddingHorizontal
					,@TextDecoration
					,@FontStyle
					,@PaddingVertical
					,@Enabled
					)
				-- Get the identity value
				SET @DetailFieldStyleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldStyle_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Insert] TO [sp_executeall]
GO
