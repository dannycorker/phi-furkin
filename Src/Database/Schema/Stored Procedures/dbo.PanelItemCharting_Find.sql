SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PanelItemCharting table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_Find]
(

	@SearchUsingOR bit   = null ,

	@PanelItemChartingID int   = null ,

	@ClientID int   = null ,

	@PanelItemChartingName varchar (250)  = null ,

	@PanelID int   = null ,

	@CreatedBy int   = null ,

	@ChartID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PanelItemChartingID]
	, [ClientID]
	, [PanelItemChartingName]
	, [PanelID]
	, [CreatedBy]
	, [ChartID]
    FROM
	[dbo].[PanelItemCharting] WITH (NOLOCK) 
    WHERE 
	 ([PanelItemChartingID] = @PanelItemChartingID OR @PanelItemChartingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PanelItemChartingName] = @PanelItemChartingName OR @PanelItemChartingName IS NULL)
	AND ([PanelID] = @PanelID OR @PanelID IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([ChartID] = @ChartID OR @ChartID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PanelItemChartingID]
	, [ClientID]
	, [PanelItemChartingName]
	, [PanelID]
	, [CreatedBy]
	, [ChartID]
    FROM
	[dbo].[PanelItemCharting] WITH (NOLOCK) 
    WHERE 
	 ([PanelItemChartingID] = @PanelItemChartingID AND @PanelItemChartingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PanelItemChartingName] = @PanelItemChartingName AND @PanelItemChartingName is not null)
	OR ([PanelID] = @PanelID AND @PanelID is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([ChartID] = @ChartID AND @ChartID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_Find] TO [sp_executeall]
GO
