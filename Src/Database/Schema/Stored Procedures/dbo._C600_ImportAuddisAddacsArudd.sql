SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-08
-- Description:	Import Auddis Transactions
-- MODS
-- JEL 2018-03-19 Changed inserts to new BACsError tables rather than basic tables
-- JEL 2018-03-21 Trigger the reconciliation procs
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
	CREATE PROCEDURE [dbo].[_C600_ImportAuddisAddacsArudd]
	@XMLRaw NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

--declare @XMLRaw NVARCHAR(MAX)=''

	DECLARE @BACSImportLeadID INT = 218,   -- UPDATE NEEDED specific
			@BACSImportMatterID INT = 233, -- UPDATE NEEDED specific
			@ClientID INT = dbo.fnGetPrimaryClientID()			   -- 600 specific              

	DECLARE @AuddisReport TABLE (Unid INT IDENTITY (1,1), BACSFileDate VARCHAR(10), UserNumber VARCHAR(2000), RecordType VARCHAR(2000), EffectiveDate VARCHAR(2000), Reference VARCHAR(2000), PayerName VARCHAR(2000), PayerAccountNumber VARCHAR(2000), PayerSortCode VARCHAR(2000), ReasonCode VARCHAR(2000), ASON VARCHAR(2000), TransactionCode VARCHAR(2000), OrigSortCode VARCHAR(2000), OrigAccountNumber VARCHAR(2000), OriginalProcDate VARCHAR(2000), OriginatorName VARCHAR(2000), Amount VARCHAR(2000))

	DECLARE @ImportedRows TABLE (UNID INT, TableRowID INT)
	
	DECLARE @XML XML
	
	DECLARE @ImportFileID VARCHAR(100), 
			@AdviceType VARCHAR(100)

	SELECT @Xml = CASE WHEN CHARINDEX('<?xml version="1.0" encoding="utf-8"?>',@XMLRaw) > 0 THEN 
		REPLACE(@XmlRaw, '<?xml version="1.0" encoding="utf-8"?>', '')
		ELSE
		REPLACE(@XmlRaw, '<?xml version="1.0" encoding="ISO-8859-1"?>', '')
		END
		
	IF (SELECT n.c.value('(@reportType)[1]' , 'varchar(2000)')
		FROM @XML.nodes('//BACSDocument/Data/ARUDD/Header') n(c)) IS NOT NULL
	BEGIN
	
		-- This is an ARUDD Report (collection failure)
		
		;with MessagingAdvice AS (
			SELECT  '' AS [BACSFileDate],
					'' AS [UserNumber],
					'' AS [RecordType],
					n.c.value('(@originalProcessingDate)[1]' , 'varchar(2000)') AS [EffectiveDate],
					--LTRIM(RTRIM(n.c.value('(@ref)[1]' , 'varchar(2000)'))) AS [Reference],
					n.c.value('(PayerAccount/@ref)[1]' , 'varchar(2000)') AS [Reference],
					n.c.value('(PayerAccount/@name)[1]' , 'varchar(2000)') AS [PayerName],
					n.c.value('(PayerAccount/@number)[1]' , 'varchar(2000)') AS [PayerAccountNumber],
					n.c.value('(PayerAccount/@sortCode)[1]' , 'varchar(2000)') AS [PayerSortCode],
					n.c.value('(@returnDescription)[1]' , 'varchar(2000)') AS [ReasonCode],
					'' AS [AOSN],
					n.c.value('(@transCode)[1]' , 'varchar(2000)') AS [TransactionCode],
					'' AS [OrigSortCode],
					'' AS [OrigAccountNumber],
					'' AS [OriginalProcDate],
					'' AS [OriginatorName],
					n.c.value('(@valueOf)[1]' , 'varchar(2000)') AS [Amount]
			FROM @XML.nodes('//BACSDocument/Data/ARUDD/Advice/OriginatingAccountRecords/OriginatingAccountRecord/ReturnedDebitItem') n(c)
		)
		insert into @AuddisReport
		select *
		from MessagingAdvice

		-- update BACS File Date
		UPDATE @AuddisReport set BACSFileDate=@XML.value('(/BACSDocument/Data/ARUDD/Header/@currentProcessingDate)[1]', 'varchar(2000)')
				
		-- replace description with code where possible so that later it can be interpreted
		UPDATE ar
		SET ReasonCode=CASE WHEN code.DetailValue IS NULL THEN ReasonCode ELSE code.DetailValue END
		FROM @AuddisReport ar
		LEFT JOIN ResourceListDetailValues descr WITH (NOLOCK) ON ar.ReasonCode=descr.DetailValue AND descr.DetailFieldID=170199	
		LEFT JOIN ResourceListDetailValues typ WITH (NOLOCK) ON descr.ResourceListID=typ.ResourceListID AND typ.DetailFieldID=170200 AND typ.ValueInt=69985 -- ARUDD	
		LEFT JOIN ResourceListDetailValues code WITH (NOLOCK) ON descr.ResourceListID=code.ResourceListID AND typ.DetailFieldID=170198	
		
		SELECT @AdviceType = 'ARUDD'
		SELECT @ImportFileID=@XML.value('(/BACSDocument/Data/ARUDD/Header/@reportType)[1]', 'varchar(2000)') +
										'.' + @XML.value('(/BACSDocument/Data/ARUDD/Header/@adviceNumber)[1]', 'varchar(2000)')
--select @AdviceType,@ImportFileID	
	END
	ELSE
	BEGIN

		-- ADDACS or AUDDIS
		
		;with MessagingAdvice AS (
			SELECT  '' AS [BACSFileDate],
					n.c.value('(@user-number)[1]' , 'varchar(2000)') AS [UserNumber],
					n.c.value('(@record-type)[1]' , 'varchar(2000)') AS [RecordType],
					n.c.value('(@effective-date)[1]' , 'varchar(2000)') AS [EffectiveDate],
					LTRIM(RTRIM(n.c.value('(@reference)[1]' , 'varchar(2000)'))) AS [Reference],
					n.c.value('(@payer-name)[1]' , 'varchar(2000)') AS [PayerName],
					n.c.value('(@payer-account-number)[1]' , 'varchar(2000)') AS [PayerAccountNumber],
					n.c.value('(@payer-sort-code)[1]' , 'varchar(2000)') AS [PayerSortCode],
					n.c.value('(@reason-code)[1]' , 'varchar(2000)') AS [ReasonCode],
					n.c.value('(@aosn)[1]' , 'varchar(2000)') AS [AOSN],
					n.c.value('(@transaction-code)[1]' , 'varchar(2000)') AS [TransactionCode],
					n.c.value('(@orig-sort-code)[1]' , 'varchar(2000)') AS [OrigSortCode],
					n.c.value('(@orig-account-number)[1]' , 'varchar(2000)') AS [OrigAccountNumber],
					n.c.value('(@original-proc-date)[1]' , 'varchar(2000)') AS [OriginalProcDate],
					n.c.value('(@originator-name)[1]' , 'varchar(2000)') AS [OriginatorName],
					'' AS [Amount]
			FROM @XML.nodes('//BACSDocument/Data/MessagingAdvices/MessagingAdvice') n(c)
		)

		insert into @AuddisReport
		select *
		from MessagingAdvice
		
		-- ensure transaction code is not null (it is not included in ADDACS files)	
		UPDATE @AuddisReport set TransactionCode='' WHERE TransactionCode IS NULL

		-- update BACS File Date
		UPDATE @AuddisReport set BACSFileDate=@XML.value('(/BACSDocument/Data/MessagingAdvices/MessagingHeader/@report-generation-date)[1]', 'varchar(2000)')

		SELECT @AdviceType=@XML.value('(/BACSDocument/Data/MessagingAdvices/MessagingHeader/@advice-type)[1]', 'varchar(2000)')
		SELECT @ImportFileID=@XML.value('(/BACSDocument/Data/MessagingAdvices/MessagingHeader/@envelope-sequence-number)[1]', 'varchar(2000)')

	END
	
	IF @AdviceType = 'ARUDD'
	BEGIN 

	
		INSERT INTO BACSPaymentFailures 
		(ClientID, 
		 FileName,
		 RowNum,
		 AccountNumber,
		 SortCode,
		 Name,
		 PaymentAmount,
		 BACsProcessedDate,
		 FailureReason,
		 FailureCode,
		 Reference,
		 ImportStatusID,
		 ImportedDate)
		 SELECT 
		 @ClientID,
		 @ImportFileID,
		 a.Unid,
		 a.PayerAccountNumber,
		 a.PayerSortCode,
		 a.PayerName,
		 a.Amount,
		 a.EffectiveDate,
		 a.ReasonCode,
		 a.TransactionCode,
		 a.Reference,
		 1, 
		 a.BACSFileDate		 
		 FROM @AuddisReport a
	
	END
	ELSE
	IF @AdviceType = 'AUDDIS'
	BEGIN 
	
		INSERT INTO BacsMandateFailures 
		(ClientID, 
		 FileType,
		 FileName,
		 RowNo,
		 AccountNumber,
		 SortCode,
		 Name,
		 NewAccountNumber,
		 NewSortCode,
		 NewName,
		 BACsProcessedDate,
		 FailureReason,
		 FailureCode,
		 Reference,
		 ImportStatusID,
		 ImportedDate)
		 SELECT 
		 @ClientID,
		 @AdviceType,
		 @ImportFileID,
		 a.Unid,
		 a.PayerAccountNumber,
		 a.PayerSortCode,
		 a.PayerName,
		 a.OrigAccountNumber,
		 a.OrigSortCode,
		 a.OriginatorName,		 
		 a.EffectiveDate,
		 a.TransactionCode,
		 a.ReasonCode,
		 a.Reference,
		 1, 
		 a.BACSFileDate		 
		 FROM @AuddisReport a

	
	END
	ELSE IF @AdviceType = 'ADDACS'
	BEGIN 
	
		INSERT INTO BacsMandateFailures 
		(ClientID, 
		 FileType,
		 FileName,
		 RowNo,
		 AccountNumber,
		 SortCode,
		 Name,
		 NewAccountNumber,
		 NewSortCode,
		 NewName,
		 BACsProcessedDate,
		 FailureReason,
		 FailureCode,
		 Reference,
		 ImportStatusID,
		 ImportedDate)
		 SELECT 
		 @ClientID,
		 @AdviceType,
		 @ImportFileID,
		 a.Unid,
		 a.PayerAccountNumber,
		 a.PayerSortCode,
		 a.PayerName,
		 a.OrigAccountNumber,
		 a.OrigSortCode,
		 a.OriginatorName,		 
		 a.EffectiveDate,
		 a.TransactionCode,
		 a.ReasonCode,
		 a.Reference,
		 1, 
		 a.BACSFileDate		 
		 FROM @AuddisReport a
	
	END 
	
	EXEC Billing__ReconcileMandateFailures @ClientID 
	EXEC Billing__ReconcilePaymentFailures @ClientID 

	SELECT ''

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportAuddisAddacsArudd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ImportAuddisAddacsArudd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportAuddisAddacsArudd] TO [sp_executeall]
GO
