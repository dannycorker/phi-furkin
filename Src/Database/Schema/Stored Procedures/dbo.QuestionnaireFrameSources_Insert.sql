SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuestionnaireFrameSources table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_Insert]
(

	@QuestionnaireFrameSourceID int    OUTPUT,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@PageNumber int   ,

	@SourceUrl varchar (555)  ,

	@FrameType int   
)
AS


				
				INSERT INTO [dbo].[QuestionnaireFrameSources]
					(
					[ClientID]
					,[ClientQuestionnaireID]
					,[PageNumber]
					,[SourceUrl]
					,[FrameType]
					)
				VALUES
					(
					@ClientID
					,@ClientQuestionnaireID
					,@PageNumber
					,@SourceUrl
					,@FrameType
					)
				-- Get the identity value
				SET @QuestionnaireFrameSourceID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Insert] TO [sp_executeall]
GO
