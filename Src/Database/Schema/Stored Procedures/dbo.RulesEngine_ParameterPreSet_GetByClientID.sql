SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-10-14
-- Description:	Returns all rule parameter presets
-- Modified:	2014-11-12	SB	Added sort order and included other rule sets
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterPreSet_GetByClientID]
(
	@ClientID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	RuleParameterPreSetID, Name, ParameterTypeID, Value, DataTypeID, ClientID, Parent, SourceID
	FROM	dbo.RulesEngine_RuleParameters_PreSet WITH (NOLOCK)
	WHERE	ClientID = @ClientID
	UNION 
	SELECT	(rs.RuleSetID * -1) AS RuleParameterPreSetID, 'Rule Set: ' + Name AS Name, 3 AS ParameterTypeID, 
			CAST(rs.RuleSetID AS VARCHAR) AS Value, 2 AS DataTypeID, ClientID, NULL AS Parent, NULL AS SourceID
	FROM	dbo.RulesEngine_RuleSets rs WITH (NOLOCK)
	WHERE	ClientID = @ClientID
	ORDER BY Name
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterPreSet_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterPreSet_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterPreSet_GetByClientID] TO [sp_executeall]
GO
