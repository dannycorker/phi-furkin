SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the WorkflowTaskCompleted table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTaskCompleted_Update]
(

	@WorkflowTaskCompletedID int   ,

	@ClientID int   ,

	@WorkflowTaskID int   ,

	@WorkflowGroupID int   ,

	@AutomatedTaskID int   ,

	@Priority int   ,

	@AssignedTo int   ,

	@AssignedDate datetime   ,

	@LeadID int   ,

	@CaseID int   ,

	@EventTypeID int   ,

	@FollowUp bit   ,

	@Important bit   ,

	@CreationDate datetime   ,

	@Escalated bit   ,

	@EscalatedBy int   ,

	@EscalationReason varchar (255)  ,

	@EscalationDate datetime   ,

	@Disabled bit   ,

	@CompletedBy int   ,

	@CompletedOn datetime   ,

	@CompletionDescription varchar (255)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[WorkflowTaskCompleted]
				SET
					[ClientID] = @ClientID
					,[WorkflowTaskID] = @WorkflowTaskID
					,[WorkflowGroupID] = @WorkflowGroupID
					,[AutomatedTaskID] = @AutomatedTaskID
					,[Priority] = @Priority
					,[AssignedTo] = @AssignedTo
					,[AssignedDate] = @AssignedDate
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[EventTypeID] = @EventTypeID
					,[FollowUp] = @FollowUp
					,[Important] = @Important
					,[CreationDate] = @CreationDate
					,[Escalated] = @Escalated
					,[EscalatedBy] = @EscalatedBy
					,[EscalationReason] = @EscalationReason
					,[EscalationDate] = @EscalationDate
					,[Disabled] = @Disabled
					,[CompletedBy] = @CompletedBy
					,[CompletedOn] = @CompletedOn
					,[CompletionDescription] = @CompletionDescription
				WHERE
[WorkflowTaskCompletedID] = @WorkflowTaskCompletedID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTaskCompleted_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_Update] TO [sp_executeall]
GO
