SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-12-2012
-- Description:	Inserts a call list record
-- =============================================
CREATE PROCEDURE [dbo].[__InsertCallListRecord]

	@ClientID INT,
	@LeadID INT,
	@CaseID INT,
	@RowID INT,
	@ListID INT,
	@SchemaName VARCHAR(2000),
	@Terminal VARCHAR(2000),
	@UploadedToUltraDateTime VARCHAR(2000),
	@LastSuppressedDateTime VARCHAR(2000),
	@LastUnsuppressedDateTime VARCHAR(2000),
	@Error VARCHAR(2000),
	@Suppressed VARCHAR(2000)

AS
BEGIN

		DECLARE @TableRowID INT,
			@DetailFieldPageID INT,
			@DetailFieldID INT,
			@354FieldID INT, -- RowID
			@355FieldID INT, -- ListID
			@356FieldID INT, -- Schema Name
			@357FieldID INT, -- CaseID
			@358FieldID INT, -- Terminal
			@359FieldID INT, -- Uploaded To Ultra Date Time
			@360FieldID INT, -- Last Suppressed Date Time
			@361FieldID INT, -- Last Unsuppressed Date Time
			@362FieldID INT, -- Error
			@363FieldID INT, -- Suppressed
			@LeadTypeID INT,
			@CustomerID INT
			
		
	SELECT @CustomerID=CustomerID, @LeadTypeID = LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID						

	SELECT @354FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 354
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @355FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 355
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @356FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 356
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @357FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 357
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @358FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 358
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @359FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 359
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @360FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 360
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @361FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 361
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @362FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 362
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @363FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 363
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID	
	
	SELECT @DetailFieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 354
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID

	DECLARE @LogEntry VARCHAR(2000)
	
	SELECT	@LogEntry = 
			  '@ClientID = '		+ ISNULL(CONVERT(VARCHAR,@ClientID),'NULL')		 + CHAR(13) + CHAR(10)
			+ '@CaseID = '			+ ISNULL(CONVERT(VARCHAR,@CaseID),'NULL')		 + CHAR(13) + CHAR(10)
			+ '@RowID = '			+ ISNULL(CONVERT(VARCHAR,@RowID),'NULL')		 + CHAR(13) + CHAR(10)
			+ '@ListID = '			+ ISNULL(CONVERT(VARCHAR,@ListID),'NULL')		 + CHAR(13) + CHAR(10)
			+ '@LeadID = '			+ ISNULL(CONVERT(VARCHAR,@LeadID),'NULL')		 + CHAR(13) + CHAR(10)
			+ '@DetailFieldID = '	+ ISNULL(CONVERT(VARCHAR,@DetailFieldID),'NULL') + CHAR(13) + CHAR(10)
			+ '@LeadTypeID = '		+ ISNULL(CONVERT(VARCHAR,@LeadTypeID),'NULL')	 + CHAR(13) + CHAR(10)
	
	exec _C00_LogIt 'Info', '__InsertCallListRecord', 'Test Log', @LogEntry, 26932 /*Cathal S*/
	
	
	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
	VALUES (@ClientID, @CustomerID, @DetailFieldID, @DetailFieldPageID, 1, 1)
	
	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	VALUES  (@ClientID, @CustomerID, @TableRowID, @354FieldID, CONVERT(VARCHAR(2000),@RowID)),
			(@ClientID, @CustomerID, @TableRowID, @355FieldID, CONVERT(VARCHAR(2000),@ListID)),
			(@ClientID, @CustomerID, @TableRowID, @356FieldID, @SchemaName),
			(@ClientID, @CustomerID, @TableRowID, @357FieldID, CONVERT(VARCHAR(2000),@CaseID)),
			(@ClientID, @CustomerID, @TableRowID, @358FieldID, CONVERT(VARCHAR(2000),@Terminal)),
			(@ClientID, @CustomerID, @TableRowID, @359FieldID, CONVERT(VARCHAR(2000),@UploadedToUltraDateTime)),
			(@ClientID, @CustomerID, @TableRowID, @360FieldID, CONVERT(VARCHAR(2000),@LastSuppressedDateTime)),	
			(@ClientID, @CustomerID, @TableRowID, @361FieldID, CONVERT(VARCHAR(2000),@LastUnsuppressedDateTime)),	
			(@ClientID, @CustomerID, @TableRowID, @362FieldID, CONVERT(VARCHAR(2000),@Error)),	
			(@ClientID, @CustomerID, @TableRowID, @363FieldID, CONVERT(VARCHAR(2000),@Suppressed))			
	
	EXEC __InsertCallHistoryRecord	@ClientID, 
									@LeadID, 
									@RowID, 
									'',		--TransactionID
									'',		--AgentFirstName
									'',		--AgentLastName
									'',		--UserName
									'',		--UserID -- RunAsUSer
									'',		--DateTime Of Call
									'',		--Deleted from Ultra
									@UploadedToUltraDateTime,
									@ListID,
									@SchemaName,
									@Error,
									'true',
									@CaseID
										
END


GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallListRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__InsertCallListRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallListRecord] TO [sp_executeall]
GO
