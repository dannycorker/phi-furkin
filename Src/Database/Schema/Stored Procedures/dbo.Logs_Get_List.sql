SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Logs table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_Get_List]

AS


				
				SELECT
					[LogID],
					[LogDateTime],
					[TypeOfLogEntry],
					[ClassName],
					[MethodName],
					[LogEntry],
					[ClientPersonnelID]
				FROM
					[dbo].[Logs] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Get_List] TO [sp_executeall]
GO
