SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditingCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_Update]
(

	@SqlQueryEditingCriteriaSetMemberID int   ,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryEditingCriteriaSetID int   ,

	@SqlQueryEditingColumnID int   ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditingCriteriaSetMember]
				SET
					[ClientID] = @ClientID
					,[SqlQueryEditingID] = @SqlQueryEditingID
					,[SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID
					,[SqlQueryEditingColumnID] = @SqlQueryEditingColumnID
					,[Criteria1] = @Criteria1
					,[Criteria2] = @Criteria2
				WHERE
[SqlQueryEditingCriteriaSetMemberID] = @SqlQueryEditingCriteriaSetMemberID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Update] TO [sp_executeall]
GO
