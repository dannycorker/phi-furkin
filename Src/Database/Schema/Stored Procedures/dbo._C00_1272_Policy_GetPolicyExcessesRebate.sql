SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-24
-- Description:	Gets the policy excesses rebate for the current active vet on the lead
-- Used by:		Calculation
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetPolicyExcessesRebate] 
(
	@MatterID INT,
	@CurrentPolicyMatterID INT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	IF @CurrentPolicyMatterID IS NULL
	BEGIN
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	END
	
	DECLARE @CorportateGroup INT
	SELECT @CorportateGroup = rdvCorp.ValueInt
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.LeadDetailValues ldvVet WITH (NOLOCK) ON l.LeadID = ldvVet.LeadID AND ldvVet.DetailFieldID = 146215
	INNER JOIN dbo.ResourceListDetailValues rdvCorp WITH (NOLOCK) ON ldvVet.ValueInt = rdvCorp.ResourceListID AND rdvCorp.DetailFieldID = 147430
	WHERE l.LeadID = @PolicyLeadID
	
	
	SELECT MIN(tdvRebate.ValueMoney) 
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvCorp WITH (NOLOCK) ON r.TableRowID = tdvCorp.TableRowID AND tdvCorp.DetailFieldID = 147431
	INNER JOIN dbo.TableDetailValues tdvRebate WITH (NOLOCK) ON r.TableRowID = tdvRebate.TableRowID AND tdvRebate.DetailFieldID = 147432
	WHERE r.DetailFieldID = 147433
	AND r.DetailFieldPageID = 16323
	AND tdvCorp.ValueInt = @CorportateGroup
	AND r.MatterID = @CurrentPolicyMatterID
	
	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicyExcessesRebate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetPolicyExcessesRebate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicyExcessesRebate] TO [sp_executeall]
GO
