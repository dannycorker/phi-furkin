SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2009-03-11
-- Description:	Get Next Scheduled DataLoaderFile 
-- 2015-05-21 LB Added Check to stop disabled maps from attempting to load.
-- =============================================
CREATE PROCEDURE [dbo].[DataLoaderFile__GetNextScheduledFile]
	
AS

BEGIN

	SET NOCOUNT ON;

	SELECT TOP 1 dlf.DataLoaderFileID,
	dlf.ClientID,
	dlf.DataLoaderMapID,
	dlf.FileStatusID,
	dlf.SourceFileNameAndPath,
	dlf.TargetFileName,
	dlf.TargetFileLocation,
	dlf.FileFormatID,
	dlf.ScheduledDateTime,
	dlf.DataLoadedDateTime,
	dlf.UploadedBy,
	dlf.RowsInFile
	FROM dbo.DataLoaderFile dlf 
	INNER JOIN DataLoaderMap dm WITH (NOLOCK) on dm.DataLoaderMapID = dlf.DataLoaderMapID
	WHERE FileStatusID = 2 
	AND dm.EnabledForUse = 1
	AND TargetFileLocation = 'Inbox'
	AND ScheduledDateTime < dbo.fn_GetDate_Local()
	AND ScheduledDateTime > DATEADD(WEEK,-1,dbo.fn_GetDate_Local())
	ORDER BY ScheduledDateTime 
	
	SELECT @@ROWCOUNT

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile__GetNextScheduledFile] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile__GetNextScheduledFile] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile__GetNextScheduledFile] TO [sp_executeall]
GO
