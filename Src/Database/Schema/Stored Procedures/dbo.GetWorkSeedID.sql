SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green & Alex Elger
-- Create date: 2009-02-04
-- Description:	Create a new seed value for use in any of the WorkNNN tables
-- =============================================
CREATE PROCEDURE [dbo].[GetWorkSeedID] 
	@JobNotes varchar(500) = null
AS
BEGIN
	SET NOCOUNT ON;

	-- This work table only has one column (the identity column)
	-- so use the special "DEFAULT VALUES" clause to make sql server
	-- enter a new row. This creates a new int, which SCOPE_IDENTITY() then
	-- picks up and returns to the calling code.
	--INSERT workseed DEFAULT VALUES
	INSERT dbo.WorkSeed (WhenCreated, JobNotes) 
	SELECT dbo.fn_GetDate_Local(), @JobNotes 

	RETURN SCOPE_IDENTITY()
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetWorkSeedID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetWorkSeedID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetWorkSeedID] TO [sp_executeall]
GO
