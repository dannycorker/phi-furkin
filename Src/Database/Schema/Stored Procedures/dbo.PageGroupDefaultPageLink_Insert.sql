SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PageGroupDefaultPageLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroupDefaultPageLink_Insert]
(

	@PageGroupDefaultPageLinkID int    OUTPUT,

	@ClientID int   ,

	@PageGroupID int   ,

	@DetailFieldPageID int   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[PageGroupDefaultPageLink]
					(
					[ClientID]
					,[PageGroupID]
					,[DetailFieldPageID]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@PageGroupID
					,@DetailFieldPageID
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @PageGroupDefaultPageLinkID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroupDefaultPageLink_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Insert] TO [sp_executeall]
GO
