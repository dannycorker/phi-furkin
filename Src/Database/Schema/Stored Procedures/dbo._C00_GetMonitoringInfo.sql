SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-08-09
-- Description:	Get monitoring info for node red
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetMonitoringInfo]
	@Type INT = 1
AS
BEGIN

	SET NOCOUNT ON;

	/*
		Type 
		1 = Task
		2 = Log error
		3 = not yet known..

	*/

	IF @Type = 1
	BEGIN

		SELECT atr.*, at.Taskname
		FROM AutomatedTaskResult atr WITH (NOLOCK)
		INNER JOIN AutomatedTask at WITH (NOLOCK) ON at.TaskID = atr.TaskID
		WHERE atr.Complete = 0
		AND atr.Description <> 'Success'
		AND atr.RunDate > GETDATE()-1
		ORDER BY atr.AutomatedTaskResultID DESC

	END
	ELSE
	IF @Type = 2
	BEGIN

		SELECT TOP 250 *, DB_NAME() AS [DBName]
		FROM Logs l WITH (NOLOCK)
		WHERE l.TypeOfLogEntry = 'Error'
		AND l.LogDateTime > GETDATE()-1
		ORDER BY l.LogID DESC

	END
	ELSE IF @Type = 3
	BEGIN

		SELECT p.ClientID,
			SUM(ISNULL([Email Out], 0)) AS [EmailOut], 
			SUM(ISNULL([Email In], 0)) AS [EmailIn], 
			SUM(ISNULL([General], 0)) AS [General], 
			SUM(ISNULL([Letter Out], 0)) AS [LetterOut], 
			SUM(ISNULL([Letter In], 0)) AS [LetterIn], 
			SUM(ISNULL([Process Start], 0)) AS [ProcessStart], 
			SUM(ISNULL([SMS Out], 0)) AS [SMSOut], 
			SUM(ISNULL([SMS Survey Start], 0)) AS [SMSSurveyStart], 
			SUM(ISNULL([Case Summary], 0)) AS [CaseSummary], 
			SUM(ISNULL([Process Stopped], 0)) AS [ProcessStopped], 
			SUM(ISNULL(p.[Counter2HoursAgo], 0)) AS [Counter2HoursAgo], 
			SUM(ISNULL(p.[CounterLastHour], 0)) AS [CounterLastHour],
			((
			CONVERT(NUMERIC(18,2),SUM(ISNULL(p.[CounterLastHour], 0)))
			/
			NULLIF(CONVERT(NUMERIC(18,2),SUM(ISNULL(p.[Counter2HoursAgo], 1))), 0) 
			)*100.00) AS [PctDiff]
		FROM 
		(
			SELECT COUNT(*) AS [Counter], est.EventSubtypeName, le.ClientID,
				SUM(
				CASE 
					WHEN le.WhenCreated BETWEEN DATEADD(HH, -2, GETDATE()) AND DATEADD(HH, -1, GETDATE())  THEN 1 
					ELSE 0 
				END
				) AS [Counter2HoursAgo], 
				SUM(
				CASE 
					WHEN le.WhenCreated > DATEADD(HH, -1, GETDATE())  THEN 1 
					ELSE 0 
				END
				) AS [CounterLastHour]
			FROM LeadEvent le WITH (NOLOCK)
			Inner Join EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
			INNER JOIN EventSubtype est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID
			WHERE le.WhenCreated > DATEADD(HH, -2, GETDATE())
			GROUP BY est.EventSubtypeName, le.ClientID
		) AS SourceTable
		PIVOT 
		(
		MAX(Counter)
		FOR EventSubTypeName IN ([Email Out], [Email In], [General], [Letter Out], [Letter In], [Process Start], [SMS Out], [SMS Survey Start], [Case Summary], [Process Stopped])
		) AS p
		GROUP BY p.ClientID
		ORDER BY p.ClientID

	END
	ELSE IF @Type = 4
	BEGIN

		SELECT DB_NAME() AS [DBName], SUM(CASE
			WHEN DATEDIFF(MINUTE, l.LoginDateTime, GETDATE()) < 60 THEN 1 ELSE 0 END
		) AS [LoginsLastHour],
		SUM(CASE
			WHEN DATEDIFF(MINUTE, l.LoginDateTime, GETDATE()) > 60 THEN 1 ELSE 0 END
		) AS [LoginsPreviousHour], COUNT(*) AS [TotalLogins]
		FROM LoginHistory l WITH (NOLOCK)
		WHERE l.LoginDateTime > DATEADD(HH, -2, GETDATE())

	END
	ELSE IF @Type = 5
	BEGIN

		SELECT *, DB_NAME() AS [DBName]
		FROM AutomatedEventQueue l WITH (NOLOCK)
		WHERE l.ErrorDateTime > GETDATE()-1

	END
	ELSE 
	IF @Type = 6
	BEGIN
	
		SELECT *, DB_NAME() AS [DBName]
		FROM Logs l WITH (NOLOCK)
		WHERE l.LogDateTime > GETDATE()-1
		AND (l.ClassName IN ('EventManager') OR l.ClassName LIKE 'AquariumScheduler%')
		AND l.LogEntry NOT LIKE '%<br />%'
		ORDER BY 1
	
	END

END
GO
