SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ValidationCriteriaFieldTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriteriaFieldTypes_Insert]
(

	@ValidationCriteriaFieldTypeID int    OUTPUT,

	@Allowable nvarchar (25)  
)
AS


				
				INSERT INTO [dbo].[ValidationCriteriaFieldTypes]
					(
					[Allowable]
					)
				VALUES
					(
					@Allowable
					)
				-- Get the identity value
				SET @ValidationCriteriaFieldTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriteriaFieldTypes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_Insert] TO [sp_executeall]
GO
