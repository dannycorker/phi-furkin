SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-03
-- Description:	Create a PortalUserCase record with these values, unless it would be redundant
-- JWG 2014-01-31 #25493 Set the level of access (0=none, 1=view, 4=full). These have different meanings in the portal than the 
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserCase__GrantAccess]
	@PortalUserID int,
	@LeadID int,
	@CaseID int,
	@ClientID int, 
	@AccessLevel tinyint = 4
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		JWG 2014-01-31 #25493 Set the level of access (0=none, 1=view, 4=full). 
		These have different meanings in the portal than the main app though. View=Full in the portal, and None is achieved
		by the absence of PortalUserCase records.
	*/
	IF @AccessLevel = 0
	BEGIN
		/* Delete entries for this PortalUser, Lead and {one Case | all Cases} */
		DELETE dbo.PortalUserCase 
		WHERE PortalUserID = @PortalUserID 
		AND ClientID = @ClientID 
		AND LeadID = @LeadID 
		AND ((CaseID = @CaseID) OR (@CaseID IS NULL)) 
	END
	ELSE
	BEGIN
		/*
			Make sure that there isn't already a record for this LeadID and CaseID
			(or for all cases for this lead).
		*/
		IF NOT EXISTS (
			SELECT * 
			FROM dbo.PortalUserCase puc WITH (NOLOCK) 
			WHERE puc.PortalUserID = @PortalUserID
			AND (puc.LeadID = @LeadID)
			AND ((puc.CaseID = @CaseID) OR (puc.CaseID IS NULL))
			
		)
		BEGIN
			INSERT INTO PortalUserCase (PortalUserID, LeadID, CaseID, ClientID) 
			VALUES (@PortalUserID, @LeadID, @CaseID, @ClientID) 
		END
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GrantAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__GrantAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GrantAccess] TO [sp_executeall]
GO
