SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientQuestionnaireLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaireLayout_Delete]
(

	@ClientQuestionnaireLayoutID int   
)
AS


				DELETE FROM [dbo].[ClientQuestionnaireLayout] WITH (ROWLOCK) 
				WHERE
					[ClientQuestionnaireLayoutID] = @ClientQuestionnaireLayoutID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaireLayout_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Delete] TO [sp_executeall]
GO
