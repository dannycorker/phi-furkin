SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2016-03-09
-- Description:	Get a ClientPersonnel record with lookup "expanded"
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__GetExpandedByClientPersonnelID]
(
	@ClientPersonnelID INT
)
AS
BEGIN

	SELECT cp.*, t.Title, co.OfficeName, cpag.GroupName, cpm.UserName
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	LEFT JOIN dbo.Titles t WITH (NOLOCK) ON cp.TitleID = t.TitleID
	LEFT JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = cp.ClientOfficeID
	LEFT JOIN dbo.ClientPersonnelAdminGroups cpag WITH (NOLOCK) ON cpag.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID
	LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = cp.ManagerID
	WHERE cp.ClientPersonnelID = @ClientPersonnelID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetExpandedByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetExpandedByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetExpandedByClientPersonnelID] TO [sp_executeall]
GO
