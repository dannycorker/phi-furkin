SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the BankStatementAnalyserAccounts table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BankStatementAnalyserAccounts_Find]
(

	@SearchUsingOR bit   = null ,

	@BankStatementAnalyserAccountID int   = null ,

	@ClientID int   = null ,

	@BankStatementPageCount int   = null ,

	@CostPerPage money   = null ,

	@LastResetDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [BankStatementAnalyserAccountID]
	, [ClientID]
	, [BankStatementPageCount]
	, [CostPerPage]
	, [LastResetDate]
    FROM
	[dbo].[BankStatementAnalyserAccounts] WITH (NOLOCK) 
    WHERE 
	 ([BankStatementAnalyserAccountID] = @BankStatementAnalyserAccountID OR @BankStatementAnalyserAccountID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([BankStatementPageCount] = @BankStatementPageCount OR @BankStatementPageCount IS NULL)
	AND ([CostPerPage] = @CostPerPage OR @CostPerPage IS NULL)
	AND ([LastResetDate] = @LastResetDate OR @LastResetDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [BankStatementAnalyserAccountID]
	, [ClientID]
	, [BankStatementPageCount]
	, [CostPerPage]
	, [LastResetDate]
    FROM
	[dbo].[BankStatementAnalyserAccounts] WITH (NOLOCK) 
    WHERE 
	 ([BankStatementAnalyserAccountID] = @BankStatementAnalyserAccountID AND @BankStatementAnalyserAccountID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([BankStatementPageCount] = @BankStatementPageCount AND @BankStatementPageCount is not null)
	OR ([CostPerPage] = @CostPerPage AND @CostPerPage is not null)
	OR ([LastResetDate] = @LastResetDate AND @LastResetDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStatementAnalyserAccounts_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Find] TO [sp_executeall]
GO
