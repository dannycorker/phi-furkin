SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the HyperlinkStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget_Delete]
(

	@HyperlinkStandardTargetID int   
)
AS


				DELETE FROM [dbo].[HyperlinkStandardTarget] WITH (ROWLOCK) 
				WHERE
					[HyperlinkStandardTargetID] = @HyperlinkStandardTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_Delete] TO [sp_executeall]
GO
