SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-08-31
-- Description:	Saves the show output options for columns in the sql query editing tables
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__AddRemoveColumnOutput]
(
	@SqlQueryEditingID INT,
	@ColumnsToAdd dbo.tvpInt READONLY,
	@ColumnsToRemove dbo.tvpInt READONLY
)
AS


UPDATE c
SET c.ShowColumn = 1
FROM dbo.SqlQueryEditingColumns c
INNER JOIN @ColumnsToAdd a ON c.SqlQueryEditingColumnID = a.AnyID
WHERE c.SqlQueryEditingID = @SqlQueryEditingID

UPDATE c
SET c.ShowColumn = 0
FROM dbo.SqlQueryEditingColumns c
INNER JOIN @ColumnsToRemove r ON c.SqlQueryEditingColumnID = r.AnyID
WHERE c.SqlQueryEditingID = @SqlQueryEditingID







GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__AddRemoveColumnOutput] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__AddRemoveColumnOutput] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__AddRemoveColumnOutput] TO [sp_executeall]
GO
