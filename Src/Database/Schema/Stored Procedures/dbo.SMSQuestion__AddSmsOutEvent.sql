SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-03-2013
-- Description:	Creates a document type for the question and an sms out event for the question for each lead type
-- Updates:		2015-06-24	SB	Set the event subtype to be voice if the questionnaire is voice
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestion__AddSmsOutEvent]

	@ClientID INT,	
	@Question VARCHAR(960),
	@SMSQuestionID INT,	
	@WhoCreateUserID INT,
	@ToTarget VARCHAR(250) -- eg., '[!A:Mobile Number]' or even [!Mobile]

AS
BEGIN

	DECLARE @Events TABLE (EventtypeID INT)
	
	DECLARE @SMSQuestionnaireID INT
	DECLARE @QuestionnaireTitle VARCHAR(250)
																																																																																								   
	INSERT INTO DocumentType (ClientID,LeadTypeID,DocumentTypeName,DocumentTypeDescription,Header,Template,Footer,CanBeAutoSent,EmailSubject,EmailBodyText,InputFormat,OutputFormat,Enabled,RecipientsTo,RecipientsCC,RecipientsBCC,ReadOnlyTo,ReadOnlyCC,ReadOnlyBCC,SendToMultipleRecipients, MultipleRecipientDataSourceType,MultipleRecipientDataSourceID,SendToAllByDefault,ExcelTemplatePath,FromDetails,ReadOnlyFrom)
	VALUES (@ClientID,NULL,'SMS For Question' + CAST(@SMSQuestionID AS VARCHAR),'SMS For Question' + CAST(@SMSQuestionID AS VARCHAR),NULL,NULL,NULL,1,'',@Question,'HTML','SMS',1,@ToTarget,NULL,NULL,0,0,0,0,NULL,NULL,0,NULL,'',0)

	DECLARE @DocumentTypeID INT
	SET @DocumentTypeID= SCOPE_IDENTITY()
	
	UPDATE SMSQuestion
	SET DocumentTypeID=@DocumentTypeID
	WHERE SMSQuestionID=@SMSQuestionID	
	
	SELECT @SMSQuestionnaireID = SMSQuestionnaireID
	FROM SMSQuestion WITH (NOLOCK) 
	WHERE SMSQuestionID = @SMSQuestionID
	
	SELECT @QuestionnaireTitle = Title FROM SMSQuestionnaire WITH (NOLOCK) WHERE SMSQuestionnaireID=@SMSQuestionnaireID

	DECLARE @FolderID INT
	SELECT @FolderID=FolderID FROM Folder WHERE LEFT(@QuestionnaireTitle,50) = FolderName
	
	IF @FolderID IS NULL
	BEGIN
	
		INSERT INTO Folder (ClientID, FolderTypeID, FolderName, FolderDescription, WhenCreated, WhoCreated, Personal)
		VALUES (@ClientID, 3, LEFT(@QuestionnaireTitle,50),@QuestionnaireTitle,dbo.fn_GetDate_Local(),@WhoCreateUserID,0)
		
		SET @FolderID = SCOPE_IDENTITY()
		
	END
	
	INSERT INTO DocumentTypeFolderLink (DocumentTypeID, FolderID)
	VALUES (@DocumentTypeID, @FolderID)
	
	DECLARE @EventSubType INT = 13,
			@IsVoice BIT
			
	SELECT @IsVoice = IsVoice
	FROM dbo.SMSQuestionnaire WITH (NOLOCK) 
	WHERE SMSQuestionnaireID = @SMSQuestionnaireID
	
	IF @IsVoice = 1
	BEGIN
	
		SELECT @EventSubType = 26 
	
	END

	-- Add an event for each leadtype defined in SMSOutgoingPhoneNumber 
	INSERT INTO EventType (ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort, FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, AquariumEventAfterEvent, EventSubtypeID, DocumentTypeID, LeadTypeID, AllowCustomTimeUnits, InProcess, KeyEvent, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments, SignatureRequired, SignatureOverride, VisioX, VisioY, AquariumEventSubtypeID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	OUTPUT inserted.EventTypeID INTO @Events (EventtypeID)
	SELECT @ClientID,'SMS Out Event For Question ' + CAST(@SMSQuestionID AS VARCHAR),'SMS Out Event For Question ' + CAST(@SMSQuestionID AS VARCHAR),1,0,NULL,NULL,1,NULL,NULL,@EventSubType,@DocumentTypeID,s.LeadTypeID,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,@WhoCreateUserID,dbo.fn_GetDate_Local(),@WhoCreateUserID,dbo.fn_GetDate_Local()
	FROM SMSOutgoingPhoneNumber s WITH (NOLOCK) 
	WHERE SMSQuestionnaireID=@SMSQuestionnaireID

	-- Add each event to SAE
	INSERT INTO EventTypeSql (ClientID, EventTypeID, PostUpdateSql, IsNativeSql)
	SELECT @ClientID, e.EventtypeID, 'EXEC dbo._C00_PostSMSSurveyTableInsert @LeadEventID',0
	FROM @Events e	
	
	-- Map events to question
	INSERT INTO SMSQuestionEventType (ClientID, SMSQuestionID, EventTypeID)
	SELECT @ClientID, @SMSQuestionID, e.EventTypeID
	FROM @Events e
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__AddSmsOutEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion__AddSmsOutEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__AddSmsOutEvent] TO [sp_executeall]
GO
