SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the HyperlinkSpecialisedDetailFieldTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkDetailFieldID]
(

	@HyperlinkDetailFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[HyperlinkSpecialisedDetailFieldTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[RowField],
					[Notes]
				FROM
					[dbo].[HyperlinkSpecialisedDetailFieldTarget] WITH (NOLOCK) 
				WHERE
					[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_GetByHyperlinkDetailFieldID] TO [sp_executeall]
GO
