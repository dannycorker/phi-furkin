SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-07-2012
-- Description:	Finds a lead based upon the given mobile number
--
-- This Attempts to find a customer via a mobile telephone and a client id
-- Used for the Complaint Screen Demo
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FindCustomerViaMobileAndClientID]

	@MobilePhoneNumber VARCHAR(MAX),
	@ClientID INT,
	@LeadTypeID INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 1 MobileTelephone, c.CustomerID, l.LeadID, cs.CaseID, c.ClientID, m.MatterID, l.LeadTypeID
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	INNER JOIN dbo.Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID AND m.CaseID = cs.CaseID
	WHERE 
		  c.MobileTelephone <>'' AND 
		  REPLACE(MobileTelephone, ' ', '') = @MobilePhoneNumber		 
		  AND c.Test = 0
		  AND c.ClientID=@ClientID
		  AND l.LeadTypeID =@LeadTypeID
	ORDER BY cs.WhenCreated DESC

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerViaMobileAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindCustomerViaMobileAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerViaMobileAndClientID] TO [sp_executeall]
GO
