SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Logs table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_Insert]
(

	@LogID int    OUTPUT,

	@LogDateTime datetime   ,

	@TypeOfLogEntry varchar (6)  ,

	@ClassName varchar (512)  ,

	@MethodName varchar (512)  ,

	@LogEntry varchar (MAX)  ,

	@ClientPersonnelID int   
)
AS


				
				INSERT INTO [dbo].[Logs]
					(
					[LogDateTime]
					,[TypeOfLogEntry]
					,[ClassName]
					,[MethodName]
					,[LogEntry]
					,[ClientPersonnelID]
					)
				VALUES
					(
					@LogDateTime
					,@TypeOfLogEntry
					,@ClassName
					,@MethodName
					,@LogEntry
					,@ClientPersonnelID
					)
				-- Get the identity value
				SET @LogID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Insert] TO [sp_executeall]
GO
