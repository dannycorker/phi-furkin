SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- ALTER date: 2015-11-18
-- Description:	Calculate data storage by client for billing purposes
-- =============================================
CREATE PROCEDURE [dbo].[ClientDataUsage_CalcAll]
	@ClosePreviousRunInProgress BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
	@RunAsUser VARCHAR(50),
	@TableName SYSNAME,
	@SumFixedAndPrecision INT = 0, 
	@NullableColumnCount INT = 0, 
	@BytesForNullBitmap INT = 0, 
	@BitColumnCount INT = 0, 
	@BytesForBitColumns INT = 0, 
	@FixedOverheadPerRow INT = 4, 
	@VariableColumnList VARCHAR(MAX) = '', 
	@VariableColumnListSql VARCHAR(MAX) = '', 
	@DynamicSql VARCHAR(MAX) = '', 
	@InProgressTimeInSeconds INT = 0,
	@LastTimeInSeconds INT = 0,
	@RemainingTimeInSeconds INT = 0,
	@CurrentBillingRunID INT = 0, 
	@STAGE_STARTED VARCHAR(50) = 'Started',
	@STAGE_COMPLETE VARCHAR(50) = 'Complete'
	
	DECLARE @TablesOfInterest TABLE (TableName SYSNAME, Processed BIT)

	/*
		Start by checking to see if a run is already in progress:
	*/
	SELECT @RunAsUser = SUSER_NAME()
	
	IF EXISTS(SELECT * FROM dbo.ClientDataUsageBillingRun c WITH (NOLOCK) WHERE c.WhenFinished IS NULL)
	BEGIN
		
		/* If the "Kill Existing" flag is set then close the old run and carry on with the new one, otherwise go home early */
		IF @ClosePreviousRunInProgress = 0
		BEGIN
			SELECT 'The following calcs are already in progress. Please wait, or re-run using @ClosePreviousRunInProgress = 1' AS 'Calcs In Progress'
			
			BEGIN TRY
				SELECT TOP (1) @LastTimeInSeconds = h.TimeInSeconds 
				FROM dbo.ClientDataUsageBillingRunHistory h WITH (NOLOCK) 
				WHERE h.CurrentStage = @STAGE_COMPLETE 
				ORDER BY h.ClientDataUsageBillingRunID DESC
				
				SELECT TOP (1) @InProgressTimeInSeconds = DATEDIFF(second, c.WhenStarted, dbo.fn_GetDate_Local()) 
				FROM dbo.ClientDataUsageBillingRun c WITH (NOLOCK) 
				WHERE c.WhenFinished IS NULL 
				ORDER BY c.ClientDataUsageBillingRunID  DESC
				
				SELECT @RemainingTimeInSeconds = @LastTimeInSeconds - @InProgressTimeInSeconds
				
				SELECT 'Estimated time remaining from historical runs is ' + CAST(@RemainingTimeInSeconds AS VARCHAR) + ' second' + CASE WHEN ABS(@RemainingTimeInSeconds) = 1 THEN '' ELSE 's' END + CASE WHEN @RemainingTimeInSeconds < 1 THEN ' (already expected to be complete)' ELSE '' END AS 'Estimated Time Remaining'
				
			END TRY
			BEGIN CATCH
				SELECT 'Cannot calculate a finish time from historical runs - sorry' AS 'Estimated Time Remaining'
			END CATCH
			
			SELECT * 
			FROM dbo.ClientDataUsageBillingRun c WITH (NOLOCK) 
			WHERE c.WhenFinished IS NULL 
			ORDER BY c.ClientDataUsageBillingRunID 
			
			RETURN
		END
		
		
		/* Close all open billing runs, move to history and start afresh */
		EXEC dbo.ClientDataUsage_CloseAndArchive @BillingRunID = 0, @ArchivedBy = @RunAsUser
		
		
	END
	
	
	/* Start a billing run */
	INSERT INTO dbo.ClientDataUsageBillingRun(WhoStarted, WhenStarted, CurrentStage)
	SELECT @RunAsUser, dbo.fn_GetDate_Local(), @STAGE_STARTED 
	
	SELECT @CurrentBillingRunID = SCOPE_IDENTITY()
	
	/* ALTER a record for every client in this database */
	INSERT INTO dbo.ClientDataUsage(ClientDataUsageBillingRunID, ClientID)
	SELECT @CurrentBillingRunID, ClientID 
	FROM dbo.ClientInfo c 
	
	
	/* Now start totting up the data usage */
	INSERT INTO @TablesOfInterest(TableName) VALUES
	('Cases')
	,('CaseDetailValues')
	,('ClientDetailValues')
	,('ClientPersonnelDetailValues')
	,('Contact')
	,('ContactDetailValues')
	,('Customers')
	,('CustomerDetailValues')
	,('DetailValueHistory')
	,('DocumentType')
	,('Lead')
	,('LeadDetailValues')
	,('LeadEvent')
	,('Matter')
	,('MatterDetailValues')
	,('Objects')
	,('ObjectDetailValues')
	,('ResourceList')
	,('ResourceListDetailValues')
	,('SqlQuery')
	,('SqlQueryHistory')
	,('TableDetailValues')
	,('TableDetailValuesHistory')
	
	/* ALTER work records for all these tables and for all clients */
	INSERT INTO dbo.ClientDataUsageWork (
		ClientDataUsageBillingRunID,
		ClientID,
		TableName,
		RecordCount,
		FixedBytesPerRow,
		FixedBytesForThisClient,
		VariableBytesForThisClient,
		TotalBytesForThisClient,
		IsCustomTable
	)
	SELECT 
		@CurrentBillingRunID, 
		c.ClientID,
		t.TableName,
		0 AS RecordCount,
		0 AS FixedBytesPerRow,
		0 AS FixedBytesForThisClient,
		0 AS VariableBytesForThisClient,
		0 AS TotalBytesForThisClient, 
		0 AS IsCustomTable
	FROM dbo.ClientDataUsage c WITH (NOLOCK) 
	CROSS JOIN @TablesOfInterest t 
	
	/* Loop through all these tables one at a time */
	WHILE EXISTS (SELECT * FROM @TablesOfInterest t WHERE t.Processed IS NULL)
	BEGIN
		
		/* Get one table to work on (for all clients at once) */
		SELECT TOP (1) @TableName = t.TableName 
		FROM @TablesOfInterest t 
		WHERE t.Processed IS NULL 
		ORDER BY t.TableName 
		
		/* Show progress */
		UPDATE dbo.ClientDataUsageBillingRun 
		SET CurrentStage = 'Shared table calcs: ' + @TableName + ' at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
		
		/* Clear any variables used in the loop */
		SELECT @SumFixedAndPrecision = 0,
		@NullableColumnCount = 0,
		@BytesForNullBitmap = 0,
		@BitColumnCount = 0,
		@BytesForBitColumns = 0,
		@VariableColumnList = '',
		@VariableColumnListSql = '',
		@DynamicSql = ''
		
		/* Sum up the lengths of the fixed-length and semi-fixed-length columns first */
		SELECT @SumFixedAndPrecision = ISNULL(SUM(sdt.FixedLengthInBytes), 0) + ISNULL(SUM(sdtp.PreciseLengthInBytes), 0)
		FROM INFORMATION_SCHEMA.COLUMNS c WITH (NOLOCK) 
		INNER JOIN dbo.SqlDataType sdt WITH (NOLOCK) ON sdt.DataTypeName = c.DATA_TYPE 
		LEFT JOIN dbo.SqlDataTypePrecision sdtp WITH (NOLOCK) ON sdtp.SqlDataTypeID = sdt.SqlDataTypeID AND CASE WHEN c.DATETIME_PRECISION IS NOT NULL THEN c.DATETIME_PRECISION ELSE c.NUMERIC_PRECISION END BETWEEN sdtp.PrecisionLowerBound AND sdtp.PrecisionUpperBound
		WHERE c.TABLE_NAME = @TableName
		
		/* Count the number of NULLable columns */
		SELECT @NullableColumnCount = COUNT(*) 
		FROM INFORMATION_SCHEMA.COLUMNS c WITH (NOLOCK) 
		WHERE c.TABLE_NAME = @TableName 
		AND c.IS_NULLABLE = 'YES'
		
		/*
			Deliberately use integer arithmetic here, to avoid any unwanted remainders.  
			If there are zero nullable columns in the table then ((0+7)/8) comes out as zero, perfect.
			If there are 1-8 bytes then (8/8) all the way up to (15/8) comes out as 1 byte required, perfect.
			From 9-16 comes out as (16/8) up to (23/8) = 2 bytes and so on.
		*/
		SELECT @BytesForNullBitmap = ((@NullableColumnCount + 7) / 8)
		
		/* Similarly, count the number of BIT columns */
		SELECT @BitColumnCount = COUNT(*)
		FROM INFORMATION_SCHEMA.COLUMNS c WITH (NOLOCK) 
		WHERE c.TABLE_NAME = @TableName 
		AND c.DATA_TYPE = 'BIT'
		
		/* Exactly the same logic as for the null bitmap calculation */
		SELECT @BytesForBitColumns = ((@BitColumnCount + 7) / 8)
		
		/* List the columns for which we need to count the variable datalengths to get exact storage per client */
		SELECT @VariableColumnList += ',' + c.COLUMN_NAME, @VariableColumnListSql += '+ ISNULL(SUM( CAST( DATALENGTH(' + c.COLUMN_NAME + ') AS BIGINT) ), 0) ' 
		FROM INFORMATION_SCHEMA.COLUMNS c WITH (NOLOCK) 
		INNER JOIN dbo.SqlDataType sdt WITH (NOLOCK) ON sdt.DataTypeName = c.DATA_TYPE 
		LEFT JOIN dbo.SqlDataTypePrecision sdtp WITH (NOLOCK) ON sdtp.SqlDataTypeID = sdt.SqlDataTypeID AND CASE WHEN c.DATETIME_PRECISION IS NOT NULL THEN c.DATETIME_PRECISION ELSE c.NUMERIC_PRECISION END BETWEEN sdtp.PrecisionLowerBound AND sdtp.PrecisionUpperBound
		WHERE c.TABLE_NAME = @TableName 
		AND sdt.LengthVariesByData = 1
		ORDER BY c.TABLE_NAME, c.ORDINAL_POSITION
		
		/* Although this total-fixed-bytes-per-row is the same for every client, there is little cost duplicating it on every row for easy maths later */
		UPDATE dbo.ClientDataUsageWork 
		SET FixedBytesPerRow = @SumFixedAndPrecision + @BytesForNullBitmap + @BytesForBitColumns + @FixedOverheadPerRow 
		WHERE TableName = @TableName 
		
		/* Count the rows in this table for each client */
		SELECT @DynamicSql = '
			;WITH InnerSql AS 
			(
				SELECT t.ClientID, COUNT(*) AS rc 
				FROM dbo.' + @TableName + ' t WITH (NOLOCK) 
				GROUP BY t.ClientID
			)
			UPDATE dbo.ClientDataUsageWork 
			SET RecordCount = i.rc 
			FROM InnerSql i 
			CROSS JOIN dbo.ClientDataUsageWork w 
			WHERE i.ClientID = w.ClientID 
			AND w.TableName = ''' + @TableName + ''' '
		
		PRINT(@DynamicSql)
		EXEC(@DynamicSql)
		
		

		/* Remove the leading comma by STUFFing nothing into the first 1 character of the string */
		IF @VariableColumnList LIKE ',%'
		BEGIN
			SELECT @VariableColumnList = STUFF(@VariableColumnList, 1, 1, ''),
			@VariableColumnListSql = 'SELECT ClientID, ' + STUFF(@VariableColumnListSql, 1, 1, '') + ' AS VariableLengthDataByClient FROM dbo.' + @TableName + ' WITH (NOLOCK) GROUP BY ClientID'
		END
		
		/* Add up the variable length bytes in this table for each client. 
		
			The dynamic sql will look like this, with some extra CASTs and ISNULLs:
			
			;WITH InnerSql AS 
			(
				SELECT t.ClientID, SUM(DATALENGTH(DocumentTypeName)) + SUM(DATALENGTH(DocumentTypeDescription)) + SUM(...) AS VariableLengthDataByClient 
				FROM dbo.DocumentType t WITH (NOLOCK) 
				GROUP BY t.ClientID
			)
			UPDATE dbo.ClientDataUsageWork 
			SET VariableBytesForThisClient = i.VariableLengthDataByClient
			...etc
			
			Check that this table has any variable length columns before doing any work.
		*/
		IF @VariableColumnListSql > ''
		BEGIN
			SELECT @DynamicSql = '
				;WITH InnerSql AS 
				(
					' + @VariableColumnListSql + '
				)
				UPDATE dbo.ClientDataUsageWork 
				SET VariableBytesForThisClient = i.VariableLengthDataByClient
				FROM InnerSql i 
				CROSS JOIN dbo.ClientDataUsageWork w 
				WHERE i.ClientID = w.ClientID 
				AND w.TableName = ''' + @TableName + ''' '
			
			PRINT(@DynamicSql)
			EXEC(@DynamicSql)
		END
		
		/* Fixed Bytes for each table, for all tables and clients at once */
		UPDATE dbo.ClientDataUsageWork 
		SET FixedBytesForThisClient = CAST(FixedBytesPerRow AS BIGINT) * CAST(RecordCount AS BIGINT)

		/* Total Bytes per table, for all tables and clients at once */
		UPDATE dbo.ClientDataUsageWork 
		SET TotalBytesForThisClient = FixedBytesForThisClient + VariableBytesForThisClient
		
		/* Mark this table as complete and move on to the next */
		UPDATE @TablesOfInterest 
		SET Processed = 1
		WHERE TableName = @TableName 
		
	END
	
	/* Show progress */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = 'Shared table calcs: Updating totals'
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
	/* Total Bytes for standard tables, for all clients at once */
	;WITH InnerSql AS 
	(
		SELECT w.ClientID, SUM(w.TotalBytesForThisClient) AS sb 
		FROM dbo.ClientDataUsageWork w WITH (NOLOCK) 
		WHERE IsCustomTable = 0
		GROUP BY w.ClientID 
	)
	UPDATE dbo.ClientDataUsage 
	SET StandardTablesBytes = i.sb 
	FROM InnerSql i 
	INNER JOIN dbo.ClientDataUsage u ON i.ClientID = u.ClientID 
	
	
	/* Show progress */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = 'Custom table calcs: Running now at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
	;WITH InnerSql AS 
	(
		SELECT 
		mt.clientid, 
		t.NAME AS TableName,
		s.Name AS SchemaName,
		p.rows AS RowCounts,
		SUM(a.total_pages) * 8 AS TotalSpaceKB, 
		SUM(a.used_pages) * 8 AS UsedSpaceKB, 
		(SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB
		FROM sys.tables t
		INNER JOIN dbo.MigrationTable mt WITH (NOLOCK) ON mt.TableName = t.name AND mt.ClientID > 0
		INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id
		INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
		INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id
		LEFT OUTER JOIN sys.schemas s ON t.schema_id = s.schema_id
		WHERE t.NAME NOT LIKE 'dt%' 
		AND t.is_ms_shipped = 0
		AND i.OBJECT_ID > 255 
		GROUP BY mt.clientid, t.Name, s.Name, p.Rows
	)
	INSERT INTO dbo.ClientDataUsageWork (
		ClientDataUsageBillingRunID,
		ClientID,
		TableName,
		RecordCount,
		FixedBytesPerRow,
		FixedBytesForThisClient,
		VariableBytesForThisClient,
		TotalBytesForThisClient,
		IsCustomTable
	)
	SELECT 
		@CurrentBillingRunID, 
		i.ClientID,
		i.TableName,
		i.RowCounts AS RecordCount,
		0 AS FixedBytesPerRow,
		0 AS FixedBytesForThisClient,
		0 AS VariableBytesForThisClient,
		(1024 * i.TotalSpaceKB) AS TotalBytesForThisClient, 
		1 AS IsCustomTable
	FROM InnerSql i 
	
	
	/* Show progress */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = 'Custom table calcs: Updating totals at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
	/* Total Bytes for custom tables, for all clients at once */
	;WITH InnerSql AS 
	(
		SELECT w.ClientID, SUM(w.TotalBytesForThisClient) AS sb 
		FROM dbo.ClientDataUsageWork w WITH (NOLOCK) 
		WHERE w.IsCustomTable = 1
		GROUP BY w.ClientID 
	)
	UPDATE dbo.ClientDataUsage 
	SET CustomTablesBytes = i.sb 
	FROM InnerSql i 
	INNER JOIN dbo.ClientDataUsage u ON i.ClientID = u.ClientID 
	
	
	/* Show progress */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = 'LeadDocument calcs: Updating totals at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
	/* LeadDocument calcs */
	;WITH InnerSql AS 
	(
		SELECT ld.ClientID, SUM(CAST(ISNULL(ld.DocumentBLOBSize, 0) AS BIGINT) + CAST(ISNULL(ld.EmailBLOBSize, 0) AS BIGINT)) AS TotalDocAndEmailBytes
		FROM dbo.LeadDocument ld WITH (NOLOCK) 
		GROUP BY ld.ClientID 
	)
	UPDATE dbo.ClientDataUsage 
	SET DocumentBytes = i.TotalDocAndEmailBytes 
	FROM InnerSql i 
	INNER JOIN dbo.ClientDataUsage u ON i.ClientID = u.ClientID 
	
	
	/* Show progress */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = 'Client folder space calcs: Updating totals at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
	/* ClientImages folders etc */
	;WITH InnerSql AS 
	(
		SELECT c.ClientID, SUM(ISNULL(c.TotalBytesForThisClient, 0)) AS TotalBytesForThisClient
		FROM [AquariusMaster].dbo.ClientDataUsageDiskSpace c WITH (NOLOCK) 
		GROUP BY c.ClientID 
	)
	UPDATE dbo.ClientDataUsage 
	SET ClientImagesBytes = i.TotalBytesForThisClient 
	FROM InnerSql i 
	INNER JOIN dbo.ClientDataUsage u ON i.ClientID = u.ClientID 
	
	
	/* Show progress */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = 'Summing up all the sub-totals at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
	/* Add all the sub-totals together to get the final bytes totals */
	UPDATE dbo.ClientDataUsage 
	SET TotalBytes = ISNULL(DocumentBytes, 0) + ISNULL(ClientImagesBytes, 0)  + ISNULL(StandardTablesBytes, 0) + ISNULL(CustomTablesBytes, 0)
	
	
	/* Show progress - close the billing run here */
	UPDATE dbo.ClientDataUsageBillingRun 
	SET CurrentStage = @STAGE_COMPLETE, WhenFinished = dbo.fn_GetDate_Local(), TimeInSeconds = DATEDIFF(second, WhenStarted, dbo.fn_GetDate_Local())
	WHERE ClientDataUsageBillingRunID = @CurrentBillingRunID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDataUsage_CalcAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientDataUsage_CalcAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDataUsage_CalcAll] TO [sp_executeall]
GO
