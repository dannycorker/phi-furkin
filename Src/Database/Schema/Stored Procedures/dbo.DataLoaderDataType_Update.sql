SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderDataType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDataType_Update]
(

	@DataLoaderDataTypeID int   ,

	@OriginalDataLoaderDataTypeID int   ,

	@DataTypeName varchar (250)  ,

	@DataTypeDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderDataType]
				SET
					[DataLoaderDataTypeID] = @DataLoaderDataTypeID
					,[DataTypeName] = @DataTypeName
					,[DataTypeDescription] = @DataTypeDescription
				WHERE
[DataLoaderDataTypeID] = @OriginalDataLoaderDataTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDataType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Update] TO [sp_executeall]
GO
