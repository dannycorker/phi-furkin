SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_GetByCaseIDEventTypeIDEventDeletedClientIDWhenCreated]
(

	@CaseID int   ,

	@EventTypeID int   ,

	@EventDeleted bit   ,

	@ClientID int   ,

	@WhenCreated datetime   
)
AS


				SELECT
					[LeadEventID],
					[ClientID],
					[LeadID],
					[WhenCreated],
					[WhoCreated],
					[Cost],
					[Comments],
					[EventTypeID],
					[NoteTypeID],
					[FollowupDateTime],
					[WhenFollowedUp],
					[AquariumEventType],
					[NextEventID],
					[CaseID],
					[LeadDocumentID],
					[NotePriority],
					[DocumentQueueID],
					[EventDeleted],
					[WhoDeleted],
					[DeletionComments],
					[ContactID],
					[BaseCost],
					[DisbursementCost],
					[DisbursementDescription],
					[ChargeOutRate],
					[UnitsOfEffort],
					[CostEnteredManually],
					[IsOnHold],
					[HoldLeadEventID]
				FROM
					[dbo].[LeadEvent] WITH (NOLOCK) 
				WHERE
										[CaseID] = @CaseID
					AND [EventTypeID] = @EventTypeID
					AND [EventDeleted] = @EventDeleted
					AND [ClientID] = @ClientID
					AND [WhenCreated] = @WhenCreated
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_GetByCaseIDEventTypeIDEventDeletedClientIDWhenCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_GetByCaseIDEventTypeIDEventDeletedClientIDWhenCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_GetByCaseIDEventTypeIDEventDeletedClientIDWhenCreated] TO [sp_executeall]
GO
