SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PortalCaseListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalCaseListDisplay_Get_List]

AS


				
				SELECT
					[PortalCaseListDisplayID],
					[ClientID],
					[LeadTypeID],
					[Field1Caption],
					[Field1TypeID],
					[Field1DetailFieldID],
					[Field1ColumnDetailFieldID],
					[Field2Caption],
					[Field2TypeID],
					[Field2DetailFieldID],
					[Field2ColumnDetailFieldID],
					[Field3Caption],
					[Field3TypeID],
					[Field3DetailFieldID],
					[Field3ColumnDetailFieldID],
					[Field4Caption],
					[Field4TypeID],
					[Field4DetailFieldID],
					[Field4ColumnDetailFieldID],
					[Field5Caption],
					[Field5TypeID],
					[Field5DetailFieldID],
					[Field5ColumnDetailFieldID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SqlQueryText]
				FROM
					[dbo].[PortalCaseListDisplay] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalCaseListDisplay_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Get_List] TO [sp_executeall]
GO
