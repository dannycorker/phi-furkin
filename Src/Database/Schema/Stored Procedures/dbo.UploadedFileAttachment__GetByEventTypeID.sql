SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2012
-- Description:	Gets an UploadedFileAttachment record by the given event type id
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFileAttachment__GetByEventTypeID]
	
	@EventTypeID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT ufa.*, uf.UploadedFileTitle FROM UploadedFileAttachment ufa WITH (NOLOCK) 
	INNER JOIN dbo.UploadedFile uf WITH (NOLOCK) ON uf.UploadedFileID = ufa.UploadedFileID
	WHERE EventTypeID=@EventTypeID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFileAttachment__GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__GetByEventTypeID] TO [sp_executeall]
GO
