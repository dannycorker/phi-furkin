SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateDroppedOutCustomerEmailSentDate]

@CustomerID int,
@ReminderEmailSentDate datetime

as

Update DroppedOutCustomers
SET ReminderEmailSentDate=@ReminderEmailSentDate
where CustomerID = @CustomerID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDroppedOutCustomerEmailSentDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateDroppedOutCustomerEmailSentDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDroppedOutCustomerEmailSentDate] TO [sp_executeall]
GO
