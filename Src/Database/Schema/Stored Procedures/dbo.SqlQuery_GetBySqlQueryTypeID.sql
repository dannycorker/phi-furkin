SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_GetBySqlQueryTypeID]
(

	@SqlQueryTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[QueryID],
					[ClientID],
					[QueryText],
					[QueryTitle],
					[AutorunOnline],
					[OnlineLimit],
					[BatchLimit],
					[SqlQueryTypeID],
					[FolderID],
					[IsEditable],
					[IsTemplate],
					[IsDeleted],
					[WhenCreated],
					[CreatedBy],
					[OwnedBy],
					[RunCount],
					[LastRundate],
					[LastRuntime],
					[LastRowcount],
					[MaxRuntime],
					[MaxRowcount],
					[AvgRuntime],
					[AvgRowcount],
					[Comments],
					[WhenModified],
					[ModifiedBy],
					[LeadTypeID],
					[ParentQueryID],
					[IsParent],
					[SqlQueryTemplateID],
					[OutputFormat],
					[ShowInCustomSearch],
					[OutputFileExtension],
					[OutputSeparatorCharmapID],
					[OutputEncapsulatorCharmapID],
					[SuppressHeaderRow],
					[LockAllTables],
					[SourceID]
				FROM
					[dbo].[SqlQuery] WITH (NOLOCK) 
				WHERE
					[SqlQueryTypeID] = @SqlQueryTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_GetBySqlQueryTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_GetBySqlQueryTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_GetBySqlQueryTypeID] TO [sp_executeall]
GO
