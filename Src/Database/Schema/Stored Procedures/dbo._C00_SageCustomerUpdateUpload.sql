SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-20
-- Description:	Upload CustomerUpdate XML
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SageCustomerUpdateUpload]
	@CustomerUpdateData VARCHAR(MAX),
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO LogXML (ClientID, ContextID, ContextVarchar, InvalidXML, LogDateTime, LogXMLEntry)
	VALUES (@ClientID, 94, 'XML Upload', NULL, dbo.fn_GetDate_Local(), @CustomerUpdateData)
	
	INSERT INTO AsyncQueue (ClientID, WhenCreated, QueueTypeID, Status, Payload, Outcome, WhenCompleted)
	VALUES (@ClientID, dbo.fn_GetDate_Local(), 3, 1, @CustomerUpdateData, NULL, NULL)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageCustomerUpdateUpload] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SageCustomerUpdateUpload] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageCustomerUpdateUpload] TO [sp_executeall]
GO
