SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-03-25
-- Description:	Checks to see if an uploaded file id is used in multiple places so it can be safely deleted
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFile__CheckIfUsed]
	@UploadedFileID INT,
	@DetailFieldID INT,
	@DetailFieldSubType INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @Count INT 
	SELECT @Count = 0
	
	IF @DetailFieldSubType = 1
	BEGIN
		SELECT @Count = COUNT(*)
		FROM LeadDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	ELSE IF @DetailFieldSubType = 2
	BEGIN
		SELECT @Count = COUNT(*)
		FROM MatterDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	ELSE IF @DetailFieldSubType = 10
	BEGIN
		SELECT @Count = COUNT(*)
		FROM CustomerDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	ELSE IF @DetailFieldSubType = 11
	BEGIN
		SELECT @Count = COUNT(*)
		FROM CaseDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	ELSE IF @DetailFieldSubType = 12
	BEGIN
		SELECT @Count = COUNT(*)
		FROM ClientDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	ELSE IF @DetailFieldSubType = 13
	BEGIN
		SELECT @Count = COUNT(*)
		FROM ClientPersonnelDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	ELSE IF @DetailFieldSubType = 14
	BEGIN
		SELECT @Count = COUNT(*)
		FROM ContactDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID = @DetailFieldID
		AND ValueInt = @UploadedFileID
	END
	
	SELECT @Count AS [Count]
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile__CheckIfUsed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile__CheckIfUsed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile__CheckIfUsed] TO [sp_executeall]
GO
