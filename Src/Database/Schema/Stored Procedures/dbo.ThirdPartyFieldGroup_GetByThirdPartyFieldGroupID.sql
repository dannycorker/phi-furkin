SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldGroup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_GetByThirdPartyFieldGroupID]
(

	@ThirdPartyFieldGroupID int   
)
AS


				SELECT
					[ThirdPartyFieldGroupID],
					[ThirdPartySystemID],
					[GroupName],
					[GroupDescription],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldGroup] WITH (NOLOCK) 
				WHERE
										[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_GetByThirdPartyFieldGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_GetByThirdPartyFieldGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_GetByThirdPartyFieldGroupID] TO [sp_executeall]
GO
