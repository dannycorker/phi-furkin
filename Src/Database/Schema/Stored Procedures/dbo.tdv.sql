SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-25
-- Description:	Pivot all TableDetailValues records for all tables for a Lead or Matter
-- =============================================
CREATE PROCEDURE [dbo].[tdv] 
	@LeadID int, 
	@MatterID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TDVContainerFields TABLE (DetailFieldID int) 
	
	DECLARE @TableCount int, @DetailFieldID int
	
	/* Show all TableDetailValues for all tables for this lead */
	INSERT INTO @TDVContainerFields (DetailFieldID)
	SELECT df.DetailFieldID 
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.LeadTypeID = l.LeadTypeID AND df.QuestionTypeID IN (16, 19) AND df.[Enabled] = 1 
	WHERE l.LeadID = @LeadID
	
	SELECT @TableCount = @@rowcount
	
	SELECT * 
	FROM @TDVContainerFields

	/*SELECT * 
	FROM dbo.DetailFields df WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields dft WITH (NOLOCK) on dft.DetailFieldPageID = df.TableDetailFieldPageID 
	LEFT JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.DetailFieldID = dft.DetailFieldID AND tdv.LeadID = @LeadID AND (@MatterID IS NULL OR tdv.MatterID = @MatterID)
	WHERE df.LeadTypeID = 37 
	AND df.QuestionTypeID IN (16, 19) 
	AND df.[Enabled] = 1 
	ORDER BY df.DetailFieldID, tdv.LeadID, tdv.MatterID, dft.DetailFieldID, tdv.TableDetailValueID*/  
	
	WHILE @TableCount > 0
	BEGIN
		
		SELECT TOP 1 @DetailFieldID = t.DetailFieldID
		FROM @TDVContainerFields t
		
		EXEC dbo.TableRows__GetByDetailFieldIDLeadIDMatterID @DetailFieldID, @LeadID, @MatterID

		DELETE @TDVContainerFields 
		WHERE DetailFieldID = @DetailFieldID
		
		SELECT @TableCount -= 1
		
	END
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[tdv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[tdv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[tdv] TO [sp_executeall]
GO
