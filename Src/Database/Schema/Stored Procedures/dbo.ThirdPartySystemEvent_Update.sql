SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartySystemEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_Update]
(

	@ThirdPartySystemEventID int   ,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@Name varchar (250)  ,

	@Description varchar (250)  ,

	@EventTypeID int   ,

	@LeadTypeID int   ,

	@ThirdPartySystemKey int   ,

	@MessageName varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartySystemEvent]
				SET
					[ThirdPartySystemID] = @ThirdPartySystemID
					,[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[Name] = @Name
					,[Description] = @Description
					,[EventTypeID] = @EventTypeID
					,[LeadTypeID] = @LeadTypeID
					,[ThirdPartySystemKey] = @ThirdPartySystemKey
					,[MessageName] = @MessageName
				WHERE
[ThirdPartySystemEventID] = @ThirdPartySystemEventID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Update] TO [sp_executeall]
GO
