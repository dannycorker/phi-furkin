SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get all the base breeds
-- Mods:		2015-05-11	DCM	Update to use Brand/affinity specific breed lists
--				2015-05-15	SB	Allow species to be null so can re-use for the breed auto complete
--				2016-08-09  DCM protect against @filter being NULL
--				2016-10-21	IS	return the min and max breed age
--				2017-01-02	DCM properly protect against @filter being NULL
--				2017-05-30	CPS default to CP
--				2017-07-14	CPS add Excluded Breed flag
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C603_PA_Search_Breed]
(

	 @ClientId INT = NULL
	,@ClientPersonnelId INT = NULL
	,@Filter VARCHAR(50) = NULL
	,@PetSpeciedId INT = NULL
	,@BrandID INT = NULL
)
AS
BEGIN

	IF @ClientId is NULL
	BEGIN
		SELECT @ClientId = dbo.fnGetPrimaryClientID()
	END

/*Note, min and max age appear to be the wrong way ound here, but they are handled correctly in the Q&B. Just be aware*/

	;WITH InnerSql AS 
	(
				SELECT	r.ResourceListID AS Id, rdvBreed.DetailValue AS Value, 
				CASE WHEN rdvBreedMinAge.ValueInt > 0 THEN rdvBreedMinAge.ValueInt ELSE NULL END AS MinAge, 
				CASE WHEN rdvBreedMaxAge.ValueInt > 0 THEN rdvBreedMaxAge.ValueInt ELSE NULL END AS MaxAge,
				-CASE WHEN rdvOrder.ValueInt <> 0 THEN rdvOrder.ValueInt ELSE 10000 END AS Sort
				,ISNULL(rdvExcluded.ValueInt,0) [Excluded]
				,CASE WHEN rdvExcluded.ValueInt = 1 THEN 'Unfortunately the selected breed cannot be offered cover.' END [ExcludedNarrative]
				, rdvBreed.*
				, afflist.ResourceListID AS BrandId
				, rdvSpecies.ValueInt as SpeciesId
		FROM dbo.ResourceList r WITH (NOLOCK)
		INNER JOIN dbo.ResourceListDetailValues rdvSpecies WITH (NOLOCK) ON r.ResourceListID = rdvSpecies.ResourceListID AND rdvSpecies.DetailFieldID = 144269
		INNER JOIN dbo.ResourceListDetailValues rdvBreed WITH (NOLOCK) ON r.ResourceListID = rdvBreed.ResourceListID AND rdvBreed.DetailFieldID = 144270 /*Pet Breed*/
		LEFT JOIN dbo.ResourceListDetailValues rdvBreedMinAge WITH (NOLOCK) ON r.ResourceListID = rdvBreedMinAge.ResourceListID AND rdvBreedMinAge.DetailFieldID = 177293
		LEFT JOIN dbo.ResourceListDetailValues rdvBreedMaxAge WITH (NOLOCK) ON r.ResourceListID = rdvBreedMaxAge.ResourceListID AND rdvBreedMaxAge.DetailFieldID = 177292
		LEFT JOIN dbo.ResourceListDetailValues rdvOrder WITH (NOLOCK) ON r.ResourceListID = rdvOrder.ResourceListID AND rdvOrder.DetailFieldID = 175583
		INNER JOIN dbo.ResourceListDetailValues rdvEnabled WITH (NOLOCK) ON r.ResourceListID = rdvEnabled.ResourceListID AND rdvEnabled.DetailFieldID = 175584
		INNER JOIN dbo.ResourceListDetailValues rdvList WITH (NOLOCK) ON r.ResourceListID = rdvList.ResourceListID AND rdvList.DetailFieldID = 170010
		INNER JOIN dbo.ResourceListDetailValues affList WITH (NOLOCK) ON rdvList.ValueInt=affList.ValueInt AND afflist.DetailFieldID = 175592
		LEFT JOIN ResourceListDetailValues rdvExcluded WITH ( NOLOCK ) on rdvExcluded.ResourceListID = r.ResourceListID AND rdvExcluded.DetailFieldID = 177897 /*Excluded Breed*/
		WHERE	(ISNULL(@Filter, '') = '' OR rdvBreed.DetailValue LIKE '%' + @Filter + '%')
		AND		(rdvSpecies.ValueInt = @PetSpeciedId OR @PetSpeciedId IS NULL)
		AND		r.ClientID = @ClientId
		AND		rdvBreed.DetailValue > '' -- Prevent empty breeds
		AND		(@BrandID IS NULL OR afflist.ResourceListID = @BrandID)
		AND		rdvEnabled.ValueInt <> 5145 /*No*/
	)
	SELECT Id, Value, MinAge, MaxAge, i.Excluded, i.ExcludedNarrative, BrandId, SpeciesId
	FROM InnerSql i 
	ORDER BY Sort, Value
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C603_PA_Search_Breed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C603_PA_Search_Breed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C603_PA_Search_Breed] TO [sp_executeall]
GO
