SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:           Paul Richardson
-- Create date: 26/03/2018
-- Description:      Updates the card transaction and account with the response results
--                         If the transaction id is empty the transaction has failed
-- 2018-03-27 ACE - Update Payment, CPS, PPPS and add required event
-- 2018-04-24 JEL - changed matching params to work with the information we have available 
-- 2019-03-15 JEL - Added filters to only update customerledger in the CPS and PPPS if the ID is currently NULL 
-- 2019-10-21 CPS for JIRA LPC-28	| Removed C600-specific renewal rules
-- 2019-10-21 CPS for JIRA LPC-28	| Include useful comments when applying the event
-- 2019-12-06 CPS for JIRA LPC-202	| Stop blocking CardTransaction update when the ErrorCode is NULL
-- 2019-12-09 CPS for JIRA LPC-181	| Trigger the card payer process once a positive notification comes back
-- 2019-12-09 CPS for JIRA LPC-181	| CustomerLedgerID is NULL filter removed following discussion with JEL
-- 2019-12-09 CPS for JIRA LPC-205	| Include LeadTypeID so we can filter CardReturnProcessing by LeadType
-- 2020-01-13 CPS for JIRA LPC-356	| Replace hard-coded ClientID with function
-- 2020-01-14 CPS for JIRA SDLPC-94 | Changed from c.OrderID to c.ParentID in refund parsing to line up with CardTransactionMap changes in Stable branch
-- 2020-04-03 GPR					| Added two WITH (NOLOCK) hints to procedure
-- 2020-09-03 GPR for JIRA PPET-373	| Assigned CustomerID and MatterID when CustomerPaymentScheduleID is zero / CardTransactionID not present
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction_UpdateWithResponse]
	 @CardTransactionID			INT
	,@ErrorCode					VARCHAR (100)
	,@ErrorMessage				VARCHAR (500)
	,@TransactionID				VARCHAR (50)
	,@AuthCode					VARCHAR (250)
	,@StatusMsg					VARCHAR (250)
	,@ClientPaymentGatewayID	INT = NULL
	,@MerchantCode				VARCHAR (200) 
	,@JournalType				VARCHAR (100) 
	,@MaskedCardNumber			VARCHAR (200)
	,@ExpiryDate				DATE
	,@ExpiryMonth				VARCHAR (2)
	,@ExpiryYear				VARCHAR (4)
	,@Amount					DECIMAL (18,2) 
	,@CustomerRefNum			VARCHAR(200) = NULL /*AKA Token*/
AS
BEGIN

    SET NOCOUNT ON;
      
	DECLARE @FnGetDateLocal DATETIME = dbo.fn_GetDate_Local()

    DECLARE	 @AccountID							INT
			,@PaymentID							INT
			,@CustomerPaymentScheduleID			INT
			,@CustomerLedgerID					INT
			,@RunAsUserID						INT = dbo.fn_C600_GetAqAutomationUser()
			,@EventToAdd						INT
			,@ClientID							INT = dbo.fnGetPrimaryClientID()
			,@CaseID							INT
			,@LeadEventID						INT
			,@EventTypeID						INT
			,@LeadID							INT
			,@DateForEvent						DATETIME = @FnGetDateLocal
			,@CustomerID						INT
			,@MatterID							INT
			,@PolicyMatterID					INT
			,@Description						VARCHAR(200) = ''
			,@LogEntry							VARCHAR (MAX)
			,@AmountNet							DECIMAL(18,2)
			,@AmountVat							DECIMAL (18,2)
			,@PurchasedProductID				INT
			,@PurchasedProductPaymentScheduleID INT 
			,@EventComments						VARCHAR(2000) = ''
			,@LogID								INT
			,@RowCount							INT
			,@LeadTypeID						INT
			,@ContraCustomerLedgerID			INT
			,@LogMessage						VARCHAR(1024)

	DECLARE @FailedPPPS	TABLE (PPPSID INT, PurchasedProductID INT)
	DECLARE @PetMatters	TABLE (MatterID INT, RetryPrepared BIT DEFAULT (0) )
                     
    DECLARE @InsertedCPS TABLE (CustomerPaymentScheduleID INT, PurchasedProductPaymentScheduleID INT)
                     
    SELECT @LogEntry =  ' CardTransactionID '		+ CAST(ISNULL(@CardTransactionID		,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Error Code '				+ CAST(ISNULL(@ErrorCode				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Error Message '			+ CAST(ISNULL(@ErrorMessage				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' TransactionID '			+ CAST(ISNULL(@TransactionID			,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' AuthCode '				+ CAST(ISNULL(@AuthCode					,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' StatusMsg '				+ CAST(ISNULL(@StatusMsg				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' ClientPaymentGateWayID '	+ CAST(ISNULL(@ClientPaymentGatewayID	,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' MerchantCode '			+ CAST(ISNULL(@MerchantCode				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' JournalType '				+ CAST(ISNULL(@JournalType				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' MaskedCardNumber '		+ CAST(ISNULL(@MaskedCardNumber			,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' ExpiryDate '				+ CAST(ISNULL(@ExpiryDate				,'' ) as VARCHAR) + CHAR(13)+CHAR(10) +
						' ExpiryMonth '				+ CAST(ISNULL(@ExpiryMonth				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' ExpiryYear '				+ CAST(ISNULL(@ExpiryYear				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Amount  '					+ CAST(ISNULL(@Amount					,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Card Token  '				+ CAST(ISNULL(@CustomerRefNum			,'0') as VARCHAR)
						  
	EXEC @LogID = _C00_LogIt  'PROC', 'CardTransaction_UpdateWithResponse', 'CardTransaction_UpdateWithResponse Values',@LogEntry,58552 /*Aquarium Automation*/
		  
	SELECT @EventComments += 'LogID ' + ISNULL(CONVERT(VARCHAR,@LogID),'NULL') + CHAR(13)+CHAR(10) + @LogEntry

  	SELECT @CustomerID = c.CustomerID, 
			@Amount = c.Amount, 
			@CustomerPaymentScheduleID = c.CustomerPaymentScheduleID, 
			@MatterID = c.ObjectID, 
			@PurchasedProductID = c.ParentID, 
			@AccountID = c.AccountID
	FROM CardTransaction c WITH (NOLOCK) 
	WHERE c.CardTransactionID = @CardTransactionID	
	
	SELECT @PaymentID = p.PaymentID 
	FROM dbo.Payment p WITH (NOLOCK) 
	WHERE p.CardtransactionID = @CardTransactionID
				
	/*If possible, we want to match on the CustomerPaymentScheduleID, if this is NULL on the CT record, then pick up from the customer ID and Amount*/ 
	IF ISNULL(@CustomerPaymentScheduleID,0) <> 0 
	BEGIN
	    
		SELECT	 @CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				,@AccountID					= cps.AccountID
				,@CaseID					= m.CaseID
				,@LeadID					= m.LeadID
				,@ClientID					= m.ClientID
				,@AmountNet					= cps.PaymentNet
				,@AmountVat					= cps.PaymentVAT
				,@CustomerLedgerID          = cps.CustomerLedgerID
				,@LeadTypeID				= l.LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN Account a WITH (NOLOCK) on a.AccountID = cps.AccountID
		INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = a.ObjectID
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
		WHERE cps.CustomerID = @CustomerID 
		AND cps.PaymentGross = @Amount
		AND cps.PaymentStatusID NOT IN (4,6) /*Not failed or Paid*/ 			
		AND cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
	    
	END
	ELSE
	BEGIN

		SELECT	 @CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				,@AccountID					= cps.AccountID
				,@CaseID					= m.CaseID
				,@LeadID					= m.LeadID
				,@ClientID					= m.ClientID
				,@AmountNet					= cps.PaymentNet
				,@AmountVat					= cps.PaymentVAT
				,@CustomerLedgerID          = cps.CustomerLedgerID
				,@LeadTypeID				= l.LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
				,@MatterID					= m.MatterID /*GPR 2020-09-03 for JIRA PPET-373*/
				,@CustomerID				= cps.CustomerID /*GPR 2020-09-03 for JIRA PPET-373*/
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN Account a WITH (NOLOCK) on a.AccountID = cps.AccountID
		INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = a.ObjectID
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
		WHERE cps.CustomerID = @CustomerID 
		AND cps.PaymentGross = @Amount
		AND cps.PaymentStatusID NOT IN (4,6) /*Not failed or Paid*/ 
	    
	END
	
	PRINT '@CustomerPaymentScheduleID = ' + ISNULL(CONVERT(VARCHAR,@CustomerPaymentScheduleID),'NULL')
		
	
		/*Update the relevant schedule records if it worked*/ 
	IF @StatusMsg = 'Success'
	BEGIN 

		UPDATE PurchasedProductPaymentSchedule 
		Set PaymentStatusID = 6, 
			CustomerLedgerID = @CustomerLedgerID, 
			ReconciledDate = @FnGetDateLocal
		From PurchasedProductPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE CustomerPaymentSchedule  
		Set PaymentStatusID = 6, 
			CustomerLedgerID = @CustomerLedgerID, 
			ReconciledDate = @FnGetDateLocal
		From CustomerPaymentSchedule 
		WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		SELECT @Description += LEFT(p.ProductDescription,CHARINDEX('(',p.ProductDescription,1)-1) + ' ' + pt.PurchasedProductPaymentScheduleTypeName + ' '
		FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
		INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.PurchasedProductID = pp.PurchasedProductID 
		INNER JOIN PurchasedProductPaymentScheduleType pt WITH (NOLOCK) on pt.PurchasedProductPaymentScheduleTypeID = pp.PurchasedProductPaymentScheduleTypeID
		WHERE pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
									
		UPDATE CustomerLedger 
		SET TransactionDescription = @Description, 
			TransactionReference = @CardTransactionID
		WHERE CustomerLedgerID = @CustomerLedgerID 

		UPDATE p 
		SET p.PaymentStatusID = 6,
				p.FailureReason = @ErrorMessage, 
				p.WhenModified = @FnGetDateLocal, 
				p.WhoModified = @RunAsUserID
		FROM Payment p 
		WHERE p.PaymentID = @PaymentID

		UPDATE ct
		SET StatusMsg = @StatusMsg, 
			TransactionID = @TransactionID
		FROM CardTransaction ct
		Where ct.CardTransactionID = @CardTransactionID 

	END 

	/*Update the relevant schedule records if it failed*/ 
	IF EXISTS (
		SELECT * 
		FROM CardReturnProcessing crt WITH (NOLOCK) 
		WHERE crt.FailureCode = @StatusMsg 
		AND @StatusMsg <> 'Success'
	) 
	BEGIN

		SELECT @EventToAdd = c.EventToApply, 
				@RunAsUserID = c.AddEventAs, 
				@ErrorMessage = c.FailureDescription
		FROM CardReturnProcessing c WITH (NOLOCK) 
		INNER JOIN EventType et WITH (NOLOCK) on et.EventTypeID = c.EventToApply
		WHERE c.FailureCode IN ('-1', @StatusMsg) /*-1 is a fallback event...*/
		AND c.ClientID IN (0, @ClientID)
		--AND c.ReasonCodeType = @ClientPaymentGatewayID /*GPR 2020-04-02 removed as not required*/
		AND et.LeadTypeID = @LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
		ORDER BY c.ClientID DESC, c.FailureCode DESC

		UPDATE ct 
		SET StatusMsg = @StatusMsg, 
			TransactionID = @TransactionID, 
			ErrorMessage = @ErrorMessage 
		FROM CardTransaction ct
		Where ct.CardTransactionID = @CardTransactionID 
				
		/*So not ok, this means we have a failure. Time to do the fail thing...*/
		UPDATE p 
		SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
				p.FailureReason = @ErrorMessage, 
				p.WhenModified = @FnGetDateLocal, 
				p.WhoModified = @RunAsUserID
		FROM Payment p 
		WHERE p.PaymentID = @PaymentID

		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		SELECT p.ClientID, 
				p.CustomerID, 
				PaymentDateTime, 
				p.FailureCode, 
				p.FailureReason, 
				CAST(@FnGetDateLocal AS DATE), 
				p.CardTransactionID, 
				'Failure notification ' + p.FailureReason, 
				-p.PaymentNet, 
				-p.PaymentVAT, 
				-p.PaymentGross, 
				NULL, 
				NULL, 
				NULL, 
				@PaymentID, 
				NULL, 
				58552 /*@AqAutomation*/, 
				@FnGetDateLocal 
		FROM Payment p WITH (NOLOCK) 
		WHERE p.PaymentID = @PaymentID
	            
		SELECT @ContraCustomerLedgerID = SCOPE_IDENTITY()

		-- Reverse customerpaymentschedule, reverse payment status
		UPDATE cps  
		SET PaymentStatusID = 4, 
			ReconciledDate = @FnGetDateLocal, 
			CustomerLedgerID = @CustomerLedgerID 
		FROM CustomerPaymentSchedule cps WITH (NOLOCK)
		INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		-- Purchasedproductpaymentschedule, reverse payment status
		UPDATE ppps  
		SET PaymentStatusID = 4, 
			ReconciledDate = @FnGetDateLocal, 
			ContraCustomerLedgerID = @ContraCustomerLedgerID, 
			CustomerLedgerID = @CustomerLedgerID 
		OUTPUT inserted.PurchasedProductPaymentScheduleID, inserted.PurchasedProductID 
			INTO @FailedPPPS (PPPSID, PurchasedProductID)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID
		WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

		/*GPR 2020-04-20 for AAG-679*/
		SELECT @LeadEventID = c.LatestLeadEventID 
		FROM Lead l WITH (NOLOCK)
		INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
		WHERE l.LeadTypeID = @LeadTypeID
		AND l.CustomerID = @CustomerID

		SELECT @LogEntry = 'SecureCo Event To Add ' + CONVERT(VARCHAR(100), @EventToAdd) + ' ' 
			+	CHAR(13) + CHAR(10)
			+ 'SecureCo LeadEventID ' + CONVERT(VARCHAR(100), @LeadEventID)

		/*GPR 2020-04-22*/
		EXEC _C00_LogIt 'Info', 'Billing_UpdateWithResponse','SecureCo',  @LogEntry, 58550

		/*GPR 2020-04-21 added NULL check*/
		IF @LeadEventID IS NOT NULL AND @EventToAdd IS NOT NULL 
		BEGIN

			EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID, @EventToAdd, 58552 /*AQ Automation*/, -1

		END

	END
	
END














GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction_UpdateWithResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction_UpdateWithResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction_UpdateWithResponse] TO [sp_executeall]
GO
