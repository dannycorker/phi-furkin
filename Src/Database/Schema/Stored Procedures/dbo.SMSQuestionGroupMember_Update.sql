SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SMSQuestionGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_Update]
(

	@SMSQuestionGroupMemberID int   ,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@SMSQuestionID int   ,

	@SMSQuestionGroupID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SMSQuestionGroupMember]
				SET
					[ClientID] = @ClientID
					,[SMSQuestionnaireID] = @SMSQuestionnaireID
					,[SMSQuestionID] = @SMSQuestionID
					,[SMSQuestionGroupID] = @SMSQuestionGroupID
				WHERE
[SMSQuestionGroupMemberID] = @SMSQuestionGroupMemberID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Update] TO [sp_executeall]
GO
