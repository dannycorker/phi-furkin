SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuestionnaireFonts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_Insert]
(

	@QuestionnaireFontID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@PartNameID int   ,

	@FontFamily varchar (50)  ,

	@FontSize varchar (50)  ,

	@FontColour varchar (50)  ,

	@FontWeight varchar (50)  ,

	@FontAlignment varchar (50)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[QuestionnaireFonts]
					(
					[ClientQuestionnaireID]
					,[PartNameID]
					,[FontFamily]
					,[FontSize]
					,[FontColour]
					,[FontWeight]
					,[FontAlignment]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@PartNameID
					,@FontFamily
					,@FontSize
					,@FontColour
					,@FontWeight
					,@FontAlignment
					,@ClientID
					)
				-- Get the identity value
				SET @QuestionnaireFontID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Insert] TO [sp_executeall]
GO
