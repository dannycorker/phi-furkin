SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-31
-- Description:	Set an automated task to run right now.
-- JWG	2010-02-10 New options to reset to normal scheduled time, or to the specified time passed in
-- SB	2012-01-23 Added new param to allow to hide the __describe call at the end
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__RunNow]
	@TaskID int,
	@RevertToNormalRuntime bit = 0,
	@SetNextRunTimeToThis datetime = null,
	@ShowOutput BIT = 1
AS
BEGIN
	
	DECLARE @CharDate char(10), 
	@RunAtHour int, 
	@RunAtMinute int
	
	/* If we have been given an explicit NextRunDateTime then use it. Otherwise run immediately */
	IF @SetNextRunTimeToThis IS NULL
	BEGIN
		SET @SetNextRunTimeToThis = dbo.fn_GetDate_Local()
	END
	
	/* If we have been told to go back to the regular run pattern then use that instead */
	IF @RevertToNormalRuntime = 1
	BEGIN
		SELECT @CharDate = cast(convert(varchar, @SetNextRunTimeToThis, 126) as char(10))
	
		SELECT @RunAtHour = at.RunAtHour, 
		@RunAtMinute = at.RunAtMinute,
		@CharDate = dbo.fnDateOnlyChar(at.NextRunDateTime)
		FROM dbo.AutomatedTask at (nolock)
		WHERE TaskID = @TaskID
	
		SET @SetNextRunTimeToThis = convert(datetime, @CharDate + ' ' + right('00' + cast(@RunAtHour as varchar), 2) + ':' + right('00' + cast(@RunAtMinute as varchar), 2) + ':00')
	END
	
	/* Apply the update */
	UPDATE dbo.AutomatedTask 
	SET NextRunDateTime = @SetNextRunTimeToThis
	WHERE TaskID = @TaskID
	
	IF @ShowOutput = 1
	BEGIN
		/* Show the latest values */
		EXEC [dbo].[AutomatedTask__Describe] @TaskID 
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__RunNow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__RunNow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__RunNow] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__RunNow] TO [sp_executehelper]
GO
