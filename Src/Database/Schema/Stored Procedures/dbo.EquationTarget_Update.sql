SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EquationTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EquationTarget_Update]
(

	@EquationTargetID int   ,

	@ClientID int   ,

	@EquationDetailFieldID int   ,

	@DetailFieldID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@SqlFunctionID int   ,

	@IsEquation bit   ,

	@IsColumnSum bit   ,

	@ColumnFieldID int   ,

	@DetailFieldSubTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EquationTarget]
				SET
					[ClientID] = @ClientID
					,[EquationDetailFieldID] = @EquationDetailFieldID
					,[DetailFieldID] = @DetailFieldID
					,[Target] = @Target
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
					,[SqlFunctionID] = @SqlFunctionID
					,[IsEquation] = @IsEquation
					,[IsColumnSum] = @IsColumnSum
					,[ColumnFieldID] = @ColumnFieldID
					,[DetailFieldSubTypeID] = @DetailFieldSubTypeID
				WHERE
[EquationTargetID] = @EquationTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget_Update] TO [sp_executeall]
GO
