SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 26/08/2016
-- Description:	Creates history for the Purchased Product
-- 2016-10-12 ACE Updated to include additional fee
-- 2017-11-01 CPS Updated to include LeadEvent
-- 2021-04-15 ACE Added CurrencyID
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductHistory__Backup]
	@PurchasedProductID INT,	-- the product to backup
	@UserID INT
	,@LeadEventID	INT = NULL-- CPS 2017-11-01
AS
BEGIN
		
	SET NOCOUNT ON;
	
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	DECLARE @PurchasedProductCount INT
	SELECT @PurchasedProductCount = COUNT(PurchasedProductID) 
	FROM PurchasedProduct WITH (NOLOCK) WHERE PurchasedProductID=@PurchasedProductID
	IF(@PurchasedProductCount>0)-- a product already exists, so perform backup
	BEGIN
		INSERT INTO PurchasedProductHistory (PurchasedProductID, ClientID, CustomerID, AccountID, PaymentFrequencyID, NumberOfInstallments, ProductPurchasedOnDate, ValidFrom, ValidTo, PreferredPaymentDay, FirstPaymentDate, RemainderUpFront, ProductName, ProductDescription, ProductCostNet, ProductCostVAT, ProductCostGross, ProductCostCalculatedWithRuleSetID, ProductCostCalculatedOn, ProductCostBreakdown, ObjectID, ObjectTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID, PremiumCalculationDetailID, OriginalWhoCreated, OriginalWhenCreated, OriginalWhoModified, OriginalWhenModified, ProductAdditionalFee, CurrencyID)
		SELECT                               PurchasedProductID, ClientID, CustomerID, AccountID, PaymentFrequencyID, NumberOfInstallments, ProductPurchasedOnDate, ValidFrom, ValidTo, PreferredPaymentDay, FirstPaymentDate, RemainderUpFront, ProductName, ProductDescription, ProductCostNet, ProductCostVAT, ProductCostGross, ProductCostCalculatedWithRuleSetID, ProductCostCalculatedOn, ProductCostBreakdown, ObjectID, ObjectTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID, PremiumCalculationDetailID, @UserID,				@WhenCreated,		@UserID,			@WhenCreated,			ProductAdditionalFee, CurrencyID
		FROM PurchasedProduct WITH (NOLOCK) WHERE PurchasedProductID=@PurchasedProductID
    END
    
    IF @LeadEventID <> 0
    BEGIN
		EXEC _C00_SetLeadEventComments @LeadEventID, 'Purchased Product Backed Up', 1
    END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductHistory__Backup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductHistory__Backup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductHistory__Backup] TO [sp_executeall]
GO
