SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeSql table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_Delete]
(

	@EventTypeSqlID int   
)
AS


				DELETE FROM [dbo].[EventTypeSql] WITH (ROWLOCK) 
				WHERE
					[EventTypeSqlID] = @EventTypeSqlID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Delete] TO [sp_executeall]
GO
