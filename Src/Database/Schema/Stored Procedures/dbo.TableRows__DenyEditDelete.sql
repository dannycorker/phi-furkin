SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-01-03
-- Description:	Unlock / Lock table rows by ID
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__DenyEditDelete]
@DenyEdit bit,
@DenyDelete bit,
@TableRowID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE TableRows 
	SET DenyEdit = @DenyEdit, DenyDelete = @DenyDelete
	WHERE TableRowID = @TableRowID
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__DenyEditDelete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__DenyEditDelete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__DenyEditDelete] TO [sp_executeall]
GO
