SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-08-23
-- Description:	Bring old and unclaimed PPS entries up to date
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BringUncollectedScheduleItemsUpToDate]
(
	 @MatterID				INT
	,@LeadEventID			INT
	,@ActivationDate		DATE
	,@AllPetsForCustomer	BIT = 0
)
AS
BEGIN

	DECLARE  @ClientID				INT
			,@WhoCreated			INT = 58552 /*Aquarium Automation*/
			,@PurchasedProductID	INT
			,@AccountID				INT
			,@EventComments			VARCHAR(2000) = ''
			,@RowCount				INT
			,@PurchasedProducts		dbo.tvpIntInt
			,@CustomerID			INT
			
	SELECT @EventComments += 'Bring old schedule items up to date.' + CHAR(13)+CHAR(10)
							+' MatterID: ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + CHAR(13)+CHAR(10)
							+' Activation Date: ' + ISNULL(CONVERT(VARCHAR,@ActivationDate,103),'NULL') + CHAR(13)+CHAR(10)

	SELECT	 @ClientID		= le.ClientID
			,@WhoCreated	= le.WhoCreated
			,@CustomerID	= m.CustomerID
	FROM LeadEvent le WITH ( NOLOCK )
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = le.CaseID
	WHERE le.LeadEventID = @LeadEventID

	INSERT @PurchasedProducts ( ID1, ID2 )
	SELECT	 pp.PurchasedProductID
			,pp.AccountID
	FROM PurchasedProduct pp WITH ( NOLOCK )
	INNER JOIN Matter m WITH (NOLOCK) on pp.PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(m.MatterID,@ActivationDate)
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID AND l.LeadTypeID = 1492 /*Policy Admin*/
	WHERE m.CustomerID = @CustomerID
	AND ( @MatterID = m.MatterID OR @AllPetsForCustomer = 1 )
	
	SELECT @EventComments += ' PurchasedProductID: ' + ISNULL(CONVERT(VARCHAR,pp.ID1),'NULL') + CHAR(13)+CHAR(10)
	FROM @PurchasedProducts pp 
	
	SELECT @ActivationDate = dbo.fn_C00_CalculateActualCollectionDate(@AccountID, 1, dbo.fnAddWorkingDays(@ActivationDate,10), 0, 0.00)
	
	SELECT @EventComments += ' Earliest Valid Date: ' + ISNULL(CONVERT(VARCHAR,@ActivationDate,103),'NULL') + CHAR(13)+CHAR(10)
	
	/*Update the PPS records to match the new dates*/
	UPDATE pps
	SET ActualCollectionDate = @ActivationDate
	FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK )
	INNER JOIN @PurchasedProducts pp on pp.ID1 = pps.PurchasedProductID
	WHERE pps.PaymentStatusID = 1 /*New*/
	AND pps.ActualCollectionDate <= @ActivationDate
	AND pps.PaymentGross >= 0.00
	/*!! NO SQL HERE !!*/
	SELECT @RowCount = @@ROWCOUNT
		
	SELECT @EventComments += ' ' + CONVERT(VARCHAR,@RowCount) + ' records updated.' + CHAR(13)+CHAR(10)
	
	SELECT TOP 1 @AccountID = pp.ID2
	FROM @PurchasedProducts pp
	
	/*Rebuild the CPS*/
	EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @ActivationDate, @WhoCreated
	SELECT @EventComments += ' Customer Payment Schedule rebuilt for AccountID ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL') + '.' + CHAR(13)+CHAR(10)
		
	EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	PRINT @EventComments		
	
	RETURN @RowCount
	
END


















GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BringUncollectedScheduleItemsUpToDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BringUncollectedScheduleItemsUpToDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BringUncollectedScheduleItemsUpToDate] TO [sp_executeall]
GO
