SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-05-19
-- Description:	Soft delete data. IE Move Customer
--				/Lead/Case/Matter to ClientID 20
-- =============================================
CREATE PROCEDURE [dbo].[SoftDeleteDataForDPA]
	@ObjectIDToRemove INT,
	@ObjectLevel INT,
	@DateForDeletion DATETIME,
	@Notes VARCHAR(2000),
	@Debug BIT
AS
BEGIN

	SET NOCOUNT ON;

	/*
		This stored procedure will update, if moving a customer, the entire Customer/Lead/Case/Matter to client 20.
		If it's only set to move part of the Lead/Case/Matter then a new customer will need to be added and the existing 
		lower objects moving to it.
		
		If it's the last lead on a customer, or last case on the last lead, or last matter on last case on last lead then
		the whole Customer can be moved over.
	*/
	
	DECLARE @CustomerID INT,
			@LeadID INT,
			@CaseID INT,
			@ClientID INT,
			@NewCustomerID INT,
			@NewLeadID INT,
			@NewCaseID INT,
			@MatterCount INT,
			@CaseCount INT,
			@LeadCount INT,
			@RemoveWholeCustomer BIT = 0,
			@RemoveWholeLead BIT = 0,
			@RemoveWholeCase BIT = 0,
			@LastRowCount INT,
			@CustomerIDToBeRemoved INT = NULL,
			@LeadIDToBeRemoved INT = NULL,
			@CaseIDToBeRemoved INT = NULL,
			@MatterIDToBeRemoved INT = NULL,
			@CaseIDs tvpInt,
			@LogXmlEntry VARCHAR(MAX)
	
	DECLARE @TableOfIDs TABLE (CustomerID INT, LeadID INT, CaseID INT, MatterID INT)
	
	DECLARE @LogOutput TABLE (UNID INT IDENTITY (1,1), LogArea VARCHAR(50), LogResult INT)
	
	IF @ObjectLevel = 10
	BEGIN
	
		SELECT @CustomerIDToBeRemoved = @ObjectIDToRemove
		
	END
	ELSE
	IF @ObjectLevel = 1
	BEGIN
	
		SELECT @LeadIDToBeRemoved = @ObjectIDToRemove
		
	END
	ELSE
	IF @ObjectLevel = 11
	BEGIN
	
		SELECT @CaseIDToBeRemoved = @ObjectIDToRemove
		
	END
	ELSE
	IF @ObjectLevel = 2
	BEGIN
	
		SELECT @MatterIDToBeRemoved = @ObjectIDToRemove
		
	END
	
	IF (@CustomerIDToBeRemoved > 0 OR @LeadIDToBeRemoved > 0 OR @CaseIDToBeRemoved > 0 OR @MatterIDToBeRemoved > 0)
	BEGIN
	
		/*Only do work if there is a customer, lead, case or matter passed in.*/
		IF @MatterIDToBeRemoved > 0
		BEGIN
		
			/*If the MatterID was passed in then check upwards to see where the move can stop.*/
			SELECT @CaseID = m.CaseID, @LeadID = m.LeadID, @CustomerID = m.CustomerID, @ClientID = m.ClientID
			FROM Matter m WITH (NOLOCK)
			WHERE m.MatterID = @MatterIDToBeRemoved
		
			/*Get the number of matters in this parent case so we can see if the matter and case can be moved*/
			SELECT @MatterCount = COUNT(*)
			FROM Matter m WITH (NOLOCK)
			WHERE m.CaseID = @CaseID
		
			/*Get the number of cases in this parent lead so we can see if the matter, case and lead can be moved*/
			SELECT @CaseCount = COUNT(*)
			FROM Cases c WITH (NOLOCK)
			WHERE c.LeadID = @LeadID
			
			/*
				Get the number of leads for this customer so we can see if we can move the whole customer. 
				Dont want to leave any orphaned leads/customers etc.
			*/
			SELECT @LeadCount = COUNT(*)
			FROM Lead l WITH (NOLOCK)
			WHERE l.CustomerID = @CustomerID
			
			IF @LeadCount = 1
			BEGIN
			
				/*If we have 1 lead then we can remove the customer as well if the below items have only 1 entity*/
				SELECT @RemoveWholeCustomer = 1
			
			END

			IF @CaseCount = 1
			BEGIN
			
				/*There is only one case for this lead so if there is only one matter for the case then the matter, case and lead can go.*/
				SELECT @RemoveWholeLead = 1
			
			END

			IF @MatterCount = 1
			BEGIN
			
				/*As there is only one matter in this case we can move the matter and it's parent case.*/
				SELECT @RemoveWholeCase = 1
			
			END
			
		END
		
		IF @CaseIDToBeRemoved > 0
		BEGIN
		
			/*Get the LeadID of the case that is to be removed*/
			SELECT @LeadID = c.LeadID, @CaseID = c.CaseID 
			FROM Cases c WITH (NOLOCK)
			WHERE c.CaseID = @CaseIDToBeRemoved 
			
			/*Get the CustomerID of the case that is to be removed*/
			SELECT @CustomerID = l.CustomerID, @ClientID = l.ClientID
			FROM Lead l WITH (NOLOCK)
			WHERE l.LeadID = @LeadID
			
			/*Get the number of cases in the lead. If there is only one then we can potentially remove the lead as well*/
			SELECT @CaseCount = COUNT(*)
			FROM Cases c WITH (NOLOCK)
			WHERE c.LeadID = @LeadID
			
			/*Get the number of leads for this customer. If there is only one and there is only one case then the whold lot can get moved*/
			SELECT @LeadCount = COUNT(*)
			FROM Lead l WITH (NOLOCK)
			WHERE l.CustomerID = @CustomerID
			
			IF @LeadCount = 1
			BEGIN
			
				/*As there is only one lead the whole customer can go, however if there is more than one case it cant..*/
				SELECT @RemoveWholeCustomer = 1
			
			END

			IF @CaseCount = 1
			BEGIN
				
				/*Ok, there is only one case for this lead, if there is only one lead for the customer we can move the whole customer*/
				SELECT @RemoveWholeLead = 1
			
			END
			
			/*Set this flag to 1 as we are going to be removing this case anyway*/
			SELECT @RemoveWholeCase = 1
		
		END
		
		IF @LeadIDToBeRemoved > 0
		BEGIN
			
			/*Get the CustomerID of the lead that is to be removed*/
			SELECT @CustomerID = l.CustomerID, @ClientID = l.ClientID, @LeadID = l.LeadID
			FROM Lead l WITH (NOLOCK)
			WHERE l.LeadID = @LeadIDToBeRemoved
			
			/*Get the number of leads on the parent customer*/
			SELECT @LeadCount = COUNT(*)
			FROM Lead l WITH (NOLOCK)
			WHERE l.CustomerID = @CustomerID
		
			IF @LeadCount = 1
			BEGIN
			
				/*If this is the only lead on the customer then we can remove the customer, lead case and matter*/
				SELECT @RemoveWholeCustomer = 1
			
			END
			
			/*Need to set the following so we can determine what to remove later on.*/
			SELECT @RemoveWholeLead = 1,
					@RemoveWholeCase = 1

		END
		
		IF @CustomerIDToBeRemoved > 0
		BEGIN
		
			/*
				Ok, so the CustomerID has been passed in and that means that the whole thing needs to be moved.
			*/
			SELECT @RemoveWholeCustomer = 1,
					@RemoveWholeLead = 1,
					@RemoveWholeCase = 1
			
			SELECT @CustomerID = @CustomerIDToBeRemoved
		
			SELECT @ClientID = c.ClientID
			FROM Customers c WITH (NOLOCK)
			WHERE c.CustomerID = @CustomerIDToBeRemoved
		
		END
		
		IF @RemoveWholeCase = 0
		BEGIN
		
			/*
				So it looks like there are multiple matters in this one case. We will have to create a new
				Customer, lead and case and then move the matter only
			*/
			INSERT INTO Customers (ClientID, TitleID, IsBusiness, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, HomeTelephone, HomeTelephoneVerifiedAndValid, MobileTelephone, MobileTelephoneVerifiedAndValid, CompanyTelephone, CompanyTelephoneVerifiedAndValid, WorksTelephone, WorksTelephoneVerifiedAndValid, Address1, Address2, Town, County, PostCode, Website, HasDownloaded, DownloadedOn, AquariumStatusID, ClientStatusID, Test, CompanyName, Occupation, Employer, PhoneNumbersVerifiedOn, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, DefaultContactID, DefaultOfficeID, AddressVerified, CountryID, SubClientID, CustomerRef, WhoChanged, WhenChanged, ChangeSource, EmailAddressVerifiedAndValid, EmailAddressVerifiedOn, Comments, AllowSmsCommandProcessing)
			VALUES (20, 0, 0, 'Stub', '', 'Stub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
			
			SELECT @NewCustomerID = SCOPE_IDENTITY()
			
			INSERT INTO Lead (ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
			VALUES (20, NULL, @NewCustomerID, 20, 2, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, dbo.fn_GetDate_Local())
			
			SELECT @NewLeadID = SCOPE_IDENTITY()
			
			INSERT INTO Cases (LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
			VALUES (@NewLeadID, 20, 1, 'Case 1', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 2795/*Aquarium Automation*/, dbo.fn_GetDate_Local(), 2795/*Aquarium Automation*/, dbo.fn_GetDate_Local(), NULL)
			
			SELECT @NewCaseID = SCOPE_IDENTITY()
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Move Matter Only', NULL)
			
			/*Add a record to the ArchivedObjects Table*/
			INSERT INTO ArchivedObjects (ClientID, CustomerID, LeadID, CaseID, MatterID, NewCustomerID, NewLeadID, NewCaseID, DateArchived, WhoArchived, DateForDeletion, DateDeleted, Notes, Status)
			VALUES (@ClientID, @CustomerID, @LeadID, @CaseID, @MatterIDToBeRemoved, @NewCustomerID, @NewLeadID, @NewCaseID, dbo.fn_GetDate_Local(), SUSER_NAME(), @DateForDeletion, NULL, @Notes, 1)

			INSERT INTO @TableOfIDs (CustomerID, LeadID, CaseID, MatterID)
			SELECT NULL, NULL, NULL, @MatterIDToBeRemoved
		
			SELECT @CustomerID = NULL, @LeadID = NULL, @CaseID = NULL

		END
		ELSE
		IF @RemoveWholeLead = 0
		BEGIN
		
			/*
				It looks like there is only one matter for this case, we can move the whole case but not its parent lead
			*/
			INSERT INTO Customers (ClientID, TitleID, IsBusiness, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, HomeTelephone, HomeTelephoneVerifiedAndValid, MobileTelephone, MobileTelephoneVerifiedAndValid, CompanyTelephone, CompanyTelephoneVerifiedAndValid, WorksTelephone, WorksTelephoneVerifiedAndValid, Address1, Address2, Town, County, PostCode, Website, HasDownloaded, DownloadedOn, AquariumStatusID, ClientStatusID, Test, CompanyName, Occupation, Employer, PhoneNumbersVerifiedOn, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, DefaultContactID, DefaultOfficeID, AddressVerified, CountryID, SubClientID, CustomerRef, WhoChanged, WhenChanged, ChangeSource, EmailAddressVerifiedAndValid, EmailAddressVerifiedOn, Comments, AllowSmsCommandProcessing)
			VALUES (20, 0, 0, 'Stub', '', 'Stub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
			
			SELECT @NewCustomerID = SCOPE_IDENTITY()
			
			INSERT INTO Lead (ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
			VALUES (20, NULL, @NewCustomerID, 20, 2, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, dbo.fn_GetDate_Local())
			
			SELECT @NewLeadID = SCOPE_IDENTITY()
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Move Matter and Case', NULL)
			
			/*Add a record to the ArchivedObjects Table*/
			INSERT INTO ArchivedObjects (ClientID, CustomerID, LeadID, CaseID, MatterID, NewCustomerID, NewLeadID, NewCaseID, DateArchived, WhoArchived, DateForDeletion, DateDeleted, Notes, Status)
			SELECT @ClientID, @CustomerID, @LeadID, m.CaseID, m.MatterID, @NewCustomerID, @NewLeadID, NULL, dbo.fn_GetDate_Local(), SUSER_NAME(), @DateForDeletion, NULL, @Notes, 1
			FROM Matter m
			WHERE m.CaseID = @CaseID

			INSERT INTO @TableOfIDs (CustomerID, LeadID, CaseID, MatterID)
			SELECT NULL, NULL, m.CaseID, m.MatterID
			FROM Matter m
			WHERE m.CaseID = @CaseID
		
			SELECT @CustomerID = NULL, @LeadID = NULL

		END
		ELSE 
		IF @RemoveWholeCustomer = 0
		BEGIN
		
			/*
				We can move the whole lead, but not the customer. 
			*/
			INSERT INTO Customers (ClientID, TitleID, IsBusiness, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, HomeTelephone, HomeTelephoneVerifiedAndValid, MobileTelephone, MobileTelephoneVerifiedAndValid, CompanyTelephone, CompanyTelephoneVerifiedAndValid, WorksTelephone, WorksTelephoneVerifiedAndValid, Address1, Address2, Town, County, PostCode, Website, HasDownloaded, DownloadedOn, AquariumStatusID, ClientStatusID, Test, CompanyName, Occupation, Employer, PhoneNumbersVerifiedOn, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, DefaultContactID, DefaultOfficeID, AddressVerified, CountryID, SubClientID, CustomerRef, WhoChanged, WhenChanged, ChangeSource, EmailAddressVerifiedAndValid, EmailAddressVerifiedOn, Comments, AllowSmsCommandProcessing)
			VALUES (20, 0, 0, 'Stub', '', 'Stub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
			
			SELECT @NewCustomerID = SCOPE_IDENTITY()
			
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Move Matter, Case and Lead', NULL)
			
			/*Add a record to the ArchivedObjects Table*/
			INSERT INTO ArchivedObjects (ClientID, CustomerID, LeadID, CaseID, MatterID, NewCustomerID, NewLeadID, NewCaseID, DateArchived, WhoArchived, DateForDeletion, DateDeleted, Notes, Status)
			SELECT @ClientID, @CustomerID, m.LeadID, m.CaseID, m.MatterID, @NewCustomerID, NULL, NULL, dbo.fn_GetDate_Local(), SUSER_NAME(), @DateForDeletion, NULL, @Notes, 1
			FROM Matter m
			WHERE m.LeadID = @LeadID

			INSERT INTO @TableOfIDs (CustomerID, LeadID, CaseID, MatterID)
			SELECT NULL, m.LeadID, m.CaseID, m.MatterID
			FROM Matter m
			WHERE m.LeadID = @LeadID
		
			SELECT @CustomerID = NULL
		
		END
		ELSE
		IF @RemoveWholeCustomer = 1
		BEGIN
		
			/*
				Ok, we can remove the whole customer, lead, case and matter.
			*/
			INSERT INTO @LogOutput (LogArea, LogResult)
			VALUES ('Move Matter, Case, Lead and Customer',NULL)
		
			/*Add a record to the ArchivedObjects Table*/
			INSERT INTO ArchivedObjects (ClientID, CustomerID, LeadID, CaseID, MatterID, NewCustomerID, NewLeadID, NewCaseID, DateArchived, WhoArchived, DateForDeletion, DateDeleted, Notes, Status)
			SELECT @ClientID, @CustomerID, m.LeadID, m.CaseID, m.MatterID, NULL, NULL, NULL, dbo.fn_GetDate_Local(), SUSER_NAME(), @DateForDeletion, NULL, @Notes, 1
			FROM Matter m
			WHERE m.CustomerID = @CustomerID
		
			INSERT INTO @TableOfIDs (CustomerID, LeadID, CaseID, MatterID)
			SELECT m.CustomerID, m.LeadID, m.CaseID, m.MatterID
			FROM Matter m
			WHERE m.CustomerID = @CustomerID
		
		END
		
		/*Update portal user case.*/
		UPDATE puc
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, puc.LeadID)
		FROM PortalUserCase puc
		INNER JOIN @TableOfIDs t ON (t.LeadID = puc.LeadID OR t.CaseID = puc.CaseID)
		WHERE puc.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('PortalUserCase', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update automated event queue.*/
		UPDATE aeq 
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, aeq.LeadID), CustomerID = ISNULL(@NewCustomerID, aeq.CustomerID)
		FROM AutomatedEventQueue aeq
		INNER JOIN @TableOfIDs t ON (t.CustomerID = aeq.CustomerID OR t.LeadID = aeq.LeadID OR t.CaseID = aeq.CaseID)
		WHERE aeq.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('AutomatedEventQueue', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update case detail values.*/
		UPDATE cdv
		SET cdv.ClientID = 20 /*You would never need to update the CaseID here..*/
		FROM CaseDetailValues cdv WITH (NOLOCK)
		INNER JOIN @TableOfIDs t ON (t.CaseID = cdv.CaseID)
		WHERE cdv.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CaseDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL
	
		/*Update case transfer mapping.*/
		UPDATE ctm
		SET ClientID = 20, ctm.CustomerID = ISNULL(@NewCustomerID, ctm.CustomerID), ctm.LeadID = ISNULL(@NewLeadID, ctm.LeadID)
		FROM CaseTransferMapping ctm
		INNER JOIN @TableOfIDs t ON (t.CustomerID = ctm.CustomerID OR t.LeadID = ctm.LeadID OR t.CaseID = ctm.CaseID)
		WHERE ctm.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CaseTransferMapping', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update ClientCustomerMails.*/
		UPDATE cm
		SET ClientID = 20
		FROM ClientCustomerMails cm
		INNER JOIN @TableOfIDs t ON (t.CustomerID = cm.CustomerID)
		WHERE cm.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientCustomerMails', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update ClientOfficeLead.*/
		UPDATE col
		SET ClientID = 20
		FROM ClientOfficeLead col
		INNER JOIN @TableOfIDs t ON (t.LeadID = col.LeadID)
		WHERE col.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientOfficeLead', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update ClientPersonnelFavourite.*/
		UPDATE cpf
		SET ClientID = 20
		FROM ClientPersonnelFavourite cpf
		INNER JOIN @TableOfIDs t ON (t.LeadID = cpf.LeadID)
		WHERE cpf.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientPersonnelFavourite', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update Customers.*/
		UPDATE cu
		SET ClientID = 20, Test = 1, ChangeSource = 'SoftDelete For DPA'
		FROM Customers cu
		INNER JOIN @TableOfIDs t ON (t.CustomerID = cu.CustomerID)
		WHERE cu.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Customer', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update contact detail values.*/
		UPDATE cdv
		SET ClientID = 20
		FROM ContactDetailValues cdv
		INNER JOIN Contact c on c.ContactID = cdv.ContactID 
		INNER JOIN @TableOfIDs t ON t.CustomerID = c.CustomerID
		WHERE cdv.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ContactDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update customer answers.*/
		UPDATE CustomerAnswers
		SET ClientID = 20
		FROM CustomerAnswers
		INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = CustomerAnswers.CustomerQuestionnaireID
		INNER JOIN @TableOfIDs t ON t.CustomerID = cq.CustomerID
		WHERE cq.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT

		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerAnswers', CONVERT(VARCHAR,@LastRowCount))

		SELECT @LastRowCount = NULL

		/*Update customer detail values.*/
		UPDATE cdv
		SET ClientID = 20
		FROM CustomerDetailValues cdv
		INNER JOIN @TableOfIDs t ON t.CustomerID = cdv.CustomerID
		WHERE cdv.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update Contact.*/
		UPDATE c
		SET ClientID = 20
		FROM Contact c
		INNER JOIN @TableOfIDs t ON t.CustomerID = c.CustomerID
		WHERE c.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Contact', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update customer history.*/
		UPDATE ch
		SET ClientID = 20
		FROM CustomerHistory ch
		INNER JOIN @TableOfIDs t ON t.CustomerID = ch.CustomerID
		WHERE ch.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT

		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerHistory', CONVERT(VARCHAR,@LastRowCount))

		SELECT @LastRowCount = NULL

		/*Update customer match key.*/
		UPDATE cmk
		SET ClientID = 20
		FROM CustomerMatchKey cmk
		INNER JOIN @TableOfIDs t ON t.CustomerID = cmk.CustomerID
		WHERE cmk.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerMatchKey', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update customer office.*/
		UPDATE co
		SET ClientID = 20
		FROM CustomerOffice co
		INNER JOIN @TableOfIDs t ON t.CustomerID = co.CustomerID
		WHERE co.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerOffice', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update customer outcomes.*/
		UPDATE co
		SET ClientID = 20
		FROM CustomerOutcomes co
		INNER JOIN @TableOfIDs t ON t.CustomerID = co.CustomerID
		WHERE co.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT

		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerOutcomes', CONVERT(VARCHAR,@LastRowCount))

		SELECT @LastRowCount = NULL

		/*Update detailfield lock for matter fields.*/
		UPDATE dfl
		SET ClientID = 20
		FROM DetailFieldLock dfl
		INNER JOIN @TableOfIDs t ON t.MatterID = dfl.LeadOrMatterID
		WHERE dfl.LeadOrMatter = 2
		AND dfl.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update detailfield lock for lead fields.*/
		UPDATE dfl
		SET ClientID = 20
		FROM DetailFieldLock dfl
		INNER JOIN @TableOfIDs t ON t.LeadID = dfl.LeadOrMatterID
		WHERE dfl.LeadOrMatter = 1
		AND dfl.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update detailfield lock for case fields.*/
		UPDATE dfl
		SET ClientID = 20
		FROM DetailFieldLock dfl
		INNER JOIN @TableOfIDs t ON t.CaseID = dfl.LeadOrMatterID
		WHERE dfl.LeadOrMatter = 11
		AND dfl.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Cases)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update detailfield lock for customer fields.*/
		UPDATE dfl
		SET ClientID = 20
		FROM DetailFieldLock dfl
		INNER JOIN @TableOfIDs t ON t.CustomerID = dfl.LeadOrMatterID
		WHERE dfl.LeadOrMatter = 10
		AND dfl.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailFieldLock (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update matter detail value history.*/
		UPDATE dvh
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, dvh.LeadID)
		FROM DetailValueHistory dvh
		INNER JOIN @TableOfIDs t ON t.MatterID = dvh.MatterID
		WHERE dvh.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Matter)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update lead detail value history.*/
		UPDATE dvh
		SET ClientID = 20
		FROM DetailValueHistory dvh
		INNER JOIN @TableOfIDs t ON t.LeadID = dvh.LeadID
		WHERE dvh.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update case detail value history.*/
/*		UPDATE dvh
		SET ClientID = 20
		FROM DetailValueHistory dvh
		INNER JOIN @TableOfIDs t ON t.CaseID = dvh.CaseID
		WHERE dvh.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Cases Of Lead)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL
*/
		/*Update customer detail value history.*/
		UPDATE dvh
		SET ClientID = 20
		FROM DetailValueHistory dvh
		INNER JOIN @TableOfIDs t ON t.CustomerID = dvh.CustomerID
		WHERE dvh.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DetailValueHistory (Customer)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update diary reminder for case.*/
		UPDATE DiaryReminder
		SET ClientID = 20
		FROM DiaryReminder
		INNER JOIN DiaryAppointment d on d.DiaryAppointmentID = DiaryReminder.DiaryAppointmentID
		INNER JOIN @TableOfIDs t ON (t.CaseID = d.CaseID OR t.LeadID = d.LabelID)
		WHERE d.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DiaryReminder (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update diary appointment.*/
		UPDATE da
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, da.LeadID)
		FROM DiaryAppointment da 
		INNER JOIN @TableOfIDs t ON (t.CaseID = da.CaseID OR t.LeadID = da.LeadID)
		WHERE da.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DiaryAppointment (Case)', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

	
		/*Update document batch detail.*/
		UPDATE dbd
		SET ClientID = 20
		FROM DocumentBatchDetail dbd
		INNER JOIN @TableOfIDs t ON t.MatterID = dbd.MatterID
		WHERE dbd.ClientID = @ClientID 
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DocumentBatchDetail', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update dropped out customers.*/
		UPDATE DroppedOutCustomerAnswers
		SET ClientID = 20
		FROM DroppedOutCustomerAnswers doc
		INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = doc.CustomerQuestionnaireID
		INNER JOIN @TableOfIDs t ON t.CustomerID = cq.CustomerID
		WHERE doc.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DroppedOutCustomerAnswers', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update dropped out customer questionnaires.*/
		UPDATE docq
		SET ClientID = 20
		FROM DroppedOutCustomerQuestionnaires docq
		INNER JOIN CustomerQuestionnaires cq ON cq.CustomerQuestionnaireID = docq.CustomerQuestionnaireID
		INNER JOIN @TableOfIDs t ON t.CustomerID = cq.CustomerID
		WHERE docq.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DroppedOutCustomerQuestionnaires', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL
		
		/*Update file export event.*/
		UPDATE FileExportEvent
		SET ClientID = 20
		FROM FileExportEvent
		INNER JOIN LeadEvent le ON le.LeadEventID = FileExportEvent.LeadEventID
		INNER JOIN @TableOfIDs t ON (t.CaseID = le.CaseID)
		WHERE le.CaseID = @CaseID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('FileExportEvent', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update housekeeping leadevent to delete.*/
		UPDATE hsktd
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, hsktd.LeadID)
		FROM HskLeadEventToDelete hsktd
		INNER JOIN @TableOfIDs t ON t.CaseID = hsktd.CaseID
		WHERE hsktd.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('HskLeadEventToDelete', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*
			Call the common SP for the update of documents.
		*/
		INSERT INTO @CaseIDs (AnyID)
		SELECT DISTINCT t.CaseID
		FROM @TableOfIDs t
		WHERE t.CaseID > 0

		--EXEC dbo.LeadDocument__Delete @CaseIDs, @NewLeadID, 0

		/*Update esignature status.*/
		UPDATE LeadDocumentEsignatureStatus 
		SET ClientID = 20
		FROM LeadDocumentEsignatureStatus 
		INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentEsignatureStatus.LeadDocumentID
		INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
		INNER JOIN @TableOfIDs t ON t.CaseID = le.CaseID
		WHERE le.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadDocumentEsignatureStatus', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update housekeeping leadevent to delete.*/
		UPDATE LeadDocumentFS
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, LeadDocumentFS.LeadID)
		FROM LeadDocumentFS
		INNER JOIN LeadDocument ld ON ld.LeadDocumentID = LeadDocumentFS.LeadDocumentID
		INNER JOIN LeadEvent le ON le.LeadDocumentID = ld.LeadDocumentID
		INNER JOIN @TableOfIDs t ON t.CaseID = le.CaseID
		WHERE le.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadDocumentFS', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update leadevent thread completion.*/
		UPDATE LeadEventThreadCompletion
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, LeadEventThreadCompletion.LeadID)
		FROM LeadEventThreadCompletion
		INNER JOIN LeadEvent le ON le.LeadEventID = LeadEventThreadCompletion.FromLeadEventID
		INNER JOIN @TableOfIDs t ON t.CaseID = le.CaseID
		WHERE le.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadEventThreadCompletion', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		-- Set context info to allow access to table
		DECLARE @ContextInfo VARBINARY(100) = CAST('DPADelete' AS VARBINARY)
		SET CONTEXT_INFO @ContextInfo

		/*Update leadevent.*/
		UPDATE LeadEvent 
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, LeadEvent.LeadID)
		FROM LeadEvent 
		INNER JOIN @TableOfIDs t ON t.CaseID = LeadEvent.CaseID
		WHERE LeadEvent.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		SET CONTEXT_INFO 0x0

		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadEvent', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update lead assignment.*/
		UPDATE la 
		SET ClientID = 20
		FROM LeadAssignment la
		INNER JOIN @TableOfIDs t ON t.LeadID = la.LeadID
		WHERE la.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadAssignment', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update lead detail values.*/
		UPDATE ldv
		SET ClientID = 20
		FROM LeadDetailValues ldv 
		INNER JOIN @TableOfIDs t ON t.LeadID = ldv.LeadID
		WHERE ldv.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update lead view history.*/
		UPDATE LeadViewHistory
		SET ClientID = 20
		FROM LeadViewHistory
		INNER JOIN @TableOfIDs t ON t.LeadID = LeadViewHistory.LeadID
		WHERE LeadViewHistory.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadViewHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update lead view history archive.*/
		UPDATE LeadViewHistoryArchive
		SET ClientID = 20
		FROM LeadViewHistoryArchive
		INNER JOIN @TableOfIDs t ON t.LeadID = LeadViewHistoryArchive.LeadID
		WHERE LeadViewHistoryArchive.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('LeadViewHistoryArchive', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update Matterdetailvalues.*/
		UPDATE mdv
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, mdv.LeadID)
		FROM MatterDetailValues mdv
		INNER JOIN @TableOfIDs t ON t.MatterID = mdv.MatterID
		WHERE mdv.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('MatterDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update Matter page choice.*/
		UPDATE mpc
		SET ClientID = 20
		FROM MatterPageChoice mpc
		INNER JOIN @TableOfIDs t ON t.MatterID = mpc.MatterID
		WHERE mpc.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('MatterPageChoice', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*
			MigratonSpecificRecords and MigratonSpecificRecords2 are for transiant data.
			As such we dont need to worry about these 2 tables.
		*/
		
		/*Update Matter page choice.*/
		UPDATE ode
		SET ClientID = 20
		FROM OutcomeDelayedEmails ode
		INNER JOIN @TableOfIDs t ON t.CustomerID = ode.CustomerID
		WHERE ode.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('OutcomeDelayedEmails', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update partner match key.*/
		UPDATE PartnerMatchKey
		SET ClientID = 20
		FROM PartnerMatchKey 
		INNER JOIN Partner p ON p.PartnerID = PartnerMatchKey.PartnerID
		INNER JOIN @TableOfIDs t ON t.CustomerID = p.CustomerID
		WHERE p.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('PartnerMatchKey', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update partner.*/
		UPDATE Partner
		SET ClientID = 20
		FROM Partner 
		INNER JOIN @TableOfIDs t ON t.CustomerID = Partner.CustomerID
		WHERE Partner.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Partner', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update portal user.*/
		UPDATE pu
		SET ClientID = 20, Enabled = 0
		FROM PortalUser pu
		INNER JOIN @TableOfIDs t ON t.CustomerID = pu.CustomerID
		WHERE pu.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('PortalUser', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update quill case mapping (may not be required going forward).*/
		UPDATE QuillCaseMapping
		SET ClientID = 20
		FROM QuillCaseMapping
		INNER JOIN @TableOfIDs t ON t.CaseID = QuillCaseMapping.CaseID 
		WHERE QuillCaseMapping.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('QuillCaseMapping', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update quill customer mapping (may not be required going forward).*/
		UPDATE QuillCustomerMapping
		SET ClientID = 20
		FROM QuillCustomerMapping
		INNER JOIN @TableOfIDs t ON t.CustomerID = QuillCustomerMapping.CustomerID
		WHERE QuillCustomerMapping.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('QuillCustomerMapping', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update quill customer mapping (may not be required going forward).*/
		UPDATE RPIClaimStatus
		SET ClientID = 20
		FROM RPIClaimStatus
		INNER JOIN @TableOfIDs t ON t.MatterID = RPIClaimStatus.MatterID 
		WHERE RPIClaimStatus.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('RPIClaimStatus', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail value history.*/
		UPDATE tdvh
		SET ClientID = 20
		FROM TableDetailValuesHistory tdvh
		INNER JOIN @TableOfIDs t ON t.CustomerID = tdvh.CustomerID 
		WHERE tdvh.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Customer TableDetailValuesHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail value history.*/
		UPDATE tdvh
		SET ClientID = 20
		FROM TableDetailValuesHistory tdvh
		INNER JOIN @TableOfIDs t ON t.LeadID = tdvh.LeadID AND (tdvh .MatterID IS NULL OR tdvh.MatterID = 0)
		WHERE tdvh.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Lead TableDetailValuesHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail value history.*/
		UPDATE tdvh
		SET ClientID = 20
		FROM TableDetailValuesHistory tdvh
		INNER JOIN @TableOfIDs t ON t.CaseID = tdvh.CaseID
		WHERE tdvh.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Cases TableDetailValuesHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail value history.*/
		UPDATE tdvh
		SET ClientID = 20
		FROM TableDetailValuesHistory tdvh
		INNER JOIN @TableOfIDs t ON t.MatterID = tdvh.MatterID
		WHERE tdvh.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Matter TableDetailValuesHistory', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail values.*/
		UPDATE tdv
		SET ClientID = 20
		FROM TableDetailValues tdv
		INNER JOIN @TableOfIDs t ON t.CustomerID = tdv.CustomerID 
		WHERE tdv.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Customer TableDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail values.*/
		UPDATE tdv
		SET ClientID = 20
		FROM TableDetailValues tdv
		INNER JOIN @TableOfIDs t ON t.LeadID = tdv.LeadID AND (tdv.MatterID IS NULL OR tdv.MatterID = 0)
		WHERE tdv.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Lead TableDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail values.*/
		UPDATE tdv
		SET ClientID = 20
		FROM TableDetailValues tdv
		INNER JOIN @TableOfIDs t ON t.CaseID = tdv.CaseID
		WHERE tdv.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Cases TableDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table detail values.*/
		UPDATE tdv
		SET ClientID = 20
		FROM TableDetailValues tdv
		INNER JOIN @TableOfIDs t ON t.MatterID = tdv.MatterID
		WHERE tdv.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Matter TableDetailValues', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table rows.*/
		UPDATE tr
		SET ClientID = 20
		FROM TableRows tr
		INNER JOIN @TableOfIDs t ON t.CustomerID = tr.CustomerID 
		WHERE tr.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Customer TableRows', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table rows.*/
		UPDATE tr
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, tr.LeadID)
		FROM TableRows tr
		INNER JOIN @TableOfIDs t ON t.LeadID = tr.LeadID AND (tr .MatterID IS NULL OR tr.MatterID = 0)
		WHERE tr.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Lead TableRows', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table rows.*/
		UPDATE tr
		SET ClientID = 20
		FROM TableRows tr
		INNER JOIN @TableOfIDs t ON t.CaseID = tr.CaseID
		WHERE tr.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Cases TableRows', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update table rows.*/
		UPDATE tr
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, tr.LeadID)
		FROM TableRows tr
		INNER JOIN @TableOfIDs t ON t.MatterID = tr.MatterID
		WHERE tr.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Matter TableRows', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update workflow task completed.*/
		UPDATE wtc
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, wtc.LeadID)
		FROM WorkflowTaskCompleted wtc
		INNER JOIN @TableOfIDs t ON (t.CaseID = wtc.CaseID OR t.LeadID = wtc.LeadID) 
		WHERE wtc.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('WorkflowTaskCompleted', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update bill, not sure who bill is...*/
		UPDATE b
		SET ClientID = 20, CaseID = ISNULL(@NewCaseID, b.CaseID), LeadID = ISNULL(@NewLeadID, b.LeadID), CustomerID = ISNULL(@NewCustomerID, b.CustomerID)
		FROM Bill b
		INNER JOIN @TableOfIDs t on (t.CustomerID = b.CustomerID OR t.LeadID = b.LeadID OR t.CaseID = b.CaseID)
		WHERE b.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Bill', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update department.*/
		UPDATE d
		SET ClientID = 20
		FROM Department d
		INNER JOIN @TableOfIDs t ON t.CustomerID = d.CustomerID
		WHERE d.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Department', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update dropped out customers.*/
		UPDATE d
		SET ClientID = 20
		FROM DroppedOutCustomers d
		INNER JOIN @TableOfIDs t ON t.CustomerID = d.CustomerID
		WHERE d.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('DroppedOutCustomers', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update matter.*/
		UPDATE m
		SET ClientID = 20, CaseID = ISNULL(@NewCaseID, m.CaseID), LeadID = ISNULL(@NewLeadID, m.LeadID), CustomerID = ISNULL(@NewCustomerID, m.CustomerID)
		FROM Matter m
		INNER JOIN @TableOfIDs t ON t.MatterID = m.MatterID
		WHERE m.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Matter', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update workflow task.*/
		UPDATE wt
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, wt.LeadID)
		FROM WorkflowTask wt 
		INNER JOIN @TableOfIDs t ON t.CaseID = wt.CaseID
		WHERE wt.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('WorkflowTask', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update cases.*/
		UPDATE ca
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, ca.LeadID)
		FROM Cases ca
		INNER JOIN @TableOfIDs t ON t.CaseID = ca.CaseID
		WHERE ca.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Cases', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update customer questionnaires.*/
		UPDATE cq
		SET ClientID = 20
		FROM CustomerQuestionnaires cq
		INNER JOIN @TableOfIDs t ON t.CustomerID = cq.CustomerID
		WHERE cq.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('CustomerQuestionnaires', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update incoming post.*/
		UPDATE ipe 
		SET ClientID = 20, CustomerID = ISNULL(@NewCustomerID, ipe.CustomerID), LeadID = ISNULL(@NewLeadID, ipe.LeadID), CaseID = ISNULL(@NewCaseID, ipe.CaseID)
		FROM IncomingPostEvent ipe
		INNER JOIN @TableOfIDs t ON (t.CustomerID = ipe.CustomerID OR t.LeadID = ipe.LeadID OR t.CaseID = ipe.CaseID OR t.MatterID = ipe.MatterID)
		WHERE ipe.ClientID = @ClientID
		
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('IncomingPostEvent', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update incoming post event value.*/
		UPDATE ipev 
		SET ClientID = 20, CustomerID = ISNULL(@NewCustomerID, ipev.CustomerID), LeadID = ISNULL(@NewLeadID, ipev.LeadID), CaseID = ISNULL(@NewCaseID, ipev.CaseID)
		FROM IncomingPostEventValue ipev
		INNER JOIN @TableOfIDs t ON (t.CustomerID = ipev.CustomerID OR t.LeadID = ipev.LeadID OR t.CaseID = ipev.CaseID OR t.MatterID = ipev.MatterID)
		WHERE ipev.ClientID = @ClientID

		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('IncomingPostEventValue', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update lead.*/
		UPDATE l 
		SET ClientID = 20, CustomerID = ISNULL(@NewCustomerID, l.CustomerID)
		FROM Lead l
		INNER JOIN @TableOfIDs t ON t.LeadID = l.LeadID
		WHERE l.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('Lead', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

		/*Update ClientPersonnelAccess.*/
		UPDATE cpa 
		SET ClientID = 20, LeadID = ISNULL(@NewLeadID, cpa.LeadID)
		FROM ClientPersonnelAccess cpa 
		INNER JOIN @TableOfIDs t ON (t.LeadID = cpa.LeadID OR t.CaseID = cpa.CaseID)
		WHERE cpa.ClientID = @ClientID
	
		SELECT @LastRowCount = @@ROWCOUNT
		
		INSERT INTO @LogOutput (LogArea, LogResult)
		VALUES ('ClientPersonnelAccess', CONVERT(VARCHAR,@LastRowCount))
		
		SELECT @LastRowCount = NULL

	END

	/*LeadDocument (IE From the AquariusDocsXXX needs doing!*/
	
	SELECT *
	FROM @LogOutput

	SELECT @LogXmlEntry = 
	(
		SELECT 	CONVERT(VARCHAR,@ObjectIDToRemove) AS [ObjectID],
				CONVERT(VARCHAR,@ObjectLevel) AS [ObjectLevel],
				CONVERT(VARCHAR(10),@DateForDeletion,120) AS [DeletionDate],
				@Notes AS [Notes],
				CONVERT(VARCHAR,@Debug) AS [Debug],
				CONVERT(VARCHAR,dbo.fn_GetDate_Local(),120) [DateTime],
				SUSER_NAME() AS [UsersName], 
					CAST ( ( SELECT * FROM @LogOutput FOR XML RAW ('CaseList') ) as XML )
		FOR XML RAW ('Values')
	)

	SELECT @LogXmlEntry = @LogXmlEntry + 
	(
		SELECT CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121) [DateTime],
					CAST ( ( SELECT * FROM @TableOfIDs FOR XML RAW ('CaseList') ) as XML )
		FOR XML RAW ('Values')
	)

	INSERT INTO LogXML(ClientID, LogDateTime, ContextID, ContextVarchar, LogXMLEntry)
	VALUES (20, dbo.fn_GetDate_Local(), 0, 'DPADelete', CAST(@LogXmlEntry AS XML))

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SoftDeleteDataForDPA] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SoftDeleteDataForDPA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SoftDeleteDataForDPA] TO [sp_executeall]
GO
