SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-01-09
-- Description:	Show subclients
-- =============================================
CREATE PROCEDURE [dbo].[sc]
@ClientID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM SubClient sc WITH (NOLOCK)
	WHERE ((sc.ClientID = @ClientID and @ClientID IS NOT NULL) OR (@ClientID IS NULL))
	ORDER BY sc.SubClientID DESC

END




GO
GRANT VIEW DEFINITION ON  [dbo].[sc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[sc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[sc] TO [sp_executeall]
GO
