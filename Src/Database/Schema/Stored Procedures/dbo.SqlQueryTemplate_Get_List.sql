SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_Get_List]

AS


				
				SELECT
					[SqlQueryTemplateID],
					[ClientID],
					[Name],
					[Description],
					[TemplatePath],
					[DataWorksheetNumber],
					[DataStartCell],
					[TemplateFormat],
					[FolderID],
					[CreatedBy],
					[CreatedOn],
					[ModifiedBy],
					[ModifiedOn],
					[Enabled]
				FROM
					[dbo].[SqlQueryTemplate] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Get_List] TO [sp_executeall]
GO
