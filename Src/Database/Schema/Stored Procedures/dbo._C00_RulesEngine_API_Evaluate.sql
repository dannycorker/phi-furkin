SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2018-07-06
-- Description:	Web Event Proc: prepare/handle rules engine api call
-- 2019-10-13 GPR for JIRA LPC-35 | created to 603 
-- 2019-10-15 CPS for JIRA LPC-35 | Used client-level config for the RulesEngineService rather than a hard-coded function
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RulesEngine_API_Evaluate]
	 @ContextKey				UNIQUEIDENTIFIER
	,@AquariumEventSubtypeID	INT
	,@ClientID					INT
	,@LeadID					INT
	,@CaseID					INT
	,@ClientPersonnelID			INT
	,@Request					NVARCHAR(MAX) = NULL
	,@Response					NVARCHAR(MAX) = NULL
	,@RequestHeader				NVARCHAR(MAX) = NULL
	,@ResponseHeader			NVARCHAR(MAX) = NULL
	,@LeadEventID				INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	PRINT CHAR(13)+CHAR(10) + OBJECT_NAME(@@ProcID)
	
	DECLARE  @PayloadXml		XML
			,@FullPayloadXml	XML
			,@DetailFieldID		INT
			,@ContextID			INT

	SELECT @ContextID = ISNULL(@LeadEventID,-1)

	-- PREPARE REQUEST
	IF @Response IS NULL
	BEGIN
		DECLARE  @CalculationTypeID		INT
				,@CalculationTypeName	VARCHAR(50)

		/*Find the mapped Calculation Type*/
		SELECT	 @CalculationTypeID = ct.CalculationTypeID
				,@CalculationTypeName = ct.CalculationTypeName
		FROM dbo.LeadEvent le WITH ( NOLOCK )
		INNER JOIN dbo.ThirdPartySystemEvent se WITH ( NOLOCK ) ON se.EventTypeID = le.EventTypeID
		INNER JOIN dbo.CalculationType ct WITH ( NOLOCK ) ON ct.CalculationTypeID = se.ThirdPartySystemKey
		WHERE le.LeadEventID = @LeadEventID
		AND se.MessageName = 'CalculationType'


		-- Web Client Params
		SELECT dbo.fn_C600_GetDatabaseSpecificConfigValue('RulesEngineService') [BaseURL], 'POST' [Method], 'UTF8' [Encoding], 'xml' [PrepareType], 'json' [ApiType], 'xml' [HandleType]
		
		-- Query String (empty row or empty [Name] are ignored)
		SELECT  '' [Name], '' [Value]
		
		-- Header Params (empty row or empty [Name] are ignored)
		SELECT  'Content-Type' [Name], 'application/xml' [Value]
		
		-- pick up the calculation XML
		EXEC dbo.RulesEngine_PrepareCalculation @LeadEventID = @LeadEventID, @ReturnTvpContentsOnly = 0, @XmlOutput = @PayloadXml OUTPUT

		DECLARE @Evaluations TABLE ( EvaluationXml XML NOT NULL )
		INSERT  @Evaluations ( EvaluationXml )
		SELECT  @PayloadXml

		DECLARE  @RequestKey		VARCHAR(2000) = CONVERT(VARCHAR,@LeadEventID) + '_' + ISNULL(CONVERT(VARCHAR,ISNULL(@CalculationTypeID,1)),'NULL')
				,@RequestDetails	VARCHAR(2000) = ISNULL(@CalculationTypeName,'Calculation')

		EXEC dbo._C00_LogIt  @TypeOfLogEntry	= 'Info'  -- varchar(6)
							,@ClassName			= 'RatingEngine'       -- varchar(512)
							,@MethodName		= '_C00_RulesEngine_API_Evaluate'      -- varchar(512)
							,@LogEntry			= 'PayloadReturned'        -- varchar(1024)
							,@ClientPersonnelID	= @ClientPersonnelID -- int

		-- XML Payload
		SELECT @FullPayloadXml =
		(
		SELECT  @RequestKey [RequestKey],
				@RequestDetails [RequestDetails],
				(
				SELECT ev.EvaluationXml [Evaluation]
				FROM @Evaluations ev 
				FOR XML PATH(''), TYPE, ROOT('Evaluations')
				)
		FOR XML PATH(''), TYPE, ROOT('EvaluationRequest')
		)
		
		SELECT @FullPayloadXml AS [Payload]

		EXEC dbo._C00_LogItXml @ClientID, @ContextID, '_C00_RulesEngine_API_Evaluate', @FullPayloadXml

	END
	ELSE
	-- HANDLE RESPONSE
	BEGIN
		
		DECLARE  @PassedOverrides	dbo.tvpIntVarcharVarchar
				,@RuleSetID			INT
				,@EvaluatedXml		XML = @Response
				,@CalculateTypeID	INT = 3
				,@DisplayOnly		BIT = 0
				,@RequestXml		XML = @Request
				,@NoteText			VARCHAR(MAX) = ''
				,@ErrorMessage		VARCHAR(2000) = ''
				,@StartDate			DATE
				,@EndDate			DATE
				,@MatterID			INT
				,@LeadTypeID		INT

		SELECT TOP (1)	 @CalculateTypeID	= se.ThirdPartySystemKey
						,@MatterID			= m.MatterID
						,@LeadTypeID		= l.LeadTypeID
						,@ClientID			= m.ClientID
		FROM dbo.LeadEvent le WITH ( NOLOCK ) 
		INNER JOIN dbo.ThirdPartySystemEvent se WITH ( NOLOCK ) ON se.EventTypeID = le.EventTypeID AND se.MessageName = 'CalculationType'
		INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = le.CaseID
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID
		WHERE le.LeadEventID = @LeadEventID
		ORDER BY m.MatterID

		IF @CalculateTypeID IS NULL
		BEGIN
			SELECT @ErrorMessage = '<br><br><font color="red">Error: No ThirdPartySystem record for EventType ' + ISNULL(CONVERT(VARCHAR,et.EventTypeID) + ' (' + et.EventTypeName + ')','') + '</font>'
			FROM dbo.LeadEvent le WITH ( NOLOCK )
			INNER JOIN dbo.EventType et WITH ( NOLOCK ) ON et.EventTypeID = le.EventTypeID
			WHERE le.LeadEventID = @LeadEventID
			
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END
		ELSE
		IF @CalculateTypeID = 10
		BEGIN
			SELECT @RuleSetID = ISNULL(r.c.value('(@Output)[1]', 'VARCHAR(2000)'),'0')
			FROM @EvaluatedXml.nodes('//Rule[last()]') AS r(c)
			WHERE ISNUMERIC(r.c.value('(@Output)[1]', 'VARCHAR(2000)')) = 1
			
			SELECT @DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 108, 4593) -- AB Controller Decision

			EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @RuleSetID, @LeadID, @ClientPersonnelID /*Stamp the AB controller decision at lead level*/
		END
		ELSE
		BEGIN
			SELECT @RuleSetID = @RequestXml.value('(//RuleSetID)[1]','INT')
		
			INSERT @PassedOverrides
			SELECT	 b.value('(ParameterTypeID)[1]','INT') [ParameterTypeID]
					,b.value('(ParameterValue)[1]','VARCHAR(100)') [ParameterValue]
					,b.value('(Value)[1]','VARCHAR(2000)') [Value]
			FROM @RequestXml.nodes('(//CalculatedValues/*)') a(b)
			WHERE b.value('(Value)[1]','VARCHAR(2000)') <> ''

			EXEC [dbo].[_C00_LogItXML] @ClientID, @CaseID, '_C00_RulesEngine_API_Evaluate', @ResponseHeader, @Response
			EXEC dbo._C00_1273_Policy_CalculatePremiumForPeriod_Evaluated @LeadEventID = @LeadEventID,        -- int
																		  @RuleSetID = @RuleSetID,          -- int
																		  @EvaluatedXml = @EvaluatedXml,    -- xml
																		  @PassedOverrides = @PassedOverrides, -- tvpIntVarcharVarchar
																		  @CalculateTypeID = @CalculateTypeID,    -- int
																		  @DisplayOnly = @DisplayOnly      -- bit



			IF @CalculateTypeID IN ( 3,9 )
			BEGIN
				EXEC dbo._C00_RulesEngine_ReratePolicy_Evaluated @LeadEventID = @LeadEventID, @CalculationTypeID = @CalculateTypeID
			END



			SELECT @NoteText	+=  'RuleSetID: ' + ISNULL(CONVERT(VARCHAR,@RuleSetID),'NULL') + CHAR(13)+CHAR(10)
								 +	'CalculationType: ' + ISNULL(CONVERT(VARCHAR,@CalculateTypeID),'NULL') + ISNULL('(' + ( SELECT TOP 1 ct.CalculationTypeName FROM dbo.CalculationType ct WITH ( NOLOCK ) WHERE ct.CalculationTypeID = @CalculateTypeID ORDER BY ct.CalculationTypeID ) + ')','') + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)


			SELECT @NoteText += 'Result: ' + ISNULL(r.c.value('(@Output)[1]', 'VARCHAR(2000)'),'NULL')
			FROM @EvaluatedXml.nodes('//Rule[last()]') AS r(c)

			SELECT @NoteText += CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)

			SELECT @NoteText += ps.Name + ': ' + ISNULL(owf.AnyValue2,'NULL') + ISNULL(' (' + lli.ItemValue + ')','') + CHAR(13)+CHAR(10)
			FROM @PassedOverrides owf 
			INNER JOIN dbo.RulesEngine_RuleParameters_PreSet ps WITH ( NOLOCK ) ON ps.Value = owf.AnyValue1
			LEFT JOIN dbo.LookupListItems lli WITH ( NOLOCK ) ON CONVERT(VARCHAR(20),lli.LookupListItemID) = owf.AnyValue2 AND ps.DataTypeID = 6 -- LookupList
			ORDER BY ps.Name

		END

	END

	EXEC dbo._C00_LogIt @TypeOfLogEntry = 'Info',  -- varchar(6)
						@ClassName = '_C00_RulesEngine_API_Evaluate',       -- varchar(512)
						@MethodName = @LeadEventID,      -- varchar(512)
						@LogEntry = @NoteText,        -- varchar(1024)
						@ClientPersonnelID = @ClientPersonnelID -- int
	
END













GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RulesEngine_API_Evaluate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RulesEngine_API_Evaluate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RulesEngine_API_Evaluate] TO [sp_executeall]
GO
