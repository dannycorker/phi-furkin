SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ChartSeriesType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesType_Get_List]

AS


				
				SELECT
					[ChartSeriesTypeID],
					[ChartSeriesTypeName]
				FROM
					[dbo].[ChartSeriesType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Get_List] TO [sp_executeall]
GO
