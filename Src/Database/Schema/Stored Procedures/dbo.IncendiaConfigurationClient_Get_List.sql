SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the IncendiaConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_Get_List]

AS


				
				SELECT
					[IncendiaConfigurationClientID],
					[AquariumClientID],
					[Username],
					[Password],
					[URI]
				FROM
					[dbo].[IncendiaConfigurationClient] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Get_List] TO [sp_executeall]
GO
