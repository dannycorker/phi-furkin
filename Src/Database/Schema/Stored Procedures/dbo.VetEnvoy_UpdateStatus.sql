SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-23
-- Description:	Write a row to the history table ready for actioning
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_UpdateStatus]
(
	@ClientID INT,
	@ConversationID UNIQUEIDENTIFIER,
	@Status INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ConversationPK INT,
			@Service VARCHAR(50)
			
	SELECT	@ConversationPK = ConversationPK,
			@Service = Service
	FROM dbo.VetEnvoy_Conversations WITH (NOLOCK) 
	WHERE ConversationID = @ConversationID
	
	IF @ConversationPK > 0
	BEGIN
	
		IF NOT EXISTS
		(
			SELECT *
			FROM dbo.VetEnvoy_ConversationHistory WITH (NOLOCK) 
			WHERE ConversationPK = @ConversationPK
			AND Status >= @Status -- Only send an update if not already sent or have sent a later status code
		)
		BEGIN
		
			INSERT INTO dbo.VetEnvoy_ConversationHistory (ClientID, ConversationPK, ConversationID, Received, Service, Status)
			VALUES (@ClientID, @ConversationPK, @ConversationID, dbo.fn_GetDate_Local(), @Service, @Status)
			
		END
	
	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_UpdateStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_UpdateStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_UpdateStatus] TO [sp_executeall]
GO
