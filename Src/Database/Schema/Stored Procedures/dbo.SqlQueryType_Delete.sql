SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryType_Delete]
(

	@SqlQueryTypeID int   
)
AS


				DELETE FROM [dbo].[SqlQueryType] WITH (ROWLOCK) 
				WHERE
					[SqlQueryTypeID] = @SqlQueryTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_Delete] TO [sp_executeall]
GO
