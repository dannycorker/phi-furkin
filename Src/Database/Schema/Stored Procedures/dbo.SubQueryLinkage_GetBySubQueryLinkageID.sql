SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SubQueryLinkage table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_GetBySubQueryLinkageID]
(

	@SubQueryLinkageID int   
)
AS


				SELECT
					[SubQueryLinkageID],
					[ClientID],
					[SubQueryID],
					[LinksToTableName],
					[LinksToColumnName],
					[LinkageDataType],
					[ParamTableName],
					[ParamColumnName],
					[ParamColumnDataType],
					[ParamColumnHelperSql],
					[LeadTypeFilterSql]
				FROM
					[dbo].[SubQueryLinkage] WITH (NOLOCK) 
				WHERE
										[SubQueryLinkageID] = @SubQueryLinkageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_GetBySubQueryLinkageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_GetBySubQueryLinkageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_GetBySubQueryLinkageID] TO [sp_executeall]
GO
