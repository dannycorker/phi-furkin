SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-09-12
-- Description:	Get Automated Tasks Sql Query
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__GetByClientAndSqlQuery] 
	@ClientID int, 
	@SqlQueryID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	at.*
	FROM	AutomatedTaskParam ap WITH (NOLOCK)
	INNER JOIN AutomatedTask at WITH (NOLOCK) ON at.TaskID = ap.TaskID
	where	at.ClientID  = @ClientID
	and		ParamValue = @SqlQueryID
	and		ParamName = 'SQL_QUERY'
	ORDER BY at.TaskID ASC, at.Enabled DESC
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__GetByClientAndSqlQuery] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__GetByClientAndSqlQuery] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__GetByClientAndSqlQuery] TO [sp_executeall]
GO
