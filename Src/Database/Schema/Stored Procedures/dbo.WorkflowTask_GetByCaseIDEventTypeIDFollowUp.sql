SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowTask table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTask_GetByCaseIDEventTypeIDFollowUp]
(

	@CaseID int   ,

	@EventTypeID int   ,

	@FollowUp bit   
)
AS


				SELECT
					[WorkflowTaskID],
					[WorkflowGroupID],
					[AutomatedTaskID],
					[Priority],
					[AssignedTo],
					[AssignedDate],
					[LeadID],
					[CaseID],
					[EventTypeID],
					[ClientID],
					[FollowUp],
					[Important],
					[CreationDate],
					[Escalated],
					[EscalatedBy],
					[EscalationReason],
					[EscalationDate],
					[Disabled],
					[DisabledReason],
					[DisabledDate]
				FROM
					[dbo].[WorkflowTask] WITH (NOLOCK) 
				WHERE
										[CaseID] = @CaseID
					AND [EventTypeID] = @EventTypeID
					AND [FollowUp] = @FollowUp
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetByCaseIDEventTypeIDFollowUp] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_GetByCaseIDEventTypeIDFollowUp] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetByCaseIDEventTypeIDFollowUp] TO [sp_executeall]
GO
