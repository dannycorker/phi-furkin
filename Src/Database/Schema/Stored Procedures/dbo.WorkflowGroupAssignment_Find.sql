SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the WorkflowGroupAssignment table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroupAssignment_Find]
(

	@SearchUsingOR bit   = null ,

	@WorkflowGroupAssignmentID int   = null ,

	@WorkflowGroupID int   = null ,

	@ClientPersonnelID int   = null ,

	@Priority int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [WorkflowGroupAssignmentID]
	, [WorkflowGroupID]
	, [ClientPersonnelID]
	, [Priority]
	, [ClientID]
    FROM
	[dbo].[WorkflowGroupAssignment] WITH (NOLOCK) 
    WHERE 
	 ([WorkflowGroupAssignmentID] = @WorkflowGroupAssignmentID OR @WorkflowGroupAssignmentID IS NULL)
	AND ([WorkflowGroupID] = @WorkflowGroupID OR @WorkflowGroupID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([Priority] = @Priority OR @Priority IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [WorkflowGroupAssignmentID]
	, [WorkflowGroupID]
	, [ClientPersonnelID]
	, [Priority]
	, [ClientID]
    FROM
	[dbo].[WorkflowGroupAssignment] WITH (NOLOCK) 
    WHERE 
	 ([WorkflowGroupAssignmentID] = @WorkflowGroupAssignmentID AND @WorkflowGroupAssignmentID is not null)
	OR ([WorkflowGroupID] = @WorkflowGroupID AND @WorkflowGroupID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([Priority] = @Priority AND @Priority is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroupAssignment_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Find] TO [sp_executeall]
GO
