SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Logs table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_Find]
(

	@SearchUsingOR bit   = null ,

	@LogID int   = null ,

	@LogDateTime datetime   = null ,

	@TypeOfLogEntry varchar (6)  = null ,

	@ClassName varchar (512)  = null ,

	@MethodName varchar (512)  = null ,

	@LogEntry varchar (MAX)  = null ,

	@ClientPersonnelID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LogID]
	, [LogDateTime]
	, [TypeOfLogEntry]
	, [ClassName]
	, [MethodName]
	, [LogEntry]
	, [ClientPersonnelID]
    FROM
	[dbo].[Logs] WITH (NOLOCK) 
    WHERE 
	 ([LogID] = @LogID OR @LogID IS NULL)
	AND ([LogDateTime] = @LogDateTime OR @LogDateTime IS NULL)
	AND ([TypeOfLogEntry] = @TypeOfLogEntry OR @TypeOfLogEntry IS NULL)
	AND ([ClassName] = @ClassName OR @ClassName IS NULL)
	AND ([MethodName] = @MethodName OR @MethodName IS NULL)
	AND ([LogEntry] = @LogEntry OR @LogEntry IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LogID]
	, [LogDateTime]
	, [TypeOfLogEntry]
	, [ClassName]
	, [MethodName]
	, [LogEntry]
	, [ClientPersonnelID]
    FROM
	[dbo].[Logs] WITH (NOLOCK) 
    WHERE 
	 ([LogID] = @LogID AND @LogID is not null)
	OR ([LogDateTime] = @LogDateTime AND @LogDateTime is not null)
	OR ([TypeOfLogEntry] = @TypeOfLogEntry AND @TypeOfLogEntry is not null)
	OR ([ClassName] = @ClassName AND @ClassName is not null)
	OR ([MethodName] = @MethodName AND @MethodName is not null)
	OR ([LogEntry] = @LogEntry AND @LogEntry is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Find] TO [sp_executeall]
GO
