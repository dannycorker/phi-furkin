SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-09-2014
-- Description:	Gets the else tag for the given document standard target
-- =============================================
CREATE PROCEDURE [dbo].[DocumentStandardTarget__GetElse]

	@DocumentTypeID INT,
	@RuleID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM DocumentStandardTarget WITH (NOLOCK) 
	WHERE DocumentTypeID=@DocumentTypeID AND IsSpecial=1 AND  Target LIKE '%ELSE%' + CAST(@RuleID AS VARCHAR) + '%'


END


GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget__GetElse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget__GetElse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget__GetElse] TO [sp_executeall]
GO
