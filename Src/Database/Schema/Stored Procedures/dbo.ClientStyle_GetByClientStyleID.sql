SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientStyle table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_GetByClientStyleID]
(

	@ClientStyleID int   
)
AS


				SELECT
					[ClientStyleID],
					[ClientID],
					[StyleRuleID],
					[StyleValue]
				FROM
					[dbo].[ClientStyle] WITH (NOLOCK) 
				WHERE
										[ClientStyleID] = @ClientStyleID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_GetByClientStyleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_GetByClientStyleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_GetByClientStyleID] TO [sp_executeall]
GO
