SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the InvoiceMappingEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_Update]
(

	@InvoiceMappingEventID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@EventTypeID int   ,

	@Type char (1)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[InvoiceMappingEvent]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[EventTypeID] = @EventTypeID
					,[Type] = @Type
				WHERE
[InvoiceMappingEventID] = @InvoiceMappingEventID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Update] TO [sp_executeall]
GO
