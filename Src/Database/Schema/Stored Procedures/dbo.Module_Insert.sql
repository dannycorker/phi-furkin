SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Module table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Module_Insert]
(

	@ModuleID int   ,

	@ModuleName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[Module]
					(
					[ModuleID]
					,[ModuleName]
					)
				VALUES
					(
					@ModuleID
					,@ModuleName
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Module_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Module_Insert] TO [sp_executeall]
GO
