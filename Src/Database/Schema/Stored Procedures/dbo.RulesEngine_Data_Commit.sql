SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-19
-- Description:	commits import
-- IS 2016-10-03 Ticket #39889: Underwriting - Breed Risk Level Importing issues 
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Data_Commit]
(
	@ClientID INT,
	@UserID INT,
	@ImportID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @ImportXml XML,
			@ParentItemID INT,
			@ImportType VARCHAR(250),
			@TotalRows INT = 0

	SELECT	@ImportXml = LogXMLEntry,
			@ParentItemID = ContextID,
			@ImportType = ContextVarchar
	FROM	LogXML WITH (NOLOCK) 
	WHERE	LogXMLID = @ImportID


	IF @ImportType = 'Options'
	BEGIN
	
		INSERT INTO RulesEngine_ParameterOptions (RuleParameterID, OperatorID, Val1, Val2, OptionOrder, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified)
		SELECT	ds.RuleParameterID, 
				ds.OperatorID, 
				COALESCE(CAST(ds.LookupListItemID1 AS VARCHAR(20)), ds.Value1), 
				COALESCE(CAST(ds.LookupListItemID2 AS VARCHAR(20)), ds.Value2), 
				0, 
				@ClientID, 
				dbo.fn_GetDate_Local(), 
				@UserID, 
				dbo.fn_GetDate_Local(), 
				@UserID
		FROM	dbo.fn_RulesEngine_ParameterOptions_Parse(@ClientID, @UserID, @ParentItemID, @ImportXml) ds
		WHERE	ds.ParameterOptionID IS NULL	
		AND		ds.Error = ''
		AND		ds.RowID > 1
	
		SELECT @TotalRows = @TotalRows + @@ROWCOUNT
		
		UPDATE po
			SET OperatorID = ds.OperatorID,
				Val1 = COALESCE(CAST(ds.LookupListItemID1 AS VARCHAR(20)), ds.Value1), 
				Val2 = COALESCE(CAST(ds.LookupListItemID2 AS VARCHAR(20)), ds.Value2),
				WhenModified = dbo.fn_GetDate_Local(), 
				WhoModified = @UserID
		FROM	RulesEngine_ParameterOptions po
		INNER JOIN dbo.fn_RulesEngine_ParameterOptions_Parse(@ClientID, @UserID, @ParentItemID, @ImportXml) ds ON ds.ParameterOptionID = po.ParameterOptionID
		WHERE	ds.Error = ''
		AND		ds.RowID > 1
		
		SELECT @TotalRows = @TotalRows + @@ROWCOUNT

	END
	ELSE IF @ImportType = 'Outputs'
	BEGIN
		
		DECLARE @DeltaTable TABLE  (RuleID INT, ParameterOptionID INT, TransformID INT, Value VARCHAR(250), CoordID INT)
		
		-- IS 2016-10-03 Ticket #39889: Underwriting - Breed Risk Level Importing issues 
		INSERT INTO @DeltaTable
		SELECT	
			@ParentItemID RuleID,
			pc.AnyValue,
			tr.TransformID,
			o.Value,
			CHECKSUM(CAST(o.RowID AS VARCHAR) + o.Coordinates)
		FROM	fn_RulesEngine_Outputs_Parse(@ClientID, @UserID, @ParentItemID, @ImportXml) o
		INNER JOIN RulesEngine_Transforms tr WITH (NOLOCK) ON tr.Name = o.Transform
		CROSS APPLY fnTableOfValuesFromCSV(REPLACE(o.Coordinates, '|', ',')) pc
		WHERE	Error = ''
		

		DELETE co
		FROM dbo.RulesEngine_OutputCoordinates co
		INNER JOIN dbo.RulesEngine_RuleOutputs o ON o.RuleOutputID = co.RuleOutputID
		WHERE o.RuleID = @ParentItemID	

		DELETE dbo.RulesEngine_RuleOutputs
		WHERE RuleID = @ParentItemID
		
		INSERT INTO RulesEngine_RuleOutputs (RuleID, Value, TransformID, SourceID)
		SELECT	 RuleID, Value, TransformID, CoordID
		FROM	 @DeltaTable
		GROUP BY RuleID, Value, TransformID, CoordID

		SELECT @TotalRows = @TotalRows + @@ROWCOUNT
		
		INSERT INTO RulesEngine_OutputCoordinates (RuleOutputID,ParameterOptionID)
		SELECT	 ro.RuleOutputID, dt.ParameterOptionID
		FROM	 @DeltaTable dt
		INNER JOIN RulesEngine_RuleOutputs ro ON ro.SourceID = dt.CoordID AND ro.RuleID = dt.RuleID

		UPDATE ro
		SET		ro.SourceID = NULL
		FROM RulesEngine_RuleOutputs ro WITH (NOLOCK) 
		WHERE ro.RuleID = @ParentItemID
		
		
	END

	SELECT @TotalRows TotalRows
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Data_Commit] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Data_Commit] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Data_Commit] TO [sp_executeall]
GO
