SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-18
-- Description:	Gets test file header for bulk evaluation
-- IS	2016-08-17	Added Evaluate Ruleset By RuleSet/ChangeSet/ByEffectiveDate
-- IS	2016-11-23  redesign query, to improve performance
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_TestFile_Get]
(
	@ClientID INT, 
	@UserID INT, 
	@RuleSetID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	-- redesign query, to improve performance
	SELECT	TOP 100
		tf.TestFileID, tf.FileName, tf.WhenCreated, cs.ChangeSetID, cs.TagName ChangeSetName, rs.RuleSetID, rs.Name RuleSetName,
		(
			SELECT	COUNT(*) 
			FROM	RulesEngine_TestFileItem WITH (NOLOCK) 
			WHERE	TestFileID = tf.TestFileID
		) ItemCount,
		(
			SELECT	COUNT(DISTINCT tfir.TestFileItemID) 
			FROM	RulesEngine_TestFileItem tfi WITH (NOLOCK)
			INNER JOIN  RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfi.TestFileItemID = tfir.TestFileItemID
			WHERE	tfi.TestFileID = tf.TestFileID
		) ProcessedItemCount
	FROM	dbo.RulesEngine_TestFile tf WITH (NOLOCK) 
	INNER JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON cs.ChangeSetID = tf.ChangeSetID
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = tf.RuleSetID
	WHERE	rs.RuleSetID = @RuleSetID
	AND		rs.ClientID = @ClientID
	ORDER BY tf.TestFileID DESC

	--SELECT TOP 100
	--	tf.TestFileID, tf.FileName, tf.WhenCreated, cs.ChangeSetID, cs.TagName ChangeSetName, rs.RuleSetID, rs.Name RuleSetName, 
	--	COUNT(DISTINCT tfi.TestFileItemID) ItemCount,
	--	COUNT(DISTINCT tfir.TestFileItemID) ProcessedItemCount
	--FROM dbo.RulesEngine_TestFile tf WITH (NOLOCK) 
	--INNER JOIN RulesEngine_TestFileItem tfi WITH (NOLOCK) ON tf.TestFileID = tfi.TestFileID
	--LEFT JOIN RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfir.TestFileItemID = tfi.TestFileItemID
	--INNER JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON cs.ChangeSetID = tf.ChangeSetID
	--INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = tf.RuleSetID
	--WHERE	rs.RuleSetID = @RuleSetID
	--AND		rs.ClientID = @ClientID
	--GROUP BY tf.TestFileID, tf.FileName, tf.WhenCreated, cs.ChangeSetID, cs.TagName, rs.RuleSetID, rs.Name
	--ORDER BY tf.TestFileID DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_TestFile_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_TestFile_Get] TO [sp_executeall]
GO
