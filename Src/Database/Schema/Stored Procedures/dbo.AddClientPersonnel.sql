SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddClientPersonnel    Script Date: 08/09/2006 12:22:49 ******/

CREATE PROCEDURE [dbo].[AddClientPersonnel]
@ClientID int,
@ClientOfficeID int,
@TitleID int,
@FirstName nvarchar(100),
@MiddleName nvarchar(100), 
@LastName nvarchar(100),
@JobTitle nvarchar(100),
@Password nvarchar(65), 
@MobileTelephone nvarchar(50), 
@EmailAddress nvarchar(255),
@HomeTelephone nvarchar(50),
@OfficeTelephone nvarchar(50),
@OfficeExt nvarchar(10),
@Salt nvarchar(25),
@AttemptedLogins int

as

DECLARE @UserCount int

SELECT @UserCount = count(*) from ClientPersonnel where ClientID = @ClientID

insert into ClientPersonnel 
(ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, 
JobTitle, ClientPersonnel.Password, MobileTelephone,
EmailAddress, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, Salt, AttemptedLogins, ClientPersonnelAdminGroupID) 
values (@ClientID, @ClientOfficeID,@TitleID, @FirstName, @MiddleName, @LastName, 
@JobTitle, @Password, @MobileTelephone,
@EmailAddress, @HomeTelephone, @OfficeTelephone, @OfficeExt, @Salt, @AttemptedLogins, CASE @UserCount WHEN 0 THEN 1 ELSE 2 END)




GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientPersonnel] TO [sp_executeall]
GO
