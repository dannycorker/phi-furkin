SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		James Lewis
-- Create date: 2016-07-21
-- Description:	Once the shceme uploader Datamap has run, trigger this as an automated task to find or create the resourcelist entry for that treatment/limit 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Reconcile_Scheme_Upload] 

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @CutOff	DATETIME = DATEADD(Minute,4,CURRENT_TIMESTAMP)
			,@ClientID	INT		 = dbo.fnGetPrimaryClientID()

	DECLARE @RowsToInsert TABLE (TableRowID INT, Section INT, Sub INT, RLID INT,LeadID INT, MatterID INT) 

	INSERT INTO @RowsToInsert (TableRowID,Section,Sub,RLID, LeadID, MatterID) 
	/*Find the values where the lookuplist matched*/ 
	SELECT tr.TableRowID, tdv.ValueInt, tdv1.ValueInt,0, tdv.LeadID, tdv.MatterID
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 176946 and tdv.ValueInt > 0 -- Seciton 
	INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 176947 and tdv1.ValueInt > 0 -- sub 
	and not exists (select * FROM TableDetailValues tdv2 WITH (NOLOCK) 
					where tdv2.DetailFieldID = 144357
					and tdv2.ResourceListID > 0
					and tdv2.TableRowID = tr.TableRowID) 
	
	/*Find the Values where the lookuplist match failed, but we did get a free text match*/ 
	INSERT INTO @RowsToInsert (TableRowID,Section,Sub,RLID, LeadID, MatterID) 
	SELECT tr.TableRowID, tdv.ValueInt, l.LookupListItemID,0, tdv.LeadID, tdv.MatterID
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 176946 and tdv.ValueInt > 0 -- Seciton 
	INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 176947 and tdv1.ValueInt = 0 -- sub
	INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID =  176957 and tdv2.DetailValue <> ''
	INNER JOIN dbo.LookupListItems l WITH (NOLOCK) on l.ItemValue = tdv2.DetailValue and l.LookupListID = 3722
	and not exists (select * FROM TableDetailValues tdv2 WITH (NOLOCK) 
					where tdv2.DetailFieldID = 144357
					and tdv2.ResourceListID > 0
					and tdv2.TableRowID = tr.TableRowID) 
					

	UPDATE @RowsToInsert 
	SET RLID = rl.ResourceListID 
	FROM dbo.ResourceList rl WITH (NOLOCK)
	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = rl.ResourceListID  and rldv.DetailFieldID = 146189
	INNER JOIN dbo.ResourceListDetailValues rldv1 WITH (NOLOCK) on rldv1.ResourceListID = rl.ResourceListID and rldv1.DetailFieldID = 146190 
	INNER JOIN @RowsToInsert a on a.Section = rldv.ValueINT and a.sub = rldv1.ValueINT 

	WHILE EXISTS (SELECT * FROM @RowsToInsert a Where a.RLID  = 0 ) and CURRENT_TIMESTAMP < @CutOff /*Cutoff incase of timeouts*/ 
	BEGIN 

		DECLARE @TableRowID INT,
				@ResourceListID INT 
				
		SELECT TOP 1 @TableRowID = a.TableRowID FROM @RowsToInsert a 
		where a.RLID = 0 
		
		INSERT INTO ResourceList (ClientID) 
		VALUES (@ClientID) 
		SELECT @ResourceListID = SCOPE_IDENTITY() 
		
		INSERT INTO ResourceListDetailValues (ClientID,DetailFieldID,DetailValue,ResourceListID,LeadOrMatter) 
		SELECT @ClientID,146189,a.Section,@ResourceListID,4
		FROM @RowsToInsert a where a.TableRowID = @TableRowID 

		INSERT INTO ResourceListDetailValues (ClientID,DetailFieldID,DetailValue,ResourceListID,LeadOrMatter) 
		SELECT @ClientID,146190,a.Sub,@ResourceListID,4
		FROM @RowsToInsert a where a.TableRowID = @TableRowID 
		
		UPDATE @RowsToInsert 
		set RLID = @ResourceListID 
		where TableRowID = @TableRowID 
		
	END

		INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,LeadID,MatterID,ClientID)
		Select a.TableRowID,a.RLID,144357,a.LeadID,a.MatterID,@ClientID
		FROM @RowsToInsert a
		where a.RLID > 0 
		
	
	
	/*Trigger the batch again if we still have rows to rec*/ 	
	IF EXISTS (SELECT * FROM @RowsToInsert a Where a.RLID = 0)  
	BEGIN 
		EXEC AutomatedTask__RunNow  20806,0,@CutOff,0  
	END
	
	SELECT *
	FROM @RowsToInsert
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Reconcile_Scheme_Upload] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Reconcile_Scheme_Upload] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Reconcile_Scheme_Upload] TO [sp_executeall]
GO
