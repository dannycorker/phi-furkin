SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] 
         AS
         SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification   



GO
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [sp_executeall]
GO
