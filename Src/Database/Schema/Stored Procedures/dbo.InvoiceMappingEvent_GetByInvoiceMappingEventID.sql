SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the InvoiceMappingEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_GetByInvoiceMappingEventID]
(

	@InvoiceMappingEventID int   
)
AS


				SELECT
					[InvoiceMappingEventID],
					[ClientID],
					[LeadTypeID],
					[EventTypeID],
					[Type]
				FROM
					[dbo].[InvoiceMappingEvent] WITH (NOLOCK) 
				WHERE
										[InvoiceMappingEventID] = @InvoiceMappingEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_GetByInvoiceMappingEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_GetByInvoiceMappingEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_GetByInvoiceMappingEventID] TO [sp_executeall]
GO
