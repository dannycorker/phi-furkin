SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DroppedOutCustomerAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerAnswers_Insert]
(

	@CustomerAnswerID int    OUTPUT,

	@CustomerQuestionnaireID int   ,

	@MasterQuestionID int   ,

	@Answer varchar (MAX)  ,

	@QuestionPossibleAnswerID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[DroppedOutCustomerAnswers]
					(
					[CustomerQuestionnaireID]
					,[MasterQuestionID]
					,[Answer]
					,[QuestionPossibleAnswerID]
					,[ClientID]
					)
				VALUES
					(
					@CustomerQuestionnaireID
					,@MasterQuestionID
					,@Answer
					,@QuestionPossibleAnswerID
					,@ClientID
					)
				-- Get the identity value
				SET @CustomerAnswerID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerAnswers_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerAnswers_Insert] TO [sp_executeall]
GO
