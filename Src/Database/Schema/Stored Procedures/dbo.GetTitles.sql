SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetTitles    Script Date: 08/09/2006 12:22:42 ******/

CREATE PROCEDURE [dbo].[GetTitles] AS
SELECT Titles.TitleID, Titles.Title
FROM Titles
ORDER BY Titles.Title ASC





GO
GRANT VIEW DEFINITION ON  [dbo].[GetTitles] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTitles] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTitles] TO [sp_executeall]
GO
