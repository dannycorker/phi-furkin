SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[XeroHandleResponse] 
(
	@ResponseXML XML,
	@QueryID int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@ClientID int,
		@XMLContainerName varchar(100),
		@iPrepareDoc int,
		@RowCountAffected int = 0

	SELECT
		@ClientID = ClientID,
		@XMLContainerName = t.XMLContainerName
	FROM dbo.ThirdPartyMappingQuery q WITH (NOLOCK) 
	INNER JOIN dbo.ThirdPartyMappingTemplate t WITH (NOLOCK) ON q.ThirdPartySystemID = t.ThirdPartySystemID AND q.ThirdPartyFieldGroupID = t.ThirdPartyFieldGroupID
	WHERE QueryID = @QueryID

	--SELECT @ResponseXML = REPLACE(@ResponseXML, '''', '')

	exec dbo.LogXML__AddEntry @ClientID, @QueryID, 'XeroHandleResponse', @ResponseXML
	
	EXEC sp_xml_preparedocument @iPrepareDoc OUTPUT, @ResponseXML

	IF @XMLContainerName = 'Invoices'
	BEGIN
		DECLARE @XeroInvoices TABLE
		(
			Date Datetime,
			DueDate Datetime,
			Status varchar(50),
			LineAmountTypes varchar(50),
			SubTotal money,
			TotalTax money,
			Total money,
			UpdatedDateUTC Datetime,
			CurrencyCode varchar(50),
			Type varchar(50),
			InvoiceID varchar(50),
			InvoiceNumber varchar(100),
			Reference varchar(50),
			AmountDue money,
			AmountPaid money,
			ContactName varchar(100),
			ContactID varchar(50),
			XeroError varchar(250)
		);

		INSERT INTO @XeroInvoices
		(
			Date,
			DueDate,
			Status,
			LineAmountTypes,
			SubTotal,
			TotalTax,
			Total,
			UpdatedDateUTC,
			CurrencyCode,
			Type,
			InvoiceID,
			InvoiceNumber,
			Reference,
			AmountDue,
			AmountPaid,
			ContactName,
			ContactID,
			XeroError)
		SELECT		
			Date,
			DueDate,
			Status,
			LineAmountTypes,
			SubTotal,
			TotalTax,
			Total,
			CONVERT(varchar(23), CAST(UpdatedDateUTC as datetime), 120) UpdatedDateUTC,
			CurrencyCode,
			Type,
			InvoiceID,
			REPLACE(InvoiceNumber, '''', ''),
			Reference,
			AmountDue,
			AmountPaid,
			Contact.value('/Contact[1]/Name[1]', 'varchar(100)') ContactName,
			Contact.value('/Contact[1]/ContactID[1]', 'varchar(50)') ContactID,
			ValidationErrors.value('/ValidationErrors[1]/ValidationError[1]/Message[1]', 'varchar(250)') XeroError	
		FROM OPENXML (@iPrepareDoc, '/Invoices/Invoice', 2)
		WITH
		(	
			Date Datetime,
			DueDate Datetime,
			Status varchar(50),
			LineAmountTypes varchar(50),
			SubTotal money,
			TotalTax money,
			Total money,
			UpdatedDateUTC Datetime,
			CurrencyCode varchar(50),
			Type varchar(50),
			InvoiceID varchar(50),
			InvoiceNumber varchar(100),
			Reference varchar(50),
			AmountDue money,
			AmountPaid money,
			Contact xml,
			Payments xml,
			ValidationErrors xml
		)

		UPDATE TableDetailValues
			SET DetailValue = unpvt.RealValue
		FROM TableDetailValues tdv
		INNER JOIN 
			(
				SELECT	
						tdv.TableRowID,
						COALESCE(CONVERT(varchar(255), xer.Date,120),'') as [134773],
						COALESCE(CONVERT(varchar(255), xer.DueDate,120),'') as [134774],
						COALESCE(CONVERT(varchar(255), xer.ContactName),'') as [134772],
						COALESCE(CONVERT(varchar(255), xer.Total),'') as [134777],
						COALESCE(CONVERT(varchar(255), xer.Status),'') as [134778],
						COALESCE(CONVERT(varchar(255), xer.AmountDue),'') as [134779],
						COALESCE(CONVERT(varchar(255), xer.AmountPaid),'') as [134780],
						COALESCE(CONVERT(varchar(255), xer.InvoiceID),'') as [134781], /*Xero Invoice ID*/
						COALESCE(CONVERT(varchar(255), xer.UpdatedDateUTC,120),'') as [134782],
						COALESCE(CONVERT(varchar(255), xer.XeroError),'') as [135886]
				FROM	TableDetailValues tdv WITH (NOLOCK)
				INNER JOIN @XeroInvoices xer 
					ON tdv.ClientID = @ClientID AND tdv.DetailFieldID = 134771 AND tdv.DetailValue = xer.InvoiceNumber
			) p
			UNPIVOT
			(
				RealValue FOR ColumnName IN
				(
					[134772], [134773], [134774], [134777], [134778], [134779], [134780], [134781], [134782], [135886]
				)
			) as unpvt
		ON tdv.TableRowID = unpvt.TableRowID
		AND	tdv.DetailFieldID = Cast(unpvt.ColumnName as int)
		
		SET @ResponseXML =
		(
			select * from @XeroInvoices FOR XML AUTO
		)
		
		exec dbo.LogXML__AddEntry @ClientID, @QueryID, 'XeroHandledInvoicesResponse', @ResponseXML
		
		SELECT @RowCountAffected = @@ROWCOUNT / 10
		
		/*Copy Amount Due down to Matter Level so Tony can use it in documents.  Added by CS for support ticket #12561*/
		UPDATE MatterDetailValues 
		SET DetailValue = xer.AmountDue
		FROM TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN @XeroInvoices xer ON tdv.ClientID = @ClientID AND tdv.DetailFieldID = 134771 AND tdv.DetailValue = xer.InvoiceNumber /*Invoice Number*/
		INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tdv.TableRowID and tdv2.DetailFieldID = 134770 /*Matter ID*/
		INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = tdv2.ValueInt
		INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 148171 /*Xero Outstanding Amount*/
		/*Copy Amount Due down to Matter Level so Tony can use it in documents.  Added by CS for support ticket #12561*/
	END
	
	IF @XMLContainerName = 'Payments'
	BEGIN
	
		DECLARE @XeroPayments TABLE
		(
			IdentityPaymentID int identity,
			PaymentID varchar(50),
			Date Datetime,
			Amount money,
			Reference varchar(100),
			CurrencyRate money,
			PaymentType varchar(50),
			Status varchar(50),
			UpdatedDateUTC Datetime,
			AccountID varchar(50),
			AccountCode varchar(50),
			InvoiceContactID varchar(50),
			InvoiceContactName varchar(100),
			InvoiceLineAmountTypes varchar(50),
			InvoiceCurrencyCode varchar(50),
			InvoiceType varchar(50),
			InvoiceID varchar(50),
			InvoiceNumber varchar(100),
			TableRowID int,
			CustomerID int,
			LeadType varchar(50),
			MatterID varchar(50),
			XeroError varchar(250)
		);

		DECLARE @XeroPaymentTableRows TABLE (TableRowID int, IdentityPaymentID int)

		INSERT INTO @XeroPayments
		(
			PaymentID,
			Date,
			Amount,
			Reference,
			CurrencyRate,
			PaymentType,
			Status,
			UpdatedDateUTC,
			AccountID,
			AccountCode,
			InvoiceContactID,
			InvoiceContactName,
			InvoiceLineAmountTypes,
			InvoiceCurrencyCode,
			InvoiceType,
			InvoiceID,
			InvoiceNumber,
			XeroError
		)
		SELECT		
			PaymentID,
			Date,
			Amount,
			Reference,
			CurrencyRate,
			PaymentType,
			Status,
			CONVERT(varchar(23), CAST(UpdatedDateUTC as datetime), 120) UpdatedDateUTC,
			Account.value('/Account[1]/AccountID[1]', 'varchar(50)') AccountID,
			Account.value('/Account[1]/Code[1]', 'varchar(50)') AccountCode,
			Invoice.value('/Invoice[1]/Contact[1]/ContactID[1]', 'varchar(50)') InvoiceContactID,
			Invoice.value('/Invoice[1]/Contact[1]/Name[1]', 'varchar(50)') InvoiceContactName,
			Invoice.value('/Invoice[1]/LineAmountTypes[1]', 'varchar(50)') InvoiceLineAmountTypes,
			Invoice.value('/Invoice[1]/CurrencyCode[1]', 'varchar(50)') InvoiceCurrencyCode,
			Invoice.value('/Invoice[1]/Type[1]', 'varchar(50)') InvoiceType,
			Invoice.value('/Invoice[1]/InvoiceID[1]', 'varchar(50)') InvoiceID,
			REPLACE(Invoice.value('/Invoice[1]/InvoiceNumber[1]', 'varchar(50)'),'''', '') InvoiceNumber,
			ValidationErrors.value('/ValidationErrors[1]/ValidationError[1]/Message[1]', 'varchar(250)') XeroError
		FROM OPENXML (@iPrepareDoc, '/Payments/Payment', 2)
		WITH
		(	
			PaymentID varchar(50),
			Date Datetime,
			Amount money,
			Reference varchar(100),
			CurrencyRate money,
			PaymentType varchar(50),
			Status varchar(50),
			UpdatedDateUTC Datetime,
			Account xml,
			Invoice xml,
			ValidationErrors xml
		)
		
		UPDATE @XeroPayments
		SET 
			CustomerID = tdv_r.CustomerID,
			MatterID = tdv_m.DetailValue,
			LeadType = tdv_l.DetailValue
		FROM @XeroPayments xero
		INNER JOIN TableDetailValues tdv_r WITH (NOLOCK) 
			ON	tdv_r.ClientID = @ClientID 
			AND tdv_r.DetailFieldID = 134771 
			AND tdv_r.DetailValue = xero.InvoiceNumber
		INNER JOIN TableDetailValues tdv_m WITH (NOLOCK) 
			ON	tdv_m.TableRowID = tdv_r.TableRowID 
			AND tdv_m.DetailFieldID = 134770
		INNER JOIN TableDetailValues tdv_l WITH (NOLOCK) 
			ON	tdv_l.TableRowID = tdv_r.TableRowID 
			AND tdv_l.DetailFieldID = 134769

		UPDATE @XeroPayments
			SET TableRowID = tdv.TableRowID 
		FROM @XeroPayments xero
		INNER JOIN
		(
			SELECT
				CustomerID,
				TableRowID,
				MAX([134842]) LeadType,
				MAX([134843]) MatterID,
				MAX([134845]) InvoiceNumber,
				MAX([134846]) AccountCode,
				MAX([134847]) Date,
				MAX([134848]) Amount,
				MAX([134849]) PaymentID
			FROM
			(
				SELECT	DetailFieldID, RTRIM(ISNULL(DetailValue,'')) DetailValue, CustomerID, TableRowID
				FROM	TableDetailValues WITH (NOLOCK) 
				where	ClientID = @ClientID
				and		DetailFieldID in
				(
					134842,
					134843,
					134844,
					134845,
					134846,
					134847,
					134848,
					134849
				)
			) AS SourceTable
			PIVOT
			(
				MAX(DetailValue)
				FOR DetailFieldID IN 
				(
					[134842],
					[134843],
					[134844],
					[134845],
					[134846],
					[134847],
					[134848],
					[134849]
				)
			) AS PivotTable
			GROUP BY CustomerID, TableRowID
		) tdv 
		ON	
		tdv.AccountCode = CAST(xero.AccountCode AS varchar(250))
		AND tdv.Amount = CAST(xero.Amount AS varchar(250))
		AND CONVERT(VARCHAR(23), CAST(tdv.Date as Datetime), 120) = CONVERT(VARCHAR(23), CAST(xero.Date as Datetime), 120)
		AND tdv.InvoiceNumber = CAST(xero.InvoiceNumber AS varchar(250))
		
		UPDATE TableDetailValues
			SET DetailValue = unpvt.RealValue
		FROM TableDetailValues tdv WITH (NOLOCK) 
		INNER JOIN 
			(
				SELECT	
					tdv.TableRowID,
					COALESCE(CONVERT(varchar(255), xer.LeadType),'') as [134842],
					COALESCE(CONVERT(varchar(255), xer.MatterID),'') as [134843],
					COALESCE(CONVERT(varchar(255), xer.Reference),'') as [134844],
					COALESCE(CONVERT(varchar(255), xer.InvoiceNumber),'') as [134845],
					COALESCE(CONVERT(varchar(255), xer.AccountCode),'') as [134846],
					COALESCE(CONVERT(varchar(255), xer.Date, 120),'') as [134847],
					COALESCE(CONVERT(varchar(255), xer.Amount),'') as [134848],
					COALESCE(CONVERT(varchar(255), xer.PaymentID),'') as [134849],
					COALESCE(CONVERT(varchar(255), xer.XeroError),'') as [135887]
				FROM	TableDetailValues tdv WITH (NOLOCK)
				INNER JOIN @XeroPayments xer 
					ON	tdv.ClientID = @ClientID 
						AND tdv.DetailFieldID = 134845 
						AND tdv.DetailValue = xer.InvoiceNumber
			) p
			UNPIVOT
			(
				RealValue FOR ColumnName IN
				(
					[134842], [134843], [134844],[134845], [134846], [134847], [134848], [134849], [135887]
				)
			) as unpvt
		ON	tdv.TableRowID = unpvt.TableRowID 
			AND tdv.DetailFieldID = Cast(unpvt.ColumnName as int) 
			AND ISNULL(tdv.DetailValue, '') = ''
		
		SELECT @RowCountAffected = @@ROWCOUNT / 9
		
		INSERT INTO TableRows (ClientID, DetailFieldID, DetailFieldPageID, CustomerID, SourceID)
			OUTPUT	inserted.TableRowID, inserted.SourceID INTO @XeroPaymentTableRows
			SELECT	@ClientID, 134840, 15145, xer.CustomerID, xer.IdentityPaymentID 
			FROM	@XeroPayments xer 
			WHERE	xer.TableRowID IS NULL
			and xer.CustomerID is not null
		
		INSERT INTO TableDetailValues (TableRowID, DetailFieldID, DetailValue, ClientID, CustomerID)
			SELECT	TableRowID, ColumnName, RealValue, @ClientID, CustomerID
			FROM
			(
				SELECT	
					r.TableRowID,
					p.CustomerID,
					COALESCE(CONVERT(varchar(255), p.LeadType),'') as [134842],
					COALESCE(CONVERT(varchar(255), p.MatterID),'') as [134843],
					COALESCE(CONVERT(varchar(255), p.Reference),'') as  [134844],
					COALESCE(CONVERT(varchar(255), p.InvoiceNumber),'') as [134845],
					COALESCE(CONVERT(varchar(255), p.AccountCode),'') as [134846],
					COALESCE(CONVERT(varchar(255), p.Date, 120),'') as [134847],
					COALESCE(CONVERT(varchar(255), p.Amount),'') as [134848],
					COALESCE(CONVERT(varchar(255), p.PaymentID),'') as [134849],
					COALESCE(CONVERT(varchar(255), p.XeroError),'') as [135887]
				FROM	@XeroPaymentTableRows r
				INNER JOIN @XeroPayments p 
					ON p.IdentityPaymentID = r.IdentityPaymentID
					and p.CustomerID is not null
			) pvt
			UNPIVOT
			(
				RealValue FOR ColumnName IN
				(
					[134842], [134843], [134844], [134845], [134846], [134847], [134848], [134849], [135887]
				)
			) as unpvt
	
		SET @ResponseXML =
		(
			select * from @XeroPayments FOR XML AUTO
		)
		
		exec dbo.LogXML__AddEntry @ClientID, @QueryID, 'XeroHandledPaymentResponse', @ResponseXML
	
		SELECT @RowCountAffected += @@ROWCOUNT / 9
		
	END
	
	IF @XMLContainerName = 'CreditNotes'
	BEGIN

		DECLARE @XeroCreditNotes TABLE
		(
			CreditNoteID varchar(50),
			CreditNoteNumber varchar(100),
			ContactID varchar(50),
			ContactName varchar(100),
			Date Datetime,
			Status varchar(50),
			LineAmountTypes varchar(50),
			SubTotal Money,
			TotalTax Money,
			Total Money,
			UpdatedDateUTC Datetime,
			CurrencyCode varchar(50),
			Type varchar(50),
			SentToContact varchar(50),
			XeroError varchar(250)
		)

		INSERT INTO @XeroCreditNotes
		(
			CreditNoteID,
			CreditNoteNumber,
			ContactID,
			ContactName,
			Date,
			Status,
			LineAmountTypes,
			SubTotal,
			TotalTax,
			Total,
			UpdatedDateUTC,
			CurrencyCode,
			Type,
			SentToContact,
			XeroError
		)
		SELECT		
			CreditNoteID,
			REPLACE(CreditNoteNumber, '''', ''),
			Contact.value('/Contact[1]/ContactID[1]', 'varchar(50)') ContactID,
			Contact.value('/Contact[1]/Name[1]', 'varchar(100)') ContactName,
			Date,
			Status,
			LineAmountTypes,
			SubTotal,
			TotalTax,
			Total,
			CONVERT(varchar(23), CAST(UpdatedDateUTC as datetime), 120) UpdatedDateUTC,
			CurrencyCode,
			Type,
			SentToContact,
			ValidationErrors.value('/ValidationErrors[1]/ValidationError[1]/Message[1]', 'varchar(250)') XeroError
		FROM OPENXML (@iPrepareDoc, '/CreditNotes/CreditNote', 2)
		WITH
		(	
			CreditNoteID varchar(50),
			CreditNoteNumber varchar(100),
			Date Datetime,
			Status varchar(50),
			LineAmountTypes varchar(50),
			SubTotal Money,
			TotalTax Money,
			Total Money,
			UpdatedDateUTC Datetime,
			CurrencyCode varchar(50),
			Type varchar(50),
			SentToContact varchar(50),
			Contact xml,
			ValidationErrors xml
		)

		UPDATE TableDetailValues
			SET DetailValue = unpvt.RealValue
		FROM TableDetailValues tdv
		INNER JOIN 
			(
				SELECT	
						tdv.TableRowID,
						COALESCE(CONVERT(varchar(255), xer.Status),'') as [134858],
						COALESCE(CONVERT(varchar(255), xer.UpdatedDateUTC),'') as [134859],
						COALESCE(CONVERT(varchar(255), xer.CreditNoteID),'') as [134860],
						COALESCE(CONVERT(varchar(255), xer.XeroError),'') as [135888]
				FROM	TableDetailValues tdv WITH (NOLOCK)
				INNER JOIN @XeroCreditNotes xer 
					ON tdv.ClientID = @ClientID AND tdv.DetailFieldID = 134852 AND tdv.DetailValue = xer.CreditNoteNumber
			) p
			UNPIVOT
			(
				RealValue FOR ColumnName IN
				(
					[134858], [134859], [134860], [135888]
				)
			) as unpvt
		ON tdv.TableRowID = unpvt.TableRowID
		AND	tdv.DetailFieldID = Cast(unpvt.ColumnName as int)
		
		
		SET @ResponseXML =
		(
			select * from @XeroCreditNotes FOR XML AUTO
		)
		
		exec dbo.LogXML__AddEntry @ClientID, @QueryID, 'XeroHandledCreditNotesResponse', @ResponseXML
		
		SELECT @RowCountAffected = @@ROWCOUNT / 4
		
	END

	EXEC sp_xml_removedocument @iPrepareDoc
	
	SELECT @RowCountAffected AS 'RowCount'

END




GO
GRANT VIEW DEFINITION ON  [dbo].[XeroHandleResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[XeroHandleResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[XeroHandleResponse] TO [sp_executeall]
GO
