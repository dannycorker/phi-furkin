SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PaymentCardType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentCardType_Get_List]

AS


				
				SELECT
					[PaymentCardTypeID],
					[PaymentCardType],
					[DisplayOrder]
				FROM
					[dbo].[PaymentCardType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentCardType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Get_List] TO [sp_executeall]
GO
