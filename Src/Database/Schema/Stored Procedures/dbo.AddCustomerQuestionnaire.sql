SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddCustomerQuestionnaire    Script Date: 08/09/2006 12:22:49 ******/

CREATE PROCEDURE [dbo].[AddCustomerQuestionnaire]
@CustomerID int,
@ClientQuestionnaireID int,
@TrackingID int,
@Referrer nvarchar(512), 
@SearchTerms nvarchar(512),
@ClientID int

AS

insert into CustomerQuestionnaires (CustomerID, ClientQuestionnaireID, TrackingID, Referrer, SearchTerms, SubmissionDate, ClientID)
values (@CustomerID, @ClientQuestionnaireID,@TrackingID, @Referrer, @SearchTerms, dbo.fn_GetDate_Local(), @ClientID )


declare @CustomerQuestionnaireID int

set @CustomerQuestionnaireID = SCOPE_IDENTITY()

select CustomerQuestionnaireID
from CustomerQuestionnaires WITH (NOLOCK) 
where CustomerQuestionnaireID = @CustomerQuestionnaireID
GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddCustomerQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerQuestionnaire] TO [sp_executeall]
GO
