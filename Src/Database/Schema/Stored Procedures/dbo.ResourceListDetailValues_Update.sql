SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ResourceListDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_Update]
(

	@ResourceListDetailValueID int   ,

	@ResourceListID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@LeadOrMatter int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ResourceListDetailValues]
				SET
					[ResourceListID] = @ResourceListID
					,[ClientID] = @ClientID
					,[DetailFieldID] = @DetailFieldID
					,[LeadOrMatter] = @LeadOrMatter
					,[DetailValue] = @DetailValue
					,[ErrorMsg] = @ErrorMsg
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
				WHERE
[ResourceListDetailValueID] = @ResourceListDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Update] TO [sp_executeall]
GO
