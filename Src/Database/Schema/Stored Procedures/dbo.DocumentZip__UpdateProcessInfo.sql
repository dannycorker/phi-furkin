SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 12-Feb-2009
-- Description:	Proc to Update Doc Zip Status
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZip__UpdateProcessInfo]
(
	@DocumentZipID int,
	@StatusID int,
	@DocumentFileName varchar(2000) = NULL,
	@ZipFileName varchar(2000) = NULL, 
	@DocumentZipFileID int = NULL
)


AS
BEGIN
	SET NOCOUNT ON;

	Update dbo.DocumentZip 
	Set StatusID = @StatusID, 
	DocumentFileName = @DocumentFileName, 
	ZipFileName = @ZipFileName, 
	DocumentZipFileID = @DocumentZipFileID 
	Where DocumentZipID = @DocumentZipID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__UpdateProcessInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__UpdateProcessInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__UpdateProcessInfo] TO [sp_executeall]
GO
