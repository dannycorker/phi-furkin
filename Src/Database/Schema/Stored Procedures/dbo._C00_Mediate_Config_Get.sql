SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date:	2020-01-27
-- Description:	returns the app json config
-- 2020-03-02 CPS for JIRA LAGCLAIM-414	| Updated to take an email address
-- 2020-03-11 CPS for JIRA LAGCLAIM		| Hard-coded expiry to 2 hours at request of Conor
-- 2020-03-30 CPS for JIRA AAG-557		| Copied from Aquarius603Dev and Updated to AAG fields
-- 2020-05-18 CPS for JIRA AAG-490		| Expanded to include V2 values
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Mediate_Config_Get]
(
	 @AppContext	VARCHAR(500) = '' /*localhost or [client]-[env].aquarium-software.com*/
)
AS
BEGIN
 
	SET NOCOUNT ON

	SELECT	 cp.ClientPersonnelID
			,cp.EmailAddress [Username]
			,cp.[Password]		-- also [Client_Secret]
			,cp.Salt
			,luliInOut.ItemValue [Queue]
			,cpdvBsURL.DetailValue [BaseUrl]
			,cpdvApKey.DetailValue [AppKey]
			,120 [TokenExpiryMinutes] -- 2020-03-11 CPS for JIRA LAGCLAIM | Hard-coded to 2 hours at request of Conor
			,'AquariumV2' [Destination]
			,'client_credentials' [Grant_Type]
			,'api_client' [Client_ID]
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnelDetailValues cpdvInOut WITH (NOLOCK) on cpdvInOut.ClientPersonnelID = cp.ClientPersonnelID AND cpdvInOut.DetailFieldID = 313892 /*In or Out*/
	INNER JOIN dbo.LookupListItems luliInOut WITH (NOLOCK) on luliInOut.LookupListItemID = cpdvInOut.ValueInt
	INNER JOIN dbo.ClientPersonnelDetailValues cpdvBsURL WITH (NOLOCK) on cpdvBsURL.ClientPersonnelID = cp.ClientPersonnelID AND cpdvBsURL.DetailFieldID = 313893 /*Base URL (for outbound calls)*/
	INNER JOIN dbo.ClientPersonnelDetailValues cpdvApKey WITH (NOLOCK) on cpdvApKey.ClientPersonnelID = cp.ClientPersonnelID AND cpdvApKey.DetailFieldID = 313894 /*AppKey*/
	WHERE cp.AccountDisabled = 0

	DECLARE @AppUrls TABLE ( [AppKey] VARCHAR(2000) NOT NULL
							,[RequestType] VARCHAR(2000) NOT NULL
							,[Url] VARCHAR(2000) NOT NULL
							,[TokenName] VARCHAR(200) NULL
						)
	INSERT @AppUrls ( AppKey, RequestType, [Url], TokenName )
	VALUES	 ( 'app.key.vision.607.out', 'Authorization'		, 'https://trupanion-uat.aqvision.pet/connect/token'			, 'access_token' )
			,( 'app.key.vision.607.out', 'VisionCustomerRequest', 'https://trupanion-uat.aqvision.pet/api/claims/Customer'		, NULL )
			,( 'app.key.vision.607.out', 'VisionPetRequest'		, 'https://trupanion-uat.aqvision.pet/api/claims/Pets'			, NULL )
			,( 'app.key.vision.607.out', 'VisionPolicyRequest'	, 'https://trupanion-uat.aqvision.pet/api/claims/Policy'		, NULL )
			,( 'app.key.vision.607.out', 'VisionClaimRequest'	, 'https://trupanion-uat.aqvision.pet/api/claims/Claim'			, NULL )

	SELECT  [AppKey]
           ,[RequestType]
           ,[Url]
           ,[TokenName]
	FROM @AppUrls

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Config_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Mediate_Config_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Config_Get] TO [sp_executeall]
GO
