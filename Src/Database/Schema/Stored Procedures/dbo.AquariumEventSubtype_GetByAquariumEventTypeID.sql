SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumEventSubtype table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_GetByAquariumEventTypeID]
(

	@AquariumEventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AquariumEventSubtypeID],
					[AquariumEventTypeID],
					[AquariumEventSubtypeName],
					[AquariumEventSubtypeDescription],
					[WhenCreated],
					[WhenModified],
					[ClientID],
					[StoredProcedure]
				FROM
					[dbo].[AquariumEventSubtype] WITH (NOLOCK) 
				WHERE
					[AquariumEventTypeID] = @AquariumEventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_GetByAquariumEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_GetByAquariumEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_GetByAquariumEventTypeID] TO [sp_executeall]
GO
