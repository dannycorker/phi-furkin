SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientOfficeIPAddresses table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_Insert]
(

	@ClientOfficeIpAddressID int    OUTPUT,

	@ClientOfficeID int   ,

	@ClientOfficeIPAddress char (15)  ,

	@ClientID int   ,

	@Notes varchar (200)  
)
AS


				
				INSERT INTO [dbo].[ClientOfficeIPAddresses]
					(
					[ClientOfficeID]
					,[ClientOfficeIPAddress]
					,[ClientID]
					,[Notes]
					)
				VALUES
					(
					@ClientOfficeID
					,@ClientOfficeIPAddress
					,@ClientID
					,@Notes
					)
				-- Get the identity value
				SET @ClientOfficeIpAddressID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_Insert] TO [sp_executeall]
GO
