SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetAnswersForCustomersClientQuestionnaire    Script Date: 08/09/2006 12:22:50 ******/

CREATE PROCEDURE [dbo].[GetAnswersForCustomersClientQuestionnaire] @CustomerID int, @ClientQuestionnaireID int

as
/*2012-11-14 Nolocked By CS*/

SELECT     dbo.CustomerQuestionnaires.CustomerQuestionnaireID, dbo.CustomerQuestionnaires.ClientQuestionnaireID, 
                      dbo.CustomerQuestionnaires.CustomerID, dbo.CustomerQuestionnaires.SubmissionDate, dbo.CustomerAnswers.MasterQuestionID, 
                      dbo.CustomerAnswers.Answer, dbo.CustomerAnswers.QuestionPossibleAnswerID, dbo.CustomerAnswers.CustomerAnswerID, dbo.CustomerQuestionnaires.ClientID 
FROM         dbo.CustomerQuestionnaires WITH (NOLOCK)  INNER JOIN
                      dbo.CustomerAnswers WITH (NOLOCK)  ON dbo.CustomerQuestionnaires.CustomerQuestionnaireID = dbo.CustomerAnswers.CustomerQuestionnaireID
WHERE     (dbo.CustomerQuestionnaires.CustomerID = @CustomerID) AND (dbo.CustomerQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID)





GO
GRANT VIEW DEFINITION ON  [dbo].[GetAnswersForCustomersClientQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAnswersForCustomersClientQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAnswersForCustomersClientQuestionnaire] TO [sp_executeall]
GO
