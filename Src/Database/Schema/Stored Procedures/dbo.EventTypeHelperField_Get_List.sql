SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeHelperField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeHelperField_Get_List]

AS


				
				SELECT
					[EventTypeHelperFieldID],
					[EventTypeID],
					[DetailFieldID],
					[LeadTypeID],
					[ClientID],
					[ForceEditableOverride]
				FROM
					[dbo].[EventTypeHelperField] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Get_List] TO [sp_executeall]
GO
