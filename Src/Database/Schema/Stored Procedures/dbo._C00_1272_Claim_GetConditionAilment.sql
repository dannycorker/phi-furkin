SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-09
-- Description:	Gets all the ailment from the linked matters
-- Used by:		DF 145673
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetConditionAilment] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT mdv.ValueInt AS ConditionID, rdvAilment1.DetailValue AS Ailment1, rdvAilment2.DetailValue AS Ailment2
	FROM Matter m
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144504
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment1 WITH (NOLOCK) ON rdvAilment1.ResourceListID = mdv.ValueInt AND rdvAilment1.DetailFieldID = 144340
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment2 WITH (NOLOCK) ON rdvAilment2.ResourceListID = mdv.ValueInt AND rdvAilment2.DetailFieldID = 144341
	WHERE m.MatterID = @MatterID
	AND mdv.ValueInt IS NOT NULL

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetConditionAilment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetConditionAilment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetConditionAilment] TO [sp_executeall]
GO
