SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChartSeriesSetting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting_Delete]
(

	@ChartSeriesSettingID int   
)
AS


				DELETE FROM [dbo].[ChartSeriesSetting] WITH (ROWLOCK) 
				WHERE
					[ChartSeriesSettingID] = @ChartSeriesSettingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Delete] TO [sp_executeall]
GO
