SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuillCustomerMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCustomerMapping_Insert]
(

	@QuillCustomerMappingID int    OUTPUT,

	@ClientID int   ,

	@CustomerID int   ,

	@QuillClientCode varchar (50)  
)
AS


				
				INSERT INTO [dbo].[QuillCustomerMapping]
					(
					[ClientID]
					,[CustomerID]
					,[QuillClientCode]
					)
				VALUES
					(
					@ClientID
					,@CustomerID
					,@QuillClientCode
					)
				-- Get the identity value
				SET @QuillCustomerMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCustomerMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Insert] TO [sp_executeall]
GO
