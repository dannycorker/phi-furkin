SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDroppedOutCustomer] @CustomerID int 
AS
BEGIN
Delete From DroppedOutCustomers where CustomerID = @CustomerID;
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDroppedOutCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteDroppedOutCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDroppedOutCustomer] TO [sp_executeall]
GO
