SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-30
-- Description:	Quick check of the last [n] log entries in reverse order
-- =============================================
CREATE PROCEDURE [dbo].[timeout]
	@rows int = 50
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP (@rows) l.LogDateTime, 
	cp.ClientID, 
	l.ClientPersonnelID, 
	cp.UserName, 
	l.ClassName, 
	l.MethodName, 
	CASE WHEN l.LogEntry LIKE '%attempt % of%' THEN LEFT(RIGHT(l.LogEntry, 7), 6) ELSE '' END as [Attempt], /* 'This was attempt 2 of 3.' */
	l.LogEntry,
	l.LogID
	FROM dbo.Logs l (nolock) 
	LEFT JOIN dbo.ClientPersonnel cp (nolock) ON cp.ClientPersonnelID = l.ClientPersonnelID 
	WHERE l.LogEntry LIKE '%timeout%' 
	AND l.LogEntry NOT LIKE '%Called RPI Update Claim status%'
	ORDER BY 1 DESC 

END




GO
GRANT VIEW DEFINITION ON  [dbo].[timeout] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[timeout] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[timeout] TO [sp_executeall]
GO
