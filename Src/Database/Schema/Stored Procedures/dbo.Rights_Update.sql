SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Rights table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Rights_Update]
(

	@RightID int   ,

	@OriginalRightID int   ,

	@RightName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Rights]
				SET
					[RightID] = @RightID
					,[RightName] = @RightName
				WHERE
[RightID] = @OriginalRightID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Rights_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Update] TO [sp_executeall]
GO
