SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOfficeIPAddresses table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientOfficeIpAddressID],
					[ClientOfficeID],
					[ClientOfficeIPAddress],
					[ClientID],
					[Notes]
				FROM
					[dbo].[ClientOfficeIPAddresses] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_GetByClientID] TO [sp_executeall]
GO
