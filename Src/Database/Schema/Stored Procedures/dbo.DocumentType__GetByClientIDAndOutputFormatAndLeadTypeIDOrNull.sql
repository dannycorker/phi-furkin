SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

----------------------------------------------------------------------------------------------------
-- Date Created: 30 August 2007

-- Created By:  Chris Townsend
-- Purpose: Select records from the DocumentType table with a given lead type id or where the LeadTypeId is null
--			and for a specific OutputFormat list (eg: 'SMS' or 'MS_WORD_2003','PDF')
-- JWG 2013-10-10 #23429 Don't send the BLOBs back when this is just used to populate a DDL on CustomersDocumentOut.aspx
----------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[DocumentType__GetByClientIDAndOutputFormatAndLeadTypeIDOrNull]
(
	@ClientID INT ,
	@LeadTypeID INT,
	@OutputFormat VARCHAR(200)
)
AS
BEGIN

	SET ANSI_NULLS OFF

	DECLARE @SQLStatement VARCHAR(5000)

	SELECT @SQLStatement = 				
					/*'SELECT **/
					'SELECT [DocumentTypeID], [ClientID], [LeadTypeID], [DocumentTypeName], [DocumentTypeDescription], [Header], NULL AS [Template], [Footer], [CanBeAutoSent], [EmailSubject], [EmailBodyText], [InputFormat], [OutputFormat], [Enabled], [RecipientsTo], [RecipientsCC], [RecipientsBCC], [ReadOnlyTo], [ReadOnlyCC], [ReadOnlyBCC], [SendToMultipleRecipients], [MultipleRecipientDataSourceType], [MultipleRecipientDataSourceID], [SendToAllByDefault], [ExcelTemplatePath], [FromDetails], [ReadOnlyFrom], [SourceID], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified], [FolderID],IsThunderheadTemplate,ThunderheadUniqueTemplateID, ThunderheadDocumentFormat, [DocumentTitleTemplate], [CreateNewVersionWhenSaved]
					FROM
						dbo.[DocumentType]
					WHERE
						[ClientID] =' + CAST(@ClientID AS VARCHAR(10)) + '
						AND ([LeadTypeID] = ' + CAST(@LeadTypeID AS VARCHAR(10)) + ' OR ' + '[LeadTypeID] is NULL)'
						+ 'AND Enabled = 1'
						+ 'AND OutputFormat IN (' + @OutputFormat + ')'
						+ 'AND SendToMultipleRecipients = 0' -- Don't allow MR docs in the template list
						+ ' ORDER BY DocumentTypeName'
						+ ' SELECT @@ROWCOUNT'
						

	EXEC(@SQLStatement)				

	SELECT @@ROWCOUNT

END












GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByClientIDAndOutputFormatAndLeadTypeIDOrNull] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByClientIDAndOutputFormatAndLeadTypeIDOrNull] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByClientIDAndOutputFormatAndLeadTypeIDOrNull] TO [sp_executeall]
GO
