SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-10-2014
-- Description:	Deletes all the standard targets with the given script id
-- =============================================
CREATE PROCEDURE [dbo].[ScriptStandardTarget__DeleteByScriptID]
	
	@ClientID INT,
	@ScriptID INT	
	
AS
BEGIN
	SET NOCOUNT ON;

    DELETE FROM ScriptStandardTarget
	WHERE ScriptID=@ScriptID AND ClientID=@ClientID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget__DeleteByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget__DeleteByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget__DeleteByScriptID] TO [sp_executeall]
GO
