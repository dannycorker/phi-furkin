SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeSql table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_Insert]
(

	@EventTypeSqlID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@PostUpdateSql varchar (MAX)  ,

	@IsNativeSql bit   
)
AS


				
				INSERT INTO [dbo].[EventTypeSql]
					(
					[ClientID]
					,[EventTypeID]
					,[PostUpdateSql]
					,[IsNativeSql]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@PostUpdateSql
					,@IsNativeSql
					)
				-- Get the identity value
				SET @EventTypeSqlID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_Insert] TO [sp_executeall]
GO
