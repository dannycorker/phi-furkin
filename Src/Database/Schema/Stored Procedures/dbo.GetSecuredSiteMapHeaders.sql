SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetSecuredSiteMapHeaders    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetSecuredSiteMapHeaders] AS


SELECT     SiteMapID, Url, Title, Description, ParentID, Secured
FROM         dbo.SiteMap
WHERE     (Url IS NULL) AND (Secured = 1) OR
                      (Url = N'""')




GO
GRANT VIEW DEFINITION ON  [dbo].[GetSecuredSiteMapHeaders] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSecuredSiteMapHeaders] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSecuredSiteMapHeaders] TO [sp_executeall]
GO
