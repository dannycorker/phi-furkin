SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PortalUserGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroup_Get_List]

AS


				
				SELECT
					[PortalUserGroupID],
					[ClientID],
					[GroupName],
					[GroupDescription],
					[ShowGroupMessage],
					[MessageTitle],
					[MessageBody],
					[HideOwnCustomer]
				FROM
					[dbo].[PortalUserGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Get_List] TO [sp_executeall]
GO
