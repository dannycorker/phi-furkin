SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PageGroupDefaultPageLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroupDefaultPageLink_Delete]
(

	@PageGroupDefaultPageLinkID int   
)
AS


				DELETE FROM [dbo].[PageGroupDefaultPageLink] WITH (ROWLOCK) 
				WHERE
					[PageGroupDefaultPageLinkID] = @PageGroupDefaultPageLinkID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroupDefaultPageLink_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroupDefaultPageLink_Delete] TO [sp_executeall]
GO
