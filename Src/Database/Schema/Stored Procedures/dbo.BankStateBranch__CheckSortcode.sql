SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17/02/2020
-- Description:	Checks the given Australian sort code exists within the BankStateBranch table.
-- If it does exist, it returns the matched BankStateBranch record.
-- Please note the given sortcode does not contain hyphens and must be six characters long.
-- The Bank State Branch table stores the sortcode with a hyphen eg.,  123-456
-- =============================================
CREATE PROCEDURE [dbo].[BankStateBranch__CheckSortcode]
	@SortCode VARCHAR(6)
AS
BEGIN

	SET NOCOUNT ON;

	IF (LEN(@SortCode)=6)
	BEGIN

		--Add hyphen to the middle of the sortcode
		DECLARE @Col1 VARCHAR(3), 
				@Col2 VARCHAR(3),
				@BsbCode VARCHAR(7)

		SET @Col1 = LEFT(@SortCode, 3)
		SET @Col2 = RIGHT(@SortCode,3)
		SET @BsbCode = @Col1 + '-' + @Col2

		--Lookup Bank State Branch
		SELECT * FROM AquariusMaster.dbo.BankStateBranch WITH (NOLOCK)
		WHERE BsbCode = @BsbCode

	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStateBranch__CheckSortcode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStateBranch__CheckSortcode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStateBranch__CheckSortcode] TO [sp_executeall]
GO
