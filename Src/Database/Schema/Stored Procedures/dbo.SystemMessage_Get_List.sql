SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SystemMessage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessage_Get_List]

AS


				
				SELECT
					[SystemMessageID],
					[ClientID],
					[SystemMessageName],
					[SystemMessageText],
					[Enabled]
				FROM
					[dbo].[SystemMessage] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessage_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Get_List] TO [sp_executeall]
GO
