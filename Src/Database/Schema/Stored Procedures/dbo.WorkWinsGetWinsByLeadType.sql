SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-08
-- Description:	Used by fnGetWinsByLeadType to do the hard work, because
--              functions cannot use commands like INSERT, DELETE on real tables
-- =============================================
CREATE PROCEDURE [dbo].[WorkWinsGetWinsByLeadType] 
	@LeadTypeID int, 
	@DateFrom datetime,
	@DateTo datetime,
	@Flags varchar(2000) 
AS
BEGIN
	-- Make sure we are selecting all possible times within the date range specified
	SELECT  @DateFrom = convert(datetime, convert(char(10), @DateFrom, 126) + ' 00:00:00'),
			@DateTo = convert(datetime, convert(char(10), @DateTo, 126) + ' 23:59:59')

	-- Set up various fields of interest, depending on the lead type and flags requested
	DECLARE @WinAmountFieldID1 int, @WinAmountFieldID2 int, @InitialOfferFieldID int, @ClientID int

	SELECT @WinAmountFieldID1 = waf.WinAmountField1, 
	@WinAmountFieldID2 = waf.WinAmountField2, 
	@InitialOfferFieldID = waf.OfferAmountField1, 
	@ClientID = waf.ClientID 
	FROM dbo.WinAmountField waf (nolock) 
	WHERE waf.LeadTypeID = @LeadTypeID 
	
	-- Get a unique ID to link all the following selects together,
	-- and keep them apart from other queries.
	DECLARE @WorkWinSeedID int
	
	EXEC @WorkWinSeedID = [dbo].[GetWorkWinSeedID] 
	
	-- Select all Matters with a win recorded in that range (some will be filtered out below)	
	INSERT INTO dbo.WorkWinsPossibleValidMatters (WorkWinSeedID, MatterID, DetailFieldID) 
	SELECT @WorkWinSeedID, dvh.MatterID, @WinAmountFieldID1
	FROM dbo.DetailValueHistory dvh 
	WHERE dvh.ClientID = @ClientID
	AND dvh.DetailFieldID = @WinAmountFieldID1
	AND dvh.WhenSaved BETWEEN @DateFrom AND @DateTo

	UNION
	
	SELECT @WorkWinSeedID, dvh2.MatterID, @WinAmountFieldID1
	FROM DetailValueHistory dvh2 
	WHERE dvh2.ClientID = @ClientID
	AND dvh2.DetailFieldID = @WinAmountFieldID2
	AND dvh2.WhenSaved BETWEEN @DateFrom AND @DateTo
	AND @WinAmountFieldID2 IS NOT NULL
	
	
	-- Note the date of the first NON-BLANK entry (ie no time-wasters!)
	INSERT dbo.WorkWinsFirstValidSave (WorkWinSeedID, MatterID, DetailFieldID, WhenSaved)
	SELECT @WorkWinSeedID, dvh.MatterID, dvh.DetailFieldID, MIN(dvh.WhenSaved)
	FROM dbo.WorkWinsPossibleValidMatters pvm (nolock)
	INNER JOIN DetailValueHistory dvh (nolock) ON dvh.MatterID = pvm.MatterID AND dvh.DetailFieldID = pvm.DetailFieldID AND replace(dvh.FieldValue, '£', '') <> ''
	WHERE pvm.WorkWinSeedID = @WorkWinSeedID 
	GROUP BY dvh.MatterID, dvh.DetailFieldID 


	-- For Matters that do have a NON-BLANK entry somewhere in their history, note down the
	-- latest date that a value was saved so that we can count the valid entries.
	INSERT dbo.WorkWinsLatestValidSave (WorkWinSeedID, MatterID, DetailFieldID, WhenSaved)
	SELECT @WorkWinSeedID, dvh.MatterID, dvh.DetailFieldID, MAX(dvh.WhenSaved)
	FROM dbo.WorkWinsFirstValidSave fvs (nolock)
	INNER JOIN DetailValueHistory dvh (nolock) ON fvs.MatterID = dvh.MatterID AND fvs.DetailFieldID = dvh.DetailFieldID AND dvh.WhenSaved >= fvs.WhenSaved 
	WHERE fvs.WorkWinSeedID = @WorkWinSeedID 
	GROUP BY dvh.MatterID, dvh.DetailFieldID 


	-- Now pick out the results to return
	-- This selects Matters with a valid win between the date range 
	-- requested (from @FirstValidSave and @LastValidSave),
	-- the current Win Amount figure (from MatterDetailValues),
	-- and the total count of valid saves for all time (DetailValueHistory, @FirstValidSave and @LastValidSave)
	-- REPLACE currency symbol and commas with blanks so they can be converted to numbers.
	INSERT dbo.WorkWinsCurrent (WorkWinSeedID, CustomerID, LeadID, CaseID, MatterID, WinAmount, OfferAmount, FirstValidSave, LastValidSave, WinCount) 
	SELECT @WorkWinSeedID, m.CustomerID, m.LeadID, m.CaseID, m.MatterID, replace(replace(mdv_win.DetailValue, '£', ''), ',', ''), replace(replace(isnull(mdv_offer.DetailValue, ''), '£', ''), ',', ''), fvs.WhenSaved, lvs.WhenSaved, count(*)
	FROM dbo.WorkWinsLatestValidSave lvs (nolock) 
	INNER JOIN dbo.WorkWinsFirstValidSave (nolock) fvs ON fvs.WorkWinSeedID = lvs.WorkWinSeedID AND fvs.MatterID = lvs.MatterID
	INNER JOIN Matter m (nolock) ON m.MatterID = lvs.MatterID
	INNER JOIN Customers c (nolock) ON c.CustomerID = m.CustomerID
	INNER JOIN MatterDetailValues mdv_win (nolock) ON mdv_win.MatterID = fvs.MatterID AND mdv_win.DetailFieldID = fvs.DetailFieldID
	LEFT JOIN MatterDetailValues mdv_offer (nolock) ON mdv_offer.MatterID = fvs.MatterID AND mdv_offer.DetailFieldID = @InitialOfferFieldID 
	INNER JOIN DetailValueHistory dvh (nolock) ON dvh.MatterID = lvs.MatterID AND dvh.DetailFieldID = mdv_win.DetailFieldID AND dvh.WhenSaved BETWEEN fvs.WhenSaved AND lvs.WhenSaved
	WHERE lvs.WorkWinSeedID = @WorkWinSeedID 
	AND (c.Test = 0 OR c.Test IS NULL) 
	AND c.LastName NOT LIKE 'Test%' 
	GROUP BY m.CustomerID, m.LeadID, m.CaseID, m.MatterID, mdv_win.DetailValue, mdv_offer.DetailValue, fvs.WhenSaved, lvs.WhenSaved


	-- Now apply any Event-based rules specified in the Flags etc.
	-- For simple lead types (ie most of them) this is now enough to return the results.
	-- For early Bank Charges and Credit Card Charge lead types, though, there are more
	-- checks to do first.
	DECLARE @WinBlockingCount int, @WinRequiredEventCount int
	
	SELECT @WinBlockingCount = count(*)
	FROM dbo.WinEventType wet (nolock) 
	WHERE wet.LeadTypeID = @LeadTypeID 
	AND wet.Flags = @Flags 
	AND wet.MakesAWin = 0
	
	-- If it is possible for any events to "deny" a win, check for them now.
	IF @WinBlockingCount > 0 
	BEGIN
	
		-- We need to remove records from the win list if a "blocking event" is in place
		DELETE dbo.WorkWinsCurrent 
		FROM dbo.WorkWinsCurrent cw 
		INNER JOIN dbo.LeadEvent le (nolock) ON le.CaseID = cw.CaseID AND le.EventDeleted = 0
		INNER JOIN dbo.WinEventType wet (nolock) ON wet.EventTypeID = le.EventTypeID AND wet.LeadTypeID = @LeadTypeID AND wet.Flags = @Flags AND wet.MakesAWin = 0
		WHERE cw.WorkWinSeedID = @WorkWinSeedID 

	END
	
	SELECT @WinRequiredEventCount = count(*)
	FROM dbo.WinEventType wet (nolock) 
	WHERE wet.LeadTypeID = @LeadTypeID 
	AND wet.Flags = @Flags 
	AND wet.MakesAWin = 1
	
	IF @WinRequiredEventCount > 0
	BEGIN
	
		-- We also need to ignore records if a "winning event" is not in place,
		-- so create a list of cases-with-win-events if required...
		INSERT dbo.WorkWinsCasesWithWins (WorkWinSeedID, CaseID)
		SELECT @WorkWinSeedID, cw.CaseID 
		FROM dbo.WorkWinsCurrent cw (nolock) 
		INNER JOIN dbo.LeadEvent le (nolock) ON le.CaseID = cw.CaseID AND le.EventDeleted = 0
		INNER JOIN dbo.WinEventType wet (nolock) ON wet.EventTypeID = le.EventTypeID AND wet.LeadTypeID = @LeadTypeID AND wet.Flags = @Flags AND wet.MakesAWin = 1
		WHERE cw.WorkWinSeedID = @WorkWinSeedID 
		
		-- ...and then delete everything from the wins list that 
		-- doesn't have a match in the new list.
		DELETE dbo.WorkWinsCurrent 
		FROM dbo.WorkWinsCurrent cw 
		LEFT JOIN dbo.WorkWinsCasesWithWins www ON www.WorkWinSeedID = cw.WorkWinSeedID AND www.CaseID = cw.CaseID
		WHERE cw.WorkWinSeedID = @WorkWinSeedID 
		AND www.CaseID IS NULL
		
	END
	
	SELECT * FROM dbo.WorkWinsCurrent cw (nolock)
	WHERE cw.WorkWinSeedID = @WorkWinSeedID 
	
	RETURN @WorkWinSeedID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkWinsGetWinsByLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkWinsGetWinsByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkWinsGetWinsByLeadType] TO [sp_executeall]
GO
