SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptEventTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_Delete]
(

	@ScriptEventTargetID int   
)
AS


				DELETE FROM [dbo].[ScriptEventTarget] WITH (ROWLOCK) 
				WHERE
					[ScriptEventTargetID] = @ScriptEventTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Delete] TO [sp_executeall]
GO
