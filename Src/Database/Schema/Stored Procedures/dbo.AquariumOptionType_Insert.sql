SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AquariumOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOptionType_Insert]
(

	@AquariumOptionTypeID int   ,

	@AquariumOptionTypeName varchar (50)  ,

	@AquariumOptionTypeDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[AquariumOptionType]
					(
					[AquariumOptionTypeID]
					,[AquariumOptionTypeName]
					,[AquariumOptionTypeDescription]
					)
				VALUES
					(
					@AquariumOptionTypeID
					,@AquariumOptionTypeName
					,@AquariumOptionTypeDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOptionType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Insert] TO [sp_executeall]
GO
