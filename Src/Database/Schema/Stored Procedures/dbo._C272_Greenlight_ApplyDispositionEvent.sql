SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-01-07
-- Description:	Take a customer/case along with a greenlight disposition, and apply the mapped events
-- =============================================
CREATE PROCEDURE [dbo].[_C272_Greenlight_ApplyDispositionEvent]
	 @CustomerID		INT
	,@CaseID			INT = NULL
	,@DispositionName	VARCHAR(2000)
	,@ClientPersonnelID	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ToAction TABLE ( CaseID INT, LeadTypeID INT, EventToApply INT )

	/*Find a list of the cases to action, according to the Customer/CaseID supplied*/
	INSERT @ToAction ( CaseID, LeadTypeID, EventToApply )
	SELECT c.CaseID, l.LeadTypeID, 0
	FROM Customers cu WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) on l.CustomerID = cu.CustomerID 
	INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID 
	WHERE cu.CustomerID = @CustomerID
	AND ( c.CaseID = @CaseID or @CaseID IS NULL )

	/*Find the events to apply for each lead type*/
	;WITH DispositionMapping as
	(
	SELECT tr.TableRowID, tdv_lt.ValueInt [LeadTypeID], tdv_dn.DetailValue [DispositionName], tdv_et.ValueInt [EventTypeID]
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_lt WITH (NOLOCK) on tdv_lt.TableRowID = tr.TableRowID and tdv_lt.DetailFieldID = 216284 /*LeadTypeID*/
	LEFT JOIN TableDetailValues tdv_dn WITH (NOLOCK) on tdv_dn.TableRowID = tr.TableRowID and tdv_dn.DetailFieldID = 216285 /*Disposition Name*/
	INNER JOIN TableDetailValues tdv_et WITH (NOLOCK) on tdv_et.TableRowID = tr.TableRowID and tdv_et.DetailFieldID = 216286 /*Disposition EventTypeID*/
	WHERE tr.DetailFieldID = 216287 /*Greenlight Disposition Mapping*/
	)
	UPDATE ta
	SET	EventToApply = dm.EventTypeID
	FROM DispositionMapping dm 
	INNER JOIN @ToAction ta on ta.LeadTypeID = dm.LeadTypeID
	WHERE dm.DispositionName = @DispositionName
		OR	(
			dm.DispositionName IS NULL
			AND NOT EXISTS ( SELECT * 
							 FROM DispositionMapping sdm 
							 INNER JOIN @ToAction sta on sta.LeadTypeID = sdm.LeadTypeID
							 WHERE sdm.DispositionName = @DispositionName )
			)

	/*Having picked up the event type, insert into the AutomatedEventQueue to queue the event*/
	INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
	SELECT m.ClientID, m.CustomerID, m.LeadID, m.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @ClientPersonnelID, ta.EventToApply, @ClientPersonnelID, CASE WHEN et.InProcess = 0 THEN -1 ELSE 0 END, 5, 0, 0 /*Greenlight System*/
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Cases ca WITH (NOLOCK) on ca.CaseID = m.CaseID
	INNER JOIN @ToAction ta on ta.CaseID = ca.CaseID
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadEventID = ca.ProcessStartLeadEventID
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ta.EventToApply
	WHERE ta.EventToApply > 0

	SELECT @@ROWCOUNT [EventsQueued]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_ApplyDispositionEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C272_Greenlight_ApplyDispositionEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_ApplyDispositionEvent] TO [sp_executeall]
GO
