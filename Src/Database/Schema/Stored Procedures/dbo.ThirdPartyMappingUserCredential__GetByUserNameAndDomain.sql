SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-10-2012
-- Description:	Gets The ThirdPartyMappingUserCredential by username, domain and ThirdPartySystemID
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential__GetByUserNameAndDomain]

	@Username VARCHAR(2000),
	@Domain VARCHAR(2000),
	@ThirdPartySystemID INT

AS
BEGIN
	
	SELECT * FROM ThirdPartyMappingUserCredential uc WITH (NOLOCK) 
	WHERE uc.UserName = @Username AND 
		  uc.Domain = @Domain AND 
		  uc.ThirdPartySystemID = @ThirdPartySystemID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetByUserNameAndDomain] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential__GetByUserNameAndDomain] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetByUserNameAndDomain] TO [sp_executeall]
GO
