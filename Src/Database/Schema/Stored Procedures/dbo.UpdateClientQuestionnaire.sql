SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Object:  Stored Procedure dbo.UpdateClientQuestionnaire    Script Date: 08/09/2006 12:22:46 ******/

CREATE PROCEDURE [dbo].[UpdateClientQuestionnaire]
@ClientQuestionnaireID int,
@ClientID int,
@QuestionnaireTitle nvarchar(255), 
@QuestionnaireDescription text,
@QuestionsPerPage int,
@QuestionnaireIntroductionText text,
@HideIntro bit,
@QuestionnaireFooterText text,
@HideFooter bit,
@QuestionnaireFooterIframe text,
@QuestionnaireHeaderIframe text,
@QuestionnaireFooterInternal bit,
@QuestionnaireHeaderInternal bit,
@QuestionnaireFooterIframeHeight int,
@QuestionnaireHeaderIframeHeight int,
@QuestionnaireFooterIframeWidth int,
@QuestionnaireHeaderIframeWidth int,
@FrameMode bit,
@LayoutCss text,
@LayoutCssFileName nvarchar(512),
@ImportDirectlyIntoLeadManager bit,
@RunAsClientPersonnelID int = null,
@RememberAnswers bit = 1 

as
BEGIN

	-- JG 2008-06-13 Cater for Move Questionnaire, which has no "RunAs" userid built in
	IF @RunAsClientPersonnelID IS NULL
	BEGIN
		SELECT @RunAsClientPersonnelID = MIN(ClientPersonnelID) 
		FROM ClientPersonnel 
		WHERE ClientID = @ClientID
	END

	Update ClientQuestionnaires
	SET ClientID = @ClientID,
	QuestionnaireTitle = @QuestionnaireTitle,
	QuestionnaireDescription = @QuestionnaireDescription,
	QuestionsPerPage = @QuestionsPerPage,
	QuestionnaireIntroductionText = @QuestionnaireIntroductionText,
	HideIntro = @HideIntro,
	QuestionnaireFooterText =@QuestionnaireFooterText,
	HideFooter = @HideFooter,
	QuestionnaireFooterIframe = @QuestionnaireFooterIframe,
	QuestionnaireHeaderIframe = @QuestionnaireHeaderIframe,
	QuestionnaireFooterInternal = @QuestionnaireFooterInternal,
	QuestionnaireHeaderInternal = @QuestionnaireHeaderInternal,
	QuestionnaireFooterIframeHeight = @QuestionnaireFooterIframeHeight,
	QuestionnaireHeaderIframeHeight = @QuestionnaireHeaderIframeHeight,
	QuestionnaireFooterIframeWidth = @QuestionnaireFooterIframeWidth,
	QuestionnaireHeaderIframeWidth = @QuestionnaireHeaderIframeWidth,
	FrameMode = @FrameMode,
	LayoutCss = @LayoutCss,
	LayoutCssFileName = @LayoutCssFileName,
	ImportDirectlyIntoLeadManager = @ImportDirectlyIntoLeadManager,
	RunAsClientPersonnelID = @RunAsClientPersonnelID,
	RememberAnswers = @RememberAnswers
	where ClientQuestionnaireID = @ClientQuestionnaireID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnaire] TO [sp_executeall]
GO
