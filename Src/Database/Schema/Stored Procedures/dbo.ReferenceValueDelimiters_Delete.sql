SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReferenceValueDelimiters table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDelimiters_Delete]
(

	@ReferenceValueDelimiterID int   
)
AS


				DELETE FROM [dbo].[ReferenceValueDelimiters] WITH (ROWLOCK) 
				WHERE
					[ReferenceValueDelimiterID] = @ReferenceValueDelimiterID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDelimiters_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Delete] TO [sp_executeall]
GO
