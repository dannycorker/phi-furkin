SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuestionnaireParts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireParts_Insert]
(

	@PartNameID int    OUTPUT,

	@PartName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[QuestionnaireParts]
					(
					[PartName]
					)
				VALUES
					(
					@PartName
					)
				-- Get the identity value
				SET @PartNameID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireParts_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Insert] TO [sp_executeall]
GO
