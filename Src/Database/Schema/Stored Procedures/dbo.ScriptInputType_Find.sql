SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptInputType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptInputType_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptInputTypeID int   = null ,

	@InputType varchar (50)  = null ,

	@Template varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptInputTypeID]
	, [InputType]
	, [Template]
    FROM
	[dbo].[ScriptInputType] WITH (NOLOCK) 
    WHERE 
	 ([ScriptInputTypeID] = @ScriptInputTypeID OR @ScriptInputTypeID IS NULL)
	AND ([InputType] = @InputType OR @InputType IS NULL)
	AND ([Template] = @Template OR @Template IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptInputTypeID]
	, [InputType]
	, [Template]
    FROM
	[dbo].[ScriptInputType] WITH (NOLOCK) 
    WHERE 
	 ([ScriptInputTypeID] = @ScriptInputTypeID AND @ScriptInputTypeID is not null)
	OR ([InputType] = @InputType AND @InputType is not null)
	OR ([Template] = @Template AND @Template is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptInputType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Find] TO [sp_executeall]
GO
