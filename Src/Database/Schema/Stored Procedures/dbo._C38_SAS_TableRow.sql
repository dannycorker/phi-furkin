SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-11-25
-- Description:	SAS TableRow for Client 38
-- =============================================
CREATE PROCEDURE [dbo].[_C38_SAS_TableRow] 
@ClientPersonnelID int,
@CustomerID int = NULL,
@LeadID int = NULL,
@CaseID int = NULL, 
@MatterID int = NULL,
@TableRowID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DetailFieldID int
	
	SELECT @DetailFieldID = tr.DetailFieldID
	FROM TableRows tr WITH (NOLOCK)
	WHERE TableRowID = @TableRowID

	/*Expense Sheets*/
	IF @DetailFieldID = 141493
	BEGIN
	
		IF (SELECT ISNULL(ValueInt,0) FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.TableRowID = @TableRowID and tdv.DetailFieldID = 141513) = 0
		BEGIN
	
			/*Standard Set Who And When Saved*/
			EXEC dbo._C00_TableRowSetSaved @TableRowID, @ClientPersonnelID, 141508, 141509, 141513
	
		END
	
	END

	/*Expenses*/
	IF @DetailFieldID = 141491
	BEGIN
	
		/*Standard Set Who And When Saved*/
		EXEC dbo._C00_TableRowSetSaved @TableRowID, @ClientPersonnelID, 141510, 141511, 141512
	
	END

	/*Milage*/
	IF @DetailFieldID = 141492
	BEGIN
	
		/*Standard Set Who And When Saved*/
		EXEC dbo._C00_TableRowSetSaved @TableRowID, @ClientPersonnelID, 141514, 141515, 141516
	
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_SAS_TableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_SAS_TableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_SAS_TableRow] TO [sp_executeall]
GO
