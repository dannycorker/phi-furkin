SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-27
-- Description:	Gets the policy co-insurance for the selected policy sections for a set of grouped claims
-- Used by:		Calculation
-- Modified:	2012-11-22 - Robin Hall - Allow Pet Type to be unspecified on co-insurance record
--				2013-06-19 - Robin Hall - Special processing option to waive co-insurance (for cockup on PDSA renewals)
--				2014-06-27 - Simon Brushett - Added breed filtering to oc-insurance table
--				2014-08-27 - Simon Brushett - Refactored so can use in summary
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetGroupedPolicyCoInsurance] 
(
	@MatterID INT,
	@CurrentPolicyMatterID INT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @CurrentPolicyMatterID IS NULL
	BEGIN
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	END
	
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	SELECT ResourceListID, Section, SubSection, ExcessPercentage, DateCutOff, CoInsLinkedTo, MinThreshold, Breed
	FROM dbo.fn_C00_1272_Policy_GetPolicyCoInsurance(@LeadID, @CurrentPolicyMatterID)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyCoInsurance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyCoInsurance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyCoInsurance] TO [sp_executeall]
GO
