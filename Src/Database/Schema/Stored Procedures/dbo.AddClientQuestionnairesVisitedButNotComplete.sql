SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddClientQuestionnairesVisitedButNotComplete]

@ClientQuestionnaireID int,
@VisitedDate datetime,
@IPAddress nvarchar(50),
@ClientID int


As

insert ClientQuestionnairesVisitedButNotComplete (ClientQuestionnaireID, VisitedDate, IPAddress, ClientID)
values (@ClientQuestionnaireID, @VisitedDate, @IPAddress, @ClientID)

declare @ClientQuestionnairesVisitedButNotCompleteID int

set @ClientQuestionnairesVisitedButNotCompleteID = SCOPE_IDENTITY()

select ClientQuestionnairesVisitedButNotCompleteID from ClientQuestionnairesVisitedButNotComplete where ClientQuestionnairesVisitedButNotCompleteID = @ClientQuestionnairesVisitedButNotCompleteID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnairesVisitedButNotComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientQuestionnairesVisitedButNotComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnairesVisitedButNotComplete] TO [sp_executeall]
GO
