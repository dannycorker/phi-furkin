SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CurrencyRate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_Update]
(

	@CurrencyRateID int   ,

	@CurrencyId int   ,

	@CurrencyCode char (3)  ,

	@FromGBPRate decimal (18, 10)  ,

	@ToGBPRate decimal (18, 10)  ,

	@ConversionDate datetime   ,

	@IsLatest bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CurrencyRate]
				SET
					[CurrencyId] = @CurrencyId
					,[CurrencyCode] = @CurrencyCode
					,[FromGBPRate] = @FromGBPRate
					,[ToGBPRate] = @ToGBPRate
					,[ConversionDate] = @ConversionDate
					,[IsLatest] = @IsLatest
				WHERE
[CurrencyRateID] = @CurrencyRateID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Update] TO [sp_executeall]
GO
