SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget__DeleteByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleStandardTarget] WITH (ROWLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget__DeleteByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget__DeleteByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget__DeleteByLeadTypeID] TO [sp_executeall]
GO
