SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset
-- =============================================
CREATE PROCEDURE [dbo].[Asset__Get] 
	@ClientPersonnelID INT = NULL,
	@AssetID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		a.[AssetID],
		a.[ClientID],
		a.[AssetTypeID],
		a.[AssetSubTypeID],
		a.[LocationID],
		a.[AssetName],
		a.[Note],
		a.[SecureNote],
		a.[SecureUserName],
		a.[SecurePassword],
		a.[SecureOther],
		a.[Version],
		a.[ValidFrom],
		a.[ValidTo],
		a.[ReminderDue],
		a.[ReminderTimeUnitID],
		a.[ReminderTimeUnitQuantity],
		a.[ReminderNotificationGroupID],
		a.[Enabled],
		a.[Deleted],
		a.[WhoCreated],
		a.[WhenCreated],
		a.[WhoModified],
		a.[WhenModified]
	FROM 
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND @AssetID = a.[AssetID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Asset__Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset__Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset__Get] TO [sp_executeall]
GO
