SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-11
-- Description:	Get a list of all the table rows that
--              need to be recalculated when a lead/matter is saved
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetForTableEquations] 
	@ClientID int, 
	@LeadTypeID int, 
	@LeadID int,
	@MatterID int = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	FROM dbo.TableRows tr (nolock) 
	WHERE tr.LeadID = @LeadID 
	AND (tr.MatterID = @MatterID OR (@MatterID IS NULL AND (tr.MatterID IS NULL OR tr.MatterID = 0)))
	AND EXISTS (
		SELECT * 
		FROM dbo.TableDetailValues tdv (nolock) 
		WHERE tdv.LeadID = @LeadID
		AND tdv.DetailFieldID IN (
			SELECT df.DetailFieldID
			FROM dbo.DetailFields df_p (nolock) 
			INNER JOIN dbo.DetailFields df (nolock) ON df.DetailFieldPageID = df_p.TableDetailFieldPageID
			WHERE df_p.ClientID = @ClientID
			AND df_p.TableDetailFieldPageID IS NOT NULL
			/*AND df_p.LeadTypeID = @LeadTypeID*/
		)
	)
		
	ORDER BY tr.TableRowID 

	SELECT @@ROWCOUNT


END





GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetForTableEquations] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetForTableEquations] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetForTableEquations] TO [sp_executeall]
GO
