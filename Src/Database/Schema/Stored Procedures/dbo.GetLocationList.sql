SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Location List
-- =============================================
CREATE PROCEDURE [dbo].[GetLocationList] 
	@ClientPersonnelID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		loc.[LocationID],
		loc.[ClientID],
		loc.[LocationName],
		loc.[Note],
		loc.[Enabled],
		loc.[Deleted],
		loc.[WhoCreated],
		loc.[WhenCreated],
		loc.[WhoModified],
		loc.[WhenModified]
	FROM 
		[dbo].[Location] loc WITH (NOLOCK),
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND loc.[LocationID] = a.[LocationID]
	ORDER BY loc.[LocationID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetLocationList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLocationList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLocationList] TO [sp_executeall]
GO
