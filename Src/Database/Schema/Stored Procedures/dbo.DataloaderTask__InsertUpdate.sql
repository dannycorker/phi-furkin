SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-06-08
-- Description:	Add update remove dataloaderautomatedtask
-- =============================================
CREATE PROCEDURE [dbo].[DataloaderTask__InsertUpdate]
	@ClientID INT,
	@MapID INT,
	@TaskID INT
AS
BEGIN

	SET NOCOUNT ON;

	IF @TaskID > 0
	BEGIN

		UPDATE d
		SET AutomatedTaskID = @TaskID
		FROM DataLoaderAutomatedTask d
		WHERE d.ClientID = @ClientID
		AND d.DataloaderMapID = @MapID
		
		IF @@ROWCOUNT = 0
		BEGIN
		
			INSERT INTO DataLoaderAutomatedTask (ClientID, DataloaderMapID, AutomatedTaskID, DelaySeconds)
			VALUES (@ClientID, @MapID, @TaskID, 10)
		
		END

	END
	ELSE
	BEGIN
	
		/*No TaskID, remove any task on the map if it exists*/
		DELETE d 
		FROM DataLoaderAutomatedTask d
		WHERE d.DataloaderMapID = @MapID
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DataloaderTask__InsertUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataloaderTask__InsertUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataloaderTask__InsertUpdate] TO [sp_executeall]
GO
