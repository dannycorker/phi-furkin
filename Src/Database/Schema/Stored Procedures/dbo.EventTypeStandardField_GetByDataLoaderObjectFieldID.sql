SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeStandardField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_GetByDataLoaderObjectFieldID]
(

	@DataLoaderObjectFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeStandardFieldID],
					[EventTypeID],
					[DataLoaderObjectFieldID],
					[ClientID],
					[Mandatory]
				FROM
					[dbo].[EventTypeStandardField] WITH (NOLOCK) 
				WHERE
					[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_GetByDataLoaderObjectFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_GetByDataLoaderObjectFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_GetByDataLoaderObjectFieldID] TO [sp_executeall]
GO
