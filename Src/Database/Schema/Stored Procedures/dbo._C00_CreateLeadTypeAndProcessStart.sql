SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-10-2014
-- Description:	Creates a leadtype and its associated process start event
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateLeadTypeAndProcessStart]

	@ClientID INT,
	@UserID INT,
	@ScriptName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO LeadType (ClientID, LeadTypeName, LeadTypeDescription,Enabled, Live,MatterDisplayName,IsBusiness,LeadDisplayName,CaseDisplayName,CustomerDisplayName,UseEventDisbursements,UseEventComments,IsRPIEnabled,FollowupWorkingDaysOnly,CalculateAllMatters,SmsGatewayID)
	VALUES (@ClientID, @ScriptName, @ScriptName,1,1,'',0,'','','',1,1,0,0,0,1)
	
	DECLARE @LeadTypeID INT
	SET @LeadTypeID = SCOPE_IDENTITY()
	
	INSERT INTO EventType (ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort,AvailableManually,EventSubtypeID,LeadTypeID,AllowCustomTimeUnits,InProcess,KeyEvent,UseEventCosts,UseEventDisbursements,UseEventComments,SignatureRequired,SignatureOverride,WhoCreated,WhenCreated,WhoModified,WhenModified,FollowupWorkingDaysOnly)
	VALUES (@ClientID, 'Process Start', 'Process Start',1,0,1,10,@LeadTypeID,0,0,0,0,0,0,0,0,@UserID,dbo.fn_GetDate_Local(),@UserID,dbo.fn_GetDate_Local(),0)
	
	SELECT @LeadTypeID LeadTypeID
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateLeadTypeAndProcessStart] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateLeadTypeAndProcessStart] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateLeadTypeAndProcessStart] TO [sp_executeall]
GO
