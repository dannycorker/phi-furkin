SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[ShowEventChoiceHierarchy]
(
	@RootEventTypeID int
)
AS
BEGIN
	DECLARE @EventHierarchyTreeID int

	SET @EventHierarchyTreeID = IsNull((SELECT MAX(EventHierarchyTreeID) FROM EventHierarchyTree), 0) + 1

	EXEC ShowInnerEventChoiceHierarchy @RootEventTypeID, @EventHierarchyTreeID

	--SELECT * FROM EventHierarchyTree

	DELETE EventHierarchyTree WHERE EventHierarchyTreeID = @EventHierarchyTreeID
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ShowEventChoiceHierarchy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ShowEventChoiceHierarchy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ShowEventChoiceHierarchy] TO [sp_executeall]
GO
