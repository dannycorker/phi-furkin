SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CustomerOffice table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice__FindNoLock]
(

	@SearchUsingOR bit   = null ,

	@OfficeID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@OfficeName varchar (100)  = null ,

	@MainPhoneNumber varchar (100)  = null ,

	@FaxNumber varchar (100)  = null ,

	@Address1 varchar (100)  = null ,

	@Address2 varchar (100)  = null ,

	@Town varchar (100)  = null ,

	@County varchar (100)  = null ,

	@Postcode varchar (100)  = null ,

	@Country varchar (100)  = null ,

	@Notes varchar (255)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OfficeID]
	, [ClientID]
	, [CustomerID]
	, [OfficeName]
	, [MainPhoneNumber]
	, [FaxNumber]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [Notes]
    FROM
	[dbo].[CustomerOffice] with (nolock)
    WHERE 
	 ([OfficeID] = @OfficeID OR @OfficeID is null)
	AND ([ClientID] = @ClientID OR @ClientID is null)
	AND ([CustomerID] = @CustomerID OR @CustomerID is null)
	AND ([OfficeName] = @OfficeName OR @OfficeName is null)
	AND ([MainPhoneNumber] = @MainPhoneNumber OR @MainPhoneNumber is null)
	AND ([FaxNumber] = @FaxNumber OR @FaxNumber is null)
	AND ([Address1] = @Address1 OR @Address1 is null)
	AND ([Address2] = @Address2 OR @Address2 is null)
	AND ([Town] = @Town OR @Town is null)
	AND ([County] = @County OR @County is null)
	AND ([Postcode] = @Postcode OR @Postcode is null)
	AND ([Country] = @Country OR @Country is null)
	AND ([Notes] = @Notes OR @Notes is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OfficeID]
	, [ClientID]
	, [CustomerID]
	, [OfficeName]
	, [MainPhoneNumber]
	, [FaxNumber]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Postcode]
	, [Country]
	, [Notes]
    FROM
	[dbo].[CustomerOffice] with (nolock)
    WHERE 
	 ([OfficeID] = @OfficeID AND @OfficeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([OfficeName] = @OfficeName AND @OfficeName is not null)
	OR ([MainPhoneNumber] = @MainPhoneNumber AND @MainPhoneNumber is not null)
	OR ([FaxNumber] = @FaxNumber AND @FaxNumber is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([Postcode] = @Postcode AND @Postcode is not null)
	OR ([Country] = @Country AND @Country is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	Select @@ROWCOUNT			
  END
				





GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice__FindNoLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice__FindNoLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice__FindNoLock] TO [sp_executeall]
GO
