SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EsignatureApiKey table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureApiKey_Insert]
(

	@EsignatureApiKeyID int    OUTPUT,

	@ClientID int   ,

	@ApiKey varchar (50)  
)
AS


				
				INSERT INTO [dbo].[EsignatureApiKey]
					(
					[ClientID]
					,[ApiKey]
					)
				VALUES
					(
					@ClientID
					,@ApiKey
					)
				-- Get the identity value
				SET @EsignatureApiKeyID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureApiKey_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Insert] TO [sp_executeall]
GO
