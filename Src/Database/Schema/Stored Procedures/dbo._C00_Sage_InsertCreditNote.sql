SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-05-31
-- Description:	Create a Sage 200 credit note in Aquarium
-- Added Invoice number PR 06-06-2012
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_InsertCreditNote]
	@ClientID INT,
	@LeadID INT,
	@DatePosted VARCHAR(30) = NULL,
	@NominalCode VARCHAR(30) = NULL,
	@NominalCostCentre VARCHAR(100) = NULL,
	@NominalDepartment VARCHAR(100) = NULL,
	@CreditNoteNetAmount VARCHAR(100) = NULL,
	@SageUniqueReference VARCHAR(100) = NULL,
	@SageCompanyName VARCHAR(100) = NULL,
	@Message VARCHAR(2000) = NULL,
	@TransactionType VARCHAR(30),
	@InvoiceNumber VARCHAR(30) = NULL,
	@Debug BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ClientID = 3 AND @TransactionType = 2 -- Transaction Type 2 is Credit Note
	BEGIN
	
		SET @TransactionType = '4226'
	
	END
	
	DECLARE @LeadTypeID INT,
	@NewTableRowID INT,
	/* ThirdPartyMapping field IDs */
	@DatePostedFieldTP INT = 162,
	@MessageFieldTP INT = 165,
	@NominalAccountFieldTP INT = 170,
	@NominalCostCentreFieldTP INT = 171,
	@NominalDepartmentFieldTP INT = 172,
	@TransactionTypeFieldTP INT = 166,
	@SageCompanyNameFieldTP INT = 167,
	@CreditNoteNetAmountFieldTP INT = 184,
	@SageUniqueReferenceFieldTP INT = 185,
	@InvoiceNumberFieldTP INT = 160,
	/* Detail fields looked up from ThirdPartyMapping */
	@DatePostedField INT,
	@DatePostedPage INT,
	@DatePostedColumnField INT,
	@MessageColumnField INT,
	@NominalAccountColumnField INT,
	@NominalCostCentreColumnField INT,
	@NominalDepartmentColumnField INT,
	@TransactionTypeColumnField INT,
	@SageCompanyNameColumnField INT,
	@CreditNoteNetAmountColumnField INT,
	@SageUniqueReferenceColumnField INT,
	@InvoiceNumberField INT,
	@InvoiceNumberColumnField INT,
	@InvoiceNumberPage INT
	
	SELECT @LeadTypeID = l.LeadTypeID 
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID 
	
	SELECT @InvoiceNumberField = t.DetailFieldID, @InvoiceNumberColumnField = t.ColumnFieldID, @InvoiceNumberPage = df.DetailFieldPageID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
	WHERE t.ThirdPartyFieldID = @InvoiceNumberFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* DatePosted 
	SELECT @DatePostedField = t.DetailFieldID, @DatePostedColumnField = t.ColumnFieldID, @DatePostedPage = df.DetailFieldPageID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
	WHERE t.ThirdPartyFieldID = @DatePostedFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID */

	/* DatePosted */
	/* 
		This works differently to Invoices. Only the ColumnField is provided (in the DetailField column, doh), 
		so look up the real field by joining to DetailFields again on TableDetailFieldPageID and LeadTypeID.
	*/
	SELECT @DatePostedField = dfp.DetailFieldID, @DatePostedColumnField = t.DetailFieldID, @DatePostedPage = dfp.DetailFieldPageID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
	INNER JOIN dbo.DetailFields dfp WITH (NOLOCK) ON dfp.TableDetailFieldPageID = df.DetailFieldPageID 
	WHERE t.ThirdPartyFieldID = @DatePostedFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID AND dfp.LeadTypeID=@LeadTypeID
	
	/* Message */
	SELECT @MessageColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @MessageFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* @NominalAccount */
	SELECT @NominalAccountColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @NominalAccountFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* NominalCostCentre */
	SELECT @NominalCostCentreColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @NominalCostCentreFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* NominalDepartment */
	SELECT @NominalDepartmentColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @NominalDepartmentFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* TransactionType */
	SELECT @TransactionTypeColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @TransactionTypeFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* SageCompanyName */
	SELECT @SageCompanyNameColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @SageCompanyNameFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* CreditNoteNetAmount */
	SELECT @CreditNoteNetAmountColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @CreditNoteNetAmountFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* SageUniqueReference */
	SELECT @SageUniqueReferenceColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @SageUniqueReferenceFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	IF @Debug = 1
	BEGIN
		SELECT @LeadTypeID AS LeadTypeID,
		@DatePostedField AS DatePostedField,
		@DatePostedPage AS DatePostedPage,
		@DatePostedColumnField AS DatePostedColumnField,
		@MessageColumnField AS MessageColumnField,
		@NominalAccountColumnField AS NominalAccountColumnField,
		@NominalCostCentreColumnField AS NominalCostCentreColumnField,
		@NominalDepartmentColumnField AS NominalDepartmentColumnField,
		@TransactionTypeColumnField AS TransactionTypeColumnField,
		@SageCompanyNameColumnField AS SageCompanyNameColumnField,
		@CreditNoteNetAmountColumnField AS CreditNoteNetAmountColumnField,
		@SageUniqueReferenceColumnField AS SageUniqueReferenceColumnField
	END
	ELSE
	BEGIN
		INSERT INTO TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
		VALUES (@ClientID, @LeadID, @DatePostedField, @DatePostedPage, 1, 1)
		
		SELECT @NewTableRowID = SCOPE_IDENTITY()
	
		
		INSERT INTO TableDetailValues (ClientID, LeadID, TableRowID, DetailFieldID, DetailValue)
		VALUES  (@ClientID, @LeadID, @NewTableRowID, @DatePostedColumnField, @DatePosted),
				(@ClientID, @LeadID, @NewTableRowID, @MessageColumnField, @Message),
				(@ClientID, @LeadID, @NewTableRowID, @NominalAccountColumnField, @NominalCode),
				(@ClientID, @LeadID, @NewTableRowID, @NominalCostCentreColumnField, @NominalCostCentre),
				(@ClientID, @LeadID, @NewTableRowID, @NominalDepartmentColumnField, @NominalDepartment),
				(@ClientID, @LeadID, @NewTableRowID, @TransactionTypeColumnField, @TransactionType),
				(@ClientID, @LeadID, @NewTableRowID, @SageCompanyNameColumnField, @SageCompanyName),
				(@ClientID, @LeadID, @NewTableRowID, @CreditNoteNetAmountColumnField, @CreditNoteNetAmount),
				(@ClientID, @LeadID, @NewTableRowID, @SageUniqueReferenceColumnField, @SageUniqueReference),
				(@ClientID, @LeadID, @NewTableRowID, @InvoiceNumberField, @InvoiceNumber)
		
			/* Added By PR 18-06-2012 Places the data at the customer level for BF*/
		IF @ClientID = 3
		BEGIN
			
			DECLARE @CustNewTableRowID INT
			DECLARE @CustomerID INT
			
			SELECT @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
			VALUES (@ClientID, @CustomerID, 138005, 15489, 1, 1)
		
			SELECT @CustNewTableRowID = SCOPE_IDENTITY()
			
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			VALUES  (@ClientID, @CustomerID, @CustNewTableRowID, 137993, CONVERT(VARCHAR, @InvoiceNumber)),
				(@ClientID, @CustomerID, @CustNewTableRowID, 137994, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)),				
				(@ClientID, @CustomerID, @CustNewTableRowID, 137998, CONVERT(VARCHAR, @CreditNoteNetAmount)),
				(@ClientID, @CustomerID, @CustNewTableRowID, 149782, CONVERT(VARCHAR, @LeadID))
		
		END								
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InsertCreditNote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_InsertCreditNote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InsertCreditNote] TO [sp_executeall]
GO
