SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2016-03-08
-- Description:	Get a partner with lookup "expanded"
-- =============================================


CREATE PROCEDURE [dbo].[Partner__GetExpandedByCustomerID]
(
	@CustomerID int   
)
AS
BEGIN
				
		SELECT p.*, t.Title
		FROM [dbo].[Partner] p WITH (NOLOCK) 
		LEFT JOIN dbo.Titles t WITH (NOLOCK) ON p.TitleID = t.TitleID
		WHERE p.[CustomerID] = @CustomerID
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner__GetExpandedByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner__GetExpandedByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner__GetExpandedByCustomerID] TO [sp_executeall]
GO
