SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Functions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_Get_List]

AS


				
				SELECT
					[FunctionID],
					[FunctionName],
					[FunctionDescription],
					[FunctionTypeID],
					[ModuleID],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[Functions] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Get_List] TO [sp_executeall]
GO
