SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUserGroupPanelItemChartingAccess table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserGroupPanelItemChartingAccessID int   = null ,

	@ClientID int   = null ,

	@PortalUserGroupID int   = null ,

	@PanelItemChartingID int   = null ,

	@HasAccess bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserGroupPanelItemChartingAccessID]
	, [ClientID]
	, [PortalUserGroupID]
	, [PanelItemChartingID]
	, [HasAccess]
    FROM
	[dbo].[PortalUserGroupPanelItemChartingAccess] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserGroupPanelItemChartingAccessID] = @PortalUserGroupPanelItemChartingAccessID OR @PortalUserGroupPanelItemChartingAccessID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PortalUserGroupID] = @PortalUserGroupID OR @PortalUserGroupID IS NULL)
	AND ([PanelItemChartingID] = @PanelItemChartingID OR @PanelItemChartingID IS NULL)
	AND ([HasAccess] = @HasAccess OR @HasAccess IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserGroupPanelItemChartingAccessID]
	, [ClientID]
	, [PortalUserGroupID]
	, [PanelItemChartingID]
	, [HasAccess]
    FROM
	[dbo].[PortalUserGroupPanelItemChartingAccess] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserGroupPanelItemChartingAccessID] = @PortalUserGroupPanelItemChartingAccessID AND @PortalUserGroupPanelItemChartingAccessID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PortalUserGroupID] = @PortalUserGroupID AND @PortalUserGroupID is not null)
	OR ([PanelItemChartingID] = @PanelItemChartingID AND @PanelItemChartingID is not null)
	OR ([HasAccess] = @HasAccess AND @HasAccess is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess_Find] TO [sp_executeall]
GO
