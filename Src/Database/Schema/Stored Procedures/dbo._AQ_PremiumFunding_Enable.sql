SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2020-01-29
-- Description:	Enable or disable Premium Funding related Tasks/Events
-- =============================================
CREATE PROCEDURE [dbo].[_AQ_PremiumFunding_Enable]
	-- Add the parameters for the stored procedure here
	@ClientID INT,
	@Enabled BIT,
	@ClientPersonnelID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Log VARCHAR(3)

	/*
	AutomatedTask
	22943	Premium Credit Renewal Payment
	22942	Auto Renew PCL Policies without a valid title
	22927	Apply for new premium funding application - Auto
	22920	Expire Policy - Unsuccessful P.F. Response
	22917	Premium Funding Adjustment Successful
	22916	No response from Premium Funding Adjustment
	22915	NB No Premium Funding Response
	22914	Premium Credit Payment
	*/

	UPDATE task
	SET task.Enabled = @Enabled, task.WhoModified = @ClientPersonnelID, task.WhenModified = dbo.fn_GetDate_Local()
	FROM AutomatedTask task WITH (NOLOCK)
	WHERE task.TaskID IN (22943,22942,22927,22920,22917,22916,22915,22914)
	AND task.ClientID = @ClientID
	AND task.Enabled <> @Enabled

	/*
	EventType
	158974	Cancelled by Premium Funder
	158971	Premium Funding Renewal Unsuccessful
	158954	Premium Funding Adjustment Successful
	158941	Premium Funding Adjustment Unsuccessful
	158938	Renewal Window MTA (PremiumFunding Policy) Applied
	158937	Renewal Window MTA (PremiumFunding Policy)
	158935	Premium Collection Failure - Remove Flag
	158934	Premium Collection Failure - Apply Flag
	*/


	UPDATE eventtype
	SET eventtype.Enabled = @Enabled, eventtype.WhoModified = @ClientPersonnelID, eventtype.WhenModified = dbo.fn_GetDate_Local()
	FROM EventType eventtype WITH (NOLOCK)
	WHERE eventtype.EventTypeID IN (158974,158971,158954,158941,158938,158937,158935,158934)
	AND eventtype.ClientID = @ClientID
	AND eventtype.Enabled <> @Enabled


	/*Log change*/
	SELECT @Log = CASE @Enabled WHEN 1 THEN 'On' ELSE 'Off' END
	EXEC [dbo].[_C00_LogIt] 'Info', '_AQ_PremiumFunding_Enable', 'Premium Funding Config', @Log, @ClientPersonnelID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_AQ_PremiumFunding_Enable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_AQ_PremiumFunding_Enable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_AQ_PremiumFunding_Enable] TO [sp_executeall]
GO
