SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28/06/2016
-- Description:	Inserts a purchased product
-- 2016-10-12 ACE Updated to include additional fee
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProduct__Insert]	
	@ClientID INT, 
	@CustomerID INT,
	@AccountID INT,
	@PaymentFrequencyID INT,
	@NumberOfInstallments INT =  NULL,
	@ProductPurchasedOnDate DATETIME = NULL,
	@ValidFrom DATETIME = NULL,
	@ValidTo DATETIME = NULL,
	@PreferredPaymentDay INT = NULL,
	@FirstPaymentDate DATETIME = NULL,
	@RemainderUpFront BIT = NULL,
	@ProductName VARCHAR(250) = NULL,
	@ProductDescription VARCHAR(2000) = NULL,
	@ProductCostNet NUMERIC(18,2) = NULL,
	@ProductCostVAT NUMERIC(18,2) = NULL,
	@ProductCostGross NUMERIC(18,2) = NULL,
	@ProductCostCalculatedWithRuleSetID INT = NULL,
	@ProductCostCalculatedOn DATETIME = NULL,
	@ProductCostBreakdown XML = NULL,
	@ObjectID INT = NULL,
	@ObjectTypeID INT = NULL,
	@WhoCreated INT,
	@WhenCreated DATETIME,
	@WhoModified INT,
	@WhenModified DATETIME,
	@PaymentScheduleSuccessfullyCreated BIT = NULL,
	@PaymentScheduleCreatedOn INT = NULL,
	@PaymentScheduleFailedDataLoaderLogID INT = NULL,
	@PremiumCalculationDetailID INT = NULL,
	@ProductAdditionalFee NUMERIC(18, 2) = NULL

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO PurchasedProduct(ClientID, CustomerID, AccountID, PaymentFrequencyID, NumberOfInstallments, ProductPurchasedOnDate, ValidFrom, ValidTo, PreferredPaymentDay, FirstPaymentDate, RemainderUpFront, ProductName, ProductDescription, ProductCostNet, ProductCostVAT, ProductCostGross, ProductCostCalculatedWithRuleSetID, ProductCostCalculatedOn, ProductCostBreakdown, ObjectID, ObjectTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, PaymentScheduleSuccessfullyCreated, PaymentScheduleCreatedOn, PaymentScheduleFailedDataLoaderLogID, PremiumCalculationDetailID, ProductAdditionalFee)
	VALUES (@ClientID, @CustomerID, @AccountID, @PaymentFrequencyID, @NumberOfInstallments, @ProductPurchasedOnDate, @ValidFrom, @ValidTo, @PreferredPaymentDay, @FirstPaymentDate, @RemainderUpFront, @ProductName, @ProductDescription, @ProductCostNet, @ProductCostVAT, @ProductCostGross, @ProductCostCalculatedWithRuleSetID, @ProductCostCalculatedOn, @ProductCostBreakdown, @ObjectID, @ObjectTypeID, @WhoCreated, @WhenCreated, @WhoModified, @WhenModified, @PaymentScheduleSuccessfullyCreated, @PaymentScheduleCreatedOn, @PaymentScheduleFailedDataLoaderLogID, @PremiumCalculationDetailID, @ProductAdditionalFee)

	SELECT SCOPE_IDENTITY() AS PurchasedProductID 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProduct__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProduct__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProduct__Insert] TO [sp_executeall]
GO
