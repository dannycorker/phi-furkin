SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 08 October 2007

-- Created By:  Jim Green
-- Purpose: Select records from the ClientPersonnel table by Group, but some groups
-- are shared by many clients, so restrict the data that comes back.  Also, add an
-- indicator at the end to show if they have any special rights not common to the group.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel__GetByClientAndGroupSpecial]
(

	@ClientID int, @AdminGroupID int   
)
AS
BEGIN

	WITH UserDiffs AS (
		SELECT ClientPersonnelID AS [UserID] 
		FROM UserFunctionControl ufc 
		UNION 
		SELECT ClientPersonnelID AS [UserID] 
		FROM UserRightsDynamic urd
	) 
	SELECT
		cp.[ClientPersonnelID],
		cp.[ClientID],
		cp.[ClientOfficeID],
		cp.[TitleID],
		cp.[FirstName],
		cp.[MiddleName],
		cp.[LastName],
		cp.[JobTitle],
		cp.[Password],
		cp.[ClientPersonnelAdminGroupID],
		cp.[MobileTelephone],
		cp.[HomeTelephone],
		cp.[OfficeTelephone],
		cp.[OfficeTelephoneExtension],
		cp.[EmailAddress],
		cp.[ChargeOutRate],
		cp.[UserName],
		cp.[Salt],
		cp.[AttemptedLogins],
		cp.[AccountDisabled],
		cp.[ManagerID],
		cp.[LanguageID],
		CASE COALESCE(UserDiffs.UserID, 0) WHEN 0 THEN '' ELSE 'Yes' END as 'HasDifferences' ,
		cp.SubClientID
	FROM
		[dbo].[ClientPersonnel] cp LEFT JOIN UserDiffs ON UserDiffs.UserID = cp.ClientPersonnelID 
	WHERE
		cp.[ClientID] = @ClientID AND cp.[ClientPersonnelAdminGroupID] = @AdminGroupID
	ORDER BY 
		cp.[UserName], cp.[ClientPersonnelID]

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetByClientAndGroupSpecial] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetByClientAndGroupSpecial] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetByClientAndGroupSpecial] TO [sp_executeall]
GO
