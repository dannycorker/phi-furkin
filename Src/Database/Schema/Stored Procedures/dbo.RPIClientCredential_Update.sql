SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIClientCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientCredential_Update]
(

	@RPIClientCredentialID int   ,

	@ClientID int   ,

	@UserName varchar (250)  ,

	@Password varchar (50)  ,

	@AsUser varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIClientCredential]
				SET
					[ClientID] = @ClientID
					,[UserName] = @UserName
					,[Password] = @Password
					,[AsUser] = @AsUser
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[RPIClientCredentialID] = @RPIClientCredentialID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientCredential_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientCredential_Update] TO [sp_executeall]
GO
