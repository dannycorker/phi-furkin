SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the MatterListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_Update]
(

	@MatterListDisplayID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@MatterRef bit   ,

	@MatterStatus bit   ,

	@MatterID bit   ,

	@Field1 int   ,

	@Field2 int   ,

	@Field3 int   ,

	@Field4 int   ,

	@Field5 int   ,

	@Field1ColumnDetailFieldID int   ,

	@Field2ColumnDetailFieldID int   ,

	@Field3ColumnDetailFieldID int   ,

	@Field4ColumnDetailFieldID int   ,

	@Field5ColumnDetailFieldID int   ,

	@SqlQueryText varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[MatterListDisplay]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[MatterRef] = @MatterRef
					,[MatterStatus] = @MatterStatus
					,[MatterID] = @MatterID
					,[Field1] = @Field1
					,[Field2] = @Field2
					,[Field3] = @Field3
					,[Field4] = @Field4
					,[Field5] = @Field5
					,[Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID
					,[Field2ColumnDetailFieldID] = @Field2ColumnDetailFieldID
					,[Field3ColumnDetailFieldID] = @Field3ColumnDetailFieldID
					,[Field4ColumnDetailFieldID] = @Field4ColumnDetailFieldID
					,[Field5ColumnDetailFieldID] = @Field5ColumnDetailFieldID
					,[SqlQueryText] = @SqlQueryText
				WHERE
[MatterListDisplayID] = @MatterListDisplayID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Update] TO [sp_executeall]
GO
