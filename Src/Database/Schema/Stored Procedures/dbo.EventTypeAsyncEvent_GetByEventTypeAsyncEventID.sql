SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAsyncEvent table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAsyncEvent_GetByEventTypeAsyncEventID]
(

	@EventTypeAsyncEventID int   
)
AS


				SELECT
					[EventTypeAsyncEventID],
					[ClientID],
					[SourceEventTypeID],
					[AsyncEventTypeID],
					[FollowupThreadNumber],
					[AquariumOptionID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[EventTypeAsyncEvent] WITH (NOLOCK) 
				WHERE
										[EventTypeAsyncEventID] = @EventTypeAsyncEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_GetByEventTypeAsyncEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAsyncEvent_GetByEventTypeAsyncEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_GetByEventTypeAsyncEventID] TO [sp_executeall]
GO
