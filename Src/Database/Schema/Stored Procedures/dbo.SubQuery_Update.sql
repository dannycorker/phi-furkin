SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SubQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQuery_Update]
(

	@SubQueryID int   ,

	@ClientID int   ,

	@SubQueryName varchar (50)  ,

	@SubQueryDesc varchar (500)  ,

	@SubQueryText varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SubQuery]
				SET
					[ClientID] = @ClientID
					,[SubQueryName] = @SubQueryName
					,[SubQueryDesc] = @SubQueryDesc
					,[SubQueryText] = @SubQueryText
				WHERE
[SubQueryID] = @SubQueryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Update] TO [sp_executeall]
GO
