SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PageGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroup_Insert]
(

	@PageGroupID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@PageGroupName varchar (100)  ,

	@PageGroupDescription varchar (255)  ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[PageGroup]
					(
					[ClientID]
					,[LeadTypeID]
					,[PageGroupName]
					,[PageGroupDescription]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@PageGroupName
					,@PageGroupDescription
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @PageGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Insert] TO [sp_executeall]
GO
