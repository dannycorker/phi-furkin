SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-06-27
-- Description:	Inserts an invoice row for the given customer 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_InvoiceTableRows_Insert]
(	
	@ClientID INT,
	@LeadID INT,
	@InvoiceNumber varchar(max),	
	@DateRaised varchar(max),	
	@InvoiceNet varchar(max),	
	@InvoiceVAT varchar(max),	
	@InvoiceTotal varchar(max),	
	@InvoiceBalance varchar(max)	
)
AS
BEGIN
	DECLARE @CustomerID INT,
			@InvoiceTableDetailFieldID INT,
			@InvoiceNumberDetailFieldID INT,
			@DateRaisedDetailFieldID INT,
			@InvoiceNetDetailFieldID INT,
			@InvoiceVATDetailFieldID INT,
			@InvoiceTotalDetailFieldID INT,
			@InvoiceBalanceDetailFieldID INT,
			@InvoicePageID INT,
			@NewTableRowID INT
		
	SELECT @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID AND ClientID=@ClientID
	
	SELECT @InvoiceTableDetailFieldID=tpfm.DetailFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=190
	
	SELECT @InvoicePageID=DetailFieldPageID FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @InvoiceTableDetailFieldID

	SELECT @InvoiceNumberDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=191
	
	SELECT @DateRaisedDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=192

	SELECT @InvoiceNetDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=212
	
	SELECT @InvoiceVATDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=213
	
	SELECT @InvoiceTotalDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=195
		
	SELECT @InvoiceBalanceDetailFieldID=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=196

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
	VALUES (@ClientID, @CustomerID, @InvoiceTableDetailFieldID, @InvoicePageID, 1, 1)
		
	SELECT @NewTableRowID = SCOPE_IDENTITY()
			
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	VALUES  (@ClientID, @CustomerID, @NewTableRowID, @InvoiceNumberDetailFieldID, @InvoiceNumber),
			(@ClientID, @CustomerID, @NewTableRowID, @DateRaisedDetailFieldID, @DateRaised),
			(@ClientID, @CustomerID, @NewTableRowID, @InvoiceNetDetailFieldID, @InvoiceNet),
			(@ClientID, @CustomerID, @NewTableRowID, @InvoiceVATDetailFieldID, @InvoiceVAT),
			(@ClientID, @CustomerID, @NewTableRowID, @InvoiceTotalDetailFieldID, @InvoiceTotal),
			(@ClientID, @CustomerID, @NewTableRowID, @InvoiceBalanceDetailFieldID, @InvoiceBalance)
			
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InvoiceTableRows_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_InvoiceTableRows_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InvoiceTableRows_Insert] TO [sp_executeall]
GO
