SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-01-24
-- Description:	Gets a list of Equation Targets based upon the EquationDetailFieldID
-- =============================================
CREATE PROCEDURE [dbo].[EquationTarget__GetByEquationDetailFieldID]
(

	@EquationDetailFieldID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[EquationTargetID],
					[ClientID],
					[EquationDetailFieldID],
					[DetailFieldID],
					[Target],
					[ObjectName],
					[PropertyName],
					[SqlFunctionID],					
					[IsEquation],
					[IsColumnSum],
					[ColumnFieldID],
					[DetailFieldSubTypeID]
				FROM
					[dbo].[EquationTarget]
				WHERE
					[EquationDetailFieldID] = @EquationDetailFieldID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget__GetByEquationDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget__GetByEquationDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget__GetByEquationDetailFieldID] TO [sp_executeall]
GO
