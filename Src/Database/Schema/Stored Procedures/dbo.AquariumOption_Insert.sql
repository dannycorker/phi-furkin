SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AquariumOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_Insert]
(

	@AquariumOptionID int   ,

	@AquariumOptionTypeID int   ,

	@AquariumOptionName varchar (50)  ,

	@AquariumOptionDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[AquariumOption]
					(
					[AquariumOptionID]
					,[AquariumOptionTypeID]
					,[AquariumOptionName]
					,[AquariumOptionDescription]
					)
				VALUES
					(
					@AquariumOptionID
					,@AquariumOptionTypeID
					,@AquariumOptionName
					,@AquariumOptionDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Insert] TO [sp_executeall]
GO
