SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ActiveSession table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_GetByEmailAddressThirdPartySystemId]
(

	@EmailAddress varchar (255)  ,

	@ThirdPartySystemId int   
)
AS


				SELECT
					[SessionID],
					[EmailAddress],
					[ThirdPartySystemId]
				FROM
					[dbo].[ActiveSession] WITH (NOLOCK) 
				WHERE
										[EmailAddress] = @EmailAddress
					AND [ThirdPartySystemId] = @ThirdPartySystemId
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetByEmailAddressThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_GetByEmailAddressThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_GetByEmailAddressThirdPartySystemId] TO [sp_executeall]
GO
