SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuestionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionTypes_Delete]
(

	@QuestionTypeID int   
)
AS


				DELETE FROM [dbo].[QuestionTypes] WITH (ROWLOCK) 
				WHERE
					[QuestionTypeID] = @QuestionTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionTypes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Delete] TO [sp_executeall]
GO
