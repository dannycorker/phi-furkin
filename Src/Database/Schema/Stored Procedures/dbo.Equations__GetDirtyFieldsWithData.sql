SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-02-08
-- Description:	Get Equationy stuff for PR
-- 2011-02-17 JWG Fixed column types in table definition. Join to the right DetailFields record (df_target) or everything looks like an equation rather than a lookup list etc
-- MODIFIED:	2014-07-21	SB	Updated to use view
-- MODIFIED:	2015-02-04	ACE	Updated to use customer tables (IE "IN (@LeadTypeID,0)") #30916
-- =============================================
CREATE PROCEDURE [dbo].[Equations__GetDirtyFieldsWithData]
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@ClientID INT,
	@ClientPersonnelID INT = NULL,
	@ContactID INT = NULL,
	@LeadTypeID INT = NULL,
	@DirtyFields tvpint readonly
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DetailFields TABLE (
		[DetailFieldID] [INT],
		[ClientID] [INT] ,
		[LeadOrMatter] [TINYINT] ,
		[FieldName] [VARCHAR](50) ,
		[FieldCaption] [VARCHAR](100) ,
		[QuestionTypeID] [INT] ,
		[Required] [BIT] ,
		[Lookup] [BIT] ,
		[LookupListID] [INT] ,
		[LeadTypeID] [INT] ,
		[Enabled] [BIT] ,
		[DetailFieldPageID] [INT] ,
		[FieldOrder] [INT] ,
		[MaintainHistory] [BIT] ,
		[EquationText] [VARCHAR](2000) ,
		[MasterQuestionID] [INT] ,
		[FieldSize] [INT] ,
		[LinkedDetailFieldID] [INT] ,
		[ValidationCriteriaFieldTypeID] [INT] ,
		[ValidationCriteriaID] [INT] ,
		[MinimumValue] [VARCHAR](50) ,
		[MaximumValue] [VARCHAR](50) ,
		[RegEx] [VARCHAR](2000) ,
		[ErrorMessage] [VARCHAR](250) ,
		[ResourceListDetailFieldPageID] [INT] ,
		[TableDetailFieldPageID] [INT] ,
		[DefaultFilter] [VARCHAR](250) ,
		[ColumnEquationText] [VARCHAR](2000) ,
		[Editable] [BIT] ,
		[Hidden] [BIT] ,
		[LastReferenceInteger] [INT] ,
		[ReferenceValueFormatID] [INT] ,
		[Encrypt] [BIT] ,
		[ShowCharacters] [INT] ,
		[NumberOfCharactersToShow] [INT] ,
		[TableEditMode] [INT] ,
		[DisplayInTableView] [BIT] 
	)

	DECLARE @EquatoinFields TABLE (DetailFieldID INT, LeadOrMatter INT, EquationText VARCHAR(2000), TargetFieldID INT, FieldOrder INT)

	EXEC dbo.DetailValues__CreateAll @ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientPersonnelID, @ContactID
	
	DECLARE @InsertCount int,
			@TestID int = 0

	INSERT INTO @DetailFields
	EXEC dbo.DetailFields__GetAllByID @CustomerID, @LeadID, @CaseID, @MatterID, @ClientID, @ClientPersonnelID, @ContactID

	/*Add this on live*/
    INSERT INTO @DetailFields
    SELECT df_table.[DetailFieldID],df_table.[ClientID],df_table.[LeadOrMatter],df_table.[FieldName],df_table.[FieldCaption],df_table.[QuestionTypeID],df_table.[Required],df_table.[Lookup],df_table.[LookupListID],df_table.[LeadTypeID],df_table.[Enabled],df_table.[DetailFieldPageID],df_table.[FieldOrder],df_table.[MaintainHistory],df_table.[EquationText],df_table.[MasterQuestionID],df_table.[FieldSize],df_table.[LinkedDetailFieldID],df_table.[ValidationCriteriaFieldTypeID],df_table.[ValidationCriteriaID],df_table.[MinimumValue],df_table.[MaximumValue],df_table.[RegEx],df_table.[ErrorMessage],df_table.[ResourceListDetailFieldPageID],df_table.[TableDetailFieldPageID],df_table.[DefaultFilter],df_table.[ColumnEquationText],df_table.[Editable],df_table.[Hidden],df_table.[LastReferenceInteger],df_table.[ReferenceValueFormatID],df_table.[Encrypt],df_table.[ShowCharacters],df_table.[NumberOfCharactersToShow],df_table.[TableEditMode],df_table.[DisplayInTableView]
    from dbo.fnDetailFieldsShared(@ClientID) df 
    INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df_table ON df_table.DetailFieldPageID = df.TableDetailFieldPageID and df_table.QuestionTypeID = 10
    where df.LeadTypeID IN (@LeadTypeID,0)
    and df.QuestionTypeID IN (16,19)
    
	/* 
		The app demands an int for LeadOrMatter, so it falls over trying to convert 1 to an int.
		This CAST helps it out.
	*/
	INSERT INTO @EquatoinFields (DetailFieldID, LeadOrMatter, EquationText, TargetFieldID, FieldOrder)
	SELECT et.EquationDetailFieldID, CAST(df.LeadOrMatter as int) as [LeadOrMatter], df.EquationText, et.DetailFieldID, df.FieldOrder
	FROM @DetailFields df
	INNER JOIN EquationTarget et WITH (NOLOCK)  on et.EquationDetailFieldID = df.DetailFieldID
	INNER JOIN @DirtyFields t ON t.AnyID = et.DetailFieldID 
	WHERE df.QuestionTypeID = 10

	/*
		2013-06-12 ACE Added invoke function so it will not break any type of DM leadtype
		2013-06-13 ACE Moved this section up before the while loop.
	*/
	INSERT INTO @EquatoinFields (DetailFieldID, LeadOrMatter, EquationText, TargetFieldID, FieldOrder)
	SELECT df.DetailFieldID, CAST(df.LeadOrMatter as int) as [LeadOrMatter], df.EquationText, NULL, df.FieldOrder
	FROM @DetailFields df
	WHERE df.QuestionTypeID = 10
	AND (
		df.EquationText LIKE '%InvokeFunction%' 
		OR 
		df.EquationText LIKE '%TableColumnSum%'
		)
	AND (df.LeadTypeID = @LeadTypeID /*OR df.LeadTypeID in (0,1,2,3)*/)
	AND df.ClientID = @ClientID

	INSERT INTO @EquatoinFields (DetailFieldID, LeadOrMatter, EquationText, TargetFieldID, FieldOrder)
	SELECT df.DetailFieldID , CAST(df.LeadOrMatter as int) as [LeadOrMatter], df.EquationText, NULL, df.FieldOrder
	FROM @DetailFields df
	WHERE df.QuestionTypeID = 10
	AND EXISTS (
		SELECT *
		FROM EquationTarget et WITH (NOLOCK)  
		WHERE et.EquationDetailFieldID = df.DetailFieldID 
		and et.ObjectName IN ('Customers', 'Contact', 'Lead', 'Clients', 'Partner', 'Cases', 'ClientOffices', 'ClientPersonnel', 'Matter')
	)
	/*2015-10-02 ACE Removed redundant diplicate fields*/
	AND NOT EXISTS (
		SELECT *
		FROM @EquatoinFields e
		WHERE e.DetailFieldID = df.DetailFieldID
	)
	
	/* 2013-07-22 ACE For ticket #22262 @@RowCount has been replaced with the below
	SELECT @InsertCount = @@ROWCOUNT 
	*/

	SELECT @InsertCount = COUNT(*)
	FROM @EquatoinFields
	
	WHILE @InsertCount > 0 --AND @TestID <=6
	BEGIN
	
		INSERT INTO @EquatoinFields (DetailFieldID, LeadOrMatter, EquationText, TargetFieldID, FieldOrder)
		SELECT et.EquationDetailFieldID, CAST(df.LeadOrMatter as int) as [LeadOrMatter], df.EquationText, et.DetailFieldID, df.FieldOrder
		FROM @DetailFields df
		INNER JOIN EquationTarget et WITH (NOLOCK)  on et.EquationDetailFieldID = df.DetailFieldID
		INNER JOIN @EquatoinFields ef ON ef.DetailFieldID = et.DetailFieldID
		WHERE df.QuestionTypeID = 10
		AND NOT EXISTS (
			SELECT *
			FROM @EquatoinFields e
			WHERE e.DetailFieldID = et.EquationDetailFieldID
		)

		SELECT @InsertCount = @@ROWCOUNT

		SELECT @TestID = @TestID+1

	END
	
	/*
		ACE 2015-08-25 #34129 Added the following so the equation scheduler can use an equation as a 
		dirty field.
	*/
	INSERT INTO @EquatoinFields (DetailFieldID, LeadOrMatter, EquationText, TargetFieldID, FieldOrder)
	SELECT df.DetailFieldID, df.LeadOrMatter, df.EquationText, et.DetailFieldID, df.FieldOrder
	FROM @DirtyFields d
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = d.AnyID AND df.QuestionTypeID = 10
	LEFT JOIN EquationTarget et WITH (NOLOCK)  on et.EquationDetailFieldID = df.DetailFieldID
	WHERE NOT EXISTS (
		SELECT *
		FROM @EquatoinFields e 
		WHERE e.DetailFieldID = d.AnyID
	)
	
	/*2015-10-02 ACE Added Distinct*/
	SELECT DISTINCT DetailFieldID, LeadOrMatter, EquationText, ef.FieldOrder 
	FROM @EquatoinFields ef
	ORDER BY ef.FieldOrder

	SELECT e.EquationDetailFieldID, e.DetailFieldID, e.ColumnFieldID, e.Target, e.IsEquation, e.IsColumnSum, e.DetailFieldSubTypeID, 
		ISNULL( CASE df_target.QuestionTypeID  
		WHEN 4 THEN COALESCE(luli_cdv.ItemValue, luli_ldv.ItemValue, luli_cadv.ItemValue, luli_mdv.ItemValue, luli_cldv.ItemValue, luli_cpdv.ItemValue, luli_codv.ItemValue) 
		WHEN 2 THEN COALESCE(luli_cdv.ItemValue, luli_ldv.ItemValue, luli_cadv.ItemValue, luli_mdv.ItemValue, luli_cldv.ItemValue, luli_cpdv.ItemValue, luli_codv.ItemValue) 
--		WHEN 5 THEN CONVERT(VARCHAR(10),COALESCE(cdv.ValueDate, ldv.ValueDate, cadv.ValueDate, mdv.ValueDate, cldv.ValueDate, cpdv.ValueDate, codv.ValueDate),103)
		ELSE COALESCE(cdv.DetailValue, ldv.DetailValue, cadv.DetailValue, mdv.DetailValue, cldv.DetailValue, cpdv.DetailValue, codv.DetailValue) 
		END, '') AS DetailValue
	FROM @DetailFields df
	INNER JOIN EquationTarget e WITH (NOLOCK) ON e.EquationDetailFieldID = df.DetailFieldID
	LEFT JOIN fnDetailFieldsShared(@ClientID) df_target ON df_target.DetailFieldID = e.DetailFieldID
	LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK)  ON cdv.DetailFieldID = e.DetailFieldID AND cdv.CustomerID = @CustomerID
	LEFT JOIN LookupListItems luli_cdv WITH (NOLOCK) ON cdv.ValueInt = luli_cdv.LookupListItemID AND luli_cdv.LookupListID = df_target.LookupListID
	LEFT JOIN LeadDetailValues ldv WITH (NOLOCK)  ON ldv.DetailFieldID = e.DetailFieldID AND ldv.LeadID = @LeadID
	LEFT JOIN LookupListItems luli_ldv WITH (NOLOCK) ON ldv.ValueInt = luli_ldv.LookupListItemID AND luli_ldv.LookupListID = df_target.LookupListID
	LEFT JOIN CaseDetailValues cadv WITH (NOLOCK)  ON cadv.DetailFieldID = e.DetailFieldID AND cadv.CaseID = @CaseID
	LEFT JOIN LookupListItems luli_cadv WITH (NOLOCK) ON cadv.ValueInt = luli_cadv.LookupListItemID AND luli_cadv.LookupListID = df_target.LookupListID
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK)  ON mdv.DetailFieldID = e.DetailFieldID AND mdv.MatterID = @MatterID
	LEFT JOIN LookupListItems luli_mdv WITH (NOLOCK) ON mdv.ValueInt = luli_mdv.LookupListItemID AND luli_mdv.LookupListID = df_target.LookupListID
	LEFT JOIN ClientDetailValues cldv WITH (NOLOCK)  ON cldv.DetailFieldID = e.DetailFieldID AND cldv.ClientID = @ClientID
	LEFT JOIN LookupListItems luli_cldv WITH (NOLOCK) ON cldv.ValueInt = luli_cldv.LookupListItemID AND luli_cldv.LookupListID = df_target.LookupListID
	LEFT JOIN ClientPersonnelDetailValues cpdv WITH (NOLOCK)  ON cpdv.DetailFieldID = e.DetailFieldID AND cpdv.ClientPersonnelID = @ClientPersonnelID
	LEFT JOIN LookupListItems luli_cpdv WITH (NOLOCK) ON cpdv.ValueInt = luli_cpdv.LookupListItemID AND luli_cpdv.LookupListID = df_target.LookupListID
	LEFT JOIN ContactDetailValues codv WITH (NOLOCK)  ON codv.DetailFieldID = e.DetailFieldID AND codv.ContactID = @ContactID
	LEFT JOIN LookupListItems luli_codv WITH (NOLOCK) ON codv.ValueInt = luli_codv.LookupListItemID AND luli_codv.LookupListID = df_target.LookupListID
	WHERE df.QuestionTypeID = 10

END

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__GetDirtyFieldsWithData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations__GetDirtyFieldsWithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__GetDirtyFieldsWithData] TO [sp_executeall]
GO
