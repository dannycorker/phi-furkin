SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClassNodeAuth table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_Update]
(

	@ClassNodeAuthID int   ,

	@ClientID int   ,

	@ClassNodeID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@RightsLevel int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Deleted bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClassNodeAuth]
				SET
					[ClientID] = @ClientID
					,[ClassNodeID] = @ClassNodeID
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[RightsLevel] = @RightsLevel
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[Deleted] = @Deleted
				WHERE
[ClassNodeAuthID] = @ClassNodeAuthID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Update] TO [sp_executeall]
GO
