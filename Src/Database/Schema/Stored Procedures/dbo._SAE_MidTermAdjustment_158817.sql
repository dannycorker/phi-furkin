SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-05
-- Description:	Sql After Event - Create Card Transaction (MTA)
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_158817]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@AccountID						INT
			,@CustomerPaymentScheduleID		INT
			,@Amount						MONEY
			,@PaymentTypeID					INT
			,@ClientPaymentGatewayID		INT
			,@ActualCollectionDate			DATE
			,@PaymentGross					MONEY
			,@PaymentNet					MONEY
			,@PaymentTax					MONEY
			,@FirstName						VARCHAR(100)
			,@LastName						VARCHAR(100)
			,@Address1						VARCHAR(50)
			,@Address2						VARCHAR(50)
			,@AddressTown					VARCHAR(50)
			,@AddressCounty					VARCHAR(50)
			,@PostCode						VARCHAR(50)
/* ===================================================================================================================== */

	/*Select and assign Aquarium IDs using the LeadEventID*/
	SELECT TOP (1)   @LeadID		= le.LeadID
					,@EventTypeID	= le.EventTypeID
					,@CustomerID	= l.CustomerID
					,@ClientID		= l.ClientID
					,@CaseID		= le.CaseID
					,@WhoCreated	= le.WhoCreated
					,@MatterID		= m.MatterID
					,@WhenCreated	= le.WhenCreated
					,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
					,@LeadTypeID	= l.LeadTypeID
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */
		
	/*Post MTA capture Card Details*/ 
	IF @EventTypeID IN (158817) /*Create Card Transactions*/
	BEGIN 
			
		SELECT @AccountID = NULLIF(dbo.fnGetSimpleDvAsInt(180020,@MatterID),0) /*AccountID for One Off Payment*/
		
		SELECT TOP 1 @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
					,@Amount = c.PaymentGross
					,@AccountID = ISNULL(@AccountID,c.AccountID)
		FROM CustomerPaymentSchedule c WITH (NOLOCK)
		INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID 
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = p.PurchasedProductID
		WHERE c.CustomerID = @CustomerID
		AND c.PaymentStatusID = 1
		AND p.PurchasedProductPaymentScheduleTypeID IN  (6,7,8) /*MTA Adjustment | Cancellation Adjustment | Reinstatement Adjustment*/
		AND pp.ObjectID = @MatterID 
		ORDER BY c.CustomerPaymentScheduleID DESC

		UPDATE cps
		SET AccountID = @AccountID
		FROM CustomerPaymentSchedule cps 
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE ppps
		SET AccountID = @AccountID
		FROM PurchasedProductPaymentSchedule ppps 
		WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		SELECT @PaymentTypeID = 1,
			   @ClientPaymentGatewayID = 7
		SELECT	 @ActualCollectionDate = cps.ActualCollectionDate
				,@PaymentGross = cps.PaymentGross
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		SELECT	 @FirstName		= cu.FirstName
				,@LastName		= cu.LastName
				,@Address1		= cu.Address1
				,@Address2		= cu.Address2
				,@AddressTown	= cu.Town
				,@AddressCounty = cu.County
				,@Postcode		= cu.PostCode
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID 

		EXEC CardTransaction__PrepareThirdPartyFields @MatterID, @WhoCreated, 'Adjustment Payment', @PaymentGross, @FirstName, @LastName, @Address1, @Address2, @AddressTown, @AddressCounty, 'GB', @Postcode, @CustomerPaymentScheduleID, @LeadEventID
		EXEC IncomingPayment__PrepareThirdPartyFields @MatterID, @WhoCreated, 69931 /*PaymentTypeID = "Credit Card"*/, 'Adjustment Payment', @LeadEventID, @PaymentNet, @PaymentTax, @PaymentGross, @CustomerPaymentScheduleID, 1, @ActualCollectionDate, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @LeadEventID, NULL, NULL, NULL, @LeadEventID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_158817] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_MidTermAdjustment_158817] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_158817] TO [sp_executeall]
GO
