SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventChoice table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventChoiceID],
					[EventTypeID],
					[Description],
					[NextEventTypeID],
					[ClientID],
					[LeadTypeID],
					[ThreadNumber],
					[EscalationEvent],
					[Field],
					[LogicalOperator],
					[Value1],
					[Value2],
					[SqlClauseForInclusion],
					[Weighting],
					[ValueTypeID]
				FROM
					[dbo].[EventChoice] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_GetByClientID] TO [sp_executeall]
GO
