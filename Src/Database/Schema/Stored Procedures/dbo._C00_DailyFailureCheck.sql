SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-20
-- Description:	Check for yesterday's failures and give the project team a heads-up in case of errors
-- 2018-10-10	AHOD Updated for C600 and added @DateRange
-- 2019-11-05	CPS for JIRA LPC-76  | Include AutomatedEventQueue errors
-- 2020-01-13	CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-12-15	AAD Restrict ScriptRunner results to current environment
-- 2021-01-18 CPS for TRUP | PoC for including data integrity checks within the daily file (pseudo-regression test)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DailyFailureCheck]
(
	@DateRange VARCHAR(50) = 'yesterday'
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FromDate DATE, @ToDate DATE, @GLID INT, @Text VARCHAR(2000)
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	DECLARE @Environment VARCHAR(50) = db_name() /* 2020-12-15 AAD */
	
	IF @DateRange NOT IN ('yesterday','today','last month','this month') 
		SELECT @DateRange = 'yesterday'
		
	-- Monthly
	IF @DateRange = 'this month'
	BEGIN
		IF DATEPART(DAY,dbo.fn_GetDate_Local()) = 1 
		BEGIN -- if it is the first of the month, then the date range is the whole of last month
			SELECT @FromDate=DATEADD(mm, DATEDIFF(mm, 0, dbo.fn_GetDate_Local()) - 1, 0)
		END
		ELSE
		BEGIN -- otherwise it is from the 1st of this month to yesterday
			SELECT @FromDate=DATEADD(mm, DATEDIFF(mm, 0, dbo.fn_GetDate_Local()), 0)
		END	
	END
	ELSE IF @DateRange = 'last month'
		SELECT @FromDate=DATEADD(mm, DATEDIFF(mm, 0, dbo.fn_GetDate_Local()) - 1, 0) -- last month
		
	---- Daily
	ELSE IF @DateRange = 'yesterday'
		SELECT @FromDate=CAST(DATEADD(DAY,-1,dbo.fn_GetDate_Local()) AS DATE) -- yesterday
	ELSE IF @DateRange = 'today'
		SELECT @FromDate=CAST(dbo.fn_GetDate_Local() AS DATE) -- today

	SELECT 'ZipErrors' [Type],*
	FROM Logs l WITH ( NOLOCK ) 
	WHERE l.TypeOfLogEntry = 'Error'
	AND l.MethodName = 'Zip'
	AND l.LogDateTime >= @FromDate

	SELECT 'BatchJobErrors' [Type]
			,at.TaskID
			,at.Taskname
			,COUNT(atr.AutomatedTaskResultID)	[Failures]
			,MIN(atr.RecordCount)				[MinRecordCount]
			,MAX(atr.RecordCount)				[MaxRecordCount]
			,MIN(atr.RunDate)					[FirstRun]
			,MAX(atr.RunDate)					[LatestRun]
			,et.EventTypeID
			,et.EventTypeName
			,LEFT(atr.Description,100)			[Description]
	FROM AutomatedTaskResult atr WITH ( NOLOCK ) 
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) ON at.TaskID = atr.TaskID 
	LEFT JOIN dbo.AutomatedTaskParam atp WITH ( NOLOCK ) on atp.TaskID = at.TaskID AND atp.ParamName = 'LEADEVENT_TO_ADD'
	LEFT JOIN dbo.EventType et WITH ( NOLOCK ) on et.EventTypeID = atp.ParamValue
	WHERE atr.Complete = 0 
	AND atr.RunDate >= @FromDate
	AND NOT EXISTS ( SELECT *
					 FROM dbo.AutomatedTaskResult s_atr WITH ( NOLOCK ) 
					 WHERE s_atr.TaskID = at.TaskID
					 AND s_atr.Complete = 1 
					 AND s_atr.RunDate > atr.RunDate )
	AND atr.Description NOT LIKE '%Object reference not set%'
	GROUP BY at.TaskID, at.Taskname, et.EventTypeID, et.EventTypeName, LEFT(atr.Description,100)
	ORDER BY [Failures] DESC

	SELECT 'BillingErrors' [Type],*
	FROM Logs l WITH ( NOLOCK ) 
	WHERE l.TypeOfLogEntry = 'Error'
	AND l.LogEntry like '%billing%'
	AND l.LogDateTime >= @FromDate
	AND l.LogEntry NOT LIKE '%Object reference not set%'
	
	SELECT 'ScriprunnerErrors' AS [Type]
	, r.AutomatedScriptResultID
	, s.ScriptID, ss.AutomatedScriptScheduleID AS ScheduleID, s.ScriptName, ss.Name ScheduleName, ss.ServerIP
	, r.RunDate, r.DurationSeconds, r.RecordCount, LEFT(r.Description,1000) [Description]
	FROM [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptResult r WITH (NOLOCK) 
	INNER JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptSchedule ss WITH (NOLOCK) ON r.AutomatedScriptScheduleID = ss.AutomatedScriptScheduleID
	INNER JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScript s WITH (NOLOCK) ON r.ScriptID = s.ScriptID
	INNER JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptParam p WITH (NOLOCK) ON s.ScriptID = p.ScriptID AND p.ParamName = 'dbname'
	LEFT JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptParamValue pv WITH (NOLOCK) ON ss.AutomatedScriptScheduleID = pv.AutomatedScriptScheduleID AND p.AutomatedScriptParamID = pv.AutomatedScriptParamID
	WHERE r.ClientID = @ClientID
	AND r.Complete = 0
	AND r.RunDate >= @FromDate
	AND ISNULL(pv.ParamValue, p.DefaultValue) = @Environment /* 2020-12-15 AAD */
	ORDER BY r.RunDate

	SELECT 'Overdue Scriptrunner Tasks' AS [Type]
		,s.ScriptID, ss.AutomatedScriptScheduleID AS ScheduleID, s.ScriptName, ss.Name ScheduleName, ss.ServerIP, ss.NextRunDateTime, ss.LockDateTime [Lock Date]
	FROM [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptSchedule ss WITH (NOLOCK)
	INNER JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScript s WITH (NOLOCK) ON ss.ScriptID = s.ScriptID
	INNER JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptParam p WITH (NOLOCK) ON s.ScriptID = p.ScriptID AND p.ParamName = 'dbname'
	LEFT JOIN [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScriptParamValue pv WITH (NOLOCK) ON ss.AutomatedScriptScheduleID = pv.AutomatedScriptScheduleID AND p.AutomatedScriptParamID = pv.AutomatedScriptParamID
	WHERE ss.ClientID = @ClientID
	AND ss.Enabled = 1
	AND ss.NextRunDateTime < @FromDate
	AND ISNULL(pv.ParamValue, p.DefaultValue) = @Environment /* 2020-12-15 AAD */
	ORDER BY ss.NextRunDateTime

	SELECT 'Overdue Batch Jobs' [Type], *
	FROM AutomatedTask at WITH ( NOLOCK ) 
	WHERE at.NextRunDateTime < @FromDate
	AND at.Enabled = 1
	ORDER BY at.NextRunDateTime

	SELECT 'AutomatedEventQueue' [Type], et.EventTypeID, et.EventTypeName, est.EventSubtypeName, sae.PostUpdateSql, eq.ErrorMessage, COUNT(eq.AutomatedEventQueueID) [Failures], MAX(m.MatterID) [ExampleMatterID]
	FROM AutomatedEventQueue eq WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = eq.CaseID
	INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = eq.AutomatedEventTypeID
	INNER JOIN dbo.EventSubtype est WITH (NOLOCK) on est.EventSubtypeID = et.EventSubtypeID
	LEFT JOIN dbo.EventTypeSql sae WITH (NOLOCK) on sae.EventTypeID = et.EventTypeID
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = eq.CustomerID AND cu.Test = 0
	WHERE eq.ErrorDateTime >= @FromDate
	GROUP BY et.EventTypeID, et.EventTypeName, est.EventSubtypeName, sae.PostUpdateSql, eq.ErrorMessage
	ORDER BY [Failures] DESC

	-- 2021-01-18 CPS for TRUP | PoC for including data integrity checks within the daily file (pseudo-regression test)
	SELECT 'Data Integrity - Sum of PPPS does not equal CPS' [IntegrityCheck Type], COUNT(*) [AffectedRecords], cps.CustomerID [CustomerID], 'EXEC AskBill ' + ISNULL(CONVERT(VARCHAR,cps.CustomerID),'NULL') [Notes]
	FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	LEFT JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) on ppps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = cps.CustomerID AND cu.Test = 0
	GROUP BY cps.CustomerPaymentScheduleID, cps.PaymentGross, cps.CustomerID
	HAVING SUM(ppps.PaymentGross) <> cps.PaymentGross
		UNION ALL
	SELECT 'Data Integrity - "New" Collections more than 30 days old' [IntegrityCheck Type], COUNT(*) [AffectedRecords], cps.CustomerID [CustomerID], 'EXEC AskBill ' + ISNULL(CONVERT(VARCHAR,cps.CustomerID),'NULL') [Notes]
	FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = cps.CustomerID AND cu.Test = 0
	WHERE cps.PaymentStatusID = 1
	AND cps.ActualCollectionDate < DATEADD(DAY,-30,@FromDate)
	GROUP BY cps.CustomerID
	HAVING COUNT(*) <> 0
		UNION ALL
	SELECT 'Data Integrity - ClientAccountID Mismatch vs Affinity' [IntegrityCheck Type], COUNT(*) [AffectedRecords], cdv.CustomerID [CustomerID], 'EXEC AskBill ' + ISNULL(CONVERT(VARCHAR,cdv.CustomerID),'NULL') + ' -- Checking Account, PurchasedProduct, CustomerPaymentSchedule, PurchasedProductPaymentSchedule' [Notes]
	FROM CustomerDetailValues cdv WITH (NOLOCK) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = cdv.CustomerID AND cu.Test = 0
	INNER JOIN ResourceListDetailValues rdvColl WITH (NOLOCK) on rdvColl.ResourceListID = cdv.ValueInt and rdvColl.DetailFieldID = 179949 /*Collections AccountID*/
	INNER JOIN ResourceListDetailValues rdvClam WITH (NOLOCK) on rdvClam.ResourceListID = cdv.ValueInt and rdvClam.DetailFieldID = 179950 /*Claim Payments AccountID*/
	WHERE cdv.DetailFieldID = 170144 /*Affinity Details*/
	AND EXISTS ( SELECT * FROM 
				( 
				 SELECT ac.ClientAccountID
	             FROM Account ac WITH (NOLOCK) 
				 WHERE ac.CustomerID = cdv.CustomerID 
					UNION ALL
				 SELECT pp.ClientAccountID
				 FROM PurchasedProduct pp WITH (NOLOCK) 
				 WHERE pp.CustomerID = cdv.CustomerID
					UNION ALL
				 SELECT cps.ClientAccountID
				 FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
				 WHERE cps.CustomerID = cdv.CustomerID
					UNION ALL
				 SELECT ppps.ClientAccountID
				 FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
				 WHERE ppps.CustomerID = cdv.CustomerID
			   ) billing
			  WHERE billing.ClientAccountID NOT IN ( rdvColl.ValueInt,rdvClam.ValueInt )
			  )
	GROUP BY cdv.CustomerID
	HAVING COUNT(*) <> 0

END


















GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DailyFailureCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DailyFailureCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DailyFailureCheck] TO [sp_executeall]
GO
