SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-06
-- Description:	SQL Before Table Row Delete
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SBTD]
	@TableRowID INT, 
	@ClientPersonnelID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@TableDFID INT,
			@LeadID INT,
			@MatterID INT,
			@NewTableRowID INT,
			@Usersname VARCHAR(201)

	SELECT @TableDFID = tr.DetailFieldID, @LeadID = tr.LeadID, @MatterID = tr.MatterID
	FROM TableRows tr WITH (NOLOCK)
	WHERE tr.TableRowID = @TableRowID
	
	DECLARE
		@ControlID INT,
		@FolderToUse VARCHAR(100),
		@LinkID INT,
		@ObjectName VARCHAR(500),
		@ObjectTypeID INT,
		@ReportFileExt VARCHAR(10)

	SELECT @TableDFID=DetailFieldID,@ClientID=ClientID FROM TableRows tr WITH (NOLOCK) WHERE TableRowID=@TableRowID
	
	-- A deletion from Document Autozip Table Configuration
	if @TableDFID=170149
	BEGIN

		-- just delete the document link (leave the folder control file in place since this may be used by many documents) 
		DELETE FROM DocumentZipFtpControlLink WHERE LinkNotes LIKE CONVERT(VARCHAR(30),@TableRowID) + ' %'
													AND ClientID=@ClientID

	END
		
	-- A deletion from Report Autozip Table Configuration
	if @TableDFID=170167
	BEGIN

		-- update
		SELECT @LinkID=atcc.AutomatedTaskCommandControlID
		FROM AutomatedTaskCommandControl atcc WITH (NOLOCK) 
		WHERE atcc.JobNotes LIKE CONVERT(VARCHAR(30),@TableRowID) + ' %' AND ClientID=@ClientID

		DELETE FROM AutomatedTaskCommandLine WHERE ClientID=@ClientID AND AutomatedTaskCommandControlID=@LinkID
		DELETE FROM AutomatedTaskCommandControl WHERE ClientID=@ClientID AND AutomatedTaskCommandControlID=@LinkID
		
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SBTD] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SBTD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SBTD] TO [sp_executeall]
GO
