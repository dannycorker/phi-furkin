SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DashboardItem table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItem_Find]
(

	@SearchUsingOR bit   = null ,

	@DashboardItemID int   = null ,

	@DashboardID int   = null ,

	@DashboardItemTypeID int   = null ,

	@ItemID int   = null ,

	@ClientID int   = null ,

	@LayoutPosition int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DashboardItemID]
	, [DashboardID]
	, [DashboardItemTypeID]
	, [ItemID]
	, [ClientID]
	, [LayoutPosition]
    FROM
	[dbo].[DashboardItem] WITH (NOLOCK) 
    WHERE 
	 ([DashboardItemID] = @DashboardItemID OR @DashboardItemID IS NULL)
	AND ([DashboardID] = @DashboardID OR @DashboardID IS NULL)
	AND ([DashboardItemTypeID] = @DashboardItemTypeID OR @DashboardItemTypeID IS NULL)
	AND ([ItemID] = @ItemID OR @ItemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LayoutPosition] = @LayoutPosition OR @LayoutPosition IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DashboardItemID]
	, [DashboardID]
	, [DashboardItemTypeID]
	, [ItemID]
	, [ClientID]
	, [LayoutPosition]
    FROM
	[dbo].[DashboardItem] WITH (NOLOCK) 
    WHERE 
	 ([DashboardItemID] = @DashboardItemID AND @DashboardItemID is not null)
	OR ([DashboardID] = @DashboardID AND @DashboardID is not null)
	OR ([DashboardItemTypeID] = @DashboardItemTypeID AND @DashboardItemTypeID is not null)
	OR ([ItemID] = @ItemID AND @ItemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LayoutPosition] = @LayoutPosition AND @LayoutPosition is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItem_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItem_Find] TO [sp_executeall]
GO
