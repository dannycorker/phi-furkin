SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserRightsDynamic table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamic_GetByRightID]
(

	@RightID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[UserRightsDynamicID],
					[ClientPersonnelID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[UserRightsDynamic] WITH (NOLOCK) 
				WHERE
					[RightID] = @RightID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_GetByRightID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamic_GetByRightID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_GetByRightID] TO [sp_executeall]
GO
