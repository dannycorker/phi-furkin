SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryCriteriaSet table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSet_Update]
(

	@SqlQueryCriteriaSetID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SetName varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryCriteriaSet]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[SetName] = @SetName
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[SourceID] = @SourceID
				WHERE
[SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSet_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Update] TO [sp_executeall]
GO
