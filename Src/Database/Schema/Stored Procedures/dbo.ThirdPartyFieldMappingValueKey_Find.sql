SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyFieldMappingValueKey table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyFieldMappingValueKeyID int   = null ,

	@ClientID int   = null ,

	@FieldMappingID int   = null ,

	@DetailValue varchar (2000)  = null ,

	@LookupListItemID int   = null ,

	@ThirdPartyKey varchar (50)  = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyFieldMappingValueKeyID]
	, [ClientID]
	, [FieldMappingID]
	, [DetailValue]
	, [LookupListItemID]
	, [ThirdPartyKey]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyFieldMappingValueKey] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldMappingValueKeyID] = @ThirdPartyFieldMappingValueKeyID OR @ThirdPartyFieldMappingValueKeyID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FieldMappingID] = @FieldMappingID OR @FieldMappingID IS NULL)
	AND ([DetailValue] = @DetailValue OR @DetailValue IS NULL)
	AND ([LookupListItemID] = @LookupListItemID OR @LookupListItemID IS NULL)
	AND ([ThirdPartyKey] = @ThirdPartyKey OR @ThirdPartyKey IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyFieldMappingValueKeyID]
	, [ClientID]
	, [FieldMappingID]
	, [DetailValue]
	, [LookupListItemID]
	, [ThirdPartyKey]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyFieldMappingValueKey] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldMappingValueKeyID] = @ThirdPartyFieldMappingValueKeyID AND @ThirdPartyFieldMappingValueKeyID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FieldMappingID] = @FieldMappingID AND @FieldMappingID is not null)
	OR ([DetailValue] = @DetailValue AND @DetailValue is not null)
	OR ([LookupListItemID] = @LookupListItemID AND @LookupListItemID is not null)
	OR ([ThirdPartyKey] = @ThirdPartyKey AND @ThirdPartyKey is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Find] TO [sp_executeall]
GO
