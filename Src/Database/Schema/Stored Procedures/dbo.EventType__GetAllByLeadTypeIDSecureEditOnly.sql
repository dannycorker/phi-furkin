SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------
-- Date Created: 29 May 2007

-- Created By:  Chris, Jim
-- Purpose: Select all in-process events for the current Lead Type, as long as the user has access to them
-- but this time, only return event types they can EDIT. Exclude Hold events
-- ACE 2013-07-23 Remove the AvailableManually restriction so that workflow automated events can be selected for followup by people.
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetAllByLeadTypeIDSecureEditOnly] 
(
	@LeadTypeID int,
	@UserID int	
)
AS
BEGIN

	SET ANSI_NULLS OFF
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

	SELECT et.*
	FROM
		dbo.fnEventTypeShared(@ClientID) et INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) f ON et.EventTypeID = f.objectid
	WHERE et.InProcess = 1  
		--EXISTS (SELECT [EventTypeID],[NextEventTypeID] FROM dbo.[EventChoice] ec WHERE (et.[EventTypeID] = ec.[EventTypeID] OR et.[EventTypeID] = ec.[NextEventTypeID]))
		AND et.LeadTypeID = @LeadTypeID
		AND [EventSubTypeID] NOT IN (7,8,9)
		AND f.rightid > 1
		AND (Enabled = 1)
	ORDER BY EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON

END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetAllByLeadTypeIDSecureEditOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetAllByLeadTypeIDSecureEditOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetAllByLeadTypeIDSecureEditOnly] TO [sp_executeall]
GO
