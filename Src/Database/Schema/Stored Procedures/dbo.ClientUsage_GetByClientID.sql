SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientUsage table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientUsage_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[ClientID],
					[Validity],
					[ValidityHash],
					[Usage],
					[UsageHash]
				FROM
					[dbo].[ClientUsage] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientUsage_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_GetByClientID] TO [sp_executeall]
GO
