SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.UpdateQuestionnaireFont    Script Date: 08/09/2006 12:22:42 ******/

CREATE PROCEDURE [dbo].[UpdateQuestionnaireFont]
@QuestionnaireFontID int,
@ClientQuestionnaireID int,
@PartNameID int, 
@FontFamily nvarchar(50),
@FontSize nvarchar(50),
@FontColour nvarchar(50),
@FontWeight nvarchar(50),
@FontAlignment nvarchar(50),
@ClientID int = null

as

Update QuestionnaireFonts

Set ClientQuestionnaireID = @ClientQuestionnaireID,
PartNameID = @PartNameID,
FontFamily = @FontFamily,
FontSize = @FontSize,
FontColour = @FontColour,
FontWeight = @FontWeight,
FontAlignment = @FontAlignment
where QuestionnaireFontID = @QuestionnaireFontID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateQuestionnaireFont] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateQuestionnaireFont] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateQuestionnaireFont] TO [sp_executeall]
GO
