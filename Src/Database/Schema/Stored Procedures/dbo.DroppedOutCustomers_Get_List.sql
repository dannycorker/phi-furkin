SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DroppedOutCustomers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_Get_List]

AS


				
				SELECT
					[CustomerID],
					[ClientID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[HomeTelephone],
					[MobileTelephone],
					[CompanyTelephone],
					[WorksTelephone],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[HasDownloaded],
					[DownloadedOn],
					[AquariumStatusID],
					[ClientStatusID],
					[Test],
					[CompanyName],
					[Occupation],
					[Employer],
					[ReminderEmailSentDate],
					[ReminderSmsSentDate],
					[CountryID]
				FROM
					[dbo].[DroppedOutCustomers] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Get_List] TO [sp_executeall]
GO
