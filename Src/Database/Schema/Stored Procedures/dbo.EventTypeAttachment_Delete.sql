SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeAttachment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_Delete]
(

	@AttachmentID int   
)
AS


				DELETE FROM [dbo].[EventTypeAttachment] WITH (ROWLOCK) 
				WHERE
					[AttachmentID] = @AttachmentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Delete] TO [sp_executeall]
GO
