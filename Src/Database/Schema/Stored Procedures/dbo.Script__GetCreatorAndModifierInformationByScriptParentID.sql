SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 27-04-2015
-- Description:	Get Script and creator/modifier information for the children of the given script ID
-- =============================================
CREATE PROCEDURE [dbo].[Script__GetCreatorAndModifierInformationByScriptParentID]

	@ScriptParentID INT

AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT s.*, 
		 cpCreator.FirstName CreatorFirstName, cpCreator.MiddleName CreatorMiddleName, cpCreator.LastName CreatorLastName,
		 cpModifier.FirstName ModifierFirstName, cpModifier.MiddleName ModifierMiddleName, cpModifier.LastName ModifierLastName
	FROM Script s WITH (NOLOCK) 
	LEFT JOIN ClientPersonnel cpCreator WITH (NOLOCK) on cpCreator.ClientPersonnelID=s.WhoCreated
	LEFT JOIN ClientPersonnel cpModifier WITH (NOLOCK) on cpModifier.ClientPersonnelID=s.WhoModified
	WHERE s.ScriptParentID=@ScriptParentID ORDER BY s.WhenCreated DESC
    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetCreatorAndModifierInformationByScriptParentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetCreatorAndModifierInformationByScriptParentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetCreatorAndModifierInformationByScriptParentID] TO [sp_executeall]
GO
