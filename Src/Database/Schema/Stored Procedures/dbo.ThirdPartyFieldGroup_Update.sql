SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_Update]
(

	@ThirdPartyFieldGroupID int   ,

	@ThirdPartySystemID int   ,

	@GroupName varchar (50)  ,

	@GroupDescription varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyFieldGroup]
				SET
					[ThirdPartySystemID] = @ThirdPartySystemID
					,[GroupName] = @GroupName
					,[GroupDescription] = @GroupDescription
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Update] TO [sp_executeall]
GO
