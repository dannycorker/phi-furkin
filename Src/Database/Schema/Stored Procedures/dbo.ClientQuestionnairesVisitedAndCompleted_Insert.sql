SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientQuestionnairesVisitedAndCompleted table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedAndCompleted_Insert]
(

	@ClientQuestionnairesVisitedAndCompletedID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@VisitedDate datetime   ,

	@IPAddress varchar (50)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[ClientQuestionnairesVisitedAndCompleted]
					(
					[ClientQuestionnaireID]
					,[VisitedDate]
					,[IPAddress]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@VisitedDate
					,@IPAddress
					,@ClientID
					)
				-- Get the identity value
				SET @ClientQuestionnairesVisitedAndCompletedID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_Insert] TO [sp_executeall]
GO
