SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-14
-- Description:	Get Paymnent Types
-- =============================================
CREATE PROCEDURE [dbo].[PaymentStatus__GetAll]

AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM PaymentStatus WITH (NOLOCK)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentStatus__GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentStatus__GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentStatus__GetAll] TO [sp_executeall]
GO
