SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Bill table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill_Find]
(

	@SearchUsingOR bit   = null ,

	@BillID int   = null ,

	@ClientID int   = null ,

	@Reference varchar (100)  = null ,

	@Description varchar (2000)  = null ,

	@CreatedDate datetime   = null ,

	@DiaryAppointmentID int   = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@BillStatusID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [BillID]
	, [ClientID]
	, [Reference]
	, [Description]
	, [CreatedDate]
	, [DiaryAppointmentID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [BillStatusID]
    FROM
	[dbo].[Bill] WITH (NOLOCK) 
    WHERE 
	 ([BillID] = @BillID OR @BillID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Reference] = @Reference OR @Reference IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
	AND ([DiaryAppointmentID] = @DiaryAppointmentID OR @DiaryAppointmentID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([BillStatusID] = @BillStatusID OR @BillStatusID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [BillID]
	, [ClientID]
	, [Reference]
	, [Description]
	, [CreatedDate]
	, [DiaryAppointmentID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [BillStatusID]
    FROM
	[dbo].[Bill] WITH (NOLOCK) 
    WHERE 
	 ([BillID] = @BillID AND @BillID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Reference] = @Reference AND @Reference is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([CreatedDate] = @CreatedDate AND @CreatedDate is not null)
	OR ([DiaryAppointmentID] = @DiaryAppointmentID AND @DiaryAppointmentID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([BillStatusID] = @BillStatusID AND @BillStatusID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Find] TO [sp_executeall]
GO
