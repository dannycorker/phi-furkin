SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Robin Hall
-- Create date: 2015-02-05
-- Description:	Generic proc to return table of stuff for a custom detail field.
-- Designed to be useful for all clients but originally implemented for Sterling - Zen#30201
-- 2020-01-13 CPS for JIRA LPC-356 | Removed hard-coded @ClientID 600 section that was encapsulating the second stored proc call.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CustomControlData]
	@DetailFieldID INT,
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL
AS
BEGIN

	EXEC _C600_CustomControlData @DetailFieldID, @CustomerID, @LeadID, @CaseID, @MatterID	

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomControlData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CustomControlData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CustomControlData] TO [sp_executeall]
GO
