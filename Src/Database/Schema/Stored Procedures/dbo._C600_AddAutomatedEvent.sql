SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-06
-- Description:	Adds entry to the automated event queue
-- =============================================
CREATE PROCEDURE [dbo].[_C600_AddAutomatedEvent] 
(
	@MatterID INT, 
	@AutomatedEventTypeID INT,
	@ThreadToFollowUp INT,
	@WhoCreated INT	= NULL
)

AS
BEGIN

	SET NOCOUNT ON;

--declare @MatterID INT = 50022137, 
--	@AutomatedEventTypeID INT = 123,
--	@ThreadToFollowUp INT = 2,
--	@WhoCreated INT = 44412
	
	DECLARE @LeadEventID INT
	
	SELECT TOP(1) @LeadEventID=le.LeadEventID,
		@WhoCreated=CASE WHEN @WhoCreated IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @WhoCreated END	
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Cases ca WITH (NOLOCK) ON m.CaseID=ca.CaseID
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND ca.CaseID=le.CaseID AND le.WhenFollowedUp IS NULL
	INNER JOIN LeadEventThreadCompletion letc WITH (NOLOCK) ON letc.FromLeadEventID=le.LeadEventID 
		AND ( @ThreadToFollowUp <= 0
		      OR  ( letc.ThreadNumberRequired = @ThreadToFollowUp AND letc.ToLeadEventID IS NULL ) )
	WHERE m.MatterID=@MatterID

	IF @LeadEventID IS NOT NULL
		EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @AutomatedEventTypeID, @WhoCreated, @ThreadToFollowUp
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_AddAutomatedEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_AddAutomatedEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_AddAutomatedEvent] TO [sp_executeall]
GO
