SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 02-Nov-2015
-- Description:	Proc to move a case to new lead and customer
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MoveCaseToNewLead]
(
@CaseIDToMove int,
@AquariumClientID Int
)


AS
BEGIN


	DECLARE @LeadTypeIDTOCreate INT,
			@ClientID			INT,
			@NewCustomerID      INT,
			@NewLeadID			INT,
			@OldLeadID			INT,
			@LeadID				INT,
			@LogEntry			VARCHAR (100)
			
			

	/*
		Sys move Case to new Lead/Customer.
	*/

	SELECT @LeadTypeIDTOCreate=Lead.LeadTypeID, @ClientID = Lead.ClientID, @LeadID = Lead.LeadID
	FROM Lead
	INNER JOIN Cases ON cases.LeadID = Lead.LeadID AND Cases.caseID = @CaseIDToMove

	IF EXISTS (SELECT * FROM Cases c WITH (NOLOCK) where c.LeadID = @LeadID and c.CaseID <> @CaseIDToMove)
	BEGIN
	
		IF @AquariumClientID = @ClientID
		BEGIN

			INSERT INTO [dbo].[Customers]([ClientID],[TitleID],[IsBusiness],[FirstName],[LastName],[DayTimeTelephoneNumberVerifiedAndValid],[HomeTelephoneVerifiedAndValid],[MobileTelephoneVerifiedAndValid],[CompanyTelephone],[CompanyTelephoneVerifiedAndValid], [WorksTelephoneVerifiedAndValid], [Website],[HasDownloaded],[DownloadedOn],[AquariumStatusID],[ClientStatusID],[Test],[PhoneNumbersVerifiedOn],[DoNotEmail],[DoNotSellToThirdParty],[AgreedToTermsAndConditions],[DateOfBirth],[DefaultContactID],[DefaultOfficeID])
			VALUES (@AquariumClientID,0,0,@CaseIDToMove,@CaseIDToMove,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL)

			SELECT @NewCustomerID = SCOPE_IDENTITY()

			INSERT INTO [dbo].[Lead]([ClientID],[LeadRef],[CustomerID],[LeadTypeID],[AquariumStatusID],[ClientStatusID],[BrandNew],[Assigned],[AssignedTo],[AssignedBy],[AssignedDate],[RecalculateEquations],[Password],[Salt],[WhenCreated])
			VALUES(@AquariumClientID, '',@NewCustomerID,@LeadTypeIDTOCreate,2,NULL,0,0,NULL,NULL,NULL,0,NULL,NULL,dbo.fn_GetDate_Local())

			SELECT @NewLeadID = SCOPE_IDENTITY()

			SELECT @OldLeadID = LeadID
			FROM Cases
			WHERE CaseID = @CaseIDToMove

			UPDATE Cases
			SET LeadID = @NewLeadID, CaseNum = 1, CaseRef = 'Case 1'
			WHERE CaseID = @CaseIDToMove

			UPDATE Matter
			SET LeadID = @NewLeadID, CustomerID = @NewCustomerID
			WHERE CaseID = @CaseIDToMove

			UPDATE MatterDetailValues
			SET MatterDetailValues.LeadID = @NewLeadID
			FROM MatterDetailValues
			INNER JOIN Matter ON Matter.MatterID = MatterDetailValues.MatterID AND Matter.CaseID = @CaseIDToMove

			UPDATE TableRows
			SET TableRows.LeadID = @NewLeadID
			FROM TableRows
			INNER JOIN Matter ON Matter.MatterID = TableRows.MatterID AND Matter.CaseID = @CaseIDToMove

			UPDATE TableDetailValues
			SET TableDetailValues.LeadID = @NewLeadID
			FROM TableDetailValues
			INNER JOIN Matter ON Matter.MatterID = TableDetailValues.MatterID AND Matter.CaseID = @CaseIDToMove

			UPDATE LeadEvent
			SET LeadID = @NewLeadID
			WHERE CaseID = @CaseIDToMove

			UPDATE LeadDocument
			SET LeadID = @NewLeadID
			FROM LeadDocument
			INNER JOIN LeadEvent ON LeadEvent.LeadDocumentID = LeadDocument.LeadDocumentID AND LeadEvent.CaseID = @CaseIDToMove

			UPDATE LeadEventThreadCompletion
			SET LeadID = @NewLeadID
			WHERE CaseID = @CaseIDToMove

			UPDATE CaseTransferMapping
			SET LeadID = @NewLeadID, CustomerID = @NewCustomerID
			WHERE CaseID = @CaseIDToMove

			UPDATE DocumentQueue
			SET LeadID = @NewLeadID, CustomerID = @NewCustomerID
			WHERE CaseID = @CaseIDToMove

			UPDATE DetailValueHistory
			SET DetailValueHistory.LeadID = @NewLeadID
			FROM DetailValueHistory
			INNER JOIN Matter ON Matter.MatterID = DetailValueHistory.MatterID AND Matter.CaseID = @CaseIDToMove

		END

		DELETE FROM LeadDetailValues
		WHERE LeadID = @LeadID
		AND DetailFieldID = 37380
	END
	ELSE 
	BEGIN
	
		SELECT @LogEntry = '<BR><BR><font color="red">Error:  You are attempting to remove the last case on this lead. Stop it. CaseID ' + ISNULL(CONVERT(VARCHAR,@CaseIDToMove),'NULL') + '</font>'
		RAISERROR( @LogEntry, 16, 1 )
		RETURN
	
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MoveCaseToNewLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MoveCaseToNewLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MoveCaseToNewLead] TO [sp_executeall]
GO
