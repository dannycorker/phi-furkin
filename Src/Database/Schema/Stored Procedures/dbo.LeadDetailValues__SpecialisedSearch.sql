SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 21 September2009
-- Description:	Does a Like Search for lead detail values
-- =============================================
CREATE PROCEDURE [dbo].[LeadDetailValues__SpecialisedSearch]

@ClientID varchar(12),
@DetailFieldID varchar(12),
@FindInformation varchar(max),
@StartRow int = null,
@EndRow int = null

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @query VARCHAR(MAX)
	
	SET @query = '
	SELECT * FROM
	(	
	SELECT ROW_NUMBER() OVER (order by Customers.CustomerID) as RowNumber,dbo.Titles.Title, 
	dbo.Customers.*, dbo.Lead.LeadID, dbo.LeadDetailValues.DetailValue, dbo.Cases.CaseID
	FROM dbo.LeadDetailValues 
	INNER JOIN dbo.Lead ON dbo.LeadDetailValues.LeadID = dbo.Lead.LeadID 
	INNER JOIN dbo.Customers ON dbo.Lead.CustomerID = dbo.Customers.CustomerID 
	INNER JOIN dbo.Titles ON dbo.Customers.TitleID = dbo.Titles.TitleID 
	INNER JOIN dbo.Cases ON dbo.Cases.LeadID = dbo.Lead.LeadID
	where dbo.customers.ClientID=' + @ClientID + ' and dbo.LeadDetailValues.DetailFieldID=' + @DetailFieldID + ' '

	IF(@FindInformation is not null)
	BEGIN
		SELECT @query = @query + ' and (' + @FindInformation + ')'		
	END
	SELECT @query = @query + ' ) As Results'

	
	IF (@StartRow is not null AND @EndRow is not null)
	BEGIN
		SELECT @query = @query + ' WHERE RowNumber Between ' + CONVERT(Varchar(10), @StartRow) + ' and ' + CONVERT(Varchar(10), @EndRow)
	END

	
	EXECUTE(@query)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__SpecialisedSearch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues__SpecialisedSearch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues__SpecialisedSearch] TO [sp_executeall]
GO
