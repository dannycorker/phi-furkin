SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM SOFTWARE
-- Create date: 2020-02-03
-- Description:	Sql After Event - TESTING: Rollback all dates
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Testing_156739]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			,@EventComments					VARCHAR(2000) = ''
			/* Specific to procedure */
			,@ValueInt						INT
			,@PolicyOneVisionID				INT
			,@WellnessOneVisionID			INT
			,@ExamFeeOneVisionID			INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	/* TESTING - Rollback all dates */
	IF @EventTypeID IN (156739)
	BEGIN
		
		/*CPS 2017-09-18 update the LeadEvent comments*/
		SELECT @ValueInt = dbo.fnGetSimpleDvAsInt(175431,@MatterID) /*Amount to rollback all dates (0 = 1 month; > 0 = number days)*/
		SELECT @EventComments += 'Rolled back by ' + CASE WHEN @ValueInt = 0 THEN '1 month.' ELSE ISNULL(CONVERT(VARCHAR,@ValueInt),'NULL') + ' days.' END + CHAR(13)+CHAR(10)
						
		EXEC _C600_Testing_RollBackAllDates @MatterID
		
		-- 2020-02-16 CPS for JIRA AAG-108 | Allow completing of billing at the same time as rolling back.
		IF dbo.fnGetSimpleDvAsInt(179951,@MatterID) = 1 /*Complete Billing*/
		BEGIN
			
			/*GPR 2021-03-10 for FURKIN-390*/
			IF @ClientID <> 607
			BEGIN
				EXEC _SAE_Testing_158770 @LeadEventID
			END
			ELSE
			BEGIN
					/*FURKIN*/		 
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 868 -- Collections Furkin (DEV)
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 869 -- Refunds Furkin (DEV)
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 876 -- Collections Furkin (QA)
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 877 -- Refunds Furkin (QA)

					/*PHI DIRECT*/		 
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 870 -- Collections PHI (DEV)
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 871 -- Refunds PHI (DEV)
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 179, 878 -- Collections PHI (QA)
					EXEC [926125-SQLCLU12\SQL12].[AquariusMaster].dbo.AutomatedScript__RunNow 879, 871 -- Refunds PHI (QA)
			END

			SELECT @EventComments += 'Billing Completed.' + CHAR(13)+CHAR(10)
		END
		
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

		/*2020-10-09 ALM SDPRU-84*/
		IF (dbo.fnGetSimpleDvAsInt (313931, @LeadID) = 5144) OR (dbo.fnGetSimpleDvAsInt (314235, @MatterID) > 0)
		BEGIN 
			SELECT @ErrorMessage = '<br /><br /><font color="red">This policy has add ons, please contact AQ to rollback dates.</font>'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END

		/*2020-07-28 ALM AAG-968*/
		SELECT @PolicyOneVisionID = ov.OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

		SELECT @WellnessOneVisionID = ov.OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness'

		SELECT @ExamFeeOneVisionID = ov.OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'

		IF @PolicyOneVisionID IS NOT NULL 
		BEGIN
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'RollbackPolicy', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID

			/*2020-08-06 ALM Adding delay to stop the second call interferring with the first*/
			WAITFOR DELAY '00:00:05'

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'RollbackPolicyTerms', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID
		END

		IF @WellnessOneVisionID IS NOT NULL
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'RollbackWellness', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID
		END

		IF @ExamFeeOneVisionID IS NOT NULL
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'RollbackExamFee', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID
		END

	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Testing_156739] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Testing_156739] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Testing_156739] TO [sp_executeall]
GO
