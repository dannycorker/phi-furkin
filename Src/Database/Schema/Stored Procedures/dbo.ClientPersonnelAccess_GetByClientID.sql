SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelAccess table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPersonnelAccessID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[LeadID],
					[CaseID],
					[AccessRuleID],
					[SourceLeadEventID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[AccessLevel]
				FROM
					[dbo].[ClientPersonnelAccess] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_GetByClientID] TO [sp_executeall]
GO
