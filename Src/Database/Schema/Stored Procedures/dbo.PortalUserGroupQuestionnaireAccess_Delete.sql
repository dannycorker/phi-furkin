SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalUserGroupQuestionnaireAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_Delete]
(

	@PortalUserGroupQuestionnaireAccessID int   
)
AS


				DELETE FROM [dbo].[PortalUserGroupQuestionnaireAccess] WITH (ROWLOCK) 
				WHERE
					[PortalUserGroupQuestionnaireAccessID] = @PortalUserGroupQuestionnaireAccessID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Delete] TO [sp_executeall]
GO
