SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuestionnaireFonts table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_Find]
(

	@SearchUsingOR bit   = null ,

	@QuestionnaireFontID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@PartNameID int   = null ,

	@FontFamily varchar (50)  = null ,

	@FontSize varchar (50)  = null ,

	@FontColour varchar (50)  = null ,

	@FontWeight varchar (50)  = null ,

	@FontAlignment varchar (50)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuestionnaireFontID]
	, [ClientQuestionnaireID]
	, [PartNameID]
	, [FontFamily]
	, [FontSize]
	, [FontColour]
	, [FontWeight]
	, [FontAlignment]
	, [ClientID]
    FROM
	[dbo].[QuestionnaireFonts] WITH (NOLOCK) 
    WHERE 
	 ([QuestionnaireFontID] = @QuestionnaireFontID OR @QuestionnaireFontID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([PartNameID] = @PartNameID OR @PartNameID IS NULL)
	AND ([FontFamily] = @FontFamily OR @FontFamily IS NULL)
	AND ([FontSize] = @FontSize OR @FontSize IS NULL)
	AND ([FontColour] = @FontColour OR @FontColour IS NULL)
	AND ([FontWeight] = @FontWeight OR @FontWeight IS NULL)
	AND ([FontAlignment] = @FontAlignment OR @FontAlignment IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuestionnaireFontID]
	, [ClientQuestionnaireID]
	, [PartNameID]
	, [FontFamily]
	, [FontSize]
	, [FontColour]
	, [FontWeight]
	, [FontAlignment]
	, [ClientID]
    FROM
	[dbo].[QuestionnaireFonts] WITH (NOLOCK) 
    WHERE 
	 ([QuestionnaireFontID] = @QuestionnaireFontID AND @QuestionnaireFontID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([PartNameID] = @PartNameID AND @PartNameID is not null)
	OR ([FontFamily] = @FontFamily AND @FontFamily is not null)
	OR ([FontSize] = @FontSize AND @FontSize is not null)
	OR ([FontColour] = @FontColour AND @FontColour is not null)
	OR ([FontWeight] = @FontWeight AND @FontWeight is not null)
	OR ([FontAlignment] = @FontAlignment AND @FontAlignment is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Find] TO [sp_executeall]
GO
