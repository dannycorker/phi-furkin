SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Updates the statusID of the DocumentZip record
-- Date: 13/02/2009
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentZip__UpdateStatus]
(
	@DocumentZipID int ,
	@StatusID int
)
AS
BEGIN
	
	-- Modify the updatable columns
	UPDATE [dbo].[DocumentZip]
	SET [StatusID] = @StatusID
	WHERE [DocumentZipID] = @DocumentZipID 


	/* DEBUG: JWG 2011-05-19 */
	--EXEC dbo._C00_LogIt 'Debug', 'DocumentZip__UpdateStatus', @DocumentZipID, @StatusID, 93 

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__UpdateStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__UpdateStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__UpdateStatus] TO [sp_executeall]
GO
