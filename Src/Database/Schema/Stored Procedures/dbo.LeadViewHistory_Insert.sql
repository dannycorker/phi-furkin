SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadViewHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_Insert]
(

	@LeadViewHistoryID int    OUTPUT,

	@ClientPersonnelID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@WhenViewed datetime   
)
AS


				
				INSERT INTO [dbo].[LeadViewHistory]
					(
					[ClientPersonnelID]
					,[ClientID]
					,[LeadID]
					,[WhenViewed]
					)
				VALUES
					(
					@ClientPersonnelID
					,@ClientID
					,@LeadID
					,@WhenViewed
					)
				-- Get the identity value
				SET @LeadViewHistoryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Insert] TO [sp_executeall]
GO
