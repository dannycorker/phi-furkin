SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2012-05-30
-- Description:	Create a Sage 200 payment record in Aquarium
--				ACE 2013-09-03 Changed VARCHAR(30) to 2000 for @InvoiceNumber
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_InsertPayment]
	@ClientID INT,
	@LeadID INT,
	@InvoiceNumber VARCHAR(2000) = NULL,
	@DatePosted VARCHAR(30) = NULL,
	@Amount VARCHAR(30) = NULL,
	@SageUniqueReference VARCHAR(30) = NULL,
	@Debug BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadTypeID INT,
	@NewTableRowID INT,
	/* ThirdPartyMapping field IDs */
	@PaymentInvoiceNumberFieldTP INT = 179,
	@PaymentDateFieldTP INT = 180,
	@PaymentPostedDateFieldTP INT = 181,
	@PaymentAmountFieldTP INT = 182,
	@PaymentSageAccountEntryKeyFieldTP INT = 183,
	/* Detail fields looked up from ThirdPartyMapping */
	@PaymentInvoiceNumberField INT,
	@PaymentInvoiceNumberPage INT,
	@PaymentInvoiceNumberColumnField INT,
	@PaymentDateColumnField INT,
	@PaymentPostedDateColumnField INT,
	@PaymentAmountColumnField INT,
	@PaymentSageAccountEntryKeyColumnField INT
	
	SELECT @LeadTypeID = l.LeadTypeID 
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID 
	
	/* PaymentInvoiceNumber */
	SELECT @PaymentInvoiceNumberField = t.DetailFieldID, @PaymentInvoiceNumberColumnField = t.ColumnFieldID, @PaymentInvoiceNumberPage = df.DetailFieldPageID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.DetailFieldID 
	WHERE t.ThirdPartyFieldID = @PaymentInvoiceNumberFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* PaymentDate */
	SELECT @PaymentDateColumnField = t.ColumnFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @PaymentDateFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* PaymentDate */
	SELECT @PaymentPostedDateColumnField = t.ColumnFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @PaymentPostedDateFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* PaymentDate */
	SELECT @PaymentAmountColumnField = t.ColumnFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @PaymentAmountFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	/* PaymentDate */
	SELECT @PaymentSageAccountEntryKeyColumnField = t.ColumnFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @PaymentSageAccountEntryKeyFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	IF @Debug = 1
	BEGIN
		SELECT @LeadTypeID AS LeadTypeID,
		@PaymentInvoiceNumberField AS PaymentInvoiceNumberField,
		@PaymentInvoiceNumberPage AS PaymentInvoiceNumberPage,
		@PaymentInvoiceNumberColumnField AS PaymentInvoiceNumberColumnField,
		@PaymentDateColumnField AS PaymentDateColumnField,
		@PaymentPostedDateColumnField AS PaymentPostedDateColumnField,
		@PaymentAmountColumnField AS PaymentAmountColumnField,
		@PaymentSageAccountEntryKeyColumnField AS PaymentSageAccountEntryKeyColumnField
	END
	ELSE
	BEGIN
		INSERT INTO TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
		VALUES (@ClientID, @LeadID, @PaymentInvoiceNumberField, @PaymentInvoiceNumberPage, 1, 1)
		
		SELECT @NewTableRowID = SCOPE_IDENTITY()
	
		
		INSERT INTO TableDetailValues (ClientID, LeadID, TableRowID, DetailFieldID, DetailValue)
		VALUES  (@ClientID, @LeadID, @NewTableRowID, @PaymentInvoiceNumberColumnField,  CONVERT(VARCHAR, @InvoiceNumber)),
				(@ClientID, @LeadID, @NewTableRowID, @PaymentDateColumnField, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)),
				(@ClientID, @LeadID, @NewTableRowID, @PaymentPostedDateColumnField,  CONVERT(VARCHAR, @DatePosted)),
				(@ClientID, @LeadID, @NewTableRowID, @PaymentAmountColumnField,  CONVERT(VARCHAR, @Amount)),
				(@ClientID, @LeadID, @NewTableRowID, @PaymentSageAccountEntryKeyColumnField,  CONVERT(VARCHAR, @SageUniqueReference))
		
		/* Added By PR 18-06-2012 Places the data at the customer level for BF*/
		IF @ClientID = 3
		BEGIN
			
			DECLARE @CustNewTableRowID INT
			DECLARE @CustomerID INT
			
			SELECT @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

			INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
			VALUES (@ClientID, @CustomerID, 138006, 15489, 1, 1)
		
			SELECT @CustNewTableRowID = SCOPE_IDENTITY()
			
			INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
			VALUES  (@ClientID, @CustomerID, @CustNewTableRowID, 138000,  CONVERT(VARCHAR, @InvoiceNumber)),
				(@ClientID, @CustomerID, @CustNewTableRowID, 138001, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)),				
				(@ClientID, @CustomerID, @CustNewTableRowID, 138002,  CONVERT(VARCHAR, @Amount))
		
		END		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InsertPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_InsertPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InsertPayment] TO [sp_executeall]
GO
