SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields

CREATE PROCEDURE [dbo].[DetailFields__GetEnabledFieldsByDetailFieldPageID]
	-- Add the parameters for the stored procedure here
	@DetailFieldPageID int,
	@ClientID int
AS
	SET NOCOUNT ON

	SELECT
		*
	FROM
		dbo.fnDetailFieldsShared(@ClientID)
	WHERE
		[DetailFieldPageID] = @DetailFieldPageID
		AND [ClientID] = @ClientID		
		AND [Enabled] = 1
		ORDER BY FieldOrder


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledFieldsByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetEnabledFieldsByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledFieldsByDetailFieldPageID] TO [sp_executeall]
GO
