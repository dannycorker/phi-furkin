SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-11-12
-- Description:	Take an XML package representing a customer and create the customer record within this database
--
-- Modifications:
-- 2019-11-28 MAB multiple changes to implement version 2 of Liberty Mediate API. Individual modification comments omitted to keep code clean.
-- 2019-12-17 MAB multiple changes to implement version 3 of Liberty Mediate API.
-- 2020-01-06 MAB removed GET and DELETE HTTP methods, call harvest customer JSON function and re-introduced @ShowNewCustomer parameter
-- 2020-01-07 MAB validate input data for LAGCLAIM-290 + added title ID for compatibility with Vision shape
-- 2020-01-08 MAB modified to ignore titleId and use title to lookup the ID (as an interim solution until Vision is implemented).
-- 2020-01-10 MAB modified validation to allow for non-mandatory fields
-- 2020-07-14 ALM updated [dbo].[fn_C600_Harvest_Customer_JSON] to [dbo].[fn_C600_Mediate_Harvest_Customer]
-- 2021-05-10 AAD for FURKIN-611 - Use AffinityID from XML rather than always using the default
-- =============================================
CREATE PROCEDURE [dbo].[_C600_API_Receive_Customer]
(
	@XmlPayload				XML
	,@HttpMethod			VARCHAR(50)
	,@ObjectID				UNIQUEIDENTIFIER
	,@ParentObjectID		UNIQUEIDENTIFIER
	,@RunAsUserID			INT
	,@JsonResponse			NVARCHAR(MAX) OUTPUT
	,@ShowNewCustomer		BIT = 0
)
AS
BEGIN

	/*
		Configuration Data
	*/
	DECLARE	@ClientID										INT				= 600
			,@SendDocumentsDetailFieldID					INT				= 170257	/*Send documents by*/
			,@VulnerabilityDetailFieldID					INT				= 177671	/*DDA Details (if other)*/
			,@CommunicationMethodLLID						INT				= 3843		/*ID of Communication Method Lookup List*/
			,@CustomerAffinityDetailFieldID					INT				= 170144	/*ID of Customer detail field for Affinity Details*/
			,@AffinityShortCodeResourceListDetailFieldID	INT				= 170127	/*ID of the short code detail field in the Affinity resource list*/
			,@DefaultTargetAffinityResourceListID			INT				= 2000184	/*Resource list ID of default affinity for target system = Furkin*/
			,@CustomerAPITransferMappingIdDetailFieldID		INT				= 313784	/*ID of Customer API Transfer Mapping ID*/

	DECLARE	@CustomerID						INT
			,@Title							VARCHAR(50)
			,@FirstName						VARCHAR(100)
			,@MiddleName					VARCHAR(100)
			,@LastName						VARCHAR(100)
			,@EmailAddress					VARCHAR(255)
			,@HomeTelephone					VARCHAR(50)
			,@MobileTelephone				VARCHAR(50)
			,@Address1						VARCHAR(200)
			,@Address2						VARCHAR(200)
			,@Town							VARCHAR(200)
			,@County						VARCHAR(200)
			,@PostCode						VARCHAR(50)
			,@Test							BIT
			,@DateOfBirth					DATE
			,@CountryName					VARCHAR(255)
			,@CustomerRef					VARCHAR(100)
			,@WhoChanged					INT
			,@WhenChanged					DATETIME
			,@Comments						VARCHAR(MAX)
			,@LanguageName					VARCHAR(50)
			,@VulnerabilityDetails			VARCHAR(2000)
			,@CommunicationPreference		VARCHAR(500)
			,@AffinityId					INT
			,@AffinityName					VARCHAR(2000)
			,@QueueID						INT
			,@TargetCustomerID				INT

			,@LanguageID					INT
			,@CountryID						INT
			,@SendDocumentsID				INT
			,@TitleID						INT
											
			,@NewCustomerID					INT				= 0
			,@DateTimeCompleted				DATETIME2		= CURRENT_TIMESTAMP
			,@ChangeSource					VARCHAR(2000)
			,@ErrorMessage					VARCHAR(2000)	= ''	/*2020-01-07 MAB Added*/
			,@ErrorMessageSeparator			VARCHAR(3)		= ' | '

	SELECT	 @CustomerID				= cr.value('(customerID)				[1]','INT')
			,@Title						= cr.value('(title)						[1]','VARCHAR(50)')
			,@TitleID					= cr.value('(titleId)					[1]','INT') /*2020-01-07 MAB Added for compatibility with Vision shape*/
			,@FirstName					= cr.value('(firstName)					[1]','VARCHAR(100)')
			,@MiddleName				= cr.value('(middleName)				[1]','VARCHAR(100)')
			,@LastName					= cr.value('(lastName)					[1]','VARCHAR(100)')
			,@EmailAddress				= cr.value('(email)						[1]','VARCHAR(255)')
			,@HomeTelephone				= cr.value('(phoneNumber)				[1]','VARCHAR(50)')
			,@MobileTelephone			= cr.value('(mobileNumber)				[1]','VARCHAR(50)')
			,@Test						= ISNULL(cr.value('(test)				[1]','BIT'),0)
			,@DateOfBirth				= cr.value('(birthdayDate)				[1]','DATE')
			,@CustomerRef				= cr.value('(customerRef)				[1]','VARCHAR(100)')
			,@WhoChanged				= cr.value('(whoChanged)				[1]','INT')
			,@WhenChanged				= cr.value('(whenChanged)				[1]','DATETIME')
			,@Comments					= cr.value('(comments)					[1]','VARCHAR(MAX)')
			,@LanguageName				= cr.value('(language)					[1]','VARCHAR(50)')
			,@VulnerabilityDetails		= cr.value('(ddaDescription)			[1]','VARCHAR(2000)')
			,@CommunicationPreference	= cr.value('(communicationPreference)	[1]','VARCHAR(500)')
			,@AffinityId				= cr.value('(affinityId)				[1]','INT')
			,@AffinityName				= cr.value('(affinityName)				[1]','VARCHAR(2000)')
			,@QueueID					= cr.value('(QueueID)					[1]','INT')
	FROM @XmlPayload.nodes('/CustomerRequest') AS n(cr)

	SELECT	@Address1				= addr.value('(address1)					[1]','VARCHAR(200)')
			,@Address2				= addr.value('(address2)					[1]','VARCHAR(200)')
			,@Town					= addr.value('(town)						[1]','VARCHAR(200)')
			,@County				= addr.value('(county)						[1]','VARCHAR(200)')
			,@PostCode				= addr.value('(postcode)					[1]','VARCHAR(50)')
			,@CountryName			= addr.value('(countryName)					[1]','VARCHAR(255)')
	FROM @XmlPayload.nodes('(/CustomerRequest/addresses/Address)[1]') AS n(addr)
			
	/*If no time of change provided, then default to now.*/
	SELECT @WhenChanged = CASE WHEN @WhenChanged > '1900-01-01 00:00:00.000' THEN @WhenChanged ELSE @DateTimeCompleted END
			
	/*Look-up mapped fields*/
	IF @CountryName IS NOT NULL /*2020-01-10 MAB optional field*/
	BEGIN
		SELECT @CountryID = c.CountryID FROM Country c WITH (NOLOCK) WHERE c.CountryName = @CountryName

		/*2020-01-07 MAB Validate input data for for LAGCLAIM-290*/
		IF @CountryID IS NULL
		BEGIN
			SELECT @ErrorMessage += 'Could not convert countryName=' + ISNULL(@CountryName, 'NULL') + ' to its lookup ID in table Country' + @ErrorMessageSeparator
		END
	END
	
	IF @LanguageName IS NOT NULL /*2020-01-10 MAB optional field*/
	BEGIN
		SELECT	@LanguageID = l.LanguageID FROM Language l WITH (NOLOCK) WHERE l.LanguageName = @LanguageName

		/*2020-01-07 MAB Validate input data for for LAGCLAIM-290*/
		IF @LanguageID IS NULL
		BEGIN
			SELECT @ErrorMessage += 'Could not convert language=' + ISNULL(@LanguageName, 'NULL') + ' to its lookup ID in table Language' + @ErrorMessageSeparator
		END
	END

	IF @Title IS NOT NULL /*2020-01-10 MAB optional field*/
	BEGIN
		SELECT @TitleID = t.TitleID FROM Titles t WITH (NOLOCK) WHERE t.Title = @Title

		/*2020-01-07 MAB Validate input data for for LAGCLAIM-290*/
		IF @TitleID = 0 AND @Title <> 'Unknown'
		BEGIN
			SELECT @ErrorMessage += 'Could not convert title=' + ISNULL(@Title, 'NULL') + ' to its lookup ID in table Titles' + @ErrorMessageSeparator
		END
	END
	ELSE
	BEGIN
		SELECT @TitleID = 0 /*Default to 'Unknown' if no title description supplied*/
	END

	SELECT	@SendDocumentsID = lli.LookupListItemID FROM LookupListItems lli WITH (NOLOCK) WHERE lli.ItemValue = @CommunicationPreference AND lli.LookupListID = @CommunicationMethodLLID
	
	/*2020-01-07 MAB Validate input data for for LAGCLAIM-290*/
	IF @SendDocumentsID IS NULL
	BEGIN
		SELECT @ErrorMessage += 'Could not convert communicationPreference=' + ISNULL(@CommunicationPreference, 'NULL') + ' to its lookup ID in table LookupListItems' + @ErrorMessageSeparator
	END

	/*If a validation error ocurred, raise it - it will be caught in the calling procedure _C00_Mediate_Pop*/
	IF @ErrorMessage > ''
	BEGIN
		SELECT @ErrorMessage = SUBSTRING(@ErrorMessage, 1, LEN(@ErrorMessage)-2) /*Remove the final separator character*/

		RAISERROR (@ErrorMessage, 16, 1)

		RETURN
	END

	IF @HttpMethod = 'POST'
	BEGIN
		SELECT @ChangeSource ='Inserted by ' + OBJECT_NAME(@@ProcID)
								+ ' from Affinity = ' + ISNULL(@AffinityName,'NULL')
								+ ' using QueueID = ' + ISNULL(CONVERT(VARCHAR,@QueueID),'NULL')

		/*Create the new customer record*/
		INSERT Customers ( [ClientID], [TitleID], [IsBusiness], [FirstName], [MiddleName], [LastName], [EmailAddress], [DayTimeTelephoneNumber], [DayTimeTelephoneNumberVerifiedAndValid], [HomeTelephone], [HomeTelephoneVerifiedAndValid], [MobileTelephone], [MobileTelephoneVerifiedAndValid], [CompanyTelephone], [CompanyTelephoneVerifiedAndValid], [WorksTelephone], [WorksTelephoneVerifiedAndValid], [Address1], [Address2], [Town], [County], [PostCode], [Website], [HasDownloaded], [DownloadedOn], [AquariumStatusID], [ClientStatusID], [Test], [CompanyName], [Occupation], [Employer], [PhoneNumbersVerifiedOn], [DoNotEmail], [DoNotSellToThirdParty], [AgreedToTermsAndConditions], [DateOfBirth], [DefaultContactID], [DefaultOfficeID], [AddressVerified], [CountryID], [SubClientID], [CustomerRef], [WhoChanged], [WhenChanged], [ChangeSource], [EmailAddressVerifiedAndValid], [EmailAddressVerifiedOn], [Comments], [AllowSmsCommandProcessing], [LanguageID], [Longitude], [Latitude] )
		VALUES ( @ClientID, @TitleID, 0, @FirstName, @MiddleName, @LastName, @EmailAddress, NULL, 0, @HomeTelephone, 0, @MobileTelephone, 0, NULL, 0, NULL, 0, @Address1, @Address2, @Town, @County, @PostCode, NULL, 1, @DateTimeCompleted, 2, NULL, @Test, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @DateOfBirth, NULL, NULL, NULL, @CountryID, NULL, @CustomerRef, @RunAsUserID, @WhenChanged, @ChangeSource, NULL, NULL, @Comments, NULL, @LanguageID, NULL, NULL )

		/*Get the new customer ID*/
		SELECT @TargetCustomerID = SCOPE_IDENTITY()

		/*Assign the customer to its affinity.*/
		INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @TargetCustomerID, @CustomerAffinityDetailFieldID, CONVERT(VARCHAR(10), ISNULL(@AffinityId, @DefaultTargetAffinityResourceListID))) /* Use AffinityID from XML rather than always using the default. 2021-05-10 AAD for FURKIN-611 */

		/*Key customer-level fields.*/
		INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @TargetCustomerID, @SendDocumentsDetailFieldID, CONVERT(VARCHAR(10), @SendDocumentsID))

		INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @TargetCustomerID, @VulnerabilityDetailFieldID, @VulnerabilityDetails)

		/*Save the transfer mapping ID*/
		INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @TargetCustomerID, @CustomerAPITransferMappingIdDetailFieldID, CONVERT(VARCHAR(40), @ObjectID))
	END

	IF @HttpMethod = 'PUT'
	BEGIN

		SELECT @ChangeSource = 'Updated by ' + OBJECT_NAME(@@ProcID)
								+ ' from Affinity = ' + ISNULL(@AffinityName,'NULL')
								+ ' using QueueID = ' + ISNULL(CONVERT(VARCHAR,@QueueID),'NULL')

		/*Get the target Customer ID from the passed in Transfer Mapping GUID*/
		SELECT @TargetCustomerID = cdv.CustomerID
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE DetailValue = CONVERT(VARCHAR(40), @ObjectID)
		AND cdv.DetailFieldID = @CustomerAPITransferMappingIdDetailFieldID

		/*2020-01-07 MAB Validate that a customer match was found*/
		IF @TargetCustomerID is NULL
		BEGIN
			SELECT @ErrorMessage = 'No mapped CustomerID found for ObjectID ' + ISNULL(CONVERT(VARCHAR(40), @ObjectID), 'NULL')
			
			RAISERROR( @ErrorMessage, 16, 1 )

			RETURN
		END

		/*Update the existing customer record*/
		UPDATE cu
		SET  TitleID			= @TitleID
			,FirstName			= @FirstName		
			,MiddleName			= @MiddleName		
			,LastName			= @LastName		
			,EmailAddress		= @EmailAddress	
			,HomeTelephone		= @HomeTelephone	
			,MobileTelephone	= @MobileTelephone
			,Address1			= @Address1		
			,Address2			= @Address2		
			,Town				= @Town			
			,County				= @County			
			,PostCode			= @PostCode		
			,Test				= @Test			
			,DateOfBirth		= @DateOfBirth	
			,CountryID			= @CountryID
			,LanguageID			= @LanguageID
			,CustomerRef		= @CustomerRef	
			,ChangeSource		= @ChangeSource
			,WhoChanged			= @RunAsUserID
			,WhenChanged		= @WhenChanged
			,DownloadedOn		= @DateTimeCompleted
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @TargetCustomerID

		/*Update key customer-level fields*/
		EXEC _C00_SimpleValueIntoField @DetailFieldID = @SendDocumentsDetailFieldID, @DetailValue = @SendDocumentsID, @ObjectID = @TargetCustomerID, @ClientPersonnelID = @RunAsUserID

		EXEC _C00_SimpleValueIntoField @DetailFieldID = @VulnerabilityDetailFieldID, @DetailValue = @VulnerabilityDetails, @ObjectID = @TargetCustomerID, @ClientPersonnelID = @RunAsUserID
	END

	/*Assemble response and return it*/
	SELECT @JsonResponse = dbo.fn_C600_Mediate_Harvest_Customer(@TargetCustomerID,NULL,0,0,0) /* Added NULL @PolicyID param. Call to function was wrong shape and proc wouldn't update otherwise. 2021-05-10 AAD for FURKIN-611 */

	/*Return the whole customer object*/
	IF @ShowNewCustomer = 1
	BEGIN
		SELECT	 cu.*
				,lli.LookupListItemID	[SendDocumentsBy_LookupListItemID]
				,lli.ItemValue			[SendDocumentsBy_ItemValue]
				,dbo.fn_C600_GetVulnerabilityDdaDetailsAsString(@NewCustomerID, DEFAULT, DEFAULT) AS [VulnerabilityDdaDetails]
		FROM Customers cu WITH (NOLOCK) 
		LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = cu.CustomerID AND cdv.DetailFieldID = @SendDocumentsDetailFieldID /*Send documents by*/
		LEFT JOIN LookupListItems lli WITH (NOLOCK) on lli.LookupListItemID = cdv.ValueInt
		WHERE cu.CustomerID = @NewCustomerID
	END

	RETURN @TargetCustomerID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Customer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_API_Receive_Customer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Customer] TO [sp_executeall]
GO
