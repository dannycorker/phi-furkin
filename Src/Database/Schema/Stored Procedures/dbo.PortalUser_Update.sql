SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUser table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser_Update]
(

	@PortalUserID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@Username varchar (50)  ,

	@Password varchar (65)  ,

	@Salt varchar (25)  ,

	@ClientPersonnelID int   ,

	@Enabled bit   ,

	@AttemptedLogins int   ,

	@PortalUserGroupID int   
)
AS


				
				
				-- If we don't have an outer tranasction e.g. from the lead event 
				DECLARE @TranCount INT = @@TRANCOUNT
				IF @TranCount = 0
				BEGIN
					BEGIN TRAN
				END
				
				BEGIN TRY
		
				-- Updates the master record
				EXEC AquariusMaster.dbo.AQ_PortalUser_Update @PortalUserID, @Username
				
				-- Set context info to allow access to table
				DECLARE @ContextInfo VARBINARY(100) = CAST('PortalUser' AS VARBINARY)
				SET CONTEXT_INFO @ContextInfo
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalUser]
				SET
					[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[Username] = @Username
					,[Password] = @Password
					,[Salt] = @Salt
					,[ClientPersonnelID] = @ClientPersonnelID
					,[Enabled] = @Enabled
					,[AttemptedLogins] = @AttemptedLogins
					,[PortalUserGroupID] = @PortalUserGroupID
				WHERE
[PortalUserID] = @PortalUserID 
				
				
				-- If we don't have an outer tran we can commit here
				IF @TranCount = 0
				BEGIN
					COMMIT
				END
				
				-- Clear the things we set above
				SET CONTEXT_INFO 0x0
				
				END TRY			
				BEGIN CATCH    
					
					-- If we have any tran open then we need to rollback
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK
					END
					
					-- Clear the things we set above
					SET CONTEXT_INFO 0x0
					
					DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
					DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
					DECLARE @ErrorState INT = ERROR_STATE()

					RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
					
					
				END CATCH
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Update] TO [sp_executeall]
GO
