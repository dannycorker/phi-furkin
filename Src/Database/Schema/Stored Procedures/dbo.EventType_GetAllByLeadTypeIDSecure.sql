SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*
----------------------------------------------------------------------------------------------------
-- Date Created: 29 May 2007

-- Created By:  Chris, Jim
-- Purpose: Select all in-process events for the current Lead Type, as long as the user has access to them
--			2012-02-20	SB	Don't show events of a specific subtype if they are disabled
--			2014-07-22	SB	Updated to use function for shared lead type
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType_GetAllByLeadTypeIDSecure] 
(
	@LeadTypeID int,
	@UserID int
)
AS

	SET ANSI_NULLS OFF
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

		SELECT
		[EventTypeID],
		[ClientID],
		[EventTypeName],
		[EventTypeDescription],
		[Enabled],
		[UnitsOfEffort],
		[FollowupTimeUnitsID],
		[FollowupQuantity],
		[AvailableManually],
		[StatusAfterEvent],
		[AquariumEventAfterEvent],
		et.[EventSubtypeID],
		[DocumentTypeID],
		[LeadTypeID],
		[AllowCustomTimeUnits],
		[InProcess]
	FROM
		dbo.fnEventTypeShared(@ClientID) et 
		INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON et.EventTypeID = fnEventTypeSecure.objectid
		INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON et.EventSubtypeID = est.EventSubtypeID 
	WHERE et.InProcess = 1 
		--EXISTS (SELECT [EventTypeID],[NextEventTypeID] FROM dbo.[EventChoice] ec WHERE (et.[EventTypeID] = ec.[EventTypeID] OR et.[EventTypeID] = ec.[NextEventTypeID]))
		AND et.LeadTypeID = @LeadTypeID
		AND et.[EventSubTypeID] NOT IN (7,8,9)
		AND et.Enabled = 1
		AND est.Available = 1
	ORDER BY EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON










GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetAllByLeadTypeIDSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_GetAllByLeadTypeIDSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetAllByLeadTypeIDSecure] TO [sp_executeall]
GO
