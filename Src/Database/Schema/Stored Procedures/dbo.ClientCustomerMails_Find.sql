SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientCustomerMails table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientCustomerMails_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientCustomerMailID int   = null ,

	@ClientID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@CustomerID int   = null ,

	@ClientAreaMailingListID int   = null ,

	@DateSent datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientCustomerMailID]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [CustomerID]
	, [ClientAreaMailingListID]
	, [DateSent]
    FROM
	[dbo].[ClientCustomerMails] WITH (NOLOCK) 
    WHERE 
	 ([ClientCustomerMailID] = @ClientCustomerMailID OR @ClientCustomerMailID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([ClientAreaMailingListID] = @ClientAreaMailingListID OR @ClientAreaMailingListID IS NULL)
	AND ([DateSent] = @DateSent OR @DateSent IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientCustomerMailID]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [CustomerID]
	, [ClientAreaMailingListID]
	, [DateSent]
    FROM
	[dbo].[ClientCustomerMails] WITH (NOLOCK) 
    WHERE 
	 ([ClientCustomerMailID] = @ClientCustomerMailID AND @ClientCustomerMailID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([ClientAreaMailingListID] = @ClientAreaMailingListID AND @ClientAreaMailingListID is not null)
	OR ([DateSent] = @DateSent AND @DateSent is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientCustomerMails_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Find] TO [sp_executeall]
GO
