SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AutomatedTask table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTask_Find]
(

	@SearchUsingOR bit   = null ,

	@TaskID int   = null ,

	@ClientID int   = null ,

	@Taskname varchar (100)  = null ,

	@Description varchar (250)  = null ,

	@Enabled bit   = null ,

	@RunAtHour int   = null ,

	@RunAtMinute int   = null ,

	@RepeatTimeUnitsID int   = null ,

	@RepeatTimeQuantity int   = null ,

	@NextRunDateTime datetime   = null ,

	@WorkflowTask bit   = null ,

	@AlreadyRunning bit   = null ,

	@AutomatedTaskGroupID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@MaximumAllowableErrors int   = null ,

	@EventSubTypeThresholding bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TaskID]
	, [ClientID]
	, [Taskname]
	, [Description]
	, [Enabled]
	, [RunAtHour]
	, [RunAtMinute]
	, [RepeatTimeUnitsID]
	, [RepeatTimeQuantity]
	, [NextRunDateTime]
	, [WorkflowTask]
	, [AlreadyRunning]
	, [AutomatedTaskGroupID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [MaximumAllowableErrors]
	, [EventSubTypeThresholding]
    FROM
	[dbo].[AutomatedTask] WITH (NOLOCK) 
    WHERE 
	 ([TaskID] = @TaskID OR @TaskID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Taskname] = @Taskname OR @Taskname IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([RunAtHour] = @RunAtHour OR @RunAtHour IS NULL)
	AND ([RunAtMinute] = @RunAtMinute OR @RunAtMinute IS NULL)
	AND ([RepeatTimeUnitsID] = @RepeatTimeUnitsID OR @RepeatTimeUnitsID IS NULL)
	AND ([RepeatTimeQuantity] = @RepeatTimeQuantity OR @RepeatTimeQuantity IS NULL)
	AND ([NextRunDateTime] = @NextRunDateTime OR @NextRunDateTime IS NULL)
	AND ([WorkflowTask] = @WorkflowTask OR @WorkflowTask IS NULL)
	AND ([AlreadyRunning] = @AlreadyRunning OR @AlreadyRunning IS NULL)
	AND ([AutomatedTaskGroupID] = @AutomatedTaskGroupID OR @AutomatedTaskGroupID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([MaximumAllowableErrors] = @MaximumAllowableErrors OR @MaximumAllowableErrors IS NULL)
	AND ([EventSubTypeThresholding] = @EventSubTypeThresholding OR @EventSubTypeThresholding IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TaskID]
	, [ClientID]
	, [Taskname]
	, [Description]
	, [Enabled]
	, [RunAtHour]
	, [RunAtMinute]
	, [RepeatTimeUnitsID]
	, [RepeatTimeQuantity]
	, [NextRunDateTime]
	, [WorkflowTask]
	, [AlreadyRunning]
	, [AutomatedTaskGroupID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [MaximumAllowableErrors]
	, [EventSubTypeThresholding]
    FROM
	[dbo].[AutomatedTask] WITH (NOLOCK) 
    WHERE 
	 ([TaskID] = @TaskID AND @TaskID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Taskname] = @Taskname AND @Taskname is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([RunAtHour] = @RunAtHour AND @RunAtHour is not null)
	OR ([RunAtMinute] = @RunAtMinute AND @RunAtMinute is not null)
	OR ([RepeatTimeUnitsID] = @RepeatTimeUnitsID AND @RepeatTimeUnitsID is not null)
	OR ([RepeatTimeQuantity] = @RepeatTimeQuantity AND @RepeatTimeQuantity is not null)
	OR ([NextRunDateTime] = @NextRunDateTime AND @NextRunDateTime is not null)
	OR ([WorkflowTask] = @WorkflowTask AND @WorkflowTask is not null)
	OR ([AlreadyRunning] = @AlreadyRunning AND @AlreadyRunning is not null)
	OR ([AutomatedTaskGroupID] = @AutomatedTaskGroupID AND @AutomatedTaskGroupID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([MaximumAllowableErrors] = @MaximumAllowableErrors AND @MaximumAllowableErrors is not null)
	OR ([EventSubTypeThresholding] = @EventSubTypeThresholding AND @EventSubTypeThresholding is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_Find] TO [sp_executeall]
GO
