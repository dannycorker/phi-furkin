SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelAdminGroups table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAdminGroups_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPersonnelAdminGroupID],
					[ClientID],
					[GroupName]
				FROM
					[dbo].[ClientPersonnelAdminGroups] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAdminGroups_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_GetByClientID] TO [sp_executeall]
GO
