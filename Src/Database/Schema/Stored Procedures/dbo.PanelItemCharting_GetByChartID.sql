SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PanelItemCharting table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItemCharting_GetByChartID]
(

	@ChartID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PanelItemChartingID],
					[ClientID],
					[PanelItemChartingName],
					[PanelID],
					[CreatedBy],
					[ChartID]
				FROM
					[dbo].[PanelItemCharting] WITH (NOLOCK) 
				WHERE
					[ChartID] = @ChartID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_GetByChartID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItemCharting_GetByChartID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItemCharting_GetByChartID] TO [sp_executeall]
GO
