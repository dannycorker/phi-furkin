SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the MatterPageChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_Update]
(

	@MatterPageChoiceID int   ,

	@ClientID int   ,

	@MatterID int   ,

	@DetailFieldPageID int   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[MatterPageChoice]
				SET
					[ClientID] = @ClientID
					,[MatterID] = @MatterID
					,[DetailFieldPageID] = @DetailFieldPageID
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[MatterPageChoiceID] = @MatterPageChoiceID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Update] TO [sp_executeall]
GO
