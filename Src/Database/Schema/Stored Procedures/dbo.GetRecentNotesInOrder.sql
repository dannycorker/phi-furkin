SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-06-2015
-- Description:	Get recent notes in order
-- =============================================
CREATE PROCEDURE [dbo].[GetRecentNotesInOrder]
	@LeadID INT,
	@CaseID INT = NULL,
	@NoteCount INT = 5,
	@ShowDeleted BIT = 0,	
	@ClientPersonnelID INT = 0,
	@MinimumRightsRequired INT = 1
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT TOP (@NoteCount)
		le.LeadEventID AS 'LeadEventID',
		le.LeadID AS 'LeadID',		
		le.Comments AS 'Comments',		
		ISNULL ( nt.NoteTypeName, et.EventTypeName) AS 'EventType',
		CONVERT(CHAR(11),le.WhenCreated,103) + LEFT(CONVERT(CHAR(5),LE.WhenCreated,108),5) AS 'When',
		cp.UserName AS 'Who',
		le.EventDeleted,
		le.NotePriority AS [NotePriority],
		ISNULL(nt.AlertColour, '') AS [AlertColour],
		ISNULL(nt.NormalColour, '') AS [NormalColour]
	FROM
		LeadEvent le WITH (NOLOCK)  
		LEFT OUTER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID
		LEFT OUTER JOIN NoteType nt WITH (NOLOCK) ON le.NoteTypeID = nt.NoteTypeID
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON le.WhoCreated = cp.ClientPersonnelID
		LEFT JOIN ClientOption clop WITH (NOLOCK) ON clop.ClientID = cp.ClientID
		LEFT JOIN dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) snt ON snt.ObjectID = le.NoteTypeID
	WHERE 
		le.NoteTypeID > 0 AND -- only show notes
		le.LeadID = @LeadID AND 
		(le.CaseID = @CaseID OR @CaseID IS NULL) AND
		CASE
			WHEN @ShowDeleted = 0 AND (le.EventDeleted = 0 OR le.EventDeleted IS NULL) THEN 1
			WHEN @ShowDeleted = 1 THEN 1
		END = 1		
		AND	
			CASE
				WHEN et.EventTypeID IS NULL AND snt.ObjectID IS NULL THEN 0
				ELSE 1 
			END = 1			
	ORDER BY ISNULL(le.NotePriority,0) DESC, le.WhenCreated DESC, le.LeadEventID DESC
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[GetRecentNotesInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetRecentNotesInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetRecentNotesInOrder] TO [sp_executeall]
GO
