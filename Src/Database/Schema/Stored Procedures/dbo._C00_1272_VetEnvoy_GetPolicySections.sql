SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2013-09-02
-- Description:	Gets the claim items and the policy sections
-- Used by:		DF 162693
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_VetEnvoy_GetPolicySections] 
(
	@MatterID INT,
	@LeadID INT
)
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	r.TableRowID, tdvSection.ResourceListID AS SectionID, tdvDesc.DetailValue AS Description, tdvType.DetailValue AS Type, tdvInvoice.DetailValue AS InvoiceNo, 
			tdvDate.ValueDate AS Date, tdvQuant.DetailValue AS Quantity, tdvTotal.DetailValue AS Total			
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvSection WITH (NOLOCK) ON r.TableRowID = tdvSection.TableRowID AND tdvSection.DetailFieldID = 162680
	INNER JOIN dbo.TableDetailValues tdvDesc WITH (NOLOCK) ON r.TableRowID = tdvDesc.TableRowID AND tdvDesc.DetailFieldID = 162681
	INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 162682
	INNER JOIN dbo.TableDetailValues tdvInvoice WITH (NOLOCK) ON r.TableRowID = tdvInvoice.TableRowID AND tdvInvoice.DetailFieldID = 162683
	INNER JOIN dbo.TableDetailValues tdvDate WITH (NOLOCK) ON r.TableRowID = tdvDate.TableRowID AND tdvDate.DetailFieldID = 162684
	INNER JOIN dbo.TableDetailValues tdvQuant WITH (NOLOCK) ON r.TableRowID = tdvQuant.TableRowID AND tdvQuant.DetailFieldID = 162685
	INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 162688
	WHERE r.MatterID = @MatterID
	
	EXEC dbo._C00_1272_Policy_GetPolicySections @MatterID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_VetEnvoy_GetPolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_VetEnvoy_GetPolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_VetEnvoy_GetPolicySections] TO [sp_executeall]
GO
