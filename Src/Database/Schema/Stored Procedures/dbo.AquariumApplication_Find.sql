SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AquariumApplication table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumApplication_Find]
(

	@SearchUsingOR bit   = null ,

	@AquariumApplicationID int   = null ,

	@ApplicationName varchar (50)  = null ,

	@ApplicationDescription varchar (250)  = null ,

	@CurrentVersion varchar (50)  = null ,

	@MinimumVersion varchar (50)  = null ,

	@CurrentVersionValidFrom datetime   = null ,

	@CurrentVersionValidTo datetime   = null ,

	@CurrentVersionDeployedOn datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AquariumApplicationID]
	, [ApplicationName]
	, [ApplicationDescription]
	, [CurrentVersion]
	, [MinimumVersion]
	, [CurrentVersionValidFrom]
	, [CurrentVersionValidTo]
	, [CurrentVersionDeployedOn]
    FROM
	[dbo].[AquariumApplication] WITH (NOLOCK) 
    WHERE 
	 ([AquariumApplicationID] = @AquariumApplicationID OR @AquariumApplicationID IS NULL)
	AND ([ApplicationName] = @ApplicationName OR @ApplicationName IS NULL)
	AND ([ApplicationDescription] = @ApplicationDescription OR @ApplicationDescription IS NULL)
	AND ([CurrentVersion] = @CurrentVersion OR @CurrentVersion IS NULL)
	AND ([MinimumVersion] = @MinimumVersion OR @MinimumVersion IS NULL)
	AND ([CurrentVersionValidFrom] = @CurrentVersionValidFrom OR @CurrentVersionValidFrom IS NULL)
	AND ([CurrentVersionValidTo] = @CurrentVersionValidTo OR @CurrentVersionValidTo IS NULL)
	AND ([CurrentVersionDeployedOn] = @CurrentVersionDeployedOn OR @CurrentVersionDeployedOn IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AquariumApplicationID]
	, [ApplicationName]
	, [ApplicationDescription]
	, [CurrentVersion]
	, [MinimumVersion]
	, [CurrentVersionValidFrom]
	, [CurrentVersionValidTo]
	, [CurrentVersionDeployedOn]
    FROM
	[dbo].[AquariumApplication] WITH (NOLOCK) 
    WHERE 
	 ([AquariumApplicationID] = @AquariumApplicationID AND @AquariumApplicationID is not null)
	OR ([ApplicationName] = @ApplicationName AND @ApplicationName is not null)
	OR ([ApplicationDescription] = @ApplicationDescription AND @ApplicationDescription is not null)
	OR ([CurrentVersion] = @CurrentVersion AND @CurrentVersion is not null)
	OR ([MinimumVersion] = @MinimumVersion AND @MinimumVersion is not null)
	OR ([CurrentVersionValidFrom] = @CurrentVersionValidFrom AND @CurrentVersionValidFrom is not null)
	OR ([CurrentVersionValidTo] = @CurrentVersionValidTo AND @CurrentVersionValidTo is not null)
	OR ([CurrentVersionDeployedOn] = @CurrentVersionDeployedOn AND @CurrentVersionDeployedOn is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumApplication_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Find] TO [sp_executeall]
GO
