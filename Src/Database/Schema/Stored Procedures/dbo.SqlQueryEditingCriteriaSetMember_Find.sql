SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditingCriteriaSetMember table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingCriteriaSetMemberID int   = null ,

	@ClientID int   = null ,

	@SqlQueryEditingID int   = null ,

	@SqlQueryEditingCriteriaSetID int   = null ,

	@SqlQueryEditingColumnID int   = null ,

	@Criteria1 varchar (250)  = null ,

	@Criteria2 varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingCriteriaSetMemberID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryEditingCriteriaSetID]
	, [SqlQueryEditingColumnID]
	, [Criteria1]
	, [Criteria2]
    FROM
	[dbo].[SqlQueryEditingCriteriaSetMember] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingCriteriaSetMemberID] = @SqlQueryEditingCriteriaSetMemberID OR @SqlQueryEditingCriteriaSetMemberID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID OR @SqlQueryEditingCriteriaSetID IS NULL)
	AND ([SqlQueryEditingColumnID] = @SqlQueryEditingColumnID OR @SqlQueryEditingColumnID IS NULL)
	AND ([Criteria1] = @Criteria1 OR @Criteria1 IS NULL)
	AND ([Criteria2] = @Criteria2 OR @Criteria2 IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingCriteriaSetMemberID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryEditingCriteriaSetID]
	, [SqlQueryEditingColumnID]
	, [Criteria1]
	, [Criteria2]
    FROM
	[dbo].[SqlQueryEditingCriteriaSetMember] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingCriteriaSetMemberID] = @SqlQueryEditingCriteriaSetMemberID AND @SqlQueryEditingCriteriaSetMemberID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID AND @SqlQueryEditingCriteriaSetID is not null)
	OR ([SqlQueryEditingColumnID] = @SqlQueryEditingColumnID AND @SqlQueryEditingColumnID is not null)
	OR ([Criteria1] = @Criteria1 AND @Criteria1 is not null)
	OR ([Criteria2] = @Criteria2 AND @Criteria2 is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Find] TO [sp_executeall]
GO
