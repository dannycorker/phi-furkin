SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Bill table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill_GetByLeadID]
(

	@LeadID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[BillID],
					[ClientID],
					[Reference],
					[Description],
					[CreatedDate],
					[DiaryAppointmentID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[BillStatusID]
				FROM
					[dbo].[Bill] WITH (NOLOCK) 
				WHERE
					[LeadID] = @LeadID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_GetByLeadID] TO [sp_executeall]
GO
