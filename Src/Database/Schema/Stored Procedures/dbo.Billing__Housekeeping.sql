SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-03-14
-- Description:	Billing Housekeeping 
-- 2018-12-20	AHOD Update to set all Account which do not have a CPS entry associated with it to expire
-- 2020-03-02	GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[Billing__Housekeeping]
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;

	CREATE TABLE #UpdatedCPS (CustomerPaymentScheduleID INT, CustomerLedgerID INT)

	CREATE TABLE #UpdatedValues (UpdateTable VARCHAR(100), UpdateCount INT)

	DECLARE @UpdateCount INT,
			@ExpiryDate DATE

	DECLARE @CountryID INT

	BEGIN TRY
		BEGIN TRANSACTION
	
		/*
			Update any account references that have an ampersand in them as the billing xml conversion doesnt handle them..
			Update the customer payment schedule to paid where the effective date has passed by 1 day.
			Update any associated purchased product payment schedule records as required.
			Update any payment records 
		*/

		/*2018-07-20 ACE - Make sure we dont have any ampersands in our account mandate refs..*/
		UPDATE a
		SET Reference = REPLACE(Reference, '&', ''), AccountHolderName  = REPLACE(AccountHolderName, '&', '')
		FROM Account a 
		WHERE (a.Reference LIKE '%&%' or a.AccountHolderName like '%&%')

		SELECT @UpdateCount = @@ROWCOUNT

		INSERT INTO #UpdatedValues (UpdateTable, UpdateCount)
		VALUES ('AccountRefFixes', @UpdateCount)

		/*
			2018-07-05 ACE Use the CPS Actual Collection Date not the effective date (Defect 758)
		*/
		UPDATE cps
		SET PaymentStatusID=6 /*Paid*/
		OUTPUT inserted.CustomerPaymentScheduleID, inserted.CustomerLedgerID INTO #UpdatedCPS (CustomerPaymentScheduleID, CustomerLedgerID)
		FROM CustomerPaymentSchedule cps
		INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID
		INNER JOIN Customerledger cl WITH ( NOLOCK ) on cl.CustomerLedgerID = cps.CustomerLedgerID 
		WHERE cps.PaymentStatusID = 2 
		/*2018-06-22 ACE - Changed to today..*/
		/*AND cl.EffectivePaymentDate <= DATEADD(DAY,-1,dbo.fn_GetDate_Local())*/
		--AND cl.EffectivePaymentDate <= dbo.fn_GetDate_Local()
		AND cps.ActualCollectionDate <= dbo.fn_GetDate_Local()
		AND a.AccountTypeID = 1 -- bank (card is 2)
	
		SELECT @UpdateCount = @@ROWCOUNT

		INSERT INTO #UpdatedValues (UpdateTable, UpdateCount)
		VALUES ('CustomerPaymentSchedule', @UpdateCount)

		/**/
		UPDATE ppps
		SET PaymentStatusID=6
		FROM PurchasedProductPaymentSchedule ppps
		INNER JOIN #UpdatedCPS cps ON  ppps.CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID
	
		SELECT @UpdateCount = @@ROWCOUNT

		INSERT INTO #UpdatedValues (UpdateTable, UpdateCount)
		VALUES ('PurchasedProductPaymentSchedule', @UpdateCount)

		UPDATE p
		SET DateReceived=cl.EffectivePaymentDate,
			PaymentStatusID=6
		FROM Payment p
		INNER JOIN #UpdatedCPS cps ON  p.CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID
		INNER JOIN Customerledger cl WITH ( NOLOCK ) on cl.CustomerLedgerID = cps.CustomerLedgerID 

		SELECT @UpdateCount = @@ROWCOUNT

		INSERT INTO #UpdatedValues (UpdateTable, UpdateCount)
		VALUES ('Payment', @UpdateCount)

		/*GPR 2020-03-02 for AAG-202*/
		SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
		WHERE cl.ClientID = @ClientID
		
		IF @CountryID = 14 /*Australia*/
		BEGIN

			UPDATE AccountMandate 
			SET MandateStatusID = 3 
			WHERE MandateStatusID = 2 
			AND FirstAcceptablePaymentDate <= dbo.fn_GetDate_Local()  

			SELECT @UpdateCount = @@ROWCOUNT

			INSERT INTO #UpdatedValues (UpdateTable, UpdateCount)
			VALUES ('AccountMandate', @UpdateCount)

		END

		SELECT *
		FROM #UpdatedValues
		
		/*2018-12-20 AHOD Update to set all Account which do not have a CPS entry associated with it to expire*/
		
		SELECT @ExpiryDate=dbo.fnAddWorkingDays (dbo.fn_GetDate_Local(),15)
		
		UPDATE a
		SET a.ExpiryDate = @ExpiryDate
		FROM Account a WITH ( NOLOCK ) 
		WHERE a.ExpiryDate IS NULL
		AND NOT EXISTS (SELECT * FROM CustomerPaymentSchedule cps WITH ( NOLOCK ) WHERE cps.AccountID = a.AccountID AND cps.PaymentStatusID in (1,2,5))
		AND a.Active = 1
		AND a.AccountUseID = 1
		AND a.AccountTypeID = 1
		AND a.ClientAccountID IN (1, 8)
		
		COMMIT

	END TRY
	BEGIN CATCH
		
		ROLLBACK

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 

	END CATCH

	DROP TABLE #UpdatedCPS 
	DROP TABLE #UpdatedValues

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__Housekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__Housekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__Housekeeping] TO [sp_executeall]
GO
