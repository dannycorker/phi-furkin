SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2007-08-10
-- Description:	Get Appointments by user and date range
-- UPDATE:		Simon Brushett
--				2011-07-04
--				Added EndDate and AllDayEvent for calendar
-- =============================================
CREATE PROCEDURE [dbo].[GetDiaryAppointmentsByUser]
	-- Add the parameters for the stored procedure here
	@ClientPersonnelID int, 
	@FromDate datetime = '1901-01-01', 
	@ToDate datetime = '2499-01-01',
	@ShowCompleted bit 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Reminders can exist for multiple personnel.
	-- Appointment only occurs once, no matter how many people are invited.
	SELECT 
		da.DiaryAppointmentID, 
		da.ClientID, 
		da.DiaryAppointmentTitle, 
		da.DiaryAppointmentText, 
		ClientPersonnel.UserName, 
		da.DueDate, 
		CASE da.Completed WHEN 1 THEN 'Yes' ELSE '' END as 'Completed', 
		da.LeadID, 
		da.CaseID, 
		dr.ReminderDate,
		l.LeadRef,
		c.CaseRef,
		da.ExportVersion,
		da.EndDate,
		da.AllDayEvent  
	FROM DiaryReminder dr 
	INNER JOIN DiaryAppointment da ON da.DiaryAppointmentID = dr.DiaryAppointmentID 
	INNER JOIN ClientPersonnel ON da.CreatedBy = ClientPersonnel.ClientPersonnelID
	LEFT OUTER JOIN Lead l ON l.LeadID = da.LeadID 
	LEFT OUTER JOIN Cases c ON c.CaseID = da.CaseID 
	WHERE dr.ClientPersonnelID = @ClientPersonnelID 
	--AND da.DueDate BETWEEN @FromDate AND @ToDate 
	--AND da.Completed <= @ShowCompleted 
	AND 
	(
		(@ShowCompleted = 1 AND da.Completed = 1 AND da.DueDate BETWEEN @FromDate AND @ToDate) 
			OR
		(da.Completed = 0 AND da.DueDate <= @ToDate) 
	)
	ORDER BY da.DueDate ASC, dr.ReminderDate ASC, da.DiaryAppointmentTitle
	-- if da.Completed = 1 then it will only show up if the
	-- user has set @ShowCompleted to 1 as well.

END










GO
GRANT VIEW DEFINITION ON  [dbo].[GetDiaryAppointmentsByUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDiaryAppointmentsByUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDiaryAppointmentsByUser] TO [sp_executeall]
GO
