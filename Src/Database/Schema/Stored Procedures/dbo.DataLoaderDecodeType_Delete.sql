SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderDecodeType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDecodeType_Delete]
(

	@DataLoaderDecodeTypeID int   
)
AS


				DELETE FROM [dbo].[DataLoaderDecodeType] WITH (ROWLOCK) 
				WHERE
					[DataLoaderDecodeTypeID] = @DataLoaderDecodeTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDecodeType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Delete] TO [sp_executeall]
GO
