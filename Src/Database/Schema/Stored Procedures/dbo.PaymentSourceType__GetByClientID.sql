SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 02-08-2016
-- Description:	Gets a list of payment source types for the given client, includes client zero payment source types
-- =============================================
CREATE PROCEDURE [dbo].[PaymentSourceType__GetByClientID]

	@ClientID INT

AS
BEGIN	
	
	SET NOCOUNT ON;

	SELECT * FROM PaymentSourceType WITH (NOLOCK) WHERE ClientID=@ClientID OR ClientID=0
    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentSourceType__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentSourceType__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentSourceType__GetByClientID] TO [sp_executeall]
GO
