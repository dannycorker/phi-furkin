SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- ALTER date:	2014-07-02
-- Description:	MarkPolicyAsPaid
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MarkPolicyAsPaid]
(
	@TableRowID INT
)
AS
BEGIN

--declare @TableRowID INT = 21837934
 
	SET NOCOUNT ON;
	
	DECLARE @AqAutomation INT,
			@DatePaid VARCHAR(10),
			@TransactionDateTime VARCHAR(16),
			@MatterID INT,
			@NewCollectionsTableRowID INT,
			@PremiumTableRowID INT
	
	SELECT @AqAutomation=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (tr.ClientID,53,'CFG|AqAutomationCPID',0) 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE TableRowID=@TableRowID
	
	-- collections table (add paid row)
	EXEC @NewCollectionsTableRowID=_C600_CopyTableRow @TableRowID, NULL, NULL
	
	SELECT @TransactionDateTime=CONVERT(VARCHAR(16),dbo.fn_GetDate_Local(),120)
	
	EXEC _C00_SimpleValueIntoField 175467,@TransactionDateTime,@NewCollectionsTableRowID,@AqAutomation
	EXEC _C00_SimpleValueIntoField 175469,72150,@NewCollectionsTableRowID,@AqAutomation

	-- premiums table
	SELECT @PremiumTableRowID=ValueInt,
			@DatePaid=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
	FROM TableDetailValues WITH (NOLOCK) WHERE DetailFieldID=175471 AND TableRowID=@TableRowID
	
	-- status
	EXEC _C00_SimpleValueIntoField 170078,69898,@PremiumTableRowID,@AqAutomation
	-- Date Paid
	EXEC _C00_SimpleValueIntoField 175473,@DatePaid,@PremiumTableRowID,@AqAutomation
	
	-- Add Next Event on collections lead
	SELECT @MatterID=tr.MatterID FROM TableRows tr WITH (NOLOCK) WHERE tr.TableRowID=@TableRowID 
	EXEC _C600_AddAutomatedEvent @MatterID, 155289, 1
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MarkPolicyAsPaid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MarkPolicyAsPaid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MarkPolicyAsPaid] TO [sp_executeall]
GO
