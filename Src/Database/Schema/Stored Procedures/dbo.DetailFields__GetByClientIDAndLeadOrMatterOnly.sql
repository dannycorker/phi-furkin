SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-08-2012
-- Description:	Gets the detail field information for the given detail field subtype
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetByClientIDAndLeadOrMatterOnly] 

	@ClientID int, 	
	@LeadOrMatter int 

AS
BEGIN
				
				SELECT *					
				FROM dbo.fnDetailFieldsShared(@ClientID)
				WHERE
					[ClientID] = @ClientID AND					
					[LeadOrMatter] = @LeadOrMatter AND
					[Enabled] = 1 
				ORDER BY [FieldName]	ASC			

END







GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatterOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatterOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatterOnly] TO [sp_executeall]
GO
