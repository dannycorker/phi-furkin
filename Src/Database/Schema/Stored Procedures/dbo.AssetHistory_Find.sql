SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AssetHistory table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetHistory_Find]
(

	@SearchUsingOR bit   = null ,

	@AssetHistoryID int   = null ,

	@AssetID int   = null ,

	@ClientID int   = null ,

	@AssetTypeID int   = null ,

	@AssetSubTypeID int   = null ,

	@LocationID int   = null ,

	@AssetName varchar (255)  = null ,

	@Note varchar (255)  = null ,

	@SecureNote varchar (255)  = null ,

	@SecureUserName varchar (255)  = null ,

	@SecurePassword varchar (255)  = null ,

	@SecureOther varchar (255)  = null ,

	@Version varchar (255)  = null ,

	@ValidFrom datetime   = null ,

	@ValidTo datetime   = null ,

	@ReminderDue datetime   = null ,

	@ReminderTimeUnitID int   = null ,

	@ReminderTimeUnitQuantity int   = null ,

	@ReminderNotificationGroupID int   = null ,

	@Enabled bit   = null ,

	@Deleted bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AssetHistoryID]
	, [AssetID]
	, [ClientID]
	, [AssetTypeID]
	, [AssetSubTypeID]
	, [LocationID]
	, [AssetName]
	, [Note]
	, [SecureNote]
	, [SecureUserName]
	, [SecurePassword]
	, [SecureOther]
	, [Version]
	, [ValidFrom]
	, [ValidTo]
	, [ReminderDue]
	, [ReminderTimeUnitID]
	, [ReminderTimeUnitQuantity]
	, [ReminderNotificationGroupID]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[AssetHistory] WITH (NOLOCK) 
    WHERE 
	 ([AssetHistoryID] = @AssetHistoryID OR @AssetHistoryID IS NULL)
	AND ([AssetID] = @AssetID OR @AssetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([AssetTypeID] = @AssetTypeID OR @AssetTypeID IS NULL)
	AND ([AssetSubTypeID] = @AssetSubTypeID OR @AssetSubTypeID IS NULL)
	AND ([LocationID] = @LocationID OR @LocationID IS NULL)
	AND ([AssetName] = @AssetName OR @AssetName IS NULL)
	AND ([Note] = @Note OR @Note IS NULL)
	AND ([SecureNote] = @SecureNote OR @SecureNote IS NULL)
	AND ([SecureUserName] = @SecureUserName OR @SecureUserName IS NULL)
	AND ([SecurePassword] = @SecurePassword OR @SecurePassword IS NULL)
	AND ([SecureOther] = @SecureOther OR @SecureOther IS NULL)
	AND ([Version] = @Version OR @Version IS NULL)
	AND ([ValidFrom] = @ValidFrom OR @ValidFrom IS NULL)
	AND ([ValidTo] = @ValidTo OR @ValidTo IS NULL)
	AND ([ReminderDue] = @ReminderDue OR @ReminderDue IS NULL)
	AND ([ReminderTimeUnitID] = @ReminderTimeUnitID OR @ReminderTimeUnitID IS NULL)
	AND ([ReminderTimeUnitQuantity] = @ReminderTimeUnitQuantity OR @ReminderTimeUnitQuantity IS NULL)
	AND ([ReminderNotificationGroupID] = @ReminderNotificationGroupID OR @ReminderNotificationGroupID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AssetHistoryID]
	, [AssetID]
	, [ClientID]
	, [AssetTypeID]
	, [AssetSubTypeID]
	, [LocationID]
	, [AssetName]
	, [Note]
	, [SecureNote]
	, [SecureUserName]
	, [SecurePassword]
	, [SecureOther]
	, [Version]
	, [ValidFrom]
	, [ValidTo]
	, [ReminderDue]
	, [ReminderTimeUnitID]
	, [ReminderTimeUnitQuantity]
	, [ReminderNotificationGroupID]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[AssetHistory] WITH (NOLOCK) 
    WHERE 
	 ([AssetHistoryID] = @AssetHistoryID AND @AssetHistoryID is not null)
	OR ([AssetID] = @AssetID AND @AssetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([AssetTypeID] = @AssetTypeID AND @AssetTypeID is not null)
	OR ([AssetSubTypeID] = @AssetSubTypeID AND @AssetSubTypeID is not null)
	OR ([LocationID] = @LocationID AND @LocationID is not null)
	OR ([AssetName] = @AssetName AND @AssetName is not null)
	OR ([Note] = @Note AND @Note is not null)
	OR ([SecureNote] = @SecureNote AND @SecureNote is not null)
	OR ([SecureUserName] = @SecureUserName AND @SecureUserName is not null)
	OR ([SecurePassword] = @SecurePassword AND @SecurePassword is not null)
	OR ([SecureOther] = @SecureOther AND @SecureOther is not null)
	OR ([Version] = @Version AND @Version is not null)
	OR ([ValidFrom] = @ValidFrom AND @ValidFrom is not null)
	OR ([ValidTo] = @ValidTo AND @ValidTo is not null)
	OR ([ReminderDue] = @ReminderDue AND @ReminderDue is not null)
	OR ([ReminderTimeUnitID] = @ReminderTimeUnitID AND @ReminderTimeUnitID is not null)
	OR ([ReminderTimeUnitQuantity] = @ReminderTimeUnitQuantity AND @ReminderTimeUnitQuantity is not null)
	OR ([ReminderNotificationGroupID] = @ReminderNotificationGroupID AND @ReminderNotificationGroupID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Find] TO [sp_executeall]
GO
