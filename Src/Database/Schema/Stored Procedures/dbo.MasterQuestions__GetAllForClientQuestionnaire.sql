SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 22/10/08
-- Description: Gets all records from the MasterQuestions table for the given ClientQuestionnaireID
-- =============================================

CREATE PROCEDURE [dbo].[MasterQuestions__GetAllForClientQuestionnaire]
	@ClientQuestionnaireID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
		mq.[MasterQuestionID],
		mq.[ClientQuestionnaireID],
		mq.[QuestionTypeID],
		qt.[Name] AS 'QuestionTypeName',
		mq.[QuestionText],
		mq.[QuestionOrder],
		mq.[DefaultAnswerID],
		qpa.[AnswerText] AS 'DefaultAnswer',
		mq.[Mandatory],
		mq.[QuestionToolTip],
		mq.[LinkedQuestionnaireMasterQuestionID],
		mq.[Active],
		mq.[MasterQuestionStatus],
		mqs.[MasterQuestionStatusName],
		mq.[TextboxHeight],
		mq.[ClientID],
		mq.[AnswerPosition],
		mq.[DisplayAnswerAs],
		mq.[NumberOfAnswersPerRow]
	  FROM
		[dbo].[MasterQuestions] mq
	  INNER JOIN QuestionTypes qt ON qt.QuestionTypeID = mq.QuestionTypeID
	  LEFT JOIN QuestionPossibleAnswers qpa ON qpa.QuestionPossibleAnswerID = mq.DefaultAnswerID
	  LEFT JOIN MasterQuestionStatus mqs ON mqs.MasterQuestionStatusID = mq.MasterQuestionStatus
	  WHERE
		mq.ClientQuestionnaireID = @ClientQuestionnaireID
	  AND
		mq.MasterQuestionStatus <> 3
	  ORDER BY
		mq.QuestionOrder ASC
				
END
			






GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions__GetAllForClientQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions__GetAllForClientQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions__GetAllForClientQuestionnaire] TO [sp_executeall]
GO
