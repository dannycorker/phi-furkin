SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the MatterPageChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_Insert]
(

	@MatterPageChoiceID int    OUTPUT,

	@ClientID int   ,

	@MatterID int   ,

	@DetailFieldPageID int   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[MatterPageChoice]
					(
					[ClientID]
					,[MatterID]
					,[DetailFieldPageID]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@MatterID
					,@DetailFieldPageID
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @MatterPageChoiceID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Insert] TO [sp_executeall]
GO
