SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-02-29
-- Description:	Tell us all about a Client Questionnaire
--				CS 2013-09-10 - Added fn_C00_GetUrlByDatabase() to control URLs
-- =============================================

CREATE PROC [dbo].[ClientQuestionnaire__Describe] 
	@ClientQuestionnaireID int = null 
AS
BEGIN

	SELECT ClientQuestionnaireID, ClientID, QuestionnaireTitle, QuestionnaireDescription, QuestionsPerPage, QuestionnaireLogoFileName, QuestionnaireFlowDiagram, QuestionnaireIntroductionText, HideIntro, QuestionnaireFooterText, HideFooter, QuestionnaireAddressLookup, Published, CustomerInformationAtStart, LinkedQuestionnaireClientQuestionnaireID, QuestionnaireFooterIframe, QuestionnaireHeaderIframe, QuestionnaireFooterInternal, QuestionnaireHeaderInternal, QuestionnaireFooterIframeHeight, QuestionnaireHeaderIframeHeight, QuestionnaireFooterIframeWidth, QuestionnaireHeaderIframeWidth, QuestionnaireBackGroundImage, QuestionnaireBackGroundImageFileName, DefaultEmailAddress, MailingListType, FrameMode, LayoutCss, LayoutCssFileName, ImportDirectlyIntoLeadManager, RunAsClientPersonnelID, RememberAnswers
	FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) 
	WHERE cq.ClientQuestionnaireID = @ClientQuestionnaireID

	SELECT mq.MasterQuestionID, mq.QuestionText, mq.QuestionOrder, mq.Mandatory, qt.QuestionTypeID, qt.Name, count(isnull(pa.QuestionPossibleAnswerID,1)) [Possible Answers], df.DetailFieldID [Links to Field], df.FieldName, df.LeadTypeID, df.LeadOrMatter, df.Enabled 
	FROM dbo.MasterQuestions mq WITH (NOLOCK) 
	INNER JOIN dbo.QuestionTypes qt WITH (NOLOCK) on qt.QuestionTypeID = mq.QuestionTypeID 
	LEFT JOIN dbo.QuestionPossibleAnswers pa WITH (NOLOCK) on pa.MasterQuestionID = mq.MasterQuestionID
	LEFT JOIN dbo.DetailFieldLink fl WITH (NOLOCK) on fl.MasterQuestionID = mq.MasterQuestionID 
	LEFT JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = fl.DetailFieldID 
	WHERE mq.ClientQuestionnaireID = @ClientQuestionnaireID
	and (mq.MasterQuestionStatus = 1 or mq.ClientID = 204)
	GROUP BY mq.MasterQuestionID, mq.QuestionText, mq.QuestionOrder, mq.Mandatory, qt.QuestionTypeID, qt.Name, df.DetailFieldID, df.FieldName, df.LeadTypeID, df.LeadOrMatter, df.Enabled 
	ORDER BY mq.QuestionOrder, df.LeadTypeID

	SELECT o.OutcomeID, o.OutcomeName, COUNT(co.CustomerOutcomeID) [Total], lt.LeadTypeID, lt.LeadTypeName, o.EmailCustomer
	FROM dbo.Outcomes o WITH (NOLOCK) 
	LEFT JOIN dbo.CustomerOutcomes co WITH (NOLOCK) on co.OutcomeID = o.OutcomeID 
	LEFT JOIN dbo.LeadTypeLink l WITH (NOLOCK) on l.OutcomeID = o.OutcomeID 
	LEFT JOIN dbo.LeadType lt WITH (NOLOCK) on lt.LeadTypeID = l.LeadTypeID 
	WHERE o.ClientQuestionnaireID = @ClientQuestionnaireID 
	GROUP BY o.OutcomeID, o.OutcomeName, lt.LeadTypeID, lt.LeadTypeName , o.EmailCustomer
	union
	SELECT '0' [OutcomeID], 'Total' [OutcomeName],  COUNT(*) [Total], 0 [LeadTypeID], '' [LeadTypeName], ''
	FROM dbo.CustomerQuestionnaires cq WITH (NOLOCK) 
	WHERE cq.ClientQuestionnaireID = @ClientQuestionnaireID 
		
	SELECT top(20) cu.Test,cu.Fullname [Last 20 Customers], cu.CustomerID, l.LeadID, m.MatterID, cq.SubmissionDate, cq.CustomerQuestionnaireID, cq.TrackingID, t.TrackingName, co.CustomerOutcomeID , o.OutcomeName, isnull('exec dbo._C00_SetCustomerToTest ' + convert(varchar,c.LatestLeadEventID)+ ', ' + CASE cu.Test WHEN 1 THEN '0' ELSE '1' END,'-no events applied-')  [Toggle Test Flag]
	FROM dbo.Customers cu WITH (NOLOCK) 
	LEFT JOIN dbo.Lead l WITH (NOLOCK) on l.CustomerID = cu.CustomerID 
	INNER JOIN dbo.CustomerQuestionnaires cq WITH (NOLOCK) on cq.CustomerID = cu.CustomerID and cq.ClientQuestionnaireID = @ClientQuestionnaireID
	LEFT JOIN dbo.Tracking t WITH (NOLOCK) on t.TrackingID = cq.TrackingID
	LEFT JOIN dbo.CustomerOutcomes co WITH (NOLOCK) on co.CustomerID = cu.CustomerID
	LEFT JOIN dbo.Outcomes o WITH (NOLOCK) on o.OutcomeID = co.OutcomeID
	LEFT JOIN dbo.Cases c WITH (NOLOCK) on c.LeadID = l.LeadID and c.CaseNum = 1
	LEFT JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.CaseID 
	ORDER BY cq.CustomerQuestionnaireID desc

	SELECT t.TrackingID, t.TrackingName, t.StartDate, t.EndDate, COUNT(cq.CustomerQuestionnaireID) [Submitted], replace(dbo.fn_C00_GetUrlByDatabase() + 'QuestionnairePreview.aspx?id=@Questionnaire&trackingID=','@Questionnaire',@ClientQuestionnaireID) + convert(varchar,t.TrackingID) [URL]
	FROM dbo.Tracking t WITH (NOLOCK) 
	LEFT JOIN dbo.CustomerQuestionnaires cq WITH (NOLOCK) on cq.TrackingID = t.TrackingID
	WHERE t.ClientQuestionnaireID = @ClientQuestionnaireID
	GROUP BY t.TrackingID, t.TrackingName, t.StartDate, t.EndDate
	ORDER BY t.TrackingID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaire__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaire__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaire__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaire__Describe] TO [sp_executehelper]
GO
