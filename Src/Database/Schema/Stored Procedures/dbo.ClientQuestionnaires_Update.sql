SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaires_Update]
(

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@QuestionnaireTitle varchar (255)  ,

	@QuestionnaireDescription varchar (MAX)  ,

	@QuestionsPerPage int   ,

	@QuestionnaireLogo image   ,

	@QuestionnaireLogoFileName varchar (512)  ,

	@QuestionnaireFlowDiagram varchar (MAX)  ,

	@QuestionnaireIntroductionText varchar (MAX)  ,

	@HideIntro bit   ,

	@QuestionnaireFooterText varchar (MAX)  ,

	@HideFooter bit   ,

	@QuestionnaireAddressLookup bit   ,

	@Published bit   ,

	@CustomerInformationAtStart bit   ,

	@LinkedQuestionnaireClientQuestionnaireID int   ,

	@QuestionnaireFooterIframe varchar (MAX)  ,

	@QuestionnaireHeaderIframe varchar (MAX)  ,

	@QuestionnaireFooterInternal bit   ,

	@QuestionnaireHeaderInternal bit   ,

	@QuestionnaireFooterIframeHeight int   ,

	@QuestionnaireHeaderIframeHeight int   ,

	@QuestionnaireFooterIframeWidth int   ,

	@QuestionnaireHeaderIframeWidth int   ,

	@QuestionnaireBackGroundImage image   ,

	@QuestionnaireBackGroundImageFileName varchar (512)  ,

	@DefaultEmailAddress varchar (255)  ,

	@MailingListType int   ,

	@FrameMode bit   ,

	@LayoutCss varchar (MAX)  ,

	@LayoutCssFileName varchar (512)  ,

	@ImportDirectlyIntoLeadManager bit   ,

	@RunAsClientPersonnelID int   ,

	@RememberAnswers bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientQuestionnaires]
				SET
					[ClientID] = @ClientID
					,[QuestionnaireTitle] = @QuestionnaireTitle
					,[QuestionnaireDescription] = @QuestionnaireDescription
					,[QuestionsPerPage] = @QuestionsPerPage
					,[QuestionnaireLogo] = @QuestionnaireLogo
					,[QuestionnaireLogoFileName] = @QuestionnaireLogoFileName
					,[QuestionnaireFlowDiagram] = @QuestionnaireFlowDiagram
					,[QuestionnaireIntroductionText] = @QuestionnaireIntroductionText
					,[HideIntro] = @HideIntro
					,[QuestionnaireFooterText] = @QuestionnaireFooterText
					,[HideFooter] = @HideFooter
					,[QuestionnaireAddressLookup] = @QuestionnaireAddressLookup
					,[Published] = @Published
					,[CustomerInformationAtStart] = @CustomerInformationAtStart
					,[LinkedQuestionnaireClientQuestionnaireID] = @LinkedQuestionnaireClientQuestionnaireID
					,[QuestionnaireFooterIframe] = @QuestionnaireFooterIframe
					,[QuestionnaireHeaderIframe] = @QuestionnaireHeaderIframe
					,[QuestionnaireFooterInternal] = @QuestionnaireFooterInternal
					,[QuestionnaireHeaderInternal] = @QuestionnaireHeaderInternal
					,[QuestionnaireFooterIframeHeight] = @QuestionnaireFooterIframeHeight
					,[QuestionnaireHeaderIframeHeight] = @QuestionnaireHeaderIframeHeight
					,[QuestionnaireFooterIframeWidth] = @QuestionnaireFooterIframeWidth
					,[QuestionnaireHeaderIframeWidth] = @QuestionnaireHeaderIframeWidth
					,[QuestionnaireBackGroundImage] = @QuestionnaireBackGroundImage
					,[QuestionnaireBackGroundImageFileName] = @QuestionnaireBackGroundImageFileName
					,[DefaultEmailAddress] = @DefaultEmailAddress
					,[MailingListType] = @MailingListType
					,[FrameMode] = @FrameMode
					,[LayoutCss] = @LayoutCss
					,[LayoutCssFileName] = @LayoutCssFileName
					,[ImportDirectlyIntoLeadManager] = @ImportDirectlyIntoLeadManager
					,[RunAsClientPersonnelID] = @RunAsClientPersonnelID
					,[RememberAnswers] = @RememberAnswers
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[ClientQuestionnaireID] = @ClientQuestionnaireID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Update] TO [sp_executeall]
GO
