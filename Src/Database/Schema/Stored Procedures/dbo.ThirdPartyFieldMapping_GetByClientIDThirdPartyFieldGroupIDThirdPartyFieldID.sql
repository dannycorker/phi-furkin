SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_GetByClientIDThirdPartyFieldGroupIDThirdPartyFieldID]
(

	@ClientID int   ,

	@ThirdPartyFieldGroupID int   ,

	@ThirdPartyFieldID int   
)
AS


				SELECT
					[ThirdPartyFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[ThirdPartyFieldID],
					[DetailFieldID],
					[ColumnFieldID],
					[ThirdPartyFieldGroupID],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
					AND [ThirdPartyFieldID] = @ThirdPartyFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_GetByClientIDThirdPartyFieldGroupIDThirdPartyFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_GetByClientIDThirdPartyFieldGroupIDThirdPartyFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_GetByClientIDThirdPartyFieldGroupIDThirdPartyFieldID] TO [sp_executeall]
GO
