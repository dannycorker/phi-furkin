SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChartSetting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSetting_Insert]
(

	@ChartSettingID int    OUTPUT,

	@ChartID int   ,

	@SettingName varchar (100)  ,

	@SettingValue varchar (100)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[ChartSetting]
					(
					[ChartID]
					,[SettingName]
					,[SettingValue]
					,[ClientID]
					)
				VALUES
					(
					@ChartID
					,@SettingName
					,@SettingValue
					,@ClientID
					)
				-- Get the identity value
				SET @ChartSettingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSetting_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Insert] TO [sp_executeall]
GO
