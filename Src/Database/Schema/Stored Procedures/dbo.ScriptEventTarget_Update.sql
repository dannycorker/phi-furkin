SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptEventTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_Update]
(

	@ScriptEventTargetID int   ,

	@ScriptID int   ,

	@SectionID int   ,

	@ClientID int   ,

	@Target varchar (250)  ,

	@EventTypeID int   ,

	@EventTypeName varchar (250)  ,

	@EventSubtypeID int   ,

	@Format varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptEventTarget]
				SET
					[ScriptID] = @ScriptID
					,[SectionID] = @SectionID
					,[ClientID] = @ClientID
					,[Target] = @Target
					,[EventTypeID] = @EventTypeID
					,[EventTypeName] = @EventTypeName
					,[EventSubtypeID] = @EventSubtypeID
					,[Format] = @Format
				WHERE
[ScriptEventTargetID] = @ScriptEventTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Update] TO [sp_executeall]
GO
