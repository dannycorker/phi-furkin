SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-05-12
-- Description:	Delete matter data up for DPA 1996
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDataForDPA]
	@ClientID INT,
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

	/*If it's not the no triggers user reject the attempt*/
	IF system_user <> 'notriggers'
	BEGIN
		RETURN
	END

	/*Create a temporary table to hold the ID's that we are going to remove*/
	DECLARE @CustomerID INT, @LeadID INT, @CaseID INT
	
	DECLARE @MatterCountForCase INT,
			@CaseCountForLead INT,
			@LeadCountForCustomer INT
	
	SELECT @CustomerID = m.CustomerID, @LeadID = m.LeadID, @CaseID = m.CaseID
	FROM Matter m WITH (NOLOCK)
	WHERE m.MatterID = @MatterID
	AND m.ClientID = @ClientID
	
	IF @@ROWCOUNT = 0
	BEGIN
	
		PRINT 'Either this Matter Doesnt exist or belongs to a different client'
	
	END
	
	/*So we have some matter info to remove. Lets get on with it...*/
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDataForDPA] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteDataForDPA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDataForDPA] TO [sp_executeall]
GO
