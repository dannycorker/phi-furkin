SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteCustomerAnswer] @CustomerQuestionnaireID int 
AS
BEGIN
Delete from CustomerAnswers where CustomerQuestionnaireID = @CustomerQuestionnaireID;
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteCustomerAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteCustomerAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteCustomerAnswer] TO [sp_executeall]
GO
