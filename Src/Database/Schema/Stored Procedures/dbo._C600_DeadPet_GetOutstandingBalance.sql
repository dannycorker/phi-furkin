SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-09-07
-- Description:	Retrieve the remaining unpaid premiums if date of death entered during claims processing 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_DeadPet_GetOutstandingBalance]
		@CaseID INT ,
		@LeadEventID INT = NULL,
		@PetDoD DATE
		
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @WhoCreated INT,@ClientID INT = dbo.fnGetPrimaryClientID()
	SELECT @LeadID=cs.LeadID FROM Cases cs WITH (NOLOCK) WHERE cs.CaseID = @CaseID
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@LeadID
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.CaseID=@CaseID
	
	IF ISNULL(@LeadEventID,'') = '' 
	BEGIN 
		SELECT @LeadEventID = c.LatestInProcessLeadEventID FROM Cases c WITH (NOLOCK) where c.CaseID = @CaseID
	END

	SELECT @WhoCreated = le.WhoCreated FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID=@LeadEventID
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
		
	DECLARE @DifferenceInMonthlyPremium NUMERIC(18,2),
			@DifferenceInMonthlyPremiumVAT NUMERIC(18,2),
			@CoverFrom DATETIME,
			@CoverTo DATETIME,
			@CancellationCoverPeriodGross NUMERIC(18,2),
			@WithinAdjustmentDatePaymentVAT NUMERIC(18,2),
			@NumberOfDaysInPeriod INT,
			@NumberOfDaysLeftInPeriod INT,
			@OneOffPaymentForCancellationPeriod NUMERIC(18,2),
			@OneOffPaymentForCancellationPeriodVAT NUMERIC(18,2),
			@PaymentForRemainderOfSchedule NUMERIC(18,2),
			@PaymentNetForRemainderOfSchedule NUMERIC(18,2),
			@PaymentVATForRemainderOfSchedule NUMERIC(18,2),
			@PurchasedProductPaymentScheduleID INT,
			@OneOffPaymentGross NUMERIC(18,2),
			@OneOffPaymentNet NUMERIC(18,2),
			@OneOffPaymentVAT NUMERIC(18,2),
			@NewCoverTo DATETIME,
			@NewCoverFrom DATETIME,
			@NewPaymentDate DATETIME,
			@PolicyLeadID INT = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter (@MatterID),
			@PolicyMatterID INT = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID),
			@AccountID INT		
			
		DECLARE @NewSchedule TABLE
	(
		ID INT,	
		CoverFrom DATETIME, 
		CoverTo DATETIME, 
		AccountID INT, 
		PaymentNet NUMERIC(18,2),
		PaymentVAT NUMERIC(18,2),
		PaymentGross NUMERIC(18,2)		
	)

	/*First Get the purchasedproduct ID*/ 
	DECLARE @PurchasedProductID INT = dbo.fnGetSimpleDvAsINT(177074,@PolicyMAtterID) 
		
	-- get payment schedule data for cancellation date
	SELECT	@CoverFrom=CoverFrom, 
			@CoverTo=CoverTo, 
			@CancellationCoverPeriodGross=PaymentGross, 						
			@WithinAdjustmentDatePaymentVAT=PaymentVAT,
			@NewPaymentDate=PaymentDate
	FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
	WHERE CoverFrom<=@PetDoD AND CoverTo>=@PetDoD AND PurchasedProductID=@PurchasedProductID
	
	SET @NumberOfDaysInPeriod = DATEDIFF(DAY,@CoverFrom,@CoverTo) + 1				
	SET @NumberOfDaysLeftInPeriod = DATEDIFF(DAY,@PetDoD,@CoverTo) + 1
	SET @OneOffPaymentForCancellationPeriod = (@CancellationCoverPeriodGross / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod
	SET @OneOffPaymentForCancellationPeriodVAT = (@WithinAdjustmentDatePaymentVAT / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod
	
	-- Calculate remainder up until the first payment that has not been reconciled
	INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID)
	SELECT PurchasedProductPaymentScheduleID, 
		   PaymentGross - PaymentVAT, 
		   PaymentVAT, 
		   PaymentGross,
		   CoverFrom, 
		   CoverTo, 
		   AccountID
	FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
	WHERE CoverFrom>@CoverTo--PaymentDate>@CancellationDate 
	AND PurchasedProductID=@PurchasedProductID AND ReconciledDate IS  NULL
	
	SELECT @PaymentForRemainderOfSchedule = SUM(PaymentGross) FROM @NewSchedule
	SELECT @PaymentNetForRemainderOfSchedule = SUM(PaymentNet) FROM @NewSchedule
	SELECT @PaymentVATForRemainderOfSchedule = SUM(PaymentVAT) FROM @NewSchedule
	
	SELECT @OneOffPaymentGross = ISNULL(@OneOffPaymentForCancellationPeriod + @PaymentForRemainderOfSchedule,0.00)
	SELECT @OneOffPaymentVAT = @OneOffPaymentForCancellationPeriodVAT + @PaymentVATForRemainderOfSchedule
	SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT

	/*Write the outstanding premium to the claims field for deduction*/
	
	/*Enable once tested*/ 
	--EXEC _C00_SimpleValueintoField 176945 ,@OneOffPaymentGross,@MatterID,@WhoCreated
	RETURN @OneOffPaymentGross 
	
	/*Do we now need to update the PurchasesProductPaymentSchedule to remove the payments we've just take?*/ 
	--Update PurchasedProductPaymentSchedule 
	--Set PaymentStatusID = 2 
	--From @NewSchedule n 
	--INNER JOIN PurchasedProductPaymentSchedule p on n.ID =  p.PurchasedProductPaymentScheduleID 
	
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DeadPet_GetOutstandingBalance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_DeadPet_GetOutstandingBalance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DeadPet_GetOutstandingBalance] TO [sp_executeall]
GO
