SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-05-18
-- Description:	SP to copy AutomatedTasks
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__CreateCopy]
(
	@FromAutomatedTaskID INT, 
	@ClientPersonnelID INT, 
	@ClientID INT
)


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @AutomatedTaskID INT = 0,
			@RunAtHour INT, 
			@RunAtMinute INT, 
			@RepeatTimeUnitsID INT, 
			@RepeatTimeQuantity INT, 
			@NextRundateTime DATETIME, 
			@SQLQueryID INT,
			@Description VARCHAR(250),
			@Taskname VARCHAR(100),
			@Enabled INT,
			@WorkflowTask INT,
			@AutomatedTaskGroupID INT,
			@ClientIDCheck INT

	SELECT	@RunAtHour = RunAtHour, 
			@RunAtMinute = RunAtMinute, 
			@RepeatTimeUnitsID = RepeatTimeUnitsID, 
			@RepeatTimeQuantity = RepeatTimeQuantity,
			@NextRundateTime = NextRunDateTime, 
			@ClientIDCheck = at.ClientID,
			@Description = at.Description,
			@Enabled = at.Enabled,
			@WorkflowTask = at.WorkflowTask,
			@AutomatedTaskGroupID = at.AutomatedTaskGroupID
	FROM AutomatedTask AT (NOLOCK)
	WHERE at.TaskID = @FromAutomatedTaskID

	IF @ClientID <> @ClientIDCheck SELECT @AutomatedTaskID = -1

	IF @AutomatedTaskID <> -1
	BEGIN

		INSERT INTO AutomatedTask (ClientID, Taskname, Description, Enabled, RunAtHour, RunAtMinute, RepeatTimeUnitsID, RepeatTimeQuantity, WorkflowTask, NextRunDateTime, AlreadyRunning, AutomatedTaskGroupID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, MaximumAllowableErrors, EventSubTypeThresholding)
		VALUES (@ClientID, @Taskname, @Description, 0, @RunAtHour, @RunAtMinute, @RepeatTimeUnitsID, @RepeatTimeQuantity, @WorkflowTask, @NextRundateTime, 0, @AutomatedTaskGroupID, @FromAutomatedTaskID, @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), 50, 1)

		SELECT @AutomatedTaskID = SCOPE_IDENTITY()

		INSERT INTO AutomatedTaskParam (TaskID, ClientID, ParamName, ParamValue)
		SELECT @AutomatedTaskID, @ClientID, ParamName, ParamValue
		FROM AutomatedTaskParam atp WITH (NOLOCK)
		WHERE atp.TaskID = @FromAutomatedTaskID

	END

	SELECT @AutomatedTaskID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__CreateCopy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__CreateCopy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__CreateCopy] TO [sp_executeall]
GO
