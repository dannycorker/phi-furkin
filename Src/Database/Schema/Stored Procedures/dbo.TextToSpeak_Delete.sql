SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the TextToSpeak table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TextToSpeak_Delete]
(

	@TextToSpeakID int   
)
AS


				DELETE FROM [dbo].[TextToSpeak] WITH (ROWLOCK) 
				WHERE
					[TextToSpeakID] = @TextToSpeakID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TextToSpeak_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Delete] TO [sp_executeall]
GO
