SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkflowTaskCompleted table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTaskCompleted_GetByWorkflowTaskCompletedID]
(

	@WorkflowTaskCompletedID int   
)
AS


				SELECT
					[WorkflowTaskCompletedID],
					[ClientID],
					[WorkflowTaskID],
					[WorkflowGroupID],
					[AutomatedTaskID],
					[Priority],
					[AssignedTo],
					[AssignedDate],
					[LeadID],
					[CaseID],
					[EventTypeID],
					[FollowUp],
					[Important],
					[CreationDate],
					[Escalated],
					[EscalatedBy],
					[EscalationReason],
					[EscalationDate],
					[Disabled],
					[CompletedBy],
					[CompletedOn],
					[CompletionDescription]
				FROM
					[dbo].[WorkflowTaskCompleted] WITH (NOLOCK) 
				WHERE
										[WorkflowTaskCompletedID] = @WorkflowTaskCompletedID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_GetByWorkflowTaskCompletedID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTaskCompleted_GetByWorkflowTaskCompletedID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_GetByWorkflowTaskCompletedID] TO [sp_executeall]
GO
