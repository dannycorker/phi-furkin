SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-02-11
-- Description:	#30930 Used in the MandatoryFieldEditor to override the Editable setting of DetailFields.
--              An EventTypeHelperField with ForceEditableOverride set to 1 will now allow the user to edit it regardless 
--              of all other settings and user rights (so deny them access to the event to lock them out fully).
--              If Mandatory fields are being selected, rather than helper fields, then always set Editable to 1, or this 
--              field has no business being mandatory!
--              View-only rights on the DetailFieldPage level will still stop users in their tracks though.
--              And Encrypted field rights will still be honoured in the app no matter what this proc returns.
--Update Date:2015-07-07
--Updater:		Angel Petrov
--				#33164 Mandatory Fields now have a Forced Editable Override column that acts the same as the one for Helpes Fields.
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetByDetailFieldIDForMHEditor]
(
	@DetailFieldID INT,
	@ClientID INT,
	@EventTypeID INT,
	@MorHorNULL VARCHAR(1) = NULL /* 'M' for Mandatory fields, 'H' for Helper fields, NULL for natural DetailFields results */ 
)
AS
BEGIN
	
	/*
		The [Editable] property is the one we are concerned with here. 
		If this comes back as a 1 then the user stands a chance of editing the field in the Mandatory/Helper field editor.
	*/
	SELECT f.[DetailFieldID],
	f.[ClientID],
	f.[LeadOrMatter],
	f.[FieldName],
	f.[FieldCaption],
	f.[QuestionTypeID],
	f.[Required],
	f.[Lookup],
	f.[LookupListID],
	f.[LeadTypeID],
	f.[Enabled],
	f.[DetailFieldPageID],
	f.[FieldOrder],
	f.[MaintainHistory],
	f.[EquationText],
	f.[MasterQuestionID],
	f.[FieldSize],
	f.[LinkedDetailFieldID],
	f.[ValidationCriteriaFieldTypeID],
	f.[ValidationCriteriaID],
	f.[MinimumValue],
	f.[MaximumValue],
	f.[RegEx],
	f.[ErrorMessage],
	f.[ResourceListDetailFieldPageID],
	f.[TableDetailFieldPageID],
	f.[DefaultFilter],
	f.[ColumnEquationText],
	CAST(CASE 
		WHEN @MorHorNULL = 'M' THEN ISNULL(etmf.ForceEditableOverride, f.[Editable])/* This is for a user to enter a Mandatory field, so it must be editable, #33164 update: This now works like the Helper field case. */
		WHEN @MorHorNULL = 'H' THEN ISNULL(ethf.ForceEditableOverride, f.[Editable]) /* Helper field, might be 1, 0, or just use the DetailField.Editable value if there is one, otherwise 0 */
		ELSE f.[Editable] /* Just use the DetailField.Editable flag as it stands */
	END AS BIT) AS [Editable],
	f.[Hidden],
	f.[LastReferenceInteger],
	f.[ReferenceValueFormatID],
	f.[Encrypt],
	f.[ShowCharacters],
	f.[NumberOfCharactersToShow],
	f.[TableEditMode],
	f.[DisplayInTableView],
	f.[ObjectTypeID],
	f.[SourceID],
	f.[WhoCreated],
	f.[WhenCreated],
	f.[WhoModified],
	f.[WhenModified],
	f.[DetailFieldStyleID],
	f.[Hyperlink],
	f.[IsShared]
	FROM dbo.fnDetailFieldsShared(@ClientID) f 
	LEFT JOIN dbo.EventTypeHelperField ethf WITH (NOLOCK) ON ethf.EventTypeID = @EventTypeID AND ethf.DetailFieldID = f.DetailFieldID 
	LEFT JOIN dbo.EventTypeMandatoryField etmf WITH (NOLOCK) ON etmf.EventTypeID = @EventTypeID And etmf.DetailFieldID = f.DetailFieldID /*This has been added to answer to the requirements in ticket #33164*/
	/* An unnecessary join if @MorHorNULL = 'M' but data volumes are very low */
	WHERE f.[DetailFieldID] = @DetailFieldID 
	
	SELECT @@ROWCOUNT
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByDetailFieldIDForMHEditor] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByDetailFieldIDForMHEditor] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByDetailFieldIDForMHEditor] TO [sp_executeall]
GO
