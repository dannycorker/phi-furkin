SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSection_Insert]
(

	@ScriptSectionID int    OUTPUT,

	@ScriptID int   ,

	@ClientID int   ,

	@SectionTitle varchar (250)  ,

	@SectionOrder int   ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@WhenModified datetime   ,

	@WhoModified int   ,

	@SourceID int   ,

	@NextEventTypeID int   
)
AS


				
				INSERT INTO [dbo].[ScriptSection]
					(
					[ScriptID]
					,[ClientID]
					,[SectionTitle]
					,[SectionOrder]
					,[WhenCreated]
					,[WhoCreated]
					,[WhenModified]
					,[WhoModified]
					,[SourceID]
					,[NextEventTypeID]
					)
				VALUES
					(
					@ScriptID
					,@ClientID
					,@SectionTitle
					,@SectionOrder
					,@WhenCreated
					,@WhoCreated
					,@WhenModified
					,@WhoModified
					,@SourceID
					,@NextEventTypeID
					)
				-- Get the identity value
				SET @ScriptSectionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Insert] TO [sp_executeall]
GO
