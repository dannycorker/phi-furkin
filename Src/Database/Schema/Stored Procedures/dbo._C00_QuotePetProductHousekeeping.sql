SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-02-03
-- Description:	Delete QuotePetProductCheckpoint records that are no longer necessary.  CR021
--	2019-10-11	GPR for LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[_C00_QuotePetProductHousekeeping]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @CutoffDate	DATE = dbo.fn_GetDate_Local()-61
			,@LogEntry		VARCHAR(MAX) = ''
			,@AqAutomation	INT = 58552

	SELECT	 @LogEntry = 'Cutoff Date: ' + CONVERT(VARCHAR,@CutoffDate,121) + CHAR(13)+CHAR(10)

	CREATE TABLE #QuotePetProductIDs ( QuotePetProductID INT, QuotePetID INT )

	/*Pick up a list of the IDs to be cleared*/
	INSERT #QuotePetProductIDs ( QuotePetProductID, QuotePetID )
	SELECT qpp.QuotePetProductID, qpp.QuotePetID
	FROM QuotePetProduct qpp WITH ( NOLOCK ) 
	INNER JOIN dbo.QuotePet qp WITH ( NOLOCK ) on qp.QuotePetID = qpp.QuotePetID
	WHERE qpp.WhenCreated < @CutoffDate
	AND NOT EXISTS ( SELECT *
	                 FROM LeadDetailValues ldv WITH ( NOLOCK ) 
					 WHERE ldv.DetailFieldID = 180288 /*QuotePetProductID*/ /*GPR 2019-10-11 updated to C603 DetailFieldID*/
					 AND ldv.ValueInt = qpp.QuotePetProductID )
	AND qp.LeadEventID is NULL

	DELETE qppc
	FROM QuotePetProductCheckpoint qppc WITH ( NOLOCK ) 
	INNER JOIN #QuotePetProductIDs id on id.QuotePetProductID = qppc.QuotePetProductID
	/*!! NO SQL HERE !!*/
	SELECT @LogEntry += CONVERT(VARCHAR,@@RowCount) + ' QuotePetProductCheckpoint records deleted.' + CHAR(13)+CHAR(10)

	DELETE qpp
	FROM QuotePetProduct qpp WITH ( NOLOCK ) 
	INNER JOIN #QuotePetProductIDs id on id.QuotePetProductID = qpp.QuotePetProductID
	/*!! NO SQL HERE !!*/
	SELECT @LogEntry += CONVERT(VARCHAR,@@RowCount) + ' QuotePetProduct records deleted.' + CHAR(13)+CHAR(10)

	DELETE qp
	FROM QuotePet qp WITH ( NOLOCK ) 
	INNER JOIN #QuotePetProductIDs id on id.QuotePetID = qp.QuotePetID
	/*!! NO SQL HERE !!*/
	SELECT @LogEntry += CONVERT(VARCHAR,@@RowCount) + ' QuotePet records deleted.' + CHAR(13)+CHAR(10)

	EXEC _C00_LogIt 'Info', '_C00_QuotePetProductHousekeeping', 'Records Deleted', @LogEntry, @AqAutomation  

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_QuotePetProductHousekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_QuotePetProductHousekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_QuotePetProductHousekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_QuotePetProductHousekeeping] TO [sp_executehelper]
GO
