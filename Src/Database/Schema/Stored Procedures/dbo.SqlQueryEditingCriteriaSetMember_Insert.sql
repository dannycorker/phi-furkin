SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditingCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_Insert]
(

	@SqlQueryEditingCriteriaSetMemberID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryEditingCriteriaSetID int   ,

	@SqlQueryEditingColumnID int   ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditingCriteriaSetMember]
					(
					[ClientID]
					,[SqlQueryEditingID]
					,[SqlQueryEditingCriteriaSetID]
					,[SqlQueryEditingColumnID]
					,[Criteria1]
					,[Criteria2]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryEditingID
					,@SqlQueryEditingCriteriaSetID
					,@SqlQueryEditingColumnID
					,@Criteria1
					,@Criteria2
					)
				-- Get the identity value
				SET @SqlQueryEditingCriteriaSetMemberID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_Insert] TO [sp_executeall]
GO
