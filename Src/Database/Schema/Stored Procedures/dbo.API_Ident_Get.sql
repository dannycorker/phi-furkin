SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-10-28	
-- Description:	API_Ident_Detail
-- =============================================
CREATE PROCEDURE [dbo].[API_Ident_Get]
	@ClientPersonnelID INT,
	@ThirdPartySystemID INT,
	@AppKey VARCHAR(250),
	@SessionID VARCHAR(50),
	@SessionData VARCHAR(2000),
	@HostAddress VARCHAR(250),
	@RegId VARCHAR(250) = NULL,
	@Platform VARCHAR(250) = NULL,
	@Version VARCHAR(250) = NULL
AS
BEGIN

	--exec _C00_LogIt   'Info', 'API','Ident Get', @AppKey, @ClientPersonnelID
	
	IF NOT EXISTS (SELECT * FROM API_Ident WITH (NOLOCK) WHERE ClientPersonnelID = @ClientPersonnelID AND ThirdPartySystemID = @ThirdPartySystemID AND AppKey = @AppKey)
	BEGIN
		
		DECLARE @IdentID UNIQUEIDENTIFIER
		SELECT @IdentID = NEWID()
		
		INSERT	INTO API_Ident (IdentID, ClientID, SubClientID, ClientPersonnelID, ThirdPartySystemID, SessionID, PIN, RegID, HostAddress, AppKey, Platform, Version)
		SELECT	TOP 1 @IdentID, cp.ClientID, cp.SubClientID, cp.ClientPersonnelID, cp.ThirdPartySystemID, @SessionID, '0000', @RegId, @HostAddress, @AppKey, @Platform, @Version
		FROM	API_App app WITH (NOLOCK) 
		INNER JOIN API_AppAccess aa WITH (NOLOCK) ON app.AppKey = aa.AppKey AND app.Enabled = 1 AND aa.Enabled = 1 
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON aa.ClientID = cp.ClientID AND (aa.SubClientID = cp.SubClientID OR aa.SubClientID IS NULL)
		WHERE	cp.ClientPersonnelID = @ClientPersonnelID
		AND		cp.ThirdPartySystemId = @ThirdPartySystemID
		AND		aa.AppKey = @AppKey
		AND		cp.AccountDisabled = 0

	END
	ELSE
	BEGIN

		UPDATE	API_Ident 
		SET		SessionID = @SessionID,
				RegID = @RegId,
				Platform = @Platform, 
				Version = @Version
		WHERE	ClientPersonnelID = @ClientPersonnelID 
		AND		ThirdPartySystemID = @ThirdPartySystemID 
		AND		AppKey = @AppKey
		
	END
	
	SELECT *
	FROM (
		SELECT	TOP 1
				cp.EmailAddress email,
				ISNULL(cp.FirstName, '') +' '+ ISNULL(cp.LastName, '') fullName,
				ISNULL(ident.Platform,'') platform,
				ISNULL(ident.Version,'') version,
				ident.IdentID identKey,
				@SessionData tknKey,
				ISNULL(ident.PIN, '') pin,
				COALESCE(ident.Config, aa.Config, app.Config) config
		FROM	API_App app WITH (NOLOCK) 
		INNER JOIN API_AppAccess aa WITH (NOLOCK) ON app.AppKey = aa.AppKey AND app.Enabled = 1 AND aa.Enabled = 1 
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON aa.ClientID = cp.ClientID AND (aa.SubClientID = cp.SubClientID OR aa.SubClientID IS NULL)
		INNER JOIN API_Ident ident WITH (NOLOCK) ON  cp.ClientPersonnelID = ident.ClientPersonnelID AND cp.ThirdPartySystemId = ident.ThirdPartySystemID
		WHERE	cp.ClientPersonnelID = @ClientPersonnelID
		AND		cp.ThirdPartySystemId = @ThirdPartySystemID
		AND		cp.AccountDisabled = 0
		AND		aa.AppKey = @AppKey
		AND		ident.SessionID = @SessionID
	) as ident
	FOR XML AUTO, ELEMENTS

END

GO
GRANT VIEW DEFINITION ON  [dbo].[API_Ident_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Ident_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Ident_Get] TO [sp_executeall]
GO
