SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-24
-- Description:	Clear the lock details from AutomatedEventQueue records for a particular scheduler that has crashed
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__ClearEventsToProcess]
	@SchedulerID int, 
	@ServerName varchar(50) 
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Clear the lock details from AutomatedEventQueue records for a particular scheduler that has crashed.
		Do not resubmit successful records again though.
	*/
	UPDATE dbo.AutomatedEventQueue 
	SET SchedulerID = NULL, 
	ServerName = NULL, 
	LockDateTime = NULL 
	WHERE SchedulerID = @SchedulerID 
	AND ServerName = @ServerName 
	AND LockDateTime IS NOT NULL 
	AND SuccessDateTime IS NULL

END





GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__ClearEventsToProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__ClearEventsToProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__ClearEventsToProcess] TO [sp_executeall]
GO
