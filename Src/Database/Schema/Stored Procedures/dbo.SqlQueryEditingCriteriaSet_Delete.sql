SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryEditingCriteriaSet table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSet_Delete]
(

	@SqlQueryEditingCriteriaSetID int   
)
AS


				DELETE FROM [dbo].[SqlQueryEditingCriteriaSet] WITH (ROWLOCK) 
				WHERE
					[SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSet_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Delete] TO [sp_executeall]
GO
