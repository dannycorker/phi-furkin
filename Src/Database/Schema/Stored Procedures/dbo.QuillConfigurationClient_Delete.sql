SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuillConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationClient_Delete]
(

	@QuillConfigurationClientID int   
)
AS


				DELETE FROM [dbo].[QuillConfigurationClient] WITH (ROWLOCK) 
				WHERE
					[QuillConfigurationClientID] = @QuillConfigurationClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationClient_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Delete] TO [sp_executeall]
GO
