SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDroppedOutCustomerAnswer] @CustomerQuestionnaireID int 
AS
BEGIN
Delete From DroppedOutCustomerAnswers where CustomerQuestionnaireID = @CustomerQuestionnaireID;
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDroppedOutCustomerAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteDroppedOutCustomerAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteDroppedOutCustomerAnswer] TO [sp_executeall]
GO
