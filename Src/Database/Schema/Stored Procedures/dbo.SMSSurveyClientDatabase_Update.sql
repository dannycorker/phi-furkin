SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- ALTER date: 24-04-2013
-- Description:	Updates the SMSSurveyClientDatabase on Aquarius Master
-- =============================================
CREATE PROCEDURE [dbo].[SMSSurveyClientDatabase_Update]
	@ClientID INT,
	@OldOutgoingPhoneNumber VARCHAR(250),
	@OutgoingPhoneNumber VARCHAR(250)
AS
BEGIN

	EXEC AquariusMaster.dbo.AQ_SMSSurveyClientDatabase_Update @ClientID, @OldOutgoingPhoneNumber, @OutgoingPhoneNumber

END


GO
GRANT VIEW DEFINITION ON  [dbo].[SMSSurveyClientDatabase_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSSurveyClientDatabase_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSSurveyClientDatabase_Update] TO [sp_executeall]
GO
