SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the LeadDocument table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS
BEGIN

	/* This should never be used! */
	RETURN -1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetPaged] TO [sp_executeall]
GO
