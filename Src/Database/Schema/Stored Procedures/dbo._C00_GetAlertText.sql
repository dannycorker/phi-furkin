SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Robin Hall
-- Create date: 2015-04-27
-- Description:	Generate alert text for custom control
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetAlertText]

	@CaseID INT
	,@DetailFieldID INT

AS
BEGIN

	DECLARE 
		@ClientID INT
		,@ProcName NVARCHAR(1000) = 'EXEC _C%1_GetAlertText %2, %3'

	-- Get the ClientID for this Case
	SELECT @ClientID = ClientID 
	FROM Cases 
	WHERE CaseID = @CaseID
	
	-- Generate the proc call for the client-specific proc
	SELECT @ProcName = ISNULL(
		REPLACE(REPLACE(REPLACE(@ProcName
			,'%1', CONVERT(VARCHAR, @ClientID))
			,'%2', CONVERT(VARCHAR, @CaseID))
			,'%3', CONVERT(VARCHAR, @DetailFieldID))
		, @ProcName)
		
	-- Exec the proc call
	BEGIN TRY
	
		EXEC sp_executesql @ProcName

	END TRY
	BEGIN CATCH
        
		SELECT NULL
		SELECT 1 AS CanApplyEvent, '' AS TitleText
	
	END CATCH
	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetAlertText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetAlertText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetAlertText] TO [sp_executeall]
GO
