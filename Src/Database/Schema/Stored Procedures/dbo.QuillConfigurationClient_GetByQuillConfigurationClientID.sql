SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillConfigurationClient table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationClient_GetByQuillConfigurationClientID]
(

	@QuillConfigurationClientID int   
)
AS


				SELECT
					[QuillConfigurationClientID],
					[AquariumClientID],
					[QuillClientID],
					[Username],
					[Password],
					[URI]
				FROM
					[dbo].[QuillConfigurationClient] WITH (NOLOCK) 
				WHERE
										[QuillConfigurationClientID] = @QuillConfigurationClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_GetByQuillConfigurationClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationClient_GetByQuillConfigurationClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_GetByQuillConfigurationClientID] TO [sp_executeall]
GO
