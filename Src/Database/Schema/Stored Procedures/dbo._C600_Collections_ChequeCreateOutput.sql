SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-22
-- Description:	Output cheque print xml
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_ChequeCreateOutput]
	@Payments dbo.tvpGenericPaymentData READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ReferenceLen INT = 10,
			@DateLength INT = 20,
			@Payee1Len INT = 40,
			@AmountLen INT = 11,
			@ChequeLen INT = 12,
			@ReasonForRefundLen INT = 40,
			@TrailingBlankLines INT = 24,
			@AddressLineTotal INT
			 

	-- NOTE we are using LeadID for address block lines spec & MatterID for resultant number extra trailing lines to include

	SELECT	LEFT(p.Reference + REPLICATE(' ',@ReferenceLen),@ReferenceLen) +
	
			LEFT(DATENAME(DAY, SettlementDate) + 	
			CASE 
				WHEN DATENAME(DAY, SettlementDate) IN ('1', '21', '31') THEN 'st'
				WHEN DATENAME(DAY, SettlementDate) IN ('2', '22') THEN 'nd'
				WHEN DATENAME(DAY, SettlementDate) IN ('3', '23') THEN 'rd'
				ELSE 'th'
				END +
			' ' +
			LEFT(DATENAME(MONTH, SettlementDate),3) +
			' ' +
			DATENAME(YEAR, SettlementDate)
			+ REPLICATE(' ',@DateLength),@DateLength) +
			
			LEFT(p.PayeeFullname + REPLICATE(' ',@Payee1Len),@Payee1Len) + 
			
			LEFT(CAST(p.Amount AS VARCHAR) + REPLICATE(' ',@AmountLen),@AmountLen) + 
			
			LEFT(CAST(p.ChequeNumber AS VARCHAR) + REPLICATE(' ',@ChequeLen),@ChequeLen) + 
			
			REPLICATE(CHAR(13)+CHAR(10),2) +
			
			p.AddresseeFullname + 
			REPLICATE(CHAR(13)+CHAR(10),1) +
			
			CASE WHEN LeadID IN (1,4,6,9) 
			THEN
				p.AddressLine1 + CASE WHEN p.AddressLine2 <> '' THEN ', ' + p.AddressLine2 ELSE '' END +
				REPLICATE(CHAR(13)+CHAR(10),1)
			ELSE
				''
			END	+
			
			CASE WHEN LeadID IN (3,4,8,9)
			THEN
				p.AddressLine3 + CASE WHEN p.AddressCounty <> '' THEN 
									CASE WHEN p.AddressLine3 <> '' 
										THEN ', ' + p.AddressCounty 
										ELSE p.AddressCounty END
									ELSE '' END +
				REPLICATE(CHAR(13)+CHAR(10),1) 
			ELSE
				''
			END +
			
			CASE WHEN LeadID IN (5,6,8,9)
			THEN
				p.AddressPostcode + 
				REPLICATE(CHAR(13)+CHAR(10),1)
			ELSE
				''
			END + 
			
			CASE WHEN ParagraphText='' THEN
				REPLICATE(CHAR(13)+CHAR(10),@TrailingBlankLines+ABS(MatterID))
			ELSE
				REPLICATE(CHAR(13)+CHAR(10), CASE WHEN MatterID<=0 THEN ABS(MatterID)+1 ELSE MatterID END) +
				LEFT(ParagraphText,@ReasonForRefundLen) + 
				REPLICATE(CHAR(13)+CHAR(10),@TrailingBlankLines-CASE WHEN MatterID<=0 THEN 1 ELSE 0 END)
			END	
	
	FROM @Payments p
	ORDER BY ChequeNumber

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_ChequeCreateOutput] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_ChequeCreateOutput] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_ChequeCreateOutput] TO [sp_executeall]
GO
