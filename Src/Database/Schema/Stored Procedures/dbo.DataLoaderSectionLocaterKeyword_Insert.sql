SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderSectionLocaterKeyword table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_Insert]
(

	@DataLoaderSectionLocaterKeywordID int    OUTPUT,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderObjectTypeID int   ,

	@DataLoaderMapSectionID int   ,

	@Keyword varchar (250)  ,

	@DataLoaderKeywordMatchTypeID int   ,

	@KeywordNotes varchar (2000)  ,

	@RelativeRow int   ,

	@AbsoluteCol int   ,

	@IsForSectionStart bit   
)
AS


				
				INSERT INTO [dbo].[DataLoaderSectionLocaterKeyword]
					(
					[ClientID]
					,[DataLoaderMapID]
					,[DataLoaderObjectTypeID]
					,[DataLoaderMapSectionID]
					,[Keyword]
					,[DataLoaderKeywordMatchTypeID]
					,[KeywordNotes]
					,[RelativeRow]
					,[AbsoluteCol]
					,[IsForSectionStart]
					)
				VALUES
					(
					@ClientID
					,@DataLoaderMapID
					,@DataLoaderObjectTypeID
					,@DataLoaderMapSectionID
					,@Keyword
					,@DataLoaderKeywordMatchTypeID
					,@KeywordNotes
					,@RelativeRow
					,@AbsoluteCol
					,@IsForSectionStart
					)
				-- Get the identity value
				SET @DataLoaderSectionLocaterKeywordID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Insert] TO [sp_executeall]
GO
