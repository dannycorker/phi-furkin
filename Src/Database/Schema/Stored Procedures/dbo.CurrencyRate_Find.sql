SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CurrencyRate table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_Find]
(

	@SearchUsingOR bit   = null ,

	@CurrencyRateID int   = null ,

	@CurrencyId int   = null ,

	@CurrencyCode char (3)  = null ,

	@FromGBPRate decimal (18, 10)  = null ,

	@ToGBPRate decimal (18, 10)  = null ,

	@ConversionDate datetime   = null ,

	@IsLatest bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CurrencyRateID]
	, [CurrencyId]
	, [CurrencyCode]
	, [FromGBPRate]
	, [ToGBPRate]
	, [ConversionDate]
	, [IsLatest]
    FROM
	[dbo].[CurrencyRate] WITH (NOLOCK) 
    WHERE 
	 ([CurrencyRateID] = @CurrencyRateID OR @CurrencyRateID IS NULL)
	AND ([CurrencyId] = @CurrencyId OR @CurrencyId IS NULL)
	AND ([CurrencyCode] = @CurrencyCode OR @CurrencyCode IS NULL)
	AND ([FromGBPRate] = @FromGBPRate OR @FromGBPRate IS NULL)
	AND ([ToGBPRate] = @ToGBPRate OR @ToGBPRate IS NULL)
	AND ([ConversionDate] = @ConversionDate OR @ConversionDate IS NULL)
	AND ([IsLatest] = @IsLatest OR @IsLatest IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CurrencyRateID]
	, [CurrencyId]
	, [CurrencyCode]
	, [FromGBPRate]
	, [ToGBPRate]
	, [ConversionDate]
	, [IsLatest]
    FROM
	[dbo].[CurrencyRate] WITH (NOLOCK) 
    WHERE 
	 ([CurrencyRateID] = @CurrencyRateID AND @CurrencyRateID is not null)
	OR ([CurrencyId] = @CurrencyId AND @CurrencyId is not null)
	OR ([CurrencyCode] = @CurrencyCode AND @CurrencyCode is not null)
	OR ([FromGBPRate] = @FromGBPRate AND @FromGBPRate is not null)
	OR ([ToGBPRate] = @ToGBPRate AND @ToGBPRate is not null)
	OR ([ConversionDate] = @ConversionDate AND @ConversionDate is not null)
	OR ([IsLatest] = @IsLatest AND @IsLatest is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Find] TO [sp_executeall]
GO
