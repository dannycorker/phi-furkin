SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset SubType List
-- =============================================
CREATE PROCEDURE [dbo].[AssetSubType__GetList] 
	@ClientPersonnelID INT = NULL,
	@AssetTypeID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ast.[AssetSubTypeID],
		ast.[AssetSubTypeName],
		ast.[Note],
		ast.[Enabled],
		ast.[Deleted],
		ast.[WhoCreated],
		ast.[WhenCreated],
		ast.[WhoModified],
		ast.[WhenModified]
	FROM 
		[dbo].[AssetSubType] ast WITH (NOLOCK)
		INNER JOIN [dbo].[AssetType] at WITH (NOLOCK) ON at.[AssetSubTypeID] = ast.[AssetSubTypeID],
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND @AssetTypeID = at.[AssetTypeID]
	ORDER BY ast.[AssetSubTypeID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType__GetList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetSubType__GetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType__GetList] TO [sp_executeall]
GO
