SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 30-04-2013
-- Description:	Gets a list of enabled detail fields by the page id in field order
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetEnabledFieldsByPageID]
	
	@DetailFieldPageID INT,
	@LeadOrMatter INT,
	@ClientID INT = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.fnDetailFieldsShared(@ClientID)
	WHERE DetailFieldPageID = @DetailFieldPageID 
	AND LeadOrMatter = @LeadOrMatter 
	AND Enabled = 1
	ORDER BY FieldOrder ASC
    
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledFieldsByPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetEnabledFieldsByPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledFieldsByPageID] TO [sp_executeall]
GO
