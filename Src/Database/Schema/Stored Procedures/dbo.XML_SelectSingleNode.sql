SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-11-26
-- Description:	Creates and execs dynamic sql to get the value of a single element as defined in the passed in XPath expression
-- =============================================
CREATE PROCEDURE [dbo].[XML_SelectSingleNode] 
	@Xml XML,
	@XPath VARCHAR(1000),
	@NodeValue VARCHAR(8000) OUTPUT,
	@Namespace VARCHAR(1000) = NULL,
	@NamespacePrefix VARCHAR(50)= NULL
AS
BEGIN
	
	DECLARE @EncodedXPath VARCHAR(2000)
	SELECT @EncodedXPath = REPLACE(@XPath, '''', '''''')
	
	DECLARE @ReturnVal VARCHAR(8000)
	
	DECLARE @Sql NVARCHAR(1000)
	SELECT @Sql = N'SELECT @ReturnValOut = @Xml.value('''  
	IF @Namespace IS NOT NULL AND @NamespacePrefix IS NOT NULL
	BEGIN
		SELECT @Sql += N'declare namespace ' + @NamespacePrefix + '="' + @Namespace + '";'
	END
	SELECT @Sql += @EncodedXPath + N''', ''VARCHAR(8000)'')'
	
	EXEC sp_executesql @Sql, N'@Xml XML, @ReturnValOut VARCHAR(8000) OUTPUT', @Xml, @ReturnValOut = @ReturnVal OUTPUT
	
	SELECT @NodeValue = @ReturnVal
END




GO
GRANT VIEW DEFINITION ON  [dbo].[XML_SelectSingleNode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[XML_SelectSingleNode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[XML_SelectSingleNode] TO [sp_executeall]
GO
