SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Chris Townsend
-- Create date: 23rd January 2008
-- Description:	Deletes workflow group assignments
--				based on a passed in list of WorkflowGroupAssignmentIDs
-- =============================================
CREATE PROCEDURE [dbo].[Workflow_DeleteAssignments] 

@ClientID int,
@AssignmentIDList varchar(8000)

AS
BEGIN

	SET NOCOUNT ON

	DELETE WorkflowGroupAssignment
	FROM dbo.fnTableOfIDsFromCSV(@AssignmentIDList) t 
	WHERE WorkflowGroupAssignment.WorkflowGroupAssignmentID = t.anyid 
	AND	WorkflowGroupAssignment.ClientID = @ClientID

END











GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_DeleteAssignments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Workflow_DeleteAssignments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_DeleteAssignments] TO [sp_executeall]
GO
