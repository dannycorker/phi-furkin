SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ReferenceValueDelimiters table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDelimiters_Get_List]

AS


				
				SELECT
					[ReferenceValueDelimiterID],
					[Delimiter]
				FROM
					[dbo].[ReferenceValueDelimiters] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDelimiters_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Get_List] TO [sp_executeall]
GO
