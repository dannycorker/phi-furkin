SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DetailFieldPages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_Update]
(

	@DetailFieldPageID int   ,

	@ClientID int   ,

	@LeadOrMatter tinyint   ,

	@LeadTypeID int   ,

	@PageName varchar (50)  ,

	@PageCaption varchar (50)  ,

	@PageOrder int   ,

	@Enabled bit   ,

	@ResourceList bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@IsShared bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DetailFieldPages]
				SET
					[ClientID] = @ClientID
					,[LeadOrMatter] = @LeadOrMatter
					,[LeadTypeID] = @LeadTypeID
					,[PageName] = @PageName
					,[PageCaption] = @PageCaption
					,[PageOrder] = @PageOrder
					,[Enabled] = @Enabled
					,[ResourceList] = @ResourceList
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[IsShared] = @IsShared
				WHERE
[DetailFieldPageID] = @DetailFieldPageID 
AND [ClientID] = @ClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Update] TO [sp_executeall]
GO
