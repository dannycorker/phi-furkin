SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomTableSQLOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomTableSQLOption_Insert]
(

	@CustomTableSQLOptionID int    OUTPUT,

	@ClientID int   ,

	@DetailFieldID int   ,

	@OptionType varchar (100)  ,

	@OptionValue varchar (100)  
)
AS


				
				INSERT INTO [dbo].[CustomTableSQLOption]
					(
					[ClientID]
					,[DetailFieldID]
					,[OptionType]
					,[OptionValue]
					)
				VALUES
					(
					@ClientID
					,@DetailFieldID
					,@OptionType
					,@OptionValue
					)
				-- Get the identity value
				SET @CustomTableSQLOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQLOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Insert] TO [sp_executeall]
GO
