SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis 
-- Create date: 2018-06-19
-- Description:	Create a refund to be automatically requested tonight, Or a refund that has already been paid 
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[Billing_Support_FailPayment]
(
	@CustomerPaymentScheduleIDToFail INT,
	@Notes VARCHAR(MAX),
	@AddRetry BIT = 0
)
AS
BEGIN

	DECLARE @CustomerID INT,
			@Now DATE = dbo.fn_GetDate_Local(), 
			@PaymentNet DECIMAL (18,2),
			@PaymentIPT DECIMAL (18,2), 
			@PostCode VARCHAR(20),
			@ClientID INT,
			@ActualCollectionDate DATE,
			@PaymentStatusID INT, 
			@CustomerPaymentScheduleID INT,
			@PurchasedProductPaymentScheduleID INT,
			@ClientAccountID INT,
			@CollectionsMatterID INT,
			@PaymentTypeID INT,
			@PaymentID INT,
			@CardTransactionID INT,
			@AccountRef VARCHAR (20),
			@CustomerLedgerID INT,
			@NewCustomerPaymentScheduleID INT 
	
	SELECT @PaymentID = p.PaymentID FROM Payment p with (NOLOCK) 
	WHERE p.CustomerPaymentScheduleID = @CustomerPaymentScheduleIDToFail		

	SELECT @ClientAccountID = cps.ClientAccountID 
	FROM CustomerPaymentSchedule cps with (NOLOCK)
	where cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleIDToFail

	UPDATE p 
	SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
		p.FailureReason = @Notes, 
		p.WhenModified = @Now, 
		p.WhoModified = 58552
	FROM Payment p
	WHERE p.PaymentID = @PaymentID

	-- insert a GL reversal record
	INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
	SELECT cl.ClientID, cl.CustomerID, EffectivePaymentDate, p.FailureCode, p.FailureReason, @Now, TransactionReference, 'Failure notification ' + p.FailureReason, -cl.TransactionNet, -cl.TransactionVAT, -cl.TransactionGross, cl.LeadEventID, cl.ObjectID, cl.ObjectTypeID, @PaymentID, OutgoingPaymentID, 58552 /*@AqAutomation*/, @Now
	FROM CustomerLedger cl WITH (NOLOCK) 
	INNER JOIN Payment p on p.PaymentID = cl.PaymentID
	WHERE cl.PaymentID=@PaymentID
	
	SELECT @CustomerLedgerID = SCOPE_IDENTITY()

	-- reverse customerpaymentschedule, reverse payment status
	UPDATE cps  
	SET PaymentStatusID=4, ReconciledDate=@Now
	FROM CustomerPaymentSchedule cps
	INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
	WHERE p.PaymentID=@PaymentID
	
	-- purchasedproductpaymentschedule, reverse payment status
	UPDATE ppps  
	SET PaymentStatusID=4, ReconciledDate=@Now, ContraCustomerLedgerID = @CustomerLedgerID
	FROM PurchasedProductPaymentSchedule ppps
	INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
	WHERE p.PaymentID=@PaymentID
	
	/*Retry*/
	IF @AddRetry = 1 
	BEGIN 
				
		INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, WhoCreated, WhenCreated, SourceID, ClientAccountID, RelatedObjectID, RelatedObjectTypeID)
		SELECT cps.ClientID, cps.CustomerID, cps.AccountID, @Now, @Now, cps.PaymentNet, cps.PaymentVAT, cps.PaymentGross, 5 /*Retry*/, NULL, NULL, cps.WhoCreated, @Now, cps.CustomerPaymentScheduleID, @ClientAccountID, cps.RelatedObjectID, cps.RelatedObjectTypeID
		FROM CustomerPaymentSchedule cps WITH (NOLOCK)
		INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
		WHERE p.PaymentID=@PaymentID

		SELECT @NewCustomerPaymentScheduleID = SCOPE_IDENTITY()
	
		INSERT INTO PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
		SELECT ppps.ClientID, ppps.CustomerID, ppps.AccountID, ppps.PurchasedProductID, @Now, ppps.CoverFrom, ppps.CoverTo, @Now, ppps.PaymentNet, ppps.PaymentVAT, ppps.PaymentGross, 5 /*Retry*/, NULL, NULL, @NewCustomerPaymentScheduleID, ppps.WhoCreated, @Now, ppps.PurchasedProductPaymentScheduleParentID, 3, @ClientAccountID, ppps.TransactionFee
		FROM PurchasedProductPaymentSchedule ppps
		INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
		WHERE p.PaymentID=@PaymentID

	END 

END 
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_FailPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing_Support_FailPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_FailPayment] TO [sp_executeall]
GO
