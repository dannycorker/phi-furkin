SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the GroupAndUserEditingControl table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupAndUserEditingControl_Find]
(

	@SearchUsingOR bit   = null ,

	@EditingID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientPersonnelID int   = null ,

	@WhoIsEditing int   = null ,

	@EditStartedAt datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EditingID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [WhoIsEditing]
	, [EditStartedAt]
    FROM
	[dbo].[GroupAndUserEditingControl] WITH (NOLOCK) 
    WHERE 
	 ([EditingID] = @EditingID OR @EditingID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([WhoIsEditing] = @WhoIsEditing OR @WhoIsEditing IS NULL)
	AND ([EditStartedAt] = @EditStartedAt OR @EditStartedAt IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EditingID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [WhoIsEditing]
	, [EditStartedAt]
    FROM
	[dbo].[GroupAndUserEditingControl] WITH (NOLOCK) 
    WHERE 
	 ([EditingID] = @EditingID AND @EditingID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([WhoIsEditing] = @WhoIsEditing AND @WhoIsEditing is not null)
	OR ([EditStartedAt] = @EditStartedAt AND @EditStartedAt is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupAndUserEditingControl_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Find] TO [sp_executeall]
GO
