SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-20
-- Description:	Upload Stock XML
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SageStockUpload]
	@StockData VARCHAR(MAX),
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO LogXML (ClientID, ContextID, ContextVarchar, InvalidXML, LogDateTime, LogXMLEntry)
	VALUES (@ClientID, 88, 'XML Upload', NULL, dbo.fn_GetDate_Local(), @StockData)
	
	INSERT INTO AsyncQueue (ClientID, WhenCreated, QueueTypeID, Status, Payload, Outcome, WhenCompleted)
	VALUES (@ClientID, dbo.fn_GetDate_Local(), 7, 1, @StockData, NULL, NULL)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageStockUpload] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SageStockUpload] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageStockUpload] TO [sp_executeall]
GO
