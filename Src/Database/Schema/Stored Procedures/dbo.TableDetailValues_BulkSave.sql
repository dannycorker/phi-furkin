SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-09-23
-- Description:	Inserts or updates multiple table detail values at once
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues_BulkSave]
(
	@TableRowID INT = NULL,
	@ClientID INT,
	@DetailFieldSubType INT,
	@ObjectID INT,
	@DetailFieldID INT,
	@DetailFieldPageID INT,
	@ResourceListID INT = NULL,
	@DetailValues tvpDetailValueData READONLY
)

AS
BEGIN
	
	IF @TableRowID IS NULL
	BEGIN
	
		IF @DetailFieldSubType = 1
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, LeadID)
			VALUES(@ClientID, @DetailFieldID, @DetailFieldPageID, @ObjectID)
		END
		IF @DetailFieldSubType = 2
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, MatterID, LeadID)
			SELECT @ClientID, @DetailFieldID, @DetailFieldPageID, @ObjectID, LeadID
			FROM Matter 
			WHERE MatterID = @ObjectID
		END
		IF @DetailFieldSubType = 10
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, CustomerID)
			VALUES(@ClientID, @DetailFieldID, @DetailFieldPageID, @ObjectID)
		END
		IF @DetailFieldSubType = 11
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, CaseID)
			VALUES(@ClientID, @DetailFieldID, @DetailFieldPageID, @ObjectID)
		END
		IF @DetailFieldSubType = 12
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID)
			VALUES(@ClientID, @DetailFieldID, @DetailFieldPageID)
		END
		IF @DetailFieldSubType = 13
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @DetailFieldPageID, @ObjectID)
		END
		IF @DetailFieldSubType = 14
		BEGIN
			INSERT INTO TableRows(ClientID, DetailFieldID, DetailFieldPageID, ContactID)
			VALUES(@ClientID, @DetailFieldID, @DetailFieldPageID, @ObjectID)
		END
		
		SELECT @TableRowID = SCOPE_IDENTITY()
	
	END
	
	
	UPDATE tdv 
	SET DetailValue = v.DetailValue, EncryptedValue = v.EncryptedValue
	FROM dbo.TableDetailValues tdv 
	INNER JOIN @DetailValues v ON tdv.TableDetailValueID = v.DetailValueID AND tdv.ClientID = v.ClientID AND tdv.LeadID = v.ObjectID
	
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 1 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, LeadID)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ObjectID
		FROM @DetailValues
		WHERE DetailFieldSubType = 1 
		AND DetailValueID < 1
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 2 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, MatterID, LeadID)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, v.ClientID, DetailValue, EncryptedValue, ObjectID, m.LeadID
		FROM @DetailValues v
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON v.ObjectID = m.MatterID 
		WHERE DetailFieldSubType = 2
		AND DetailValueID < 1
		
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 10 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, CustomerID)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ObjectID
		FROM @DetailValues
		WHERE DetailFieldSubType = 10
		AND DetailValueID < 1
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 11 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, CaseID)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ObjectID
		FROM @DetailValues
		WHERE DetailFieldSubType = 11 
		AND DetailValueID < 1
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 12 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue
		FROM @DetailValues
		WHERE DetailFieldSubType = 12 
		AND DetailValueID < 1
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 13 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ClientPersonnelID)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ObjectID
		FROM @DetailValues
		WHERE DetailFieldSubType = 13
		AND DetailValueID < 1
		
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 14 AND DetailValueID < 1)
	BEGIN
		
		INSERT dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ContactID)
		SELECT @TableRowID, @ResourceListID, DetailFieldID, ClientID, DetailValue, EncryptedValue, ObjectID
		FROM @DetailValues
		WHERE DetailFieldSubType = 14
		AND DetailValueID < 1
		
	END	
	
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_BulkSave] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_BulkSave] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_BulkSave] TO [sp_executeall]
GO
