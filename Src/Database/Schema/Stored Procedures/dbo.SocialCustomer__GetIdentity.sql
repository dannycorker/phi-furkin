SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2015
-- Description:	Gets the identitys associated with a customer
-- =============================================
CREATE PROCEDURE [dbo].[SocialCustomer__GetIdentity]
	@CustomerID INT,
	@ClientID INT,
	@LeadTypeID INT,
	@SocialTypeID INT,
	@ScreenName VARCHAR(250),
	@SocialUserID VARCHAR(50)	
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @LeadID INT
	SELECT TOP 1 @LeadID=LeadID FROM Lead WITH (NOLOCK) 
	WHERE CustomerID=@CustomerID AND LeadTypeID = @LeadTypeID
    
    SELECT TOP 1 m.CustomerID, m.LeadID, m.CaseID, m.MatterID, sc.SocialCustomerID
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN dbo.SocialCustomer sc WITH (NOLOCK) ON sc.CustomerID = m.CustomerID AND sc.SocialUserID=@SocialUserID
	WHERE m.LeadID=@LeadID AND m.CustomerID=@CustomerID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__GetIdentity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialCustomer__GetIdentity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__GetIdentity] TO [sp_executeall]
GO
