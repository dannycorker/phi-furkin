SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-10-18
-- Description:	Pull documents for a particular lead or case through the document queue.
--				Adapted from CreateDocumentZipsFromAutomatedTask
-- =============================================
CREATE PROCEDURE [dbo].[CreateDocumentZipsForLeadOrCase] 
	@ClientID int,
	@LeadID int,
	@CaseID int = null,
	@DocumentsPerZip int = 100,
	@DocumentTypeIDs dbo.tvpInt READONLY,
	@ZipPrefix varchar(50),
	@EmailAddress varchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @DocumentZipInformationID int,
			@ServerMapPath varchar(2000), 
			@SecurePath char(1), 
			@HostPath varchar(250), 
			@WordTempDirectory varchar(250), 
			@StatusID int

	SELECT	@ServerMapPath = 'ClientImages\Client_' + convert(varchar, @ClientID) + '_Images\DocumentZips', 
			@SecurePath = 's', 
			@HostPath = 'www.aquarium-software.com', 
			@WordTempDirectory = 'WordTemp',
			@StatusID = 35
	
	-- Insert a Zip Information header record for this batch of records
	INSERT INTO [dbo].[DocumentZipInformation] (ClientID, ServerMapPath, SecurePath, HostPath, WordTempDirectory, DocumentsPerZip, ZipPrefix, StatusID,EmailOnSuccess,EmailOnError)
	VALUES (@ClientID, @ServerMapPath, @SecurePath, @HostPath, @WordTempDirectory, @DocumentsPerZip, @ZipPrefix, @StatusID,@EmailAddress,@EmailAddress)

	SELECT @DocumentZipInformationID = SCOPE_IDENTITY()

	-- Insert the detail records
	INSERT INTO dbo.DocumentZip (DocumentZipInformationID, ClientID, DocumentQueueID, StatusID, WhenStored) 
	SELECT @DocumentZipInformationID, @ClientID, dq.DocumentQueueID, @StatusID, dbo.fn_GetDate_Local()
	FROM dbo.DocumentQueue dq 
	INNER JOIN @DocumentTypeIDs dt on dt.AnyID = dq.DocumentTypeID
	WHERE dq.ClientID = @ClientID
	AND dq.[WhenCreated] IS NULL
	AND dq.LeadID = @LeadID
	and (dq.CaseID = @CaseID or @CaseID is null)
	AND NOT EXISTS 
	(
		SELECT * 
		FROM DocumentZip dz 
		WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
	)
	
	IF @@RowCount = 0
	BEGIN
		DELETE dzi
		FROM DocumentZipInformation dzi
		WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID
		
		SELECT @DocumentZipInformationID = -1
	END
	
	RETURN @DocumentZipInformationID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CreateDocumentZipsForLeadOrCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreateDocumentZipsForLeadOrCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreateDocumentZipsForLeadOrCase] TO [sp_executeall]
GO
