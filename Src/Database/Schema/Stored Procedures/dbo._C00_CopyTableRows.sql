SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-13
-- Description:	Copy table rows and fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyTableRows]
	@tvpIntRowIDs tvpInt READONLY,
	@NewDetailFieldID int = null,
	@NewDetailFieldPageID int = null,
	@ReturnResults bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	/* Declare a table to hold the new values and the old ones they map to */
	DECLARE @NewTableRows TABLE (NewTableRowID int, OldTableRowID int)
	
	/* Create the new rows first, using DetailFieldPageID to hold the old RowID temporarily */
	INSERT INTO dbo.TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID)
	OUTPUT inserted.TableRowID, inserted.DetailFieldPageID INTO @NewTableRows (NewTableRowID, OldTableRowID)
	SELECT trOld.ClientID, trOld.LeadID, trOld.MatterID, COALESCE(@NewDetailFieldID, trOld.DetailFieldID), trOld.TableRowID, trOld.DenyEdit, trOld.DenyDelete, trOld.CustomerID, trOld.CaseID, trOld.ClientPersonnelID, trOld.ContactID 
	FROM @tvpIntRowIDs t 
	INNER JOIN dbo.TableRows trOld WITH (NOLOCK) ON trOld.TableRowID = t.AnyID 

	/* Copy all the values across */
	INSERT INTO dbo.TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, EncryptedValue, ErrorMsg, CustomerID, CaseID, ClientPersonnelID, ContactID)
	SELECT ntr.NewTableRowID, tdvOld.ResourceListID, tdvOld.DetailFieldID, tdvOld.DetailValue, tdvOld.LeadID, tdvOld.MatterID, tdvOld.ClientID, tdvOld.EncryptedValue, tdvOld.ErrorMsg, tdvOld.CustomerID, tdvOld.CaseID, tdvOld.ClientPersonnelID, tdvOld.ContactID 
	FROM @NewTableRows ntr 
	INNER JOIN dbo.TableDetailValues tdvOld WITH (NOLOCK) ON tdvOld.TableRowID = ntr.OldTableRowID
	
	/* Now set the DetailFieldPageID values to the as appropriate */
	IF @NewDetailFieldPageID IS NULL
	BEGIN
		/* Use the old value */
		UPDATE trNew 
		SET DetailFieldPageID = trOld.DetailFieldPageID
		FROM @NewTableRows ntr
		INNER JOIN dbo.TableRows trOld WITH (NOLOCK) ON trOld.TableRowID = ntr.OldTableRowID
		INNER JOIN dbo.TableRows trNew WITH (NOLOCK) ON trNew.TableRowID = ntr.NewTableRowID
	END
	ELSE
	BEGIN
		/* Use the new value */
		UPDATE dbo.TableRows 
		SET DetailFieldPageID = @NewDetailFieldPageID
		FROM @NewTableRows ntr
		INNER JOIN dbo.TableRows trNew WITH (NOLOCK) ON trNew.TableRowID = ntr.NewTableRowID
	END
	
	/* Show the new values if requested */
	IF @ReturnResults = 1
	BEGIN
		SELECT *
		FROM @NewTableRows ntr
		INNER JOIN dbo.TableRows trNew WITH (NOLOCK) ON trNew.TableRowID = ntr.NewTableRowID 
		INNER JOIN dbo.TableDetailValues tdvNew WITH (NOLOCK) ON tdvNew.TableRowID = trNew.TableRowID
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyTableRows] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyTableRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyTableRows] TO [sp_executeall]
GO
