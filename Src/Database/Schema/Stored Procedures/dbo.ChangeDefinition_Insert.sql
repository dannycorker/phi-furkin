SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChangeDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDefinition_Insert]
(

	@ChangeDefinitionID int    OUTPUT,

	@ClientID int   ,

	@ChangeDescription varchar (255)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@RequiredDate datetime   ,

	@Deleted bit   
)
AS


				
				INSERT INTO [dbo].[ChangeDefinition]
					(
					[ClientID]
					,[ChangeDescription]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[RequiredDate]
					,[Deleted]
					)
				VALUES
					(
					@ClientID
					,@ChangeDescription
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@RequiredDate
					,@Deleted
					)
				-- Get the identity value
				SET @ChangeDefinitionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDefinition_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Insert] TO [sp_executeall]
GO
