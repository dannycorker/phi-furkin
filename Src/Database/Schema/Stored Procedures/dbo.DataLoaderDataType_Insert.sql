SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderDataType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDataType_Insert]
(

	@DataLoaderDataTypeID int   ,

	@DataTypeName varchar (250)  ,

	@DataTypeDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderDataType]
					(
					[DataLoaderDataTypeID]
					,[DataTypeName]
					,[DataTypeDescription]
					)
				VALUES
					(
					@DataLoaderDataTypeID
					,@DataTypeName
					,@DataTypeDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDataType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDataType_Insert] TO [sp_executeall]
GO
