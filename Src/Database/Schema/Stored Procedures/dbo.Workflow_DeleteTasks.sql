SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Chris Townsend
-- Create date: 31st January 2008
-- Description:	Deletes workflow tasks
--				based on a passed in list of WorkflowTaskIds
-- =============================================
CREATE PROCEDURE [dbo].[Workflow_DeleteTasks] 

@ClientID int,
@TaskIDList varchar(8000)

AS
BEGIN

	SET NOCOUNT ON

	DELETE WorkflowTask
	FROM dbo.fnTableOfIDsFromCSV(@TaskIDList) t 
	WHERE WorkflowTask.WorkflowTaskID = t.anyid 
	AND	WorkflowTask.ClientID = @ClientID

END












GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_DeleteTasks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Workflow_DeleteTasks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_DeleteTasks] TO [sp_executeall]
GO
