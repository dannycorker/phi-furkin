SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LDAPConfig table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LDAPConfig_Find]
(

	@SearchUsingOR bit   = null ,

	@LDAPConfigID int   = null ,

	@ClientID int   = null ,

	@QueryString varchar (2000)  = null ,

	@QueryAccountName varchar (2000)  = null ,

	@QueryAccountPassword varchar (2000)  = null ,

	@Enabled bit   = null ,

	@Domain varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LDAPConfigID]
	, [ClientID]
	, [QueryString]
	, [QueryAccountName]
	, [QueryAccountPassword]
	, [Enabled]
	, [Domain]
    FROM
	[dbo].[LDAPConfig] WITH (NOLOCK) 
    WHERE 
	 ([LDAPConfigID] = @LDAPConfigID OR @LDAPConfigID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([QueryString] = @QueryString OR @QueryString IS NULL)
	AND ([QueryAccountName] = @QueryAccountName OR @QueryAccountName IS NULL)
	AND ([QueryAccountPassword] = @QueryAccountPassword OR @QueryAccountPassword IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Domain] = @Domain OR @Domain IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LDAPConfigID]
	, [ClientID]
	, [QueryString]
	, [QueryAccountName]
	, [QueryAccountPassword]
	, [Enabled]
	, [Domain]
    FROM
	[dbo].[LDAPConfig] WITH (NOLOCK) 
    WHERE 
	 ([LDAPConfigID] = @LDAPConfigID AND @LDAPConfigID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([QueryString] = @QueryString AND @QueryString is not null)
	OR ([QueryAccountName] = @QueryAccountName AND @QueryAccountName is not null)
	OR ([QueryAccountPassword] = @QueryAccountPassword AND @QueryAccountPassword is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Domain] = @Domain AND @Domain is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Find] TO [sp_executeall]
GO
