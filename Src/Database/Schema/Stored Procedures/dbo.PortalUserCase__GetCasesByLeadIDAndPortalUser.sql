SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 20080627
-- Description:	Gets a list of cases that the current PortalUser has access to.
-- JWG 2009-07-15 Added special naming for Daresbury (client 49) compared to everyone else.
--                This used to be hardcoded in the app, and will probably end up in a db table one day.
-- JWG 2010-03-01 Inner join to vMatterDetailValues to show names not IDs in the "Case 1 : We are winning!" list
-- JWG 2014-05-13 #26507 LatestRecordsFirst 
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserCase__GetCasesByLeadIDAndPortalUser] 
	@PortalUserID INT,
	@LeadID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LatestRecordsFirst BIT = 0
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	INNER JOIN dbo.PortalUser pu WITH (NOLOCK) ON pu.ClientID = cl.ClientID
	WHERE pu.PortalUserID = @PortalUserID
	
	-- If the client wants to see "Business Name" instead of "Case 1" etc, as dictated by MatterListDisplay, 
	-- then get the top matter for each Case here.
	-- There should only ever be 1 MatterListDisplay record for a lead type, 
	-- but InterResolve had 9 once, so play it safe. 
	SELECT i.LeadID, i.CaseID, i.CaseRef, i.ListOrder, CASE i.ClientID WHEN 49 THEN CASE i.ListOrder WHEN 1 THEN 'Vendor: ' ELSE 'Purchaser: ' END ELSE '' END + i.CaseRef AS [TailoredCaseRef] 
	FROM (
		SELECT puc.LeadID, c.CaseID, c.CaseRef + ': ' + ISNULL(v.DetailValue, '') AS CaseRef, ROW_NUMBER() OVER( ORDER BY puc.LeadID, c.CaseID ) AS [ListOrder], c.ClientID
		FROM PortalUserCase puc 
		INNER JOIN dbo.Lead l ON l.LeadID = puc.LeadID 
		INNER JOIN dbo.Cases c ON puc.LeadID = c.LeadID 
		INNER JOIN dbo.Matter m ON m.CaseID = c.CaseID AND m.RefLetter = (SELECT MIN(m2.RefLetter) FROM dbo.Matter m2 WHERE m2.CaseID = c.CaseID) 
		LEFT JOIN dbo.MatterListDisplay ml ON ml.LeadTypeID = l.LeadTypeID AND ml.MatterListDisplayID = (SELECT MAX(ml2.MatterListDisplayID) FROM dbo.MatterListDisplay ml2 WHERE ml2.LeadTypeID = l.LeadTypeID) 
		LEFT JOIN dbo.vMatterDetailValues v ON v.MatterID = m.MatterID AND v.DetailFieldID = ml.Field1 
		WHERE puc.LeadID = @leadID 
		AND puc.PortalUserID = @portalUserID 
		AND (puc.CaseID IS NULL OR c.CaseID = puc.CaseID) 
	) AS i
	/*ORDER BY i.ListOrder, i.LeadID, i.CaseID*/
	ORDER BY 
		/*
			JWG 2014-05-13 #26507 LatestRecordsFirst
			The dynamic part is really the ASC/DESC rather than the column to order by, but this is the only way to do it at present.
		*/
		CASE @LatestRecordsFirst WHEN 1 THEN i.LeadID END DESC, 
		CASE @LatestRecordsFirst WHEN 0 THEN i.LeadID END ASC, 
		CASE @LatestRecordsFirst WHEN 1 THEN i.CaseID END DESC, 
		CASE @LatestRecordsFirst WHEN 0 THEN i.CaseID END ASC 


END








GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetCasesByLeadIDAndPortalUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase__GetCasesByLeadIDAndPortalUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase__GetCasesByLeadIDAndPortalUser] TO [sp_executeall]
GO
