SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyFieldMappingValueKey table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_Update]
(

	@ThirdPartyFieldMappingValueKeyID int   ,

	@ClientID int   ,

	@FieldMappingID int   ,

	@DetailValue varchar (2000)  ,

	@LookupListItemID int   ,

	@ThirdPartyKey varchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyFieldMappingValueKey]
				SET
					[ClientID] = @ClientID
					,[FieldMappingID] = @FieldMappingID
					,[DetailValue] = @DetailValue
					,[LookupListItemID] = @LookupListItemID
					,[ThirdPartyKey] = @ThirdPartyKey
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ThirdPartyFieldMappingValueKeyID] = @ThirdPartyFieldMappingValueKeyID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Update] TO [sp_executeall]
GO
