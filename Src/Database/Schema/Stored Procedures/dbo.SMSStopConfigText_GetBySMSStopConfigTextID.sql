SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[SMSStopConfigText_GetBySMSStopConfigTextID](
@SMSStopConfigTextID int
)
as 
begin

	select s.LeadTypeID, s.OutgoingPhoneNumber, s.SMSTextToAppend
	From SMSStopConfigText s with(nolock)
	where s.SMSStopConfigTextID = @SMSStopConfigTextID

end
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_GetBySMSStopConfigTextID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStopConfigText_GetBySMSStopConfigTextID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_GetBySMSStopConfigTextID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStopConfigText_GetBySMSStopConfigTextID] TO [sp_executehelper]
GO
