SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-19
-- Description:	exports ruleset items
-- CPS 2018-03-16 order by Name
-- 2018-09-12	GPR updated to match version on C433
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Data_Export]
(
	@ClientID INT,
	@UserID INT,
	@ParentID INT,
	@ExportType VARCHAR(250),
	@FilterParams dbo.tvpIntVarcharVarchar READONLY
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @OutputText VARCHAR(MAX) ='',
			@DynamicParamNames VARCHAR(MAX) = ''
	
	IF @ExportType = 'Options'
	BEGIN
	
		SELECT @OutputText = '"Operator","Value1","Value2","ID"'+CHAR(13)+CHAR(10)

		SELECT
			@OutputText += '"'+
			op.Name +'","'+
			COALESCE(ll.ItemValue,po.Val1, '') +'","'+
			COALESCE(ll2.ItemValue,po.Val2, '') +'","'+
			CAST(po.ParameterOptionID AS VARCHAR) +'"'
			+CHAR(13)+CHAR(10)
		FROM dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) ON rp.RuleParameterID = po.RuleParameterID
		LEFT JOIN dbo.DetailFields df WITH (NOLOCK) ON CAST(df.DetailFieldID AS VARCHAR(20)) = rp.Value AND rp.DataTypeID = 6
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID AND po.Val1 = CAST(ll.LookupListItemID AS VARCHAR(20))
		LEFT JOIN dbo.LookupListItems ll2 WITH (NOLOCK) ON df.LookupListID = ll2.LookupListID AND po.Val2 = CAST(ll2.LookupListItemID AS VARCHAR(20))
		INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON po.OperatorID = op.OperatorID
		INNER JOIN @FilterParams fv ON po.RuleParameterID = fv.AnyID AND COALESCE(po.Val1, po.Val2,'') LIKE ISNULL(fv.AnyValue1, '') + '%'
		WHERE	rp.RuleParameterID = @ParentID
		AND		rp.ClientID = @ClientID
		ORDER BY rp.Name

	END
	ELSE IF @ExportType = 'TestFileResult'
	BEGIN
	
		SELECT	@DynamicParamNames += ',"'+ rp.Name +'"'
		FROM	RulesEngine_TestFileItemOverride tfo WITH (NOLOCK) 
		INNER JOIN	RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = tfo.RuleParameterID
		WHERE	TestFileItemID IN 
		(
			SELECT	TOP 1 TestFileItemID 
			FROM	RulesEngine_TestFileItem WITH (NOLOCK) 
			WHERE	TestFileID = @ParentID
		)
		ORDER BY tfo.RuleParameterID

		SELECT @OutputText = '"Input","CustomerID","LeadID","CaseID","MatterID","EvaluateByEffectiveDate","EvaluateByChangeSetID"'+@DynamicParamNames+',"TestFileID","TestFileItemID","Processed","ChangeSet","RuleSet","RuleID","RuleName","RuleCheckpoint","InputValue","Transform","Value","OutputValue","ErrorMessage"'+CHAR(13)+CHAR(10)
				
		--SELECT
		--	@OutputText +='"'+
		--	ISNULL(CAST(tfi.Input AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfi.CustomerID AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfi.LeadID AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfi.CaseID AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfi.MatterID AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfi.EvaluateByEffectiveDate AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfi.EvaluateByChangeSetID AS VARCHAR),'') +
		--	ISNULL((    
		--		SELECT '","' + ISNULL(tfo.ParameterValue,'') AS [text()]
		--		FROM RulesEngine_TestFileItemOverride tfo WITH (NOLOCK) 
		--		WHERE tfo.TestFileItemID = tfi.TestFileItemID
		--		ORDER BY tfo.RuleParameterID
		--		FOR XML PATH('')
		--	)+'","', '","')+
		--	ISNULL(CAST(tfi.TestFileID AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfir.TestFileItemID AS VARCHAR),'') +'","'+
		--	ISNULL(CONVERT(CHAR(22), tfir.WhenProcessed, 120),'') +'","'+
		--	ISNULL(CAST(cs.TagName AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(rs.Name AS VARCHAR),'') +'","'+
		--	ISNULL(CAST(tfir.RuleID AS VARCHAR),'') +'","'+
		--	ISNULL(tfir.RuleName,'') +'","'+
		--	ISNULL(tfir.RuleCheckpoint,'') +'","'+
		--	ISNULL(tfir.InputValue,'') +'","'+
		--	ISNULL(tfir.Transform,'') +'","'+
		--	ISNULL(tfir.Value,'') +'","'+
		--	ISNULL(tfir.OutputValue,'') +'","'+
		--	ISNULL(tfir.ErrorMessage,'')  +'"'
		--	+CHAR(13)+CHAR(10)
		--FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
		--LEFT JOIN RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfi.TestFileItemID = tfir.TestFileItemID
		--LEFT JOIN RulesEngine_Rules r WITH (NOLOCK)  ON r.RuleID = tfir.RuleID
		--LEFT JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID
		--LEFT JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID
		--WHERE	tfi.TestFileID = @ParentID
		--AND		tfi.ClientID = @ClientID
		--ORDER BY tfi.TestFileItemID, tfir.TestFileItemResultID DESC
		
		
		DECLARE @DynamicParamValues TABLE (TestFileItemID INT, DynamicParamValues VARCHAR(2000))
		INSERT INTO @DynamicParamValues(TestFileItemID,DynamicParamValues)
		SELECT 
			tfi.TestFileItemID,
			ISNULL((    
				SELECT '","' + ISNULL(tfo.ParameterValue,'') AS [text()]
				FROM RulesEngine_TestFileItemOverride tfo WITH (NOLOCK) 
				WHERE tfo.TestFileItemID = tfi.TestFileItemID
				ORDER BY tfo.RuleParameterID
				FOR XML PATH('')
			)+'","', '","')
		FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
		WHERE	tfi.TestFileID = @ParentID
		AND		tfi.ClientID = @ClientID
		
			
		SELECT
			@OutputText +='"'+
			ISNULL(CAST(tfi.Input AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfi.CustomerID AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfi.LeadID AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfi.CaseID AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfi.MatterID AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfi.EvaluateByEffectiveDate AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfi.EvaluateByChangeSetID AS VARCHAR),'') 
			+
			ISNULL(CAST(dpv.DynamicParamValues AS VARCHAR(2000)),'') 
			+
			ISNULL(CAST(tfi.TestFileID AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.TestFileItemID AS VARCHAR),'') +'","'+
			ISNULL(CONVERT(CHAR(22), tfir.WhenProcessed, 120),'') +'","'+
			ISNULL(CAST(cs.TagName AS VARCHAR),'') +'","'+
			ISNULL(CAST(rs.Name AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.RuleID AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.RuleName AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.RuleCheckpoint AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.InputValue AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.Transform AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.Value AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.OutputValue AS VARCHAR),'') +'","'+
			ISNULL(CAST(tfir.ErrorMessage AS VARCHAR),'')+'"'
			+CHAR(13)+CHAR(10)
		FROM  RulesEngine_TestFileItem tfi WITH (NOLOCK)
		INNER JOIN @DynamicParamValues dpv ON tfi.TestFileItemID = dpv.TestFileItemID
		INNER JOIN RulesEngine_TestFileItemResult tfir WITH (NOLOCK) ON tfi.TestFileItemID = tfir.TestFileItemID
		LEFT JOIN RulesEngine_Rules r WITH (NOLOCK)  ON r.RuleID = tfir.RuleID
		LEFT JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID
		LEFT JOIN RulesEngine_ChangeSets cs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID
		WHERE	tfi.TestFileID = @ParentID
		AND		tfi.ClientID = @ClientID
		ORDER BY tfi.TestFileItemID, tfir.TestFileItemResultID DESC
		
		
	END
	
	SELECT @OutputText Response
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Data_Export] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Data_Export] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Data_Export] TO [sp_executeall]
GO
