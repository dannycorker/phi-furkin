SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteria table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingCriteriaID]
(

	@SqlQueryEditingCriteriaID int   
)
AS


				SELECT
					[SqlQueryEditingCriteriaID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[SqlQueryEditingColumnID],
					[CriteriaText],
					[Criteria1],
					[Criteria2],
					[CriteriaName],
					[SubQueryID],
					[SubQueryLinkType],
					[ParamValue],
					[IsSecurityClause],
					[CriteriaSubstitutions],
					[IsParameterizable],
					[Comparison],
					[IsJoinClause]
				FROM
					[dbo].[SqlQueryEditingCriteria] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingCriteriaID] = @SqlQueryEditingCriteriaID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingCriteriaID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingCriteriaID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingCriteriaID] TO [sp_executeall]
GO
