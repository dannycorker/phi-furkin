SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FieldValidation table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FieldValidationID],
					[ClientID],
					[FieldID],
					[ValidationGroup],
					[ValidationType],
					[MaxValue],
					[MinValue],
					[ErrorMessage],
					[AccessLevelRestrictions]
				FROM
					[dbo].[FieldValidation] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_GetByClientID] TO [sp_executeall]
GO
