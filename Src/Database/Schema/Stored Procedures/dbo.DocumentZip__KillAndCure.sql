SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-05-28
-- Description:	Fix a particular zip that has crashed or stopped for any other reason.
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZip__KillAndCure]
	@DocumentZipInformationID int, 
	@MaxPerZip int = 100
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
	@DocsPerZip int, 
	@TotalDocs int, 
	@DocsDone int, 
	@DocsFailed int, 
	@DocsToDo int, 
	@ModuloRemainder int 
	
	SELECT @DocsPerZip = dzi.DocumentsPerZip 
	FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
	WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID 
	
	SELECT @TotalDocs = COUNT(*)
	FROM dbo.DocumentZip dz WITH (NOLOCK) 
	WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
	
	SELECT @DocsDone = COUNT(*)
	FROM dbo.DocumentZip dz WITH (NOLOCK) 
	WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
	AND dz.StatusID = 31
	
	SELECT @DocsFailed = COUNT(*)
	FROM dbo.DocumentZip dz WITH (NOLOCK) 
	WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
	AND dz.StatusID = 34
	
	SELECT @DocsToDo = COUNT(*)
	FROM dbo.DocumentZip dz WITH (NOLOCK) 
	WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
	AND dz.StatusID = 35
	
	SELECT @ModuloRemainder = @DocsDone % @DocsPerZip
	
	SELECT @DocumentZipInformationID as DocumentZipInformationID, 
	@DocsPerZip as DocsPerZip, 
	@TotalDocs as TotalDocs, 
	@DocsDone as DocsDone, 
	@DocsFailed as DocsFailed, 
	@DocsToDo as DocsToDo, 
	@ModuloRemainder as ModuloRemainder
	
	
	;
	WITH DocsToReDo AS 
	(
		SELECT dz.*, ROW_NUMBER() OVER(ORDER BY dz.DocumentZipID) as rn
		FROM dbo.DocumentZip dz WITH (NOLOCK) 
		WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
		AND dz.StatusID = 31
	)
	UPDATE dbo.DocumentZip 
	SET StatusID = 35, DocumentFileName = null, ZipFileName = null 
	FROM dbo.DocumentZip dz 
	INNER JOIN DocsToReDo dr ON dr.DocumentZipID = dz.DocumentZipID 
	WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
	AND dz.StatusID = 31 
	AND dr.rn > (@DocsDone - @ModuloRemainder)
	
	UPDATE dbo.DocumentZip 
	SET StatusID = 35, DocumentFileName = null, ZipFileName = null, DocumentZipFileID = NULL 
	WHERE DocumentZipInformationID = @DocumentZipInformationID
	AND StatusID IN (32, 34)
	
	UPDATE dbo.DocumentZipInformation  
	SET StatusID = 35, DocumentsPerZip = cast(dbo.fnMinOf(@MaxPerZip, @DocsPerZip) as int), EmailOnError = ISNULL(EmailOnSuccess,'support@aquarium-software.com') /*Added by LB to prevent report spam*/
	WHERE DocumentZipInformationID = @DocumentZipInformationID 
	
	EXEC dbo.DocumentZip__Describe @DocumentZipInformationID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__KillAndCure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__KillAndCure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__KillAndCure] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__KillAndCure] TO [sp_executehelper]
GO
