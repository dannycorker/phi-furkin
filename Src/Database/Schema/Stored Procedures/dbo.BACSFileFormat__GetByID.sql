SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 04/08/2016
-- Description:	Gets a BACS file format by the given identity
-- =============================================
CREATE PROCEDURE [dbo].[BACSFileFormat__GetByID]
	@BACSFileFormatID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM BACSFileFormat WITH (NOLOCK) WHERE BACSFileFormatID=@BACSFileFormatID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__GetByID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BACSFileFormat__GetByID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__GetByID] TO [sp_executeall]
GO
