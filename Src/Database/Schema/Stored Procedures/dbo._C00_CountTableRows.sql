SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-05-23
-- Description:	Count Table Rows by id
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CountTableRows]
	@numberoffields int, 
	@leadid int, 
	@matterid int,
	@tdfid int, 
	@tdfv varchar(2000) = '%', 
	@tdfid2 int = NULL,
	@tdfv2 varchar(2000) = '%',
	@tdfid3 int = NULL,
	@tdfv3 varchar(2000) = '%',
	@tdfid4 int = NULL,
	@tdfv4 varchar(2000) = '%'


AS
BEGIN
	SET NOCOUNT ON;

	if @numberoffields = 1 
	begin
		select count(*) as Counter
		from tabledetailvalues tdv with (nolock)
		where tdv.leadid = @leadid
		and tdv.matterid = @matterid
		and tdv.detailfieldid = @tdfid
		and tdv.detailvalue like @tdfv
	end

	if @numberoffields = 2 
	begin
		select count(*) as Counter
		from tabledetailvalues tdv with (nolock)
		inner join tabledetailvalues tdv2 on tdv2.tablerowid = tdv.tablerowid and tdv2.detailfieldid = @tdfid2 and tdv2.detailvalue like @tdfv2
		where tdv.leadid = @leadid
		and tdv.matterid = @matterid
		and tdv.detailfieldid = @tdfid
		and tdv.detailvalue like @tdfv
	end

	if @numberoffields = 3 
	begin
		select count(*) as Counter
		from tabledetailvalues tdv with (nolock)
		inner join tabledetailvalues tdv2 on tdv2.tablerowid = tdv.tablerowid and tdv2.detailfieldid = @tdfid2 and tdv2.detailvalue like @tdfv2
		inner join tabledetailvalues tdv3 on tdv3.tablerowid = tdv.tablerowid and tdv3.detailfieldid = @tdfid3 and tdv3.detailvalue like @tdfv3
		where tdv.leadid = @leadid
		and tdv.matterid = @matterid
		and tdv.detailfieldid = @tdfid
		and tdv.detailvalue like @tdfv
	end

	if @numberoffields = 4 
	begin
		select count(*) as Counter
		from tabledetailvalues tdv with (nolock)
		inner join tabledetailvalues tdv2 on tdv2.tablerowid = tdv.tablerowid and tdv2.detailfieldid = @tdfid2 and tdv2.detailvalue like @tdfv2
		inner join tabledetailvalues tdv3 on tdv3.tablerowid = tdv.tablerowid and tdv3.detailfieldid = @tdfid3 and tdv3.detailvalue like @tdfv3
		inner join tabledetailvalues tdv4 on tdv4.tablerowid = tdv.tablerowid and tdv4.detailfieldid = @tdfid4 and tdv4.detailvalue like @tdfv4
		where tdv.leadid = @leadid
		and tdv.matterid = @matterid
		and tdv.detailfieldid = @tdfid
		and tdv.detailvalue like @tdfv
	end

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CountTableRows] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CountTableRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CountTableRows] TO [sp_executeall]
GO
