SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOffices table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_GetByCountryID]
(

	@CountryID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientOfficeID],
					[ClientID],
					[OfficeName],
					[BuildingName],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Country],
					[PostCode],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[OfficeFax],
					[CountryID],
					[LanguageID],
					[SubClientID],
					[AdminClientPersonnelID],
					[CurrencyID]
				FROM
					[dbo].[ClientOffices] WITH (NOLOCK) 
				WHERE
					[CountryID] = @CountryID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_GetByCountryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_GetByCountryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_GetByCountryID] TO [sp_executeall]
GO
