SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CustomerInformationFieldDefinitions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_Get_List]

AS


				
				SELECT
					[CustomerInformationFieldDefinitionID],
					[ClientQuestionnaireID],
					[FieldAtBeginningOfQuestionnaire],
					[OrdinalPosition],
					[FieldName],
					[FieldLabel],
					[Mandatory],
					[VerifyPhoneNumber],
					[ClientID]
				FROM
					[dbo].[CustomerInformationFieldDefinitions] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Get_List] TO [sp_executeall]
GO
