SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author: Simon Brushett
-- Create date: 2011-08-17
-- Description:	Old DAL proc, not current as no index any more.
-- =============================================


CREATE PROCEDURE [dbo].[MatterDetailValues__GetByMatterIDDetailFieldID]
(

	@MatterID INT,
	@DetailFieldID INT   
)
AS


				SELECT *
				FROM
					[dbo].[MatterDetailValues]
				WHERE
					[MatterID] = @MatterID
					AND [DetailFieldID] = @DetailFieldID
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByMatterIDDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues__GetByMatterIDDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByMatterIDDetailFieldID] TO [sp_executeall]
GO
