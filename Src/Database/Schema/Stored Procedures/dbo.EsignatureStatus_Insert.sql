SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureStatus_Insert]
(

	@EsignatureStatusID int    OUTPUT,

	@EsignatureStatusDescription varchar (50)  ,

	@EsignatureStatus varchar (50)  
)
AS


				
				INSERT INTO [dbo].[EsignatureStatus]
					(
					[EsignatureStatusDescription]
					,[EsignatureStatus]
					)
				VALUES
					(
					@EsignatureStatusDescription
					,@EsignatureStatus
					)
				-- Get the identity value
				SET @EsignatureStatusID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Insert] TO [sp_executeall]
GO
