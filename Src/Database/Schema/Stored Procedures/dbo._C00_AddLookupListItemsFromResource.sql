SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-07-30
-- Description:	Add lookup list items from a resource
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AddLookupListItemsFromResource]
	@FromResourceListIDColumn INT,
	@ToLookupListID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT
	
	SELECT @ClientID = lu.ClientID
	FROM LookupList lu WITH (NOLOCK)
	WHERE lu.LookupListID = @ToLookupListID

	INSERT INTO LookupListItems (ClientID, LookupListID, Enabled, ItemValue)
	SELECT @ClientID, @ToLookupListID, 1, rldv.DetailValue
	FROM ResourceListDetailValues rldv WITH (NOLOCK)
	WHERE rldv.DetailFieldID = @FromResourceListIDColumn
	AND NOT EXISTS (
		SELECT *
		FROM LookupListItems luli WITH (NOLOCK)
		WHERE luli.LookupListID = @ToLookupListID
		AND luli.ItemValue = rldv.DetailValue
	)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddLookupListItemsFromResource] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AddLookupListItemsFromResource] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddLookupListItemsFromResource] TO [sp_executeall]
GO
