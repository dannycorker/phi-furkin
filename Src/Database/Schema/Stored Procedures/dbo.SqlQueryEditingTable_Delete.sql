SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryEditingTable table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingTable_Delete]
(

	@SqlQueryEditingTableID int   
)
AS


				DELETE FROM [dbo].[SqlQueryEditingTable] WITH (ROWLOCK) 
				WHERE
					[SqlQueryEditingTableID] = @SqlQueryEditingTableID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Delete] TO [sp_executeall]
GO
