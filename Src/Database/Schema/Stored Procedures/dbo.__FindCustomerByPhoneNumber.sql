SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2011
-- Description:	Gets the customerid, leadID, caseID and fullname by checking the PhoneNumber matches
--				either the MobilePhone, HomeTelephone or the WorksTelephone
--				The results are limited to the top 5 since they will be returned via the web service
--				to the aquarium ultra application
-- JWG 2011-07-22 Added LeadType and filtered to first case only
-- =============================================
CREATE PROCEDURE [dbo].[__FindCustomerByPhoneNumber]
	@ClientID INT,
	@PhoneNumber VARCHAR(50),
	@RowsToShow int = 5
AS
BEGIN
	
	IF @ClientID=3 OR @ClientID=2
	BEGIN
	
		;WITH InnerSql AS 
		(
			SELECT c.Fullname, c.PostCode, c.CustomerID, l.LeadID, ca.CaseID, ca.CaseNum, lt.LeadTypeName, ma.MatterID PolicyID, ma.MatterRef, ma.RefLetter, ls.StatusName,
			ROW_NUMBER() OVER(PARTITION BY ca.LeadID ORDER BY ca.CaseNum) as rn 
			FROM dbo.Customers c WITH (NOLOCK) 
			INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
			INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
			INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
			INNER JOIN dbo.Matter ma WITH (NOLOCK) ON ma.CaseID = ca.CaseID AND ma.LeadID = l.LeadID
			INNER JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = ca.ClientStatusID
			WHERE c.ClientID = @ClientID 
			AND ((c.HomeTelephone = @PhoneNumber) OR (c.MobileTelephone = @PhoneNumber) OR (c.WorksTelephone = @PhoneNumber) OR (c.DayTimeTelephoneNumber = @PhoneNumber))
		)
		SELECT TOP (@RowsToShow) i.* 
		FROM InnerSql i 
	
	END
	ELSE
	BEGIN
	
		;WITH InnerSql AS 
		(
			SELECT c.Fullname, c.PostCode, c.CustomerID, l.LeadID, ca.CaseID, lt.LeadTypeName, 
			ROW_NUMBER() OVER(PARTITION BY ca.LeadID ORDER BY ca.CaseNum) as rn 
			FROM dbo.Customers c WITH (NOLOCK) 
			INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
			INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
			INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
			WHERE c.ClientID = @ClientID 
			AND ((c.HomeTelephone = @PhoneNumber) OR (c.MobileTelephone = @PhoneNumber) OR (c.WorksTelephone = @PhoneNumber) OR (c.DayTimeTelephoneNumber = @PhoneNumber))
		)
		SELECT TOP (@RowsToShow) i.* 
		FROM InnerSql i 
		WHERE i.rn = 1 /* First case only. CaseNum 1 doesn't always exists eg after case transfer. */
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__FindCustomerByPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__FindCustomerByPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__FindCustomerByPhoneNumber] TO [sp_executeall]
GO
