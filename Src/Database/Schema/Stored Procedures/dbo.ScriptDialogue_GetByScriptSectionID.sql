SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptDialogue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_GetByScriptSectionID]
(

	@ScriptSectionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptDialogueID],
					[ScriptSectionID],
					[ClientID],
					[Dialogue],
					[HtmlDialogue],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified]
				FROM
					[dbo].[ScriptDialogue] WITH (NOLOCK) 
				WHERE
					[ScriptSectionID] = @ScriptSectionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_GetByScriptSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_GetByScriptSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_GetByScriptSectionID] TO [sp_executeall]
GO
