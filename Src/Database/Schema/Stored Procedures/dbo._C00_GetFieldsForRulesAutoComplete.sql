SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 02-10-2014
-- Description:	Looks at the fields that are not in the resources and tables lead type
-- Includes customer level standard fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetFieldsForRulesAutoComplete]	
	@Filter VARCHAR(250),
	@ClientID INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT CONVERT(VARCHAR, df.DetailFieldID) + '/' + df.FieldName + '/' + CONVERT(VARCHAR,dfcol.DetailFieldID) + '/' + dfCol.FieldName + '/' + lt.LeadTypeName + '/' + ds.DetailFieldSubTypeName Target
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN DetailFields dfCol WITH (NOLOCK) ON dfCol.DetailFieldPageID=df.ResourceListDetailFieldPageID
	INNER JOIN DetailFieldSubType ds WITH (NOLOCK) ON ds.DetailFieldSubTypeID = df.LeadOrMatter
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID=df.LeadTypeID
	WHERE df.ClientID=@ClientID AND df.QuestionTypeID IN (14,16,19) AND 
	(df.FieldName LIKE '%'+ @Filter + '%' OR dfCol.FieldName LIKE '%'+ @Filter + '%' OR 
	(CONVERT(VARCHAR, df.DetailFieldID) + '/' + df.FieldName + '/' + CONVERT(VARCHAR,dfcol.DetailFieldID) + '/' + dfCol.FieldName + '/' + lt.LeadTypeName + '/' + ds.DetailFieldSubTypeName) LIKE '%' + @Filter + '%')
	AND LeadTypeName NOT LIKE '%Resources%Tables%'
	UNION
	SELECT CONVERT(VARCHAR, DetailFieldID) + '/' + df.FieldName + '/' + lt.LeadTypeName + '/' + ds.DetailFieldSubTypeName Target
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN DetailFieldSubType ds WITH (NOLOCK) ON ds.DetailFieldSubTypeID = df.LeadOrMatter
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID=df.LeadTypeID
	WHERE df.ClientID=@ClientID AND questiontypeid NOT IN (14,20,21) AND 
	(FieldName LIKE '%' + @Filter + '%' OR 
	(CONVERT(VARCHAR, DetailFieldID) + '/' + df.FieldName + '/' + lt.LeadTypeName + '/' + ds.DetailFieldSubTypeName) LIKE '%' + @Filter + '%')
	AND LeadTypeName NOT LIKE '%Resources%Tables%' AND LeadOrMatter<>4
	UNION
	SELECT t.ObjectTypeName + '.' + f.FieldName Target 
	FROM DataLoaderObjectField f WITH (NOLOCK) 
	INNER JOIN DataLoaderObjectType t WITH (NOLOCK) ON t.DataLoaderObjectTypeID = f.DataLoaderObjectTypeID
	WHERE f.DataLoaderObjectTypeID=1 AND f.FieldName LIKE '%'+ @Filter + '%'
	/*UNION
	SELECT OBJECT_NAME(object_id) target
	FROM sys.sql_modules WITH (NOLOCK) 
	WHERE OBJECT_NAME(object_id) like '%fn_C00_1273_RulesEngine_%'*/
	ORDER BY target ASC


END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFieldsForRulesAutoComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetFieldsForRulesAutoComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFieldsForRulesAutoComplete] TO [sp_executeall]
GO
