SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_Update]
(

	@ClientStyleID int   ,

	@ClientID int   ,

	@StyleRuleID int   ,

	@StyleValue varchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientStyle]
				SET
					[ClientID] = @ClientID
					,[StyleRuleID] = @StyleRuleID
					,[StyleValue] = @StyleValue
				WHERE
[ClientStyleID] = @ClientStyleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Update] TO [sp_executeall]
GO
