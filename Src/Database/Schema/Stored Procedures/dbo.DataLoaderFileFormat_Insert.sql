SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderFileFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileFormat_Insert]
(

	@DataLoaderFileFormatID int   ,

	@FileFormatName varchar (250)  ,

	@FileFormatDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderFileFormat]
					(
					[DataLoaderFileFormatID]
					,[FileFormatName]
					,[FileFormatDescription]
					)
				VALUES
					(
					@DataLoaderFileFormatID
					,@FileFormatName
					,@FileFormatDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileFormat_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Insert] TO [sp_executeall]
GO
