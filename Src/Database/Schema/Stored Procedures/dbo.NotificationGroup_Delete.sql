SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the NotificationGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroup_Delete]
(

	@NotificationGroupID int   
)
AS


				DELETE FROM [dbo].[NotificationGroup] WITH (ROWLOCK) 
				WHERE
					[NotificationGroupID] = @NotificationGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_Delete] TO [sp_executeall]
GO
