SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyMappingSystemCredential table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyMappingSystemCredentialID int   = null ,

	@ThirdPartySystemID int   = null ,

	@ClientID int   = null ,

	@ConsumerKey varchar (500)  = null ,

	@ConsumerSecret varchar (500)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyMappingSystemCredentialID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [ConsumerKey]
	, [ConsumerSecret]
    FROM
	[dbo].[ThirdPartyMappingSystemCredential] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingSystemCredentialID] = @ThirdPartyMappingSystemCredentialID OR @ThirdPartyMappingSystemCredentialID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ConsumerKey] = @ConsumerKey OR @ConsumerKey IS NULL)
	AND ([ConsumerSecret] = @ConsumerSecret OR @ConsumerSecret IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyMappingSystemCredentialID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [ConsumerKey]
	, [ConsumerSecret]
    FROM
	[dbo].[ThirdPartyMappingSystemCredential] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingSystemCredentialID] = @ThirdPartyMappingSystemCredentialID AND @ThirdPartyMappingSystemCredentialID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ConsumerKey] = @ConsumerKey AND @ConsumerKey is not null)
	OR ([ConsumerSecret] = @ConsumerSecret AND @ConsumerSecret is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Find] TO [sp_executeall]
GO
