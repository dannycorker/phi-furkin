SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












-- =============================================
-- Author:		Chris Townsend
-- Create date: 21st January 2008
-- Description:	Gets Workflow Group Assignments by the ClientID 
--				and optionally the UserID (ClientPersonnelID) and WorkflowGroupID
--				includes full Group Name and Assignee Name
-- =============================================
CREATE PROCEDURE [dbo].[Workflow_GetAssignments] 

@ClientID int,
@UserID int = 0,
@WorkflowGroupID int = 0

AS
BEGIN

	SET NOCOUNT ON

	SELECT	wga.WorkflowGroupAssignmentID, 
			wga.WorkflowGroupID,
			wg.[Name] AS 'GroupName',
			wga.ClientPersonnelID,
			cp.FirstName + ' ' + cp.LastName AS 'AssigneeName',
			wga.Priority
	FROM	WorkflowGroupAssignment wga

	INNER JOIN ClientPersonnel cp ON wga.ClientPersonnelID = cp.ClientPersonnelID
	INNER JOIN WorkflowGroup wg ON wga.WorkflowGroupID = wg.WorkflowGroupID	

	WHERE	wga.ClientID = @ClientID
	
	AND (@UserID = 0 OR wga.ClientPersonnelID = @UserID)
	AND (@WorkflowGroupID = 0 OR wga.WorkflowGroupID = @WorkflowGroupID)
	-- AND CASE WHEN @UserID = 0 THEN 1 WHEN wga.ClientPersonnelID = @UserID THEN 1 ELSE 0 END = 1

	ORDER BY wga.Priority

END












GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_GetAssignments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Workflow_GetAssignments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_GetAssignments] TO [sp_executeall]
GO
