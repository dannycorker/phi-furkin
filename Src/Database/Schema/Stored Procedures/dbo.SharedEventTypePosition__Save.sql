SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-09-2014
-- Description:	Saves the visio x,y position of the shared event type
-- =============================================
CREATE PROCEDURE [dbo].[SharedEventTypePosition__Save]
	@EventTypeID INT,
	@ClientID INT,
	@LeadTypeID INT,
	@VisioX DECIMAL(18,8),
	@VisioY DECIMAL(18,8)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Count INT
	SELECT @Count = Count(*) FROM SharedEventTypePosition WITH (NOLOCK) 
	WHERE EventTypeID = @EventTypeID AND LeadTypeID = @LeadTypeID AND ClientID = @ClientID
	
	IF @Count = 0 -- INSERT new record
	BEGIN
		
		INSERT INTO SharedEventTypePosition (EventTypeID, ClientID, LeadTypeID,VisioX, VisioY)
		VALUES (@EventTypeID,@ClientID,@LeadTypeID,@VisioX,@VisioY)		
	
	END
	ELSE -- UPDATE
	BEGIN

		DECLARE @ID INT

		SELECT TOP 1 @ID = SharedEventTypePositionID FROM SharedEventTypePosition WITH (NOLOCK) 
		WHERE EventTypeID = @EventTypeID AND LeadTypeID = @LeadTypeID AND ClientID = @ClientID
	
		UPDATE SharedEventTypePosition
		SET VisioX=@VisioX, VisioY = @VisioY
		WHERE SharedEventTypePositionID = @ID
		
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SharedEventTypePosition__Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SharedEventTypePosition__Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SharedEventTypePosition__Save] TO [sp_executeall]
GO
