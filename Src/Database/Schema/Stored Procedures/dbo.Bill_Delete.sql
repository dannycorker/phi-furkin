SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Bill table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill_Delete]
(

	@BillID int   
)
AS


				DELETE FROM [dbo].[Bill] WITH (ROWLOCK) 
				WHERE
					[BillID] = @BillID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Delete] TO [sp_executeall]
GO
