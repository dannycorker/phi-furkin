SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LDAPConfig table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LDAPConfig_Update]
(

	@LDAPConfigID int   ,

	@ClientID int   ,

	@QueryString varchar (2000)  ,

	@QueryAccountName varchar (2000)  ,

	@QueryAccountPassword varchar (2000)  ,

	@Enabled bit   ,

	@Domain varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LDAPConfig]
				SET
					[ClientID] = @ClientID
					,[QueryString] = @QueryString
					,[QueryAccountName] = @QueryAccountName
					,[QueryAccountPassword] = @QueryAccountPassword
					,[Enabled] = @Enabled
					,[Domain] = @Domain
				WHERE
[LDAPConfigID] = @LDAPConfigID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Update] TO [sp_executeall]
GO
