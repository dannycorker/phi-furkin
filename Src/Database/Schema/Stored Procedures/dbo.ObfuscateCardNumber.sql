SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 23/08/2016
-- Description:	Masks the card number and updates the third party field value with the masked card number
-- =============================================
CREATE PROCEDURE [dbo].[ObfuscateCardNumber]
	@CardMaskingMethodID INT,
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@LeadTypeID INT,	
	@ThirdPartyFieldID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FieldID INT, @DetailFieldSubtypeID INT 
	SELECT @FieldID = tpfm.DetailFieldID, @DetailFieldSubtypeID = df.LeadOrMatter FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tpfm.DetailFieldID
	WHERE tpfm.ClientID = @ClientID AND tpfm.ThirdPartyFieldID = @ThirdPartyFieldID AND tpfm.LeadTypeID = @LeadTypeID

	DECLARE @CardNumber VARCHAR(2000)
	SELECT @CardNumber = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,@ThirdPartyFieldID)

	DECLARE @xx VARCHAR(20) DECLARE @length INT, @MaskedCreditCardNumber VARCHAR(2000)
	SET @length = 20 
	SET @xx = 'XXXXXXXXXXXXXXXXXXX'

	IF @CardMaskingMethodID = 1
	BEGIN
		SET @MaskedCreditCardNumber = SUBSTRING(@xx, 0, @length - LEN(RIGHT(@CardNumber, 4))) + RIGHT(@CardNumber, 4)		
	END
	ELSE IF @CardMaskingMethodID = 2
	BEGIN
		SET @MaskedCreditCardNumber = LEFT(@CardNumber, 4) + SUBSTRING(@xx, 0, @length - LEN(LEFT(@CardNumber, 4)))		
	END
	ELSE IF @CardMaskingMethodID = 3
	BEGIN 
		SET @MaskedCreditCardNumber = SUBSTRING(@xx, 0, @length) 
	END
	ELSE
	BEGIN 
		SET @MaskedCreditCardNumber = SUBSTRING(@xx, 0, @length) 
	END

	DECLARE @ObjectID INT
	IF @DetailFieldSubtypeID = 1
	BEGIN
		SELECT @ObjectID = @LeadID 		
	END

	IF @DetailFieldSubtypeID = 2
	BEGIN
		SELECT @ObjectID = @MatterID
	END
		
	IF @DetailFieldSubtypeID = 10
	BEGIN
		SELECT @ObjectID = @CustomerID 		
	END
	
	IF @DetailFieldSubtypeID = 11
	BEGIN
		SELECT @ObjectID = @CaseID 
	END

	EXEC dbo._C00_SimpleValueIntoField @FieldID, @MaskedCreditCardNumber, @ObjectID, NULL
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ObfuscateCardNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObfuscateCardNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObfuscateCardNumber] TO [sp_executeall]
GO
