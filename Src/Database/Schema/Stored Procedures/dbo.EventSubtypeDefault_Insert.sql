SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventSubtypeDefault table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_Insert]
(

	@EventSubtypeDefaultID int    OUTPUT,

	@ClientID int   ,

	@EventSubtypeID int   ,

	@EventTypeID int   ,

	@LeadTypeID int   ,

	@ClientPersonnelID int   
)
AS


				
				INSERT INTO [dbo].[EventSubtypeDefault]
					(
					[ClientID]
					,[EventSubtypeID]
					,[EventTypeID]
					,[LeadTypeID]
					,[ClientPersonnelID]
					)
				VALUES
					(
					@ClientID
					,@EventSubtypeID
					,@EventTypeID
					,@LeadTypeID
					,@ClientPersonnelID
					)
				-- Get the identity value
				SET @EventSubtypeDefaultID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Insert] TO [sp_executeall]
GO
