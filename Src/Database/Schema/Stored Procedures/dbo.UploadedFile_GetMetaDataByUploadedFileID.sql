SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UploadedFile table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_GetMetaDataByUploadedFileID]
(

	@UploadedFileID int   
)
AS

				SELECT
					[UploadedFileID],
					[ClientID],
					[FolderID],
					[UploadedFileName],
					[UploadedFileTitle],
					[UploadedFileDescription],					
					[UploadedFileGUID],
					[UploadedFileSize],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[UploadedFile]
				WHERE
					[UploadedFileID] = @UploadedFileID
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_GetMetaDataByUploadedFileID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_GetMetaDataByUploadedFileID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_GetMetaDataByUploadedFileID] TO [sp_executeall]
GO
