SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson & Alex Elger
-- Create date: 04-06-2015
-- Description:	Bulk Variable Header Dataload
--			  :	Script Import - imports customer,lead,case,matter, contacts, departments, offices and detail values
-- @ActionToPerform : when a duplicate is found
		-- Keep_Existing = 1,  Keep the existing record and reject the new one - so essentially do nothing!
        -- Keep_New = 2,       Keep the new record and remove the existing
        -- Reset_Older = 3,    Update the existing record with the new information        
        -- Insert = 4          Inserts the new record regardless of duplicate
-- =============================================
CREATE PROCEDURE [dbo].[VariableHeaderDataLoadBulk__Import]
	@ClientID INT,
	@LeadTypeID INT,
	@ListID INT,
	@ImportID INT,
	@ActionToPerform INT,
	@HeaderColumns dbo.tvpIntVarchar READONLY,
	@UserID INT
AS
BEGIN

	SET NOCOUNT ON;

	declare @SQL NVARCHAR(MAX) 

	DECLARE @CustomerXML XML, 
			@LeadXml XML,
			@CaseXML XML,
			@MatterXml XML,
			@PartnerXml XML,
			@OfficeXml XML,
			@DepartmentXml XML,
			@ContactXml XML,
			@DetailValuesXml XML,
			@ThirdPartyLeadID INT

	DECLARE @TempXML TABLE (TempXML XML)

	DECLARE @VariableHeaderDataloadBulkID INT, 
			@TestID INT = 0,
			@RowCounter INT

	SELECT TOP 1 @VariableHeaderDataloadBulkID = v.VariableHeaderDataloadBulkID, @ThirdPartyLeadID = v.Value1
	FROM VariableHeaderDataloadBulk v
	WHERE v.CustomerID IS NULL
	ORDER BY VariableHeaderDataloadBulkID ASC

	--insert into @HeaderColumns (AnyID, AnyValue)
	--values (1, '1.1'),
	--		(2, '1.2'),
	--		(3, '1.3'),
	--		(4, '1.4'),
	--		(5, '1.5'),
	--		(6, '1.7'),
	--		(7, '10.54')

	WHILE @VariableHeaderDataloadBulkID > @TestID
	BEGIN

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 = '1'

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)



		END

		PRINT @SQL

		SELECT @CustomerXML = t.TempXML
		FROM @TempXML t

		--SELECT @CustomerXML AS [CustomerXML]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 = '2'

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @LeadXML = t.TempXML
		FROM @TempXML t

		--SELECT @LeadXML AS [LeadXML]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 = '3'

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @CaseXML = t.TempXML
		FROM @TempXML t

		--SELECT @CaseXML AS [CaseXML]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 = '4'

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @MatterXML = t.TempXML
		FROM @TempXML t

		--SELECT @MatterXML AS [MatterXML]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 IN ('14')

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @PartnerXml = t.TempXML
		FROM @TempXML t

		--SELECT @PartnerXml AS [PartnerXML]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 IN ('9')

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @OfficeXml = t.TempXML
		FROM @TempXML t
		
		/*2016-05-24 ACE Temp Log Office XML*/
		INSERT INTO LogXML (ClientID, ContextID, ContextVarchar, LogDateTime, LogXMLEntry)
		VALUES (@ClientID, @VariableHeaderDataloadBulkID, 'VHDLOffice', dbo.fn_GetDate_Local(), @OfficeXml)

		--SELECT @OfficeXml AS [OfficeXml]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 IN ('8')

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @DepartmentXml = t.TempXML
		FROM @TempXML t

		--SELECT @DepartmentXml AS [DepartmentXml]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = (SELECT TOP 1 '

		SELECT @SQL += 'VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [' + dt.ObjectTypeName + '/' + d.FieldName + '], '
		FROM @HeaderColumns t
		CROSS APPLY dbo.fnColsFromString(t.AnyValue, '.') a
		INNER JOIN DataLoaderObjectField d WITH (NOLOCK) ON d.DataLoaderObjectFieldID = a.Col2
		INNER JOIN DataLoaderObjectType dt WITH (NOLOCK) ON dt.DataLoaderObjectTypeID = a.Col1
		WHERE a.Col1 IN ('10')

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @ContactXml = t.TempXML
		FROM @TempXML t

		--SELECT @ContactXml AS [ContactXml]

		DELETE FROM @TempXML

		SELECT @SQL = 'declare @XML XML

		SET @XML = ( SELECT TOP 1 '

		SELECT @SQL += '' + t.AnyValue + ' AS [Values'+CONVERT(VARCHAR,AnyID)+'/DetailFieldID], VALUE' + CONVERT(VARCHAR,AnyID) + ' AS [Values' + CONVERT(VARCHAR,AnyID) + '/DetailValue],'
		FROM @HeaderColumns t
		WHERE t.AnyID > 1
		AND (dbo.fnIsInt(t.AnyValue) = 1 AND CONVERT(NUMERIC,t.AnyValue) > 16)

		SELECT @RowCounter = @@ROWCOUNT

		SELECT @SQL = LEFT(@SQL, LEN(@SQL)-1)

		--SELECT @SQL = @SQL + ' INTO #temp FROM VariableHeaderDataloadBulk WITH (NOLOCK)'

		SELECT @SQL = @SQL + ' FROM VariableHeaderDataloadBulk WITH (NOLOCK) WHERE VariableHeaderDataloadBulkID = ' + CONVERT(VARCHAR,@VariableHeaderDataloadBulkID)

		--SELECT @SQL = @SQL + ' SELECT * FROM #Temp'

		SELECT @SQL = @SQL + ' FOR XML PATH (''DetailFieldID''))'

		SELECT @SQL = @SQL + ' SELECT @XML '

		--SELECT @SQL = @SQL + ' Drop Table #Temp'

		IF @RowCounter > 0
		BEGIN

			INSERT INTO @TempXML (TempXML)
			EXEC (@SQL)

		END

		PRINT @SQL

		SELECT @DetailValuesXml = t.TempXML
		FROM @TempXML t

		--SELECT @DetailValuesXml AS [DetailValuesXml]

		DELETE FROM @TempXML

		SELECT @TestID = @VariableHeaderDataloadBulkID

		PRINT @VariableHeaderDataloadBulkID

		EXEC dbo.VariableHeaderDataLoad__Import @ClientID, @ThirdPartyLeadID, @ListID, @ImportID, @ActionToPerform, @CustomerXML, @LeadXML, @CaseXML, @MatterXML, @PartnerXml, @OfficeXML, @DepartmentXML, @ContactXML, @DepartmentXml, @UserID, @LeadTypeID

		/*Now, move onto the next row..*/
		SELECT TOP 1 @VariableHeaderDataloadBulkID = v.VariableHeaderDataloadBulkID, @ThirdPartyLeadID = v.Value1
		FROM VariableHeaderDataloadBulk v
		WHERE v.CustomerID IS NULL
		AND v.VariableHeaderDataloadBulkID > @VariableHeaderDataloadBulkID
		ORDER BY VariableHeaderDataloadBulkID ASC

	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoadBulk__Import] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoadBulk__Import] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoadBulk__Import] TO [sp_executeall]
GO
