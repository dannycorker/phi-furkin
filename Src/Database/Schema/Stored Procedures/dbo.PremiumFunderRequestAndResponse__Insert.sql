SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-11-15
-- Description:	Insert into PremiumFunderRequest and PremiumFunderResponse
-- 2020-01-08 PR Updated Field lengths to correspond with actual column sizes.
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderRequestAndResponse__Insert]
	@PremiumFunderRequestID INT = 0,
	@ClientID INT,
	@CustomerID INT = 0,
	@MatterID INT = NULL,
	@Title INT = NULL,
	@Firstname VARCHAR(100) = NULL,
	@Lastname VARCHAR(100) = NULL,
	@PostCode VARCHAR(10) = NULL,
	@Email VARCHAR(250) = NULL,
	@externalQuoteReference NVARCHAR(250),
	@RequestJson NVARCHAR(MAX),
	@ResponseJson NVARCHAR(MAX),
	@WhenCreated DATETIME,
    @WhoCreated INT

AS
BEGIN

	SET NOCOUNT ON;


	IF ISNULL(@CustomerID,0) = 0 
	BEGIN
		INSERT dbo.Customers (ClientID, TitleID, FirstName, LastName, PostCode, EmailAddress, IsBusiness)	
		VALUES (@ClientID, @Title, @Firstname, @Lastname, ISNULL(@PostCode, ''), ISNULL(@Email,''), 0)				
		SELECT @CustomerID = SCOPE_IDENTITY()
	END

	IF @PremiumFunderRequestID  = 0
	BEGIN	

		INSERT INTO PremiumFunderRequest ([ClientID],[CustomerID],[MatterID],[RequestJson], [externalQuoteReference], [WhenCreated],[WhoCreated])
		VALUES (@ClientID, @CustomerID, @MatterID, @RequestJson, @externalQuoteReference, @WhenCreated, @WhoCreated)
     
		SELECT @PremiumFunderRequestID = SCOPE_IDENTITY()

		INSERT INTO [PremiumFunderResponse] ([PremiumFunderRequestID],[ClientID],[CustomerID],[MatterID],[ResponseJson],[WhenCreated],[WhoCreated])
		VALUES (@PremiumFunderRequestID, @ClientID, @CustomerID, @MatterID, @ResponseJson, @WhenCreated, @WhoCreated)

	END
    ELSE
	BEGIN

		UPDATE request
		SET RequestJson = @RequestJson, WhenCreated = @WhenCreated, WhoCreated = @WhoCreated
		FROM PremiumFunderRequest request WITH (NOLOCK)
		WHERE request.PremiumFunderRequestID = @PremiumFunderRequestID

		UPDATE response
		SET ResponseJson = @ResponseJson, WhenCreated = @WhenCreated, WhoCreated = @WhoCreated
		FROM PremiumFunderResponse response WITH (NOLOCK)
		WHERE response.PremiumFunderRequestID = @PremiumFunderRequestID

	END

	SELECT @PremiumFunderRequestID AS PremiumFunderRequestID, @CustomerID AS CustomerID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderRequestAndResponse__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderRequestAndResponse__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderRequestAndResponse__Insert] TO [sp_executeall]
GO
