SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - New terms received from PH (no match)
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_155221]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@MattersToCopy					dbo.tvpIntInt
			,@MatterID1						INT


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

			
	/* There is no match, so clear temporary match fields ahead of the real lead set up	*/
	IF @EventTypeID IN (155221) -- New terms received from PH (no match)
	BEGIN
	
		-- Just delete the premium & premium history tables rows & the premium history document merge fields	
		DELETE FROM TableRows WHERE DetailFieldID IN (170088,175336) AND MatterID=@MatterID
		UPDATE MatterDetailValues SET DetailValue='' 
		FROM MatterDetailValues mdv 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID=mdv.DetailFieldID AND df.DetailFieldPageID=19046
		-- Ticket #35677
		--WHERE @MatterID=@MatterID
		WHERE MatterID=@MatterID
			AND df.DetailFieldID <> 175453 -- Do NOT update pet number
		
		-- ..and if multipet update match details on other matters so same account is used
		INSERT INTO @MattersToCopy
		SELECT * FROM dbo.fn_C00_1272_Policy_GetPreMatchPetList(@CustomerID)
		
		IF EXISTS ( SELECT * FROM @MattersToCopy WHERE ID2 > 1 ) -- multipet
		BEGIN
		
			SELECT @MatterID1=ID1 FROM @MattersToCopy WHERE ID2 = 1 
		
			UPDATE mdv
			SET DetailValue=CASE mdv.DetailFieldID 
					WHEN 175285 THEN CAST(@MatterID1 AS VARCHAR)
					WHEN 175279 THEN ''
					WHEN 175280 THEN '72330'
					END
			FROM MatterDetailValues mdv 
			INNER JOIN @MattersToCopy mtc ON mdv.MatterID=mtc.ID1
			WHERE mdv.DetailFieldID IN (175285,175279,175280)
				AND mtc.ID2<>1 -- always leave the first Matter as Resolved - NO MATCH			
		
		END
		
	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155221] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_155221] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155221] TO [sp_executeall]
GO
