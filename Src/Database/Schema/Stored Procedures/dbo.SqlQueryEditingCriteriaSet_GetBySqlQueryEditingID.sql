SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteriaSet table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingCriteriaSetID],
					[ClientID],
					[SqlQueryEditingID],
					[SetName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[SqlQueryEditingCriteriaSet] WITH (NOLOCK) 
				WHERE
					[SqlQueryEditingID] = @SqlQueryEditingID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingID] TO [sp_executeall]
GO
