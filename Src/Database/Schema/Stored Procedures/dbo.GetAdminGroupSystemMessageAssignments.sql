SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 17/03/2010
-- Description:	Gets all admin groups along with their system message assignments
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminGroupSystemMessageAssignments]
	-- Add the parameters for the stored procedure here
	@ClientID int,
	@SystemMessageID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT cpag.ClientPersonnelAdminGroupID, cpag.GroupName, sma.SystemMessageID
	FROM dbo.ClientPersonnelAdminGroups cpag WITH (NOLOCK) 
	LEFT JOIN dbo.SystemMessageAssignment sma WITH (NOLOCK) 
		ON cpag.ClientPersonnelAdminGroupID = sma.ClientPersonnelAdminGroupID
		AND sma.SystemMessageID = @SystemMessageID
	WHERE cpag.ClientID = @ClientID
    END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminGroupSystemMessageAssignments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAdminGroupSystemMessageAssignments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminGroupSystemMessageAssignments] TO [sp_executeall]
GO
