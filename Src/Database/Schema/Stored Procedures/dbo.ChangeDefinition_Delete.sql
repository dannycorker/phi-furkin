SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChangeDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDefinition_Delete]
(

	@ChangeDefinitionID int   
)
AS


				DELETE FROM [dbo].[ChangeDefinition] WITH (ROWLOCK) 
				WHERE
					[ChangeDefinitionID] = @ChangeDefinitionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDefinition_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Delete] TO [sp_executeall]
GO
