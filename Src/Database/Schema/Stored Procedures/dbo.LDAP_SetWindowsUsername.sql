SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================
-- Author:		Jan Wilson
-- Create date: 16-12-2014
-- Description:	Updates the windows username where LDAP 
--              is used to authenticate the account
-- ===========================================================
CREATE PROCEDURE [dbo].[LDAP_SetWindowsUsername]
(
	@ClientID							INT,
	@ClientPersonnelID					INT,
	@WindowsUsername					VARCHAR(255)
)
AS
	DECLARE @LDAPConfigID						INT	
	DECLARE @ThirdPartySystemID					INT = 47
	DECLARE @ThirdPartyMappingUserCredentialID  INT
	
	SELECT TOP 1
		@ThirdPartyMappingUserCredentialID = TPMUC.ThirdPartyMappingUserCredentialID,
		@LDAPConfigID = TPMUC.LDAPConfigID
	FROM
		dbo.ThirdPartyMappingUserCredential TPMUC WITH (NOLOCK) 
	WHERE
		TPMUC.UserID = @ClientPersonnelID
		AND TPMUC.ClientID = @ClientID
		AND TPMUC.ThirdPartySystemID = @ThirdPartySystemID
	
	IF (@ThirdPartyMappingUserCredentialID IS NULL)
	BEGIN
		-- Create the entry if it can't be found
		SELECT @LDAPConfigID = LDAP.LDAPConfigID FROM dbo.LDAPConfig LDAP WITH (NOLOCK) WHERE LDAP.ClientID = @ClientID 
	
		EXEC dbo.ThirdPartyMappingUserCredential_Insert   
			@ThirdPartyMappingUserCredentialID, @ThirdPartySystemID, @ClientID, @ClientPersonnelID, @WindowsUsername, 
			'',				-- Password
			'',             -- Salt
			'',             -- Domain
			0,				-- Attempted Logins
			0,				-- Account Disabled
			NULL,			-- Third Party Client ID
			@LDAPConfigID,  -- LDAP Config Id
			NULL			-- Impersonate Third Party System ID 
	END
	ELSE
	BEGIN
		-- An entry was found, so update it!
		UPDATE dbo.ThirdPartyMappingUserCredential
		SET UserName = @WindowsUsername
		WHERE ThirdPartyMappingUserCredentialID = @ThirdPartyMappingUserCredentialID 
		AND ClientID = @ClientID
	END

	RETURN @ThirdPartyMappingUserCredentialID

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAP_SetWindowsUsername] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAP_SetWindowsUsername] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAP_SetWindowsUsername] TO [sp_executeall]
GO
