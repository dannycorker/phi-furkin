SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2011-12-08
-- Description:	Allows a quick insert into the portal user table correctly setting the context which this table enforces
-- JWG 2017-03-17 #42393 Cannot have these distributed transactions from other DB clusters
-- =============================================


CREATE PROCEDURE [dbo].[PortalUser_Insert_Helper]
(
	@ClientID INT,
	@CustomerID INT,
	@Username VARCHAR(50),
	@Password VARCHAR(65),
	@Salt VARCHAR (25),
	@ClientPersonnelID INT,
	@Enabled BIT = 1,
	@AttemptedLogins INT = 0,
	@PortalUserGroupID INT = NULL
)
AS

BEGIN
	
	-- If we don't have an outer tranasction e.g. from the lead event 
	/*DECLARE @TranCount INT = @@TRANCOUNT
	IF @TranCount = 0
	BEGIN
		BEGIN TRAN
	END*/
	
	BEGIN TRY
	
		-- Get a new key from the master DB
		DECLARE @MasterPortalUserID INT
		EXEC @MasterPortalUserID = AquariusMaster.dbo.AQ_PortalUser_Insert @ClientID, @Username
					
		IF @MasterPortalUserID > 0
		BEGIN
		
		
			-- Set context info to allow access to table
			DECLARE @ContextInfo VARBINARY(100) = CAST('PortalUser' AS VARBINARY)
			SET CONTEXT_INFO @ContextInfo
			
			SET IDENTITY_INSERT [dbo].[PortalUser] ON
						
			INSERT INTO [dbo].[PortalUser] ([PortalUserID], [ClientID], [CustomerID], [Username], [Password], [Salt], [ClientPersonnelID], [Enabled], [AttemptedLogins], [PortalUserGroupID])
			VALUES (@MasterPortalUserID, @ClientID, @CustomerID, @Username, @Password, @Salt, @ClientPersonnelID, @Enabled, @AttemptedLogins, @PortalUserGroupID)
			
			-- If we don't have an outer tran we can commit here
			/*IF @TranCount = 0
			BEGIN
				COMMIT
			END*/

			-- Clear the things we set above
			SET IDENTITY_INSERT [dbo].[PortalUser] OFF
			SET CONTEXT_INFO 0x0
			
		END
		
		-- Get the identity value
		RETURN @MasterPortalUserID
		
	END TRY			
	BEGIN CATCH    
	    
	    -- If we have any tran open then we need to rollback
	    /*IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK
		END*/
		
		-- Clear the things we set above
		SET IDENTITY_INSERT [dbo].[PortalUser] OFF
		SET CONTEXT_INFO 0x0
		
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
		DECLARE @ErrorState INT = ERROR_STATE()

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
        

    END CATCH
			
END




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Insert_Helper] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_Insert_Helper] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Insert_Helper] TO [sp_executeall]
GO
