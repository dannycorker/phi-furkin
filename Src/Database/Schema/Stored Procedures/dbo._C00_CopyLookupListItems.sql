SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-08-24
-- Description:	Copy LookuplistItems
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyLookupListItems] 
@FromLookupListID int,
@ToClientID int,
@CopyDisabledItems bit = 1,
@ToLookupListID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @LookupList TABLE (LookupListID int)
	
	/*If LookupListID is not supplied, copy the lookup into the new client*/
	IF @ToLookupListID IS NULL
	BEGIN
	
		INSERT INTO LookupList (LookupListName, LookupListDescription, ClientID, Enabled, SortOptionID)
		OUTPUT inserted.LookupListID INTO @LookupList (LookupListID)
		SELECT LookupListName, LookupListDescription, @ToClientID, Enabled, SortOptionID
		FROM LookupList lu WITH (NOLOCK)
		WHERE lu.LookupListID = @FromLookupListID
			
		/*Cant use scope identity here as it's a insert into select from table statement*/
		SELECT @ToLookupListID = l.LookupListID
		FROM @LookupList l
		
		SELECT @ToLookupListID
	
	END

	IF @ToLookupListID IS NOT NULL
	BEGIN

		/*Copy the values over into the new lookup*/
		INSERT INTO LookupListItems (LookupListID, ItemValue, ClientID, Enabled, SortOrder)
		SELECT @ToLookupListID, ItemValue, @ToClientID, Enabled, SortOrder
		FROM LookupListItems luli WITH (NOLOCK)
		WHERE luli.LookupListID = @FromLookupListID
		AND ((@CopyDisabledItems = 1) OR (@CopyDisabledItems = 0 AND luli.Enabled = 1))
		
		SELECT @@ROWCOUNT AS [Rows Added]

	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyLookupListItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyLookupListItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyLookupListItems] TO [sp_executeall]
GO
