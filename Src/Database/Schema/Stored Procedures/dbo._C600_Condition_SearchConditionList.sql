SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-10-23
-- Description:	Condition search 
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Condition_SearchConditionList] 
(
	@Search VARCHAR(100)
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT rdvCondition.ResourceListID,rdvCondition.DetailValue Condition, rdvCategory.DetailValue Category
	FROM ResourceListDetailValues rdvCondition WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvCategory WITH (NOLOCK) ON rdvCategory.ResourceListID=rdvCondition.ResourceListID AND rdvCategory.DetailFieldID=177424
	WHERE rdvCondition.ClientID=@ClientID AND rdvCondition.DetailFieldID=177423 AND rdvCondition.DetailValue like '%' + @Search + '%'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Condition_SearchConditionList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Condition_SearchConditionList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Condition_SearchConditionList] TO [sp_executeall]
GO
