SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-12-04
-- Description: Adds an entry to the LeadEventCallback table
-- =============================================
CREATE PROCEDURE [dbo].[LeadEventCallbacks__LogCallback]
(
	@ClientID INT,
    @LeadEventID INT,
    @ProviderID INT,
    @CallbackTypeID INT,
    @EventDateTime DATETIME,
    @SentTo VARCHAR(150),
    @IpAddress VARCHAR(20),
    @Url VARCHAR(500)
)
AS
BEGIN

declare @logentry varchar(2000)
SELECT @LogEntry = 'I was called'
EXEC _C00_LogIt 'Info', 'LeadEventCallbacks__LogCallback', 'LeadEventCallbacks__LogCallback', @LogEntry,69
		

	INSERT dbo.LeadEventCallbacks(ClientID, LeadEventID, ProviderID, CallbackTypeID, EventDateTime, SentTo, IpAddress, Url)
	VALUES (@ClientID, @LeadEventID, @ProviderID, @CallbackTypeID, @EventDateTime, @SentTo, @IpAddress, @Url)
	
	DECLARE @EventTypeID INT
			
	SELECT @EventTypeID = le.EventTypeID
	FROM dbo.LeadEvent le WITH(NOLOCK)
	WHERE le.LeadEventID = @LeadEventID
	
	IF @EventTypeID IN (155342) AND @CallbackTypeID = 79 -- click on Confirmation of Pet Photo link
	BEGIN
	
		EXEC dbo._C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, 155978, 44645
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventCallbacks__LogCallback] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventCallbacks__LogCallback] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventCallbacks__LogCallback] TO [sp_executeall]
GO
