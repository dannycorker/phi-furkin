SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuestionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionTypes_Insert]
(

	@QuestionTypeID int    OUTPUT,

	@Name varchar (50)  ,

	@Description varchar (50)  ,

	@Multiselect bit   ,

	@EcatcherField bit   
)
AS


				
				INSERT INTO [dbo].[QuestionTypes]
					(
					[Name]
					,[Description]
					,[Multiselect]
					,[EcatcherField]
					)
				VALUES
					(
					@Name
					,@Description
					,@Multiselect
					,@EcatcherField
					)
				-- Get the identity value
				SET @QuestionTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionTypes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Insert] TO [sp_executeall]
GO
