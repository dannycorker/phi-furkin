SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-03
-- Description:	Get the full file and path of a zip file
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipFile__GetFileAndPath]
	@DocumentZipFileID int, 
	@ClientID int
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/* Get the full file and path of the zip file, as long as the ClientID is correct */
	SELECT dzf.ServerMapPath 
	FROM dbo.DocumentZipFile dzf WITH (NOLOCK) 
	WHERE dzf.DocumentZipFileID = @DocumentZipFileID 
	AND dzf.ClientID = @ClientID 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__GetFileAndPath] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipFile__GetFileAndPath] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__GetFileAndPath] TO [sp_executeall]
GO
