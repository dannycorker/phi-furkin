SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 24-Mar-2009
-- Description:	Proc to Case Transfer Docs Over
-- JWG 2011-01-31 Cater for LeadDocumentFS 
-- JWG 2012-07-12 Still insert into LeadDocument and LeadDocumentFS, but read from vLeadDocumentList to cater for archived documents.
-- CS  2013-04-08 Added @MaintainWhenCreated to allow transferred documents to show the same date as their originals
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CaseTransferDocs]
(
	@CaseID INT,
	@OldEventTypeID INT,
	@NewEventTypeID INT,
	@NewClientPersonnelID INT,
	@LastOnly BIT = 0,
	@MaintainWhenCreated bit = 0
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NewCaseID INT,
			@NewClientID INT,
			@NewLeadID INT,
			@NewLeadDocumentID INT,
			@TestID INT = 0,
			@LeadEventID INT = 0,
			@OldClientID INT,
			@OldLeadDocumentID INT

	/* Get New Case ID Information */
	SELECT @NewCaseID = ctm.NewCaseID, 
	@NewLeadID = ctm.NewLeadID, 
	@NewClientID = ctm.NewClientID, 
	@OldClientID = ctm.ClientID 
	FROM dbo.CaseTransferMapping ctm WITH (NOLOCK) 
	WHERE ctm.CaseID = @CaseID

	/* Get the earliest LeadEvent id so we can get all of the docs */
	IF @LastOnly = 0
	BEGIN

		SELECT TOP 1 @LeadEventID = le.LeadEventID, 
		@OldLeadDocumentID = le.LeadDocumentID, 
		@NewLeadDocumentID = NULL
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.EventTypeID = @OldEventTypeID
		AND le.CaseID = @CaseID
		AND le.EventDeleted = 0
		ORDER BY le.LeadEventID

	END

	/* Get the latest LeadeventID so we only get the latest version of the doc */
	IF @LastOnly = 1
	BEGIN

		SELECT TOP 1 @LeadEventID = le.LeadEventID, 
		@OldLeadDocumentID = le.LeadDocumentID, 
		@NewLeadDocumentID = NULL
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.EventTypeID = @OldEventTypeID
		AND le.CaseID = @CaseID
		AND le.EventDeleted = 0
		ORDER BY le.LeadEventID DESC

	END

	WHILE (@LeadEventID > 0 AND @LeadEventID > @TestID)
	BEGIN
		IF @OldLeadDocumentID > 0
		BEGIN
			/* Transfer over Docs to a new lead document*/
			INSERT INTO [dbo].[LeadDocument]
			(
				/* JWG 2012-07-12 Need all columns in here again, except for the blobs and archive details */
				ClientID, 
				LeadID, 
				DocumentTypeID, 
				LeadDocumentTitle, 
				UploadDateTime, 
				WhoUploaded, 
				DocumentBLOB, 
				FileName, 
				EmailBLOB, 
				DocumentFormat, 
				EmailFrom, 
				EmailTo, 
				CcList, 
				BccList, 
				ElectronicSignatureDocumentKey, 
				Encoding, 
				ContentFormat, 
				ZipFormat, 
				DocumentBlobSize, 
				EmailBlobSize, 
				DocumentDatabaseID, 
				WhenArchived, 
				DocumentTypeVersionID		
			)
			SELECT 
			@NewClientID, 
			@NewLeadID, 
			ld.DocumentTypeID, 
			ld.LeadDocumentTitle, 
			ld.UploadDateTime, 
			ld.WhoUploaded, 
			CAST('' AS VARBINARY) AS DocumentBLOB, 
			ld.FileName, 
			NULL AS EmailBLOB, 
			ld.DocumentFormat, 
			ld.EmailFrom, 
			ld.EmailTo, 
			ld.CcList, 
			ld.BccList, 
			ld.ElectronicSignatureDocumentKey, 
			ld.Encoding, 
			ld.ContentFormat, 
			ld.ZipFormat, 
			ld.DocumentBlobSize, 
			ld.EmailBlobSize, 
			NULL AS DocumentDatabaseID, 
			NULL AS WhenArchived, 
			ld.DocumentTypeVersionID
			FROM dbo.LeadDocument ld WITH (NOLOCK) 
			WHERE ld.LeadDocumentID = @OldLeadDocumentID 
		
			SELECT @NewLeadDocumentID = SCOPE_IDENTITY()
			
			/* INSERT LeadDocumentFS */
			/* FROM LeadDocumentFS */

			SET IDENTITY_INSERT [dbo].[LeadDocumentFS] ON
			
			INSERT INTO [dbo].[LeadDocumentFS]([LeadDocumentID],[ClientID],[LeadID],[DocumentTypeID],[LeadDocumentTitle],[UploadDateTime],[WhoUploaded],[DocumentBLOB],[FileName],[EmailBLOB],[DocumentFormat],[EmailFrom],[EmailTo],[CcList],[BccList],[ElectronicSignatureDocumentKey], [Encoding], [ContentFormat], [ZipFormat]) 
			SELECT @NewLeadDocumentID, @NewClientID, @NewLeadID, ld.DocumentTypeID, ld.LeadDocumentTitle, ld.UploadDateTime, ld.WhoUploaded, ld.DocumentBLOB, ld.FileName, ld.EmailBLOB,ld.DocumentFormat,ld.EmailFrom,ld.EmailTo,ld.CcList,ld.BccList,ld.ElectronicSignatureDocumentKey, ld.Encoding, ld.ContentFormat, ld.ZipFormat 
			FROM dbo.vLeadDocumentList ld WITH (NOLOCK) 
			WHERE ld.LeadDocumentID = @OldLeadDocumentID 

			SET IDENTITY_INSERT [dbo].[LeadDocumentFS] OFF
			
		END

		/* Create a new LeadEvent for the transferred document */
		INSERT INTO [dbo].[LeadEvent]([ClientID],[LeadID],[WhenCreated],[WhoCreated],[Cost],[Comments],[EventTypeID],[NoteTypeID],[FollowupDateTime],[WhenFollowedUp],[AquariumEventType],[NextEventID],[CaseID],[LeadDocumentID],[NotePriority],[DocumentQueueID],[EventDeleted],[WhoDeleted],[DeletionComments],[ContactID],[BaseCost],[DisbursementCost],[DisbursementDescription],[ChargeOutRate],[UnitsOfEffort],[CostEnteredManually],[IsOnHold],[HoldLeadEventID])
		SELECT @NewClientID,@NewLeadID, CASE @MaintainWhenCreated WHEN 1 THEN LeadEvent.WhenCreated ELSE DATEADD(mi,1,dbo.fn_GetDate_Local()) END ,@NewClientPersonnelID,Cost,Comments,@NewEventTypeID,NoteTypeID,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local(),AquariumEventType,NULL,@NewCaseID,@NewLeadDocumentID,NotePriority,DocumentQueueID,EventDeleted,WhoDeleted,DeletionComments,ContactID,BaseCost,DisbursementCost,DisbursementDescription,ChargeOutRate,UnitsOfEffort,CostEnteredManually,IsOnHold,HoldLeadEventID
		FROM dbo.LeadEvent WITH (NOLOCK) 
		WHERE LeadEvent.LeadEventID = @LeadEventID

		SELECT @TestID = @LeadEventID

		SELECT TOP 1 @LeadEventID = le.LeadEventID, 
		@OldLeadDocumentID = le.LeadDocumentID, 
		@NewLeadDocumentID = NULL 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.EventTypeID = @OldEventTypeID
		AND le.CaseID = @CaseID
		AND le.EventDeleted = 0
		AND le.LeadEventID > @LeadEventID
		ORDER BY le.LeadEventID

	END
	
	/* 
		2011-08-16 ACE
		Trigger will not fire recursively so, update cases to set last event etc
		2012-06-21 JWG Set these on the NEW case, not the old one!
	*/
	UPDATE dbo.Cases 
	SET LatestLeadEventID = @LeadEventID, 
	LatestOutOfProcessLeadEventID = @LeadEventID,
	LatestNonNoteLeadEventID = @LeadEventID
	WHERE CaseID = @NewCaseID --@CaseID
	and @MaintainWhenCreated = 1 /*If we are maintaining the WhenCreated dates, we won't want to update this*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransferDocs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CaseTransferDocs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransferDocs] TO [sp_executeall]
GO
