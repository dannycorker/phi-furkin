SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get all the base titles
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_Title]
(
	@ClientId INT,
	@ClientPersonnelId INT
)
AS
BEGIN

	SELECT TitleId, Title [Text]  
	FROM Titles t WITH (NOLOCK)
	WHERE TitleID IN (1,2,3,4,6,14)
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_Title] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_Title] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_Title] TO [sp_executeall]
GO
