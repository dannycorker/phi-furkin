SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-08-12
-- Description:	TEST proc for claim calculation testing
-- =============================================
CREATE PROCEDURE [dbo].[__SB_Test_Deductible] 
(
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@Output XML = NULL OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @DeductRows TABLE
	(
		ID INT IDENTITY,
		Name VARCHAR(50),
		FromDate DATE,
		ToDate DATE,
		Deductible DECIMAL(18, 2),
		Balance DECIMAL(18, 2)
	)
	INSERT @DeductRows (Name, FromDate, ToDate, Deductible, Balance) VALUES
	('A & I More', '2012-08-06', '2013-08-05', 250.00, 250.00),
	('A & I Max', '2013-08-06', '2014-08-05', 150, 150.00)

	SELECT @Output = 
	( 
		SELECT *
		FROM @DeductRows
		FOR XML PATH ('Row'), ROOT ('Data')
	)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[__SB_Test_Deductible] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__SB_Test_Deductible] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__SB_Test_Deductible] TO [sp_executeall]
GO
