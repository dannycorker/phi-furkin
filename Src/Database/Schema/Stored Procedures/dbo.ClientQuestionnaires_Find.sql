SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientQuestionnaires table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaires_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientQuestionnaireID int   = null ,

	@ClientID int   = null ,

	@QuestionnaireTitle varchar (255)  = null ,

	@QuestionnaireDescription varchar (MAX)  = null ,

	@QuestionsPerPage int   = null ,

	@QuestionnaireLogo image   = null ,

	@QuestionnaireLogoFileName varchar (512)  = null ,

	@QuestionnaireFlowDiagram varchar (MAX)  = null ,

	@QuestionnaireIntroductionText varchar (MAX)  = null ,

	@HideIntro bit   = null ,

	@QuestionnaireFooterText varchar (MAX)  = null ,

	@HideFooter bit   = null ,

	@QuestionnaireAddressLookup bit   = null ,

	@Published bit   = null ,

	@CustomerInformationAtStart bit   = null ,

	@LinkedQuestionnaireClientQuestionnaireID int   = null ,

	@QuestionnaireFooterIframe varchar (MAX)  = null ,

	@QuestionnaireHeaderIframe varchar (MAX)  = null ,

	@QuestionnaireFooterInternal bit   = null ,

	@QuestionnaireHeaderInternal bit   = null ,

	@QuestionnaireFooterIframeHeight int   = null ,

	@QuestionnaireHeaderIframeHeight int   = null ,

	@QuestionnaireFooterIframeWidth int   = null ,

	@QuestionnaireHeaderIframeWidth int   = null ,

	@QuestionnaireBackGroundImage image   = null ,

	@QuestionnaireBackGroundImageFileName varchar (512)  = null ,

	@DefaultEmailAddress varchar (255)  = null ,

	@MailingListType int   = null ,

	@FrameMode bit   = null ,

	@LayoutCss varchar (MAX)  = null ,

	@LayoutCssFileName varchar (512)  = null ,

	@ImportDirectlyIntoLeadManager bit   = null ,

	@RunAsClientPersonnelID int   = null ,

	@RememberAnswers bit   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientQuestionnaireID]
	, [ClientID]
	, [QuestionnaireTitle]
	, [QuestionnaireDescription]
	, [QuestionsPerPage]
	, [QuestionnaireLogo]
	, [QuestionnaireLogoFileName]
	, [QuestionnaireFlowDiagram]
	, [QuestionnaireIntroductionText]
	, [HideIntro]
	, [QuestionnaireFooterText]
	, [HideFooter]
	, [QuestionnaireAddressLookup]
	, [Published]
	, [CustomerInformationAtStart]
	, [LinkedQuestionnaireClientQuestionnaireID]
	, [QuestionnaireFooterIframe]
	, [QuestionnaireHeaderIframe]
	, [QuestionnaireFooterInternal]
	, [QuestionnaireHeaderInternal]
	, [QuestionnaireFooterIframeHeight]
	, [QuestionnaireHeaderIframeHeight]
	, [QuestionnaireFooterIframeWidth]
	, [QuestionnaireHeaderIframeWidth]
	, [QuestionnaireBackGroundImage]
	, [QuestionnaireBackGroundImageFileName]
	, [DefaultEmailAddress]
	, [MailingListType]
	, [FrameMode]
	, [LayoutCss]
	, [LayoutCssFileName]
	, [ImportDirectlyIntoLeadManager]
	, [RunAsClientPersonnelID]
	, [RememberAnswers]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ClientQuestionnaires] WITH (NOLOCK) 
    WHERE 
	 ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([QuestionnaireTitle] = @QuestionnaireTitle OR @QuestionnaireTitle IS NULL)
	AND ([QuestionnaireDescription] = @QuestionnaireDescription OR @QuestionnaireDescription IS NULL)
	AND ([QuestionsPerPage] = @QuestionsPerPage OR @QuestionsPerPage IS NULL)
	AND ([QuestionnaireLogoFileName] = @QuestionnaireLogoFileName OR @QuestionnaireLogoFileName IS NULL)
	AND ([QuestionnaireFlowDiagram] = @QuestionnaireFlowDiagram OR @QuestionnaireFlowDiagram IS NULL)
	AND ([QuestionnaireIntroductionText] = @QuestionnaireIntroductionText OR @QuestionnaireIntroductionText IS NULL)
	AND ([HideIntro] = @HideIntro OR @HideIntro IS NULL)
	AND ([QuestionnaireFooterText] = @QuestionnaireFooterText OR @QuestionnaireFooterText IS NULL)
	AND ([HideFooter] = @HideFooter OR @HideFooter IS NULL)
	AND ([QuestionnaireAddressLookup] = @QuestionnaireAddressLookup OR @QuestionnaireAddressLookup IS NULL)
	AND ([Published] = @Published OR @Published IS NULL)
	AND ([CustomerInformationAtStart] = @CustomerInformationAtStart OR @CustomerInformationAtStart IS NULL)
	AND ([LinkedQuestionnaireClientQuestionnaireID] = @LinkedQuestionnaireClientQuestionnaireID OR @LinkedQuestionnaireClientQuestionnaireID IS NULL)
	AND ([QuestionnaireFooterIframe] = @QuestionnaireFooterIframe OR @QuestionnaireFooterIframe IS NULL)
	AND ([QuestionnaireHeaderIframe] = @QuestionnaireHeaderIframe OR @QuestionnaireHeaderIframe IS NULL)
	AND ([QuestionnaireFooterInternal] = @QuestionnaireFooterInternal OR @QuestionnaireFooterInternal IS NULL)
	AND ([QuestionnaireHeaderInternal] = @QuestionnaireHeaderInternal OR @QuestionnaireHeaderInternal IS NULL)
	AND ([QuestionnaireFooterIframeHeight] = @QuestionnaireFooterIframeHeight OR @QuestionnaireFooterIframeHeight IS NULL)
	AND ([QuestionnaireHeaderIframeHeight] = @QuestionnaireHeaderIframeHeight OR @QuestionnaireHeaderIframeHeight IS NULL)
	AND ([QuestionnaireFooterIframeWidth] = @QuestionnaireFooterIframeWidth OR @QuestionnaireFooterIframeWidth IS NULL)
	AND ([QuestionnaireHeaderIframeWidth] = @QuestionnaireHeaderIframeWidth OR @QuestionnaireHeaderIframeWidth IS NULL)
	AND ([QuestionnaireBackGroundImageFileName] = @QuestionnaireBackGroundImageFileName OR @QuestionnaireBackGroundImageFileName IS NULL)
	AND ([DefaultEmailAddress] = @DefaultEmailAddress OR @DefaultEmailAddress IS NULL)
	AND ([MailingListType] = @MailingListType OR @MailingListType IS NULL)
	AND ([FrameMode] = @FrameMode OR @FrameMode IS NULL)
	AND ([LayoutCss] = @LayoutCss OR @LayoutCss IS NULL)
	AND ([LayoutCssFileName] = @LayoutCssFileName OR @LayoutCssFileName IS NULL)
	AND ([ImportDirectlyIntoLeadManager] = @ImportDirectlyIntoLeadManager OR @ImportDirectlyIntoLeadManager IS NULL)
	AND ([RunAsClientPersonnelID] = @RunAsClientPersonnelID OR @RunAsClientPersonnelID IS NULL)
	AND ([RememberAnswers] = @RememberAnswers OR @RememberAnswers IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientQuestionnaireID]
	, [ClientID]
	, [QuestionnaireTitle]
	, [QuestionnaireDescription]
	, [QuestionsPerPage]
	, [QuestionnaireLogo]
	, [QuestionnaireLogoFileName]
	, [QuestionnaireFlowDiagram]
	, [QuestionnaireIntroductionText]
	, [HideIntro]
	, [QuestionnaireFooterText]
	, [HideFooter]
	, [QuestionnaireAddressLookup]
	, [Published]
	, [CustomerInformationAtStart]
	, [LinkedQuestionnaireClientQuestionnaireID]
	, [QuestionnaireFooterIframe]
	, [QuestionnaireHeaderIframe]
	, [QuestionnaireFooterInternal]
	, [QuestionnaireHeaderInternal]
	, [QuestionnaireFooterIframeHeight]
	, [QuestionnaireHeaderIframeHeight]
	, [QuestionnaireFooterIframeWidth]
	, [QuestionnaireHeaderIframeWidth]
	, [QuestionnaireBackGroundImage]
	, [QuestionnaireBackGroundImageFileName]
	, [DefaultEmailAddress]
	, [MailingListType]
	, [FrameMode]
	, [LayoutCss]
	, [LayoutCssFileName]
	, [ImportDirectlyIntoLeadManager]
	, [RunAsClientPersonnelID]
	, [RememberAnswers]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[ClientQuestionnaires] WITH (NOLOCK) 
    WHERE 
	 ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([QuestionnaireTitle] = @QuestionnaireTitle AND @QuestionnaireTitle is not null)
	OR ([QuestionnaireDescription] = @QuestionnaireDescription AND @QuestionnaireDescription is not null)
	OR ([QuestionsPerPage] = @QuestionsPerPage AND @QuestionsPerPage is not null)
	OR ([QuestionnaireLogoFileName] = @QuestionnaireLogoFileName AND @QuestionnaireLogoFileName is not null)
	OR ([QuestionnaireFlowDiagram] = @QuestionnaireFlowDiagram AND @QuestionnaireFlowDiagram is not null)
	OR ([QuestionnaireIntroductionText] = @QuestionnaireIntroductionText AND @QuestionnaireIntroductionText is not null)
	OR ([HideIntro] = @HideIntro AND @HideIntro is not null)
	OR ([QuestionnaireFooterText] = @QuestionnaireFooterText AND @QuestionnaireFooterText is not null)
	OR ([HideFooter] = @HideFooter AND @HideFooter is not null)
	OR ([QuestionnaireAddressLookup] = @QuestionnaireAddressLookup AND @QuestionnaireAddressLookup is not null)
	OR ([Published] = @Published AND @Published is not null)
	OR ([CustomerInformationAtStart] = @CustomerInformationAtStart AND @CustomerInformationAtStart is not null)
	OR ([LinkedQuestionnaireClientQuestionnaireID] = @LinkedQuestionnaireClientQuestionnaireID AND @LinkedQuestionnaireClientQuestionnaireID is not null)
	OR ([QuestionnaireFooterIframe] = @QuestionnaireFooterIframe AND @QuestionnaireFooterIframe is not null)
	OR ([QuestionnaireHeaderIframe] = @QuestionnaireHeaderIframe AND @QuestionnaireHeaderIframe is not null)
	OR ([QuestionnaireFooterInternal] = @QuestionnaireFooterInternal AND @QuestionnaireFooterInternal is not null)
	OR ([QuestionnaireHeaderInternal] = @QuestionnaireHeaderInternal AND @QuestionnaireHeaderInternal is not null)
	OR ([QuestionnaireFooterIframeHeight] = @QuestionnaireFooterIframeHeight AND @QuestionnaireFooterIframeHeight is not null)
	OR ([QuestionnaireHeaderIframeHeight] = @QuestionnaireHeaderIframeHeight AND @QuestionnaireHeaderIframeHeight is not null)
	OR ([QuestionnaireFooterIframeWidth] = @QuestionnaireFooterIframeWidth AND @QuestionnaireFooterIframeWidth is not null)
	OR ([QuestionnaireHeaderIframeWidth] = @QuestionnaireHeaderIframeWidth AND @QuestionnaireHeaderIframeWidth is not null)
	OR ([QuestionnaireBackGroundImageFileName] = @QuestionnaireBackGroundImageFileName AND @QuestionnaireBackGroundImageFileName is not null)
	OR ([DefaultEmailAddress] = @DefaultEmailAddress AND @DefaultEmailAddress is not null)
	OR ([MailingListType] = @MailingListType AND @MailingListType is not null)
	OR ([FrameMode] = @FrameMode AND @FrameMode is not null)
	OR ([LayoutCss] = @LayoutCss AND @LayoutCss is not null)
	OR ([LayoutCssFileName] = @LayoutCssFileName AND @LayoutCssFileName is not null)
	OR ([ImportDirectlyIntoLeadManager] = @ImportDirectlyIntoLeadManager AND @ImportDirectlyIntoLeadManager is not null)
	OR ([RunAsClientPersonnelID] = @RunAsClientPersonnelID AND @RunAsClientPersonnelID is not null)
	OR ([RememberAnswers] = @RememberAnswers AND @RememberAnswers is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Find] TO [sp_executeall]
GO
