SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset List
-- =============================================
CREATE PROCEDURE [dbo].[GetAssetList] 
	@ClientPersonnelID INT = NULL,
	@AssetTypeID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		a.[AssetID],
		a.[ClientID],
		a.[AssetTypeID],
		at.[AssetSubTypeID],
		a.[LocationID],
		a.[AssetName],
		a.[Note],
		a.[SecureNote],
		a.[SecureUserName],
		a.[SecurePassword],
		a.[SecureOther],
		a.[Version],
		a.[ValidFrom],
		a.[ValidTo],
		a.[ReminderDue],
		a.[ReminderTimeUnitID],
		a.[ReminderTimeUnitQuantity],
		a.[ReminderNotificationGroupID],
		a.[Enabled],
		a.[Deleted]
	FROM 
		[dbo].[Asset] a WITH (NOLOCK) 
		INNER JOIN [dbo].[AssetType] at WITH (NOLOCK) ON at.[AssetTypeID] = a.[AssetTypeID]
		INNER JOIN [dbo].[AssetSubType] ast WITH (NOLOCK) ON ast.[AssetSubTypeID] = at.[AssetSubTypeID]
		LEFT JOIN [dbo].[Location] al WITH (NOLOCK) ON al.[LocationID] = a.[LocationID]
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND (@AssetTypeID IS NULL OR @AssetTypeID = a.[AssetTypeID])
	ORDER BY a.[AssetTypeID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetAssetList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAssetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAssetList] TO [sp_executeall]
GO
