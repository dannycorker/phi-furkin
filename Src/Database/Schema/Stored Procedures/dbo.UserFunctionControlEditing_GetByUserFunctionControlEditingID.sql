SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserFunctionControlEditing table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing_GetByUserFunctionControlEditingID]
(

	@UserFunctionControlEditingID int   
)
AS


				SELECT
					[UserFunctionControlEditingID],
					[ClientPersonnelID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[UserFunctionControlEditing] WITH (NOLOCK) 
				WHERE
										[UserFunctionControlEditingID] = @UserFunctionControlEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_GetByUserFunctionControlEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing_GetByUserFunctionControlEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_GetByUserFunctionControlEditingID] TO [sp_executeall]
GO
