SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the MasterQuestions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_Delete]
(

	@MasterQuestionID int   
)
AS


				DELETE FROM [dbo].[MasterQuestions] WITH (ROWLOCK) 
				WHERE
					[MasterQuestionID] = @MasterQuestionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Delete] TO [sp_executeall]
GO
