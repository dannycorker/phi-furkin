SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01/03/2016
-- Description:	Inserts a script rule group
-- =============================================
CREATE PROCEDURE [dbo].[ScriptRuleGroup__Insert]
	
	@ScriptRuleID INT, 
	@ClientID INT, 
	@DetailFieldID INT, 
	@ComparatorID INT, 
	@Value VARCHAR(2000), 
	@OperatorID INT = NULL, 
	@GroupID INT, 
	@GroupOperatorID INT = NULL
	
AS
BEGIN
		
	SET NOCOUNT ON;

	INSERT INTO ScriptRuleGroup(ScriptRuleID, ClientID, DetailFieldID, ComparatorID, Value, OperatorID, GroupID, GroupOperatorID)
	VALUES (@ScriptRuleID, @ClientID, @DetailFieldID, @ComparatorID, @Value, @OperatorID, @GroupID, @GroupOperatorID)    
	
	SELECT SCOPE_IDENTITY() ScriptRuleGroupID	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRuleGroup__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptRuleGroup__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRuleGroup__Insert] TO [sp_executeall]
GO
