SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Paul Richardson>
-- Create date: <Create Date,,11 January 2010>
-- Description:	<Description,,Gets a list of detail fields that have 
--                            enabled linked field information for the given
--							  display page id>
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetByLinkedDetailFieldPageID] 
@ClientID int,
@LeadTypeID int,
@DisplayOnPageID int
AS
BEGIN

	SELECT df.*
	FROM dbo.fnDetailFieldsShared(@ClientID) df
	INNER JOIN dbo.LinkedDetailFields ON df.LinkedDetailFieldID = dbo.LinkedDetailFields.LinkedDetailFieldID
	WHERE (dbo.LinkedDetailFields.DisplayOnPageID = @DisplayOnPageID) 
	AND (dbo.LinkedDetailFields.Enabled = 1) 
	AND (df.Enabled = 1) 
	AND (df.LeadTypeID = @LeadTypeID)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByLinkedDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByLinkedDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByLinkedDetailFieldPageID] TO [sp_executeall]
GO
