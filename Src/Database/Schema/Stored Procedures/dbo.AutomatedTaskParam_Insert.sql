SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AutomatedTaskParam table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_Insert]
(

	@AutomatedTaskParamID int    OUTPUT,

	@TaskID int   ,

	@ClientID int   ,

	@ParamName varchar (50)  ,

	@ParamValue varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[AutomatedTaskParam]
					(
					[TaskID]
					,[ClientID]
					,[ParamName]
					,[ParamValue]
					)
				VALUES
					(
					@TaskID
					,@ClientID
					,@ParamName
					,@ParamValue
					)
				-- Get the identity value
				SET @AutomatedTaskParamID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Insert] TO [sp_executeall]
GO
