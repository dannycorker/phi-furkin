SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Chris Townsend
-- Purpose: Select records to be used on a custom search from the SqlQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery__GetForCustomSearchByClientID]
(
	@ClientID int   
)
AS

				SET ANSI_NULLS OFF
				
				SELECT
					*
				FROM
					[dbo].[SqlQuery] WITH (NOLOCK) 
				WHERE
					([ClientID] = @ClientID OR [ClientID] = 0)
					AND [FolderID] <> -1 
					AND COALESCE(ShowInCustomSearch, 0) = 1
					-- AND QueryID NOT IN (6513, 8004) /* JWG 2010-02-23 Don't let LA2Plus pod users run these! */
 				ORDER BY 
					[QueryTitle] asc

				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetForCustomSearchByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__GetForCustomSearchByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetForCustomSearchByClientID] TO [sp_executeall]
GO
