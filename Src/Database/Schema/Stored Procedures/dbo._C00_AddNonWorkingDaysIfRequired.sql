SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-05-23
-- Description:	SQL Afterevent for C00 For adding on non-working days to events
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AddNonWorkingDaysIfRequired]
	@LeadEventID int

AS
BEGIN
	SET NOCOUNT ON;

	Declare @myERROR int,
			@NoOfNonWorkingDays int


	select @NoOfNonWorkingDays=count(*)
	from leadevent
	inner join workingdays on Date between dbo.fn_GetDate_Local() and FollowUpDateTime and isworkday = 0
	where leadevent.leadeventid = @LeadEventID

	If (@NoOfNonWorkingDays) > 0
	Begin

		Update LeadEvent
		Set FollowUpdateTime = dateadd(dd,@NoOfNonWorkingDays, FollowUpdateTime)
		Where LeadEvent.LeadEventID = @LeadEventID

	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddNonWorkingDaysIfRequired] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AddNonWorkingDaysIfRequired] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddNonWorkingDaysIfRequired] TO [sp_executeall]
GO
