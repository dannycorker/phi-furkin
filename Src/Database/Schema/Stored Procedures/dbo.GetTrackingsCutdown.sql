SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetTrackings    Script Date: 15/05/2008 10:23:01 ******/
CREATE PROCEDURE [dbo].[GetTrackingsCutdown] 
	@ClientID int
AS

-- Get the most recent first by default
SELECT TOP 20 TrackingID, TrackingName,TrackingDescription,StartDate,EndDate,ClientQuestionnaireID,ClientID
FROM Tracking
WHERE ClientID = @ClientID
ORDER BY TrackingID DESC



GO
GRANT VIEW DEFINITION ON  [dbo].[GetTrackingsCutdown] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTrackingsCutdown] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTrackingsCutdown] TO [sp_executeall]
GO
