SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Dashboard table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_Insert]
(

	@DashboardID int    OUTPUT,

	@DashboardName nchar (10)  ,

	@DashboardDescription nchar (10)  ,

	@OwnerID nchar (10)  ,

	@DashboardLayoutID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[Dashboard]
					(
					[DashboardName]
					,[DashboardDescription]
					,[OwnerID]
					,[DashboardLayoutID]
					,[ClientID]
					)
				VALUES
					(
					@DashboardName
					,@DashboardDescription
					,@OwnerID
					,@DashboardLayoutID
					,@ClientID
					)
				-- Get the identity value
				SET @DashboardID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Insert] TO [sp_executeall]
GO
