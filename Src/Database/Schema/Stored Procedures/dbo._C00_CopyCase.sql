SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-07
-- Description:	Copy case and associated vales/history/table rows
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyCase]
	@CaseID INT,
	@PageMapToCopy dbo.tvpInt READONLY,
	@FieldMapToExclude dbo.tvpInt READONLY,
	@FieldToStampOriginalCaseID INT = NULL,
	@FieldToStampOriginalMatterID INT = NULL,
	@ClientPersonnelID INT,
	@ReturnMatterID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @NewCaseID INT,
			@LeadID INT,
			@CaseNum INT,
			@DetailFieldSubTypeID INT, /*AKA LeadOrMatter*/
			@MatterList VARCHAR(2000),
			@RefLetterNumberStart INT,
			@LogMessage VARCHAR(MAX),
			@LogCounter INT,
			@ClientID INT,
			@CDVsCopied INT
			
	DECLARE @AnyInt TABLE (MyInt INT, MyInt2 INT)
	
	DECLARE @TableRowIDs TABLE (OldTableRowID INT, NewTableRowID INT)
	
	DECLARE @LeadEvents TABLE (FromLeadEventID INT, ToLeadEventID INT)
	
	DECLARE @LeadEventCopyTo TABLE (
		ToInt INT IDENTITY(1, 1), 
		NewLeadEventID INT
	)
	
	DECLARE @LeadEventCopyFrom TABLE (
		FromInt INT IDENTITY(1, 1), 
		OldLeadEventID INT
	)
	
	/*
		1. Create New Case
		2. Create Mew Matter
		3. Copy LeadEvents
		4. Copy LETC
		5. Update Cases to have the correct xLeadEventIDs
		6. Copy Case DetailValues
		7. Copy MDV's
		8. Copy tablerows and associated detailvalues
		9. Copy detailvalue history
	*/
	
	/*We will need the Lead ID later on so grab it now*/
	SELECT @LeadID = c.LeadID, @ClientID = c.ClientID
	FROM Cases c WITH (NOLOCK)
	WHERE c.CaseID = @CaseID
	
	/*Get the max case num for incrementing on case insert*/
	SELECT TOP 1 @CaseNum = c.CaseNum 
	FROM Cases c WITH (NOLOCK) 
	WHERE c.LeadID = @LeadID 
	ORDER BY CaseID DESC 

	/*
		Add case output the case id into a temp table just incase maxdops is 
		incorrectly configured on any server this is deployed to.
	*/
	INSERT INTO Cases (LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
	OUTPUT inserted.CaseID INTO @AnyInt (MyInt)
	SELECT LeadID, ClientID, @CaseNum+1, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID
	FROM Cases c WITH (NOLOCK) 
	WHERE c.CaseID = @CaseID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' Case to LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'Cases', @LogMessage, @ClientPersonnelID
		
	/*As there is only one case get the case id from the table variable*/
	SELECT @NewCaseID = a.MyInt
	FROM @AnyInt a 

	/*Now remove the case ID from the table as we will re-use it later on*/
	DELETE FROM @AnyInt

	/*
		Get the count of matters for this lead. We cant use the value from 
		the number of cases as there may be more than one matter per case.
	*/
	SELECT @RefLetterNumberStart = COUNT(*)
	FROM Matter WITH (NOLOCK)
	WHERE LeadID = @LeadID

	/*
		Add Matter outputting the old and new Matter IDs into the table variable for use later on
		We need to be clever here and use row_number() and the matter count to correctly 
		reference any potential multiple matter scenarios.
	*/
	INSERT INTO Matter (ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	OUTPUT inserted.SourceID, inserted.MatterID INTO @AnyInt (MyInt, MyInt2)
	SELECT ClientID, MatterRef, CustomerID, LeadID, MatterStatus, dbo.fnRefLetterFromCaseNum(ROW_NUMBER() OVER (PARTITION BY (SELECT 1) ORDER BY m.MatterID)+@RefLetterNumberStart), BrandNew, @NewCaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, MatterID
	FROM Matter m WITH (NOLOCK)
	WHERE m.CaseID = @CaseID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' Matter(s) to LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'Matter', @LogMessage, @ClientPersonnelID
		
	/*
		Add LeadEvents
		First turn off SAE from firing.
	*/
	DECLARE @ContextInfo VARBINARY(100) = CAST('NoSAE' AS VARBINARY)
	SET CONTEXT_INFO @ContextInfo
	
	SELECT @LogMessage = 'Preparing to start LeadEvent Copy'
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'LeadEvent', @LogMessage, @ClientPersonnelID

	/*
		Insert the leadevents replacing the Case ID with the new Case ID
		Update the comments to add that this is a copied case.
	*/
	INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
	SELECT ClientID, LeadID, WhenCreated, WhoCreated, Cost, LEFT(Comments + CHAR(13) + CHAR(10) + 'LeadEvent copied from CaseID ' + CONVERT(VARCHAR(2000),@CaseID),2000), EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, @NewCaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID
	FROM LeadEvent le WITH (NOLOCK)
	WHERE le.CaseID = @CaseID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' LeadEvents to LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'LeadEvent', @LogMessage, @ClientPersonnelID

	/* Note the order of the new events */
	INSERT INTO @LeadEventCopyTo (NewLeadEventID)
	SELECT LeadEventID 
	FROM LeadEvent le  WITH (NOLOCK)
	WHERE CaseID = @NewCaseID
	ORDER BY le.LeadEventID ASC
	
	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' LeadEvents to @LeadEventCopyTo LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'LeadEvent', @LogMessage, @ClientPersonnelID

	/* Note the order of the old events */
	INSERT INTO @LeadEventCopyFrom (OldLeadEventID)
	SELECT LeadEventID 
	FROM LeadEvent le  WITH (NOLOCK)
	WHERE CaseID = @CaseID
	ORDER BY le.LeadEventID ASC
	
	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' LeadEvents to @LeadEventCopyFrom LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'LeadEvent', @LogMessage, @ClientPersonnelID

	/* Use the two table variables to match up the old and new events in order (1, 2, 3 etc) */
	UPDATE LeadEvent
	SET NextEventID = NewLeadEventID
	FROM LeadEvent
	INNER JOIN @LeadEventCopyFrom lecf ON lecf.OldLeadEventID = LeadEvent.NextEventID
	INNER JOIN @LeadEventCopyTo etct ON etct.ToInt = lecf.FromInt
	WHERE CaseID = @NewCaseID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Updated ' + CONVERT(VARCHAR,@LogCounter) + ' LeadEvents On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'LeadEvent', @LogMessage, @ClientPersonnelID

	/*
		Insert the leadevent thread completion records swapping out the old leadeventids for the new ones. 
		We have to left join on the lecff tables as it might be the last one in the process thread and therefore
		not be followed up. If we inner then we cant bring back the semi empty record.
	*/
	INSERT INTO LeadEventThreadCompletion (ClientID, LeadID, CaseID, FromLeadEventID, FromEventTypeID, ThreadNumberRequired, ToLeadEventID, ToEventTypeID)
	SELECT ClientID, LeadID, @NewCaseID, etct.NewLeadEventID, FromEventTypeID, ThreadNumberRequired, etctf.NewLeadEventID, ToEventTypeID
	FROM LeadEventThreadCompletion letc WITH (NOLOCK)
	LEFT JOIN @LeadEventCopyFrom lecff ON lecff.OldLeadEventID = letc.ToLeadEventID
	LEFT JOIN @LeadEventCopyTo etctf ON etctf.ToInt = lecff.FromInt
	INNER JOIN @LeadEventCopyFrom lecf ON lecf.OldLeadEventID = letc.FromEventTypeID
	INNER JOIN @LeadEventCopyTo etct ON etct.ToInt = lecf.FromInt
	WHERE letc.CaseID = @CaseID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' LeadEventThreadCompletion records On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'LeadEventTC', @LogMessage, @ClientPersonnelID

	/*Update the case to set the correct leadevents*/
	EXEC dbo.SetLatestCaseEvents @NewCaseID

	/*Insert the case detail values swapping out the CaseIDs where the field isnt in the list passed into the proc*/
	INSERT INTO CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue, ErrorMsg, EncryptedValue, SourceID)
	SELECT c.ClientID, @NewCaseID, c.DetailFieldID, DetailValue, ErrorMsg, EncryptedValue, c.SourceID
	FROM CaseDetailValues c WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = c.DetailFieldID
	INNER JOIN @PageMapToCopy p ON p.AnyID = df.DetailFieldPageID
	WHERE c.CaseID = @CaseID
	AND NOT EXISTS (
		SELECT *
		FROM @FieldMapToExclude f
		WHERE f.AnyID = df.DetailFieldID
	)

	SELECT @LogCounter = @@ROWCOUNT
	
	SELECT @CDVsCopied = @LogCounter

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' CaseDetailValues On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'CaseDetailValues', @LogMessage, @ClientPersonnelID

	/*
		Here we need to do the matter detail values swapping out the matterid(s) as we go.
		We also need to exclude any fields that are in the list to exclude.
	*/
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue, ErrorMsg, OriginalDetailValueID, OriginalLeadID, EncryptedValue, SourceID)
	SELECT mdv.ClientID, LeadID, a.MyInt2, mdv.DetailFieldID, DetailValue, ErrorMsg, OriginalDetailValueID, OriginalLeadID, EncryptedValue, mdv.SourceID
	FROM MatterDetailValues mdv WITH (NOLOCK)
	INNER JOIN @AnyInt a ON a.MyInt = mdv.MatterID
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = mdv.DetailFieldID
	INNER JOIN @PageMapToCopy p ON p.AnyID = df.DetailFieldPageID
	WHERE NOT EXISTS (
		SELECT *
		FROM @FieldMapToExclude f
		WHERE f.AnyID = df.DetailFieldID
	)

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' MatterDetailValues On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'MatterDetailValues', @LogMessage, @ClientPersonnelID

	/*Copy the tablerows for the case*/
	INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, SourceID)
	OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowIDs (OldTableRowID, NewTableRowID)
	SELECT ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, @NewCaseID, ClientPersonnelID, ContactID, TableRowID
	FROM TableRows t WITH (NOLOCK)
	WHERE t.CaseID = @CaseID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' TableRows (Case) On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'TableRows', @LogMessage, @ClientPersonnelID

	/*Copy the tablerows for the Matter*/
	INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, SourceID)
	OUTPUT inserted.SourceID, inserted.TableRowID INTO @TableRowIDs (OldTableRowID, NewTableRowID)
	SELECT ClientID, LeadID, a.MyInt2, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, TableRowID
	FROM TableRows t WITH (NOLOCK)
	INNER JOIN @AnyInt a ON a.MyInt = t.MatterID

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' TableRows (Matter) On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'TableRows', @LogMessage, @ClientPersonnelID

	/*
		Insert the TableDetailValues, inner join on the tablerows table variable to swap out the
		old table row ids for the new ones and left joing onto anyint to get the matter ids where
		required. We will also use this to set the case id correctly if the matter id is null..
	*/
	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, EncryptedValue, ErrorMsg, CustomerID, CaseID, ClientPersonnelID, ContactID)
	SELECT tr.NewTableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, a.MyInt2, ClientID, EncryptedValue, ErrorMsg, CustomerID, CASE WHEN a.MyInt2 IS NULL THEN @NewCaseID ELSE NULL END, ClientPersonnelID, ContactID
	FROM TableDetailValues t WITH (NOLOCK)
	INNER JOIN @TableRowIDs tr ON tr.OldTableRowID = t.TableRowID
	LEFT JOIN @AnyInt a ON a.MyInt = t.MatterID
	WHERE NOT EXISTS (
		SELECT *
		FROM @FieldMapToExclude f
		WHERE f.AnyID = t.DetailFieldID
	)

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' TableDetailValues (Matter) On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'TableDetailValues', @LogMessage, @ClientPersonnelID

	IF @CDVsCopied > 0
	BEGIN

		/*DetailValueHistory Case*/
		INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID, CaseID, ContactID, ClientPersonnelDetailValueID)
		SELECT dvh.ClientID, dvh.DetailFieldID, dvh.LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID, @NewCaseID, ContactID, ClientPersonnelDetailValueID
		FROM DetailValueHistory dvh WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dvh.DetailFieldID and df.LeadOrMatter = 11
		WHERE dvh.CaseID = @CaseID
		AND dvh.ClientID = @ClientID
		AND NOT EXISTS (
			SELECT *
			FROM @FieldMapToExclude f
			WHERE f.AnyID = dvh.DetailFieldID
		)

		SELECT @LogCounter = @@ROWCOUNT

		SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' DetailValueHistory (Case) On LeadID: ' + CONVERT(VARCHAR,@LeadID)
		
		EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'DetailValueHistory', @LogMessage, @ClientPersonnelID

	END

	/*DetailValueHistory Matter*/
	INSERT INTO DetailValueHistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID, CaseID, ContactID, ClientPersonnelDetailValueID)
	SELECT ClientID, DetailFieldID, LeadOrMatter, LeadID, a.MyInt2, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID, CaseID, ContactID, ClientPersonnelDetailValueID
	FROM DetailValueHistory dvh WITH (NOLOCK)
	INNER JOIN @AnyInt a ON a.MyInt = dvh.MatterID
	AND NOT EXISTS (
		SELECT *
		FROM @FieldMapToExclude f
		WHERE f.AnyID = dvh.DetailFieldID
	)

	SELECT @LogCounter = @@ROWCOUNT

	SELECT @LogMessage = 'Added ' + CONVERT(VARCHAR,@LogCounter) + ' DetailValueHistory (Matter) On LeadID: ' + CONVERT(VARCHAR,@LeadID)
	
	EXEC dbo._C00_LogIt 'SP', '_C00_CopyCase', 'DetailValueHistory', @LogMessage, @ClientPersonnelID

	IF @FieldToStampOriginalCaseID > 0
	BEGIN
	
		/*A field has been passed in so we need to figure out if it's case or matter then stamp the original id in.*/
		SELECT @DetailFieldSubTypeID = df.LeadOrMatter
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldID = @FieldToStampOriginalCaseID
		
		IF @DetailFieldSubTypeID = 2
		BEGIN
		
			/*Matter Field*/
			INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue, ErrorMsg, OriginalDetailValueID, OriginalLeadID, EncryptedValue, SourceID)
			SELECT ClientID, LeadID, MatterID, @FieldToStampOriginalCaseID, @CaseID, '', NULL, NULL, '', NULL
			FROM @AnyInt a
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = a.MyInt2
		
		END
		ELSE
		IF @DetailFieldSubTypeID = 11
		BEGIN
		
			/*Case level field*/
			EXEC dbo._C00_SimpleValueIntoField @FieldToStampOriginalCaseID, @CaseID, @NewCaseID, @ClientPersonnelID
		
		END
	
	END
	
	IF @FieldToStampOriginalMatterID > 0
	BEGIN
	
		/*Ok, so we have to stamp the original matter id */
		/*A field has been passed in so we need to figure out if it's case or matter then stamp the original id in.*/
		SELECT @DetailFieldSubTypeID = df.LeadOrMatter
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldID = @FieldToStampOriginalCaseID
		
		IF @DetailFieldSubTypeID = 2
		BEGIN

			/*So its a matter level field. We will put the original matter ID(s) in each of the new matter(s)*/
			INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue, ErrorMsg, OriginalDetailValueID, OriginalLeadID, EncryptedValue, SourceID)
			SELECT ClientID, LeadID, MatterID, @FieldToStampOriginalMatterID, a.MyInt, '', NULL, NULL, '', NULL
			FROM @AnyInt a
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = a.MyInt2

		END
		ELSE
		IF @DetailFieldSubTypeID = 11
		BEGIN

			/*Case level field but it wants the matters.. I think on this one we should add a list of matter IDs*/
			SELECT @MatterList = ''

			SELECT @MatterList += CONVERT(VARCHAR,a.MyInt) + ','
			FROM @AnyInt a

			SELECT @MatterList = LEFT(@MatterList,LEN(@MatterList)-1)

			EXEC dbo._C00_SimpleValueIntoField @FieldToStampOriginalMatterID, @CaseID, @MatterList, @ClientPersonnelID

		END

	END
	
	IF @ReturnMatterID = 1
	BEGIN

		SELECT a.MyInt2 AS [MatterID]
		FROM @AnyInt a
	
	END
	ELSE
	BEGIN
	
		RETURN @NewCaseID 
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyCase] TO [sp_executeall]
GO
