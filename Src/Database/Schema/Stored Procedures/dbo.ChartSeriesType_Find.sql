SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChartSeriesType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesType_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartSeriesTypeID int   = null ,

	@ChartSeriesTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartSeriesTypeID]
	, [ChartSeriesTypeName]
    FROM
	[dbo].[ChartSeriesType] WITH (NOLOCK) 
    WHERE 
	 ([ChartSeriesTypeID] = @ChartSeriesTypeID OR @ChartSeriesTypeID IS NULL)
	AND ([ChartSeriesTypeName] = @ChartSeriesTypeName OR @ChartSeriesTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartSeriesTypeID]
	, [ChartSeriesTypeName]
    FROM
	[dbo].[ChartSeriesType] WITH (NOLOCK) 
    WHERE 
	 ([ChartSeriesTypeID] = @ChartSeriesTypeID AND @ChartSeriesTypeID is not null)
	OR ([ChartSeriesTypeName] = @ChartSeriesTypeName AND @ChartSeriesTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Find] TO [sp_executeall]
GO
