SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the TimeUnits table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TimeUnits_Delete]
(

	@TimeUnitsID int   
)
AS


				DELETE FROM [dbo].[TimeUnits] WITH (ROWLOCK) 
				WHERE
					[TimeUnitsID] = @TimeUnitsID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimeUnits_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Delete] TO [sp_executeall]
GO
