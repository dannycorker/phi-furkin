SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the RPIFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldGroup_Get_List]

AS


				
				SELECT
					[RPIFieldGroupID],
					[GroupName]
				FROM
					[dbo].[RPIFieldGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Get_List] TO [sp_executeall]
GO
