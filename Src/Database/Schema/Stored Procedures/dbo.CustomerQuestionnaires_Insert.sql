SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerQuestionnaires_Insert]
(

	@CustomerQuestionnaireID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@SubmissionDate datetime   ,

	@TrackingID int   ,

	@Referrer varchar (512)  ,

	@SearchTerms varchar (512)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[CustomerQuestionnaires]
					(
					[ClientQuestionnaireID]
					,[CustomerID]
					,[SubmissionDate]
					,[TrackingID]
					,[Referrer]
					,[SearchTerms]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@CustomerID
					,@SubmissionDate
					,@TrackingID
					,@Referrer
					,@SearchTerms
					,@ClientID
					)
				-- Get the identity value
				SET @CustomerQuestionnaireID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerQuestionnaires_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Insert] TO [sp_executeall]
GO
