SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-29
-- Description:	Allows a quick update of the client personnel table correctly setting the context which this table enforces
-- =============================================


CREATE PROCEDURE [dbo].[ClientPersonnel_Update_ThirdPartySystemId]
(
	@ClientPersonnelID INT,
	@ThirdPartySystemId INT,
	@SubClientID INT = NULL
)
AS

BEGIN
			
	-- Updates the master record
	EXEC AquariusMaster.dbo.AQ_ClientPersonnel_Update_ThirdPartySystemId @ClientPersonnelID, @ThirdPartySystemId
	
	-- Set context info to allow access to table
	DECLARE @ContextInfo VARBINARY(100) = CAST('ClientPersonnel' AS VARBINARY)
	SET CONTEXT_INFO @ContextInfo
	
	UPDATE dbo.ClientPersonnel
	SET ThirdPartySystemId = @ThirdPartySystemId
	WHERE ClientPersonnelID = @ClientPersonnelID	
	
	IF @SubClientID IS NOT NULL
	BEGIN
	
		UPDATE dbo.ClientPersonnel
		SET SubClientID = @SubClientID
		WHERE ClientPersonnelID = @ClientPersonnelID	
	
	END
	
	-- Clear the things we set above
	SET CONTEXT_INFO 0x0	
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Update_ThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Update_ThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Update_ThirdPartySystemId] TO [sp_executeall]
GO
