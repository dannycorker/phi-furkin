SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Dave Morgan
-- Create date:	  01/11/2016	
-- Description:   Update the payment account details on unpaid items in a purchased product schedule
-- Mods
--	2016-11-23 DCM set @FromDate to payment date not Cover From date
--	2017-08-23 CPS updated call to fn_C00_CalculateActualCollectionDate
--  2017-09-22 JEL Set mandate cancel flag when 
--  2017-11-01 CPS call PurchasedProduct_Backup with @LeadEventID
--  2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
--	2021-02-12 ACE Removed redundant lead table lead lookup. Moved to matter.
-- =============================================
CREATE PROCEDURE [dbo].[ChangePaymentAccount_CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT
AS
BEGIN

--declare
--	@ClientID INT = 384,
--	@CaseID INT = 2438,
--	@LeadEventID INT = 27960

	SET NOCOUNT ON;
	  
	DECLARE @Success BIT
     
	DECLARE @CountryID INT
	  
	-- Get the third party fields
 	-- Gets the customer, lead, matter identities and the leadtype
	DECLARE @CustomerID INT, 
			@LeadID INT, 
			@MatterID INT, 
			@LeadTypeID INT, 
			@WhoCreated INT

	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID , @LeadID = m.LeadID 
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID=@CaseID
	
	SELECT @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID 
	FROM Lead l WITH (NOLOCK) 
	WHERE l.LeadID=@LeadID
	
	SELECT @WhoCreated = le.WhoCreated 
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID

	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	--4413	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN

		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	

		SELECT @AccountID=AccountID 
		FROM PurchasedProduct WITH (NOLOCK) 
		WHERE PurchasedProductID=@PurchasedProductID

	END	
					
	--4526 New account number
	DECLARE @NewAccountID_DV VARCHAR(2000), @IsInt_NewAccountID BIT, @NewAccountID INT
	SELECT @NewAccountID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4526)	
	SELECT @IsInt_NewAccountID = dbo.fnIsInt(@NewAccountID_DV)		
	IF(@IsInt_NewAccountID=1)
	BEGIN

		SELECT @NewAccountID = CAST(@NewAccountID_DV AS INT)

	END	
	
	--4523 Next payment date override
	DECLARE @OverrideDate_DV VARCHAR(2000), @IsDateTime_OverrideDate BIT, @OverrideDate DATETIME
	SELECT @OverrideDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4523)		
	SELECT @IsDateTime_OverrideDate = dbo.fnIsDateTime(@OverrideDate_DV)		
	IF(@IsDateTime_OverrideDate=1)
	BEGIN

		SELECT @OverrideDate = CAST(@OverrideDate_DV AS DATETIME)

	END	

    BEGIN TRY  
      
            DECLARE @ScheduleID INT = 1,
                        @TestID INT = 0,  
                        @FirstPaymentDate DATE,
                        @ValidFrom DATE, 
                        @ValidTo DATE,
                        @PreferredPaymentDay INT, 
                        @ProductCostGross NUMERIC(18,2),
                        @ProductCostVAT NUMERIC(18,2),
                        @PaymentFrequencyID INT,
                        @NumberOfInstallments INT = 12,
                        @RemainderUpFront BIT,
                        @Payment NUMERIC(36,18),
                        @RoundedPayment NUMERIC(18,2),
                        @RoundedCost NUMERIC(18,2),
                        @RemainderPayment NUMERIC(18,2),
                        @PaymentScheduleValidFrom DATE,
						@PaymentVAT NUMERIC(36,18),
                        @RoundedPaymentVAT NUMERIC(18,2),
                        @RoundedCostVAT NUMERIC(18,2),
                        @RemainderPaymentVAT NUMERIC(18,2),
                        @PurchasedProductPaymentScheduleID INT,
                        @CoverFromDay TINYINT,
                        @FromDate DATE,
                        @WaitType INT,
                        @WaitPeriod INT,
                        @MandateReadyDate DATE,
                        @OldMatterID INT,
						@AccountTypeID INT
                   

                       
            DECLARE @NewSchedule TABLE ( ScheduleID INT, 
										 PaymentDate DATE, 
										 ActualCollectionDate DATE, 
										 CoverFrom DATE, 
										 CoverTo DATE, 
										 PaymentNet NUMERIC(18,2), 
										 PaymentVAT NUMERIC(18,2), 
										 PaymentGross NUMERIC(18,2),
										 PaymentStatusID INT,
										 [Type] INT
										) 

            -- performs backup of payment schedules if required
			EXEC dbo.PurchasedProductHistory__Backup @PurchasedProductID, @WhoCreated, @LeadEventID
			EXEC PurchasedProductPaymentScheduleHistory__Backup @PurchasedProductID, @WhoCreated, 0  -- don't delete the current purchased product schedule, we're just going to update the uncollected rows
			EXEC CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 0          
			
			UPDATE PurchasedProduct 
			SET AccountID=@NewAccountID,
				WhenModified=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) 
			WHERE PurchasedProductID = @PurchasedProductID			                             
			
            INSERT INTO @NewSchedule (ScheduleID,PaymentDate,ActualCollectionDate,CoverFrom,CoverTo,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,[Type])
            SELECT ppps.PurchasedProductPaymentScheduleID,ppps.PaymentDate,ppps.ActualCollectionDate,ppps.CoverFrom,ppps.CoverTo,ppps.PaymentNet,ppps.PaymentVAT,ppps.PaymentGross,ppps.PaymentStatusID,ppps.PurchasedProductPaymentScheduleTypeID
            FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
            WHERE ppps.PurchasedProductID=@PurchasedProductID
            AND ppps.CustomerLedgerID IS NULL

            /* 
                  Set the actual collection date 
                  1. For card payments on new schedules, the actual collection day = payment day
                  2. For direct debit payments on new schedules, add mandate wait to the first payment & weekend/bank holiday wait to all 
            
            */ 
            
            SELECT @AccountTypeID=a.AccountTypeID
            FROM Account a WITH (NOLOCK)
            WHERE a.AccountID=@NewAccountID

			-- update earliest date if required ( override set, during mandate wait or in the past )
			SELECT TOP 1 @FirstPaymentDate=ns.PaymentDate, @PurchasedProductPaymentScheduleID=ns.ScheduleID, @FromDate=PaymentDate 
			FROM @NewSchedule ns 
			ORDER BY ns.PaymentDate
			
			-- Override date is king unless it falls within mandate wait
			IF @OverrideDate IS NOT NULL
			BEGIN
			
				SELECT @FirstPaymentDate = @OverrideDate
				
			END 
			
			/*GPR 2020-03-02 for AAG-202*/
			SELECT @CountryID = cl.CountryID 
			FROM Clients cl WITH (NOLOCK)
			WHERE cl.ClientID = @ClientID
	
			IF @CountryID <> 14 /*Australia*/
			BEGIN	

				IF @AccountTypeID = 1
				BEGIN			

					SELECT TOP 1 @MandateReadyDate = am.FirstAcceptablePaymentDate 
					FROM AccountMandate am WITH (NOLOCK) 
					WHERE am.AccountID = @NewAccountID 
					AND am.MandateStatusID IN (1,2,3) 
					ORDER BY AccountMandateID DESC
				
					IF @MandateReadyDate IS NULL
					BEGIN
				
						SELECT @MandateReadyDate=dbo.fn_C00_CalculateActualCollectionDate(@NewAccountID,@AccountTypeID,dbo.fn_GetDate_Local(),4, 0.00)
				
					END
			
				END
			END
			
			IF @FirstPaymentDate < @MandateReadyDate
			BEGIN
			
				SELECT @FirstPaymentDate=@MandateReadyDate

			END

			-- if there is no override then we need to be sure the standard business wait period is applied
			SELECT @WaitPeriod=MandateWait 
			FROM dbo.BillingConfiguration b WITH (NOLOCK)
			WHERE b.ClientID = @ClientID

			IF @FirstPaymentDate < dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),@WaitPeriod) 
			AND @OverrideDate IS NULL
			BEGIN
			
				SELECT @FirstPaymentDate = dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),@WaitPeriod)
			
			END

			IF @AccountTypeID = 1
			BEGIN

				UPDATE @NewSchedule 
				SET PaymentDate = @FirstPaymentDate, ActualCollectionDate = dbo.fnAddWorkingDays(@FirstPaymentDate,3) 
				WHERE ScheduleID = @PurchasedProductPaymentScheduleID
			
				UPDATE ns
				SET PaymentDate = dbo.fn_C00_CalculateActualCollectionDate(@NewAccountID,@AccountTypeID,ActualCollectionDate,5, ns.PaymentGross)
				FROM @NewSchedule ns 
				WHERE ns.ScheduleID <> @PurchasedProductPaymentScheduleID

			END
			ELSE
			BEGIN

				UPDATE ns
				SET PaymentDate = dbo.fn_C00_CalculateActualCollectionDate(@NewAccountID,@AccountTypeID,ActualCollectionDate,5, ns.PaymentGross)
				FROM @NewSchedule ns 

			END


			--update PPPS
                   
			UPDATE ppps
			SET PaymentDate=n.PaymentDate,
				ActualCollectionDate=n.ActualCollectionDate,
				AccountID=@NewAccountID
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
			INNER JOIN @NewSchedule n ON n.ScheduleID=ppps.PurchasedProductPaymentScheduleID
			
			--rebuild customer payment schedule 
			EXEC dbo.Billing__RebuildCustomerPaymentSchedule @NewAccountID, @FromDate, @WhoCreated     
            
            -- if the old account no longer has any scheduled payments to collect make it inactive
            IF NOT EXISTS ( 
				SELECT * 
				FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
				WHERE cps.CustomerLedgerID IS NULL 
				AND cps.AccountID=@AccountID 
			)
            BEGIN

				UPDATE Account 
				SET Active=0 
				WHERE AccountID = @AccountID

            END

            SET @Success = 1
      
      END TRY  
      BEGIN CATCH  
      
            UPDATE PurchasedProduct
            SET PaymentScheduleSuccessfullyCreated=0, PaymentScheduleCreatedOn=NULL
            WHERE PurchasedProductID=@PurchasedProductID
            
            SET @Success = 0
            
      END CATCH  
      
      IF @Success = 1 
      BEGIN
      
		/*Need to mark the old account record as inactive and trigger the mandate cancel*/
		SELECT @OldMatterID = m.MatterID FROM Matter m WITH ( NOLOCK ) 
		INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID 
		where mdv.DetailFieldID = 176973 /*Account ID on collection lead type Matter*/
		and mdv.DetailValue = @AccountID 
		and m.CustomerID = @CustomerID 
		
		/*Set flag to send cancellation if all collections for case have cleared*/
		EXEC _C00_SimpleValueIntoField 176895,5144,@OldMatterID,@WhoCreated
		
      END 
      
      SELECT @Success Success
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangePaymentAccount_CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangePaymentAccount_CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangePaymentAccount_CreateFromThirdPartyFields] TO [sp_executeall]
GO
