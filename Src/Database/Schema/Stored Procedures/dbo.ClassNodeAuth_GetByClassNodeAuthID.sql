SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClassNodeAuth table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_GetByClassNodeAuthID]
(

	@ClassNodeAuthID int   
)
AS


				SELECT
					[ClassNodeAuthID],
					[ClientID],
					[ClassNodeID],
					[ClientPersonnelAdminGroupID],
					[RightsLevel],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Deleted]
				FROM
					[dbo].[ClassNodeAuth] WITH (NOLOCK) 
				WHERE
										[ClassNodeAuthID] = @ClassNodeAuthID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_GetByClassNodeAuthID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_GetByClassNodeAuthID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_GetByClassNodeAuthID] TO [sp_executeall]
GO
