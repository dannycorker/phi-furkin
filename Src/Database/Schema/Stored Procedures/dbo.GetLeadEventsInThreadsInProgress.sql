SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-07
-- Description:	Get all event threads in progress for a case
-- =============================================
CREATE PROCEDURE [dbo].[GetLeadEventsInThreadsInProgress] 
	
	@CaseID int 

AS
BEGIN
	SET NOCOUNT ON;

	-- Relevant list of in-process events.
	-- Excludes redundant events from expired threads
	select le.leadeventid, et.eventtypeid, et.eventtypename 
	from cases c 
	inner join leadevent le on c.CaseID = le.CaseID 
	inner join eventtype et on le.EventTypeID = et.EventTypeID 
	inner join EventChoice ec on ec.EventTypeID = et.EventTypeID 
	inner join EventType net on net.EventTypeID = ec.NextEventTypeID 
	WHERE le.whenfollowedup IS NULL
	AND le.EventDeleted = 0 
	AND et.InProcess = 1 
	AND et.Enabled = 1 
	AND net.Enabled = 1
	AND c.caseid = @caseid

END















GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadEventsInThreadsInProgress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadEventsInThreadsInProgress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadEventsInThreadsInProgress] TO [sp_executeall]
GO
