SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeEquation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeEquation_Delete]
(

	@EventTypeEquationID int   
)
AS


				DELETE FROM [dbo].[EventTypeEquation] WITH (ROWLOCK) 
				WHERE
					[EventTypeEquationID] = @EventTypeEquationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Delete] TO [sp_executeall]
GO
