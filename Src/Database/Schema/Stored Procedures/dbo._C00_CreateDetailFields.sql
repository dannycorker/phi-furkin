SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create DetailFields for Tables Lead and Matter
--              JWG 2009-07-28 Added checks for the right LeadType, and Enabled fields.
--              DM  2011-08-23 Fixed TableDetailValues for Lead, checking for dftr.LeadOrMatter=1
--				DM  2014-06-16 Fixed TableDetailValues for Matter, removing LeadID filter
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateDetailFields]
(
	@DFString varchar(max),
	@LeadID int,
	@DFStringIsThirdPartyFieldID bit = 0 -- added dcm 10/4/14
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int, 
		CustomerID int
	);
	
	IF @DFStringIsThirdPartyFieldID = 1 -- added dcm 10/4/14
	BEGIN
	
		DECLARE @NewDFString VARCHAR(MAX)
	
		SELECT @NewDFString=COALESCE(@NewDFString + ',', '') + CONVERT(VARCHAR(30),tpfm.DetailFieldID) 
		FROM fnTableOfIDsFromCSV(@DFString) f 
		INNER JOIN ThirdPartyFieldMapping tpfm WITH (NOLOCK) ON f.AnyID=tpfm.ThirdPartyFieldID
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID=tpfm.LeadTypeID AND l.LeadID=@LeadID
		
		SELECT @DFString=@NewDFString
	
	END

	INSERT INTO @LeadsAndMatters (LeadID, MatterID, CustomerID)
	SELECT DISTINCT m.LeadID, m.MatterID  , m.CustomerID
	FROM dbo.Matter m (nolock) 
	WHERE m.LeadID = @LeadID

	Declare	@myERROR int,
			@ClientID int,
			@LeadTypeID int 

	Select	@ClientID=Lead.ClientID, 
			@LeadTypeID = Lead.LeadTypeID
	From dbo.Lead (nolock) 
	Where Lead.LeadID = @LeadID


	/* Create all necessary LeadDetailValues records that don't already exist */
	INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
	SELECT Distinct @ClientID,@LeadID,f.anyid,'' 
	FROM fnTableOfIDsFromCSV(@DFString) f 
	Inner join detailfields df WITH (NOLOCK) on df.detailfieldid = f.anyid AND df.LeadOrMatter = 1 AND df.LeadTypeID = @LeadTypeID --AND df.Enabled = 1
	WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WITH (NOLOCK) WHERE ldv.LeadID = @LeadID AND ldv.DetailFieldID = f.anyid) 
	
	INSERT INTO CaseDetailValues (ClientID,CaseID,DetailFieldID,DetailValue)
	SELECT Distinct @ClientID,m.CaseID,f.anyid,'' 
	FROM @LeadsAndMatters lam 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = lam.MatterID
	Cross join detailfields df WITH (NOLOCK) 
	Inner join fnTableOfIDsFromCSV(@DFString) f on f.anyid = df.detailfieldid
	WHERE NOT EXISTS (SELECT * FROM CaseDetailValues cdv WITH (NOLOCK) WHERE cdv.CaseID = m.CaseID AND cdv.DetailFieldID = f.anyid) 
	AND df.LeadOrMatter = 11 AND df.LeadTypeID = @LeadTypeID --AND df.Enabled = 1

	/* Create all necessary MatterDetailValues records that don't already exist */
	INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
	SELECT Distinct @ClientID,lam.LeadID,lam.MatterID,f.anyid,'' 
	FROM @LeadsAndMatters lam 
	Cross join detailfields df WITH (NOLOCK) 
	Inner join fnTableOfIDsFromCSV(@DFString) f on f.anyid = df.detailfieldid
	WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WITH (NOLOCK) WHERE mdv.MatterID = lam.MatterID AND mdv.DetailFieldID = f.anyid) 
	AND df.LeadOrMatter = 2 AND df.LeadTypeID = @LeadTypeID --AND df.Enabled = 1
	
	INSERT INTO CustomerDetailValues(ClientID, CustomerID, DetailFieldID, DetailValue)
	SELECT Distinct @ClientID, lam.CustomerID, f.AnyID, ''
	FROM @LeadsAndMatters lam 
	Cross join detailfields df WITH (NOLOCK) 
	Inner join fnTableOfIDsFromCSV(@DFString) f on f.anyid = df.detailfieldid
	WHERE NOT EXISTS (SELECT * FROM CustomerDetailValues cdv WITH (NOLOCK) WHERE cdv.CustomerID = lam.CustomerID AND cdv.DetailFieldID = f.anyid) 
	AND df.LeadOrMatter = 10 --AND df.Enabled = 1
	
	/* Create all necessary TableDetailValues records that don't already exist for Matter*/
	INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,LeadID,MatterID,ClientID,EncryptedValue)
	SELECT Distinct tr.TableRowID, '',f.anyid, '', lam.LeadID,lam.MatterID, @ClientID, ''
	FROM TableRows tr WITH (NOLOCK) 
	Cross join detailfields df WITH (NOLOCK) 
	Inner join fnTableOfIDsFromCSV(@DFString) f on f.anyid = df.detailfieldid
	Inner Join @LeadsAndMatters lam on lam.MatterID = tr.MatterID
	Inner Join DetailFieldPages dfp WITH (NOLOCK) on dfp.DetailFieldPageID = df.DetailFieldPageID -- AND dfp.LeadTypeID = @LeadTypeID
	WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.MatterID = lam.MatterID AND tdv.DetailFieldID = f.anyid and tdv.TableRowID = tr.TableRowID) 
	AND df.LeadOrMatter IN (6,8) --AND df.Enabled = 1
	--and TR.LeadID = @LeadID -- dcm 16/06/2014


	/* Create all necessary TableDetailValues records that don't already exist for Lead*/
	INSERT INTO TableDetailValues (TableRowID,ResourceListID,DetailFieldID,DetailValue,LeadID,MatterID,ClientID,EncryptedValue)
	SELECT Distinct tr.TableRowID, '',f.anyid, '', @LeadID,NULL, @ClientID, ''
	FROM TableRows tr WITH (NOLOCK) 
	Cross join detailfields df WITH (NOLOCK) 
	INNER JOIN DetailFields dftr WITH (NOLOCK) ON tr.DetailFieldID=dftr.DetailFieldID and dftr.LeadOrMatter=1
	Inner join fnTableOfIDsFromCSV(@DFString) f on f.anyid = df.detailfieldid
	Inner Join DetailFieldPages dfp WITH (NOLOCK) on dfp.DetailFieldPageID = df.DetailFieldPageID --AND dfp.LeadTypeID = @LeadTypeID
	WHERE NOT EXISTS (SELECT * FROM TableDetailValues tdv WITH (NOLOCK) WHERE tdv.LeadID = @LeadID and (tdv.MatterID is NULL or tdv.MatterID = 0) AND tdv.DetailFieldID = f.anyid and tdv.TableRowID = tr.TableRowID) 
	AND df.LeadOrMatter IN (6,8) --AND df.Enabled = 1
	and TR.LeadID = @LeadID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDetailFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateDetailFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDetailFields] TO [sp_executeall]
GO
