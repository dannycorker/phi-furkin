SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the RPIFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_Get_List]

AS


				
				SELECT
					[RPIFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[RPIFieldID],
					[DetailFieldID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ColumnDetailFieldID]
				FROM
					[dbo].[RPIFieldMapping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_Get_List] TO [sp_executeall]
GO
