SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderSectionLocaterKeyword table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderSectionLocaterKeywordID]
(

	@DataLoaderSectionLocaterKeywordID int   
)
AS


				SELECT
					[DataLoaderSectionLocaterKeywordID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderMapSectionID],
					[Keyword],
					[DataLoaderKeywordMatchTypeID],
					[KeywordNotes],
					[RelativeRow],
					[AbsoluteCol],
					[IsForSectionStart]
				FROM
					[dbo].[DataLoaderSectionLocaterKeyword] WITH (NOLOCK) 
				WHERE
										[DataLoaderSectionLocaterKeywordID] = @DataLoaderSectionLocaterKeywordID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderSectionLocaterKeywordID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderSectionLocaterKeywordID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderSectionLocaterKeywordID] TO [sp_executeall]
GO
