SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-10-19
-- Description:	Get Resource Data By ResourceListID
-- =============================================
CREATE PROCEDURE [dbo].[rldv]
	@ResourceListID int = null, 
	@DetailFieldID int = null,
	@DetailFieldPageID int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ClientID int
	
	IF @DetailFieldPageID is not null
	BEGIN
		EXEC dbo.ResourceListDetailValues__GetPivotedResourceListByDetailFieldPageID @DetailFieldPageID
	END
	ELSE
	IF @DetailFieldID IS NULL
	BEGIN
		SELECT top 1 @DetailFieldPageID = df.DetailFieldPageID, @ClientID = rldv.ClientID
		FROM ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = rldv.DetailFieldID
		WHERE ResourceListID = @ResourceListID
		
		EXEC dbo.ResourceListDetailValues__GetPivotedResourceListByDetailFieldPageID @DetailFieldPageID
	END
	ELSE
	BEGIN
		SELECT *
		FROM ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = rldv.DetailFieldID
		WHERE rldv.DetailFieldID = @DetailFieldID
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[rldv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[rldv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[rldv] TO [sp_executeall]
GO
