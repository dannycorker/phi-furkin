SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Mark Law
-- Create date: 04-Feb-2009
-- Description:	Proc to Copy Partner Details from Matter/Lead to Partner Customer Details
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyPartnerDetails]
(
	@LeadEventID int,
	@DetailFieldPageID int,
	@TitleFieldID int,
	@FirstNameFieldID int,
	@MiddleNameFieldID int,
	@LastNameFieldID int,
	@EmailFieldID int,
	@DayTimeTelFieldID int,
	@HomeTelFieldID int,
	@MobileTelFieldID int,
	@Address1FieldID int,
	@Address2FieldID int,
	@TownFieldID int,
	@CountyFieldID int,
	@PostCodeFieldID int,
	@OccupationFieldID int,
	@EmployerFieldID int,
	@DOBFieldID int,
	@SameAddressID int=NULL
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare	@myERROR int,
		@LeadID int,
		@EventTypeID int,
		@MatterID int,
		@CustomerID int,
		@ClientID int,
		@CaseID int,
		@LeadOrMatter int,
		@TitleValue varchar(200),
		@FirstNameValue varchar(100),
		@MiddleNameValue varchar(100),
		@LastNameValue varchar(100),
		@DOBValue datetime,
		@Address1Value varchar(200),
		@Address2Value varchar(200),
		@TownValue varchar(200),
		@CountyValue varchar(200),
		@EmailValue varchar(255),
		@HomeTelValue varchar(50),
		@DayTimeTelValue varchar(50),
		@MobileTelValue varchar(50),
		@OccupationValue varchar(100),
		@EmployerValue varchar(100),
		@PostCodeValue varchar(10),
		@SameAddress varchar(10),
		@PartnerID int

	Select Top 1
	@LeadID = Lead.LeadID,
	@EventTypeID = LeadEvent.EventTypeID,
	@CustomerID = Customers.CustomerID,
	@MatterID = Matter.MatterID,
	@ClientID = Matter.ClientID,
	@CaseID = LeadEvent.CaseID
	from customers with (nolock) 
	inner join lead with (nolock) on lead.customerid = customers.customerid
	inner join matter with (nolock) on matter.leadid = lead.leadid
	inner join leadevent with (nolock) on lead.leadid = leadevent.leadid
	inner join ClientPersonnel with (nolock) on ClientPersonnel.ClientPersonnelID = LeadEvent.WhoCreated
	where leadevent.leadeventid = @LeadEventID 

	select @LeadOrMatter = LeadOrMatter
	from detailfieldpages with (nolock) 
	where clientid = @ClientID
	and detailfieldpageid = @DetailFieldPageID	
	
	if @LeadOrMatter = 1
	begin

		select @TitleValue = t.titleid
		from lookuplistitems lli with (nolock) 
		left join leaddetailvalues ldv with (nolock) on ldv.detailfieldid = @TitleFieldID
		left join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		left join titles t with (nolock) on t.title = lli.itemvalue COLLATE Latin1_General_CI_AS
		where ldv.leadid = @LeadID
		and lli.LookupListItemID = (select ldv.detailvalue from leaddetailvalues ldv with (nolock) left join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID where ldv.clientid = @ClientID and ldv.detailfieldid = @TitleFieldID and ldv.leadid = @LeadID)
		
		select @FirstNameValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @FirstNameFieldID

		select @MiddleNameValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @MiddleNameFieldID

		select @LastNameValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @LastNameFieldID

		select @DOBValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @DOBFieldID

		select @EmailValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @EmailFieldID

		select @HomeTelValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @HomeTelFieldID

		select @DayTimeTelValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @DayTimeTelFieldID

		select @MobileTelValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @MobileTelFieldID

		select @OccupationValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @OccupationFieldID

		select @EmployerValue = ldv.DetailValue
		from leaddetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @EmployerFieldID

		if @SameAddressID is not null
		begin
			select @SameAddress = ldv.DetailValue
			from leaddetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @SameAddressID
		end
			
		/* If Same is true then grab from customer details and use this to populate partner details */
		if @SameAddress = 'True'
		begin
			select @Address1Value = Address1, @Address2Value = Address2, @TownValue = Town, @CountyValue = County, @PostCodeValue = PostCode
			from customers with (nolock) 
			where customerid = @CustomerID
		end
		else
		begin
			select @Address1Value = ldv.DetailValue
			from leaddetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @Address1FieldID

			select @Address2Value = ldv.DetailValue
			from leaddetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @Address2FieldID

			select @TownValue = ldv.DetailValue
			from leaddetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @TownFieldID

			select @CountyValue = ldv.DetailValue
			from leaddetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @CountyFieldID
			
			select @PostCodeValue = ldv.DetailValue
			from leaddetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @PostCodeFieldID

			Select @SameAddress = 0
		end
	end
	
	if @LeadOrMatter = 2
	begin
		select @TitleValue = t.titleid
		from lookuplistitems lli with (nolock) 
		left join matterdetailvalues ldv with (nolock) on ldv.detailfieldid = @TitleFieldID
		left join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		left join titles t with (nolock) on t.title = lli.itemvalue COLLATE Latin1_General_CI_AS
		where ldv.leadid = @LeadID
		and lli.LookupListItemID = (select ldv.detailvalue from matterdetailvalues ldv left join Cases c On c.LeadID = ldv.LeadID and c.CaseID = @CaseID where ldv.clientid = @ClientID and ldv.detailfieldid = @TitleFieldID and ldv.leadid = @LeadID and ldv.matterid = @MatterID)
		
		select @FirstNameValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @FirstNameFieldID
		and ldv.matterid = @MatterID

		select @MiddleNameValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @MiddleNameFieldID
		and ldv.matterid = @MatterID

		select @LastNameValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @LastNameFieldID
		and ldv.matterid = @MatterID

		select @DOBValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @DOBFieldID
		and ldv.matterid = @MatterID

		select @EmailValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @EmailFieldID
		and ldv.matterid = @MatterID

		select @HomeTelValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @HomeTelFieldID
		and ldv.matterid = @MatterID

		select @DayTimeTelValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @DayTimeTelFieldID
		and ldv.matterid = @MatterID

		select @MobileTelValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @MobileTelFieldID
		and ldv.matterid = @MatterID

		select @OccupationValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @OccupationFieldID
		and ldv.matterid = @MatterID

		select @EmployerValue = ldv.DetailValue
		from matterdetailvalues ldv with (nolock) 
		inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		where ldv.clientid = @ClientID
		and ldv.leadid = @LeadID
		and ldv.detailfieldid = @EmployerFieldID
		and ldv.matterid = @MatterID

		if @SameAddressID is not null
		begin
			select @SameAddress = ldv.DetailValue
			from matterdetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @SameAddressID
			and ldv.matterid = @MatterID
		end
			
		/* If Same is true then grab from customer details and use this to populate partner details */
		if @SameAddress = 'True'
		begin
			select @Address1Value = Address1, @Address2Value = Address2, @TownValue = Town, @CountyValue = County, @PostCodeValue = PostCode
			from customers with (nolock) 
			where customerid = @CustomerID
		end
		else
		begin
			select @Address1Value = ldv.DetailValue
			from matterdetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @Address1FieldID
			and ldv.matterid = @MatterID

			select @Address2Value = ldv.DetailValue
			from matterdetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @Address2FieldID
			and ldv.matterid = @MatterID

			select @TownValue = ldv.DetailValue
			from matterdetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @TownFieldID
			and ldv.matterid = @MatterID

			select @CountyValue = ldv.DetailValue
			from matterdetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @CountyFieldID
			and ldv.matterid = @MatterID

			select @PostCodeValue = ldv.DetailValue
			from matterdetailvalues ldv with (nolock) 
			inner join Cases c with (nolock) On c.LeadID = ldv.LeadID and c.CaseID = @CaseID
			where ldv.clientid = @ClientID
			and ldv.leadid = @LeadID
			and ldv.detailfieldid = @PostCodeFieldID
			and ldv.matterid = @MatterID

			Select @SameAddress = 0
		end
	end
	
	if @TitleValue = ''
	begin
		set @TitleValue = 0
	end
	
	select top 1 @PartnerID = PartnerID from dbo.Partner with (nolock) where customerid = @CustomerID	

	If (@FirstNameValue <> '' And @LastNameValue <> '' And @FirstNameValue IS NOT NULL And @LastNameValue IS NOT NULL)
	Begin
		if @PartnerID is null
		begin
			insert into [dbo].[Partner]([CustomerID],[ClientID],[UseCustomerAddress],[TitleID],[FirstName],[MiddleName],[LastName],[EmailAddress],[DayTimeTelephoneNumber],[HomeTelephone],[MobileTelephone],[Address1],[Address2],[Town],[County],[PostCode],[Occupation],[Employer],[DateOfBirth])
			values (@CustomerID,@ClientID,@SameAddress,@TitleValue,@FirstNameValue,@MiddleNameValue,@LastNameValue,@EmailValue,@DayTimeTelValue,@HomeTelValue,@MobileTelValue,@Address1Value,@Address2Value,@TownValue,@CountyValue,@PostCodeValue,@OccupationValue,@EmployerValue,@DOBValue)
		end
		else
		begin
			update dbo.Partner
			set
				CustomerID = @CustomerID,
				ClientID = @ClientID,
				UseCustomerAddress = @SameAddress,
				TitleID = @TitleValue,
				FirstName = @FirstNameValue,
				MiddleName = @MiddleNameValue,
				LastName = @LastNameValue,
				EmailAddress = @EmailValue,
				DayTimeTelephoneNumber = @DayTimeTelValue,
				HomeTelephone = @HomeTelValue,
				MobileTelephone = @MobileTelValue,
				Address1 = @Address1Value,
				Address2 = @Address2Value,
				Town = @TownValue,
				County = @CountyValue,
				PostCode = @PostCodeValue,
				Occupation = @OccupationValue,
				Employer = @EmployerValue,
				DateOfBirth = @DOBValue
			Where CustomerID = @CustomerID
			
		end
	End
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyPartnerDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyPartnerDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyPartnerDetails] TO [sp_executeall]
GO
