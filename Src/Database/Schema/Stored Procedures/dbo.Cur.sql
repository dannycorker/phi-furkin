SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-12
-- Description:	Convert an amount from any currency to any currency.
-- =============================================
CREATE PROCEDURE [dbo].[Cur] 
	@CurrencyID int = null,
	@CurrencyCode char(3) = null,			/* Leave this empty to show all currencies */   
	@ConversionDate datetime = null,		/* Leave this empty to get the latest rates */   
	@UseNextAvailableIfNotFound bit = 1,	/* If user asks for data before 2011, show them the data from January 2011 be default */
	@SortOrder tinyint = 1
AS
BEGIN
	SET NOCOUNT ON;

	SELECT c.* 
	FROM dbo.Currency c WITH (NOLOCK) 
	WHERE (@CurrencyID IS NULL OR c.CurrencyID = @CurrencyID)
	AND (@CurrencyCode IS NULL OR c.CurrencyCode = @CurrencyCode) 
	ORDER BY CASE @SortOrder WHEN 1 THEN c.CurrencyID ELSE c.CurrencyCode END ASC
	
	SELECT cr.* 
	FROM dbo.CurrencyRate cr WITH (NOLOCK) 
	WHERE (@CurrencyID IS NULL OR cr.CurrencyID = @CurrencyID)
	AND (@CurrencyCode IS NULL OR cr.CurrencyCode = @CurrencyCode) 
	AND cr.IsLatest = 1 
	ORDER BY CASE @SortOrder WHEN 1 THEN cr.CurrencyID ELSE cr.CurrencyCode END ASC
	
	/* Always show Euros and Dollars */
	SELECT cr.* 
	FROM dbo.CurrencyRate cr WITH (NOLOCK) 
	WHERE cr.CurrencyID IN (46, 151) 
	AND cr.IsLatest = 1 
	ORDER BY cr.CurrencyID ASC
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Cur] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cur] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cur] TO [sp_executeall]
GO
