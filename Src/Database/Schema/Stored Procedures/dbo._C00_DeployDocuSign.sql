SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-01-18
-- Description:	Proc To enable docusign for a client. This will insert into the relevant tables and create a batch job to check for valid events. 
-- EventTypes and Documents will need to be setup in the front end. Once the below has been run the screens will become available under DOCUMENTS in the lead manager
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DeployDocuSign] 

        @UserName VARCHAR(100), 
		@Password VARCHAR (100),
		@IntergrationKey VARCHAR(200),
		@AccountKey VARCHAR (200),
		@ClientID INT,
		@WhoCreated INT,
		@Email VARCHAR(50)
AS 
BEGIN

	SET NOCOUNT ON;

	DECLARE @DSPTypeID INT,
			@DSPAccountID INT,
			@DSPUserID INT,
			@GenericReportFolderID INT,
			@Now DATETIME = dbo.fn_GetDate_Local(),
			@QueryID INT,
			@BatchID INT,
			@ParamName VARCHAR(50),
			@ParamValue VARCHAR (50)
			
	SELECT @DSPTypeID = d.DSPTypeID FROM DocumentSignProviderType d WITH (NOLOCK) where d.DSPTypeName = 'DocuSign'

	IF ISNULL(@DSPTypeID,0) = 0 
	BEGIN 
		INSERT INTO DocumentSignProviderType (DSPTypeID, DSPTypeName,DSPSignatureTemplate)
		VALUES (1,'DocuSign','<<Signature{0}>>')
		SELECT @DSPTypeID =  1
	END 

	SELECT @DSPAccountID = da.DSPAccountID FROM DocumentSignProviderAccount da WITH (NOLOCK) Where da.ClientID = @ClientID

	IF ISNULL(@DSPAccountID,0) = 0 
	BEGIN 
		INSERT INTO DocumentSignProviderAccount (DSPTypeID,ClientID,IntegrationKey,AccountKey)
		VALUES (@DSPTypeID,@ClientID,@IntergrationKey,@AccountKey)
		SELECT @DSPAccountID =  SCOPE_IDENTITY() 
	END

	SELECT @DSPUserID = du.DSPUserID FROM DocumentSignProviderUser du WITH (NOLOCK) where du.ClientID = @ClientID and du.DSPTypeID = @DSPAccountID and du.DSPTypeID = @DSPTypeID

	IF ISNULL(@DSPUserID,0) = 0 
	BEGIN
		INSERT INTO DocumentSignProviderUser (DSPTypeID,DSPAccountID,ClientID,UserName,Password)
		VALUES (@DSPTypeID,@DSPAccountID,@ClientID,@UserName,@Password)
		SELECT @DSPUserID = SCOPE_IDENTITY()
	END 
			
	SELECT @GenericReportFolderID = f.folderID
	FROM Folder f WITH (NOLOCK) 
	where f.ClientID = @ClientID
	and f.FolderName = 'Automation Reports'
		
	IF ISNULL(@GenericReportFolderID,0) = 0 
	BEGIN
		
		INSERT INTO Folder (ClientID,FolderTypeID,FolderName,FolderDescription,WhenCreated,WhoCreated,Personal)
		VALUES (@ClientID,1,'Automation Reports','Automation Reports',@Now,@WhoCreated,0)
		SELECT @GenericReportFolderID = SCOPE_IDENTITY()
	
	END
				
	INSERT INTO SqlQuery (ClientID,QueryText,QueryTitle,OnlineLimit,BatchLimit,SqlQueryTypeID,FolderID,IsEditable,WhenCreated,CreatedBy,OwnedBy,Comments,WhenModified,ModifiedBy,LeadTypeID,OutputFormat)
	VALUES (@ClientID,'/*Intentionally empty*/','DocuSign Automation',100000,100000,3,@GenericReportFolderID,0,@Now,@WhoCreated,@WhoCreated,'DocuSign Automation',@Now,@WhoCreated,NULL,'CSV')
	SELECT @QueryID = SCOPE_IDENTITY()
	
	EXECUTE AutomatedTask_Insert     @BatchID OUTPUT,@ClientID,'DocuSign Automation','DocuSign Automation',1,9,0,4,1,@Now,0,NULL,NULL,NULL,@WhoCreated,@Now,@WhoCreated,@Now,100,1

	SELECT @ParamName = 'SEND_RESULTS' ,@ParamValue ='No'
	EXECUTE AutomatedTaskParam_Insert   NULL  ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue
	     
	SELECT @ParamName = 'ALERT_ON_ERROR_EMAIL' ,@ParamValue = @Email
	EXECUTE AutomatedTaskParam_Insert   NULL ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue
	      
	SELECT @ParamName = 'RUN_AS_USERID' ,@ParamValue = @WhoCreated
	EXECUTE AutomatedTaskParam_Insert   NULL ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue   
	
	SELECT @ParamName = 'SQL_QUERY' ,@ParamValue = CAST(@QueryID as VARCHAR)
	EXECUTE AutomatedTaskParam_Insert  NULL ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue          
	      
	SELECT @ParamName = 'FILE_PREFIX' ,@ParamValue ='DocuSign'
	EXECUTE AutomatedTaskParam_Insert  NULL  ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue
	
	SELECT @ParamName = 'SEND_TO_WEBSERVICE' ,@ParamValue ='True'
	EXECUTE AutomatedTaskParam_Insert  NULL  ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue
	
	SELECT @ParamName = 'WEBSERVICE_NAME' ,@ParamValue ='DocuSign'
	EXECUTE AutomatedTaskParam_Insert  NULL  ,@BatchID  ,@ClientID  ,@ParamName  ,@ParamValue

	PRINT  'BATCHID = ' + CAST(@BatchID as VARCHAR(50)) + 'QueryID = '  + CAST(@QueryID as VARCHAR(50)) + 'DSPTypeID =' + CAST(@DSPTypeID as VARCHAR(50)) 
		   + 'DSPAccountID =' + CAST(@DSPAccountID as VARCHAR(50)) + 'DSPTUserID =' + CAST(@DSPUserID as VARCHAR(50))

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeployDocuSign] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DeployDocuSign] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeployDocuSign] TO [sp_executeall]
GO
