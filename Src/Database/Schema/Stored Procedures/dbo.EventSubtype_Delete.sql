SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_Delete]
(

	@EventSubtypeID int   
)
AS


				DELETE FROM [dbo].[EventSubtype] WITH (ROWLOCK) 
				WHERE
					[EventSubtypeID] = @EventSubtypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Delete] TO [sp_executeall]
GO
