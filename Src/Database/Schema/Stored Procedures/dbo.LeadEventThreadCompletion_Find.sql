SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadEventThreadCompletion table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadEventThreadCompletionID int   = null ,

	@ClientID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@FromLeadEventID int   = null ,

	@FromEventTypeID int   = null ,

	@ThreadNumberRequired int   = null ,

	@ToLeadEventID int   = null ,

	@ToEventTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadEventThreadCompletionID]
	, [ClientID]
	, [LeadID]
	, [CaseID]
	, [FromLeadEventID]
	, [FromEventTypeID]
	, [ThreadNumberRequired]
	, [ToLeadEventID]
	, [ToEventTypeID]
    FROM
	[dbo].[LeadEventThreadCompletion] WITH (NOLOCK) 
    WHERE 
	 ([LeadEventThreadCompletionID] = @LeadEventThreadCompletionID OR @LeadEventThreadCompletionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([FromLeadEventID] = @FromLeadEventID OR @FromLeadEventID IS NULL)
	AND ([FromEventTypeID] = @FromEventTypeID OR @FromEventTypeID IS NULL)
	AND ([ThreadNumberRequired] = @ThreadNumberRequired OR @ThreadNumberRequired IS NULL)
	AND ([ToLeadEventID] = @ToLeadEventID OR @ToLeadEventID IS NULL)
	AND ([ToEventTypeID] = @ToEventTypeID OR @ToEventTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadEventThreadCompletionID]
	, [ClientID]
	, [LeadID]
	, [CaseID]
	, [FromLeadEventID]
	, [FromEventTypeID]
	, [ThreadNumberRequired]
	, [ToLeadEventID]
	, [ToEventTypeID]
    FROM
	[dbo].[LeadEventThreadCompletion] WITH (NOLOCK) 
    WHERE 
	 ([LeadEventThreadCompletionID] = @LeadEventThreadCompletionID AND @LeadEventThreadCompletionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([FromLeadEventID] = @FromLeadEventID AND @FromLeadEventID is not null)
	OR ([FromEventTypeID] = @FromEventTypeID AND @FromEventTypeID is not null)
	OR ([ThreadNumberRequired] = @ThreadNumberRequired AND @ThreadNumberRequired is not null)
	OR ([ToLeadEventID] = @ToLeadEventID AND @ToLeadEventID is not null)
	OR ([ToEventTypeID] = @ToEventTypeID AND @ToEventTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Find] TO [sp_executeall]
GO
