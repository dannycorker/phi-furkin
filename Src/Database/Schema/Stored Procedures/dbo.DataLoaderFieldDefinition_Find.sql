SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderFieldDefinition table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderFieldDefinitionID int   = null ,

	@ClientID int   = null ,

	@DataLoaderMapID int   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@DataLoaderMapSectionID int   = null ,

	@DataLoaderObjectFieldID int   = null ,

	@DetailFieldID int   = null ,

	@DetailFieldAlias varchar (500)  = null ,

	@NamedValue varchar (250)  = null ,

	@Keyword varchar (250)  = null ,

	@DataLoaderKeywordMatchTypeID int   = null ,

	@RowRelativeToKeyword int   = null ,

	@ColRelativeToKeyword int   = null ,

	@SectionRelativeRow int   = null ,

	@SectionAbsoluteCol int   = null ,

	@ValidationRegex varchar (250)  = null ,

	@Equation varchar (2000)  = null ,

	@IsMatchField bit   = null ,

	@DecodeTypeID int   = null ,

	@DefaultLookupItemID int   = null ,

	@SourceDataLoaderFieldDefinitionID int   = null ,

	@Notes varchar (2000)  = null ,

	@AllowErrors bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderFieldDefinitionID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderObjectTypeID]
	, [DataLoaderMapSectionID]
	, [DataLoaderObjectFieldID]
	, [DetailFieldID]
	, [DetailFieldAlias]
	, [NamedValue]
	, [Keyword]
	, [DataLoaderKeywordMatchTypeID]
	, [RowRelativeToKeyword]
	, [ColRelativeToKeyword]
	, [SectionRelativeRow]
	, [SectionAbsoluteCol]
	, [ValidationRegex]
	, [Equation]
	, [IsMatchField]
	, [DecodeTypeID]
	, [DefaultLookupItemID]
	, [SourceDataLoaderFieldDefinitionID]
	, [Notes]
	, [AllowErrors]
    FROM
	[dbo].[DataLoaderFieldDefinition] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID OR @DataLoaderFieldDefinitionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DataLoaderMapID] = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	AND ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([DataLoaderMapSectionID] = @DataLoaderMapSectionID OR @DataLoaderMapSectionID IS NULL)
	AND ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID OR @DataLoaderObjectFieldID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([DetailFieldAlias] = @DetailFieldAlias OR @DetailFieldAlias IS NULL)
	AND ([NamedValue] = @NamedValue OR @NamedValue IS NULL)
	AND ([Keyword] = @Keyword OR @Keyword IS NULL)
	AND ([DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID OR @DataLoaderKeywordMatchTypeID IS NULL)
	AND ([RowRelativeToKeyword] = @RowRelativeToKeyword OR @RowRelativeToKeyword IS NULL)
	AND ([ColRelativeToKeyword] = @ColRelativeToKeyword OR @ColRelativeToKeyword IS NULL)
	AND ([SectionRelativeRow] = @SectionRelativeRow OR @SectionRelativeRow IS NULL)
	AND ([SectionAbsoluteCol] = @SectionAbsoluteCol OR @SectionAbsoluteCol IS NULL)
	AND ([ValidationRegex] = @ValidationRegex OR @ValidationRegex IS NULL)
	AND ([Equation] = @Equation OR @Equation IS NULL)
	AND ([IsMatchField] = @IsMatchField OR @IsMatchField IS NULL)
	AND ([DecodeTypeID] = @DecodeTypeID OR @DecodeTypeID IS NULL)
	AND ([DefaultLookupItemID] = @DefaultLookupItemID OR @DefaultLookupItemID IS NULL)
	AND ([SourceDataLoaderFieldDefinitionID] = @SourceDataLoaderFieldDefinitionID OR @SourceDataLoaderFieldDefinitionID IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([AllowErrors] = @AllowErrors OR @AllowErrors IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderFieldDefinitionID]
	, [ClientID]
	, [DataLoaderMapID]
	, [DataLoaderObjectTypeID]
	, [DataLoaderMapSectionID]
	, [DataLoaderObjectFieldID]
	, [DetailFieldID]
	, [DetailFieldAlias]
	, [NamedValue]
	, [Keyword]
	, [DataLoaderKeywordMatchTypeID]
	, [RowRelativeToKeyword]
	, [ColRelativeToKeyword]
	, [SectionRelativeRow]
	, [SectionAbsoluteCol]
	, [ValidationRegex]
	, [Equation]
	, [IsMatchField]
	, [DecodeTypeID]
	, [DefaultLookupItemID]
	, [SourceDataLoaderFieldDefinitionID]
	, [Notes]
	, [AllowErrors]
    FROM
	[dbo].[DataLoaderFieldDefinition] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID AND @DataLoaderFieldDefinitionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DataLoaderMapID] = @DataLoaderMapID AND @DataLoaderMapID is not null)
	OR ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([DataLoaderMapSectionID] = @DataLoaderMapSectionID AND @DataLoaderMapSectionID is not null)
	OR ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID AND @DataLoaderObjectFieldID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([DetailFieldAlias] = @DetailFieldAlias AND @DetailFieldAlias is not null)
	OR ([NamedValue] = @NamedValue AND @NamedValue is not null)
	OR ([Keyword] = @Keyword AND @Keyword is not null)
	OR ([DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID AND @DataLoaderKeywordMatchTypeID is not null)
	OR ([RowRelativeToKeyword] = @RowRelativeToKeyword AND @RowRelativeToKeyword is not null)
	OR ([ColRelativeToKeyword] = @ColRelativeToKeyword AND @ColRelativeToKeyword is not null)
	OR ([SectionRelativeRow] = @SectionRelativeRow AND @SectionRelativeRow is not null)
	OR ([SectionAbsoluteCol] = @SectionAbsoluteCol AND @SectionAbsoluteCol is not null)
	OR ([ValidationRegex] = @ValidationRegex AND @ValidationRegex is not null)
	OR ([Equation] = @Equation AND @Equation is not null)
	OR ([IsMatchField] = @IsMatchField AND @IsMatchField is not null)
	OR ([DecodeTypeID] = @DecodeTypeID AND @DecodeTypeID is not null)
	OR ([DefaultLookupItemID] = @DefaultLookupItemID AND @DefaultLookupItemID is not null)
	OR ([SourceDataLoaderFieldDefinitionID] = @SourceDataLoaderFieldDefinitionID AND @SourceDataLoaderFieldDefinitionID is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([AllowErrors] = @AllowErrors AND @AllowErrors is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Find] TO [sp_executeall]
GO
