SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Tracking table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Tracking_Update]
(

	@TrackingID int   ,

	@TrackingName varchar (100)  ,

	@TrackingDescription varchar (255)  ,

	@StartDate datetime   ,

	@EndDate datetime   ,

	@ClientQuestionnaireID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Tracking]
				SET
					[TrackingName] = @TrackingName
					,[TrackingDescription] = @TrackingDescription
					,[StartDate] = @StartDate
					,[EndDate] = @EndDate
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[ClientID] = @ClientID
				WHERE
[TrackingID] = @TrackingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Tracking_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Update] TO [sp_executeall]
GO
