SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-04-02
-- Description:	Get any missing mandatory fields like Customers.LastName for this event
-- =============================================
CREATE PROCEDURE [dbo].[GetMissingMandatoryEventTypeStandardFields] 
	@EventTypeID INT, 
	@CustomerID INT, 
	@LeadID INT, 
	@CaseID INT, 
	@Debug BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PartnerID INT, 
	@Sql VARCHAR(MAX)
	
	SELECT TOP (1) @PartnerID = p.PartnerID 
	FROM dbo.Partner p WITH (NOLOCK) 
	WHERE p.CustomerID = @CustomerID 
	
	/*
		Don't let the absence of a partner null out the dynamic sql below. 
		If this is a partner kind of event, then no data will be found for those fields with PartnerID = -1 
		so any mandatory fields will successfully prevent the event from being applied.
	*/
	SELECT @PartnerID = ISNULL(@PartnerID, -1)
	
	/*
		The EventTypeStandardField table holds values like these:
		
				EventTypeStandardFieldID	EventTypeID	DataLoaderObjectFieldID	ClientID	Mandatory
				835							156417		92						3			0
				836							156417		93						3			1
				837							156417		95						3			1		
		
		This first block of Sql joins DataLoaderObjectField to DataLoaderObjectType to decode the real tables and columns involved:
		
				Partner.TitleID optional
				Partner.FirstName
				Partner.LastName
		
		If any fields are marked as helper fields, then do send them back for display;
		If all the fields are marked as mandatory fields, then only send back any blank ones.
	*/
	SELECT @Sql = '
	SELECT t.ObjectTypeName ObjectName, f.FieldName, esf.* 
	FROM dbo.EventTypeStandardField esf WITH (NOLOCK) 
	INNER JOIN dbo.DataLoaderObjectField f WITH (NOLOCK) ON f.DataLoaderObjectFieldID = esf.DataLoaderObjectFieldID
	INNER JOIN dbo.DataLoaderObjectType t WITH (NOLOCK) ON t.DataLoaderObjectTypeID = f.DataLoaderObjectTypeID
	WHERE esf.EventTypeID = ' + CAST(@EventTypeID AS VARCHAR) 
	
	/* Now check for any that are marked as mandatory for this event, but are still blank */
	SELECT @Sql += ' 
	AND ((esf.Mandatory = 0) OR (esf.Mandatory = 1 AND NOT EXISTS(SELECT * FROM dbo.' + CASE f.DataLoaderObjectTypeID 
	WHEN 1 THEN 'Customers x WITH (NOLOCK) WHERE x.CustomerID = ' + CAST(@CustomerID AS VARCHAR) 
	WHEN 2 THEN 'Lead x WITH (NOLOCK) WHERE x.LeadID = ' + CAST(@LeadID AS VARCHAR) 
	WHEN 3 THEN 'Cases x WITH (NOLOCK) WHERE x.CaseID = ' + CAST(@CaseID AS VARCHAR) 
	WHEN 4 THEN 'Matter x WITH (NOLOCK) WHERE x.CaseID = ' + CAST(@CaseID AS VARCHAR) 
	WHEN 14 THEN 'Partner x WITH (NOLOCK) WHERE x.PartnerID = ' + CAST(@PartnerID AS VARCHAR) 
	ELSE t.ObjectTypeName + ' x WITH (NOLOCK) WHERE 1=2 ' END + ' AND x.' + f.FieldName + CASE WHEN f.FieldName = 'Title' THEN 'ID' ELSE '' END + CASE WHEN f.DataTypeID BETWEEN 12 AND 16 THEN ' > '''' ' ELSE ' IS NOT NULL ' END + '))) ' 
	FROM EventTypeStandardField esf WITH (NOLOCK) 
	INNER JOIN dbo.DataLoaderObjectField f WITH (NOLOCK) ON f.DataLoaderObjectFieldID = esf.DataLoaderObjectFieldID 
	INNER JOIN dbo.DataLoaderObjectType t WITH (NOLOCK) ON t.DataLoaderObjectTypeID = f.DataLoaderObjectTypeID
	WHERE esf.EventTypeID = @EventTypeID
	
	IF @Debug = 1
	BEGIN
		PRINT @Sql
	END
	
	/* Send the list back to the caller (eg StandardCustomer.ascx) */
	EXEC (@Sql)
	
END 

GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryEventTypeStandardFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMissingMandatoryEventTypeStandardFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryEventTypeStandardFields] TO [sp_executeall]
GO
