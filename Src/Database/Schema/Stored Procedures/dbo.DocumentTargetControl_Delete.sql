SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_Delete]
(

	@DocumentTargetControlID int   
)
AS


				DELETE FROM [dbo].[DocumentTargetControl] WITH (ROWLOCK) 
				WHERE
					[DocumentTargetControlID] = @DocumentTargetControlID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Delete] TO [sp_executeall]
GO
