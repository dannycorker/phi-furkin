SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CustomerInformationFieldDefinitions table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomerInformationFieldDefinitionID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@FieldAtBeginningOfQuestionnaire bit   = null ,

	@OrdinalPosition int   = null ,

	@FieldName varchar (255)  = null ,

	@FieldLabel varchar (900)  = null ,

	@Mandatory bit   = null ,

	@VerifyPhoneNumber bit   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomerInformationFieldDefinitionID]
	, [ClientQuestionnaireID]
	, [FieldAtBeginningOfQuestionnaire]
	, [OrdinalPosition]
	, [FieldName]
	, [FieldLabel]
	, [Mandatory]
	, [VerifyPhoneNumber]
	, [ClientID]
    FROM
	[dbo].[CustomerInformationFieldDefinitions] WITH (NOLOCK) 
    WHERE 
	 ([CustomerInformationFieldDefinitionID] = @CustomerInformationFieldDefinitionID OR @CustomerInformationFieldDefinitionID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([FieldAtBeginningOfQuestionnaire] = @FieldAtBeginningOfQuestionnaire OR @FieldAtBeginningOfQuestionnaire IS NULL)
	AND ([OrdinalPosition] = @OrdinalPosition OR @OrdinalPosition IS NULL)
	AND ([FieldName] = @FieldName OR @FieldName IS NULL)
	AND ([FieldLabel] = @FieldLabel OR @FieldLabel IS NULL)
	AND ([Mandatory] = @Mandatory OR @Mandatory IS NULL)
	AND ([VerifyPhoneNumber] = @VerifyPhoneNumber OR @VerifyPhoneNumber IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomerInformationFieldDefinitionID]
	, [ClientQuestionnaireID]
	, [FieldAtBeginningOfQuestionnaire]
	, [OrdinalPosition]
	, [FieldName]
	, [FieldLabel]
	, [Mandatory]
	, [VerifyPhoneNumber]
	, [ClientID]
    FROM
	[dbo].[CustomerInformationFieldDefinitions] WITH (NOLOCK) 
    WHERE 
	 ([CustomerInformationFieldDefinitionID] = @CustomerInformationFieldDefinitionID AND @CustomerInformationFieldDefinitionID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([FieldAtBeginningOfQuestionnaire] = @FieldAtBeginningOfQuestionnaire AND @FieldAtBeginningOfQuestionnaire is not null)
	OR ([OrdinalPosition] = @OrdinalPosition AND @OrdinalPosition is not null)
	OR ([FieldName] = @FieldName AND @FieldName is not null)
	OR ([FieldLabel] = @FieldLabel AND @FieldLabel is not null)
	OR ([Mandatory] = @Mandatory AND @Mandatory is not null)
	OR ([VerifyPhoneNumber] = @VerifyPhoneNumber AND @VerifyPhoneNumber is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Find] TO [sp_executeall]
GO
