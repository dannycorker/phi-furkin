SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPersonnelOptionTypes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptionTypes_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPersonnelOptionTypeID int   = null ,

	@OptionTypeName nchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPersonnelOptionTypeID]
	, [OptionTypeName]
    FROM
	[dbo].[ClientPersonnelOptionTypes] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID OR @ClientPersonnelOptionTypeID IS NULL)
	AND ([OptionTypeName] = @OptionTypeName OR @OptionTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPersonnelOptionTypeID]
	, [OptionTypeName]
    FROM
	[dbo].[ClientPersonnelOptionTypes] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID AND @ClientPersonnelOptionTypeID is not null)
	OR ([OptionTypeName] = @OptionTypeName AND @OptionTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptionTypes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Find] TO [sp_executeall]
GO
