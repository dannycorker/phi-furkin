SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Dave Morgan
-- Create date:	  18/10/2016	
-- Description:   Reschedules payment dates for uncollected payments on a purchased product
-- Mods
--	2016-11-10 DCM Added clientID to billing config selection
--  2017-08-23 CPS accomodate separate admin fee rows
--	2017-08-23 CPS updated call to fn_C00_CalculateActualCollectionDate
--  2017-11-01 CPS Call PurchasedProductHistory__Backup with @LeadEventID
--  2018-04-18 JEL Changed Override logic from using the PurchaseProductPaymentScheduleID variable to CPS variable so we get grouped payments*/
--  2019-04-11 PL  Modified inserts of PAyment and actuall collection date to be in line with NB process #54793
--  2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
--  2021-03-10 AAD for FURKIN-237 | Update Payment Day Billing (170168) so it's accurate after a payment day change
-- =============================================
CREATE PROCEDURE [dbo].[ReschedulePaymentDate_CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT = NULL
AS
BEGIN


--	declare
--	@ClientID INT = 384,
--	@CaseID INT = 2325,
--	@LeadEventID INT = NULL

	SET NOCOUNT ON;
	  
	DECLARE @Success BIT
      
	-- Get the third party fields
 	-- Gets the customer, lead, matter identities and the leadtype
	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @WhoCreated INT
	SELECT @LeadID=cs.LeadID FROM Cases cs WITH (NOLOCK) WHERE cs.CaseID = @CaseID
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@LeadID
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.CaseID=@CaseID
	
	SELECT @WhoCreated = le.WhoCreated FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID=@LeadEventID
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	--4413	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN
		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
		SELECT @AccountID=AccountID FROM PurchasedProduct WITH (NOLOCK) WHERE PurchasedProductID=@PurchasedProductID
	END	
					
	--4522 New payment day
	DECLARE @NewPaymentDay_DV VARCHAR(2000), @IsInt_NewPaymentDay BIT, @NewPaymentDay INT
	SELECT @NewPaymentDay_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4522)	
	SELECT @IsInt_NewPaymentDay = dbo.fnIsInt(@NewPaymentDay_DV)		
	IF(@IsInt_NewPaymentDay=1)
	BEGIN
		SELECT @NewPaymentDay = CAST(ItemValue AS INT) FROM LookupListItems WITH (NOLOCK) WHERE LookupListItemID=@NewPaymentDay_DV
	END	
	
	--4523 Next payment date override
	DECLARE @OverrideDate_DV VARCHAR(2000), @IsDateTime_OverrideDate BIT, @OverrideDate DATETIME
	SELECT @OverrideDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4523)		
	SELECT @IsDateTime_OverrideDate = dbo.fnIsDateTime(@OverrideDate_DV)		
	IF(@IsDateTime_OverrideDate=1)
	BEGIN
		SELECT @OverrideDate = CAST(@OverrideDate_DV AS DATETIME)
	END	

	PRINT '@OverrideDate'

    BEGIN TRY  
      
            DECLARE @ScheduleID INT = 1,
                        @TestID INT = 0,  
                        @FirstPaymentDate DATE,
                        @ValidFrom DATE, 
                        @ValidTo DATE,
                        @PreferredPaymentDay INT, 
                        @ProductCostGross NUMERIC(18,2),
                        @ProductCostVAT NUMERIC(18,2),
                        @PaymentFrequencyID INT,
                        @NumberOfInstallments INT = 12,
                        @RemainderUpFront BIT,
                        @Payment NUMERIC(36,18),
                        @RoundedPayment NUMERIC(18,2),
                        @RoundedCost NUMERIC(18,2),
                        @RemainderPayment NUMERIC(18,2),
                        @PaymentScheduleValidFrom DATE,
						@PaymentVAT NUMERIC(36,18),
                        @RoundedPaymentVAT NUMERIC(18,2),
                        @RoundedCostVAT NUMERIC(18,2),
                        @RemainderPaymentVAT NUMERIC(18,2),
                        @PurchasedProductPaymentScheduleID INT,
                        @CoverFromDay TINYINT,
                        @FromDate DATE,
                        @WaitType INT,
                        @PaymentMethod INT, 
                        @WaitPeriod INT,
                        @CustomerPaymentScheduleID INT,
						@CountryID INT
                   

                       
            DECLARE @NewSchedule TABLE ( ScheduleID INT, 
										 PaymentDate DATE, 
										 ActualCollectionDate DATE, 
										 CoverFrom DATE, 
										 CoverTo DATE, 
										 PaymentNet NUMERIC(18,2), 
										 PaymentVAT NUMERIC(18,2), 
										 PaymentGross NUMERIC(18,2),
										 PaymentStatusID INT,
										 [Type] INT
										)

            -- performs backup of payment schedules if required
            IF @LeadEventID IS NOT NULL
            BEGIN
				EXEC dbo.PurchasedProductHistory__Backup @PurchasedProductID, @WhoCreated, @LeadEventID
				EXEC PurchasedProductPaymentScheduleHistory__Backup @PurchasedProductID, @WhoCreated, 0  -- don't delete the current purchased product schedule, we're just going to update the uncollected rows
				EXEC CustomerPaymentScheduleHistory__Backup @CustomerID, @WhoCreated, 0 -- don't delete the current schedule, we're just going to update the uncollected rows         
            
				UPDATE PurchasedProduct 
				SET PreferredPaymentDay=@NewPaymentDay,
					WhenModified=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) 
				WHERE PurchasedProductID = @PurchasedProductID			
			END

			/* Update Payment Day Billing in case it has changed. 2021-03-10 AAD for FURKIN-237 */
			IF @NewPaymentDay > 0
			BEGIN
				
				EXEC _C00_SimpleValueIntoField 170168, @NewPaymentDay, @MatterID

			END
	 
            SELECT      @ClientID = product.ClientID,
                        @AccountID = product.AccountID, 
                        @ValidFrom = product.ValidFrom,
                        @ValidTo = product.ValidTo,
                        @PreferredPaymentDay = @NewPaymentDay,
                        @ProductCostGross = product.ProductCostGross,
                        @ProductCostVAT = product.ProductCostVAT,
                        @PaymentFrequencyID = product.PaymentFrequencyID,
                        @NumberOfInstallments = product.NumberOfInstallments,
                        @RemainderUpFront = product.RemainderUpFront,
                        @CustomerID = product.CustomerID,
                        @WhoCreated = product.WhoCreated
            FROM PurchasedProduct product
            WHERE product.PurchasedProductID = @PurchasedProductID
            
            -- ensure number of increments is set
            IF @NumberOfInstallments IS NULL
            BEGIN
            
                  SELECT @NumberOfInstallments=pf.DefaultInstallmentLength 
                  FROM PaymentFrequency pf WITH (NOLOCK) 
                  WHERE pf.PaymentFrequencyID=@PaymentFrequencyID
                  
            END

            DECLARE @Schedule TABLE
            (
                  ID INT,
                  DATE DATE,
                  ToDate DATE
            )
          
            DECLARE @PaymentSchedule TABLE
            (
                  ID INT,
                  DATE DATE
            )
          
          SELECT @CoverFromDay = DATEPART(DD, @ValidFrom)

            -- ensure that payment day always fall within a cover period
            IF DATEPART(DAY,@ValidFrom) > @PreferredPaymentDay
            BEGIN
				SELECT @PaymentScheduleValidFrom = CAST(DATEADD(m, DATEDIFF(m, -1, @ValidFrom), 0) AS DATE)
            END
            ELSE
            BEGIN
				SELECT @PaymentScheduleValidFrom = @ValidFrom
            END

            INSERT @PaymentSchedule (ID, DATE) -- Creates the payment dates for the schedule
            SELECT * FROM dbo.fnCreateSchedule(@PaymentFrequencyID, @NumberOfInstallments, @ValidFrom, @PaymentScheduleValidFrom, CAST(@PreferredPaymentDay AS TINYINT))
            
            INSERT @Schedule (ID, DATE) -- Creates the cover dates for the schedule
            SELECT * FROM dbo.fnCreateSchedule(@PaymentFrequencyID, @NumberOfInstallments, @ValidFrom, DATEADD(DD,-1,@ValidFrom), CAST(@CoverFromDay AS TINYINT))
            
            IF @NumberOfInstallments = 1
            BEGIN             
                  -- Single payment for a year's cover
                  UPDATE s
                  SET ToDate = @ValidTo
                  FROM @Schedule s
                  
            END
            ELSE
            BEGIN
                  -- payment schedule with multiple installments
                  UPDATE s
                  SET ToDate = DATEADD(DAY, -1, DATEADD(MONTH, 1, s.Date))
                  FROM @Schedule s
            END       
                       
			/* PL 2019-04-11 Changed Payment Date  to Actual Collection date in the insert */ 
            INSERT INTO @NewSchedule (ScheduleID,PaymentDate,ActualCollectionDate,CoverFrom,CoverTo,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,[Type])
            SELECT ppps.PurchasedProductPaymentScheduleID,NULL,ps.[DATE],ppps.CoverFrom,ppps.CoverTo,ppps.PaymentNet,ppps.PaymentVAT,ppps.PaymentGross,ppps.PaymentStatusID,ppps.PurchasedProductPaymentScheduleTypeID
            FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
            INNER JOIN @Schedule s ON ppps.CoverFrom=s.[DATE]
            INNER JOIN @PaymentSchedule ps ON s.ID=ps.ID
            WHERE ppps.PurchasedProductID=@PurchasedProductID
            AND ppps.CustomerLedgerID IS NULL

            /* 
                  Set the actual collection date 
                  1. For card payments on new schedules, the actual collection day = payment day
                  2. For direct debit payments on new schedules, add mandate wait to the first payment & weekend/bank holiday wait to all 
            
            */ 
            
            
            SELECT @PaymentMethod=a.AccountTypeID 
            FROM PurchasedProduct pp WITH (NOLOCK) 
            INNER JOIN Account a WITH (NOLOCK) ON pp.AccountID=a.AccountID
            WHERE pp.PurchasedProductID=@PurchasedProductID

			-- update earliest date if required ( override set, during mandate wait or in the past )
			SELECT TOP 1 @FirstPaymentDate=ns.PaymentDate, @PurchasedProductPaymentScheduleID=ns.ScheduleID 
			FROM @NewSchedule ns 
			WHERE ns.[Type] <> 5 /*Scheduled*/ /*CPS 2017-08-23 Don't allow admin fees to disrupt this.*/
			ORDER BY ns.PaymentDate		
			
			/*2018-04-18 JEL Grab the customer payment schedule ID for the first payment we get so we can check if it has another PPPS record grouped into it*/ 
			SELECT @CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID 
			FROM PurchasedProductPaymentSchedule ppps WITH ( NOLOCK ) 
			WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
			
			-- Override date is king unless it falls within mandate wait
			IF @OverrideDate IS NOT NULL
			BEGIN
			
				SELECT @FirstPaymentDate = @OverrideDate
				
			END 
			

			/*GPR 2020-03-02 for AAG-202*/
			SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
			WHERE cl.ClientID = @ClientID
	
			IF @CountryID <> 14 /*Australia*/
			BEGIN
				IF @FirstPaymentDate < ( SELECT TOP 1 am.FirstAcceptablePaymentDate FROM AccountMandate am WITH (NOLOCK) 
										 WHERE am.AccountID = @AccountID 
										 AND am.MandateStatusID IN (1,2,3) 
										 ORDER BY AccountMandateID DESC ) 
										 AND @PaymentMethod=1
				BEGIN
				
					SELECT @FirstPaymentDate=( SELECT TOP 1 am.FirstAcceptablePaymentDate FROM AccountMandate am WITH (NOLOCK) 
											 WHERE am.AccountID = @AccountID 
											 AND am.MandateStatusID IN (1,2,3) 
											 ORDER BY AccountMandateID DESC )
					
				END
			END

			-- if there is no override then we need to be sure the standard business wait period is applied
			SELECT @WaitPeriod=RegularPaymentWait 
			FROM dbo.BillingConfiguration WITH (NOLOCK) 
			WHERE ClientID=@ClientID

			IF @FirstPaymentDate < DATEADD(DAY,@WaitPeriod,dbo.fn_GetDate_Local()) AND @OverrideDate IS NULL
			BEGIN
			
				SELECT @FirstPaymentDate = DATEADD(DAY,@WaitPeriod,dbo.fn_GetDate_Local())
			
			END
			
			/*Update the first item AND any AdminFees that come along with it*/
			/*2018-04-18 JEL Changed this from the PurchaseProductPaymentScheduleID variable to CPS variable so we get grouped payments*/
			UPDATE ns
			SET PaymentDate=@FirstPaymentDate
			FROM @NewSchedule ns
			INNER JOIN PurchasedProductPaymentSchedule p WITH ( NOLOCK ) on p.PurchasedProductPaymentScheduleID = ns.ScheduleID
			WHERE p.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

			/* Removed by PL 2019-04-11
			UPDATE ns
			SET ActualCollectionDate = dbo.fn_C00_CalculateActualCollectionDate(@AccountID,@PaymentMethod,PaymentDate,0, ns.PaymentGross)
			FROM @NewSchedule ns 
			*/

			IF @ClientID = 607
			BEGIN

				UPDATE ns
				SET PaymentDate = ns.ActualCollectionDate
				FROM @NewSchedule ns 
            
			END
			ELSE
			BEGIN

				UPDATE ns
				SET PaymentDate = dbo.fn_C00_CalculateActualCollectionDate(@AccountID,@PaymentMethod,ActualCollectionDate,5, ns.PaymentGross)
				FROM @NewSchedule ns 

			END

		
            /*CPS 2017-08-23 AdminFees have no CoverFrom/To dates, so update them to match the next valid schedule date*/
            /*GPR 2021-05-07 removed for FURKIN-595 / SDFURPHI-53*/
			/*
			;WITH FirstScheduleItem AS
            (
				SELECT TOP (1) * 
				FROM @NewSchedule ns 
				ORDER BY ns.ActualCollectionDate
            )
            INSERT INTO @NewSchedule (ScheduleID,PaymentDate,ActualCollectionDate,CoverFrom,CoverTo,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,[Type])
            SELECT	 pps.PurchasedProductPaymentScheduleID
					,ns.PaymentDate
					,ns.ActualCollectionDate
					,pps.CoverFrom
					,pps.CoverTo
					,pps.PaymentNet
					,pps.PaymentVAT
					,pps.PaymentGross
					,pps.PaymentStatusID
					,pps.PurchasedProductPaymentScheduleTypeID
            FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK )
            CROSS APPLY FirstScheduleItem ns
            WHERE pps.PurchasedProductID = @PurchasedProductID
            AND pps.PaymentStatusID = 1
            AND pps.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
            */

            IF @LeadEventID IS NULL
            BEGIN
            
				SELECT n.*, ps.PaymentStatusName, ppt.PurchasedProductPaymentScheduleTypeName AS [PurchasedProductPaymentScheduleType]
				FROM @NewSchedule n
				INNER JOIN PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = n.PaymentStatusID
				INNER JOIN PurchasedProductPaymentScheduleType ppt WITH (NOLOCK) ON ppt.PurchasedProductPaymentScheduleTypeID = n.Type
				ORDER BY n.PaymentDate ASC /*GPR 2021-05-07 Added ORDER BY for FURKIN-595 / SDFURPHI-53*/
            END
            ELSE
            BEGIN
            
				--update PPPS
                       
				UPDATE ppps
				SET PaymentDate=n.PaymentDate,
					ActualCollectionDate=n.ActualCollectionDate
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
				INNER JOIN @NewSchedule n ON n.ScheduleID=ppps.PurchasedProductPaymentScheduleID
				
				--rebuild customer payment schedule 
				EXEC dbo.Billing__RebuildCustomerPaymentSchedule @AccountID, @FromDate, @WhoCreated     
           
            END
            

            SET @Success = 1
      
      END TRY  
      BEGIN CATCH  
      
            UPDATE PurchasedProduct
            SET PaymentScheduleSuccessfullyCreated=0, PaymentScheduleCreatedOn=NULL
            WHERE PurchasedProductID=@PurchasedProductID
            
            SET @Success = 0
            
      END CATCH  
      
      SELECT @Success Success
      
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ReschedulePaymentDate_CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReschedulePaymentDate_CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReschedulePaymentDate_CreateFromThirdPartyFields] TO [sp_executeall]
GO
