SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-12-02
-- Description:	Quick check of the last [n] automatedtask entries in reverse order
-- =============================================
CREATE PROCEDURE [dbo].[BatchCheck]
	@rows int = 50,
	@taskid int = NULL,
	@clientid int = NULL,
	@status char(1) = '%',
	@chardate char(10) = NULL,
	@complete int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP (@rows) * 
	FROM AutomatedTaskResult atr 
	INNER JOIN AutomatedTask task ON task.TaskID = atr.TaskID 
	WHERE atr.Description LIKE @status + '%' 
	AND (atr.TaskID = @taskid OR @taskid IS NULL)
	AND (datediff(dd, atr.RunDate, @chardate) = 0 OR @chardate IS NULL)
	AND (atr.Complete = @complete OR @complete IS NULL)
	AND (atr.ClientID = @clientid OR @clientid IS NULL)
	ORDER BY 1 DESC
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[BatchCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BatchCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BatchCheck] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[BatchCheck] TO [sp_executehelper]
GO
