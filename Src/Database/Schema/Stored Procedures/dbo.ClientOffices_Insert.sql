SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientOffices table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_Insert]
(

	@ClientOfficeID int    OUTPUT,

	@ClientID int   ,

	@OfficeName varchar (50)  ,

	@BuildingName varchar (50)  ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (50)  ,

	@Country varchar (200)  ,

	@PostCode varchar (10)  ,

	@OfficeTelephone varchar (50)  ,

	@OfficeTelephoneExtension varchar (10)  ,

	@OfficeFax varchar (50)  ,

	@CountryID int   ,

	@LanguageID int   ,

	@SubClientID int   ,

	@AdminClientPersonnelID int   ,

	@CurrencyID int   
)
AS


				
				INSERT INTO [dbo].[ClientOffices]
					(
					[ClientID]
					,[OfficeName]
					,[BuildingName]
					,[Address1]
					,[Address2]
					,[Town]
					,[County]
					,[Country]
					,[PostCode]
					,[OfficeTelephone]
					,[OfficeTelephoneExtension]
					,[OfficeFax]
					,[CountryID]
					,[LanguageID]
					,[SubClientID]
					,[AdminClientPersonnelID]
					,[CurrencyID]
					)
				VALUES
					(
					@ClientID
					,@OfficeName
					,@BuildingName
					,@Address1
					,@Address2
					,@Town
					,@County
					,@Country
					,@PostCode
					,@OfficeTelephone
					,@OfficeTelephoneExtension
					,@OfficeFax
					,@CountryID
					,@LanguageID
					,@SubClientID
					,@AdminClientPersonnelID
					,@CurrencyID
					)
				-- Get the identity value
				SET @ClientOfficeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Insert] TO [sp_executeall]
GO
