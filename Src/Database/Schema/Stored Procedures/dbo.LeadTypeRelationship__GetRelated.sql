SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-29
-- Description:	Gets a list of related leads and cases for the document parser
-- Modified:	2014-10-21	SB	Added in lead type ID and also changed the union statement at the end to prevent duplicates
-- =============================================
CREATE PROCEDURE [dbo].[LeadTypeRelationship__GetRelated] 
(
	@LeadID INT,
	@CaseID INT
)	
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Identities TABLE
	(
		LeadID INT,
		CaseID INT
	)
	
	-- Lead, case and matter links
	;WITH LeadsFromTo AS 
	(
		SELECT ToLeadID AS LeadID, 3 AS Priority
		FROM dbo.LeadTypeRelationship WITH (NOLOCK) 
		WHERE FromLeadID = @LeadID
	),
	LeadsToFrom AS 
	(
		SELECT FromLeadID AS LeadID, 3 AS Priority
		FROM dbo.LeadTypeRelationship WITH (NOLOCK) 
		WHERE ToLeadID = @LeadID
	),
	CasesFromTo AS 
	(
		SELECT c.LeadID, r.ToCaseID AS CaseID, 2 AS Priority
		FROM dbo.LeadTypeRelationship r WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON r.ToCaseID = c.CaseID
		WHERE r.FromCaseID = @CaseID
	),
	CasesToFrom AS 
	(
		SELECT c.LeadID, r.FromCaseID AS CaseID, 2 AS Priority
		FROM dbo.LeadTypeRelationship r WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON r.FromCaseID = c.CaseID
		WHERE r.ToCaseID = @CaseID
	),
	MattersFromTo AS 
	(
		SELECT mTo.LeadID, mTo.CaseID, 1 AS Priority
		FROM dbo.LeadTypeRelationship r WITH (NOLOCK) 
		INNER JOIN dbo.Matter mFrom WITH (NOLOCK) ON r.FromMatterID = mFrom.MatterID
		INNER JOIN dbo.Matter mTo WITH (NOLOCK) ON r.ToMatterID = mTo.MatterID
		WHERE mFrom.CaseID = @CaseID
	),
	MattersToFrom AS 
	(
		SELECT mFrom.LeadID, mFrom.CaseID, 1 AS Priority
		FROM dbo.LeadTypeRelationship r WITH (NOLOCK) 
		INNER JOIN dbo.Matter mFrom WITH (NOLOCK) ON r.FromMatterID = mFrom.MatterID
		INNER JOIN dbo.Matter mTo WITH (NOLOCK) ON r.ToMatterID = mTo.MatterID
		WHERE mTo.CaseID = @CaseID
	),
	AllLeads AS
	(
		SELECT LeadID, CaseID, Priority FROM MattersFromTo
		UNION
		SELECT LeadID, CaseID, Priority FROM MattersToFrom
		UNION 
		SELECT LeadID, CaseID, Priority FROM CasesFromTo
		UNION
		SELECT LeadID, CaseID, Priority FROM CasesToFrom
		UNION 
		SELECT LeadID, NULL, Priority FROM LeadsFromTo
		UNION
		SELECT LeadID, NULL, Priority FROM LeadsToFrom
	),
	-- Get the most specific links... matter, then case, then lead
	SortedLeads AS
	(
		SELECT	LeadID, CaseID, Priority,
				ROW_NUMBER() OVER(PARTITION BY LeadID ORDER BY Priority) as rn 
		FROM AllLeads
	),
	DistinctLeads AS
	(
		SELECT LeadID, CaseID, Priority 
		FROM SortedLeads
		WHERE rn = 1
	),
	-- For lead links just get the last case
	LeadOnlyCases AS
	(
		SELECT	d.LeadID, c.CaseID,
				ROW_NUMBER() OVER(PARTITION BY c.LeadID ORDER BY c.CaseID DESC) as rn 
		FROM DistinctLeads d
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON d.LeadID = c.LeadID
		WHERE d.Priority = 3
	)
	
	-- Get the rows that come from case and matter links
	INSERT @Identities (LeadID, CaseID)
	SELECT LeadID, CaseID
	FROM DistinctLeads
	WHERE Priority = 1
	UNION
	SELECT LeadID, CaseID
	FROM DistinctLeads
	WHERE Priority = 1
	UNION
	-- And take the first case ID for the leads
	SELECT LeadID, CaseID 
	FROM LeadOnlyCases
	WHERE rn = 1
	
	
	SELECT i.LeadID, i.CaseID, l.LeadTypeID
	FROM @Identities i
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON i.LeadID = l.LeadID
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeRelationship__GetRelated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeRelationship__GetRelated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeRelationship__GetRelated] TO [sp_executeall]
GO
