SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientQuestionnairesVisitedButNotComplete table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedButNotComplete_Insert]
(

	@ClientQuestionnairesVisitedButNotCompleteID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@VisitedDate datetime   ,

	@IPAddress varchar (50)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[ClientQuestionnairesVisitedButNotComplete]
					(
					[ClientQuestionnaireID]
					,[VisitedDate]
					,[IPAddress]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@VisitedDate
					,@IPAddress
					,@ClientID
					)
				-- Get the identity value
				SET @ClientQuestionnairesVisitedButNotCompleteID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Insert] TO [sp_executeall]
GO
