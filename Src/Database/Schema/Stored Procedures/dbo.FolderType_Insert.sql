SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FolderType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FolderType_Insert]
(

	@FolderTypeID int   ,

	@FolderTypeName varchar (50)  ,

	@FolderTypeDescription varchar (250)  
)
AS


				
				INSERT INTO [dbo].[FolderType]
					(
					[FolderTypeID]
					,[FolderTypeName]
					,[FolderTypeDescription]
					)
				VALUES
					(
					@FolderTypeID
					,@FolderTypeName
					,@FolderTypeDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FolderType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Insert] TO [sp_executeall]
GO
