SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_Update]
(

	@ClientRelationshipID int   ,

	@ClientRelationshipName varchar (200)  ,

	@OutgoingClientID int   ,

	@ReceivingClientID int   ,

	@OutgoingLeadTypeID int   ,

	@IncomingLeadTypeID int   ,

	@OutgoingEventTypeID int   ,

	@IncomingEventTypeID int   ,

	@Enabled bit   ,

	@ClearLeadRefs bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientRelationship]
				SET
					[ClientRelationshipName] = @ClientRelationshipName
					,[OutgoingClientID] = @OutgoingClientID
					,[ReceivingClientID] = @ReceivingClientID
					,[OutgoingLeadTypeID] = @OutgoingLeadTypeID
					,[IncomingLeadTypeID] = @IncomingLeadTypeID
					,[OutgoingEventTypeID] = @OutgoingEventTypeID
					,[IncomingEventTypeID] = @IncomingEventTypeID
					,[Enabled] = @Enabled
					,[ClearLeadRefs] = @ClearLeadRefs
				WHERE
[ClientRelationshipID] = @ClientRelationshipID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Update] TO [sp_executeall]
GO
