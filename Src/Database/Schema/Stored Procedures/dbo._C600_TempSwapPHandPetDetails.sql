SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-08-17
-- Description: Swap temporary Pet or Ph details to their permanent location - used for generating a quote during an MTA
-- you will need to place the temporary details back(undo the effects of this proc) until you have vet confirmation & Customer confirmation for the quote
-- To do this, run the proc again
-- Mods:
-- 2017-02-22 DCM check '' or 0 for pet sex
-- 2017-08-03 CPS populate WhenChanged, WhoChanged, ChangeNotes for the history
-- 2017-08-09 CPS added Neutered
-- 2018-11-16 GPR Defect 1212 Address Picker fix, uncommented and added variables, moved above Postcode block
-- =============================================
CREATE PROCEDURE [dbo].[_C600_TempSwapPHandPetDetails]
(
	@Customerid INT
)

AS
BEGIN


	SET NOCOUNT ON;

	DECLARE  @LeadID	INT = (SELECT DBO.fnGetSimpleDvAsInt(177038,@Customerid)) /*TempLeadID*/
	DECLARE  @MatterID	INT = (SELECT DBO.fnGetSimpleDvAsInt(177039,@Customerid)) /*TempMatterID*/
	DECLARE  @CaseID	INT 
			,@TempValue VARCHAR(200)
			,@PermanentValue VARCHAR(200)
	
	
	DECLARE @WhoCreated INT = 58552 /*Aquarium Automation*/

	SELECT @CaseID = (SELECT TOP 1 m.CaseID FROM  Matter m WITH (NOLOCK) WHERE m.MatterID = @MatterID)

	/*Switch temporary pet details to permanent*/
	

	-- Pet Vicious
	SELECT @TempValue = dbo.fnGetDv(178405,@CaseID) /*Vicious Tendencies*/

	IF @TempValue > '' AND @TempValue <> '0'
	BEGIN 
		
		SELECT @PermanentValue = dbo.fnGetDv(177452,@CaseID) /*Has your pet ever shown aggressive tendencies?*/
		
		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 177452, @TempValue, @LeadID			/*Has your pet ever shown aggressive tendencies?*/
		EXEC dbo._C00_SimpleValueIntoField 178405, @PermanentValue, @CustomerID /*Vicious Tendencies*/
	
	END
	
	-- Pet Purchase Cost	
	SELECT @TempValue = dbo.fnGetDv(175352,@CaseID) /*Pet Purchase Cost*/
	IF @TempValue > ''
	BEGIN 
		
		SELECT @PermanentValue = dbo.fnGetDv(144339,@CaseID) /*Pet Purchase Cost*/
		
		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 144339, @TempValue, @LeadID			/*Pet Purchase Cost*/
		EXEC dbo._C00_SimpleValueIntoField 175352, @PermanentValue, @CustomerID /*Pet Purchase Cost*/
	
	END
	
	
	/*Campagin Codes and discounts*/
	/*GPR 2018-06-25 Modified to use fnGetSimpleDv*/
	SELECT @TempValue = dbo.fnGetSimpleDv(179912, @Customerid)
	IF @TempValue > ''
	BEGIN 
		
		SELECT @PermanentValue = dbo.fnGetSimpleDv(175488,@MatterID)
		
		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 175488, @TempValue, @MatterID
		EXEC dbo._C00_SimpleValueIntoField 179912, @PermanentValue, @CustomerID
	
	END
	
	
	-- Pet Sex	
	SELECT @TempValue = dbo.fnGetDv(175329,@CaseID)	/*Pet Sex*/
	IF NOT (@TempValue = '' OR @TempValue = 0)
	BEGIN 
					
		SELECT @PermanentValue = dbo.fnGetDv(144275,@CaseID) /*Pet Sex*/
		
		EXEC dbo._C00_SimpleValueIntoField 144275, @TempValue, @LeadID			/*Pet Sex*/
		EXEC dbo._C00_SimpleValueIntoField 175329, @PermanentValue, @CustomerID /*Pet Sex*/

	END

	-- Pet Neutered	
	SELECT @TempValue = dbo.fnGetDv(178406,@CaseID)	/*Pet Neutered*/
	IF NOT (@TempValue = '' OR @TempValue = 0)
	BEGIN 

		SELECT @PermanentValue = dbo.fnGetDv(152783,@CaseID) /*Pet Neutered*/

		EXEC dbo._C00_SimpleValueIntoField 152783, @TempValue, @LeadID			/*Pet Neutered*/
		EXEC dbo._C00_SimpleValueIntoField 178406, @PermanentValue, @CustomerID /*Pet Neutered*/

	END
	
	-- Pet Breed
	SELECT @TempValue = dbo.fnGetDv(175328,@CaseID)		 
	IF @TempValue >''
	BEGIN 
		
		SELECT @PermanentValue = dbo.fnGetDv(144272,@CaseID) /*Pet Type*/
		
		EXEC dbo._C00_SimpleValueIntoField 144272, @TempValue, @LeadID			/*Pet Type*/
		EXEC dbo._C00_SimpleValueIntoField 175328, @PermanentValue, @CustomerID /*Pet Breed*/

	END
	
	-- Pet Date Of Birth	
	SELECT @TempValue = dbo.fnGetDv(175326,@CaseID)	/*Pet Date Of Birth*/
	IF @TempValue >''
	BEGIN 
				
		SELECT @PermanentValue = dbo.fnGetDv(144274,@CaseID) /*Pet Date of Birth*/
				
		EXEC dbo._C00_SimpleValueIntoField 144274, @TempValue, @LeadID /*Pet Date of Birth*/
		EXEC dbo._C00_SimpleValueIntoField 175326, @PermanentValue, @CustomerID /*Pet Date Of Birth*/
	
	END

	-- Pet Name
	SELECT @TempValue = dbo.fnGetDv(175325,@CaseID)	/*Pet Name*/
	IF @TempValue >''
	BEGIN 

		SELECT @PermanentValue = dbo.fnGetDv(144268,@CaseID) /*Pet Name*/

		EXEC dbo._C00_SimpleValueIntoField 144268, @TempValue, @LeadID			/*Pet Name*/
		EXEC dbo._C00_SimpleValueIntoField 175325,@PermanentValue, @CustomerID	/*Pet Name*/

		
	END
	
	
	---------------
	
	---------------
				
		SELECT @TempValue = dbo.fnGetDvLuli(175310,@CaseID) /*Title*/
		
		IF @TempValue<>''
		BEGIN
		
			SELECT @PermanentValue = dbo.fnGetDvAsInt(175310,@CaseID) /*Title*/

			UPDATE Customers 
			SET TitleID=( SELECT t.TitleID FROM Titles t WITH (NOLOCK)
							INNER JOIN LookupListItems lli WITH (NOLOCK) ON t.Title=lli.ItemValue 
							AND lli.LookupListItemID=dbo.fnGetDvAsInt(175310,@CaseID) )  
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175310, @PermanentValue, @CustomerID /*Title*/
	
		END
				
		-- FirstName
			
		SELECT @TempValue = dbo.fnGetDv(175311,@CaseID) /*First Name*/
		
		IF @TempValue<>''
		BEGIN
			
			SELECT @PermanentValue =  c.FirstName FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
			
			UPDATE Customers SET FirstName=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175311, @PermanentValue, @CustomerID /*First Name*/
		
		END
				
		-- MiddleName
			
		SELECT @TempValue = dbo.fnGetDv(175312,@CaseID) /*Middle Name*/
		
		IF @TempValue<>''
		BEGIN
		
			SELECT @PermanentValue = c.MiddleName FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
		
			UPDATE Customers SET MiddleName=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175312, @PermanentValue, @CustomerID /*Middle Name*/
		
		END
				
		-- Last Name
			
		SELECT @TempValue = dbo.fnGetDv(175313,@CaseID) /*Last Name*/
		
		IF @TempValue<>''
		BEGIN

			SELECT @PermanentValue = c.LastName FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
					
			UPDATE Customers SET LastName=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175313, @PermanentValue, @CustomerID /*Last Name*/
		
		END
		
		IF EXISTS (SELECT * FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
				   WHERE cdv.CustomerID = @Customerid 
				   AND cdv.DetailFieldID IN (175314,175315,175316,175317,175318) 
				   AND cdv.DetailValue <> '') 
		BEGIN 

			-- Address1
			SELECT @TempValue = ''
			SELECT @TempValue = dbo.fnGetDv(175314,@CaseID) /*Address 1*/
					
			SELECT @PermanentValue = c.Address1 FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
			
			UPDATE Customers SET Address1=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175314, @PermanentValue, @CustomerID /*Address 1*/
						
			-- Address2
			SELECT @TempValue = ''
			SELECT @TempValue = dbo.fnGetDv(175315,@CaseID) /*Address 2*/
	
			SELECT @PermanentValue = c.Address2 FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
			
			UPDATE Customers SET Address2=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175315, @PermanentValue, @CustomerID /*Address 2*/
		
			-- Town
			SELECT @TempValue = ''
			SELECT @TempValue = dbo.fnGetDv(175316,@CaseID)
		
			SELECT @PermanentValue = c.Town FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
			
			UPDATE Customers SET Town=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175316, @PermanentValue, @CustomerID /*Town*/
				
			-- County
			SELECT @TempValue = ''
			SELECT @TempValue = dbo.fnGetDv(175317,@CaseID)

			SELECT @PermanentValue = c.County FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
			
			UPDATE Customers SET County=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175317, @PermanentValue, @CustomerID /*County*/
		
		
		
		---- Address Picker change	 /*GPR 2018-11-16 Defect 1212 moved to before Postcode block, uncommented and added declaration of variables*/
		--DECLARE @Packed VARCHAR(2000), @Address1 VARCHAR(2000), @Address2 VARCHAR(2000), @Town VARCHAR(2000), @County VARCHAR(2000), @PostCode VARCHAR(2000)
		--DECLARE @Unpacked TABLE (Line VARCHAR(2000), ID INT) 
		
		--SELECT @Packed = DetailValue
		--FROM dbo.CustomerDetailValues WITH (NOLOCK) 
		--WHERE DetailFieldID = 175478
		--AND CustomerID = @CustomerID 

		--INSERT @Unpacked (Line)
		--SELECT AnyValue
		--FROM dbo.fnTableOfValues(@Packed, '_')

		--DECLARE @Count INT = 0
		--SELECT @Count = COUNT(*) 
		--FROM @Unpacked

		--IF @Count = 5
		--BEGIN
				
		--	SELECT @Address1=Line FROM @Unpacked WHERE ID = 1 
		--	SELECT @Address2=Line FROM @Unpacked WHERE ID = 2 
		--	SELECT @Town=Line FROM @Unpacked WHERE ID = 3
		--	SELECT @County=Line FROM @Unpacked WHERE ID = 4 
		--	SELECT @PostCode=Line FROM @Unpacked WHERE ID = 5 
						
		--	UPDATE dbo.Customers
		--	SET Address1	= @Address1,
		--		Address2	= @Address2,
		--		Town		= @Town,
		--		County		= @County,
		--		PostCode	= @PostCode
		--	WHERE CustomerID = @CustomerID
					
		--	EXEC dbo._C00_SimpleValueIntoField 175478, '', @CustomerID
			
		--END
	
				
			-- Postcode
			SELECT @TempValue = ''
			SELECT @TempValue = dbo.fnGetDv(175318,@CaseID) /*Postcode*/

			SELECT @PermanentValue = c.PostCode FROM customers c WITH (NOLOCK)  WHERE CustomerID=@CustomerID
		
			UPDATE Customers SET PostCode=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175318, @PermanentValue, @CustomerID /*Postcode*/
		
		END
				
		-- DOB
			
		SELECT @TempValue = dbo.fnGetDv(175319,@CaseID)
		
		IF @TempValue<>''
		BEGIN
		
			SELECT @PermanentValue = c.DateOfBirth FROM Customers c WITH (NOLOCK) WHERE c.CustomerID =@Customerid
			
			UPDATE Customers SET DateOfBirth=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175319, @PermanentValue, @CustomerID /*Date Of Birth*/
		
		END
				
		-- Email
			
		SELECT @TempValue = dbo.fnGetDv(175320,@CaseID) /*E-mail*/
		
		IF @TempValue<>''
		BEGIN
			

			SELECT @PermanentValue = c.emailaddress FROM Customers c WITH (NOLOCK) WHERE c.CustomerID =@Customerid
			
			UPDATE Customers SET EmailAddress=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175320, @PermanentValue, @CustomerID /*E-mail*/
		
		END
				
		-- Home phone
			
		SELECT @TempValue = dbo.fnGetDv(175321,@CaseID) /*Home Phone*/
		
		IF @TempValue<>''
		BEGIN
			
			
			SELECT @PermanentValue = c.HomeTelephone FROM Customers c WITH (NOLOCK) WHERE c.CustomerID =@Customerid
			
			UPDATE Customers SET HomeTelephone=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175321, @PermanentValue, @CustomerID /*Home Phone*/
		
		END
				
		-- Daytime phone
			
		SELECT @TempValue = dbo.fnGetDv(175322,@CaseID) /*Daytime Phone*/
		
		IF @TempValue<>''
		BEGIN
		
			SELECT @PermanentValue = c.DayTimeTelephoneNumber FROM Customers c WITH (NOLOCK) WHERE c.CustomerID =@Customerid
		
			UPDATE Customers SET DayTimeTelephoneNumber=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175322, @PermanentValue, @CustomerID /*Daytime Phone*/
		
		END
				
		-- Mobile phone
			
		SELECT @TempValue = dbo.fnGetDv(175323,@CaseID) /*Mobile Phone*/
		
		IF @TempValue<>''
		BEGIN
						
			SELECT @PermanentValue = c.MobileTelephone FROM Customers c WITH (NOLOCK) WHERE c.CustomerID =@Customerid
			
			UPDATE Customers SET MobileTelephone=@TempValue 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID)
			WHERE CustomerID=@CustomerID
			
			EXEC dbo._C00_SimpleValueIntoField 175323, @PermanentValue, @CustomerID /*Mobile Phone*/
		
		END
	
	

END










GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_TempSwapPHandPetDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_TempSwapPHandPetDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_TempSwapPHandPetDetails] TO [sp_executeall]
GO
