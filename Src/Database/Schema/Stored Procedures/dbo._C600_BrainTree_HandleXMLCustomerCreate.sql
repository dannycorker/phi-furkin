SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-02-02
-- Description:	Handle response from create customer
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BrainTree_HandleXMLCustomerCreate]
	@MatterID INT,
	@ClientPersonnelID INT,
	@XMLPayload VARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO dbo.BrainTreeCustomerResponse ([MatterID], [CreateCustomerXML], [WhoCreated], [WhenCreated])
	VALUES (@MatterID, @XMLPayload, @ClientPersonnelID, dbo.fn_GetDate_Local())

END
GO
GRANT EXECUTE ON  [dbo].[_C600_BrainTree_HandleXMLCustomerCreate] TO [sp_executeall]
GO
