SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the WorkflowGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroup_Get_List]

AS


				
				SELECT
					[WorkflowGroupID],
					[Name],
					[Description],
					[ManagerID],
					[ClientID],
					[Enabled],
					[SortOrder],
					[AssignedLeadsOnly],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[WorkflowGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroup_Get_List] TO [sp_executeall]
GO
