SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DetailFieldPages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_Get_List]

AS


				
				SELECT
					[DetailFieldPageID],
					[ClientID],
					[LeadOrMatter],
					[LeadTypeID],
					[PageName],
					[PageCaption],
					[PageOrder],
					[Enabled],
					[ResourceList],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[IsShared]
				FROM
					[dbo].[DetailFieldPages] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Get_List] TO [sp_executeall]
GO
