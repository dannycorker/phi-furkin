SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomerDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerDetailValues_Insert]
(

	@CustomerDetailValueID int    OUTPUT,

	@ClientID int   ,

	@CustomerID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[CustomerDetailValues]
					(
					[ClientID]
					,[CustomerID]
					,[DetailFieldID]
					,[DetailValue]
					,[ErrorMsg]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@CustomerID
					,@DetailFieldID
					,@DetailValue
					,@ErrorMsg
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@SourceID
					)
				-- Get the identity value
				SET @CustomerDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues_Insert] TO [sp_executeall]
GO
