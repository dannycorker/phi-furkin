SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-11-2015
-- Description:	Social Feed Get Event By Name
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__GetEventByName]
	@SocialFeedID INT,
	@LeadTypeID INT,
	@EventName VARCHAR(50)		
AS
BEGIN
	
	
	SET NOCOUNT ON;

	SELECT ClientID, EventTypeID, EventTypeName, LeadTypeID FROM EventType WITH (NOLOCK) 
	WHERE EventSubtypeID = 29 AND 
	     (SocialFeedID=@SocialFeedID OR SocialFeedID=0) AND 
	      EventTypeName LIKE '%' + @EventName + '%' AND
          LeadTypeID = @LeadTypeID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetEventByName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__GetEventByName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetEventByName] TO [sp_executeall]
GO
