SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSeriesSetting table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting_GetByChartSeriesID]
(

	@ChartSeriesID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChartSeriesSettingID],
					[ChartSeriesID],
					[SettingName],
					[SettingValue],
					[ClientID]
				FROM
					[dbo].[ChartSeriesSetting] WITH (NOLOCK) 
				WHERE
					[ChartSeriesID] = @ChartSeriesID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_GetByChartSeriesID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting_GetByChartSeriesID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_GetByChartSeriesID] TO [sp_executeall]
GO
