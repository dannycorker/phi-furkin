SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-02-02
-- Description:	Handle Payment Response Adapted from the one inc/windcave version.
-- ACE 2020-0-15 - Masked card - last 4 digits only.
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Braintree_HandleXMLResponse]
	@MatterID INT,
	@ClientPersonnelID INT,
	@BrainTreePaymentMethodCreateID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @AccountID INT,
			@LeadEventID INT,
			@NewMatterID INT,
			@ValueInt INT,
			@PurchasedProductID INT,
			@PolMatterID INT,
			@AuthoriryToUseAccount INT,
			@ObjectID INT,
			@ClientID INT = [dbo].[fnGetPrimaryClientID](),
			@WhoCreated INT = 58552 /*Aquarium Automation*/,
			@PALeadEventID INT,
			@PACaseID INT,
			@Now DATETIME = dbo.fn_GetDate_Local(), 
			@SessionXML XML,
			@MaskedCardNumber VARCHAR(20),
			@ExpiryDate DATE,
			@ExpiryMonth VARCHAR(2),
			@ExpiryYear VARCHAR(2),
			@CardType VARCHAR(20), /*Visa etc*/
			@CardToken VARCHAR(100),
			@CardholderName VARCHAR(200)

	DECLARE	@AccountUpdates TABLE ( PurchasedProductID INT, PolMatterID INT, NewColMatterID INT, Done INT )

	SELECT TOP 1 @SessionXML = r.ResponseXML
	FROM BrainTreePaymentMethodCreate r WITH (NOLOCK)
	WHERE r.BrainTreePaymentMethodCreateID = @BrainTreePaymentMethodCreateID

	SELECT @ExpiryMonth = @SessionXML.value('(/Root/Target/ExpirationMonth)[1]', 'VARCHAR(2)'),
			@ExpiryYear = RIGHT(@SessionXML.value('(/Root/Target/ExpirationYear)[1]', 'VARCHAR(4)'), 2),
			@MaskedCardNumber = RIGHT(@SessionXML.value('(/Root/Target/MaskedNumber)[1]', 'VARCHAR(20)'), 4),
			@CardType = @SessionXML.value('(/Root/Target/CardType)[1]', 'VARCHAR(20)'),
			@CardToken = @SessionXML.value('(/Root/Target/Token)[1]', 'VARCHAR(20)'),
			@CardholderName = @SessionXML.value('(/Root/Target/CardholderName)[1]', 'VARCHAR(20)')

	/*GPR 2020-09-10 moved from _C600_SAE and trimmed for PPET-125*/
	SELECT TOP(1) @LeadEventID = le.LeadEventID 
	FROM LeadEvent le WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
	AND m.MatterID = @MatterID
	ORDER BY le.WhenCreated DESC

	EXEC @NewMatterID = dbo._C600_CreateACollectionsCase @LeadEventID
			
	SELECT @AccountID = accid.ValueInt 
	FROM MatterDetailValues accid WITH (NOLOCK) 
	WHERE accid.MatterID = @NewMatterID 
	AND accid.DetailFieldID = 176973
		
	UPDATE LeadEvent 
	SET Comments = 'Billing system payment account changed to ' + CAST(@AccountID AS VARCHAR) + '.'
	WHERE LeadEventID = @LeadEventID

	INSERT INTO @AccountUpdates (PurchasedProductID, PolMatterID, NewColMatterID, Done)
	SELECT ppid.ValueInt, ppid.MatterID, @NewMatterID, 0 
	FROM LeadTypeRelationship ltr WITH (NOLOCK) 
	INNER JOIN MatterDetailValues ppid WITH (NOLOCK) ON ltr.FromMatterID = ppid.MatterID AND ppid.DetailFieldID = 177074
	WHERE ltr.ToMatterID = @MatterID 
	AND ltr.ToLeadTypeID = 1493
	
	-- do the update and relink the matters
	WHILE EXISTS ( SELECT * FROM @AccountUpdates WHERE Done = 0 )
	BEGIN
		
		SELECT TOP 1 @NewMatterID = NewColMatterID, @PurchasedProductID = PurchasedProductID, @PolMatterID = PolMatterID 
		FROM @AccountUpdates 
		WHERE Done=0

		-- relink matterID
		UPDATE LeadTypeRelationship
		SET ToMatterID = @NewMatterID
		WHERE FromMatterID = @PolMatterID 
		AND ToLeadTypeID=1493

		/*#430 Copy over the authority to use account..*/
		SELECT @AuthoriryToUseAccount = dbo.fnGetSimpleDvAsInt(179922,@MatterID)
		EXEC dbo._C00_SimpleValueIntoField 179904,@AuthoriryToUseAccount, @PolMatterID, @WhoCreated

		-- set up third party fields
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 1492, 105, 4526)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AccountID, @PolMatterID -- New Account Number
			
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 1492, 105, 4413)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PurchasedProductID, @PolMatterID -- purchased product id
			
		-- if this is a collections lead we'll need to pass a PA LeadEventID because that's where the third party fields live
		SELECT TOP 1 @PALeadEventID = ca.LatestInProcessLeadEventID, @PACaseID = ca.CaseID 
		FROM Cases ca WITH (NOLOCK) 
		INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID = m.CaseID
		WHERE m.MatterID = @PolMatterID
			
		EXEC dbo.ChangePaymentAccount_CreateFromThirdPartyFields @ClientID, @PACaseID, @PALeadEventID
			
		EXEC _C600_Collections_RefreshDocumentationFields @NewMatterID
			
		UPDATE @AccountUpdates 
		SET Done = 1 
		WHERE PurchasedProductID = @PurchasedProductID

		SELECT @AccountID = a.AccountID 
		FROM Account a WITH (NOLOCK) 
		WHERE a.ObjectID = @NewMatterID

		SELECT @ExpiryDate = dbo.fnBillingGetExpiryFromYearAndMonth(@ExpiryYear, @ExpiryMonth)

		UPDATE Account 
		SET  AccountHolderName = @CardholderName,
				FriendlyName = '',
				AccountNumber = @MaskedCardNumber, 
				MaskedCardNumber = @MaskedCardNumber,
				AccountTypeID = 2,
				CardToken = @CardToken, 
				ExpiryDate= @ExpiryDate,
				ExpiryMonth = @ExpiryMonth,
				ExpiryYear = @ExpiryYear,
				CardType = @CardType,

				WhenModified = @Now,
				WhoModified = @ClientPersonnelID
		WHERE AccountID = @AccountID	 
			
		EXEC _C600_UpdateLetterTextField 175462, 'PDCHANGEACCOUNT', @NewMatterID 

		EXEC _C00_SimpleValueIntoField  180262, @PolMatterID, @matterID, @WhoCreated 
		EXEC _C00_SimpleValueIntoField  180262, @PolMatterID, @NewMatterID, @WhoCreated 	

	END

END
GO
