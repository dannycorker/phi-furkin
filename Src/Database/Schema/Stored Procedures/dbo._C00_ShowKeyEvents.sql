SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-03-16
-- Description:	Return data for Management Dashboard C00
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ShowKeyEvents]
	@ClientID INT 
AS
BEGIN

	SET NOCOUNT ON;

	IF @ClientID = 361 
	SELECT @ClientID = 304

	/*Turnover by month*/
	SELECT CONVERT(VARCHAR,DATEPART(YYYY,le.WhenCreated)) + '-' + CASE WHEN DATEPART(MM,le.WhenCreated) < 10 THEN '0' ELSE '' END + CONVERT(VARCHAR,DATEPART(MM,le.WhenCreated)) AS [Event Date], 
		SUM(CASE WHEN le.EventTypeID = 138559 THEN 1 ELSE 0 END) AS [Start], 
		SUM(CASE WHEN le.EventTypeID = 138560 THEN 1 ELSE 0 END) AS [End], 
		SUM(CASE WHEN le.EventTypeID = 138580 THEN 1 ELSE 0 END) AS [Missed], 
		SUM(CASE WHEN le.EventTypeID = 138569 THEN 1 ELSE 0 END) AS [No Agent]
	FROM LeadEvent le WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID
	INNER JOIN EventType et with (nolock) ON et.EventTypeID = le.EventTypeID
	WHERE le.ClientID = @ClientID and l.LeadTypeID = 1395
	AND le.EventDeleted = 0 AND et.InProcess = 1
	AND le.EventTypeID IN (138559,138560,138580,138569)
	GROUP BY CONVERT(VARCHAR,DATEPART(YYYY,le.WhenCreated)), CONVERT(VARCHAR,DATEPART(MM,le.WhenCreated)),CASE WHEN DATEPART(MM,le.WhenCreated) < 10 THEN '0' ELSE '' END
	ORDER BY CONVERT(VARCHAR,DATEPART(YYYY,le.WhenCreated)), CONVERT(VARCHAR,DATEPART(MM,le.WhenCreated))
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ShowKeyEvents] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ShowKeyEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ShowKeyEvents] TO [sp_executeall]
GO
