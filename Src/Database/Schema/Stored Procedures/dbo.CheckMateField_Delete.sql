SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CheckMateField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateField_Delete]
(

	@CheckMateFieldID int   
)
AS


				DELETE FROM [dbo].[CheckMateField] WITH (ROWLOCK) 
				WHERE
					[CheckMateFieldID] = @CheckMateFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Delete] TO [sp_executeall]
GO
