SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeSql table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeSqlID],
					[ClientID],
					[EventTypeID],
					[PostUpdateSql],
					[IsNativeSql]
				FROM
					[dbo].[EventTypeSql] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_GetByClientID] TO [sp_executeall]
GO
