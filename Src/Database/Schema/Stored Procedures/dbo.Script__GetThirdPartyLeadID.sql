SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-10-2015
-- Description:	Get thirdparty lead ID for customer
-- =============================================
CREATE PROCEDURE [dbo].[Script__GetThirdPartyLeadID]
	@ClientID INT,
	@CustomerID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ThirdPartyLeadID_DetailFieldID INT
	
	SELECT @ThirdPartyLeadID_DetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4200 AND LeadTypeID=0 -- Customer level field

    IF @ThirdPartyLeadID_DetailFieldID>0 --ThirdPartyLeadID has been mapped
    BEGIN
		
		-- This should return a single record since thirdPartyLeadID is unique to each customer
		SELECT cdv.DetailValue ThirdPartyLeadID FROM CustomerDetailValues cdv WITH (NOLOCK) 		
		WHERE cdv.DetailFieldID = @ThirdPartyLeadID_DetailFieldID AND
		      cdv.ClientID = @ClientID AND cdv.CustomerID=@CustomerID
		
    END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetThirdPartyLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetThirdPartyLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetThirdPartyLeadID] TO [sp_executeall]
GO
