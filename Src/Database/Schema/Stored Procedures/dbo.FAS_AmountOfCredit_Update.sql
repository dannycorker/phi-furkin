SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FAS_AmountOfCredit table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_Update]
(

	@FasAmountOfCreditID int   ,

	@FasCheckerDataID int   ,

	@Description varchar (512)  ,

	@CostAmount decimal (19, 4)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FAS_AmountOfCredit]
				SET
					[FasCheckerDataID] = @FasCheckerDataID
					,[Description] = @Description
					,[CostAmount] = @CostAmount
				WHERE
[FasAmountOfCreditID] = @FasAmountOfCreditID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Update] TO [sp_executeall]
GO
