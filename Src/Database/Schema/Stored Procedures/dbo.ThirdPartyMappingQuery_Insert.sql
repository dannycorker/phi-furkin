SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyMappingQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_Insert]
(

	@ThirdPartyMappingQueryID int    OUTPUT,

	@ClientID int   ,

	@ThirdPartySystemID int   ,

	@ThirdPartyFieldGroupID int   ,

	@QueryID int   ,

	@Method int   ,

	@ActionURI varchar (2000)  ,

	@SchemaName varchar (250)  ,

	@BaseIndex varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ThirdPartyMappingQuery]
					(
					[ClientID]
					,[ThirdPartySystemID]
					,[ThirdPartyFieldGroupID]
					,[QueryID]
					,[Method]
					,[ActionURI]
					,[SchemaName]
					,[BaseIndex]
					)
				VALUES
					(
					@ClientID
					,@ThirdPartySystemID
					,@ThirdPartyFieldGroupID
					,@QueryID
					,@Method
					,@ActionURI
					,@SchemaName
					,@BaseIndex
					)
				-- Get the identity value
				SET @ThirdPartyMappingQueryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Insert] TO [sp_executeall]
GO
