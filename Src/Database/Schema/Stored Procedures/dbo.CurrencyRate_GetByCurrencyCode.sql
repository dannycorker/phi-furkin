SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CurrencyRate table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_GetByCurrencyCode]
(

	@CurrencyCode char (3)  
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CurrencyRateID],
					[CurrencyId],
					[CurrencyCode],
					[FromGBPRate],
					[ToGBPRate],
					[ConversionDate],
					[IsLatest]
				FROM
					[dbo].[CurrencyRate] WITH (NOLOCK) 
				WHERE
					[CurrencyCode] = @CurrencyCode
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_GetByCurrencyCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_GetByCurrencyCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_GetByCurrencyCode] TO [sp_executeall]
GO
