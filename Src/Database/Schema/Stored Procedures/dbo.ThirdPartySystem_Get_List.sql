SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ThirdPartySystem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystem_Get_List]

AS


				
				SELECT
					[ThirdPartySystemId],
					[SystemName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartySystem] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Get_List] TO [sp_executeall]
GO
