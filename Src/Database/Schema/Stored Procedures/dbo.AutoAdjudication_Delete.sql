SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AutoAdjudication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_Delete]
(

	@AutoAdjudicationID int   
)
AS


				DELETE FROM [dbo].[AutoAdjudication] WITH (ROWLOCK) 
				WHERE
					[AutoAdjudicationID] = @AutoAdjudicationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Delete] TO [sp_executeall]
GO
