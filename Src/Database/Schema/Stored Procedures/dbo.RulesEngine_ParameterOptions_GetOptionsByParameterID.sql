SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-30
-- Description:	Returns all options for a specific parameter
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterOptions_GetOptionsByParameterID]
(
	@ClientID INT,
	@ParameterID INT,
	@ParameterOptionID INT = NULL,
	@Filter VARCHAR(250) = NULL
)


AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @ParameterTypeID INT,
			@DetailFieldID INT
	SELECT	@ParameterTypeID = p.DataTypeID,
			@DetailFieldID =	CASE 
									WHEN p.DataTypeID = 6 THEN p.Value
									ELSE 0
								END
	FROM dbo.RulesEngine_RuleParameters p WITH (NOLOCK) 
	WHERE p.RuleParameterID = @ParameterID
	
	;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		WHERE DetailFieldID = @DetailFieldID
		AND df.ClientID = @ClientID
	)
	
	SELECT	TOP 500
		o.ParameterOptionID, o.OperatorID, op.Name AS Operator, o.RuleParameterID, o.Val1, o.Val2,
		o.WhenCreated, cpCreate.UserName AS WhoCreated, o.WhenModified, cpMod.UserName AS WhoModified,
		CASE
			WHEN @ParameterTypeID = 6 AND ll.LookupListItemID IS NOT NULL THEN
					REPLACE(op.OperatorText, '{0}', ll.ItemValue)
			ELSE	REPLACE(REPLACE(op.OperatorText, '{0}', o.Val1), '{1}', ISNULL(o.Val2, ''))
		END AS OperatorText
	FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON o.OperatorID = op.OperatorID
	INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON o.WhoCreated = cpCreate.ClientPersonnelID
	INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON o.WhoModified = cpMod.ClientPersonnelID
	LEFT JOIN LookupList ll ON ll.LookupListItemID =	CASE WHEN @ParameterTypeID = 6 THEN o.Val1 ELSE NULL END
	WHERE o.ClientID = @ClientID
	AND o.RuleParameterID = @ParameterID
	AND (@ParameterOptionID IS NULL OR o.ParameterOptionID = @ParameterOptionID)
	AND 
	(
		(ISNULL(o.Val1,'') + ISNULL(o.Val2,'') LIKE ISNULL(@Filter,'')+'%')
		OR
		(ll.ItemValue LIKE ISNULL(@Filter,'')+'%')
	)
	ORDER BY o.OptionOrder
	
	
	SELECT 'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord
	UNION ALL
	SELECT s.Name AS Title, '#/ruleset/' + CAST(r.RuleSetID AS VARCHAR) + '/rules' AS Path, 2 AS Ord
	FROM dbo.RulesEngine_RuleSets s WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleSetID = s.RuleSetID
	INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON r.RuleID = p.RuleID
	WHERE p.RuleParameterID = @ParameterID
	UNION ALL
	SELECT r.Name AS Title, '#/rule/' + CAST(r.RuleID AS VARCHAR) AS Path, 3 AS Ord
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON r.RuleID = p.RuleID
	WHERE p.RuleParameterID = @ParameterID
	UNION ALL
	SELECT p.Name AS Title, '#/parameter/' + CAST(@ParameterID AS VARCHAR) + '/options' AS Path, 4 AS Ord
	FROM dbo.RulesEngine_RuleParameters p WITH (NOLOCK) 
	WHERE p.RuleParameterID = @ParameterID
	ORDER BY Ord
	
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_GetOptionsByParameterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterOptions_GetOptionsByParameterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_GetOptionsByParameterID] TO [sp_executeall]
GO
