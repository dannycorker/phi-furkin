SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2018-04-10
-- Description:	Runs the premium calculations for quotes, monthly and renewals with evaluated xml
-- 2018-12-2018 AHOD Removed Line 81 following Code review with SB for Perf Testing
-- 2019-10-11 GPR removed redundent code blocks (previously commented out and not used in C60X build) for LPC-35
-- 2020-01-13 CPS for JIRA LPC-356	| Replace hard-coded ClientID with function
-- 2020-09-11 GPR for JIRA PPET-400 | Return TaxLevel1 and TaxLevel2
-- GPR 2020-10-11 Added 'Reduction' for SDPRU-111
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_CalculatePremium_Evaluated]
(
	@CaseID INT = NULL, -- NULL for quotes
	@StartDate DATE = NULL, -- for renewals and quotes
	@TermsDate DATE = NULL, -- for renewals and quotes
	@SchemeID INT = NULL, -- for renewals and quotes
	@PremiumDate DATE = NULL, -- MUST be passed in for monthly premium calc.  NULL for renewals and quotes
	@QuoteLogContextID	INT = NULL,
	@EvaluatedXml XML = NULL,
	@XmlRequest XML = NULL,
	@PetXml XML = NULL,
	@PetID INT = NULL,
	@PolicySchemeID INT = NULL,
	@PolicyMatterID INT = NULL,
	@PetNumber INT = NULL, --  MUST be passed in for monthly premium calc and renewals. NULL for quotes 
	@MarketingCode VARCHAR(100) = NULL --  May be NULL for monthly premium calc or quotes 
)
AS
BEGIN

	DECLARE @Data TABLE
	(
		TermsDate DATE,
		SchemeID INT,
		PolicyMatterID INT,
		RuleSetID INT,
		AnnualPremium MONEY,
		Discount MONEY,
		PremiumLessDiscount MONEY,
		FirstMonthly MONEY,
		RecurringMonthly MONEY,
		Net MONEY,
		Commission MONEY,
		GrossNet MONEY,
		DiscountGrossNet MONEY,
		PAFIfBeforeIPT MONEY,
		PAFBeforeIPTGrossNet MONEY,
		IPT MONEY,
		IPTGrossNet MONEY,
		PAFIfAfterIPT MONEY,
		GrossGross MONEY,
		PremiumCalculationID INT,
		TaxLevel1 MONEY,
		TaxLevel2 MONEY,
		Reduction MONEY,
		DiscountRuleOutput DECIMAL(18,2)
	)
	
	DECLARE  @ClientID			INT = dbo.fnGetPrimaryClientID()
			,@MonthsFree		INT
			,@TempXML			XML
			,@XmlOutput			XML = '<Discounts></Discounts>'
			,@Reduction			MONEY
			,@DiscountRuleOutput	DECIMAL(18,2)
	
	---- Here we could look up collar and cap amounts
	---- Or policy limit, excess, coinsurance values if these are not specified in the ruleset
	---- NB... they could also be passed through the API live voluntary excess
	
	DECLARE @RuleSetID INT
	SELECT @RuleSetID = dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(@PolicyMatterID, @TermsDate)

	DECLARE @Overrides dbo.tvpIntVarcharVarchar
	INSERT @Overrides ( AnyID , AnyValue1, AnyValue2 )
	SELECT 1, 'EvaluateByEffectiveDate', CONVERT(VARCHAR,@TermsDate,121)
	
	PRINT '@RuleSetID = ' + ISNULL(CONVERT(VARCHAR,@RuleSetID),'NULL')

	/*CS 2016-10-13 brought in from C384*/
	DECLARE @GUID INT, @DummyXML XML
	
	Select @DummyXML = '<ClientId>' + CAST(@ClientID AS VARCHAR) + '</ClientId>'	

	DECLARE @AqAutomation INT = dbo.fn_C00_GetAutomationUser(@ClientID)

	INSERT dbo.PremiumCalculationDetail(ClientID, ProductCostBreakdown, WhoCreated, WhenCreated)
	VALUES (@ClientID,@DummyXML,@AqAutomation,dbo.fn_GetDate_Local())
		
	SELECT @GUID = SCOPE_IDENTITY()

	DECLARE @AnnualPremium MONEY,
			@Output XML

	SELECT	@Output = t.c.query('.')
	FROM	@EvaluatedXml.nodes('//RuleSet') t(c)
	WHERE	t.c.value('@Key[1]','VARCHAR(2000)') = CAST(@PetID AS VARCHAR(50))+'_'+CAST(@PolicySchemeID AS VARCHAR(50))+'_'+CAST(@RuleSetID AS VARCHAR(50))
	
	PRINT 'Key = ' + ISNULL(CAST(@PetID AS VARCHAR(50))+'_'+CAST(@PolicySchemeID AS VARCHAR(50))+'_'+CAST(@RuleSetID AS VARCHAR(50)),'NULL')

	SELECT @AnnualPremium = r.c.value('(@Output)[1]', 'MONEY')
    FROM @Output.nodes('/RuleSet/Rule[last()]') AS r(c)

	DECLARE @GrossNet MONEY = 0.0, @GrossNetWithDiscount MONEY = 0.0, @GrossGross MONEY = 0.0,
			@IPT MONEY = 0.0, @IPTGrossNet MONEY = 0.0, @Commission MONEY = 0.0, @Discount MONEY = 0.0, 
			@PAF MONEY = 0.0, @PAFIfBeforeIPT MONEY=0.0, @PAFBeforeIPTGrossNet MONEY=0.0, @PAFIfAfterIPT MONEY=0.0,
			@FirstMonthly MONEY = 0.0, @RecurringMonthly MONEY = 0.0, @GrossGrossPlusDiscount MONEY = 0.0

	-- Strip rule checkpoint values from the output and store
	EXEC _C00_1273_Premium_StoreCalculationValues @ClientID, @Output, @GUID
	
	-- if the rules engine responds with less than zero for the annual premium, a quote should not be given for the product
	IF @AnnualPremium<0 SELECT @AnnualPremium=0

	-- Calculate key premium data
	IF @AnnualPremium > 0
	BEGIN
				
		SELECT	@FirstMonthly = FirstMonthly,
				@RecurringMonthly = RecurringMonthly
		FROM dbo.fn_C00_1273_SplitPremium(@AnnualPremium)

		SELECT @GrossGross=@AnnualPremium,@GrossGrossPlusDiscount=@AnnualPremium
		
		SELECT TOP 1 @IPT=ROUND(CAST(pcdv.RuleInputOutputDelta AS MONEY),2) 
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @GUID 
		AND pcdv.RuleCheckpoint IN ('IPT', 'Tax')

		/*GPR 2020-09-09 for PPET-400*/
		/*GPR 2020-09-11 alteration to use RuleTransformValue for PPET-400*/
		/*GPR 2020-10-01 added MinTaxLevel2 for SDPRU-62*/
		DECLARE @TaxLevel1 DECIMAL(18,4), @TaxLevel2 DECIMAL(18,4)
		DECLARE @MinTaxLevel2 DECIMAL(18,4), @TaxLevel2Override DECIMAL(18,4)

		SELECT TOP 1 @TaxLevel1 = pcdv.RuleTransformValue
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @GUID 
		AND pcdv.RuleCheckpoint = 'TaxLevel1'

		SELECT TOP 1 @TaxLevel2 = pcdv.RuleTransformValue
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @GUID 
		AND pcdv.RuleCheckpoint = 'TaxLevel2'

		/*GPR 2020-11-01 for SDPRU-111*/
		SELECT TOP 1 @Reduction = ISNULL(pcdv.RuleInputOutputDelta,0.00)
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @GUID 
		AND pcdv.RuleCheckpoint = 'Discounts'

		/*GPR 2020-11-09 for SDPRU-111 - DiscountRuleOutput*/
		SELECT TOP 1 @DiscountRuleOutput = ISNULL(pcdv.RuleOutput,1)
		FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @GUID 
		AND pcdv.RuleCheckpoint IN ('Vet or Staff + Internet Partner')

		/*GPR 2020-10-01 SDPRU-62*/
		--SELECT TOP 1 @MinTaxLevel2 = pcdv.RuleTransformValue
		--FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		--WHERE pcdv.PremiumCalculationDetailID = @GUID 
		--AND pcdv.RuleCheckpoint IN ('Get Minimum Municipal Value')

		--SELECT TOP 1 @TaxLevel2Override = (CAST(pcdv.RuleOutput AS DECIMAL(18,2))) - (CAST(pcdv.RuleInput AS DECIMAL(18,2)))
		--FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		--WHERE pcdv.PremiumCalculationDetailID = @GUID 
		--AND pcdv.RuleCheckpoint IN ('Min Municipal Tax')

		/*GPR 2020-09-11*/
		SELECT @TaxLevel1 = ABS((@AnnualPremium * (1 - @TaxLevel1)) - @AnnualPremium)  /*AnnualPremium is without Tax so we must apply it and then subtract the annualpremium to get the value of the tax*/
		SELECT @TaxLevel2 = ABS((@AnnualPremium * (1 - @TaxLevel2)) - @AnnualPremium)  /*AnnualPremium is without Tax so we must apply it and then subtract the annualpremium to get the value of the tax*/

		/*GPR 2020-10-01 SDPRU-62*/
		--IF @TaxLevel2 < @TaxLevel2Override
		--BEGIN
		--	SELECT @TaxLevel2 = @TaxLevel2Override
		--END

		INSERT @Data (TermsDate, SchemeID, PolicyMatterID, RuleSetID, AnnualPremium, Discount, PremiumLessDiscount, FirstMonthly, RecurringMonthly, Net, Commission, GrossNet, DiscountGrossNet, PAFIfBeforeIPT, PAFBeforeIPTGrossNet, IPT, IPTGrossNet, PAFIfAfterIPT, GrossGross, PremiumCalculationID, TaxLevel1, TaxLevel2, Reduction, DiscountRuleOutput)
		VALUES (@TermsDate, @SchemeID, @PolicyMatterID, @RuleSetID, @GrossGrossPlusDiscount, @Discount, @GrossGross, @FirstMonthly, @RecurringMonthly, @AnnualPremium, @Commission, @GrossNet, @GrossNetWithDiscount, @PAFIfBeforeIPT, @PAFBeforeIPTGrossNet, @IPT, @IPTGrossNet, @PAFIfAfterIPT, @GrossGross, @GUID, @TaxLevel1, @TaxLevel2, @Reduction, @DiscountRuleOutput)

	END
	
	SELECT * 
	FROM @Data
	 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremium_Evaluated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CalculatePremium_Evaluated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculatePremium_Evaluated] TO [sp_executeall]
GO
