SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailFieldAlias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_Insert]
(

	@DetailFieldAliasID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@DetailFieldID int   ,

	@DetailFieldAlias varchar (500)  
)
AS


				
				INSERT INTO [dbo].[DetailFieldAlias]
					(
					[ClientID]
					,[LeadTypeID]
					,[DetailFieldID]
					,[DetailFieldAlias]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@DetailFieldID
					,@DetailFieldAlias
					)
				-- Get the identity value
				SET @DetailFieldAliasID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Insert] TO [sp_executeall]
GO
