SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UserGroupMenuOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuOption_Insert]
(

	@UserGroupMenuOptionID int    OUTPUT,

	@ClientPersonnelAdminGroupID int   ,

	@ClientID int   ,

	@MasterPageName varchar (20)  ,

	@PanelItemAction varchar (10)  ,

	@PanelItemName varchar (50)  ,

	@PanelItemCaption varchar (50)  ,

	@PanelItemIcon varchar (50)  ,

	@PanelItemURL varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Comments varchar (250)  ,

	@ItemOrder int   ,

	@ShowInFancyBox bit   
)
AS


				
				INSERT INTO [dbo].[UserGroupMenuOption]
					(
					[ClientPersonnelAdminGroupID]
					,[ClientID]
					,[MasterPageName]
					,[PanelItemAction]
					,[PanelItemName]
					,[PanelItemCaption]
					,[PanelItemIcon]
					,[PanelItemURL]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[Comments]
					,[ItemOrder]
					,[ShowInFancyBox]
					)
				VALUES
					(
					@ClientPersonnelAdminGroupID
					,@ClientID
					,@MasterPageName
					,@PanelItemAction
					,@PanelItemName
					,@PanelItemCaption
					,@PanelItemIcon
					,@PanelItemURL
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@Comments
					,@ItemOrder
					,@ShowInFancyBox
					)
				-- Get the identity value
				SET @UserGroupMenuOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Insert] TO [sp_executeall]
GO
