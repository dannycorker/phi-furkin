SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChangeDocument table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_Insert]
(

	@ChangeDocumentID int    OUTPUT,

	@DocumentData varbinary (MAX)  ,

	@DocumentExtension varchar (10)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   
)
AS


				
				INSERT INTO [dbo].[ChangeDocument]
					(
					[DocumentData]
					,[DocumentExtension]
					,[WhenCreated]
					,[WhoCreated]
					)
				VALUES
					(
					@DocumentData
					,@DocumentExtension
					,@WhenCreated
					,@WhoCreated
					)
				-- Get the identity value
				SET @ChangeDocumentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Insert] TO [sp_executeall]
GO
