SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- ====================================================================================
-- Author:		Jim Green
-- Create date: 2009-10-06
-- Description:	Handle the database migration for a customer moving to a standalone DB.
-- ====================================================================================
CREATE PROCEDURE [dbo].[am] 
	@RequestedStageID INT = NULL,
	@RequestedClientID INT = NULL,
	@RequestedUsername VARCHAR(50) = NULL
AS
BEGIN

	SET NOCOUNT ON;


	DECLARE @LastStageID INT, 
	@NextStageID INT,
	@LastStageName VARCHAR(100), 
	@NextStageName VARCHAR(100),
	@Username VARCHAR(50),
	@ClientID INT
	
	DECLARE @TableID INT, 
	@TableSql VARCHAR(MAX),
	@TableName VARCHAR(1024)
	
	
	/* Check that we are not on the live database! */
	IF DB_NAME() = N'Aquarius'
	BEGIN
		SELECT 'This is the live Aquarius database!  You cannot perform migration on a database with this name!'
		
		/* Go no further than this! */
		RETURN
	END
	ELSE
	BEGIN
		SELECT 'Database is ' + DB_NAME() 
	END
	

	/* Check where we are up to so far */
	SELECT TOP (1) @LastStageID = m.StageID, 
	@Username = m.Username, 
	@LastStageName = ms.StageName, 
	@ClientID = m.ClientID 
	FROM dbo.MigrationControl m 
	LEFT JOIN dbo.MigrationStage ms ON ms.MigrationStageID = m.StageID 
	
	/* Check what is next to do */
	SELECT TOP (1) @NextStageID = ms.MigrationStageID, 
	@NextStageName = ms.StageName 
	FROM dbo.MigrationStage ms 
	WHERE ms.MigrationStageID > COALESCE(@LastStageID, 0) 
	ORDER BY ms.MigrationStageID ASC

	
	/* If @RequestedStageID IS NULL it means that the user is just checking the migration status */
	IF @RequestedStageID IS NULL
	BEGIN
		
		SELECT 'Last stage was [' + COALESCE(@LastStageName, 'NONE') + '] (ID = ' + COALESCE(CONVERT(VARCHAR, @LastStageID), 'NONE') + ')'
		
		SELECT 'Next stage is  [' + COALESCE(@NextStageName, 'NONE') + '] (ID = ' + COALESCE(CONVERT(VARCHAR, @NextStageID), 'NONE') + ')'
		
		SELECT mc.* FROM dbo.MigrationControl mc
		
		SELECT ml.* FROM dbo.MigrationLog ml
		
		/* Go no further than this! */
		RETURN
	END
	ELSE
	BEGIN
	
		/* Migration is underway. Check that the user has asked for a valid next step. */
		IF @RequestedStageID = @NextStageID
		BEGIN 
			
			/* Do all the hard work here in this section */
			
			/* 10 = "Set Client ID" */
			IF @RequestedStageID = 10
			BEGIN
			
				IF @RequestedClientID > 0 AND @RequestedUsername <> ''
				BEGIN

					DELETE dbo.MigrationLog
					
					DELETE dbo.MigrationControl
					
					INSERT INTO dbo.MigrationControl (MigrationControlID, StageID, Username, ClientID)
					VALUES (1, @RequestedStageID, @RequestedUsername, @RequestedClientID)
					
					INSERT INTO dbo.MigrationLog(StageID, LogMessage)
					SELECT @RequestedStageID, 'ClientID set to ' + CONVERT(VARCHAR, @RequestedClientID) + ' at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
					
					UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
					
				END
				ELSE
				BEGIN
				
					SELECT 'Please pass in the RequestedClientID and RequestedUsername values for this stage'
				
				END
				
			END
			
			/* 20 = "Reset All Flags" */
			IF @RequestedStageID = 20
			BEGIN
			
				/* Clear down the disposable Migration tables */
				UPDATE dbo.MigrationTable 
				SET IsClearedDown = 0, IsComplete = 0 
				
				DELETE dbo.MigrationTrigger
				
				DELETE dbo.MigrationForeignKey
				
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Disposable tables cleared down at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
					
			END			
			
			/* 30 = "Check Tables" */
			IF @RequestedStageID = 30
			BEGIN
				
				/* Select out a handy statement for the migration user to execute manually */
				SELECT 'EXEC dbo.MigrationObjectIdentification 
					@Type = ''T'',	/* All, Tables, Functions, Procs */ 
					@AutoPopulate = 0,	/* Create records automatically? */
					@ObjectName = NULL, /* Allows correction of one object at a time */
					@TableType = NULL'
	
				/* Check for any tables that aren't handled in our MigrationTable table */
				SELECT * 
				FROM INFORMATION_SCHEMA.TABLES ist 
				LEFT JOIN dbo.MigrationTable m ON m.TableName = ist.TABLE_NAME 
				WHERE ist.TABLE_TYPE <> 'VIEW' 
				AND m.TableName IS NULL
				ORDER BY ist.TABLE_NAME
				
				IF @@ROWCOUNT > 0
				BEGIN
					SELECT 'Please add all the above unknown tables to dbo.MigrationTable before continuing!'
				END
				ELSE
				BEGIN
					INSERT INTO dbo.MigrationLog(StageID, LogMessage)
					SELECT @RequestedStageID, 'Check Tables completed at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
					
					UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				END
				
			END
			
			/* 
				We could also check for dropped objects too, eg Dropped Tables:
				
				SELECT * 
				FROM dbo.MigrationTable m (NOLOCK)
				LEFT JOIN INFORMATION_SCHEMA.TABLES ist ON m.TableName = ist.TABLE_NAME 
				WHERE ist.TABLE_NAME IS NULL
				ORDER BY m.TableName

				BEGIN TRAN
				DELETE dbo.MigrationTable
				FROM dbo.MigrationTable m 
				LEFT JOIN INFORMATION_SCHEMA.TABLES ist ON m.TableName = ist.TABLE_NAME 
				WHERE ist.TABLE_NAME IS NULL
				ROLLBACK
				COMMIT
			*/
			
			/* 40 = "Check Procs" */
			IF @RequestedStageID = 40
			BEGIN
				/* Check for any procs that aren't handled in our MigrationProc table */
				SELECT * 
				FROM INFORMATION_SCHEMA.ROUTINES isr 
				LEFT JOIN dbo.MigrationProc m ON m.ProcName = isr.ROUTINE_NAME
				WHERE isr.ROUTINE_TYPE = 'PROCEDURE' 
				AND isr.ROUTINE_NAME LIKE '[_]%'
				AND isr.ROUTINE_NAME NOT LIKE '[_]C00%'
				AND m.ProcName IS NULL
								
				IF @@ROWCOUNT > 0
				BEGIN
					SELECT 'Please add all the above unknown procs to dbo.MigrationProc before continuing!'
				END
				ELSE
				BEGIN
					INSERT INTO dbo.MigrationLog(StageID, LogMessage)
					SELECT @RequestedStageID, 'Check Procs completed at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
					
					UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				END
				
			END
			
			/* 50 = "Check Functions" */
			IF @RequestedStageID = 50
			BEGIN
				/* Check for any functions that aren't handled in our MigrationProc table */
				SELECT * 
				FROM INFORMATION_SCHEMA.ROUTINES isr 
				LEFT JOIN dbo.MigrationFunction m ON m.FunctionName = isr.ROUTINE_NAME
				WHERE isr.ROUTINE_TYPE = 'FUNCTION' 
				AND isr.ROUTINE_NAME LIKE 'fn[_]C%'
				AND m.FunctionName IS NULL
								
				IF @@ROWCOUNT > 0
				BEGIN
					SELECT 'Please add all the above unknown functions to dbo.MigrationFunction before continuing!'
				END
				ELSE
				BEGIN
					INSERT INTO dbo.MigrationLog(StageID, LogMessage)
					SELECT @RequestedStageID, 'Check Procs completed at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
					
					UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				END
				
			END

			/* 60 = "Populate Triggers" */
			IF @RequestedStageID = 60
			BEGIN
			
				INSERT INTO [dbo].[MigrationTrigger] 
				(
					[TableName],
					[DisableText],
					[EnableText],
					[IsEnabled]
				)
				SELECT
					OBJECT_NAME(parent_obj),
					'ALTER TABLE [dbo].[' + OBJECT_NAME(parent_obj) + '] DISABLE TRIGGER ALL ',
					'ALTER TABLE [dbo].[' + OBJECT_NAME(parent_obj) + '] ENABLE TRIGGER ALL ',
					1
				FROM sysobjects
				WHERE OBJECTPROPERTY(id, 'IsTrigger') = 1 AND parent_obj IN	(SELECT id FROM sysobjects WHERE xtype = 'U')

				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Triggers populated at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END			
			
			/* 70 = "Populate FKs" */
			IF @RequestedStageID = 70
			BEGIN
			
				DECLARE @fkName VARCHAR(800), @tabName VARCHAR(800), @refName VARCHAR(800)
				DECLARE @isDel INT, @isUpd INT, @fkCol VARCHAR(8000), @refCol VARCHAR(8000)
				DECLARE @pline VARCHAR(8000), @dline VARCHAR(8000) 

				DECLARE fkCursor CURSOR FOR
					SELECT DISTINCT OBJECT_NAME(constid), OBJECT_NAME(fkeyid), 
						OBJECT_NAME(rkeyid), 
						OBJECTPROPERTY ( constid , 'CnstIsDeleteCascade' ),
						OBJECTPROPERTY ( constid , 'CnstIsUpdateCascade' )
					FROM sysforeignkeys k 
					ORDER BY OBJECT_NAME(fkeyid)

				OPEN fkCursor

				FETCH NEXT FROM fkCursor 
					INTO @fkName, @tabName, @refName, @isDel, @isUpd

				WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT @fkCol = NULL
					SELECT @fkCol = ISNULL(@fkCol + ', ','') + '[' + COL_NAME(fkeyid, fkey) + ']'
					FROM sysforeignkeys 
					WHERE OBJECT_NAME(constid) = @fkName 
					ORDER BY keyno

					SELECT @refCol = NULL
					SELECT @refCol = ISNULL(@refCol + ', ','') + '[' + COL_NAME(rkeyid, rkey) + ']'
					FROM sysforeignkeys 
					WHERE OBJECT_NAME(constid) = @fkName 
					ORDER BY keyno

					SELECT @dline = 'ALTER TABLE [dbo].[' + @tabName + '] DROP CONSTRAINT ' + @fkName 
						
					SELECT @pline = 'ALTER TABLE [dbo].[' + @tabName + '] ADD CONSTRAINT [' + @fkName + ']' + ' FOREIGN KEY (' + @fkCol + ') REFERENCES [dbo].[' + @refName + '] (' + @refCol + ')' + CASE @isDel WHEN 1 THEN ' ON DELETE CASCADE' ELSE '' END + CASE @isUpd WHEN 1 THEN ' ON UPDATE CASCADE' ELSE '' END 
					
					--select @pline
					--print @pline
					
					INSERT INTO [dbo].[MigrationForeignKey]
					(
						[TableName],
						[ForeignKeyName],
						[DropText],
						[CreateText],
						[IsEnabled]
					)
					SELECT
						@tabName,
						@fkName,
						@dline,
						@pline,
						1
					
					FETCH NEXT FROM fkCursor 
						INTO @fkName, @tabName, @refName, @isDel, @isUpd
				END

				CLOSE fkCursor
				DEALLOCATE fkCursor

				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Foreign Keys populated at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END			

			/* 80 = "Rowcounts" */
			IF @RequestedStageID = 80
			BEGIN
				
				SELECT TOP (1) @ClientID = m.ClientID FROM dbo.MigrationControl m 
				
				UPDATE dbo.MigrationTable SET TotalRows = (SELECT COUNT(*) FROM dbo.TableDetailValues t (NOLOCK)) WHERE TableName = 'TableDetailValues'
				UPDATE dbo.MigrationTable SET ClientRows = (SELECT COUNT(*) FROM dbo.TableDetailValues t (NOLOCK) WHERE t.ClientID = @ClientID) WHERE TableName = 'TableDetailValues'
				
				UPDATE dbo.MigrationTable SET TotalRows = (SELECT COUNT(*) FROM dbo.LeadDetailValues t (NOLOCK)) WHERE TableName = 'LeadDetailValues'
				UPDATE dbo.MigrationTable SET ClientRows = (SELECT COUNT(*) FROM dbo.LeadDetailValues t (NOLOCK) WHERE t.ClientID = @ClientID) WHERE TableName = 'LeadDetailValues'
				
				UPDATE dbo.MigrationTable SET TotalRows = (SELECT COUNT(*) FROM dbo.MatterDetailValues t (NOLOCK)) WHERE TableName = 'MatterDetailValues'
				UPDATE dbo.MigrationTable SET ClientRows = (SELECT COUNT(*) FROM dbo.MatterDetailValues t (NOLOCK) WHERE t.ClientID = @ClientID) WHERE TableName = 'MatterDetailValues'
				
				UPDATE dbo.MigrationTable SET TotalRows = (SELECT COUNT(*) FROM dbo.DetailValueHistory t (NOLOCK)) WHERE TableName = 'DetailValueHistory'
				UPDATE dbo.MigrationTable SET ClientRows = (SELECT COUNT(*) FROM dbo.DetailValueHistory t (NOLOCK) WHERE t.ClientID = @ClientID) WHERE TableName = 'DetailValueHistory'
				
				UPDATE dbo.MigrationTable SET TotalRows = (SELECT COUNT(*) FROM dbo.LeadDocument t (NOLOCK)) WHERE TableName = 'LeadDocument'
				UPDATE dbo.MigrationTable SET ClientRows = (SELECT COUNT(*) FROM dbo.LeadDocument t (NOLOCK) WHERE t.ClientID = @ClientID) WHERE TableName = 'LeadDocument'
				
				UPDATE dbo.MigrationTable SET TotalRows = (SELECT COUNT(*) FROM dbo.DocumentType t (NOLOCK)) WHERE TableName = 'DocumentType'
				UPDATE dbo.MigrationTable SET ClientRows = (SELECT COUNT(*) FROM dbo.DocumentType t (NOLOCK) WHERE t.ClientID = @ClientID) WHERE TableName = 'DocumentType'
				
				SELECT mt.TableName, mt.ClientRows, mt.TotalRows, CASE mt.TotalRows WHEN 0 THEN 0.00 ELSE (100.00 * CONVERT(DECIMAL(18, 2), mt.ClientRows)) / CONVERT(DECIMAL(18, 2), mt.TotalRows) END AS [Percentage]  
				FROM dbo.MigrationTable mt 
				WHERE mt.TableName IN ('TableDetailValues', 'LeadDetailValues', 'MatterDetailValues', 'DetailValueHistory', 'LeadDocument', 'DocumentType')
				ORDER BY mt.TableName

				SELECT 'If you want to truncate any of these, rather than deleting row by row, then use these statements:'
				UNION
				SELECT 'UPDATE dbo.MigrationTable SET TruncateText = ''TRUNCATE TABLE dbo.TableDetailValues'' WHERE TableName = ''TableDetailValues'' '
				UNION
				SELECT 'UPDATE dbo.MigrationTable SET TruncateText = ''TRUNCATE TABLE dbo.LeadDetailValues'' WHERE TableName = ''LeadDetailValues'' '
				UNION
				SELECT 'UPDATE dbo.MigrationTable SET TruncateText = ''TRUNCATE TABLE dbo.MatterDetailValues'' WHERE TableName = ''MatterDetailValues'' '
				UNION
				SELECT 'UPDATE dbo.MigrationTable SET TruncateText = ''TRUNCATE TABLE dbo.DetailValueHistory'' WHERE TableName = ''DetailValueHistory'' '
				UNION
				SELECT 'UPDATE dbo.MigrationTable SET TruncateText = ''TRUNCATE TABLE dbo.LeadDocument'' WHERE TableName = ''LeadDocument'' '
				UNION
				SELECT 'UPDATE dbo.MigrationTable SET TruncateText = ''TRUNCATE TABLE dbo.DocumentType'' WHERE TableName = ''DocumentType'' '
				
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Rowcounts populated at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			/* 90 = "Freestyle" */
			IF @RequestedStageID = 90
			BEGIN
				
				SELECT 'Freestyle updates complete. Tables changed are as follows:'
				 
				SELECT *  
				FROM dbo.MigrationTable mt 
				WHERE mt.TableName IN ('TableDetailValues', 'LeadDetailValues', 'MatterDetailValues', 'DetailValueHistory', 'LeadDocument', 'DocumentType')
				AND mt.TruncateText IS NOT NULL
				ORDER BY mt.TableName

				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Freestyle complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
				
			/* 100 = "Disable Triggers" */
			IF @RequestedStageID = 100
			BEGIN
				
				DECLARE @TriggerID INT, 
				@TriggerText VARCHAR(MAX)
				
				SELECT TOP (1) @TriggerID = mt.MigrationTriggerID, @TriggerText = mt.DisableText, @TableName = mt.TableName 
				FROM dbo.MigrationTrigger mt 
				WHERE mt.IsEnabled = 1 
				
				WHILE @TriggerID IS NOT NULL
				BEGIN
				
					EXEC(@TriggerText)
					
					INSERT INTO dbo.MigrationLog(StageID, LogMessage)
					SELECT @RequestedStageID, 'Triggers disabled for ' + @TableName
					
					UPDATE dbo.MigrationTrigger 
					SET IsEnabled = 0 
					WHERE MigrationTriggerID = @TriggerID
					
					SET @TriggerID = NULL
					
					SELECT TOP (1) @TriggerID = mt.MigrationTriggerID, @TriggerText = mt.DisableText, @TableName = mt.TableName 
					FROM dbo.MigrationTrigger mt 
					WHERE mt.IsEnabled = 1 
					
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Trigger disables complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
				
			/* 110 = "Drop FKs" */
			IF @RequestedStageID = 110
			BEGIN
				
				DECLARE @DropFKID INT, 
				@DropFKText VARCHAR(MAX),
				@DropFKName VARCHAR(1024)
				
				SELECT TOP (1) @DropFKID = mfk.MigrationForeignKeyID, @DropFKText = mfk.DropText, @DropFKName = mfk.ForeignKeyName 
				FROM dbo.MigrationForeignKey mfk 
				WHERE mfk.IsEnabled = 1 
				
				WHILE @DropFKID IS NOT NULL
				BEGIN
				
					EXEC(@DropFKText)
					
					INSERT INTO dbo.MigrationLog(StageID, LogMessage)
					SELECT @RequestedStageID, 'FK dropped ' + @DropFKName
					
					UPDATE dbo.MigrationForeignKey 
					SET IsEnabled = 0 
					WHERE MigrationForeignKeyID = @DropFKID
					
					SET @DropFKID = NULL
					
					SELECT TOP (1) @DropFKID = mfk.MigrationForeignKeyID, @DropFKText = mfk.DropText, @DropFKName = mfk.ForeignKeyName 
					FROM dbo.MigrationForeignKey mfk 
					WHERE mfk.IsEnabled = 1 
					
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Drop FKs complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END

			/* 120 = "Delete Data By FKs" */
			IF @RequestedStageID = 120
			BEGIN
				
				SET @TableID = -1
				
				SELECT @TableID

				WHILE @TableID IS NOT NULL
				BEGIN
				
				SELECT @TableID

					SELECT TOP (1) @TableID = mt.MigrationTableID, @TableSql = REPLACE(mt.DeleteText, '@ClientID', @ClientID), @TableName = mt.TableName 
					FROM dbo.MigrationTable mt 
					WHERE mt.DeleteText IS NOT NULL
					AND mt.FKToClientIDTableName <> ''
					AND mt.ExecutionOrder IS NOT NULL
					AND mt.TruncateText IS NULL
					AND mt.IsClearedDown = 0
					ORDER BY mt.ExecutionOrder
					
					--Select @TableID

					
					IF @TableID > 0
					BEGIN
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Deleting Table by FK : ' + @TableName + ' ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 

						EXEC(@TableSql)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Table deleted by FK : ' + @TableName + ' ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
						
						UPDATE dbo.MigrationTable 
						SET IsClearedDown = 1, 
						IsComplete = 1
						WHERE MigrationTableID = @TableID
						
						SET @TableID = -1
					END
					ELSE
					BEGIN
						SET @TableID = NULL
					END
							
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Drop FKs complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			/* 130 = "Delete Data By ClientID" */
			IF @RequestedStageID = 130
			BEGIN
					
				SET @TableID = -1
				
				WHILE @TableID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @TableID = mt.MigrationTableID, @TableSql = REPLACE(mt.DeleteText, '@ClientID', @ClientID), @TableName = mt.TableName 
					FROM dbo.MigrationTable mt 
					WHERE mt.DeleteText IS NOT NULL
					AND mt.FKToClientIDTableName = ''
					AND mt.ExecutionOrder IS NOT NULL
					AND mt.TruncateText IS NULL
					AND mt.IsClearedDown = 0
					ORDER BY mt.ExecutionOrder
					
					IF @TableID > 0
					BEGIN
						EXEC(@TableSql)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Table deleted by ClientID : ' + @TableName
						
						--Select @TableSql

						UPDATE dbo.MigrationTable 
						SET IsClearedDown = 1, 
						IsComplete = 1
						WHERE MigrationTableID = @TableID
						
						SET @TableID = -1
					END
					ELSE
					BEGIN
						SET @TableID = NULL
					END
							
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Drop FKs complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			
			/* 140 = "Truncate Tables" */
			IF @RequestedStageID = 140
			BEGIN
					
				SET @TableID = -1
				
				WHILE @TableID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @TableID = mt.MigrationTableID, @TableSql = mt.TruncateText, @TableName = mt.TableName 
					FROM dbo.MigrationTable mt 
					WHERE mt.TruncateText IS NOT NULL
					AND mt.ExecutionOrder IS NOT NULL
					AND mt.IsClearedDown = 0
					ORDER BY mt.ExecutionOrder
					
					IF @TableID > 0
					BEGIN
						EXEC(@TableSql)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Table truncated : ' + @TableName
						
						UPDATE dbo.MigrationTable 
						SET IsClearedDown = 1, 
						IsComplete = CASE WHEN RepopulateText IS NULL THEN 1 ELSE 0 END
						WHERE MigrationTableID = @TableID
						
						SET @TableID = -1
					END
					ELSE
					BEGIN
						SET @TableID = NULL
					END
							
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Truncates complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			
			/* 143 = "Drop Procs By ClientID" */
			IF @RequestedStageID = 143
			BEGIN
					
				SET @TableID = -1
				
				WHILE @TableID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @TableID = mt.MigrationProcID, @TableSql = REPLACE(mt.DropText, '@ClientID', @ClientID), @TableName = mt.ProcName 
					FROM dbo.MigrationProc mt 
					WHERE mt.DropText IS NOT NULL
					AND mt.IsComplete = 0
				
					IF @TableID > 0
					BEGIN
						EXEC(@TableSql)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Proc dropped by ClientID : ' + @TableName
						
						UPDATE dbo.MigrationProc 
						SET IsComplete = 1
						WHERE MigrationProcID = @TableID
						
						SET @TableID = -1
					END
					ELSE
					BEGIN
						SET @TableID = NULL
					END
							
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Drop Procs By ClientID complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END


			/* 146 = "Drop Functions By ClientID" */
			IF @RequestedStageID = 146
			BEGIN
					
				SET @TableID = -1
				
				WHILE @TableID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @TableID = mt.MigrationFunctionID, @TableSql = REPLACE(mt.DropText, '@ClientID', @ClientID), @TableName = mt.FunctionName 
					FROM dbo.MigrationFunction mt 
					WHERE mt.DropText IS NOT NULL
					AND mt.IsComplete = 0
				
					IF @TableID > 0
					BEGIN
						EXEC(@TableSql)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Function dropped by ClientID : ' + @TableName
						
						UPDATE dbo.MigrationFunction 
						SET IsComplete = 1
						WHERE MigrationFunctionID = @TableID
						
						SET @TableID = -1
					END
					ELSE
					BEGIN
						SET @TableID = NULL
					END
							
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Drop Functions By ClientID complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END

			
			/* 150 = "Drop Tables By ClientID" */
			IF @RequestedStageID = 150
			BEGIN
					
				SET @TableID = -1
				
				WHILE @TableID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @TableID = mt.MigrationTableID, @TableSql = REPLACE(mt.DropText, '@ClientID', @ClientID), @TableName = mt.TableName 
					FROM dbo.MigrationTable mt 
					WHERE mt.DropText IS NOT NULL
					AND mt.ExecutionOrder IS NOT NULL
					AND mt.IsClearedDown = 0
					AND mt.NeverDrop = 0
					AND (mt.ClientID IN (0, @ClientID) OR mt.AlwaysDrop = 1)
					ORDER BY mt.ExecutionOrder
					
					IF @TableID > 0
					BEGIN
						EXEC(@TableSql)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Table dropped by ClientID : ' + @TableName
						
						UPDATE dbo.MigrationTable 
						SET IsClearedDown = 1, 
						IsComplete = CASE WHEN RepopulateText IS NULL THEN 1 ELSE 0 END
						WHERE MigrationTableID = @TableID
						
						SET @TableID = -1
					END
					ELSE
					BEGIN
						SET @TableID = NULL
					END
							
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Truncates complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			
			/* 160 = "Repopulate Tables From Other DB" */
			IF @RequestedStageID = 160
			BEGIN
				
				SELECT 'Repopulate tables should by now be complete. This is for tables you chose to truncate in step 90 above.'
				
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Repopulates complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			
			/* 170 = "Create Foreign Keys" */
			IF @RequestedStageID = 170
			BEGIN
				
				DECLARE @CreateFKID INT = -1, 
				@CreateFKText VARCHAR(MAX),
				@CreateFKName VARCHAR(1024)
				
				WHILE @CreateFKID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @CreateFKID = mfk.MigrationForeignKeyID, @CreateFKText = mfk.CreateText, @CreateFKName = mfk.ForeignKeyName 
					FROM dbo.MigrationForeignKey mfk 
					WHERE mfk.IsEnabled = 0 
					
					IF @CreateFKID > 0
					BEGIN
						
						EXEC(@CreateFKText)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'FK created : ' + @CreateFKName
						
						UPDATE dbo.MigrationForeignKey 
						SET IsEnabled = 1 
						WHERE MigrationForeignKeyID = @CreateFKID
						
						SET @CreateFKID = -1
					END
					ELSE
					BEGIN
						SET @CreateFKID = NULL
					END
					
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Create FKs complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			
			/* 180 = "Enable Triggers" */
			IF @RequestedStageID = 180
			BEGIN
				
				DECLARE @EnableTriggerID INT = -1, 
				@EnableTriggerText VARCHAR(MAX),
				@EnableTriggerName VARCHAR(1024)
				
				WHILE @EnableTriggerID IS NOT NULL
				BEGIN
				
					SELECT TOP (1) @EnableTriggerID = mt.MigrationTriggerID, @EnableTriggerText = mt.EnableText, @EnableTriggerName = mt.TableName 
					FROM dbo.MigrationTrigger mt 
					WHERE mt.IsEnabled = 0 
					
					IF @EnableTriggerID > 0
					BEGIN
						
						EXEC(@EnableTriggerText)
						
						INSERT INTO dbo.MigrationLog(StageID, LogMessage)
						SELECT @RequestedStageID, 'Triggers enabled for ' + @EnableTriggerName
						
						UPDATE dbo.MigrationTrigger 
						SET IsEnabled = 1 
						WHERE MigrationTriggerID = @EnableTriggerID
						
						SET @EnableTriggerID = -1
					END
					ELSE
					BEGIN
						SET @EnableTriggerID = NULL
					END
					
				END
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Enable Trigers complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
			
			/* 190 = "Index Housekeeping" */
			IF @RequestedStageID = 190
			BEGIN
				
				EXEC dbo.AquariumIndexHousekeeping 
								
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Index Housekeeping complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END

			/* 200 = "AquariumHousekeeping Edit" */
			IF @RequestedStageID = 200
			BEGIN
				
				SELECT 'Migration is now complete. Edit the AquariumHousekeeping proc to remove calls to specific client procs, then test the app.'
				
				INSERT INTO dbo.MigrationLog(StageID, LogMessage)
				SELECT @RequestedStageID, 'Migration complete at ' + CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 126) 
				
				UPDATE dbo.MigrationControl SET StageID = @RequestedStageID
				
			END
				
			
			
			/* Fetch what is next to do */
			SELECT TOP (1) @LastStageID = m.StageID
			FROM dbo.MigrationControl m 
	
			SELECT TOP (1) @NextStageID = ms.MigrationStageID, 
			@NextStageName = ms.StageName 
			FROM dbo.MigrationStage ms 
			WHERE ms.MigrationStageID > COALESCE(@LastStageID, 0) 
			ORDER BY ms.MigrationStageID ASC
			
			IF @@ROWCOUNT > 0 
			BEGIN
				SELECT 'Next stage is ' + COALESCE(@NextStageName, 'NONE') + ' (ID = ' + COALESCE(CONVERT(VARCHAR, @NextStageID), 'NONE') + ')'
			END
			ELSE
			BEGIN
				SELECT 'All Done!'
			END
			
		END
		ELSE
		BEGIN
			SELECT 'Last stage was [' + COALESCE(@LastStageName, 'NONE') + '] (ID = ' + COALESCE(CONVERT(VARCHAR, @LastStageID), 'NONE') + ')'
			
			SELECT 'Next stage is  [' + COALESCE(@NextStageName, 'NONE') + '] (ID = ' + COALESCE(CONVERT(VARCHAR, @NextStageID), 'NONE') + ')'
			
			SELECT 'Requested stage ' + CONVERT(VARCHAR, @RequestedStageID) + ' is not available'
			
			/* Go no further than this! */
			RETURN
		END
		
	END
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[am] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[am] TO [sp_executeall]
GO
