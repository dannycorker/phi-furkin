SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_Insert]
(

	@ClientStyleID int    OUTPUT,

	@ClientID int   ,

	@StyleRuleID int   ,

	@StyleValue varchar (100)  
)
AS


				
				INSERT INTO [dbo].[ClientStyle]
					(
					[ClientID]
					,[StyleRuleID]
					,[StyleValue]
					)
				VALUES
					(
					@ClientID
					,@StyleRuleID
					,@StyleValue
					)
				-- Get the identity value
				SET @ClientStyleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Insert] TO [sp_executeall]
GO
