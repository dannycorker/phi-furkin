SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ActiveSession table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_Insert]
(

	@SessionID varchar (50)  ,

	@EmailAddress varchar (255)  ,

	@ThirdPartySystemId int   
)
AS


				
				INSERT INTO [dbo].[ActiveSession]
					(
					[SessionID]
					,[EmailAddress]
					,[ThirdPartySystemId]
					)
				VALUES
					(
					@SessionID
					,@EmailAddress
					,@ThirdPartySystemId
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Insert] TO [sp_executeall]
GO
