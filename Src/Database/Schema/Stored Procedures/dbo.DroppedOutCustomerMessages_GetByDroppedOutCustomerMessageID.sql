SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DroppedOutCustomerMessages table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerMessages_GetByDroppedOutCustomerMessageID]
(

	@DroppedOutCustomerMessageID int   
)
AS


				SELECT
					[DroppedOutCustomerMessageID],
					[ClientID],
					[ClientQuestionnaireID],
					[EmailFromAddress],
					[EmailSubject],
					[Email],
					[SmsMessage]
				FROM
					[dbo].[DroppedOutCustomerMessages] WITH (NOLOCK) 
				WHERE
										[DroppedOutCustomerMessageID] = @DroppedOutCustomerMessageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_GetByDroppedOutCustomerMessageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerMessages_GetByDroppedOutCustomerMessageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_GetByDroppedOutCustomerMessageID] TO [sp_executeall]
GO
