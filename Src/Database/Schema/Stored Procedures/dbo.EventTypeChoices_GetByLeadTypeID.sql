SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------
-- Date Created: 05 February 2007

-- Created By:  Chris Dixon
-- Purpose: Select EventType records and their EventChoices for a LeadType
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeChoices_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


SELECT
            let.EventTypeID,
            let.ClientID,
            let.EventTypeName,
            let.EventTypeDescription,
            ec.EventChoiceID,
            ec.Description AS EventChoiceDescription,
            ec.NextEventTypeID,
            let2.EventTypeDescription AS NextEventTypeDescription,
            let.Enabled,
            let.AvailableManually,
            let.StatusAfterEvent,
            let.AquariumEventAfterEvent,
            let.EventSubtypeID, 
            IsNull(es.EventSubtypeName,'') AS EventSubtypeName,
            let.LeadTypeID,
            let.DocumentTypeID,
            -- let.EscalationEventType
			CASE WHEN ec.EscalationEvent = 1 THEN 'Yes' ELSE 'No' END AS IsEscalationEvent
FROM
            EventType let
            LEFT OUTER JOIN EventSubtype es ON let.EventSubtypeID = es.EventSubtypeID
            LEFT OUTER JOIN EventChoice ec ON let.EventTypeID = ec.EventTypeID
            LEFT OUTER JOIN EventType let2 ON let2.EventTypeID = ec.NextEventTypeID
WHERE          
            let.LeadTypeID = @LeadTypeID
ORDER BY 
            let.EventTypeDescription, let2.EventTypeDescription





GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeChoices_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeChoices_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeChoices_GetByLeadTypeID] TO [sp_executeall]
GO
