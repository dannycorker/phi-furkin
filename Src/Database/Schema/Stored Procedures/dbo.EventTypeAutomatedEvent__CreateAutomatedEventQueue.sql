SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-21
-- Description:	Check EventTypeAutomatedEvent when a LeadEvent is applied, adding any AutomatedEventQueue records if required
--				CS 2013-04-10 - blocked this when the case was created after the event (for copied/split cases)
--				AMG 2014-01-27 - Added new column EarliestRunDateTime (that honours EventTypeAutomatedEvent.Delay)
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueue]
	@ClientID int,
	@LeadID int,
	@CaseID int,
	@LeadEventID int,
	@EventTypeID int,
	@WhenCreated datetime,
	@WhoCreated int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CaseCreated date 
	
	SELECT @CaseCreated = c.WhenCreated
	FROM Cases c WITH (NOLOCK) 
	WHERE c.CaseID = @CaseID
	
	INSERT INTO dbo.AutomatedEventQueue (
		ClientID,
		CustomerID,
		LeadID,
		CaseID,
		FromLeadEventID,
		FromEventTypeID,
		WhenCreated,
		WhoCreated,
		AutomatedEventTypeID,
		RunAsUserID,
		ThreadToFollowUp,
		InternalPriority,
		BumpCount,
		ErrorCount,
		/* AMG 2014-01-27 Honour the new EventTypeAutomatedEvent delay column */
		EarliestRunDateTime
	)
	SELECT 
		@ClientID,
		l.CustomerID,
		@LeadID,
		@CaseID,
		@LeadEventID,
		@EventTypeID,
		@WhenCreated,
		@WhoCreated,
		et.AutomatedEventTypeID,
		ISNULL(et.RunAsUserID, @WhoCreated),
		et.ThreadToFollowUp,
		et.InternalPriority,
		0 as BumpCount,
		0 as ErrorCount,
		/* AMG 2014-01-27 Honour the new EventTypeAutomatedEvent delay column */
		DateAdd(s, ISNULL(et.DelaySeconds, 0), dbo.fn_GetDate_Local())
	FROM dbo.EventTypeAutomatedEvent et WITH (NOLOCK) 
	CROSS JOIN dbo.Lead l WITH (NOLOCK) 
	WHERE et.EventTypeID = @EventTypeID 
	AND l.LeadID = @LeadID
	AND @CaseCreated < @WhenCreated /*CS for support ticket #20185*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__CreateAutomatedEventQueue] TO [sp_executeall]
GO
