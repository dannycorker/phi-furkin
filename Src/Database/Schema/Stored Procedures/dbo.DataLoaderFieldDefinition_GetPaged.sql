SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the DataLoaderFieldDefinition table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [DataLoaderFieldDefinitionID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([DataLoaderFieldDefinitionID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [DataLoaderFieldDefinitionID]'
				SET @SQL = @SQL + ' FROM [dbo].[DataLoaderFieldDefinition] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[DataLoaderFieldDefinitionID], O.[ClientID], O.[DataLoaderMapID], O.[DataLoaderObjectTypeID], O.[DataLoaderMapSectionID], O.[DataLoaderObjectFieldID], O.[DetailFieldID], O.[DetailFieldAlias], O.[NamedValue], O.[Keyword], O.[DataLoaderKeywordMatchTypeID], O.[RowRelativeToKeyword], O.[ColRelativeToKeyword], O.[SectionRelativeRow], O.[SectionAbsoluteCol], O.[ValidationRegex], O.[Equation], O.[IsMatchField], O.[DecodeTypeID], O.[DefaultLookupItemID], O.[SourceDataLoaderFieldDefinitionID], O.[Notes], O.[AllowErrors]
				FROM
				    [dbo].[DataLoaderFieldDefinition] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[DataLoaderFieldDefinitionID] = PageIndex.[DataLoaderFieldDefinitionID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[DataLoaderFieldDefinition] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_GetPaged] TO [sp_executeall]
GO
