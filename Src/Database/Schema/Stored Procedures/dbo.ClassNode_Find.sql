SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClassNode table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_Find]
(

	@SearchUsingOR bit   = null ,

	@ClassNodeID int   = null ,

	@ClientID int   = null ,

	@ParentClassNodeID int   = null ,

	@NodeType int   = null ,

	@Name varchar (255)  = null ,

	@ClassDescription varchar (255)  = null ,

	@NodeOrder int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@Deleted bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClassNodeID]
	, [ClientID]
	, [ParentClassNodeID]
	, [NodeType]
	, [Name]
	, [ClassDescription]
	, [NodeOrder]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Deleted]
    FROM
	[dbo].[ClassNode] WITH (NOLOCK) 
    WHERE 
	 ([ClassNodeID] = @ClassNodeID OR @ClassNodeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ParentClassNodeID] = @ParentClassNodeID OR @ParentClassNodeID IS NULL)
	AND ([NodeType] = @NodeType OR @NodeType IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([ClassDescription] = @ClassDescription OR @ClassDescription IS NULL)
	AND ([NodeOrder] = @NodeOrder OR @NodeOrder IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClassNodeID]
	, [ClientID]
	, [ParentClassNodeID]
	, [NodeType]
	, [Name]
	, [ClassDescription]
	, [NodeOrder]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Deleted]
    FROM
	[dbo].[ClassNode] WITH (NOLOCK) 
    WHERE 
	 ([ClassNodeID] = @ClassNodeID AND @ClassNodeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ParentClassNodeID] = @ParentClassNodeID AND @ParentClassNodeID is not null)
	OR ([NodeType] = @NodeType AND @NodeType is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([ClassDescription] = @ClassDescription AND @ClassDescription is not null)
	OR ([NodeOrder] = @NodeOrder AND @NodeOrder is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Find] TO [sp_executeall]
GO
