SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUserOptionType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOptionType_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserOptionTypeID int   = null ,

	@OptionTypeName nchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserOptionTypeID]
	, [OptionTypeName]
    FROM
	[dbo].[PortalUserOptionType] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserOptionTypeID] = @PortalUserOptionTypeID OR @PortalUserOptionTypeID IS NULL)
	AND ([OptionTypeName] = @OptionTypeName OR @OptionTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserOptionTypeID]
	, [OptionTypeName]
    FROM
	[dbo].[PortalUserOptionType] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserOptionTypeID] = @PortalUserOptionTypeID AND @PortalUserOptionTypeID is not null)
	OR ([OptionTypeName] = @OptionTypeName AND @OptionTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOptionType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Find] TO [sp_executeall]
GO
