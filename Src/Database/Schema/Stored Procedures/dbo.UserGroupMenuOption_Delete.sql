SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UserGroupMenuOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuOption_Delete]
(

	@UserGroupMenuOptionID int   
)
AS


				DELETE FROM [dbo].[UserGroupMenuOption] WITH (ROWLOCK) 
				WHERE
					[UserGroupMenuOptionID] = @UserGroupMenuOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Delete] TO [sp_executeall]
GO
