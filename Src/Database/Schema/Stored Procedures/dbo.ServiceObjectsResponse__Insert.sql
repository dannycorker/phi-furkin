SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-02-15
-- Description:	Save response from Service Object
-- =============================================
CREATE PROCEDURE [dbo].[ServiceObjectsResponse__Insert]
	@ClientID INT,
	@ClientPersonnelID INT,
	@MatterID INT,
	@ResponseXML XML
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ServiceObjectsResponseID INT

	/*Insert here*/
	INSERT INTO _C607_ServiceObjectsResponse([MatterID], [ResponseXML], [WhoCreated], [WhenCreated])
	VALUES (@MatterID, @ResponseXML, @ClientPersonnelID, dbo.fn_GetDate_Local())

	SELECT @ServiceObjectsResponseID = SCOPE_IDENTITY()

	SELECT @ServiceObjectsResponseID AS [ServiceObjectID]

END
GO
GRANT EXECUTE ON  [dbo].[ServiceObjectsResponse__Insert] TO [sp_executeall]
GO
