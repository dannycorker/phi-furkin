SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SearchQueryValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_Update]
(

	@SearchQueryValueID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@SearchQueryID int   ,

	@SearchFieldID int   ,

	@SearchFieldOperatorID int   ,

	@FieldValue varchar (2000)  ,

	@FieldValueID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SearchQueryValue]
				SET
					[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[SearchQueryID] = @SearchQueryID
					,[SearchFieldID] = @SearchFieldID
					,[SearchFieldOperatorID] = @SearchFieldOperatorID
					,[FieldValue] = @FieldValue
					,[FieldValueID] = @FieldValueID
				WHERE
[SearchQueryValueID] = @SearchQueryValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Update] TO [sp_executeall]
GO
