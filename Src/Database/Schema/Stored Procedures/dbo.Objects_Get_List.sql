SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Objects table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Objects_Get_List]

AS


				
				SELECT
					[ObjectID],
					[ClientID],
					[SubClientID],
					[ObjectTypeID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Name],
					[Details],
					[SourceID]
				FROM
					[dbo].[Objects] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Objects_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Get_List] TO [sp_executeall]
GO
