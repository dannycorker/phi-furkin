SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFields table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_GetByDetailFieldPageID]
(

	@DetailFieldPageID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailFieldID],
					[ClientID],
					[LeadOrMatter],
					[FieldName],
					[FieldCaption],
					[QuestionTypeID],
					[Required],
					[Lookup],
					[LookupListID],
					[LeadTypeID],
					[Enabled],
					[DetailFieldPageID],
					[FieldOrder],
					[MaintainHistory],
					[EquationText],
					[MasterQuestionID],
					[FieldSize],
					[LinkedDetailFieldID],
					[ValidationCriteriaFieldTypeID],
					[ValidationCriteriaID],
					[MinimumValue],
					[MaximumValue],
					[RegEx],
					[ErrorMessage],
					[ResourceListDetailFieldPageID],
					[TableDetailFieldPageID],
					[DefaultFilter],
					[ColumnEquationText],
					[Editable],
					[Hidden],
					[LastReferenceInteger],
					[ReferenceValueFormatID],
					[Encrypt],
					[ShowCharacters],
					[NumberOfCharactersToShow],
					[TableEditMode],
					[DisplayInTableView],
					[ObjectTypeID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[DetailFieldStyleID],
					[Hyperlink],
					[IsShared]
				FROM
					[dbo].[DetailFields] WITH (NOLOCK) 
				WHERE
					[DetailFieldPageID] = @DetailFieldPageID
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_GetByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_GetByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_GetByDetailFieldPageID] TO [sp_executeall]
GO
