SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-07-15
-- Description:	Sets first value or increments value of next regular payment date
-- Mods
--  2016-01-13 DCM added GetNextAnnualCollectionDate
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_IncrementNextRegularPaymentDate] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50022615
	
	DECLARE 
			@AqUserID INT,
			@CollectionsCaseID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@CollectionsMatterID INT,
			@DateDue DATE,
			@FromDate DATE,
			@MonthYear DATE,
			@NextRegularDate DATE,
			@PaymentDay INT,
			@PaymentTypeID INT,
			@TableRowID INT
				
	DECLARE @Data TABLE
	(
		FromDate DATE,
		Type INT
	)
	
	SELECT @AqUserID=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@ClientID,53,'CFG|AqAutomationCPID',0)
			
	-- find appropriate collections matter for the matter passed in
	IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @CollectionsMatterID=@MatterID
		
	END
	ELSE
	BEGIN
	
		SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493

	END

	SELECT @CollectionsCaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@CollectionsMatterID

	-- only increment if monthly collections and not already incremented for another policy 
	-- but set if monthly or annual and blank (not set) or annual and the flag says we can
	IF ( dbo.fnGetDvAsInt(170145,@CollectionsCaseID) = 69943 AND dbo.fnGetDvAsInt(175600,@CollectionsCaseID) = 5144 )
		OR
		( dbo.fnGetDvAsInt(170145,@CollectionsCaseID) IN (69944,69943) AND ISNULL(dbo.fngetdv(175599,@CollectionsCaseID),'')='' )
		OR 
		(dbo.fnGetDvAsInt (170145,@CollectionsCaseID)=69944 AND dbo.fnGetDvAsInt (175602,@CollectionsCaseID)=5144)
	BEGIN

		SELECT @PaymentDay=dbo.fnGetDvAsInt(170184,@CollectionsCaseID)

		-- if next regular payment field is empty, set it
		IF ISNULL(dbo.fngetdv(175599,@CollectionsCaseID),'')=''
		BEGIN

			-- this should only occur if the premiums table has no entries (for this term)
			-- get the from date for the next ( should be first period )
			-- Set the next due date to be during the period starting at the from date using the payment day field
		
			SELECT @PaymentTypeID = 69897 -- Default to a monthly payment but this might be reset to a first payment in the function if this failed
			
			INSERT @Data (FromDate, Type)
			SELECT FromDate, Type
			FROM dbo.fn_C600_GetNextPremiumFromDate(@MatterID, @PaymentTypeID)

			SELECT @FromDate = FromDate, @PaymentTypeID = Type
			FROM @Data
			
			IF @PaymentDay<DATEPART(DAY,@FromDate) -- next payment day is in next month
				SELECT @FromDate=DATEADD(MONTH,1,@FromDate)	

		END
		ELSE -- field has a value, increment it
		BEGIN
		
			IF dbo.fnGetDvAsInt (170145,@CollectionsCaseID)=69944
			BEGIN
				DELETE FROM @Data
				INSERT INTO @Data
				EXEC dbo._C600_GetNextAnnualCollectionDate @MatterID
				SELECT @FromDate = FromDate FROM @Data			
			END
			ELSE
			BEGIN
				SELECT @FromDate=DATEADD(MONTH,1,dbo.fnGetDvAsDate(175599,@CollectionsCaseID))
			END
			
		END
		
		SELECT @NextRegularDate = CAST (
									CAST(DATEPART(YYYY,@FromDate) AS VARCHAR) + '-' +
									CAST(DATEPART(MM,@FromDate) AS VARCHAR) + '-' +
									CASE WHEN DATEPART(DAY,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FromDate)+1,0))) < @PaymentDay 
									THEN
										RIGHT('0' + CAST(DATEPART(DAY,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FromDate)+1,0))) AS VARCHAR),2)
									ELSE
										RIGHT('0' + CAST(@PaymentDay AS VARCHAR),2)
									END
								  AS DATE )

		EXEC dbo._C00_SimpleValueIntoField 175599, @NextRegularDate, @CollectionsMatterID, @AqUserID		
		EXEC dbo._C00_SimpleValueIntoField 175600, 5145, @CollectionsMatterID, @AqUserID		

	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_IncrementNextRegularPaymentDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_IncrementNextRegularPaymentDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_IncrementNextRegularPaymentDate] TO [sp_executeall]
GO
