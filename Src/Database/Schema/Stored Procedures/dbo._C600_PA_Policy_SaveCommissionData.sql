SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2018-07-31
-- Description:	Save the commission data for this Policy (PM163, C600)
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_SaveCommissionData]
(
	 @MatterID			INT
	,@LeadEventID		INT = NULL
)
AS
BEGIN

	DECLARE  @ErrorMessage		VARCHAR(2000) = ''
			,@TableRowID		INT
			,@PolicyNumber		VARCHAR(100)
			,@Affinity			VARCHAR(20)
			,@Introducer		VARCHAR(2000)
			,@StartDate			VARCHAR(2000)
			,@CommissionPaid	MONEY
			,@PremiumCalculationID INT
			
	IF 1492 /*Policy Admin*/ =
	(
	SELECT l.LeadTypeID
	FROM Matter m WITH ( NOLOCK ) 
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
	WHERE m.MatterID = @MatterID
	)
	BEGIN
	
		SELECT @PremiumCalculationID = dbo.fnGetSimpleDv(175711,@MatterID)
		SELECT @PolicyNumber	= dbo.fnGetSimpleDv(170050,@MatterID)	/*Policy Number*/
		SELECT @Affinity		= rldv.DetailValue FROM MatterDetailValues mdv WITH ( NOLOCK ) INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) ON rldv.ResourceListID = mdv.ValueInt WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = 170034 AND rldv.DetailFieldID = 144314
		SELECT @Introducer		= lli.ItemValue FROM MatterDetailValues mdv WITH ( NOLOCK ) INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) ON rldv.ResourceListID = mdv.ValueInt INNER JOIN LookupListItems lli WITH ( NOLOCK ) ON lli.LookupListItemID = rldv.ValueInt WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = 180112 AND rldv.DetailFieldID = 180105
		SELECT @StartDate		= dbo.fnGetSimpleDv(170036,@MatterID)	/*StartDate*/
		SELECT @CommissionPaid	= pcdv.RuleInputOutputDelta FROM PremiumCalculationDetailValues pcdv WITH (NOLOCK) WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationID AND pcdv.RuleCheckpoint = 'Commission' /*Commission from PremiumCalculation*/
		
		IF @LeadEventID > 0
		BEGIN
			EXEC @TableRowID = _C00_CreateTableRowFromLeadEvent @LeadEventID,180121 /*Commission*/, 1, 1
		END
		ELSE
		BEGIN
			/*Create the new row.  Most objectIDs will be NULL*/
			INSERT TableRows ( ClientID, MatterID, DetailFieldID, DetailFieldPageID )
			SELECT TOP 1 m.ClientID, @MatterID, 180121, 19128 /*Commission*/
			FROM Matter m WITH ( NOLOCK )
			WHERE m.MatterID = @MatterID
			
			/*Pick up the new ID*/
			SELECT @TableRowID = SCOPE_IDENTITY()
			
			/*Fill in all of the TableDetailValues, and @DenyEdit/@DenyDelete*/
			EXEC _C00_CompleteTableRow @TableRowID, @MatterID, 1, 1
		END
		
		IF @TableRowID > 0
		BEGIN
		
			SELECT @Introducer = ISNULL(@Introducer,'N/A')
			EXEC _C00_SimpleValueIntoField 180122, @PolicyNumber, @TableRowID -- Policy Number
			EXEC _C00_SimpleValueIntoField 180124, @Affinity, @TableRowID -- Affinity
			EXEC _C00_SimpleValueIntoField 180125, @Introducer, @TableRowID -- Introducer
			EXEC _C00_SimpleValueIntoField 180126, @StartDate, @TableRowID -- Policy Start Date
			EXEC _C00_SimpleValueIntoField 180129, @CommissionPaid, @TableRowID -- Commission Paid
	
		END
		ELSE
		BEGIN
			SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  Failed to create TableRow for MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + ', LeadEventID ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL') + '</font>'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END		
	END
	ELSE
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No Policy Admin match found for MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
	RETURN @TableRowID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveCommissionData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_SaveCommissionData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveCommissionData] TO [sp_executeall]
GO
