SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the FolderType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FolderType_Update]
(

	@FolderTypeID int   ,

	@OriginalFolderTypeID int   ,

	@FolderTypeName varchar (50)  ,

	@FolderTypeDescription varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[FolderType]
				SET
					[FolderTypeID] = @FolderTypeID
					,[FolderTypeName] = @FolderTypeName
					,[FolderTypeDescription] = @FolderTypeDescription
				WHERE
[FolderTypeID] = @OriginalFolderTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FolderType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Update] TO [sp_executeall]
GO
