SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-09-29
-- Description:	List all AutomatedTaskHistory for a task
-- =============================================
CREATE PROCEDURE [dbo].[ath] 
	@TaskID int = NULL, 
	@ClientID int = NULL,
	@MaxRows int = 500 
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @TaskID is null and @ClientID is not null
	BEGIN

		Select TOP (@MaxRows) ath.*
		FROM AutomatedTaskHistory ath WITH (NOLOCK) 
		WHERE ath.ClientID = @ClientID
		ORDER BY ath.TaskHistoryID desc

	END
	ELSE
	BEGIN

		Select TOP (@MaxRows) ath.*
		FROM AutomatedTaskHistory ath WITH (NOLOCK) 
		WHERE ath.TaskID = @TaskID
		ORDER BY ath.TaskHistoryID desc

	END

END






GO
GRANT VIEW DEFINITION ON  [dbo].[ath] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ath] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ath] TO [sp_executeall]
GO
