SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReferenceValueDelimiters table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDelimiters_Insert]
(

	@ReferenceValueDelimiterID int    OUTPUT,

	@Delimiter char (1)  
)
AS


				
				INSERT INTO [dbo].[ReferenceValueDelimiters]
					(
					[Delimiter]
					)
				VALUES
					(
					@Delimiter
					)
				-- Get the identity value
				SET @ReferenceValueDelimiterID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDelimiters_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Insert] TO [sp_executeall]
GO
