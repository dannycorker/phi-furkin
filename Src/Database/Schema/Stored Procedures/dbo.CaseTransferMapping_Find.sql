SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CaseTransferMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@CaseTransferMappingID int   = null ,

	@ClientRelationshipID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@MatterID int   = null ,

	@NewClientID int   = null ,

	@NewCustomerID int   = null ,

	@NewLeadID int   = null ,

	@NewCaseID int   = null ,

	@NewMatterID int   = null ,

	@CaseTransferStatusID int   = null ,

	@NewLeadTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CaseTransferMappingID]
	, [ClientRelationshipID]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [MatterID]
	, [NewClientID]
	, [NewCustomerID]
	, [NewLeadID]
	, [NewCaseID]
	, [NewMatterID]
	, [CaseTransferStatusID]
	, [NewLeadTypeID]
    FROM
	[dbo].[CaseTransferMapping] WITH (NOLOCK) 
    WHERE 
	 ([CaseTransferMappingID] = @CaseTransferMappingID OR @CaseTransferMappingID IS NULL)
	AND ([ClientRelationshipID] = @ClientRelationshipID OR @ClientRelationshipID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([NewClientID] = @NewClientID OR @NewClientID IS NULL)
	AND ([NewCustomerID] = @NewCustomerID OR @NewCustomerID IS NULL)
	AND ([NewLeadID] = @NewLeadID OR @NewLeadID IS NULL)
	AND ([NewCaseID] = @NewCaseID OR @NewCaseID IS NULL)
	AND ([NewMatterID] = @NewMatterID OR @NewMatterID IS NULL)
	AND ([CaseTransferStatusID] = @CaseTransferStatusID OR @CaseTransferStatusID IS NULL)
	AND ([NewLeadTypeID] = @NewLeadTypeID OR @NewLeadTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CaseTransferMappingID]
	, [ClientRelationshipID]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [MatterID]
	, [NewClientID]
	, [NewCustomerID]
	, [NewLeadID]
	, [NewCaseID]
	, [NewMatterID]
	, [CaseTransferStatusID]
	, [NewLeadTypeID]
    FROM
	[dbo].[CaseTransferMapping] WITH (NOLOCK) 
    WHERE 
	 ([CaseTransferMappingID] = @CaseTransferMappingID AND @CaseTransferMappingID is not null)
	OR ([ClientRelationshipID] = @ClientRelationshipID AND @ClientRelationshipID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([NewClientID] = @NewClientID AND @NewClientID is not null)
	OR ([NewCustomerID] = @NewCustomerID AND @NewCustomerID is not null)
	OR ([NewLeadID] = @NewLeadID AND @NewLeadID is not null)
	OR ([NewCaseID] = @NewCaseID AND @NewCaseID is not null)
	OR ([NewMatterID] = @NewMatterID AND @NewMatterID is not null)
	OR ([CaseTransferStatusID] = @CaseTransferStatusID AND @CaseTransferStatusID is not null)
	OR ([NewLeadTypeID] = @NewLeadTypeID AND @NewLeadTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Find] TO [sp_executeall]
GO
