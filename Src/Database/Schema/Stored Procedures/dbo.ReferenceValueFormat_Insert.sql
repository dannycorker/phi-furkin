SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReferenceValueFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueFormat_Insert]
(

	@ReferenceValueFormatID int    OUTPUT,

	@ClientID int   ,

	@ReferenceValueName varchar (50)  ,

	@ReferenceValueDescription varchar (255)  ,

	@IntegerSectionSize int   ,

	@IntegerStartPoint int   ,

	@DateFormat varchar (50)  ,

	@YearFormat varchar (50)  ,

	@DelimiterID int   ,

	@Section1TypeID int   ,

	@Section1DetailFieldID int   ,

	@Section2TypeID int   ,

	@Section2DetailFieldID int   ,

	@Section3TypeID int   ,

	@Section3DetailFieldID int   ,

	@Section4TypeID int   ,

	@Section4DetailFieldID int   ,

	@Section5TypeID int   ,

	@Section5DetailFieldID int   ,

	@LeftPadIntegerSection bit   ,

	@IncludeBlankSections bit   ,

	@EnforceUniqueness bit   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[ReferenceValueFormat]
					(
					[ClientID]
					,[ReferenceValueName]
					,[ReferenceValueDescription]
					,[IntegerSectionSize]
					,[IntegerStartPoint]
					,[DateFormat]
					,[YearFormat]
					,[DelimiterID]
					,[Section1TypeID]
					,[Section1DetailFieldID]
					,[Section2TypeID]
					,[Section2DetailFieldID]
					,[Section3TypeID]
					,[Section3DetailFieldID]
					,[Section4TypeID]
					,[Section4DetailFieldID]
					,[Section5TypeID]
					,[Section5DetailFieldID]
					,[LeftPadIntegerSection]
					,[IncludeBlankSections]
					,[EnforceUniqueness]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@ReferenceValueName
					,@ReferenceValueDescription
					,@IntegerSectionSize
					,@IntegerStartPoint
					,@DateFormat
					,@YearFormat
					,@DelimiterID
					,@Section1TypeID
					,@Section1DetailFieldID
					,@Section2TypeID
					,@Section2DetailFieldID
					,@Section3TypeID
					,@Section3DetailFieldID
					,@Section4TypeID
					,@Section4DetailFieldID
					,@Section5TypeID
					,@Section5DetailFieldID
					,@LeftPadIntegerSection
					,@IncludeBlankSections
					,@EnforceUniqueness
					,@SourceID
					)
				-- Get the identity value
				SET @ReferenceValueFormatID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueFormat_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Insert] TO [sp_executeall]
GO
