SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Returns all calculations for a specific client 
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_Calculations_Get]
(
	@ClientID INT,
	@CalculationID INT = NULL
)


AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	c.CalculationID, c.Name, c.WhenCreated, cpCreate.UserName AS WhoCreated, c.WhenModified, cpMod.UserName AS WhoModified, c.Description
	FROM dbo.CalcsEngine_Calculations c WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON c.WhoCreated = cpCreate.ClientPersonnelID
	INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON c.WhoModified = cpMod.ClientPersonnelID
	WHERE c.ClientID = @ClientID
	AND (@CalculationID IS NULL OR c.CalculationID = @CalculationID)
	
	SELECT 'Calculation Engine' AS Title, '#/calculations' AS Path
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Calculations_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_Calculations_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Calculations_Get] TO [sp_executeall]
GO
