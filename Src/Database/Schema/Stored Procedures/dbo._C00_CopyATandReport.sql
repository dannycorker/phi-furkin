SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-11-24
-- Description:	Copy AT and Report
-- JWG 2011-09-01 Option to pass in an existing report to use for the new batch job
-- DCM 2014-07-16 Set AutomatedTask.SourceID
-- DCM 2014-08-01 Added argument to pass through 'use SourceID' to CopyReport 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyATandReport]
	@FromAutomatedTaskID INT, 
	@DestinationFolderID INT = NULL,
	@BatchJobPreFix VARCHAR(20) = '',
	@BatchJobReplaceText VARCHAR(10) = '',
	@EmailFrom VARCHAR(2000) = '',
	@EmailTo VARCHAR(2000) = '',
	@FromLeadTypeID INT = NULL,
	@ToLeadTypeID INT = NULL,
	@RunAsUserID INT = NULL,
	@NewQueryID INT = NULL,
	@UseSourceID INT = 1
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @RepeatTimeQuantity INT,
			@RepeatTimeUnitsID INT,
			@RunAtHour VARCHAR(2),
			@NewClientID INT,
			@QueryName VARCHAR(250),
			@NextRundateTime DATETIME,
			@OldQueryID INT,
			@OldClientID INT,
			@FollowupEvent VARCHAR(200),
			@ThreadFollowup VARCHAR(200),
			@LeadEventToAdd VARCHAR(200),
			@WorkFlowGroupID VARCHAR(100),
			@WorkType VARCHAR(100),
			@WorkflowImportant VARCHAR(100),
			@WorkflowPriority VARCHAR(100),
			@AutomatedTaskID INT,
			@RunAtMinute VARCHAR(2),
			@NewWorkFlowGroupID INT,
			@WorkFlowGroupName VARCHAR(100),
			@WorkFlowGroupEnabled INT,
			@SendResults VARCHAR(20),
			@CustomerEmailBCCColumn VARCHAR(2000),
			@CustomerEmailCCColumn VARCHAR(2000),
			@CustomerEmailToColumn VARCHAR(2000),
			@CustomerEmailFromColumn VARCHAR(2000)

	SELECT	@RunAtHour = RunAtHour, 
			@RunAtMinute = RunAtMinute, 
			@RepeatTimeUnitsID = RepeatTimeUnitsID, 
			@RepeatTimeQuantity = RepeatTimeQuantity,
			@NextRundateTime = NextRunDateTime, 
			@OldClientID = at.ClientID,
			@OldQueryID = atp.ParamValue,
			@QueryName = LEFT(REPLACE(at.Taskname, @BatchJobReplaceText, @BatchJobPreFix),250)
	FROM AutomatedTask AT (NOLOCK)
	INNER JOIN AutomatedTaskParam atp (NOLOCK) ON atp.TaskID = at.TaskID AND atp.ParamName = 'SQL_QUERY'
	WHERE at.TaskID = @FromAutomatedTaskID
	
	IF @NewQueryID > 0
	BEGIN
		SELECT @NewClientID = sq.ClientID, 
		@DestinationFolderID = sq.FolderID 
		FROM dbo.SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @NewQueryID 
	END
	ELSE
	BEGIN
		SELECT @NewClientID = ClientID, @RunAsUserID = ISNULL(@RunAsUserID, WhoCreated )
		FROM Folder WITH (NOLOCK) 
		WHERE FolderID = @DestinationFolderID
	END
	
	SELECT @FollowupEvent = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'FOLLOWUP_EVENT'

	SELECT @ThreadFollowup = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'THREAD_FOLLOWUP'

	SELECT @LeadEventToAdd = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'LEADEVENT_TO_ADD'

	SELECT @WorkFlowGroupID = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'WORKFLOW_GROUP_ID'
	
	IF @WorkFlowGroupID > 0 AND (@OldClientID<>@NewClientID)
	BEGIN

		SELECT TOP 1 @NewWorkFlowGroupID=wgnew.WorkflowGroupID,
		@WorkFlowGroupName=wgold.Name,
		@WorkFlowGroupEnabled=wgold.Enabled 
		FROM WorkflowGroup wgold WITH (NOLOCK)
		LEFT JOIN WorkflowGroup wgnew WITH (NOLOCK) ON wgold.Name=wgnew.Name AND wgnew.ClientID=@NewClientID 
		WHERE wgold.WorkflowGroupID=@WorkFlowGroupID
		
		IF @NewWorkFlowGroupID IS NULL
		BEGIN
			INSERT INTO WorkflowGroup (Name,Description,ManagerID,ClientID,Enabled)
			VALUES (@WorkFlowGroupName,@WorkFlowGroupName,@RunAsUserID,@NewClientID,@WorkFlowGroupEnabled)

	 		SELECT @WorkFlowGroupID = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN

			SELECT @WorkFlowGroupID = @NewWorkFlowGroupID

		END
		
	END
	
	SELECT @WorkType = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'WORK_TYPE'

	SELECT @WorkflowImportant = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'WORKFLOW_IMPORTANT'

	SELECT @WorkflowPriority = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'WORKFLOW_PRIORITY'

	IF @EmailFrom = ''
	BEGIN
	
		SELECT @EmailFrom = ParamValue
		FROM AutomatedTaskParam WITH (NOLOCK) 
		WHERE TaskID = @FromAutomatedTaskID
		AND ParamName = 'EMAIL_FROM'

	END

	IF @EmailTo = ''
	BEGIN
	
		SELECT @EmailTo = ParamValue
		FROM AutomatedTaskParam WITH (NOLOCK) 
		WHERE TaskID = @FromAutomatedTaskID
		AND ParamName = 'EMAIL_TO'

	END
		
	IF @EmailTo IS NULL OR @EmailTo = '' 
		SELECT @SendResults = 'No'
	ELSE
		SELECT @SendResults = 'NotEmpty'

	SELECT @CustomerEmailBCCColumn = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'CUSTOMER_EMAIL_BCC_COLUMN'

	SELECT @CustomerEmailCCColumn = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'CUSTOMER_EMAIL_CC_COLUMN'

	SELECT @CustomerEmailFromColumn = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'CUSTOMER_EMAIL_FROM_COLUMN'

	SELECT @CustomerEmailToColumn = ParamValue
	FROM AutomatedTaskParam WITH (NOLOCK) 
	WHERE TaskID = @FromAutomatedTaskID
	AND ParamName = 'CUSTOMER_EMAIL_TO_COLUMN'

	IF @FromLeadTypeID > 0 AND @ToLeadTypeID > 0
	BEGIN
		SELECT @LeadEventToAdd = et2.EventTypeID
		FROM EventType et1 WITH (NOLOCK) 
		INNER JOIN EventType et2 WITH (NOLOCK)  ON et2.EventTypeName = et1.EventTypeName AND et2.LeadTypeID = @ToLeadTypeID AND et2.InProcess = et1.InProcess
		WHERE et1.EventTypeID = @LeadEventToAdd
	END
				
	SELECT @OldClientID = ClientID
	FROM SqlQuery  WITH (NOLOCK) 
	WHERE QueryID = @OldQueryID
	
	IF @RunAsUserID IS NULL
	BEGIN
		SELECT @RunAsUserID = ParamValue
		FROM AutomatedTaskParam WITH (NOLOCK) 
		WHERE TaskID = @FromAutomatedTaskID
		AND ParamName = 'RUN_AS_USERID'
	END
	
	IF @NewQueryID IS NULL
	BEGIN
		IF @QueryName NOT LIKE @BatchJobPreFix + '%' 
		BEGIN
			SET @QueryName = @BatchJobPreFix + @QueryName
		END
		
		-- replace any characters in Queryname not allowed in a filename with a hyphen
		
		SELECT @QueryName =
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@QueryName,
				'\','-'),
				'/','-'),
				':','-'),
				'*','-'),
				'?','-'),
				'"','-'),
				'<','-'),
				'>','-'),
				'|','-')

		EXEC @NewQueryID = dbo._C00_CopyReport @OldQueryID, @DestinationFolderID, @FromLeadTypeID, @ToLeadTypeID, @BatchJobReplaceText, @BatchJobPreFix, @UseSourceID
	END
	
	INSERT INTO AutomatedTask (ClientID, Description, Enabled, NextRunDateTime, RepeatTimeQuantity, RepeatTimeUnitsID, RunAtHour, RunAtMinute, Taskname, WorkflowTask, SourceID)
	VALUES (@NewClientID, LEFT(@QueryName,50), 1, @NextRundateTime, @RepeatTimeQuantity, @RepeatTimeUnitsID, @RunAtHour, @RunAtMinute,LEFT(@QueryName,50), 0, @FromAutomatedTaskID)

	SELECT @AutomatedTaskID = SCOPE_IDENTITY()

	/*SELECT @NewQueryID, @RunAsUserID, @QueryName, @EmailTo, @EmailFrom, @SendResults*/

	INSERT INTO AutomatedTaskParam (TaskID, ClientID, ParamName, ParamValue)
	VALUES (@AutomatedTaskID, @NewClientID, 'SQL_QUERY', CONVERT(VARCHAR,@NewQueryID)),
		(@AutomatedTaskID, @NewClientID, 'RUN_AS_USERID', CONVERT(VARCHAR,@RunAsUserID)),
		 (@AutomatedTaskID, @NewClientID, 'FILE_PREFIX', @QueryName),
		 (@AutomatedTaskID, @NewClientID, 'ALERT_ON_ERROR_EMAIL', 'batch.errors@aquarium-software.com,' + @EmailTo),
		 (@AutomatedTaskID, @NewClientID, 'MESSAGE_HEADER', @QueryName),
		 (@AutomatedTaskID, @NewClientID, 'EMAIL_TO', @EmailTo),
		 (@AutomatedTaskID, @NewClientID, 'EMAIL_FROM', @EmailFrom),
		 (@AutomatedTaskID, @NewClientID, 'EMAIL_BCC', ''),
		 (@AutomatedTaskID, @NewClientID, 'EMAIL_CC', ''),
		 (@AutomatedTaskID, @NewClientID, 'OUTPUT_COLUMNS', ''),
		 (@AutomatedTaskID, @NewClientID, 'OUTPUT_HEADER', ''),
		 (@AutomatedTaskID, @NewClientID, 'EMAIL_SIGNOFF', ''),
		 (@AutomatedTaskID, @NewClientID, 'EMAIL_SALUTATION', ''),
		 (@AutomatedTaskID, @NewClientID, 'ALREADY_RUNNING', '0'),
		 (@AutomatedTaskID, @NewClientID, 'SEND_RESULTS', @SendResults),
		 (@AutomatedTaskID, @NewClientID, 'FOLLOWUP_EVENT', ISNULL(@FollowupEvent, '0')),
		 (@AutomatedTaskID, @NewClientID, 'THREAD_FOLLOWUP', ISNULL(@ThreadFollowup, '')),
		 (@AutomatedTaskID, @NewClientID, 'LEADEVENT_TO_ADD', ISNULL(@LeadEventToAdd, '')),
		 (@AutomatedTaskID, @NewClientID, 'WORKFLOW_GROUP_ID', ISNULL(@WorkFlowGroupID, '')),
		 (@AutomatedTaskID, @NewClientID, 'WORK_TYPE', ISNULL(@WorkType, '')),
		 (@AutomatedTaskID, @NewClientID, 'WORKFLOW_IMPORTANT', ISNULL(@WorkflowImportant, '')),
		 (@AutomatedTaskID, @NewClientID, 'WORKFLOW_PRIORITY', ISNULL(@WorkflowPriority, '')),
		 (@AutomatedTaskID, @NewClientID, 'CUSTOMER_EMAIL_BCC_COLUMN', ISNULL(@CustomerEmailBCCColumn, '')),
		 (@AutomatedTaskID, @NewClientID, 'CUSTOMER_EMAIL_CC_COLUMN', ISNULL(@CustomerEmailCCColumn, '')),
		 (@AutomatedTaskID, @NewClientID, 'CUSTOMER_EMAIL_FROM_COLUMN', ISNULL(@CustomerEmailFromColumn, '')),
		 (@AutomatedTaskID, @NewClientID, 'CUSTOMER_EMAIL_TO_COLUMN', ISNULL(@CustomerEmailToColumn, ''))

	RETURN @AutomatedTaskID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyATandReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyATandReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyATandReport] TO [sp_executeall]
GO
