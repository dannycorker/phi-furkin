SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the DetailFields table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [DetailFieldID] int, [ClientID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([DetailFieldID], [ClientID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [DetailFieldID], [ClientID]'
				SET @SQL = @SQL + ' FROM [dbo].[DetailFields] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[DetailFieldID], O.[ClientID], O.[LeadOrMatter], O.[FieldName], O.[FieldCaption], O.[QuestionTypeID], O.[Required], O.[Lookup], O.[LookupListID], O.[LeadTypeID], O.[Enabled], O.[DetailFieldPageID], O.[FieldOrder], O.[MaintainHistory], O.[EquationText], O.[MasterQuestionID], O.[FieldSize], O.[LinkedDetailFieldID], O.[ValidationCriteriaFieldTypeID], O.[ValidationCriteriaID], O.[MinimumValue], O.[MaximumValue], O.[RegEx], O.[ErrorMessage], O.[ResourceListDetailFieldPageID], O.[TableDetailFieldPageID], O.[DefaultFilter], O.[ColumnEquationText], O.[Editable], O.[Hidden], O.[LastReferenceInteger], O.[ReferenceValueFormatID], O.[Encrypt], O.[ShowCharacters], O.[NumberOfCharactersToShow], O.[TableEditMode], O.[DisplayInTableView], O.[ObjectTypeID], O.[SourceID], O.[WhoCreated], O.[WhenCreated], O.[WhoModified], O.[WhenModified], O.[DetailFieldStyleID], O.[Hyperlink], O.[IsShared]
				FROM
				    [dbo].[DetailFields] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[DetailFieldID] = PageIndex.[DetailFieldID]
					AND O.[ClientID] = PageIndex.[ClientID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[DetailFields] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_GetPaged] TO [sp_executeall]
GO
