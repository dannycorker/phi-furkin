SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-11-30
-- Description:	SP to copy Report
-- Modified: dcm 01/08/2014 added option to use sourceid instead of name in creating ID replacement map
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyReport]
@OldQueryID int,
@DestinationFolderID int,
@FromLeadTypeID int,
@ToLeadTypeID int,
@BatchJobReplaceText varchar(20),
@BatchJobPreFix varchar(20),
@UseSourceID INT = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



    -- Insert statements for procedure here

	declare @OldClientID int,
			@NewClientID int,
			@QueryName varchar(250),
			@RunAsUserID int,
			@EditingID int,
			@NewQueryID int,
			@TableRowID int,
			@TestID int,
			@IsEditable int,
			@QueryText varchar(max)

	Select @QueryName = LEFT(replace(SqlQuery.QueryTitle, @BatchJobReplaceText, @BatchJobPreFix),250), @OldClientID = ClientID, @IsEditable = IsEditable, @QueryText = QueryText
	From SqlQuery WITH (NOLOCK) 
	Where QueryID = @OldQueryID

	If @QueryName NOT LIKE @BatchJobPreFix + '%' SET @QueryName = @BatchJobPreFix + ' ' + @QueryName

	Select @NewClientID = ClientID, @RunAsUserID = WhoCreated 
	From Folder WITH (NOLOCK) 
	Where FolderID = @DestinationFolderID
			
	INSERT INTO [dbo].[SqlQueryEditing]([ClientID],[SqlQueryID],[WhoIsEditing],[EditStartedAt],[QueryText],[QueryTitle],[LeadTypeID],[FolderID],[ParentQueryID])
	VALUES(@NewClientID,@OldQueryID,@RunAsUserID,dbo.fn_GetDate_Local(),@QueryName,@QueryName,@ToLeadTypeID,@DestinationFolderID,@OldQueryID)

	Select @EditingID = SCOPE_IDENTITY()
		
	exec dbo.CopySqlQueryForEditing @OldQueryID, @EditingID

	Update SqlQueryEditingColumns
	Set ClientID = @NewClientID
	Where SqlQueryEditingID = @EditingID

	declare @FieldsandEvents table (TableRowID int IDENTITY(1,1), FromFieldID int, ToFieldID int, FieldOrEvent varchar(1))

	insert into @FieldsandEvents (FromFieldID, ToFieldID, FieldOrEvent)
	Select @FromLeadTypeID, @ToLeadTypeID, 'T'

	insert into @FieldsandEvents (FromFieldID, ToFieldID, FieldOrEvent)
	Select et.EventTypeID, et1.EventTypeID, 'E'
	From EventType et WITH (NOLOCK) 
	Inner Join EventType et1 WITH (NOLOCK) on ( ( et1.EventTypeName = et.EventTypeName AND @UseSourceID=0) OR (et1.SourceID = et.EventTypeID AND @UseSourceID = 1) )
		and et1.ClientID = @NewClientID and et1.InProcess = et.InProcess
	--INNER JOIN dbo.LeadType lt1 WITH (NOLOCK) ON lt1.LeadTypeID = et.LeadTypeID 
	--INNER JOIN LeadType lt2 WITH (NOLOCK) ON lt2.LeadTypeID = et1.LeadTypeID and lt2.LeadTypeName = lt1.LeadTypeName
	Where ((et.LeadTypeID = @FromLeadTypeID and @FromLeadTypeID <> 0) OR (et.ClientID = @OldClientID and @FromLeadTypeID = 0))
	AND ((et1.LeadTypeID = @ToLeadTypeID AND @ToLeadTypeID <> 0) OR @ToLeadTypeID = 0)
	
	insert into @FieldsandEvents (FromFieldID, ToFieldID, FieldOrEvent)
	Select df.DetailFieldID, df1.DetailFieldID, 'F'
	From DetailFields df WITH (NOLOCK) 
	Inner Join DetailFields df1 WITH (NOLOCK) on ( ( df1.FieldName = df.FieldName AND @UseSourceID=0) OR (df1.SourceID=df.DetailFieldID  AND @UseSourceID=1) )
		and df1.LeadOrMatter = df.LeadOrMatter
	--INNER JOIN dbo.LeadType lt1 WITH (NOLOCK) ON lt1.LeadTypeID = df.LeadTypeID 
	--INNER JOIN LeadType lt2 WITH (NOLOCK) ON lt2.LeadTypeID = df1.LeadTypeID and lt2.LeadTypeName = lt1.LeadTypeName
	Where ((df.LeadTypeID = @FromLeadTypeID and @FromLeadTypeID <> 0) OR (df.ClientID = @OldClientID and @FromLeadTypeID = 0))
	AND ((df1.LeadTypeID = @ToLeadTypeID and @ToLeadTypeID <> 0) OR (df1.ClientID = @NewClientID))

	insert into @FieldsandEvents (FromFieldID, ToFieldID, FieldOrEvent)
	Select df.DetailFieldID, df1.DetailFieldID, 'F'
	From DetailFields df WITH (NOLOCK) 
	Inner Join DetailFields df1 WITH (NOLOCK) on ( ( df1.FieldName = df.FieldName AND @UseSourceID=0) OR (df1.SourceID=df.DetailFieldID  AND @UseSourceID=1) ) 
		and df1.ClientID = @NewClientID and df1.LeadOrMatter IN (4,6,8)
	Where df.ClientID = @OldClientID
	AND df.LeadOrMatter IN (4,6,8)

	insert into @FieldsandEvents (FromFieldID, ToFieldID, FieldOrEvent)
	Select luli.LookupListItemID, luli1.LookupListItemID, 'L'
	From LookupListItems luli WITH (NOLOCK) 
	Inner Join LookupList lu WITH (NOLOCK)  on lu.LookupListID = luli.LookupListID
	Inner Join LookupListItems luli1 WITH (NOLOCK) on ( ( luli.ItemValue = luli1.ItemValue AND @UseSourceID=0) OR (luli1.SourceID=luli.LookupListItemID AND @UseSourceID=1 ) )
		and luli1.ClientID = @NewClientID 
	Inner Join LookupList lu1 WITH (NOLOCK)  on lu1.LookupListID = luli1.LookupListID 
		and ( ( lu1.LookupListName = lu.LookupListName AND @UseSourceID=0 ) OR (lu1.SourceID=lu.LookupListID AND @UseSourceID=1 ) )
	Where luli.ClientID = @OldClientID

	insert into @FieldsandEvents (FromFieldID, ToFieldID, FieldOrEvent)
	Select ls.StatusID, ls1.StatusID, 'S'
	From LeadStatus ls WITH (NOLOCK) 
	Inner Join LeadStatus ls1 on ( ( ls.StatusName = ls1.StatusName AND @UseSourceID=0 ) OR ( ls1.SourceID=ls.SourceID AND @UseSourceID=1 ) ) 
		and ls1.ClientID = @NewClientID 
	Where ls.ClientID = @OldClientID

	update SqlQueryEditingTable
	Set ClientID = @NewClientID
	Where SqlQueryEditingID = @EditingID

	Select @TableRowID = 0, @TestID = 0
	
	Select top 1 @TableRowID = TableRowID
	From SqlQueryEditingCriteria  WITH (NOLOCK) 
	INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and SqlQueryEditingCriteria.CriteriaText like '%EventTypeID%'
	and fe.FieldOrEvent = 'E'
	Order By FromFieldID Asc

	While @TableRowID > @TestID 
	Begin

		Update SqlQueryEditingCriteria 
		Set ClientID = @NewClientID, CriteriaText = REPLACE(REPLACE(CriteriaText, FromFieldID, ToFieldID), @OldClientID, @NewClientID), Criteria1 = ToFieldID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%' and fe.TableRowID = @TableRowID
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and SqlQueryEditingCriteria.CriteriaText like '%EventTypeID%'
		and fe.FieldOrEvent = 'E'

		Select @TestID = @TableRowID

		Select top 1 @TableRowID = TableRowID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and SqlQueryEditingCriteria.CriteriaText like '%EventTypeID%'
		and fe.FieldOrEvent = 'E'
		And TableRowID > @TableRowID
		Order By FromFieldID Asc

	End

	Select @TableRowID = 0, @TestID = 0

	Select top 1 @TableRowID = TableRowID
	From SqlQueryEditingCriteria  WITH (NOLOCK) 
	INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and SqlQueryEditingCriteria.CriteriaText like '%DetailFieldID%'
	and fe.FieldOrEvent = 'F'
	Order By FromFieldID Asc

	While @TableRowID > @TestID 
	Begin

		Update SqlQueryEditingCriteria 
		Set ClientID = @NewClientID, CriteriaText = REPLACE(REPLACE(CriteriaText, FromFieldID, ToFieldID), @OldClientID, @NewClientID), Criteria1 = ToFieldID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%' and fe.TableRowID = @TableRowID
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and SqlQueryEditingCriteria.CriteriaText like '%DetailFieldID%'
		and fe.FieldOrEvent = 'F'

		Select @TestID = @TableRowID

		Select top 1 @TableRowID = TableRowID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and SqlQueryEditingCriteria.CriteriaText like '%DetailFieldID%'
		and fe.FieldOrEvent = 'F'
		And TableRowID > @TableRowID
		Order By FromFieldID Asc

	End

	Select @TableRowID = 0, @TestID = 0

	Select top 1 @TableRowID = TableRowID
	From SqlQueryEditingCriteria 
	INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and (SqlQueryEditingCriteria.CriteriaText like '%ClientStatusID%' OR SqlQueryEditingCriteria.CriteriaText like '%StatusID%')
	and fe.FieldOrEvent = 'S'
	Order By FromFieldID Asc

	While @TableRowID > @TestID 
	Begin

		Update SqlQueryEditingCriteria 
		Set ClientID = @NewClientID, CriteriaText = REPLACE(REPLACE(CriteriaText, FromFieldID, ToFieldID), @OldClientID, @NewClientID), Criteria1 = ToFieldID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%' and fe.TableRowID = @TableRowID
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and (SqlQueryEditingCriteria.CriteriaText like '%ClientStatusID%' OR SqlQueryEditingCriteria.CriteriaText like '%StatusID%')
		and fe.FieldOrEvent = 'S'

		Select @TestID = @TableRowID

		Select top 1 @TableRowID = TableRowID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and (SqlQueryEditingCriteria.CriteriaText like '%ClientStatusID%' OR SqlQueryEditingCriteria.CriteriaText like '%StatusID%')
		and fe.FieldOrEvent = 'S'
		And TableRowID > @TableRowID
		Order By FromFieldID Asc

	End

	Select @TableRowID = 0, @TestID = 0

	Select top 1 @TableRowID = TableRowID
	From SqlQueryEditingCriteria 
	INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and (SqlQueryEditingCriteria.CriteriaText like '%LookupListItemID%' OR SqlQueryEditingCriteria.CriteriaText like '%DetailValue%')
	and fe.FieldOrEvent = 'L'
	Order By FromFieldID Asc

	While @TableRowID > @TestID 
	Begin

		Update SqlQueryEditingCriteria 
		Set ClientID = @NewClientID, CriteriaText = REPLACE(REPLACE(CriteriaText, FromFieldID, ToFieldID), @OldClientID, @NewClientID), Criteria1 = ToFieldID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%' and fe.TableRowID = @TableRowID
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and (SqlQueryEditingCriteria.CriteriaText like '%LookupListItemID%' OR SqlQueryEditingCriteria.CriteriaText like '%DetailValue%')
		and fe.FieldOrEvent = 'L'

		Select @TestID = @TableRowID

		Select top 1 @TableRowID = TableRowID
		From SqlQueryEditingCriteria 
		INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%'
		Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
		and (SqlQueryEditingCriteria.CriteriaText like '%LookupListItemID%' OR SqlQueryEditingCriteria.CriteriaText like '%DetailValue%')
		and fe.FieldOrEvent = 'L'
		And TableRowID > @TableRowID
		Order By FromFieldID Asc

	End

	Update SqlQueryEditingCriteria 
	Set ClientID = @NewClientID, CriteriaText = REPLACE(REPLACE(CriteriaText, FromFieldID, ToFieldID), @OldClientID, @NewClientID), Criteria1 = ToFieldID
	From SqlQueryEditingCriteria 
	INNER Join @FieldsandEvents fe on SqlQueryEditingCriteria.CriteriaText like '%' + convert(varchar,fe.FromFieldID) + '%' and fe.TableRowID = @TableRowID
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and (SqlQueryEditingCriteria.CriteriaText like '%LeadTypeID%')
	and fe.FieldOrEvent = 'T'

	Update SqlQueryEditingCriteria 
	Set ClientID = @NewClientID, CriteriaText = REPLACE(CriteriaText, '= ' + convert(varchar,@OldClientID), '= ' + convert(varchar,@NewClientID))
	From SqlQueryEditingCriteria 
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and SqlQueryEditingCriteria.CriteriaText NOT like '%EventTypeID%'
	and SqlQueryEditingCriteria.CriteriaText NOT like '%DetailFieldID%'

	Update SqlQueryEditingCriteria 
	Set ClientID = @NewClientID, CriteriaText = REPLACE(CriteriaText, 'ClientID IN (0, ' + convert(varchar,@OldClientID) + ')', 'ClientID IN (0, ' + convert(varchar,@NewClientID) + ')')
	From SqlQueryEditingCriteria 
	Where  SqlQueryEditingCriteria.SqlQueryEditingID = @EditingID 
	and SqlQueryEditingCriteria.CriteriaText NOT like '%EventTypeID%'
	and SqlQueryEditingCriteria.CriteriaText NOT like '%DetailFieldID%'

	INSERT INTO [dbo].[SqlQuery]([ClientID],[QueryText],[QueryTitle],[AutorunOnline],[OnlineLimit],[BatchLimit],[SqlQueryTypeID],[FolderID],[IsEditable],[IsTemplate],[IsDeleted],[WhenCreated],[CreatedBy],[OwnedBy],[RunCount],[LastRundate],[LastRuntime],[LastRowcount],[MaxRuntime],[MaxRowcount],[AvgRuntime],[AvgRowcount],[Comments],[WhenModified],[ModifiedBy],[LeadTypeID],[ParentQueryID],[IsParent])
	VALUES(@NewClientID,CASE @IsEditable WHEN 1 THEN '' ELSE @QueryText END,LEFT(@QueryName,250),0,0,0,3,@DestinationFolderID,@IsEditable,0,0,dbo.fn_GetDate_Local(),@RunAsUserID,@RunAsUserID,0,NULL,1,0,1,0,1,0,'',dbo.fn_GetDate_Local(),@RunAsUserID,@ToLeadTypeID,NULL,0)

	Select @NewQueryID = SCOPE_IDENTITY()

	exec dbo.CommitSqlQueryForEditing 0, @EditingID, @NewQueryID

	update SqlQueryCriteria
	Set ParamValue = Criteria1
	where CriteriaText like '%exists%'
	and Criteria1 <> ''
	and Criteria1 <> ParamValue
	and SqlQueryID = @NewQueryID

	Return @NewQueryID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyReport] TO [sp_executeall]
GO
