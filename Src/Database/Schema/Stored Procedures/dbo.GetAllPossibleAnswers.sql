SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetAllPossibleAnswers    Script Date: 08/09/2006 12:22:51 ******/

CREATE PROCEDURE [dbo].[GetAllPossibleAnswers] @ClientQuestionnaireID int

 AS

SELECT     dbo.MasterQuestions.MasterQuestionID, dbo.QuestionPossibleAnswers.QuestionPossibleAnswerID, dbo.QuestionPossibleAnswers.AnswerText, dbo.QuestionPossibleAnswers.Branch,
                      dbo.MasterQuestions.ClientQuestionnaireID, dbo.MasterQuestions.QuestionTypeID
FROM         dbo.MasterQuestions LEFT OUTER JOIN
                      dbo.QuestionPossibleAnswers ON dbo.MasterQuestions.MasterQuestionID = dbo.QuestionPossibleAnswers.MasterQuestionID
WHERE     (dbo.MasterQuestions.ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllPossibleAnswers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAllPossibleAnswers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAllPossibleAnswers] TO [sp_executeall]
GO
