SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DisplayPosition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DisplayPosition_Insert]
(

	@DisplayPositionNum smallint   ,

	@DisplayPositionName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[DisplayPosition]
					(
					[DisplayPositionNum]
					,[DisplayPositionName]
					)
				VALUES
					(
					@DisplayPositionNum
					,@DisplayPositionName
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DisplayPosition_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Insert] TO [sp_executeall]
GO
