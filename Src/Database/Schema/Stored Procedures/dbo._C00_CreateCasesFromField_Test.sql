SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-03-08
-- Description:	Proc to Create a new case etc for 
--				each number in a field IE Field Number Of 
--				PPI Cases = 3 create 3 ppi cases
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateCasesFromField_Test]
(
@CustomerID int,
@LeadID int,
@CaseID int,
@ClientID int,
@WhoCreated int,
@NewLeadtypeID int,
@FieldIDForNoOfCases int,
@DetailFieldIDToAddValueTo int = NULL,
@ValueToAdd int = NULL,
@DetailFieldIDToAddValueTo2 int = NULL,
@ValueToAdd2 int = NULL,
@FieldtoPopulateWithNewMatterID int = NULL,
@ExcludeFieldFromCopy int = NULL
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare @NumberOfCases int,
			@TestID int = 0,
			@NewLeadID int,
			@OldMatterID int,
			@NewMatterID int
	
	/* Get the number of cases to create */
	SELECT @NumberOfCases = dbo.fnGetDvAsInt (@FieldIDForNoOfCases, @CaseID)

	WHILE @NumberOfCases > @TestID AND @TestID <=20
	BEGIN
	
		/* Create new lead and or add to lead */
		--exec @NewLeadID = dbo._C00_CreateNewLeadForCust @CustomerID, @NewLeadtypeID, @ClientID, @WhoCreated
		select @NewLeadID = 1062624

		/* Get Matter ID created before */	
		Select top 1 @NewMatterID = MatterID
		From Matter WITH (NOLOCK) 
		Where LeadID = @NewLeadID
		Order By MatterID Desc

		Select TOP 1 @OldMatterID = MatterID
		From Matter WITH (NOLOCK) 
		Where CaseID = @CaseID

		/* Add Matter detail values to new matter from current lead */	
		Insert Into MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue, EncryptedValue)
		Select distinct dfb.ClientID, @NewLeadID, @NewMatterID, dfb.DetailFieldID, DetailValue, EncryptedValue
		From MatterDetailValues WITH (NOLOCK) 
		Inner Join DetailFieldAlias dfa WITH (NOLOCK) On dfa.DetailFieldID = MatterDetailValues.DetailFieldID
		/*Inner Join DetailFields df WITH (NOLOCK) on df.DetailFieldID = dfa.DetailFieldID and df.LeadOrMatter = 2  /*Not needed as the field IS a matter field there is no reason to try and restrict the fields to matter ones. */*/
		Inner Join DetailFieldAlias dfb WITH (NOLOCK) On dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID = @NewLeadtypeID
		Inner Join DetailFields df2 WITH (NOLOCK) on df2.DetailFieldID = dfb.DetailFieldID and df2.LeadOrMatter = 2
		Where MatterDetailValues.MatterID = @OldMatterID
		and ((MatterDetailValues.DetailFieldID <> @ExcludeFieldFromCopy and @ExcludeFieldFromCopy IS Not NULL) OR @ExcludeFieldFromCopy IS NULL)
		and not exists(
			select *
			From MatterDetailValues mdvt WITH (NOLOCK) 
			Where mdvt.MatterID = MatterDetailValues.MatterID
			And mdvt.DetailFieldID = dfb.DetailFieldID
		)

		/* Copy Lead Detail Values*/
		--Insert Into LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue, EncryptedValue)
		Select dfb.ClientID, @NewLeadID, dfb.DetailFieldID, DetailValue, EncryptedValue
		From LeadDetailValues WITH (NOLOCK) 
		Inner Join DetailFieldAlias dfa WITH (NOLOCK) On dfa.DetailFieldID = LeadDetailValues.DetailFieldID 
		/*Inner Join DetailFields df WITH (NOLOCK) on df.DetailFieldID = dfa.DetailFieldID and df.LeadOrMatter = 1 /*Not needed as the field IS a lead field there is no reason to try and restrict the fields to lead ones. */*/
		Inner Join DetailFieldAlias dfb WITH (NOLOCK) On dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID = @NewLeadtypeID
		Inner Join DetailFields df2 WITH (NOLOCK) on df2.DetailFieldID = dfb.DetailFieldID and df2.LeadOrMatter = 1
		Where LeadDetailValues.LeadID = @LeadID
		and ((LeadDetailValues.DetailFieldID <> @ExcludeFieldFromCopy and @ExcludeFieldFromCopy IS Not NULL) OR @ExcludeFieldFromCopy IS NULL)
		and not exists(
			select *
			From LeadDetailValues ldvt WITH (NOLOCK) 
			Where ldvt.LeadID = @NewLeadID
			And ldvt.DetailFieldID = dfb.DetailFieldID
		)

		/* Copy Matter Detail Values*/
		--Insert Into MatterDetailValues (ClientID,LeadID, MatterID,DetailFieldID,DetailValue, EncryptedValue)
		Select dfb.ClientID, @NewLeadID, @NewMatterID, dfb.DetailFieldID, DetailValue, EncryptedValue
		From LeadDetailValues WITH (NOLOCK) 
		Inner Join DetailFieldAlias dfa WITH (NOLOCK) On dfa.DetailFieldID = LeadDetailValues.DetailFieldID 
		/*Inner Join DetailFields df WITH (NOLOCK) on df.DetailFieldID = dfa.DetailFieldID and df.LeadOrMatter = 1 /*Not needed as the field IS a lead field there is no reason to try and restrict the fields to lead ones. */*/
		Inner Join DetailFieldAlias dfb WITH (NOLOCK) On dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID = @NewLeadtypeID
		Inner Join DetailFields df2 WITH (NOLOCK) on df2.DetailFieldID = dfb.DetailFieldID and df2.LeadOrMatter = 2
		Where LeadDetailValues.LeadID = @LeadID
		and ((LeadDetailValues.DetailFieldID <> @ExcludeFieldFromCopy and @ExcludeFieldFromCopy IS Not NULL) OR @ExcludeFieldFromCopy IS NULL)
		and not exists(
			select *
			From MatterDetailValues mdv2 WITH (NOLOCK) 
			Where mdv2.LeadID = @NewLeadID
			and mdv2.MatterID = @NewMatterID
			And mdv2.DetailFieldID = df2.DetailFieldID
		)

		IF @FieldtoPopulateWithNewMatterID IS NOT NULL
		BEGIN 
		
			--exec dbo._C00_CreateDetailFields @FieldtoPopulateWithNewMatterID, @LeadID
		
			Update LeadDetailValues 
			SET DetailValue = CONVERT(Varchar(10), @NewMatterID) + CHAR(13) + CHAR(10) + DetailValue
			FROM LeadDetailValues
			Where LeadID = @LeadID
			and DetailFieldID = @FieldtoPopulateWithNewMatterID
		
		END

		/* If there is a value passed in create it */
		IF @ValueToAdd IS NOT NULL
		BEGIN
		
			--Insert Into MatterDetailValues (ClientID,LeadID, MatterID,DetailFieldID,DetailValue)
			Select @ClientID, @NewLeadID, @NewMatterID, @DetailFieldIDToAddValueTo, @ValueToAdd

			/* If there is a value2 passed in create it */
			IF @ValueToAdd2 IS NOT NULL
			BEGIN

				--Insert Into LeadDetailValues (ClientID,LeadID, DetailFieldID,DetailValue)
				Select @ClientID, @NewLeadID, @DetailFieldIDToAddValueTo2, @ValueToAdd2
				Where Not Exists (
					Select *
					From LeadDetailValues WITH (NOLOCK) 
					Where LeadID = @NewLeadID
					And DetailFieldID = @DetailFieldIDToAddValueTo2
					)

			END

		END

		/* Incriment Test by 1 */
		Select @TestID = @TestID + 1

	END	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateCasesFromField_Test] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateCasesFromField_Test] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateCasesFromField_Test] TO [sp_executeall]
GO
