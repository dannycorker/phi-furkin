SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ReferenceValueDateFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDateFormat_Get_List]

AS


				
				SELECT
					[ReferenceValueDateFormatID],
					[DateFormat]
				FROM
					[dbo].[ReferenceValueDateFormat] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDateFormat_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Get_List] TO [sp_executeall]
GO
