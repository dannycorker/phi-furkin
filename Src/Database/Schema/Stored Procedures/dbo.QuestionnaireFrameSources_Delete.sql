SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuestionnaireFrameSources table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_Delete]
(

	@QuestionnaireFrameSourceID int   
)
AS


				DELETE FROM [dbo].[QuestionnaireFrameSources] WITH (ROWLOCK) 
				WHERE
					[QuestionnaireFrameSourceID] = @QuestionnaireFrameSourceID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Delete] TO [sp_executeall]
GO
