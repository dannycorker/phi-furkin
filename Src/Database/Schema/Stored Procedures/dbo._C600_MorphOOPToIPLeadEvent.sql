SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-09
-- Description:	Morph an OOP event into the specified IP following up the threads specified
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID from LeadEvent
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MorphOOPToIPLeadEvent] 
(
	@LeadEventID INT, 
	@IPEventTypeID INT,
	@ThreadToFollowUp INT = 1
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE 
			@CaseID INT,
			@EventTypeID INT,
			@FULeadEventID INT,
			@LeadID INT,
			@LETCID INT,
			@ClientID INT

	DECLARE @Threads TABLE ( LETCID INT, LEID INT, Done INT )
			
	SELECT @CaseID=CaseID, @LeadID=LeadID, @EventTypeID=EventTypeID,@ClientID=ClientID
	FROM LeadEvent WITH (NOLOCK) 
	WHERE LeadEventID=@LeadEventID
			
	-- Morph the event
	UPDATE LeadEvent
	SET EventTypeID = @IPEventTypeID, WhenFollowedUp = NULL, NextEventID = NULL, FollowupDateTime=NULL
	WHERE LeadEventID = @LeadEventID
				
	-- do the follow stuff
	INSERT INTO @Threads
	SELECT LeadEventThreadCompletionID,FromLeadEventID,0 
	FROM LeadEventThreadCompletion 
	WHERE CaseID=@CaseID 
	AND ( ThreadNumberRequired=@ThreadToFollowUp OR @ThreadToFollowUp=0 ) 
	AND ToLeadEventID IS NULL

	SELECT * FROM @Threads

	WHILE EXISTS ( SELECT * FROM @Threads t WHERE t.Done=0 )
	BEGIN

		SELECT TOP 1 @LETCID=t.LETCID FROM @Threads t WHERE t.Done=0
		
		UPDATE LeadEventThreadCompletion 
		SET ToEventTypeID=@IPEventTypeID,
			ToLeadEventID=@LeadEventID
		WHERE LeadEventThreadCompletionID=@LETCID
		
		UPDATE @Threads SET Done=1 WHERE LETCID=@LETCID
		
	END

	UPDATE @Threads SET Done=0

	WHILE EXISTS ( SELECT * FROM @Threads t WHERE t.Done=0 )
	BEGIN

		SELECT TOP 1 @FULeadEventID=t.LEID FROM @Threads t WHERE t.Done=0
		
		IF NOT EXISTS ( SELECT * FROM LeadEventThreadCompletion WITH (NOLOCK) 
						WHERE FromLeadEventID=@FULeadEventID AND ToLeadEventID IS NULL )
		BEGIN
			UPDATE LeadEvent 
			SET NextEventID=@LeadEventID,
				WhenFollowedUp=dbo.fn_GetDate_Local()
			WHERE LeadEventID=@FULeadEventID
		END
		
		UPDATE @Threads SET Done=1 WHERE LEID=@FULeadEventID
		
	END	
	
	INSERT INTO LeadEventThreadCompletion (ClientID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,ThreadNumberRequired)
	SELECT @ClientID,@LeadID,@CaseID,@LeadEventID,@EventTypeID,ec.ThreadNumber 
	FROM EventChoice ec WITH (NOLOCK) 
	WHERE ec.EventTypeID=@EventTypeID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MorphOOPToIPLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MorphOOPToIPLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MorphOOPToIPLeadEvent] TO [sp_executeall]
GO
