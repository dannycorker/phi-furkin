SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-10-15
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[___RestartIISIfRequd] 
@Server varchar(100),
@Service varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @CountOfLoginErrors int,
			@FailureThreshold int = 10

	SELECT @CountOfLoginErrors = COUNT(*)
	FROM Logs WITH (NOLOCK)
	WHERE MethodName = 'AuthenticateUser'
	and LogEntry = 'General database error prevented login checks : Cannot validate subscription : Please try again later'
	AND LogDateTime > DATEADD(mi,-5,dbo.fn_GetDate_Local())

	IF @CountOfLoginErrors >= @FailureThreshold
	BEGIN
	
		IF NOT EXISTS (Select * From dbo.__RestartService rs WITH (NOLOCK) WHERE rs.DateTimeRestarted < DATEADD(mi,-5,dbo.fn_GetDate_Local()))
		BEGIN
			
			INSERT INTO __RestartService (Server, Service, DateTimeRestarted)
			Select @Server, @Service, dbo.fn_GetDate_Local()
			
			SELECT 'iisreset
amail -s localhost -t support@aquarium-software.com -f "servicerestart@aquarium-software.com" -a "Web1 IIS Reset due to too many AuthenticateUser failures"
amail -s localhost -t alex.elger@aquarium-software.com -f "servicerestart@aquarium-software.com" -a "Web1 IIS Reset due to too many AuthenticateUser failures"'

		
		END
	
	END
	ELSE
	BEGIN
		
		Select '@echo Nothing to do'
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[___RestartIISIfRequd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[___RestartIISIfRequd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[___RestartIISIfRequd] TO [sp_executeall]
GO
