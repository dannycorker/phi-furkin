SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderFileStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileStatus_Delete]
(

	@DataLoaderFileStatusID int   
)
AS


				DELETE FROM [dbo].[DataLoaderFileStatus] WITH (ROWLOCK) 
				WHERE
					[DataLoaderFileStatusID] = @DataLoaderFileStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Delete] TO [sp_executeall]
GO
