SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadTypePageTitleTargetControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleTargetControl_Get_List]

AS


				
				SELECT
					[LeadTypePageTitleTargetControlID],
					[ClientID],
					[LeadTypeID],
					[LastParsed]
				FROM
					[dbo].[LeadTypePageTitleTargetControl] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleTargetControl_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Get_List] TO [sp_executeall]
GO
