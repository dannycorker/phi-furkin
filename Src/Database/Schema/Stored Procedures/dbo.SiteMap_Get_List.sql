SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SiteMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SiteMap_Get_List]

AS


				
				SELECT
					[SiteMapID],
					[Url],
					[Title],
					[Description],
					[ParentID],
					[Secured]
				FROM
					[dbo].[SiteMap] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SiteMap_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_Get_List] TO [sp_executeall]
GO
