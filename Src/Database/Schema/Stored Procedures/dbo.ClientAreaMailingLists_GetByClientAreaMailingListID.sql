SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientAreaMailingLists table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_GetByClientAreaMailingListID]
(

	@ClientAreaMailingListID int   
)
AS


				SELECT
					[ClientAreaMailingListID],
					[Name],
					[Email],
					[PostCode],
					[OutcomeID],
					[YellowPagesAreaCode],
					[ClientID],
					[ClientQuestionnaireID],
					[OnHold]
				FROM
					[dbo].[ClientAreaMailingLists] WITH (NOLOCK) 
				WHERE
										[ClientAreaMailingListID] = @ClientAreaMailingListID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_GetByClientAreaMailingListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_GetByClientAreaMailingListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_GetByClientAreaMailingListID] TO [sp_executeall]
GO
