SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Chart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_Delete]
(

	@ChartID int   
)
AS


				DELETE FROM [dbo].[Chart] WITH (ROWLOCK) 
				WHERE
					[ChartID] = @ChartID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_Delete] TO [sp_executeall]
GO
