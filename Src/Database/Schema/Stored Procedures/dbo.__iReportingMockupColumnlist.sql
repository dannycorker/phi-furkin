SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-17
-- Description:	Demo for iReporting Mockup
-- =============================================
CREATE PROCEDURE [dbo].[__iReportingMockupColumnlist]
AS
BEGIN
	SET NOCOUNT ON;

	/*
	SELECT TOP (1) vLeadEventFast.CaseID, vLeadEventFast.AgeInDays, vLeadEventFast.Comments, vLeadEventFast.WhoCreated, vLeadEventFast.WhenCreated, vLeadFast.LeadTypeID, vLeadFast.LeadTypeName, vCaseWithLatestEvent.EventOrNoteTypeName, vCaseWithLatestEvent.AgeInDays
	FROM vLeadEventFast WITH (NOLOCK) 
	INNER JOIN vLeadFast WITH (NOLOCK) ON vLeadEventFast.LeadID = vLeadFast.LeadID
	INNER JOIN vCaseWithLatestEvent WITH (NOLOCK) ON vCaseWithLatestEvent.LeadID = vLeadFast.LeadID
	WHERE vCaseWithLatestEvent.ClientID = 4 
	AND (vLeadFast.LeadTypeID = 503) 
	AND vLeadFast.ClientID = 4 
	AND vLeadEventFast.ClientID = 4 
	*/

	DECLARE @Columns TABLE (
		[TableName] varchar(100), 
		[ColumnName] varchar(100),
		[JoinType] varchar(5),
		[OriginalDataType] varchar(20),
		[Functions] varchar(100),
		[Filters] varchar(100),
		[CurrentDataType]  varchar(20),
		[Alias] varchar(100),
		[Show] tinyint,
		[OutputOrder] tinyint,
		[SortOrder] tinyint,
		[SortDirection] varchar(4),
		[Nullable] tinyint
	)

	INSERT @Columns VALUES 
	('vLeadEventFast','CaseID','','int','ToUpper(ConvertToText(vLeadEventFast.CaseID))','','varchar','Case ID',1,1,3,'ASC', 0),
	('vLeadEventFast','AgeInDays','','int','','','int','Oldest Events First',1,2,1,'DESC', 0),
	('vLeadEventFast','Comments','','varchar','','','varchar','Event Comments',1,3,NULL,'', 1),
	('vLeadEventFast','WhoCreated','','int','','','int','Username',1,4,NULL,'', 0),
	('vLeadEventFast','WhenCreated','','datetime','','','int','Event Date',1,5,NULL,'', 0),
	('vLeadFast','LeadTypeID','INNER','int','','vLeadFast.LeadTypeID = 503','int','',0,6,NULL,'', 0),
	('vLeadFast','LeadTypeName','INNER','varchar','','','varchar','',1,7,NULL,'', 0),
	('vCaseWithLatestEvent','EventOrNoteTypeName','LEFT','varchar','','','varchar','',1,8,2,'ASC', 1),
	('vCaseWithLatestEvent','AgeInDays','LEFT','int','','','int','',1,9,NULL,'', 1)

	SELECT c.[TableName], 
		c.[ColumnName],
		c.[JoinType],
		c.[OriginalDataType],
		c.[Functions],
		c.[Filters],
		c.[CurrentDataType],
		c.[Alias],
		c.[Show],
		c.[OutputOrder],
		c.[SortOrder],
		c.[SortDirection],
		c.[Nullable],
		CASE WHEN c.[SortOrder] > 0 THEN CAST(c.[SortOrder] as varchar) + ' ' + c.[SortDirection] ELSE '' END as [DisplaySortOrder] 
	FROM @Columns c ORDER BY c.OutputOrder
END




GO
GRANT VIEW DEFINITION ON  [dbo].[__iReportingMockupColumnlist] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__iReportingMockupColumnlist] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__iReportingMockupColumnlist] TO [sp_executeall]
GO
