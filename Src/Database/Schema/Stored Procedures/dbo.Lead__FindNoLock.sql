SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Lead table passing nullable parameters
-- Table Comment: One LEAD is stored per Customer Questionnaire submission
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead__FindNoLock]
(

	@SearchUsingOR bit   = null ,

	@LeadID int   = null ,

	@ClientID int   = null ,

	@LeadRef varchar (100)  = null ,

	@CustomerID int   = null ,

	@LeadTypeID int   = null ,

	@AquariumStatusID int   = null ,

	@ClientStatusID int   = null ,

	@BrandNew bit   = null ,

	@Assigned bit   = null ,

	@AssignedTo int   = null ,

	@AssignedBy int   = null ,

	@AssignedDate datetime   = null ,

	@RecalculateEquations bit   = null ,

	@Password varchar (65)  = null ,

	@Salt varchar (25)  = null ,

	@WhenCreated datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadID]
	, [ClientID]
	, [LeadRef]
	, [CustomerID]
	, [LeadTypeID]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [BrandNew]
	, [Assigned]
	, [AssignedTo]
	, [AssignedBy]
	, [AssignedDate]
	, [RecalculateEquations]
	, [Password]
	, [Salt]
	, [WhenCreated]
    FROM
	[dbo].[Lead] with (nolock)
    WHERE 
	 ([LeadID] = @LeadID OR @LeadID is null)
	AND ([ClientID] = @ClientID OR @ClientID is null)
	AND ([LeadRef] = @LeadRef OR @LeadRef is null)
	AND ([CustomerID] = @CustomerID OR @CustomerID is null)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID is null)
	AND ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID is null)
	AND ([ClientStatusID] = @ClientStatusID OR @ClientStatusID is null)
	AND ([BrandNew] = @BrandNew OR @BrandNew is null)
	AND ([Assigned] = @Assigned OR @Assigned is null)
	AND ([AssignedTo] = @AssignedTo OR @AssignedTo is null)
	AND ([AssignedBy] = @AssignedBy OR @AssignedBy is null)
	AND ([AssignedDate] = @AssignedDate OR @AssignedDate is null)
	AND ([RecalculateEquations] = @RecalculateEquations OR @RecalculateEquations is null)
	AND ([Password] = @Password OR @Password is null)
	AND ([Salt] = @Salt OR @Salt is null)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadID]
	, [ClientID]
	, [LeadRef]
	, [CustomerID]
	, [LeadTypeID]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [BrandNew]
	, [Assigned]
	, [AssignedTo]
	, [AssignedBy]
	, [AssignedDate]
	, [RecalculateEquations]
	, [Password]
	, [Salt]
	, [WhenCreated]
    FROM
	[dbo].[Lead] with (nolock)
    WHERE 
	 ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadRef] = @LeadRef AND @LeadRef is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([ClientStatusID] = @ClientStatusID AND @ClientStatusID is not null)
	OR ([BrandNew] = @BrandNew AND @BrandNew is not null)
	OR ([Assigned] = @Assigned AND @Assigned is not null)
	OR ([AssignedTo] = @AssignedTo AND @AssignedTo is not null)
	OR ([AssignedBy] = @AssignedBy AND @AssignedBy is not null)
	OR ([AssignedDate] = @AssignedDate AND @AssignedDate is not null)
	OR ([RecalculateEquations] = @RecalculateEquations AND @RecalculateEquations is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([Salt] = @Salt AND @Salt is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	Select @@ROWCOUNT			
  END
				





GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__FindNoLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__FindNoLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__FindNoLock] TO [sp_executeall]
GO
