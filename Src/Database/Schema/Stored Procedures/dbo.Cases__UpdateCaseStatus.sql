SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------
-- Date Created: 2010-07-20

-- Created By:   Jim Green
-- Description:  Update Client Status and Aquarium Status on Cases, and Lead if appropriate.
-- 2015-10-28 JWG #35165 Check for AquariumStatusID of NULL and treat it like 2 (Accepted/Open)
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases__UpdateCaseStatus]
(
	@CaseID int,
	@CaseStatusID int = null,
	@AquariumStatusID int = null
)
AS
BEGIN
	
	DECLARE @LeadID int
	
	/* Set friendly client status on the Cases record first of all */
	IF @CaseStatusID IS NOT NULL
	BEGIN
		UPDATE dbo.Cases 
		SET ClientStatusID = @CaseStatusID 
		WHERE CaseID = @CaseID 
		AND (ClientStatusID IS NULL OR ClientStatusID <> @CaseStatusID)
	END
	
	/* If concrete Aquarium status is defined for the event that just happened, process that too */
	IF @AquariumStatusID IS NOT NULL
	BEGIN
	
		/* Set concrete Aquarium status on the Cases record if it has changed */
		UPDATE dbo.Cases 
		SET	AquariumStatusID = @AquariumStatusID 
		WHERE CaseID = @CaseID 
		AND AquariumStatusID <> @AquariumStatusID 

		/* Get the Lead info */		
		SELECT @LeadID = ca.LeadID 
		FROM dbo.Cases ca 
		WHERE ca.CaseID = @CaseID 
		
		/* 
			Set concrete Aquarium status on the Lead record if it has changed. 
			Only set it to closed (4) if all the Cases are closed now.
		*/
		UPDATE dbo.Lead 
		SET	AquariumStatusID = @AquariumStatusID 
		WHERE LeadID = @LeadID 
		AND AquariumStatusID <> @AquariumStatusID 
		AND (
				(@AquariumStatusID <> 4) 
			OR
				/* 2015-10-28 JWG #35165 */
				/*(NOT EXISTS (SELECT * FROM dbo.Cases ca WHERE ca.LeadID = @LeadID AND ca.AquariumStatusID <> 4))*/
				(NOT EXISTS (SELECT * FROM dbo.Cases ca WHERE ca.LeadID = @LeadID AND ISNULL(ca.AquariumStatusID, 2) <> 4))
			)
		
	END
	
END			


GO
GRANT VIEW DEFINITION ON  [dbo].[Cases__UpdateCaseStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases__UpdateCaseStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases__UpdateCaseStatus] TO [sp_executeall]
GO
