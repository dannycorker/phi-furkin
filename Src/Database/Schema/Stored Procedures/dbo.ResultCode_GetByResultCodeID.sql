SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ResultCode table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResultCode_GetByResultCodeID]
(

	@ResultCodeID varchar (4)  
)
AS


				SELECT
					[ResultCodeID],
					[Description]
				FROM
					[dbo].[ResultCode] WITH (NOLOCK) 
				WHERE
										[ResultCodeID] = @ResultCodeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_GetByResultCodeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResultCode_GetByResultCodeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_GetByResultCodeID] TO [sp_executeall]
GO
