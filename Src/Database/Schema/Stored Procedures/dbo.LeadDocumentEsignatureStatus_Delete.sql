SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadDocumentEsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_Delete]
(

	@LeadDocumentEsignatureStatusID int   
)
AS


				DELETE FROM [dbo].[LeadDocumentEsignatureStatus] WITH (ROWLOCK) 
				WHERE
					[LeadDocumentEsignatureStatusID] = @LeadDocumentEsignatureStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Delete] TO [sp_executeall]
GO
