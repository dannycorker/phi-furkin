SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-06-2013
-- Description:	Gets a list of event type equations by event type
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeEquation__DeleteByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DELETE FROM EventTypeEquation
	WHERE EventTypeID=@EventTypeID AND ClientID = @ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation__DeleteByEventTypeID] TO [sp_executeall]
GO
