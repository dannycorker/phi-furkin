SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the TemplateType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TemplateType_Delete]
(

	@TemplateTypeID int   
)
AS


				DELETE FROM [dbo].[TemplateType] WITH (ROWLOCK) 
				WHERE
					[TemplateTypeID] = @TemplateTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TemplateType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Delete] TO [sp_executeall]
GO
