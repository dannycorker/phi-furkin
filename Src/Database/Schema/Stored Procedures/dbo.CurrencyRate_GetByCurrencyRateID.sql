SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CurrencyRate table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_GetByCurrencyRateID]
(

	@CurrencyRateID int   
)
AS


				SELECT
					[CurrencyRateID],
					[CurrencyId],
					[CurrencyCode],
					[FromGBPRate],
					[ToGBPRate],
					[ConversionDate],
					[IsLatest]
				FROM
					[dbo].[CurrencyRate] WITH (NOLOCK) 
				WHERE
										[CurrencyRateID] = @CurrencyRateID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_GetByCurrencyRateID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_GetByCurrencyRateID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_GetByCurrencyRateID] TO [sp_executeall]
GO
