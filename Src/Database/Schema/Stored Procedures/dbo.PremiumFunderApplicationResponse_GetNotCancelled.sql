SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 26/11/2019
-- Description:	Gets all application responses that have not been cancelled
-- 2019-11-26 GPR added ORDER BY to get latest loanApplication for Customer
-- 2019-11-29 GPR modified to take @MatterID and return associated PremiumFunderApplication
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplicationResponse_GetNotCancelled]
	@MatterID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM PremiumFunderApplicationPolicy petpolicy WITH (NOLOCK)
	INNER JOIN PremiumFunderApplicationResponse loanApplication WITH (NOLOCK) ON loanApplication.PremiumFunderRequestID = petpolicy.PremiumFunderRequestID
	WHERE petpolicy.PAMatterID = @MatterID and loanApplication.PremiumFunderRequestID NOT IN 
		(
			SELECT cancelAgreement.PremiumFunderRequestID 
			FROM PremiumFunderCancelAgreementResponse cancelAgreement WITH (NOLOCK) 
			WHERE cancelAgreement.MatterID = petpolicy.PAMatterID
		)
	ORDER BY 1 DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetNotCancelled] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplicationResponse_GetNotCancelled] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetNotCancelled] TO [sp_executeall]
GO
