SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- ALTER date: 24-04-2013
-- Description:	Inserts a record into the SMSSurveyClientDatabase on Aquarius Master
-- =============================================
CREATE PROCEDURE [dbo].[SMSSurveyClientDatabase_Insert]
	@ClientID INT,
	@OutgoingPhoneNumber VARCHAR(250)
AS
BEGIN

	EXEC AquariusMaster.dbo.AQ_SMSSurveyClientDatabase_Insert @ClientID, @OutgoingPhoneNumber

END


GO
GRANT VIEW DEFINITION ON  [dbo].[SMSSurveyClientDatabase_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSSurveyClientDatabase_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSSurveyClientDatabase_Insert] TO [sp_executeall]
GO
