SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-11-01
-- Description:	Creates a new collections case (for a new payment account)
-- 2017-05-25 JEL Include Account Holder Name
-- 2017-09-22 JEL Clean sort codes
-- 2020-08-07 GPR updated for AAG
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CreateACollectionsCase] 
(
	@LeadEventID INT
)

AS
BEGIN



	SET NOCOUNT ON;

	DECLARE
			@AccountID INT,
			@AccountNumber VARCHAR(100),
			@AccountName VARCHAR(100),
			@AqAutomation INT,
			@CardIdentifier VARCHAR(100),
			@CardExpire VARCHAR(30),
			@CardToken VARCHAR (100),
			@CaseID INT,
			@ClientID INT,
			@ColCaseID INT,
			@ColLeadID INT,
			@ColMatterID INT,
			@CustomerID INT,
			@EventTypeID INT,
			@LeadID INT,
			@LeadTypeID INT,
			@NewLeadEventID INT,
			@NextEventTypeID INT,
			@PaymentMethod INT,
			@PayRef VARCHAR(100),
			@SortCode VARCHAR(100)
			,@EventComments VARCHAR(2000) = ''
	
	SELECT  TOP 1
			@CaseID = le.CaseID,
			@AqAutomation = dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0),
			@LeadTypeID = l.LeadTypeID,
			@LeadID = l.LeadID,
			@EventTypeID=le.EventTypeID,
			@ClientID=l.ClientID
	FROM LeadEvent le WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON le.LeadID=l.LeadID 
	WHERE le.LeadEventID = @LeadEventID
	
	-- get collections lead 
	IF @LeadTypeID=1492 -- pol admin
	BEGIN
	
		SELECT @ColLeadID=m2.LeadID, @CustomerID=m2.CustomerID
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON m.MatterID=ltr.FromMatterID AND ltr.ToLeadTypeID=1493
		INNER JOIN Matter m2 WITH (NOLOCK) ON ltr.ToMatterID=m2.MatterID
		WHERE m.CaseID=@CaseID
	
	END
	ELSE
	BEGIN
	
		SELECT TOP 1 @ColLeadID=m.LeadID,@CustomerID=m.CustomerID 
		FROM Matter m WITH (NOLOCK) 
		WHERE m.CaseID=@CaseID
	
	END

	EXEC @ColMatterID = _C00_CreateNewCaseForLead @ColLeadID, @AqAutomation, 1
	SELECT @ColCaseID = m.CaseID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID = @ColMatterID
	
	-- add process start
	EXEC @NewLeadEventID=_C00_AddProcessStart @ColCaseID, @AqAutomation

	-- add payment details to the new matter
	IF @LeadTypeID=1492 -- policy admin 
	BEGIN
		SELECT @AccountName=dbo.fnGetDv(170250,@CaseID)
		SELECT @AccountNumber=dbo.fnGetDv(170252,@CaseID)
		SELECT @SortCode=dbo.fnGetDv(170251,@CaseID)
		SELECT @CardIdentifier=dbo.fnGetDv(177400,@CaseID)
		SELECT @CardExpire = dbo.fnGetDv(177401,@CaseID)
		SELECT @CardToken = dbo.fnGetDv(177399,@CaseID)
	END  
	ELSE
	BEGIN
		SELECT @AccountName=dbo.fnGetDv(177446,@CaseID)
		SELECT @AccountNumber=dbo.fnGetDv(175382,@CaseID)
		SELECT @SortCode=dbo.fnGetDv(175363,@CaseID)
		SELECT @CardIdentifier=dbo.fnGetDv(177400,@CaseID)
		SELECT @CardExpire = dbo.fnGetDv(177401,@CaseID)
		SELECT @CardToken = dbo.fnGetDv(177399,@CaseID)
	END

	/*GPR 2020-07-14 for AAG-999*/
	IF @ClientID IN (604)
	BEGIN
		SELECT @CardToken = dbo.fnGetSimpleDV(313890,@CustomerID)
		SELECT @AccountName = dbo.fnGetSimpleDV(313878,@CustomerID)
	END

	SELECT @PaymentMethod= CASE dbo.fnGetDvAsInt(170246,@CaseID) 
								WHEN 74474 THEN 69930 -- DD
								ELSE 69931 -- CP
							END
	
	/*Clean any '-' out of sort code*/ 	
	SELECT @SortCode = REPLACE(@SortCode,'-','')   

	-- set mandate reference field
--	SELECT @PayRef=dbo.fn_C600_GetMandateReference(@ColMatterID)

	INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue) VALUES
	(@ClientID,@ColLeadID,@ColMatterID,170188,@AccountName),
	(@ClientID,@ColLeadID,@ColMatterID,170190,@AccountNumber),
	(@ClientID,@ColLeadID,@ColMatterID,170189,@SortCode),
	(@ClientID,@ColLeadID,@ColMatterID,170193,@CardIdentifier),
	(@ClientID,@ColLeadID,@ColMatterID,170191,@CardToken),
	(@ClientID,@ColLeadID,@ColMatterID,170192,@CardExpire),
	(@ClientID,@ColLeadID,@ColMatterID,170115,CAST(@PaymentMethod AS VARCHAR))
	--(@ClientID,@ColLeadID,@ColMatterID,175460,@PayRef)

	SELECT @NextEventTypeID=CASE @PaymentMethod WHEN 69930 
									THEN 150180 -- DD Collections
								 	ELSE 150182 -- CC Collections
			 						END

	-- add collections event
	INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
	SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, @ColCaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, @NextEventTypeID, @AqAutomation, 1, 5, 0, 5
	FROM Cases c WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.CaseID = c.CaseID AND le.LeadEventID=@NewLeadEventID

	IF @EventTypeID = 156879 /*iF it is a one off payment we don't want to reschedule automatically*/
	BEGIN 
		-- create account in new billing system				
		EXEC @AccountID=_C600_BillingSystem_CreateAccountRecord @ColMatterID, @AqAutomation, 0
	
	END
	ELSE 
	BEGIN
		-- create account in new billing system					
		EXEC @AccountID=_C600_BillingSystem_CreateAccountRecord @ColMatterID, @AqAutomation, 1

				-- set mandate reference field
		SELECT @PayRef=dbo.fn_C600_GetMandateReference(@ColMatterID)
		EXEC dbo._C00_SimpleValueIntoField 175460, @PayRef, @ColMatterID, @AqAutomation

	END

	---- Account type - DD
	IF @PaymentMethod = 69930
	BEGIN

		-- set mandate reference field
		SELECT @PayRef=dbo.fn_C600_GetMandateReference(@ColMatterID)
		EXEC dbo._C00_SimpleValueIntoField 175460, @PayRef, @ColMatterID, @AqAutomation

	
	END
	
	SELECT @EventComments += 'Collections MatterID ' + ISNULL(CONVERT(VARCHAR,@ColMatterID),'NULL') + ' created with AccountID ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL') + '.' + CHAR(13)+CHAR(10)+'PayRef'+ISNULL(CONVERT(VARCHAR,@PayRef),'No Pay Ref')
	EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

	--SELECT @PayRef
	--SELECT @PaymentMethod
	--SELECT @ColMatterID


	RETURN @ColMatterID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreateACollectionsCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CreateACollectionsCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreateACollectionsCase] TO [sp_executeall]
GO
