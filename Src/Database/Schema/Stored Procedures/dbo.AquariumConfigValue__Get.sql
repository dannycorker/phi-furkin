SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack	
-- Create date: 2012-07-18
-- Description:	Aquarium Config Value Getter
-- =============================================
CREATE PROCEDURE [dbo].[AquariumConfigValue__Get] 
	@ConfigID INT,
	@ClientID INT, 
	@SubClientID INT
AS
BEGIN
	
	SELECT	
		acv.ConfigValueID,
		acv.ConfigID, 
		acv.ClientID, 
		acv.SubClientID, 
		acv.Name, 
		acv.Value, 
		acv.ValueInt, 
		acv.ValueMoney, 
		acv.ValueDate, 
		acv.ValueDateTime, 
		acv.Salt
	FROM	AquariumConfigValue acv WITH (NOLOCK)
	WHERE
		(acv.ConfigID = @ConfigID)
	AND	(acv.ClientID = @ClientID)
	AND	(acv.SubClientID = @SubClientID)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfigValue__Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumConfigValue__Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfigValue__Get] TO [sp_executeall]
GO
