SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all ClientPersonnel for a client
-- =============================================
CREATE PROCEDURE [dbo].[co] 
	@ClientID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.ClientOffices co (nolock) 
	WHERE co.ClientID = @ClientID 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[co] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[co] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[co] TO [sp_executeall]
GO
