SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[TableRows__GetByParams]
(
		
	@LeadID int,	

	@DetailFieldID int,

	@DetailFieldPageID int,
	
	@ClientID int,
	
	@MatterID int   = null
)

AS

    SELECT
	  [TableRowID]
	, [ClientID]
	, [LeadID]
	, [MatterID]
	, [DetailFieldID]
	, [DetailFieldPageID]
	, [DenyEdit]
	, [DenyDelete]
	, [CustomerID]
	, [CaseID]
	, [ClientPersonnelID]
	, [ContactID]
	, [SourceID] 
    FROM
	[dbo].[TableRows] WITH (NOLOCK) 
    WHERE 	 
	([ClientID] = @ClientID)
	AND ([LeadID] = @LeadID)
	AND ([MatterID] = @MatterID OR @MatterID is null)
	AND ([DetailFieldID] = @DetailFieldID)
	AND ([DetailFieldPageID] = @DetailFieldPageID)
						
	Select @@ROWCOUNT			





GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByParams] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetByParams] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByParams] TO [sp_executeall]
GO
