SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-07
-- Description:	Gets the matters grouped into related conditions based on the table at lead level
-- Used by:		DF 146353
-- Modified:	2014-10-17	SB	Added date of loss to output
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetGroupedClaims] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	m.MatterID, r.ParentID, m.MatterRef, mdvDate.ValueDate AS TreatmentStart, mdvAilment.ValueInt AS AilmentID, rdvAilment.DetailValue AS Ailment,
			LEFT(mdvDesc.DetailValue, 25) AS Description, ISNULL(mdvLoss.ValueDate, mdvDate.ValueDate) AS DateOfLoss
	FROM dbo.fn_C00_1272_GetClaimRelationships(@LeadID) r
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON r.ClaimID = m.MatterID 
	LEFT JOIN dbo.MatterDetailValues mdvDate WITH (NOLOCK) ON m.MatterID = mdvDate.MatterID AND mdvDate.DetailFieldID = 144366
	LEFT JOIN dbo.MatterDetailValues mdvAilment WITH (NOLOCK) ON m.MatterID = mdvAilment.MatterID AND mdvAilment.DetailFieldID = 144504
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment WITH (NOLOCK) ON mdvAilment.ValueInt = rdvAilment.ResourceListID AND rdvAilment.DetailFieldID = 144340
	LEFT JOIN dbo.MatterDetailValues mdvDesc WITH (NOLOCK) ON m.MatterID = mdvDesc.MatterID AND mdvDesc.DetailFieldID = 144332
	LEFT JOIN dbo.MatterDetailValues mdvLoss WITH (NOLOCK) ON m.MatterID = mdvLoss.MatterID AND mdvLoss.DetailFieldID = 144892
	ORDER BY r.TableRowID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetGroupedClaims] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetGroupedClaims] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetGroupedClaims] TO [sp_executeall]
GO
