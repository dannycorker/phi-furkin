SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentTypeFolderLink table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink__AddAllLeadTypeDocumentsToFolder]
(
	@LeadTypeID int,
	@FolderID int   
)
AS
				SET ANSI_NULLS OFF

				INSERT INTO 
					[dbo].[DocumentTypeFolderLink]
					(
						[DocumentTypeID],
						[FolderID]
					)
				SELECT
					[DocumentTypeID],
					@FolderID
				FROM
					[dbo].[DocumentType]
				WHERE
					LeadTypeID = @LeadTypeID
				
				Select @@ROWCOUNT

				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink__AddAllLeadTypeDocumentsToFolder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink__AddAllLeadTypeDocumentsToFolder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink__AddAllLeadTypeDocumentsToFolder] TO [sp_executeall]
GO
