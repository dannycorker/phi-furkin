SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetMasterQuestions    Script Date: 08/09/2006 12:22:51 ******/
/****** Modified by Paul Richardson 23/10/2013 - Added QuestionPossibleAnswerSortOrder		******/
CREATE PROCEDURE [dbo].[GetMasterQuestions] 

	@ClientQuestionnaireID INT

AS

	SELECT MasterQuestionID, 
		ClientQuestionnaireID, 
		QuestionText, 
		QuestionTypeID, 
		TextboxHeight, 
		QuestionOrder, 
		DefaultAnswerID, 
		Mandatory, 
		QuestionToolTip, 
		LinkedQuestionnaireMasterQuestionID, 
		Active, 
		MasterQuestionStatus, 
		ClientID, 
		AnswerPosition, 
		DisplayAnswerAs, 
		NumberOfAnswersPerRow, 
		QuestionPossibleAnswerSortOrder 
	FROM MasterQuestions
	WHERE ClientQuestionnaireID = @ClientQuestionnaireID
	ORDER BY QuestionOrder


GO
GRANT VIEW DEFINITION ON  [dbo].[GetMasterQuestions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMasterQuestions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMasterQuestions] TO [sp_executeall]
GO
