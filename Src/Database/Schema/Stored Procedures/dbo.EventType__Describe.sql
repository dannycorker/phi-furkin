SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-16
-- Description:	Helpful details about an EventType
-- 2013-09-10 CPS					| Added fn_C00_GetUrlByDatabase() to control URLs
-- 2016-12-09 CPS					| Added AquariumEventSubtype details, including billing module stored procedures
-- 2017-01-03 CPS					| Changed to PredicateRuleAssigned, rather than PredicateRule
-- 2018-03-16 CPS					| Added detailed Predicate Rules section
-- 2018-06-07 CPS					| added EventTypeOptions section
-- 2018-06-12 CPS					| added EventTypeAutomatedWorkflow
-- 2018-07-15 CPS					| added ThirdPartySystemEvent lookup
-- 2019-10-15 CPS for JIRA LPC-76	| Updated from Aquarius433Dev to include detailed PredicateRules support
-- 2019-10-31 CPS for JIRA LPC-28	| Included EventTypePaymentGateway support
-- =============================================
CREATE PROCEDURE [dbo].[EventType__Describe] 
	@EventTypeID int
AS
BEGIN
	SET NOCOUNT ON;

	PRINT OBJECT_NAME(@@ProcID)
	
	-- Headline information
	SELECT 'Headline Info' as [Headline Info], et.EventTypeID, et.EventTypeName, lt.LeadTypeID, lt.LeadTypeName, et.Enabled, et.AvailableManually, et.InProcess, et.KeyEvent, et.EventSubtypeID
	,dbo.fn_C00_GetUrlByDatabase() + 'EventTypeEdit.aspx?EventTypeID=' + convert(varchar,@EventTypeID) [Editing URL]
	,dbo.fn_C00_GetUrlByDatabase() + 'EventChoiceEdit.aspx?et=' + convert(varchar,et.EventTypeID) + '&lt=' + convert(varchar,et.LeadTypeID) [Edit Choices]
	FROM dbo.EventType et WITH (NOLOCK) 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = et.LeadTypeID 
	WHERE et.EventTypeID = @EventTypeID 

	-- Show all values
	SELECT 'Event Type Info' as [Event Type Info], et.* 
	FROM dbo.EventType et WITH (NOLOCK)
	WHERE et.EventTypeID = @EventTypeID 

	IF EXISTS ( SELECT * FROM dbo.ThirdPartySystemEvent se WITH ( NOLOCK ) WHERE se.EventTypeID = @EventTypeID )
	BEGIN
		SELECT 'ThisPartySystemEvents' [ThisPartySystemEvents], se.*, ps.SystemName
	    FROM dbo.ThirdPartySystemEvent se WITH ( NOLOCK ) 
		INNER JOIN dbo.ThirdPartySystem ps WITH ( NOLOCK ) ON ps.ThirdPartySystemId = se.ThirdPartySystemID
		WHERE se.EventTypeID = @EventTypeID
	END
	
	-- Lookup Info
	SELECT 'Lookup Info' as [Lookup Info], 
	est.EventSubtypeName, 
	ls.StatusName as [ClientStatus],  
	aet.AquariumEventTypeName, 
	aest.AquariumEventSubtypeName,
	aest.StoredProcedure [SubTypeStoredProcedure],
	ast.AquariumStatusName, 
	dt.DocumentTypeName, 
	( SELECT COUNT(*) FROM PredicateRuleAssigned pr WITH ( NOLOCK ) WHERE pr.EventTypeID = @EventTypeID ) [PredicateRules] /*CS 2014-12-21*/
	,f.FolderName
	, 'exec dbo.EventType__Copy ' + convert(varchar,@EventTypeID) [Copy Event],
	ISNULL(dbo.fn_C00_GetUrlByDatabase() + 'DocumentManagement/EditDocument.aspx?returnUrl=DocumentMgr.aspx&did=' + convert(varchar,df.DocumentTypeID) + '&fid=' + convert(varchar,df.FolderID) + '&a=Edit','') [Edit Document]
	FROM dbo.EventType et WITH (NOLOCK) 
	LEFT JOIN dbo.EventSubtype est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID 
	LEFT JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = et.StatusAfterEvent 
	LEFT JOIN dbo.AquariumEventType aet WITH (NOLOCK) ON aet.AquariumEventTypeID = et.AquariumEventAfterEvent 
	LEFT JOIN AquariumEventSubType aest WITH ( NOLOCK ) on aest.AquariumEventSubtypeID = et.AquariumEventSubtypeID
	LEFT JOIN dbo.AquariumStatus ast WITH (NOLOCK) ON ast.AquariumStatusID = aet.AquariumStatusAfterEvent 
	LEFT JOIN dbo.DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = et.DocumentTypeID
	LEFT JOIN DocumentTypeFolderLink df WITH (NOLOCK) ON df.DocumentTypeID = dt.DocumentTypeID 
	LEFT JOIN Folder f WITH (NOLOCK) ON f.FolderID = df.FolderID 
	WHERE et.EventTypeID = @EventTypeID 

	IF 25 = ( SELECT et.EventSubtypeID FROM EventType et WITH (NOLOCK) WHERE et.EventTypeID = @EventTypeID ) /*Payment gateway*/
	BEGIN
		SELECT	 'PaymentGatewayDetails' [PaymentGatewayDetails]
				,epg.*
				,pg.PaymentGatewayID,pg.Name
				,tt.TransactionType,tt.Description
				,cpg.*
		FROM EventTypePaymentGateway epg WITH (NOLOCK) 
		LEFT JOIN dbo.ClientPaymentGateway cpg WITH (NOLOCK) on cpg.ClientPaymentGatewayID = epg.ClientPaymentGatewayID
		LEFT JOIN dbo.PaymentGateway pg WITH (NOLOCK) on pg.PaymentGatewayID = cpg.PaymentGatewayID
		LEFT JOIN dbo.TransactionType tt WITH (NOLOCK) on tt.TransactionTypeID = epg.TransactionTypeID
		WHERE epg.EventTypeID = @EventTypeID
	END

	IF 0 < ( SELECT COUNT(*) FROM PredicateRuleAssigned pr WITH ( NOLOCK ) WHERE pr.EventTypeID = @EventTypeID ) /*CPS 2018-03-16*/
	BEGIN
		SELECT 'PredicateRules' [PredicateRules], pra.*, dt.DocumentTypeName, f.FolderName , pr.*
		FROM PredicateRuleAssigned pra WITH ( NOLOCK ) 
		INNER JOIN PredicateRule pr WITH ( NOLOCK ) on pr.PredicateRuleID = pra.PredicateRuleID
		LEFT JOIN DocumentType dt WITH ( NOLOCK ) on dt.DocumentTypeID = pra.OverrideDocumentTypeID
		LEFT JOIN DocumentTypeFolderLink fl WITH ( NOLOCK ) on fl.DocumentTypeID = dt.DocumentTypeID
		LEFT JOIN Folder f WITH ( NOLOCK ) on f.FolderID = fl.FolderID
		WHERE pra.EventTypeID = @EventTypeID
	END

	IF 1 = (SELECT InProcess FROM dbo.EventType WITH (NOLOCK) WHERE EventTypeID = @EventTypeID)
	BEGIN

		-- Event Choices leading away from this event type
		SELECT 'Event Choices From Here' as [Event Choices From Here], ec.*, to_et.* 
		FROM dbo.EventType et WITH (NOLOCK) 
		LEFT JOIN dbo.EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = et.EventTypeID 
		LEFT JOIN dbo.EventType to_et WITH (NOLOCK) ON to_et.EventTypeID = ec.NextEventTypeID 
		WHERE et.EventTypeID = @EventTypeID 
		
		-- Event Choices leading to this event type
		SELECT 'Event Choices To Here' as [Event Choices To Here], ec.*, from_et.* 
		FROM dbo.EventType et WITH (NOLOCK) 
		LEFT JOIN dbo.EventChoice ec WITH (NOLOCK) ON ec.NextEventTypeID = et.EventTypeID 
		LEFT JOIN dbo.EventType from_et WITH (NOLOCK) ON from_et.EventTypeID = ec.EventTypeID 
		WHERE et.EventTypeID = @EventTypeID 

	END	
	
	-- Sql After Event
	IF EXISTS ( SELECT * FROM dbo.EventTypeSql ets WITH (NOLOCK)  WHERE ets.EventTypeID = @EventTypeID )
	BEGIN
		SELECT 'Sql After Event' as [Sql After Event], ets.*
		FROM dbo.EventTypeSql ets WITH (NOLOCK) 
		WHERE ets.EventTypeID = @EventTypeID 
	END
	ELSE
	BEGIN
		SELECT 'EXEC ets null, ' + convert(varchar,@EventTypeID) + ', 1' [No SAE]
	END

	/*CPS 2018-06-07*/
	SELECT aqo.AquariumOptionID, aqo.AquariumOptionName, eto.*
	FROM AquariumOption aqo WITH ( NOLOCK ) 
	LEFT JOIN EventTypeOption eto WITH ( NOLOCK ) on eto.EventTypeID = @EventTypeID and eto.AquariumOptionID = aqo.AquariumOptionID
	WHERE aqo.AquariumOptionTypeID = 9 /*EventType Options*/
	
	-- Event Type Attachments
	SELECT 'Event Type Attachment' as [Event Type Attachment], eta.*, et.*
	FROM EventTypeAttachment eta WITH (NOLOCK) 
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = eta.AttachmentEventTypeID
	WHERE eta.EventTypeID = @EventTypeID 
	
	-- Event Type Automated Task
	SELECT 'Event Type Automated Task' as [Event Type Automated Task], eta.*, at.Taskname, at.Enabled
	FROM EventTypeAutomatedTask eta WITH (NOLOCK) 
	INNER JOIN AutomatedTask at WITH (NOLOCK) on at.TaskID = eta.AutomatedTaskID
	WHERE eta.EventTypeID = @EventTypeID 
	
	-- Automated Task:Event Type
	SELECT 'Automated Task that adds me' as [Automated Task that adds me], at.*
	FROM dbo.AutomatedTask at WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = at.TaskID 
	WHERE atp.ParamName = 'LEADEVENT_TO_ADD'
	AND atp.ParamValue = convert(varchar, @EventTypeID)
	
	/*EventTypeAutomatedEvent*/ /*CS 2012-05-07*/
	IF Exists ( SELECT * FROM dbo.EventTypeAutomatedEvent ae WITH (NOLOCK) WHERE ae.EventTypeID = @EventTypeID or ae.AutomatedEventTypeID = @EventTypeID)
	BEGIN
		SELECT 'Automated Event that Triggers me' [EventTypeAutomatedEvent], et.EventTypeID,et.EventTypeName,ae.RunAsUserID,ae.ThreadToFollowUp,ae.InternalPriority
		FROM dbo.EventTypeAutomatedEvent ae WITH (NOLOCK) 
		INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ae.EventTypeID 
		WHERE ae.AutomatedEventTypeID = @EventTypeID
		
		SELECT 'Automated Event that I Trigger' [EventTypeAutomatedEvent], et.EventTypeID,et.EventTypeName,ae.RunAsUserID,ae.ThreadToFollowUp,ae.InternalPriority
		FROM dbo.EventTypeAutomatedEvent ae WITH (NOLOCK) 
		INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ae.AutomatedEventTypeID 
		WHERE ae.EventTypeID = @EventTypeID
	END

	SELECT 'Automated Workflow that I Create' [EventTypeAutomatedWorkflow], wg.Name, aw.*
	FROM EventTypeAutomatedWorkflow aw WITH ( NOLOCK ) 
	INNER JOIN WorkflowGroup wg WITH ( NOLOCK ) on wg.WorkflowGroupID = aw.WorkflowGroupID
	WHERE aw.EventTypeID = @EventTypeID
	
	-- Event Type Field Completion
	SELECT 'Event Type Field Completion' as [Event Type Field Completion], etf.*, df.FieldName, df.LeadOrMatter
	FROM dbo.EventTypeFieldCompletion etf WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = etf.DetailFieldID
	WHERE etf.EventTypeID = @EventTypeID 
	
	-- Event Type Limitation
	SELECT 'Event Type Limitation' as [Event Type Limitation], etl.*
	FROM dbo.EventTypeLimitation etl WITH (NOLOCK) 
	WHERE etl.EventTypeIDFrom = @EventTypeID 
	
	-- Event Type Mandatory Field
	SELECT 'Event Type Mandatory Field' as [Event Type Mandatory Field], etm.*, df.FieldName, df.LeadOrMatter, qt.Name
	FROM dbo.EventTypeMandatoryField etm WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = etm.DetailFieldID
	INNER JOIN QuestionTypes qt WITH (NOLOCK) on qt.QuestionTypeID = df.QuestionTypeID
	WHERE etm.EventTypeID = @EventTypeID 
	
	-- Event Type Helper Field
	SELECT 'Event Type Helper Field' as [Event Type Helper Field], eth.*, df.FieldName, df.LeadOrMatter, qt.Name
	FROM dbo.EventTypeHelperField eth WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = eth.DetailFieldID
	INNER JOIN QuestionTypes qt WITH (NOLOCK) on qt.QuestionTypeID = df.QuestionTypeID
	WHERE eth.EventTypeID = @EventTypeID 
	
	-- Event Type Mandatory Event
	SELECT 'Event Type Mandatory Event' as [Event Type Mandatory Event], etm.*
	FROM dbo.EventTypeMandatoryEvent etm WITH (NOLOCK) 
	WHERE etm.ToEventTypeID = @EventTypeID 
	
	-- Event Type Group
	SELECT 'Event Type Group' as [Event Type Group], eg.EventGroupName,egm.*
	FROM dbo.EventGroupMember egm WITH (NOLOCK) 
	INNER JOIN dbo.EventGroup eg WITH (NOLOCK) ON eg.EventGroupID = egm.EventGroupID
	WHERE egm.EventTypeID = @EventTypeID 
	
	-- Show all history for this event type
	SELECT 'Event Type History' as [Event Type History], eth.* 
	FROM dbo.EventTypeHistory eth WITH (NOLOCK)
	WHERE eth.EventTypeID = @EventTypeID 
	ORDER BY eth.EventTypeHistoryID 
	
END














GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[EventType__Describe] TO [sp_executehelper]
GO
