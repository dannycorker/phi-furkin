SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadViewHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_Get_List]

AS


				
				SELECT
					[LeadViewHistoryID],
					[ClientPersonnelID],
					[ClientID],
					[LeadID],
					[WhenViewed]
				FROM
					[dbo].[LeadViewHistory] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Get_List] TO [sp_executeall]
GO
