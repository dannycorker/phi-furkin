SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2017-07-07
-- Description:	get sql snippets
-- =============================================
CREATE PROCEDURE [dbo].[tom]
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @HelpTable TABLE ( [Description] Varchar(max), [SQL] Varchar(max), ID int Identity(1,1) not null)

	INSERT INTO @HelpTable
	select 'Search Content of procs and functions' [Description]
	, 'SELECT OBJECT_NAME(object_id)
	FROM sys.sql_modules WITH (NOLOCK) 
	WHERE definition LIKE ''%         %''' [SQL]

	UNION

	select 'Get procs called within a proc'
	,'SELECT fn.*
	FROM dbo.fnStoredProceduresGetAllRecursive(''_C427_SAE_Claim'') fn
	ORDER BY fn.[Level] ASC'
	
	union

	select 'View Change Control Table'
	,'select top 100 * from ChangeControl cc with(nolock) order by cc.ChangeControlID desc'
	

	union 

	select 'Print Change Control Proc' 
	,'
	DECLARE @V VARCHAR(MAX) 
	,@ChangeControl INT =  78896

	select top 1 @V = cc.ObjectReference from ChangeControl cc with(nolock) where cc.ChangeControlID = @ChangeControl order by cc.ChangeControlID DESC
	 /*Replace line breaks with special char*/
	  select @V = replace(@V, CHAR(13) + CHAR(10), ''¬'') /*output lines of text as rows*/ 
	select * from [dbo].[fnTableOfValues](@V,''¬'')
	'
	

	union

	select  'DetailFieldAlias Insert'
	,'exec [_C00_DetailFieldAlias_Insert] @ClientID, @LeadTypeID, @CreatedBy, @Enabled'
	

	union 

	select 'Script Lock Delete'
	,'DECLARE @ScriptID INT = ????
	DECLARE @ScriptLockID int = (SELECT TOP 1 ScriptLockID FROM dbo.ScriptLock WHERE ScriptID=@ScriptID)
	EXEC dbo.ScriptLock_Delete @ScriptLockID'

	union 

	select 'Lead Event Comments proc'
	,'exec _C00_SetLeadEventComments @LeadEventID, @Comments'

	union

	select 'Table SQL Writer'
	,'exec _C00_EasyTableGet @DetailFieldID'

	union 

	select 'DocumentDelta Saving'
	,'select count(*) [TotalDocs] ,sum(dd.bytessaved) /1000000.00 as [MBSaved]
		, sum(dd.originalBlobSize) /1000000.00  [OriginalBlobSize]
		, sum(dd.deltaBlobSize) /1000000.00  [NewBlobSize]
		from documentdelta dd with(nolock)
		where dd.isChildDelta = 1 
		'


	union 

	select 'Grant Permission on Stored Proc'
	,'GRANT EXEC on _C00_LeadDocument_NoBlob_GetByLeadDocumentID to sp_executeall'



	union 

	select 'Search tables for column name'
	,'SELECT o.name [Table], c.name [Column], o.type
		FROM sysobjects o 
		INNER JOIN syscolumns c on c.id = o.id
		WHERE o.type = ''U''
		and c.name like ''%invoice%''
		ORDER BY o.name'


	UNION 

	select 'Get your ClientPersonneldID'
	,'select cp.clientpersonnelID,cp.clientid,cp.username,cp.emailaddress
	from ClientPersonnel cp with(nolock) 
	where cp.UserName = ''Thomas Doyle''
	and cp.IsAquarium = 1'


	union 
	
	select 'Add a function to ireporting'
	,'
	DECLARE @SqlFunctionID int
	select @SqlFunctionID = max(sqlfunctionid) + 1 from SqlFunction with(nolock)

	insert into SqlFunction ( SqlFunctionID, SqlFunctionName, FunctionAlias, FunctionDescription, RestrictedToDataTypeID, WhenCreated, WhenModified, IsPreferred, Example, ExampleResult, ResultDataTypeID, ExpressionMask, IsAggregate)
	values (@SqlFunctionID, ''dbo.fn_C427_isPaymentRowOnHold'',''isPaymentRowOnHold'',''is payment row on hold'',2,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local(),0,''dbo.fn_C427_isPaymentRowOnHold(6446953)'',''0'',2,''dbo.fn_C427_isPaymentRowOnHold({0})'',0)


	insert into SqlFunctionParam (SqlFunctionID, ParamName, ParamDescription, ParamOrder, IsMandatory, SqlFunctionDataTypeID, WhenCreated, WhenModified, IsRecurring, DecimalPlaces, MinValue, MaxValue, MaxLength, UseQueryColumn, QuoteValue, PrefixText, SuffixText, ListTypeID)
	values (@SqlFunctionID, ''Any column'', ''any'', 1, 0, 2, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), 0, null, null, null, null, 0, 0, null, null, null)

	GRANT EXEC on fn_C427_isPaymentRowOnHold to sp_executeall
	'

	union 

	select 'Change Field Order'
	,'
	DECLARE @DetailfieldPageID int =26042
	SELECT TOP 1000 
	ROW_NUMBER ( ) OVER ( partition by 1 order by df.fieldorder asc )
	,''EXEC dbo._C00_DetailField_Update '' + CAST(df.detailfieldid as varchar(90)) + '',''''fieldorder'''','' + '''''''' + cast (ROW_NUMBER ( ) OVER ( partition by 1 order by df.fieldorder asc ) as varchar(20)) + ''''''''

	,* 
	FROM  DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldPageID = @DetailfieldPageID
	order by df.FieldOrder asc'

	union 

	select 'Change Field Order - in excel'
	,'   DECLARE @DetailfieldPageID int =26469  
    SELECT TOP 1000    ROW_NUMBER ( ) OVER ( partition by 1 order by df.fieldorder asc )   
	,''=CONCATENATE("''
	+''EXEC dbo._C00_DetailField_Update '' + CAST(df.detailfieldid as varchar(90)) 
	+ '',''''fieldorder'''','' + '''''''' 
	+''",''
	+''ROW(),''
	
	+''"''''"'' +'',''+ ''"/*''
	+ CAST(DF.FIELDNAME AS VARCHAR(2000))
	+ ''*/"''
	+'')''
	,*    FROM  DetailFields df WITH (NOLOCK)  
	WHERE df.DetailFieldPageID = @DetailfieldPageID  
	AND DF.Enabled = 1
	order by df.FieldOrder asc'


	union 

	select 'Apply An Event'
	,'exec dbo._C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @EventTypeID, @WhoCreated, -1'


	union 

	select 'CSV From column'
	,'
	declare @CsvMatters varchar(max) 

	select @CsvMatters = COALESCE(cast(@CsvMatters  as varchar) + '','', '''') + CAST(matterID AS VARCHAR) 
	From @Matters
	select @CsvMatters'

	
	union

	select 'Compare 2 versions in ChangeControl' 
	,'EXEC ChangeControlDiff 71216,71386,3'


	union 

	select 'Add a table to ireporting'
	,'EXEC [_C00_iReporting_AddTable] ''TABLENAME'' '


	union
	select 'TODO: deploy this proc to all Databases'
	,''

	union
	select 'Map Process'
	,'EXEC [MapProcess] @EventTypeID'

	union 
	select
	'Add a function to ireporting (app must be restarted to take effect)'
	,'
	DECLARE @InputSqlDataType int = 2
	,@OutputSQLDataType INT = 1

	INSERT INTO SqlFunction (SqlFunctionID, [SqlFunctionName], [FunctionAlias], [FunctionDescription], [RestrictedToDataTypeID], [WhenCreated], [WhenModified], [IsPreferred], [Example], [ExampleResult], [ResultDataTypeID], [ExpressionMask], [IsAggregate])
	VALUES (423,''dbo.fn_C427_PaymentBarredGetOnHoldName'', ''fn_C427_PaymentBarredGetOnHoldName'', ''gets bar name from a bar that is blocking a payment'' 
	,2,dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), 0,''dbo.fn_C427_PaymentBarredGetOnHoldName(12345678)'', ''2018-02-10'', 1, ''dbo.fn_C427_PaymentBarredGetOnHoldName({0})''
	,0)

	INSERT INTO SqlFunctionParam ([SqlFunctionID], [ParamName], [ParamDescription], [ParamOrder], [IsMandatory], [SqlFunctionDataTypeID], [WhenCreated], [WhenModified], [IsRecurring], [UseQueryColumn], [QuoteValue])
	VALUES (423, ''Any Column'', ''any'',1, 0, 2, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), 0, 0,0)'

	union
	select
	'Check dependancies'
	,'EXEC [DependancyCheck]'

	union 
	select 'Add debug menu options'
	,'exec _C00_DebugMenuOptions @ClientID'

	union 
	select 'run invoice tests'
	
	,'EXEC Test_SupplierInvoice_01VatStatusStandard_FullPayment_GBP_SingleClaimItem
		EXEC Test_SupplierInvoice_02VatStatusReverse_FullPayment_GBP_SingleClaimItem
		EXEC Test_SupplierInvoice_03VatStatusZero_FullPayment_GBP_SingleClaimItem
		EXEC Test_SupplierInvoice_04VatStatusStandard_PartPayment_GBP_SingleClaimItem
		EXEC Test_SupplierInvoice_05VatStatusReverse_PartPayment_GBP_SingleClaimItem
		EXEC Test_SupplierInvoice_06VatStatusZero_PartPayment_GBP_SingleClaimItem 
		EXEC Test_SupplierInvoice_07VatStatusStandard_FullPayment_USD_SingleClaimItem 
		EXEC Test_SupplierInvoice_08VatStatusReverse_FullPayment_USD_SingleClaimItem
		EXEC Test_SupplierInvoice_09VatStatusZero_FullPayment_USD_SingleClaimItem
		EXEC Test_SupplierInvoice_10VatStatusStandard_PartPayment_USD_SingleClaimItem
		EXEC Test_SupplierInvoice_11VatStatusReverse_PartPayment_USD_SingleClaimItem
		EXEC Test_SupplierInvoice_12VatStatusZero_PartPayment_USD_SingleClaimItem
		EXEC Test_SupplierInvoice_13VatStatusStandard_FullPayment_GBP_MultipleClaimItems_SingleItemInvoice
		'
	UNION
	select 'Delete a matter'
	, 'SELECT ''EXEC [SoftDeleteDataForDPA] '' +CAST(M.CaseID AS VARCHAR(200))+'', 11, ''''2018-12-01'''','''''''',1''
	,* fROM Matter M WITH(NOLOCK) WHERE M.MatterID IN (27594990)'
	UNION
	select 'View tran file details'
	, '
	use [master]
	RESTORE HEADERONLY   
	FROM DISK = N''\\926141-FileClu2\AqShared\SharedFtp\Client_427\int\TranLogs\Aquarius427Integration_Tran_2018-08-06_094303_5301291.trn''
	'
	UNION 
	 SELECT  'Find Duplicate events applied by bad Batch Job Queries' 
	, 'with cte as (
		select COUNT(*) [rn], le.eventtypeid, le.CaseID, MAX(le.whencreated) [WhenCreated], MAX(le.comments) [comments]
		From leadevent le with(nolock)
		where le.whencreated > dbo.fnDateOnlyNDaysAgo(7)
		and le.Comments like ''%AquariumNet Batch%''
		group by le.CaseID, le.eventtypeid
	)
	select max(cte.rn) rn, cte.EventTypeID, MAX(cte.caseid) [CaseID], max(cte.WhenCreated) WhenCreated, cte.comments, et.EventTypeName
	From cte  cte
	inner join EventType et with(nolock) on et.eventtypeid = cte.eventtypeid
	where rn > 1 
	group by cte.EventTypeID, cte.comments, et.EventTypeName
	order by 1 desc'

	UNION 
	SELECT 'Move customer to client','declare @CustomerID int = 27741983
update m
set m.clientid=427
from Matter m
where m.customerid = @CustomerID 

 update l
 set l.ClientID=427
From matter m 
inner join Lead l on l.LeadID = m.LeadID
where m.customerid = @CustomerID 


update l
set l.ClientID = 427
From matter m 
inner join cases l on l.caseid= m.caseid
where m.customerid = @CustomerID 


update l
set l.ClientID = 427
From matter m 
inner join customers l on l.customerid = m.customerid
where m.customerid = @CustomerID 


update c
set c.ClientID = 427
 From matter m 
inner join lead l on l.leadid = m.leadid
inner join customers c on c.customerid = l.customerid
where m.customerid = @CustomerID 

'
 UNION
SELECT 'Workflowtask Create'
,'exec [dbo].[_C00_WorkflowTask_Create] @CaseID, @WorkflowGroupID, @EventTypeID'
UNION 
SELECT 'Rulesengine - Check value is in range'
,'

DECLARE @RuleParameterID int = 11543
DECLARE @i DECIMAL(10, 4) 
,@increment decimal(10,4) = 0.0001

DECLARE @VAL TABLE (VAL1 NUMERIC(18,4), VAL2 NUMERIC(18,4))

INSERT INTO @VAL
select p.Val1, p.Val2

From RulesEngine_ParameterOptions p
where RuleParameterID in (@RuleParameterID)
and isnumeric(p.Val1) = 1 and ISNUMERIC(p.val2) = 1 

select @i = MIN(val1) from @VAL

WHILE @i < (select MAX(val2) from @VAL)
begin

	if not exists (
		select * 
		From @VAL 
		where @i between VAL1 and VAL2
	)
	begin
		
		print cast(@i as varchar(200)) + '' was not in range!''
	end

	SELECT @i += @increment

end'












	SELECT * 
	FROM @HelpTable ORDER BY ID ASC


END
GO
GRANT VIEW DEFINITION ON  [dbo].[tom] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[tom] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[tom] TO [sp_executeall]
GO
