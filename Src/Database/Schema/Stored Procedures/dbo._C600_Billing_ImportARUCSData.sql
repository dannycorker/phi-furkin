SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-06
-- Description:	Import Arudd Data to Arudd Processing Table
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_ImportARUCSData]
	 @XMLRaw VARCHAR(MAX),
	 @FileName VARCHAR(100) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XML XML = @XMLRaw,
			@ClientID INT = dbo.fnGetPrimaryClientID()

	/*
		NB 
		Incoming sort code can have hyphens in them...
		Amount will be positive so we will need to flip this on import so we can reconcile it 
			as an outgoing payment.
	*/
	INSERT INTO BACSPaymentFailures (ClientID, FileName, FailureFileType, RowNum, AccountNumber, SortCode, Name, PaymentAmount, BACSProcessedDate, FailureReason, FailureCode, Reference, ImportStatusID, ImportedDate, Comments)
	OUTPUT inserted.*
	SELECT  @ClientID, 
			@FileName AS [FileName],
			'ARUCS' AS [FailureFileType],
			ROW_NUMBER() OVER(ORDER BY (SELECT 1)) AS [RowNum],
			n.c.value('(ReceiverAccount/@number)[1]' , 'varchar(2000)') AS [PayerAccountNumber],
			REPLACE(n.c.value('(ReceiverAccount/@sortCode)[1]' , 'varchar(2000)'), '-', '') AS [PayerSortCode],
			n.c.value('(ReceiverAccount/@name)[1]' , 'varchar(2000)') AS [PayerName],
			(n.c.value('(@valueOf)[1]' , 'NUMERIC(18,2)'))*-1.00 AS [Amount],
			NULL AS [OriginalProcessingDate],
			n.c.value('(@returnDescription)[1]' , 'varchar(2000)') AS [ReasonDescription],
			n.c.value('(@returnCode)[1]' , 'varchar(2000)') AS [ReasonCode],
			LTRIM(RTRIM(n.c.value('(@ref)[1]' , 'varchar(2000)'))) AS [Reference],
			1 AS [ImportStatusID],
			dbo.fn_GetDate_Local() AS [ImportedDate], 
			'' AS [Comments]
	FROM @XML.nodes('//BACSDocument/Data/ARUCS/Advice/OriginatingAccountRecords/OriginatingAccountRecord/ReturnedCreditItem') n(c)
	OPTION (OPTIMIZE FOR ( @XML = NULL ))

	EXEC Billing__ReconcilePaymentFailures @ClientID

	SELECT *
	FROM BacsPaymentFailures b
	WHERE b.FileName = @FileName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportARUCSData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_ImportARUCSData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportARUCSData] TO [sp_executeall]
GO
