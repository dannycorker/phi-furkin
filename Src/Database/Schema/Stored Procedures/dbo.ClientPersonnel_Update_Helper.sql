SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- ALTER date: 2013-03-08
-- Description:	Allows a quick update to key ClientPersonnel details that are trigger-protected
-- JWG 2017-03-17 #42393 Cannot have these distributed transactions from other DB clusters
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel_Update_Helper]
(
	@ClientPersonnelID INT,
	@EmailAddress VARCHAR (255),
	@ThirdPartySystemId INT
)
AS
BEGIN
	-- If we don't have an outer tranasction e.g. from the lead event 
	/*DECLARE @TranCount INT = @@TRANCOUNT
	IF @TranCount = 0
	BEGIN
		BEGIN TRAN
	END*/
	
	BEGIN TRY

	-- Updates the master record
	EXEC AquariusMaster.dbo.AQ_ClientPersonnel_Update @ClientPersonnelID, @EmailAddress, @ThirdPartySystemId
	
	-- Set context info to allow access to table
	DECLARE @ContextInfo VARBINARY(100) = CAST('ClientPersonnel' AS VARBINARY)
	SET CONTEXT_INFO @ContextInfo
	
	
	-- Modify the updateable columns
	UPDATE dbo.ClientPersonnel
	SET EmailAddress = @EmailAddress, ThirdPartySystemId = @ThirdPartySystemId
	WHERE ClientPersonnelID = @ClientPersonnelID 
	
	
	-- If we don't have an outer tran we can commit here
	/*IF @TranCount = 0
	BEGIN
		COMMIT
	END*/
	
	-- Clear the things we set above
	SET CONTEXT_INFO 0x0
	
	END TRY			
	BEGIN CATCH    
		
		-- If we have any tran open then we need to rollback
		/*IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK
		END*/
		
		-- Clear the things we set above
		SET CONTEXT_INFO 0x0
		
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
		DECLARE @ErrorState INT = ERROR_STATE()

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
		
		
	END CATCH
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Update_Helper] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Update_Helper] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Update_Helper] TO [sp_executeall]
GO
