SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-19
-- Description:	Gets the status of the policy
-- Used by:		DF 147678
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetPolicyStatus] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @MatterID INT
	SELECT TOP 1 @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	SELECT	PolicyStart, PolicyEnd,
			CASE
				WHEN PolicyStatusID = 43003 THEN 1
				ELSE 0
			END AS Cancelled
	FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
	 
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicyStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetPolicyStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicyStatus] TO [sp_executeall]
GO
