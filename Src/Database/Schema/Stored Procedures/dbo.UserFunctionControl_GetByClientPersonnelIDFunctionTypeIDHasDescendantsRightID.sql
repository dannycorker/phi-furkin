SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserFunctionControl table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_GetByClientPersonnelIDFunctionTypeIDHasDescendantsRightID]
(

	@ClientPersonnelID int   ,

	@FunctionTypeID int   ,

	@HasDescendants int   ,

	@RightID int   
)
AS


				SELECT
					[UserFunctionControlID],
					[ClientPersonnelID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[UserFunctionControl] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelID] = @ClientPersonnelID
					AND [FunctionTypeID] = @FunctionTypeID
					AND [HasDescendants] = @HasDescendants
					AND [RightID] = @RightID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_GetByClientPersonnelIDFunctionTypeIDHasDescendantsRightID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_GetByClientPersonnelIDFunctionTypeIDHasDescendantsRightID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_GetByClientPersonnelIDFunctionTypeIDHasDescendantsRightID] TO [sp_executeall]
GO
