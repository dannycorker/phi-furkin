SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-04
-- Description:	Add a note and force SAN
-- 2018-12-03 JEL removed pppsID from selects so the sums actually group 
-- 2019-07-15 JML #57769 removed block of code setting PaymentGroupedIntoID which we think is no longer used.
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReconcileOneOffPayment]
(
	@OneOffPaymnents dbo.tvpVarchar10 READONLY /*PuchaseProductPaymentScheduleID,AccountID,TakePaymentNow*/
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @CurrentProduct INT,
			@CurrentAccount INT,
			@EarliestCoverFrom DATE ,
			@EarliestCoverTo DATE ,
			@ParentID INT,
			@LogXml XML,
			@Now DATE = dbo.fn_GetDate_Local(),
			@ClientID	INT = dbo.fnGetPrimaryClientID()
	
	SELECT @LogXml =
	(
		SELECT	* 
		FROM	@OneOffPaymnents
		FOR XML AUTO
	)
	
	--Askbill 138253 

	EXEC _C00_LogItXML @ClientID, 44412, '_C600_ReconcileOneOffPayment', @LogXml
	
	/*First update all payments with a scheduled date in the future that we are taking now as ignore*/  
	UPDATE p
	SET PaymentStatusID = 3
	FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
	INNER JOIN @OneOffPaymnents o on o.Val1 = CAST(p.PurchasedProductPaymentScheduleID AS VARCHAR)
	Where o.Val3 = '1'
	and p.PaymentStatusID in (1,5) -- Status in new/retry only
	
	/*Recreate the initial Table with productID*/ 
	DECLARE @OneOffPaymentsPr TABLE (PurchaseProductPaymentScheduleID INT, PurchaseProduct INT, AccountID INT, TakePaymentNow BIT) 
	INSERT INTO @OneOffPaymentsPr (PurchaseProductPaymentScheduleID,PurchaseProduct,AccountID,TakePaymentNow) 
	SELECT o.Val1,p.PurchasedProductID,o.Val2,1  
	FROM @OneOffPaymnents o 
	INNER JOIN dbo.PurchasedProductPaymentSchedule p WITH (NOLOCK) on CAST(p.PurchasedProductPaymentScheduleID AS VARCHAR) = o.Val1 --p.PurchasedProductPaymentScheduleID = o.AnyID1
	Where o.Val3 = '1' 
	
	/*Get a list of the purchaseproducts we want*/ 
	DECLARE @ProductsTable TABLE (PurchaseProductID INT, AccountID INT , Processed BIT) 
	INSERT INTO @ProductsTable (PurchaseProductID,AccountID,Processed)
	SELECT DISTINCT o.PurchaseProduct,o.AccountID,0 
	FROM @OneOffPaymentsPr  o

	/*Now prepare the new rows for insert, where product and account are the same we group these rows*/ 
	DECLARE @PaymentRowsToInsert TABLE (PurchaseProductID INT, AccountID INT, PaymentGross DECIMAL (18,2),PaymentNet DECIMAL (18,2),PaymentVAT DECIMAL (18,2),ParentID INT, CoverFrom DATE, CoverTo DATE, CustomerID INT, PurchasedProductPaymentParentID INT, ClientAccountID INT, TransactionFee NUMERIC(18,2)) 
	
	/*Now Loop through them all to get the row for each we want to insert*/ 	
	WHILE EXISTS (SELECT * FROM @ProductsTable Where Processed = 0) 
	BEGIN 
		
		SELECT TOP 1 @CurrentAccount = p.AccountID, @CurrentProduct = p.PurchaseProductID 
		FROM @ProductsTable p
		Where p.Processed = 0 
		
		SELECT @EarliestCoverFrom =  MIN(ps.CoverFrom), @EarliestCoverTo = MAX(ps.CoverTo)  FROM PurchasedProductPaymentSchedule ps
		INNER JOIN @OneOffPaymentsPr o on o.PurchaseProductPaymentScheduleID = ps.PurchasedProductPaymentScheduleID 
		where o.PurchaseProduct = @CurrentProduct 
		and o.AccountID = @CurrentAccount 
		
		/*For the current product and accout, sum all of the payments we are taking now*/
		INSERT INTO @PaymentRowsToInsert (PaymentGross,PaymentNet,PaymentVAT,AccountID,PurchaseProductID, CoverFrom, CoverTo, CustomerID, ClientAccountID, TransactionFee)
		SELECT SUM(ps.PaymentGross),SUM(ps.PaymentNet), SUM(ps.PaymentVAT) ,@CurrentAccount,@CurrentProduct,@EarliestCoverFrom,@EarliestCoverTo , ps.CustomerID,  ps.ClientAccountID, SUM(ps.TransactionFee)
		FROM PurchasedProductPaymentSchedule ps WITH (NOLOCK) 
		INNER JOIN @OneOffPaymentsPr o on o.PurchaseProductPaymentScheduleID = ps.PurchasedProductPaymentScheduleID 
		WHERE o.PurchaseProduct = @CurrentProduct 
		and o.AccountID = @CurrentAccount
		GROUP BY ps.CustomerID, ps.ClientAccountID
		
		/*Now insert the rows to ppps*/ 
		INSERT INTO PurchasedProductPaymentSchedule (ClientID,CustomerID,AccountID,PurchasedProductID,ActualCollectionDate,CoverFrom,CoverTo,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
		SELECT @ClientID,p.CustomerID, p.AccountID,p.PurchaseProductID,dbo.fn_GetDate_Local(),p.CoverFrom,p.CoverTo,dbo.fn_GetDate_Local(),p.PaymentNet,p.PaymentVAT,p.PaymentGross,1,44412, dbo.fn_GetDate_Local(), p.PurchasedProductPaymentParentID, 3, p.ClientAccountID, p.TransactionFee
		FROM @PaymentRowsToInsert p
		
		/*Removed the follwoign block off the back of ticket #57769 - this column appears to be redunant but when populated it seems to be stopping certain processes*/
		/*
			2021-01-21 ACE
			Reintroduced this as it's impossible to figure out if an arrear (final failure) 
			has been taken...
		*/
		SELECT @ParentID = SCOPE_IDENTITY()

		/*Parent for new missed payment row must be the same as the origional missed payment*/ 
		UPDATE PurchasedProductPaymentSchedule 
		SET PaymentGroupedIntoID = @ParentID 
		FROM PurchasedProductPaymentSchedule p 
		INNER JOIN @OneOffPaymentsPr o on o.PurchaseProductPaymentScheduleID = p.PurchasedProductPaymentScheduleID 
		WHERE o.PurchaseProduct = @CurrentProduct 
		and o.AccountID = @CurrentAccount

		/*2018-06-04 ACE - Stolen from C384 DB..*/
		DELETE FROM @PaymentRowsToInsert
		
		UPDATE @ProductsTable 
		SET Processed = 1 
		WHERE PurchaseProductID = @CurrentProduct 
		and AccountID = @CurrentAccount

		/*2018-04-17 ACE moved this into the while loop...*/
		EXEC Billing__RebuildCustomerPaymentSchedule  @CurrentAccount ,@Now, 44412

	END 

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReconcileOneOffPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReconcileOneOffPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReconcileOneOffPayment] TO [sp_executeall]
GO
