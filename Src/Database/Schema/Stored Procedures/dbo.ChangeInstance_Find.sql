SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChangeInstance table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeInstance_Find]
(

	@SearchUsingOR bit   = null ,

	@ChangeInstanceID int   = null ,

	@ClientID int   = null ,

	@ChangeDefinitionID int   = null ,

	@ChangeTypeID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@Deleted bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChangeInstanceID]
	, [ClientID]
	, [ChangeDefinitionID]
	, [ChangeTypeID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Deleted]
    FROM
	[dbo].[ChangeInstance] WITH (NOLOCK) 
    WHERE 
	 ([ChangeInstanceID] = @ChangeInstanceID OR @ChangeInstanceID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ChangeDefinitionID] = @ChangeDefinitionID OR @ChangeDefinitionID IS NULL)
	AND ([ChangeTypeID] = @ChangeTypeID OR @ChangeTypeID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChangeInstanceID]
	, [ClientID]
	, [ChangeDefinitionID]
	, [ChangeTypeID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Deleted]
    FROM
	[dbo].[ChangeInstance] WITH (NOLOCK) 
    WHERE 
	 ([ChangeInstanceID] = @ChangeInstanceID AND @ChangeInstanceID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ChangeDefinitionID] = @ChangeDefinitionID AND @ChangeDefinitionID is not null)
	OR ([ChangeTypeID] = @ChangeTypeID AND @ChangeTypeID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeInstance_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeInstance_Find] TO [sp_executeall]
GO
