SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-05-2013
-- Description:	Adds a record to the client billing table for sms without event
-- Modified by PR 27/01/2014 added MobileTelephone column
-- =============================================
CREATE PROCEDURE [dbo].[BillClientForSmsWithoutEvent]
	@ClientID INT,
	@ClientPersonnelID INT,
	@Message VARCHAR(MAX),
	@MobileTelephone VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @ClientID = 2
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes, MobileTelephone)
		VALUES (@ClientID, 55, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 0.0, @Message, @MobileTelephone)
	END
	ELSE
	BEGIN		
		
		DECLARE @CustomerID INT = -1
		
		IF @ClientID=261
		BEGIN
			
			SELECT @CustomerID=CustomerID FROM Customers WITH (NOLOCK) WHERE ClientID=@ClientID AND MobileTelephone=@MobileTelephone
			
			IF @CustomerID IS NULL OR @CustomerID=-1 -- if customer not found then create the customer
			BEGIN
								
				DECLARE @LeadTypeID INT = 1372 -- Dial A Phone LeadType
				DECLARE @TitleID INT  = 0
				DECLARE @FirstName VARCHAR(100) = ''
				DECLARE @LastName VARCHAR(100) = ''
				DECLARE @EmailAddress VARCHAR(255)
				DECLARE @IsBusiness BIT = 0
				DECLARE @BusinessName VARCHAR(200)
				DECLARE @Address1 VARCHAR(200)
				DECLARE @Address2 VARCHAR(200)
				DECLARE @Town VARCHAR(200)
				DECLARE @County VARCHAR(200)
				DECLARE @PostCode VARCHAR(200)
				DECLARE @SitePhone VARCHAR(200)
				DECLARE @WhoCreated INT = @ClientPersonnelID
				DECLARE @Website VARCHAR(200)
				DECLARE @CountryID INT
				DECLARE @CreateLeadRegardless INT = 1
				DECLARE @ReturnNewMatterID INT = 0
				DECLARE @AddProcessStart INT = 1


				EXECUTE @CustomerID = dbo._C00_CreateNewCustomer
				   @ClientID
				  ,@LeadTypeID
				  ,@TitleID
				  ,@FirstName
				  ,@LastName
				  ,@EmailAddress
				  ,@IsBusiness
				  ,@BusinessName
				  ,@Address1
				  ,@Address2
				  ,@Town
				  ,@County
				  ,@PostCode
				  ,@SitePhone
				  ,@WhoCreated
				  ,@Website
				  ,@CountryID
				  ,@CreateLeadRegardless
				  ,@ReturnNewMatterID
				  ,@AddProcessStart
				  ,@MobileTelephone
				  
			END
		END		
	
		
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes, MobileTelephone, SourceObjectUID)
		VALUES (@ClientID, 55, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 4.0, @Message, @MobileTelephone, @CustomerID)	

	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForSmsWithoutEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillClientForSmsWithoutEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForSmsWithoutEvent] TO [sp_executeall]
GO
