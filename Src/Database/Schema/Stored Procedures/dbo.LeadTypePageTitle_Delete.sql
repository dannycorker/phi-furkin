SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitle_Delete]
(

	@LeadTypeID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitle] WITH (ROWLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitle_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Delete] TO [sp_executeall]
GO
