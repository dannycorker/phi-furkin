SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 09-07-2013
-- Description:	Gets the client billing detail record by the message guid
-- =============================================
CREATE PROCEDURE [dbo].[ClientBillingDetail__GetByMessageGUID]

	@MessageGUID VARCHAR(36)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM ClientBillingDetail WITH (NOLOCK) 
	WHERE MessageGUID = @MessageGUID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[ClientBillingDetail__GetByMessageGUID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientBillingDetail__GetByMessageGUID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientBillingDetail__GetByMessageGUID] TO [sp_executeall]
GO
