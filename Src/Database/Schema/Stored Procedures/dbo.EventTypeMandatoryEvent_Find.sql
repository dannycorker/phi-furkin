SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeMandatoryEvent table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeMandatoryEventID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@ToEventTypeID int   = null ,

	@RequiredEventID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeMandatoryEventID]
	, [ClientID]
	, [LeadTypeID]
	, [ToEventTypeID]
	, [RequiredEventID]
    FROM
	[dbo].[EventTypeMandatoryEvent] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeMandatoryEventID] = @EventTypeMandatoryEventID OR @EventTypeMandatoryEventID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ToEventTypeID] = @ToEventTypeID OR @ToEventTypeID IS NULL)
	AND ([RequiredEventID] = @RequiredEventID OR @RequiredEventID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeMandatoryEventID]
	, [ClientID]
	, [LeadTypeID]
	, [ToEventTypeID]
	, [RequiredEventID]
    FROM
	[dbo].[EventTypeMandatoryEvent] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeMandatoryEventID] = @EventTypeMandatoryEventID AND @EventTypeMandatoryEventID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ToEventTypeID] = @ToEventTypeID AND @ToEventTypeID is not null)
	OR ([RequiredEventID] = @RequiredEventID AND @RequiredEventID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Find] TO [sp_executeall]
GO
