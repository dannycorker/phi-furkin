SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Location table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Location_GetByLocationID]
(

	@LocationID int   
)
AS


				SELECT
					[LocationID],
					[ClientID],
					[LocationName],
					[Note],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Location] WITH (NOLOCK) 
				WHERE
										[LocationID] = @LocationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Location_GetByLocationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Location_GetByLocationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_GetByLocationID] TO [sp_executeall]
GO
