SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LookupListItems table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_Find]
(

	@SearchUsingOR bit   = null ,

	@LookupListItemID int   = null ,

	@LookupListID int   = null ,

	@ItemValue varchar (500)  = null ,

	@ClientID int   = null ,

	@Enabled bit   = null ,

	@SortOrder int   = null ,

	@ValueInt int   = null ,

	@ValueMoney money   = null ,

	@ValueDate date   = null ,

	@ValueDateTime datetime2   = null ,

	@SourceID int   = null ,

	@IsShared bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LookupListItemID]
	, [LookupListID]
	, [ItemValue]
	, [ClientID]
	, [Enabled]
	, [SortOrder]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [SourceID]
	, [IsShared]
    FROM
	dbo.fnLookupListItemsShared(@ClientID)
    WHERE 
	 ([LookupListItemID] = @LookupListItemID OR @LookupListItemID IS NULL)
	AND ([LookupListID] = @LookupListID OR @LookupListID IS NULL)
	AND ([ItemValue] = @ItemValue OR @ItemValue IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([SortOrder] = @SortOrder OR @SortOrder IS NULL)
	AND ([ValueInt] = @ValueInt OR @ValueInt IS NULL)
	AND ([ValueMoney] = @ValueMoney OR @ValueMoney IS NULL)
	AND ([ValueDate] = @ValueDate OR @ValueDate IS NULL)
	AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LookupListItemID]
	, [LookupListID]
	, [ItemValue]
	, [ClientID]
	, [Enabled]
	, [SortOrder]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [SourceID]
	, [IsShared]
    FROM
	dbo.fnLookupListItemsShared(@ClientID) 
    WHERE 
	 ([LookupListItemID] = @LookupListItemID AND @LookupListItemID is not null)
	OR ([LookupListID] = @LookupListID AND @LookupListID is not null)
	OR ([ItemValue] = @ItemValue AND @ItemValue is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([SortOrder] = @SortOrder AND @SortOrder is not null)
	OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
	OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
	OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
	OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Find] TO [sp_executeall]
GO
