SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 23 February 2007

-- Created By:  ()
-- Purpose: Select records from the EventType table through an index
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType__GetByClientIDAndEventSubtypeID]
(

	@ClientID int, @EventSubtypeID int    
)
AS


		SELECT *
		FROM
			dbo.fnEventTypeShared(@ClientID)
		WHERE
			[ClientID] = @ClientID 
		AND [EventSubtypeID] = @EventSubtypeID
		AND [Enabled] = 1 

		Select @@ROWCOUNT





GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByClientIDAndEventSubtypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByClientIDAndEventSubtypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByClientIDAndEventSubtypeID] TO [sp_executeall]
GO
