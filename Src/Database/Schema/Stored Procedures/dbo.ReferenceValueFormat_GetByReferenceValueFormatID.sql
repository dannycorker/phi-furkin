SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReferenceValueFormat table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueFormat_GetByReferenceValueFormatID]
(

	@ReferenceValueFormatID int   
)
AS


				SELECT
					[ReferenceValueFormatID],
					[ClientID],
					[ReferenceValueName],
					[ReferenceValueDescription],
					[IntegerSectionSize],
					[IntegerStartPoint],
					[DateFormat],
					[YearFormat],
					[DelimiterID],
					[Section1TypeID],
					[Section1DetailFieldID],
					[Section2TypeID],
					[Section2DetailFieldID],
					[Section3TypeID],
					[Section3DetailFieldID],
					[Section4TypeID],
					[Section4DetailFieldID],
					[Section5TypeID],
					[Section5DetailFieldID],
					[LeftPadIntegerSection],
					[IncludeBlankSections],
					[EnforceUniqueness],
					[SourceID]
				FROM
					[dbo].[ReferenceValueFormat] WITH (NOLOCK) 
				WHERE
										[ReferenceValueFormatID] = @ReferenceValueFormatID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_GetByReferenceValueFormatID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueFormat_GetByReferenceValueFormatID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_GetByReferenceValueFormatID] TO [sp_executeall]
GO
