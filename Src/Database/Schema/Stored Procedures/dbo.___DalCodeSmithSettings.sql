SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
	Author	: Jan Wilson
	Date	: 24th November 2011
	
	Description: Returns the CodeSmith DAL generation property settings
	Returns: XML Document (note: does not include the prolog that is required for CodeSmith)
*/

CREATE PROCEDURE [dbo].[___DalCodeSmithSettings]
AS
BEGIN

SET QUOTED_IDENTIFIER ON;
SET NOCOUNT ON;

DECLARE @DATA_XML XML
SET @DATA_XML = '<root>' +(SELECT DISTINCT TableName, IsDALTable FROM MigrationTable WHERE (IsDALTable = 1) FOR XML AUTO) + '</root>'

SELECT
	CAST(
	@DATA_XML.query(
	'
declare default element namespace "http://www.codesmithtools.com/schema/csp.xsd";	
<codeSmith>
  <variables>
    <add key="ConnectionString1" value="Data Source=A1;Initial Catalog=AquariusDev;uid=sa;pwd=jim" />
  </variables>
  <propertySets>
	  <propertySet name="AquariumNetTiers.txt" output="AquariumNetTiers.txt" template="AquariumNetTiers.cst">
		<property name="ExecuteSql">False</property>
		<property name="SQLFolderName">SQL</property>
		<property name="ViewReport">True</property>
		<property name="AutoIncrementBuildVersion">True</property>
		<property name="LaunchVisualStudio">False</property>
		<property name="IncludeUnitTest">None</property>
		<property name="IncludeComponentLayer">None</property>
		<property name="IncludeWCFDataAttributes">False</property>
		<property name="SerializeEntityState">False</property>
		<property name="EntLibVersion">v4_1</property>
		<property name="VisualStudioVersion">v2008</property>
		<property name="DotNetVersion">v3_5</property>
		<property name="IncludeXmlAttributes">True</property>
		<property name="CustomCodeFolderName">App_Code</property>
		<property name="EqualitySemantics">Value</property>
		<property name="TimeStandard">Local</property>
		<property name="BusinessLogicLayerNameSpace">BLL</property>
		<property name="DataAccessLayerNameSpace">DAL</property>
		<property name="UnitTestsNameSpace">UnitTests</property>
		<property name="SignAssembly">True</property>
		<property name="GenerateWebLibrary">False</property>
		<property name="IncludeDesignTimeSupport">False</property>
		<property name="DataSourceEnableTransactionDefault">True</property>
		<property name="GenerateWebservice">False</property>
		<property name="AttemptCreateLocalVirtualDirectory">False</property>
		<property name="GenerateWebsite">False</property>
		<property name="UseWebAppProject">False</property>
		<property name="OverwriteWebConfig">False</property>
		<property name="IncludeAtlasLibrary">False</property>
		<property name="IncludeAtlasToolkit">False</property>
		<property name="GenerateWebsiteAdmin">False</property>
		<property name="DateFormat">MM/dd/yyyy</property>
		<property name="RetryEnabled">False</property>
		<property name="RetryMaxAttempts">5</property>
		<property name="RetrySleepTime">1000</property>
		<property name="LibraryPath">References</property>
		<property name="IncludeCustoms">True</property>
		<property name="CustomNonMatchingReturnType">DataSet</property>
		<property name="IncludeDrop">True</property>
		<property name="DropStyle">Entity</property>
		<property name="IncludeInsert">True</property>
		<property name="IncludeUpdate">True</property>
		<property name="IncludeSave">True</property>
		<property name="IncludeDelete">True</property>
		<property name="IncludeGet">True</property>
		<property name="IncludeGetList">True</property>
		<property name="IncludeGetListByFK">True</property>
		<property name="IncludeGetListByIX">True</property>
		<property name="IncludeFind">True</property>
		<property name="IncludeManyToMany">True</property>
		<property name="IncludeRelations">True</property>
		<property name="IsolationLevel">None</property>
		<property name="UseTimestampConcurrency">True</property>
		<property name="InsertSuffix">_Insert</property>
		<property name="UpdateSuffix">_Update</property>
		<property name="DeleteSuffix">_Delete</property>
		<property name="SelectSuffix">_Get</property>
		<property name="SelectAllSuffix">_List</property>
		<property name="FindSuffix">_Find</property>
		<property name="GenerateWinLibrary">False</property>
		<property name="RootNameSpace">AqNET</property>
		<property name="OutputDirectory">..\Output</property>
		<property name="MappingFile">..\Output\AquariusDevMapping.config</property>
		<property name="ComponentLayerNameSpace" />
	    
		<property name="SourceTables">
		  <connectionString>$(ConnectionString1)</connectionString>
		  <providerType>SchemaExplorer.SqlSchemaProvider,SchemaExplorer.SqlSchemaProvider</providerType>
			<tableList>
 			{for $p in /*/*[@IsDALTable="1"]
				return
					<table>
						<owner>dbo</owner>
						<name>{data($p/@TableName)}</name>
					</table>
			}
			</tableList>
		</property>
	    
		<property name="EnumTables">
		  <connectionString>$(ConnectionString1)</connectionString>
		  <providerType>SchemaExplorer.SqlSchemaProvider,SchemaExplorer.SqlSchemaProvider</providerType>
			<tableList>
			  <table>
				<owner>dbo</owner>
				<name>AutoAdjudicationFieldType</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>AutoAdjudicationType</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>BillStatus</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>CaseTransferStatus</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>DataLoaderDecodeType</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>DataLoaderFileFormat</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>DataLoaderFileStatus</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>DataLoaderObjectAction</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>DataLoaderObjectType</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>DataLoaderSectionLocaterType</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>EventSubtype</name>
			  </table>
			  <table>
				<owner>dbo</owner>
				<name>TemplateType</name>
			  </table>
			</tableList>
		</property>

		<property name="WebServiceOutputPath">..\Output\WebServices</property>
		<property name="WebServiceUrl">http://localhost/AquariusDevServices</property>
		<property name="WebAdminSiteName">AquariusDev Application</property>
		<property name="ValidationType">NetTiers</property>
		<property name="GenerateWebSecurity">False</property>
		<property name="UseMD5Hash">False</property>
		<property name="ChooseSourceDatabase">
		  <connectionString>Data Source=A1;Initial Catalog=AquariusDev;uid=sa;pwd=jim</connectionString>
		  <providerType>SchemaExplorer.SqlSchemaProvider,SchemaExplorer.SqlSchemaProvider</providerType>
		</property>
		<property name="CompanyName">Aquarium Software</property>
		<property name="CompanyURL">https://www.aquarium-software.com</property>
		<property name="RetrySleepStyle">Constant</property>
		<property name="IncludeGeneratedDate">False</property>
		<property name="NameConversion">None</property>
		<property name="IncludeDatabaseFeatures">None</property>
		<property name="MethodNames">
	      <MethodNamesProperty xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="">
			<Get>Get</Get>
			<GetAll>GetAll</GetAll>
			<GetPaged>GetPaged</GetPaged>
			<Find>Find</Find>
			<Insert>Insert</Insert>
			<Update>Update</Update>
			<Save>Save</Save>
			<Delete>Delete</Delete>
			<DeepLoad>DeepLoad</DeepLoad>
			<DeepSave>DeepSave</DeepSave>
			<GetTotalItems>GetTotalItems</GetTotalItems>
			<BulkInsert>BulkInsert</BulkInsert>
		  </MethodNamesProperty>
		</property>
		<property name="StrippedTablePrefixes">tbl;tbl_</property>
		<property name="StrippedTableSuffixes">_t</property>
		<property name="EntityFormat">{{0}}</property>
		<property name="EntityKeyFormat">{{0}}Key</property>
		<property name="CollectionFormat">{{0}}Collection</property>
		<property name="GenericViewFormat">VList&lt;{{0}}&gt;</property>
		<property name="GenericListFormat">TList&lt;{{0}}&gt;</property>
		<property name="ProviderFormat">{{0}}Provider</property>
		<property name="InterfaceFormat">I{{0}}</property>
		<property name="BaseClassFormat">{{0}}Base</property>
		<property name="EnumFormat">{{0}}List</property>
		<property name="ManyToManyFormat">{{0}}From{{1}}</property>
		<property name="ServiceClassNameFormat">{{0}}Service</property>
		<property name="ColumnClassNameFormat">{{0}}Column</property>
		<property name="ComparerClassNameFormat">{{0}}Comparer</property>
		<property name="EventHandlerClassNameFormat">{{0}}EventHandler</property>
		<property name="EventArgsClassNameFormat">{{0}}EventArgs</property>
		<property name="ParseDbColDefaultVal">False</property>
		<property name="ChangeUnderscoreToPascalCase">True</property>
		<property name="SafeNamePrefix">SafeName_</property>
		<property name="UsePascalCasing">Style1</property>
		<property name="AliasFilePath" />
		<property name="ProcedurePrefix" />
		<property name="CustomProcedureStartsWith">{{0}}__</property>
		<property name="CSPUseDefaultValForNonNullableTypes">True</property>
		<property name="AllowCustomProcMultipleResults">True</property>

	  </propertySet>
	</propertySets>
</codeSmith>	
') AS VARCHAR(MAX))

END




GO
GRANT VIEW DEFINITION ON  [dbo].[___DalCodeSmithSettings] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[___DalCodeSmithSettings] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[___DalCodeSmithSettings] TO [sp_executeall]
GO
