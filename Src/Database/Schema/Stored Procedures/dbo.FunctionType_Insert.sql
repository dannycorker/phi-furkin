SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FunctionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_Insert]
(

	@FunctionTypeID int   ,

	@ModuleID int   ,

	@ParentFunctionTypeID int   ,

	@FunctionTypeName varchar (50)  ,

	@Level int   ,

	@IsVirtual bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[FunctionType]
					(
					[FunctionTypeID]
					,[ModuleID]
					,[ParentFunctionTypeID]
					,[FunctionTypeName]
					,[Level]
					,[IsVirtual]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@FunctionTypeID
					,@ModuleID
					,@ParentFunctionTypeID
					,@FunctionTypeName
					,@Level
					,@IsVirtual
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Insert] TO [sp_executeall]
GO
