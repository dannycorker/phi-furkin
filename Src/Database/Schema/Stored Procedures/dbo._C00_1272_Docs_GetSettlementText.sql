SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-04-03
-- Description:	Gets all the text for the settlement so it can be edited
-- Used by:		df_157745
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Docs_GetSettlementText] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	mdvDead.MatterDetailValueID AS DeadPetID, mdvDead.DetailValue AS DeadPetValue,
			mdvSummary.MatterDetailValueID AS SummaryID, mdvSummary.DetailValue AS SummaryValue
	FROM dbo.Matter m 
	LEFT JOIN dbo.MatterDetailValues mdvDead WITH (NOLOCK) ON m.MatterID = mdvDead.MatterID AND mdvDead.DetailFieldID = 156628
	LEFT JOIN dbo.MatterDetailValues mdvSummary WITH (NOLOCK) ON m.MatterID = mdvSummary.MatterID AND mdvSummary.DetailFieldID = 152764
	WHERE m.MatterID = @MatterID

	SELECT r.TableRowID AS ID, tdv.DetailValue AS Value
	FROM dbo.TableRows r
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID AND tdv.DetailFieldID = 148348
	WHERE r.MatterID = @MatterID
	AND r.DetailFieldID = 152765

	SELECT r.TableRowID AS ID, tdv.DetailValue AS Value
	FROM dbo.TableRows r
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID AND tdv.DetailFieldID = 148348
	WHERE r.MatterID = @MatterID
	AND r.DetailFieldID = 148349


END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_GetSettlementText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Docs_GetSettlementText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_GetSettlementText] TO [sp_executeall]
GO
