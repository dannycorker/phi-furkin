SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ActiveSession table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_Get_List]

AS


				
				SELECT
					[SessionID],
					[EmailAddress],
					[ThirdPartySystemId]
				FROM
					[dbo].[ActiveSession] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Get_List] TO [sp_executeall]
GO
