SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SMSQuestionnaire table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_Insert]
(

	@SMSQuestionnaireID int    OUTPUT,

	@ClientID int   ,

	@Title varchar (250)  ,

	@Description varchar (2000)  ,

	@Enabled bit   ,

	@ToTarget varchar (250)  ,

	@CreateCustomerIfNotFound bit   ,

	@UseSentimentAnalysis bit   ,

	@RuleSetID int   ,

	@IsVoice bit   ,

	@Timeout int   ,

	@Repeat int   ,

	@RepeatPrefix varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[SMSQuestionnaire]
					(
					[ClientID]
					,[Title]
					,[Description]
					,[Enabled]
					,[ToTarget]
					,[CreateCustomerIfNotFound]
					,[UseSentimentAnalysis]
					,[RuleSetID]
					,[IsVoice]
					,[Timeout]
					,[Repeat]
					,[RepeatPrefix]
					)
				VALUES
					(
					@ClientID
					,@Title
					,@Description
					,@Enabled
					,@ToTarget
					,@CreateCustomerIfNotFound
					,@UseSentimentAnalysis
					,@RuleSetID
					,@IsVoice
					,@Timeout
					,@Repeat
					,@RepeatPrefix
					)
				-- Get the identity value
				SET @SMSQuestionnaireID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Insert] TO [sp_executeall]
GO
