SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ApplicationBranding table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ApplicationBranding_Delete]
(

	@ApplicationBrandingID int   
)
AS


				DELETE FROM [dbo].[ApplicationBranding] WITH (ROWLOCK) 
				WHERE
					[ApplicationBrandingID] = @ApplicationBrandingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ApplicationBranding_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Delete] TO [sp_executeall]
GO
