SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CheckMateField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateField_Update]
(

	@CheckMateFieldID int   ,

	@CheckMateFieldName varchar (50)  ,

	@CheckMateFieldDescription varchar (250)  ,

	@QuestionTypeID int   ,

	@LookupListID int   ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CheckMateField]
				SET
					[CheckMateFieldName] = @CheckMateFieldName
					,[CheckMateFieldDescription] = @CheckMateFieldDescription
					,[QuestionTypeID] = @QuestionTypeID
					,[LookupListID] = @LookupListID
					,[IsEnabled] = @IsEnabled
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[CheckMateFieldID] = @CheckMateFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Update] TO [sp_executeall]
GO
