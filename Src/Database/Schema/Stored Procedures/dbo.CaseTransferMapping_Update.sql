SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CaseTransferMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_Update]
(

	@CaseTransferMappingID int   ,

	@ClientRelationshipID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@MatterID int   ,

	@NewClientID int   ,

	@NewCustomerID int   ,

	@NewLeadID int   ,

	@NewCaseID int   ,

	@NewMatterID int   ,

	@CaseTransferStatusID int   ,

	@NewLeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CaseTransferMapping]
				SET
					[ClientRelationshipID] = @ClientRelationshipID
					,[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[MatterID] = @MatterID
					,[NewClientID] = @NewClientID
					,[NewCustomerID] = @NewCustomerID
					,[NewLeadID] = @NewLeadID
					,[NewCaseID] = @NewCaseID
					,[NewMatterID] = @NewMatterID
					,[CaseTransferStatusID] = @CaseTransferStatusID
					,[NewLeadTypeID] = @NewLeadTypeID
				WHERE
[CaseTransferMappingID] = @CaseTransferMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Update] TO [sp_executeall]
GO
