SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 11/04/2016
-- Description:	Gets the resource list detail fields and values if there are any
-- =============================================
CREATE PROCEDURE [dbo].[ResourceList__GetFieldsAndValues]
	@DetailFieldID INT,
	@ResourceListID INT = 0
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @DetailFieldPageID INT
	
	SELECT @DetailFieldPageID=ResourceListDetailFieldPageID 
	FROM DetailFields WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID

    SELECT df.DetailFieldPageID, df.DetailFieldID, df.FieldCaption, df.QuestionTypeID, df.MaintainHistory, ISNULL(li.ItemValue, rdv.DetailValue) AS DetailValue FROM DetailFields df WITH (NOLOCK) 
	LEFT JOIN ResourceListDetailValues rdv WITH (NOLOCK) ON rdv.DetailFieldID = df.DetailFieldID AND rdv.ResourceListID=@ResourceListID
	LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
	LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND rdv.ValueInt = li.LookupListItemID	
	WHERE df.DetailFieldPageID=@DetailFieldPageID AND df.Enabled=1 ORDER BY df.FieldOrder ASC
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetFieldsAndValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList__GetFieldsAndValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetFieldsAndValues] TO [sp_executeall]
GO
