SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DocumentStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_Insert]
(

	@DocumentStandardTargetID int    OUTPUT,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@TemplateTypeID int   ,

	@Notes varchar (250)  ,

	@IsSpecial bit   ,

	@ExcelSheetLocation varchar (50)  ,

	@DocumentTypeVersionID int   
)
AS


				
				INSERT INTO [dbo].[DocumentStandardTarget]
					(
					[ClientID]
					,[DocumentTypeID]
					,[Target]
					,[ObjectName]
					,[PropertyName]
					,[TemplateTypeID]
					,[Notes]
					,[IsSpecial]
					,[ExcelSheetLocation]
					,[DocumentTypeVersionID]
					)
				VALUES
					(
					@ClientID
					,@DocumentTypeID
					,@Target
					,@ObjectName
					,@PropertyName
					,@TemplateTypeID
					,@Notes
					,@IsSpecial
					,@ExcelSheetLocation
					,@DocumentTypeVersionID
					)
				-- Get the identity value
				SET @DocumentStandardTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Insert] TO [sp_executeall]
GO
