SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2017-03-21
-- Description:	Get leadevent of a type for a case in the last 30 
--				seconds (based on app time)
--				Created For PR #42470
-- =============================================
CREATE PROCEDURE [dbo].[LeadEvent__CheckForLeadEvent]
	@CaseID INT,
	@EventTypeID INT,
	@WhenCreated DATETIME
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM LeadEvent le WITH (NOLOCK)
	WHERE le.CaseID = @CaseID
	AND le.EventTypeID = @EventTypeID
	AND le.WhenCreated >= @WhenCreated

END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__CheckForLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__CheckForLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__CheckForLeadEvent] TO [sp_executeall]
GO
