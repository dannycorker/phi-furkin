SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Simon Brusherr
-- Create date: 2011-07-15
-- Description:	Get Appointments for multiple users and date range - Extended GetDiaryAppointmentsByUser to handle multi user
-- =============================================
CREATE PROCEDURE [dbo].[GetDiaryAppointmentsMultiUser]
	-- Add the parameters for the stored procedure here
	@ClientPersonnelIDs dbo.TvpInt READONLY, 
	@FromDate datetime = '1901-01-01', 
	@ToDate datetime = '2499-01-01',
	@ShowCompleted bit,
	@ShowOverdue bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Reminders can exist for multiple personnel.
	-- Appointment only occurs once, no matter how many people are invited.
	SELECT 
		da.DiaryAppointmentID, 
		da.ClientID, 
		da.DiaryAppointmentTitle, 
		da.DiaryAppointmentText, 
		ClientPersonnel.UserName, 
		da.DueDate, 
		CASE da.Completed WHEN 1 THEN 'Yes' ELSE '' END as 'Completed', 
		da.LeadID, 
		da.CaseID, 
		dr.ReminderDate,
		l.LeadRef,
		c.CaseRef,
		da.ExportVersion,
		da.EndDate,
		da.AllDayEvent,
		u.UserName AS DiaryUser,
		u.ClientPersonnelID
	FROM DiaryReminder dr 
	INNER JOIN DiaryAppointment da ON da.DiaryAppointmentID = dr.DiaryAppointmentID 
	INNER JOIN ClientPersonnel ON da.CreatedBy = ClientPersonnel.ClientPersonnelID
	LEFT OUTER JOIN Lead l ON l.LeadID = da.LeadID 
	LEFT OUTER JOIN Cases c ON c.CaseID = da.CaseID 
	INNER JOIN @ClientPersonnelIDs ids ON dr.ClientPersonnelID = ids.AnyID
	INNER JOIN ClientPersonnel u ON dr.ClientPersonnelID = u.ClientPersonnelID
	--WHERE dr.ClientPersonnelID = @ClientPersonnelID 
	--AND da.DueDate BETWEEN @FromDate AND @ToDate 
	--AND da.Completed <= @ShowCompleted 
	WHERE da.Completed <= @ShowCompleted 
	AND 
	(
		(@ShowOverdue = 0 AND (da.DueDate BETWEEN @FromDate AND @ToDate OR @FromDate BETWEEN da.DueDate AND da.EndDate)) OR
		(@ShowOverdue = 1 AND (da.DueDate BETWEEN @FromDate AND @ToDate OR da.DueDate < dbo.fn_GetDate_Local()))
	)	
	ORDER BY da.DueDate ASC, dr.ReminderDate ASC, da.DiaryAppointmentTitle

END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDiaryAppointmentsMultiUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDiaryAppointmentsMultiUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDiaryAppointmentsMultiUser] TO [sp_executeall]
GO
