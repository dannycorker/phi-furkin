SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentTargetControl table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_GetByDocumentTypeID]
(

	@DocumentTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentTargetControlID],
					[ClientID],
					[DocumentTypeID],
					[LastParsed]
				FROM
					[dbo].[DocumentTargetControl] WITH (NOLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_GetByDocumentTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_GetByDocumentTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_GetByDocumentTypeID] TO [sp_executeall]
GO
