SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeEquation table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeEquation_GetByEventTypeEquationID]
(

	@EventTypeEquationID int   
)
AS


				SELECT
					[EventTypeEquationID],
					[ClientID],
					[EventTypeID],
					[DetailFieldID]
				FROM
					[dbo].[EventTypeEquation] WITH (NOLOCK) 
				WHERE
										[EventTypeEquationID] = @EventTypeEquationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_GetByEventTypeEquationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation_GetByEventTypeEquationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_GetByEventTypeEquationID] TO [sp_executeall]
GO
