SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Messages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_Get_List]

AS


				
				SELECT
					[MessageID],
					[ClientPersonnelIDTo],
					[ClientPersonnelIDFrom],
					[DateSent],
					[DateRead],
					[Status],
					[Subject],
					[MessageText],
					[DateReplied],
					[DateForwarded],
					[PreviousMessageID],
					[ClientID],
					[PortalUserIDTo],
					[PortalUserIDFrom]
				FROM
					[dbo].[Messages] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Get_List] TO [sp_executeall]
GO
