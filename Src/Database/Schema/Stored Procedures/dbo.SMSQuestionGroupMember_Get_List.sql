SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SMSQuestionGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_Get_List]

AS


				
				SELECT
					[SMSQuestionGroupMemberID],
					[ClientID],
					[SMSQuestionnaireID],
					[SMSQuestionID],
					[SMSQuestionGroupID]
				FROM
					[dbo].[SMSQuestionGroupMember] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Get_List] TO [sp_executeall]
GO
