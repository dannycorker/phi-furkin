SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 02/08/2017
-- Description:	Gets WorldPay Notifications that have not been 
--			    parsed by the script runner yet
-- Modified By JL 2018-05-24 So that card payments do not get stuck on a single payment
-- =============================================
CREATE PROCEDURE [dbo].[WorldPayNotifications__GetUnParsedNotifications]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM WorldPayNotification WITH (NOLOCK) 
	WHERE HasBeenParsed is null or HasBeenParsed=0
	ORDER BY WorldPayNotificationID DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayNotifications__GetUnParsedNotifications] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorldPayNotifications__GetUnParsedNotifications] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayNotifications__GetUnParsedNotifications] TO [sp_executeall]
GO
