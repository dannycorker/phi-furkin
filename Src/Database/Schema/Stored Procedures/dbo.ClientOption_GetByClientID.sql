SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOption table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOption_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientOptionID],
					[ClientID],
					[ZenDeskOff],
					[ZenDeskURLPrefix],
					[ZenDeskText],
					[ZenDeskTitle],
					[iDiaryOff],
					[WorkflowOff],
					[UserDirectoryOff],
					[eCatcherOff],
					[LeadAssignmentOff],
					[UserMessagesOff],
					[SystemMessagesOff],
					[UserPortalOff],
					[ReportSearchOff],
					[NormalSearchOff],
					[UseXero],
					[UseUltra],
					[UseCaseSummary],
					[UseMoreProminentReminders],
					[UseSMSSurvey],
					[UseNewAddLead],
					[DiallerInsertMethod],
					[DiallerPrimaryKey],
					[UseCHARMS],
					[TextMessageService],
					[UseThunderhead],
					[UseMemorableWordVerification],
					[ApplyInactivityTimeout],
					[InactivityTimeoutInSeconds],
					[InactivityRedirectUrl],
					[UseSentimentAnalysis],
					[ShowProcessInfoButton],
					[LatestRecordsFirst],
					[UseSecureMessage],
					[EventCommentTooltipOff],
					[EnableScripting],
					[EnableBilling],
					[UseDocumentTypeVersioning],
					[EnableSMSSTOP],
					[UseEngageMail],
					[PreventForcedLogout]
				FROM
					[dbo].[ClientOption] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOption_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_GetByClientID] TO [sp_executeall]
GO
