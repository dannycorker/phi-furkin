SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-28
-- Description:	Handle LeadClientOffice relationships when Lead assignment changes
-- =============================================
CREATE PROCEDURE [dbo].[Lead__HandleChangeOfAssignmentByUserID]
	@LeadID int,
	@NewClientPersonnelID int -- This can be null
	
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID int, @OldOfficeID int, @NewOfficeID int, @OldCount int

	IF (@LeadID IS NULL) OR (@LeadID < 1)
	BEGIN
		RETURN -1
	END

	SELECT @OldOfficeID = ClientOfficeID
	FROM ClientOfficeLead
	WHERE LeadID = @LeadID

	SELECT @OldCount = @@ROWCOUNT

	-- DELETE the existing record if necessary: if the new value of
	-- NewClientPersonnelID is null and we know that there is an existing record, delete it now.
	IF @NewClientPersonnelID IS NULL AND @OldCount > 0
	BEGIN 
		
		DELETE ClientOfficeLead 
		WHERE LeadID = @LeadID

	END

	-- Second task is to insert the new record if needed.
	IF @NewClientPersonnelID IS NOT NULL
	BEGIN 
		
		SELECT @NewOfficeID = ClientOfficeID, 
			   @ClientID = ClientID
		FROM ClientPersonnel 
		WHERE ClientPersonnelID = @NewClientPersonnelID

		IF (@OldOfficeID IS NULL) OR (@NewOfficeID <> @OldOfficeID)
		BEGIN 
			IF @OldCount > 0 
			BEGIN

				-- Update the existing record
				UPDATE ClientOfficeLead 
				SET ClientOfficeID = @NewOfficeID
				WHERE LeadID = @LeadID

			END
			ELSE
			BEGIN

				-- Insert a new record
				INSERT ClientOfficeLead (ClientID, ClientOfficeID, LeadID)
				VALUES (@ClientID, @NewOfficeID, @LeadID)

			END
		END
		-- ELSE there is no work to do because both people are in the same office!

	END


END






GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__HandleChangeOfAssignmentByUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__HandleChangeOfAssignmentByUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__HandleChangeOfAssignmentByUserID] TO [sp_executeall]
GO
