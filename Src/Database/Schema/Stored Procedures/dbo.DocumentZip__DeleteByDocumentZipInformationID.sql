SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Deletes a record in the DocumentZip table
-- Date: 13/02/2009
----------------------------------------------------------------------------------------------------
*/


Create PROCEDURE [dbo].[DocumentZip__DeleteByDocumentZipInformationID]
(

	@DocumentZipInformationID int   
)
AS


				DELETE FROM [dbo].[DocumentZip] WITH (ROWLOCK) 
				WHERE
					[DocumentZipInformationID] = @DocumentZipInformationID




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__DeleteByDocumentZipInformationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__DeleteByDocumentZipInformationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__DeleteByDocumentZipInformationID] TO [sp_executeall]
GO
