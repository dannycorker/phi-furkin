SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        Simon Brushett
-- Create date: 2013-05-30
-- Description:   Returns all paremeters for a specific rule

-- Modified By:   Austin Davies
-- Modified:      2014-06-04
-- Description:   Added ValueName column as a friendly way of outputting
--                values in the Parameters table

--                Removed RuleID as a dependency, so I can get a RuleParameter
--                without knowing what the RuleID is
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleParameters_GetParametersByRuleID]
(
      @ClientID INT,
      @RuleID INT = NULL,
      @RuleParameterID INT = NULL
)


AS
BEGIN
      
      SET NOCOUNT ON;

      SELECT      p.RuleParameterID, p.Name, p.ParameterTypeID, pt.Name AS ParameterType, p.Value, p.DataTypeID, dt.Name AS DataType,
                  p.WhenCreated, cpCreate.UserName AS WhoCreated, p.WhenModified, cpMod.UserName AS WhoModified, p.Parent,
                  CASE p.ParameterTypeID WHEN 1 THEN dfValue.FieldName
                        WHEN 2 THEN p.Value -- Ignore since already VARCHAR (Customer Field)
                        WHEN 3 THEN reSets.Name
                        WHEN 4 THEN p.Value -- Ignore since already VARCHAR (Function)
                        WHEN 5 THEN dfParent.PageName + ' : ' + dfChild.FieldName
                  END AS ValueName, p.RuleParameterPreSetID,
                  p.RuleID
      FROM dbo.RulesEngine_RuleParameters p WITH (NOLOCK) 
      INNER JOIN dbo.RulesEngine_ParameterTypes pt WITH (NOLOCK) ON p.ParameterTypeID = pt.ParameterTypeID
      INNER JOIN dbo.RulesEngine_DataTypes dt WITH (NOLOCK) ON p.DataTypeID = dt.TypeID
      INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON p.WhoCreated = cpCreate.ClientPersonnelID
      INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON p.WhoModified = cpMod.ClientPersonnelID
      LEFT JOIN dbo.DetailFields dfValue WITH (NOLOCK) ON CASE WHEN IsNumeric(p.Value) = 1 THEN p.Value ELSE NULL END = dfValue.DetailFieldID AND p.ParameterTypeID = 1
      LEFT JOIN dbo.RulesEngine_RuleSets reSets WITH (NOLOCK) ON CASE WHEN IsNumeric(p.Value) = 1 THEN p.Value ELSE NULL END = reSets.RuleSetID AND p.ParameterTypeID = 3
      LEFT JOIN dbo.DetailFields dfChild WITH (NOLOCK) ON CASE WHEN IsNumeric(p.Value) = 1 THEN p.Value ELSE NULL END = dfChild.DetailFieldID AND p.ParameterTypeID = 5
      LEFT JOIN dbo.DetailFieldPages dfParent WITH (NOLOCK) ON CASE WHEN IsNumeric(p.Value) = 1 THEN dfChild.DetailFieldPageID ELSE NULL END = dfParent.DetailFieldPageID AND p.ParameterTypeID = 5
      WHERE p.ClientID = @ClientID
      AND (@RuleID IS NULL OR p.RuleID = @RuleID)
      AND (@RuleParameterID IS NULL OR p.RuleParameterID = @RuleParameterID)
      
      
      
      SELECT 'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord
      UNION ALL
      SELECT s.Name AS Title, '#/ruleset/' + CAST(r.RuleSetID AS VARCHAR) + '/rules' AS Path, 2 AS Ord
      FROM dbo.RulesEngine_RuleSets s WITH (NOLOCK) 
      INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleSetID = s.RuleSetID
      WHERE r.RuleID = @RuleID
      UNION ALL
      SELECT r.Name AS Title, '#/rule/' + CAST(@RuleID AS VARCHAR) AS Path, 3 AS Ord
      FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
      WHERE r.RuleID = @RuleID
      ORDER BY Ord

      
END




GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_GetParametersByRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleParameters_GetParametersByRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleParameters_GetParametersByRuleID] TO [sp_executeall]
GO
