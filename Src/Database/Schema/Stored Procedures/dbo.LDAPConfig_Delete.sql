SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LDAPConfig table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LDAPConfig_Delete]
(

	@LDAPConfigID int   
)
AS


				DELETE FROM [dbo].[LDAPConfig] WITH (ROWLOCK) 
				WHERE
					[LDAPConfigID] = @LDAPConfigID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAPConfig_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAPConfig_Delete] TO [sp_executeall]
GO
