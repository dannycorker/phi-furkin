SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPersonnelOptions table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPersonnelOptionID int   = null ,

	@ClientPersonnelID int   = null ,

	@ClientPersonnelOptionTypeID int   = null ,

	@OptionValue varchar (255)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPersonnelOptionID]
	, [ClientPersonnelID]
	, [ClientPersonnelOptionTypeID]
	, [OptionValue]
	, [ClientID]
    FROM
	[dbo].[ClientPersonnelOptions] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelOptionID] = @ClientPersonnelOptionID OR @ClientPersonnelOptionID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID OR @ClientPersonnelOptionTypeID IS NULL)
	AND ([OptionValue] = @OptionValue OR @OptionValue IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPersonnelOptionID]
	, [ClientPersonnelID]
	, [ClientPersonnelOptionTypeID]
	, [OptionValue]
	, [ClientID]
    FROM
	[dbo].[ClientPersonnelOptions] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelOptionID] = @ClientPersonnelOptionID AND @ClientPersonnelOptionID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID AND @ClientPersonnelOptionTypeID is not null)
	OR ([OptionValue] = @OptionValue AND @OptionValue is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Find] TO [sp_executeall]
GO
