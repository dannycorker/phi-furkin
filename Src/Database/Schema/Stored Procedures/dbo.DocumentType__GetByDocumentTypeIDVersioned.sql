SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType__GetByDocumentTypeIDVersioned]
(
	@DocumentTypeID int   
)
AS
BEGIN

	DECLARE @ClientID INT
	DECLARE @OutputFormat VARCHAR(24)
	DECLARE @UseDocumentTypeVersioning BIT
	
	SELECT @ClientID=ClientID, @OutputFormat=OutputFormat 
	FROM DocumentType WITH (NOLOCK) WHERE DocumentTypeID=@DocumentTypeID
	
	SELECT @UseDocumentTypeVersioning=UseDocumentTypeVersioning 
	FROM ClientOption WITH (NOLOCK) WHERE ClientID=@ClientID	
	
	IF @OutputFormat <> 'EXCEL' AND @UseDocumentTypeVersioning=1  -- not excel and versioning is on
	BEGIN
		
		--Choose active version
		DECLARE @DocumentTypeVersionID INT 
		SELECT @DocumentTypeVersionID = dbo.fnGetActiveDocumentTypeVersion(@DocumentTypeID)
		
		IF @DocumentTypeVersionID>0 -- Active version found
		BEGIN
		
			DECLARE @Info VARCHAR(MAX)
			SET @Info = 'Chose Document Type Version ID: ' + CAST(@DocumentTypeVersionID AS VARCHAR(16)) + ' For Document Type ID: ' + CAST(@DocumentTypeID AS VARCHAR(16))
			EXEC _C00_LogIt 'Info','DocumentType__GetByDocumentTypeID','ChoosenVersion', @info, NULL 
		
			SELECT
				[DocumentTypeID],
				[ClientID],
				[LeadTypeID],
				[DocumentTypeName],
				[DocumentTypeDescription],
				[Header],
				[Template],
				[Footer],
				[CanBeAutoSent],
				[EmailSubject],
				[EmailBodyText],
				[InputFormat],
				[OutputFormat],
				[Enabled],
				[RecipientsTo],
				[RecipientsCC],
				[RecipientsBCC],
				[ReadOnlyTo],
				[ReadOnlyCC],
				[ReadOnlyBCC],
				[SendToMultipleRecipients],
				[MultipleRecipientDataSourceType],
				[MultipleRecipientDataSourceID],
				[SendToAllByDefault],
				[ExcelTemplatePath],
				[FromDetails],
				[ReadOnlyFrom],
				[SourceID],
				[WhoCreated],
				[WhenCreated],
				[WhoModified],
				[WhenModified],
				[FolderID],
				[IsThunderheadTemplate],
				[ThunderheadUniqueTemplateID],
				[ThunderheadDocumentFormat],
				[DocumentTitleTemplate],
				Cast(1 AS BIT) AS [CreateNewVersionWhenSaved]
			FROM
				[dbo].[DocumentTypeVersion] WITH (NOLOCK) 
			WHERE
				[DocumentTypeVersionID] = @DocumentTypeVersionID
		
		END
		ELSE
		BEGIN
			EXEC [DocumentType__GetByDocumentTypeIDBasic] @DocumentTypeID
		END
	
	END
	ELSE	
	BEGIN
		EXEC [DocumentType__GetByDocumentTypeIDBasic] @DocumentTypeID
	END				
										
	SELECT @@ROWCOUNT
					
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByDocumentTypeIDVersioned] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByDocumentTypeIDVersioned] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByDocumentTypeIDVersioned] TO [sp_executeall]
GO
