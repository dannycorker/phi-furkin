SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadTypePageTitleStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_Get_List]

AS


				
				SELECT
					[LeadTypePageTitleStandardTargetID],
					[ClientID],
					[LeadTypeID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes]
				FROM
					[dbo].[LeadTypePageTitleStandardTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Get_List] TO [sp_executeall]
GO
