SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Contact table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Contact_Delete]
(

	@ContactID int   
)
AS


				DELETE FROM [dbo].[Contact] WITH (ROWLOCK) 
				WHERE
					[ContactID] = @ContactID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact_Delete] TO [sp_executeall]
GO
