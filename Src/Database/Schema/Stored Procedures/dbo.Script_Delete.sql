SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Script table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Script_Delete]
(

	@ScriptID int   
)
AS


				DELETE FROM [dbo].[Script] WITH (ROWLOCK) 
				WHERE
					[ScriptID] = @ScriptID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Delete] TO [sp_executeall]
GO
