SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptPositionTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_Get_List]

AS


				
				SELECT
					[ScriptPositionTargetID],
					[ScriptID],
					[SectionID],
					[ClientID],
					[PositionName],
					[LinkText]
				FROM
					[dbo].[ScriptPositionTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Get_List] TO [sp_executeall]
GO
