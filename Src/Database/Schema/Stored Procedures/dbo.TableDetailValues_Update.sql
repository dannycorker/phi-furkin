SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the TableDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableDetailValues_Update]
(

	@TableDetailValueID int   ,

	@TableRowID int   ,

	@ResourceListID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@LeadID int   ,

	@MatterID int   ,

	@ClientID int   ,

	@EncryptedValue varchar (3000)  ,

	@ErrorMsg varchar (1000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@CustomerID int   ,

	@CaseID int   ,

	@ClientPersonnelID int   ,

	@ContactID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[TableDetailValues]
				SET
					[TableRowID] = @TableRowID
					,[ResourceListID] = @ResourceListID
					,[DetailFieldID] = @DetailFieldID
					,[DetailValue] = @DetailValue
					,[LeadID] = @LeadID
					,[MatterID] = @MatterID
					,[ClientID] = @ClientID
					,[EncryptedValue] = @EncryptedValue
					,[ErrorMsg] = @ErrorMsg
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[CustomerID] = @CustomerID
					,[CaseID] = @CaseID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[ContactID] = @ContactID
				WHERE
[TableDetailValueID] = @TableDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Update] TO [sp_executeall]
GO
