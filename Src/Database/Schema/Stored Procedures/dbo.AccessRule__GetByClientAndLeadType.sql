SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-28
-- Description:	Get Access Rules for a client by optional LeadTypeID
-- =============================================
CREATE PROCEDURE [dbo].[AccessRule__GetByClientAndLeadType]
	@ClientID int, 
	@ShowAll bit = 1,
	@LeadTypeID int = null
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT ar.AccessRuleID, 
	ar.ClientID, 
	ar.AccessRuleName, 
	ar.AccessRuleEnabled, 
	CASE WHEN df.DetailFieldID IS NULL THEN 'None' ELSE df.FieldName + ' (' + CAST(df.DetailFieldID as varchar) + ')' END as [DetailFieldToCompare], 
	CASE 
		WHEN ar.ValueToCompare IS NULL THEN 'Always' 
		ELSE CASE 
				WHEN ar.ValueDecoder = 'OUTCOME' THEN 'Outcome: ' + o.OutcomeName
				WHEN ar.ValueDecoder = 'TRACKING' THEN 'Tracking: ' + tr.TrackingName
				WHEN df.LookupListID > 0 THEN 'Lookup: ' + luli.ItemValue 
				WHEN df.QuestionTypeID = 14 THEN 'Resource: ' + rldv.DetailValue 
				ELSE 'Other value '
		END + ' (' + CAST(ar.ValueToCompare as varchar) + ')' 
	END as [ValueToCompare],
	dlot.ObjectTypeName,
	CASE 
		WHEN ar.PortalUserID IS NOT NULL THEN 'Portal: '
		WHEN ar.ClientPersonnelID IS NOT NULL THEN 'User: '
		ELSE 'Group: '
	END + COALESCE(cpag.GroupName, cp.UserName, pu.Username, '') as [Recipient],
	ar.WhenModified,
	ar.AccessRuleDescription + ', last saved by ' + cp_Who.UserName + ' on ' + convert(varchar, ar.WhenModified, 120) as [Tooltip], 
	ar.AccessLevel 
	FROM dbo.AccessRule ar WITH (NOLOCK) 
	INNER JOIN dbo.DataLoaderObjectType dlot WITH (NOLOCK) ON dlot.DataLoaderObjectTypeID = ar.DataLoaderObjectTypeID 
	INNER JOIN dbo.ClientPersonnel cp_Who WITH (NOLOCK) ON cp_Who.ClientPersonnelID = ar.WhoModified 
	LEFT JOIN dbo.ClientPersonnelAdminGroups cpag WITH (NOLOCK) ON cpag.ClientPersonnelAdminGroupID = ar.ClientPersonnelAdminGroupID 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = ar.ClientPersonnelID 
	LEFT JOIN dbo.PortalUser pu WITH (NOLOCK) ON pu.PortalUserID = ar.PortalUserID 
	LEFT JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ar.DetailFieldID 
	LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListID = df.LookupListID AND luli.LookupListItemID = ar.ValueToCompare 
	LEFT JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = ar.ValueToCompare AND rldv.DetailFieldID = ar.ValueDecoderColumnFieldID 
	LEFT JOIN dbo.Outcomes o WITH (NOLOCK) ON o.OutcomeID = ar.ValueToCompare AND ar.ValueDecoder = 'OUTCOME'
	LEFT JOIN dbo.Tracking tr WITH (NOLOCK) ON tr.TrackingID = ar.ValueToCompare AND ar.ValueDecoder = 'TRACKING'
	WHERE ar.ClientID = @ClientID 
	AND (
		/* Option 1: Show all batch jobs */
		(@ShowAll = 1)
		OR
		/* Option 2: Only show batch jobs with this lead type */
		(@LeadTypeID > 0 AND (@LeadTypeID = ar.LeadTypeID))
		OR
		/* Option 3: Only show batch jobs that have no lead type */
		(@LeadTypeID IS NULL AND ar.LeadTypeID IS NULL)
	) 
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule__GetByClientAndLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule__GetByClientAndLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule__GetByClientAndLeadType] TO [sp_executeall]
GO
