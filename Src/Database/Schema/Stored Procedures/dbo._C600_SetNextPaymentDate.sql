SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-06
-- Description:	Sets Next Payment Day taking into account short months, working days and any override or wait period
-- Mods
--	2015-04-20 DCM maxrow
--	2015-05-05 DCM seed date in max row
--	2015-07-15 DCM use separate date field for date next regualr payment
--  2015-09-03 DCM allow NPD to be brought forward on Other Adjustment (3), i.e. MTA, if the collection is not annual
--  2016-09-16 DCM cancellation of a policy on a multipet account - only update the next payment date if waiting period takes new date beyond existing date
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SetNextPaymentDate] 
(
	@MatterID INT,
	@DateAdjustmentType INT -- 1 Regular Payment, 
							-- 2 Mandate Wait, 
							-- 3 Other adjustment, 
							-- 4 Retry payment collection, 
							-- 5 Post collection, 
							-- 7 force for linked policy, 
							-- 8 change collection day
							-- 9 waiting days where policy cancelled on multi-policy collections matter
							-- 10 force early retry of collection
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50022656,
--	@DateAdjustmentType INT = 4
	
	DECLARE @AqUserID INT,
			@CaseID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@CollectionsMatterID INT,
			@CurrentPaymentDateSetting DATE,
			@IsAnnual INT,
			@LastDayOfMonth INT,
			@LeadEventID INT,
			@MandateWaitDate DATE,
			@NewBasePaymentDate DATE,
			@NewPaymentDate DATE,
			@OverrideDate DATE,
			@OverrideUsed INT = 0,
			@PACaseID INT,
			@PaymentDay INT,
			@PremiumsMatterID INT,
			@RegPaymentDate DATE,
			@SeedDate DATE,
			@WaitingDays INT  
			
	SELECT @AqUserID=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@ClientID,53,'CFG|AqAutomationCPID',0)
	
	-- find appropriate collections matter for the matter passed in
	IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @CollectionsMatterID=@MatterID
		
	END
	ELSE
	BEGIN
	
		SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
		SELECT @PACaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@MatterID

	END
	SELECT @CaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@CollectionsMatterID
	SELECT @IsAnnual=CASE WHEN dbo.fnGetDvAsInt(170145,@CaseID)=69944 THEN 1 ELSE 0 END
	SELECT @CurrentPaymentDateSetting=dbo.fnGetDvAsDate(170185,@CaseID),
			@RegPaymentDate=dbo.fnGetDvAsDate(175599,@CaseID),
			@PaymentDay=dbo.fnGetDvAsInt(170184,@CaseID),
			@MandateWaitDate=dbo.fn_C600_GetMandateWaitDate (@CollectionsMatterID)	

	-- find seed date
	IF @DateAdjustmentType=1 -- regular payment
	BEGIN
	
		-- if mandate status is pending and the current date is later than the regular payment date use current date.
		-- if the mandate status is failed use the current date setting
		-- if there is a ready to retry collection event not followed up use current date
		IF EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.EventDeleted=0 AND le.WhenFollowedUp IS NULL AND le.EventTypeID=155358 AND le.CaseID=@CaseID)
			SELECT @SeedDate=@CurrentPaymentDateSetting
		ELSE IF dbo.fnGetDvAsInt(170101,@CaseID)=69919 -- failed
			SELECT @SeedDate=@CurrentPaymentDateSetting
		ELSE IF NOT ( dbo.fnGetDvAsInt(170101,@CaseID)=69917 -- pending
			AND @CurrentPaymentDateSetting>dbo.fnGetDvAsDate(175599,@CaseID) ) -- reg payment date
			SELECT @SeedDate=dbo.fnGetDvAsDate(175599,@CaseID)
		ELSE
			SELECT @SeedDate=@CurrentPaymentDateSetting
		
	END
	ELSE IF @DateAdjustmentType=4 -- retrying collection
	BEGIN
	
		SELECT @SeedDate=CAST(le.FollowupDateTime AS DATE), @LeadEventID=le.LeadEventID 
		FROM LeadEvent le WITH (NOLOCK) 
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=le.CaseID AND m.MatterID=@CollectionsMatterID
		WHERE le.EventDeleted=0 AND le.WhenFollowedUp IS NULL AND le.EventTypeID=150099
		AND NOT EXISTS (
			SELECT * FROM LeadEvent le2 WITH (NOLOCK) 
			WHERE le2.CaseID=le.CaseID AND le2.EventTypeID=le.EventTypeID 
			AND le2.EventDeleted=0 AND le2.WhenFollowedUp IS NULL
			AND le2.LeadEventID>le.LeadEventID
		)

	END
	ELSE IF @DateAdjustmentType = 5 -- reset after a collection
	BEGIN
	
		-- This is run after a collection event. There may be 'ready' rows still awaiting collection. Set seed date
		-- to the earliest date due of these or to NULL if there are none
		SELECT @SeedDate=NULL
		SELECT @SeedDate=MIN(dd.ValueDate) FROM  TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues crid WITH (NOLOCK) ON tr.TableRowID=crid.TableRowID AND crid.DetailFieldID=170073
		INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078
		INNER JOIN TableDetailValues dd WITH (NOLOCK) ON tr.TableRowID=dd.TableRowID AND dd.DetailFieldID=175597
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON tr.MatterID=ltr.FromMatterID AND ltr.FromLeadTypeID=1492
		WHERE tr.DetailFieldID=170088 AND ltr.ToMatterID=@CollectionsMatterID
			AND stat.ValueInt=72154 -- Ready
			AND crid.DetailValue=''	
	
		-- if annual set seed date to date next regular collection
		IF @IsAnnual=1
			SELECT @SeedDate=@RegPaymentDate
			
	END
	ELSE IF @DateAdjustmentType = 7 -- manual delay in collection date for waiting time when policies linked 
	BEGIN
	
		SELECT @SeedDate=dbo.fnGetDvAsDate(175608,@CaseID)	
	
	END
	ELSE IF @DateAdjustmentType = 8 -- payment day changed 
	BEGIN
	
		/* need to update regular payment day, update any premium rows on this collections case which have a due date of
		   the existing regular payment day and also update the NDP if it is using the existing regualr payment date. (NPD updated
		   at foot of this stored proc.) 
		
		   Note. regular payment day is only set to be a date within the latest period in the premiums table (latest 'ready' or 
		   for safety later than today), this means that early payers will be brought within normal payment period if they change
		   payment day.  Note this is to stop the situation where old date was on e.g 28/8 & new day is 1 and the DNRP is set to 1/8 rather 
		   then 1/9
		   
		*/
		
		SELECT @SeedDate=ISNULL(MAX(pfrom.ValueDate),dbo.fn_GetDate_Local()) 
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078 AND stat.ValueInt=72154 -- ready
		INNER JOIN TableDetailValues pfrom WITH (NOLOCK) ON tr.TableRowID=pfrom.TableRowID AND pfrom.DetailFieldID=170076
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromMatterID=tr.MatterID AND ltr.FromLeadTypeID=1492
			AND ltr.ToMatterID=@CollectionsMatterID
		WHERE tr.DetailFieldID=170088
		
		IF @PaymentDay<DATEPART(DAY,@SeedDate) -- next payment day is in next month
			SELECT @SeedDate=DATEADD(MONTH,1,@SeedDate)	

		SELECT @SeedDate = CAST (
										CAST(DATEPART(YYYY,@SeedDate) AS VARCHAR) + '-' +
										CAST(DATEPART(MM,@SeedDate) AS VARCHAR) + '-' +
										CASE WHEN DATEPART(DAY,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@SeedDate)+1,0))) < @PaymentDay 
										THEN
											RIGHT('0' + CAST(DATEPART(DAY,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@SeedDate)+1,0))) AS VARCHAR),2)
										ELSE
											RIGHT('0' + CAST(@PaymentDay AS VARCHAR),2)
										END
									  AS DATE )
		
		EXEC _C00_SimpleValueIntoField 175599, @SeedDate, @CollectionsMatterID, @AqUserID -- regular payment date
		UPDATE dd -- date due rows
		SET DetailValue=CONVERT(VARCHAR(10),@SeedDate,120)
		FROM TableDetailValues dd  
		INNER JOIN TableRows tr WITH (NOLOCK) ON dd.TableRowID=tr.TableRowID 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON tr.MatterID=ltr.FromMatterID AND ltr.ToLeadTypeID=1493
		INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078 AND stat.ValueInt=72154 -- ready
		WHERE dd.DetailFieldID=175597 AND dd.ValueDate=@RegPaymentDate
		AND ltr.ToMatterID=@CollectionsMatterID
	
	END
	ELSE
	BEGIN
	
		SELECT @SeedDate=CONVERT(DATE,dbo.fn_GetDate_Local())
		
	END

	-- if override calculate new date based on that
	SELECT @OverrideDate=dbo.fnGetDvAsDate(175407,@CaseID),
			@WaitingDays=ValueInt
	FROM ClientDetailValues WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND DetailFieldID=170228
	
    -- override date is set and, if before end of waiting period, user has confirmed customer has given explicit consent
	IF ( ( @OverrideDate IS NOT NULL 
			AND 5144 = dbo.fnGetDvAsInt(175409,@CaseID)
			AND @OverrideDate < DATEADD(DAY,@WaitingDays,CONVERT(DATE,dbo.fn_GetDate_Local())) )
		OR
		( @OverrideDate IS NOT NULL
			AND @OverrideDate >= DATEADD(DAY,@WaitingDays,CONVERT(DATE,dbo.fn_GetDate_Local())) )
		)
		AND @DateAdjustmentType <> 2 -- mandate waiting time can't be overriden
	BEGIN
	
		SELECT @NewPaymentDate=dbo.fnGetNextWorkingDate(@OverrideDate,0), @OverrideUsed=1

	END
	ELSE IF @DateAdjustmentType IN  (3, -- other adjustment
									 10) -- 10 force early retry of collection
	BEGIN

		SELECT @NewPaymentDate=dbo.fnGetNextWorkingDate(DATEADD(DAY,@WaitingDays,@SeedDate),0)
	
	END
	ELSE IF @DateAdjustmentType IN  (9) -- 9 waiting days where policy cancelled on multi-policy collections matter
	BEGIN

		IF dbo.fnGetNextWorkingDate(DATEADD(DAY,@WaitingDays,@SeedDate),0) > @CurrentPaymentDateSetting
		BEGIN
		
			SELECT @NewPaymentDate=dbo.fnGetNextWorkingDate(DATEADD(DAY,@WaitingDays,@SeedDate),0)
		
		END
		ELSE
		BEGIN
		
			SELECT @NewPaymentDate=@CurrentPaymentDateSetting
		
		END
	
	END
	
	ELSE IF @DateAdjustmentType = 4 -- retry collection
	BEGIN

		SELECT @NewPaymentDate=dbo.fnGetNextWorkingDate(@SeedDate,0)
	
	END
	ELSE IF @DateAdjustmentType = 2 -- mandate
	BEGIN

		SELECT @NewPaymentDate=dbo.fnGetNextWorkingDate(DATEADD(DAY,lodgement.ValueInt+submit.ValueInt,@SeedDate),0)
		FROM ClientDetailValues submit WITH (NOLOCK) 
		INNER JOIN ClientDetailValues lodgement WITH (NOLOCK) ON submit.ClientID=lodgement.ClientID and lodgement.DetailFieldID=170227
		WHERE submit.DetailFieldID=170226 AND submit.ClientID=@ClientID
		
		IF @OverrideDate IS NOT NULL AND @OverrideDate > @NewPaymentDate
			SELECT @NewPaymentDate=@OverrideDate
	
	END
	ELSE
	BEGIN -- regular payment; post collection
	
		IF @SeedDate IS NOT NULL
			SELECT @NewPaymentDate=dbo.fnGetNextWorkingDate(@SeedDate,0)
		
	END
	
--select @NewPaymentDate

	IF  NOT ( @NewPaymentDate < @CurrentPaymentDateSetting AND @DateAdjustmentType=2  ) -- don't update for mandate waiting period if collection date already more than that
		AND NOT ( @SeedDate IS NULL AND @DateAdjustmentType=5 ) -- don't update if just made a collection and there are no more ready rows
		AND NOT (@CurrentPaymentDateSetting<>dbo.fnGetNextWorkingDate(@RegPaymentDate,0) AND @DateAdjustmentType=8 AND @IsAnnual=1 ) -- don't update if change of payment day but current payment date not using regular payment date and annual
        AND NOT (@NewPaymentDate < @MandateWaitDate AND @DateAdjustmentType=8 ) -- don't allow update payment date change to bring NPD within mandate waiting period    
		AND NOT (@CurrentPaymentDateSetting>@NewPaymentDate AND @DateAdjustmentType=9 ) -- don't update if changing because of cancellation of a policy on multi-policy and existing date is already beyond waiting period
		AND NOT (@NewPaymentDate < @CurrentPaymentDateSetting AND @DateAdjustmentType=3 AND @OverrideUsed=0 AND @IsAnnual=0 ) -- don't allow MTA adjustment date to be earlier than NPD on monthly payments unless overriden
        AND NOT (@NewPaymentDate < @MandateWaitDate AND @DateAdjustmentType=3 ) -- don't update payment date change brings NPD within mandate waiting period    
		EXEC _C00_SimpleValueIntoField 170185, @NewPaymentDate, @CollectionsMatterID, @AqUserID

	--IF @SeedDate IS NULL AND @DateAdjustmentType=5 AND @IsAnnual=1 -- set to blank if post collection and it is an annual payment
	--	EXEC _C00_SimpleValueIntoField 170185, '', @CollectionsMatterID, @AqUserID
	
	-- if retrying collection make followupdatetime=next payment date ( because initial setting of followuptime doesn't take into working days)
	IF @DateAdjustmentType=4 AND @NewPaymentDate IS NOT NULL AND @LeadEventID IS NOT NULL
		UPDATE LeadEvent SET FollowupDateTime=@NewPaymentDate WHERE LeadEventID=@LeadEventID
	 	
	-- clear any override settings
	EXEC _C00_SimpleValueIntoField 175407, '', @CollectionsMatterID, @AqUserID
	EXEC _C00_SimpleValueIntoField 175409, '', @CollectionsMatterID, @AqUserID
	
	-- now set amount due at next date
	EXEC _C600_SetNextPaymentAmount @CollectionsMatterID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetNextPaymentDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SetNextPaymentDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetNextPaymentDate] TO [sp_executeall]
GO
