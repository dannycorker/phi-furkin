SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PatchNote table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PatchNote_GetByPatchID]
(

	@PatchID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PatchNoteID],
					[PatchID],
					[Location],
					[PatchNoteText],
					[AdminOnly]
				FROM
					[dbo].[PatchNote] WITH (NOLOCK) 
				WHERE
					[PatchID] = @PatchID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_GetByPatchID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PatchNote_GetByPatchID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_GetByPatchID] TO [sp_executeall]
GO
