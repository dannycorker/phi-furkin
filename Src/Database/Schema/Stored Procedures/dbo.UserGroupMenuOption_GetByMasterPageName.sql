SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserGroupMenuOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuOption_GetByMasterPageName]
(

	@MasterPageName varchar (20)  
)
AS


				SELECT
					[UserGroupMenuOptionID],
					[ClientPersonnelAdminGroupID],
					[ClientID],
					[MasterPageName],
					[PanelItemAction],
					[PanelItemName],
					[PanelItemCaption],
					[PanelItemIcon],
					[PanelItemURL],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Comments],
					[ItemOrder],
					[ShowInFancyBox]
				FROM
					[dbo].[UserGroupMenuOption] WITH (NOLOCK) 
				WHERE
										[MasterPageName] = @MasterPageName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_GetByMasterPageName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption_GetByMasterPageName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_GetByMasterPageName] TO [sp_executeall]
GO
