SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Deletes a record in the DocumentZipInformation table
-- Date: 13/02/2009
----------------------------------------------------------------------------------------------------
*/


Create PROCEDURE [dbo].[DocumentZipInformation__Delete]
(

	@DocumentZipInformationID int   
)
AS


				DELETE FROM [dbo].[DocumentZipInformation] WITH (ROWLOCK) 
				WHERE
					[DocumentZipInformationID] = @DocumentZipInformationID





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__Delete] TO [sp_executeall]
GO
