SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-07-2016
-- Description:	Creates an outgoing payment record from the third party mapped fields.
--				Also creates a customer ledger entry
-- Mods
--	2016-08-18 DCM Corrected setting of PolicyObjectID
--  2016-10-19 DCM Make customer ledger when created full date & time
--	2016-11-14 DCM Update ParentOutgoingPaymentID and PaymentStatusID
--  2017-09-01 JEL changed transaction date to be getdate rather than claim payment date
-- =============================================
CREATE PROCEDURE [dbo].[OutgoingPayment__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Gets the customer, lead, matter identities and the leadtype
	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @WhoCreated INT
	SELECT @LeadID=cs.LeadID FROM Cases cs WITH (NOLOCK) WHERE cs.CaseID = @CaseID
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@LeadID
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.CaseID=@CaseID
	
	SELECT @WhoCreated = le.WhoCreated FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID=@LeadEventID
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created

	--4338	54	ClaimObjectTypeID
	DECLARE @ClaimObjectTypeID_DV VARCHAR(2000), @ClaimObjectTypeID INT, @IsInt_ClaimObjectTypeID BIT
	SELECT @ClaimObjectTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4339)
	SELECT @IsInt_ClaimObjectTypeID = dbo.fnIsInt(@ClaimObjectTypeID_DV)
	IF(@IsInt_ClaimObjectTypeID=1)
	BEGIN
		SELECT @ClaimObjectTypeID = CAST(@ClaimObjectTypeID_DV AS INT)
	END
	
	--4339	54	ClaimObjectID
	DECLARE @ClaimObjectID_DV VARCHAR(2000), @IsInt_ClaimObjectID BIT, @ClaimObjectID INT
	SELECT @ClaimObjectID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4338)
	SELECT @IsInt_ClaimObjectID = dbo.fnIsInt(@ClaimObjectID_DV)
	IF(@IsInt_ClaimObjectID=1)
	BEGIN
		SELECT @ClaimObjectID = CAST(@ClaimObjectID_DV AS INT)
	END
	
	--4340	54	PolicyObjectTypeID
	DECLARE @PolicyObjectTypeID_DV VARCHAR(2000), @IsInt_PolicyObjectTypeID BIT, @PolicyObjectTypeID INT
	SELECT @PolicyObjectTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4340)
	SELECT @IsInt_PolicyObjectTypeID = dbo.fnIsInt(@PolicyObjectTypeID_DV)
	IF(@IsInt_PolicyObjectTypeID=1)
	BEGIN
		SELECT @PolicyObjectTypeID = CAST(@PolicyObjectTypeID_DV AS INT)
	END
			
	--4341	54	PolicyObjectID
	DECLARE @PolicyObjectID_DV VARCHAR(2000), @IsInt_PolicyObjectID BIT, @PolicyObjectID INT
	SELECT @PolicyObjectID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4341)
	SELECT @IsInt_PolicyObjectID = dbo.fnIsInt(@PolicyObjectID_DV)
	IF(@IsInt_PolicyObjectID=1)
	BEGIN
		SELECT @PolicyObjectID = CAST(@PolicyObjectID_DV AS INT)
	END
		
	--4342	54	PayeeTitle -- is an interger see Titles table
	DECLARE @PayeeTitle_DV VARCHAR(2000), @IsInt_PayeeTitle BIT, @PayeeTitle INT
	SELECT @PayeeTitle_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4342)
	SELECT @IsInt_PayeeTitle = dbo.fnIsInt(@PayeeTitle_DV)
	IF(@IsInt_PayeeTitle=1)
	BEGIN
		SELECT @PayeeTitle = CAST(@PayeeTitle_DV AS INT)
	END	
	
	--4343	54	PayeeFirstName
	DECLARE @PayeeFirstName_DV VARCHAR(2000)
	SELECT @PayeeFirstName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4343)	
	
	--4344	54	PayeeLastName
	DECLARE @PayeeLastName_DV VARCHAR(2000)
	SELECT @PayeeLastName_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4344)	
	
	--4345	54	PayeeOrganisation
	DECLARE @PayeeOrganisation_DV VARCHAR(100)
	SELECT @PayeeOrganisation_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4345)		
		
	--4346	54	PayeeAddress1
	DECLARE @PayeeAddress1_DV VARCHAR(2000)
	SELECT @PayeeAddress1_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4346)		
	
	--4347	54	PayeeAddress2
	DECLARE @PayeeAddress2_DV VARCHAR(2000)
	SELECT @PayeeAddress2_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4347)		
		
	--4348	54	PayeeAddress3
	DECLARE @PayeeAddress3_DV VARCHAR(2000)
	SELECT @PayeeAddress3_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4348)		

	--4349	54	PayeeAddress4
	DECLARE @PayeeAddress4_DV VARCHAR(2000)
	SELECT @PayeeAddress4_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4349)		
	
	--4350	54	PayeeAddress5
	DECLARE @PayeeAddress5_DV VARCHAR(2000)
	SELECT @PayeeAddress5_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4350)		
	
	--4351	54	PayeePostcode
	DECLARE @PayeePostcode_DV VARCHAR(2000)
	SELECT @PayeePostcode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4351)		
		
	--4352	54	PayeeCountryID
	DECLARE @PayeeCountryID_DV VARCHAR(2000), @IsInt_PayeeCountryID BIT, @PayeeCountryID INT
	SELECT @PayeeCountryID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4352)	
	SELECT @IsInt_PayeeCountryID = dbo.fnIsInt(@PayeeCountryID_DV)	
	IF(@IsInt_PayeeCountryID=1)
	BEGIN
		SELECT @PayeeCountryID = CAST(@PayeeCountryID_DV AS INT)
	END
	
	--4353	54	PayeeReference
	DECLARE @PayeeReference_DV VARCHAR(2000)
	SELECT @PayeeReference_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4353)		

	--4354	54	PolicyholderReference
	DECLARE @PolicyholderReference_DV VARCHAR(2000)
	SELECT @PolicyholderReference_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4354)		
	
	
	--4355	54	PaymentTypeID
	DECLARE @PaymentTypeID_DV VARCHAR(2000), @IsInt_PaymentTypeID BIT, @PaymentTypeID INT, @ItemID_PaymentTypeID INT, @PaymentTypeID_ItemValue VARCHAR(500)
	SELECT @PaymentTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4355)	
	SELECT @IsInt_PaymentTypeID = dbo.fnIsInt(@PaymentTypeID_DV)		
	IF(@IsInt_PaymentTypeID=1)
	BEGIN
		SELECT @ItemID_PaymentTypeID = CAST(@PaymentTypeID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual payment type
		SELECT @PaymentTypeID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_PaymentTypeID
		SELECT @PaymentTypeID = pt.PaymentTypeID FROM  PaymentTypes pt WITH (NOLOCK) WHERE pt.PaymentTypeName = @PaymentTypeID_ItemValue
	END		

	--4356	54	PaymentAmount
	DECLARE @PaymentAmount_DV VARCHAR(2000), @PaymentAmount NUMERIC(18,2)
	SELECT @PaymentAmount_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4356)		
	
	DECLARE @LogEntry VARCHAR(MAX),@MethodName VARCHAR(200)
	BEGIN TRY  
		SELECT @PaymentAmount = CAST(@PaymentAmount_DV AS NUMERIC(18,2)) 
	END TRY  
	BEGIN CATCH  

		SELECT CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		SELECT @MethodName = 'Cast Payment Amount'
		SELECT @LogEntry = 'Error found ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() + ' Line ' + ISNULL(CONVERT(VARCHAR,ERROR_LINE()),'NULL')
		
		EXEC dbo._C00_LogIt 'Error', 'OutgoingPayment__CreateFromThirdPartyFields', @MethodName, @LogEntry, @WhoCreated

	END CATCH 
	
	--4357	54	PaymentDate
	DECLARE @PaymentDate_DV VARCHAR(2000), @IsDateTime_PaymentDate BIT, @PaymentDate DATETIME
	SELECT @PaymentDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4357)		
	SELECT @IsDateTime_PaymentDate = dbo.fnIsDateTime(@PaymentDate_DV)		
	IF(@IsDateTime_PaymentDate=1)
	BEGIN
		SELECT @PaymentDate = CAST(@PaymentDate_DV AS DATETIME)
	END			

	--4358	54	PaymentRunDate
	DECLARE @PaymentRunDate_DV VARCHAR(2000), @IsDateTime_PaymentRunDate BIT, @PaymentRunDate DATETIME
	SELECT @PaymentRunDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4358)
	SELECT @IsDateTime_PaymentRunDate = dbo.fnIsDateTime(@PaymentRunDate_DV)		
	IF(@IsDateTime_PaymentRunDate=1)
	BEGIN
		SELECT @PaymentRunDate = CAST(@PaymentDate_DV AS DATETIME)
	END						
		
	--4359	54	DateCreated
	DECLARE @DateCreated_DV VARCHAR(2000), @IsDateTime_DateCreated BIT, @DateCreated DATETIME
	SELECT @DateCreated_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4359)	
	SELECT @IsDateTime_DateCreated = dbo.fnIsDateTime(@DateCreated_DV)		
	IF(@IsDateTime_DateCreated=1)
	BEGIN
		SELECT @DateCreated = CAST(@DateCreated_DV AS DATETIME)
	END								

	--4360	54	OriginalPaymentReferrence
	DECLARE @OriginalPaymentReferrence_DV VARCHAR(2000)
	SELECT @OriginalPaymentReferrence_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4360)		
	
	--4361	54	ChequeNumber
	DECLARE @ChequeNumber_DV VARCHAR(2000)
	SELECT @ChequeNumber_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4361)		
	
	--4362	54	AccountNumber;
	DECLARE @AccountNumber_DV VARCHAR(2000)
	SELECT @AccountNumber_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4362)		
	
	--4363	54	Sortcode
	DECLARE @Sortcode_DV VARCHAR(2000)
	SELECT @Sortcode_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4363)		
	
	--4364	54	MaskedCardNumber	
	DECLARE @MaskedCardNumber_DV VARCHAR(2000)
	SELECT @MaskedCardNumber_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4364)		
	
	--4365	54	PaymentSourceTypeID
	DECLARE @PaymentSourceTypeID_DV VARCHAR(2000), @IsInt_PaymentSourceTypeID BIT, @PaymentSourceTypeID INT, @ItemValue_PaymentSourceType VARCHAR(500)
	DECLARE @PaymentSourceTypeID_ItemID INT, @ObjectID INT, @ObjectTypeID INT
	SELECT @PaymentSourceTypeID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4365)	
	SELECT @IsInt_PaymentSourceTypeID = dbo.fnIsInt(@PaymentSourceTypeID_DV)		
	IF(@IsInt_PaymentSourceTypeID=1)
	BEGIN
		SELECT @PaymentSourceTypeID_ItemID = CAST(@PaymentSourceTypeID_DV AS INT) --  this is a LookupListItemID and must be dereferenced back to an actualPaymentType By Name
		SELECT @ItemValue_PaymentSourceType = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE llItem.LookupListItemID = @PaymentSourceTypeID_ItemID 
		SELECT @PaymentSourceTypeID = pst.PaymentSourceTypeID FROM PaymentSourceType pst WITH (NOLOCK) WHERE SourceType = @ItemValue_PaymentSourceType
		IF @PaymentSourceTypeID = 1 -- claim
		BEGIN
			SELECT @ObjectID = @ClaimObjectID
			SELECT @ObjectTypeID = @ClaimObjectTypeID
		END
		ELSE IF @PaymentSourceTypeID =2 -- policy refund
		BEGIN
			SELECT @ObjectID = @PolicyObjectID
			SELECT @ObjectTypeID = @PolicyObjectTypeID		
		END
	END			
		
	DECLARE @OutgoingPaymentID INT		
	--Inserts the thirdpartymapped fields into the outgoing payment table	
	IF ((@PaymentAmount IS NOT NULL) AND (@PaymentAmount>0)) -- Checks payment is not null and is greater than 0
	BEGIN
	
		INSERT INTO OutgoingPayment(ClientID, CustomerID, LeadEventID, ClaimObjectTypeID, ClaimObjectID, PolicyObjectTypeID, PolicyObjectID, PayeeTitle, PayeeFirstName, PayeeLastName, PayeeOrganisation, PayeeAddress1, PayeeAddress2, PayeeAddress3, PayeeAddress4, PayeeAddress5, PayeePostcode, PayeeCountryID, PayeeReference, PolicyholderReference, PaymentTypeID, PaymentAmount, PaymentDate, PaymentRunDate, DateCreated, OriginalPaymentReferrence, ChequeNumber, AccountNumber, Sortcode, MaskedCardNumber, PaymentSourceTypeID, PaymentStatusID)
		VALUES (@ClientID, @CustomerID, @LeadEventID, @ClaimObjectTypeID, @ClaimObjectID, @PolicyObjectTypeID, @PolicyObjectID, @PayeeTitle, @PayeeFirstName_DV, @PayeeLastName_DV, @PayeeOrganisation_DV, @PayeeAddress1_DV, @PayeeAddress2_DV, @PayeeAddress3_DV, @PayeeAddress4_DV, @PayeeAddress5_DV, @PayeePostcode_DV, @PayeeCountryID, @PayeeReference_DV, @PolicyholderReference_DV, @PaymentTypeID, @PaymentAmount, @PaymentDate, @PaymentRunDate, @DateCreated, @OriginalPaymentReferrence_DV, @ChequeNumber_DV, @AccountNumber_DV, @Sortcode_DV, @MaskedCardNumber_DV, @PaymentSourceTypeID, 1)		
		
		SELECT @OutgoingPaymentID = SCOPE_IDENTITY()
		
		UPDATE OutgoingPayment
		SET ParentOutgoingPaymentID=OutgoingPaymentID
		WHERE OutgoingPaymentID=@OutgoingPaymentID
		
		-- Insert CustomerLedger Entry		
		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		VALUES (@ClientID, @CustomerID, NULL, NULL, NULL, dbo.fn_GetDate_Local(), @PayeeReference_DV, @ItemValue_PaymentSourceType, NULL, NULL, -@PaymentAmount, @LeadEventID, @ObjectID, @ObjectTypeID, NULL, @OutgoingPaymentID, @WhoCreated, dbo.fn_GetDate_Local())
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[OutgoingPayment__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutgoingPayment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutgoingPayment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
