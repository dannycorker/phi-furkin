SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-06-23
-- Description:	_C00_ApplyPageGroup
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ApplyPageGroup] 
@MatterID int,
@PageGroupID int,
@WhoChanged int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Insert into MatterPageChoice (ClientID, MatterID, DetailFieldPageID, WhenModified, WhoModified)
	Select ClientID, @MatterID, dg.DetailFieldPageID, dbo.fn_GetDate_Local(), @WhoChanged
	From PageGroupDefaultPageLink dg WITH (NOLOCK)
	Where dg.PageGroupID = @PageGroupID
	and Not exists (
		Select *
		From MatterPageChoice mpc
		where MatterID = @MatterID
		and mpc.DetailFieldPageID = dg.DetailFieldPageID
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ApplyPageGroup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ApplyPageGroup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ApplyPageGroup] TO [sp_executeall]
GO
