SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientRelationship table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_GetByIncomingLeadTypeID]
(

	@IncomingLeadTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientRelationshipID],
					[ClientRelationshipName],
					[OutgoingClientID],
					[ReceivingClientID],
					[OutgoingLeadTypeID],
					[IncomingLeadTypeID],
					[OutgoingEventTypeID],
					[IncomingEventTypeID],
					[Enabled],
					[ClearLeadRefs]
				FROM
					[dbo].[ClientRelationship] WITH (NOLOCK) 
				WHERE
					[IncomingLeadTypeID] = @IncomingLeadTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_GetByIncomingLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_GetByIncomingLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_GetByIncomingLeadTypeID] TO [sp_executeall]
GO
