SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-03-09
-- Description:	Get nice In contact user details
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetNiceInContactUserDetails]
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT cpdv_agentID.DetailValue AS [AgentID],
			cpdv_token.DetailValue AS [Token],
			cpdv_baseURI.DetailValue AS [BaseURI]
	FROM ClientPersonnelDetailValues cpdv_agentID WITH (NOLOCK)
	INNER JOIN ClientPersonnelDetailValues cpdv_token WITH (NOLOCK) ON cpdv_token.ClientPersonnelID = cpdv_agentID.ClientPersonnelID AND cpdv_token.DetailFieldID = 315885
	INNER JOIN ClientPersonnelDetailValues cpdv_baseURI WITH (NOLOCK) ON cpdv_baseURI.ClientPersonnelID = cpdv_agentID.ClientPersonnelID AND cpdv_baseURI.DetailFieldID = 315886
	WHERE cpdv_agentID.DetailFieldID = 315887
	AND cpdv_agentID.ClientID = @ClientID
	AND cpdv_agentID.ClientPersonnelID = @ClientPersonnelID
	AND cpdv_agentID.DetailValue <> ''

END
GO
GRANT EXECUTE ON  [dbo].[_C600_GetNiceInContactUserDetails] TO [sp_executeall]
GO
