SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Script table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Script_Update]
(

	@ScriptID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ScriptName varchar (250)  ,

	@ScriptDescription varchar (2000)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@WhenModified datetime   ,

	@WhoModified int   ,

	@FinishEventTypeID int   ,

	@Published bit   ,

	@Disabled bit   ,

	@ScriptParentID int   ,

	@StartEventTypeID int   ,

	@ShowNotes bit   ,

	@NumberOfNotesToShow int   ,

	@IntegrateWithDialler bit   ,

	@ApplyFinishEventTo int   ,

	@Redact bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Script]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[ScriptName] = @ScriptName
					,[ScriptDescription] = @ScriptDescription
					,[WhenCreated] = @WhenCreated
					,[WhoCreated] = @WhoCreated
					,[WhenModified] = @WhenModified
					,[WhoModified] = @WhoModified
					,[FinishEventTypeID] = @FinishEventTypeID
					,[Published] = @Published
					,[Disabled] = @Disabled
					,[ScriptParentID] = @ScriptParentID
					,[StartEventTypeID] = @StartEventTypeID
					,[ShowNotes] = @ShowNotes
					,[NumberOfNotesToShow] = @NumberOfNotesToShow
					,[IntegrateWithDialler] = @IntegrateWithDialler
					,[ApplyFinishEventTo] = @ApplyFinishEventTo
					,[Redact] = @Redact
				WHERE
[ScriptID] = @ScriptID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Update] TO [sp_executeall]
GO
