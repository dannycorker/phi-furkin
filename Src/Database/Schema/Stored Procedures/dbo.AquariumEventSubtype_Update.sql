SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AquariumEventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_Update]
(

	@AquariumEventSubtypeID int   ,

	@OriginalAquariumEventSubtypeID int   ,

	@AquariumEventTypeID int   ,

	@AquariumEventSubtypeName varchar (100)  ,

	@AquariumEventSubtypeDescription varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@ClientID int   ,

	@StoredProcedure varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AquariumEventSubtype]
				SET
					[AquariumEventSubtypeID] = @AquariumEventSubtypeID
					,[AquariumEventTypeID] = @AquariumEventTypeID
					,[AquariumEventSubtypeName] = @AquariumEventSubtypeName
					,[AquariumEventSubtypeDescription] = @AquariumEventSubtypeDescription
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
					,[ClientID] = @ClientID
					,[StoredProcedure] = @StoredProcedure
				WHERE
[AquariumEventSubtypeID] = @OriginalAquariumEventSubtypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Update] TO [sp_executeall]
GO
