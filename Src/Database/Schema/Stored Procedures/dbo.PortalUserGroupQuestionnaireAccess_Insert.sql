SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalUserGroupQuestionnaireAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_Insert]
(

	@PortalUserGroupQuestionnaireAccessID int    OUTPUT,

	@ClientID int   ,

	@PortalUserGroupID int   ,

	@ClientQuestionnaireID int   ,

	@HasAccess bit   
)
AS


				
				INSERT INTO [dbo].[PortalUserGroupQuestionnaireAccess]
					(
					[ClientID]
					,[PortalUserGroupID]
					,[ClientQuestionnaireID]
					,[HasAccess]
					)
				VALUES
					(
					@ClientID
					,@PortalUserGroupID
					,@ClientQuestionnaireID
					,@HasAccess
					)
				-- Get the identity value
				SET @PortalUserGroupQuestionnaireAccessID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Insert] TO [sp_executeall]
GO
