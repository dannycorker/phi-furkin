SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FAS_FeesAndCharges table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_FeesAndCharges_Delete]
(

	@FasFeesAndChargesID int   
)
AS


				DELETE FROM [dbo].[FAS_FeesAndCharges] WITH (ROWLOCK) 
				WHERE
					[FasFeesAndChargesID] = @FasFeesAndChargesID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_FeesAndCharges_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_FeesAndCharges_Delete] TO [sp_executeall]
GO
