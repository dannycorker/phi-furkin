SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the NotificationGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroupMember_Insert]
(

	@NotificationGroupMemberID int    OUTPUT,

	@NotificationGroupID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   
)
AS


				
				INSERT INTO [dbo].[NotificationGroupMember]
					(
					[NotificationGroupID]
					,[ClientID]
					,[ClientPersonnelID]
					)
				VALUES
					(
					@NotificationGroupID
					,@ClientID
					,@ClientPersonnelID
					)
				-- Get the identity value
				SET @NotificationGroupMemberID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroupMember_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroupMember_Insert] TO [sp_executeall]
GO
