SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartySystemEvent table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystemEvent_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartySystemEventID int   = null ,

	@ThirdPartySystemID int   = null ,

	@ClientID int   = null ,

	@SubClientID int   = null ,

	@Name varchar (250)  = null ,

	@Description varchar (250)  = null ,

	@EventTypeID int   = null ,

	@LeadTypeID int   = null ,

	@ThirdPartySystemKey int   = null ,

	@MessageName varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartySystemEventID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [SubClientID]
	, [Name]
	, [Description]
	, [EventTypeID]
	, [LeadTypeID]
	, [ThirdPartySystemKey]
	, [MessageName]
    FROM
	[dbo].[ThirdPartySystemEvent] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartySystemEventID] = @ThirdPartySystemEventID OR @ThirdPartySystemEventID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ThirdPartySystemKey] = @ThirdPartySystemKey OR @ThirdPartySystemKey IS NULL)
	AND ([MessageName] = @MessageName OR @MessageName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartySystemEventID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [SubClientID]
	, [Name]
	, [Description]
	, [EventTypeID]
	, [LeadTypeID]
	, [ThirdPartySystemKey]
	, [MessageName]
    FROM
	[dbo].[ThirdPartySystemEvent] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartySystemEventID] = @ThirdPartySystemEventID AND @ThirdPartySystemEventID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ThirdPartySystemKey] = @ThirdPartySystemKey AND @ThirdPartySystemKey is not null)
	OR ([MessageName] = @MessageName AND @MessageName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent_Find] TO [sp_executeall]
GO
