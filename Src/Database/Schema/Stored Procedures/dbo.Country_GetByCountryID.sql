SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Country table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_GetByCountryID]
(

	@CountryID int   
)
AS


				SELECT
					[CountryID],
					[CountryName],
					[CountryNamePlain],
					[Alpha2Code],
					[Alpha3Code],
					[NumericCode],
					[ISO3166Dash2Code],
					[WhenCreated],
					[WhenModified],
					[UsePostcode],
					[PostcodeRegex],
					[DiallingCode]
				FROM
					[dbo].[Country] WITH (NOLOCK) 
				WHERE
										[CountryID] = @CountryID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_GetByCountryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_GetByCountryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_GetByCountryID] TO [sp_executeall]
GO
