SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2020-04-02
-- Description:	Add a JSON package to the queue table for trasmission to Vision
-- 2020-04-02 CPS for JIRA AAG-607 | Create general function for use in SAE
-- 2020-07-09 ALM Added @OneVisionID to _C00_Mediate_Push call
-- 2020-07-29 ALM Added @RequestType Policy 
-- 2020-07-30 ALM Added @RequestType PaymentAccount
-- 2020-08-03 ALM Added @RequestType Renewal
-- 2020-09-16 ALM Added @RequestType ExamFeeNote
-- 2021-04-01 ACE Removed redundant lead join
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Mediate_QueueForVision]
(
	 @RequestType	VARCHAR(50)
	,@HttpMethod	VARCHAR(5)	 -- "Post" or "Put"
	,@LeadEventID	INT
	,@OneVisionID	INT
)
AS
BEGIN
 
	SET NOCOUNT ON

	DECLARE	 @ApiOutboundKey				VARCHAR(50)
			,@TransferRequestType			VARCHAR(50)
			,@TransferResponseType			VARCHAR(50)
			,@ObjectID						VARCHAR(100)
			,@ParentObjectID				VARCHAR(100)
			,@EventComments					VARCHAR(2000) = '' + CHAR(13)+CHAR(10)
			,@JsonString					VARCHAR(MAX) = ''
			,@QueueID						INT
			,@LeadID						INT
			,@CustomerID					INT
			,@ValueBinary					VARBINARY(MAX)
			,@DocumentTitle					VARCHAR(100) = ''
			,@WhenCreated					DATETIME
			,@LeadDocumentID				INT
			,@ClientID						INT
			,@ClaimApiClientPersonnelID		INT
			,@PAMatterID					INT

	SELECT	 @LeadID	  = m.LeadID
			,@CustomerID  = m.CustomerID
			,@PAMatterID  = m.MatterID 
			,@WhenCreated = le.WhenCreated
			,@ClientID	  = le.ClientID
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
	WHERE le.LeadEventID = @LeadEventID

	/*Pick up transfer-specific data.  This will eventually be stored in proper tables*/	
	SELECT	@JsonString = 
	CASE	@RequestType
			WHEN 'Customer'				THEN dbo.fn_C600_Mediate_Harvest_Customer(@CustomerID,@PAMatterID,1,0,0)
			WHEN 'Pets'					THEN dbo.fn_C600_Mediate_Harvest_Pet	 (@LeadID	 ,1,1,0)
			WHEN 'Policy'				THEN dbo.fn_C600_Mediate_Harvest_Policy	 (@PAMatterID,1)
			WHEN 'PaymentAccount'		THEN dbo.fn_C600_Mediate_Harvest_PaymentAccount(@PAMatterID) 
			WHEN 'Renewal'				THEN dbo.fn_C600_Mediate_Harvest_Renewal(@PAMatterID) 
			WHEN 'RollbackPolicy'		THEN dbo.fn_C600_Mediate_Harvest_RollbackPolicy(@PAMatterID) 
			WHEN 'RollbackPolicyTerms'	THEN dbo.fn_C600_Mediate_Harvest_RollbackPolicyTerm(@PAMatterID) 
			WHEN 'VisionExamNote'		THEN dbo.fn_C600_Mediate_Harvest_Note(@LeadID, 1)
			WHEN 'VisionWellnessNote'	THEN dbo.fn_C600_Mediate_Harvest_Note(@LeadID, 2)
			WHEN 'Wellness'				THEN dbo.fn_C600_Mediate_Harvest_Wellness	 (@PAMatterID,1)
			WHEN 'RollbackWellness'		THEN dbo.fn_C600_Mediate_Harvest_RollbackWellness(@PAMatterID) 
			WHEN 'PolicyTerm'			THEN dbo.fn_C600_Mediate_Harvest_PolicyTerm (@PAMatterID)
			WHEN 'ExamFee'				THEN dbo.fn_C600_Mediate_Harvest_ExamFee (@PAMatterID)
			WHEN 'RollbackExamFee'		THEN dbo.fn_C600_Mediate_Harvest_ExamFee (@PAMatterID)
			ELSE ''
	END

	SELECT	@ObjectID = --CONVERT(VARCHAR(MAX),NEWID())
	CASE	@RequestType
			WHEN 'Customer'					THEN @CustomerID
			WHEN 'Pets'						THEN @LeadID
			WHEN 'Policy'					THEN @PAMatterID
			WHEN 'PaymentAccount'			THEN @PAMatterID
			WHEN 'Renewal'					THEN @PAMatterID
			WHEN 'RollbackPolicy'			THEN @PAMatterID
			WHEN 'RollbackPolicyTerms'		THEN @PAMatterID
			WHEN 'VisionExamNote'			THEN @LeadID
			WHEN 'VisionWellnessNote'		THEN @LeadID
			WHEN 'Wellness'					THEN @PAMatterID
			WHEN 'RollbackWellness'			THEN @PAMatterID
			WHEN 'PolicyTerm'				THEN @PAMatterID
			WHEN 'ExamFee'					THEN @PAMatterID
			WHEN 'RollbackExamFee'			THEN @PAMatterID
			ELSE ''
	END

	SELECT	@ParentObjectID = @CustomerID

	SELECT	 @ValueBinary = CAST(@JsonString as VARBINARY(MAX))
			,@ClaimApiClientPersonnelID = dbo.fn_C600_GetDatabaseSpecificConfigValue('ClaimsApiClientPersonnelID')

	SELECT @ApiOutboundKey = dbo.fnGetSimpleDv(313894,@ClaimApiClientPersonnelID) /*AppKey*/

	/*Add the request to the queue*/	
	IF @RequestType = 'Customer'
	BEGIN
		SELECT	 @TransferRequestType	= 'VisionCustomerRequest'
				,@TransferResponseType	= 'SimpleValueResponse'
	END
	ELSE
	IF @RequestType = 'Pets'
	BEGIN
		SELECT	 @TransferRequestType	= 'VisionPetRequest'
				,@TransferResponseType	= 'VisionPetResponse'
	END
	ELSE
	IF @RequestType in ('Policy', 'RollbackPolicy')
	BEGIN
		SELECT	 @TransferRequestType	= 'VisionPolicyRequest'
				,@TransferResponseType	= 'VisionPolicyResponse'
	END
	IF @RequestType in ('Renewal', 'RollbackPolicyTerms', 'Wellness', 'RollbackWellness', 'PolicyTerm', 'ExamFee', 'RollbackExamFee')
	BEGIN
		SELECT	 @TransferRequestType	= 'VisionPolicyTermRequest'
				,@TransferResponseType	= 'VisionPolicyTermResponse'
	END
	IF @RequestType = 'PaymentAccount'
	BEGIN
		SELECT	 @TransferRequestType	= 'VisionPaymentRequest'
				,@TransferResponseType	= 'VisionPaymentResponse'
	END
	IF @RequestType = 'VisionWellnessNote' OR @RequestType = 'VisionExamNote'
	BEGIN
		SELECT	 @TransferRequestType	= 'VisionNotesRequest'
				,@TransferResponseType	= 'VisionNotesRespone'
	END
	
	
	EXEC [_C00_Mediate_Push]	 @AppKey			= @ApiOutboundKey
								,@Queue				= 'Out'
								,@Flow				= 'Queue'
								,@Action			= 'Push'
								,@Method			= @HttpMethod 
								,@RequestBody		= @JsonString
								,@RequestType		= @TransferRequestType
								,@ResponseBody		= NULL
								,@ResponseType		= @TransferResponseType
								,@Exception			= NULL
								,@ExceptionCount	= 0
								,@ObjectID			= @ObjectID
								,@ParentObjectID	= NULL
								,@QueueID			= @QueueID OUTPUT
								,@SuppressOutput	= 1
								,@OneVisionID		= @OneVisionID

	/*Store the request JSON as a lead document against this event*/
	SELECT @EventComments	+= 'OutboundQueueID '	+ ISNULL(CONVERT(VARCHAR	 ,@QueueID)		  ,'NULL') + ' queued for export.'  + CHAR(13)+CHAR(10)
							+  'Parent ObjectID = ' + ISNULL(CONVERT(VARCHAR(MAX),@ParentObjectID),'NULL')							+ CHAR(13)+CHAR(10)
							+  'Transfer Type = '	+ ISNULL(CONVERT(VARCHAR(MAX),@RequestType)	  ,'NULL')

	SELECT @DocumentTitle	=  ISNULL(@RequestType,'NULL')
							+  '_LeadEventID_' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
							+  '_QueueID_' + ISNULL(CONVERT(VARCHAR,@QueueID),'NULL') + '.json'

	EXEC @LeadDocumentID = _C00_CreateLeadDocument @ClientID, @LeadID, @ValueBinary, @DocumentTitle, NULL, @WhenCreated, @ClaimApiClientPersonnelID, @DocumentTitle,NULL, 'TXT', NULL, NULL, NULL, NULL, NULL, NULL, 'TXT', NULL, NULL

	UPDATE le
	SET  LeadDocumentID = @LeadDocumentID
		,Comments = ISNULL(le.Comments + CHAR(13)+CHAR(10),'') + @EventComments
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Mediate_QueueForVision] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Mediate_QueueForVision] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Mediate_QueueForVision] TO [sp_executeall]
GO
