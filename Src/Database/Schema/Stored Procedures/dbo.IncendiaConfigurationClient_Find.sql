SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the IncendiaConfigurationClient table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_Find]
(

	@SearchUsingOR bit   = null ,

	@IncendiaConfigurationClientID int   = null ,

	@AquariumClientID int   = null ,

	@Username varchar (50)  = null ,

	@Password varchar (50)  = null ,

	@URI varchar (150)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [IncendiaConfigurationClientID]
	, [AquariumClientID]
	, [Username]
	, [Password]
	, [URI]
    FROM
	[dbo].[IncendiaConfigurationClient] WITH (NOLOCK) 
    WHERE 
	 ([IncendiaConfigurationClientID] = @IncendiaConfigurationClientID OR @IncendiaConfigurationClientID IS NULL)
	AND ([AquariumClientID] = @AquariumClientID OR @AquariumClientID IS NULL)
	AND ([Username] = @Username OR @Username IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([URI] = @URI OR @URI IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [IncendiaConfigurationClientID]
	, [AquariumClientID]
	, [Username]
	, [Password]
	, [URI]
    FROM
	[dbo].[IncendiaConfigurationClient] WITH (NOLOCK) 
    WHERE 
	 ([IncendiaConfigurationClientID] = @IncendiaConfigurationClientID AND @IncendiaConfigurationClientID is not null)
	OR ([AquariumClientID] = @AquariumClientID AND @AquariumClientID is not null)
	OR ([Username] = @Username AND @Username is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([URI] = @URI AND @URI is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Find] TO [sp_executeall]
GO
