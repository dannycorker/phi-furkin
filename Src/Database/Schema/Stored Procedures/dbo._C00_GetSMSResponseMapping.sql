SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 21-09-2012
-- Description:	Gets the next event to apply based upon the customers response
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetSMSResponseMapping]

	@ClientID INT,
	@EventTypeID VARCHAR(2000),
	@CustomerResponse VARCHAR(2000)	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Client 2 Version --
	--SELECT	tr.TableRowID TablRowID, 
	--		tdvEventTypeID.DetailValue [Question SMS EventTypeID],
	--		tdvResponse.DetailValue [Customer Response],
	--		tdvEventToApply.DetailValue [EventToApply],
	--		tdvCustomerResponseCode.DetailValue [Customer Unique Response Code]
	--FROM TableRows tr WITH (NOLOCK) 
	--INNER JOIN dbo.TableDetailValues tdvEventTypeID WITH (NOLOCK) ON tdvEventTypeID.TableRowID = tr.TableRowID AND tdvEventTypeID.DetailValue=@EventTypeID AND tdvEventTypeID.DetailFieldID=157234
	--INNER JOIN dbo.TableDetailValues tdvResponse WITH (NOLOCK) ON tdvResponse.TableRowID = tr.TableRowID AND tdvResponse.DetailValue=@CustomerResponse AND tdvResponse.DetailFieldID=157214
	--INNER JOIN dbo.TableDetailValues tdvEventToApply WITH (NOLOCK) ON tdvEventToApply.TableRowID = tr.TableRowID AND tdvEventToApply.DetailFieldID=157215
	--INNER JOIN dbo.TableDetailValues tdvCustomerResponseCode WITH (NOLOCK) ON tdvCustomerResponseCode.TableRowID = tr.TableRowID AND tdvCustomerResponseCode.DetailFieldID=157230
	--WHERE tr.ClientID=@ClientID

--380	157479	157475
--381	157479	157476
--382	157479	157477
--383	157479	157478

	DECLARE @QuestionFieldID INT
	DECLARE @ResponseFieldID INT
	DECLARE @EventToApplyFieldID INT
	DECLARE @UniqueResponseFieldID INT

	SELECT @QuestionFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=380
	SELECT @ResponseFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=381
	SELECT @EventToApplyFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=382
	SELECT @UniqueResponseFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=383
	
	--Client 253 Version --
		SELECT	tr.TableRowID TablRowID, 
			tdvEventTypeID.DetailValue [Question SMS EventTypeID],
			tdvResponse.DetailValue [Customer Response],
			tdvEventToApply.DetailValue [EventToApply],
			tdvCustomerResponseCode.DetailValue [Customer Unique Response Code]
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvEventTypeID WITH (NOLOCK) ON tdvEventTypeID.TableRowID = tr.TableRowID AND tdvEventTypeID.DetailValue=@EventTypeID AND tdvEventTypeID.DetailFieldID=@QuestionFieldID
	INNER JOIN dbo.TableDetailValues tdvResponse WITH (NOLOCK) ON tdvResponse.TableRowID = tr.TableRowID AND tdvResponse.DetailValue=@CustomerResponse AND tdvResponse.DetailFieldID=@ResponseFieldID
	INNER JOIN dbo.TableDetailValues tdvEventToApply WITH (NOLOCK) ON tdvEventToApply.TableRowID = tr.TableRowID AND tdvEventToApply.DetailFieldID=@EventToApplyFieldID
	INNER JOIN dbo.TableDetailValues tdvCustomerResponseCode WITH (NOLOCK) ON tdvCustomerResponseCode.TableRowID = tr.TableRowID AND tdvCustomerResponseCode.DetailFieldID=@UniqueResponseFieldID
	WHERE tr.ClientID=@ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetSMSResponseMapping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetSMSResponseMapping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetSMSResponseMapping] TO [sp_executeall]
GO
