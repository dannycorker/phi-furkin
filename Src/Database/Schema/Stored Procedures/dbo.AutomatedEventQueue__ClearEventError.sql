SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-24
-- Description:	Clear previous error so the event will resubmit
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__ClearEventError]
	@AutomatedEventQueueID int
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Clear previous error so the event will resubmit
	*/
	UPDATE dbo.AutomatedEventQueue 
	SET ErrorMessage = NULL
	WHERE AutomatedEventQueueID = @AutomatedEventQueueID 

END






GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__ClearEventError] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__ClearEventError] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__ClearEventError] TO [sp_executeall]
GO
