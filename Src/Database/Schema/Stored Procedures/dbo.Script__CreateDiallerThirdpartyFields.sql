SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 18/06/2015
-- Description:	Checks to see if the following third party system (50 - Script Dialler, third party field group 95)
-- field mappings exist:-
--  ThirdPartyFieldID	Field Name
--	4200				Thirdparty LeadID
--	4201				ListID
--	4202				ImportID
--	4203				Dialler Callback URL
--
-- If they do not exist it creates the fields and the thirdparty mappings
-- =============================================
CREATE PROCEDURE [dbo].[Script__CreateDiallerThirdpartyFields]
	@ClientID INT
AS
BEGIN
	
	SET NOCOUNT ON;

	-------------------------------- Client Level Dialler Callback URL Mapping 4203 ------------------------------
	DECLARE @MappingID_DiallerCallbackURL INT
	DECLARE @ApiInformationDetailFieldPageID INT
	DECLARE @CallbackURL_FieldID INT
	
	SELECT @MappingID_DiallerCallbackURL=ThirdPartyFieldMappingID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4203
	
    IF @MappingID_DiallerCallbackURL IS NULL -- mapping does not exist for callback URL
	BEGIN
		
		SELECT @ApiInformationDetailFieldPageID=DetailFieldPageID 
		FROM DetailFieldPages WITH (NOLOCK) 
		WHERE PageName='API Information' AND LeadOrMatter=12 AND LeadTypeID=1 AND ClientID=@ClientID
		
		IF @ApiInformationDetailFieldPageID IS NULL -- client level api page does not exist so create it and add callback url field
		BEGIN
			-- Create API Information Page at the Client Level
			INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled)
			VALUES (@ClientID, 12, 1,'API Information', 'API Information',0,1)

			SET @ApiInformationDetailFieldPageID = SCOPE_IDENTITY()

			-- Create Call Back URL detail field
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
			VALUES (@ClientID, 12, 'Dialler Callback URL','Dialler Callback URL',1,0,0,NULL,0,1,@ApiInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)
			
			SET @CallbackURL_FieldID = SCOPE_IDENTITY()
			
			-- Create third party mapping record 
			INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
			VALUES (@ClientID, 1, 4203, @CallbackURL_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
		END
		ELSE -- API page does exist 
		BEGIN 
			
			SELECT @CallbackURL_FieldID=DetailFieldID 
			FROM DetailFields WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND FieldName='Dialler Callback URL' AND DetailFieldPageID=@ApiInformationDetailFieldPageID
			
			IF @CallbackURL_FieldID IS NULL -- field does not exist so make it and map it
			BEGIN
				-- Create Call Back URL detail field
				INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
				VALUES (@ClientID, 12, 'Dialler Callback URL','Dialler Callback URL',1,0,0,NULL,0,1,@ApiInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)
			
				SET @CallbackURL_FieldID = SCOPE_IDENTITY()
			
				-- Create third party mapping record 
				INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
				VALUES (@ClientID, 1, 4203, @CallbackURL_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())

			END
			ELSE -- field exists so just create mapping since we only fot here because the mapping did not exist in the first place
			BEGIN
			
				INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
				VALUES (@ClientID, 1, 4203, @CallbackURL_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())			
				
			END
		
		END
	END
	-------------------------------- End of Client Level Dialler Callback URL Mapping 4203 ------------------------------
		
	-------------------------------- Customer Level Dialler Mappings 4200,4201,4202 -------------------------------------
	DECLARE @MappingID_ThirdpartyLeadID INT
	DECLARE @MappingID_ListID INT
	DECLARE @MappingID_ImportID INT
	DECLARE @ListInformationDetailFieldPageID INT
	DECLARE @ThirdpartyLeadID_FieldID INT
	DECLARE @ListID_FieldID INT
	DECLARE @ImportID_FieldID INT

	SELECT @ListInformationDetailFieldPageID=DetailFieldPageID 
	FROM DetailFieldPages WITH (NOLOCK) 
	WHERE PageName='List Information' AND LeadOrMatter=10 AND LeadTypeID=0 AND ClientID=@ClientID
		
	IF @ListInformationDetailFieldPageID IS NULL -- list page does not exist
	BEGIN
		
		-- Create the list information page
		INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled)
		VALUES (@ClientID, 10, 0,'List Information', 'List Information',0,1)
	
		SET @ListInformationDetailFieldPageID = SCOPE_IDENTITY()
		
		-- Create ThirdPartyLeadID Field
		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
		VALUES (@ClientID, 10, 'Thirdparty LeadID','Thirdparty LeadID',1,0,0,NULL,0,1,@ListInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)

		SET @ThirdpartyLeadID_FieldID = SCOPE_IDENTITY()

		-- Create List ID field
		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
		VALUES(@ClientID, 10, 'ListID','ListID',1,0,0,NULL,0,1,@ListInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)

		SET @ListID_FieldID = SCOPE_IDENTITY()

		-- Create Import ID field
		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
		VALUES(@ClientID, 10, 'ImportID','ImportID',1,0,0,NULL,0,1,@ListInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)
	
		SET @ImportID_FieldID = SCOPE_IDENTITY()
		
		INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
		VALUES (@ClientID, 0, 4200, @ThirdpartyLeadID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
			   (@ClientID, 0, 4201, @ListID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
			   (@ClientID, 0, 4202, @ImportID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())			   
	
	END
	ELSE -- List Information Page exists -- check each field
	BEGIN
	
		--------------------- Thirdparty LeadID Check/Creation -------------------------
		SELECT @ThirdpartyLeadID_FieldID=DetailFieldID 
		FROM DetailFields WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND FieldName='Thirdparty LeadID' AND DetailFieldPageID=@ListInformationDetailFieldPageID
		
		IF @ThirdpartyLeadID_FieldID IS NULL -- third party lead id field does not exist so create it
		BEGIN
			
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
			VALUES (@ClientID, 10, 'Thirdparty LeadID','Thirdparty LeadID',1,0,0,NULL,0,1,@ListInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)

			SET @ThirdpartyLeadID_FieldID = SCOPE_IDENTITY()
			
			INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
			VALUES (@ClientID, 0, 4200, @ThirdpartyLeadID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
			
		END
		ELSE  -- the thirdparty leadid field exists - check mapping
		BEGIN
		
			SELECT @MappingID_ThirdpartyLeadID=ThirdPartyFieldMappingID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND ThirdPartyFieldID=4200 AND DetailFieldID=@ThirdpartyLeadID_FieldID
			
			IF @MappingID_ThirdpartyLeadID IS NULL --- mapping does not exist so create it
			BEGIN
			
				INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
				VALUES (@ClientID, 0, 4200, @ThirdpartyLeadID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
			
			END
		
		END
		--------------------- End Thirdparty LeadID Check/Creation -------------------------
		
		--------------------- ListID Check/Creation -------------------------
		SELECT @ListID_FieldID=DetailFieldID 
		FROM DetailFields WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND FieldName='ListID' AND DetailFieldPageID=@ListInformationDetailFieldPageID
		
		IF @ListID_FieldID IS NULL -- List id field does not exist so create it
		BEGIN
			
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
			VALUES (@ClientID, 10, 'ListID','ListID',1,0,0,NULL,0,1,@ListInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)

			SET @ListID_FieldID = SCOPE_IDENTITY()
			
			INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
			VALUES (@ClientID, 0, 4201, @ListID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
			
		END
		ELSE  -- the ListID field exists - check mapping
		BEGIN
		
			SELECT @MappingID_ListID=ThirdPartyFieldMappingID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND ThirdPartyFieldID=4201 AND DetailFieldID=@ListID_FieldID
			
			IF @MappingID_ListID IS NULL --- mapping does not exist so create it
			BEGIN
			
				INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
				VALUES (@ClientID, 0, 4201, @ListID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
			
			END
		
		END
		--------------------- End ListID Check/Creation -------------------------
		
		--------------------- ImportID Check/Creation -------------------------
		SELECT @ImportID_FieldID=DetailFieldID 
		FROM DetailFields WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND FieldName='ImportID' AND DetailFieldPageID=@ListInformationDetailFieldPageID
		
		IF @ImportID_FieldID IS NULL -- Import id field does not exist so create it
		BEGIN
			
			INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
			VALUES (@ClientID, 10, 'ImportID','ImportID',1,0,0,NULL,0,1,@ListInformationDetailFieldPageID,1,0,'',NULL,1,NULL,1,1,'','','','',NULL,NULL,'','',1,0,0,0,0,0,0,0,1)

			SET @ImportID_FieldID = SCOPE_IDENTITY()
			
			INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
			VALUES (@ClientID, 0, 4202, @ImportID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
			
		END
		ELSE  -- the ImportID field exists - check mapping
		BEGIN
		
			SELECT @MappingID_ImportID=ThirdPartyFieldMappingID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND ThirdPartyFieldID=4202 AND DetailFieldID=@ImportID_FieldID
			
			IF @MappingID_ImportID IS NULL --- mapping does not exist so create it
			BEGIN
			
				INSERT INTO ThirdPartyFieldMapping  (ClientID, LeadTypeID, ThirdPartyFieldID, DetailFieldID, ColumnFieldID, ThirdPartyFieldGroupID, IsEnabled, WhenCreated, WhenModified)
				VALUES (@ClientID, 0, 4202, @ImportID_FieldID, NULL, 95,1, dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
			
			END
		
		END
		--------------------- End ImportID Check/Creation -------------------------			
	
	END
			
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__CreateDiallerThirdpartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__CreateDiallerThirdpartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__CreateDiallerThirdpartyFields] TO [sp_executeall]
GO
