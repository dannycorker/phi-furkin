SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-09-10
-- Description:	Admin dashboard account Latest PatchNotes
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminDashboardLatestPatchNotes]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT pn.PatchNoteID, pn.Location, pn.PatchNoteText
	FROM PatchNote pn WITH (NOLOCK)
	WHERE AdminOnly = 0
	AND pn.PatchID = (
		SELECT TOP 1 p.PatchID 
		FROM Patch p WITH (NOLOCK)
		ORDER BY PatchID DESC)
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardLatestPatchNotes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAdminDashboardLatestPatchNotes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAdminDashboardLatestPatchNotes] TO [sp_executeall]
GO
