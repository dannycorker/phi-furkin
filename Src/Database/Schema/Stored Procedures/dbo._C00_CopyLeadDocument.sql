SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2009-09-09
-- Description:	Copy a LeadDocument record to another lead (optionally for a different client).
-- JWG 2012-07-12 Still insert into LeadDocument and LeadDocumentFS, but read from vLeadDocumentList to cater for archived documents.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyLeadDocument] 
	@From_LeadDocumentID int = NULL, 
	@To_LeadID int = NULL, 
	@To_ClientID int = NULL, 
	@FromLeadEventID int = NULL 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @NewClientID INT, 
	@NewLeadID INT,
	@NewLeadDocumentID INT 
	
	/* If a LeadEventID was passed in, use that to get the LeadDocumentID */
	IF @From_LeadDocumentID IS NULL AND @FromLeadEventID > 0
	BEGIN
		SELECT @From_LeadDocumentID = le.LeadDocumentID 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @FromLeadEventID 
	END
	
	/* If new lead and/or client were specified, use them, otherwise default to the current values from the document */
	SELECT @NewClientID = COALESCE(@To_ClientID, ld.ClientID), 
	@NewLeadID = COALESCE(@To_LeadID, ld.LeadID)
	FROM dbo.LeadDocument ld WITH (NOLOCK) 
	WHERE ld.LeadDocumentID = @From_LeadDocumentID 
	
	IF @From_LeadDocumentID > 0
	BEGIN

		INSERT INTO [dbo].[LeadDocument]
		(
			/* JWG 2012-07-12 Need all columns in here again, except for the blobs and archive details */
			ClientID, 
			LeadID, 
			DocumentTypeID, 
			LeadDocumentTitle, 
			UploadDateTime, 
			WhoUploaded, 
			DocumentBLOB, 
			FileName, 
			EmailBLOB, 
			DocumentFormat, 
			EmailFrom, 
			EmailTo, 
			CcList, 
			BccList, 
			ElectronicSignatureDocumentKey, 
			Encoding, 
			ContentFormat, 
			ZipFormat, 
			DocumentBlobSize, 
			EmailBlobSize, 
			DocumentDatabaseID, 
			WhenArchived, 
			DocumentTypeVersionID		
		)
		SELECT 
		@NewClientID, 
		@NewLeadID, 
		ld.DocumentTypeID, 
		ld.LeadDocumentTitle, 
		ld.UploadDateTime, 
		ld.WhoUploaded, 
		CAST('' AS VARBINARY) AS DocumentBLOB, 
		ld.FileName, 
		NULL AS EmailBLOB, 
		ld.DocumentFormat, 
		ld.EmailFrom, 
		ld.EmailTo, 
		ld.CcList, 
		ld.BccList, 
		ld.ElectronicSignatureDocumentKey, 
		ld.Encoding, 
		ld.ContentFormat, 
		ld.ZipFormat, 
		ld.DocumentBlobSize, 
		ld.EmailBlobSize, 
		NULL AS DocumentDatabaseID, 
		NULL AS WhenArchived, 
		ld.DocumentTypeVersionID
		FROM dbo.LeadDocument ld WITH (NOLOCK) 
		WHERE ld.LeadDocumentID = @From_LeadDocumentID 
	
		SELECT @NewLeadDocumentID = SCOPE_IDENTITY()

		/* Check that the create worked */
		IF @NewLeadDocumentID > 0
		BEGIN
		
			/* INSERT LeadDocumentFS */
			/* FROM LeadDocumentFS */

			SET IDENTITY_INSERT [dbo].[LeadDocumentFS] ON
		
			INSERT INTO [dbo].[LeadDocumentFS]([LeadDocumentID],[ClientID],[LeadID],[DocumentTypeID],[LeadDocumentTitle],[UploadDateTime],[WhoUploaded],[DocumentBLOB],[FileName],[EmailBLOB],[DocumentFormat],[EmailFrom],[EmailTo],[CcList],[BccList],[ElectronicSignatureDocumentKey], [Encoding], [ContentFormat], [ZipFormat]) 
			SELECT @NewLeadDocumentID, @NewClientID, @NewLeadID, ld.DocumentTypeID, ld.LeadDocumentTitle, ld.UploadDateTime, ld.WhoUploaded, ld.DocumentBLOB, ld.FileName, ld.EmailBLOB,ld.DocumentFormat,ld.EmailFrom,ld.EmailTo,ld.CcList,ld.BccList,ld.ElectronicSignatureDocumentKey, ld.Encoding, ld.ContentFormat, ld.ZipFormat 
			FROM dbo.vLeadDocumentList ld WITH (NOLOCK) 
			WHERE ld.LeadDocumentID = @From_LeadDocumentID 

			SET IDENTITY_INSERT [dbo].[LeadDocumentFS] OFF
			
		END		
		ELSE
		BEGIN
			/* No new LeadDocument found */
			SELECT @NewLeadDocumentID = -1
		END	
	END
	ELSE
	BEGIN
		/* No old LeadDocumentID available */
		SELECT @NewLeadDocumentID = -1
	END
	
	RETURN @NewLeadDocumentID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyLeadDocument] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyLeadDocument] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyLeadDocument] TO [sp_executeall]
GO
