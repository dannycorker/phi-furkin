SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Ian Slack
-- Date: 
-- Purpose: Select records from the DetailFieldPages table through @LeadTypeID, @ClientID
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DetailFieldPages__GetByLeadTypeIDAndClientID]
(
	@LeadTypeID int,
	@ClientID int
)
AS
	SET ANSI_NULLS ON

	SELECT dfp.*
	FROM dbo.fnDetailFieldPagesShared(@ClientID) dfp 
	WHERE
		[LeadTypeID] = @LeadTypeID
	AND
		[ClientID] = @ClientID

	SELECT @@ROWCOUNT
				
			




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetByLeadTypeIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetByLeadTypeIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetByLeadTypeIDAndClientID] TO [sp_executeall]
GO
