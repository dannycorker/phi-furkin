SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AquariumOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_GetByAquariumOptionID]
(

	@AquariumOptionID int   
)
AS


				SELECT
					[AquariumOptionID],
					[AquariumOptionTypeID],
					[AquariumOptionName],
					[AquariumOptionDescription]
				FROM
					[dbo].[AquariumOption] WITH (NOLOCK) 
				WHERE
										[AquariumOptionID] = @AquariumOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_GetByAquariumOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_GetByAquariumOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_GetByAquariumOptionID] TO [sp_executeall]
GO
