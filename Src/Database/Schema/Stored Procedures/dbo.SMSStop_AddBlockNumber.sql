SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-06-19
-- Description:
-- =============================================

CREATE PROCEDURE [dbo].[SMSStop_AddBlockNumber]
(
	@MobileNumber	varchar(50)	
	, @LeadTypeID	int
	, @ClientID		int	
)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO SMSSTOP (ClientID, LeadTypeID, MobileNumber, StopSMS, CustomerID, OptOutRequestSent, Created, Updated)
	VALUES (@ClientID, @LeadTypeID, @MobileNumber, 1, 0,0,dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())


END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_AddBlockNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStop_AddBlockNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_AddBlockNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStop_AddBlockNumber] TO [sp_executehelper]
GO
