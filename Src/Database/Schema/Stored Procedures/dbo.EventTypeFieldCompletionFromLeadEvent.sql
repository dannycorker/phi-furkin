SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-19
-- Description:	Populate detail fields based on events 
--              JWG 2009-06-01 New "Insert" option called "Always Delete" (5)
--              This enables administrators to force the user to enter a
--              value every single time they apply an event of this type.
--              JWG 2010-08-20 Store DetailValueHistory if required.
--              JWG 2011-05-12 CustomerDetailValues too.
--				CS 2012-05-06  Fixed problem whereby LeadEvent duplicate deltion (by trigger) was wiping out Linked fields
--				PR 2016-01-29 Added CaseDetailValues too.
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeFieldCompletionFromLeadEvent] 
	@LeadEventID int,
	@WhenCreated datetime = null,  -- This will be looked up if null 
	@AquariumOptionTypeID int = 1, -- 1 = Insert (default), 2 = Delete
	@EventTypeID int = null, 
	@LeadID int = null, 
	@CaseID int = null, 
	@ClientID int = null, 
	@ClientPersonnelID int = null 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ETFCRowcount int,
	@DFList varchar(max),
	@LoopDetailFieldID int, 
	@LoopDetailFieldLeadOrMatter int,  -- Lead or Matter
	@LoopOptionID int,                 -- Used for Insert/Delete option as determined by @AquariumOptionTypeID
	@LastGoodLeadEventID int, 
	@LoopMaintainHistory bit, 
	@ValueForHistory varchar(30) = '', 
	@CustomerID int = null,
	@DeletionComments varchar(100)

	-- This table caters for multiple fields after one event,
	-- avoiding the use of a cursor that might lock TempDB.
	DECLARE @ETFCList TABLE (
		DetailFieldID int, 
		LeadOrMatter int, 
		InsertOptionID int,
		DeleteOptionID int, 
		MaintainHistory bit
	)

    -- If all IDs are known then we have slightly less work to do,
	-- otherwise look them all up from the LeadEvent record.
	IF @EventTypeID IS NULL OR @LeadID IS NULL OR @CaseID IS NULL OR @WhenCreated IS NULL OR @ClientID IS NULL OR @ClientPersonnelID IS NULL 
	BEGIN

		SELECT @EventTypeID = le.EventTypeID,
		@LeadID = le.LeadID, 
		@CaseID = le.CaseID,
		@WhenCreated = le.WhenCreated, 
		@ClientID = le.ClientID, 
		@ClientPersonnelID = le.WhoCreated,
		@DeletionComments = le.DeletionComments
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @LeadEventID

	END

	IF @EventTypeID IS NOT NULL
	BEGIN

		-- Get the list of all possible work to do
		INSERT @ETFCList(DetailFieldID, LeadOrMatter, InsertOptionID, DeleteOptionID, MaintainHistory) 
		SELECT e.DetailFieldID, df.LeadOrMatter, e.InsertOptionID, e.DeleteOptionID, df.MaintainHistory 
		FROM dbo.EventTypeFieldCompletion e WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = e.DetailFieldID 
		WHERE e.EventTypeID = @EventTypeID 

		SET @ETFCRowcount = @@ROWCOUNT
		
	END

	-- Ensure that any fields we need actually exist
	-- This needs to be done regardless of whether we are inserting or deleting the event
	-- as deletes can populate fields too.
	IF @ETFCRowcount > 0 
	BEGIN
		SELECT @DFList = ''

		-- Recursive field population; this creates a string of values eg
		-- 1234,5678,
		SELECT @DFList = @DFList + convert(varchar, DetailFieldID) + ',' 
		FROM @ETFCList

		-- Remove the trailing comma
		SELECT @DFList = LEFT(@DFList, (LEN(@DFList) - 1) )

		-- Create the fields
		EXEC dbo._C00_CreateDetailFields @DFList, @LeadID

	END

	WHILE @ETFCRowcount > 0 
	BEGIN

		-- Find the first/next field to populate/depopulate
		SELECT TOP 1 
		@LoopDetailFieldID = e.DetailFieldID, 
		@LoopDetailFieldLeadOrMatter = e.LeadOrMatter, 
		@LoopOptionID = CASE @AquariumOptionTypeID WHEN 1 THEN e.InsertOptionID WHEN 2 THEN e.DeleteOptionID ELSE NULL END, 
		@LoopMaintainHistory = e.MaintainHistory 
		FROM @ETFCList e 

		/* Get the CustomerID now for repeated later use */
		IF @LoopDetailFieldLeadOrMatter = 10
		BEGIN
			SELECT @CustomerID = l.CustomerID 
			FROM dbo.Lead l WITH (NOLOCK) 
			WHERE l.LeadID = @LeadID 
		END
		
		-- Option 1 = inserted event. 
		IF @AquariumOptionTypeID = 1
		BEGIN

			IF @LoopOptionID <> 4 -- 4 = Never change on insert
			BEGIN

				-- Work out the value to store. This is used for history later too.
				SELECT @ValueForHistory = CASE @LoopOptionID WHEN 5 THEN '' ELSE convert(char(10), @WhenCreated, 126) END
				
				IF @LoopDetailFieldLeadOrMatter = 1
				BEGIN

					-- Update LeadDetailValues
					UPDATE LeadDetailValues 
					SET DetailValue = @ValueForHistory 
					WHERE LeadID = @LeadID 
					AND DetailFieldID = @LoopDetailFieldID 
					AND (
						(@LoopOptionID = 1)
						OR
						(@LoopOptionID = 2 AND (DetailValue IS NULL OR DetailValue = ''))
						OR
						(@LoopOptionID = 3 AND (DetailValue IS NOT NULL AND DetailValue <> ''))
						OR
						(@LoopOptionID = 5)
					)

				END
				IF @LoopDetailFieldLeadOrMatter = 2
				BEGIN

					-- Update any MatterDetailValues for this Case
					-- that match the option rules.
					UPDATE MatterDetailValues 
					SET DetailValue = @ValueForHistory
					FROM dbo.MatterDetailValues mdv 
					INNER JOIN dbo.Matter m ON m.MatterID = mdv.MatterID 
					WHERE m.CaseID = @CaseID 
					AND mdv.DetailFieldID = @LoopDetailFieldID 
					AND (
						(@LoopOptionID = 1)
						OR
						(@LoopOptionID = 2 AND (mdv.DetailValue IS NULL OR mdv.DetailValue = ''))
						OR
						(@LoopOptionID = 3 AND (mdv.DetailValue IS NOT NULL AND mdv.DetailValue <> ''))
						OR
						(@LoopOptionID = 5)
					)

				END
				IF @LoopDetailFieldLeadOrMatter = 10
				BEGIN
					
					/* Update CustomerDetailValues */
					UPDATE dbo.CustomerDetailValues 
					SET DetailValue = @ValueForHistory 
					WHERE CustomerID = @CustomerID 
					AND DetailFieldID = @LoopDetailFieldID 
					AND (
						(@LoopOptionID = 1)
						OR
						(@LoopOptionID = 2 AND (DetailValue IS NULL OR DetailValue = ''))
						OR
						(@LoopOptionID = 3 AND (DetailValue IS NOT NULL AND DetailValue <> ''))
						OR
						(@LoopOptionID = 5)
					)

				END
				IF @LoopDetailFieldLeadOrMatter = 11
				BEGIN
					
					/* Update CaseDetailValues */
					UPDATE dbo.CaseDetailValues 
					SET DetailValue = @ValueForHistory 
					WHERE CaseID = @CaseID 
					AND DetailFieldID = @LoopDetailFieldID 
					AND (
						(@LoopOptionID = 1)
						OR
						(@LoopOptionID = 2 AND (DetailValue IS NULL OR DetailValue = ''))
						OR
						(@LoopOptionID = 3 AND (DetailValue IS NOT NULL AND DetailValue <> ''))
						OR
						(@LoopOptionID = 5)
					)

				END				
				
			END 

		END
		
		-- Option 2 = deleted events. 
		IF @AquariumOptionTypeID = 2 
		BEGIN


			IF @LoopOptionID <> 13 -- 13 = Never change on delete
			BEGIN

				-- By default, blank out the field
				SELECT @ValueForHistory = '', 
				@LastGoodLeadEventID = NULL

				-- 12 = Revert if possible 
				IF @LoopOptionID = 12
				BEGIN

					IF @DeletionComments like 'Duplicate deleted by Aquarium %' /*CS 2012-05-06 For trigger-deleted duplicates, look forward not backwards*/					
					BEGIN
						SELECT @LastGoodLeadEventID = Min(LeadEventID) 
						FROM LeadEvent WITH (NOLOCK) 
						WHERE CaseID = @CaseID 
						AND EventTypeID = @EventTypeID
						AND EventDeleted = 0 
						AND WhenCreated > @WhenCreated 
					END
					ELSE
					BEGIN
						SELECT @LastGoodLeadEventID = Max(LeadEventID) 
						FROM LeadEvent WITH (NOLOCK) 
						WHERE CaseID = @CaseID 
						AND EventTypeID = @EventTypeID
						AND EventDeleted = 0 
						AND WhenCreated < @WhenCreated 
					END
					
					-- If a suitable prior LeadEvent was found, fetch its date,
					-- otherwise the field is going to be blanked out anyway.
					IF @LastGoodLeadEventID IS NOT NULL
					BEGIN 
						SELECT @ValueForHistory = convert(char(10), WhenCreated, 126) 
						FROM LeadEvent WITH (NOLOCK) 
						WHERE LeadEventID = @LastGoodLeadEventID 
					END
				END

				IF @LoopDetailFieldLeadOrMatter = 1
				BEGIN

					-- Blank out LeadDetailValues
					UPDATE LeadDetailValues 
					SET DetailValue = @ValueForHistory 
					WHERE LeadID = @LeadID 
					AND DetailFieldID = @LoopDetailFieldID 

				END
				IF @LoopDetailFieldLeadOrMatter = 2
				BEGIN

					-- Update any MatterDetailValues for this Case
					-- that match the option rules.
					UPDATE MatterDetailValues 
					SET DetailValue = @ValueForHistory 
					FROM dbo.MatterDetailValues mdv 
					INNER JOIN dbo.Matter m ON m.MatterID = mdv.MatterID 
					WHERE m.CaseID = @CaseID 
					AND mdv.DetailFieldID = @LoopDetailFieldID 

				END
				IF @LoopDetailFieldLeadOrMatter = 10
				BEGIN

					/* Blank out CustomerDetailValues */
					UPDATE CustomerDetailValues 
					SET DetailValue = @ValueForHistory 
					WHERE CustomerID = @CustomerID 
					AND DetailFieldID = @LoopDetailFieldID 

				END
				IF @LoopDetailFieldLeadOrMatter = 11
				BEGIN

					/* Blank out CaseDetailValues */
					UPDATE CaseDetailValues 
					SET DetailValue = @ValueForHistory 
					WHERE CaseID = @CaseID 
					AND DetailFieldID = @LoopDetailFieldID 

				END				


			END

		END

		-- Maintain history if required for this field
		IF @LoopMaintainHistory = 1
		BEGIN
		
			-- LDV history
			IF @LoopDetailFieldLeadOrMatter = 1
			BEGIN
				INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, FieldValue, WhenSaved, ClientPersonnelID)
				VALUES (@ClientID, @LoopDetailFieldID, @LoopDetailFieldLeadOrMatter, @LeadID, @ValueForHistory, dbo.fn_GetDate_Local(), @ClientPersonnelID)
			END
			
			-- MDV history
			IF @LoopDetailFieldLeadOrMatter = 2
			BEGIN
				INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
				SELECT @ClientID, @LoopDetailFieldID, @LoopDetailFieldLeadOrMatter, @LeadID, m.MatterID, @ValueForHistory, dbo.fn_GetDate_Local(), @ClientPersonnelID
				FROM dbo.Matter m WITH (NOLOCK) 
				WHERE m.CaseID = @CaseID 
			END
			
			/* CDV history */
			IF @LoopDetailFieldLeadOrMatter = 10
			BEGIN
				INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, CustomerID, FieldValue, WhenSaved, ClientPersonnelID)
				VALUES (@ClientID, @LoopDetailFieldID, @LoopDetailFieldLeadOrMatter, @CustomerID, @ValueForHistory, dbo.fn_GetDate_Local(), @ClientPersonnelID)
			END
			
			/* CaseDV history */
			IF @LoopDetailFieldLeadOrMatter = 11
			BEGIN
				INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, CaseID, FieldValue, WhenSaved, ClientPersonnelID)
				VALUES (@ClientID, @LoopDetailFieldID, @LoopDetailFieldLeadOrMatter, @CaseID, @ValueForHistory, dbo.fn_GetDate_Local(), @ClientPersonnelID)
			END
			
		END
		
		-- Delete the row we have just used
		DELETE @ETFCList WHERE DetailFieldID = @LoopDetailFieldID

		-- Look for more work
		SET @ETFCRowcount = @ETFCRowcount - 1
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletionFromLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletionFromLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletionFromLeadEvent] TO [sp_executeall]
GO
