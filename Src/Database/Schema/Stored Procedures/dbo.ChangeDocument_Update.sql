SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChangeDocument table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_Update]
(

	@ChangeDocumentID int   ,

	@DocumentData varbinary (MAX)  ,

	@DocumentExtension varchar (10)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChangeDocument]
				SET
					[DocumentData] = @DocumentData
					,[DocumentExtension] = @DocumentExtension
					,[WhenCreated] = @WhenCreated
					,[WhoCreated] = @WhoCreated
				WHERE
[ChangeDocumentID] = @ChangeDocumentID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Update] TO [sp_executeall]
GO
