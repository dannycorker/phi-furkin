SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
 
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-06-12
-- Description:	Writes either a credit or debit row to the general ledger table
-- Modified:    DCM 2104-09-15 copied from 235
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Premium_WriteGeneralLedger] 
(
	--@PremiumRows dbo.tvpInt READONLY,
	@PremiumRows dbo.tvpIntMoney READONLY,
	@Debit BIT
)
 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Data TABLE
	(
		ID INT IDENTITY,
		MatterID INT,
		TableRowID INT,
		Amount VARCHAR(2000),
		FromDate VARCHAR(2000),
		ToDate VARCHAR(2000), 
		Province VARCHAR(2000)
	)
	
	DECLARE @ClientID INT
 
	INSERT @Data (MatterID, TableRowID, Amount, FromDate, ToDate, Province)
	SELECT	m.MatterID, r.TableRowID
		--, tdvAmount.DetailValue AS Amount
		, CASE WHEN p.Value = 0 THEN tdvAmount.DetailValue ELSE p.Value END AS Amount
		, tdvFrom.DetailValue AS FromDate, tdvTo.DetailValue AS ToDate, c.County AS Province
	FROM dbo.Customers c WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON l.LeadID = m.LeadID
	INNER JOIN dbo.TableRows r WITH (NOLOCK) ON m.MatterID = r.MatterID
	--INNER JOIN @PremiumRows p ON r.TableRowID = p.AnyID
	INNER JOIN @PremiumRows p ON r.TableRowID = p.ID
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 170070
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 170076
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 170077
	--Why were these joins here? I don't event think the DFID is right -- ROH 2014-01-20
	--INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 170078
	--INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON tdvType.ValueInt = ll.LookupListItemID
	INNER JOIN LeadTypeShare lts WITH (NOLOCK) ON l.LeadTypeID=lts.SharedTo
	WHERE lts.SharedFrom = 1273 AND lts.ClientID=@ClientID
	AND r.DetailFieldID = 170088
	AND tdvAmount.ValueMoney > 0
	
	SELECT TOP 1 @ClientID=m.ClientID 
	FROM Matter m WITH (NOLOCK)
	INNER JOIN @Data d ON m.MatterID=d.MatterID 
	
	DECLARE @TableRowIDs TABLE
	(
		ID INT IDENTITY,
		TableRowID INT
	)
 
	INSERT INTO dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID)
	OUTPUT inserted.TableRowID INTO @TableRowIDs
	SELECT 0, MatterID, 170089, 19011
	FROM @Data
 
	DECLARE @FieldIDs TABLE ( FieldID INT )
	INSERT @FieldIDs (FieldID) VALUES
	(170079), -- Amount
	(170080), -- From 
	(170081), -- To
	(170082), -- Type
	(170085)  -- Parent Table Row ID
 
 
	INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, MatterID, ClientID, DetailValue)
	SELECT	t.TableRowID, f.FieldID, d.MatterID, @ClientID, 
			CASE f.FieldID
				WHEN 170079 THEN d.Amount	-- Amount
				WHEN 170080 THEN d.FromDate	-- From 
				WHEN 170081 THEN d.ToDate	-- To
				WHEN 170085 THEN CAST(d.TableRowID AS VARCHAR)	-- Parent Table Row ID
				WHEN 170082 THEN CASE @Debit
									WHEN 1 THEN '69905' 	-- Type
									ELSE '69906'
								 END
			END
	FROM @Data d
	INNER JOIN @TableRowIDs t ON d.ID = t.ID
	CROSS JOIN @FieldIDs f
 
 
END
 
 
 


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Premium_WriteGeneralLedger] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Premium_WriteGeneralLedger] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Premium_WriteGeneralLedger] TO [sp_executeall]
GO
