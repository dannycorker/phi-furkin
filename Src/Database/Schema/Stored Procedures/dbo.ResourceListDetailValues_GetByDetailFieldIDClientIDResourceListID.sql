SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ResourceListDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_GetByDetailFieldIDClientIDResourceListID]
(

	@DetailFieldID int   ,

	@ClientID int   ,

	@ResourceListID int   
)
AS


				SELECT
					[ResourceListDetailValueID],
					[ResourceListID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime]
				FROM
					[dbo].[ResourceListDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
					AND [ResourceListID] = @ResourceListID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_GetByDetailFieldIDClientIDResourceListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_GetByDetailFieldIDClientIDResourceListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_GetByDetailFieldIDClientIDResourceListID] TO [sp_executeall]
GO
