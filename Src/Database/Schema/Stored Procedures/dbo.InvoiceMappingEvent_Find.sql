SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the InvoiceMappingEvent table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_Find]
(

	@SearchUsingOR bit   = null ,

	@InvoiceMappingEventID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@EventTypeID int   = null ,

	@Type char (1)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [InvoiceMappingEventID]
	, [ClientID]
	, [LeadTypeID]
	, [EventTypeID]
	, [Type]
    FROM
	[dbo].[InvoiceMappingEvent] WITH (NOLOCK) 
    WHERE 
	 ([InvoiceMappingEventID] = @InvoiceMappingEventID OR @InvoiceMappingEventID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([Type] = @Type OR @Type IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [InvoiceMappingEventID]
	, [ClientID]
	, [LeadTypeID]
	, [EventTypeID]
	, [Type]
    FROM
	[dbo].[InvoiceMappingEvent] WITH (NOLOCK) 
    WHERE 
	 ([InvoiceMappingEventID] = @InvoiceMappingEventID AND @InvoiceMappingEventID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([Type] = @Type AND @Type is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Find] TO [sp_executeall]
GO
