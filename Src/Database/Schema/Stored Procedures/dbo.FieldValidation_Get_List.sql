SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FieldValidation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_Get_List]

AS


				
				SELECT
					[FieldValidationID],
					[ClientID],
					[FieldID],
					[ValidationGroup],
					[ValidationType],
					[MaxValue],
					[MinValue],
					[ErrorMessage],
					[AccessLevelRestrictions]
				FROM
					[dbo].[FieldValidation] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Get_List] TO [sp_executeall]
GO
