SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the MasterQuestions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_Get_List]

AS


				
				SELECT
					[MasterQuestionID],
					[ClientQuestionnaireID],
					[QuestionTypeID],
					[QuestionText],
					[QuestionOrder],
					[DefaultAnswerID],
					[Mandatory],
					[QuestionToolTip],
					[LinkedQuestionnaireMasterQuestionID],
					[Active],
					[MasterQuestionStatus],
					[TextboxHeight],
					[ClientID],
					[AnswerPosition],
					[DisplayAnswerAs],
					[NumberOfAnswersPerRow],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[QuestionPossibleAnswerSortOrder]
				FROM
					[dbo].[MasterQuestions] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Get_List] TO [sp_executeall]
GO
