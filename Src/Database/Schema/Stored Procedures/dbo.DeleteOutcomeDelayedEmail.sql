SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteOutcomeDelayedEmail] 

@OutcomeDelayedEmailID int

AS

Delete From OutcomeDelayedEmails
Where OutcomeDelayedEmailID = @OutcomeDelayedEmailID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeDelayedEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteOutcomeDelayedEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomeDelayedEmail] TO [sp_executeall]
GO
