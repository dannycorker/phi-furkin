SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderObjectField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_Update]
(

	@DataLoaderObjectFieldID int   ,

	@OriginalDataLoaderObjectFieldID int   ,

	@DataLoaderObjectTypeID int   ,

	@FieldName varchar (250)  ,

	@DataTypeID int   ,

	@IsMandatory bit   ,

	@IsNormalisedField bit   ,

	@IsABitField bit   ,

	@IsClientOwned bit   ,

	@IsUniqueTableID bit   ,

	@IsWriteOnce bit   ,

	@HelperText varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderObjectField]
				SET
					[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
					,[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[FieldName] = @FieldName
					,[DataTypeID] = @DataTypeID
					,[IsMandatory] = @IsMandatory
					,[IsNormalisedField] = @IsNormalisedField
					,[IsABitField] = @IsABitField
					,[IsClientOwned] = @IsClientOwned
					,[IsUniqueTableID] = @IsUniqueTableID
					,[IsWriteOnce] = @IsWriteOnce
					,[HelperText] = @HelperText
				WHERE
[DataLoaderObjectFieldID] = @OriginalDataLoaderObjectFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Update] TO [sp_executeall]
GO
