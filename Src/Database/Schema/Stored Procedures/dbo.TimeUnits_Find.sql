SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the TimeUnits table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TimeUnits_Find]
(

	@SearchUsingOR bit   = null ,

	@TimeUnitsID int   = null ,

	@TimeUnitsName varchar (50)  = null ,

	@SQLTimeUnits varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TimeUnitsID]
	, [TimeUnitsName]
	, [SQLTimeUnits]
    FROM
	[dbo].[TimeUnits] WITH (NOLOCK) 
    WHERE 
	 ([TimeUnitsID] = @TimeUnitsID OR @TimeUnitsID IS NULL)
	AND ([TimeUnitsName] = @TimeUnitsName OR @TimeUnitsName IS NULL)
	AND ([SQLTimeUnits] = @SQLTimeUnits OR @SQLTimeUnits IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TimeUnitsID]
	, [TimeUnitsName]
	, [SQLTimeUnits]
    FROM
	[dbo].[TimeUnits] WITH (NOLOCK) 
    WHERE 
	 ([TimeUnitsID] = @TimeUnitsID AND @TimeUnitsID is not null)
	OR ([TimeUnitsName] = @TimeUnitsName AND @TimeUnitsName is not null)
	OR ([SQLTimeUnits] = @SQLTimeUnits AND @SQLTimeUnits is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimeUnits_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Find] TO [sp_executeall]
GO
