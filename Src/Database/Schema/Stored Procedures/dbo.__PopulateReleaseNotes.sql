SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-02-17
-- Description:	Populate Release Notes from temp table
-- =============================================
CREATE PROCEDURE [dbo].[__PopulateReleaseNotes]

AS
BEGIN
	SET NOCOUNT ON;

	declare @PatchID int, @PatchVersion int 

	Select distinct @PatchVersion = PatchVer
	From _RawPatch
	
	Insert Into Patch(PatchDate, PatchVersion)
	Select dbo.fn_GetDate_Local(), @PatchVersion

	Select @PatchID = SCOPE_IDENTITY()

	insert into PatchNote(PatchID, Location, PatchNoteText, AdminOnly)
	Select @PatchID, Location, LEFT(Note, 200), LTRIM(AdminOnly)
	From _RawPatch

	truncate table _RawPatch

END
GO
GRANT VIEW DEFINITION ON  [dbo].[__PopulateReleaseNotes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__PopulateReleaseNotes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__PopulateReleaseNotes] TO [sp_executeall]
GO
