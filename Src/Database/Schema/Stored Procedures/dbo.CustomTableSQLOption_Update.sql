SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CustomTableSQLOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomTableSQLOption_Update]
(

	@CustomTableSQLOptionID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@OptionType varchar (100)  ,

	@OptionValue varchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CustomTableSQLOption]
				SET
					[ClientID] = @ClientID
					,[DetailFieldID] = @DetailFieldID
					,[OptionType] = @OptionType
					,[OptionValue] = @OptionValue
				WHERE
[CustomTableSQLOptionID] = @CustomTableSQLOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQLOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_Update] TO [sp_executeall]
GO
