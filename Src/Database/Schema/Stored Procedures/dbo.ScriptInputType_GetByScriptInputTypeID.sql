SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptInputType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptInputType_GetByScriptInputTypeID]
(

	@ScriptInputTypeID int   
)
AS


				SELECT
					[ScriptInputTypeID],
					[InputType],
					[Template]
				FROM
					[dbo].[ScriptInputType] WITH (NOLOCK) 
				WHERE
										[ScriptInputTypeID] = @ScriptInputTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_GetByScriptInputTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptInputType_GetByScriptInputTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_GetByScriptInputTypeID] TO [sp_executeall]
GO
