SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Equations table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Equations_GetByEquationID]
(

	@EquationID int   
)
AS


				SELECT
					[EquationID],
					[EquationName],
					[Equation],
					[ClientQuestionnaireID],
					[ClientID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Equations] WITH (NOLOCK) 
				WHERE
										[EquationID] = @EquationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_GetByEquationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations_GetByEquationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_GetByEquationID] TO [sp_executeall]
GO
