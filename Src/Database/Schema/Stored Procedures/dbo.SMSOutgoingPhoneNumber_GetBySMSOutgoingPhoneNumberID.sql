SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSOutgoingPhoneNumber table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_GetBySMSOutgoingPhoneNumberID]
(

	@SMSOutgoingPhoneNumberID int   
)
AS


				SELECT
					[SMSOutgoingPhoneNumberID],
					[ClientID],
					[SMSQuestionnaireID],
					[LeadTypeID],
					[OutgoingPhoneNumber],
					[SMSSurveyStartEventTypeID],
					[MMSLetterInEvent],
					[SmsGatewayID],
					[HasIntroductionText],
					[IntroductionTextFrom],
					[IntroductionText],
					[DelayInSeconds],
					[IntroductionTextEventTypeID],
					[IntroductionTextDocumentTypeID],
					[NoCLIRejectText]
				FROM
					[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
				WHERE
										[SMSOutgoingPhoneNumberID] = @SMSOutgoingPhoneNumberID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_GetBySMSOutgoingPhoneNumberID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_GetBySMSOutgoingPhoneNumberID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_GetBySMSOutgoingPhoneNumberID] TO [sp_executeall]
GO
