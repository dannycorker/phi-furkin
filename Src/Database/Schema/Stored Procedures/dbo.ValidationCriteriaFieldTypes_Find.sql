SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ValidationCriteriaFieldTypes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriteriaFieldTypes_Find]
(

	@SearchUsingOR bit   = null ,

	@ValidationCriteriaFieldTypeID int   = null ,

	@Allowable nvarchar (25)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ValidationCriteriaFieldTypeID]
	, [Allowable]
    FROM
	[dbo].[ValidationCriteriaFieldTypes] WITH (NOLOCK) 
    WHERE 
	 ([ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID OR @ValidationCriteriaFieldTypeID IS NULL)
	AND ([Allowable] = @Allowable OR @Allowable IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ValidationCriteriaFieldTypeID]
	, [Allowable]
    FROM
	[dbo].[ValidationCriteriaFieldTypes] WITH (NOLOCK) 
    WHERE 
	 ([ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID AND @ValidationCriteriaFieldTypeID is not null)
	OR ([Allowable] = @Allowable AND @Allowable is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriteriaFieldTypes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_Find] TO [sp_executeall]
GO
