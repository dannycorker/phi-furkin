SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-10-19
-- Description:	Client 38 (Aquarium Billing SAS)
-- =============================================
CREATE PROCEDURE [dbo].[_C38_SAS]
@ClientPersonnelID int,
@CustomerID int = NULL,
@LeadID int = NULL,
@CaseID int = NULL, 
@MatterID int = NULL,
@TableRowID int = NULL,
@ResourceListID int = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @DetailFieldID int, 
			@LeadTypeID int,
			@UsersName varchar(201),
			@AquariumClientID int,
			@ClientRelationshipID int

	/* Get username for later on */
	SELECT @UsersName = UserName
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID

	/* Get LeadTypeID for use Later On */
	SELECT @LeadTypeID = l.LeadTypeID
	FROM Lead l WITH (NOLOCK)
	WHERE l.LeadID = @LeadID
	
	Select @AquariumClientID = ldv.DetailValue 
	From LeadDetailValues ldv 
	where ldv.DetailFieldID = 6453
	and ldv.LeadID = @LeadID

	/* Get CaseID as it is not passed in */
	IF @MatterID IS NOT NULL
	BEGIN
	
		SELECT @CaseID = m.CaseID 
		FROM dbo.Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @MatterID 
	
	END

	/*New TableRow Sub Proc*/
	IF @TableRowID > 0
	BEGIN
	
		EXEC dbo._C38_SAS_TableRow @ClientPersonnelID, @CustomerID, @LeadID, @CaseID, @MatterID, @TableRowID 
	
	END

	/* Lead Table */
	If @MatterID IS NULL and @TableRowID IS NOT NULL
	BEGIN

		/* Get the detailfield of the table */	
		SELECT @DetailFieldID = DetailFieldID 
		FROM TableRows WITH (NOLOCK)
		WHERE TableRowID = @TableRowID

		If @DetailFieldID = 36710
		BEGIN

			/* If the Clientrelationship is empty then add the row into the "real" table */
			IF EXISTS (Select * from TableDetailValues tdv WITH (NOLOCK) Where DetailFieldID = 36709 and DetailValue IS NULL And TableRowID = @TableRowID)
			BEGIN
			
				INSERT INTO ClientRelationship(ClientRelationshipName,OutgoingClientID,ReceivingClientID,OutgoingLeadTypeID,IncomingLeadTypeID,OutgoingEventTypeID,IncomingEventTypeID,Enabled,ClearLeadRefs)
				Select tdv_RelationshipName.Detailvalue,
				@AquariumClientID,
				tdv_ReceivingClientID.DetailValue,
				tdv_OutgoingLeadTypeID.DetailValue,
				tdv_IncomingLeadTypeID.DetailValue,
				tdv_OutgoingEventTypeID.DetailValue,
				tdv_IncomingEventTypeID.DetailValue,
				Case tdv_Enabled.DetailValue WHEN 'true' THEN 1 ELSE 0 END,
				Case tdv_ClearLeadRefs.DetailValue  WHEN 'true' THEN 1 ELSE 0 END
				From tabledetailvalues tdv_RelationshipName
				inner join TableDetailValues tdv_ReceivingClientID ON tdv_ReceivingClientID.TableRowID = @TableRowID and tdv_ReceivingClientID.DetailFieldID = 36702
				inner join TableDetailValues tdv_OutgoingLeadTypeID ON tdv_OutgoingLeadTypeID.TableRowID = @TableRowID and tdv_OutgoingLeadTypeID.DetailFieldID = 36703
				inner join TableDetailValues tdv_IncomingLeadTypeID ON tdv_IncomingLeadTypeID.TableRowID = @TableRowID and tdv_IncomingLeadTypeID.DetailFieldID = 36704
				inner join TableDetailValues tdv_OutgoingEventTypeID ON tdv_OutgoingEventTypeID.TableRowID = @TableRowID and tdv_OutgoingEventTypeID.DetailFieldID = 36705
				inner join TableDetailValues tdv_IncomingEventTypeID ON tdv_IncomingEventTypeID.TableRowID = @TableRowID and tdv_IncomingEventTypeID.DetailFieldID = 36706
				inner join TableDetailValues tdv_Enabled ON tdv_Enabled.TableRowID = @TableRowID and tdv_Enabled.DetailFieldID = 36707
				inner join TableDetailValues tdv_ClearLeadRefs ON tdv_ClearLeadRefs.TableRowID = @TableRowID and tdv_ClearLeadRefs.DetailFieldID = 36708
				where tdv_RelationshipName.TableRowID = @TableRowID
				and tdv_RelationshipName.DetailFieldID = 36701

				Select @ClientRelationshipID = SCOPE_IDENTITY() 

				Update TableDetailValues
				Set DetailValue = @ClientRelationshipID 
				Where TableRowID = @TableRowID
				And DetailFieldID = 36709
			
			END
			ELSE 
			BEGIN
			
				Update ClientRelationship
				Set ClientRelationshipName = tdv_RelationshipName.Detailvalue,
				OutgoingClientID = @AquariumClientID,
				ReceivingClientID = tdv_ReceivingClientID.DetailValue,
				OutgoingLeadTypeID = tdv_OutgoingLeadTypeID.DetailValue,
				IncomingLeadTypeID = tdv_IncomingLeadTypeID.DetailValue,
				OutgoingEventTypeID = tdv_OutgoingEventTypeID.DetailValue,
				IncomingEventTypeID = tdv_IncomingEventTypeID.DetailValue,
				Enabled = Case tdv_Enabled.DetailValue WHEN 'true' THEN 1 ELSE 0 END,
				ClearLeadRefs = Case tdv_ClearLeadRefs.DetailValue  WHEN 'true' THEN 1 ELSE 0 END
				From ClientRelationship
				Inner Join tabledetailvalues tdv_RelationshipID on tdv_RelationshipID.DetailFieldID = 36709 and tdv_RelationshipID.DetailValue = ClientRelationship.ClientRelationshipID
				inner join TableDetailValues tdv_RelationshipName ON tdv_RelationshipName.TableRowID = tdv_RelationshipID.TableRowID and tdv_RelationshipName.DetailFieldID = 36701
				inner join TableDetailValues tdv_ReceivingClientID ON tdv_ReceivingClientID.TableRowID = tdv_RelationshipID.TableRowID and tdv_ReceivingClientID.DetailFieldID = 36702
				inner join TableDetailValues tdv_OutgoingLeadTypeID ON tdv_OutgoingLeadTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_OutgoingLeadTypeID.DetailFieldID = 36703
				inner join TableDetailValues tdv_IncomingLeadTypeID ON tdv_IncomingLeadTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_IncomingLeadTypeID.DetailFieldID = 36704
				inner join TableDetailValues tdv_OutgoingEventTypeID ON tdv_OutgoingEventTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_OutgoingEventTypeID.DetailFieldID = 36705
				inner join TableDetailValues tdv_IncomingEventTypeID ON tdv_IncomingEventTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_IncomingEventTypeID.DetailFieldID = 36706
				inner join TableDetailValues tdv_Enabled ON tdv_Enabled.TableRowID = tdv_RelationshipID.TableRowID and tdv_Enabled.DetailFieldID = 36707
				inner join TableDetailValues tdv_ClearLeadRefs ON tdv_ClearLeadRefs.TableRowID = tdv_RelationshipID.TableRowID and tdv_ClearLeadRefs.DetailFieldID = 36708
				Where tdv_RelationshipName.TableRowID = @TableRowID

			END

		END

	END
	
	/*Customer Save (Company Name/Phone Number Change) -- Cant use @LeadID as its passed in*/
	IF @CustomerID IS NOT NULL AND @LeadID IS NULL
	BEGIN

		UPDATE Clients
		SET CompanyName = cu.CompanyName
		FROM Customers cu
		INNER JOIN Lead l on l.CustomerID = cu.CustomerID 
		INNER JOIN Clients ON l.LeadRef = Clients.ClientID
		WHERE cu.CustomerID = @CustomerID
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_SAS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_SAS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_SAS] TO [sp_executeall]
GO
