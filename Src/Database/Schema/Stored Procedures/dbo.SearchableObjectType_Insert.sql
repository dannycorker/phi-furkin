SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SearchableObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchableObjectType_Insert]
(

	@SearchableObjectTypeID int   ,

	@SearchableObject varchar (250)  ,

	@PKColumnName varchar (250)  ,

	@JoinTable varchar (250)  ,

	@JoinColumn varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[SearchableObjectType]
					(
					[SearchableObjectTypeID]
					,[SearchableObject]
					,[PKColumnName]
					,[JoinTable]
					,[JoinColumn]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@SearchableObjectTypeID
					,@SearchableObject
					,@PKColumnName
					,@JoinTable
					,@JoinColumn
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchableObjectType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Insert] TO [sp_executeall]
GO
