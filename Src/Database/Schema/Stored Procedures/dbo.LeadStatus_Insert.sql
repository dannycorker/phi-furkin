SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_Insert]
(

	@StatusID int    OUTPUT,

	@ClientID int   ,

	@StatusName varchar (50)  ,

	@StatusDescription varchar (250)  ,

	@SourceID int   ,

	@LeadTypeID int   ,

	@IsShared bit   
)
AS


				
				INSERT INTO [dbo].[LeadStatus]
					(
					[ClientID]
					,[StatusName]
					,[StatusDescription]
					,[SourceID]
					,[LeadTypeID]
					,[IsShared]
					)
				VALUES
					(
					@ClientID
					,@StatusName
					,@StatusDescription
					,@SourceID
					,@LeadTypeID
					,@IsShared
					)
				-- Get the identity value
				SET @StatusID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Insert] TO [sp_executeall]
GO
