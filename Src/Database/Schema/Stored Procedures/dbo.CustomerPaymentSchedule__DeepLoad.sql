SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-05-20
-- Description:	Deep loads the customer payment schedule
--				Table one contains the customer payment schedule for the given ledger entry
--				Table two contains the related purchased product payment schedule
--
--				There can be only one customer payment record per customer ledger entry since it is 
--				the consolidated purchase product payment schedule for a single date
-- =============================================
CREATE PROCEDURE [dbo].[CustomerPaymentSchedule__DeepLoad]
	@CustomerLedgerID INT,
	@CustomerID INT,
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CustomerPaymentScheduleID INT
	
	SELECT Top 1 @CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID 
	FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	WHERE cps.CustomerLedgerID=@CustomerLedgerID AND cps.CustomerID=@CustomerID AND cps.ClientID=@ClientID
	ORDER BY cps.PaymentDate DESC

	EXEC CustomerPaymentSchedule__GetByID @CustomerPaymentScheduleID, @ClientID
	
	EXEC PurchasedProductPaymentSchedule__GetByCustomerPaymentSchedule @CustomerPaymentScheduleID, @CustomerID, @ClientID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__DeepLoad] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerPaymentSchedule__DeepLoad] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerPaymentSchedule__DeepLoad] TO [sp_executeall]
GO
