SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 2009-20-04
-- Description:	Gets a list of ResourceListIDs for the scheduler to invoke multiple recipient events
-- =============================================

CREATE PROCEDURE [dbo].[GetResourceListIdsForMultiRecipientList]

	@LeadID int,
	@DetailFieldID int,
	@ClientID int
AS
BEGIN

	SELECT     

		DISTINCT(rdv.ResourceListID)
		
		FROM         

		MatterDetailValues mdv WITH (NOLOCK)

		INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) ON rdv.ResourceListID = mdv.DetailValue 
		INNER JOIN DetailFields df WITH (NOLOCK) ON rdv.DetailFieldID = df.DetailFieldID

		WHERE		mdv.DetailFieldID = @DetailFieldID
					AND mdv.LeadID = @LeadID
					AND df.Enabled = 1
					AND mdv.ClientID = @ClientID					

		ORDER BY rdv.ResourceListID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListIdsForMultiRecipientList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetResourceListIdsForMultiRecipientList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListIdsForMultiRecipientList] TO [sp_executeall]
GO
