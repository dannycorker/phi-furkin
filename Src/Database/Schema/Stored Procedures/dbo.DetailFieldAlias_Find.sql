SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailFieldAlias table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailFieldAliasID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@DetailFieldID int   = null ,

	@DetailFieldAlias varchar (500)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailFieldAliasID]
	, [ClientID]
	, [LeadTypeID]
	, [DetailFieldID]
	, [DetailFieldAlias]
    FROM
	[dbo].[DetailFieldAlias] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldAliasID] = @DetailFieldAliasID OR @DetailFieldAliasID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([DetailFieldAlias] = @DetailFieldAlias OR @DetailFieldAlias IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailFieldAliasID]
	, [ClientID]
	, [LeadTypeID]
	, [DetailFieldID]
	, [DetailFieldAlias]
    FROM
	[dbo].[DetailFieldAlias] WITH (NOLOCK) 
    WHERE 
	 ([DetailFieldAliasID] = @DetailFieldAliasID AND @DetailFieldAliasID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([DetailFieldAlias] = @DetailFieldAlias AND @DetailFieldAlias is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Find] TO [sp_executeall]
GO
