SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the TableDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableDetailValues_Insert]
(

	@TableDetailValueID int    OUTPUT,

	@TableRowID int   ,

	@ResourceListID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@LeadID int   ,

	@MatterID int   ,

	@ClientID int   ,

	@EncryptedValue varchar (3000)  ,

	@ErrorMsg varchar (1000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@CustomerID int   ,

	@CaseID int   ,

	@ClientPersonnelID int   ,

	@ContactID int   
)
AS


				
				INSERT INTO [dbo].[TableDetailValues]
					(
					[TableRowID]
					,[ResourceListID]
					,[DetailFieldID]
					,[DetailValue]
					,[LeadID]
					,[MatterID]
					,[ClientID]
					,[EncryptedValue]
					,[ErrorMsg]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[CustomerID]
					,[CaseID]
					,[ClientPersonnelID]
					,[ContactID]
					)
				VALUES
					(
					@TableRowID
					,@ResourceListID
					,@DetailFieldID
					,@DetailValue
					,@LeadID
					,@MatterID
					,@ClientID
					,@EncryptedValue
					,@ErrorMsg
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@CustomerID
					,@CaseID
					,@ClientPersonnelID
					,@ContactID
					)
				-- Get the identity value
				SET @TableDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Insert] TO [sp_executeall]
GO
