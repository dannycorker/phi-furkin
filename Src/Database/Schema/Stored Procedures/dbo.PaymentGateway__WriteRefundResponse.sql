SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-21
-- Description:	Writes the refund response to the refund table
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteRefundResponse]
	@ClientID INT,
	@CustomerID INT,
	@TableRowID INT,
	@RefundResponseStatus VARCHAR(2000),
	@RefundResponseStatusDetail VARCHAR(2000),
	@RefundResponseVPSTxId VARCHAR(2000),
	@RefundResponseTxAuthNo VARCHAR(2000)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TransactionDate DATETIME
	SET @TransactionDate = CURRENT_TIMESTAMP

	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 565 THEN @RefundResponseStatus
																	WHEN 566 THEN @RefundResponseStatusDetail
																	WHEN 567 THEN @RefundResponseVPSTxId
																	WHEN 568 THEN @RefundResponseTxAuthNo
																	WHEN 628 THEN CONVERT(VARCHAR, @TransactionDate,120)
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (565,566,567,568,628)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRefundResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteRefundResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRefundResponse] TO [sp_executeall]
GO
