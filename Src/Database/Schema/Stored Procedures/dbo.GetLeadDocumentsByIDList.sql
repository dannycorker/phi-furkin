SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Chris Townsend
-- Create date: 14th August 2007
-- Description:	Gets LeadDocuments based on a list of IDs
-- JWG 2012-07-12 Use vLeadDocumentList to cater for archived documents.  Also copy all columns to LeadDocument now.
-- =============================================
CREATE PROCEDURE [dbo].[GetLeadDocumentsByIDList] 

@ClientID int,
@leadDocumentIDList varchar(200)

AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		ldfs.[LeadDocumentID],
		ldfs.[ClientID],
		ldfs.[LeadID],
		ldfs.[DocumentTypeID],
		ldfs.[LeadDocumentTitle],
		ldfs.[UploadDateTime], 
		ldfs.[WhoUploaded],
		ldfs.[DocumentBLOB],
		ldfs.[FileName],
		ldfs.[EmailBLOB],
		ldfs.[DocumentFormat],
		ldfs.[EmailFrom],
		ldfs.[EmailTo],
		ldfs.[CcList],
		ldfs.[BccList],
		ldfs.[ZipFormat],
		dt.InputFormat,  
		dt.OutputFormat, 
		outer_apply.CaseID		

	FROM dbo.vLeadDocumentList ldfs WITH (NOLOCK) 
	
	/* 
	Create a subset of records from the LeadEvent table to avoid 
	getting duplicate records where a LeadDocument exists for more than one LeadEvent 
	*/
	OUTER APPLY
	(
		SELECT TOP 1 le.LeadEventID, le.CaseID
		FROM LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadDocumentID = ldfs.LeadDocumentID
	) AS outer_apply
	LEFT JOIN dbo.DocumentType dt WITH (NOLOCK) on ldfs.DocumentTypeID = dt.DocumentTypeID 
	INNER JOIN dbo.fnTableOfIDsFromCSVDistinct(@leadDocumentIDList) f ON f.AnyID = ldfs.LeadDocumentID
	WHERE	
		ldfs.ClientID = @ClientID 

END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadDocumentsByIDList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadDocumentsByIDList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadDocumentsByIDList] TO [sp_executeall]
GO
