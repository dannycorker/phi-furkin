SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Paul Richardson>
-- Create date: <21-07-2010>
-- Description:	<Gets the Detail Field Alias sorted by the lenght of the alias name descending>
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldAlias__GetByClientIDSortedByAliasLength]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[DetailFieldAliasID],
					[ClientID],
					[LeadTypeID],
					[DetailFieldID],
					[DetailFieldAlias]
				FROM
					[dbo].[DetailFieldAlias]
				WHERE
					[ClientID] = @ClientID
				ORDER BY DATALENGTH([DetailFieldAlias]) DESC
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON





GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias__GetByClientIDSortedByAliasLength] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias__GetByClientIDSortedByAliasLength] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias__GetByClientIDSortedByAliasLength] TO [sp_executeall]
GO
