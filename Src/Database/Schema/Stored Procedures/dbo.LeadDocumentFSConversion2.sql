SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-27
-- Description:	LeadDocumentFS Filestream conversion of existing LeadDocument records
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocumentFSConversion2]
	@NumberToConvert int = null,			/* This many in total  */
	@LoopMax int = 10,						/* This many at a time */
	@FinishTime datetime = null,			/* Stop by this time   */
	@SmallestSize bigint = 1000,			/* 1 KB */
	@LargestSize bigint = 1000000000,		/* 1 GB */
	@OptionalPause bit = 0,					/* Pause after each block of records has been converted */
	@PauseLength varchar(8) = '00:00:10'			/* How long to pause for before starting on the next block */
AS
BEGIN
	SET NOCOUNT ON;

	SELECT convert(varchar, dateadd(ms, 1, dbo.fn_GetDate_Local()), 120), ' LeadDocumentFSConversion2 Start'
		
	DECLARE @LoopCount int = 0, 
	@NumberConverted int = 0
	
	DECLARE @DocsToDo TABLE (LeadDocumentID int)
	
	BEGIN
		/* Find the biggest docs remaining */
		INSERT INTO @DocsToDo (LeadDocumentID) 
		SELECT TOP (@LoopMax) ld.LeadDocumentID 
		FROM dbo._LeadDocumentsBySize ld WITH (NOLOCK) 
		WHERE ld.DocSize BETWEEN @SmallestSize AND @LargestSize 
		ORDER BY ld.DocSize DESC
	END
	
	WHILE EXISTS (SELECT * FROM @DocsToDo)
	BEGIN
		
		/* Check for any that the app has done for us */
		BEGIN
			/* Set size to zero */
			UPDATE dbo._LeadDocumentsBySize 
			SET DocSize = 0, EmailSize = 0 
			FROM @DocsToDo d
			INNER JOIN dbo.LeadDocumentFS fs WITH (NOLOCK) ON fs.LeadDocumentID = d.LeadDocumentID
			INNER JOIN dbo._LeadDocumentsBySize ldbs WITH (NOLOCK) ON fs.LeadDocumentID = ldbs.LeadDocumentID
			
			/* Remove from this list */
			DELETE @DocsToDo
			FROM @DocsToDo d
			INNER JOIN dbo.LeadDocumentFS fs WITH (NOLOCK) ON fs.LeadDocumentID = d.LeadDocumentID
		END

		SET IDENTITY_INSERT [dbo].[LeadDocumentFS] ON
		
		BEGIN TRY
			INSERT INTO [dbo].[LeadDocumentFS]
				([LeadDocumentID]
				,[ClientID]
				,[LeadID]
				,[DocumentTypeID]
				,[LeadDocumentTitle]
				,[UploadDateTime]
				,[WhoUploaded]
				,[DocumentBLOB]
				,[FileName]
				,[EmailBLOB]
				,[DocumentFormat]
				,[EmailFrom]
				,[EmailTo]
				,[CcList]
				,[BccList]
				,[ElectronicSignatureDocumentKey]
				,[Encoding]
				,[ContentFormat]
				,[ZipFormat]
				)
			SELECT
				ld.LeadDocumentID
				,ClientID
				,LeadID
				,DocumentTypeID
				,LeadDocumentTitle
				,UploadDateTime
				,WhoUploaded
				,DocumentBLOB
				,FileName
				,EmailBLOB
				,DocumentFormat
				,EmailFrom
				,EmailTo
				,CcList
				,BccList
				,ElectronicSignatureDocumentKey
				,Encoding
				,ContentFormat
				,ZipFormat
			FROM @DocsToDo dtd 
			INNER JOIN dbo.LeadDocument ld ON ld.LeadDocumentID = dtd.LeadDocumentID 

			SET IDENTITY_INSERT [dbo].[LeadDocumentFS] OFF
			
			/* NULL out the original BLOB data */
			UPDATE
				[dbo].[LeadDocument]
			SET
				 [DocumentBLOB] = CAST('' AS varbinary)
				,[EmailBLOB] = CAST('' AS varbinary)
				,[EmailFrom] = NULL
				,[EmailTo] = NULL
				,[CcList] = NULL
				,[BccList] = NULL
				,[ElectronicSignatureDocumentKey] = NULL
				,[Encoding] = NULL
				,[ContentFormat] = NULL
				,[ZipFormat] = NULL
			FROM @DocsToDo dtd 
			INNER JOIN dbo.LeadDocument ld ON ld.LeadDocumentID = dtd.LeadDocumentID 
			
			/* Show the new size of zero in our TODO list */
			UPDATE dbo._LeadDocumentsBySize 
			SET DocSize = 0, 
			EmailSize = 0 
			FROM @DocsToDo dtd 
			INNER JOIN dbo._LeadDocumentsBySize ldbs ON ldbs.LeadDocumentID = dtd.LeadDocumentID 

			SELECT @NumberConverted += (SELECT COUNT(*) FROM @DocsToDo)
			
			--SELECT * FROM @DocsToDo
			
			DELETE @DocsToDo
		END TRY
		BEGIN CATCH
			/* Store failures for later remedial action */
			UPDATE dbo._LeadDocumentsBySize 
			SET DocSize = -1, 
			EmailSize = -1 
			FROM @DocsToDo dtd 
			INNER JOIN dbo._LeadDocumentsBySize ldbs ON ldbs.LeadDocumentID = dtd.LeadDocumentID 

			DELETE @DocsToDo
		END CATCH
		
		/* Check to see if we have done enough work for now */
		IF (@FinishTime IS NOT NULL AND DATEADD(ms, 1, dbo.fn_GetDate_Local()) > @FinishTime) OR (@NumberToConvert > 0 AND @NumberConverted >= @NumberToConvert)
		BEGIN
			PRINT 'All done for today!'
		END
		ELSE
		BEGIN
		
			/* Optional pause to let the rest of the world have a go! */
			IF @OptionalPause = 1
			BEGIN
				WAITFOR DELAY @PauseLength
			END
			
			/* Find the biggest docs remaining */
			INSERT INTO @DocsToDo (LeadDocumentID) 
			SELECT TOP (@LoopMax) ld.LeadDocumentID 
			FROM dbo._LeadDocumentsBySize ld WITH (NOLOCK) 
			WHERE ld.DocSize BETWEEN @SmallestSize AND @LargestSize 
			ORDER BY ld.DocSize DESC
		END


	END
	
	SELECT convert(varchar, dateadd(ms, 1, dbo.fn_GetDate_Local()), 120), ' Finished'

	SELECT @NumberConverted AS [Number Converted]

END




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentFSConversion2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentFSConversion2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentFSConversion2] TO [sp_executeall]
GO
