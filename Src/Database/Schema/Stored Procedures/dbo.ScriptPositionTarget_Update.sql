SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptPositionTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_Update]
(

	@ScriptPositionTargetID int   ,

	@ScriptID int   ,

	@SectionID int   ,

	@ClientID int   ,

	@PositionName varchar (250)  ,

	@LinkText varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptPositionTarget]
				SET
					[ScriptID] = @ScriptID
					,[SectionID] = @SectionID
					,[ClientID] = @ClientID
					,[PositionName] = @PositionName
					,[LinkText] = @LinkText
				WHERE
[ScriptPositionTargetID] = @ScriptPositionTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Update] TO [sp_executeall]
GO
