SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeAttachment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_Update]
(

	@AttachmentID int   ,

	@EventTypeID int   ,

	@AttachmentEventTypeID int   ,

	@All bit   ,

	@AttachmentEventGroupID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeAttachment]
				SET
					[EventTypeID] = @EventTypeID
					,[AttachmentEventTypeID] = @AttachmentEventTypeID
					,[All] = @All
					,[AttachmentEventGroupID] = @AttachmentEventGroupID
				WHERE
[AttachmentID] = @AttachmentID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Update] TO [sp_executeall]
GO
