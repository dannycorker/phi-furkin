SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-07-20
-- Description:	Set up access to a newly created lead or all leads
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ISI_ConfigureUserLeadAccess]
	 @LeadEventID		INT 
	,@CurrentLeadOnly	BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	/*
	There is a mismatch between CustomerSearchMultiValue and fnUserLeadAccess
	
	With LeadsBelongToOffices and UserCanSeeLeadsAssignedToOthers switched off,
	we get a scenario where a lead in my office assigned to somebody else will
	not appear in the search, but I do have access.
	If I turn UserCanSeeLeadsAssignedToOthers on, it appears in the search, but
	gives me access to EVERYBODY's leads
	
	To counter that I'm picking the scenario that makes the search work, and then
	controlling everything by ClientPersonnelAccess
	
	We do all leads at the point of process start, because of the risk of people
	switching users between offices.  I can't fire anything after creation of a
	user, and can't rely on people to click Save under ClientPersonnelDetailValues
	*/

	DECLARE	 @LogID			INT
			,@Xml			XML
			,@ClientID		INT
			,@WhoCreated	INT
			,@LogEntry		VARCHAR(2000)
			,@LeadID		INT

	SELECT @ClientID = le.ClientID, @WhoCreated = le.WhoCreated, @LeadID = le.LeadID
	FROM LeadEvent le WITH ( NOLOCK ) 
	WHERE le.LeadEventID = @LeadEventID


	/*Make sure all leads have access rules in place.  Default to NO access*/
	INSERT ClientPersonnelAccess ( ClientID, ClientPersonnelID, LeadID, SourceLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, AccessLevel )
	SELECT l.ClientID, cp.ClientPersonnelID, l.LeadID, @LeadEventID, @WhoCreated, dbo.fn_GetDate_Local(), @WhoCreated, dbo.fn_GetDate_Local(), 0
	FROM Lead l WITH ( NOLOCK ) 
	CROSS JOIN ClientPersonnel cp WITH ( NOLOCK )
	WHERE l.ClientID = @ClientID
	AND cp.ClientID = @ClientID
	AND cp.IsAquarium = 0
	and not exists ( SELECT * 
	                 FROM ClientPersonnelAccess pa WITH ( NOLOCK ) 
	                 WHERE pa.LeadID = l.LeadID 
	                 AND pa.ClientPersonnelID = cp.ClientPersonnelID )


	/*Pick up the list of Leads to create access records for*/
	DECLARE @ToAdd TABLE ( ClientPersonnelAccessID INT,LeadID INT, ClientOfficeID INT, ClientPersonnelID INT, TriggerLeadEventID INT, AccessLevel INT )
	INSERT @ToAdd (ClientPersonnelAccessID, LeadID, ClientOfficeID, ClientPersonnelID,TriggerLeadEventID,AccessLevel)
	SELECT pa.ClientPersonnelAccessID, l.LeadID, cp.ClientOfficeID, cp.ClientPersonnelID, @LeadEventID,CASE WHEN cp.ClientOfficeID = ol.ClientOfficeID THEN 4 ELSE 0 END 
	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	INNER JOIN ClientOfficeLead ol WITH ( NOLOCK ) on ol.LeadID = l.LeadID
	INNER JOIN ClientPersonnelAccess pa WITH ( NOLOCK ) on pa.LeadID = l.LeadID 
	INNER JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = pa.ClientPersonnelID AND cp.IsAquarium = 0
	WHERE (@CurrentLeadOnly = 0 or l.LeadID = @LeadID)
	AND (
			(pa.AccessLevel = 4 AND cp.ClientOfficeID <> ol.ClientOfficeID)
			OR
			(pa.AccessLevel < 4 AND cp.ClientOfficeID = ol.ClientOfficeID)
		)

	/*Use XML to log the details*/
	SELECT @Xml = 
	(
		SELECT *
		FROM @ToAdd ClientPersonnelAccess
		FOR XML Auto, Root ('Log')

	)
	INSERT LogXML ( ClientID, ContextID, ContextVarchar, LogDateTime, LogXMLEntry )
	VALUES ( @ClientID, @LeadEventID, '_C00_ISI_ConfigureUserLeadAccess', dbo.fn_GetDate_Local(), @Xml )

	SELECT @LogID = SCOPE_IDENTITY()


	/*Update ClientPersonnelAccess records*/
	UPDATE pa
	SET AccessLevel = ta.AccessLevel
	FROM ClientPersonnelAccess pa 
	INNER JOIN @ToAdd ta on ta.ClientPersonnelAccessID = pa.ClientPersonnelAccessID
	WHERE pa.ClientID = @ClientID
	
		
	SELECT @LogEntry = CONVERT(VARCHAR,@@ROWCOUNT) + ' entries added. LogXmlId ' + CONVERT(VARCHAR,@LogID)
	PRINT @LogEntry

	/*Feed back to the LeadEvent*/
	EXEC dbo._C00_SetLeadEventComments @LeadEventID, @LogEntry, 1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_ConfigureUserLeadAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ISI_ConfigureUserLeadAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ISI_ConfigureUserLeadAccess] TO [sp_executeall]
GO
