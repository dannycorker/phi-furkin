SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditing table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SELECT
					[SqlQueryEditingID],
					[ClientID],
					[SqlQueryID],
					[WhoIsEditing],
					[EditStartedAt],
					[QueryText],
					[QueryTitle],
					[LeadTypeID],
					[FolderID],
					[ParentQueryID],
					[LockAllTables]
				FROM
					[dbo].[SqlQueryEditing] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingID] = @SqlQueryEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_GetBySqlQueryEditingID] TO [sp_executeall]
GO
