SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the ClientOption table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOption_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ClientOptionID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([ClientOptionID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ClientOptionID]'
				SET @SQL = @SQL + ' FROM [dbo].[ClientOption] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[ClientOptionID], O.[ClientID], O.[ZenDeskOff], O.[ZenDeskURLPrefix], O.[ZenDeskText], O.[ZenDeskTitle], O.[iDiaryOff], O.[WorkflowOff], O.[UserDirectoryOff], O.[eCatcherOff], O.[LeadAssignmentOff], O.[UserMessagesOff], O.[SystemMessagesOff], O.[UserPortalOff], O.[ReportSearchOff], O.[NormalSearchOff], O.[UseXero], O.[UseUltra], O.[UseCaseSummary], O.[UseMoreProminentReminders], O.[UseSMSSurvey], O.[UseNewAddLead], O.[DiallerInsertMethod], O.[DiallerPrimaryKey], O.[UseCHARMS], O.[TextMessageService], O.[UseThunderhead], O.[UseMemorableWordVerification], O.[ApplyInactivityTimeout], O.[InactivityTimeoutInSeconds], O.[InactivityRedirectUrl], O.[UseSentimentAnalysis], O.[ShowProcessInfoButton], O.[LatestRecordsFirst], O.[UseSecureMessage], O.[EventCommentTooltipOff], O.[EnableScripting], O.[EnableBilling], O.[UseDocumentTypeVersioning], O.[EnableSMSSTOP], O.[UseEngageMail], O.[PreventForcedLogout]
				FROM
				    [dbo].[ClientOption] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[ClientOptionID] = PageIndex.[ClientOptionID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ClientOption] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOption_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_GetPaged] TO [sp_executeall]
GO
