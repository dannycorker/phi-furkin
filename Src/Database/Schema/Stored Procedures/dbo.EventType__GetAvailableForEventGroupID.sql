SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim
-- Create date: 2013-01-25
-- Description:	Get minimal EventType details for the EventGroupEdit page (list of available events)
-- SB  2014-07-10 Updated to use view which includes shared event types
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetAvailableForEventGroupID] 
	@EventGroupID INT,
	@ClientID INT,
	@LeadTypeID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Get all possible events for the right lead type(s) that do not already belong to this event group */
	SELECT et.EventTypeID, 
	et.EventTypeName, 
	lt.LeadTypeName, 
	CASE WHEN et.Enabled = 1 THEN 'Y' ELSE '' END as [Enabled], 
	CASE WHEN et.InProcess = 1 THEN 'Y' ELSE '' END as [InProcess], 
	est.EventSubtypeName 
	FROM dbo.fnEventTypeShared(@ClientID) et
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = et.LeadTypeID
	INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON est.EventSubtypeID = et.EventSubtypeID 
	WHERE et.ClientID = @ClientID 
	AND (@LeadTypeID = -1 OR et.LeadTypeID = @LeadTypeID) /* LeadType is optional for EventGroup but enforce it if selected. */
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.EventGroupMember egm WITH (NOLOCK) 
		WHERE egm.EventGroupID = @EventGroupID 
		AND egm.EventTypeID = et.EventTypeID	
	)
	ORDER BY et.EventTypeName, lt.LeadTypeName
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetAvailableForEventGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetAvailableForEventGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetAvailableForEventGroupID] TO [sp_executeall]
GO
