SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all DetailFieldPages for a client, leadtype
-- =============================================
CREATE PROCEDURE [dbo].[dfp] 
	@ClientID int, 
	@LeadTypeID int = NULL, 
	@LeadOrMatter int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.DetailFieldPages dfp (nolock) 
	WHERE (dfp.ClientID = @ClientID) 
	AND (dfp.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
	AND (dfp.LeadOrMatter = @LeadOrMatter OR @LeadOrMatter IS NULL) 
	ORDER BY dfp.ClientID, dfp.LeadTypeID, dfp.LeadOrMatter, dfp.PageCaption 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[dfp] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dfp] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dfp] TO [sp_executeall]
GO
