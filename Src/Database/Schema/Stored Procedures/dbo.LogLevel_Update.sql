SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LogLevel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LogLevel_Update]
(

	@LogLevelID int   ,

	@OriginalLogLevelID int   ,

	@LogLevelName varchar (250)  ,

	@LogLevelDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LogLevel]
				SET
					[LogLevelID] = @LogLevelID
					,[LogLevelName] = @LogLevelName
					,[LogLevelDescription] = @LogLevelDescription
				WHERE
[LogLevelID] = @OriginalLogLevelID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogLevel_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Update] TO [sp_executeall]
GO
