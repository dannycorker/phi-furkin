SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UploadedFile table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_Insert]
(

	@UploadedFileID int    OUTPUT,

	@ClientID int   ,

	@FolderID int   ,

	@UploadedFileName varchar (255)  ,

	@UploadedFileTitle varchar (1000)  ,

	@UploadedFileDescription varchar (2000)  ,

	@UploadedFile varbinary (MAX)  ,

	@UploadedFileGUID uniqueidentifier    OUTPUT,

	@UploadedFileSize bigint    OUTPUT,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[UploadedFile]
					(
					[ClientID]
					,[FolderID]
					,[UploadedFileName]
					,[UploadedFileTitle]
					,[UploadedFileDescription]
					,[UploadedFile]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@FolderID
					,@UploadedFileName
					,@UploadedFileTitle
					,@UploadedFileDescription
					,@UploadedFile
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @UploadedFileID = SCOPE_IDENTITY()
									
				-- Select computed columns into output parameters
				SELECT
 @UploadedFileSize = [UploadedFileSize]
				FROM
					[dbo].[UploadedFile] WITH (NOLOCK) 
				WHERE
[UploadedFileID] = @UploadedFileID 
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Insert] TO [sp_executeall]
GO
