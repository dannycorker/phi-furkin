SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuestionnaireColours table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_Get_List]

AS


				
				SELECT
					[QuestionnaireColourID],
					[ClientQuestionnaireID],
					[Page],
					[Header],
					[Intro],
					[Body],
					[Footer],
					[ClientID]
				FROM
					[dbo].[QuestionnaireColours] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Get_List] TO [sp_executeall]
GO
