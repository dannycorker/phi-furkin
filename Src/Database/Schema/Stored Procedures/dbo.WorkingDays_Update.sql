SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the WorkingDays table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_Update]
(

	@WorkingDayID int   ,

	@Year int   ,

	@Month int   ,

	@Day int   ,

	@DayNumber int   ,

	@IsWorkDay bit   ,

	@IsWeekDay bit   ,

	@IsBankHoliday bit   ,

	@Date datetime   ,

	@WeekNumber tinyint   ,

	@DayNumberOfWeek tinyint   ,

	@CharDate char (10)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[WorkingDays]
				SET
					[Year] = @Year
					,[Month] = @Month
					,[Day] = @Day
					,[DayNumber] = @DayNumber
					,[IsWorkDay] = @IsWorkDay
					,[IsWeekDay] = @IsWeekDay
					,[IsBankHoliday] = @IsBankHoliday
					,[Date] = @Date
					,[WeekNumber] = @WeekNumber
					,[DayNumberOfWeek] = @DayNumberOfWeek
					,[CharDate] = @CharDate
				WHERE
[WorkingDayID] = @WorkingDayID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Update] TO [sp_executeall]
GO
