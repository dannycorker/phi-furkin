SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2015-09-23
-- Description:	Housekeeping for RemoteAuthentication Passcodes that have expired.
-- =============================================
CREATE PROCEDURE [dbo].[RemoteAuthentication_Housekeeping] 
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @LocalDate DATETIME = dbo.fn_GetDate_Local()
	/*
		ValidUntil is the column that dictates when to delete each record.
		Move them to the history table for reference. 
		The clause OUTPUT deleted.[column name] INTO dbo.RemoteAuthHistory achieves this.
	*/
	DELETE dbo.RemoteAuth 
		OUTPUT deleted.[AuthKey], deleted.[RequestedUsername], deleted.[ClientPersonnelID], deleted.[ClientID], deleted.[WhenRequested], deleted.[IPAddress], deleted.[IsValid], deleted.[ValidUntil], deleted.[WhenFirstUsed], deleted.[WhenLastUsed], deleted.[UsageCount], @LocalDate, 'Valid' 
		INTO dbo.RemoteAuthHistory(AuthKey, RequestedUsername, ClientPersonnelID, ClientID, WhenRequested, IPAddress, IsValid, ValidUntil, WhenFirstUsed, WhenLastUsed, UsageCount, WhenRemoved, Notes) 
	FROM dbo.RemoteAuth r 
	WHERE r.ValidUntil < @LocalDate
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoteAuthentication_Housekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoteAuthentication_Housekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoteAuthentication_Housekeeping] TO [sp_executeall]
GO
