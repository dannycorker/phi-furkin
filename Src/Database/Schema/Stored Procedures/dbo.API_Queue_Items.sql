SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_GetQueueItems
-- =============================================
CREATE PROCEDURE [dbo].[API_Queue_Items]
	@QID UNIQUEIDENTIFIER
AS
BEGIN
	
	SELECT	TOP 1 QueueID, IdentID, AppKey, ObjectKey, ParentQID, HandleDT, ErrorMessage
	FROM	API_Queue WITH (NOLOCK) 
	WHERE	ParentQID = @QID
	AND		HandleDT IS NULL
	AND		ErrorMessage IS NOT NULL
	UNION ALL
	SELECT	QueueID, IdentID, AppKey, ObjectKey, ParentQID, HandleDT, ErrorMessage
	FROM	API_Queue WITH (NOLOCK) 
	WHERE	QueueID = @QID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Queue_Items] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Queue_Items] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Queue_Items] TO [sp_executeall]
GO
