SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-11-2012
-- Description:	Deletes Event type automated work flow by event type id
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedWorkflow__DeleteByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	DELETE FROM EventTypeAutomatedWorkflow
	WHERE EventTypeID=@EventTypeID AND ClientID=@ClientID	

END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflow__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedWorkflow__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedWorkflow__DeleteByEventTypeID] TO [sp_executeall]
GO
