SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ben Crinion
-- Create date: 01-September-2008
-- Description:	Validates Lead and Case belong to specified Client and belong to each other.
-- =============================================
CREATE PROCEDURE [dbo].[ValidateLeadCaseIDAndUserID]
	@ClientID int ,
	@LeadID int ,
	@CaseID int,
	@ClientPersonnelID int
AS
BEGIN

	DECLARE @UserOK bit
	DECLARE @LeadTypeID int

	SELECT @LeadTypeID = 0

	SET NOCOUNT ON;

	SELECT @LeadTypeID = l.LeadTypeID
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID
	AND	EXISTS (
		SELECT	ll.*
		FROM dbo.Lead ll WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON ll.LeadID = c.LeadID
		WHERE ll.LeadID = @LeadID
		AND ll.ClientID = @ClientID
		AND c.ClientID = @ClientID
		AND c.CaseID = @CaseID
	)
	AND EXISTS
	(
		SELECT * 
		FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
		WHERE cp.ClientPersonnelID	= @ClientPersonnelID
		AND cp.ClientID	= @ClientID
	)

	RETURN @LeadTypeID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[ValidateLeadCaseIDAndUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidateLeadCaseIDAndUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidateLeadCaseIDAndUserID] TO [sp_executeall]
GO
