SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingSystemCredential table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartySystemID]
(

	@ThirdPartySystemID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyMappingSystemCredentialID],
					[ThirdPartySystemID],
					[ClientID],
					[ConsumerKey],
					[ConsumerSecret]
				FROM
					[dbo].[ThirdPartyMappingSystemCredential] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemID] = @ThirdPartySystemID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartySystemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartySystemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartySystemID] TO [sp_executeall]
GO
