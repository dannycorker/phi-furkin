SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-05
-- Description:	List missing mandatory events for a Lead and Case
-- =============================================
CREATE PROCEDURE [dbo].[GetMissingMandatoryEvents]  
	@EventTypeID int, 
	@LeadID int, 
	@CaseID int 
AS
BEGIN

	-- List all missing mandatory events for this Case
	SELECT et.EventTypeName as MissingEventTypeName, et.EventTypeID as MissingEventTypeID 
	FROM dbo.EventTypeMandatoryEvent etme (nolock)
	INNER JOIN dbo.EventType et (nolock) ON et.EventTypeID = etme.RequiredEventID  
	INNER JOIN dbo.Lead l (nolock) ON etme.LeadTypeID = l.LeadTypeID AND l.LeadID = @LeadID 
	WHERE etme.ToEventTypeID = @EventTypeID 
	/*AND et.Enabled = 1 */
	AND NOT EXISTS( 
		SELECT * 
		FROM dbo.LeadEvent le (nolock) 
		WHERE le.LeadID = @LeadID 
		AND le.CaseID = @CaseID 
		AND le.EventTypeID = etme.RequiredEventID 
		AND le.EventDeleted = 0 
	)
	ORDER BY 1 
	
	/* Some callers need the count back as the return code */
	RETURN @@ROWCOUNT
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryEvents] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMissingMandatoryEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryEvents] TO [sp_executeall]
GO
