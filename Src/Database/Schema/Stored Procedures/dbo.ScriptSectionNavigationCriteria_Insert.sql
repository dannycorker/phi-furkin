SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptSectionNavigationCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria_Insert]
(

	@ScriptSectionNavigationCriteriaID int    OUTPUT,

	@ClientID int   ,

	@FromStepSectionID int   ,

	@ToStepSectionID int   ,

	@WizardButton varchar (12)  ,

	@DetailFieldID int   ,

	@CriteriaOperator varchar (12)  ,

	@Value varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[ScriptSectionNavigationCriteria]
					(
					[ClientID]
					,[FromStepSectionID]
					,[ToStepSectionID]
					,[WizardButton]
					,[DetailFieldID]
					,[CriteriaOperator]
					,[Value]
					)
				VALUES
					(
					@ClientID
					,@FromStepSectionID
					,@ToStepSectionID
					,@WizardButton
					,@DetailFieldID
					,@CriteriaOperator
					,@Value
					)
				-- Get the identity value
				SET @ScriptSectionNavigationCriteriaID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Insert] TO [sp_executeall]
GO
