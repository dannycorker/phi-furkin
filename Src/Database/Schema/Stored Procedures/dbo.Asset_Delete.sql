SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Asset table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Asset_Delete]
(

	@AssetID int   
)
AS


				DELETE FROM [dbo].[Asset] WITH (ROWLOCK) 
				WHERE
					[AssetID] = @AssetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_Delete] TO [sp_executeall]
GO
