SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPersonnelAdminGroups table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAdminGroups_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientID int   = null ,

	@GroupName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPersonnelAdminGroupID]
	, [ClientID]
	, [GroupName]
    FROM
	[dbo].[ClientPersonnelAdminGroups] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([GroupName] = @GroupName OR @GroupName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPersonnelAdminGroupID]
	, [ClientID]
	, [GroupName]
    FROM
	[dbo].[ClientPersonnelAdminGroups] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([GroupName] = @GroupName AND @GroupName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAdminGroups_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_Find] TO [sp_executeall]
GO
