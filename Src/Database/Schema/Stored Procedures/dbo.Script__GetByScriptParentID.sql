SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2014
-- Description:	Gets a list of children scripts by there Parent Script ID
-- =============================================
CREATE PROCEDURE [dbo].[Script__GetByScriptParentID]
	@ClientID INT,
	@ScriptParentID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM Script WITH (NOLOCK) 
	WHERE ClientID=@ClientID and ScriptParentID=@ScriptParentID
	ORDER BY WhenCreated DESC
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetByScriptParentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetByScriptParentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetByScriptParentID] TO [sp_executeall]
GO
