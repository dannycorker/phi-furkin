SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 09/01/2014
-- Description:	Deletes a pending account record 
-- =============================================
CREATE PROCEDURE [dbo].[PendingAccount__DeleteRecord]	
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM PendingAccount
	WHERE ClientPersonnelID=@ClientPersonnelID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__DeleteRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PendingAccount__DeleteRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__DeleteRecord] TO [sp_executeall]
GO
