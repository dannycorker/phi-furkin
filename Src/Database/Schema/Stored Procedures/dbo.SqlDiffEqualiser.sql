SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-12
-- Description:	List columns on tables where we mess with DAL procs on the live DB.
--              Any changes between dev and last-known-live-list will show up during the DALGen process,
--              warning us to check these procs and change "ReplaceAmendedDALProcs.sql" if need be.
-- =============================================
CREATE PROCEDURE [dbo].[SqlDiffEqualiser] 
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @TableList TABLE (
		TableName varchar(100)
	)

	INSERT @TableList(TableName) VALUES 
	 ('AutoAdjudication')
	,('DetailFieldAlias')
	,('DocumentType')
	,('LeadDocument')
	,('MatterDetailValues')
	,('PortalUserCase')
	,('TableDetailValues')
	,('TableRows')
	,('UploadedFile')

	SELECT c.TABLE_NAME, c.ORDINAL_POSITION, c.COLUMN_NAME, c.DATA_TYPE 
	FROM @TableList tl
	INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = tl.TableName
	ORDER BY c.TABLE_NAME, c.ORDINAL_POSITION
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlDiffEqualiser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlDiffEqualiser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlDiffEqualiser] TO [sp_executeall]
GO
