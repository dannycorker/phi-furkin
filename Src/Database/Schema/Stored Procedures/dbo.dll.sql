SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-11
-- Description:	DataLoaderLog helper
-- =============================================
CREATE PROCEDURE [dbo].[dll] 
	@ShowAllForLatestFile bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @DataLoaderFileID int, 
	@LastDataLoaderLogID int, 
	@FirstForThisFileDataLoaderLogID int
	
	/* Quick count of log records to start with  */
	EXEC dbo.rc 'DataLoaderLog' 
	
	
	/* Now get details of the latest file */
	SELECT TOP 1 @DataLoaderFileID = dll.DataLoaderFileID, 
	@LastDataLoaderLogID = dll.DataLoaderLogID 
	FROM dbo.DataLoaderLog dll WITH (NOLOCK) 
	ORDER BY dll.DataLoaderLogID DESC 
	
	IF @ShowAllForLatestFile = 0
	BEGIN
		SELECT @FirstForThisFileDataLoaderLogID = MIN(DataLoaderLogID)
		FROM dbo.DataLoaderLog dll WITH (NOLOCK) 
		WHERE dll.DataLoaderFileID = @DataLoaderFileID 
	END
		
	/* First and last records only */
	SELECT RIGHT(dll.Message, 22) as [TimeStamp], 
	dll.DataLoaderFileID, 
	dll.ClientID, 
	dll.RowIndex, 
	dll.ColIndex, 
	dll.Message, 
	dll.LogLevel,
	dll.NodeName,
	dll.DataLoaderLogID,
	dll.DataLoaderMapID,
	dll.DataLoaderMapSectionID,
	dll.DataLoaderObjectFieldID,
	dll.DataLoaderFieldDefinitionID
	FROM dbo.DataLoaderLog dll WITH (NOLOCK) 
	WHERE dll.DataLoaderFileID = @DataLoaderFileID
	AND (@ShowAllForLatestFile = 1 OR dll.DataLoaderLogID IN (@LastDataLoaderLogID, @FirstForThisFileDataLoaderLogID)) 
	ORDER BY dll.DataLoaderLogID DESC

	SELECT 'fn_C00_DataLoaderETA' [Tom's Shiny Function], *-- , DATEADD(minute,eta.etaMinutes,eta.StartTime) [CompletionTime]
	FROM dbo.fn_C00_DataLoaderETA(@DataLoaderFileID) eta
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[dll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dll] TO [sp_executeall]
GO
