SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChartSeriesType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesType_Delete]
(

	@ChartSeriesTypeID int   
)
AS


				DELETE FROM [dbo].[ChartSeriesType] WITH (ROWLOCK) 
				WHERE
					[ChartSeriesTypeID] = @ChartSeriesTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Delete] TO [sp_executeall]
GO
