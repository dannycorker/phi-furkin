SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryColumns table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryColumns_GetBySqlQueryIDSqlQueryTableID]
(

	@SqlQueryID int   ,

	@SqlQueryTableID int   
)
AS


				SELECT
					[SqlQueryColumnID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryTableID],
					[CompleteOutputText],
					[ColumnAlias],
					[ShowColumn],
					[DisplayOrder],
					[SortOrder],
					[SortType],
					[TempColumnID],
					[TempTableID],
					[IsAggregate],
					[ColumnDataType],
					[ManipulatedDataType],
					[ColumnNaturalName],
					[SourceID],
					[ComplexValue]
				FROM
					[dbo].[SqlQueryColumns] WITH (NOLOCK) 
				WHERE
										[SqlQueryID] = @SqlQueryID
					AND [SqlQueryTableID] = @SqlQueryTableID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_GetBySqlQueryIDSqlQueryTableID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryColumns_GetBySqlQueryIDSqlQueryTableID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_GetBySqlQueryIDSqlQueryTableID] TO [sp_executeall]
GO
