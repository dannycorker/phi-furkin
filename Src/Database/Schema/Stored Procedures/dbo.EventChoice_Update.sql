SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice_Update]
(

	@EventChoiceID int   ,

	@EventTypeID int   ,

	@Description varchar (50)  ,

	@NextEventTypeID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ThreadNumber int   ,

	@EscalationEvent bit   ,

	@Field varchar (50)  ,

	@LogicalOperator varchar (50)  ,

	@Value1 varchar (50)  ,

	@Value2 varchar (50)  ,

	@SqlClauseForInclusion varchar (2000)  ,

	@Weighting int   ,

	@ValueTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventChoice]
				SET
					[EventTypeID] = @EventTypeID
					,[Description] = @Description
					,[NextEventTypeID] = @NextEventTypeID
					,[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[ThreadNumber] = @ThreadNumber
					,[EscalationEvent] = @EscalationEvent
					,[Field] = @Field
					,[LogicalOperator] = @LogicalOperator
					,[Value1] = @Value1
					,[Value2] = @Value2
					,[SqlClauseForInclusion] = @SqlClauseForInclusion
					,[Weighting] = @Weighting
					,[ValueTypeID] = @ValueTypeID
				WHERE
[EventChoiceID] = @EventChoiceID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_Update] TO [sp_executeall]
GO
