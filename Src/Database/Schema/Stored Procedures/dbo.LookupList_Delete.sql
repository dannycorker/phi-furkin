SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LookupList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_Delete]
(

	@LookupListID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[LookupList] WITH (ROWLOCK) 
				WHERE
					[LookupListID] = @LookupListID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_Delete] TO [sp_executeall]
GO
