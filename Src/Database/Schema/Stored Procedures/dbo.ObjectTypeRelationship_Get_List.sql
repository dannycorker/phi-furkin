SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ObjectTypeRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_Get_List]

AS


				
				SELECT
					[ObjectTypeRelationshipID],
					[FromObjectTypeID],
					[ToObjectTypeID],
					[RelationshipTypeID],
					[RelationshipName]
				FROM
					[dbo].[ObjectTypeRelationship] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Get_List] TO [sp_executeall]
GO
