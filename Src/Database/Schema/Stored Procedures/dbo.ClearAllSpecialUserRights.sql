SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-02
-- Description:	Clear all special User Rights etc
-- =============================================
CREATE PROCEDURE [dbo].[ClearAllSpecialUserRights]
	-- Add the parameters for the stored procedure here
	@UserID int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

	DELETE UserRightsDynamic 
	WHERE ClientPersonnelID = @UserID

	DELETE UserFunctionControl
	WHERE ClientPersonnelID = @UserID

	-- If this user is/was being edited, kill that session now as well
	EXEC [dbo].[CancelUserRightsForEditing] @UserID

	COMMIT
	
END









GO
GRANT VIEW DEFINITION ON  [dbo].[ClearAllSpecialUserRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClearAllSpecialUserRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClearAllSpecialUserRights] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[ClearAllSpecialUserRights] TO [sp_executehelper]
GO
