SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-04-16
-- Description:	Gets a nicely formatted list of values to be
--				used in a Grid
-- =============================================
CREATE PROCEDURE [dbo].[RPIFieldMapping__GetByClientIDForGrid]
	@ClientID int,
	@LeadTypeID int,
	@RPIFieldGroupID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	rpifm.RPIFieldMappingID, 
			rpif.RPIFieldID,
			rpif.RPIFieldName,
			rpifg.GroupName,
			dfs.DetailFieldSubTypeDescription AS 'RPIFieldType',
			rpifm.DetailFieldID AS 'AQDetailFieldID', 
			df.FieldName AS 'AQFieldName',
			rpifm.ColumnDetailFieldID,
			cdf.FieldName AS 'ColumnDF', 
			CASE
				WHEN rpifm.IsEnabled = 1 THEN 'True'
				WHEN rpifm.IsEnabled = 0 THEN 'False'
				ELSE 'False'
			END AS 'IsEnabled',
			rpif.DetailFieldSubtypeID,
			CASE 
				WHEN rpifm.RPIFieldMappingID IS NULL THEN 'False' 
				ELSE 'True' 
			END AS 'Mapped'
	FROM RPIField rpif WITH (NOLOCK)
	LEFT JOIN RPIFieldMapping rpifm WITH (NOLOCK) ON rpifm.RPIFieldID = rpif.RPIFieldID AND rpifm.LeadTypeID = @LeadTypeID AND rpifm.ClientID = @ClientID
	LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = rpifm.DetailFieldID
	LEFT JOIN DetailFields cdf WITH (NOLOCK) ON cdf.DetailFieldID = rpifm.ColumnDetailFieldID
	LEFT JOIN RPIFieldGroup rpifg WITH (NOLOCK) ON rpifg.RPIFieldGroupID = rpif.RPIFieldGroupID
	INNER JOIN DetailFieldSubType dfs WITH (NOLOCK) ON dfs.DetailFieldSubTypeID = rpif.DetailFieldSubtypeID
	WHERE rpif.RPIFieldGroupID = @RPIFieldGroupID OR @RPIFieldGroupID = 0
	AND rpif.IsEnabled = 1
END




GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping__GetByClientIDForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping__GetByClientIDForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping__GetByClientIDForGrid] TO [sp_executeall]
GO
