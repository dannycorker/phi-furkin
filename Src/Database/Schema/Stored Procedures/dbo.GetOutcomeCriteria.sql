SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetOutcomeCriteria    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[GetOutcomeCriteria] @OutcomeID int
AS

SELECT     OutcomeCriterias.OutcomeID, QuestionPossibleAnswers.AnswerText, OutcomeCriterias.OutcomeCriteriaID, 
                      OutcomeCriterias.QuestionPossibleAnswerID, QuestionPossibleAnswers.MasterQuestionID, OutcomeCriterias.ClientID 
FROM         QuestionPossibleAnswers INNER JOIN
                      OutcomeCriterias ON QuestionPossibleAnswers.QuestionPossibleAnswerID = OutcomeCriterias.QuestionPossibleAnswerID
WHERE     (OutcomeCriterias.OutcomeID = @OutcomeID)
ORDER BY QuestionPossibleAnswers.MasterQuestionID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCriteria] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeCriteria] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCriteria] TO [sp_executeall]
GO
