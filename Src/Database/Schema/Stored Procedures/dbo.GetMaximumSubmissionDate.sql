SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetMaximumSubmissionDate]

@ClientQuestionnaireID int

AS

SELECT     ClientQuestionnaireID, MAX(SubmissionDate) AS MaxSubmissionDate
FROM         dbo.CustomerQuestionnaires
GROUP BY ClientQuestionnaireID
HAVING      (ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetMaximumSubmissionDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMaximumSubmissionDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMaximumSubmissionDate] TO [sp_executeall]
GO
