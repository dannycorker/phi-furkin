SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-30
-- Description:	Quick check of the last [n] log entries in reverse order
-- =============================================
CREATE PROCEDURE [dbo].[LogCheck]
	@rows int = 50,
	@typeofentry varchar(50) = null,
	@classname varchar(50) = null,
	@userid int = null,
	@clientid int = null
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (@typeofentry IS NULL AND @classname IS NULL AND @userid IS NULL AND @clientid IS NULL)
	BEGIN
		SELECT TOP (@rows) l.LogDateTime, 
		l.TypeOfLogEntry,
		l.ClientPersonnelID, 
		l.ClassName, 
		l.MethodName, 
		l.LogEntry,
		l.LogID
		FROM dbo.logs l WITH (nolock) 
		ORDER BY l.LogDateTime DESC 
	END
	ELSE
	BEGIN
		SELECT TOP (@rows) CONVERT(VARCHAR, l.LogDateTime, 120) as [Time], 
		l.TypeOfLogEntry as [Type],
		cp.ClientID as [Client], 
		l.ClientPersonnelID as [User], 
		cp.UserName, 
		l.ClassName as [Class], 
		l.MethodName as [Method], 
		CASE WHEN l.LogEntry LIKE '%attempt % of%' THEN LEFT(RIGHT(l.LogEntry, 7), 6) ELSE '' END as [Attempt], /* 'This was attempt 2 of 3.' */
		l.LogEntry,
		l.LogID
		FROM dbo.logs l WITH (NOLOCK) 
		LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = l.ClientPersonnelID 
		WHERE (@typeofentry IS NULL OR l.TypeOfLogEntry LIKE @typeofentry) 
		AND (@classname IS NULL OR l.ClassName LIKE @classname) 
		AND (@userid IS NULL OR l.ClientPersonnelID = @userid) 
		AND (@clientid IS NULL OR cp.ClientID = @clientid)
		ORDER BY l.LogDateTime DESC 
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LogCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogCheck] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[LogCheck] TO [sp_executehelper]
GO
