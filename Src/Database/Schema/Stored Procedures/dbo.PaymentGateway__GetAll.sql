SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 04/08/2016
-- Description:	Gets a list of payment gateways
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__GetAll] 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM PaymentGateway WITH (NOLOCK) 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__GetAll] TO [sp_executeall]
GO
