SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-07-20
-- Description:	Policy Discounts – Get By MatterID associated discounts on the policy.
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetDiscountDataByPolicy]
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = [dbo].[fnGetPrimaryClientID]()

	SELECT mdv.MatterID, discountcode.DetailValue AS [DiscountCode], discountdescription.DetailValue AS [Description], discountvalue.DetailValue AS [Discount], lli.ItemValue AS [Type]
	FROM Matter matter WITH (NOLOCK)
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = matter.MatterID
	INNER JOIN ResourceList rl WITH (NOLOCK) ON rl.ResourceListID = mdv.ValueInt
	INNER JOIN ResourceListDetailValues discountcode ON discountcode.ResourceListID = rl.ResourceListID AND discountcode.DetailFieldID = 177363
	INNER JOIN ResourceListDetailValues discountdescription ON discountdescription.ResourceListID = rl.ResourceListID AND discountdescription.DetailFieldID = 179704
	INNER JOIN ResourceListDetailValues discountvalue ON discountvalue.ResourceListID = rl.ResourceListID AND discountvalue.DetailFieldID = 179715
	INNER JOIN ResourceListDetailValues discounttype ON discounttype.ResourceListID = rl.ResourceListID AND discounttype.DetailFieldID = 179706
	INNER JOIN LookupListItems lli WITH (NOLOCK) ON lli.LookupListItemID = discounttype.ValueInt
	WHERE mdv.MatterID = @MatterID
	AND mdv.DetailFieldID = 175488 /*Campaign Code*/
	AND mdv.ClientID = @ClientID
	AND matter.ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetDiscountDataByPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetDiscountDataByPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetDiscountDataByPolicy] TO [sp_executeall]
GO
