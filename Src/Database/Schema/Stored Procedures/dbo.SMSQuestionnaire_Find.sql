SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SMSQuestionnaire table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_Find]
(

	@SearchUsingOR bit   = null ,

	@SMSQuestionnaireID int   = null ,

	@ClientID int   = null ,

	@Title varchar (250)  = null ,

	@Description varchar (2000)  = null ,

	@Enabled bit   = null ,

	@ToTarget varchar (250)  = null ,

	@CreateCustomerIfNotFound bit   = null ,

	@UseSentimentAnalysis bit   = null ,

	@RuleSetID int   = null ,

	@IsVoice bit   = null ,

	@Timeout int   = null ,

	@Repeat int   = null ,

	@RepeatPrefix varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SMSQuestionnaireID]
	, [ClientID]
	, [Title]
	, [Description]
	, [Enabled]
	, [ToTarget]
	, [CreateCustomerIfNotFound]
	, [UseSentimentAnalysis]
	, [RuleSetID]
	, [IsVoice]
	, [Timeout]
	, [Repeat]
	, [RepeatPrefix]
    FROM
	[dbo].[SMSQuestionnaire] WITH (NOLOCK) 
    WHERE 
	 ([SMSQuestionnaireID] = @SMSQuestionnaireID OR @SMSQuestionnaireID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Title] = @Title OR @Title IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([ToTarget] = @ToTarget OR @ToTarget IS NULL)
	AND ([CreateCustomerIfNotFound] = @CreateCustomerIfNotFound OR @CreateCustomerIfNotFound IS NULL)
	AND ([UseSentimentAnalysis] = @UseSentimentAnalysis OR @UseSentimentAnalysis IS NULL)
	AND ([RuleSetID] = @RuleSetID OR @RuleSetID IS NULL)
	AND ([IsVoice] = @IsVoice OR @IsVoice IS NULL)
	AND ([Timeout] = @Timeout OR @Timeout IS NULL)
	AND ([Repeat] = @Repeat OR @Repeat IS NULL)
	AND ([RepeatPrefix] = @RepeatPrefix OR @RepeatPrefix IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SMSQuestionnaireID]
	, [ClientID]
	, [Title]
	, [Description]
	, [Enabled]
	, [ToTarget]
	, [CreateCustomerIfNotFound]
	, [UseSentimentAnalysis]
	, [RuleSetID]
	, [IsVoice]
	, [Timeout]
	, [Repeat]
	, [RepeatPrefix]
    FROM
	[dbo].[SMSQuestionnaire] WITH (NOLOCK) 
    WHERE 
	 ([SMSQuestionnaireID] = @SMSQuestionnaireID AND @SMSQuestionnaireID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Title] = @Title AND @Title is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([ToTarget] = @ToTarget AND @ToTarget is not null)
	OR ([CreateCustomerIfNotFound] = @CreateCustomerIfNotFound AND @CreateCustomerIfNotFound is not null)
	OR ([UseSentimentAnalysis] = @UseSentimentAnalysis AND @UseSentimentAnalysis is not null)
	OR ([RuleSetID] = @RuleSetID AND @RuleSetID is not null)
	OR ([IsVoice] = @IsVoice AND @IsVoice is not null)
	OR ([Timeout] = @Timeout AND @Timeout is not null)
	OR ([Repeat] = @Repeat AND @Repeat is not null)
	OR ([RepeatPrefix] = @RepeatPrefix AND @RepeatPrefix is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_Find] TO [sp_executeall]
GO
