SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-06-11
-- Description:	Return a customer object by LeadRef
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Customer_SearchByPolicyNumber]
(
	 @PolicyNumber	VARCHAR(100) = ''
	,@PostCode		VARCHAR(2000) = NULL
	,@Forename		VARCHAR(2000) = NULL
	,@Lastname		VARCHAR(2000) = NULL
)
AS
BEGIN
 
	SET NOCOUNT ON;
	
	DECLARE  @MatchFound		BIT = 0
			,@CustomerID		INT
			,@LogEntry			VARCHAR(2000) = ''
	
	SELECT TOP 1 @MatchFound = 1, @CustomerID = cu.CustomerID 
	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID 
	WHERE l.LeadTypeID = 1492 /*Policy Admin*/
	AND l.LeadRef = @PolicyNumber
	AND ( REPLACE(cu.PostCode,' ','') = REPLACE(@PostCode,' ','') or @PostCode is NULL )	/*CPS 2017-08-25 for ticket #45113*/
	AND ( cu.FirstName = @Forename or @Forename is NULL )									/*CPS 2017-08-25 for ticket #45113*/
	AND ( cu.LastName = @Lastname or @Lastname is NULL )									/*CPS 2017-08-25 for ticket #45113*/

	ORDER BY cu.Test DESC, l.LeadID DESC
	
	IF @CustomerID <> 0
	BEGIN
	
		SELECT	 cu.CustomerID
				,@MatchFound					[MatchFound]
				,cu.Test						[TestCustomer]
				,ISNULL(t.Title,'')				[Title]
				,ISNULL(cu.FirstName,'')		[FirstName]
				,ISNULL(cu.LastName,'')			[LastName]
				,ISNULL(cu.Address1,'')			[Address1]
				,ISNULL(cu.Address2,'')			[Address2]
				,ISNULL(cu.Town,'')				[Town]
				,ISNULL(cu.County,'')			[County]
				,ISNULL(cu.PostCode,'')			[PostCode]
				,ISNULL(cu.EmailAddress,'')		[EmailAddress]
				,ISNULL(cu.HomeTelephone,'')	[HomeTelephone]
				,ISNULL(cu.MobileTelephone,'')	[MobileTelephone]
				,cu.DateOfBirth					[DateOfBirth]
				,dbo.fnGetSimpleDvLuli(170257/*Send documents by*/,cu.CustomerID) [SendDocumentsBy]
				,dbo.fnGetSimpleDv    (170257/*Send documents by*/,cu.CustomerID) [SendDocumentsById]
				,COUNT(l.LeadID)				[TotalPolicyCount]
				,COUNT(mdv.MatterDetailValueID) [LivePolicyCount]
		FROM Customers cu WITH ( NOLOCK )
		 LEFT JOIN Titles t WITH ( NOLOCK ) on t.TitleID = cu.TitleID
		INNER JOIN Lead l WITH ( NOLOCK ) on l.CustomerID = cu.CustomerID AND l.LeadTypeID = 1492 /*Policy Admin*/
		 LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.LeadID = l.LeadID AND mdv.DetailFieldID = 170038 /*Policy Status*/ AND mdv.ValueInt IN ( 43002 /*Live*/,74573 /*Underwriting*/ )
		WHERE cu.CustomerID = @CustomerID
		GROUP BY cu.CustomerID
				,cu.Test						
				,ISNULL(t.Title,'')
				,ISNULL(cu.FirstName,'')
				,ISNULL(cu.LastName,'')
				,ISNULL(cu.Address1,'')
				,ISNULL(cu.Address2,'')
				,ISNULL(cu.Town,'')
				,ISNULL(cu.County,'')
				,ISNULL(cu.PostCode,'')
				,ISNULL(cu.EmailAddress,'')
				,ISNULL(cu.HomeTelephone,'')
				,ISNULL(cu.MobileTelephone,'')
				,cu.DateOfBirth
				,dbo.fnGetSimpleDvLuli(170257/*Send documents by*/,cu.CustomerID)
				,dbo.fnGetSimpleDv    (170257/*Send documents by*/,cu.CustomerID)
		FOR XML AUTO
	END
	ELSE
	BEGIN
		SELECT	 0				[CustomerID]
				,@MatchFound	[MatchFound]
	END
	
	SELECT @LogEntry = ' @PolicyNumber = ''' + @PolicyNumber + ''', @Postcode '''+ ISNULL(@Postcode,'NULL') +' @Forename = ''' + ISNULL(@Forename, 'NULL')+ ''', @Lastname '''+ ISNULL(@Lastname,'NULL') +''' -- @CustomerID = ' + ISNULL(CONVERT(VARCHAR,@CustomerID),'NULL')
	EXEC _C00_LogIt 'Info', '_C600_LOG', '_C600_PA_Customer_SearchByPolicyNumber', @LogEntry, 0
		
END









GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Customer_SearchByPolicyNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Customer_SearchByPolicyNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Customer_SearchByPolicyNumber] TO [sp_executeall]
GO
