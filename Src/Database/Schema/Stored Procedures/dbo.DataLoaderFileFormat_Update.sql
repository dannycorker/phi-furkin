SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderFileFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileFormat_Update]
(

	@DataLoaderFileFormatID int   ,

	@OriginalDataLoaderFileFormatID int   ,

	@FileFormatName varchar (250)  ,

	@FileFormatDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderFileFormat]
				SET
					[DataLoaderFileFormatID] = @DataLoaderFileFormatID
					,[FileFormatName] = @FileFormatName
					,[FileFormatDescription] = @FileFormatDescription
				WHERE
[DataLoaderFileFormatID] = @OriginalDataLoaderFileFormatID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileFormat_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Update] TO [sp_executeall]
GO
