SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
 -- Author:		Paul Richardson
 -- Create date: 11-07-2017
 -- Description:	Deletes all the [DocumentSpecialisedDetailFieldTarget]
 --              records that have the give document type version id
 -- =============================================
CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget__DeleteByDocumentTypeVersionID]
(
@DocumentTypeVersionID int
)
AS
 
 
DELETE FROM [dbo].[DocumentSpecialisedDetailFieldTarget] WITH (ROWLOCK)
WHERE [DocumentTypeVersionID] = @DocumentTypeVersionID
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget__DeleteByDocumentTypeVersionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
