SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuestionnaireFrameSources table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_Find]
(

	@SearchUsingOR bit   = null ,

	@QuestionnaireFrameSourceID int   = null ,

	@ClientID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@PageNumber int   = null ,

	@SourceUrl varchar (555)  = null ,

	@FrameType int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuestionnaireFrameSourceID]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [PageNumber]
	, [SourceUrl]
	, [FrameType]
    FROM
	[dbo].[QuestionnaireFrameSources] WITH (NOLOCK) 
    WHERE 
	 ([QuestionnaireFrameSourceID] = @QuestionnaireFrameSourceID OR @QuestionnaireFrameSourceID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([PageNumber] = @PageNumber OR @PageNumber IS NULL)
	AND ([SourceUrl] = @SourceUrl OR @SourceUrl IS NULL)
	AND ([FrameType] = @FrameType OR @FrameType IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuestionnaireFrameSourceID]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [PageNumber]
	, [SourceUrl]
	, [FrameType]
    FROM
	[dbo].[QuestionnaireFrameSources] WITH (NOLOCK) 
    WHERE 
	 ([QuestionnaireFrameSourceID] = @QuestionnaireFrameSourceID AND @QuestionnaireFrameSourceID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([PageNumber] = @PageNumber AND @PageNumber is not null)
	OR ([SourceUrl] = @SourceUrl AND @SourceUrl is not null)
	OR ([FrameType] = @FrameType AND @FrameType is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Find] TO [sp_executeall]
GO
