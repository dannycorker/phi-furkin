SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgaan
-- Create date: 2016-09-08
-- Description:	Performs a billing reinstatement.
--				Adapted from MidTermAdjustment__CreateFromThirdPartyFields
-- Mods
-- 2016/09/16 DCM Added setting PurchasedProductPaymentScheduleParentID
-- 2017-08-29 Added consideration for policies cancelled in cooling off period
-- 2017-09-27 CPS added SetDocumentFields call.  Would rather not have a C433-specific call in a generic proc, but this is the last SQL that runs before the letters go out
-- 2018-02-19 GPR changed call for _C{}_SetDocumentFields to _C600_SetDocumentFields for C600
-- 2018-03-19 JEL Added consideration for client account table
-- 2020-08-20 ALM | Updated for US Cancellation Logic for PPET-110
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee 
-- 2021-01-20 ACE FURKIN-130 - Add Enrollment Fee
-- 2021-04-16 ACE FURKIN-499 fixed addition of the written off payment
-- =============================================
CREATE PROCEDURE [dbo].[Reinstatement__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CustomerID INT, 
			@LeadID INT, 
			@MatterID INT, 
			@LeadTypeID INT, 
			@WhoCreated INT, 
			@CountryID INT,
			@WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120), -- current date time for when created
			@StateCode VARCHAR(50),
			@AccountTypeID INT,
			@PaymentTypeLuli INT,
			@PaymentMethod INT,
			@TransactionFee NUMERIC(18,2),
			/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
			@EnrollmentFeeNet NUMERIC(18,2),
			@EnrollmentFeeTax NUMERIC(18,2),
			@EnrollmentFeeGross NUMERIC(18,2)

	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID, @LeadID = m.LeadID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID = @CaseID

	SELECT @CustomerID = l.CustomerID, 
			@LeadTypeID = l.LeadTypeID, 
			@CountryID = c.CountryID, 
			@StateCode = c.County
	FROM Lead l WITH (NOLOCK) 
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	WHERE l.LeadID = @LeadID
	
	SELECT @WhoCreated = le.WhoCreated 
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID

	--4514	Description
	DECLARE @Description_DV VARCHAR(2000)
	SELECT @Description_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4514)		

	--4515	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN
		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
		SELECT @AccountID=AccountID FROM PurchasedProduct WITH (NOLOCK) WHERE PurchasedProductID=@PurchasedProductID
	END

	--4516	PurchasedProductPaymentScehuleIDToReverse
	DECLARE @PurchasedProductPaymentScehuleIDToReverse_DV VARCHAR(2000), @IsInt_PurchasedProductPaymentScehuleIDToReverse BIT, @PurchasedProductPaymentScehuleIDToReverse INT
	SELECT @PurchasedProductPaymentScehuleIDToReverse_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4516)	
	SELECT @IsInt_PurchasedProductPaymentScehuleIDToReverse = dbo.fnIsInt(@PurchasedProductPaymentScehuleIDToReverse_DV)		
	IF(@IsInt_PurchasedProductPaymentScehuleIDToReverse=1)
	BEGIN
		SELECT @PurchasedProductPaymentScehuleIDToReverse = CAST(@PurchasedProductPaymentScehuleIDToReverse_DV AS INT)	
	END	


	EXEC  [dbo].[_C00_LogIt] 'Info', 'Reinstatement__CreateFromThirdPartyFields', '@PurchasedProductPaymentScehuleIDToReverse', @PurchasedProductPaymentScehuleIDToReverse, 58550

	
	--4530	AdjustmentValueOnly (0=No update billing records; 1=Yes just record adjustment value in defined DF)
	DECLARE @AdjustmentValueOnly_DV VARCHAR(2000), @IsInt_AdjustmentValueOnly BIT, @AdjustmentValueOnly INT
	SELECT @AdjustmentValueOnly_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4530)	
	SELECT @IsInt_AdjustmentValueOnly = dbo.fnIsInt(@AdjustmentValueOnly_DV)		
	IF(@IsInt_AdjustmentValueOnly=1)
	BEGIN
		SELECT @AdjustmentValueOnly = CAST(@AdjustmentValueOnly_DV AS INT)
	END
	SELECT @AdjustmentValueOnly=CASE WHEN @AdjustmentValueOnly IS NULL THEN 0 ELSE
									CASE WHEN @AdjustmentValueOnly NOT IN (1,0) THEN 0
										ELSE @AdjustmentValueOnly 
									END
								END		
	PRINT @AdjustmentValueOnly
								
	--4531	AdjustmentValueDFID
	DECLARE @AdjustmentValueDFID INT
	SELECT @AdjustmentValueDFID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 105, 4531)

	--4533	TermAdjustmentValueDFID
	DECLARE @TermAdjustmentValueDFID INT
	SELECT @TermAdjustmentValueDFID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 105, 4533)

	--4534	FirstMonthTotalDFID
	DECLARE @FirstMonthTotalDFID INT
	SELECT @FirstMonthTotalDFID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 105, 4534)
	
	--4401	AdjustmentDate	
	DECLARE @AdjustmentDate_DV VARCHAR(2000), @IsDateTime_AdjustmentDate BIT, @AdjustmentDate DATETIME
	SELECT @AdjustmentDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4401)		
	SELECT @IsDateTime_AdjustmentDate = dbo.fnIsDateTime(@AdjustmentDate_DV)		
	IF(@IsDateTime_AdjustmentDate=1)
	BEGIN
		SELECT @AdjustmentDate = CAST(@AdjustmentDate_DV AS DATETIME)
	END
	
	/*Pick up the clientAccount Record to use*/ 
	DECLARE @ClientAccountID INT 

	SELECT @ClientAccountID = p.ClientAccountID 
	FROM PurchasedProduct p WITH (NOLOCK)
	WHERE p.PurchasedProductID = @PurchasedProductID
	
	PRINT @AdjustmentDate
	
	DECLARE 
			@CancellationCoverPeriodGross NUMERIC(18,2),
			@CoverFrom DATETIME,
			@CoverTo DATETIME,
			@CustomerPaymentScheduleID INT,
			@DifferenceInMonthlyPremium NUMERIC(18,2),
			@DifferenceInMonthlyPremiumVAT NUMERIC(18,2),
			@FromDate DATE,
			@ID INT,
			@InsertNewCPSRow INT = 0,
			@NewCoverFrom DATETIME,
			@NewCoverTo DATETIME,
			@NewPaymentDate DATE,
			@NumberOfDaysInPeriod INT,
			@NumberOfDaysLeftInPeriod INT,
			@OneOffPaymentForCancellationPeriod NUMERIC(18,2),
			@OneOffPaymentForCancellationPeriodVAT NUMERIC(18,2),
			@OneOffPaymentGross NUMERIC(18,2),
			@OneOffPaymentNet NUMERIC(18,2),
			@OneOffPaymentVAT NUMERIC(18,2),
			@PaymentForRemainderOfSchedule NUMERIC(18,2),
			@PaymentGross MONEY,
			@PaymentNet MONEY,
			@PaymentNetForRemainderOfSchedule NUMERIC(18,2),
			@PaymentVATForRemainderOfSchedule NUMERIC(18,2),
			@PaymentVAT MONEY,
			@PurchasedProductPaymentScheduleID INT,
			@WithinAdjustmentDatePaymentVAT NUMERIC(18,2),
			@CancellationReason INT,
			@StartDate DATE,
			@CancellationType INT,
			@DaysIntoPolicyYear	INT, 
			@IsRenewal BIT, 
			@CoolingOffPeriod INT
						
	DECLARE @NewSchedule TABLE
	(
		ID INT,	
		CoverFrom DATETIME, 
		CoverTo DATETIME, 
		AccountID INT, 
		PaymentNet NUMERIC(18,2),
		PaymentVAT NUMERIC(18,2),
		PaymentGross NUMERIC(18,2),
		PaymentDate DATE		
	)

	
	/* Procedure 
	
		1. Create a payment schedule row for the refund reversal (if one is specified in the third party fields)
			and for all missed payments
		2. Undo all ignored past rows in the purchased product payment schedule & assign these to the one-off payment
		3. Undo all ignored future rows and
		4. Update the CPS for future payments (turn off ignore or add to existing new entries, as appropriate) 
			
	*/
							
	-- get payment schedule data for refund to be reversed
	
	-- there was neither a refund nor an additional payment ( additional payments are possible where a cancellation is done in the future )
	IF @PurchasedProductPaymentScehuleIDToReverse = 0
	BEGIN

		SELECT @OneOffPaymentGross = 0, 
			   @OneOffPaymentVAT = 0,
			   @OneOffPaymentNET = 0
		
	END	
	-- there was an additional payment
	ELSE IF 0 < (
		SELECT PaymentGross 
		FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
		WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScehuleIDToReverse
		AND PaymentGross > 0 
	)
	BEGIN
	
		SELECT @OneOffPaymentGross = ppps2.PaymentGross - ppps1.PaymentGross,
				@OneOffPaymentVAT = ppps2.PaymentVAT - ppps1.PaymentVAT,
				@NewCoverFrom = DATEADD(DAY, 1, ppps2.CoverFrom),
				@NewCoverTo = ppps2.CoverTo
		FROM PurchasedProductPaymentSchedule ppps1 WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) ON ppps1.PurchasedProductID = ppps2.PurchasedProductID 
				AND ppps2.PaymentStatusID = 3
				AND ppps1.CoverFrom = ppps2.CoverFrom
		WHERE ppps1.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScehuleIDToReverse
		
		SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT
	
	END
	-- there was a refund
	ELSE
	BEGIN

		SELECT @OneOffPaymentGross = ISNULL(-PaymentGross,0), 
			   @OneOffPaymentVAT = ISNULL(-PaymentVAT,0),
			   @OneOffPaymentNET = ISNULL(-PaymentNet,0),
			   @NewCoverFrom = CoverFrom,
			   @NewCoverTo = CoverTo 
		FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
		WHERE PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScehuleIDToReverse
	
	END

	PRint 'yup'

	/*ALM/GPR 2021-04-11 for FURKIN-425 | Reverse 'Written Off' and 'Failed - Written Off'*/
	INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID)
	SELECT p.PurchasedProductPaymentScheduleID, 
		   p.PaymentGross - p.PaymentVAT, 
		   p.PaymentVAT, 
		   p.PaymentGross,
		   p.CoverFrom, 
		   p.CoverTo, 
		   p.AccountID
	FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
	LEFT JOIN PurchasedProductPaymentSchedule pp WITH (NOLOCK) ON pp.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScehuleIDToReverse 
	WHERE p.PaymentDate <= @AdjustmentDate
	AND p.PurchasedProductID = @PurchasedProductID 
	AND p.PaymentStatusID IN (10,11) -- Written Off / Failed - Written Off 
	--AND p.PurchasedProductPaymentScheduleID <> @PurchasedProductPaymentScehuleIDToReverse -- Not Equal to Standard Paid Cancellation Adjustment /*GPR 2021-04-26 removed for FURKIN-499*/
	AND (
			p.CoverFrom = pp.CoverFrom 
			OR 
			ISNULL(pp.PurchasedProductPaymentScheduleID,0) = 0
		) 

	-- add the payments for the collections that should already have been made but exclude the payment from the period we have split up at cancellation
	INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID)
	SELECT p.PurchasedProductPaymentScheduleID, 
		   p.PaymentGross - p.PaymentVAT, 
		   p.PaymentVAT, 
		   p.PaymentGross,
		   p.CoverFrom, 
		   p.CoverTo, 
		   p.AccountID
	FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
	LEFT JOIN PurchasedProductPaymentSchedule pp WITH (NOLOCK) ON pp.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScehuleIDToReverse 
	WHERE p.PaymentDate <= @AdjustmentDate--PaymentDate>@CancellationDate 
	AND p.PurchasedProductID = @PurchasedProductID 
	AND p.PaymentStatusID = 3 
	AND p.PurchasedProductPaymentScheduleID <> @PurchasedProductPaymentScehuleIDToReverse
	AND (
			p.CoverFrom <> pp.CoverFrom 
			OR 
			ISNULL(pp.PurchasedProductPaymentScheduleID,0) = 0
		) 

	SELECT @PaymentForRemainderOfSchedule = SUM(PaymentGross) 
	FROM @NewSchedule

	SELECT @PaymentNetForRemainderOfSchedule = SUM(PaymentNet) 
	FROM @NewSchedule

	SELECT @PaymentVATForRemainderOfSchedule = SUM(PaymentVAT) 
	FROM @NewSchedule

	SELECT @OneOffPaymentGross = @OneOffPaymentGross + ISNULL(@PaymentForRemainderOfSchedule,0)
	SELECT @OneOffPaymentVAT = @OneOffPaymentVAT + ISNULL(@PaymentVATForRemainderOfSchedule,0)
	SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT

	-- The one-off adjustment is shown as covering the period from the earliest From date and the latest To date in the payments contributing to it 
	SELECT @NewCoverFrom = CASE WHEN @NewCoverFrom <= ISNULL(MIN(CoverFrom),@NewCoverFrom) THEN @NewCoverFrom ELSE MIN(CoverFrom) END FROM @NewSchedule
	SELECT @NewCoverTo = CASE WHEN @NewCoverTo >= ISNULL(MAX(CoverTo),@NewCoverTo) THEN @NewCoverTo  ELSE MAX(CoverTo) END FROM @NewSchedule

	SELECT @AccountID=AccountID 
	FROM PurchasedProduct WITH (NOLOCK) 
	WHERE PurchasedProductID = @PurchasedProductID
	
	IF ISNULL(@NewCoverTo,'') = ''
	BEGIN 
		
		/*2020-08-20 ALM PPET-110*/
		IF @CountryID = 233 /*United States*/
		BEGIN 

			SELECT @CancellationReason = dbo.fnGetSimpleDvAsInt(170054,@MatterID),
					@StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)

			SELECT @CancellationType = 
			CASE 
				WHEN @CancellationReason IN (69907,69911,72031,72032,72033,72035,72037,72038,74421,74519,74521,74523,74525,75135,75136,75141) THEN 1 
				ELSE 2 
			END

			SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())

			SELECT @IsRenewal = 
				CASE 
					WHEN COUNT (*) > 1 THEN 1 
					ELSE 0 
				END 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID

			SELECT @CoolingOffPeriod = [dbo].[fn_C00_USCancellationCoolingOffPeriod] (@StateCode, @CancellationType, @IsRenewal, @DaysIntoPolicyYear)
		
			/*If the policy was cancelled in the cooling off period, we need to make sure we reinstate from inception*/
			SELECT @NewCoverTo =  CASE WHEN DATEADD(DAY,@CoolingOffPeriod,p.ProductPurchasedOnDate) >= @AdjustmentDate THEN p.ValidFrom ELSE @AdjustmentDate END
			FROM PurchasedProduct p WITH (NOLOCK) 
			WHERE p.purchasedProductID = @PurchasedProductID

		END
		ELSE 
		BEGIN

			/*If the policy was cancelled in the cooling off period, we need to make sure we reinstate from inception*/
			SELECT @NewCoverTo =  CASE WHEN DATEADD(DAY,bc.CoolingOffPeriod,p.ProductPurchasedOnDate) >= @AdjustmentDate THEN p.ValidFrom ELSE @AdjustmentDate END
			FROM PurchasedProduct p WITH (NOLOCK) 
			INNER JOIN BillingConfiguration bc WITH (NOLOCK) on bc.ClientID = p.ClientID  
			WHERE p.purchasedProductID = @PurchasedProductID

		END

	END 
	
	PRint '@out'

	-- Find all future PPPS records 
	DELETE FROM @NewSchedule
	
	INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID, PaymentDate)
	SELECT ppps.PurchasedProductPaymentScheduleID, 
		   ppps.PaymentGross - ppps.PaymentVAT, 
		   ppps.PaymentVAT, 
		   ppps.PaymentGross,
		   ppps.CoverFrom, 
		   ppps.CoverTo, 
		   0,
		   ppps.PaymentDate
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	WHERE ppps.CoverFrom >= CAST(@NewCoverTo AS DATE)
	AND ppps.PurchasedProductID = @PurchasedProductID 
	AND ppps.PaymentStatusID = 3 /*Scheduled Retry*/
	
	PRINT @NewCoverTo

	SELECT top 1 @NewPaymentDate = w.[Date]  
	FROM WorkingDays w WITH (NOLOCK) 
	INNER JOIN BillingConfiguration b on b.ClientID = @ClientID 
	Where w.Date >= CONVERT(DATE,DATEADD(DAY,b.OneOffAdjustmentWait,dbo.fn_GetDate_Local())) 
			
	/*CPS 2017-09-27 on request of JEL*/
	SELECT @NewPaymentDate = dbo.fnGetNextWorkingDate(@NewPaymentDate,0)

	SET @FromDate = @NewPaymentDate

	IF @AdjustmentValueOnly=0 
	BEGIN
	
		/*ALM/GPR 2020-11-26*/
		SELECT @TransactionFee = 0.00 /*Transaction Fee not applied for MTAs*/

		IF ISNULL(@OneOffPaymentNET,0) <> 0 
		AND ISNULL(@OneOffPaymentGross,0) <> 0 
		AND ISNULL(@OneOffPaymentVAT,0) <> 0
		BEGIN

			/*ALM/GPR 2020-11-26 removed below block as TransactionFee not applied to Adjustment values - for SDPRU-162 and SDPRU-165*/
			--SELECT @AccountTypeID = a.AccountTypeID
			--FROM Account a WITH (NOLOCK) 
			--WHERE a.AccountID = @AccountID

			--SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
			--		THEN 69930 /*Direct Debit*/
			--		ELSE 69931 /*Credit Card*/
			--		END

			--SELECT @PaymentMethod = 69943 /*Monthly*/

			--SELECT @TransactionFee = ISNULL(dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod), 0.00)

			/*If we have yet to pay then don't*/
			IF EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
					   WHERE p.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScehuleIDToReverse 
					   AND p.CustomerLedgerID IS NULL 
					   AND p.PaymentStatusID = 1
					   AND p.PaymentGross = (@OneOffPaymentGross * -1)) 
			BEGIN 
				
				/*Set the cancellation adjustment to ignore*/ 
				UPDATE PurchasedProductPaymentSchedule 
				SET PaymentStatusID = 3 
				WHere PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScehuleIDToReverse 

				-- Insert the one-off adjustment record into PPPS with a status of ignore 
				INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
				VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @NewPaymentDate, @NewCoverFrom, @NewCoverTo, @NewPaymentDate, @OneOffPaymentNET, @OneOffPaymentVAT, @OneOffPaymentGross + @TransactionFee, 3, NULL, NULL, NULL, @WhoCreated, @WhenCreated, 8, @ClientAccountID, @TransactionFee)

				SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
						
				UPDATE PurchasedProductPaymentSchedule
				SET PurchasedProductPaymentScheduleParentID = @PurchasedProductPaymentScheduleID
				WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
			
			END
			ELSE
			BEGIN		   
			
				-- Insert the one-off adjustment record into PPPS with the payment date of the first future record 
				INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID,ClientAccountID, TransactionFee)
				VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, @NewPaymentDate, @NewCoverFrom, @NewCoverTo, @NewPaymentDate, @OneOffPaymentNET, @OneOffPaymentVAT, @OneOffPaymentGross + @TransactionFee, 1, NULL, NULL, NULL, @WhoCreated, @WhenCreated, 8,@ClientAccountID, @TransactionFee)

				SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
						
				UPDATE PurchasedProductPaymentSchedule
				SET PurchasedProductPaymentScheduleParentID = @PurchasedProductPaymentScheduleID
				WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
			
			END 		
		END
		
		/*
			ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee
			Enrollment fee
			If we have a negative enrollment fee thats new then we can remove it.
			If we have a total of 0 and there are enrollment fees then we need to re-add it
		*/
		IF EXISTS (
			SELECT *
			FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
			WHERE s.PurchasedProductID = @PurchasedProductID
			AND s.PaymentStatusID = 1 /*New*/
			AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
			AND s.PaymentGross < 0.00
		)
		BEGIN

			/*We have a refund enrollment fee. As we are reinstating we should remove that.*/
			DELETE s
			FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
			WHERE s.PurchasedProductID = @PurchasedProductID
			AND s.PaymentStatusID = 1 /*New*/
			AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
			AND s.PaymentGross < 0.00

		END

		IF EXISTS (
			SELECT *
			FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
			WHERE s.PurchasedProductID = @PurchasedProductID
			AND s.PaymentStatusID = 6 /*Paid*/
			AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
			AND s.PaymentGross > 0.00
		)
		AND (
			SELECT SUM(s.PaymentGross)
			FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
			WHERE s.PurchasedProductID = @PurchasedProductID
			AND s.PaymentStatusID = 6 /*Paid*/
			AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
		) = 0.00
		BEGIN

			/*
				ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee
				We have a paid admin fee but when summing the fee up we get 0.
				This means we have cancelled and refunded and both the incoming payment and refund has been paid.
				In this instance we need to add a new payment request for the enrollment fee
			*/
			SELECT @EnrollmentFeeNet = dbo.fnGetSimpleDvAsMoney(315878, @MatterID),
					@EnrollmentFeeTax = dbo.fnGetSimpleDvAsMoney(315880, @MatterID),
					@EnrollmentFeeGross = dbo.fnGetSimpleDvAsMoney(315881, @MatterID)

			INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, 
					PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, 
					WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
			VALUES (@ClientID, @CustomerID, @AccountID, @PurchasedProductID, @NewPaymentDate, @NewCoverFrom, @NewCoverTo, @NewPaymentDate, 
					@EnrollmentFeeNet, @EnrollmentFeeTax, @EnrollmentFeeGross, 1 /*New*/, NULL, NULL, NULL, 
					@WhoCreated, @WhenCreated, 5 /*Admin Fee*/, @ClientAccountID, 0.00
					)

			SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
						
			UPDATE PurchasedProductPaymentSchedule
			SET PurchasedProductPaymentScheduleParentID = @PurchasedProductPaymentScheduleID
			WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		END
		/*End add enrollment fee*/

		-- update ppps rows to remove ignore
		UPDATE ppps
		SET PaymentStatusID = 1
		FROM PurchasedProductPaymentSchedule ppps
		INNER JOIN @NewSchedule ns ON ns.ID = ppps.PurchasedProductPaymentScheduleID
		
		-- update the payment day of the earliest scheduled payment if earlier than the new payment day
		UPDATE PurchasedProductPaymentSchedule 
		SET ActualCollectionDate = @NewPaymentDate
		FROM PurchasedProductPaymentSchedule
		WHERE PurchasedProductID = @PurchasedProductID
		AND PaymentStatusID = 1
		AND ActualCollectionDate < @NewPaymentDate
		
		-- Make sure we are dealing with the admin fee, this will do any admin fees for this product
		UPDATE ppps
		SET PaymentStatusID = 1 
		FROM PurchasedProductPaymentSchedule ppps
		Where ppps.PaymentStatusID = 3 
		AND ppps.PurchasedProductPaymentScheduleTypeID = 5
		AND ppps.PurchasedProductID = @PurchasedProductID
		
		-- Rebuild cps
		EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @FromDate, @WhoCreated	
		
	END
	ELSE
	BEGIN
	
		-- adjustment amount for cover period between cancellation and now
		EXEC _C00_SimpleValueIntoField @AdjustmentValueDFID,@OneOffPaymentGross,@MatterID 

		EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@OneOffPaymentGross,@MatterID 
	
	END
	
	-- 2020-02-13 CPS for JIRA AAG-30 | Update adjustment amounts
	EXEC WrittenPremium_UpdateAdjustment @ClientID, @CaseID, @LeadEventID

	EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'Reinstatement' /*CPS 2017-09-27*/ /*GPR 2018-02-19 set to 600*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Reinstatement__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Reinstatement__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Reinstatement__CreateFromThirdPartyFields] TO [sp_executeall]
GO
