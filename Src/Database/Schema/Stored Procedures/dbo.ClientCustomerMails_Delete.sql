SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientCustomerMails table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientCustomerMails_Delete]
(

	@ClientCustomerMailID int   
)
AS


				DELETE FROM [dbo].[ClientCustomerMails] WITH (ROWLOCK) 
				WHERE
					[ClientCustomerMailID] = @ClientCustomerMailID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientCustomerMails_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientCustomerMails_Delete] TO [sp_executeall]
GO
