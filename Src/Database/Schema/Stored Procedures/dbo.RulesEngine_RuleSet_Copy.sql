SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jan Wilson
-- Create date: 2014-05-27
-- Description: Copies a rule set
-- Updated:		Jan Wilson
--				2014-06-06
--				Updated to lookup the new rule id for the RuleOutput
--              table (code not completed yet).
--				2014-12-05	SB	Fixed the rule output copying when a [!P: param is used
-- 2016-10-06 DCM Added checkpoint column into rule copy
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSet_Copy]
(
	@ClientID		INT,
	@RuleSetID		INT,
	@RequestedByID	INT
)

AS
BEGIN
	
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION

		-- Insert Rule Set
		INSERT INTO RulesEngine_RuleSets(Name, InputTypeID, OutputTypeID, ClientID, WhenCreated, WhoCreated, WhenModified,
			WhoModified, Description, SourceID, ExposeForUse, RuleSetTypeID, ChangeSetID)
		SELECT
			'Copy of ' + RS.Name, RS.InputTypeID, RS.OutputTypeID, RS.ClientID, dbo.fn_GetDate_Local(), @RequestedByID, dbo.fn_GetDate_Local(),
			@RequestedByID, RS.Description, RS.RuleSetID, RS.ExposeForUse, RS.RuleSetTypeID, RS.ChangeSetID
		FROM
			RulesEngine_RuleSets RS
		WHERE
			RuleSetID = @RuleSetId
			AND ClientID = @ClientID
			
		IF @@ROWCOUNT != 1 
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		
		-- Insert Rules
		INSERT INTO RulesEngine_Rules(Name, RuleSetID, RuleOrder, ClientID, WhenCreated, WhoCreated, WhenModified, 
			WhoModified, Description, 
			SourceID,RuleCheckpoint)
		SELECT
			R.Name, RS.RuleSetID, R.RuleOrder, R.ClientID, dbo.fn_GetDate_Local(), @RequestedByID, dbo.fn_GetDate_Local(),
			@RequestedByID, R.Description, 
			R.RuleID, -- Store the original rule id (used later)
			r.RuleCheckpoint
		FROM 
			RulesEngine_Rules R
			INNER JOIN RulesEngine_RuleSets RS
			ON R.RuleSetID = RS.SourceID
			
		-- Insert Rule Parameters
		INSERT INTO RulesEngine_RuleParameters(Name, RuleID, ParameterTypeID, Value, DataTypeID, 
			ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, Parent, 
			SourceID)
		SELECT 
			RP.Name, R.RuleID, RP.ParameterTypeID, RP.Value, RP.DataTypeID, 
			RP.ClientID, dbo.fn_GetDate_Local(), @RequestedByID, dbo.fn_GetDate_Local(), @RequestedByID, RP.Parent, 
			RP.RuleParameterID -- Store the original rule parameter id (used later)
		FROM
			RulesEngine_RuleParameters RP
			INNER JOIN RulesEngine_Rules R
			ON RP.RuleID = R.SourceId

		-- Insert Parameter Options
		INSERT INTO RulesEngine_ParameterOptions(RuleParameterID, OperatorID, Val1, Val2, OptionOrder, ClientID, 
			WhenCreated, WhoCreated, WhenModified, WhoModified, 
			SourceId)
		SELECT
			RP.RuleParameterID, RPO.OperatorID, RPO.Val1, RPO.Val2, RPO.OptionOrder, RPO.ClientID, 
			dbo.fn_GetDate_Local(), @RequestedByID, dbo.fn_GetDate_Local(), @RequestedByID, 
			RPO.ParameterOptionID -- Store the original parameter option id (used later)
		FROM
			dbo.RulesEngine_ParameterOptions RPO
			INNER JOIN dbo.RulesEngine_RuleParameters RP
			ON RPO.RuleParameterID = RP.SourceID

		-- Insert Rule Outputs
		INSERT INTO RulesEngine_RuleOutputs(RuleID, Value, TransformID, SourceID)
		SELECT
			R.RuleID, 
			-- Need to work out how to set any [!P: values with the new reference value
			CASE 
				WHEN SUBSTRING(RO.Value,1,4) = '[!P:'
					AND EXISTS (
						SELECT * FROM RulesEngine_RuleParameters 
						WHERE CAST(SourceID AS VARCHAR(MAX)) = dbo.fnGetCharsBeforeOrAfterSeparator(dbo.fnGetCharsBeforeOrAfterSeparator(RO.Value, ':', 0, 0), ']', 0, 1)
						)
					THEN '[!P:' + (
						SELECT
							CAST(RuleParameterID AS VARCHAR(50)) FROM RulesEngine_RuleParameters 
						WHERE
							CAST(SourceID AS VARCHAR(MAX)) = dbo.fnGetCharsBeforeOrAfterSeparator(dbo.fnGetCharsBeforeOrAfterSeparator(RO.Value, ':', 0, 0), ']', 0, 1))
							+ ']'
				ELSE
					RO.Value 
			END Value,
			RO.TransformID,
			RO.RuleOutputID
		FROM
			RulesEngine_RuleOutputs RO
			INNER JOIN RulesEngine_Rules R
			ON RO.RuleID = R.SourceID

				
		--UPDATE targetRO 
		--SET Value = '[!P:' + CAST(targetRule.RuleID AS VARCHAR) + ']'  
		--FROM dbo.RulesEngine_RuleOutputs targetRO 
		--INNER JOIN dbo.RulesEngine_RuleOutputs sourceRO ON targetRO.SourceID = sourceRO.RuleOutputID 
		--INNER JOIN dbo.RulesEngine_Rules targetRule ON targetRule.SourceID = dbo.fnGetCharsBeforeOrAfterSeparator(dbo.fnGetCharsBeforeOrAfterSeparator(sourceRO.Value, ':', 0, 0), ']', 0, 1) /* from "[!P:345]" extract just the "345" part which is the old RuleID */ 
		--WHERE targetRO.SourceID IS NOT NULL
		
		-- Insert Output Coordinates
		INSERT INTO RulesEngine_OutputCoordinates(RuleOutputID, ParameterOptionID)
		SELECT
			RO.RuleOutputID, RPO.ParameterOptionID
		FROM
			RulesEngine_OutputCoordinates OC
			INNER JOIN RulesEngine_RuleOutputs RO
			ON OC.RuleOutputID = RO.SourceID
			
			INNER JOIN RulesEngine_ParameterOptions RPO
			ON OC.ParameterOptionID = RPO.SourceID

		-- Now clear out the SourceID values 
		UPDATE RulesEngine_RuleSets SET SourceID = NULL
		UPDATE RulesEngine_Rules SET SourceID = NULL
		UPDATE RulesEngine_RuleOutputs SET SourceID = NULL
		UPDATE RulesEngine_RuleParameters SET SourceID = NULL
		UPDATE RulesEngine_ParameterOptions SET SourceID = NULL

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		
		DECLARE @Error VARCHAR(2000)
		SELECT @Error = ERROR_MESSAGE()
		
		EXEC dbo._C00_LogIt 'Error', @ClientID, 'RulesEngine_RuleSet_Copy', @Error, @RuleSetID
			
	END CATCH
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSet_Copy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSet_Copy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSet_Copy] TO [sp_executeall]
GO
