SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserRightsDynamicEditing table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamicEditing_GetByUserRightsDynamicEditingID]
(

	@UserRightsDynamicEditingID int   
)
AS


				SELECT
					[UserRightsDynamicEditingID],
					[ClientPersonnelID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[UserRightsDynamicEditing] WITH (NOLOCK) 
				WHERE
										[UserRightsDynamicEditingID] = @UserRightsDynamicEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_GetByUserRightsDynamicEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamicEditing_GetByUserRightsDynamicEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_GetByUserRightsDynamicEditingID] TO [sp_executeall]
GO
