SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DisplayPosition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DisplayPosition_Update]
(

	@DisplayPositionNum smallint   ,

	@OriginalDisplayPositionNum smallint   ,

	@DisplayPositionName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DisplayPosition]
				SET
					[DisplayPositionNum] = @DisplayPositionNum
					,[DisplayPositionName] = @DisplayPositionName
				WHERE
[DisplayPositionNum] = @OriginalDisplayPositionNum 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DisplayPosition_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Update] TO [sp_executeall]
GO
