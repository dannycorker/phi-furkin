SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadViewHistory table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_GetByClientPersonnelIDWhenViewed]
(

	@ClientPersonnelID int   ,

	@WhenViewed datetime   
)
AS


				SELECT
					[LeadViewHistoryID],
					[ClientPersonnelID],
					[ClientID],
					[LeadID],
					[WhenViewed]
				FROM
					[dbo].[LeadViewHistory] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelID] = @ClientPersonnelID
					AND [WhenViewed] = @WhenViewed
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByClientPersonnelIDWhenViewed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_GetByClientPersonnelIDWhenViewed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByClientPersonnelIDWhenViewed] TO [sp_executeall]
GO
