SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-04-03
-- Description:	Adds a new row for either user deductions or multi payee
-- Used by:		df_157745
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Docs_AddSettlementTextRow] 
(
	@MatterID INT,
	@Type INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @DetailFieldID INT,
			@DetailFieldPageID INT,
			@TableRowID INT,
			@TableDetaiilValueID INT,
			@ClientID INT
			
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
			
	IF @Type = 1 -- User Deductions
	BEGIN
		SELECT @DetailFieldID = 148349, @DetailFieldPageID = 16188
	END
	ELSE -- Multi payee
	BEGIN
		SELECT @DetailFieldID = 152765, @DetailFieldPageID = 16188 /*Settlement letter multi payee breakdown*/
	END
	
	INSERT dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID)
	VALUES (@ClientID, @MatterID, @DetailFieldID, @DetailFieldPageID)
	
	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT dbo.TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	VALUES (@ClientID, @MatterID, @TableRowID, 148348, NULL) /*Text*/

	SELECT @TableRowID AS TableRowID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_AddSettlementTextRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Docs_AddSettlementTextRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_AddSettlementTextRow] TO [sp_executeall]
GO
