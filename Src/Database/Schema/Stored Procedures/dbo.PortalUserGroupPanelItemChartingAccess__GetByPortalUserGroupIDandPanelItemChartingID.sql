SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:	Chris Townsend
-- Date:		19/07/2010
-- Purpose:		Select a record by Portal User Group ID and Panel Item Charting ID
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupPanelItemChartingAccess__GetByPortalUserGroupIDandPanelItemChartingID]
(
	@PortalUserGroupID int,
	@PanelItemChartingID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalUserGroupPanelItemChartingAccessID],
					[ClientID],
					[PortalUserGroupID],
					[PanelItemChartingID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupPanelItemChartingAccess]
				WHERE
					[PortalUserGroupID] = @PortalUserGroupID
				AND
					[PanelItemChartingID] = @PanelItemChartingID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess__GetByPortalUserGroupIDandPanelItemChartingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupPanelItemChartingAccess__GetByPortalUserGroupIDandPanelItemChartingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupPanelItemChartingAccess__GetByPortalUserGroupIDandPanelItemChartingID] TO [sp_executeall]
GO
