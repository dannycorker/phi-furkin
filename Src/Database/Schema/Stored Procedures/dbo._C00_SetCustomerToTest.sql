SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 14-May-2009
-- Description:	Proc to Change customers to and from test status
-- =============================================

CREATE PROCEDURE [dbo].[_C00_SetCustomerToTest]
(
	@LeadEventID int,
	@SetTestTo bit = 1
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@CustomerID int,
			@ClientPersonnelID int

	SELECT @CustomerID = l.CustomerID, @ClientPersonnelID = LeadEvent.WhoCreated
	FROM LeadEvent
	Inner Join Lead l on l.leadid = leadevent.leadid
	WHERE leadevent.leadeventid = @LeadEventID 

	Update Customers 
	Set Test = @SetTestTo
	Where CustomerID = @CustomerID

	Update Lead
	Set Assigned = 0, AssignedTo = NULL, AssignedBy = NULL, AssignedDate = NULL
	Where CustomerID = @CustomerID
	
	INSERT INTO WorkflowTaskCompleted (ClientID, 
	WorkflowTaskID,
	WorkflowGroupID,
	AutomatedTaskID,
	Priority,
	AssignedTo,
	AssignedDate,
	LeadID,
	CaseID,
	EventTypeID,
	FollowUp,
	Important,
	CreationDate,
	Escalated,
	EscalatedBy,
	EscalationReason,
	EscalationDate,
	Disabled,
	CompletedBy,
	CompletedOn,
	CompletionDescription
	)
	SELECT w.ClientID, 
		w.WorkflowTaskID, 
		w.WorkflowGroupID, 
		w.AutomatedTaskID, 
		w.Priority, 
		w.AssignedTo, 
		w.AssignedDate, 
		w.LeadID, 
		w.CaseID, 
		w.EventTypeID, 
		w.FollowUp, 
		w.Important, 
		w.CreationDate, 
		w.Escalated, 
		w.EscalatedBy, 
		w.EscalationReason, 
		w.EscalationDate, 
		w.Disabled, 
		@ClientPersonnelID, 
		dbo.fn_GetDate_Local(), 
		'Customer Set to Test' 
	FROM WorkflowTask w WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = w.LeadID
	INNER JOIN Customers c WITH (NOLOCK) on c.CustomerID = l.CustomerID
	WHERE c.CustomerID = @CustomerID

	DELETE WorkflowTask 
	FROM WorkflowTask w WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = w.LeadID
	INNER JOIN Customers c WITH (NOLOCK) on c.CustomerID = l.CustomerID
	WHERE c.CustomerID = @CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetCustomerToTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SetCustomerToTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetCustomerToTest] TO [sp_executeall]
GO
