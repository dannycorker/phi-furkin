SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CustomerOffice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_Update]
(

	@OfficeID int   ,

	@ClientID int   ,

	@CustomerID int   ,

	@OfficeName varchar (100)  ,

	@MainPhoneNumber varchar (100)  ,

	@FaxNumber varchar (100)  ,

	@Address1 varchar (100)  ,

	@Address2 varchar (100)  ,

	@Town varchar (100)  ,

	@County varchar (100)  ,

	@Postcode varchar (100)  ,

	@Country varchar (100)  ,

	@Notes varchar (255)  ,

	@CountryID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CustomerOffice]
				SET
					[ClientID] = @ClientID
					,[CustomerID] = @CustomerID
					,[OfficeName] = @OfficeName
					,[MainPhoneNumber] = @MainPhoneNumber
					,[FaxNumber] = @FaxNumber
					,[Address1] = @Address1
					,[Address2] = @Address2
					,[Town] = @Town
					,[County] = @County
					,[Postcode] = @Postcode
					,[Country] = @Country
					,[Notes] = @Notes
					,[CountryID] = @CountryID
				WHERE
[OfficeID] = @OfficeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Update] TO [sp_executeall]
GO
