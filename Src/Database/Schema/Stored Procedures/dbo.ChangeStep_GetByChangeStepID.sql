SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeStep table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStep_GetByChangeStepID]
(

	@ChangeStepID int   
)
AS


				SELECT
					[ChangeStepID],
					[ClientID],
					[ChangeInstanceID],
					[StepTypeID],
					[ChangeDocumentID],
					[Comment],
					[ExternalUrl],
					[ChangeStepStateID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ChangeStep] WITH (NOLOCK) 
				WHERE
										[ChangeStepID] = @ChangeStepID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_GetByChangeStepID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStep_GetByChangeStepID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_GetByChangeStepID] TO [sp_executeall]
GO
