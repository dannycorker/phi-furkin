SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-14
-- Description:	Get all EventTypeFieldCompletion records for this EventType
--              with field name included
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeFieldCompletion__GetDSByEventTypeID]
(
	@EventTypeID int   
)
AS
BEGIN

	SELECT etfc.EventTypeFieldCompletionID,
	etfc.EventTypeID,
	etfc.DetailFieldID,
	etfc.InsertOptionID,
	etfc.DeleteOptionID,
	dfp.PageName + ': ' + df.FieldCaption AS [FieldName], 
	aqi.AquariumOptionName as [InsertOptionName],
	aqd.AquariumOptionName as [DeleteOptionName]
	FROM [dbo].[EventTypeFieldCompletion] etfc 
	INNER JOIN [dbo].[DetailFields] df ON df.DetailFieldID = etfc.DetailFieldID 
	INNER JOIN [dbo].[DetailFieldPages] dfp ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	INNER JOIN [dbo].[AquariumOption] aqi ON aqi.AquariumOptionID = etfc.InsertOptionID
	INNER JOIN [dbo].[AquariumOption] aqd ON aqd.AquariumOptionID = etfc.DeleteOptionID
	WHERE etfc.EventTypeID = @EventTypeID 
	ORDER BY dfp.PageName, df.FieldCaption
		
END			







GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion__GetDSByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion__GetDSByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion__GetDSByEventTypeID] TO [sp_executeall]
GO
