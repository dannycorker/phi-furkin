SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the BillItem table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_Find]
(

	@SearchUsingOR bit   = null ,

	@BillItemID int   = null ,

	@ClientID int   = null ,

	@BillID int   = null ,

	@Description varchar (2000)  = null ,

	@ClientPersonnelID int   = null ,

	@WorkUnits int   = null ,

	@Total decimal (18, 2)  = null ,

	@ChargeOutRate int   = null ,

	@TotalIsCustomValue bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [BillItemID]
	, [ClientID]
	, [BillID]
	, [Description]
	, [ClientPersonnelID]
	, [WorkUnits]
	, [Total]
	, [ChargeOutRate]
	, [TotalIsCustomValue]
    FROM
	[dbo].[BillItem] WITH (NOLOCK) 
    WHERE 
	 ([BillItemID] = @BillItemID OR @BillItemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([BillID] = @BillID OR @BillID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([WorkUnits] = @WorkUnits OR @WorkUnits IS NULL)
	AND ([Total] = @Total OR @Total IS NULL)
	AND ([ChargeOutRate] = @ChargeOutRate OR @ChargeOutRate IS NULL)
	AND ([TotalIsCustomValue] = @TotalIsCustomValue OR @TotalIsCustomValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [BillItemID]
	, [ClientID]
	, [BillID]
	, [Description]
	, [ClientPersonnelID]
	, [WorkUnits]
	, [Total]
	, [ChargeOutRate]
	, [TotalIsCustomValue]
    FROM
	[dbo].[BillItem] WITH (NOLOCK) 
    WHERE 
	 ([BillItemID] = @BillItemID AND @BillItemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([BillID] = @BillID AND @BillID is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([WorkUnits] = @WorkUnits AND @WorkUnits is not null)
	OR ([Total] = @Total AND @Total is not null)
	OR ([ChargeOutRate] = @ChargeOutRate AND @ChargeOutRate is not null)
	OR ([TotalIsCustomValue] = @TotalIsCustomValue AND @TotalIsCustomValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Find] TO [sp_executeall]
GO
