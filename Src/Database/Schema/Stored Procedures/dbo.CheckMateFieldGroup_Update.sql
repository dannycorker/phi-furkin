SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CheckMateFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldGroup_Update]
(

	@CheckMateFieldGroupID int   ,

	@GroupName varchar (50)  ,

	@GroupDescription varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CheckMateFieldGroup]
				SET
					[GroupName] = @GroupName
					,[GroupDescription] = @GroupDescription
				WHERE
[CheckMateFieldGroupID] = @CheckMateFieldGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Update] TO [sp_executeall]
GO
