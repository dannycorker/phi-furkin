SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FAS_Documents table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_Delete]
(

	@FasDocumentID int   
)
AS


				DELETE FROM [dbo].[FAS_Documents] WITH (ROWLOCK) 
				WHERE
					[FasDocumentID] = @FasDocumentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Delete] TO [sp_executeall]
GO
