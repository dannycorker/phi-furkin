SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-04-2016
-- Description:	Gets a customer by caseid and client id
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByCaseIDAndClientID]
	@ClientID INT,
	@CaseID INT
AS
BEGIN
	
	
	SET NOCOUNT ON;

	DECLARE @CustomerID INT
	DECLARE @LeadID INT
	SELECT TOP 1 @LeadID=LeadID FROM Cases WITH (NOLOCK) WHERE CaseID=@CaseID	
	SELECT TOP 1 @CustomerID=CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID
	
	SELECT * FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByCaseIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByCaseIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByCaseIDAndClientID] TO [sp_executeall]
GO
