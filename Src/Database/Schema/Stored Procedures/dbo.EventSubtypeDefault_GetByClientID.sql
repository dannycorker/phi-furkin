SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventSubtypeDefault table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventSubtypeDefaultID],
					[ClientID],
					[EventSubtypeID],
					[EventTypeID],
					[LeadTypeID],
					[ClientPersonnelID]
				FROM
					[dbo].[EventSubtypeDefault] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_GetByClientID] TO [sp_executeall]
GO
