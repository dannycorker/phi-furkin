SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptPositionTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_Delete]
(

	@ScriptPositionTargetID int   
)
AS


				DELETE FROM [dbo].[ScriptPositionTarget] WITH (ROWLOCK) 
				WHERE
					[ScriptPositionTargetID] = @ScriptPositionTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Delete] TO [sp_executeall]
GO
