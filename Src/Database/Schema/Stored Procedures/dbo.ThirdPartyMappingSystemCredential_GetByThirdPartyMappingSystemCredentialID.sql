SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyMappingSystemCredential table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartyMappingSystemCredentialID]
(

	@ThirdPartyMappingSystemCredentialID int   
)
AS


				SELECT
					[ThirdPartyMappingSystemCredentialID],
					[ThirdPartySystemID],
					[ClientID],
					[ConsumerKey],
					[ConsumerSecret]
				FROM
					[dbo].[ThirdPartyMappingSystemCredential] WITH (NOLOCK) 
				WHERE
										[ThirdPartyMappingSystemCredentialID] = @ThirdPartyMappingSystemCredentialID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartyMappingSystemCredentialID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartyMappingSystemCredentialID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_GetByThirdPartyMappingSystemCredentialID] TO [sp_executeall]
GO
