SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__Notification]
	@ClientPersonnelID INT,
	@LeadEventID INT,
	@NotifyXML XML
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC [dbo].[_C00_LogItXML] @ClientPersonnelID, @LeadEventID, 'DocuSignNotification', @NotifyXML, NULL
	

	DECLARE @DSPEnvelopeStatus VARCHAR(50)

	SELECT	@DSPEnvelopeStatus = T.c.value('Status[1]','VARCHAR(250)')
	FROM	@NotifyXML.nodes('/DocuSignEnvelopeInformation/EnvelopeStatus') T(c)
	
	DECLARE @ReponseXML XML = @NotifyXML.query('
	for $root in //DocuSignEnvelopeInformation
	return
	<DocumentSignResponse>
			<EnvelopeKey>{data($root/EnvelopeStatus/EnvelopeID)}</EnvelopeKey>
			<StatusCode>{data($root/EnvelopeStatus/Status)}</StatusCode>
			<Recipients>
			{
				for $recips in //RecipientStatus
				return
					<DocumentSignRecipient>
						{$recips/*}
					</DocumentSignRecipient>
			}
			</Recipients>
			<Documents>
			{
				for $docs in //DocumentPDF
				return
					<DocumentSignDocument>
						{$docs/*}
					</DocumentSignDocument>
			}
			</Documents>
	</DocumentSignResponse>
	')
	
	
	UPDATE	DocumentSignProviderQueue
		SET ResponseXML = @ReponseXML,
			DSPEnvelopeStatus = @DSPEnvelopeStatus,
			WhenModified = dbo.fn_GetDate_Local()
	WHERE OutLeadEventID = @LeadEventID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__Notification] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__Notification] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__Notification] TO [sp_executeall]
GO
