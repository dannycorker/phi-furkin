SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AquariumApplication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumApplication_Get_List]

AS


				
				SELECT
					[AquariumApplicationID],
					[ApplicationName],
					[ApplicationDescription],
					[CurrentVersion],
					[MinimumVersion],
					[CurrentVersionValidFrom],
					[CurrentVersionValidTo],
					[CurrentVersionDeployedOn]
				FROM
					[dbo].[AquariumApplication] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumApplication_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumApplication_Get_List] TO [sp_executeall]
GO
