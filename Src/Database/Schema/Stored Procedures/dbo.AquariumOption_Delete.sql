SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AquariumOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_Delete]
(

	@AquariumOptionID int   
)
AS


				DELETE FROM [dbo].[AquariumOption] WITH (ROWLOCK) 
				WHERE
					[AquariumOptionID] = @AquariumOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Delete] TO [sp_executeall]
GO
