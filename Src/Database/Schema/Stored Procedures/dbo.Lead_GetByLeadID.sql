SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Lead table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_GetByLeadID]
(

	@LeadID int   
)
AS


				SELECT
					[LeadID],
					[ClientID],
					[LeadRef],
					[CustomerID],
					[LeadTypeID],
					[AquariumStatusID],
					[ClientStatusID],
					[BrandNew],
					[Assigned],
					[AssignedTo],
					[AssignedBy],
					[AssignedDate],
					[RecalculateEquations],
					[Password],
					[Salt],
					[WhenCreated]
				FROM
					[dbo].[Lead] WITH (NOLOCK) 
				WHERE
										[LeadID] = @LeadID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByLeadID] TO [sp_executeall]
GO
