SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the MasterQuestions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_Update]
(

	@MasterQuestionID int   ,

	@ClientQuestionnaireID int   ,

	@QuestionTypeID int   ,

	@QuestionText varchar (MAX)  ,

	@QuestionOrder int   ,

	@DefaultAnswerID int   ,

	@Mandatory bit   ,

	@QuestionToolTip varchar (255)  ,

	@LinkedQuestionnaireMasterQuestionID int   ,

	@Active bit   ,

	@MasterQuestionStatus int   ,

	@TextboxHeight int   ,

	@ClientID int   ,

	@AnswerPosition int   ,

	@DisplayAnswerAs int   ,

	@NumberOfAnswersPerRow int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@QuestionPossibleAnswerSortOrder int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[MasterQuestions]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[QuestionTypeID] = @QuestionTypeID
					,[QuestionText] = @QuestionText
					,[QuestionOrder] = @QuestionOrder
					,[DefaultAnswerID] = @DefaultAnswerID
					,[Mandatory] = @Mandatory
					,[QuestionToolTip] = @QuestionToolTip
					,[LinkedQuestionnaireMasterQuestionID] = @LinkedQuestionnaireMasterQuestionID
					,[Active] = @Active
					,[MasterQuestionStatus] = @MasterQuestionStatus
					,[TextboxHeight] = @TextboxHeight
					,[ClientID] = @ClientID
					,[AnswerPosition] = @AnswerPosition
					,[DisplayAnswerAs] = @DisplayAnswerAs
					,[NumberOfAnswersPerRow] = @NumberOfAnswersPerRow
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[QuestionPossibleAnswerSortOrder] = @QuestionPossibleAnswerSortOrder
				WHERE
[MasterQuestionID] = @MasterQuestionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_Update] TO [sp_executeall]
GO
