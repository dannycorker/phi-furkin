SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipInformation__Annihilate]
	@DocumentZipInformationID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ZDeletes TABLE (DocumentZipInformationID int, DocumentZipFileID int, DocumentZipID int, DocumentQueueID int, LeadEventID int)

	/* Check for any good zips within the specified DocumentZipFile and demand a split if any are found */
	IF EXISTS (
				SELECT * 
				FROM dbo.DocumentZip dz WITH (NOLOCK) 
				WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
				AND dz.StatusID = 31
	)
	BEGIN
		PRINT 'FAIL!'
		PRINT 'Please split the bad zips from the good first, using this command:'
		PRINT 'EXEC dbo.DocumentZipInformation__SplitOutFailedZips @DocumentZipInformationID = ' + COALESCE(CAST(@DocumentZipInformationID AS VARCHAR), 'NULL') +  ', @ShowResults = 1' 
	END
	ELSE
	BEGIN

		/* Gather the list of LeadEvents etc to update and delete */
		INSERT INTO @ZDeletes (DocumentZipInformationID, DocumentZipFileID, DocumentZipID, DocumentQueueID, LeadEventID) 
		SELECT dzi.DocumentZipInformationID, dz.DocumentZipFileID, dz.DocumentZipID, dq.DocumentQueueID, le.LeadEventID 
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		LEFT JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dzi.DocumentZipInformationID = dz.DocumentZipInformationID 
		LEFT JOIN dbo.DocumentQueue dq WITH (NOLOCK) ON dq.DocumentQueueID = dz.DocumentQueueID 
		LEFT JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.DocumentQueueID = dq.DocumentQueueID 
		WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID
		
		/* Show what will be deleted */
		SELECT * FROM @ZDeletes

		/* Store a comment on each LeadEvent explaining why there is no document, then remove the link to the queue */
		UPDATE dbo.LeadEvent
		SET Comments += ' Queued but never created, template now invalid, document will never be created.',
		DocumentQueueID = NULL 
		FROM dbo.LeadEvent le 
		INNER JOIN @ZDeletes zd ON zd.LeadEventID = le.LeadEventID 

		/* Delete the unwanted zip records */
		DELETE dbo.DocumentZip 
		WHERE DocumentZipInformationID = @DocumentZipInformationID

		/* Delete the unwanted queue records */
		DELETE dbo.DocumentQueue 
		FROM dbo.DocumentQueue dq 
		INNER JOIN @ZDeletes zd ON zd.DocumentQueueID = dq.DocumentQueueID

		/* Delete the unwanted link file record */
		DELETE dbo.DocumentZipFile 
		WHERE DocumentZipInformationID = @DocumentZipInformationID

		/* Delete the entire DocumentZipInformation record */
		DELETE dbo.DocumentZipInformation 
		WHERE DocumentZipInformationID = @DocumentZipInformationID

	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__Annihilate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__Annihilate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipInformation__Annihilate] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipInformation__Annihilate] TO [sp_executehelper]
GO
