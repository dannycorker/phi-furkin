SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkingDays table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_GetByDate]
(

	@Date datetime   
)
AS


				SELECT
					[WorkingDayID],
					[Year],
					[Month],
					[Day],
					[DayNumber],
					[IsWorkDay],
					[IsWeekDay],
					[IsBankHoliday],
					[Date],
					[WeekNumber],
					[DayNumberOfWeek],
					[CharDate]
				FROM
					[dbo].[WorkingDays] WITH (NOLOCK) 
				WHERE
										[Date] = @Date
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_GetByDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_GetByDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_GetByDate] TO [sp_executeall]
GO
