SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Matter table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_Delete]
(

	@MatterID int   
)
AS


				DELETE FROM [dbo].[Matter] WITH (ROWLOCK) 
				WHERE
					[MatterID] = @MatterID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Delete] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_Delete] TO [sp_executeall]
GO
