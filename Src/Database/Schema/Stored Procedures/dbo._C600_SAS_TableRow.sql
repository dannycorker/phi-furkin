SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex ELger
-- Create date: 2018-06-13
-- Description:	Proc to Run Table Row SAS for Client 600
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SAS_TableRow]
@ClientPersonnelID INT,
@CustomerID INT,
@LeadID INT,
@CaseID INT = NULL, 
@MatterID INT = NULL,
@TableRowID INT = NULL, 
@TableDFID INT,
@ResourceListID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID(),
			@EventTypeName VARCHAR(2000),
			@EventTypeID INT,
			@ClientPersonnelName VARCHAR(2000),
			@NowDateTimeVC VARCHAR(20) = CONVERT(VARCHAR(20), dbo.fn_GetDate_Local(), 120)

	/*Custom Event Followups*/
	IF @TableDFID = 180066
	BEGIN

		/*Get the users name*/
		SELECT @ClientPersonnelName = cp.UserName
		FROM ClientPersonnel cp WITH (NOLOCK)
		WHERE cp.ClientPersonnelID = @ClientPersonnelID

		/*Get the event type id from the row*/
		SELECT @EventTypeID = dbo.fnGetSimpleDvAsInt(180061,@TableRowID)

		IF @EventTypeID > 0
		BEGIN

			/*Get the event type name to save to the row*/
			SELECT @EventTypeName = et.EventTypeName
			FROM EventType et WITH (NOLOCK)
			WHERE et.EventTYpeID = @EventTypeID

			EXEC dbo._C00_SimpleValueIntoField 180062, @EventTypeName, @TableRowID, @ClientPersonnelID

			IF NOT EXISTS (
				SELECT *
				FROM EventTypeSql e WITH (NOLOCK)
				WHERE e.EventTypeID = @EventTypeID
			)
			BEGIN

				EXEC dbo.ets NULL, @EventTypeID, 1

			END

		END
		ELSE
		BEGIN

			EXEC dbo._C00_SimpleValueIntoField 180062, 'There is no event type for the specified eventtypeid', @TableRowID, @ClientPersonnelID

		END

		/*Who Saved*/
		EXEC dbo._C00_SimpleValueIntoField 180064, @ClientPersonnelName, @TableRowID, @ClientPersonnelID
		EXEC dbo._C00_SimpleValueIntoField 180065, @NowDateTimeVC, @TableRowID, @ClientPersonnelID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAS_TableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SAS_TableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAS_TableRow] TO [sp_executeall]
GO
