SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the InvoiceMappingEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMappingEvent_Delete]
(

	@InvoiceMappingEventID int   
)
AS


				DELETE FROM [dbo].[InvoiceMappingEvent] WITH (ROWLOCK) 
				WHERE
					[InvoiceMappingEventID] = @InvoiceMappingEventID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMappingEvent_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMappingEvent_Delete] TO [sp_executeall]
GO
