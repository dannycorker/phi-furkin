SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptInputType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptInputType_Insert]
(

	@ScriptInputTypeID int    OUTPUT,

	@InputType varchar (50)  ,

	@Template varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ScriptInputType]
					(
					[InputType]
					,[Template]
					)
				VALUES
					(
					@InputType
					,@Template
					)
				-- Get the identity value
				SET @ScriptInputTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptInputType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptInputType_Insert] TO [sp_executeall]
GO
