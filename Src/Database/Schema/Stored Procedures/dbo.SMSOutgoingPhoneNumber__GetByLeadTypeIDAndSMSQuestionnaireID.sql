SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-03-2013
-- Description: Gets a list of outgoing sms phone numbers by leadtype and questionnaireID
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__GetByLeadTypeIDAndSMSQuestionnaireID]
(
	@LeadTypeID INT   ,
	@SMSQuestionnaireID INT
)
AS

	SET ANSI_NULLS ON
	
	SELECT *
	FROM
		[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
	WHERE
		[LeadTypeID] = @LeadTypeID AND
		[SMSQuestionnaireID] = @SMSQuestionnaireID
	
	SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__GetByLeadTypeIDAndSMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__GetByLeadTypeIDAndSMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__GetByLeadTypeIDAndSMSQuestionnaireID] TO [sp_executeall]
GO
