SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the TableRows table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_Delete]
(

	@TableRowID int   
)
AS


				DELETE FROM [dbo].[TableRows] WITH (ROWLOCK) 
				WHERE
					[TableRowID] = @TableRowID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Delete] TO [sp_executeall]
GO
