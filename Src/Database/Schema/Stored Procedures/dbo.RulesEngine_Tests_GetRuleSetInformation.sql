SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-07-11
-- Description:	Gets the base rule set information to show on a test
-- UPDATES:		2016-06-21	SB	Update to the param hierarchy
--				2016-07-27  DCM Exclude rule input parameter types from final list (ParameterTypeID != 7)
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Tests_GetRuleSetInformation]
(
	@RuleSetID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	rs.RuleSetID AS ID, rs.Name, rs.InputTypeID, dtIn.Name AS InputType, rs.OutputTypeID, dtOut.Name AS OutputType
	FROM dbo.RulesEngine_RuleSets rs
	INNER JOIN dbo.RulesEngine_DataTypes dtIn WITH (NOLOCK) ON rs.InputTypeID = dtIn.TypeID
	INNER JOIN dbo.RulesEngine_DataTypes dtOut WITH (NOLOCK) ON rs.OutputTypeID = dtOut.TypeID
	WHERE rs.RuleSetID = @RuleSetID
	
	DECLARE @Parameters TABLE
	(
		ParameterName VARCHAR(2000),
		ParameterTypeID INT,
		ParameterTypeName VARCHAR(2000),
		Value VARCHAR(2000),
		Parent VARCHAR(2000),
		DataTypeID INT
	)
	
	;WITH Param_Hierarchy(ParentRuleSetID, RuleSetID, ParentRuleID, RuleID, RuleParameterID, Name, ParameterTypeID, Value, Parent, DataTypeID) 
	AS 
	(
		SELECT r.RuleSetID, r.RuleSetID, r.RuleID, r.RuleID, p.RuleParameterID, p.Name, p.ParameterTypeID, p.Value, p.Parent, p.DataTypeID
		FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON r.RuleID = p.RuleID
		WHERE r.RuleSetID = @RuleSetID
		
		UNION ALL
		
		SELECT h.ParentRuleSetID, r.RuleSetID, h.RuleID, r.RuleID, p.RuleParameterID, p.Name, p.ParameterTypeID, p.Value, p.Parent, p.DataTypeID
		FROM Param_Hierarchy h
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON h.Value = r.RuleSetID 
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON r.RuleID = p.RuleID
		WHERE h.ParameterTypeID = 3
		AND r.RuleSetID != @RuleSetID
		
	)

	INSERT @Parameters (ParameterName, ParameterTypeID, ParameterTypeName, Value, Parent, DataTypeID)
	SELECT DISTINCT h.Name AS ParameterName, h.ParameterTypeID, t.Name AS ParameterTypeName, h.Value, h.Parent, h.DataTypeID
	FROM Param_Hierarchy h
	INNER JOIN dbo.RulesEngine_ParameterTypes t WITH (NOLOCK) ON h.ParameterTypeID = t.ParameterTypeID
	WHERE h.ParentRuleSetID = @RuleSetID
	AND h.ParameterTypeID != 3
	AND h.ParameterTypeID != 7
	AND (h.ParameterTypeID != 2 OR h.Value NOT IN ('All', 'Any'))
	
	SELECT *
	FROM @Parameters
	
	SELECT df.DetailFieldID, ll.LookupListItemID, ll.ItemValue
	FROM @Parameters p
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON p.Value = df.DetailFieldID
	INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
	WHERE p.DataTypeID = 6
	AND ll.Enabled = 1
	ORDER BY ll.ItemValue
	
	
	SELECT 'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord
	UNION ALL
	SELECT s.Name AS Title, '#/ruleset/' + CAST(s.RuleSetID AS VARCHAR) + '/rules' AS Path, 2 AS Ord
	FROM dbo.RulesEngine_RuleSets s WITH (NOLOCK) 
	WHERE s.RuleSetID = @RuleSetID
	UNION ALL
	SELECT 'Test' AS Title, '#/ruleset/' + CAST(s.RuleSetID AS VARCHAR) + '/rules' AS Path, 3 AS Ord
	FROM dbo.RulesEngine_RuleSets s WITH (NOLOCK) 
	WHERE s.RuleSetID = @RuleSetID
	ORDER BY Ord
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Tests_GetRuleSetInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Tests_GetRuleSetInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Tests_GetRuleSetInformation] TO [sp_executeall]
GO
