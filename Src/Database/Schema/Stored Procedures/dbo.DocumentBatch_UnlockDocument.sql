SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2013-03-01
-- Description:	Releases a lock on a document providing it is not a 
--              document that has been escalated to someone.

-- Parameters:  @ClientPersonnelId
--              The user performing the request
--              @DocumentBatchDetailId
--              The document to unlock
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatch_UnlockDocument]
	@ClientPersonnelId INT,
	@DocumentBatchDetailId INT
AS
BEGIN
	UPDATE DocumentBatchDetail
	SET LockedBy = null, LockDate = null
	WHERE DocumentBatchDetailId 
		-- Find out if the most recent document is esclated
		NOT IN (SELECT TOP 1 DocumentBatchDetailID
		FROM DocumentBatchDetailEscalateReason WITH (NOLOCK)
		WHERE Escalated = 1
		AND DocumentBatchDetail.DocumentBatchDetailID = DocumentBatchDetailEscalateReason.DocumentBatchDetailID
		ORDER BY EscalatedOn DESC)
	AND Uploaded = 0
	AND LockedBy IS NOT NULL
	AND LockDate IS NOT NULL
	AND DocumentBatchDetailID = @DocumentBatchDetailId
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_UnlockDocument] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_UnlockDocument] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_UnlockDocument] TO [sp_executeall]
GO
