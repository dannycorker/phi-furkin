SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DiaryAppointment table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_GetByCaseID]
(

	@CaseID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DiaryAppointmentID],
					[ClientID],
					[DiaryAppointmentTitle],
					[DiaryAppointmentText],
					[CreatedBy],
					[DueDate],
					[EndDate],
					[AllDayEvent],
					[Completed],
					[LeadID],
					[CaseID],
					[CustomerID],
					[Version],
					[ExportVersion],
					[RecurrenceInfo],
					[DiaryAppointmentEventType],
					[ResourceInfo],
					[TempReminderTimeshiftID],
					[StatusID],
					[LabelID]
				FROM
					[dbo].[DiaryAppointment] WITH (NOLOCK) 
				WHERE
					[CaseID] = @CaseID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_GetByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_GetByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_GetByCaseID] TO [sp_executeall]
GO
