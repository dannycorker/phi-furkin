SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-08-05
-- Description:	Get Client payment gateway by client and gateway
-- =============================================
CREATE PROCEDURE [dbo].[ClientPaymentGateway__GetByClientIDPaymentGatewayID]
	@ClientID INT,
	@PaymentGatewayID INT

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @RowCount INT

	SELECT * 
	FROM ClientPaymentGateway WITH (NOLOCK) 
	WHERE ClientID = @ClientID
	AND PaymentGatewayID = @PaymentGatewayID    

	SELECT @RowCount = @@ROWCOUNT

	EXEC _C00_LogIt 'Debug','ClientPaymentGateway__GetByClientIDPaymentGatewayID', '@ClientPaymentGatewayID = ', @PaymentGatewayID, @RowCount
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByClientIDPaymentGatewayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPaymentGateway__GetByClientIDPaymentGatewayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__GetByClientIDPaymentGatewayID] TO [sp_executeall]
GO
