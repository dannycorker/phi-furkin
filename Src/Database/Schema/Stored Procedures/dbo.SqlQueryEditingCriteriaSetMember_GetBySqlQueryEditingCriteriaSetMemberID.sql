SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteriaSetMember table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingCriteriaSetMemberID]
(

	@SqlQueryEditingCriteriaSetMemberID int   
)
AS


				SELECT
					[SqlQueryEditingCriteriaSetMemberID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingCriteriaSetID],
					[SqlQueryEditingColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryEditingCriteriaSetMember] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingCriteriaSetMemberID] = @SqlQueryEditingCriteriaSetMemberID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingCriteriaSetMemberID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingCriteriaSetMemberID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingCriteriaSetMemberID] TO [sp_executeall]
GO
