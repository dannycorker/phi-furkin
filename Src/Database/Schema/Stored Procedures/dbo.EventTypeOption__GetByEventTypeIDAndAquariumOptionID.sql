SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Select records from the EventTypeOption table by EventTypeID and AquariumOptionID
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeOption__GetByEventTypeIDAndAquariumOptionID]
(
	@EventTypeID int,
	@AquariumOptionID int
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeOptionID],
					[ClientID],
					[EventTypeID],
					[AquariumOptionID],
					[OptionValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[SourceID]
				FROM
					[dbo].[EventTypeOption] WITH (NOLOCK) 
				WHERE
					[EventTypeID] = @EventTypeID
					AND [AquariumOptionID] = @AquariumOptionID
				ORDER BY EventTypeOptionID DESC
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption__GetByEventTypeIDAndAquariumOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeOption__GetByEventTypeIDAndAquariumOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption__GetByEventTypeIDAndAquariumOptionID] TO [sp_executeall]
GO
