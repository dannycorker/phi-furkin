SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-01-22
-- Description:	Following a mid-term adjustment, update the WrittenPremium adjustment value
-- 2020-01-22 CPS for JIRA LPC-394	 | Update Written Premium following the MTA completion as we need to know the total payable for the year first
--									 | Also remove redundant MTA calculation rows that serve no purpose other than to confuse people
--									 | Unfortunately we have no scope to prevent them from being created
-- 2020-02-05 CPS for JIRA AAG-91	 | Replace WrittenPremium references with PurchasedProductPaymentScheduleDetail
-- 2020-02-16 CPS for JIRA AAG-37	 | For reinstatements, flip the adjustment values from the latest cancellation
-- 2020-03-18 GPR for AAG-512		 | Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR for AAG-507		 | Updated tax related column names
-- 2020-12-04 CPS for JIRA SDPRU-182 | Pick up the adjustment date rather than using today
-- =============================================
CREATE PROCEDURE [dbo].[WrittenPremium_UpdateAdjustment]
	 @ClientID		INT
	,@CaseID		INT
	,@LeadEventID	INT
AS
BEGIN

	DECLARE  @WrittenPremiumID					INT
			,@WrittenPremiumAdjustmentGross				DECIMAL(18,4)
			,@WrittenPremiumAdjustmentNet				DECIMAL(18,4)
			,@WrittenPremiumAdjustmentIpt				DECIMAL(18,4)
			,@TotalPayableForYear						DECIMAL(18,4)
			,@SumOfAdjustments							DECIMAL(18,4)
			,@PurchasedProductID						INT
			,@MatterID									INT
			,@LatestYearStartPaymentScheduleDetailID	INT
			,@WhoCreated								INT
			,@EventComments								VARCHAR(2000) = '[' + OBJECT_NAME(@@ProcID) + ']' + CHAR(13)+CHAR(10) + '[Adjustment to Written Premium] = [Total Payable for the Year] - [Sum of adjustment values so far]' + CHAR(13)+CHAR(10)
			,@AdjustmentTypeID							INT
			,@AdjustmentDate							DATE
			,@CustomerID								INT
			,@AffinityID								INT
			,@CommissionPercentageValue					NUMERIC(18,2)

	DECLARE  @AdjustmentSums TABLE ( SumType				VARCHAR(10)
									,AdjustmentValueGross	DECIMAL(18,4)
									,AdjustmentValueNet		DECIMAL(18,4)
									,AdjustmentValueNationalTax		DECIMAL(18,4)
									,AdjustmentValueLocalTax		DECIMAL(18,4) -- GPR 2020-04-15
									,AdjustmentAdminFee				DECIMAL(18,4) -- GPR 2021-05-10
									)

	SELECT @WhoCreated = le.WhoCreated
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID = @LeadEventID

	SELECT @MatterID = m.MatterID, @CustomerID = m.CustomerID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID = @CaseID

	SELECT TOP 1 @PurchasedProductID = pp.PurchasedProductID
	FROM PurchasedProduct pp WITH (NOLOCK) 
	WHERE pp.ObjectID = @MatterID
	ORDER BY pp.ValidTo DESC, pp.ValidFrom DESC
	
	SELECT TOP 1 @WrittenPremiumID  = wp.WrittenPremiumID
				,@AdjustmentTypeID	= wp.AdjustmentTypeID
				,@AdjustmentDate	= wp.ValidFrom -- 2020-12-04 CPS for JIRA SDPRU-182 | Pick up the adjustment date rather than using today
	FROM WrittenPremium wp WITH (NOLOCK) 
	WHERE wp.MatterID = @MatterID
	AND wp.AdjustmentTypeID IN ( 2 /*MTA*/, 4 /*Cancellation*/, 6 /*Reinstatement*/, 1 /*New Business*/ ) -- GPR 2020-04-15 added NewBusiness to List
	ORDER BY wp.WrittenPremiumID DESC

	IF @AdjustmentTypeID = 6 /*Reinstatement*/
	BEGIN
		/*For a reinsatement, flip the latest cancellation row*/
		INSERT @AdjustmentSums ( SumType, AdjustmentValueGross, AdjustmentValueNet, AdjustmentValueNationalTax, AdjustmentValueLocalTax, AdjustmentAdminFee)
		SELECT TOP (1)
				 'newValue'
				,wp.AdjustmentValueGross * -1
				,wp.AdjustmentValueNet	* -1
				,wp.AdjustmentValueNationalTax	* -1 -- GPR 2020-04-15
				,wp.AdjustmentValueLocalTax	* -1
				,wp.AdjustmentAdminFee * -1 /*GPR 2021-05-10 for FURKIN-605*/
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4 /*Cancellation*/
		ORDER BY wp.WrittenPremiumID DESC

		DECLARE @AdjustmentBrokerCommission DECIMAL(18,4),@AdjustmentUWCommission DECIMAL(18,4)
		
		SELECT @AdjustmentBrokerCommission = (SELECT TOP(1) AdjustmentBrokerCommission
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4 /*Cancellation*/
		ORDER BY wp.WrittenPremiumID DESC)

		SELECT @AdjustmentUWCommission = (SELECT TOP(1) AdjustmentUnderwriterCommission
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @MatterID
		AND wp.AdjustmentTypeID = 4 /*Cancellation*/
		ORDER BY wp.WrittenPremiumID DESC)

	END

	/*GPR 2021-05-10 for FURKIN-605*/
	IF @AdjustmentTypeID IN (1,3) /*New or Renewal*/
	BEGIN
		UPDATE wp
		SET AdjustmentAdminFee = AnnualAdminFee
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.WrittenPremiumID = @WrittenPremiumID
	END
	ELSE IF @AdjustmentTypeID = 4 /*Cancellation*/ AND dbo.fnGetSimpleDVAsDate(170055,@MatterID) = dbo.fnGetSimpleDVAsDate(170035,@MatterID) /*Cancellation Date = Inception Date*/
	BEGIN
		UPDATE wp
		SET AdjustmentAdminFee = AnnualAdminFee * -1
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.WrittenPremiumID = @WrittenPremiumID
	END
	ELSE
	BEGIN
		UPDATE wp
		SET AdjustmentAdminFee = 0.00
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.WrittenPremiumID = @WrittenPremiumID
	END

	IF @WrittenPremiumID <> 0 AND @AdjustmentTypeID = 6 /*GPR 2020-04-27*/
	BEGIN

		UPDATE wp
		SET	 AdjustmentBrokerCommission = @AdjustmentBrokerCommission * -1,
		AdjustmentUnderwriterCommission = @AdjustmentUWCommission * -1
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.WrittenPremiumID = @WrittenPremiumID

	END

	/*GPR 2020-08-17 / 2020-08-18*/
	IF @AdjustmentTypeID = 2 /*MTA*/
	BEGIN

			/*GPR 2020-08-17*/
			SELECT TOP 1 @WrittenPremiumID = wp.WrittenPremiumID
					,@AdjustmentTypeID		  = wp.AdjustmentTypeID
			FROM WrittenPremium wp WITH (NOLOCK) 
			WHERE wp.MatterID = @MatterID
			--AND wp.AdjustmentTypeID IN ( 2 /*MTA*/)
			ORDER BY wp.WrittenPremiumID DESC

			SELECT @AffinityID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID) /*Affinity Details*/
			SELECT @CommissionPercentageValue = dbo.fnGetSimpleDvAsMoney(313814,@AffinityID) /*Underwriter Commission Percentage Value*/  

			UPDATE wp
			SET  AdjustmentValueGross = adj.AdjustmentGross * -1
				,AdjustmentValueNet = adj.AdjustmentNet * -1
				,AdjustmentValueNationalTax = adj.AdjustmentNationalTax * -1
				,AdjustmentValueLocalTax = adj.AdjustmentLocalTax * -1
				,AdjustmentBrokerCommission = CONVERT(NUMERIC(18,2),(adj.AdjustmentNet * ((100.00 - @CommissionPercentageValue)/100.00))*-1)
				,AdjustmentUnderwriterCommission = CONVERT(NUMERIC(18,2),(adj.AdjustmentNet - (adj.AdjustmentNet * ((100.00 - @CommissionPercentageValue)/100.00)))*-1)
				,Comments = Comments + ' Updated by WrittenPremium_UpdateAdjustment'
			FROM WrittenPremium wp WITH (NOLOCK)
			CROSS APPLY dbo.fn_C00_MidTermAdjustmentByDailyPremium(@MatterID, @AdjustmentDate) adj
			WHERE wp.WrittenPremiumID = @WrittenPremiumID


			SELECT @EventComments += 'Written Premium Adjustment: '  + ISNULL(CONVERT(VARCHAR,adj.AdjustmentGross),'NULL') 
							  +  ' (Net: ' +					 + ISNULL(CONVERT(VARCHAR,adj.AdjustmentNet),'NULL')
							  +  ', National Tax: ' +			 + ISNULL(CONVERT(VARCHAR,adj.AdjustmentNationalTax),'NULL')
							  +  ', Local Tax: ' +				 + ISNULL(CONVERT(VARCHAR,adj.AdjustmentLocalTax),'NULL') + ')'
							  + CHAR(13)+CHAR(10)
			FROM dbo.fn_C00_MidTermAdjustmentByDailyPremium(@MatterID, @AdjustmentDate) adj

			DECLARE @AdjustmentGross DECIMAL(18,4)
			SELECT @AdjustmentGross = a.AdjustmentValueGross
			FROM WrittenPremium a WITH (NOLOCK) 
			where a.WrittenPremiumID = @WrittenPremiumID

			EXEC _C00_SimpleValueIntoField 177404,@AdjustmentGross,@MatterID,@WhoCreated

	END

	IF @WrittenPremiumID <> 0 AND @AdjustmentTypeID NOT IN (4, 2 /*GPR 2020-08-18 remove MTAs as comments set above*/) /*GPR 2020-04-23 we don't want to update the AdjustmentValues for cancellations here as done in premiumforperiod*/
	BEGIN
		UPDATE wp
		SET	 AdjustmentValueGross	= newValue.AdjustmentValueGross
			,AdjustmentValueNET		= newValue.AdjustmentValueNet
			,AdjustmentValueNationalTax		= newValue.AdjustmentValueNationalTax
			,AdjustmentValueLocalTax		= newValue.AdjustmentValueLocalTax -- GPR 2020-04-15
			,Comments = Comments + ' Updated by WrittenPremium_UpdateAdjustment (2)'
		FROM WrittenPremium wp WITH (NOLOCK) 
		CROSS APPLY @AdjustmentSums newValue
		WHERE wp.WrittenPremiumID = @WrittenPremiumID
		AND newValue.SumType = 'newValue'

		/*GPR 2020-04-15 updated @EventComments to include @LocalTax*/
		SELECT @EventComments += 'Written Premium Adjustment: '  + ISNULL(CONVERT(VARCHAR,newValue.AdjustmentValueGross	),'NULL') 
							  +  ' (Net: ' +					 + ISNULL(CONVERT(VARCHAR,newValue.AdjustmentValueNet	),'NULL')
							  +  ', National Tax: ' +			 + ISNULL(CONVERT(VARCHAR,newValue.AdjustmentValueNationalTax	),'NULL')
							  +  ', Local Tax: ' +				 + ISNULL(CONVERT(VARCHAR,newValue.AdjustmentValueLocalTax	),'NULL') + ')'
							  + CHAR(13)+CHAR(10)				 
						  									 
							  + 'Total Payable for Year @ '		 + ISNULL(CONVERT(VARCHAR,payable.AdjustmentValueGross	),'NULL') 
							  +  ' (Net: ' +					 + ISNULL(CONVERT(VARCHAR,payable.AdjustmentValueNet	),'NULL')
							  +  ', National Tax: ' +			 + ISNULL(CONVERT(VARCHAR,payable.AdjustmentValueNationalTax	),'NULL')
							  +  ', Local Tax: ' +				 + ISNULL(CONVERT(VARCHAR,payable.AdjustmentValueLocalTax	),'NULL') + ')'
							  + CHAR(13)+CHAR(10)				 
						  									 
							  + 'Sum Of Previous Adjustments @ ' + ISNULL(CONVERT(VARCHAR,soFar.AdjustmentValueGross	),'NULL') 
							  +  ' (Net: ' +					 + ISNULL(CONVERT(VARCHAR,soFar.AdjustmentValueNet		),'NULL')
							  +  ', National Tax: ' +			 + ISNULL(CONVERT(VARCHAR,soFar.AdjustmentValueNationalTax		),'NULL')
							  +  ', Local Tax: ' +			 + ISNULL(CONVERT(VARCHAR,soFar.AdjustmentValueLocalTax		),'NULL') + ')'
							  + CHAR(13)+CHAR(10)		
						  
							  + 'Year Start PaymentScheduleDetailID ' + ISNULL(CONVERT(VARCHAR,@LatestYearStartPaymentScheduleDetailID),'NULL') 
							  + CHAR(13)+CHAR(10)
		FROM @AdjustmentSums newValue
		CROSS APPLY @AdjustmentSums soFar
		CROSS APPLY @AdjustmentSums payable
		WHERE newValue.SumType = 'newValue'
		AND sofar.SumType = 'soFar'
		AND payable.SumType = 'payable'
	END
	ELSE
	BEGIN
		SELECT @EventComments += 'No Written Premium row found after Year Start PaymentScheduleDetailID ' + ISNULL(CONVERT(VARCHAR,@LatestYearStartPaymentScheduleDetailID),'NULL') + CHAR(13)+CHAR(10)
	END

	/*GPR 2020-08-17 moved from much earlier in the proc to here*/
	/*Delete unnecessary and confusing MTA calculation rows*/
	DELETE wp
	FROM WrittenPremium wp WITH (NOLOCK) 
	WHERE wp.MatterID = @MatterID
	AND wp.AdjustmentTypeID = 9 /*MTA Quote*/

	SELECT @EventComments += CONVERT(VARCHAR,@@ROWCOUNT) + ' temporary calculation rows removed.' + CHAR(13)+CHAR(10)

	EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WrittenPremium_UpdateAdjustment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WrittenPremium_UpdateAdjustment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WrittenPremium_UpdateAdjustment] TO [sp_executeall]
GO
