SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyMappingQuery table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyMappingQueryID int   = null ,

	@ClientID int   = null ,

	@ThirdPartySystemID int   = null ,

	@ThirdPartyFieldGroupID int   = null ,

	@QueryID int   = null ,

	@Method int   = null ,

	@ActionURI varchar (2000)  = null ,

	@SchemaName varchar (250)  = null ,

	@BaseIndex varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyMappingQueryID]
	, [ClientID]
	, [ThirdPartySystemID]
	, [ThirdPartyFieldGroupID]
	, [QueryID]
	, [Method]
	, [ActionURI]
	, [SchemaName]
	, [BaseIndex]
    FROM
	[dbo].[ThirdPartyMappingQuery] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingQueryID] = @ThirdPartyMappingQueryID OR @ThirdPartyMappingQueryID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID OR @ThirdPartyFieldGroupID IS NULL)
	AND ([QueryID] = @QueryID OR @QueryID IS NULL)
	AND ([Method] = @Method OR @Method IS NULL)
	AND ([ActionURI] = @ActionURI OR @ActionURI IS NULL)
	AND ([SchemaName] = @SchemaName OR @SchemaName IS NULL)
	AND ([BaseIndex] = @BaseIndex OR @BaseIndex IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyMappingQueryID]
	, [ClientID]
	, [ThirdPartySystemID]
	, [ThirdPartyFieldGroupID]
	, [QueryID]
	, [Method]
	, [ActionURI]
	, [SchemaName]
	, [BaseIndex]
    FROM
	[dbo].[ThirdPartyMappingQuery] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingQueryID] = @ThirdPartyMappingQueryID AND @ThirdPartyMappingQueryID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID AND @ThirdPartyFieldGroupID is not null)
	OR ([QueryID] = @QueryID AND @QueryID is not null)
	OR ([Method] = @Method AND @Method is not null)
	OR ([ActionURI] = @ActionURI AND @ActionURI is not null)
	OR ([SchemaName] = @SchemaName AND @SchemaName is not null)
	OR ([BaseIndex] = @BaseIndex AND @BaseIndex is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Find] TO [sp_executeall]
GO
