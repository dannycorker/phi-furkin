SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PortalOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalOption_Get_List]

AS


				
				SELECT
					[PortalOptionID],
					[ClientID],
					[ShowHelpLink],
					[HelpLinkCaption],
					[HelpLinkURL],
					[LeadListShowAssignedTo],
					[LeadListShowAssignedToCaption],
					[LeadListShowCaseStatus],
					[LeadListShowCaseStatusCaption]
				FROM
					[dbo].[PortalOption] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalOption_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_Get_List] TO [sp_executeall]
GO
