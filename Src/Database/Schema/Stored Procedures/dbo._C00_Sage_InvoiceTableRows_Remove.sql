SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-06-27
-- Description:	Deletes all the invoice rows for the given customer 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_InvoiceTableRows_Remove]
(	
	@ClientID INT,
	@LeadID INT	
)
AS
BEGIN
	DECLARE @CustomerID INT,
			@InvoiceTableDetailFieldID INT
	
	
	SELECT @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID AND ClientID=@ClientID
	
	SELECT @InvoiceTableDetailFieldID=tpfm.DetailFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	WHERE ClientID = @ClientID AND ThirdPartyFieldID=190

	IF @CustomerID<>0 AND @InvoiceTableDetailFieldID<>0
	BEGIN
		DELETE FROM [dbo].[TableRows]
		WHERE ClientID=@ClientID AND 
			  DetailFieldID = @InvoiceTableDetailFieldID AND 
			  CustomerID=@CustomerID
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InvoiceTableRows_Remove] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_InvoiceTableRows_Remove] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_InvoiceTableRows_Remove] TO [sp_executeall]
GO
