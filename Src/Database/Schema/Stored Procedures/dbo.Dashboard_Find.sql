SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Dashboard table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_Find]
(

	@SearchUsingOR bit   = null ,

	@DashboardID int   = null ,

	@DashboardName nchar (10)  = null ,

	@DashboardDescription nchar (10)  = null ,

	@OwnerID nchar (10)  = null ,

	@DashboardLayoutID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DashboardID]
	, [DashboardName]
	, [DashboardDescription]
	, [OwnerID]
	, [DashboardLayoutID]
	, [ClientID]
    FROM
	[dbo].[Dashboard] WITH (NOLOCK) 
    WHERE 
	 ([DashboardID] = @DashboardID OR @DashboardID IS NULL)
	AND ([DashboardName] = @DashboardName OR @DashboardName IS NULL)
	AND ([DashboardDescription] = @DashboardDescription OR @DashboardDescription IS NULL)
	AND ([OwnerID] = @OwnerID OR @OwnerID IS NULL)
	AND ([DashboardLayoutID] = @DashboardLayoutID OR @DashboardLayoutID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DashboardID]
	, [DashboardName]
	, [DashboardDescription]
	, [OwnerID]
	, [DashboardLayoutID]
	, [ClientID]
    FROM
	[dbo].[Dashboard] WITH (NOLOCK) 
    WHERE 
	 ([DashboardID] = @DashboardID AND @DashboardID is not null)
	OR ([DashboardName] = @DashboardName AND @DashboardName is not null)
	OR ([DashboardDescription] = @DashboardDescription AND @DashboardDescription is not null)
	OR ([OwnerID] = @OwnerID AND @OwnerID is not null)
	OR ([DashboardLayoutID] = @DashboardLayoutID AND @DashboardLayoutID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Find] TO [sp_executeall]
GO
