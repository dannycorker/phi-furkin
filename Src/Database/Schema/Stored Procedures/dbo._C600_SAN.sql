SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-11-24
-- Description:	SQL After Note Create processing
-- Mods:
-- 2016-07-05 DCM introduced Note Config resource list
-- 2016-11-09 DCM ApplyToAll type 5
-- 2019-08-15 JML #58981 Duplicate notes on Claims, copied fix from CP
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SAN]
	@LeadEventID int

AS
BEGIN
	SET NOCOUNT ON;

--declare @LeadEventID INT = 894

	DECLARE @myERROR INT,
			@AllowMultiple BIT,
			@CaseID INT,
			@LeadID INT,
			@LeadTypeID INT,
			@ClientID INT,
			@NoteTypeID INT,
			@NotePriority INT,
			@WhoCreated INT,
			@NoteText VARCHAR(MAX),
			@PolicyAdminLeadID INT,
			@ApplyToAll INT -- 0=this case only (73988); 
							-- 1=all cases in this lead (73989); 
							-- 2=all cases in all leads of this lead type (73990); 
							-- 3=all of the policy holder's cases (73991)
							-- 4=all claims for that policy, the collections case for that policy if last/only live policy on it & the policy case itself (73992)
							-- 5=all claims, collections and pa linked to a collections account (74486)
							-- 6=All claims for that policy, the policy and the collections case for that policy

	-- store this note LeadEventID in the HoldLeadEventID of this note and all notes created in the cascade
	UPDATE LeadEvent SET HoldLeadEventID=@LeadEventID WHERE LeadEventID=@LeadEventID
	
	SELECT  
	@CaseID = LeadEvent.CaseID, 
	@LeadID = LeadEvent.LeadID,
	@ClientID = LeadEvent.ClientID,
	@NoteTypeID = LeadEvent.NoteTypeID,
	@WhoCreated = WhoCreated,
	@NotePriority=NotePriority,
	@LeadTypeID=l.LeadTypeID
	FROM LeadEvent 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=LeadEvent.LeadID
	WHERE LeadEventID = @LeadEventID

	/* Apply Note to all cases required for the Note type*/
	SELECT @ApplyToAll = CASE ata.ValueInt
							WHEN 73988 THEN 0
							WHEN 73989 THEN 1
							WHEN 73990 THEN 2
							WHEN 73991 THEN 3
							WHEN 73992 THEN 4 
							WHEN 74486 THEN 5
							WHEN 75850 THEN 6
							END,
		 @AllowMultiple = CASE WHEN amn.ValueInt = 5144 THEN 1 ELSE 0 END
	FROM ResourceListDetailValues ntid WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues amn WITH (NOLOCK) ON ntid.ResourceListID=amn.ResourceListID AND amn.DetailFieldID=176939
	INNER JOIN ResourceListDetailValues ata WITH (NOLOCK) ON ntid.ResourceListID=ata.ResourceListID AND ata.DetailFieldID=176938
	WHERE ntid.DetailFieldID=176936 AND ntid.ValueInt=@NoteTypeID
		
	-- get case list 
	DECLARE @NoteList TABLE (ClientID INT, ID1 INT, ID2 INT)
	
	IF @ApplyToAll = 1
	BEGIN
	
		INSERT INTO @NoteList
		SELECT @ClientID, c2.LeadID, c2.CaseID
		FROM dbo.Cases 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON  l.LeadID = Cases.LeadID
		INNER JOIN dbo.Lead l2 WITH (NOLOCK) ON  l2.LeadID = l.LeadID 
		INNER JOIN dbo.Cases c2 WITH (NOLOCK) ON  c2.leadid = l2.leadid and c2.CaseID <> @CaseID
		INNER JOIN LeadEvent src WITH (NOLOCK) ON src.CaseID=cases.CaseID AND src.LeadEventID=@LeadEventID 
		WHERE Cases.CaseID = @CaseID	
	
	END
	ELSE IF @ApplyToAll = 2
	BEGIN

		INSERT INTO @NoteList
		SELECT @ClientID, c.LeadID,c.CaseID
		FROM dbo.Cases 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON  l.LeadID = Cases.LeadID
		INNER JOIN dbo.Lead l2 WITH (NOLOCK) ON  l2.CustomerID = l.CustomerID AND l2.LeadTypeID=l.LeadTypeID
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON  c.leadid = l2.leadid and c.CaseID <> @CaseID
		INNER JOIN LeadEvent src WITH (NOLOCK) ON src.CaseID=cases.CaseID AND src.LeadEventID=@LeadEventID 
		WHERE Cases.CaseID = @CaseID

	END
	ELSE IF @ApplyToAll = 3
	BEGIN
		
		INSERT INTO @NoteList
		SELECT @ClientID, c.LeadID, c.CaseID
		FROM dbo.Cases 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON  l.LeadID = Cases.LeadID
		INNER JOIN dbo.Lead l2 WITH (NOLOCK) ON  l2.CustomerID = l.CustomerID 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON  c.leadid = l2.leadid and c.CaseID <> @CaseID
		INNER JOIN LeadEvent src WITH (NOLOCK) ON src.CaseID=cases.CaseID AND src.LeadEventID=@LeadEventID 
		WHERE Cases.CaseID = @CaseID

	END
	ELSE IF @ApplyToAll = 4
	BEGIN

		-- Policy Lead ID
		IF @LeadTypeID=1492
		BEGIN
			SELECT @PolicyAdminLeadID=@LeadID
		END
		ELSE IF @LeadTypeID=1493
		BEGIN
			SELECT TOP 1 @PolicyAdminLeadID=pam.LeadID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN Matter clm WITH (NOLOCK) ON ltr.ToMatterID=clm.MatterID AND clm.LeadID=@LeadID
			INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON clm.MatterID=ltr2.ToMatterID AND ltr2.FromLeadTypeID=1492
			INNER JOIN Matter pam WITH (NOLOCK) ON ltr2.FromMatterID=pam.MatterID
			WHERE ltr.ToLeadTypeID=1493 	
		END
		ELSE IF @LeadTypeID=1490
		BEGIN
			SELECT TOP 1 @PolicyAdminLeadID=pam.LeadID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN Matter colm WITH (NOLOCK) ON ltr.ToMatterID=colm.MatterID AND colm.LeadID=@LeadID
			INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON colm.MatterID=ltr2.ToMatterID AND ltr2.FromLeadTypeID=1492
			INNER JOIN Matter pam WITH (NOLOCK) ON ltr2.FromMatterID=pam.MatterID
			WHERE ltr.ToLeadTypeID=1490 	
		END

		-- policy admin
		INSERT INTO @NoteList
		SELECT TOP 1 @ClientID, ca.LeadID, ca.CaseID FROM Cases ca WITH (NOLOCK) WHERE ca.LeadID=@PolicyAdminLeadID

		-- claims
		INSERT INTO @NoteList
		SELECT DISTINCT @ClientID, ca.LeadID,ca.CaseID 
		FROM LeadTypeRelationship ltr  WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON ltr.FromMatterID=ltr2.FromMatterID AND ltr2.ToLeadTypeID=1490
		INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID=ltr2.ToLeadID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID=m.CaseID
		WHERE ltr.ToLeadTypeID=1490 AND ltr.FromLeadID=@PolicyAdminLeadID

		-- collections 
		INSERT INTO @NoteList
		SELECT DISTINCT @ClientID, ca.LeadID,ca.CaseID 
		FROM LeadTypeRelationship ltr  WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON ltr.FromMatterID=ltr2.FromMatterID AND ltr2.ToLeadTypeID=1493
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=ltr2.ToMatterID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID=m.CaseID
		WHERE ltr.ToLeadTypeID=1493 AND ltr.FromLeadID=@PolicyAdminLeadID
		AND 1 = (
		SELECT COUNT(*) FROM MatterDetailValues polend WITH (NOLOCK)
		INNER JOIN LeadTypeRelationship ltr3 WITH (NOLOCK) ON polend.MatterID=ltr3.FromMatterID AND ltr3.ToLeadTypeID=1493
			AND ltr3.ToMatterID=m.MatterID
		WHERE polend.DetailFieldID=170037 
		AND polend.ValueDate >= CONVERT(DATE,dbo.fn_GetDate_Local())
		)

	END
	ELSE IF @ApplyToAll=5 AND @LeadTypeID=1493
	BEGIN
	
		-- policy admin
		INSERT INTO @NoteList
		SELECT @ClientID, m2.LeadID,m2.CaseID 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON m.MatterID=ltr.ToMatterID AND ltr.FromLeadTypeID=1492
		INNER JOIN Matter m2 WITH (NOLOCK) ON ltr.FromMatterID=m2.MatterID
		WHERE m.CaseID=@CaseID
		
		-- claims 
		SELECT @ClientID, ca2.LeadID,ca2.CaseID 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON m.MatterID=ltr.ToMatterID AND ltr.FromLeadTypeID=1492
		INNER JOIN Matter m2 WITH (NOLOCK) ON ltr.FromMatterID=m2.MatterID
		INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON m2.LeadID=ltr2.FromLeadID AND ltr2.ToLeadTypeID=1490
		INNER JOIN Cases ca2 WITH (NOLOCK) ON ltr2.ToLeadID=ca2.LeadID
		WHERE m.CaseID=@CaseID
	
	END
	ELSE IF @ApplyToAll = 6
	BEGIN

		-- Policy Lead ID
		IF @LeadTypeID=1492
		BEGIN
			SELECT @PolicyAdminLeadID=@LeadID
		END
		ELSE IF @LeadTypeID=1493
		BEGIN
			SELECT TOP 1 @PolicyAdminLeadID=pam.LeadID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN Matter clm WITH (NOLOCK) ON ltr.ToMatterID=clm.MatterID AND clm.LeadID=@LeadID
			INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON clm.MatterID=ltr2.ToMatterID AND ltr2.FromLeadTypeID=1492
			INNER JOIN Matter pam WITH (NOLOCK) ON ltr2.FromMatterID=pam.MatterID
			WHERE ltr.ToLeadTypeID=1493 	
		END
		ELSE IF @LeadTypeID=1490
		BEGIN
			SELECT TOP 1 @PolicyAdminLeadID=pam.LeadID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN Matter colm WITH (NOLOCK) ON ltr.ToMatterID=colm.MatterID AND colm.LeadID=@LeadID
			INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON colm.MatterID=ltr2.ToMatterID AND ltr2.FromLeadTypeID=1492
			INNER JOIN Matter pam WITH (NOLOCK) ON ltr2.FromMatterID=pam.MatterID
			WHERE ltr.ToLeadTypeID=1490 	
		END

		-- policy admin
		INSERT INTO @NoteList
		SELECT TOP 1 @ClientID, ca.LeadID, ca.CaseID FROM Cases ca WITH (NOLOCK) WHERE ca.LeadID=@PolicyAdminLeadID

		-- claims
		-- JML #58981 - updated as per _C433_SAN
		INSERT INTO @NoteList
		SELECT @ClientID, ca.LeadID,ca.CaseID 
		FROM LeadTypeRelationship ltr  WITH (NOLOCK) 
		--INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON ltr.FromMatterID=ltr2.FromMatterID AND ltr2.ToLeadTypeID=1490 ---problem line 
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=ltr.ToMatterID  -- new line  
		--INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID=ltr.ToLeadID -- removed
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID=m.CaseID
		WHERE ltr.ToLeadTypeID=1490 AND ltr.FromLeadID=@PolicyAdminLeadID

		-- collections 
		INSERT INTO @NoteList
		SELECT @ClientID, ca.LeadID,ca.CaseID 
		FROM LeadTypeRelationship ltr  WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON ltr.FromMatterID=ltr2.FromMatterID AND ltr2.ToLeadTypeID=1493
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=ltr2.ToMatterID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID=m.CaseID
		WHERE ltr.ToLeadTypeID=1493 AND ltr.FromLeadID=@PolicyAdminLeadID
	

	END

	IF @ApplyToAll <> 0
	BEGIN

		INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, HoldLeadEventID)
		SELECT nl.ClientID, nl.ID1, dbo.fn_GetDate_Local(), @WhoCreated, 0.00, src.Comments, NULL, @NoteTypeID, NULL, dbo.fn_GetDate_Local(), NULL, NULL, nl.ID2 , src.LeadDocumentID, src.NotePriority, NULL, 0, NULL, NULL, @LeadEventID
		FROM @NoteList nl 
		INNER JOIN LeadEvent src WITH (NOLOCK) ON src.ClientID=nl.ClientID AND src.LeadEventID=@LeadEventID 
		AND nl.ID2<>src.CaseID
		AND ( @AllowMultiple = 1 OR
			  ( 
			    @AllowMultiple = 0 AND 
			    0 = ( SELECT COUNT(*) FROM LeadEvent oth WHERE oth.EventDeleted=0 AND oth.NotePriority=src.NotePriority AND oth.NoteTypeID=src.NoteTypeID AND nl.ID2=oth.CaseID ) 
			  )
			)     

	END
	
	-- don't allow duplicates
	IF @AllowMultiple = 0 
		AND 
		1 < ( SELECT COUNT(*) FROM LeadEvent oth WHERE oth.EventDeleted=0 
				AND oth.NotePriority=@NotePriority 
				AND oth.NoteTypeID=@NoteTypeID 
				AND oth.CaseID=@CaseID ) 
	BEGIN

		EXEC DeleteLeadEvent @LeadEventID, @WhoCreated, 'Auto delete', 4, 0
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAN] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SAN] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAN] TO [sp_executeall]
GO
