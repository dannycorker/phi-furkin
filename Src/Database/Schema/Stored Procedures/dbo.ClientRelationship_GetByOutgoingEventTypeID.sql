SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientRelationship table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_GetByOutgoingEventTypeID]
(

	@OutgoingEventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientRelationshipID],
					[ClientRelationshipName],
					[OutgoingClientID],
					[ReceivingClientID],
					[OutgoingLeadTypeID],
					[IncomingLeadTypeID],
					[OutgoingEventTypeID],
					[IncomingEventTypeID],
					[Enabled],
					[ClearLeadRefs]
				FROM
					[dbo].[ClientRelationship] WITH (NOLOCK) 
				WHERE
					[OutgoingEventTypeID] = @OutgoingEventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_GetByOutgoingEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_GetByOutgoingEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_GetByOutgoingEventTypeID] TO [sp_executeall]
GO
