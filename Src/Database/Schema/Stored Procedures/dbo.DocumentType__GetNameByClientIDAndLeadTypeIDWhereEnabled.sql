SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By:  ()
-- Modified By: Paul Richardson to include input and output formats for filtering
-- Modified Date: 08-November-2011
-- Purpose: Select records from the DocumentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType__GetNameByClientIDAndLeadTypeIDWhereEnabled]
(

	@ClientID int,
	@LeadTypeID int = null
)
AS

	SELECT
		[DocumentTypeID],
		[DocumentTypeName],
		[InputFormat],
		[OutputFormat]		
	FROM
		[dbo].[DocumentType]
	WHERE
		[ClientID] = @ClientID 
		AND [Enabled] = 1
		AND (@LeadTypeID IS NULL OR ([LeadTypeID] = @LeadTypeID OR LeadTypeID is null))
	ORDER BY [DocumentTypeName]

	Select @@ROWCOUNT
					
			









GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetNameByClientIDAndLeadTypeIDWhereEnabled] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetNameByClientIDAndLeadTypeIDWhereEnabled] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetNameByClientIDAndLeadTypeIDWhereEnabled] TO [sp_executeall]
GO
