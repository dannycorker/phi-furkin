SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryEditingTable table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingTable_Get_List]

AS


				
				SELECT
					[SqlQueryEditingTableID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryTableName],
					[TableAlias],
					[TableDisplayOrder],
					[JoinType],
					[JoinText],
					[JoinTableID],
					[JoinRTRID],
					[RealSqlQueryTableID]
				FROM
					[dbo].[SqlQueryEditingTable] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Get_List] TO [sp_executeall]
GO
