SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyFieldMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_GetByThirdPartyFieldMappingID]
(

	@ThirdPartyFieldMappingID int   
)
AS


				SELECT
					[ThirdPartyFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[ThirdPartyFieldID],
					[DetailFieldID],
					[ColumnFieldID],
					[ThirdPartyFieldGroupID],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
				WHERE
										[ThirdPartyFieldMappingID] = @ThirdPartyFieldMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_GetByThirdPartyFieldMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_GetByThirdPartyFieldMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_GetByThirdPartyFieldMappingID] TO [sp_executeall]
GO
