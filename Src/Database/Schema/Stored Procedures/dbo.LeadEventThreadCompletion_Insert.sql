SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadEventThreadCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_Insert]
(

	@LeadEventThreadCompletionID int    OUTPUT,

	@ClientID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@FromLeadEventID int   ,

	@FromEventTypeID int   ,

	@ThreadNumberRequired int   ,

	@ToLeadEventID int   ,

	@ToEventTypeID int   
)
AS


				
				INSERT INTO [dbo].[LeadEventThreadCompletion]
					(
					[ClientID]
					,[LeadID]
					,[CaseID]
					,[FromLeadEventID]
					,[FromEventTypeID]
					,[ThreadNumberRequired]
					,[ToLeadEventID]
					,[ToEventTypeID]
					)
				VALUES
					(
					@ClientID
					,@LeadID
					,@CaseID
					,@FromLeadEventID
					,@FromEventTypeID
					,@ThreadNumberRequired
					,@ToLeadEventID
					,@ToEventTypeID
					)
				-- Get the identity value
				SET @LeadEventThreadCompletionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Insert] TO [sp_executeall]
GO
