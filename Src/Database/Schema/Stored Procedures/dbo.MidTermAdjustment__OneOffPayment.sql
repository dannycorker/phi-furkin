SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-07-2016
-- Description:	Midterm adjustment for one off payments:
--				Annual.  Pro-rata adjustment value across the remaining period of the term, 
--				e.g. if the annual premium increases by £30 due to the MTA and two thirds of 
--				the policy year have passed, an additional payment of £10 is due.
-- 2016-08-24 DCM switched the operands in DifferenceProductCost fields so that a price increase is no longer negative
-- 2016-08-24 DCM PurchasedProductPaymentScheduleID into a variable in SELECT SCOPE_IDENTITY()
-- 2016-11-15 DCM Allow just to report the adjustment value not add it to the billing
-- 2016-11-15 DCM Allow for situation where original payment has not yet been billed
-- 2016-11-15 DCM Introduce Billing__RebuildCustomerPaymentSchedule
-- 2016-11-15 DCM Set payment date on new row(s)
-- 2016-11-16 DCM Get existing premium values from purchased product or pruchase product history, as appropriate
-- 2016-12-01 DCM Calculate number of days in term rather than assuming 365
-- 2016-12-01 DCM Configurable whether adjustment date is first day of new premium or last day of old premium
-- 2017-07-17 CPS use PurchasedProductPaymentScheduleTypeID 6 (MTA)
-- 2017-07-31 CPS populate LeadEvent comments
-- 2017-08-22 CPS Set uncollected MTA adjustments to "ignored"
-- =============================================
CREATE PROCEDURE [dbo].[MidTermAdjustment__OneOffPayment]
	
	@MidTermAdjustmentID INT,
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT,
	@RoundedCost NUMERIC (18,2),
	@RoundedCostVAT NUMERIC (18,2),
	@NewPaymentDate DATE,
	@AdjustmentDate DATE,
	@PurchasedProductID INT,
	@AdjustmentValueOnly INT = NULL,
	@AdjustmentValueDFID INT = NULL,
	@OneOffAdjustmentWait INT = NULL,
	@MinimumAdjustmentValue MONEY = NULL,
	@TermAdjustmentValueDFID INT = NULL
	
AS
BEGIN	
PRINT 'MidTermAdjustment__OneOffPayment'	
	SET NOCOUNT ON;

    DECLARE @FromDate DATETIME,
			@ToDate DATETIME
			
	DECLARE @OriginalProductCost NUMERIC(18,2),
			@OriginalProductCostVAT NUMERIC(18,2),
			@DifferenceProductCost NUMERIC(18,2),
			@DifferenceProductCostVAT NUMERIC(18,2),
			@ExistingPaymentStatusID INT,
			@MatterID INT,
			@NewProductCost NUMERIC(18,2),
			@NewProductCostVAT NUMERIC(18,2),
			@NextRegularPaymentDate DATE,
			@NumberOfDaysInTerm INT,
			@NumberOfDaysLeft INT,
			@OriginalProportionGross NUMERIC(18,2)=0,
			@OriginalProportionVAT NUMERIC(18,2)=0,
			@PurchasedProductPaymentScheduleID INT,
			@AccountID INT,
			@CustomerID INT,
			@WhoCreated INT,
			@AdjustmentTermAmount	MONEY
			,@EventComments			VARCHAR(2000) = ''
			,@RowCount				INT
			,@DiscountedDays		INT = 0
			,@LastDiscountDay		DATE
			,@DiscountedDaysLeft	INT = 0
			,@TransactionFee NUMERIC(18,2)
			,@AccountTypeID INT
			,@PaymentTypeLuli INT
			,@PaymentMethod INT
			,@StateCode VARCHAR(2) /*GPR 2020-11-22 for SDPRU-157 | changed from INT*/
			

	/*GPR 2020-11-22 for SDPRU-157*/
	SELECT @StateCode = usa.StateCode
	FROM Customers c WITH (NOLOCK)
	INNER JOIN StateByZip z WITH (NOLOCK) ON z.Zip = c.Postcode
	INNER JOIN UnitedStates usa WITH (NOLOCK) ON usa.StateID = z.StateID
	WHERE c.CustomerID = @CustomerID
	
	SELECT @AdjustmentValueOnly=ISNULL(@AdjustmentValueOnly,0),
			@MinimumAdjustmentValue=ISNULL(@MinimumAdjustmentValue,0)
	
	DECLARE @LogEntry VARCHAR(MAX)
	SELECT @LogEntry = 'AdjustmentValueOnly: ' + CAST(@AdjustmentValueOnly as VARCHAR(10))
	EXEC _C00_logit  'PROC', 'MidTermAdjustment__OneOffPayment','LogEntry',@LogEntry,44412

	DECLARE @TotalAdminFee MONEY /*GPR 2021-02-09*/

	IF @AdjustmentValueOnly=0
	BEGIN

	/*GPR 2021-02-11 removed*/
	SELECT @TotalAdminFee = SUM(pps.PaymentGross)
	FROM dbo.PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
	WHERE pps.PurchasedProductID = @PurchasedProductID
	AND pps.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/
	AND pps.PaymentStatusID <> 3 /*Ignored*/

		-- the previous payment amount has just been added to the history table
		SELECT TOP 1
			   @FromDate = pp.ValidFrom, 
			   @ToDate = pp.ValidTo, 
			   @OriginalProductCost = pp.ProductCostGross + @TotalAdminFee, /*Flipped to minus*/
			   @OriginalProductCostVAT = pp.ProductCostVAT,
			   @AccountID = pp.AccountID,
			   @CustomerID = pp.CustomerID
		FROM PurchasedProductHistory pp WITH (NOLOCK) 
		WHERE pp.PurchasedProductID=@PurchasedProductID
		ORDER BY PurchasedProductHistoryID DESC
	END
	ELSE
	BEGIN
		-- the previous payment amount is still in purchased product
		SELECT TOP 1
			   @FromDate = pp.ValidFrom, 
			   @ToDate = pp.ValidTo, 
			   @OriginalProductCost = pp.ProductCostGross + @TotalAdminFee,
			   @OriginalProductCostVAT = pp.ProductCostVAT,
			   @AccountID = pp.AccountID,
			   @CustomerID = pp.CustomerID
		FROM PurchasedProduct pp WITH (NOLOCK) 
		WHERE pp.PurchasedProductID=@PurchasedProductID
		ORDER BY PurchasedProductID DESC
	END
	
	--SELECT @OriginalProductCost = @OriginalProductCost - ISNULL(dbo.fnGetSimpleDvAsMoney(177484,@MatterID),0.00) /*Initial Admin Fee Charged*/
	
	SELECT @WhoCreated = le.WhoCreated FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID=@LeadEventID
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created

	SELECT TOP 1 @MatterID=MatterID FROM Matter WITH (NOLOCK) WHERE CaseID=@CaseID

	-- need to consider an MTA before the original payment has been billed
	SELECT TOP 1 @NextRegularPaymentDate=ppps.ActualCollectionDate, 
				 @ExistingPaymentStatusID=ppps.PaymentStatusID,
				 @PurchasedProductPaymentScheduleID=ppps.PurchasedProductPaymentScheduleID
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	WHERE ppps.PurchasedProductID=@PurchasedProductID
	AND ppps.PurchasedProductPaymentScheduleTypeID IN ( 1 /*Scheduled*/ , 6 /*MTA*/ )
	AND ppps.PaymentStatusID NOT IN ( 3 /*Ignored*/ )
	ORDER BY ppps.PurchasedProductID DESC

	SELECT top 1 @NewPaymentDate = w.[Date]  FROM WorkingDays w WITH ( NOLOCK ) 
	INNER JOIN BillingConfiguration b on b.ClientID = @ClientID 
	Where w.Date >= CONVERT(DATE,DATEADD(DAY,b.OneOffAdjustmentWait,dbo.fn_GetDate_Local())) 


	--SELECT @NewPaymentDate = DATEADD(DAY,@OneOffAdjustmentWait,dbo.fn_GetDate_Local())
	IF @NewPaymentDate < @NextRegularPaymentDate
		SELECT @NewPaymentDate = @NextRegularPaymentDate

	--calculate the number of days left
	SELECT @NumberOfDaysLeft = DATEDIFF(DAY,@AdjustmentDate,@ToDate) + CASE WHEN ISNULL(bc.AdjustmentDayIsFirstDayOfNewPremium,0)=1 THEN 1 ELSE 0 END,
			@NumberOfDaysInTerm = DATEDIFF(DAY,@FromDate,@ToDate) + CASE WHEN ISNULL(bc.AdjustmentDayIsFirstDayOfNewPremium,0)=1 THEN 1 ELSE 0 END
	FROM BillingConfiguration bc WITH ( NOLOCK )
	WHERE bc.ClientID=@ClientID 
	
	SELECT	 @DiscountedDays = SUM ( DATEDIFF(DAY,pps.CoverFrom,pps.CoverTo)+1 )  /*CPS 2017-09-19*/ 
			,@LastDiscountDay= MAX ( pps.CoverTo )
	FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK ) 
	WHERE pps.PurchasedProductPaymentScheduleTypeID = 4 /*Discount*/
	AND pps.PurchasedProductID = @PurchasedProductID
	
	SELECT @DiscountedDays = ISNULL(@DiscountedDays,0)
	
	SELECT @NumberOfDaysInTerm = @NumberOfDaysInTerm - @DiscountedDays

	SELECT @DiscountedDaysLeft = DATEDIFF(DAY,@AdjustmentDate,@LastDiscountDay)+1
	SELECT @DiscountedDaysLeft = CASE WHEN @DiscountedDaysLeft > 0 THEN @DiscountedDaysLeft ELSE 0 END
	SELECT @NumberOfDaysLeft = @NumberOfDaysLeft - @DiscountedDaysLeft

	SELECT @EventComments += '  Number of days left: ' + ISNULL(CONVERT(VARCHAR,@NumberOfDaysLeft),'NULL') + CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Number of chargeable days in term: ' + ISNULL(CONVERT(VARCHAR,@NumberOfDaysInTerm),'NULL') + CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Number of discounted days in term: ' + ISNULL(CONVERT(VARCHAR,@DiscountedDays),'NULL') + CHAR(13)+CHAR(10)
	SELECT @EventComments += '  New payment date: ' + ISNULL(CONVERT(VARCHAR,@NewPaymentDate),'NULL') + CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Existing payment status: ' + ISNULL(CONVERT(VARCHAR,@ExistingPaymentStatusID),'NULL') + CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Existing payment ID: ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductPaymentScheduleID),'NULL') + CHAR(13)+CHAR(10)
	
	-- if original payment not yet processed we need to calculate the cost for that period, 
	-- and update the existing record.  The overall adjustment value is the sum of the two
	IF @ExistingPaymentStatusID=1 -- payment not yet taken
	BEGIN
	
		SELECT @OriginalProportionGross=ROUND(@OriginalProductCost / @NumberOfDaysInTerm * (@NumberOfDaysInTerm - @NumberOfDaysLeft), 2)
		SELECT @OriginalProportionVAT=ROUND(@OriginalProductCostVAT / @NumberOfDaysInTerm * (@NumberOfDaysInTerm - @NumberOfDaysLeft),2)
		
		SELECT @EventComments += '  Original Proportion Gross: ' + ISNULL(CONVERT(VARCHAR,@OriginalProportionGross),'NULL') + CHAR(13)+CHAR(10)
		
		SELECT @AccountTypeID = a.AccountTypeID
		FROM Account a WITH (NOLOCK) 
		WHERE a.AccountID = @AccountID

		SELECT @PaymentTypeLuli = CASE WHEN @AccountTypeID = 1
				THEN 69930 /*Direct Debit*/
				ELSE 69931 /*Credit Card*/
				END

		SELECT @PaymentMethod = 69943 /*Monthly*/

		SELECT @TransactionFee = ISNULL(dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@StateCode, @PaymentTypeLuli, @PaymentMethod), 0.00)

		UPDATE PurchasedProductPaymentSchedule
		SET ActualCollectionDate=@NewPaymentDate, 
			PaymentNet=@OriginalProportionGross-@OriginalProportionVAT,-- + @TransactionFee, 
			PaymentVAT=@OriginalProportionVAT, 
			PaymentGross=@OriginalProportionGross
			/*ALM 2020-11-25*/--TransactionFee = @TransactionFee /*GPR 2020-11-22*/
		WHERE PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID
		AND @AdjustmentValueOnly = 0 /*CPS 2017-11-02 don't update if we're just trying to calculate the price*/

		SELECT @NewProductCost = ROUND(@RoundedCost / @NumberOfDaysInTerm * @NumberOfDaysLeft, 2)
		SELECT @NewProductCostVAT = ROUND(@RoundedCostVAT / @NumberOfDaysInTerm * @NumberOfDaysLeft,2)
	
		SELECT @AdjustmentTermAmount = @OriginalProductCost - (@OriginalProportionGross + @NewProductCost)
		--SELECT @AdjustmentTermAmount = (@OriginalProportionGross + @NewProductCost) - @OriginalProductCost
		
	END
	ELSE -- payment already taken
	BEGIN
			
		/*CPS 2017-08-22 Set uncollected MTA adjustments to "ignored"*/
		UPDATE pps
		SET PaymentStatusID = 3 /*Ignored*/
		FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK )
		WHERE pps.PurchasedProductID = @PurchasedProductID
		AND pps.PurchasedProductPaymentScheduleTypeID = 6 /*MTA*/
		AND pps.PaymentStatusID = 1 /*New*/
		AND @AdjustmentValueOnly = 0 /*CPS 2017-11-02 don't update if we're just trying to calculate the price*/
		
		SELECT @RowCount = @@ROWCOUNT 
		SELECT @EventComments += '  ' + ISNULL(CONVERT(VARCHAR,@RowCount),'NULL') + ' old MTA adjustments marked as "ignored".' + CHAR(13)+CHAR(10)

		--difference divided by 365 days multiplied by the number of days left			
		SELECT @DifferenceProductCost = @RoundedCost - @OriginalProductCost
		SELECT @DifferenceProductCostVAT = @RoundedCostVAT - @OriginalProductCostVAT
		

		SELECT @NewProductCost = ROUND(@DifferenceProductCost / CAST(@NumberOfDaysInTerm as DECIMAL(18,8)) * CAST(@NumberOfDaysLeft as DECIMAL(18,8)), 2)
		SELECT @NewProductCostVAT = ROUND(@DifferenceProductCostVAT / CAST(@NumberOfDaysInTerm as DECIMAL(18,8)) * CAST(@NumberOfDaysLeft as DECIMAL(18,8)),2)
		
		SELECT @AdjustmentTermAmount = @NewProductCost
		
		SELECT @LogEntry = 'NewProductCost: ' + CAST(@NewProductCost as VARCHAR(10)) + ' DifferenceProductCost ' + CAST(@DifferenceProductCost as VARCHAR(10)) 
		EXEC _C00_logit 'PROC', 'MidTermAdjustment__OneOffPayment','LogEntryForAdjustment',@LogEntry,58552
	
	END
	--create new schedule entries
	DECLARE @CustomerPaymentScheduleID INT
	--DECLARE @PurchasedProductPaymentScheduleID INT
	DECLARE @PaymentStatusID INT = 1 -- default to new payment

	SELECT @EventComments += '  Cost Difference: '	+ ISNULL(CONVERT(VARCHAR,@DifferenceProductCost),'NULL')+ CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Rounded Cost: '		+ ISNULL(CONVERT(VARCHAR,@RoundedCost),'NULL')			+ CHAR(13)+CHAR(10)
	SELECT @EventComments += '  New Cost: '			+ ISNULL(CONVERT(VARCHAR,@NewProductCost),'NULL')		+ CHAR(13)+CHAR(10)
	SELECT @EventComments += '  Old Cost: '			+ ISNULL(CONVERT(VARCHAR,@OriginalProductCost),'NULL')	+ CHAR(13)+CHAR(10)

	-- insert into purchased product payment schedule
	IF @AdjustmentValueOnly=0
	BEGIN

		/*CPS 2017-08-22 and wipe out zero-value payments*/
		UPDATE pps
		SET PaymentStatusID = 3 /*Ignored*/
		FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK ) 
		WHERE pps.PurchasedProductID = @PurchasedProductID
		AND pps.PaymentStatusID = 1 /*New*/
		AND pps.PurchasedProductPaymentScheduleTypeID = 1 /*Scheduled*/
		AND pps.PaymentGross = 0.00
	
		IF ABS(@NewProductCost) >= @MinimumAdjustmentValue
		BEGIN
			IF @NewProductCost < 0.00 
			BEGIN
				SELECT @NewPaymentDate = dbo.fnGetNextWorkingDate(dbo.fn_GetDate_Local(),1)
			END
			
					
			DECLARE @ClientAccountID INT 
			SELECT @ClientAccountID = p.ClientAccountID FROM PurchasedProduct p
			WHERE p.PurchasedProductID = @PurchasedProductID
			
			IF EXISTS (SELECT * FROM Account a WITH ( NOLOCK ) where a.AccountID = @AccountID AND a.AccountTypeID = 2) 
			BEGIN
			
				INSERT PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID, ClientAccountID)
				VALUES (@ClientID, @CustomerID,@AccountID,@PurchasedProductID,dbo.fn_GetDate_Local(),@AdjustmentDate, @ToDate, dbo.fn_GetDate_Local(), @NewProductCost-@NewProductCostVAT, @NewProductCostVAT, @NewProductCost, @PaymentStatusID, NULL, NULL, NULL, @WhoCreated, @WhenCreated, 6 /*MTA*/,@ClientAccountID)
				SELECT @PurchasedProductPaymentScheduleID=SCOPE_IDENTITY()
			
			END
			ELSE 
			BEGIN 
			
				INSERT PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID, ClientAccountID)
				VALUES (@ClientID, @CustomerID,@AccountID,@PurchasedProductID,@NewPaymentDate,@AdjustmentDate, @ToDate, @NewPaymentDate, @NewProductCost-@NewProductCostVAT, @NewProductCostVAT, @NewProductCost, @PaymentStatusID, NULL, NULL, NULL, @WhoCreated, @WhenCreated, 6 /*MTA*/,@ClientAccountID )
				SELECT @PurchasedProductPaymentScheduleID=SCOPE_IDENTITY()

			END 	
			
			-- finally rebuild the customer payment schedule
			EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @AdjustmentDate, @WhoCreated
			
			SELECT @EventComments += '  New PaymentID: ' + ISNULL(CONVERT(VARCHAR,@PurchasedProductPaymentScheduleID),'NULL') + CHAR(13)+CHAR(10)
	
		END
		
	END
	ELSE
	BEGIN

		SELECT @NewProductCost=@NewProductCost+@OriginalProportionGross
		EXEC _C00_SimpleValueIntoField @AdjustmentValueDFID,@NewProductCost,@MatterID
		
		SELECT @EventComments += '  New Product Cost: ' + ISNULL(CONVERT(VARCHAR,@NewProductCost),'NULL') + CHAR(13)+CHAR(10)

		-- remaining term adjustment amount
		--EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@AdjustmentTermAmount,@MatterID	
		--SELECT @DifferenceProductCost = @RoundedCost - @OriginalProductCost
		IF @ExistingPaymentStatusID=1
		BEGIN 
		
			SELECT @DifferenceProductCost = @NewProductCost - @OriginalProductCost
		
		END 
		
		EXEC _C00_SimpleValueIntoField @TermAdjustmentValueDFID,@DifferenceProductCost,@MatterID	
		
		SELECT @EventComments += '  Difference Product Cost: ' + ISNULL(CONVERT(VARCHAR,@DifferenceProductCost),'NULL') + CHAR(13)+CHAR(10)
		
	END
	
	EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[MidTermAdjustment__OneOffPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MidTermAdjustment__OneOffPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MidTermAdjustment__OneOffPayment] TO [sp_executeall]
GO
