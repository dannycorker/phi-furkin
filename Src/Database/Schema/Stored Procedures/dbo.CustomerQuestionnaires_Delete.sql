SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerQuestionnaires_Delete]
(

	@CustomerQuestionnaireID int   
)
AS


				DELETE FROM [dbo].[CustomerQuestionnaires] WITH (ROWLOCK) 
				WHERE
					[CustomerQuestionnaireID] = @CustomerQuestionnaireID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerQuestionnaires_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Delete] TO [sp_executeall]
GO
