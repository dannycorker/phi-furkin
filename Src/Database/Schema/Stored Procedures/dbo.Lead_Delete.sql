SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Lead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_Delete]
(

	@LeadID int   
)
AS


				DELETE FROM [dbo].[Lead] WITH (ROWLOCK) 
				WHERE
					[LeadID] = @LeadID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Delete] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Delete] TO [sp_executeall]
GO
