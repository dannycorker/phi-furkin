SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-11-18
-- Description:	Sql After Event - begin - Add or Remove MultiPet Discount
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_159022]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@TermsDate						DATE
			,@AdjustmentDate				DATE
			,@Count							INT
			,@ValueInt						INT
			,@DetailValueData				dbo.tvpDetailValueData
			,@EventComments					VARCHAR(2000)
			,@NextEventID					INT
			,@IdId							dbo.tvpIntInt
			,@WorkflowTaskID				INT

			DECLARE @NumberedValues TABLE ( ID INT Identity, Value VARCHAR(2000) )


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	DECLARE @PetCount INT

	IF @EventTypeID = 159022
	BEGIN
	
		/*IF count of Policy Matter for Customer is <=1 then the Policy is not eligible for MPD (add or remove).*/
		IF (SELECT COUNT(*) FROM Matter m WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID
			INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
			WHERE l.LeadTypeID = 1492
			AND l.CustomerID = @CustomerID
			AND c.ClientID = @ClientID) <=1
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>Policy is not eligible for MultiPet Discount.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		/*copy adjustment date to field to use for docs*/ /*NG 2020-12-09 for JIRA SDPRU-187*/
		EXEC _C600_CopyFieldToField 175442, 314353, @MatterID, @WhoCreated /*Adjustment or Cancellation Date => MTA Date*/

		-- GPR 2020-11-18 Removed as this is now handled on the Event as a user Toggle for Acceptance Criteria 2.
		--/*Get PetCount*/
		--SELECT @PetCount = COUNT(*)
		--FROM Lead l WITH (NOLOCK) 
		--INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.LeadID = l.LeadID AND mdv.DetailFieldID = 170038 /*Policy Status*/
		--INNER JOIN MatterDetailValues product WITH ( NOLOCK ) ON product.LeadID = l.LeadID AND product.DetailFieldID = 170034 /*Current Policy*/
		--WHERE l.CustomerID = @CustomerID
		--AND l.LeadTypeID = 1492 /*Policy Admin*/
		--AND mdv.ValueInt = 43002

		--/*Remove MPD*/
		--IF @PetCount = 1
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 179923, 5145, @MatterID
		--END
		--ELSE /*Add MPD*/
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 179923, 5144, @MatterID
		--END

		
	END
		


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
