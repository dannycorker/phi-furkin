SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DashboardItemType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_Update]
(

	@DashboardItemTypeID int   ,

	@DashboardItemTypeName int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DashboardItemType]
				SET
					[DashboardItemTypeName] = @DashboardItemTypeName
					,[ClientID] = @ClientID
				WHERE
[DashboardItemTypeID] = @DashboardItemTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Update] TO [sp_executeall]
GO
