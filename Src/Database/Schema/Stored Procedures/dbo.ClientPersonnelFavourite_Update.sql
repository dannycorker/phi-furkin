SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPersonnelFavourite table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite_Update]
(

	@ClientPersonnelFavouriteID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@LeadID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPersonnelFavourite]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[LeadID] = @LeadID
				WHERE
[ClientPersonnelFavouriteID] = @ClientPersonnelFavouriteID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Update] TO [sp_executeall]
GO
