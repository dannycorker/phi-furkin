SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailFields table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_Insert]
(

	@DetailFieldID int    OUTPUT,

	@ClientID int   ,

	@LeadOrMatter tinyint   ,

	@FieldName varchar (50)  ,

	@FieldCaption varchar (100)  ,

	@QuestionTypeID int   ,

	@Required bit   ,

	@Lookup bit   ,

	@LookupListID int   ,

	@LeadTypeID int   ,

	@Enabled bit   ,

	@DetailFieldPageID int   ,

	@FieldOrder int   ,

	@MaintainHistory bit   ,

	@EquationText varchar (2000)  ,

	@MasterQuestionID int   ,

	@FieldSize int   ,

	@LinkedDetailFieldID int   ,

	@ValidationCriteriaFieldTypeID int   ,

	@ValidationCriteriaID int   ,

	@MinimumValue varchar (50)  ,

	@MaximumValue varchar (50)  ,

	@RegEx varchar (2000)  ,

	@ErrorMessage varchar (250)  ,

	@ResourceListDetailFieldPageID int   ,

	@TableDetailFieldPageID int   ,

	@DefaultFilter varchar (250)  ,

	@ColumnEquationText varchar (2000)  ,

	@Editable bit   ,

	@Hidden bit   ,

	@LastReferenceInteger int   ,

	@ReferenceValueFormatID int   ,

	@Encrypt bit   ,

	@ShowCharacters int   ,

	@NumberOfCharactersToShow int   ,

	@TableEditMode int   ,

	@DisplayInTableView bit   ,

	@ObjectTypeID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@DetailFieldStyleID int   ,

	@Hyperlink nvarchar (2000)  ,

	@IsShared bit   
)
AS


				
				INSERT INTO [dbo].[DetailFields]
					(
					[ClientID]
					,[LeadOrMatter]
					,[FieldName]
					,[FieldCaption]
					,[QuestionTypeID]
					,[Required]
					,[Lookup]
					,[LookupListID]
					,[LeadTypeID]
					,[Enabled]
					,[DetailFieldPageID]
					,[FieldOrder]
					,[MaintainHistory]
					,[EquationText]
					,[MasterQuestionID]
					,[FieldSize]
					,[LinkedDetailFieldID]
					,[ValidationCriteriaFieldTypeID]
					,[ValidationCriteriaID]
					,[MinimumValue]
					,[MaximumValue]
					,[RegEx]
					,[ErrorMessage]
					,[ResourceListDetailFieldPageID]
					,[TableDetailFieldPageID]
					,[DefaultFilter]
					,[ColumnEquationText]
					,[Editable]
					,[Hidden]
					,[LastReferenceInteger]
					,[ReferenceValueFormatID]
					,[Encrypt]
					,[ShowCharacters]
					,[NumberOfCharactersToShow]
					,[TableEditMode]
					,[DisplayInTableView]
					,[ObjectTypeID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[DetailFieldStyleID]
					,[Hyperlink]
					,[IsShared]
					)
				VALUES
					(
					@ClientID
					,@LeadOrMatter
					,@FieldName
					,@FieldCaption
					,@QuestionTypeID
					,@Required
					,@Lookup
					,@LookupListID
					,@LeadTypeID
					,@Enabled
					,@DetailFieldPageID
					,@FieldOrder
					,@MaintainHistory
					,@EquationText
					,@MasterQuestionID
					,@FieldSize
					,@LinkedDetailFieldID
					,@ValidationCriteriaFieldTypeID
					,@ValidationCriteriaID
					,@MinimumValue
					,@MaximumValue
					,@RegEx
					,@ErrorMessage
					,@ResourceListDetailFieldPageID
					,@TableDetailFieldPageID
					,@DefaultFilter
					,@ColumnEquationText
					,@Editable
					,@Hidden
					,@LastReferenceInteger
					,@ReferenceValueFormatID
					,@Encrypt
					,@ShowCharacters
					,@NumberOfCharactersToShow
					,@TableEditMode
					,@DisplayInTableView
					,@ObjectTypeID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@DetailFieldStyleID
					,@Hyperlink
					,@IsShared
					)
				-- Get the identity value
				SET @DetailFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Insert] TO [sp_executeall]
GO
