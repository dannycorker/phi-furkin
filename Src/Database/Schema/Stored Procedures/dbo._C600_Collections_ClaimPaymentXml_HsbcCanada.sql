SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2021-02-01
-- Description:	Retrieve an XML package to send for EFT
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_ClaimPaymentXml_HsbcCanada]
(
	 @XmlData			XML
	,@ClientAccountID	INT = 2 
	,@Debug				BIT = 0
	,@FileName			VARCHAR(500)
	,@XmlOutput			XML = NULL OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;

	IF @XmlData is NULL
	BEGIN
		SET @XmlData = 
		'<a>
		  <row>
			<AppliedLeadEventID>0</AppliedLeadEventID>
			<CustomerPaymentScheduleID>46075</CustomerPaymentScheduleID>
			<CustomerID>143186</CustomerID>
			<LeadEventID />
			<ColMatterID />
			<AccountID>4418</AccountID>
			<Reference>_C600_ClaimDataImport</Reference>
			<Amount>120.00</Amount>
			<PaymentID>541813</PaymentID>
			<RNO>1</RNO>
			<AccountHolderName>CpsAccountName</AccountHolderName>
			<AccountNumber>10080239</AccountNumber>
			<SortCode>163424</SortCode>
			<TransactionCode>13</TransactionCode>
			<SUN>111</SUN>
			<ClientAccountNo>12345622</ClientAccountNo>
			<ClientSortCode>123452</ClientSortCode>
			<ClientAccountID>2</ClientAccountID>
			<UsersName>Bob</UsersName>
			<ReceiptOrPayment>2</ReceiptOrPayment>
			<CustomerRefNum />
			<TransactionId />
			<AccountRef>_C600_ClaimDataImport</AccountRef>
			<CardTransactionID />
			<InstitutionNumber>333</InstitutionNumber>
		  </row>
		</a>'
	END

	/*
	TERMINOLOGY

	Routing Number ~= Sort Code
		- 1 digit leading zero
		- 3 digit Financial institution number identifies the bank
		- 5 digit Transit number identifies the branch

	Debtor = Insurance Provider (Furkin or PHI Direct)

	Creditor = Customer or Vet (payee)
	*/

	DECLARE	 @TransactionCount	INT
			,@TransactionSum	DECIMAL(18,2)
			,@FriendlyName		VARCHAR(20)
			,@DebtorAccountNo	VARCHAR(35)
			,@HsbcMemberID		VARCHAR(35)
			,@ClientID			INT
			,@EftID				VARCHAR(10) = '3520794011'

	SELECT	 @HsbcMemberID		= ca.SortCode
			,@DebtorAccountNo	= RIGHT('000000000' + ca.AccountNumber,9)
			,@FriendlyName		= ca.FriendlyName
	FROM ClientAccount ca WITH (NOLOCK) 
	WHERE ca.ClientAccountID = @ClientAccountID

	SELECT	 @ClientID = ISNULL( dbo.fnGetPrimaryClientID(),0 )

	DECLARE	 @XmlGroupHeader			XML -- Block header encapsulating the whole payment instruction
			,@XmlPaymentInformation		XML -- Details of the account from which we will be taking funds
			,@XmlCreditTransaction		XML -- Details of the accounts to which we will be delivering funds

	/*
	CREDIT TRANSFER TRANSACTION INFORMATION BLOCK

	The CreditTransferTransactionInformation tag contains all information for the actual payment and the
	beneficiary party. The number of occurrences of this tag in the message equals the number of payment
	transactions to be executed. The transaction information refers to the credit information of the
	PaymentInformation block it is belonging to. Please note that not all properties available at both
	PaymentInformation and transaction level may be used concurrently. The payment type information may only
	be present at one of these levels (see PaymentTypeInformationRule in the ISO Message Definition Report).
	HSBC recommends the Payment Type Information be provided at the PaymentInformation level
	*/
	SELECT @XmlCreditTransaction = 
	(
	SELECT
			 payRow.value('(CustomerPaymentScheduleID)[1]','INT')		[PmtId/InstrId]
			,payRow.value('(PaymentID)[1]','INT')						[PmtId/EndToEndId]

			-- Amount must be provided at either Instructed Amount or Equivalent Amount but not both
			,'CAD'														[Amt/InstdAmt/@Ccy]
			,payRow.value('(Amount)[1]','DECIMAL(18,2)')				[Amt/InstdAmt]
																		
			-- Bank of the creditor. Mandatory as the identification of the creditor bank is required
			-- This will be the 9 digit local routing code where the first four digits are the bank ID and the last five digits represent the transit
			,ISNULL(payRow.value('(InstitutionNumber)[1]','VARCHAR(3)'),'') +
			 payRow.value('(SortCode)[1]','VARCHAR(12)')				[CdtrAgt/FinInstnId/ClrSysMmbId/MmbId]
			,'CA'														[CdtrAgt/FinInstnId/PstlAdr/Ctry]
																		
			-- Creditor of the payment. See section 2.4.4.8.
			-- HSBC Canada local clearing code. First four digits are always '0016' for HSBC Bank Canada. If local routing code is used, the 9 digit account number must be used in DebtorAccount
			--,payRow.value('(SortCode)[1]','VARCHAR(12)')				[Cdtr/FinInstnId/ClrSysMmbId/MmbId]
			,payRow.value('(AccountHolderName)[1]','VARCHAR(30)')		[Cdtr/Nm]
			,'CA'														[Cdtr/PstlAdr/Ctry]

			-- Creditor account at the beneficiary bank (account holder). See section 2.4.4.
			,RIGHT('000000000' + payRow.value('(AccountNumber)[1]','VARCHAR(12)'),9)	[CdtrAcct/Id/Othr/Id]
			
	FROM @XmlData.nodes('(a/row)') a(payRow)
	FOR XML PATH ('CdtTrfTxInf')
	)

	-- Count and Sum the credit transactions for control values
	SELECT	 @TransactionSum	= SUM(b.value('(InstdAmt)[1]','DECIMAL(18,2)'))
			,@TransactionCount	= COUNT(*)
	FROM @XmlCreditTransaction.nodes('CdtTrfTxInf/Amt') a(b)

	/*
	SELECT PAYMENT INFORMATION BLOCK
	The payment information block contains the debit information
	These are the banking details for the insurer
	*/
	SELECT @XmlPaymentInformation = 
	(
	SELECT TOP 1
			-- Will be reported in Payment Status Report.
			 @FileName											[PmtInfId]
			,'TRF'												[PmtMtd]
			,@TransactionCount									[NbOfTxs]
			,@TransactionSum									[CtrlSum]
			,'NURG'												[PmtTpInf/SvcLvl/Cd]
			,450												[PmtTpInf/LclInstrm/Cd]
			,CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121)	[ReqdExctnDt]
			,@FriendlyName										[Dbtr/Nm]
			,'CA'												[Dbtr/PstlAdr/Ctry]
			-- Code allocated to a financial institution or non financial institution by the ISO 9362 Registration Authority as described in ISO 9362 "Banking - Banking telecommunication messages - Business identifier code (BIC)".
			,@EftID												[Dbtr/Id/OrgId/Othr/Id]

			-- Unambiguous identification of the account of the debtor to which a debit entry will be made as a result of the transaction
			,@DebtorAccountNo									[DbtrAcct/Id/Othr/Id]
			-- Code can be either CACC (Current) or SVGS (Savings) can be provided by customer.  Default value is CACC (Current).  Appears to be ignored, but is included in sample
			,'CACC'												[DbtrAcct/Tp/Cd]

			-- Swift BIC or local bank code of the HSBC branch servicing the debit account must be provided. See section 2.4.3.4 for details
			-- HSBC Canada local clearing code. First four digits are always ‘0016’ for HSBC Bank Canada. If local routing code is used, the 9 digit account number must be used in DebtorAccount.
			,@HsbcMemberID										[DbtrAgt/FinInstnId/ClrSysMmbId/MmbId]
			-- Debtor Name is mandatory. Your company’s name (15 digits) to appear on Creditor’s bank statement.
			,@FriendlyName										[DbtrAgt/FinInstnId/Nm]
			,'CA'												[DbtrAgt/FinInstnId/PstlAdr/Ctry]

			-- append one or more transaction records in XML form
			,@XmlCreditTransaction																		

	FOR XML PATH ('PmtInf')
	)
	
	/*
	GROUP HEADER

	The group header contains information pertaining to the entire message. The Initiating Party is required as
	per the ISO 20022 standard; for HSBC the <OrgId><Othr><Id> is used to indicate the HSBC Connect
	Customer ID. The Customer ID is mandatory and is used by HSBC Connect to identify the message sender.
	*/
	SELECT @XmlGroupHeader =
	(
	SELECT TOP 1
			-- Unique message reference used with status messages returned for the processing of this file. HSBC recommends this is unique for a 12 monthly rolling period
			 LEFT(dbo.fnStripCharacters(CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),'-, ,:'),12)	[GrpHdr/MsgId]
			,CONVERT(VARCHAR,dbo.fn_GetDate_Local(),127)											[GrpHdr/CreDtTm]

			-- Indicates a file has been pre authorised or approved within the originating customer environment and no further approval is required
			,'AUTH'																					[GrpHdr/Authstn/Cd]
			
			,@TransactionCount																		[GrpHdr/NbOfTxs]
			,@TransactionSum																		[GrpHdr/CtrlSum]
			,@FriendlyName																			[GrpHdr/InitgPty/Id/OrgId/Othr/Id]

	FOR XML PATH ('')
	)

	/*Include the HSBC namespace*/
	--<?xml version="1.0" encoding="UTF-8"?>
	;WITH XMLNAMESPACES (DEFAULT 'urn:iso:std:iso:20022:tech:xsd:pain.001.001.03', 'http://www.w3.org/2001/XMLSchema-instance' as xsi)


	/*Combine the XML packages into something we can export*/
	SELECT @XmlOutput =
	(
		SELECT	 @XmlGroupHeader		
				,@XmlPaymentInformation	
		FOR XML PATH ('CstmrCdtTrfInitn'), ROOT('Document')
	)

	IF @Debug = 1
	BEGIN
		SELECT	 @XmlCreditTransaction	[@XmlCreditTransaction]
				,@XmlGroupHeader		[@XmlGroupHeader]
				,@XmlPaymentInformation	[@XmlPaymentInformation]
				,@XmlData				[@XmlData]
				,@XmlOutput				[@XmlOutput]
	END

	EXEC _C00_LogItXML @ClientID, @ClientAccountID, '_C600_Collections_ClaimPaymentXml_HsbcCanada', @XmlOutput, @FileName

END






GO
