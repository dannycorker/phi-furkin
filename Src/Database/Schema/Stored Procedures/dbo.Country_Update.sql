SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Country table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_Update]
(

	@CountryID int   ,

	@OriginalCountryID int   ,

	@CountryName varchar (255)  ,

	@CountryNamePlain varchar (255)  ,

	@Alpha2Code char (2)  ,

	@Alpha3Code char (3)  ,

	@NumericCode smallint   ,

	@ISO3166Dash2Code char (13)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@UsePostcode bit   ,

	@PostcodeRegex varchar (500)  ,

	@DiallingCode varchar (4)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Country]
				SET
					[CountryID] = @CountryID
					,[CountryName] = @CountryName
					,[CountryNamePlain] = @CountryNamePlain
					,[Alpha2Code] = @Alpha2Code
					,[Alpha3Code] = @Alpha3Code
					,[NumericCode] = @NumericCode
					,[ISO3166Dash2Code] = @ISO3166Dash2Code
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
					,[UsePostcode] = @UsePostcode
					,[PostcodeRegex] = @PostcodeRegex
					,[DiallingCode] = @DiallingCode
				WHERE
[CountryID] = @OriginalCountryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Update] TO [sp_executeall]
GO
