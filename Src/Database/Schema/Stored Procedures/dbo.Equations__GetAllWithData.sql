SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-02-08
-- Description:	Get Equationy stuff for PR
-- 2011-02-17 JWG Fixed column types in table definition. Join to the right DetailFields record (df_target) or everything looks like an equation rather than a lookup list etc
-- MODIFIED:	2014-07-21	SB	Updated to use view
-- MODIFIED:	2015-02-04	ACE	Updated to use customer tables (IE "IN (@LeadTypeID,0)") #30916
-- =============================================
CREATE PROCEDURE [dbo].[Equations__GetAllWithData]
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@ClientID INT,
	@ClientPersonnelID INT = NULL,
	@ContactID INT = NULL,
	@LeadTypeID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DetailFields TABLE (
		[DetailFieldID] [INT],
		[ClientID] [INT] ,
		[LeadOrMatter] [TINYINT] ,
		[FieldName] [VARCHAR](50) ,
		[FieldCaption] [VARCHAR](100) ,
		[QuestionTypeID] [INT] ,
		[Required] [BIT] ,
		[Lookup] [BIT] ,
		[LookupListID] [INT] ,
		[LeadTypeID] [INT] ,
		[Enabled] [BIT] ,
		[DetailFieldPageID] [INT] ,
		[FieldOrder] [INT] ,
		[MaintainHistory] [BIT] ,
		[EquationText] [VARCHAR](2000) ,
		[MasterQuestionID] [INT] ,
		[FieldSize] [INT] ,
		[LinkedDetailFieldID] [INT] ,
		[ValidationCriteriaFieldTypeID] [INT] ,
		[ValidationCriteriaID] [INT] ,
		[MinimumValue] [VARCHAR](50) ,
		[MaximumValue] [VARCHAR](50) ,
		[RegEx] [VARCHAR](2000) ,
		[ErrorMessage] [VARCHAR](250) ,
		[ResourceListDetailFieldPageID] [INT] ,
		[TableDetailFieldPageID] [INT] ,
		[DefaultFilter] [VARCHAR](250) ,
		[ColumnEquationText] [VARCHAR](2000) ,
		[Editable] [BIT] ,
		[Hidden] [BIT] ,
		[LastReferenceInteger] [INT] ,
		[ReferenceValueFormatID] [INT] ,
		[Encrypt] [BIT] ,
		[ShowCharacters] [INT] ,
		[NumberOfCharactersToShow] [INT] ,
		[TableEditMode] [INT] ,
		[DisplayInTableView] [BIT] 
	)

	EXEC dbo.DetailValues__CreateAll @ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientPersonnelID, @ContactID

	INSERT INTO @DetailFields
	EXEC dbo.DetailFields__GetAllByID @CustomerID, @LeadID, @CaseID, @MatterID, @ClientID, @ClientPersonnelID, @ContactID

	/*Add this on live*/
    INSERT INTO @DetailFields
    SELECT df_table.[DetailFieldID],df_table.[ClientID],df_table.[LeadOrMatter],df_table.[FieldName],df_table.[FieldCaption],df_table.[QuestionTypeID],df_table.[Required],df_table.[Lookup],df_table.[LookupListID],df_table.[LeadTypeID],df_table.[Enabled],df_table.[DetailFieldPageID],df_table.[FieldOrder],df_table.[MaintainHistory],df_table.[EquationText],df_table.[MasterQuestionID],df_table.[FieldSize],df_table.[LinkedDetailFieldID],df_table.[ValidationCriteriaFieldTypeID],df_table.[ValidationCriteriaID],df_table.[MinimumValue],df_table.[MaximumValue],df_table.[RegEx],df_table.[ErrorMessage],df_table.[ResourceListDetailFieldPageID],df_table.[TableDetailFieldPageID],df_table.[DefaultFilter],df_table.[ColumnEquationText],df_table.[Editable],df_table.[Hidden],df_table.[LastReferenceInteger],df_table.[ReferenceValueFormatID],df_table.[Encrypt],df_table.[ShowCharacters],df_table.[NumberOfCharactersToShow],df_table.[TableEditMode],df_table.[DisplayInTableView]
    from dbo.fnDetailFieldsShared(@ClientID) df 
    INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df_table ON df_table.DetailFieldPageID = df.TableDetailFieldPageID and df_table.QuestionTypeID = 10
    where df.LeadTypeID IN (@LeadTypeID,0)
    and df.QuestionTypeID IN (16,19)
    
	/* 
		The app demands an int for LeadOrMatter, so it falls over trying to convert 1 to an int.
		This CAST helps it out.
	*/
	SELECT df.DetailFieldID, CAST(df.LeadOrMatter as int) as [LeadOrMatter], df.EquationText
	FROM @DetailFields df
	WHERE df.QuestionTypeID = 10
	ORDER BY df.FieldOrder

	SELECT e.EquationDetailFieldID, e.DetailFieldID, e.ColumnFieldID, e.Target, e.IsEquation, e.IsColumnSum, e.DetailFieldSubTypeID, 
		ISNULL( CASE df_target.QuestionTypeID  
		WHEN 4 THEN COALESCE(luli_cdv.ItemValue, luli_ldv.ItemValue, luli_cadv.ItemValue, luli_mdv.ItemValue, luli_cldv.ItemValue, luli_cpdv.ItemValue, luli_codv.ItemValue) 
		WHEN 2 THEN COALESCE(luli_cdv.ItemValue, luli_ldv.ItemValue, luli_cadv.ItemValue, luli_mdv.ItemValue, luli_cldv.ItemValue, luli_cpdv.ItemValue, luli_codv.ItemValue) 
--		WHEN 5 THEN CONVERT(VARCHAR(10),COALESCE(cdv.ValueDate, ldv.ValueDate, cadv.ValueDate, mdv.ValueDate, cldv.ValueDate, cpdv.ValueDate, codv.ValueDate),103)
		ELSE COALESCE(cdv.DetailValue, ldv.DetailValue, cadv.DetailValue, mdv.DetailValue, cldv.DetailValue, cpdv.DetailValue, codv.DetailValue) 
		END, '') AS DetailValue
	FROM @DetailFields df
	INNER JOIN EquationTarget e WITH (NOLOCK) ON e.EquationDetailFieldID = df.DetailFieldID
	LEFT JOIN fnDetailFieldsShared(@ClientID) df_target ON df_target.DetailFieldID = e.DetailFieldID
	LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK)  ON cdv.DetailFieldID = e.DetailFieldID AND cdv.CustomerID = @CustomerID
	LEFT JOIN LookupListItems luli_cdv WITH (NOLOCK) ON cdv.ValueInt = luli_cdv.LookupListItemID AND luli_cdv.LookupListID = df_target.LookupListID
	LEFT JOIN LeadDetailValues ldv WITH (NOLOCK)  ON ldv.DetailFieldID = e.DetailFieldID AND ldv.LeadID = @LeadID
	LEFT JOIN LookupListItems luli_ldv WITH (NOLOCK) ON ldv.ValueInt = luli_ldv.LookupListItemID AND luli_ldv.LookupListID = df_target.LookupListID
	LEFT JOIN CaseDetailValues cadv WITH (NOLOCK)  ON cadv.DetailFieldID = e.DetailFieldID AND cadv.CaseID = @CaseID
	LEFT JOIN LookupListItems luli_cadv WITH (NOLOCK) ON cadv.ValueInt = luli_cadv.LookupListItemID AND luli_cadv.LookupListID = df_target.LookupListID
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK)  ON mdv.DetailFieldID = e.DetailFieldID AND mdv.MatterID = @MatterID
	LEFT JOIN LookupListItems luli_mdv WITH (NOLOCK) ON mdv.ValueInt = luli_mdv.LookupListItemID AND luli_mdv.LookupListID = df_target.LookupListID
	LEFT JOIN ClientDetailValues cldv WITH (NOLOCK)  ON cldv.DetailFieldID = e.DetailFieldID AND cldv.ClientID = @ClientID
	LEFT JOIN LookupListItems luli_cldv WITH (NOLOCK) ON cldv.ValueInt = luli_cldv.LookupListItemID AND luli_cldv.LookupListID = df_target.LookupListID
	LEFT JOIN ClientPersonnelDetailValues cpdv WITH (NOLOCK)  ON cpdv.DetailFieldID = e.DetailFieldID AND cpdv.ClientPersonnelID = @ClientPersonnelID
	LEFT JOIN LookupListItems luli_cpdv WITH (NOLOCK) ON cpdv.ValueInt = luli_cpdv.LookupListItemID AND luli_cpdv.LookupListID = df_target.LookupListID
	LEFT JOIN ContactDetailValues codv WITH (NOLOCK)  ON codv.DetailFieldID = e.DetailFieldID AND codv.ContactID = @ContactID
	LEFT JOIN LookupListItems luli_codv WITH (NOLOCK) ON codv.ValueInt = luli_codv.LookupListItemID AND luli_codv.LookupListID = df_target.LookupListID
	WHERE df.QuestionTypeID = 10

END


GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__GetAllWithData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations__GetAllWithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations__GetAllWithData] TO [sp_executeall]
GO
