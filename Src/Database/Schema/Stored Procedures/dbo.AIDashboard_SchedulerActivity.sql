SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-17
-- Description:	AI Dashboard - Scheduled Tasks
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard_SchedulerActivity] 
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	/* 
		Scheduler details 
		Show all known schedulers along with how many tasks are locked to each
	*/
	IF @SummaryOnly = 1
	BEGIN
		SELECT CASE WHEN MAX(DATEDIFF(MINUTE, dbo.fn_GetDate_Local(), ati.LockDateTime)) > 60 THEN 'Red' WHEN MAX(DATEDIFF(MINUTE, dbo.fn_GetDate_Local(), ati.LockDateTime)) > 5 THEN 'Amber' ELSE 'Green' END  AS [Status] 
		FROM dbo.AutomatedTaskInfo ati WITH (NOLOCK) 
		WHERE ati.LockDateTime IS NOT NULL 
	END
	ELSE
	BEGIN
		;WITH LockedTasks AS 
		(
			SELECT ati.*, at.AlreadyRunning, ROW_NUMBER() OVER(PARTITION BY ati.QueueID ORDER BY ati.LockDateTime ) as rn 
			FROM dbo.AutomatedTaskInfo ati WITH (NOLOCK) 
			INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) ON at.TaskID = ati.TaskID 
			WHERE ati.LockDateTime IS NOT NULL 
		),
		LockedTaskCounts AS 
		(
			SELECT lt.QueueID, lt.SchedulerID, lt.ServerName, count(*) as [LockedTaskCount]
			FROM LockedTasks lt 
			GROUP BY lt.QueueID, lt.SchedulerID, lt.ServerName 
		) 
		SELECT atq.AutomatedTaskQueueID AS [QID],
		CASE 
			WHEN lt.LockDateTime IS NULL THEN 'Green' 
			WHEN DATEDIFF(MINUTE, dbo.fn_GetDate_Local(), lt.LockDateTime) > 60 THEN 'Red' 
			WHEN DATEDIFF(MINUTE, dbo.fn_GetDate_Local(), lt.LockDateTime) > 5 THEN 'Amber' 
			ELSE 'Green' 
		END AS [QueueStatus], 
		CASE WHEN CHARINDEX(' ', atq.[Description]) > 0 THEN  LEFT(atq.[Description], (CHARINDEX(' ', atq.[Description]) - 1)) ELSE atq.[Description] END AS [Type],
		ltc.SchedulerID AS [SID],
		ltc.ServerName,
		ltc.LockedTaskCount AS [TODO],
		lt.ClientID AS [Client],
		lt.TaskID AS [Task],
		lt.LockDateTime AS [Locked Since],
		lt.RunCount AS [Runs],
		lt.LastRundate,
		lt.LastRuntime AS [Last],
		lt.MaxRuntime AS [Max],
		lt.AvgRuntime AS [Avg]
		FROM dbo.AutomatedTaskQueue atq WITH (NOLOCK) 
		LEFT JOIN LockedTaskCounts ltc ON ltc.QueueID = atq.AutomatedTaskQueueID 
		LEFT JOIN LockedTasks lt ON lt.QueueID = atq.AutomatedTaskQueueID AND lt.rn = 1 
		WHERE atq.[Enabled] = 1
		ORDER BY atq.AutomatedTaskQueueID, ltc.SchedulerID 
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_SchedulerActivity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard_SchedulerActivity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_SchedulerActivity] TO [sp_executeall]
GO
