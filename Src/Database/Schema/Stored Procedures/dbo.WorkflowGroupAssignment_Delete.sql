SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the WorkflowGroupAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroupAssignment_Delete]
(

	@WorkflowGroupAssignmentID int   
)
AS


				DELETE FROM [dbo].[WorkflowGroupAssignment] WITH (ROWLOCK) 
				WHERE
					[WorkflowGroupAssignmentID] = @WorkflowGroupAssignmentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroupAssignment_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Delete] TO [sp_executeall]
GO
