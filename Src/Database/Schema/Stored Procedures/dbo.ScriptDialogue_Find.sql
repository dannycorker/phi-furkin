SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptDialogue table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDialogue_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptDialogueID int   = null ,

	@ScriptSectionID int   = null ,

	@ClientID int   = null ,

	@Dialogue varchar (MAX)  = null ,

	@HtmlDialogue varchar (MAX)  = null ,

	@WhenCreated datetime   = null ,

	@WhoCreated int   = null ,

	@WhenModified datetime   = null ,

	@WhoModified int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptDialogueID]
	, [ScriptSectionID]
	, [ClientID]
	, [Dialogue]
	, [HtmlDialogue]
	, [WhenCreated]
	, [WhoCreated]
	, [WhenModified]
	, [WhoModified]
    FROM
	[dbo].[ScriptDialogue] WITH (NOLOCK) 
    WHERE 
	 ([ScriptDialogueID] = @ScriptDialogueID OR @ScriptDialogueID IS NULL)
	AND ([ScriptSectionID] = @ScriptSectionID OR @ScriptSectionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Dialogue] = @Dialogue OR @Dialogue IS NULL)
	AND ([HtmlDialogue] = @HtmlDialogue OR @HtmlDialogue IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptDialogueID]
	, [ScriptSectionID]
	, [ClientID]
	, [Dialogue]
	, [HtmlDialogue]
	, [WhenCreated]
	, [WhoCreated]
	, [WhenModified]
	, [WhoModified]
    FROM
	[dbo].[ScriptDialogue] WITH (NOLOCK) 
    WHERE 
	 ([ScriptDialogueID] = @ScriptDialogueID AND @ScriptDialogueID is not null)
	OR ([ScriptSectionID] = @ScriptSectionID AND @ScriptSectionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Dialogue] = @Dialogue AND @Dialogue is not null)
	OR ([HtmlDialogue] = @HtmlDialogue AND @HtmlDialogue is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDialogue_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDialogue_Find] TO [sp_executeall]
GO
