SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the HyperlinkDetailFieldTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkDetailFieldTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@HyperlinkDetailFieldTargetID int   = null ,

	@ClientID int   = null ,

	@HyperlinkDetailFieldID int   = null ,

	@Target varchar (250)  = null ,

	@DetailFieldID int   = null ,

	@TemplateTypeID int   = null ,

	@DetailFieldAlias varchar (500)  = null ,

	@Notes varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [HyperlinkDetailFieldTargetID]
	, [ClientID]
	, [HyperlinkDetailFieldID]
	, [Target]
	, [DetailFieldID]
	, [TemplateTypeID]
	, [DetailFieldAlias]
	, [Notes]
    FROM
	[dbo].[HyperlinkDetailFieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([HyperlinkDetailFieldTargetID] = @HyperlinkDetailFieldTargetID OR @HyperlinkDetailFieldTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([HyperlinkDetailFieldID] = @HyperlinkDetailFieldID OR @HyperlinkDetailFieldID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([DetailFieldAlias] = @DetailFieldAlias OR @DetailFieldAlias IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [HyperlinkDetailFieldTargetID]
	, [ClientID]
	, [HyperlinkDetailFieldID]
	, [Target]
	, [DetailFieldID]
	, [TemplateTypeID]
	, [DetailFieldAlias]
	, [Notes]
    FROM
	[dbo].[HyperlinkDetailFieldTarget] WITH (NOLOCK) 
    WHERE 
	 ([HyperlinkDetailFieldTargetID] = @HyperlinkDetailFieldTargetID AND @HyperlinkDetailFieldTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([HyperlinkDetailFieldID] = @HyperlinkDetailFieldID AND @HyperlinkDetailFieldID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([DetailFieldAlias] = @DetailFieldAlias AND @DetailFieldAlias is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkDetailFieldTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_Find] TO [sp_executeall]
GO
