SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PanelItems table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_GetByPanelItemTypeID]
(

	@PanelItemTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PanelItemID],
					[ClientID],
					[PanelItemName],
					[PanelID],
					[PanelItemTypeID],
					[CreatedBy],
					[QueryID],
					[IsGlobal],
					[XAxisColumn],
					[Orientation],
					[Margin]
				FROM
					[dbo].[PanelItems] WITH (NOLOCK) 
				WHERE
					[PanelItemTypeID] = @PanelItemTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_GetByPanelItemTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_GetByPanelItemTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_GetByPanelItemTypeID] TO [sp_executeall]
GO
