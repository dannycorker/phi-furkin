SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-12-14
-- Description:	SQL after customer creation
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SACC]
@ClientID int,
@CustomerID int,
@PartnerID int = NULL /*If the customer was created from a partner then the Partner ID will be filled in*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, @CustomerID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 10
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = @CustomerID
		AND cdv.DetailFieldID = dfo.DetailFieldID
	)
	
	INSERT INTO LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, l.LeadID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 1
	INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = @CustomerID and l.LeadTypeID = df.LeadTypeID
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM LeadDetailValues ldv WITH (NOLOCK)
		WHERE ldv.LeadID = l.LeadID
		AND ldv.DetailFieldID = dfo.DetailFieldID
	)
	
	INSERT INTO CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, c.CaseID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 11
	INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = @CustomerID and l.LeadTypeID = df.LeadTypeID
	INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM CaseDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CaseID = c.CaseID
		AND cdv.DetailFieldID = dfo.DetailFieldID
	)
	
	INSERT INTO MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, m.LeadID, m.MatterID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 2
	INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = @CustomerID and l.LeadTypeID = df.LeadTypeID
	INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM MatterDetailValues mdv WITH (NOLOCK)
		WHERE mdv.MatterID = m.MatterID
		AND mdv.DetailFieldID = dfo.DetailFieldID
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SACC] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SACC] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SACC] TO [sp_executeall]
GO
