SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ResourceList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceList_Insert]
(

	@ResourceListID int    OUTPUT,

	@ClientID int   ,

	@DetailFieldPageID int   ,

	@ResourceListHelperCode varchar (100)  ,

	@ResourceListHelperName varchar (250)  ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ResourceList]
					(
					[ClientID]
					,[DetailFieldPageID]
					,[ResourceListHelperCode]
					,[ResourceListHelperName]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@DetailFieldPageID
					,@ResourceListHelperCode
					,@ResourceListHelperName
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @ResourceListID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Insert] TO [sp_executeall]
GO
