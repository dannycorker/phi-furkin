SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DiaryAppointment table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_GetByDiaryAppointmentID]
(

	@DiaryAppointmentID int   
)
AS


				SELECT
					[DiaryAppointmentID],
					[ClientID],
					[DiaryAppointmentTitle],
					[DiaryAppointmentText],
					[CreatedBy],
					[DueDate],
					[EndDate],
					[AllDayEvent],
					[Completed],
					[LeadID],
					[CaseID],
					[CustomerID],
					[Version],
					[ExportVersion],
					[RecurrenceInfo],
					[DiaryAppointmentEventType],
					[ResourceInfo],
					[TempReminderTimeshiftID],
					[StatusID],
					[LabelID]
				FROM
					[dbo].[DiaryAppointment] WITH (NOLOCK) 
				WHERE
										[DiaryAppointmentID] = @DiaryAppointmentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_GetByDiaryAppointmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_GetByDiaryAppointmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_GetByDiaryAppointmentID] TO [sp_executeall]
GO
