SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2017-03-02
-- Description:	logs overrides in insert format
-- Mods:
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Log_Overrides]
(
	@Overrides dbo.tvpIntVarcharVarchar READONLY,
	@ClientID INT
)
AS
BEGIN

	DECLARE @LogEntry VARCHAR(MAX), @MethodName VARCHAR(500), @SP VARCHAR(500), @WhoWrote INT, @ErrorType VARCHAR(20)

	SELECT TOP 1 @SP='_C00_Log_Overrides',	
			@WhoWrote=cp.ClientPersonnelID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.IsAquarium=1 
		AND FirstName LIKE '%Aquarium%' 
		AND LastName LIKE '%Automation%'
		AND ClientID=@ClientID 
			
	SELECT @MethodName = 'ClientID: ' + CAST(@ClientID AS VARCHAR),
			@ErrorType = 'Debug',
			@LogEntry=NULL

	SELECT @LogEntry = COALESCE(@LogEntry + ', ', '') + '(' + CAST(AnyID AS VARCHAR) + ',''' + AnyValue1 + ''',' 
					  + CASE WHEN AnyValue2 IS NULL THEN 'NULL' ELSE '''' + AnyValue2 + '''' END
					  + ')' 
	FROM @Overrides	
 				
	EXEC dbo._C00_LogIt @ErrorType, @SP, @MethodName, @LogEntry, @WhoWrote

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Log_Overrides] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Log_Overrides] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Log_Overrides] TO [sp_executeall]
GO
