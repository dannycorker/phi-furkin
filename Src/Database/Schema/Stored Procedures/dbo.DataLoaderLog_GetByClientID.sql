SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderLog table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderLog_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderLogID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderMapSectionID],
					[DataLoaderFileID],
					[DataLoaderFieldDefinitionID],
					[DataLoaderObjectFieldID],
					[DetailFieldID],
					[ObjectName],
					[RowIndex],
					[ColIndex],
					[NodeName],
					[Message],
					[LogLevel]
				FROM
					[dbo].[DataLoaderLog] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderLog_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_GetByClientID] TO [sp_executeall]
GO
