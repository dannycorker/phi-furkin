SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-10
-- Description:	Create a document batch detail entry.
--
-- Parameters:  @DocumentBatchId
--              The document batch entry to update
--				@LeadDocumentID
--				The lead document reference that points 
--				to the document contents
--				@Metadata
--				The metadata which is related to the document
--
-- Remarks:     Initially, the entry is created as unprocessed
--              and unlocked.
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail_Create]

	@DocumentBatchID				INT,
	@ClientID						INT = 136,
	@LeadID							INT,
	@DocumentTypeID					INT,
	@LeadDocumentTitle				VARCHAR (1000),
	@UploadDateTime					DATETIME,
	@WhoUploaded					INT,
	@DocumentBLOB					VARBINARY(MAX),
	@FileName						VARCHAR (255),
	@EmailBLOB						VARBINARY(MAX),
	@DocumentFormat					VARCHAR(24),
	@EmailFrom						VARCHAR(512),
	@EmailTo						VARCHAR(MAX),
	@CcList							VARCHAR(MAX),
	@BccList						VARCHAR(MAX),
	@ElectronicSignatureDocumentKey VARCHAR(50),
	@Encoding						VARCHAR(50),
	@ContentFormat					VARCHAR(50),
	@ZipFormat						VARCHAR(50),
	@Metadata						VARCHAR(MAX)

AS
BEGIN

DECLARE @RC int
DECLARE @LeadDocumentID INT
DECLARE @DocumentBatchDetailID INT
DECLARE @EventTypeId INT
DECLARE @MetadataXml XML
DECLARE @MatterId INT
DECLARE @VendorResourceListId INT

	-- Create the lead document entry
	EXECUTE @RC = dbo.LeadDocument_Insert 
	   @LeadDocumentID OUTPUT
	  ,@ClientID
	  ,@LeadID
	  ,@DocumentTypeID
	  ,@LeadDocumentTitle
	  ,@UploadDateTime
	  ,@WhoUploaded
	  ,@DocumentBLOB
	  ,@FileName
	  ,@EmailBLOB
	  ,@DocumentFormat
	  ,@EmailFrom
	  ,@EmailTo
	  ,@CcList
	  ,@BccList
	  ,@ElectronicSignatureDocumentKey
	  ,@Encoding
	  ,@ContentFormat
	  ,@ZipFormat	

	SET @DocumentBatchDetailId = NULL
	
	IF (@LeadDocumentID IS NOT NULL)
	BEGIN
		-- Shred out the type from the document metadata
		-- The Type represents the event type that is required for this 
		-- document.
		-- Store this in a dedicated column so that it can be indexed if 
		-- necessary.
		
		SET @MetadataXml = CAST(@Metadata AS XML)
				
				
		SELECT
			@EventTypeId = Col.value('@Type', 'int'),
			@MatterId = 
				CASE Col.value('@MatterId', 'int')
					WHEN 0 
						THEN NULL 
					ELSE
						Col.value('@MatterId', 'int')
				END
		FROM
			@MetadataXml.nodes('/Document') Tab(Col)

		-- Get the vendor id
		-- This simplifies the selection of documents in the Document Tool
		SELECT
			@VendorResourceListId = ValueInt
		FROM
			MatterDetailValues MDV WITH (NOLOCK) 
			INNER JOIN dbo.Matter MAT WITH (NOLOCK) 
			ON MAT.MatterID=@MatterId
			
			INNER JOIN dbo.Lead LED WITH (NOLOCK)
			ON LED.LeadID = MAT.LeadID    
			
			INNER JOIN dbo.ThirdPartyFieldMapping TPM WITH (NOLOCK) 
			ON TPM.ClientID=MAT.ClientID AND TPM.LeadTypeID= LED.LeadTypeID 
			AND TPM.ThirdPartyFieldID=326
		WHERE
			MDV.DetailFieldID = tpm.DetailFieldID
			AND MDV.MatterID = @MatterID
			AND MDV.ClientID = MAT.ClientID
				
		-- Create the DocumentBatchDetail entry
		INSERT INTO DocumentBatchDetail(
			DocumentBatchID,
			EventTypeId,
			LeadDocumentID,
			DocumentMetaData,
			Uploaded,
			LockedBy,
			LockDate,
			ClientID, 
			MatterId,
			VendorResourceListId)
		VALUES
			(@DocumentBatchId,
			 @EventTypeId,
			 @LeadDocumentID,
			 @Metadata,
			 0,		-- Uploaded
			 null,	-- Locked By
			 null,	-- Lock Date
			 @ClientID,
			 @MatterId,
			 @VendorResourceListId)
			 
		SET @DocumentBatchDetailId = SCOPE_IDENTITY()	
	END
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_Create] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail_Create] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_Create] TO [sp_executeall]
GO
