SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-16
-- Description:	Clear commands for one or all Automated Task Schedulers.
-- =============================================
CREATE PROCEDURE [dbo].[SchedulerCommand__ClearCommand] 
	@SchedulerIdentifier varchar(50) = null 
AS
BEGIN

	SET NOCOUNT ON;
	
	DELETE FROM dbo.SchedulerCommand 
	WHERE (SchedulerIdentifier = @SchedulerIdentifier OR @SchedulerIdentifier IS NULL)
	
	SELECT * 
	FROM dbo.SchedulerCommand
	
	SELECT 'Remember to bounce the service afterwards!' AS [Remember to bounce the service afterwards!]
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__ClearCommand] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SchedulerCommand__ClearCommand] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__ClearCommand] TO [sp_executeall]
GO
