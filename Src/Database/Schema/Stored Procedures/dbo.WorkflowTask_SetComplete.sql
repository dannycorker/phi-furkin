SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-30
-- Description:	Move WorkflowTask details to WorkflowTaskCompleted table
-- =============================================
CREATE PROCEDURE [dbo].[WorkflowTask_SetComplete]

	@WorkflowTaskID int, 
	@CompletedBy int, 
	@CompletedOn datetime, 
	@CompletionDescription varchar(255) = null

AS
BEGIN
	
	SET NOCOUNT ON

	INSERT INTO WorkflowTaskCompleted (ClientID, 
		WorkflowTaskID,
		WorkflowGroupID,
		AutomatedTaskID,
		Priority,
		AssignedTo,
		AssignedDate,
		LeadID,
		CaseID,
		EventTypeID,
		FollowUp,
		Important,
		CreationDate,
		Escalated,
		EscalatedBy,
		EscalationReason,
		EscalationDate,
		Disabled,
		CompletedBy,
		CompletedOn,
		CompletionDescription
	)
	SELECT w.ClientID, 
		@WorkflowTaskID, 
		w.WorkflowGroupID, 
		w.AutomatedTaskID, 
		w.Priority, 
		w.AssignedTo, 
		w.AssignedDate, 
		w.LeadID, 
		w.CaseID, 
		w.EventTypeID, 
		w.FollowUp, 
		w.Important, 
		w.CreationDate, 
		w.Escalated, 
		w.EscalatedBy, 
		w.EscalationReason, 
		w.EscalationDate, 
		w.Disabled, 
		@CompletedBy, 
		@CompletedOn, 
		@CompletionDescription 
	FROM WorkflowTask w WITH (NOLOCK) 
	WHERE w.WorkflowTaskID = @WorkflowTaskID 

	DELETE WorkflowTask 
	WHERE WorkflowTaskID = @WorkflowTaskID 

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_SetComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_SetComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_SetComplete] TO [sp_executeall]
GO
