SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2016-11-28
-- Description:	Creates a new Credit Card Account record associated with the customer without assigning the new account to a purchaseproduct. 
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BillingSystem_AddNewCreditCardAccountRecord] 
(
	@CustomerID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @AccountID INT ,
			@CardToken INT = dbo.fnGetSimpleDV(177399,@CustomerID), 
			@MaskedCardNumber VARCHAR(30) = dbo.fnGetSimpleDV(177400,@CustomerID),
			@ExpiryDate DATE = dbo.fnGetSimpleDVasDate(177401,@CustomerID),
			@AccountName VARCHAR(100),
			@AccNumber VARCHAR(100),
			@AccSortCode VARCHAR(10),
			@CaseID INT,
			@ClientID INT,
			@ExpiryMonth INT,
			@ExpiryYear INT,
			@FriendlyName VARCHAR(500),
			@WhenCreated DATETIME 

	/*We only want to create the account if this is a new ref*/ 
	IF NOT EXISTS (SELECT * FROM Account a WITH (NOLOCK) where a.CustomerID = @CustomerID and a.CardToken = @CardToken) 
	BEGIN 
			
		SELECT @FriendlyName = c.FullName FROM Customers C WITH (NOLOCK) where c.CustomerID = @CustomerID

		IF @ExpiryDate IS NOT NULL
		BEGIN
		
			-- is expiry date passed in year
			IF LEFT(@ExpiryDate,4) < '2000'
			BEGIN

				SELECT @ExpiryMonth=LEFT(@ExpiryDate,2), @ExpiryYear=RIGHT(LEFT(@ExpiryDate,4),2)
				
			END		
			ELSE -- expiry date passed as year & month of date 
			BEGIN
			
				SELECT @ExpiryMonth=DATEPART(MONTH,@ExpiryDate), @ExpiryYear=DATEPART(YYYY,@ExpiryDate)	
				
			END	
				
		END			
		
			-- Create a new account record in the billing system
		INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode,CardToken, MaskedCardNumber, ExpiryMonth, ExpiryYear, ExpiryDate, AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified)
		SELECT @ClientID,
			   @CustomerID,
			   @AccountName,
			   @FriendlyName,
			   @AccNumber,
			   @AccSortCode,
			   @CardToken,
			   @MaskedCardNumber,
			   @ExpiryMonth,
			   @ExpiryYear,
			   @ExpiryDate,
			   2, -- CreditCardOnly
			   1, -- Active
			   44412,  -- who created 
			   dbo.fn_GetDate_Local(),
			   44412, -- who modified
			   dbo.fn_GetDate_Local()
						   
		SELECT @AccountID = SCOPE_IDENTITY()
		
		/*Now delete the customer level fields for reuse*/
		
		EXEC _C00_SimpleValueIntoField 177399,'',@CustomerID,44412 -- Customer Ref
		EXEC _C00_SimpleValueIntoField 177400,'',@CustomerID,44412 -- Masked Card No
		EXEC _C00_SimpleValueIntoField 177401,'',@CustomerID,44412 -- ExpiryDate
	END		
	
	RETURN @AccountID 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BillingSystem_AddNewCreditCardAccountRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BillingSystem_AddNewCreditCardAccountRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BillingSystem_AddNewCreditCardAccountRecord] TO [sp_executeall]
GO
