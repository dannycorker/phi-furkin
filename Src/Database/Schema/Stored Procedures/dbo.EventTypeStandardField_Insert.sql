SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeStandardField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeStandardField_Insert]
(

	@EventTypeStandardFieldID int    OUTPUT,

	@EventTypeID int   ,

	@DataLoaderObjectFieldID int   ,

	@ClientID int   ,

	@Mandatory bit   
)
AS


				
				INSERT INTO [dbo].[EventTypeStandardField]
					(
					[EventTypeID]
					,[DataLoaderObjectFieldID]
					,[ClientID]
					,[Mandatory]
					)
				VALUES
					(
					@EventTypeID
					,@DataLoaderObjectFieldID
					,@ClientID
					,@Mandatory
					)
				-- Get the identity value
				SET @EventTypeStandardFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeStandardField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeStandardField_Insert] TO [sp_executeall]
GO
