SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FAS_Documents table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_Insert]
(

	@FasDocumentID int    OUTPUT,

	@FasCheckerDataID int   ,

	@DocumentBlob image   
)
AS


				
				INSERT INTO [dbo].[FAS_Documents]
					(
					[FasCheckerDataID]
					,[DocumentBlob]
					)
				VALUES
					(
					@FasCheckerDataID
					,@DocumentBlob
					)
				-- Get the identity value
				SET @FasDocumentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Insert] TO [sp_executeall]
GO
