SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-02-13
-- Description:	Timeunits
-- =============================================
CREATE PROCEDURE [dbo].[tu]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	FROM TimeUnits tu WITH (NOLOCK) 
	ORDER BY tu.TimeUnitsID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[tu] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[tu] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[tu] TO [sp_executeall]
GO
