SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnelDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelDetailValues_Insert]
(

	@ClientPersonnelDetailValueID int    OUTPUT,

	@ClientPersonnelID int   ,

	@ClientID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[ClientPersonnelDetailValues]
					(
					[ClientPersonnelID]
					,[ClientID]
					,[DetailFieldID]
					,[DetailValue]
					,[ErrorMsg]
					,[EncryptedValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[SourceID]
					)
				VALUES
					(
					@ClientPersonnelID
					,@ClientID
					,@DetailFieldID
					,@DetailValue
					,@ErrorMsg
					,@EncryptedValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@SourceID
					)
				-- Get the identity value
				SET @ClientPersonnelDetailValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelDetailValues_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_Insert] TO [sp_executeall]
GO
