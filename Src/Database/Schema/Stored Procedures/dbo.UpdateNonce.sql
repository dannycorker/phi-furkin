SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Chris Townsend
-- Create date: 8th Feb 2008
-- Description:	Updates a nonce
-- =============================================
CREATE PROCEDURE [dbo].[UpdateNonce] 

@Nonces tinyint

AS
BEGIN

	SET NOCOUNT ON

	UPDATE Nonce set NonceValue = @Nonces

END












GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateNonce] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateNonce] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateNonce] TO [sp_executeall]
GO
