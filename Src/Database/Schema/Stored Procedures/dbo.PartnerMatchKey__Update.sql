SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-01-2013
-- Description:	Updates a record in PartnerMatchKey
-- =============================================
CREATE PROCEDURE [dbo].[PartnerMatchKey__Update]
	
	@ClientID INT,
	@PartnerID INT,
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250)

AS
BEGIN

	IF @DateOfBirth = ''
	BEGIN
	
		SET @DateOfBirth = NULL
	
	END

	UPDATE PartnerMatchKey
	SET ClientID = @ClientID, Firstname = @Firstname, Lastname = @Lastname, Address1 = @Address1, Postcode = @Postcode, DateOfBirth = @DateOfBirth
	WHERE PartnerID = @PartnerID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PartnerMatchKey__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__Update] TO [sp_executeall]
GO
