SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM SOFTWARE
-- Create date: 2020-02-03
-- Description:	Sql After Event - TESTING: Complete Billing
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Testing_158770]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			,@EventComments					VARCHAR(2000)
			/* Specific to procedure */
			,@ValueInt						INT
			,@PurchasedProductID			INT


	DECLARE @InsertedPayments TABLE  (CustomerPaymentScheduleID INT, PaymentID INT)
	
	DECLARE @InsertedCustomerLedger TABLE (CustomerLedgerID INT, PaymentID INT)


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


	
	IF @EventTypeID IN  (158770 /*TESTING - Complete Billing*/
						,156739 /*TESTING - Rollback all dates*/
						)
	BEGIN

		SELECT @PurchasedProductID = mdv.valueINT 
		FROM MatterDetailValues mdv WITH ( NOLOCK ) 
		where mdv.DetailFieldID = 177074
		AND mdv.MatterID = @MatterID

		/*Insert Ledgers and Payments and Update schedules*/ 

		/*Insert payment records*/
		INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, CustomerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		OUTPUT inserted.CustomerPaymentScheduleID, inserted.PaymentID INTO @InsertedPayments (CustomerPaymentScheduleID, PaymentID)
		SELECT cps.ClientID, cps.CustomerID, 
				cps.PaymentDate,
				1 AS [PaymentTypeID], 
				cps.ActualCollectionDate AS [DateReceived], 
				'' AS [PaymentDescription], 
				ISNULL(a.Reference,111), 
				cps.PaymentNet, 
				cps.PaymentVAT, 
				cps.PaymentGross, 
				NULL AS [PaymentCurrency], 
				NULL AS [RelatedOrderRef], 
				NULL AS [RelatedRequestDescription], 
				NULL AS [PayeeFullName], 
				NULL AS [MaskedAccountNumber], 
				NULL AS [AccountTypeDescription], 
				2 AS [PaymentStatusID], 
				NULL AS [DateReconciled], 
				NULL AS [ReconciliationOutcome], 
				cps.RelatedObjectID, 
				cps.RelatedObjectTypeID, 
				'' AS [Comments], 
				NULL AS [PaymentFileName], 
				NULL AS [FailureCode], 
				NULL AS [FailureReason], 
				cps.CustomerPaymentScheduleID, 
				@WhoCreated AS [WhoCreated], 
				cps.ActualCollectionDate AS [WhenCreated], 
				@WhoCreated AS [WhoModified], 
				cps.ActualCollectionDate AS [WhenModified]
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID = a.AccountID
		WHERE cps.ActualCollectionDate < dbo.fn_GetDate_Local() 
		AND cps.ClientID = @ClientID
		AND cps.PaymentStatusID in (1,2,5)
		AND EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule p WITH ( NOLOCK ) 
					 where p.PurchasedProductID = @PurchasedProductID 
				 	 AND p.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID) 


		--SELECT @@RowCount AS [Payment]

		/* Insert CustomerLedger Entries */
		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		OUTPUT inserted.CustomerLedgerID, inserted.PaymentID INTO @InsertedCustomerLedger (CustomerLedgerID, PaymentID)
		SELECT p.ClientID, p.CustomerID, p.PaymentDateTime, 
			NULL AS [FailureCode], 
			NULL AS [FailureReason], 
			dbo.fn_GetDate_Local() AS [TransactionDate], 
			p.PaymentReference, 
			p.PaymentDescription, 
			p.PaymentNet,
			p.PaymentVAT, 
			p.PaymentGross, 
			NULL AS [LeadEventID], 
			p.ObjectID AS [ObjectID], 
			p.ObjectTypeID, 
			p.PaymentID, 
			NULL AS [OutgoingPaymentID], 
			@WhoCreated, 
			p.PaymentDateTime AS [WhenCreated]
		FROM @InsertedPayments ip 
		INNER JOIN Payment p WITH (NOLOCK) ON p.PaymentID = ip.PaymentID

		UPDATE cps
		SET PaymentStatusID=6 /*Paid*/, CustomerLedgerID = ic.CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
		FROM CustomerPaymentSchedule cps 
		INNER JOIN @InsertedPayments i on i.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		INNER JOIN @InsertedCustomerLedger ic on ic.PaymentID = i.PaymentID

		
		UPDATE ppps
		SET PaymentStatusID=cps.PaymentStatusID ,  CustomerLedgerID = ic.CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
		FROM PurchasedProductPaymentSchedule ppps
		INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON  ppps.CustomerPaymentScheduleID=cps.CustomerPaymentScheduleID
		INNER JOIN @InsertedPayments i on i.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		INNER JOIN @InsertedCustomerLedger ic on ic.PaymentID = i.PaymentID
		WHERE cps.PaymentStatusID IN(6,7) /*Paid,Free*/ AND ppps.PaymentStatusID NOT IN (6,7) /*Paid,Free*/
		AND ppps.PurchasedProductID = @PurchasedProductID
	
		/*JEL 2019-11-20 Added WorldPay consideration for complete billing*/ 
		UPDATE CardTransaction 
		SET ErrorCode = 'SETTLED_BY_MERCHANT'
		Where CardTransaction.CustomerID = @CustomerID 
	
	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Testing_158770] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Testing_158770] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Testing_158770] TO [sp_executeall]
GO
