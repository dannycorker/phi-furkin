SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-11-27
-- Description:	Update Mobile Numbers to suppress spaces
--				2011-01-19 ACE Updated to use ClientHSKOptions
--              2014-01-15 JWG limit each update to 50 chars to prevent truncation errors
--				2014-06-16 ACE Fixed where standard numbers were being truncated incorrectly.
--				2016-03-31 NG Added check works telephone for 07 number
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateMobiles] 
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE Customers
	SET MobileTelephone = DayTimeTelephoneNumber, WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone = ''
	AND (DayTimeTelephoneNumber LIKE '07%' OR DayTimeTelephoneNumber LIKE '7%') 
	
	SELECT 'Daytime Update' AS [Title], @@ROWCOUNT AS [Number OF ROWS Updated]

	UPDATE Customers
	SET MobileTelephone = HomeTelephone, WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone = ''
	AND (HomeTelephone LIKE '07%' OR HomeTelephone LIKE '7%') 
	
	SELECT 'HomeTelephone Update' AS [Title], @@ROWCOUNT AS [Number OF ROWS Updated]

	UPDATE Customers
	SET MobileTelephone = REPLACE(MobileTelephone, ' ', ''), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone LIKE '% %'
	AND ISNUMERIC(REPLACE(MobileTelephone, ' ', '')) = 1 
	
	UPDATE Customers
	SET MobileTelephone = REPLACE(MobileTelephone, '-', ''), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone LIKE '%-%'

	UPDATE Customers
	SET MobileTelephone = WorksTelephone, WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone = ''
	AND (WorksTelephone LIKE '07%' OR WorksTelephone LIKE '7%') 
	
	SELECT 'Daytime Update' AS [Title], @@ROWCOUNT AS [Number OF ROWS Updated]
	
	/*
	and isnumeric(MobileTelephone) = 0 
	and isnumeric(replace(MobileTelephone, ' ', '')) = 1 
	*/
	
	SELECT 'Spaces Replaces' AS [Title], @@ROWCOUNT AS [Number OF ROWS Updated]
	
	/* 
		Replace '7555 555 555' WITH '07555 555 555' 
		If the telno is already 50 chars long, ditch the last character in most cases, except the '44' variation below
	*/
	UPDATE Customers
	SET MobileTelephone = '0' + LEFT(MobileTelephone,LEN(MobileTelephone) - CASE WHEN LEN(MobileTelephone) >=50 THEN 1 ELSE 0 END), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone LIKE '7%'
	
	UPDATE Customers
	SET MobileTelephone = '0' + RIGHT(MobileTelephone,LEN(MobileTelephone)-2), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE MobileTelephone LIKE '44%'
	
	UPDATE Customers
	SET DayTimeTelephoneNumber = '0' + LEFT(DayTimeTelephoneNumber,LEN(DayTimeTelephoneNumber)- CASE WHEN LEN(DayTimeTelephoneNumber) >=50 THEN 1 ELSE 0 END), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE DayTimeTelephoneNumber LIKE '2%'
	
	UPDATE Customers
	SET DayTimeTelephoneNumber = '0' + LEFT(DayTimeTelephoneNumber,LEN(DayTimeTelephoneNumber)-CASE WHEN LEN(DayTimeTelephoneNumber) >=50 THEN 1 ELSE 0 END), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE DayTimeTelephoneNumber LIKE '1%'
	
	UPDATE Customers
	SET HomeTelephone = '0' + LEFT(HomeTelephone,LEN(HomeTelephone)-CASE WHEN LEN(HomeTelephone) >=50 THEN 1 ELSE 0 END), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE HomeTelephone LIKE '1%'
	
	UPDATE Customers
	SET HomeTelephone = '0' + LEFT(HomeTelephone,LEN(HomeTelephone)-CASE WHEN LEN(HomeTelephone) >=50 THEN 1 ELSE 0 END), WhenChanged = dbo.fn_GetDate_Local(), ChangeSource = 'dbo._C00_UpdateMobiles'
	FROM dbo.Customers (NOLOCK) 
	INNER JOIN ClientHSKOptions chsk WITH (NOLOCK) ON chsk.ClientID = Customers.ClientID AND chsk.IncludeMobileCleanUp = 1
	WHERE HomeTelephone LIKE '2%'

	SELECT '0 Insert Replaces' AS [Title], @@ROWCOUNT AS [Number OF ROWS Updated]


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateMobiles] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateMobiles] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateMobiles] TO [sp_executeall]
GO
