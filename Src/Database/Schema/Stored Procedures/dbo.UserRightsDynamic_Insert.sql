SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UserRightsDynamic table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamic_Insert]
(

	@UserRightsDynamicID int    OUTPUT,

	@ClientPersonnelID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				INSERT INTO [dbo].[UserRightsDynamic]
					(
					[ClientPersonnelID]
					,[FunctionTypeID]
					,[LeadTypeID]
					,[ObjectID]
					,[RightID]
					)
				VALUES
					(
					@ClientPersonnelID
					,@FunctionTypeID
					,@LeadTypeID
					,@ObjectID
					,@RightID
					)
				-- Get the identity value
				SET @UserRightsDynamicID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamic_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamic_Insert] TO [sp_executeall]
GO
