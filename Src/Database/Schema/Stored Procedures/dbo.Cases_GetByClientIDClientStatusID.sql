SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Cases table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_GetByClientIDClientStatusID]
(

	@ClientID int   ,

	@ClientStatusID int   
)
AS


				SELECT
					[CaseID],
					[LeadID],
					[ClientID],
					[CaseNum],
					[CaseRef],
					[ClientStatusID],
					[AquariumStatusID],
					[DefaultContactID],
					[LatestLeadEventID],
					[LatestInProcessLeadEventID],
					[LatestOutOfProcessLeadEventID],
					[LatestNonNoteLeadEventID],
					[LatestNoteLeadEventID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ProcessStartLeadEventID]
				FROM
					[dbo].[Cases] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [ClientStatusID] = @ClientStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByClientIDClientStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_GetByClientIDClientStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByClientIDClientStatusID] TO [sp_executeall]
GO
