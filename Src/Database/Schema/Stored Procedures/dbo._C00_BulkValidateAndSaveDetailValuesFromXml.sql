SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Mark Beaumont based on initial implementation by Cathal Sherry
-- Create date: 2020-01-13
-- Description:	Validate then save data submitted in XML format, to Aquarium detail fields, based on configured mapping data.
--				Currently handles detail field updates for Customer, Lead, Matter and Table detail values.
--				NOTE: it is assumed that the source XML for table details will:
--					1) contain rows for existing data and any new data (existing rows in the target AQ table will be deleted!)
--					1) be in the format <tablerows><tablerow>row1_data</tablerow><tablerow>row2_data</tablerow>...</tablerows>
--					2) already be sorted into the order in which it is to be inserted into the AQ table.
--	Modifications:
--	2020-01-20 MAB For Jira LAGCLAIM-356 | Implement receiver for nested tables (initially to handle Deductions which are nested within Claims)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_BulkValidateAndSaveDetailValuesFromXml]
(
	 @XmlDataSource			XML								/*The data in XML format that is to be saved to the Aquarium database*/
	,@XmlToDetailFieldMap	tvpXmlToDetailFieldMap READONLY	/*The configuration data that defines how each XML node is mapped to the Aquarium database*/
	,@ObjectID				INT								/*The ID of the Aquarium object that is the target of the save - for table detail values this is the table's parent AQ object ID*/
	,@ObjectTypeID			INT								/*The type of Aquarium object (Customer, Lead, Matter, etc)*/
	,@TableDetailFieldID	INT	= NULL						/*If the data is to be saved to an Aquarium table - the detail field ID that the table is assigned to*/
	,@RunAsUserID			INT								/*The client personnel ID the save operations will be attributed to*/
)
AS
BEGIN

	DECLARE @BlackHole TABLE (	/*Used to capture and ignore the output from DetailValues_BulkSave so we don't send anything back to the requester*/
		 ID1 INT
		,ID2 INT
	)	
	
	DECLARE @FieldMapping TABLE ( 
		 DetailFieldID INT						/*ID of the target Aquarium detail field*/
		,AttributeName VARCHAR(2000)			/*Name of the source XML node's attribute*/
		,XmlDataType VARCHAR(2000)				/*Data type of the source XML node*/
		,DecodeResourceListDetailFieldID INT	/*ID of the Aquarium resource list detail field to match the XML node value against to determine the equivalent resource list ID*/
		,ValueText VARCHAR(2000)				/*The raw source XML node value if it is type Text*/
		,ValueInt INT							/*The raw source XML node value if it is type Integer*/
		,ValueMoney MONEY						/*The raw source XML node value if it is type Money*/
		,ValueDate DATE							/*The raw source XML node value if it is type Date*/
		,ValueToSave VARCHAR(2000)				/*The actual value to save in the target detail field*/
		,ValueIfNull VARCHAR(2000)				/*The value to save in the target detail field if ValueToSave evaluates to NULL*/
		,TableRowID INT							/*For table detail value mapping only: The ID of the newly inserted AQ table row to which this mapping belongs*/
	)
	
	DECLARE @SourceXmlRows TABLE ( 
		 RowNumber INT IDENTITY (1,1)
		,TableRowXml XML
		,TableRowID INT
	)

	DECLARE	@NewTableRows TABLE ( 
		  RowNumber INT
		 ,TableRowID INT
	)
	
	DECLARE
		 @ClientID INT = dbo.fnGetPrimaryClientID()
		,@ErrorMessage VARCHAR(2000) = ''
		,@DetailValueData dbo.tvpDetailValueData
		,@ErrorSeparator VARCHAR(3) = ' | '
		,@TableDetailFieldPageID INT

	/*Process table detail values first to ensure there is a set of mapping data for each data row.*/
	IF @TableDetailFieldID IS NOT NULL
	BEGIN
		/*Get the ID of the page that the table is assigned to*/
		SELECT @TableDetailFieldPageID = DetailFieldPageID
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldID = @TableDetailFieldID
		
		/*
			Delete the existing table rows in AQ. 
			NOTE: it is assumed that the source XML will include data for both existing and new table rows and that data will be in 
			the order in which it is to be inserted into the AQ table.
		*/
		DELETE tr
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.DetailFieldID = @TableDetailFieldID
		AND	(
				(@ObjectTypeID = 1  /*Lead*/     AND tr.LeadID = @ObjectID)
				OR
				(@ObjectTypeID = 2  /*Matter*/   AND tr.MatterID = @ObjectID)
				OR
				(@ObjectTypeID = 10 /*Customer*/ AND tr.CustomerID = @ObjectID)
			)

		/*
			Shred the source XML into the data for individual table rows.
			NOTE: it is assumed that the source XML will be in the format <tablerows><tablerow>row1_data</tablerow><tablerow>row2_data</tablerow>...</tablerows>
		*/
		INSERT @SourceXmlRows ( TableRowXml )
		SELECT tablerow.query('.') AS [TableRowXml]
		FROM @XmlDataSource.nodes('(//tablerows/tablerow)') tablerows(tablerow)
		
		/*Add a new AQ TableRow for each row of data in the source XML and save the newly created TableRowIDs*/
		INSERT TableRows ( ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, SourceID )
			OUTPUT inserted.TableRowID, inserted.SourceID
			INTO @NewTableRows ( TableRowID, RowNumber )
		SELECT	 @ClientID																AS [ClientID]
				,CASE WHEN @ObjectTypeID = 1/*Lead*/ THEN @ObjectID ELSE NULL END		AS [LeadID]
				,CASE WHEN @ObjectTypeID = 2/*Matter*/ THEN @ObjectID ELSE NULL END		AS [MatterID]
				,@TableDetailFieldID 													AS [DetailFieldID]
				,@TableDetailFieldPageID 												AS [DetailFieldPageID]
				,1 																		AS [DenyEdit]
				,1 																		AS [DenyDelete]
				,CASE WHEN @ObjectTypeID = 10/*Customer*/ THEN @ObjectID ELSE NULL END	AS [CustomerID]
				,sxr.RowNumber 															AS [SourceID]
		FROM @SourceXmlRows sxr
		
		/*Write the new AQ TableRow IDs back to their matching XML source rows*/
		UPDATE sxr
		SET TableRowID = ntr.TableRowID
		FROM @SourceXmlRows sxr 
		INNER JOIN @NewTableRows ntr ON ntr.RowNumber = sxr.RowNumber

		/*Initialise the local version of the mapping table using the mapping parameters passed in so that there is a set of mapping data for each new table row*/
		INSERT @FieldMapping (DetailFieldID, AttributeName, XmlDataType, DecodeResourceListDetailFieldID, ValueText, ValueInt, ValueMoney, ValueDate, ValueIfNull, TableRowID)
		SELECT	 xtdfm.DetailFieldID
				,xtdfm.AttributeName
				,xtdfm.XmlDataType
				,xtdfm.DecodeResourceListDetailFieldID
				,CASE 
					WHEN xtdfm.XmlDataType = 'TEXT'  										
					THEN sxr.TableRowXml.value('(//*[local-name() = sql:column("AttributeName")])[1]','VARCHAR(2000)')
				END AS [ValueText]
				,CASE 
					WHEN xtdfm.XmlDataType IN ('INT', 'BIT', 'RESOURCE', 'SOURCE', 'PARENT')
					THEN sxr.TableRowXml.value('(//*[local-name() = sql:column("AttributeName")])[1]','INT')
				END AS [ValueInt]
				,CASE 
					WHEN xtdfm.XmlDataType = 'MONEY'
					THEN sxr.TableRowXml.value('(//*[local-name() = sql:column("AttributeName")])[1]','MONEY')
				END AS [ValueMoney]
				,CASE 
					WHEN xtdfm.XmlDataType = 'DATE'
					THEN sxr.TableRowXml.value('(//*[local-name() = sql:column("AttributeName")])[1]','DATE')
				END AS [ValueDate]
				,xtdfm.ValueIfNull
				,sxr.TableRowID
		FROM @SourceXmlRows sxr 
		CROSS JOIN @XmlToDetailFieldMap xtdfm
		ORDER BY sxr.TableRowID, xtdfm.DetailFieldID

		/*Save the ID of the original table row ID from the source system (if configured). This will be used to match nested tables (such as claims->deductions).*/
		UPDATE	tr
		SET		SourceID = fm.ValueInt
		FROM	TableRows tr
		INNER JOIN @FieldMapping fm ON fm.TableRowID = tr.TableRowID
			AND fm.XmlDataType = 'SOURCE'
	END
	ELSE /*Detail values that are not table detail values*/
	BEGIN
		/*Initialise the local version of the mapping table using the mapping parameters passed in*/
		INSERT @FieldMapping (DetailFieldID, AttributeName, XmlDataType, DecodeResourceListDetailFieldID, ValueIfNull)
		SELECT	 xtdfm.DetailFieldID
				,xtdfm.AttributeName
				,xtdfm.XmlDataType
				,xtdfm.DecodeResourceListDetailFieldID
				,xtdfm.ValueIfNull
		FROM @XmlToDetailFieldMap xtdfm	
		
		/*Extract the source data from the XML into its appropriately typed column*/
		UPDATE fm
		SET  ValueText	=	CASE 
								WHEN fm.XmlDataType = 'TEXT'
								THEN @XmlDataSource.value('(//*[local-name() = sql:column("AttributeName")])[1]','VARCHAR(2000)')
							END
			,ValueInt	=	CASE
								WHEN fm.XmlDataType IN ('INT','BIT','RESOURCE')	
								THEN @XmlDataSource.value('(//*[local-name() = sql:column("AttributeName")])[1]','INT')
							END
			,ValueMoney	=	CASE
								WHEN fm.XmlDataType = 'MONEY'
								THEN @XmlDataSource.value('(//*[local-name() = sql:column("AttributeName")])[1]','MONEY')
							END
			,ValueDate	=	CASE 
								WHEN fm.XmlDataType = 'DATE'
								THEN @XmlDataSource.value('(//*[local-name() = sql:column("AttributeName")])[1]','DATE')
							END
		FROM @FieldMapping fm 
	END
	
	/*Correct any blank or NULL dates that SQL Server XML has represented as '1900-01-01'*/
	UPDATE @FieldMapping
	SET ValueDate = NULL
		,ValueToSave = NULL
	WHERE ValueDate = '1900-01-01'
	
	/*Convert any 1/0 values for bit type data into their equivalent Yes/No lookup list item values*/
	UPDATE fm
	SET  ValueText = lli.ItemValue
	FROM @FieldMapping fm 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = fm.DetailFieldID 
		AND df.LookupListID IS NOT NULL
	INNER JOIN LookupListItems lli WITH (NOLOCK) ON lli.LookupListID = df.LookupListID
		AND (
			(lli.ItemValue = 'Yes' AND fm.ValueInt = 1)
			OR 
			(lli.ItemValue = 'No' AND fm.ValueInt = 0)
			)
	WHERE fm.XmlDataType = 'BIT'

	/*If a resource list lookup field ID was configured, attempt to match the source XML data to it, in order to determine the equivalent resource list ID*/
	UPDATE fm
	SET ValueToSave = rdv.ResourceListID
	FROM @FieldMapping fm 
	INNER JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON rdv.DetailFieldID = fm.DecodeResourceListDetailFieldID
	WHERE rdv.DetailFieldID = fm.DecodeResourceListDetailFieldID
	AND rdv.DetailValue = fm.ValueText

	/*Report any fields that could not be matched against a resource list entry*/
	SELECT @ErrorMessage += 'Unable to map ' + fm.AttributeName + '=' + ISNULL(fm.ValueText, 'NULL') + ' to a resource list ID via resource list field ID ' + CONVERT(VARCHAR(10),fm.DecodeResourceListDetailFieldID) +  ' | '
	FROM @FieldMapping fm
	WHERE fm.DecodeResourceListDetailFieldID > 0
	AND fm.ValueToSave IS NULL
	AND fm.ValueText IS NOT NULL
	
	/*If a detail field is associated with a lookup list, attempt to match the source XML data to an item in that lookup list*/
	UPDATE fm
	SET ValueToSave = lli.LookupListItemID
	FROM @FieldMapping fm 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = fm.DetailFieldID
	INNER JOIN LookupListItems lli WITH (NOLOCK) ON lli.LookupListID = df.LookupListID
	WHERE lli.ItemValue = fm.ValueText

	/*Report any fields that could not be matched against a lookup list item*/
	SELECT TOP (1) @ErrorMessage += 'Unable to map ' + fm.AttributeName + '=' + ISNULL(fm.ValueText, 'NULL') + ' to an item in lookup list ID ' + CONVERT(VARCHAR(10), df.LookupListID) + @ErrorSeparator
	FROM @FieldMapping fm
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = fm.DetailFieldID
	INNER JOIN LookupListItems lli WITH (NOLOCK) ON lli.LookupListID = df.LookupListID
	WHERE fm.ValueToSave IS NULL
	AND fm.ValueText IS NOT NULL

	/*If a validation error occurred, raise it*/
	IF @ErrorMessage > ''
	BEGIN
		SELECT @ErrorMessage = SUBSTRING(@ErrorMessage, 1, LEN(@ErrorMessage)-(LEN(@ErrorSeparator)-1)) /*Remove the final error separator*/

		RAISERROR (@ErrorMessage, 16, 1)
		
		RETURN
	END

	/*Get the final value of those fields that are not lookup or resource list lookup items (including any fields that identify resource list IDs directly.*/
	UPDATE fm
	SET ValueToSave = COALESCE(	 CONVERT(VARCHAR,fm.ValueDate,121)
								,CONVERT(VARCHAR,fm.ValueInt)
								,CONVERT(VARCHAR,fm.ValueMoney)
								,fm.ValueText
								,'' )
	FROM @FieldMapping fm 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = fm.DetailFieldID
	WHERE (df.LookupListID IS NULL
	AND df.ResourceListDetailFieldPageID IS NULL)
	OR fm.XmlDataType = 'RESOURCE'
	
	IF @TableDetailFieldID IS NULL /*Save all detail values that are not table detail values*/
	BEGIN
		/*Gather the data ready for bulk save, according to Aquarium object type. If no value is available use the configured ValueIfNull*/
		IF @ObjectTypeID = 1 /*Lead*/
		BEGIN
			INSERT @DetailValueData (ClientID, DetailFieldSubType, DetailFieldID, DetailValueID, ObjectID, DetailValue )
			SELECT	 @ClientID													AS [ClientID]
					,1/*Lead*/													AS [DetailFieldSubType]
					,fm.DetailFieldID											AS [DetailFieldID]
					,ISNULL(ldv.LeadDetailValueID, -1/*=insert new row*/)		AS [DetailValueID]
					,@ObjectID													AS [ObjectID]
					,COALESCE(fm.ValueToSave, fm.ValueIfNull, '')				AS [DetailValue]
			FROM @FieldMapping fm 
			LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = fm.DetailFieldID AND ldv.LeadID = @ObjectID
		END
		ELSE
		IF @ObjectTypeID = 2 /*Matter*/
		BEGIN
			INSERT @DetailValueData (ClientID, DetailFieldSubType, DetailFieldID, DetailValueID, ObjectID, DetailValue )
			SELECT	 @ClientID													AS [ClientID]
					,2/*Matter*/												AS [DetailFieldSubType]
					,fm.DetailFieldID											AS [DetailFieldID]
					,ISNULL(mdv.MatterDetailValueID, -1/*=insert new row*/)		AS [DetailValueID]
					,@ObjectID													AS [ObjectID]
					,COALESCE(fm.ValueToSave, fm.ValueIfNull, '')				AS [DetailValue]
			FROM @FieldMapping fm 
			LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = fm.DetailFieldID AND mdv.MatterID = @ObjectID
		END
		ELSE
		IF @ObjectTypeID = 10 /*Customer*/
		BEGIN
			INSERT @DetailValueData (ClientID, DetailFieldSubType, DetailFieldID, DetailValueID, ObjectID, DetailValue )
			SELECT	 @ClientID													AS [ClientID]
					,10/*Customer*/												AS [DetailFieldSubType]
					,fm.DetailFieldID											AS [DetailFieldID]
					,ISNULL(cdv.CustomerDetailValueID, -1/*=insert new row*/)	AS [DetailValueID] 
					,@ObjectID													AS [ObjectID]
					,COALESCE(fm.ValueToSave, fm.ValueIfNull, '')				AS [DetailValue]
			FROM @FieldMapping fm 
			LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = fm.DetailFieldID AND cdv.CustomerID = @ObjectID
		END

		/*Save the mapped values to the target detail fields*/
		INSERT @BlackHole (ID1, ID2)
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID
	END
	ELSE /*Save table detail values*/
	BEGIN
		/*
			If the table is nested, and has a column that points to its parent table row, then that column contains the original TableRowID from the source system.
			This value is also stored in the target system's new TableRows.SourceID column. It was written there by the SOURCE entry in the parent table configuration 
			(see above). We can match these two fields to find the new TableRowID and update the parent column so it points to the new TableRow.
		*/
		UPDATE	fm
		SET		ValueInt = tr.TableRowID	/*Not required but included in case of debug*/
				,ValueToSave = tr.TableRowID
		FROM	@FieldMapping fm
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.SourceID = fm.ValueInt
		AND	(
				(@ObjectTypeID = 1  /*Lead*/     AND tr.LeadID = @ObjectID)
				OR
				(@ObjectTypeID = 2  /*Matter*/   AND tr.MatterID = @ObjectID)
				OR
				(@ObjectTypeID = 10 /*Customer*/ AND tr.CustomerID = @ObjectID)
			)
		WHERE	fm.XmlDataType = 'PARENT'

		/*Bulk insert all the new table detail fields for all the new table rows*/
		INSERT TableDetailValues ( TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, CustomerID )
		SELECT	 fm.TableRowID															AS [TableRowID]
				,CASE WHEN fm.XmlDataType = 'RESOURCE' THEN fm.ValueToSave END			AS [ResourceListID]
				,fm.DetailFieldID														AS [DetailFieldID]
				,COALESCE(fm.ValueToSave, fm.ValueIfNull, '')							AS [DetailValue]
				,CASE WHEN @ObjectTypeID = 1/*Lead*/ THEN @ObjectID ELSE NULL END		AS [LeadID]
				,CASE WHEN @ObjectTypeID = 2/*Matter*/ THEN @ObjectID ELSE NULL END		AS [MatterID]
				,@ClientID																AS [ClientID]
				,CASE WHEN @ObjectTypeID = 10/*Customer*/ THEN @ObjectID ELSE NULL END	AS [CustomerID]
		FROM @FieldMapping fm
		ORDER BY fm.TableRowID, fm.DetailFieldID
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BulkValidateAndSaveDetailValuesFromXml] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BulkValidateAndSaveDetailValuesFromXml] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BulkValidateAndSaveDetailValuesFromXml] TO [sp_executeall]
GO
