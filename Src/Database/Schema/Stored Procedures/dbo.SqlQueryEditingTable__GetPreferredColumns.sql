SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[SqlQueryEditingTable__GetPreferredColumns]
	@SqlQueryEditingTableId INT 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @reportId INT

	SET @reportId = (
		SELECT
			rt.ReportTableID
		FROM
			SqlQueryEditingTable sqe
			INNER JOIN ReportTables rt
			ON rt.ReportTableName = sqe.SqlQueryTableName
		WHERE
			sqe.SqlQueryEditingTableID = @SqlQueryEditingTableId)
	
	SELECT
		c.ReportPreferredColumnID,
		c.ReportColumnName,
		c.ShowColumn
	FROM
		SqlQueryPreferredColumns c WITH (NOLOCK)
	WHERE
		ReportTableId = @reportId
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetPreferredColumns] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable__GetPreferredColumns] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable__GetPreferredColumns] TO [sp_executeall]
GO
