SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderFieldDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_Delete]
(

	@DataLoaderFieldDefinitionID int   
)
AS


				DELETE FROM [dbo].[DataLoaderFieldDefinition] WITH (ROWLOCK) 
				WHERE
					[DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Delete] TO [sp_executeall]
GO
