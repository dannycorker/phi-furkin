SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditingColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_Update]
(

	@SqlQueryEditingColumnID int   ,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryEditingTableID int   ,

	@CompleteOutputText varchar (500)  ,

	@ColumnAlias varchar (50)  ,

	@ShowColumn bit   ,

	@DisplayOrder int   ,

	@SortOrder int   ,

	@SortType varchar (10)  ,

	@IsAggregate bit   ,

	@ColumnDataType varchar (50)  ,

	@ManipulatedDataType varchar (50)  ,

	@ColumnNaturalName varchar (50)  ,

	@RealSqlQueryColumnID int   ,

	@ComplexValue nvarchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditingColumns]
				SET
					[ClientID] = @ClientID
					,[SqlQueryEditingID] = @SqlQueryEditingID
					,[SqlQueryEditingTableID] = @SqlQueryEditingTableID
					,[CompleteOutputText] = @CompleteOutputText
					,[ColumnAlias] = @ColumnAlias
					,[ShowColumn] = @ShowColumn
					,[DisplayOrder] = @DisplayOrder
					,[SortOrder] = @SortOrder
					,[SortType] = @SortType
					,[IsAggregate] = @IsAggregate
					,[ColumnDataType] = @ColumnDataType
					,[ManipulatedDataType] = @ManipulatedDataType
					,[ColumnNaturalName] = @ColumnNaturalName
					,[RealSqlQueryColumnID] = @RealSqlQueryColumnID
					,[ComplexValue] = @ComplexValue
				WHERE
[SqlQueryEditingColumnID] = @SqlQueryEditingColumnID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Update] TO [sp_executeall]
GO
