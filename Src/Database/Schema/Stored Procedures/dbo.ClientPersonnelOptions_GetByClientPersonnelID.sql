SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelOptions table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPersonnelOptionID],
					[ClientPersonnelID],
					[ClientPersonnelOptionTypeID],
					[OptionValue],
					[ClientID]
				FROM
					[dbo].[ClientPersonnelOptions] WITH (NOLOCK) 
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_GetByClientPersonnelID] TO [sp_executeall]
GO
