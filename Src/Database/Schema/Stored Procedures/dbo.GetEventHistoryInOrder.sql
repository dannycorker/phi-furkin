SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: 15 January 2007

-- Created By:  Jim
-- Purpose: Show the event history for a lead in order of priority and date

-- 08-05-2007 JWG Added optional CaseID parameter
-- 06-06-2007 Chris Townsend, Added 'Cost' and 'Comments' fields
-- 05-07-2007 Added colour and note priority
-- 05-08-2007 JWG ANSI Joins
-- 24-10-2007 CT - added LeadDocumentID
-- 14-11-2007 CT - added 'IsBlobNull' check for empty document blobs
-- 15-09-2008 PR - added ElectronicSignatureDocumentKey
-- 28-01-2010 JWG  added EsignatureStatusID and (nolock)
-- 25-07-2011 IS   securing notes to note type access rights
-- 12-07-2012 JWG  Use LeadDocument again now that it knows about DocumentBlobSize.
-- 09-08-2012 AMG  Added LeadDocumentTitle (primarily for use in Chorus).
-- 2014-07-25 SB   Limited event choice by client ID and used event type function due to shared lead types
-- 2015-02-27 AMG  Ticket #29906 - Quadra changes to include LeadDocumentTitle in FullEventHistory grid
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetEventHistoryInOrder]
(

	@LeadID int   ,
	@CaseID int = null,
	@ShowDeleted bit = 0,
	@ClientPersonnelID int = 0,
	@MinimumRightsRequired int = 1
)
AS
BEGIN

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	SELECT
		le.LeadEventID AS 'EventID',
		le.LeadID AS 'LID',
		le.LeadDocumentID AS 'LeadDocumentID',
		d.DocumentFormat AS 'DocumentFormat',
		dt.OutputFormat,
		d.[FileName],
		le.DisbursementCost AS 'Disbursements',
		le.DisbursementDescription AS 'DisbursementDesc',
		le.BaseCost AS 'BaseCost',
		le.Cost AS 'TotalCost',
		le.CostEnteredManually AS 'ManualCost',
		
		/* AMG 2015-02-27 - Ticket #29906 - Quadra changes */
		/*
		CASE WHEN et.EventSubtypeID = 6 THEN
		'<b>To:</b> ' + COALESCE(d.EmailTo, '') + '<br/>' +
		'<b>CC:</b> ' + COALESCE(d.CcList, '') + '<br/>' +
		'<b>BCC:</b> ' + COALESCE(d.BccList, '') + '<br/><br/>' +
		le.Comments 
		ELSE
		le.Comments 
		END
		AS 'Comments',
		*/		
		
		CASE WHEN et.EventSubtypeID = 6 THEN
			'<b>To:</b> ' + COALESCE(d.EmailTo, '') + '<br/>' +
			'<b>CC:</b> ' + COALESCE(d.CcList, '') + '<br/>' +
			'<b>BCC:</b> ' + COALESCE(d.BccList, '') + '<br/><br/>' +
			le.Comments
		ELSE
			le.Comments 
		END + CASE WHEN le.LeadDocumentID > 0 AND d.LeadDocumentTitle IS NOT NULL 
            THEN '<br /><strong>Document:</strong> ' + d.LeadDocumentTitle  
            ELSE ''
            END
		AS 'Comments',
		
		
		case WHEN et.EventTypeID is null THEN d.DocumentBLOB  ELSE null END as 'Note',
		isnull ( nt.NoteTypeName, et.EventTypeName) AS 'EventType',
		/* AMG: 2010-04-15 Replaced to include date and time
			convert(char,le.WhenCreated,103) AS 'When',
		*/
		convert(char(11),le.WhenCreated,103) + LEFT(convert(char(5),LE.WhenCreated,108),5) AS 'When',
		cp.UserName AS 'Who',
		le.NotePriority as [NotePriority],
		le.EventDeleted,
		isnull(nt.AlertColour, '') as [AlertColour],
		isnull(nt.NormalColour, '') as [NormalColour],
		CASE 
			WHEN d.DocumentBLOBSize > 0 THEN 0 
			ELSE 1 
		END as IsBlobNull,
        d.ElectronicSignatureDocumentKey as 'ElectronicSignatureDocumentKey', 
		isnull(dt.DocumentTypeID, 0) as 'DocumentTypeID',
		case when dt.SendToMultipleRecipients = 1 then 'Yes' else 'No' end as SendToMultipleRecipients, 
		ldes.EsignatureStatusID, 
		ldes.LeadDocumentEsignatureStatusID,
		es.EventSubtypeName AS 'EventSubtype',
		d.LeadDocumentTitle AS 'DocumentTitle'
	FROM
		dbo.LeadEvent le (nolock)
		LEFT JOIN dbo.EventType et (nolock) ON le.EventTypeID = et.EventTypeID
		LEFT JOIN dbo.EventSubtype es (nolock) ON et.EventSubtypeID = es.EventSubtypeID
		LEFT JOIN dbo.NoteType nt (nolock) ON le.NoteTypeID = nt.NoteTypeID
		LEFT JOIN dbo.LeadDocument d WITH (NOLOCK) ON le.LeadDocumentId = d.LeadDocumentID
		LEFT JOIN dbo.DocumentType dt (nolock) ON d.DocumentTypeID = dt.DocumentTypeID
		INNER JOIN dbo.ClientPersonnel cp (nolock) ON le.WhoCreated = cp.ClientPersonnelID 
		LEFT JOIN dbo.LeadDocumentEsignatureStatus ldes (nolock) on ldes.LeadDocumentID = le.LeadDocumentID 
		LEFT JOIN dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) snt ON snt.ObjectID = le.NoteTypeID
	WHERE	le.LeadID = @LeadID
	AND		(le.CaseID = @CaseID OR @CaseID IS NULL) 
	AND
		CASE
			WHEN @ShowDeleted = 0 AND (le.EventDeleted = 0 OR le.EventDeleted is null) THEN 1
			WHEN @ShowDeleted = 1 THEN 1
			ELSE 0 
		END = 1
	AND	
		CASE
			WHEN et.EventTypeID IS NULL AND snt.ObjectID IS NULL THEN 0
			ELSE 1 
		END = 1
	ORDER BY IsNull(le.NotePriority,0) desc, le.WhenCreated desc, le.LeadEventID desc

END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventHistoryInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventHistoryInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventHistoryInOrder] TO [sp_executeall]
GO
