SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ObjectLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_Update]
(

	@ObjectLinkID int   ,

	@FromObjectID int   ,

	@ToObjectID int   ,

	@ObjectTypeRelationshipID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ObjectLink]
				SET
					[FromObjectID] = @FromObjectID
					,[ToObjectID] = @ToObjectID
					,[ObjectTypeRelationshipID] = @ObjectTypeRelationshipID
				WHERE
[ObjectLinkID] = @ObjectLinkID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Update] TO [sp_executeall]
GO
