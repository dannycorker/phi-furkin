SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypePageTitleStandardTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadTypePageTitleStandardTargetID],
					[ClientID],
					[LeadTypeID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes]
				FROM
					[dbo].[LeadTypePageTitleStandardTarget] WITH (NOLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_GetByLeadTypeID] TO [sp_executeall]
GO
