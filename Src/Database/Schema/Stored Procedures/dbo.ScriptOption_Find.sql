SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptOptionID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelID int   = null ,

	@ScriptEditorTheme varchar (250)  = null ,

	@ShowLineNumbers bit   = null ,

	@CollapseTopPanel bit   = null ,

	@CollapseLeftPanel bit   = null ,

	@CollapseRightPanel bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptOptionID]
	, [ClientID]
	, [ClientPersonnelID]
	, [ScriptEditorTheme]
	, [ShowLineNumbers]
	, [CollapseTopPanel]
	, [CollapseLeftPanel]
	, [CollapseRightPanel]
    FROM
	[dbo].[ScriptOption] WITH (NOLOCK) 
    WHERE 
	 ([ScriptOptionID] = @ScriptOptionID OR @ScriptOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ScriptEditorTheme] = @ScriptEditorTheme OR @ScriptEditorTheme IS NULL)
	AND ([ShowLineNumbers] = @ShowLineNumbers OR @ShowLineNumbers IS NULL)
	AND ([CollapseTopPanel] = @CollapseTopPanel OR @CollapseTopPanel IS NULL)
	AND ([CollapseLeftPanel] = @CollapseLeftPanel OR @CollapseLeftPanel IS NULL)
	AND ([CollapseRightPanel] = @CollapseRightPanel OR @CollapseRightPanel IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptOptionID]
	, [ClientID]
	, [ClientPersonnelID]
	, [ScriptEditorTheme]
	, [ShowLineNumbers]
	, [CollapseTopPanel]
	, [CollapseLeftPanel]
	, [CollapseRightPanel]
    FROM
	[dbo].[ScriptOption] WITH (NOLOCK) 
    WHERE 
	 ([ScriptOptionID] = @ScriptOptionID AND @ScriptOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ScriptEditorTheme] = @ScriptEditorTheme AND @ScriptEditorTheme is not null)
	OR ([ShowLineNumbers] = @ShowLineNumbers AND @ShowLineNumbers is not null)
	OR ([CollapseTopPanel] = @CollapseTopPanel AND @CollapseTopPanel is not null)
	OR ([CollapseLeftPanel] = @CollapseLeftPanel AND @CollapseLeftPanel is not null)
	OR ([CollapseRightPanel] = @CollapseRightPanel AND @CollapseRightPanel is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_Find] TO [sp_executeall]
GO
