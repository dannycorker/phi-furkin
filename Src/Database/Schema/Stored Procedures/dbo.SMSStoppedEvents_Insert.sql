SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2018-04-17
-- Description:	Log when an sms has not been sent due to the customer opting out
-- =============================================

CREATE PROCEDURE [dbo].[SMSStoppedEvents_Insert]
(
	  @ClientID		int
	, @LeadEventID int
)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO SMSStoppedEvents (ClientID, LeadEventID)
	VALUES (@ClientID, @LeadEventID)
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStoppedEvents_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSStoppedEvents_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStoppedEvents_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSStoppedEvents_Insert] TO [sp_executehelper]
GO
