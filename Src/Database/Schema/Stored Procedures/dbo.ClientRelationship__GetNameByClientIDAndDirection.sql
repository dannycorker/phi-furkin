SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientRelationship table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship__GetNameByClientIDAndDirection]
(
	@ClientID int,
	@Direction bit
)
AS

IF @Direction = 1
BEGIN 
		SELECT
			[ClientRelationshipID],
			CompanyName,
			ClientRelationshipName
		FROM
			[dbo].[ClientRelationship] cr
		INNER JOIN 
			[dbo].[Clients] c on c.ClientID = cr.ReceivingClientID
		WHERE
			cr.OutgoingClientID = @ClientID
END
ELSE
BEGIN
		SELECT
			[ClientRelationshipID],
			CompanyName,
			ClientRelationshipName
		FROM
			[dbo].[ClientRelationship] cr
		INNER JOIN 
			[dbo].[Clients] c on c.ClientID = cr.OutgoingClientID
		WHERE
			cr.ReceivingClientID = @ClientID
END
			







GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship__GetNameByClientIDAndDirection] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship__GetNameByClientIDAndDirection] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship__GetNameByClientIDAndDirection] TO [sp_executeall]
GO
