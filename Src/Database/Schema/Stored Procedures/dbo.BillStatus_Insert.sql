SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the BillStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillStatus_Insert]
(

	@BillStatusID int    OUTPUT,

	@Name varchar (50)  ,

	@Description varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[BillStatus]
					(
					[Name]
					,[Description]
					)
				VALUES
					(
					@Name
					,@Description
					)
				-- Get the identity value
				SET @BillStatusID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillStatus_Insert] TO [sp_executeall]
GO
