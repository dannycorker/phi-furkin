SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 04/08/2016
-- Description:	Inserts a client payment gateway record
-- Modified By PR:01/09/2016 Added IpAuthentication
-- =============================================
CREATE PROCEDURE [dbo].[ClientPaymentGateway__Insert] 	
	@ClientID INT, 
	@PaymentGatewayID INT, 
	@Login VARCHAR(250), 
	@UserName VARCHAR(250), 
	@Password VARCHAR(250), 
	@MerchantName VARCHAR(250), 
	@AccountID VARCHAR(250), 
	@MerchantUrl VARCHAR(MAX), 
	@TransactionKey VARCHAR(500), 
	@TerminalID VARCHAR(250), 
	@ApplySecure3D INT, 
	@TestMode BIT,
	@IpAuthentication BIT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO ClientPaymentGateway (ClientID, PaymentGatewayID, Login, UserName, Password, MerchantName, AccountID, MerchantUrl, TransactionKey, TerminalID, ApplySecure3D, TestMode, IpAuthentication)
	VALUES (@ClientID, @PaymentGatewayID, @Login, @UserName, @Password, @MerchantName, @AccountID, @MerchantUrl, @TransactionKey, @TerminalID, @ApplySecure3D, @TestMode, @IpAuthentication)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPaymentGateway__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPaymentGateway__Insert] TO [sp_executeall]
GO
