SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientQuestionnairesVisited table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisited_GetByClientQuestionnairesVisitedID]
(

	@ClientQuestionnairesVisitedID int   
)
AS


				SELECT
					[ClientQuestionnairesVisitedID],
					[ClientQuestionnaireID],
					[VisitedDate],
					[IPAddress],
					[ClientID]
				FROM
					[dbo].[ClientQuestionnairesVisited] WITH (NOLOCK) 
				WHERE
										[ClientQuestionnairesVisitedID] = @ClientQuestionnairesVisitedID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_GetByClientQuestionnairesVisitedID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisited_GetByClientQuestionnairesVisitedID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_GetByClientQuestionnairesVisitedID] TO [sp_executeall]
GO
