SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SMSOutgoingPhoneNumber table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_Insert]
(

	@SMSOutgoingPhoneNumberID int    OUTPUT,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@LeadTypeID int   ,

	@OutgoingPhoneNumber varchar (16)  ,

	@SMSSurveyStartEventTypeID int   ,

	@MMSLetterInEvent int   ,

	@SmsGatewayID int   ,

	@HasIntroductionText bit   ,

	@IntroductionTextFrom varchar (250)  ,

	@IntroductionText varchar (2000)  ,

	@DelayInSeconds int   ,

	@IntroductionTextEventTypeID int   ,

	@IntroductionTextDocumentTypeID int   ,

	@NoCLIRejectText varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[SMSOutgoingPhoneNumber]
					(
					[ClientID]
					,[SMSQuestionnaireID]
					,[LeadTypeID]
					,[OutgoingPhoneNumber]
					,[SMSSurveyStartEventTypeID]
					,[MMSLetterInEvent]
					,[SmsGatewayID]
					,[HasIntroductionText]
					,[IntroductionTextFrom]
					,[IntroductionText]
					,[DelayInSeconds]
					,[IntroductionTextEventTypeID]
					,[IntroductionTextDocumentTypeID]
					,[NoCLIRejectText]
					)
				VALUES
					(
					@ClientID
					,@SMSQuestionnaireID
					,@LeadTypeID
					,@OutgoingPhoneNumber
					,@SMSSurveyStartEventTypeID
					,@MMSLetterInEvent
					,@SmsGatewayID
					,@HasIntroductionText
					,@IntroductionTextFrom
					,@IntroductionText
					,@DelayInSeconds
					,@IntroductionTextEventTypeID
					,@IntroductionTextDocumentTypeID
					,@NoCLIRejectText
					)
				-- Get the identity value
				SET @SMSOutgoingPhoneNumberID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Insert] TO [sp_executeall]
GO
