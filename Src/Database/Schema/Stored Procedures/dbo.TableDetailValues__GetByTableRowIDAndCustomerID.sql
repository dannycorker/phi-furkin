SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-12-20
-- Description:	Gets table detail values for a single row by customer id
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__GetByTableRowIDAndCustomerID]
(

	@TableRowID int,
	@ClientID int, 
	@CustomerID int  
)
AS


				SELECT
					tdv.[TableDetailValueID],
					tdv.[TableRowID],
					tdv.[ResourceListID],
					tdv.[DetailFieldID],
					tdv.[DetailValue],
					tdv.[LeadID],
					tdv.[MatterID],
					tdv.[ClientID],
					tdv.[EncryptedValue],
					tdv.[ErrorMsg],
					tdv.[ValueInt],
					tdv.[ValueMoney],
					tdv.[ValueDate],
					tdv.[ValueDateTime],	
					tdv.[CustomerID],
					tdv.[CaseID],
					tdv.[ClientPersonnelID],
					tdv.[ContactID],
					df.[QuestionTypeID],
					df.[FieldCaption],
					CASE df.QuestionTypeID WHEN 4 THEN ll.ItemValue ELSE NULL END AS ItemValue
				FROM
					[dbo].[TableDetailValues] tdv WITH (NOLOCK) 	
					left outer join dbo.LookupListItems ll on ll.LookupListItemID = tdv.ValueInt
					INNER JOIN dbo.DetailFields df on df.DetailFieldID = tdv.DetailFieldID				
				WHERE
					tdv.[TableRowID] = @TableRowID and
					tdv.[ClientID] = @ClientID and
					tdv.[CustomerID] = @CustomerID
					
			Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByTableRowIDAndCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByTableRowIDAndCustomerID] TO [sp_executeall]
GO
