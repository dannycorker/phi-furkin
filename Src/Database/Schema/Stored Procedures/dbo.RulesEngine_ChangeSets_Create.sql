SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack (copy of RulesEngine_RuleSet_Copy)
-- Create date: 2016-08-11
-- Description: Copies all rule sets from one change set to another
-- Mods
--	2016-11-09 DCM Include RuleCheckpoint in the copy
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_ChangeSets_Create]
(
	@ClientID  INT,
	@ParentChangeSetID  INT,
	@UserID  INT
)

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @NewChangeSetID INT
	DECLARE @NewChangeSetTagName VARCHAR(250) = 'CS - ' + CONVERT(CHAR(10), dbo.fn_GetDate_Local(), 120)
		
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO RulesEngine_ChangeSets (ParentChangeSetID, ClientID, TagName, ValidFrom, ValidTo, WhenCreated, WhoCreated, WhenModified, WhoModified)
		VALUES (@ParentChangeSetID, @ClientID, @NewChangeSetTagName, NULL, NULL, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID)

		SELECT @NewChangeSetID = SCOPE_IDENTITY()
		
		IF @ParentChangeSetID IS NULL OR @ParentChangeSetID = 0 -- Handle the initial changeset
		BEGIN
		
			UPDATE	RulesEngine_RuleSets
			SET		ChangeSetID = @NewChangeSetID
			WHERE	ClientID = @ClientID
			AND		ChangeSetID IS NULL
		
		END
		ELSE
		BEGIN
			
			-- Insert Rule Set
			INSERT INTO RulesEngine_RuleSets(Name, InputTypeID, OutputTypeID, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, 
				Description, SourceID, ExposeForUse, RuleSetTypeID, ChangeSetID, ParentRuleSetID)
			SELECT
				RS.Name, RS.InputTypeID, RS.OutputTypeID, RS.ClientID, RS.WhenCreated, RS.WhoCreated, RS.WhenModified, RS.WhoModified, 
				RS.Description, RS.RuleSetID, RS.ExposeForUse, RS.RuleSetTypeID, @NewChangeSetID, ISNULL(ParentRuleSetID, RuleSetID)
			FROM
				RulesEngine_RuleSets RS
			WHERE
				(@ParentChangeSetID IS NULL OR ChangeSetID = @ParentChangeSetID)
				AND ClientID = @ClientID
				
			IF @@ROWCOUNT = 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
			
			-- Insert Rules
			INSERT INTO RulesEngine_Rules(Name, RuleSetID, RuleOrder, ClientID, WhenCreated, WhoCreated, WhenModified, 
				WhoModified, Description, RuleCheckpoint,
				SourceID)
			SELECT
				R.Name, RS.RuleSetID, R.RuleOrder, R.ClientID, R.WhenCreated, R.WhoCreated, R.WhenModified, R.WhoModified,
				R.Description, RuleCheckpoint,
				R.RuleID -- Store the original rule id (used later)
			FROM 
				RulesEngine_Rules R
				INNER JOIN RulesEngine_RuleSets RS
				ON R.RuleSetID = RS.SourceID
				
			-- Insert Rule Parameters
			INSERT INTO RulesEngine_RuleParameters(Name, RuleID, ParameterTypeID, Value, DataTypeID, 
				ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, Parent, 
				SourceID)
			SELECT 
				RP.Name, R.RuleID, RP.ParameterTypeID, RP.Value, RP.DataTypeID, 
				RP.ClientID, RP.WhenCreated, RP.WhoCreated, RP.WhenModified, RP.WhoModified, RP.Parent, 
				RP.RuleParameterID -- Store the original rule parameter id (used later)
			FROM
				RulesEngine_RuleParameters RP
				INNER JOIN RulesEngine_Rules R
				ON RP.RuleID = R.SourceId

			-- update ruleset parameter mapping
			UPDATE rp
			SET	rp.Value = rsp.RuleSetID
			FROM	RulesEngine_RuleParameters rp WITH (NOLOCK)
			INNER JOIN RulesEngine_Rules r WITH (NOLOCK)  ON r.RuleID = rp.RuleID AND rp.ParameterTypeID = 3
			INNER JOIN dbo.RulesEngine_RuleSets rs WITH (NOLOCK) ON r.RuleSetID = rs.RuleSetID
			INNER JOIN dbo.RulesEngine_RuleSets rsp WITH (NOLOCK) ON rp.Value = rsp.SourceID
			WHERE	rs.ChangeSetID = @NewChangeSetID
			
			-- Insert Parameter Options
			INSERT INTO RulesEngine_ParameterOptions(RuleParameterID, OperatorID, Val1, Val2, OptionOrder, ClientID, 
				WhenCreated, WhoCreated, WhenModified, WhoModified, 
				SourceId)
			SELECT
				RP.RuleParameterID, RPO.OperatorID, RPO.Val1, RPO.Val2, RPO.OptionOrder, RPO.ClientID, 
				RPO.WhenCreated, RPO.WhoCreated, RPO.WhenModified, RPO.WhoModified, 
				RPO.ParameterOptionID -- Store the original parameter option id (used later)
			FROM
				dbo.RulesEngine_ParameterOptions RPO
				INNER JOIN dbo.RulesEngine_RuleParameters RP
				ON RPO.RuleParameterID = RP.SourceID

			-- Insert Rule Outputs
			INSERT INTO RulesEngine_RuleOutputs(RuleID, Value, TransformID, SourceID)
			SELECT
				R.RuleID, 
				-- Need to work out how to set any [!P: values with the new reference value
				CASE 
					WHEN SUBSTRING(RO.Value,1,4) = '[!P:'
						AND EXISTS (
							SELECT * FROM RulesEngine_RuleParameters 
							WHERE CAST(SourceID AS VARCHAR(MAX)) = dbo.fnGetCharsBeforeOrAfterSeparator(dbo.fnGetCharsBeforeOrAfterSeparator(RO.Value, ':', 0, 0), ']', 0, 1)
							)
						THEN '[!P:' + (
							SELECT
								CAST(RuleParameterID AS VARCHAR(50)) FROM RulesEngine_RuleParameters 
							WHERE
								CAST(SourceID AS VARCHAR(MAX)) = dbo.fnGetCharsBeforeOrAfterSeparator(dbo.fnGetCharsBeforeOrAfterSeparator(RO.Value, ':', 0, 0), ']', 0, 1))
								+ ']'
					ELSE
						RO.Value 
				END Value,
				RO.TransformID,
				RO.RuleOutputID
			FROM
				RulesEngine_RuleOutputs RO
				INNER JOIN RulesEngine_Rules R
				ON RO.RuleID = R.SourceID

			-- Insert Output Coordinates
			INSERT INTO RulesEngine_OutputCoordinates(RuleOutputID, ParameterOptionID)
			SELECT
				RO.RuleOutputID, RPO.ParameterOptionID
			FROM
				RulesEngine_OutputCoordinates OC
				INNER JOIN RulesEngine_RuleOutputs RO
				ON OC.RuleOutputID = RO.SourceID
				
				INNER JOIN RulesEngine_ParameterOptions RPO
				ON OC.ParameterOptionID = RPO.SourceID

			-- Now clear out the SourceID values 
			UPDATE RulesEngine_RuleSets SET SourceID = NULL
			UPDATE RulesEngine_Rules SET SourceID = NULL
			UPDATE RulesEngine_RuleOutputs SET SourceID = NULL
			UPDATE RulesEngine_RuleParameters SET SourceID = NULL
			UPDATE RulesEngine_ParameterOptions SET SourceID = NULL
		
		END
		
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		
		DECLARE @Error VARCHAR(2000)
		SELECT @Error = ERROR_MESSAGE()
		
		EXEC dbo._C00_LogIt 'Error', @ClientID, 'RulesEngine_ChangeSets_Create', @Error, @NewChangeSetID
			
	END CATCH
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_Create] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ChangeSets_Create] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ChangeSets_Create] TO [sp_executeall]
GO
