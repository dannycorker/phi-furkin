SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderSectionLocaterKeyword table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_Update]
(

	@DataLoaderSectionLocaterKeywordID int   ,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderObjectTypeID int   ,

	@DataLoaderMapSectionID int   ,

	@Keyword varchar (250)  ,

	@DataLoaderKeywordMatchTypeID int   ,

	@KeywordNotes varchar (2000)  ,

	@RelativeRow int   ,

	@AbsoluteCol int   ,

	@IsForSectionStart bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderSectionLocaterKeyword]
				SET
					[ClientID] = @ClientID
					,[DataLoaderMapID] = @DataLoaderMapID
					,[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[DataLoaderMapSectionID] = @DataLoaderMapSectionID
					,[Keyword] = @Keyword
					,[DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID
					,[KeywordNotes] = @KeywordNotes
					,[RelativeRow] = @RelativeRow
					,[AbsoluteCol] = @AbsoluteCol
					,[IsForSectionStart] = @IsForSectionStart
				WHERE
[DataLoaderSectionLocaterKeywordID] = @DataLoaderSectionLocaterKeywordID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Update] TO [sp_executeall]
GO
