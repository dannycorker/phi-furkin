SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-09-15
-- Description:	Loads the PremiumCalculationDetailValues table for rules engine output xml 
-- Mods
-- 2015-11-26 DCM Removed rounding from annual split 
-- 2016-07-27 DCM Only allow one record of each named checkpoint (because checkpoited rules can be run more than once in any evaluation)
-- 2016-08-09 DCM PremCalcID passed in rather than out
-- 2017-08-15 CPS added ISNULLs
-- 2017-08-22 CPS added AND @PremiumCalculationID is not NULL to prevent "column does not allow NULL" errors
-- 2018-04-16 IS rewrite to be more performant
-- 2018-07-15 CPS support new fast C# rater
-- 2019-10-15 CPS for JIRA LPC-35 | New version brought over from Aquarius433Dev
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Premium_StoreCalculationValues] 
(
	 @ClientID				INT
	,@RulesEngineOutput		XML
	,@PremiumCalculationID	INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @Checkpoints TABLE
	(
		ID INT IDENTITY,
		RuleID VARCHAR(2000),
		Input VARCHAR(2000),
		RuleCheckpoint VARCHAR(2000),
		Transform VARCHAR(2000),
		Value VARCHAR(2000),
		[Output] VARCHAR(2000),
		Delta VARCHAR(2000),
		RuleOutputFirstMonth VARCHAR(2000),
		RuleOutputOtherMonth VARCHAR(2000),
		RuleInputOutputDeltaFirstMonth VARCHAR(2000),
		RuleInputOutputDeltaOtherMonth VARCHAR(2000)
	)
	
	INSERT INTO dbo.PremiumCalculationDetailValues (ClientID, PremiumCalculationDetailID, RuleSequence, RuleID, RuleInput, RuleCheckpoint, RuleTransform, RuleTransformValue, RuleOutput, RuleInputOutputDelta,RuleOutputFirstMonth,RuleOutputOtherMonth,RuleInputOutputDeltaFirstMonth,RuleInputOutputDeltaOtherMonth)
	SELECT
		@ClientID,
		@PremiumCalculationID,
		ROW_NUMBER() OVER(PARTITION BY @PremiumCalculationID ORDER BY @PremiumCalculationID),
		ISNULL(pet.node.value('(@RuleID)[1]', 'varchar(2000)'),''),
		ISNULL(pet.node.value('(@Input)[1]', 'varchar(2000)'),''),
		ISNULL(pet.node.value('(@RuleCheckpoint)[1]', 'varchar(2000)'),''),
		ISNULL(pet.node.value('(@Transform)[1]', 'varchar(2000)'),''),
		ISNULL(pet.node.value('(@Value)[1]', 'varchar(2000)'),''),
		ISNULL(pet.node.value('(@Output)[1]', 'varchar(2000)'),''),
		ISNULL(pet.node.value('(@Delta)[1]', 'varchar(2000)'),''),
		osplit.FirstMonthly,
		osplit.RecurringMonthly,
		dsplit.FirstMonthly,
		dsplit.RecurringMonthly
	FROM	@RulesEngineOutput.nodes('//RuleSet/Rule') AS pet(node) -- CPS 2018-07-15 add extra "/" to allow support of both old and new rater
	CROSS APPLY dbo.fn_C00_1273_StorePremiumCalcSplitPremium(ISNULL(pet.node.value('(@Output)[1]', 'varchar(2000)'),0.00)) osplit
	CROSS APPLY dbo.fn_C00_1273_StorePremiumCalcSplitPremium(ISNULL(pet.node.value('(@Delta)[1]', 'varchar(2000)'),0.00)) dsplit
	WHERE	pet.node.value('(@RuleCheckpoint)[1]', 'varchar(2000)') <> ''
		
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Premium_StoreCalculationValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Premium_StoreCalculationValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Premium_StoreCalculationValues] TO [sp_executeall]
GO
