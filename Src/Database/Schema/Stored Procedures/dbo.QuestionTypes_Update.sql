SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuestionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionTypes_Update]
(

	@QuestionTypeID int   ,

	@Name varchar (50)  ,

	@Description varchar (50)  ,

	@Multiselect bit   ,

	@EcatcherField bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuestionTypes]
				SET
					[Name] = @Name
					,[Description] = @Description
					,[Multiselect] = @Multiselect
					,[EcatcherField] = @EcatcherField
				WHERE
[QuestionTypeID] = @QuestionTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionTypes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Update] TO [sp_executeall]
GO
