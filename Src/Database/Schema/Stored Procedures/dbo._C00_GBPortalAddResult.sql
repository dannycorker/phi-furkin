SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-04-01
-- Description:	Adds a GBPortal Phone Number Result to the customers mapped detail fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GBPortalAddResult]

	@ClientID INT,
	@ClientPersonnelID INT,
	@LeadID INT,
	@CaseID INT,
	@RawResult VARCHAR(2000), -- ThirdPartyFieldID = 838
	@RegisteredCountry VARCHAR(2000), -- ThirdPartyFieldID = 842
	@RegisteredNetwork VARCHAR(2000), -- ThirdPartyFieldID = 839
	@LocationCountry VARCHAR(2000), -- ThirdPartyFieldID = 843
	@Connected VARCHAR(2000), -- ThirdPartyFieldID = 840
	@User VARCHAR(2000), -- ThirdPartyFieldID = 841
	@CommunicationsProvider VARCHAR(2000), -- ThirdPartyFieldID = 845
	@Notes VARCHAR(2000), -- ThirdPartyFieldID = 846
	@Status VARCHAR(2000), -- ThirdPartyFieldID = 847
	@TypeOfPhoneChecked VARCHAR(2000) -- mobile, home, work, daytime -- type of GBPortal lookup
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


		DECLARE @LeadTypeID INT 
		DECLARE @MatterID INT
		
		SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID
		
		-- assumes one matter per case and detail fields all mapped at the matter level
		SELECT TOP 1 @MatterID = MatterID FROM Matter WITH (NOLOCK) WHERE CaseID=@CaseID

		IF @TypeOfPhoneChecked = 'mobile' -- only store values for mobile telephone look ups
		BEGIN
		
			-- Raw result from the GB Portal Lookup
			DECLARE @RawResultDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @RawResultDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=838 -- Raw result 

			IF @RawResultDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @RawResultDetailFieldID , @RawResult, @MatterID, @ClientPersonnelID
			END
			
			-- Registered country
			DECLARE @RegistedCountryDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @RegistedCountryDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=842 -- Registered country

			IF @RegistedCountryDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @RegistedCountryDetailFieldID , @RegisteredCountry, @MatterID, @ClientPersonnelID
			END

			-- Registered Network
			DECLARE @RegistedNetworkDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @RegistedNetworkDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=839 -- Registered Network

			IF @RegistedNetworkDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @RegistedNetworkDetailFieldID , @RegisteredNetwork, @MatterID, @ClientPersonnelID
			END

			-- Location country
			DECLARE @LocationCountryDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @LocationCountryDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=843 -- Location country

			IF @LocationCountryDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @LocationCountryDetailFieldID , @LocationCountry, @MatterID, @ClientPersonnelID
			END

			-- Connected
			DECLARE @ConnectedDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @ConnectedDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=840 -- Connected

			IF @ConnectedDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @ConnectedDetailFieldID , @Connected, @MatterID, @ClientPersonnelID
			END
			
			-- User
			DECLARE @UserDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @UserDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=841 -- User

			IF @UserDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @UserDetailFieldID , @User, @MatterID, @ClientPersonnelID
			END	
			
			-- Communications Provider
			DECLARE @CommunicationsProviderDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @CommunicationsProviderDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=845 -- Communications Provider

			IF @CommunicationsProviderDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @CommunicationsProviderDetailFieldID , @CommunicationsProvider, @MatterID, @ClientPersonnelID
			END	
			
			-- Notes
			DECLARE @NotesDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @NotesDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=846 -- Notes

			IF @NotesDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @NotesDetailFieldID, @Notes, @MatterID, @ClientPersonnelID
			END		

			-- Status
			DECLARE @StatusDetailFieldID INT -- These fields should be mapped to the matter level
			SELECT @StatusDetailFieldID = DetailFieldID 
			FROM ThirdPartyFieldMapping WITH (NOLOCK) 
			WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=847 -- Status

			IF @StatusDetailFieldID IS NOT NULL
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField @StatusDetailFieldID, @Status, @MatterID, @ClientPersonnelID
			END		
			
		END
	END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GBPortalAddResult] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GBPortalAddResult] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GBPortalAddResult] TO [sp_executeall]
GO
