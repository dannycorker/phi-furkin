SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 02 March 2007
-- Created By:  ()
-- Purpose: Select records from the ClientPersonnel table through a foreign key
-- JWG 2009-01-22 Added the flag for @GetUsersInThisGroup. The UserManagementMenu page needs to
--                get all users NOT in this group, as candidates that can be added to the group.
-- JWG 2012-08-10 Power-ups for Aquarium users
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[ClientPersonnel__GetByClientIDAndAdminGroupID]
(

	@ClientID int, 
	@AdminGroupID int, 
	@GetUsersInThisGroup bit = 1,
	@ShowAquariumUsers bit = 1    
)
AS
BEGIN

	IF @GetUsersInThisGroup = 1
	BEGIN
		-- Normal usage
		SELECT
			*
		FROM
			dbo.[ClientPersonnel] cp WITH (NOLOCK) 
		WHERE
			cp.[ClientID] = @ClientID 
		AND cp.[ClientPersonnelAdminGroupID] = @AdminGroupID
		AND (@ShowAquariumUsers = 1 OR cp.IsAquarium = 0) 
		ORDER BY cp.UserName
	END
	ELSE
	BEGIN
		-- User Management usage
		SELECT
			*
		FROM
			dbo.[ClientPersonnel] cp WITH (NOLOCK) 
		WHERE
			cp.[ClientID] = @ClientID 
		AND cp.[ClientPersonnelAdminGroupID] <> @AdminGroupID
		AND (@ShowAquariumUsers = 1 OR cp.IsAquarium = 0)
		ORDER BY cp.UserName
	END

	Select @@ROWCOUNT

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetByClientIDAndAdminGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetByClientIDAndAdminGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetByClientIDAndAdminGroupID] TO [sp_executeall]
GO
