SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-04-2013
-- Description:	Counts the SMSQuestionGroups for the given SMSQuestionnaire
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestionGroup__Count]
	@SmsQuestionnaireID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	SELECT Count(*) SMSQuestionGroupCount 
	FROM SMSQuestionGroup WITH (NOLOCK) 
	WHERE SMSQuestionnaireID=@SmsQuestionnaireID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup__Count] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup__Count] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup__Count] TO [sp_executeall]
GO
