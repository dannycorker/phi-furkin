SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-09-05
-- Description:	Saves sort direction for columns in the sql query editing tables
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__SaveColumnSortDirection]
(
	@SqlQueryEditingID INT,
	@SortDirections dbo.tvpIDValue READONLY
)
AS


UPDATE c
SET c.SortType = d.AnyValue,
c.SortOrder = CASE WHEN d.AnyValue = '' THEN NULL WHEN c.SortOrder > '' THEN c.SortOrder ELSE 9999 END -- Clear out the sort order if we are clearing the sort direction
FROM dbo.SqlQueryEditingColumns c
INNER JOIN @SortDirections d ON c.SqlQueryEditingColumnID = d.AnyID
WHERE c.SqlQueryEditingID = @SqlQueryEditingID


-- Now reset the sort order
;WITH InnerSql AS 
(
SELECT SqlQueryEditingColumnID, ROW_NUMBER() OVER(ORDER BY SortOrder) as rn 
FROM dbo.SqlQueryEditingColumns WITH (NOLOCK) 
WHERE SqlQueryEditingID = @SqlQueryEditingID
AND SortType > ''
)

UPDATE c
SET c.SortOrder = i.rn
FROM dbo.SqlQueryEditingColumns c
INNER JOIN InnerSql i ON c.SqlQueryEditingColumnID = i.SqlQueryEditingColumnID




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__SaveColumnSortDirection] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__SaveColumnSortDirection] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__SaveColumnSortDirection] TO [sp_executeall]
GO
