SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-21
-- Description:	Saves an entry to the password history table
-- =============================================
CREATE PROCEDURE [dbo].[PasswordHistory__SavePasswordHistory] 
	@Password VARCHAR(65),
	@Salt VARCHAR(50),
	@ClientPersonnelID INT = NULL,
	@PortalUserID INT = NULL,
	@ClientID INT
AS
BEGIN

	INSERT PasswordHistory (Password, Salt, ClientPersonnelID, PortalUserID, DateChanged, ClientID)
	VALUES (@Password, @Salt, @ClientPersonnelID, @PortalUserID, dbo.fn_GetDate_Local(), @ClientID)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PasswordHistory__SavePasswordHistory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PasswordHistory__SavePasswordHistory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PasswordHistory__SavePasswordHistory] TO [sp_executeall]
GO
