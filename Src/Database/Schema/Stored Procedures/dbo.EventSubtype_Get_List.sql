SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_Get_List]

AS


				
				SELECT
					[EventSubtypeID],
					[EventSubtypeName],
					[EventSubtypeDescription],
					[ApplyEventMaxThreshold],
					[Available]
				FROM
					[dbo].[EventSubtype] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Get_List] TO [sp_executeall]
GO
