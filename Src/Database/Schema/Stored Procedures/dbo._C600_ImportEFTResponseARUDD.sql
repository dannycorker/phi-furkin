SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-06-01
-- Description:	Import EFT ARUDD type files
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ImportEFTResponseARUDD]
	@FileName VARCHAR(100),
	@Identifier INT,
	@ReturnDate VARCHAR(100),
	@AccountName VARCHAR(100),
	@BSBNumber VARCHAR(100),
	@AccountNumber VARCHAR(100),
	@LodgementDate VARCHAR(100),
	@LodgementReference VARCHAR(100),
	@Remitter VARCHAR(100),
	@TargetAccountName VARCHAR(100),
	@TargetAccountBSB VARCHAR(100),
	@TargetAccountNumber VARCHAR(100),
	@Amount VARCHAR(100),
	@ReturnReason VARCHAR(100),
	@ReturnReasonCode VARCHAR(100),
	@UIN VARCHAR(100),
	@TransactionID VARCHAR(100)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XMLLog XML,
			@AccountID INT,
			@MatterID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID() -- 600 specific    

	SELECT @XMLLog = (
		SELECT @FileName AS [FileName],
				@Identifier AS [Identifier],
				@ReturnDate AS [ReturnDate],
				@AccountName AS [AccountName],
				@BSBNumber AS [BSBNumber],
				@AccountNumber AS [AccountNumber],
				@LodgementDate AS [LodgementDate],
				@LodgementReference AS [LodgementReference],
				@Remitter AS [Remitter],
				@TargetAccountName AS [TargetAccountName],
				@TargetAccountBSB AS [TargetAccountBSB],
				@TargetAccountNumber AS [TargetAccountNumber],
				@Amount AS [Amount],
				@ReturnReason AS [ReturnReason],
				@ReturnReasonCode AS [ReturnReasonCode],
				@UIN AS [UIN],
				@TransactionID AS [TransactionID]
		FOR XML PATH ('root')
	)

	EXEC dbo._C00_LogItXML 604, 1, '_C600_ImportEFTResponseARUDD', @XMLLog

	/*GPR 2020-05-15*/
	SELECT	@AccountID = a.AccountID,
			@MatterID = a.ObjectID 
	FROM Account a WITH (NOLOCK)
	WHERE a.Reference = @LodgementReference
	AND a.Active = 1
	AND a.ClientID = @ClientID

	INSERT INTO BACSPaymentFailures (
		ClientID, 
		FileName, 
		FailureFileType, 
		RowNum, 
		AccountNumber, 
		SortCode, 
		Name, 
		PaymentAmount,
		BACsProcessedDate, 
		FailureReason,
		FailureCode, 
		Reference, 
		AccountID, 
		ImportedDate, 
		ImportStatusID
	)
	 SELECT @ClientID,
			@Filename, 
			@Identifier, 
			1 /*TempValue*/,
			@AccountNumber, 
			@BsbNumber, 
			@Remitter, 
			@Amount, 
			dbo.fn_GetDate_Local(), 
			@ReturnReason,
			@ReturnReasonCode, 
			@LodgementReference, 
			@AccountID, 
			dbo.fn_GetDate_Local(), 
			1 /*GPR 2020-05-15 added ImportStatusID*/
	
	EXEC Billing__ReconcilePaymentFailures @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportEFTResponseARUDD] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ImportEFTResponseARUDD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportEFTResponseARUDD] TO [sp_executeall]
GO
