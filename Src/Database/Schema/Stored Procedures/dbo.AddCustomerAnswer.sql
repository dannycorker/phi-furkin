SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddCustomerAnswer    Script Date: 08/09/2006 12:22:50 ******/

CREATE PROCEDURE [dbo].[AddCustomerAnswer]
@CustomerQuestionnaireID int,
@MasterQuestionID int,
@Answer text,
@QuestionPossibleAnswerID int,
@ClientID int

as

insert into CustomerAnswers ( CustomerQuestionnaireID, MasterQuestionID, Answer, QuestionPossibleAnswerID, ClientID)
values (@CustomerQuestionnaireID, @MasterQuestionID, @Answer, @QuestionPossibleAnswerID, @ClientID )





GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddCustomerAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerAnswer] TO [sp_executeall]
GO
