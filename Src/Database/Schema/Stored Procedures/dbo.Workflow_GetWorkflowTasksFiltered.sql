SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














-- =============================================
-- Author:		Chris Townsend
-- Create date: 28th January 2008
-- Description:	Gets Workflow Tasks that belong the User's Client and optionally
--				Workflow Group responsibilities, UserID and/or WorkflowGroupID
-- Edit:		CT 25/03/08 - limited result set to 500 and ordered by
--				Important, Priority, AssignedDate 
--				ACE 2012-08-06 Nolocked
-- =============================================
CREATE PROCEDURE [dbo].[Workflow_GetWorkflowTasksFiltered]

@ClientID int,
@ManagerID int = 0,
@UserID int = 0,
@WorkflowGroupID int = 0

AS
BEGIN

	SET NOCOUNT ON

	SELECT	TOP (500)
			wt.WorkflowTaskID,
			wt.WorkflowGroupID,
			wt.AutomatedTaskID,
			wt.Priority,
			wt.AssignedTo,
			wt.AssignedDate,
			wt.LeadID,
			wt.CaseID,
			wt.EventTypeID,
			wt.ClientID,
			wt.FollowUp,
			wt.Important,
			wt.CreationDate,
			wt.Escalated,
			wt.EscalatedBy,
			wt.EscalationReason,
			wt.EscalationDate,
			wt.Disabled,

			wg.Name AS 'WorkflowGroupName',
			cp.FirstName + ' ' + cp.LastName AS 'AssigneeName',
			cp2.FirstName + ' ' + cp2.LastName AS 'EscalatorName',
			et.EventTypeName,
			et.EventTypeDescription,
			
			c.CustomerID,
			c.Fullname AS 'CustomerName'

	FROM	WorkflowTask wt WITH (NOLOCK) 

	INNER JOIN WorkflowGroup wg WITH (NOLOCK) ON wg.WorkflowGroupID = wt.WorkflowGroupID
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = wt.EventTypeID	
	INNER JOIN AutomatedTask at WITH (NOLOCK) ON at.TaskID = wt.AutomatedTaskID
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = wt.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	LEFT JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = wt.AssignedTo
	LEFT JOIN ClientPersonnel cp2 WITH (NOLOCK) ON cp2.ClientPersonnelID = wt.EscalatedBy

	WHERE	wt.ClientID = @ClientID
	AND		(@ManagerID = 0 OR wg.ManagerID = @ManagerID)
	AND		(@UserID = 0 OR wt.AssignedTo = @UserID)
	AND		(@WorkflowGroupID = 0 OR wt.WorkflowGroupID = @WorkflowGroupID)

	ORDER BY wt.Important, wt.Priority, wt.AssignedDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_GetWorkflowTasksFiltered] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Workflow_GetWorkflowTasksFiltered] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_GetWorkflowTasksFiltered] TO [sp_executeall]
GO
