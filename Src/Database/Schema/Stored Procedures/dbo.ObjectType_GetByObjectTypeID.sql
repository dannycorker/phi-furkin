SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectType_GetByObjectTypeID]
(

	@ObjectTypeID int   
)
AS


				SELECT
					[ObjectTypeID],
					[ClientID],
					[SubClientID],
					[ObjectTypeName],
					[ObjectTypeDescription],
					[Enabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ObjectType] WITH (NOLOCK) 
				WHERE
										[ObjectTypeID] = @ObjectTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_GetByObjectTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectType_GetByObjectTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_GetByObjectTypeID] TO [sp_executeall]
GO
