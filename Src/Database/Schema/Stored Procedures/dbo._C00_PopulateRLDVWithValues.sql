SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-10-13
-- Description:	Populate/Update a tablerow
--				NOTE TableDetailValues CAN be NULL allow NULL Insertions
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateRLDVWithValues]
(
	@ClientID int,
	@ClientPersonnelID int,
	@tvpIDValue tvpIDValue READONLY,
	@ResourceListID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Chesk to see if a table row id has been supplied, if it has then update the row, otherwise add it*/
	IF @ResourceListID IS NOT NULL
	BEGIN

		/* Update any values that already exist but have changed */
		UPDATE dbo.ResourceListDetailValues 
		SET DetailValue = t.AnyValue
		FROM @tvpIDValue t 
		INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = t.AnyID AND rldv.ResourceListID = @ResourceListID
		WHERE rldv.DetailValue <> t.AnyValue
	
		/* Insert any values that don't exist already */
		INSERT INTO dbo.ResourceListDetailValues (ClientID, LeadOrMatter, ResourceListID, DetailFieldID, DetailValue)
		SELECT df.ClientID, 4, @ResourceListID, t.AnyID, t.AnyValue 
		FROM @tvpIDValue t 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 4
		WHERE NOT EXISTS (
			SELECT * 
			FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK) 
			WHERE rldv.ResourceListID = @ResourceListID
			AND rldv.DetailFieldID = t.AnyID 
		)
	
	END
	ELSE
	BEGIN
	
		/* Create Table Row */
		INSERT INTO dbo.ResourceList (ClientID)
		VALUES (@ClientID)
		
		Select @ResourceListID = SCOPE_IDENTITY()
	
		/* Insert values into for that tablerow */
		INSERT INTO dbo.ResourceListDetailValues (ClientID, LeadOrMatter, ResourceListID, DetailFieldID, DetailValue)
		SELECT df.ClientID, 4, @ResourceListID, t.AnyID, t.AnyValue 
		FROM @tvpIDValue t 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 4

	END
	
	
	Return @ResourceListID

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateRLDVWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateRLDVWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateRLDVWithValues] TO [sp_executeall]
GO
