SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 08-JUN-2009
-- Description:	Proc to add a note
-- JWG 2011-01-31 Cater for LeadDocumentFS 
-- ACE 2014-07-10 Updated note to varchar max
-- CS  2014-12-15 Allowed block on LeadDocument creation
-- CS  2016-03-05 Allowed WhenCreated override
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AddANote]
(
	@CaseID INT,
	@NoteTypeID INT,
	@NoteText VARCHAR(MAX),
	@NotePriority INT,
	@WhoCreated INT,
	@ApplyToAll INT = 0,
	@BlockLeadDocumentCreation BIT = 0
	,@WhenCreated DATETIME = NULL
)

/* Note @Apply to all 
0 = No (IE Only this case)
1 = Yes (Ie this lead)
2 = Hell yeh (do it to the customer)
*/

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NoteTypeName VARCHAR(50),
			@LeadDocumentID INT,
			@LeadID INT,
			@ClientID INT,
			@GetDate datetime = dbo.fn_GetDate_Local(), 
			@NoteTextVarBin varbinary(max),
			@LeadEventID INT,
			@LogEntry VARCHAR (MAX)
			
			
	SELECT @LeadID = ca.LeadID, 
	@ClientID = ca.ClientID, 
	@NoteTextVarBin = CONVERT(VARBINARY(MAX), @NoteText) 
	FROM dbo.Cases ca WITH (NOLOCK) 
	WHERE ca.CaseID = @CaseID

	IF @ClientID IS NULL 
	BEGIN
	
		SELECT @LogEntry	= '_C00_AddANote CaseID = ' 
							+ ISNULL(CONVERT(VARCHAR,@CaseID),'NULL')
							+ ', WhoCreated = ' 
							+ ISNULL(CONVERT(VARCHAR,@WhoCreated),'NULL')
							+ ''''
				
		EXEC _C00_LogIt 'Info', '_C600_LeadTypeRelationships', '_C00_AddaNote', @LogEntry, 58544 /*James Lewis*/						

	END

	SELECT @NoteTypeName = nt.NoteTypeName
	FROM dbo.Notetype nt WITH (NOLOCK) 
	WHERE nt.NoteTypeID = @NoteTypeID


	/*
	INSERT INTO LeadDocument(ClientID, LeadID, DocumentTypeID, LeadDocumentTitle, UploadDateTime, WhoUploaded, DocumentBLOB, [FileName], DocumentFormat) 
	SELECT ClientID, LeadID, NULL, @NoteTypeName, dbo.fn_GetDate_Local(), @WhoCreated, CONVERT(VARBINARY(MAX), @NoteText), @NoteTypeID, 'NOTE' 
	FROM Cases 
	WHERE CaseID = @CaseID
	
	SELECT @LeadDocumentID = SCOPE_IDENTITY()
	*/
	IF @BlockLeadDocumentCreation = 0 /* CS 2014-12-15 following discussion with Jim */
	BEGIN
		EXEC @LeadDocumentID = dbo._C00_CreateLeadDocument @ClientID = @ClientID, @LeadID = @LeadID, @DocumentBLOB = @NoteTextVarBin, @LeadDocumentTitle = @NoteTypeName, @DocumentTypeID = NULL, @UploadDateTime = @GetDate, @WhoUploaded = @WhoCreated, @FileName = @NoteTypeID, @DocumentFormat = 'NOTE' 
	END

	IF @ApplyToAll = 0
	BEGIN
		INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments)
		SELECT Cases.ClientID, Cases.LeadID, ISNULL(@WhenCreated,dbo.fn_GetDate_Local()), @WhoCreated, 0.00, LEFT(@NoteText,2000), NULL, @NoteTypeID, NULL, ISNULL(@WhenCreated,dbo.fn_GetDate_Local()), NULL, NULL, Cases.CaseID , @LeadDocumentID, @NotePriority, NULL, 0, NULL, NULL
		FROM dbo.Cases 
		WHERE CaseID = @CaseID
		
		SELECT @LeadEventID = SCOPE_IDENTITY()
	END

	IF @ApplyToAll = 1
	BEGIN

		INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments)
		SELECT Cases.ClientID, Cases.LeadID, ISNULL(@WhenCreated,dbo.fn_GetDate_Local()), @WhoCreated, 0.00, LEFT(@NoteText,2000), NULL, @NoteTypeID, NULL, ISNULL(@WhenCreated,dbo.fn_GetDate_Local()), NULL, NULL, Cases.CaseID , @LeadDocumentID, @NotePriority, NULL, 0, NULL, NULL
		FROM dbo.Cases 
		WHERE LeadID = @LeadID

		SELECT @LeadEventID = SCOPE_IDENTITY()
	END

	IF @ApplyToAll = 2
	BEGIN

		INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments)
		SELECT c.ClientID, c.LeadID, ISNULL(@WhenCreated,dbo.fn_GetDate_Local()), @WhoCreated, 0.00, LEFT(@NoteText,2000), NULL, @NoteTypeID, NULL, ISNULL(@WhenCreated,dbo.fn_GetDate_Local()), NULL, NULL, c.CaseID , @LeadDocumentID, @NotePriority, NULL, 0, NULL, NULL
		FROM dbo.Cases 
		INNER JOIN dbo.Lead l ON l.LeadID = Cases.LeadID
		INNER JOIN dbo.Lead l2 ON l2.CustomerID = l.CustomerID 
		INNER JOIN dbo.Cases c ON c.leadid = l2.leadid
		WHERE Cases.CaseID = @CaseID

		SELECT @LeadEventID = SCOPE_IDENTITY()
	END
	
	IF @BlockLeadDocumentCreation = 0
	BEGIN
		RETURN @LeadDocumentID
	END
	ELSE
	BEGIN
		RETURN @LeadEventID
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddANote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AddANote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddANote] TO [sp_executeall]
GO
