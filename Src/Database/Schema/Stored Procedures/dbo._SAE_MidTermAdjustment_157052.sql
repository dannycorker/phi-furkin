SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-05
-- Description:	Sql After Event - Abort MTA
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_MidTermAdjustment_157052]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@EventComments					VARCHAR(2000)
			,@WrittenPremiumID				INT

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */
		
	IF @EventTypeID = 157052  /*Abort MTA*/
	BEGIN 
		
		/*GPR 2020-04-09 no requirement to delete the table row anymore. Select out the WrittenPremiumID for this aborted MTA*/
		SELECT @WrittenPremiumID = (SELECT TOP(1) wp.WrittenPremiumID
		FROM WrittenPremium wp WHERE AdjustmentTypeID = 9
		AND wp.MatterID = @MatterID
		ORDER BY 1 DESC)

		SELECT @EventComments = 'MTA Aborted, WrittenPremiumID:' + CAST(@WrittenPremiumID as VARCHAR (20))

		/*GPR 2020-04-09 Delete the MTA Quote WrittenPremium Row*/
		DELETE FROM WrittenPremium
		WHERE WrittenPremiumID = @WrittenPremiumID

		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		
		/*SA 2018-04-17 - Defect #264_600 - Clear out address fields*/
		EXEC _C00_SimpleValueIntoField 175314,'',@CustomerID,@WhoCreated /*Address Line 1*/
		EXEC _C00_SimpleValueIntoField 175315,'',@CustomerID,@WhoCreated /*Address Line 2*/
		EXEC _C00_SimpleValueIntoField 175316,'',@CustomerID,@WhoCreated /*Town*/
		EXEC _C00_SimpleValueIntoField 175317,'',@CustomerID,@WhoCreated /*County*/
		EXEC _C00_SimpleValueIntoField 175318,'',@CustomerID,@WhoCreated /*Postcode*/
		/*GPR 2020-04-09 - clear out the Address Picker*/
		EXEC _C00_SimpleValueIntoField 175478,'',@CustomerID,@WhoCreated

	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_157052] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_MidTermAdjustment_157052] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_MidTermAdjustment_157052] TO [sp_executeall]
GO
