SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChartPalette table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartPalette_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartPaletteID int   = null ,

	@ChartPaletteName varchar (50)  = null ,

	@ChartPalettePreviewImageUrl varchar (255)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartPaletteID]
	, [ChartPaletteName]
	, [ChartPalettePreviewImageUrl]
    FROM
	[dbo].[ChartPalette] WITH (NOLOCK) 
    WHERE 
	 ([ChartPaletteID] = @ChartPaletteID OR @ChartPaletteID IS NULL)
	AND ([ChartPaletteName] = @ChartPaletteName OR @ChartPaletteName IS NULL)
	AND ([ChartPalettePreviewImageUrl] = @ChartPalettePreviewImageUrl OR @ChartPalettePreviewImageUrl IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartPaletteID]
	, [ChartPaletteName]
	, [ChartPalettePreviewImageUrl]
    FROM
	[dbo].[ChartPalette] WITH (NOLOCK) 
    WHERE 
	 ([ChartPaletteID] = @ChartPaletteID AND @ChartPaletteID is not null)
	OR ([ChartPaletteName] = @ChartPaletteName AND @ChartPaletteName is not null)
	OR ([ChartPalettePreviewImageUrl] = @ChartPalettePreviewImageUrl AND @ChartPalettePreviewImageUrl is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartPalette_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Find] TO [sp_executeall]
GO
