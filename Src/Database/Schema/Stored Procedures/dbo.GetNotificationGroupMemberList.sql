SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Notification Group Member List
-- =============================================
CREATE PROCEDURE [dbo].[GetNotificationGroupMemberList] 
	@ClientPersonnelID INT = NULL,
	@NotificationGroupID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ngm.[NotificationGroupMemberID],
		ngm.[NotificationGroupID],
		ngm.[ClientID],
		ngm.[ClientPersonnelID]
	FROM 
		[dbo].[NotificationGroupMember] ngm WITH (NOLOCK)
		INNER JOIN [dbo].[NotificationGroup] ng WITH (NOLOCK) ON ng.[NotificationGroupID] = ngm.[NotificationGroupID]
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = ngm.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND ngm.[ClientID] = cp.[ClientID]
		AND ngm.[NotificationGroupID] = ng.[NotificationGroupID]
	ORDER BY ngm.[NotificationGroupMemberID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetNotificationGroupMemberList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetNotificationGroupMemberList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetNotificationGroupMemberList] TO [sp_executeall]
GO
