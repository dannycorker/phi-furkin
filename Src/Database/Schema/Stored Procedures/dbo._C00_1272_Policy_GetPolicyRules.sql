SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-13
-- Description:	Gets the policy wording for a claim
-- Used by:		DF 145675
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetPolicyRules] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CurrentPolicyMatterID INT
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	
	DECLARE @PolicyCaseID INT
	SELECT @PolicyCaseID = CaseID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @CurrentPolicyMatterID

	SELECT DISTINCT s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, tdvRule.DetailValue AS PolicyRule
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON s.Out_ResourceListID = tdvRL.ResourceListID AND tdvRL.DetailFieldID = 146413
	INNER JOIN dbo.TableDetailValues tdvRule WITH (NOLOCK) ON tdvRL.TableRowID = tdvRule.TableRowID AND tdvRule.DetailFieldID = 146414
	WHERE r.MatterID = @MatterID 
	AND tdvRL.CaseID = @PolicyCaseID
	--UNION -- Always return all the general exclusions
	--SELECT DISTINCT tdvRL.ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, tdvRule.DetailValue AS PolicyRule
	--FROM dbo.TableRows r WITH (NOLOCK) 
	--INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON r.TableRowID = tdvRL.TableRowID AND tdvRL.DetailFieldID = 146413
	--INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON tdvRL.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	--INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	--INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON tdvRL.ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	--INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	--INNER JOIN dbo.TableDetailValues tdvRule WITH (NOLOCK) ON r.TableRowID = tdvRule.TableRowID AND tdvRule.DetailFieldID = 146414
	--WHERE r.CaseID = @PolicyCaseID
	--AND tdvRL.ResourceListID = 76571

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicyRules] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetPolicyRules] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicyRules] TO [sp_executeall]
GO
