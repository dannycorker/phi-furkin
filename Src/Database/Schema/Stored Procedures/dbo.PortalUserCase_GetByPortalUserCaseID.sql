SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserCase table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserCase_GetByPortalUserCaseID]
(

	@PortalUserCaseID int   
)
AS


				SELECT
					[PortalUserCaseID],
					[PortalUserID],
					[LeadID],
					[CaseID],
					[ClientID]
				FROM
					[dbo].[PortalUserCase] WITH (NOLOCK) 
				WHERE
										[PortalUserCaseID] = @PortalUserCaseID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_GetByPortalUserCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_GetByPortalUserCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_GetByPortalUserCaseID] TO [sp_executeall]
GO
