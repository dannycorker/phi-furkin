SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-02
-- Description:	Asset History
-- =============================================
CREATE PROCEDURE [dbo].[AssetHistory__GetList] 
	@ClientPersonnelID INT = NULL,
	@AssetID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ah.[AssetHistoryID],
		ah.[AssetID],
		ah.[ClientID],
		ah.[AssetTypeID],
		ah.[AssetSubTypeID],
		ah.[LocationID],
		ah.[AssetName],
		ah.[Note],
		ah.[SecureNote],
		ah.[SecureUserName],
		ah.[SecurePassword],
		ah.[SecureOther],
		ah.[Version],
		ah.[ValidFrom],
		ah.[ValidTo],
		ah.[ReminderDue],
		ah.[ReminderTimeUnitID],
		ah.[ReminderTimeUnitQuantity],
		ah.[ReminderNotificationGroupID],
		ah.[Enabled],
		ah.[Deleted],
		ah.[WhoCreated],
		ah.[WhenCreated],
		ah.[WhoModified],
		ah.[WhenModified]
	FROM 
		[dbo].[AssetHistory] ah WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) 
			ON cp.[ClientID] = ah.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND ah.[AssetID] = @AssetID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory__GetList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory__GetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory__GetList] TO [sp_executeall]
GO
