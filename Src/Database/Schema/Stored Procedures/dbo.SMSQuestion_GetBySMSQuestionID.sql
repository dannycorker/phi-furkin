SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSQuestion table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_GetBySMSQuestionID]
(

	@SMSQuestionID int   
)
AS


				SELECT
					[SMSQuestionID],
					[ClientID],
					[SMSQuestionnaireID],
					[Question],
					[QuestionOrder],
					[DocumentTypeID],
					[Enabled],
					[Timeout],
					[Repeat],
					[RepeatPrefix],
					[DetailFieldToSave],
					[EvaluateResponse],
					[VoiceAction],
					[RedirectTo],
					[GatherNumDigits],
					[RecordingMaxLength]
				FROM
					[dbo].[SMSQuestion] WITH (NOLOCK) 
				WHERE
										[SMSQuestionID] = @SMSQuestionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_GetBySMSQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_GetBySMSQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_GetBySMSQuestionID] TO [sp_executeall]
GO
