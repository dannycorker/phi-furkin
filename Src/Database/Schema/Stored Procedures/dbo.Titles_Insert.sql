SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Titles table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Titles_Insert]
(

	@TitleID int    OUTPUT,

	@Title varchar (50)  
)
AS


				
				INSERT INTO [dbo].[Titles]
					(
					[Title]
					)
				VALUES
					(
					@Title
					)
				-- Get the identity value
				SET @TitleID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Titles_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Titles_Insert] TO [sp_executeall]
GO
