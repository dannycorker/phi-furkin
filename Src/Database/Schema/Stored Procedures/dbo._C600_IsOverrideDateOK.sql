SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-05-25
-- Description:	Checks that override date for next payment collection is acceptable
-- Mods
-- CW 2018-04-17 Allow OMF policies through as they do not have a collections lead type.
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_IsOverrideDateOK]
	@CaseID INT,
	@WhoCreated INT = NULL,
	@DisplayOnly INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID(),
			@CollectionsCaseID INT,
			@CollectionsLeadID INT,
			@CollectionsMatterID INT,
			@DetailValue VARCHAR(20),
			@IsOK INT = 1,
			@MatterID INT,
			@PolicyAdminMatterID INT,
			@WaitingDays INT
			
	SELECT @WhoCreated=CASE WHEN @WhoCreated IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@ClientID,53,'CFG|AqAutomationCPID',0) ELSE @WhoCreated END
	
	IF EXISTS /*CW 2018-04-17*/
	(SELECT *
	FROM dbo.Cases c WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.CaseID 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170034
	WHERE mdv.ValueInt = 151577 and c.CaseID = @CaseID)
	BEGIN
	RETURN @IsOK
	END

	-- if this is a policy admin case copy the override fields to the correct collections case and flip the Caseid
	IF EXISTS ( SELECT * FROM Cases ca WITH (NOLOCK)
				INNER JOIN Lead l WITH (NOLOCK) ON ca.LeadID=l.LeadID AND l.LeadTypeID=1492 
				WHERE ca.CaseID=@CaseID )
	BEGIN

		SELECT @PolicyAdminMatterID=m.MatterID, @CollectionsMatterID=ltr.ToMatterID, @CollectionsCaseID=mc.CaseID, @CollectionsLeadID=mc.LeadID 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON m.MatterID=ltr.FromMatterID AND ltr.ToLeadTypeID=1493
		INNER JOIN Matter mc WITH (NOLOCK) ON ltr.ToMatterID=mc.MatterID
		WHERE m.CaseID=@CaseID
		
		SELECT @DetailValue=ISNULL(dbo.fnGetDv (175449, @CaseID),'')
		EXEC dbo._C00_SimpleValueIntoField 175407, @DetailValue, @CollectionsMatterID, @WhoCreated	
		IF @DisplayOnly=0
			EXEC dbo._C00_SimpleValueIntoField 175449, '', @PolicyAdminMatterID, @WhoCreated	
		
		SELECT @DetailValue=ISNULL(dbo.fnGetDv (175450, @CaseID),'')
		EXEC dbo._C00_SimpleValueIntoField 175409, @DetailValue, @CollectionsMatterID, @WhoCreated	
		IF @DisplayOnly=0
			EXEC dbo._C00_SimpleValueIntoField 175450, '', @PolicyAdminMatterID, @WhoCreated
		
		SELECT @CaseID=@CollectionsCaseID	

	END

	SELECT @WaitingDays=RegularPaymentWait FROM BillingConfiguration WITH (NOLOCK) WHERE ClientID=@ClientID
	IF dbo.fnGetDvAsDate(175407,@CaseID) IS NOT NULL 
		AND dbo.fnGetDvAsDate(175407,@CaseID) < DATEADD(DAY,@WaitingDays,CONVERT(DATE,dbo.fn_GetDate_Local()))
		AND 5144 <> dbo.fnGetDvAsInt(175409,@CaseID)
	BEGIN
	
		SELECT @IsOK=0
	
	END
	
	IF @DisplayOnly=1
	BEGIN
		
		EXEC dbo._C00_SimpleValueIntoField 175407, '', @CollectionsMatterID, @WhoCreated	
		EXEC dbo._C00_SimpleValueIntoField 175409, '', @CollectionsMatterID, @WhoCreated	
	
	END

	RETURN @IsOK
	
END

SELECT * FROM BillingConfiguration
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_IsOverrideDateOK] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_IsOverrideDateOK] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_IsOverrideDateOK] TO [sp_executeall]
GO
