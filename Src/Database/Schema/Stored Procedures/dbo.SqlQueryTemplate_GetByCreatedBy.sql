SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryTemplate table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_GetByCreatedBy]
(

	@CreatedBy int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryTemplateID],
					[ClientID],
					[Name],
					[Description],
					[TemplatePath],
					[DataWorksheetNumber],
					[DataStartCell],
					[TemplateFormat],
					[FolderID],
					[CreatedBy],
					[CreatedOn],
					[ModifiedBy],
					[ModifiedOn],
					[Enabled]
				FROM
					[dbo].[SqlQueryTemplate] WITH (NOLOCK) 
				WHERE
					[CreatedBy] = @CreatedBy
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_GetByCreatedBy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_GetByCreatedBy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_GetByCreatedBy] TO [sp_executeall]
GO
