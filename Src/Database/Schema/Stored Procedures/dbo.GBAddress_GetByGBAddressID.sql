SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GBAddress table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GBAddress_GetByGBAddressID]
(

	@GBAddressID int   
)
AS


				SELECT
					[GBAddressID],
					[ClientID],
					[WebServiceURL]
				FROM
					[dbo].[GBAddress] WITH (NOLOCK) 
				WHERE
										[GBAddressID] = @GBAddressID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_GetByGBAddressID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GBAddress_GetByGBAddressID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_GetByGBAddressID] TO [sp_executeall]
GO
