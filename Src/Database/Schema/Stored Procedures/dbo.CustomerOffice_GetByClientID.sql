SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerOffice table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[OfficeID],
					[ClientID],
					[CustomerID],
					[OfficeName],
					[MainPhoneNumber],
					[FaxNumber],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Postcode],
					[Country],
					[Notes],
					[CountryID]
				FROM
					[dbo].[CustomerOffice] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_GetByClientID] TO [sp_executeall]
GO
