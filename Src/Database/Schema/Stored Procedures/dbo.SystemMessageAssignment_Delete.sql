SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SystemMessageAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_Delete]
(

	@SystemMessageAssignmentID int   
)
AS


				DELETE FROM [dbo].[SystemMessageAssignment] WITH (ROWLOCK) 
				WHERE
					[SystemMessageAssignmentID] = @SystemMessageAssignmentID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Delete] TO [sp_executeall]
GO
