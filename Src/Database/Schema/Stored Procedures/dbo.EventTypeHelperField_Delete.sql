SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeHelperField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeHelperField_Delete]
(

	@EventTypeHelperFieldID int   
)
AS


				DELETE FROM [dbo].[EventTypeHelperField] WITH (ROWLOCK) 
				WHERE
					[EventTypeHelperFieldID] = @EventTypeHelperFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_Delete] TO [sp_executeall]
GO
