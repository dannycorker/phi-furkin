SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-01-05
-- Description:	Create All Missing Fields for an entity
-- =============================================
CREATE PROCEDURE [dbo].[DetailValues__CreateAll]
	@ClientID int,
	@CustomerID int,
	@LeadID int,
	@CaseID int,
	@MatterID int,
	@ClientPersonnelID int,
	@ContactID int
AS
BEGIN
	SET NOCOUNT ON;

	IF @ClientID > 0
	BEGIN
	
		INSERT INTO dbo.ClientDetailValues (ClientID, DetailFieldID, DetailValue)
		SELECT df.ClientID, df.DetailFieldID, ''
		FROM dbo.fnDetailFieldsShared(@ClientID) df 
		WHERE df.ClientID = @ClientID
		AND df.LeadOrMatter = 12
		AND NOT EXISTS (
			SELECT *
			FROM dbo.ClientDetailValues cdv WITH (NOLOCK)
			WHERE cdv.ClientID = @ClientID
			AND cdv.DetailFieldID = df.DetailFieldID 
		)
	
	END

	IF @CustomerID > 0
	BEGIN
	
		INSERT INTO dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
		SELECT df.ClientID, @CustomerID, df.DetailFieldID, ''
		FROM dbo.fnDetailFieldsShared(@ClientID) df 
		WHERE df.ClientID = @ClientID
		AND df.LeadOrMatter = 10
		AND NOT EXISTS (
			SELECT *
			FROM dbo.CustomerDetailValues cdv WITH (NOLOCK)
			WHERE cdv.CustomerID = @CustomerID
			AND cdv.DetailFieldID = df.DetailFieldID 
		)
	
	END

	IF @LeadID > 0
	BEGIN
	
		INSERT INTO dbo.LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
		SELECT df.ClientID, l.LeadID, df.DetailFieldID, ''
		FROM dbo.fnDetailFieldsShared(@ClientID) df
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID AND l.LeadID = @LeadID
		WHERE df.LeadOrMatter = 1
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.LeadDetailValues ldv WITH (NOLOCK)
			WHERE ldv.LeadID = l.LeadID
			AND ldv.DetailFieldID = df.DetailFieldID 
		)
	
	END
	
	IF @CaseID > 0
	BEGIN
	
		INSERT INTO dbo.CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue)
		SELECT df.ClientID, c.CaseID, df.DetailFieldID, ''
		FROM dbo.fnDetailFieldsShared(@ClientID) df 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID
		INNER JOIN dbo.Cases c WITH (NOLOCK) on c.LeadID = l.LeadID and c.CaseID = @CaseID
		WHERE df.LeadOrMatter = 11
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.CaseDetailValues cdv WITH (NOLOCK)
			WHERE cdv.CaseID = c.CaseID
			AND cdv.DetailFieldID = df.DetailFieldID 
		)
	
	END
	
	IF @MatterID > 0
	BEGIN
	
		INSERT INTO dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		SELECT df.ClientID, m.LeadID, m.MatterID, DetailFieldID, ''
		FROM dbo.fnDetailFieldsShared(@ClientID) df 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID and m.MatterID = @MatterID
		WHERE df.LeadOrMatter = 2
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.MatterDetailValues mdv WITH (NOLOCK)
			WHERE mdv.MatterID = m.MatterID
			AND mdv.DetailFieldID = df.DetailFieldID 
		)
	
	END
	
	IF @ClientPersonnelID > 0
	BEGIN
	
		INSERT INTO dbo.ClientPersonnelDetailValues (ClientID, ClientPersonnelID, DetailFieldID, DetailValue)
		SELECT df.ClientID, cp.ClientPersonnelID, DetailFieldID, ''
		FROM dbo.fnDetailFieldsShared(@ClientID) df 
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientID = df.ClientID and cp.ClientPersonnelID = @ClientPersonnelID
		WHERE df.LeadOrMatter = 13
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.ClientPersonnelDetailValues cpdv WITH (NOLOCK)
			WHERE cpdv.ClientPersonnelID = cp.ClientPersonnelID
			AND cpdv.DetailFieldID = df.DetailFieldID 
		)
	
	END
	
	IF @ContactID > 0
	BEGIN
	
		INSERT INTO dbo.ContactDetailValues (ClientID, ContactID, DetailFieldID, DetailValue)
		SELECT df.ClientID, con.ContactID, DetailFieldID, ''
		FROM fnDetailFieldsShared(@ClientID) df 
		INNER JOIN dbo.Contact con WITH (NOLOCK) on con.ClientID = df.ClientID and con.ContactID = @ContactID
		WHERE df.LeadOrMatter = 14
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.ContactDetailValues condv WITH (NOLOCK)
			WHERE condv.ContactID = con.ContactID
			AND condv.DetailFieldID = df.DetailFieldID 
		)
	
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues__CreateAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValues__CreateAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues__CreateAll] TO [sp_executeall]
GO
