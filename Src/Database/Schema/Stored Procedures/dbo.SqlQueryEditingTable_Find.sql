SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryEditingTable table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingTable_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryEditingTableID int   = null ,

	@ClientID int   = null ,

	@SqlQueryEditingID int   = null ,

	@SqlQueryTableName varchar (50)  = null ,

	@TableAlias varchar (50)  = null ,

	@TableDisplayOrder int   = null ,

	@JoinType varchar (25)  = null ,

	@JoinText varchar (500)  = null ,

	@JoinTableID int   = null ,

	@JoinRTRID int   = null ,

	@RealSqlQueryTableID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryEditingTableID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryTableName]
	, [TableAlias]
	, [TableDisplayOrder]
	, [JoinType]
	, [JoinText]
	, [JoinTableID]
	, [JoinRTRID]
	, [RealSqlQueryTableID]
    FROM
	[dbo].[SqlQueryEditingTable] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingTableID] = @SqlQueryEditingTableID OR @SqlQueryEditingTableID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryEditingID] = @SqlQueryEditingID OR @SqlQueryEditingID IS NULL)
	AND ([SqlQueryTableName] = @SqlQueryTableName OR @SqlQueryTableName IS NULL)
	AND ([TableAlias] = @TableAlias OR @TableAlias IS NULL)
	AND ([TableDisplayOrder] = @TableDisplayOrder OR @TableDisplayOrder IS NULL)
	AND ([JoinType] = @JoinType OR @JoinType IS NULL)
	AND ([JoinText] = @JoinText OR @JoinText IS NULL)
	AND ([JoinTableID] = @JoinTableID OR @JoinTableID IS NULL)
	AND ([JoinRTRID] = @JoinRTRID OR @JoinRTRID IS NULL)
	AND ([RealSqlQueryTableID] = @RealSqlQueryTableID OR @RealSqlQueryTableID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryEditingTableID]
	, [ClientID]
	, [SqlQueryEditingID]
	, [SqlQueryTableName]
	, [TableAlias]
	, [TableDisplayOrder]
	, [JoinType]
	, [JoinText]
	, [JoinTableID]
	, [JoinRTRID]
	, [RealSqlQueryTableID]
    FROM
	[dbo].[SqlQueryEditingTable] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryEditingTableID] = @SqlQueryEditingTableID AND @SqlQueryEditingTableID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryEditingID] = @SqlQueryEditingID AND @SqlQueryEditingID is not null)
	OR ([SqlQueryTableName] = @SqlQueryTableName AND @SqlQueryTableName is not null)
	OR ([TableAlias] = @TableAlias AND @TableAlias is not null)
	OR ([TableDisplayOrder] = @TableDisplayOrder AND @TableDisplayOrder is not null)
	OR ([JoinType] = @JoinType AND @JoinType is not null)
	OR ([JoinText] = @JoinText AND @JoinText is not null)
	OR ([JoinTableID] = @JoinTableID AND @JoinTableID is not null)
	OR ([JoinRTRID] = @JoinRTRID AND @JoinRTRID is not null)
	OR ([RealSqlQueryTableID] = @RealSqlQueryTableID AND @RealSqlQueryTableID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_Find] TO [sp_executeall]
GO
