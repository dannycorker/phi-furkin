SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-04
-- Description:	Updates a document batch detail record as 
--				processed

-- Parameters:  @DocumentBatchDetailID
--              The document batch entry to update
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail_UpdateAsProcessed]
	@DocumentBatchDetailID INT
AS
BEGIN
	DECLARE @DocumentBatchId INT
	
	IF (@DocumentBatchDetailID IS NOT NULL)
		BEGIN
			UPDATE DocumentBatchDetail
				SET Uploaded = 1
			WHERE
				@DocumentBatchDetailID = DocumentBatchDetailID
		END
		
	SELECT
		@DocumentBatchId = DocumentBatchId
	FROM
		DocumentBatchDetail
	WHERE
		DocumentBatchDetailID = @DocumentBatchDetailID
		
	IF (SELECT COUNT(*) FROM DocumentBatchDetail
		WHERE DocumentBatchID = @DocumentBatchId
		AND Uploaded <> 1) = 0
	BEGIN
		UPDATE
			DocumentBatch
		SET
			Processed = 1
		WHERE
			DocumentBatchId = @DocumentBatchID
	END
	
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_UpdateAsProcessed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail_UpdateAsProcessed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_UpdateAsProcessed] TO [sp_executeall]
GO
