SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 15/05/2018
-- Description:	Gets a list of SmtpClientConfig by from client id
-- =============================================
CREATE PROCEDURE [dbo].[SmtpClientConfig__GetByFromClientID]
	@ClientId INT	
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM SmtpClientConfig WITH (NOLOCK) 
	WHERE FromClientID = @ClientId
	
END;
GO
GRANT VIEW DEFINITION ON  [dbo].[SmtpClientConfig__GetByFromClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SmtpClientConfig__GetByFromClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SmtpClientConfig__GetByFromClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SmtpClientConfig__GetByFromClientID] TO [sp_executehelper]
GO
