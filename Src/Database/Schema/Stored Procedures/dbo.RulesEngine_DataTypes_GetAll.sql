SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-29
-- Description:	Returns all data types for the rule sets
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_DataTypes_GetAll]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.RulesEngine_DataTypes WITH (NOLOCK) 
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_DataTypes_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_DataTypes_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_DataTypes_GetAll] TO [sp_executeall]
GO
