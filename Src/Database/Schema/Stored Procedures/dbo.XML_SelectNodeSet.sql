SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-11-26
-- Description:	Creates and execs dynamic sql to get a section of the XML.
-- =============================================
CREATE PROCEDURE [dbo].[XML_SelectNodeSet] 
	@Xml XML,
	@XPath VARCHAR(1000),
	@NodeSet XML OUTPUT,
	@Namespace VARCHAR(1000) = NULL,
	@NamespacePrefix VARCHAR(50)= NULL
AS
BEGIN
	
	DECLARE @EncodedXPath VARCHAR(2000)
	SELECT @EncodedXPath = REPLACE(@XPath, '''', '''''')
	
	DECLARE @ReturnVal XML
	
	DECLARE @Sql NVARCHAR(1000)
	SELECT @Sql = ''
	IF @Namespace IS NOT NULL AND @NamespacePrefix IS NOT NULL
	BEGIN
		SELECT @Sql += 'WITH XMLNAMESPACES (''' + @Namespace + ''' as ' + @NamespacePrefix + ') '
	END
	SELECT @Sql += N'SELECT @ReturnValOut = @Xml.query(''' + @EncodedXPath + N''')'  
	
	EXEC sp_executesql @Sql, N'@Xml XML, @ReturnValOut XML OUTPUT', @Xml, @ReturnValOut = @ReturnVal OUTPUT
	
	SELECT @NodeSet = @ReturnVal
END




GO
GRANT VIEW DEFINITION ON  [dbo].[XML_SelectNodeSet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[XML_SelectNodeSet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[XML_SelectNodeSet] TO [sp_executeall]
GO
