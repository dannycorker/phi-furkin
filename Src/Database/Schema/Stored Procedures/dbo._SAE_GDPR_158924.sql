SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2020-01-29
-- Description:	Sql After Event - GDPR Right to Erasure
-- Begin - GDPR Right to Erasure
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_GDPR_158924]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@ActivePolicies				INT


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	IF @EventTypeID IN (158924)
	BEGIN
		SELECT @ActivePolicies = COUNT(l.LeadID)
		FROM (Lead l
		INNER JOIN Customers c ON c.CustomerID = l.CustomerID)
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID AND l.LeadTypeID = 1492 -- Policy Admin
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170038 --Policy Status
		WHERE mdv.ValueInt = 43002 -- Active
		AND c.CustomerID = @CustomerID
		GROUP BY c.CustomerID

		IF @ActivePolicies > 0
		BEGIN
		SELECT @ErrorMessage = '<br><br><font color="red">Error: This event can only be applied when the Customer has zero active Policies.</font></br><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END
		ELSE
		BEGIN
			/*Morph into in process event, following up all threads*/	
			EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 158926, 0 -- GDPR Initiate Obfuscation
		END
	END
			
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_GDPR_158924] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_GDPR_158924] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_GDPR_158924] TO [sp_executeall]
GO
