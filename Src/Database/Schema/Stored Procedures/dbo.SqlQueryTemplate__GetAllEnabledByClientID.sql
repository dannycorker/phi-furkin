SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:	Chris Townsend
-- Date:		01/06/2010
-- Purpose:		Select records from the SqlQueryTemplate table through a foreign key, 
				including client zero records and excluding disabled records
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate__GetAllEnabledByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SqlQueryTemplateID],
					[ClientID],
					[Name] + ' ' +
					CASE 
					WHEN TemplateFormat IN ('XLSX','XLSM','XLTX') THEN '(2007)'
					WHEN TemplateFormat IN ('XLS','XLT') THEN '(2003)'
					ELSE '(CSV)' END AS [Name]
					,
					[Description],
					[TemplatePath],
					[DataWorksheetNumber],
					[DataStartCell],
					[TemplateFormat],
					[FolderID],
					[CreatedBy],
					[CreatedOn],
					[ModifiedBy],
					[ModifiedOn],
					[Enabled]
				FROM
					[dbo].[SqlQueryTemplate]
				WHERE
					([ClientID] = @ClientID OR [ClientID] = 0)
				AND Enabled = 1
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate__GetAllEnabledByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate__GetAllEnabledByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate__GetAllEnabledByClientID] TO [sp_executeall]
GO
