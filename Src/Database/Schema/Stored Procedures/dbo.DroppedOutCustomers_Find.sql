SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DroppedOutCustomers table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomerID int   = null ,

	@ClientID int   = null ,

	@TitleID int   = null ,

	@FirstName varchar (100)  = null ,

	@MiddleName varchar (100)  = null ,

	@LastName varchar (100)  = null ,

	@EmailAddress varchar (255)  = null ,

	@DayTimeTelephoneNumber varchar (50)  = null ,

	@HomeTelephone varchar (50)  = null ,

	@MobileTelephone varchar (50)  = null ,

	@CompanyTelephone varchar (50)  = null ,

	@WorksTelephone varchar (50)  = null ,

	@Address1 varchar (200)  = null ,

	@Address2 varchar (200)  = null ,

	@Town varchar (200)  = null ,

	@County varchar (200)  = null ,

	@PostCode varchar (10)  = null ,

	@HasDownloaded bit   = null ,

	@DownloadedOn datetime   = null ,

	@AquariumStatusID int   = null ,

	@ClientStatusID int   = null ,

	@Test bit   = null ,

	@CompanyName varchar (100)  = null ,

	@Occupation varchar (100)  = null ,

	@Employer varchar (100)  = null ,

	@ReminderEmailSentDate datetime   = null ,

	@ReminderSmsSentDate datetime   = null ,

	@CountryID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomerID]
	, [ClientID]
	, [TitleID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [EmailAddress]
	, [DayTimeTelephoneNumber]
	, [HomeTelephone]
	, [MobileTelephone]
	, [CompanyTelephone]
	, [WorksTelephone]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [PostCode]
	, [HasDownloaded]
	, [DownloadedOn]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [Test]
	, [CompanyName]
	, [Occupation]
	, [Employer]
	, [ReminderEmailSentDate]
	, [ReminderSmsSentDate]
	, [CountryID]
    FROM
	[dbo].[DroppedOutCustomers] WITH (NOLOCK) 
    WHERE 
	 ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([TitleID] = @TitleID OR @TitleID IS NULL)
	AND ([FirstName] = @FirstName OR @FirstName IS NULL)
	AND ([MiddleName] = @MiddleName OR @MiddleName IS NULL)
	AND ([LastName] = @LastName OR @LastName IS NULL)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress IS NULL)
	AND ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber OR @DayTimeTelephoneNumber IS NULL)
	AND ([HomeTelephone] = @HomeTelephone OR @HomeTelephone IS NULL)
	AND ([MobileTelephone] = @MobileTelephone OR @MobileTelephone IS NULL)
	AND ([CompanyTelephone] = @CompanyTelephone OR @CompanyTelephone IS NULL)
	AND ([WorksTelephone] = @WorksTelephone OR @WorksTelephone IS NULL)
	AND ([Address1] = @Address1 OR @Address1 IS NULL)
	AND ([Address2] = @Address2 OR @Address2 IS NULL)
	AND ([Town] = @Town OR @Town IS NULL)
	AND ([County] = @County OR @County IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
	AND ([HasDownloaded] = @HasDownloaded OR @HasDownloaded IS NULL)
	AND ([DownloadedOn] = @DownloadedOn OR @DownloadedOn IS NULL)
	AND ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID IS NULL)
	AND ([ClientStatusID] = @ClientStatusID OR @ClientStatusID IS NULL)
	AND ([Test] = @Test OR @Test IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([Occupation] = @Occupation OR @Occupation IS NULL)
	AND ([Employer] = @Employer OR @Employer IS NULL)
	AND ([ReminderEmailSentDate] = @ReminderEmailSentDate OR @ReminderEmailSentDate IS NULL)
	AND ([ReminderSmsSentDate] = @ReminderSmsSentDate OR @ReminderSmsSentDate IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomerID]
	, [ClientID]
	, [TitleID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [EmailAddress]
	, [DayTimeTelephoneNumber]
	, [HomeTelephone]
	, [MobileTelephone]
	, [CompanyTelephone]
	, [WorksTelephone]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [PostCode]
	, [HasDownloaded]
	, [DownloadedOn]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [Test]
	, [CompanyName]
	, [Occupation]
	, [Employer]
	, [ReminderEmailSentDate]
	, [ReminderSmsSentDate]
	, [CountryID]
    FROM
	[dbo].[DroppedOutCustomers] WITH (NOLOCK) 
    WHERE 
	 ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([FirstName] = @FirstName AND @FirstName is not null)
	OR ([MiddleName] = @MiddleName AND @MiddleName is not null)
	OR ([LastName] = @LastName AND @LastName is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber AND @DayTimeTelephoneNumber is not null)
	OR ([HomeTelephone] = @HomeTelephone AND @HomeTelephone is not null)
	OR ([MobileTelephone] = @MobileTelephone AND @MobileTelephone is not null)
	OR ([CompanyTelephone] = @CompanyTelephone AND @CompanyTelephone is not null)
	OR ([WorksTelephone] = @WorksTelephone AND @WorksTelephone is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	OR ([HasDownloaded] = @HasDownloaded AND @HasDownloaded is not null)
	OR ([DownloadedOn] = @DownloadedOn AND @DownloadedOn is not null)
	OR ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([ClientStatusID] = @ClientStatusID AND @ClientStatusID is not null)
	OR ([Test] = @Test AND @Test is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([Occupation] = @Occupation AND @Occupation is not null)
	OR ([Employer] = @Employer AND @Employer is not null)
	OR ([ReminderEmailSentDate] = @ReminderEmailSentDate AND @ReminderEmailSentDate is not null)
	OR ([ReminderSmsSentDate] = @ReminderSmsSentDate AND @ReminderSmsSentDate is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Find] TO [sp_executeall]
GO
