SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AssetHistory table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetHistory_GetByAssetSubTypeID]
(

	@AssetSubTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AssetHistoryID],
					[AssetID],
					[ClientID],
					[AssetTypeID],
					[AssetSubTypeID],
					[LocationID],
					[AssetName],
					[Note],
					[SecureNote],
					[SecureUserName],
					[SecurePassword],
					[SecureOther],
					[Version],
					[ValidFrom],
					[ValidTo],
					[ReminderDue],
					[ReminderTimeUnitID],
					[ReminderTimeUnitQuantity],
					[ReminderNotificationGroupID],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[AssetHistory] WITH (NOLOCK) 
				WHERE
					[AssetSubTypeID] = @AssetSubTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_GetByAssetSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory_GetByAssetSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_GetByAssetSubTypeID] TO [sp_executeall]
GO
