SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetQuestionPossibleAnswers    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[GetQuestionPossibleAnswers] @MasterQuestionID int
as
select QuestionPossibleAnswerID, MasterQuestionID, AnswerText, Branch, LinkedQuestionnaireQuestionPossibleAnswerID, ClientID 
from QuestionPossibleAnswers
where MasterQuestionID = @MasterQuestionID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionPossibleAnswers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionPossibleAnswers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionPossibleAnswers] TO [sp_executeall]
GO
