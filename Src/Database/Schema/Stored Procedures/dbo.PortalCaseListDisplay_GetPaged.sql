SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the PortalCaseListDisplay table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalCaseListDisplay_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [PortalCaseListDisplayID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([PortalCaseListDisplayID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [PortalCaseListDisplayID]'
				SET @SQL = @SQL + ' FROM [dbo].[PortalCaseListDisplay] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[PortalCaseListDisplayID], O.[ClientID], O.[LeadTypeID], O.[Field1Caption], O.[Field1TypeID], O.[Field1DetailFieldID], O.[Field1ColumnDetailFieldID], O.[Field2Caption], O.[Field2TypeID], O.[Field2DetailFieldID], O.[Field2ColumnDetailFieldID], O.[Field3Caption], O.[Field3TypeID], O.[Field3DetailFieldID], O.[Field3ColumnDetailFieldID], O.[Field4Caption], O.[Field4TypeID], O.[Field4DetailFieldID], O.[Field4ColumnDetailFieldID], O.[Field5Caption], O.[Field5TypeID], O.[Field5DetailFieldID], O.[Field5ColumnDetailFieldID], O.[WhoCreated], O.[WhenCreated], O.[WhoModified], O.[WhenModified], O.[SqlQueryText]
				FROM
				    [dbo].[PortalCaseListDisplay] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[PortalCaseListDisplayID] = PageIndex.[PortalCaseListDisplayID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[PortalCaseListDisplay] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalCaseListDisplay_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_GetPaged] TO [sp_executeall]
GO
