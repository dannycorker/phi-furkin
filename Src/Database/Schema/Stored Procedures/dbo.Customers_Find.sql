SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Customers table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomerID int   = null ,

	@ClientID int   = null ,

	@TitleID int   = null ,

	@IsBusiness bit   = null ,

	@FirstName varchar (100)  = null ,

	@MiddleName varchar (100)  = null ,

	@LastName varchar (100)  = null ,

	@EmailAddress varchar (255)  = null ,

	@DayTimeTelephoneNumber varchar (50)  = null ,

	@DayTimeTelephoneNumberVerifiedAndValid bit   = null ,

	@HomeTelephone varchar (50)  = null ,

	@HomeTelephoneVerifiedAndValid bit   = null ,

	@MobileTelephone varchar (50)  = null ,

	@MobileTelephoneVerifiedAndValid bit   = null ,

	@CompanyTelephone varchar (50)  = null ,

	@CompanyTelephoneVerifiedAndValid bit   = null ,

	@WorksTelephone varchar (50)  = null ,

	@WorksTelephoneVerifiedAndValid bit   = null ,

	@Address1 varchar (200)  = null ,

	@Address2 varchar (200)  = null ,

	@Town varchar (200)  = null ,

	@County varchar (200)  = null ,

	@PostCode varchar (50)  = null ,

	@Website varchar (200)  = null ,

	@HasDownloaded bit   = null ,

	@DownloadedOn datetime   = null ,

	@AquariumStatusID int   = null ,

	@ClientStatusID int   = null ,

	@Test bit   = null ,

	@CompanyName varchar (100)  = null ,

	@Occupation varchar (100)  = null ,

	@Employer varchar (100)  = null ,

	@Fullname varchar (201)  = null ,

	@PhoneNumbersVerifiedOn datetime   = null ,

	@DoNotEmail bit   = null ,

	@DoNotSellToThirdParty bit   = null ,

	@AgreedToTermsAndConditions bit   = null ,

	@DateOfBirth datetime   = null ,

	@DefaultContactID int   = null ,

	@DefaultOfficeID int   = null ,

	@AddressVerified bit   = null ,

	@CountryID int   = null ,

	@SubClientID int   = null ,

	@CustomerRef varchar (100)  = null ,

	@WhoChanged int   = null ,

	@WhenChanged datetime   = null ,

	@ChangeSource varchar (200)  = null ,

	@EmailAddressVerifiedAndValid bit   = null ,

	@EmailAddressVerifiedOn datetime   = null ,

	@Comments varchar (MAX)  = null ,

	@AllowSmsCommandProcessing bit   = null ,

	@LanguageID int   = null ,

	@Longitude numeric (25, 18)  = null ,

	@Latitude numeric (25, 18)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomerID]
	, [ClientID]
	, [TitleID]
	, [IsBusiness]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [EmailAddress]
	, [DayTimeTelephoneNumber]
	, [DayTimeTelephoneNumberVerifiedAndValid]
	, [HomeTelephone]
	, [HomeTelephoneVerifiedAndValid]
	, [MobileTelephone]
	, [MobileTelephoneVerifiedAndValid]
	, [CompanyTelephone]
	, [CompanyTelephoneVerifiedAndValid]
	, [WorksTelephone]
	, [WorksTelephoneVerifiedAndValid]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [PostCode]
	, [Website]
	, [HasDownloaded]
	, [DownloadedOn]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [Test]
	, [CompanyName]
	, [Occupation]
	, [Employer]
	, [Fullname]
	, [PhoneNumbersVerifiedOn]
	, [DoNotEmail]
	, [DoNotSellToThirdParty]
	, [AgreedToTermsAndConditions]
	, [DateOfBirth]
	, [DefaultContactID]
	, [DefaultOfficeID]
	, [AddressVerified]
	, [CountryID]
	, [SubClientID]
	, [CustomerRef]
	, [WhoChanged]
	, [WhenChanged]
	, [ChangeSource]
	, [EmailAddressVerifiedAndValid]
	, [EmailAddressVerifiedOn]
	, [Comments]
	, [AllowSmsCommandProcessing]
	, [LanguageID]
	, [Longitude]
	, [Latitude]
    FROM
	[dbo].[Customers] WITH (NOLOCK) 
    WHERE 
	 ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([TitleID] = @TitleID OR @TitleID IS NULL)
	AND ([IsBusiness] = @IsBusiness OR @IsBusiness IS NULL)
	AND ([FirstName] = @FirstName OR @FirstName IS NULL)
	AND ([MiddleName] = @MiddleName OR @MiddleName IS NULL)
	AND ([LastName] = @LastName OR @LastName IS NULL)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress IS NULL)
	AND ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber OR @DayTimeTelephoneNumber IS NULL)
	AND ([DayTimeTelephoneNumberVerifiedAndValid] = @DayTimeTelephoneNumberVerifiedAndValid OR @DayTimeTelephoneNumberVerifiedAndValid IS NULL)
	AND ([HomeTelephone] = @HomeTelephone OR @HomeTelephone IS NULL)
	AND ([HomeTelephoneVerifiedAndValid] = @HomeTelephoneVerifiedAndValid OR @HomeTelephoneVerifiedAndValid IS NULL)
	AND ([MobileTelephone] = @MobileTelephone OR @MobileTelephone IS NULL)
	AND ([MobileTelephoneVerifiedAndValid] = @MobileTelephoneVerifiedAndValid OR @MobileTelephoneVerifiedAndValid IS NULL)
	AND ([CompanyTelephone] = @CompanyTelephone OR @CompanyTelephone IS NULL)
	AND ([CompanyTelephoneVerifiedAndValid] = @CompanyTelephoneVerifiedAndValid OR @CompanyTelephoneVerifiedAndValid IS NULL)
	AND ([WorksTelephone] = @WorksTelephone OR @WorksTelephone IS NULL)
	AND ([WorksTelephoneVerifiedAndValid] = @WorksTelephoneVerifiedAndValid OR @WorksTelephoneVerifiedAndValid IS NULL)
	AND ([Address1] = @Address1 OR @Address1 IS NULL)
	AND ([Address2] = @Address2 OR @Address2 IS NULL)
	AND ([Town] = @Town OR @Town IS NULL)
	AND ([County] = @County OR @County IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
	AND ([Website] = @Website OR @Website IS NULL)
	AND ([HasDownloaded] = @HasDownloaded OR @HasDownloaded IS NULL)
	AND ([DownloadedOn] = @DownloadedOn OR @DownloadedOn IS NULL)
	AND ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID IS NULL)
	AND ([ClientStatusID] = @ClientStatusID OR @ClientStatusID IS NULL)
	AND ([Test] = @Test OR @Test IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([Occupation] = @Occupation OR @Occupation IS NULL)
	AND ([Employer] = @Employer OR @Employer IS NULL)
	AND ([Fullname] = @Fullname OR @Fullname IS NULL)
	AND ([PhoneNumbersVerifiedOn] = @PhoneNumbersVerifiedOn OR @PhoneNumbersVerifiedOn IS NULL)
	AND ([DoNotEmail] = @DoNotEmail OR @DoNotEmail IS NULL)
	AND ([DoNotSellToThirdParty] = @DoNotSellToThirdParty OR @DoNotSellToThirdParty IS NULL)
	AND ([AgreedToTermsAndConditions] = @AgreedToTermsAndConditions OR @AgreedToTermsAndConditions IS NULL)
	AND ([DateOfBirth] = @DateOfBirth OR @DateOfBirth IS NULL)
	AND ([DefaultContactID] = @DefaultContactID OR @DefaultContactID IS NULL)
	AND ([DefaultOfficeID] = @DefaultOfficeID OR @DefaultOfficeID IS NULL)
	AND ([AddressVerified] = @AddressVerified OR @AddressVerified IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([CustomerRef] = @CustomerRef OR @CustomerRef IS NULL)
	AND ([WhoChanged] = @WhoChanged OR @WhoChanged IS NULL)
	AND ([WhenChanged] = @WhenChanged OR @WhenChanged IS NULL)
	AND ([ChangeSource] = @ChangeSource OR @ChangeSource IS NULL)
	AND ([EmailAddressVerifiedAndValid] = @EmailAddressVerifiedAndValid OR @EmailAddressVerifiedAndValid IS NULL)
	AND ([EmailAddressVerifiedOn] = @EmailAddressVerifiedOn OR @EmailAddressVerifiedOn IS NULL)
	AND ([Comments] = @Comments OR @Comments IS NULL)
	AND ([AllowSmsCommandProcessing] = @AllowSmsCommandProcessing OR @AllowSmsCommandProcessing IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([Longitude] = @Longitude OR @Longitude IS NULL)
	AND ([Latitude] = @Latitude OR @Latitude IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomerID]
	, [ClientID]
	, [TitleID]
	, [IsBusiness]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [EmailAddress]
	, [DayTimeTelephoneNumber]
	, [DayTimeTelephoneNumberVerifiedAndValid]
	, [HomeTelephone]
	, [HomeTelephoneVerifiedAndValid]
	, [MobileTelephone]
	, [MobileTelephoneVerifiedAndValid]
	, [CompanyTelephone]
	, [CompanyTelephoneVerifiedAndValid]
	, [WorksTelephone]
	, [WorksTelephoneVerifiedAndValid]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [PostCode]
	, [Website]
	, [HasDownloaded]
	, [DownloadedOn]
	, [AquariumStatusID]
	, [ClientStatusID]
	, [Test]
	, [CompanyName]
	, [Occupation]
	, [Employer]
	, [Fullname]
	, [PhoneNumbersVerifiedOn]
	, [DoNotEmail]
	, [DoNotSellToThirdParty]
	, [AgreedToTermsAndConditions]
	, [DateOfBirth]
	, [DefaultContactID]
	, [DefaultOfficeID]
	, [AddressVerified]
	, [CountryID]
	, [SubClientID]
	, [CustomerRef]
	, [WhoChanged]
	, [WhenChanged]
	, [ChangeSource]
	, [EmailAddressVerifiedAndValid]
	, [EmailAddressVerifiedOn]
	, [Comments]
	, [AllowSmsCommandProcessing]
	, [LanguageID]
	, [Longitude]
	, [Latitude]
    FROM
	[dbo].[Customers] WITH (NOLOCK) 
    WHERE 
	 ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([IsBusiness] = @IsBusiness AND @IsBusiness is not null)
	OR ([FirstName] = @FirstName AND @FirstName is not null)
	OR ([MiddleName] = @MiddleName AND @MiddleName is not null)
	OR ([LastName] = @LastName AND @LastName is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([DayTimeTelephoneNumber] = @DayTimeTelephoneNumber AND @DayTimeTelephoneNumber is not null)
	OR ([DayTimeTelephoneNumberVerifiedAndValid] = @DayTimeTelephoneNumberVerifiedAndValid AND @DayTimeTelephoneNumberVerifiedAndValid is not null)
	OR ([HomeTelephone] = @HomeTelephone AND @HomeTelephone is not null)
	OR ([HomeTelephoneVerifiedAndValid] = @HomeTelephoneVerifiedAndValid AND @HomeTelephoneVerifiedAndValid is not null)
	OR ([MobileTelephone] = @MobileTelephone AND @MobileTelephone is not null)
	OR ([MobileTelephoneVerifiedAndValid] = @MobileTelephoneVerifiedAndValid AND @MobileTelephoneVerifiedAndValid is not null)
	OR ([CompanyTelephone] = @CompanyTelephone AND @CompanyTelephone is not null)
	OR ([CompanyTelephoneVerifiedAndValid] = @CompanyTelephoneVerifiedAndValid AND @CompanyTelephoneVerifiedAndValid is not null)
	OR ([WorksTelephone] = @WorksTelephone AND @WorksTelephone is not null)
	OR ([WorksTelephoneVerifiedAndValid] = @WorksTelephoneVerifiedAndValid AND @WorksTelephoneVerifiedAndValid is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	OR ([Website] = @Website AND @Website is not null)
	OR ([HasDownloaded] = @HasDownloaded AND @HasDownloaded is not null)
	OR ([DownloadedOn] = @DownloadedOn AND @DownloadedOn is not null)
	OR ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([ClientStatusID] = @ClientStatusID AND @ClientStatusID is not null)
	OR ([Test] = @Test AND @Test is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([Occupation] = @Occupation AND @Occupation is not null)
	OR ([Employer] = @Employer AND @Employer is not null)
	OR ([Fullname] = @Fullname AND @Fullname is not null)
	OR ([PhoneNumbersVerifiedOn] = @PhoneNumbersVerifiedOn AND @PhoneNumbersVerifiedOn is not null)
	OR ([DoNotEmail] = @DoNotEmail AND @DoNotEmail is not null)
	OR ([DoNotSellToThirdParty] = @DoNotSellToThirdParty AND @DoNotSellToThirdParty is not null)
	OR ([AgreedToTermsAndConditions] = @AgreedToTermsAndConditions AND @AgreedToTermsAndConditions is not null)
	OR ([DateOfBirth] = @DateOfBirth AND @DateOfBirth is not null)
	OR ([DefaultContactID] = @DefaultContactID AND @DefaultContactID is not null)
	OR ([DefaultOfficeID] = @DefaultOfficeID AND @DefaultOfficeID is not null)
	OR ([AddressVerified] = @AddressVerified AND @AddressVerified is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([CustomerRef] = @CustomerRef AND @CustomerRef is not null)
	OR ([WhoChanged] = @WhoChanged AND @WhoChanged is not null)
	OR ([WhenChanged] = @WhenChanged AND @WhenChanged is not null)
	OR ([ChangeSource] = @ChangeSource AND @ChangeSource is not null)
	OR ([EmailAddressVerifiedAndValid] = @EmailAddressVerifiedAndValid AND @EmailAddressVerifiedAndValid is not null)
	OR ([EmailAddressVerifiedOn] = @EmailAddressVerifiedOn AND @EmailAddressVerifiedOn is not null)
	OR ([Comments] = @Comments AND @Comments is not null)
	OR ([AllowSmsCommandProcessing] = @AllowSmsCommandProcessing AND @AllowSmsCommandProcessing is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([Longitude] = @Longitude AND @Longitude is not null)
	OR ([Latitude] = @Latitude AND @Latitude is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_Find] TO [sp_executeall]
GO
