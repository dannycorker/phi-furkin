SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CheckMateFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldGroup_Delete]
(

	@CheckMateFieldGroupID int   
)
AS


				DELETE FROM [dbo].[CheckMateFieldGroup] WITH (ROWLOCK) 
				WHERE
					[CheckMateFieldGroupID] = @CheckMateFieldGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Delete] TO [sp_executeall]
GO
