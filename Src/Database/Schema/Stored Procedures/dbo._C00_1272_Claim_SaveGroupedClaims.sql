SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-07
-- Description:	Saves the matter ids for a policy into groups
-- Used by:		DF 146353
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SaveGroupedClaims] 
(
	@LeadID INT,
	@GroupedClaims dbo.TvpIntInt READONLY,
	@GroupAilments dbo.TvpIntInt READONLY
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	DELETE TableRows
	WHERE DetailFieldID = 146357
	AND DetailFieldPageID = 16766
	AND LeadID = @LeadID
	AND ClientID = @ClientID
	
	DECLARE @ToInsert TABLE
	(
		ID INT IDENTITY,
		ClaimID INT,
		ParentID INT
	)
	
	INSERT @ToInsert (ClaimID, ParentID)
	SELECT ID1, CASE ID2 WHEN 0 THEN NULL ELSE ID2 END
	FROM @GroupedClaims
	
	DECLARE @Inserted TABLE
	(
		ID INT IDENTITY,
		TableRowID INT
	)
	
	INSERT TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID)
	OUTPUT inserted.TableRowID INTO @Inserted
	SELECT @ClientID, @LeadID, 146357, 16766
	FROM @ToInsert
	
	-- And now insert the table detail values
	INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, LeadID, ClientID)
	-- Claim ID
	SELECT i.TableRowID, 146355, s.ClaimID AS Value, @LeadID, @ClientID
	FROM @ToInsert s
	INNER JOIN @Inserted i ON s.ID = i.ID 
	UNION
	-- Parent ID
	SELECT i.TableRowID, 146356, s.ParentID AS Value, @LeadID, @ClientID
	FROM @ToInsert s
	INNER JOIN @Inserted i ON s.ID = i.ID 
	
	
	-- Now set the ailment resource list for each of the matters that are grouped together
	-- We get a table of parents as they are in the same order as the ailments so we can join on the identity ID column
	DECLARE @Parents TABLE
	(
		ID INT IDENTITY,
		ClaimID INT,
		ChildrenCount INT,
		DateOfOnset VARCHAR(2000),
		DescriptionOfLoss VARCHAR(2000),
		BodyPart VARCHAR(2000),
		SapRef VARCHAR(2000)
	)
	INSERT @Parents (ClaimID, ChildrenCount, DateOfOnset, DescriptionOfLoss, BodyPart, SapRef)
	SELECT i.ClaimID, ISNULL(c.Children, 0), mdvDateOfOnset.DetailValue, mdvDescriptionOfLoss.DetailValue, mdvBodyPart.DetailValue, mdvSapRef.DetailValue
	FROM @ToInsert i
	LEFT JOIN 
	(
		SELECT ParentID, COUNT(ParentID) AS Children
		FROM @ToInsert
		GROUP BY ParentID
	) c ON i.ClaimID = c.ParentID
	LEFT JOIN dbo.MatterDetailValues mdvDateOfOnset WITH (NOLOCK) ON i.ClaimID = mdvDateOfOnset.MatterID AND mdvDateOfOnset.DetailFieldID = 144892
	LEFT JOIN dbo.MatterDetailValues mdvDescriptionOfLoss WITH (NOLOCK) ON i.ClaimID = mdvDescriptionOfLoss.MatterID AND mdvDescriptionOfLoss.DetailFieldID = 144332
	LEFT JOIN dbo.MatterDetailValues mdvBodyPart WITH (NOLOCK) ON i.ClaimID = mdvBodyPart.MatterID AND mdvBodyPart.DetailFieldID = 144333
	LEFT JOIN dbo.MatterDetailValues mdvSapRef WITH (NOLOCK) ON i.ClaimID = mdvSapRef.MatterID AND mdvSapRef.DetailFieldID = 147436
	WHERE i.ParentID IS NULL
	
	
	-- Updates
	-- Ailment
	UPDATE mdv
	SET DetailValue = a.ID2
	FROM @Parents p
	INNER JOIN @GroupAilments a ON p.ID = a.ID1
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144504
	WHERE mdv.ClientID = @ClientID 
	-- Date of onset
	UPDATE mdv
	SET DetailValue = ISNULL(p.DateOfOnset, '')
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144892
	WHERE mdv.ClientID = @ClientID  
	-- Claim type
	UPDATE mdv
	SET DetailValue =	CASE 
							WHEN p.ClaimID = g.ID1 AND p.ChildrenCount > 0 THEN '44356'								-- Parents with children are new claims
							WHEN p.ClaimID = g.ID2 THEN '44357'														-- Children are continuation
							WHEN p.ClaimID = g.ID1 AND p.ChildrenCount = 0 AND DetailValue != '44358' THEN '44356'	-- Parents with no children are new unless they are pre-auth
							ELSE ISNULL(DetailValue, '')
						END
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144483
	WHERE mdv.ClientID = @ClientID 
	-- Description of loss
	UPDATE mdv
	SET DetailValue = ISNULL(p.DescriptionOfLoss, '')
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144332
	WHERE mdv.ClientID = @ClientID
	AND LTRIM(RTRIM(mdv.DetailValue)) = '' 
	-- Body part
	UPDATE mdv
	SET DetailValue = ISNULL(p.BodyPart, '')
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144333
	WHERE mdv.ClientID = @ClientID 
	AND LTRIM(RTRIM(mdv.DetailValue)) = '' 
	
	-- Insert
	-- Ailment
	INSERT dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT @ClientID, @LeadID, g.ID1, 144504, a.ID2
	FROM @Parents p
	INNER JOIN @GroupAilments a ON p.ID = a.ID1
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144504 
	WHERE mdv.MatterDetailValueID IS NULL
	-- Date of onset
	INSERT dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT @ClientID, @LeadID, g.ID1, 144892, ISNULL(p.DateOfOnset, '')
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144892 
	WHERE mdv.MatterDetailValueID IS NULL
	-- Claim type
	INSERT dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT @ClientID, @LeadID, g.ID1, 144483, CASE 
											WHEN p.ClaimID = g.ID1 THEN '44356' -- Parents with children are new claims -- AND p.ChildrenCount > 0 
											WHEN p.ClaimID = g.ID2 THEN '44357'	-- Children are continuation
											ELSE ISNULL(DetailValue, '') -- This should never be hit
										END
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144483 
	WHERE mdv.MatterDetailValueID IS NULL
	-- Description of loss
	INSERT dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT @ClientID, @LeadID, g.ID1, 144332, ISNULL(p.DescriptionOfLoss, '')
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144332 
	WHERE mdv.MatterDetailValueID IS NULL
	-- Body part
	INSERT dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT @ClientID, @LeadID, g.ID1, 144333, ISNULL(p.BodyPart, '')
	FROM @Parents p
	INNER JOIN @GroupedClaims g ON (p.ClaimID = g.ID1 OR p.ClaimID = g.ID2)
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = g.ID1 AND mdv.DetailFieldID = 144333 
	WHERE mdv.MatterDetailValueID IS NULL

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveGroupedClaims] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SaveGroupedClaims] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveGroupedClaims] TO [sp_executeall]
GO
