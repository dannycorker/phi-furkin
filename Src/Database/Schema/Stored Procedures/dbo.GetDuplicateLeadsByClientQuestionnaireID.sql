SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[GetDuplicateLeadsByClientQuestionnaireID] 

@ClientQuestionnaireID int = null,
@ClientID int,

@emailaddress bit,
@postcode bit,
@lastname bit

AS

declare @mysql varchar(8000)

declare @emailaddressWhere varchar(500)
declare @emailaddressWhereP2 varchar(500)
declare @emailOrderBy varchar(500)

declare @postcodeWhere varchar(500)
declare @postcodeWhereP2 varchar(500)
declare @postcodeOrderBy  varchar(500)

declare @lastnameWhere varchar(500)
declare @lastnameWhereP2 varchar(500)
declare @lastnameOrderBy varchar(500)

declare @QuestionaireIDWhere varchar(500) 

set @emailaddressWhere = ''
set @emailaddressWhereP2 = ''
set @emailOrderBy = ''

set @postcodeWhere = ''
set @postcodeWhereP2 = ''
set @postcodeOrderBy = ''

set @lastnameWhere = ''
set @lastnameWhereP2 = ''
set @lastnameOrderBy = ''

set @QuestionaireIDWhere = ''

if @emailaddress = 1
begin
	set @emailaddressWhere = 'c2.emailaddress = ic1.emailaddress AND'
	set @emailaddressWhereP2 = 'c3.emailaddress = ic2.emailaddress AND'
	set @emailOrderBy = 'c.emailaddress, '
end

if @postcode = 1
begin
	set @postcodeWhere = ' c2.postcode = ic1.postcode AND'
	set @postcodeWhereP2 = ' c3.postcode = ic2.postcode AND'
	set @postcodeOrderBy = 'c.postcode, '
end

if @lastname = 1
begin
	set @lastnameWhere = ' c2.lastname = ic1.lastname AND'
	set @lastnameWhereP2 = ' c3.lastname = ic2.lastname AND'
	set @lastnameOrderBy = 'c.lastname, '
end

if @ClientQuestionnaireID is not null
begin
	set @QuestionaireIDWhere = ' AND clq.ClientQuestionnaireID = ' + CONVERT(VARCHAR,@ClientQuestionnaireID) + ' '
end


print '1 '
print 'bye off then' +  @emailOrderBy  + @lastnameOrderBy + @postcodeOrderBy + ' 31'


set @mysql = 'select  cuq.ClientQuestionnaireID, c.customerid, c.fullname, c.emailaddress, c.Town, c.County, c.lastname, c.postcode, cuq.SubmissionDate, clq.QuestionnaireTitle, Lead.LeadID, c.AquariumStatusID
from Customers c
inner join CustomerQuestionnaires cuq ON cuq.CustomerID = c.CustomerID
inner join ClientQuestionnaires clq ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID
left outer join Lead ON Lead.CustomerID = c.CustomerID
inner join 
(
	select ic1.customerid
	from Customers ic1
	inner join CustomerQuestionnaires cuq ON cuq.CustomerID = ic1.CustomerID
	inner join ClientQuestionnaires clq ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID
	where ic1.AquariumStatusID = 1 ' + @QuestionaireIDWhere + '
	AND exists (
		select * from customers c2 
		inner join CustomerQuestionnaires cuq ON cuq.CustomerID = c2.CustomerID
		inner join ClientQuestionnaires clq ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID
		where ' + @emailaddressWhere + @postcodeWhere + @lastnameWhere + '
		c2.AquariumStatusID <> 3
		AND c2.customerid <> ic1.customerid)
	union

	select ic2.customerid
	from Customers ic2
	inner join CustomerQuestionnaires cuq ON cuq.CustomerID = ic2.CustomerID
	inner join ClientQuestionnaires clq ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID
	where ic2.AquariumStatusID <> 3 
	and exists (
		select * from customers c3 
		inner join CustomerQuestionnaires cuq ON cuq.CustomerID = c3.CustomerID
		inner join ClientQuestionnaires clq ON clq.ClientQuestionnaireID = cuq.ClientQuestionnaireID
		where ' + @emailaddressWhereP2 + @postcodeWhereP2 + @lastnameWhereP2 + '
		c3.AquariumStatusID = 1 ' + @QuestionaireIDWhere + '
		AND c3.customerid <> ic2.customerid
	)
) as ics on c.customerid = ics.customerid
where c.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
ORDER BY ' + @emailOrderBy + @lastnameOrderBy + @postcodeOrderBy +  ' c.AquariumStatusID desc, c.customerid'


print @mysql

exec  (@mysql)






GO
GRANT VIEW DEFINITION ON  [dbo].[GetDuplicateLeadsByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDuplicateLeadsByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDuplicateLeadsByClientQuestionnaireID] TO [sp_executeall]
GO
