SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the BillItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_Update]
(

	@BillItemID int   ,

	@ClientID int   ,

	@BillID int   ,

	@Description varchar (2000)  ,

	@ClientPersonnelID int   ,

	@WorkUnits int   ,

	@Total decimal (18, 2)  ,

	@ChargeOutRate int   ,

	@TotalIsCustomValue bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[BillItem]
				SET
					[ClientID] = @ClientID
					,[BillID] = @BillID
					,[Description] = @Description
					,[ClientPersonnelID] = @ClientPersonnelID
					,[WorkUnits] = @WorkUnits
					,[Total] = @Total
					,[ChargeOutRate] = @ChargeOutRate
					,[TotalIsCustomValue] = @TotalIsCustomValue
				WHERE
[BillItemID] = @BillItemID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Update] TO [sp_executeall]
GO
