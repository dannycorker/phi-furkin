SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Chris Townsend
-- Create date: 17th January 2008
-- Description:	Gets Workflow Groups by the ClientID, including the Name of the Group Manager
-- SB	2011-10-25	Added the oldest / latest column for the group sort order
-- ACE	2015-02-05	Show the number of tasks in each group
-- =============================================
CREATE PROCEDURE [dbo].[Workflow_GetWorkflowGroupsByClientID] 

@ClientID int

AS
BEGIN

	SET NOCOUNT ON

	;
	WITH Counts AS (
		SELECT COUNT(*) AS [Counter], wt.WorkflowGroupID
		FROM WorkflowTask wt WITH (NOLOCK)
		WHERE wt.ClientID = @ClientID
		GROUP BY wt.WorkflowGroupID
	)

	SELECT	wg.WorkflowGroupID, 
			wg.[Name] + ' (' + CONVERT(VARCHAR(100),ISNULL(c.Counter,0)) + ')' AS [Name] ,
			wg.Description,
			wg.ManagerID,
			cp.FirstName + ' ' + cp.LastName AS 'ManagerName',
			CASE wg.SortOrder WHEN 'DESC' THEN 'Latest First' ELSE 'Oldest First' END AS SortOrder,
			wg.Enabled
	FROM	WorkflowGroup wg WITH (NOLOCK) 
	LEFT JOIN ClientPersonnel cp WITH (NOLOCK) ON wg.ManagerID = cp.ClientPersonnelID 
	LEFT JOIN Counts c ON c.WorkflowGroupID = wg.WorkflowGroupID 
	WHERE	wg.ClientID = @ClientID 
	ORDER BY wg.WorkflowGroupID 

END

GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_GetWorkflowGroupsByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Workflow_GetWorkflowGroupsByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Workflow_GetWorkflowGroupsByClientID] TO [sp_executeall]
GO
