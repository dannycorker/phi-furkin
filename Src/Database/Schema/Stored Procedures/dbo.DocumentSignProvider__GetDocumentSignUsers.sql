SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetDocumentSignUsers]
	@ClientID INT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	
			dspu.DSPTypeID, 
			dspu.DSPAccountID,
			dspu.DSPUserID, 
			dspu.ClientID,
			dspt.DSPTypeName +' ('+ dspu.UserName +')' DSPDetail
	FROM	DocumentSignProviderUser dspu WITH (NOLOCK) 
	INNER JOIN DocumentSignProviderAccount dspa WITH (NOLOCK)  ON dspa.DSPAccountID = dspu.DSPAccountID
	INNER JOIN DocumentSignProviderType dspt WITH (NOLOCK)  ON dspt.DSPTypeID = dspu.DSPTypeID
	WHERE	dspu.ClientID = @ClientID
	AND		dspa.IntegrationKey IS NOT NULL
	AND		dspu.UserName IS NOT NULL
	AND		dspt.DSPTypeName  IS NOT NULL
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetDocumentSignUsers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetDocumentSignUsers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetDocumentSignUsers] TO [sp_executeall]
GO
