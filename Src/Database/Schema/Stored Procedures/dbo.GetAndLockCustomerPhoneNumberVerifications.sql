SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetAndLockCustomerPhoneNumberVerifications]

@SkyNetID nvarchar(25)

AS

DECLARE @intErrorCode INT

BEGIN TRAN

Declare @topOneID int

set @topOneID = (SELECT TOP 1 PhoneNumberVerificationID FROM PhoneNumberVerifications Where SkyNetID is Null)

Update PhoneNumberVerifications
Set SkyNetID = @SkyNetID
WHERE (PhoneNumberVerificationID=@topOneID)


Select PhoneNumberVerificationID, CustomerID, ClientQuestionnaireID 
From PhoneNumberVerifications
Where SkyNetID = @SkyNetID

SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

COMMIT TRAN

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetAndLockCustomerPhoneNumberVerifications] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAndLockCustomerPhoneNumberVerifications] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAndLockCustomerPhoneNumberVerifications] TO [sp_executeall]
GO
