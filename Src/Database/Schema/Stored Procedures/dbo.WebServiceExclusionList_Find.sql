SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the WebServiceExclusionList table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WebServiceExclusionList_Find]
(

	@SearchUsingOR bit   = null ,

	@WebServiceExclusionListID int   = null ,

	@ClientPersonnelID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [WebServiceExclusionListID]
	, [ClientPersonnelID]
    FROM
	[dbo].[WebServiceExclusionList] WITH (NOLOCK) 
    WHERE 
	 ([WebServiceExclusionListID] = @WebServiceExclusionListID OR @WebServiceExclusionListID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [WebServiceExclusionListID]
	, [ClientPersonnelID]
    FROM
	[dbo].[WebServiceExclusionList] WITH (NOLOCK) 
    WHERE 
	 ([WebServiceExclusionListID] = @WebServiceExclusionListID AND @WebServiceExclusionListID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WebServiceExclusionList_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Find] TO [sp_executeall]
GO
