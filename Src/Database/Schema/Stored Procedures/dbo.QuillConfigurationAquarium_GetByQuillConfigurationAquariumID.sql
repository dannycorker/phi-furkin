SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillConfigurationAquarium table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationAquarium_GetByQuillConfigurationAquariumID]
(

	@QuillConfigurationAquariumID int   
)
AS


				SELECT
					[QuillConfigurationAquariumID],
					[Vendor],
					[APIKey]
				FROM
					[dbo].[QuillConfigurationAquarium] WITH (NOLOCK) 
				WHERE
										[QuillConfigurationAquariumID] = @QuillConfigurationAquariumID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_GetByQuillConfigurationAquariumID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationAquarium_GetByQuillConfigurationAquariumID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_GetByQuillConfigurationAquariumID] TO [sp_executeall]
GO
