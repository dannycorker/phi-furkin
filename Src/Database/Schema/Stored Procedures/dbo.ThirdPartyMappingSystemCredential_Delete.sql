SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyMappingSystemCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingSystemCredential_Delete]
(

	@ThirdPartyMappingSystemCredentialID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyMappingSystemCredential] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyMappingSystemCredentialID] = @ThirdPartyMappingSystemCredentialID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingSystemCredential_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingSystemCredential_Delete] TO [sp_executeall]
GO
