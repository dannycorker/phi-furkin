SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DashboardItemType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardItemType_Insert]
(

	@DashboardItemTypeID int    OUTPUT,

	@DashboardItemTypeName int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[DashboardItemType]
					(
					[DashboardItemTypeName]
					,[ClientID]
					)
				VALUES
					(
					@DashboardItemTypeName
					,@ClientID
					)
				-- Get the identity value
				SET @DashboardItemTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardItemType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardItemType_Insert] TO [sp_executeall]
GO
