SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-06-01
-- Description:	Return the primary and secondary phone numbers for an Aquarium customer
-- =============================================
CREATE PROCEDURE [dbo].[_C272_Greenlight_ReturnNumbersForCustomer]
	  @CustomerID		INT
	 ,@NumberToReturn	INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadID INT
			,@ClientPersonnelID	INT = 26692 /*Greenlight System*/
	
	/*
	Initially this sequence is fixed, but we may want to vary it by client or by customer
	*/
	DECLARE  @Primary			VARCHAR(50)
			,@Alternative		VARCHAR(50)

	DECLARE  @Priority	TABLE ( NumberType VARCHAR(100), Priority INT )
	INSERT	 @Priority ( NumberType, Priority )
	VALUES	( 'HomeTelephone', 1 )
			,( 'MobileTelephone', 2 )
			,( 'DaytimeTelephoneNumber', 3 )
			,( 'WorksTelephone', 4 )
			,( 'CompanyTelephone', 5 )

	SELECT * 
	FROM dbo.fn_C272_GetCustomerNumberList(@CustomerID) rn
	WHERE rn.Priority = @NumberToReturn OR @NumberToReturn = 0

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_ReturnNumbersForCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C272_Greenlight_ReturnNumbersForCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_ReturnNumbersForCustomer] TO [sp_executeall]
GO
