SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Equations table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Equations_Update]
(

	@EquationID int   ,

	@EquationName varchar (50)  ,

	@Equation varchar (MAX)  ,

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Equations]
				SET
					[EquationName] = @EquationName
					,[Equation] = @Equation
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[ClientID] = @ClientID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[EquationID] = @EquationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Equations_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Equations_Update] TO [sp_executeall]
GO
