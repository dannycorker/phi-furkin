SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 08/03/2016
-- Description:	Get all script rules for the given script
-- =============================================
CREATE PROCEDURE [dbo].[ScriptRule__GetByScriptID] 

	@ScriptID INT, 
	@ClientID INT

AS
BEGIN

	SELECT * FROM ScriptRule WITH (NOLOCK) 
	WHERE ScriptID=@ScriptID AND ClientID=@ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRule__GetByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptRule__GetByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRule__GetByScriptID] TO [sp_executeall]
GO
