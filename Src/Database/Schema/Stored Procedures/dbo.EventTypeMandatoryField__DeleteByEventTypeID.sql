SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeMandatoryField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField__DeleteByEventTypeID]
(

	@EventTypeID int   
)
AS

	DELETE FROM [dbo].[EventTypeMandatoryField] WITH (ROWLOCK) 
	WHERE
		[EventTypeID] = @EventTypeID
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField__DeleteByEventTypeID] TO [sp_executeall]
GO
