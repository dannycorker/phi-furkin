SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnelFavourite table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite_Insert]
(

	@ClientPersonnelFavouriteID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@LeadID int   
)
AS


				
				INSERT INTO [dbo].[ClientPersonnelFavourite]
					(
					[ClientID]
					,[ClientPersonnelID]
					,[LeadID]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelID
					,@LeadID
					)
				-- Get the identity value
				SET @ClientPersonnelFavouriteID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Insert] TO [sp_executeall]
GO
