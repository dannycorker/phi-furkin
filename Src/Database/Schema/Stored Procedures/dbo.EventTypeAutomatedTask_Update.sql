SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeAutomatedTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAutomatedTask_Update]
(

	@EventTypeAutomatedTaskID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@AutomatedTaskID int   ,

	@TimeDelayInSeconds int   ,

	@AddTimeDelayIfAboutToRun bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeAutomatedTask]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[AutomatedTaskID] = @AutomatedTaskID
					,[TimeDelayInSeconds] = @TimeDelayInSeconds
					,[AddTimeDelayIfAboutToRun] = @AddTimeDelayIfAboutToRun
				WHERE
[EventTypeAutomatedTaskID] = @EventTypeAutomatedTaskID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Update] TO [sp_executeall]
GO
