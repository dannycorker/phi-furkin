SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2009-02-18
-- Description:	Get all AutomatedTaskCommandControl records for this client and client zero.
--              Very similar to DocumentZipFtpControl.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTaskCommandControl__GetByClientIDInOrder] 
	@ClientID int,
	@TaskID int = null
AS
BEGIN
	
	SET NOCOUNT ON;
	
	-- Special processing to take place after the odd batch job
	-- eg 2868 and 2869 for Ashworth Law.
	SELECT con.[AutomatedTaskCommandControlID],
	con.[ClientID],
	con.[JobName],
	con.[JobNotes],
	con.[FiringOrder],
	con.[SuppressAllLaterJobs],
	con.[IsConfigFileRequired],
	con.[ConfigFileName],
	con.[IsBatFileRequired],
	con.[BatFileName],
	con.[Enabled],
	con.[MaxWaitTimeMS],
	con.[TaskID],
	con.[OutputExtension]
	FROM dbo.AutomatedTaskCommandControl con 
	WHERE con.ClientID IN (0, @ClientID) 
	AND con.Enabled = 1 
	AND (con.TaskID = @TaskID OR @TaskID IS NULL)
	ORDER BY con.FiringOrder, con.ClientID 
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskCommandControl__GetByClientIDInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskCommandControl__GetByClientIDInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskCommandControl__GetByClientIDInOrder] TO [sp_executeall]
GO
