SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AssetHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetHistory_Insert]
(

	@AssetHistoryID int    OUTPUT,

	@AssetID int   ,

	@ClientID int   ,

	@AssetTypeID int   ,

	@AssetSubTypeID int   ,

	@LocationID int   ,

	@AssetName varchar (255)  ,

	@Note varchar (255)  ,

	@SecureNote varchar (255)  ,

	@SecureUserName varchar (255)  ,

	@SecurePassword varchar (255)  ,

	@SecureOther varchar (255)  ,

	@Version varchar (255)  ,

	@ValidFrom datetime   ,

	@ValidTo datetime   ,

	@ReminderDue datetime   ,

	@ReminderTimeUnitID int   ,

	@ReminderTimeUnitQuantity int   ,

	@ReminderNotificationGroupID int   ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[AssetHistory]
					(
					[AssetID]
					,[ClientID]
					,[AssetTypeID]
					,[AssetSubTypeID]
					,[LocationID]
					,[AssetName]
					,[Note]
					,[SecureNote]
					,[SecureUserName]
					,[SecurePassword]
					,[SecureOther]
					,[Version]
					,[ValidFrom]
					,[ValidTo]
					,[ReminderDue]
					,[ReminderTimeUnitID]
					,[ReminderTimeUnitQuantity]
					,[ReminderNotificationGroupID]
					,[Enabled]
					,[Deleted]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@AssetID
					,@ClientID
					,@AssetTypeID
					,@AssetSubTypeID
					,@LocationID
					,@AssetName
					,@Note
					,@SecureNote
					,@SecureUserName
					,@SecurePassword
					,@SecureOther
					,@Version
					,@ValidFrom
					,@ValidTo
					,@ReminderDue
					,@ReminderTimeUnitID
					,@ReminderTimeUnitQuantity
					,@ReminderNotificationGroupID
					,@Enabled
					,@Deleted
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @AssetHistoryID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetHistory_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetHistory_Insert] TO [sp_executeall]
GO
