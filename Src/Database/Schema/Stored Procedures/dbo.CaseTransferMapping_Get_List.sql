SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CaseTransferMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferMapping_Get_List]

AS


				
				SELECT
					[CaseTransferMappingID],
					[ClientRelationshipID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID],
					[NewClientID],
					[NewCustomerID],
					[NewLeadID],
					[NewCaseID],
					[NewMatterID],
					[CaseTransferStatusID],
					[NewLeadTypeID]
				FROM
					[dbo].[CaseTransferMapping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferMapping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferMapping_Get_List] TO [sp_executeall]
GO
