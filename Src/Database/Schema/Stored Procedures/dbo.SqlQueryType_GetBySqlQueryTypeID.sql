SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryType_GetBySqlQueryTypeID]
(

	@SqlQueryTypeID int   
)
AS


				SELECT
					[SqlQueryTypeID],
					[TypeName]
				FROM
					[dbo].[SqlQueryType] WITH (NOLOCK) 
				WHERE
										[SqlQueryTypeID] = @SqlQueryTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_GetBySqlQueryTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryType_GetBySqlQueryTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryType_GetBySqlQueryTypeID] TO [sp_executeall]
GO
