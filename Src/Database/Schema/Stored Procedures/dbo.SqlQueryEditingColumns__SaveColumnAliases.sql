SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-08-31
-- Description:	Saves aliases for columns in the sql query editing tables
--
-- Updated 2011-11-08 Jan Wilson
-- Force the Show Column to true if an alias is set
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__SaveColumnAliases]
(
	@SqlQueryEditingID INT,
	@AliasesToSave dbo.tvpIDValue READONLY
)
AS

UPDATE c
SET c.ColumnAlias = a.AnyValue, c.ShowColumn = 1
FROM dbo.SqlQueryEditingColumns c
INNER JOIN @AliasesToSave a ON c.SqlQueryEditingColumnID = a.AnyID
WHERE c.SqlQueryEditingID = @SqlQueryEditingID
AND a.AnyValue IS NOT NULL 

UPDATE c
SET c.ColumnAlias = NULL
FROM dbo.SqlQueryEditingColumns c
INNER JOIN @AliasesToSave a ON c.SqlQueryEditingColumnID = a.AnyID
WHERE c.SqlQueryEditingID = @SqlQueryEditingID
AND a.AnyValue IS NULL 




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__SaveColumnAliases] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__SaveColumnAliases] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__SaveColumnAliases] TO [sp_executeall]
GO
