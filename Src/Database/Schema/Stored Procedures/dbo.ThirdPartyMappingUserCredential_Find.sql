SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyMappingUserCredential table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyMappingUserCredentialID int   = null ,

	@ThirdPartySystemID int   = null ,

	@ClientID int   = null ,

	@UserID int   = null ,

	@UserName varchar (255)  = null ,

	@Password varchar (65)  = null ,

	@Salt varchar (50)  = null ,

	@Domain varchar (50)  = null ,

	@AttemptedLogins int   = null ,

	@AccountDisabled bit   = null ,

	@ThirdPartyClientID varchar (50)  = null ,

	@LDAPConfigID int   = null ,

	@ImpersonateThirdPartySystemID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyMappingUserCredentialID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [UserID]
	, [UserName]
	, [Password]
	, [Salt]
	, [Domain]
	, [AttemptedLogins]
	, [AccountDisabled]
	, [ThirdPartyClientID]
	, [LDAPConfigID]
	, [ImpersonateThirdPartySystemID]
    FROM
	[dbo].[ThirdPartyMappingUserCredential] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingUserCredentialID] = @ThirdPartyMappingUserCredentialID OR @ThirdPartyMappingUserCredentialID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([UserID] = @UserID OR @UserID IS NULL)
	AND ([UserName] = @UserName OR @UserName IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([Salt] = @Salt OR @Salt IS NULL)
	AND ([Domain] = @Domain OR @Domain IS NULL)
	AND ([AttemptedLogins] = @AttemptedLogins OR @AttemptedLogins IS NULL)
	AND ([AccountDisabled] = @AccountDisabled OR @AccountDisabled IS NULL)
	AND ([ThirdPartyClientID] = @ThirdPartyClientID OR @ThirdPartyClientID IS NULL)
	AND ([LDAPConfigID] = @LDAPConfigID OR @LDAPConfigID IS NULL)
	AND ([ImpersonateThirdPartySystemID] = @ImpersonateThirdPartySystemID OR @ImpersonateThirdPartySystemID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyMappingUserCredentialID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [UserID]
	, [UserName]
	, [Password]
	, [Salt]
	, [Domain]
	, [AttemptedLogins]
	, [AccountDisabled]
	, [ThirdPartyClientID]
	, [LDAPConfigID]
	, [ImpersonateThirdPartySystemID]
    FROM
	[dbo].[ThirdPartyMappingUserCredential] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyMappingUserCredentialID] = @ThirdPartyMappingUserCredentialID AND @ThirdPartyMappingUserCredentialID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([UserID] = @UserID AND @UserID is not null)
	OR ([UserName] = @UserName AND @UserName is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([Salt] = @Salt AND @Salt is not null)
	OR ([Domain] = @Domain AND @Domain is not null)
	OR ([AttemptedLogins] = @AttemptedLogins AND @AttemptedLogins is not null)
	OR ([AccountDisabled] = @AccountDisabled AND @AccountDisabled is not null)
	OR ([ThirdPartyClientID] = @ThirdPartyClientID AND @ThirdPartyClientID is not null)
	OR ([LDAPConfigID] = @LDAPConfigID AND @LDAPConfigID is not null)
	OR ([ImpersonateThirdPartySystemID] = @ImpersonateThirdPartySystemID AND @ImpersonateThirdPartySystemID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Find] TO [sp_executeall]
GO
