SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-04
-- Description:	Save checkpoint details of a quoted product for a specific pet and return the ID
-- GPR 2019-10-10 created for Aquarius603Dev / LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[Quote_SaveQuotePetProductCheckpoint]
(
	 @QuotePetProductID	INT
	,@WhoCreated		INT
	,@SourceID			INT			  = NULL
	,@ChangeNotes		VARCHAR(2000) = NULL
	,@EvaluatedXml		XML
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @RowCount			INT
			,@WhenCreated		DATETIME = dbo.fn_GetDate_Local()

	IF @EvaluatedXml is NULL
	BEGIN
		SELECT @EvaluatedXml = qpp.EvaluatedXml
		FROM QuotePetProduct qpp WITH ( NOLOCK ) 
		WHERE qpp.QuotePetProductID = @QuotePetProductID
	END

	/*Shred the XML into a temp table*/
	DECLARE  @CheckpointsToAdd	TABLE (	 RuleSetID INT, RuleID INT, RuleOrder INT, CheckpointName VARCHAR(50)
										,Input VARCHAR(2000), Output VARCHAR(2000), Delta VARCHAR(2000), Transform VARCHAR(100)
										,Value VARCHAR(2000), ValueDecimal DECIMAL(18,8),InputDecimal DECIMAL(18,8)
										,OutputDecimal DECIMAL(18,8), DeltaDecimal DECIMAL(18,8) )

	INSERT @CheckpointsToAdd ( RuleSetID, RuleID, RuleOrder, CheckpointName, Input, Output, Delta, Transform, Value )
	SELECT	 r.RuleSetID
			,b.value('(@RuleID)[1]','INT')
			,r.RuleOrder
			,b.value('(@RuleCheckpoint)[1]','VARCHAR(50)')
			,b.value('(@Input)[1]','VARCHAR(2000)')
			,b.value('(@Output)[1]','VARCHAR(2000)')
			,b.value('(@Delta)[1]','VARCHAR(2000)')
			,b.value('(@Transform)[1]','VARCHAR(50)')
			,b.value('(@Value)[1]','VARCHAR(2000)')
	FROM @EvaluatedXml.nodes('(//RuleSet/Rule)') a(b)
	LEFT JOIN RulesEngine_Rules r WITH (NOLOCK) on r.RuleID = b.value('(@RuleID)[1]','INT')

	/*Set the typed columns*/
	UPDATE cta
	SET  InputDecimal  = CASE WHEN ISNUMERIC(cta.Input)	= 1 THEN CAST(cta.Input  as DECIMAL(18,8)) END
		,OutputDecimal = CASE WHEN ISNUMERIC(cta.Output)= 1 THEN CAST(cta.Output as DECIMAL(18,8)) END
		,DeltaDecimal  = CASE WHEN ISNUMERIC(cta.Delta)	= 1 THEN CAST(cta.Delta  as DECIMAL(18,8)) END
		,ValueDecimal  = CASE WHEN ISNUMERIC(cta.Value) = 1 THEN CAST(cta.Value  as DECIMAL(18,8)) END
	FROM @CheckpointsToAdd cta

	/*Create the new checkpoint records*/
	INSERT QuotePetProductCheckPoint ( QuotePetProductID, RuleSetID, RuleID, RuleOrder, CheckpointName
									, Input, Output, Delta, Transform, Value, ValueDecimal, InputDecimal, OutputDecimal, DeltaDecimal
									, WhenCreated, WhoCreated, SourceID, ChangeNotes )

	SELECT	 @QuotePetProductID, cta.RuleSetID, cta.RuleID, cta.RuleOrder, cta.CheckpointName, cta.Input, cta.Output, cta.Delta, cta.Transform, cta.Value
			,cta.ValueDecimal ,cta.InputDecimal, cta.OutputDecimal, cta.DeltaDecimal, @WhenCreated, @WhoCreated, @SourceID, @ChangeNotes
	FROM @CheckpointsToAdd cta 
	/*!! NO SQL HERE !!*/
	SELECT @RowCount = @@Rowcount

	-- CPS 2018-08-08 clear out the XML column once it's been used
	IF @RowCount <> 0
	BEGIN
		UPDATE qpp
		SET EvaluatedXml = NULL
		FROM QuotePetProduct qpp WITH ( NOLOCK ) 
		WHERE qpp.QuotePetProductID = @QuotePetProductID
	END	

	RETURN @RowCount
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePetProductCheckpoint] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Quote_SaveQuotePetProductCheckpoint] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePetProductCheckpoint] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePetProductCheckpoint] TO [sp_executehelper]
GO
