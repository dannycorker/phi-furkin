SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PaymentCardType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentCardType_Delete]
(

	@PaymentCardTypeID int   
)
AS


				DELETE FROM [dbo].[PaymentCardType] WITH (ROWLOCK) 
				WHERE
					[PaymentCardTypeID] = @PaymentCardTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentCardType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Delete] TO [sp_executeall]
GO
