SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Neil Afflick
-- Create date: 28/02/2019
-- Description:	Gets datalayer tracking variables for every journey
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetDataLayerJourneys]
	@Affinity int
AS
BEGIN
 
SELECT * FROM (
	
	SELECT ResourceListId AS id, FieldCaption AS fc,COALESCE(ItemValue, DetailValue) AS VALUE FROM (
    SELECT rldv.ResourceListID, df.DetailFieldID, DetailValue, df.FieldCaption, li.LookupListItemID, li.ItemValue
    FROM ResourceListDetailValues rldv
    INNER JOIN DetailFields df ON rldv.DetailFieldID = df.DetailFieldID
    LEFT JOIN LookupListItems li ON rldv.ValueInt = li.LookupListItemID
    WHERE ResourceListID IN (SELECT ResourceListID FROM ResourceListDetailValues WHERE ValueInt IN (
    SELECT LookupListItemID FROM LookupListItems WHERE LookupListID = 7680))) AS tble
    
    UNION

   SELECT ResourceListId AS id, 
	CASE 
	WHEN FieldCaption = 'Source' THEN 'SourceId' 
	WHEN FieldCaption = 'Channel' THEN 'ChannelId' 
	WHEN FieldCaption = 'Brand' THEN 'BrandId' 
	END AS fc,
	CAST(lookupListItemID AS VARCHAR) AS VALUE FROM (
	SELECT rldv.ResourceListID, df.DetailFieldID, DetailValue, df.FieldCaption, 
	CASE 
	WHEN li.LookupListItemID = 76185 THEN 2000184
	WHEN li.LookupListItemID = 76510 THEN 157876
	WHEN li.LookupListItemID = 76186 THEN 153391
	ELSE li.LookupListItemID
	END AS lookupListItemID, li.ItemValue
    FROM ResourceListDetailValues rldv
    INNER JOIN DetailFields df ON rldv.DetailFieldID = df.DetailFieldID
    LEFT JOIN LookupListItems li ON rldv.ValueInt = li.LookupListItemID
    WHERE ResourceListID IN (SELECT ResourceListID FROM ResourceListDetailValues WHERE ValueInt IN (
    SELECT LookupListItemID FROM LookupListItems WHERE LookupListID = 7680))
    AND FieldCaption in('Source', 'Channel', 'Brand')
    ) AS tble

    ) AS OUTPUT

    PIVOT (
    min(value)
        for fc in([BrandID],[SourceId],[ChannelId],[Brand],[Source],[Channel],[Description],[Token Code],[Creation/Modified Date],[Referrer URL],[DataLayerJourneySyndication], [DataLayerJourneyCampaignID],[DataLayerJourneyChannel],[DataLayerJourneyName],[DataLayerJourneyClickID])
    )p 
    

    
    
 /*    select * from (select ResourceListId as id, FieldCaption as fc, COALESCE(ItemValue, DetailValue) as Value from (
    select rldv.ResourceListID, df.DetailFieldID, DetailValue, df.FieldCaption, li.LookupListItemID, li.ItemValue
    from ResourceListDetailValues rldv
    inner join DetailFields df on rldv.DetailFieldID = df.DetailFieldID
    left join LookupListItems li on rldv.ValueInt = li.LookupListItemID
    where ResourceListID in (select ResourceListID from ResourceListDetailValues where ValueInt in (
    select LookupListItemID from LookupListItems where LookupListID = 7680)))
    as tble) as output

    PIVOT (
    min(value)
        for fc in([Brand],[Channel],[Source],[Token Code], [Referrer URL], [DataLayerJourneySyndication], [DataLayerJourneyCampaignID],[DataLayerJourneyChannel],[DataLayerJourneyName],[DataLayerJourneyClickID])
    )p */
    
    
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetDataLayerJourneys] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetDataLayerJourneys] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetDataLayerJourneys] TO [sp_executeall]
GO
