SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDocument table through an index
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_GetByLeadDocumentIDDocumentDatabaseIDClientID]
(

	@LeadDocumentID int   ,

	@DocumentDatabaseID int   ,

	@ClientID int   
)
AS
BEGIN

	SELECT
		[LeadDocumentID],
		[ClientID],
		[LeadID],
		[DocumentTypeID],
		[LeadDocumentTitle],
		[UploadDateTime],
		[WhoUploaded],
		[DocumentBLOB],
		[FileName],
		[EmailBLOB],
		[DocumentFormat],
		[EmailFrom],
		[EmailTo],
		[CcList],
		[BccList],
		[ElectronicSignatureDocumentKey],
		[Encoding],
		[ContentFormat],
		[ZipFormat],
		[DocumentBlobSize],
		[EmailBlobSize],
		[DocumentDatabaseID],
		[WhenArchived],
		[DocumentTypeVersionID]
	FROM
		[dbo].[vLeadDocumentList] WITH (NOLOCK)
	WHERE
		[LeadDocumentID] = @LeadDocumentID
		AND [DocumentDatabaseID] = @DocumentDatabaseID
		AND [ClientID] = @ClientID
		
	SELECT @@ROWCOUNT
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadDocumentIDDocumentDatabaseIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_GetByLeadDocumentIDDocumentDatabaseIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadDocumentIDDocumentDatabaseIDClientID] TO [sp_executeall]
GO
