SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ActiveSession table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession__Upsert]
(
	@EmailAddress varchar (255),
	@SessionID varchar (50)  ,
	@ThirdPartySystemId int = 0
)
AS

IF exists(SELECT * FROM [dbo].[ActiveSession] WHERE EmailAddress = @EmailAddress AND ThirdPartySystemId = @ThirdPartySystemId)
BEGIN
	print 'updating'
	UPDATE
		[dbo].[ActiveSession]
	SET SessionID = @SessionID
	WHERE 
		EmailAddress = @EmailAddress
	AND
		ThirdPartySystemId = @ThirdPartySystemId	
END
ELSE
BEGIN
	print 'inserting'
	INSERT INTO [dbo].[ActiveSession]
		(
		[SessionID],
		[EmailAddress],
		[ThirdPartySystemId]
		)
	VALUES
		(
		@SessionID
		,@EmailAddress
		,@ThirdPartySystemId
		)
END
									
							
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession__Upsert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession__Upsert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession__Upsert] TO [sp_executeall]
GO
