SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeHelperField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeHelperField_GetByEventTypeHelperFieldID]
(

	@EventTypeHelperFieldID int   
)
AS


				SELECT
					[EventTypeHelperFieldID],
					[EventTypeID],
					[DetailFieldID],
					[LeadTypeID],
					[ClientID],
					[ForceEditableOverride]
				FROM
					[dbo].[EventTypeHelperField] WITH (NOLOCK) 
				WHERE
										[EventTypeHelperFieldID] = @EventTypeHelperFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_GetByEventTypeHelperFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeHelperField_GetByEventTypeHelperFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeHelperField_GetByEventTypeHelperFieldID] TO [sp_executeall]
GO
