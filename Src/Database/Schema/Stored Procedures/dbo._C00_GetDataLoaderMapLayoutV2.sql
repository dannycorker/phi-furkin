SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-03
-- Description:	Get DataloaderMap Layout
-- TD: Added line 36, return distinct string
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetDataLoaderMapLayoutV2] 
	@ClientID INT,
	@MapID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @cols VARCHAR(MAX)=''
	
	SELECT @cols = @cols + ',' + QUOTENAME(dbo.fnRefLetterFromCaseNum(dlm.SectionAbsoluteCol+1))
	FROM dbo.DataLoaderFieldDefinition dlm
	LEFT JOIN DataLoaderObjectField dlof WITH (NOLOCK) ON dlof.DataLoaderObjectFieldID = dlm.DataLoaderObjectFieldID
	LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dlm.DetailFieldID
	WHERE DataLoaderMapID = @MapID
	AND dlm.ClientID = @ClientID
	AND dlm.SectionAbsoluteCol IS NOT NULL
	ORDER BY dlm.SectionAbsoluteCol

	DECLARE @query VARCHAR(MAX)

	SELECT @cols=STUFF(@cols, 1, 1, '')
	SELECT @cols= dbo._C00_DistinctString(@cols,',') /*Do not remove this please...If you do, please state why*/
	
	SET @query = 'SELECT * From ('
	
	SELECT @query = @query+ '
		SELECT dbo.fnRefLetterFromCaseNum(dlm.SectionAbsoluteCol+1) AS [Column Letter], COALESCE(df.FieldCaption, dlm.NamedValue, dlof.FieldName) AS [Data Required]
		FROM dbo.DataLoaderFieldDefinition dlm
		LEFT JOIN DataLoaderObjectField dlof WITH (NOLOCK) ON dlof.DataLoaderObjectFieldID = dlm.DataLoaderObjectFieldID
		LEFT JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dlm.DetailFieldID
		WHERE DataLoaderMapID = ' + CONVERT(VARCHAR,@MapID) + '
		AND dlm.SectionAbsoluteCol IS NOT NULL
		'
		
	SELECT @query += ') src
	PIVOT (MAX([Data Required]) for [Column Letter] in (' + @cols + '))	as pvt
	'

	EXEC(@query)

	PRINT @query
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataLoaderMapLayoutV2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetDataLoaderMapLayoutV2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataLoaderMapLayoutV2] TO [sp_executeall]
GO
