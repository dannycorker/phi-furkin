SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================		
-- Author:	Ian Slack		
-- Create date: 2014-09-24		
-- Description:	Retrieve Quote
-- Mods:
-- 2015-10-?? IS? Added actual retrieve quote SQL		
-- 2020-01-13 CPS for JIRA LPC-356 | Remove temp logging
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Policy_RetrieveQuote]
(		
	@XmlRequest XML,		
	@Email VARCHAR(255),		
	@SecretQuestionID VARCHAR(50),		
	@SecretQuestionValue VARCHAR(250),		
	@SaveQuoteKey VARCHAR(250),
	@QuoteSessionId INT = 0	,
	@Version VARCHAR(50) = 'QaB'
)		
AS		
BEGIN		
	
	--	2016-08-09 IS: Key Aggregator quote
	SELECT	TOP 1 
			@XmlRequest = SavedQuoteXML, 
			@SaveQuoteKey = SavedQuoteKey,
			@QuoteSessionId = QuoteSessionID
	FROM	_C600_SavedQuote WITH (NOLOCK) 
	WHERE	(
				SavedQuoteKey = @SaveQuoteKey 
				OR 
				(
					EmailAddress = @Email 
					AND 
					SecretQuestionID = @SecretQuestionID
					AND 
					SecretQuestionValue = @SecretQuestionValue
					AND
					(
						ISNULL(@QuoteSessionId,0) = 0
						OR
						@QuoteSessionId = QuoteSessionID
					) 
				)
			)
	AND		WhenExpire > dbo.fn_GetDate_Local()
	ORDER BY SavedQuoteID DESC

	UPDATE _C600_SavedQuote
		SET RetrievedDate = dbo.fn_GetDate_Local()
	WHERE SavedQuoteKey = @SaveQuoteKey
	
	IF @XmlRequest.exist('//BuyPolicyRequest') = 1
	BEGIN
		SELECT  
			@XmlRequest SavedQuoteXML,
			@SaveQuoteKey SavedQuoteKey,
			'true' AS RetrieveResult
	END
	ELSE IF @XmlRequest.exist('//SessionCache/QuoteDetails/PetsViewModel/Pets') = 0
	BEGIN

		SELECT  
			@XmlRequest SavedQuoteXML,
			@SaveQuoteKey SavedQuoteKey,
			'true' AS RetrieveResult
	END
	ELSE
	BEGIN
		SELECT  
			@XmlRequest SavedQuoteXML,
			@SaveQuoteKey SavedQuoteKey,
			'true' AS RetrieveResult
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_RetrieveQuote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_RetrieveQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_RetrieveQuote] TO [sp_executeall]
GO
