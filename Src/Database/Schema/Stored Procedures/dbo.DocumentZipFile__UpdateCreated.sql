SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-03
-- Description:	Update the whencreated details for a zip file
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipFile__UpdateCreated] 
	@DocumentZipFileID int, 
	@StatusID int = 31,
	@ZipCount int = 0, 
	@WhenCreated datetime = null 
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/* Assume the download datetime is right now unless told otherwise */
	UPDATE dbo.DocumentZipFile 
	SET ZipCount = @ZipCount, 
	StatusID = @StatusID, 
	WhenCreated = COALESCE(@WhenCreated, dbo.fn_GetDate_Local()) 
	WHERE DocumentZipFileID = @DocumentZipFileID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__UpdateCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipFile__UpdateCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__UpdateCreated] TO [sp_executeall]
GO
