SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_GetByStatusIDClientID]
(

	@StatusID int   ,

	@ClientID int   
)
AS


				SELECT
					[StatusID],
					[ClientID],
					[StatusName],
					[StatusDescription],
					[SourceID],
					[LeadTypeID],
					[IsShared]
				FROM
					dbo.fnLeadStatusShared(@ClientID)
				WHERE
										[StatusID] = @StatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_GetByStatusIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_GetByStatusIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_GetByStatusIDClientID] TO [sp_executeall]
GO
