SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-11-05
-- Description:	Gets the policy sections that are being claimed on so that the pay claim to field can be set
-- Used by:		DF 152751
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetPayMultiData] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @AlreadyPaid MONEY
	EXEC @AlreadyPaid = dbo.fn_C00_1272_GetAlreadyPaid @MatterID

	SELECT	SUM(tdvClaimed.ValueMoney) AS Claimed, SUM(tdvTotal.ValueMoney) AS Settle, @AlreadyPaid AS AlreadyPaid
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350 
	INNER JOIN dbo.ResourceListDetailValues rlSection WITH (NOLOCK) ON tdvRLID.ResourceListID = rlSection.ResourceListID AND rlSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rlSection.ValueInt = llSection.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rlSubSection WITH (NOLOCK) ON tdvRLID.ResourceListID = rlSubSection.ResourceListID AND rlSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rlSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvClaimed WITH (NOLOCK) ON r.TableRowID = tdvClaimed.TableRowID AND tdvClaimed.DetailFieldID = 144353
	INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	WHERE r.MatterID = @MatterID
	
	

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetPayMultiData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetPayMultiData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetPayMultiData] TO [sp_executeall]
GO
