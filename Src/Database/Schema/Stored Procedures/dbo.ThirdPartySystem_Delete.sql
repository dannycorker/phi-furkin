SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartySystem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartySystem_Delete]
(

	@ThirdPartySystemId int   
)
AS


				DELETE FROM [dbo].[ThirdPartySystem] WITH (ROWLOCK) 
				WHERE
					[ThirdPartySystemId] = @ThirdPartySystemId
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystem_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystem_Delete] TO [sp_executeall]
GO
