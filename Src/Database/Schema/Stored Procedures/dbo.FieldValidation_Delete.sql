SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FieldValidation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_Delete]
(

	@FieldValidationID int   
)
AS


				DELETE FROM [dbo].[FieldValidation] WITH (ROWLOCK) 
				WHERE
					[FieldValidationID] = @FieldValidationID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Delete] TO [sp_executeall]
GO
