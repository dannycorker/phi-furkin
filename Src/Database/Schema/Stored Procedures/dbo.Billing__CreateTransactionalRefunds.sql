SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-09-14
-- Description:	Create card transactional refunds (IE Max per TransactionID)
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreateTransactionalRefunds]
	@CustomerID INT, 
	@MatterID INT,
	@PurchasedProductID INT,
	@RefundAmountNet NUMERIC(18,2),
	@RefundAmountTax NUMERIC(18,2),
	@RefundAmountGross NUMERIC(18,2),
	@AccountUseID INT, /*1 = Premium, 2 = Claims*/
	@CoverFrom DATE,
	@CoverTo DATE,
	@ActualCollectionDate DATE,
	@PurchasedProductPaymentScheduleTypeID INT,
	@WhoCreated INT,
	@Debug BIT = 0,
	@PurchasedProductPaymentScheduleParentID INT = NULL,
	@TransactionFee NUMERIC(18,2) = 0.00,
	@CalledFrom VARCHAR(100) = ''
AS
BEGIN

	SET NOCOUNT ON;

	EXEC dbo._C00_LogIt 'Proc', 'Billing__CreateTransactionalRefunds', 'CalledFrom', @CalledFrom, @WhoCreated

	/*
		Requirement:
		Card payments can only be refunded to the card that they came from.
		However under some circumstances this is even further restricted. 
		IE you can only refund the amount that was taken on each transaction.

		So we will need to refund the maximum amount to each transaction until we have no more left to refund.
	*/

	DECLARE @Now DATETIME = dbo.fn_GetDate_Local(),
			@TestID INT = 0,
			@UNID INT,
			@AbsoluteMaxRefundAmount NUMERIC(18,2),
			@NewAccountID INT, 
			@NewPurchasedProductPaymentScheduleID INT,
			@NewCustomerPaymentScheduleID INT,
			@PartialRefundAmountNet NUMERIC(18,2) = 0.00,
			@PartialRefundAmountTax NUMERIC(18,2) = 0.00,
			@PartialRefundAmountGross NUMERIC(18,2) = 0.00,
			@AmountLeftToRefundGross NUMERIC(18,2) = @RefundAmountGross,
			@AmountLeftToRefundTax NUMERIC(18,2) = @RefundAmountTax,
			@MaxRefundAmount NUMERIC(18,2),
			@TransactionID VARCHAR(200),
			@XmlPayload XML,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@AccountTypeID INT,
			@ClientAccountID INT,
			@PercentageDifference NUMERIC(18,14),
			@TaxRemainder NUMERIC(18,14) = @RefundAmountTax,
			@NetRemainder NUMERIC(18,14) = @RefundAmountGross - @RefundAmountTax,
			@EarliestRefundDate DATE

	DECLARE @MaxRefunds TABLE (UNID INT IDENTITY(1,1), Amount NUMERIC(18,2), TransactionID VARCHAR(100))

	DECLARE @ProcessedRefunds TABLE (UNID INT IDENTITY(1,1), Amount NUMERIC(18,2), TransactionID VARCHAR(100))

	DECLARE @UpdateLog TABLE (LogID INT IDENTITY(1,1), UNID INT, Narative XML)

	/*
		Step 1. Get ALL transactions and amounts as there may have been a partial refund on one of them..
	*/

	INSERT INTO @MaxRefunds(Amount, TransactionID) 
	SELECT SUM(ct.Amount) AS [Amount], ct.TransactionID
	FROM CardTransaction ct WITH (NOLOCK)
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = ct.CustomerPaymentScheduleID
	WHERE ct.CustomerID = @CustomerID
	AND cps.PaymentStatusID IN (2, 6) /*Processed/Paid*/
	AND ct.Amount > 0.00
	AND EXISTS (
		SELECT *
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	)
	GROUP BY ct.TransactionID
	ORDER BY SUM(ct.Amount) DESC /*Start at the hightest sum so we as few refunds as possible..*/

	IF @Debug = 1
	BEGIN

		SELECT 'MaxRefunds', *
		FROM @MaxRefunds

	END

	INSERT INTO @ProcessedRefunds(Amount, TransactionID) 
	SELECT SUM(ct.Amount) AS [Amount], ct.TransactionID
	FROM CardTransaction ct WITH (NOLOCK)
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = ct.CustomerPaymentScheduleID
	WHERE ct.CustomerID = @CustomerID
	AND cps.PaymentStatusID IN (2, 6) /*Processed/Paid*/
	AND ct.Amount < 0.00
	AND EXISTS (
		SELECT *
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	)
	GROUP BY ct.TransactionID
	ORDER BY SUM(ct.Amount) DESC /*Start at the hightest sum so we as few refunds as possible..*/

	IF @Debug = 1
	BEGIN

		SELECT 'ProcessedRefunds', *
		FROM @ProcessedRefunds

	END

	/*
		Step 2. Remove the values that have been refunded on that transaction then delete any 0 rows.
	*/
	UPDATE m
	SET Amount = m.Amount - p.Amount
	FROM @MaxRefunds m
	INNER JOIN @ProcessedRefunds p ON p.TransactionID = m.TransactionID

	IF @Debug = 1
	BEGIN

		SELECT 'MaxRefunds - Updated', *
		FROM @MaxRefunds

	END

	/*Remove any 0 amount rows..*/
	DELETE m
	FROM @MaxRefunds m
	WHERE m.Amount = 0

	SELECT @AbsoluteMaxRefundAmount = SUM(m.Amount)
	FROM @MaxRefunds m

	IF @Debug = 1
	BEGIN

		SELECT 'MaxRefunds - Post-Delete', *
		FROM @MaxRefunds

	END

	IF @RefundAmountGross > @AbsoluteMaxRefundAmount
	BEGIN

		DECLARE @Error VARCHAR(1000) = 'Refund amount ' + CONVERT(VARCHAR(100), @RefundAmountGross) + ' is greater than the amount that can be refunded (' + CONVERT(VARCHAR(100), @AbsoluteMaxRefundAmount) + ').'
		RAISERROR(@Error,16, 6000)
		RETURN

	END

	/*Get the first row*/
	SELECT TOP 1 @UNID = m.UNID,
				@TransactionID = m.TransactionID,
				@MaxRefundAmount = m.Amount
	FROM @MaxRefunds m
	ORDER BY m.UNID

	WHILE @UNID > @TestID AND @AmountLeftToRefundGross < 0.00
	BEGIN

		/*Work out how much we can refund on this card/transaction*/
		SELECT @PartialRefundAmountGross = CASE WHEN @AmountLeftToRefundGross < @MaxRefundAmount
			THEN @AmountLeftToRefundGross
			ELSE @MaxRefundAmount
			END

		/*Update the amount left with the delta*/
		SELECT @AmountLeftToRefundGross = @AmountLeftToRefundGross - @PartialRefundAmountGross

		/*Get the percentage difference betwen the total and the partial so we can work out tax.*/
		SELECT @PercentageDifference = @PartialRefundAmountGross / @RefundAmountGross

		SELECT @PartialRefundAmountTax = @RefundAmountTax * @PercentageDifference

		SELECT @TaxRemainder = @TaxRemainder - @PartialRefundAmountTax

		SELECT @PartialRefundAmountNet = @PartialRefundAmountGross - @PartialRefundAmountTax

		SELECT @NetRemainder = @NetRemainder - @PartialRefundAmountNet

		IF @Debug = 1
		BEGIN

			SELECT @AmountLeftToRefundGross AS [AmountLeftToRefund], 
					@PartialRefundAmountNet AS [PartialRefundAmountNet],
					@PartialRefundAmountTax AS [PartialRefundAmountTax],
					@PartialRefundAmountGross AS [PartialRefundAmount], 
					@UNID AS [Unid],  
					@TestID AS [TestID],
					@PercentageDifference AS [PercentageDiff]

		END

		IF @Debug = 1
		BEGIN

			SELECT @NewAccountID = 910 + @UNID, 
					@NewPurchasedProductPaymentScheduleID = 911 + @UNID,
					@NewCustomerPaymentScheduleID = 912 + @UNID 

		END
		ELSE
		BEGIN

			SELECT @AccountTypeID = NULL, @ClientAccountID = NULL

			SELECT @AccountTypeID = a.AccountTypeID, @ClientAccountID = a.ClientAccountID, @EarliestRefundDate = DATEADD(DD, 2, ct.WhenCreated)
			FROM CardTransaction ct WITH (NOLOCK)
			INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = ct.AccountID
			WHERE ct.CustomerID = @CustomerID
			AND ct.TransactionID = @TransactionID
			
			/*If the earliest date is before the actual collection date then we can refund now.*/
			IF @EarliestRefundDate < @ActualCollectionDate
			BEGIN

				SELECT @EarliestRefundDate = @ActualCollectionDate

			END
			
			/*2021-04-15 ACE - FURKIN-514 - If the dates arent in alignment then set them to be the same.*/
			IF @EarliestRefundDate <> @ActualCollectionDate
			BEGIN

				SELECT @ActualCollectionDate = @EarliestRefundDate

			END

			IF EXISTS (
				SELECT *
				FROM Account a WITH (NOLOCK)
				WHERE a.CustomerID = @CustomerID
				AND a.CardToken = @TransactionID
				AND a.AccountTypeID = @AccountTypeID
			)
			BEGIN

				/*If we have an account with the right properties then we can use it as there is enough residual amount to add to*/
				SELECT @NewAccountID = a.AccountID
				FROM Account a WITH (NOLOCK)
				WHERE a.CustomerID = @CustomerID
				AND a.CardToken = @TransactionID
				AND a.AccountTypeID = @AccountTypeID

				/*Try getting the same CPS*/
				SELECT TOP 1 @NewCustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				FROM CustomerPaymentSchedule cps WITH (NOLOCK)
				WHERE cps.AccountID = @NewAccountID
				AND cps.CustomerID = @CustomerID
				AND cps.PaymentStatusID = 1
				ORDER BY cps.CustomerPaymentScheduleID

			END
			ELSE
			BEGIN

				INSERT INTO Account([ClientID], [CustomerID], [AccountHolderName], [FriendlyName], [AccountNumber], [Sortcode], [Reference], [CardToken], [MaskedCardNumber], [ExpiryMonth], [ExpiryYear], [ExpiryDate], [AccountTypeID], [Active], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified], [NextPaymentDate], [NextPaymentTotal], [CustomerPaymentScheduleID], [Address1], [Address2], [Town], [County], [Postcode], [CountryID], [ObjectID], [ObjectTypeID], [CardType], [FirstName], [LastName], [Bankname], [AccountSubTypeID], [SourceID], [ClientAccountID], [Comments], [AccountUseID])
				VALUES (@ClientID, 
						@CustomerID, 
						'Refund account for transaction ' + CONVERT(VARCHAR(50), @TransactionID) /*AccountHolderName*/, 
						'Refund account for transaction ' + CONVERT(VARCHAR(50), @TransactionID) /*FriendlyName*/, 
						'' /*AccountNumber*/, 
						'' /*Sortcode*/, 
						'Refund account for transaction ' + CONVERT(VARCHAR(50), @TransactionID) /*Reference*/, 
						@TransactionID /*CardToken*/, 
						NULL /*MaskedCardNumber*/, 
						NULL /*ExpiryMonth*/, 
						NULL /*ExpiryYear*/, 
						NULL /*ExpiryDate*/, 
						@AccountTypeID /*AccountTypeID*/, 
						1 /*Active*/, 
						@WhoCreated, 
						@Now /*WhenCreated*/, 
						@WhoCreated /*WhoModified*/, 
						@Now /*WhenModified*/, 
						@EarliestRefundDate /*NextPaymentDate*/, 
						@PartialRefundAmountGross /*NextPaymentTotal*/, 
						NULL /*CustomerPaymentScheduleID*/, 
						'' /*Address1*/, '' /*Address2*/, '' /*Town*/, '' /*County*/, '' /*Postcode*/, '' /*CountryID*/, 
						@MatterID /*ObjectID*/, 
						2 /*ObjectTypeID*/, 
						NULL /*CardType*/, 
						'' /*FirstName*/, 
						'' /*LastName*/, 
						'' /*Bankname*/, 
						NULL /*AccountSubTypeID*/, 
						@UNID /*SourceID*/, 
						@ClientAccountID /*ClientAccountID*/, 
						'Account created for transactional refund' /*Comments*/, 
						@AccountUseID
						)

				SELECT @NewAccountID = SCOPE_IDENTITY()

			END

			INSERT INTO PurchasedProductPaymentSchedule([ClientID], [CustomerID], [AccountID], [PurchasedProductID], [ActualCollectionDate], [CoverFrom], [CoverTo], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], [PurchasedProductPaymentScheduleTypeID], [PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID], [TransactionFee])
			VALUES (
						@ClientID, 
						@CustomerID, 
						@NewAccountID /*AccountID*/, 
						@PurchasedProductID /*PurchasedProductID*/, 
						@ActualCollectionDate /*ActualCollectionDate*/, 
						@CoverFrom /*CoverFrom*/, 
						@CoverTo /*CoverTo*/, 
						CONVERT(DATE,@EarliestRefundDate) /*PaymentDate*/, 
						@PartialRefundAmountNet /*PaymentNet*/, 
						@PartialRefundAmountTax /*PaymentVAT*/, 
						@PartialRefundAmountGross /*PaymentGross*/, 
						1 /*PaymentStatusID*/, 
						NULL /*CustomerLedgerID*/, 
						NULL /*ReconciledDate*/, 
						@NewCustomerPaymentScheduleID /*CustomerPaymentScheduleID*/, 
						@WhoCreated /*WhoCreated*/, 
						@Now /*WhenCreated*/, 
						@PurchasedProductPaymentScheduleParentID /*PurchasedProductPaymentScheduleParentID*/, 
						@PurchasedProductPaymentScheduleTypeID, 
						NULL /*PaymentGroupedIntoID*/, 
						NULL /*ContraCustomerLedgerID*/, 
						@ClientAccountID, 
						@WhoCreated /*WhoModified*/, 
						@Now /*WhenModified*/, 
						@UNID /*SourceID*/, 
						0.00 /*TransactionFee*/
					)

			SELECT @NewPurchasedProductPaymentScheduleID = SCOPE_IDENTITY()

			IF @NewCustomerPaymentScheduleID IS NULL
			BEGIN

				/*Customer Payment Schedule Next*/
				INSERT INTO dbo.CustomerPaymentSchedule ([ClientID], [CustomerID], [AccountID], [ActualCollectionDate], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [WhoCreated], [WhenCreated], [SourceID], [RelatedObjectID], [RelatedObjectTypeID], [ClientAccountID], [WhoModified], [WhenModified], [LegacyRef])
				VALUES (
							@ClientID, 
							@CustomerID, 
							@NewAccountID, 
							@ActualCollectionDate /*ActualCollectionDate*/, 
							CONVERT(DATE, @EarliestRefundDate) /*PaymentDate*/, 
							@PartialRefundAmountNet /*PaymentNet*/, 
							@PartialRefundAmountTax /*PaymentVAT*/, 
							@PartialRefundAmountGross /*PaymentGross*/, 
							1 /*PaymentStatusID*/, 
							NULL /*CustomerLedgerID*/, 
							NULL /*ReconciledDate*/, 
							@WhoCreated /*WhoCreated*/, 
							@Now /*WhenCreated*/, 
							@UNID /*SourceID*/, 
							@MatterID /*RelatedObjectID*/, 
							2 /*RelatedObjectTypeID*/, 
							@ClientAccountID, 
							@WhoCreated, 
							@Now /*WhenModified*/, 
							'' /*LegacyRef*/
						)

				SELECT @NewCustomerPaymentScheduleID = SCOPE_IDENTITY()

				/*2020-02-12 ACE Not NULL surely?*/
				IF @NewPurchasedProductPaymentScheduleID IS NOT NULL
				BEGIN

					/*Update the PPPS with the CPS*/
					UPDATE ppps
					SET PurchasedProductPaymentScheduleParentID = PurchasedProductPaymentScheduleID, 
						CustomerPaymentScheduleID = @NewCustomerPaymentScheduleID
					FROM PurchasedProductPaymentSchedule ppps 
					WHERE ppps.PurchasedProductPaymentScheduleID = @NewPurchasedProductPaymentScheduleID

				END

			END
			ELSE
			BEGIN

				UPDATE cps
				SET PaymentNet = ISNULL(cps.PaymentNet, 0.00) +  ISNULL(@PartialRefundAmountNet, 0.00),
					PaymentVAT = ISNULL(cps.PaymentVAT, 0.00) +  ISNULL(@PartialRefundAmountTax, 0.00),
					PaymentGross = ISNULL(cps.PaymentGross, 0.00) +  ISNULL(@PartialRefundAmountGross, 0.00)
				FROM CustomerPaymentSchedule cps
				WHERE cps.CustomerPaymentScheduleID = @NewCustomerPaymentScheduleID

			END

		END

		SELECT @XmlPayload = (
			SELECT	ISNULL(@UNID, -1),
					ISNULL(@AmountLeftToRefundGross, 0.00) AS [AmountLeftToRefund], 
					ISNULL(@PartialRefundAmountNet, 0.00) AS [PartialRefundAmountNet], 
					ISNULL(@PartialRefundAmountTax, 0.00) AS [PartialRefundAmountTax], 
					ISNULL(@PartialRefundAmountGross, 0.00) AS [PartialRefundAmount], 
					@TestID AS [TestID],
					ISNULL(@NewAccountID, -1) AS [NewAccountID],
					ISNULL(@NewPurchasedProductPaymentScheduleID, -1) AS [PurchasedProductPaymentScheduleID],
					ISNULL(@NewCustomerPaymentScheduleID, -1) AS [CustomerPaymentScheduleID]
			FOR XML PATH ('update')
		)

		INSERT INTO @UpdateLog(UNID, Narative)
		VALUES (@UNID, @XmlPayload)

		SELECT @TestID = @UNID

		SELECT TOP 1 @UNID = m.UNID,
					@TransactionID = m.TransactionID,
					@MaxRefundAmount = m.Amount
		FROM @MaxRefunds m
		ORDER BY m.UNID

	END

	IF @NewPurchasedProductPaymentScheduleID > 0 AND (@TaxRemainder > 0.00 OR @NetRemainder > 0.00)
	BEGIN

		UPDATE ppps
		SET PaymentNet = PaymentNet + ISNULL(@NetRemainder, 0.00), PaymentVAT = PaymentVAT + ISNULL(@TaxRemainder, 0.00)
		FROM PurchasedProductPaymentSchedule ppps
		WHERE ppps.PurchasedProductPaymentScheduleID = @NewPurchasedProductPaymentScheduleID

		UPDATE cps
		SET PaymentNet = PaymentNet + ISNULL(@NetRemainder, 0.00), PaymentVAT = PaymentVAT + ISNULL(@TaxRemainder, 0.00)
		FROM CustomerPaymentSchedule cps
		WHERE cps.CustomerPaymentScheduleID = @NewCustomerPaymentScheduleID

	END

	SELECT @XmlPayload = (
		SELECT *
		FROM @UpdateLog u
		FOR XML PATH ('updates')
	)

	EXEC dbo._C00_LogItXML 600, @PurchasedProductID, 'Billing__CreateTransactionalRefunds', @XmlPayload

END
GO
