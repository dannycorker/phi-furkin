SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditingCriteriaSet table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSet_Update]
(

	@SqlQueryEditingCriteriaSetID int   ,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SetName varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditingCriteriaSet]
				SET
					[ClientID] = @ClientID
					,[SqlQueryEditingID] = @SqlQueryEditingID
					,[SetName] = @SetName
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSet_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Update] TO [sp_executeall]
GO
