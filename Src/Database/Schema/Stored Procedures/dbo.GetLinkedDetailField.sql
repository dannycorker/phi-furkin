SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE [dbo].[GetLinkedDetailField]
@LeadTypeIDTo int,
@LeadTypeIDFrom int,
@DetailFieldIDFrom int

as

SELECT LinkedDetailFieldID, LeadTypeIDTo, LeadTypeIDFrom, DetailFieldIDTo, DetailFieldIDFrom, DisplayOnPageID, 
FieldOrder, Enabled, History, LeadOrMatter, FieldName, LeadLinkedTo, LeadLinkedFrom, IncludeLinkedField, ClientID 
FROM dbo.LinkedDetailFields
WHERE (LeadTypeIDTo = @LeadTypeIDTo) AND (LeadTypeIDFrom = @LeadTypeIDFrom) AND (DetailFieldIDFrom = @DetailFieldIDFrom)






GO
GRANT VIEW DEFINITION ON  [dbo].[GetLinkedDetailField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLinkedDetailField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLinkedDetailField] TO [sp_executeall]
GO
