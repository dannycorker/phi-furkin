SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-16
-- Description:	Helpful details about a DocumentType
--				CS 2013-09-10 - Added fn_C00_GetUrlByDatabase() to control URLs
-- 2019-10-15 CPS for JIRA LPC-76  | Added DocumentTypeVersion support
-- 2019-11-05 CPS for JIRA LPC-105 | Added [FromDetails] to DocumentTypeInfo
-- =============================================
CREATE PROCEDURE [dbo].[DocumentType__Describe] 
	@DocumentTypeID int,
	@ShowFullDetails bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FolderID int 

	SELECT TOP 1 @FolderID = dtfl.FolderID 
	FROM DocumentTypeFolderLink dtfl (nolock)
	WHERE dtfl.DocumentTypeID = @DocumentTypeID 
		
	/* Show some or all DocumentType values */
	IF @ShowFullDetails = 1
	BEGIN
		/* Show all values */
		SELECT 'Document Type Info' as [Document Type Info], dt.* 
		FROM dbo.DocumentType dt (nolock) 
		WHERE dt.DocumentTypeID = @DocumentTypeID 
	END
	ELSE
	BEGIN
		/* Show some values, but not the huge template columns */
		SELECT 'Document Type Info' as [Document Type Info], 
		NULL [DocumentTypeVersionID],
		[DocumentTypeID],
		[ClientID],
		DATALENGTH([Template]) AS [Doc Size],
		DATALENGTH([EmailBodyText]) AS [Email Size],
		[LeadTypeID],
		[DocumentTypeName],
		[DocumentTypeDescription],
		[CanBeAutoSent],
		[EmailSubject],
		[InputFormat],
		[OutputFormat],
		[Enabled],
		[FromDetails],
		[RecipientsTo],
		[RecipientsCC],
		[RecipientsBCC],
		[ReadOnlyTo],
		[ReadOnlyCC],
		[ReadOnlyBCC],
		[SendToMultipleRecipients],
		[MultipleRecipientDataSourceType],
		[MultipleRecipientDataSourceID],
		[SendToAllByDefault],
		[ExcelTemplatePath],
		[DocumentTitleTemplate],
		[WhenCreated],
		[WhoCreated],
		[WhenModified],
		[WhoModified]
		FROM dbo.DocumentType dt (nolock) 
		WHERE dt.DocumentTypeID = @DocumentTypeID 
	
		UNION ALL

		SELECT 'Document Type Info' as [Document Type Info], 
		[DocumentTypeVersionID],
		[DocumentTypeID],
		[ClientID],
		DATALENGTH([Template]) AS [Doc Size],
		DATALENGTH([EmailBodyText]) AS [Email Size],
		[LeadTypeID],
		[DocumentTypeName],
		[DocumentTypeDescription],
		[CanBeAutoSent],
		[EmailSubject],
		[InputFormat],
		[OutputFormat],
		[Enabled],
		[FromDetails],
		[RecipientsTo],
		[RecipientsCC],
		[RecipientsBCC],
		[ReadOnlyTo],
		[ReadOnlyCC],
		[ReadOnlyBCC],
		[SendToMultipleRecipients],
		[MultipleRecipientDataSourceType],
		[MultipleRecipientDataSourceID],
		[SendToAllByDefault],
		[ExcelTemplatePath],
		[DocumentTitleTemplate],
		[WhenCreated],
		[WhoCreated],
		[WhenModified],
		[WhoModified]
		FROM dbo.DocumentTypeVersion dt (nolock) 
		WHERE dt.DocumentTypeID = @DocumentTypeID 

		ORDER BY [DocumentTypeVersionID]

	END
	
	/* Show more details for info */
	SELECT TOP 1 'Other Info' as [Other Info], cl.CompanyName, lt.LeadTypeName, le.LeadID, le.LeadEventID, le.LeadDocumentID , dtc.LastParsed ,
	dbo.fn_C00_GetUrlByDatabase() + 'DocumentManagement/EditDocument.aspx?returnUrl=DocumentMgr.aspx&did=' + convert(varchar,@DocumentTypeID) + '&fid=' + convert(varchar,@FolderID) + '&a=Edit' [Edit Document]
	FROM dbo.DocumentType dt (nolock) 
	INNER JOIN dbo.Clients cl (nolock) ON cl.ClientID = dt.ClientID 
	LEFT JOIN dbo.LeadType lt (nolock) ON lt.LeadTypeID = dt.LeadTypeID 
	LEFT JOIN dbo.LeadDocument ld (nolock) ON ld.DocumentTypeID = dt.DocumentTypeID 
	LEFT JOIN dbo.LeadEvent le (nolock) ON le.LeadDocumentID = ld.LeadDocumentID 
	LEFT JOIN dbo.DocumentTargetControl dtc WITH (NOLOCK) on dtc.DocumentTypeID = dt.DocumentTypeID
	WHERE dt.DocumentTypeID = @DocumentTypeID 

	/* EventType details */
	SELECT 'EventType Details' as [EventType Details], et.* 
	FROM dbo.EventType et (nolock) 
	WHERE et.DocumentTypeID = @DocumentTypeID 
	

	/* Use Folder Link record to get folder info */
	IF @FolderID > 0
	BEGIN
		/* Folder Link record */
		SELECT 'Folder Link Info' as [Folder Link Info], dtfl.* 
		FROM DocumentTypeFolderLink dtfl (nolock)
		WHERE dtfl.DocumentTypeID = @DocumentTypeID 
		
		/* Show all Folder details */
		EXEC dbo.Folder__Describe @FolderID
	END
	
	select * 
	from DocumentTypeHistory with (nolock) 
	where DocumentTypeID=@DocumentTypeID 
	ORDER BY DocumentTypeHistoryID DESC
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__Describe] TO [sp_executehelper]
GO
