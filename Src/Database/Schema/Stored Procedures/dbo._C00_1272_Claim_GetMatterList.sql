SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-24
-- Description:	Gets the claims for the custom matter list
-- 2019-01-28 GPR altered JOIN/SELECT for Condition for C600 Defect 1373
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetMatterList] 
(
	@ClaimLeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	m.CaseID,m.MatterID, s.StatusName, llType.ItemValue AS Type, mdvStart.ValueDate AS TreatmentStart, mdvCondition.DetailValue AS Condition, 
			mdvClaimPaid.DetailValue AS ClaimPaid
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON m.CaseID = c.CaseID
	LEFT JOIN dbo.LeadStatus s WITH (NOLOCK) ON c.ClientStatusID = s.StatusID
	LEFT JOIN dbo.MatterDetailValues mdvType WITH (NOLOCK) ON m.MatterID = mdvType.MatterID AND mdvType.DetailFieldID = 144483
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON mdvType.ValueInt = llType.LookupListItemID
	LEFT JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 144366
	LEFT JOIN dbo.MatterDetailValues rldv WITH ( NOLOCK ) ON rldv.MatterID = m.MatterID AND rldv.DetailFieldID = 144504 /*GPR 2019-01-28 C600 #1373*/
	LEFT JOIN dbo.ResourceListDetailValues mdvCondition WITH ( NOLOCK ) ON mdvCondition.ResourceListID = rldv.ValueInt AND mdvCondition.DetailFieldID = 144340 /*GPR 2019-01-28 C600 #1373*/
	--LEFT JOIN dbo.MatterDetailValues mdvCondition WITH (NOLOCK) ON m.MatterID = mdvCondition.MatterID AND mdvCondition.DetailFieldID = 144472 /*GPR 2019-01-28 C600 #1373*/
	LEFT JOIN dbo.MatterDetailValues mdvClaimPaid WITH (NOLOCK) ON m.MatterID = mdvClaimPaid.MatterID AND mdvClaimPaid.DetailFieldID = 147608
	WHERE m.LeadID = @ClaimLeadID
	ORDER BY m.MatterID DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetMatterList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetMatterList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetMatterList] TO [sp_executeall]
GO
