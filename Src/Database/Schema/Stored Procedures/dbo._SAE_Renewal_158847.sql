SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2020-01-07
-- Description:	Sql After Event - Create or Update Purchased Product following a Re-Rate for Renewal, Adapted from original SAE on ET 158847
-- 2020-01-10 GPR	Added Card Specific Parts
					-- This procedure does not currently prepare field values for WorldPay
					-- This procedure does not currently create a Card Transaction
					-- This procedure does not currently cater for Card Payers where they have already paid for the renewal term
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- 2020-08-18 ACE Removed redundant lead join and leadtype variable, both not required. Removed redundant Customer lookups
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Renewal_158847]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			/* Specific to procedure */
			,@ActualCollectionDate			DATE
			,@RenewalTableRowID				INT
			,@PaymentGross					MONEY
			,@PaymentTax					MONEY
			,@PaymentNet					MONEY
			,@Comments						VARCHAR(2000) = ''
			,@AccountID						INT
			,@ColLeadID						INT
			,@ColMatterID					INT
			,@ClientAccountID				INT
			,@CustomerPaymentScheduleID		INT
			,@Amount						MONEY
			,@PaymentTypeID					INT
			,@ClientPaymentGatewayID		INT
			,@PaymentFrequencyID			INT
			,@PurchaseDate					DATE
			,@ValidFrom						DATE
			,@ValidTo						DATE
			,@PreferredPaymentDay			INT
			,@FirstPaymentDate				DATE
			,@RemainderUpFront				MONEY
			,@ProductName					VARCHAR(50)
			,@ProductDescription			VARCHAR(1000)
			,@RuleSetID						INT
			,@PremiumGross					MONEY
			,@PremiumTax					MONEY
			,@PremiumNet					MONEY
			,@PremiumCalculationDetailID	INT
			,@AdminFee						MONEY
			,@PurchasedProductID			INT
			,@ProductCostBreakdown			XML
			,@Firstname						VARCHAR(100)
			,@Lastname						VARCHAR(100)		
			,@Address1						VARCHAR(50)
			,@Address2						VARCHAR(50)
			,@AddressTown					VARCHAR(50)
			,@AddressCounty					VARCHAR(50)
			,@Postcode						VARCHAR(50)
			,@OldPurchasedProductID			INT
			,@OldPaymentGross				MONEY
			,@OldPremiumTax					MONEY
			,@OldPremiumNet					MONEY
			,@NewAccountID					INT
			,@NewCardTransactionID			INT
			,@PremiumGrossRemainder			NUMERIC(18,2)
			,@TransactionFeeRemainder		NUMERIC(18,2)
			,@PaymentGatewayID				INT
			,@MerchantCode					VARCHAR(200)
			,@PremiumNetRemainder			NUMERIC(18,2)
			,@PremiumTaxRemainder			NUMERIC(18,2)
			,@PremiumGrossLogged			NUMERIC(18,2)
			,@PremiumNetLogged				NUMERIC(18,2)
			,@PremiumTaxLogged				NUMERIC(18,2)
			,@TransactionFeeLogged			NUMERIC(18,2)
			,@PaymentID						INT
			,@ValueDate						DATE
			,@Now							DATE
			,@CustomerLedgerID				INT
			,@PaymentTypeLuli				INT
			,@PaymentMethod					INT
			,@TransactionFee				NUMERIC(18,2)
			,@CurrencyID INT

	/* 0. Identify Identities */
	SELECT TOP (1) @LeadID		= le.LeadID
					,@EventTypeID	= le.EventTypeID
					,@CustomerID	= m.CustomerID
					,@ClientID		= m.ClientID
					,@CaseID		= le.CaseID
					,@WhoCreated	= le.WhoCreated
					,@MatterID		= m.MatterID
					,@WhenCreated	= le.WhenCreated
					,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (m.ClientID,53,'CFG|AqAutomationCPID',0)
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID

	/* 1. Prepare the data */
	
	/*Actual Collection Date: Policy End Date + 1*/
	SELECT @ActualCollectionDate = DATEADD(DD, 1, dbo.fnGetSimpleDvAsDate(170037 /*Policy End Date*/,@MatterID))

	/*Payment Values from the Renewal Row*/
	SELECT TOP (1) @PaymentFrequencyID = 5 /*Annually*/,
			@PurchaseDate = dbo.fnGetDvAsDate(177418,@CaseID),
			@ValidFrom = DATEADD(DAY, 1, dbo.fnGetDvAsDate(170037,@CaseID)), /*Policy End Date*/
			@ValidTo = DATEADD(YYYY, 1, dbo.fnGetDvAsDate(170037,@CaseID)), /*Policy End Date*/
			@PreferredPaymentDay = dbo.fnGetDvAsInt(170168,@CaseID),
			@FirstPaymentDate = NULL,
			@RemainderUpFront = bf.RemainderUpFront,
			@ProductName = rlpn.DetailValue,
			@ProductDescription = petname.DetailValue + ' (Policy No: ' + polnum.DetailValue + ')' ,
			@RuleSetID = dbo.fn_C00_1273_GetRuleSetFromPolicyAndDate(@MatterID, dbo.fnGetDvAsDate(176925,@CaseID)),
			@PremiumGross = wp.AnnualPriceForRiskGross,
			@PaymentGross = wp.AnnualPriceForRiskGross,
			@PremiumTax = wp.AnnualPriceForRiskNationalTax,
			@PaymentTax = wp.AnnualPriceForRiskNationalTax,
			@PremiumNet = wp.AnnualPriceForRiskNET,
			@PremiumCalculationDetailID = wp.PremiumCalculationID,
			@AdminFee = mdvAdmin.ValueMoney
	FROM Matter m WITH (NOLOCK)
	INNER JOIN MatterDetailValues pn WITH (NOLOCK) ON m.MatterID=pn.MatterID AND pn.DetailFieldID=170034
	INNER JOIN ResourceListDetailValues rlpn WITH (NOLOCK) ON rlpn.ResourceListID=pn.ValueInt AND rlpn.DetailFieldID=146200
	INNER JOIN WrittenPremium wp WITH (NOLOCK) on wp.MatterID = m.MatterID
	INNER JOIN LeadDetailValues petname WITH (NOLOCK) ON m.LeadID=petname.LeadID AND petname.DetailFieldID=144268
	INNER JOIN MatterDetailValues polnum WITH (NOLOCK) ON m.MatterID=polnum.MatterID AND polnum.DetailFieldID=170050 /*Actual Policy Number*/
	INNER JOIN BillingConfiguration bf WITH (NOLOCK) ON bf.ClientID=m.ClientID
		LEFT JOIN MatterDetailValues mdvAdmin WITH ( NOLOCK ) on mdvAdmin.MatterID = m.MatterID AND mdvAdmin.DetailFieldID = 177470 /*Current Admin Fee*/
	WHERE m.MatterID=@MatterID 
	AND wp.AdjustmentTypeID = 2 /*Renewal*/
	ORDER BY wp.ValidFrom DESC

	SELECT	 @FirstName		= cu.FirstName
			,@LastName		= cu.LastName
			,@Address1		= cu.Address1
			,@Address2		= cu.Address2
			,@AddressTown	= cu.Town
			,@AddressCounty = cu.County
			,@Postcode		= cu.PostCode
	FROM Customers cu WITH (NOLOCK) 
	WHERE cu.CustomerID = @CustomerID 

	SELECT @PaymentNet = @PaymentGross - @PaymentTax

	SELECT @PaymentTypeLuli = 69931 /*Credit Card*/
	SELECT @PaymentMethod = 69944 /*Annually*/

	SELECT @TransactionFee = dbo._C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency(@AddressCounty, @PaymentTypeLuli, @PaymentMethod)

	/* 2.1. Create or Update the Purchased Product */
	IF NOT EXISTS (
			SELECT *
			FROM PurchasedProduct pp WITH (NOLOCK)
			WHERE pp.ObjectID = @MatterID
			AND pp.ValidFrom >= @ActualCollectionDate
		)
	BEGIN

		SELECT @Comments += CHAR(13) + CHAR(10) + 'PP Does Not Exist. Creating PP'

		/*Get the current account paying for this Policy - we should have written this to the renewal accouintID field, but default to the Purchasedproduct that was paying for this policy if not*/ 
		SELECT TOP 1 @AccountID = ISNULL(dbo.fnGetSimpleDvAsInt(180219,@MatterID),p.AccountID) FROM PurchasedProduct p with (NOLOCK) 
		WHERE p.ObjectID = @MatterID 
		Order by p.PurchasedProductID DESC  
		
		/* JEL 2018-03-19 Pick up the affinity specific client account ID to write to the customers account record*/ 
		SELECT @ClientAccountID = rldv.ValueInt 
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN Matter m WITH ( NOLOCK ) on m.CustomerID = cdv.CustomerID 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt
		WHERE m.MatterID = @MatterID 
		AND cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 179949	
		
		SELECT @CurrencyID = ca.CurrencyID
		FROM ClientAccount ca WITH (NOLOCK) 
		WHERE ca.ClientAccountID = @ClientAccountID

		EXEC @PurchasedProductID = Billing__CreatePurchasedProductAndPaymentSchedule @ClientID, @CustomerID, @AccountID, @PaymentFrequencyID, @PurchaseDate, @ValidFrom, 
			@ValidTo, @PreferredPaymentDay, @FirstPaymentDate, @RemainderUpFront, @ProductName, @ProductDescription, @PremiumNet, @PremiumTax, @PremiumGross, @RuleSetID,
			@ProductCostBreakdown, @MatterID, 2, @WhoCreated, @PremiumCalculationDetailID, @AdminFee, @ClientAccountID, @CurrencyID

		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @Comments, 0
			/*GPR 2020-01-10 - IF payment method = Card then we need to prepare thid party fields for WorldPay*/

		IF [dbo].[fnGetSimpleDvAsInt](170114, @MatterID) = 69931 /*Credit Card*/
		BEGIN

			SELECT @Comments = ''
		
			SELECT TOP 1 @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
						,@Amount = c.PaymentGross
						,@AccountID = ISNULL(@AccountID,c.AccountID)
						,@ActualCollectionDate = CASE WHEN dbo.fnGetSimpleDvAsInt(177110,@MatterID) = 74339 /*Renewal Type = "Auto"*/ THEN pp.ValidFrom ELSE p.ActualCollectionDate END
			FROM CustomerPaymentSchedule c WITH (NOLOCK)
			INNER JOIN PurchasedProductPaymentSchedule p WITH ( NOLOCK ) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID 
			INNER JOIN PurchasedProduct pp WITH ( NOLOCK ) on pp.PurchasedProductID = p.PurchasedProductID
			WHERE c.CustomerID = @CustomerID
			AND c.PaymentStatusID = 1 /*New*/
			AND p.PurchasedProductPaymentScheduleTypeID = 1 /*Scheduled*/
			AND pp.ObjectID = @MatterID
			ORDER BY c.CustomerPaymentScheduleID DESC

			SELECT @Comments += 'Renewing with the following details.'														+ CHAR(13)+CHAR(10)
								+  'Amount: £' + ISNULL(CONVERT(VARCHAR,@Amount),'NULL')										+ CHAR(13)+CHAR(10)
								+  'AccountID: ' + ISNULL(CONVERT(VARCHAR,@AccountID),'NULL')									+ CHAR(13)+CHAR(10)
								+  'CustomerPaymentScheduleID: ' + ISNULL(CONVERT(VARCHAR,@CustomerPaymentScheduleID),'NULL')	+ CHAR(13)+CHAR(10)
								+  'CollectionDate: ' + ISNULL(CONVERT(VARCHAR,@ActualCollectionDate,121),'NULL')				+ CHAR(13)+CHAR(10)

			UPDATE cps
			SET  AccountID = @AccountID
				,ActualCollectionDate = @ActualCollectionDate
				,PaymentDate = @ActualCollectionDate
			FROM CustomerPaymentSchedule cps 
			WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

			UPDATE ppps
			SET  AccountID = @AccountID
				,ActualCollectionDate = @ActualCollectionDate
				,PaymentDate = @ActualCollectionDate
			FROM PurchasedProductPaymentSchedule ppps 
			WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

			SELECT	 @ActualCollectionDate	 = cps.ActualCollectionDate
					,@PaymentGross			 = cps.PaymentGross
					,@PaymentNet			 = cps.PaymentNet
					,@PaymentTax			 = cps.PaymentVat
					,@PaymentTypeID			 = 1
					,@ClientPaymentGatewayID = 7
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

			EXEC CardTransaction__PrepareThirdPartyFields @MatterID, @WhoCreated, 'Renewal Payment', @PaymentGross, @FirstName, @LastName, @Address1, @Address2, @AddressTown, @AddressCounty, 'GB', @Postcode, @CustomerPaymentScheduleID, @LeadEventID
			EXEC IncomingPayment__PrepareThirdPartyFields @MatterID, @WhoCreated, 69931 /*PaymentTypeID = "Credit Card"*/, 'Renewal Payment', @LeadEventID, @PaymentNet, @PaymentTax, @PaymentGross, @CustomerPaymentScheduleID, 1, @ActualCollectionDate, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @LeadEventID, NULL, NULL, NULL, @LeadEventID

		END

	END
	ELSE /* 2.2. PurchasedProduct Already Exists - Update the Purchased Product */
	BEGIN

		SELECT @Comments = @Comments + CHAR(13) + CHAR(10) + 'PP Exists'

		/*Check for case 3 (payment made for full amount)*/
		SELECT @OldPremiumNet = pp.ProductCostNet, @OldPremiumTax = pp.ProductCostVAT, @OldPaymentGross = pp.ProductCostGross, @OldPurchasedProductID = pp.PurchasedProductID, @AccountID = pp.AccountID
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.ObjectID = @MatterID
		AND pp.ValidFrom >= @ActualCollectionDate

		UPDATE pp
		SET ProductCostNet = @PremiumNet, ProductCostVAT = @PremiumTax, ProductCostGross = @PremiumGross
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.PurchasedProductID = @OldPurchasedProductID
			
		/*GPR 2019-04-29 #1656*/
		EXEC _C00_LogIt 'Info', 'PremiumCalculationDetailID on PP', '_SAE_Renewals_CreatePurchasedProduct', @PremiumCalculationDetailID, 58550 /*Gavin Reynolds*/
			
		UPDATE pp
		SET PremiumCalculationDetailID = @PremiumCalculationDetailID
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.PurchasedProductID = @OldPurchasedProductID
			
		/*Card Payer Specific*/
		IF [dbo].[fnGetSimpleDvAsInt](170114, @MatterID) = 69931
		BEGIN


		IF EXISTS (SELECT SUM(ctp.PolicyPaymentAmount)
			FROM CardTransactionPolicy ctp WITH (NOLOCK)
			INNER JOIN [dbo].[CardTransaction] ct WITH (NOLOCK) ON ct.CardTransactionID = ctp.CardTransactionID
			WHERE ctp.PAMatterID = @MatterID
			AND ct.WhenCreated > dbo.fn_GetDate_Local()-30
			AND ct.ErrorCode IN ('CAPTURED','SETTLED_BY_MERCHANT','SETTLED')
			HAVING (SUM(ctp.PolicyPaymentAmount) >= @OldPaymentGross)
		)
		BEGIN

			SELECT top 1  @NewAccountID = ct.AccountID 
			FROM CardTransactionPolicy ctp WITH (NOLOCK)
			INNER JOIN [dbo].[CardTransaction] ct WITH (NOLOCK) ON ct.CardTransactionID = ctp.CardTransactionID
			INNER JOIN PurchasedProduct p with (NOLOCK) on p.PurchasedProductID = ct.ParentID
			WHERE ctp.PAMatterID = @MatterID
			AND ct.WhenCreated > dbo.fn_GetDate_Local()-30
			AND ct.ErrorCode IN ('CAPTURED','SETTLED_BY_MERCHANT','SETTLED')
			AND p.ObjectID = @MatterID 
			--HAVING (SUM(ctp.PolicyPaymentAmount) >= @OldPaymentGross)

			SELECT @Comments = @Comments + CHAR(13) + CHAR(10) + 'PP has been paid for'

			/*We have already paid off next years policy. We will need to mark the payment as paid and add a row for the remainder*/
			DELETE ppps
			FROM PurchasedProductPaymentSchedule ppps 
			WHERE ppps.PurchasedProductID = @OldPurchasedProductID
			AND ppps.PaymentStatusID = 1 /*New*/

			SELECT @PremiumNetLogged = SUM(ppps.PaymentNet), @PremiumTaxLogged = SUM(ppps.PaymentVAT), @PremiumGrossLogged = SUM(ppps.PaymentGross), @TransactionFeeLogged = SUM(ppps.TransactionFee)
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) /* SLACKY-PERF-20190705 */
			WHERE ppps.PurchasedProductID = @OldPurchasedProductID
			AND ppps.PaymentStatusID = 6 /*Paid*/

			/*Now... work out the new reamining amount between the newly calculated next years premium and the current paid amounts for next years premium and add a ppps for that. We can then re-gen the CPS*/
			SELECT @PremiumNetRemainder = @PremiumNet -      ISNULL(@PremiumNetLogged, 0.00),
					@PremiumTaxRemainder = @PremiumTax -     ISNULL(@PremiumTaxLogged, 0.00),
					@PremiumGrossRemainder = @PremiumGross - ISNULL(@PremiumGrossLogged, 0.00),
					@TransactionFeeRemainder = @TransactionFee - ISNULL(@TransactionFeeLogged, 0.00)
				
			/*If the payment is a refund, we need to ensure this PPPS record is a CardPayer event,
				Otherwise same rules apply  (set as DD to be overwritten by the response file*/ 
			IF @PremiumGrossRemainder > 0 
			BEGIN 	
				/*Insert the adjustment, keep this tracked to the historic account*/ 
				INSERT INTO PurchasedProductPaymentSchedule([ClientID], [CustomerID], [AccountID], [PurchasedProductID], [ActualCollectionDate], [CoverFrom], [CoverTo], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], [PurchasedProductPaymentScheduleTypeID], [PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID], TransactionFee)
				SELECT @ClientID, @CustomerID, @AccountID, @OldPurchasedProductID, @Now, [ValidFrom], [ValidTo], @Now, @PremiumNetRemainder, @PremiumTaxRemainder, @PremiumGrossRemainder, 1, NULL AS [CustomerLedgerID], NULL AS [ReconciledDate], NULL AS [CustomerPaymentScheduleID], @WhoCreated, dbo.fn_GetDate_Local(), NULL AS [PurchasedProductPaymentScheduleParentID], 6 /*MTA*/ AS [PurchasedProductPaymentScheduleTypeID], NULL AS [PaymentGroupedIntoID], NULL AS [ContraCustomerLedgerID], pp.[ClientAccountID], @WhoCreated, GETDATE () AS [WhenModified], NULL AS [SourceID], @TransactionFeeRemainder
				FROM PurchasedProduct pp WITH (NOLOCK)
				WHERE pp.PurchasedProductID = @OldPurchasedProductID

				EXEC dbo.Billing__RebuildCustomerPaymentSchedule @AccountID, @Now, @WhoCreated
				
				EXEC Account__SetDateAndAmountOfNextPayment @AccountID	

			END 
			ELSE 
			BEGIN 
					
				/*Insert the adjustment, keep this tracked to the card account*/ 
				INSERT INTO PurchasedProductPaymentSchedule([ClientID], [CustomerID], [AccountID], [PurchasedProductID], [ActualCollectionDate], [CoverFrom], [CoverTo], [PaymentDate], [PaymentNet], [PaymentVAT], [PaymentGross], [PaymentStatusID], [CustomerLedgerID], [ReconciledDate], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [PurchasedProductPaymentScheduleParentID], [PurchasedProductPaymentScheduleTypeID], [PaymentGroupedIntoID], [ContraCustomerLedgerID], [ClientAccountID], [WhoModified], [WhenModified], [SourceID], TransactionFee)
				SELECT @ClientID, @CustomerID, @NewAccountID, @OldPurchasedProductID, @Now, [ValidFrom], [ValidTo], @Now, @PremiumNetRemainder, @PremiumTaxRemainder, @PremiumGrossRemainder, 1, NULL AS [CustomerLedgerID], NULL AS [ReconciledDate], NULL AS [CustomerPaymentScheduleID], @WhoCreated, dbo.fn_GetDate_Local(), NULL AS [PurchasedProductPaymentScheduleParentID], 6 /*MTA*/ AS [PurchasedProductPaymentScheduleTypeID], NULL AS [PaymentGroupedIntoID], NULL AS [ContraCustomerLedgerID], pp.[ClientAccountID], @WhoCreated, GETDATE () AS [WhenModified], NULL AS [SourceID], @TransactionFeeRemainder
				FROM PurchasedProduct pp WITH (NOLOCK)
				WHERE pp.PurchasedProductID = @OldPurchasedProductID

				EXEC dbo.Billing__RebuildCustomerPaymentSchedule @NewAccountID, @Now, @WhoCreated

				EXEC Account__SetDateAndAmountOfNextPayment @NewAccountID	

			END 

			/*JEL 2018-10-19 don't create a CTID if it's a refund*/ 
			IF @PremiumGrossRemainder > 0 
			BEGIN
				/*Copy the existing record so we have the customer and amount..*/
				INSERT INTO CardTransaction (ClientID, CustomerID, Amount, WhenCreated, CustomerPaymentScheduleID, ClientPaymentGatewayID, MerchantCode,[Description],ObjectID, ObjectTypeID, AccountID )
				SELECT @ClientID, @CustomerID, @PremiumGrossRemainder, dbo.fn_GetDate_Local(), NULL, @PaymentGatewayID, @MerchantCode,'Renewal Stub',@MatterID, 2 ,@NewAccountID
			
				SELECT @NewCardTransactionID = SCOPE_IDENTITY()
			
				INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross,  DateReconciled, WhoCreated, WhenCreated, WhoModified, WhenModified,PaymentTypeID, CardTransactionID)
				VALUES (@ClientID, @CustomerID, @ActualCollectionDate, 'Card Prepayment', CAST(@NewCardTransactionID AS VARCHAR), @PremiumNetRemainder, @PremiumTaxRemainder, @PremiumGrossRemainder, NULL, @WhoCreated, @Now, @WhoCreated, @Now, 7, @NewCardTransactionID)		
		 
				SELECT  @PaymentID = SCOPE_IDENTITY()	

				/* 2020-08-18 Not used and set above
				SELECT @Firstname = c.FirstName, 
						@Lastname = c.LastName, 
						@ValueDate = c.DateOfBirth 
				FROM Customers c WITH (NOLOCK)
				WHERE c.CustomerID = @CustomerID 
				*/

				/*GPR 2019-10-31 removed*/
				--/*drop these records into the Eckoh tables so we can verify them*/ 
				--EXEC _ECKOH_CreateSessionTransaction @NewCardTransactionID,@Firstname, @Lastname, @ValueDate , @PremiumGrossRemainder

				/*We will need this if/when it is successful so we can move the process on..*/
				INSERT INTO CardTransactionPolicy (CardTransactionID, PAMatterID, WhoCreated, WhenCreated, PolicyPaymentAmount)
				SELECT @NewCardTransactionID, @MatterID, @WhoCreated, dbo.fn_GetDate_Local(), @PremiumGrossRemainder

				EXEC _C00_SimpleValueIntoField 180188, @NewCardTransactionID, @MatterID 
				EXEC _C00_SimpleValueIntoField 180211, @PremiumGrossRemainder, @MatterID 

			END
							
			SELECT @Comments = @Comments + CHAR(13) + CHAR(10) 
				+ 'PP ID: ' + CONVERT(VARCHAR(2000), @OldPurchasedProductID) 
				+ '. CT ID: ' + CONVERT(VARCHAR(2000), @NewCardTransactionID) 
				+ '. Payment ID: ' + CONVERT(VARCHAR(2000), @PaymentID) 
				+ '. Customer Ledger ID: ' + CONVERT(VARCHAR(2000), @CustomerLedgerID)

		END
		ELSE
		BEGIN

			SELECT @Comments = @Comments + CHAR(13) + CHAR(10) + 'PP has not been paid for'

			/*Ok... we have a schedule but it hasnt been paid off. Lets create an updated schedule*/
			UPDATE pp
			SET ProductCostNet = @PremiumNet, ProductCostVAT = @PremiumTax, ProductCostGross = @PremiumGross
			FROM PurchasedProduct pp
			WHERE pp.PurchasedProductID = @OldPurchasedProductID

			DELETE ppps
			FROM PurchasedProductPaymentSchedule ppps
			WHERE ppps.PurchasedProductID = @OldPurchasedProductID
			AND ppps.PaymentStatusID = 1 /*We shouldnt need this but lets keep it just in case..*/

			EXEC PurchasedProduct__CreatePaymentSchedule @OldPurchasedProductID	

			EXEC Account__SetDateAndAmountOfNextPayment @AccountID	

			/*We will have already created a Card Account in the first pass throuh renewal, need to pick this up*/ 
			SELECT @NewAccountID = ct.AccountID FROM CardTransaction ct with (NOLOCK) 
			WHERE ct.ObjectID = @MatterID 
			AND ct.ParentID = @OldPurchasedProductID 

			/*JEL 2018-10-19 dont create a CTID if refund*/ 
			IF @PremiumGross > 0 
			BEGIN 
				/*Copy the existing record so we have the customer and amount..*/
				INSERT INTO CardTransaction (ClientID, CustomerID, Amount, WhenCreated, CustomerPaymentScheduleID, ClientPaymentGatewayID, MerchantCode,[Description],ObjectID, ObjectTypeID, ParentID ,AccountID)
				SELECT @ClientID, @CustomerID, @PremiumGross, dbo.fn_GetDate_Local(), NULL, @PaymentGatewayID, @MerchantCode,'Renewal Stub',@MatterID, 2, @OldPurchasedProductID, @NewAccountID
					
			
				SELECT @NewCardTransactionID = SCOPE_IDENTITY()
			
				INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross,  DateReconciled, WhoCreated, WhenCreated, WhoModified, WhenModified,PaymentTypeID, CardTransactionID)
				VALUES (@ClientID, @CustomerID, @ActualCollectionDate, 'Card Prepayment', CAST(@NewCardTransactionID AS VARCHAR), @PremiumNet, @PremiumTax, @PremiumGross, NULL, @WhoCreated, @Now, @WhoCreated, @Now, 7, @NewCardTransactionID)		
		 
				SELECT  @PaymentID = SCOPE_IDENTITY()	

				/* 2020-08-18 Not used and set above
				SELECT @Firstname = c.FirstName, @Lastname = c.LastName, @ValueDate = c.DateOfBirth 
				FROM Customers c WITH (NOLOCK)
				WHERE c.CustomerID = @CustomerID 
				*/

				/*GPR 2019-10-31 removed*/
				--/*drop these records into the Eckoh tables so we can verify them*/ 
				--EXEC _ECKOH_CreateSessionTransaction @NewCardTransactionID,@Firstname, @Lastname, @ValueDate , @PaymentGross

				/*We will need this if/when it is successful so we can move the process on..*/
				INSERT INTO CardTransactionPolicy (CardTransactionID, PAMatterID, WhoCreated, WhenCreated, PolicyPaymentAmount)
				SELECT @NewCardTransactionID, @MatterID, @WhoCreated, dbo.fn_GetDate_Local(), @PaymentGross

				EXEC _C00_SimpleValueIntoField 180188, @NewCardTransactionID, @MatterID 
				EXEC _C00_SimpleValueIntoField 180211, @PaymentGross, @MatterID 

			END

			SELECT @Comments = @Comments + CHAR(13) + CHAR(10) 
				+ 'PP ID: ' + CONVERT(VARCHAR(2000), @OldPurchasedProductID) 
				+ '. CT ID: ' + CONVERT(VARCHAR(2000), @NewCardTransactionID) 
				+ '. Payment ID: ' + CONVERT(VARCHAR(2000), @PaymentID) 

			END

		END

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_158847] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Renewal_158847] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_158847] TO [sp_executeall]
GO
