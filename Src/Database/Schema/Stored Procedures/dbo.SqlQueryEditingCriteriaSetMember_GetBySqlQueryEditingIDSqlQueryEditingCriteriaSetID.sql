SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteriaSetMember table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingIDSqlQueryEditingCriteriaSetID]
(

	@SqlQueryEditingID int   ,

	@SqlQueryEditingCriteriaSetID int   
)
AS


				SELECT
					[SqlQueryEditingCriteriaSetMemberID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingCriteriaSetID],
					[SqlQueryEditingColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryEditingCriteriaSetMember] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingID] = @SqlQueryEditingID
					AND [SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingIDSqlQueryEditingCriteriaSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingIDSqlQueryEditingCriteriaSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSetMember_GetBySqlQueryEditingIDSqlQueryEditingCriteriaSetID] TO [sp_executeall]
GO
