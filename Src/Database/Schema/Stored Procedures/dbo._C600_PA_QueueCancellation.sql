SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 06/10/2016
-- Description:	Sets the third party fields for billing cancellation
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_QueueCancellation]
(
	@LeadEventID INT,
	@MatterID INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE 
			@CancellationDate DATE,
			@CaseID INT,
			@ClientID INT,
			@DetailValue VARCHAR(200), 
			@LeadTypeID INT,
			@ObjectID INT,
			@PurchasedProductID INT

	
	SELECT @LeadTypeID=l.LeadTypeID, @ClientID=l.ClientID, @CaseID=m.CaseID
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=m.LeadID
	WHERE MatterID=@MatterID
	
	-- use override if set
	SELECT @CancellationDate=CASE WHEN dbo.fnGetDv (175452,@CaseID)='' 
					THEN dbo.fnGetDvAsDate (170055,@CaseID) 
					ELSE dbo.fnGetDvAsDate (175452,@CaseID) END,
			@PurchasedProductID=dbo.fnGetDvAsInt (177074,@CaseID)		
	
	---- clear helper or update fields
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4397)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID  -- description
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4401)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @CancellationDate, @MatterID  -- cancellation date
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4410)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, 74329, @MatterID  -- mta payment method
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4413)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PurchasedProductID, @MatterID  -- purchase product ID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueCancellation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_QueueCancellation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueCancellation] TO [sp_executeall]
GO
