SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-08-05
-- Description:	SP to output tables into html
-- JWG 2011-01-11 Added @Client/ClientPersonnel/ContactIDForDV and reformatted sql.
-- ACE 2014-09-09 Added _C00_RTFFromSQL
-- =============================================

CREATE PROCEDURE [dbo].[_C00_RTFFromTable]
	@CustomerID INT = NULL,
	@LeadID INT, 
	@CaseID INT = NULL,
	@MatterID INT, 
	@TableDetailFieldID INT, 
	@ColList VARCHAR(1000),
	@TableProperties VARCHAR(250) = 'border="0" cellpadding="3" width="90%" align="center"',
	@Header BIT = 1,
	@WhereClause VARCHAR(MAX) = NULL, 
	@ClientIDForDV INT = NULL, 
	@ClientPersonnelIDForDV INT = NULL, 
	@ContactIDForDV INT = NULL,
	@PrintSql BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	/* Construct HTML Table as system doesnt provide functionality to do so. */
	DECLARE @FieldstoKeep TABLE(
		SortedOrder INT IDENTITY (1,1),
		DetailFieldID INT
	)

	DECLARE @TableRowsKeep TABLE(
		TableRowID INT
	)

	DECLARE @RTF VARCHAR(MAX), 
			@TableRowID INT, 
			@TestID INT, 
			@ResourceListID INT, 
			@TableDetailFieldPageID INT, 
			@ResourceListDetailFieldPageID INT,
			@ResourceListFieldOrder INT = 0,
			@RLFirstIndex INT = 0,
			@RLLastIndex INT = 0,
			@ColWidth INT,
			@ColWidth2 INT,
			@CellWidths VARCHAR(MAX),
			@SQLQuery VARCHAR(MAX),
			@Alignment VARCHAR(5) = ''

	SELECT @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableDetailFieldID

	IF @CustomerID = 10102334 AND @TableDetailFieldID = 287659 /* Basic Holder Customer Table */
	BEGIN
		/*
		CS 2015-03-11.  Proof of concept for C344
		Share document templates across all Irish clients
		For table fields, use a Client-0 basic holder field, and put the Alias in the TableProperties section
		e.g. [!Q287659:1,2,3:'SecuredCreditorTable':Header=1:]
		Will return DetailFieldID 262700
		*/
		SELECT @TableDetailFieldID = fa.DetailFieldID
		FROM Customers cu WITH ( NOLOCK ) 
		INNER JOIN DetailFieldAlias fa WITH (NOLOCK) on fa.ClientID = cu.ClientID
		WHERE cu.CustomerID = @CustomerID
		AND fa.DetailFieldAlias = @TableProperties
	END

	IF EXISTS (
		SELECT *
		FROM CustomTableSQL c
		WHERE c.DetailFieldID = @TableDetailFieldID
	)
	BEGIN
		PRINT 'CustomTableSQL'
		EXEC dbo._C00_RTFFromSQL @CustomerID, @LeadID, @CaseID, @MatterID, @TableDetailFieldID, @ClientIDForDV, @ClientPersonnelIDForDV, @ContactIDForDV, @PrintSQL, @Header
		RETURN /*CS 2015-08-25 Return so we don't get two datasets*/
		
	END
	ELSE	
	BEGIN

		SELECT @ResourceListDetailFieldPageID = df.ResourceListDetailFieldPageID,
				@ResourceListFieldOrder=df.FieldOrder
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldPageID = @TableDetailFieldPageID
		AND df.QuestionTypeID = 14

		IF @TableProperties = 'Q1' 
		BEGIN
			SELECT @WhereClause = @TableProperties
		END

		IF @WhereClause IS NULL OR @WhereClause = ''
		BEGIN

			/* Matter Table*/
			IF @MatterID > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.MatterID = @MatterID 
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			ELSE
			/* Case Table*/
			IF @CaseID > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.CaseID = @CaseID
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			ELSE
			/* Lead Table*/
			IF @LeadID > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.LeadID = @LeadID
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			ELSE
			/* Customer Table*/
			IF @CustomerID > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.CustomerID = @CustomerID
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			ELSE
			/* Client Table*/
			IF @ClientIDForDV > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.ClientID = @ClientIDForDV
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			ELSE
			/* ClientPersonnel Table*/
			IF @ClientPersonnelIDForDV > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.ClientPersonnelID = @ClientPersonnelIDForDV
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			ELSE
			
			/* Contact Table*/
			IF @ContactIDForDV > 0
			BEGIN

				INSERT INTO @TableRowsKeep (TableRowID)
				SELECT tr.TableRowID 
				FROM dbo.TableRows tr WITH (NOLOCK)
				WHERE tr.ContactID = @ContactIDForDV
				AND tr.DetailFieldID = @TableDetailFieldID
				ORDER BY tr.TableRowID
			
			END
			
		END
		ELSE
		BEGIN
			
			SELECT @WhereClause = CASE @WhereClause 
			WHEN 'ECT1' /* Only output items WHERE the "PRINT Item" has been ticked */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 136570 AND tdv.ValueInt = 1'
			WHEN 'Q1' /* Only output items WHERE the "Print Item" has been ticked */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 111435 and tdv.DetailValue = ''true'''
			WHEN 'Q2' 
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 114684 
				INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 114693 AND tdv1.ValueDate = dbo.fnDateOnlyChar (dbo.fn_GetDate_Local()) 
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv.MatterID AND mdv.DetailFieldID = 113507
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt AND rldv.detailfieldid = 113500 AND tdv.ValueMoney > rldv.ValueMoney
				'
			WHEN 'Q3' 
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 114684 
				INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 114693 AND tdv1.ValueDate < dbo.fnDateOnlyChar (dbo.fn_GetDate_Local()) 
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv.MatterID AND mdv.DetailFieldID = 113507
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt AND rldv.detailfieldid = 113500 AND tdv.ValueMoney > rldv.ValueMoney
				'
			WHEN 'Q4' /* Invoicing Table only show resords for today */
				THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 116281 and tdv_date.ValueDate = convert(date,dbo.fn_GetDate_Local())'
			WHEN 'Q5' /* Invoicing Table only show resords for today */
				THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 114693 and tdv_date.ValueDate = convert(date,dbo.fn_GetDate_Local()-1)'
			WHEN 'Q6' /* Invoicing Table only show resords for today */
				THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 116751 and tdv_date.ValueDate = convert(date,dbo.fn_GetDate_Local()-1)'
			WHEN 'Q7' /* Include in statement of facts if selected (CMU Line Items) */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 115597 and tdv.DetailValue = ''true'''
			WHEN 'Q8' /* Include in statement of facts if selected (LA Invoicing Table) */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 117271 and tdv.DetailValue = ''true'''
			WHEN 'C185-1' /* Include invoice line if due and type referral */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 119853 and tdv.ValueDate is not null
				INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 119857 and tdv1.ValueDate is null
				INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 119851 and tdv2.ValueInt = 24871
				'
			WHEN 'C185-2' /* Include invoice line if due and type settlement */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 119853 and tdv.ValueDate is not null 
				INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 119857 and tdv1.ValueDate is null
				INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 119851 and tdv2.ValueInt = 24872
				'
			WHEN 'C184-1' /* Include invoice line if due & referral or settlement LEGAL */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 118959 and tdv.ValueDate <= convert(varchar(10),dbo.fn_GetDate_Local(),120)
				INNER JOIN TableDetailValues tdv4 WITH (NOLOCK) ON tdv4.TableRowID = tr.TableRowID and tdv4.DetailFieldID = 119562 and tdv4.ValueDate is null
				INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 118995 and tdv2.ValueInt in (24577,24578)
				INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tr.TableRowID and tdv3.DetailFieldID = 122986 and tdv3.ValueInt = 24473
				'
			WHEN 'C184-2' /* Include invoice line if due and type service */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 118959 and (tdv.ValueDate <= convert(varchar(10),dbo.fn_GetDate_Local(),120) and tdv.ValueDate > DATEADD(DAY,-7,dbo.fn_GetDate_Local()))
				INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 118995 and tdv2.ValueInt = 25674
				'
			WHEN 'C184-3' /* Include invoice line if due & referral or settlement RECLAIM */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 118959  and tdv.ValueDate <= convert(varchar(10),dbo.fn_GetDate_Local(),120)
				INNER JOIN TableDetailValues tdv4 WITH (NOLOCK) ON tdv4.TableRowID = tr.TableRowID and tdv4.DetailFieldID = 119562 and tdv4.ValueDate is null
				INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 118995 and tdv2.ValueInt in (24577,24578)
				INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tr.TableRowID and tdv3.DetailFieldID = 122986 and tdv3.ValueInt = 24472
				'
			WHEN 'VHM1' /* Include in statement of facts if selected (CMU Line Items) */
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 120454 and tdv.ValueDate IS NOT NULL
				INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 120455 and tdv1.ValueDate IS NULL'
			WHEN 'C126-1' /* Show payments made last month */
				THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 112791 and DATEDIFF(mm,tdv_date.ValueDate,dbo.fn_GetDate_Local()) = 1'
			WHEN 'A1' /* Aims - Only records from this invoice */
				THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 129546
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv_invno.MatterID and mdv.DetailFieldID = 129894 and mdv.ValueInt = tdv_invno.ValueInt'
			WHEN 'C210-1' /* CDC CDC invoice lines for current invoice number */
				THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 135826
				INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = tdv_invno.LeadID and ldv.DetailFieldID = 135857 and ldv.ValueInt = tdv_invno.ValueInt'
			WHEN 'C210-2' /* CDC SC invoice lines for current invoice number */
				THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 135422
				INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = tdv_invno.LeadID and ldv.DetailFieldID = 135857 and ldv.ValueInt = tdv_invno.ValueInt'
			WHEN 'C1' /* Only return Passengers */
				THEN 'INNER JOIN TableDetailValues tdv_pass WITH (NOLOCK) on tdv_pass.TableRowID = tr.TableRowID and tdv_pass.DetailFieldID = 140051 and tdv_pass.detailvalue = ''Passenger'''
			WHEN 'C2' /* CDC SC invoice lines for current invoice number */
				THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 141416
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv_invno.MatterID and mdv.DetailFieldID = 141432 and mdv.ValueInt = tdv_invno.ValueInt'
			WHEN 'TR1' /* CDC SC invoice lines for current invoice number */
				THEN 'INNER JOIN TableDetailValues tdv_dsardate WITH (NOLOCK) on tdv_dsardate.TableRowID = tr.TableRowID and tdv_dsardate.DetailFieldID = 130985
				INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv_dsardate.MatterID and mdv.DetailFieldID = 141881 and mdv.ValueDate = tdv_dsardate.ValueDate'
			WHEN 'C211-1' /*Ecclesiastical show only invoices with "print on statment" ticked*/
				THEN 'INNER JOIN TableDetailValues tdv_print WITH (NOLOCK) ON tdv_print.TableRowID = tr.TableRowID and tdv_print.DetailFieldID = 146410 and tdv_print.ValueInt = 1'
			WHEN 'C211-2' /*Ecclesiastical only pull rows from the most recent invoice*/
				THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 136822
				INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 147020 and tdv_invoice.ValueInt = mdv_active.ValueInt'
			WHEN 'Q211-2' /*Ecclesiastical only pull rows from the most recent invoice - ACE 2012-03-05 Julie got the wrong code*/
				THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 136822
				INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 147020 and tdv_invoice.ValueInt = mdv_active.ValueInt'
			WHEN 'C173-1' /*Ecclesiastical show only invoices with "print on statment" ticked*/
				THEN 'INNER JOIN TableDetailValues tdv_print WITH (NOLOCK) ON tdv_print.TableRowID = tr.TableRowID and tdv_print.DetailFieldID = 152700 and tdv_print.ValueInt = 1'
			WHEN 'C173-2' /*Ecclesiastical only pull rows from the most recent invoice*/
				THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 152645
				INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 152664 and tdv_invoice.ValueInt = mdv_active.ValueInt'
			WHEN 'Q173-2' /*Ecclesiastical only pull rows from the most recent invoice - ACE 2012-03-05 Julie got the wrong code*/
				THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 152645
				INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 152664 and tdv_invoice.ValueInt = mdv_active.ValueInt'
			WHEN 'HR1' /*Chorus HR only show rows with Call Witness tickbox ticked*/
				THEN 'Inner join TableDetailValues tdv WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tdv.DetailFieldID = 152280 and tdv.ValueInt = 1'
			WHEN 'C158-C'
				THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tdv.DetailFieldID = 157175 and tdv.Detailvalue = ''Client Fee'' 
						AND NOT EXISTS (SELECT * FROM TableDetailValues tdv2 WITH (NOLOCK)
						WHERE tdv2.MatterID = tdv.MatterID 
						and tdv2.DetailFieldID = 157175 
						and tdv2.Detailvalue = ''Client Fee'' 
						and tdv2.TableRowID > tdv.TableRowID
						)'
			WHEN 'C235-1' /* RSA - Wellness services for Summary of Coverage */
				THEN 'INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON tr.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 164734
					  INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON tr.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 164735
					  INNER JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON tr.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 156066 AND tdvFrom.ValueDate = mdvStart.ValueDate
					  INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON tr.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 156067 AND tdvTo.ValueDate = mdvEnd.ValueDate'
			WHEN 'C218-2' /*Only pull rows from the most recent invoice*/
				THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 165980
				INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 165968 and tdv_invoice.ValueInt = mdv_active.ValueInt'
			WHEN 'C307-Priority'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246'
			WHEN 'C307-Urgent'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246 AND tdvCategory.ValueInt = 115604'
			WHEN 'C307-P1'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246 AND tdvCategory.ValueInt = 115605'
			WHEN 'C307-P2'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246 AND tdvCategory.ValueInt = 115606'
			WHEN 'C307-P3'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246 AND tdvCategory.ValueInt = 115607'
			WHEN 'C307-P4'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246 AND tdvCategory.ValueInt = 115608'
			WHEN 'C307-P5'
				THEN 'INNER JOIN TableDetailValues tdvCategory WITH (NOLOCK) on tdvCategory.TableRowID = tr.TableRowID and tdvCategory.DetailFieldID = 208246 AND tdvCategory.ValueInt = 115609'				
			ELSE ''
			END
		
			SELECT @SQLQuery = 'SELECT tr.TableRowID 
			FROM tablerows tr WITH (NOLOCK)
			' + @WhereClause + CASE 
									WHEN @MatterID > 0 THEN ' WHERE tr.MatterID = ' + CONVERT(VARCHAR, @MatterID)   
									WHEN @CaseID > 0 THEN ' WHERE tr.CaseID = ' + CONVERT(VARCHAR, @CaseID) 
									WHEN @LeadID > 0 THEN ' WHERE tr.LeadID = ' + CONVERT(VARCHAR, @LeadID) 
									WHEN @CustomerID > 0 THEN ' WHERE tr.CustomerID = ' + CONVERT(VARCHAR, @CustomerID) 
									WHEN @ClientIDForDV > 0 THEN ' WHERE tr.ClientID = ' + CONVERT(VARCHAR, @ClientIDForDV) 
									WHEN @ClientPersonnelIDForDV > 0 THEN ' WHERE tr.ClientPersonnelID = ' + CONVERT(VARCHAR, @ClientPersonnelIDForDV) 
									WHEN @ContactIDForDV > 0 THEN ' WHERE tr.ContactID = ' + CONVERT(VARCHAR, @ContactIDForDV) 
							   END  
			+ ' 
			And tr.DetailFieldID = ' + CONVERT(VARCHAR,@TableDetailFieldID)
			+	CASE 
					WHEN @WhereClause = 'C307-Priority' THEN 'ORDER BY tdvCategory.ValueInt, tr.TableRowID'
					ELSE 'ORDER BY tr.TableRowID'
				END
	
			INSERT INTO @TableRowsKeep (TableRowID)
			EXEC (@SQLQuery)
			
		END

		IF EXISTS (SELECT * FROM @TableRowsKeep) OR (@Header = 1)
		BEGIN

			INSERT INTO @FieldstoKeep (DetailFieldID)
			SELECT DetailFieldID
			FROM dbo.DetailFields df WITH (NOLOCK) 
			WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
			AND df.[Enabled] = 1
			AND df.QuestionTypeID <> 14
			AND df.FieldOrder < @ResourceListFieldOrder
			ORDER BY df.FieldOrder

			INSERT INTO @FieldstoKeep (DetailFieldID)
			SELECT df.DetailFieldID
			FROM dbo.DetailFields df WITH (NOLOCK) 
			WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
			AND df.[Enabled] = 1
			ORDER BY df.FieldOrder

			INSERT INTO @FieldstoKeep (DetailFieldID)
			SELECT DetailFieldID
			FROM dbo.DetailFields df WITH (NOLOCK) 
			WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
			AND df.[Enabled] = 1
			AND df.QuestionTypeID <> 14
			AND df.FieldOrder > @ResourceListFieldOrder
			ORDER BY df.FieldOrder

			DELETE ftk
			FROM @FieldstoKeep ftk
			WHERE NOT EXISTS (
				SELECT *
				FROM dbo.fnTableOfIDsFromCSV (@ColList) tid 
				WHERE tid.AnyID = ftk.SortedOrder
			)

			SELECT @RLLastIndex=ISNULL(MAX(ftk.SortedOrder),0),@RLFirstIndex=ISNULL(MIN(ftk.SortedOrder),0)
			FROM @FieldstoKeep ftk
			INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = ftk.DetailFieldID
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID AND df.DetailFieldPageID=@ResourceListDetailFieldPageID 

			SELECT @ColWidth = CASE @TableProperties WHEN 'ECT1' THEN 9900 
													 WHEN 'Q7' THEN 10560 
													 WHEN 'Q8' THEN 9000 
													 WHEN 'C126-1' THEN 9000
													 WHEN 'L210' THEN 14000 
													 WHEN 'C304_Inv' THEN 10500
													 WHEN 'ISI_Rep' THEN 9275
													 WHEN 'Narrow' THEN 6900
													 ELSE 8100 END/(SELECT COUNT(*) FROM @FieldstoKeep)
			
			SELECT @ColWidth2 = @ColWidth

			SELECT @RTF = '{\trowd\trhdr\trgaph30\trleft0\trrh262 '

			SELECT @CellWidths = ''

			SELECT @CellWidths = @CellWidths + CASE @TableProperties WHEN 'Q7' THEN '\clbrdrt\brdrs\clbrdrl\brdrs\clbrdrb\brdrs\clbrdrr\brdrs ' 
																	 WHEN 'L210' THEN '\clbrdrt\brdrs\clbrdrl\brdrs\clbrdrb\brdrs\clbrdrr\brdrs '
																	 WHEN 'P210' THEN '\clbrdrt\brdrs\clbrdrl\brdrs\clbrdrb\brdrs\clbrdrr\brdrs '
																	 --WHEN 'border=1' THEN '\clbrdrt\brdrs\clbrdrl\brdrs\clbrdrb\brdrs\clbrdrr\brdrs '
																	 WHEN 'border=1' THEN '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10 '
																	 WHEN 'C304_Inv' THEN '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\qc '
																	 WHEN 'ISI_Rep' THEN  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\qc '
																	 ELSE '' END + '\cellx' 
											+  CASE df.DetailFieldID 
												WHEN 218495 THEN CONVERT(VARCHAR(100),@ColWidth+@ColWidth2) /*ISI Proof of Concept*/
												ELSE CONVERT(VARCHAR(100),@ColWidth)
												END 
				, @ColWidth = @ColWidth + @ColWidth2 + ' ' + CASE df.DetailFieldID WHEN 218495 /*C325 - Repayment Table - Type*/ THEN @ColWidth2 ELSE 0 END 
			FROM @FieldstoKeep ftk
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID
				
			IF @Header = 1
			BEGIN

				SELECT @RTF = @RTF + @CellWidths

				SELECT @RTF = @RTF + CASE  WHEN @TableProperties IN('C304_Inv','ISI_Rep')
											THEN '\pard\intbl\qc\b ' +  CONVERT(VARCHAR(100),df.FieldCaption) + ' \b0\cell ' /*Centre the column header*/
											ELSE '\pard\intbl\ql\b ' +  CONVERT(VARCHAR(100),df.FieldCaption) + ' \b0\cell '
											END 
				FROM @FieldstoKeep ftk
				INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID

				SELECT @RTF = @RTF + '\pard\intbl\row '

			END

			--SELECT @RTF = @RTF + ' '
			
			SELECT @Alignment = CASE WHEN @TableProperties = 'C304_Inv' THEN '\qc' ELSE @Alignment END 

			SELECT @TestID = 0

			SELECT TOP 1 @TableRowID = TableRowID, @TestID = 0
			FROM @TableRowsKeep tr
			ORDER BY TableRowID
			
			WHILE (@TableRowID IS NOT NULL AND @TableRowID > @TestID)
			BEGIN

				SELECT @RTF = @RTF + '\trowd\trgaph30\trleft0\trrh262 ' + @CellWidths

				SELECT TOP 1 @ResourceListID = tdv.ResourceListID 
				FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
				WHERE tdv.TableRowID = @TableRowID
				AND tdv.ResourceListID IS NOT NULL

				SELECT @RTF = @RTF + '\pard\intbl' + @Alignment + ' ' + CASE WHEN df.QuestionTypeID IN (2,4) THEN ISNULL(luli.ItemValue, '') ELSE ISNULL(REPLACE(tdv.DetailValue, CHAR(13) + CHAR(10),' \line '), '') END + '\cell'
				FROM @FieldstoKeep ftk
				INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID AND df.LeadOrMatter IN (6,8)
				LEFT JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = ftk.DetailFieldID AND tdv.TableRowID = @TableRowID
				LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv.ValueInt AND luli.LookupListID = df.LookupListID
				WHERE ftk.SortedOrder < @RLFirstIndex
				ORDER BY ftk.SortedOrder

				SELECT @RTF = @RTF + '\pard\intbl' + @Alignment + ' ' + CASE WHEN df.QuestionTypeID IN (2,4) THEN ISNULL(luli.ItemValue, '') ELSE ISNULL(REPLACE(rldv.DetailValue, CHAR(13) + CHAR(10),' \line '), '') END + '\cell'
				FROM @FieldstoKeep ftk
				INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = ftk.DetailFieldID AND rldv.ResourceListID = @ResourceListID
				INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID 
				LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = rldv.ValueInt AND luli.LookupListID = df.LookupListID
				ORDER BY ftk.SortedOrder

				SELECT @RTF = @RTF + '\pard\intbl' + @Alignment + ' ' + CASE WHEN df.QuestionTypeID IN (2,4) THEN ISNULL(luli.ItemValue, '') ELSE ISNULL(REPLACE(tdv.DetailValue, CHAR(13) + CHAR(10),' \line '), '') END + '\cell'
				FROM @FieldstoKeep ftk
				INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID AND df.LeadOrMatter IN (6,8)
				LEFT JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = ftk.DetailFieldID AND tdv.TableRowID = @TableRowID
				LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv.ValueInt AND luli.LookupListID = df.LookupListID
				WHERE ftk.SortedOrder > @RLLastIndex
				ORDER BY ftk.SortedOrder

				SELECT @TestID = @TableRowID

				SELECT TOP 1 @TableRowID = tr.TableRowID, @TestID = 0
				FROM @TableRowsKeep tr
				WHERE tr.TableRowID > @TableRowID
				ORDER BY tr.TableRowID

				SELECT @RTF = @RTF + '\pard\intbl\row '

			END		

			SELECT @RTF = @RTF + '}'

		END
		ELSE 
		BEGIN
		
			SELECT @RTF = ''
		
		END
	
	END

	SELECT REPLACE(@RTF, '£', '\''a3') AS MyHtml
	
	PRINT @RTF
	
	IF @PrintSql = 1
	BEGIN
	
		PRINT @SQLQuery
	
	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RTFFromTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RTFFromTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RTFFromTable] TO [sp_executeall]
GO
