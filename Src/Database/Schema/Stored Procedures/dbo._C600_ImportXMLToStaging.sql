SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-10-11
-- Description:	Import XML to staging table
-- 2018-09-11	AHOD Set @ObfuscateEmails to false
-- 2018-12-04	AHOD Added Discount_Code to Policy Data
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ImportXMLToStaging]
	@XMLRaw VARCHAR(MAX),
	@FileName VARCHAR(2000)
AS
BEGIN

	/*
	TRUNCATE TABLE [dbo].[_C600_ImportBatch]
	TRUNCATE TABLE [dbo].[_C600_ImportClaims]
	TRUNCATE TABLE [dbo].[_C600_ImportCustomerData]
	TRUNCATE TABLE [dbo].[_C600_ImportPetData]
	TRUNCATE TABLE [dbo].[_C600_ImportPolicy]
	TRUNCATE TABLE [dbo].[_C600_ImportXMLPart1]
	*/

	SET NOCOUNT ON;

	DECLARE  @XML				XML = CONVERT(XML, @XMLRaw)
			,@ContectVarchar	VARCHAR(250)
			,@BatchID			INT
			,@StartDateTime		DATETIME = dbo.fn_GetDate_Local()
			,@ClientID			INT = dbo.fnGetPrimaryClientID()

	SELECT @ContectVarchar = '_C600_ImportXMLToStaging ' + @FileName
	EXEC dbo._C00_LogItXML @ClientID, 1, @ContectVarchar, @XML

	INSERT INTO _C600_ImportBatch ([ImportFileName], [BatchXML], [StartDateTime])
	VALUES (@FileName, @XML, @StartDateTime)

	SELECT @BatchID = SCOPE_IDENTITY()

	INSERT INTO [dbo].[_C600_ImportXMLPart1] ([PolicyXML], FileName, BatchID)
	SELECT	n.c.query('.') AS [ClientXML], @FileName, @BatchID
	FROM @xml.nodes('//Data/Client_Detail') as n(c)

	IF @@RowCount > 0
	BEGIN

		INSERT INTO _C600_ImportCustomerData ([ImportID], [BatchID], [TitleID], [FirstName], [MiddleName], [LastName], [EmailAddress], [DayTimeTelephoneNumber], [HomeTelephone], [MobileTelephone], [Address1], [Address2], [Town], [County], [PostCode], [DateOfBirth], [CountryID], [Senddocumentsby], [CustomerCareEmail], [CustomerCareSMS], [ContactMarketingCommsSMS], [ContactMarketingCommsPhone], [ContactMarketingCommsEmail], [ContactMarketingCommsPost])
		SELECT	i.ImportID, 
				@BatchID AS [BatchID],
				n.c.value('(Customer_Details/TitleID)[1]', 'VARCHAR(2000)') AS [TitleID],
				n.c.value('(Customer_Details/FirstName)[1]', 'VARCHAR(2000)') AS [FirstName],
				n.c.value('(Customer_Details/MiddleName)[1]', 'VARCHAR(2000)') AS [MiddleName],
				n.c.value('(Customer_Details/LastName)[1]', 'VARCHAR(2000)') AS [LastName],
				n.c.value('(Customer_Details/EmailAddress)[1]', 'VARCHAR(2000)') AS [EmailAddress],
				n.c.value('(Customer_Details/DayTimeTelephoneNumber)[1]', 'VARCHAR(2000)') AS [DayTimeTelephoneNumber],
				n.c.value('(Customer_Details/HomeTelephone)[1]', 'VARCHAR(2000)') AS [HomeTelephone],
				n.c.value('(Customer_Details/MobileTelephone)[1]', 'VARCHAR(2000)') AS [MobileTelephone],
				n.c.value('(Customer_Details/Address1)[1]', 'VARCHAR(2000)') AS [Address1],
				n.c.value('(Customer_Details/Address2)[1]', 'VARCHAR(2000)') AS [Address2],
				n.c.value('(Customer_Details/Town)[1]', 'VARCHAR(2000)') AS [Town],
				n.c.value('(Customer_Details/County)[1]', 'VARCHAR(2000)') AS [County],
				n.c.value('(Customer_Details/PostCode)[1]', 'VARCHAR(2000)') AS [PostCode],
				LEFT(n.c.value('(Customer_Details/DateOfBirth)[1]', 'VARCHAR(2000)'), 10) AS [DateOfBirth],
				n.c.value('(Customer_Details/CountryID)[1]', 'VARCHAR(2000)') AS [CountryID],
				n.c.value('(Customer_Details/Senddocumentsby)[1]', 'VARCHAR(2000)') AS [Senddocumentsby],
				n.c.value('(Customer_Details/CustomerCareEmail)[1]', 'VARCHAR(2000)') AS [CustomerCareEmail],
				n.c.value('(Customer_Details/CustomerCareSMS)[1]', 'VARCHAR(2000)') AS [CustomerCareSMS],
				n.c.value('(Customer_Details/ContactMarketingCommsSMS)[1]', 'VARCHAR(2000)') AS [ContactMarketingCommsSMS],
				n.c.value('(Customer_Details/ContactMarketingCommsPhone)[1]', 'VARCHAR(2000)') AS [ContactMarketingCommsPhone],
				n.c.value('(Customer_Details/ContactMarketingCommsEmail)[1]', 'VARCHAR(2000)') AS [ContactMarketingCommsEmail],
				n.c.value('(Customer_Details/ContactMarketingCommsPost)[1]', 'VARCHAR(2000)') AS [ContactMarketingCommsPost]
		FROM _C600_ImportXMLPart1 i
		CROSS APPLY PolicyXML.nodes('//Client_Detail') AS n(c)
		WHERE i.BatchID = @BatchID

		INSERT INTO _C600_ImportPetData ([ImportID], [BatchID], [BreedID], [Pre_ExistingConditionExists], [VaccinationStatus], [SPAYEDNEUTERED], [DatePurchase], [DateLastVaccination], [Microchipnumber], [AnimalName], [DateBirth], [PrimaryColour], [Sex], [PurchasePrice], [Species], [BreedName])
		SELECT i.ImportID, 
				@BatchID AS [BatchID],
				n.c.value('(Pet_Details/BreedID)[1]', 'VARCHAR(2000)') AS [BreedID], 
				n.c.value('(Pet_Details/Pre_ExistingConditionExists)[1]', 'VARCHAR(2000)') AS [Pre_ExistingConditionExists], 
				n.c.value('(Pet_Details/VaccinationStatus)[1]', 'VARCHAR(2000)') AS [VaccinationStatus], 
				n.c.value('(Pet_Details/SPAYEDNEUTERED)[1]', 'VARCHAR(2000)') AS [SPAYEDNEUTERED], 
				n.c.value('(Pet_Details/DatePurchase)[1]', 'VARCHAR(2000)') AS [DatePurchase], 
				n.c.value('(Pet_Details/DateLastVaccination)[1]', 'VARCHAR(2000)') AS [DateLastVaccination], 
				n.c.value('(Pet_Details/Microchipnumber)[1]', 'VARCHAR(2000)') AS [Microchipnumber], 
				n.c.value('(Pet_Details/AnimalName)[1]', 'VARCHAR(2000)') AS [AnimalName], 
				n.c.value('(Pet_Details/DateBirth)[1]', 'VARCHAR(2000)') AS [DateBirth], 
				n.c.value('(Pet_Details/PrimaryColour)[1]', 'VARCHAR(2000)') AS [PrimaryColour], 
				n.c.value('(Pet_Details/Sex)[1]', 'VARCHAR(2000)') AS [Sex], 
				n.c.value('(Pet_Details/PurchasePrice)[1]', 'VARCHAR(2000)') AS [PurchasePrice], 
				n.c.value('(Pet_Details/Species)[1]', 'VARCHAR(2000)') AS [Species], 
				n.c.value('(Pet_Details/BreedName)[1]', 'VARCHAR(2000)') AS [BreedName]
		FROM _C600_ImportXMLPart1 i
		CROSS APPLY PolicyXML.nodes('//Client_Detail') AS n(c)
		WHERE i.BatchID = @BatchID
		
		INSERT INTO _C600_ImportPolicy ([ImportID], [BatchID], [PolicyID], [Inception], [Start], [End],[SalesDate], [Terms_Date], [Affinity], [Product], [Product_Text], [Underwriter], [Policy_Type_Description], [Policy_Excess_Description], [Status], [TPL_Entitlement], [Co_Pay_Rate], [Excess], [Account_Holder_Name], [Bank], [Sort_Code], [Account_Number], [Mandate_Reference], [Preferred_Collection_Date], [Rating_Date_Time], [From], [To], [Event], [Previous_Annual], [Previous_Annual_IPT], [New_Annual_WP], [First_month], [Other_Month], [PremiumCalculationID], [Premium_Detail_Status], [Adjustment_Date], [Adjustment_Amount], [Adjustment_Type], [Adjustment_Application_Date], [Cancellation_Reason], [Discount], [Premiumless_Discount], [TPL_CoIns_Net], [TPL_CoIns_Gross], [TPL_CoIns_IPT], [Adjustment_Admin_Fee], [Sequence_Number], [Written_Premium_Affecting], [Non_Written_Premium_Affecting], [Discount_Code])
		SELECT i.ImportID, 
				@BatchID AS [BatchID],
				n.c.value('(Policy_Details/PolicyID)[1]', 'VARCHAR(2000)') AS [PolicyID], 
				LEFT(n.c.value('(Policy_Details/Inception)[1]', 'VARCHAR(2000)'), 10) AS [Inception], 
				LEFT(n.c.value('(Policy_Details/Start)[1]', 'VARCHAR(2000)'), 10) AS [Start], 
				LEFT(n.c.value('(Policy_Details/End)[1]', 'VARCHAR(2000)'), 10) AS [End], 
				LEFT(n.c.value('(Policy_Details/Sale_Date)[1]', 'VARCHAR(2000)'), 10) AS [SalesDate], 
				n.c.value('(Policy_Details/Terms_Date)[1]', 'VARCHAR(2000)') AS [Terms_Date], 
				n.c.value('(Policy_Details/Affinity)[1]', 'VARCHAR(2000)') AS [Affinity], 
				n.c.value('(Policy_Details/Product)[1]', 'VARCHAR(2000)') AS [Product], 
				n.c.value('(Policy_Details/Product_Text)[1]', 'VARCHAR(2000)') AS [Product_Text], 
				n.c.value('(Policy_Details/Underwriter)[1]', 'VARCHAR(2000)') AS [Underwriter], 
				n.c.value('(Policy_Details/Policy_Type_Description)[1]', 'VARCHAR(2000)') AS [Policy_Type_Description], 
				n.c.value('(Policy_Details/Policy_Excess_Description)[1]', 'VARCHAR(2000)') AS [Policy_Excess_Description], 
				n.c.value('(Policy_Details/Status)[1]', 'VARCHAR(2000)') AS [Status], 
				n.c.value('(Policy_Details/TPL_Entitlement)[1]', 'VARCHAR(2000)') AS [TPL_Entitlement], 
				n.c.value('(Policy_Details/Co_Pay_Rate)[1]', 'VARCHAR(2000)') AS [Co_Pay_Rate], 
				n.c.value('(Policy_Details/Excess)[1]', 'VARCHAR(2000)') AS [Excess], 
				n.c.value('(Policy_Details/Account_Holder_Name)[1]', 'VARCHAR(2000)') AS [Account_Holder_Name], 
				n.c.value('(Policy_Details/Bank)[1]', 'VARCHAR(2000)') AS [Bank], 
				n.c.value('(Policy_Details/Sort_Code)[1]', 'VARCHAR(2000)') AS [Sort_Code], 
				n.c.value('(Policy_Details/Account_Number)[1]', 'VARCHAR(2000)') AS [Account_Number], 
				n.c.value('(Policy_Details/Mandate_Reference)[1]', 'VARCHAR(2000)') AS [Mandate_Reference], 
				n.c.value('(Policy_Details/Preferred_Collection_Date)[1]', 'VARCHAR(2000)') AS [Preferred_Collection_Date], 
				n.c.value('(Policy_Details/Rating_Date_Time)[1]', 'VARCHAR(2000)') AS [Rating_Date_Time], 
				n.c.value('(Policy_Details/From)[1]', 'VARCHAR(2000)') AS [From], 
				n.c.value('(Policy_Details/To)[1]', 'VARCHAR(2000)') AS [To], 
				n.c.value('(Policy_Details/Event)[1]', 'VARCHAR(2000)') AS [Event], 
				n.c.value('(Policy_Details/Previous_Annual)[1]', 'VARCHAR(2000)') AS [Previous_Annual], 
				n.c.value('(Policy_Details/Previous_Annual_IPT)[1]', 'VARCHAR(2000)') AS [Previous_Annual_IPT], 
				n.c.value('(Policy_Details/New_Annual_WP)[1]', 'VARCHAR(2000)') AS [New_Annual_WP], 
				n.c.value('(Policy_Details/First_Month)[1]', 'VARCHAR(2000)') AS [First_month], 
				n.c.value('(Policy_Details/Other_Month)[1]', 'VARCHAR(2000)') AS [Other_Month], 
				n.c.value('(Policy_Details/PremiumCalculationID)[1]', 'VARCHAR(2000)') AS [PremiumCalculationID], 
				n.c.value('(Policy_Details/Premium_Detail_Status)[1]', 'VARCHAR(2000)') AS [Premium_Detail_Status], 
				n.c.value('(Policy_Details/Adjustment_Date)[1]', 'VARCHAR(2000)') AS [Adjustment_Date], 
				n.c.value('(Policy_Details/Adjustment_Amount)[1]', 'VARCHAR(2000)') AS [Adjustment_Amount], 
				n.c.value('(Policy_Details/Adjustment_Type)[1]', 'VARCHAR(2000)') AS [Adjustment_Type], 
				n.c.value('(Policy_Details/Adjustment_Application_Date)[1]', 'VARCHAR(2000)') AS [Adjustment_Application_Date], 
				n.c.value('(Policy_Details/Cancellation_Reason)[1]', 'VARCHAR(2000)') AS [Cancellation_Reason], 
				n.c.value('(Policy_Details/Discount)[1]', 'VARCHAR(2000)') AS [Discount], 
				n.c.value('(Policy_Details/Premiumless_Discount)[1]', 'VARCHAR(2000)') AS [Premiumless_Discount], 
				n.c.value('(Policy_Details/TPL_CoIns_Net)[1]', 'VARCHAR(2000)') AS [TPL_CoIns_Net], 
				n.c.value('(Policy_Details/TPL_CoIns_Gross)[1]', 'VARCHAR(2000)') AS [TPL_CoIns_Gross], 
				n.c.value('(Policy_Details/TPL_CoIns_IPT)[1]', 'VARCHAR(2000)') AS [TPL_CoIns_IPT], 
				n.c.value('(Policy_Details/Adjustment_Admin_Fee)[1]', 'VARCHAR(2000)') AS [Adjustment_Admin_Fee], 
				n.c.value('(Policy_Details/Sequence_Number)[1]', 'VARCHAR(2000)') AS [Sequence_Number], 
				n.c.value('(Policy_Details/Written_Premium_Affecting)[1]', 'VARCHAR(2000)') AS [Written_Premium_Affecting], 
				n.c.value('(Policy_Details/Non_Written_Premium_Affecting)[1]', 'VARCHAR(2000)') AS [Non_Written_Premium_Affecting],
				n.c.value('(Policy_Details/Discount_Code)[1]', 'VARCHAR(2000)') AS [Discount_Code]
		FROM _C600_ImportXMLPart1 i
		CROSS APPLY PolicyXML.nodes('//Client_Detail') AS n(c)
		WHERE i.BatchID = @BatchID

	END
	ELSE
	BEGIN

		INSERT INTO [dbo].[_C600_ImportXMLPart1] ([PolicyXML], FileName, BatchID)
		SELECT	n.c.query('.') AS [ClientXML], @FileName, @BatchID
		FROM @xml.nodes('//Claim/Client_Detail') as n(c)

	END

	INSERT INTO _C600_ImportClaims ([ImportID], [BatchID], [ClaimParentID], [ClaimID], [PolicyID], [FNOL_Description_of_loss], [Claims_Advisor_Description_Of_Loss], [Claim_Type], [Claim_Form_Type], [ClaimStatus], [Body_Part], [Body_Part_Other], [Date_of_Loss], [Time_of_loss_HH_MM], [Treatment_Start], [Treatment_End], [Diet_food_start_date], [Total_Claim_Amount], [Accident_Illness], [AilmentID], [VetID], [Treat_Start], [Treatment_End2], [Policy_Section], [Sub-Section], [Claim_Detail_Data], [User_Deductions], [Settle], [No_Cover], [Excess], [Vol_Excess], [Rebate], [Co_Ins], [Limit], [Total], [Approved], [CoinsurancePercentage], [RemainingLimit], [Date_claim_received], [Date_claim_form_sent], [Date_Third_Party_Claim_Form_Sent], [Date_claim_form_received], [Date_claim_details_entered], [Date_missing_info_requested], [Date_missing_info_received], [Date_claim_approved], [Date_claim_rejected], [Date_payment_revision_requested], [Date_claim_reapproved], [Date_first_settled_approved_or_rejected], [Date_reopened], [Date_payment_confirmed])
	SELECT i.ImportID, 
			@BatchID AS [BatchID],
			n.c.value('(ClaimParentID)[1]', 'VARCHAR(2000)') AS [ClaimParentID], 
			n.c.value('(ClaimID)[1]', 'VARCHAR(2000)') AS [ClaimID], 
			n.c.value('(PolicyID)[1]', 'VARCHAR(2000)') AS [PolicyID], 
			n.c.value('(FNOL_Description_of_loss)[1]', 'VARCHAR(2000)') AS [FNOL_Description_of_loss], 
			n.c.value('(Claims_Advisor_Description_Of_Loss)[1]', 'VARCHAR(2000)') AS [Claims_Advisor_Description_Of_Loss], 
			n.c.value('(Claim_Type)[1]', 'VARCHAR(2000)') AS [Claim_Type], 
			n.c.value('(Claim_Form_Type)[1]', 'VARCHAR(2000)') AS [Claim_Form_Type], 
			n.c.value('(Claims_Status)[1]', 'VARCHAR(2000)') AS [ClaimStatus], 
			n.c.value('(Body_Part)[1]', 'VARCHAR(2000)') AS [Body_Part], 
			n.c.value('(Body_Part_Other)[1]', 'VARCHAR(2000)') AS [Body_Part_Other], 
			LEFT(n.c.value('(Date_of_Loss)[1]', 'VARCHAR(2000)'), 10) AS [Date_of_Loss], 
			n.c.value('(Time_of_loss_HH_MM)[1]', 'VARCHAR(2000)') AS [Time_of_loss_HH_MM], 
			LEFT(n.c.value('(Treatment_Start)[1]', 'VARCHAR(2000)'), 10) AS [Treatment_Start], 
			LEFT(n.c.value('(Treatment_End)[1]', 'VARCHAR(2000)'), 10) AS [Treatment_End], 
			n.c.value('(Diet_food_start_date)[1]', 'VARCHAR(2000)') AS [Diet_food_start_date], 
			n.c.value('(Total_Claim_Amount)[1]', 'VARCHAR(2000)') AS [Total_Claim_Amount], 
			n.c.value('(Accident_Illness)[1]', 'VARCHAR(2000)') AS [Accident_Illness], 
			n.c.value('(AilmentID)[1]', 'VARCHAR(2000)') AS [AilmentID], 
			n.c.value('(VetID)[1]', 'VARCHAR(2000)') AS [VetID], 
			n.c.value('(Treat_Start)[1]', 'VARCHAR(2000)') AS [Treat_Start], 
			n.c.value('(Treatment_End)[2]', 'VARCHAR(2000)') AS [Treatment_End2], 
			n.c.value('(Policy_Section)[1]', 'VARCHAR(2000)') AS [Policy_Section], 
			n.c.value('(Sub-Section)[1]', 'VARCHAR(2000)') AS [Sub-Section], 
			n.c.value('(Claim_Detail_Data)[1]', 'VARCHAR(2000)') AS [Claim_Detail_Data], 
			n.c.value('(User_Deductions)[1]', 'VARCHAR(2000)') AS [User_Deductions], 
			n.c.value('(Settle)[1]', 'VARCHAR(2000)') AS [Settle], 
			n.c.value('(No_Cover)[1]', 'VARCHAR(2000)') AS [No_Cover], 
			n.c.value('(Excess)[1]', 'VARCHAR(2000)') AS [Excess], 
			n.c.value('(Vol_Excess)[1]', 'VARCHAR(2000)') AS [Vol_Excess], 
			n.c.value('(Rebate)[1]', 'VARCHAR(2000)') AS [Rebate], 
			n.c.value('(Co_Ins)[1]', 'VARCHAR(2000)') AS [Co_Ins], 
			n.c.value('(Limit)[1]', 'VARCHAR(2000)') AS [Limit], 
			n.c.value('(Total)[1]', 'VARCHAR(2000)') AS [Total], 
			n.c.value('(Approved)[1]', 'VARCHAR(2000)') AS [Approved], 
			n.c.value('(CoinsurancePercentage)[1]', 'VARCHAR(2000)') AS [CoinsurancePercentage], 
			n.c.value('(RemainingLimit)[1]', 'VARCHAR(2000)') AS [RemainingLimit], 
			n.c.value('(Date_claim_received)[1]', 'VARCHAR(2000)') AS [Date_claim_received], 
			n.c.value('(Date_claim_form_sent)[1]', 'VARCHAR(2000)') AS [Date_claim_form_sent], 
			n.c.value('(Date_Third_Party_Claim_Form_Sent)[1]', 'VARCHAR(2000)') AS [Date_Third_Party_Claim_Form_Sent], 
			n.c.value('(Date_claim_form_received)[1]', 'VARCHAR(2000)') AS [Date_claim_form_received], 
			n.c.value('(Date_claim_details_entered)[1]', 'VARCHAR(2000)') AS [Date_claim_details_entered], 
			n.c.value('(Date_missing_info_requested)[1]', 'VARCHAR(2000)') AS [Date_missing_info_requested], 
			n.c.value('(Date_missing_info_received)[1]', 'VARCHAR(2000)') AS [Date_missing_info_received], 
			n.c.value('(Date_claim_approved)[1]', 'VARCHAR(2000)') AS [Date_claim_approved], 
			n.c.value('(Date_claim_rejected)[1]', 'VARCHAR(2000)') AS [Date_claim_rejected], 
			n.c.value('(Date_payment_revision_requested)[1]', 'VARCHAR(2000)') AS [Date_payment_revision_requested], 
			n.c.value('(Date_claim_reapproved)[1]', 'VARCHAR(2000)') AS [Date_claim_reapproved], 
			n.c.value('(Date_first_settled_approved_or_rejected)[1]', 'VARCHAR(2000)') AS [Date_first_settled_approved_or_rejected], 
			n.c.value('(Date_reopened)[1]', 'VARCHAR(2000)') AS [Date_reopened], 
			n.c.value('(Date_payment_confirmed)[1]', 'VARCHAR(2000)') AS [Date_payment_confirmed]
	FROM _C600_ImportXMLPart1 i
	CROSS APPLY PolicyXML.nodes('//Client_Detail/Claim_Details') AS n(c)
	WHERE i.BatchID = @BatchID

	EXEC _C600_FullImport @BatchID, 1 /*AHOD 2018-11-09 Updated from 1*/

	SELECT *
	FROM _C600_ImportBatch ib 
	WHERE ib.ImportBatchID = @BatchID

	UPDATE ats
	SET NextRunDateTime = dbo.fn_GetDate_Local()
	FROM AutomatedTask ats
	WHERE ats.TaskID = 22890

	RETURN @BatchID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportXMLToStaging] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ImportXMLToStaging] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportXMLToStaging] TO [sp_executeall]
GO
