SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/****** Object:  Stored Procedure dbo.EventType_GetByClientIDFull    Script Date: 08/09/2006 12:22:50 ******/

/*
----------------------------------------------------------------------------------------------------
-- Date Created: 11 September 2006

-- Created By:  Jim Green
-- Purpose: Select EventType records, joined to all description tables
-- MODIFIED: 2014-07-22	SB	Updated to use function for shared lead type
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_GetByClientIDFull]
(

	@ClientID int   , 
	@LeadType int 
)
AS

SELECT
	EventTypeID,
	let.ClientID,
	EventTypeName,
	EventTypeDescription,
	Enabled,
	UnitsOfEffort,
	AllowCustomTimeUnits,
	FollowupTimeUnitsID as FollowupTimeUnits,
	FollowupQuantity,
	AvailableManually,
	StatusAfterEvent,
	AquariumEventAfterEvent,
	let.EventSubtypeID, 
	IsNull(convert(varchar,FollowupQuantity),'') + ' ' + IsNull(tu1.TimeUnitsName,'') as FollowupTimeUnitsName,
	IsNull(ls.StatusName,'') as StatusName,
	IsNull(aet.AquariumEventTypeName,'') as AquariumEventTypeName,
	IsNull(es.EventSubtypeName,'') as EventSubtypeName,
    DocumentTypeID,
	let.LeadTypeID,
	let.InProcess 
FROM
	fnEventTypeShared(@ClientID) let
	LEFT OUTER JOIN TimeUnits tu1 ON let.FollowupTimeUnitsID = tu1.TimeUnitsID
	LEFT OUTER JOIN LeadStatus ls ON let.StatusAfterEvent = ls.StatusID
	LEFT OUTER JOIN AquariumEventType aet ON let.AquariumEventAfterEvent = aet.AquariumEventTypeID
	LEFT OUTER JOIN EventSubtype es ON let.EventSubtypeID = es.EventSubtypeID
WHERE          
	let.ClientID = @ClientID 
AND	IsNull(let.LeadTypeID,@LeadType) = @LeadType 
ORDER BY 
	let.LeadTypeID, let.EventTypeName
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetByClientIDFull] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_GetByClientIDFull] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetByClientIDFull] TO [sp_executeall]
GO
