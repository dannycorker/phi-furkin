SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-01-21
-- Description:	Show current work in progress for clients who are building their own report tables (iReporting)
-- =============================================
CREATE PROCEDURE [dbo].[ClientReportTableBuilder__GetProgressByClientID] 
	@ClientID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TOP (1) crtb.ClientReportTableBuilderID, 
	crtb.ClientID, 
	crtb.TableName, 
	crtb.ColumnName, 
	crtb.LeadTypeID, 
	crtb.TableSubtypeID, 
	crtb.DetailFieldID, 
	crtb.ActionType, 
	crtb.WhoRequested, 
	crtb.WhenRequested, 
	crtb.WhenStarted, 
	crtb.WhenCompleted, 
	crtb.RecordsExpected, 
	crtb.RecordsProcessed, 
	crtb.RecordsAtATime, 
	crtb.UserNotes, 
	crtb.SSISNotes, 
	cp.UserName AS WhoRequestedUserName, 
	lt.LeadTypeName, 
	ISNULL(dfs.DetailFieldSubTypeName, '') AS TableSubtypeName 
	FROM dbo.ClientReportTableBuilder crtb WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = crtb.WhoRequested 
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = crtb.LeadTypeID 
	LEFT JOIN dbo.DetailFieldSubType dfs WITH (NOLOCK) ON dfs.DetailFieldSubTypeID = crtb.TableSubtypeID 
	WHERE crtb.ClientID = @ClientID 
	AND crtb.WhenStarted IS NOT NULL AND crtb.WhenCompleted IS NULL 
	ORDER BY crtb.WhenStarted 
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableBuilder__GetProgressByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientReportTableBuilder__GetProgressByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableBuilder__GetProgressByClientID] TO [sp_executeall]
GO
