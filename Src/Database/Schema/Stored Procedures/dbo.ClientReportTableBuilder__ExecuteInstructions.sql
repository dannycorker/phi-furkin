SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-01-23
-- Description:	Execute ClientReportTable instructions as specified in the ClientReportTableBuilder table. 
--              These are issued by users in iReporting and are set up in the ClientReportTableBuilder__InitialInstruction proc.
-- =============================================
CREATE PROCEDURE [dbo].[ClientReportTableBuilder__ExecuteInstructions] 
	@SSISDetails VARCHAR(100) = 'Sql Server Agent on DB5' 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
	@ClientID INT,
	@WhoRequested INT,
	@ActionType VARCHAR(1024),
	@TableName VARCHAR(1024),
	@TableSubtypeID INT,
	@ColumnName VARCHAR(1024),
	@DetailFieldID INT,
	@DetailFieldSubtypeID INT,
	@TableDetailFieldPageID INT, /* This is the definition page, not the MatterDetailValues pages that use it */
	@LeadTypeID INT,
	@DetailFieldName VARCHAR(50), 
	@QuestionTypeID INT, 
	@ClientReportTableBuilderID INT,
	@SqlStatement VARCHAR(MAX),
	@NewTableName VARCHAR(1024),
	@FormerTableName VARCHAR(1024),
	@Debug BIT = 0,		/* Set this to 1 to show helpful messages */
	@NoExec BIT = 0,	/* Set this to 1 to avoid actually creating tables etc */
	@MaxID INT,
	@RowsExpected INT,
	@RowsProcessed INT,
	@RowsAtATime INT = 0,
	@MaxRecords INT = 0,
	@ResultsOneRowOneColumnAnswer VARCHAR(1000), 
	@LookupListID INT, 
	@LookupListDecodeFieldName VARCHAR(1024),
	@LockType VARCHAR(10) = 'TABLOCKX', /* NOLOCK or TABLOCKX */ 
	@RL_Columns VARCHAR(MAX)='',
	@RL_FieldIdents VARCHAR(MAX)='',
	@LinkToiReporting BIT = 1,
	@SELECTMaxRecords VARCHAR(20) = '' 


	SET NOCOUNT ON;

	/*
		UPDATE dbo.ClientReportTableBuilder
		SET WhenStarted = NULL,
		WhenCompleted = NULL,
		SSISNotes = NULL 
		WHERE ClientReportTableBuilderID = 20
	*/


	IF @Debug = 1
	BEGIN
		SELECT * 
		FROM dbo.ClientReportTableBuilder
		ORDER BY 1 
	END


	/* Get the next instruction to follow */
	SELECT TOP 1 @ClientReportTableBuilderID = ClientReportTableBuilderID 
	FROM dbo.ClientReportTableBuilder 
	WHERE WhenStarted IS NULL 
	ORDER BY ClientReportTableBuilderID ASC 


	/* Loop around doing all instructions with one call, unless NOEXEC is on, in which case just show how the first one would be processed. */
	WHILE @ClientReportTableBuilderID IS NOT NULL
	BEGIN

		/* Lock this record */
		IF @NoExec = 0
		BEGIN
			UPDATE dbo.ClientReportTableBuilder
			SET WhenStarted = CURRENT_TIMESTAMP,
			SSISNotes = 'Picked up by ' + @SSISDetails
			WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
		END
		
		SELECT @ClientID = c.ClientID, 
		@WhoRequested = c.WhoRequested,
		@TableName = c.TableName, 
		@LeadTypeID = c.LeadTypeID, 
		@TableSubtypeID = c.TableSubtypeID, 
		@ColumnName = c.ColumnName, 
		@DetailFieldID = c.DetailFieldID, 
		@ActionType = c.ActionType, 
		@TableDetailFieldPageID = c.TableDetailFieldPageID, 
		@MaxRecords = c.MaxRecords, 
		@LockType = c.LockType 
		FROM dbo.ClientReportTableBuilder c 
		WHERE c.ClientReportTableBuilderID = @ClientReportTableBuilderID 
		
		IF @Debug = 1
		BEGIN
			SELECT @ClientID AS ClientID, 
			@TableName AS TableName, 
			@TableSubtypeID AS TableSubtypeID, 
			@ColumnName AS ColumnName, 
			@DetailFieldID AS DetailFieldID, 
			@ActionType AS ActionType,
			@TableDetailFieldPageID AS TableDetailFieldPageID, 
			@MaxRecords AS MaxRecords, 
			@LockType AS LockType 
		END
		
		SET @SELECTMaxRecords = CASE WHEN @MaxRecords > 0 THEN ' TOP (' + CAST(@MaxRecords AS VARCHAR) + ') ' ELSE '' END 
		
		/*
			ActionType values are Sql Server instructions: 
			
			CREATE = CREATE TABLE 
			CREATE_RL = CREATE TABLE using SELECT INTO from ResourceListDetailValues
			ARCHIVE = RENAME TABLE with datestamp
			DROP (column name null) = DROP TABLE (RENAME zzz for now)
			
			ADD = ADD COLUMN
			DROP = (column not null) DROP COLUMN
		*/

		
		IF @ActionType = 'ADD'
		BEGIN
			
			/*
				Table must exist
				Column must not exist
				New column details must be provided
			*/
			IF @ColumnName IS NULL
			BEGIN
				
				/*
					This is an error.
				*/
				IF @Debug = 1
				BEGIN
					PRINT 'ADD requires a column to add'
				END	
			
			END
			
				
			/*
				This is an ADD COLUMN command.
			*/
			SELECT @QuestionTypeID = df.QuestionTypeID
			FROM dbo.DetailFields df 
			WHERE df.DetailFieldID = @DetailFieldID 

			IF @QuestionTypeID IN (2, 4) 
			BEGIN
				SELECT @SqlStatement = 'ALTER TABLE dbo.' + @TableName + ' ADD [' + @ColumnName + 'LookupListItemID] INT, ' + ' [' + @ColumnName + '] VARCHAR(2000);'
			END
			ELSE
			BEGIN
				SELECT @SqlStatement = 'ALTER TABLE dbo.' + @TableName + ' ADD [' + @ColumnName + '] ' + 
				CASE @QuestionTypeID 
				WHEN 3 THEN 'BIT' 
				WHEN 5 THEN 'DATETIME2(0)' 
				WHEN 7 THEN 'DECIMAL(18,2)' 
				WHEN 8 THEN 'INT' 
				WHEN 9 THEN 'DECIMAL(18,2)' 
				ELSE 'VARCHAR(2000)' 
				END + ';'
			END
			
			
			IF @Debug = 1
			BEGIN
				PRINT @SqlStatement
			END	
			
			IF @NoExec = 0
			BEGIN
				EXEC (@SqlStatement)
				
				/* Now we need to keep this column in sync with all new data forever */
				INSERT INTO dbo.ClientReportTableMember
				(
					ClientID,
					TableName,
					ColumnName,
					DetailFieldID,
					WhoCreated,
					WhenCreated
				)
				VALUES 
				(
					@ClientID,
					@TableName,
					@ColumnName,
					@DetailFieldID,
					@WhoRequested,
					CURRENT_TIMESTAMP
				)
				
				/* Show progress */
				UPDATE dbo.ClientReportTableDefinition 
				SET WhoModified = @WhoRequested, 
				WhenModified = CURRENT_TIMESTAMP
				WHERE TableName = @TableName
				
				/* Show progress */
				UPDATE dbo.ClientReportTableBuilder
				SET SSISNotes = 'Column added by ' + @SSISDetails
				WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
				
				/* Job done */
				UPDATE dbo.ClientReportTableBuilder
				SET WhenCompleted = CURRENT_TIMESTAMP,
				SSISNotes = 'Completed by ' + @SSISDetails
				WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
			END
				
		END /* End of ADD COLUMN section */
		
		
		
		IF @ActionType = 'POPULATE'
		BEGIN
				
			/*
				This is a POPULATE COLUMN command.
			*/

			/* Calculate and show how much work is required */
			IF @TableSubtypeID = 1
			BEGIN
				SELECT @RowsExpected = COUNT(*) 
				FROM dbo.LeadDetailValues dv 
				WHERE dv.DetailFieldID = @DetailFieldID 
			END
			IF @TableSubtypeID = 2
			BEGIN
				SELECT @RowsExpected = COUNT(*) 
				FROM dbo.MatterDetailValues dv 
				WHERE dv.DetailFieldID = @DetailFieldID 
			END
			IF @TableSubtypeID IN (6, 8)
			BEGIN
				SELECT @RowsExpected = COUNT(*) 
				FROM dbo.TableDetailValues dv 
				WHERE dv.DetailFieldID = @DetailFieldID 
			END
			IF @TableSubtypeID = 10
			BEGIN
				SELECT @RowsExpected = COUNT(*) 
				FROM dbo.CustomerDetailValues dv 
				WHERE dv.DetailFieldID = @DetailFieldID 
			END
			IF @TableSubtypeID = 11
			BEGIN
				SELECT @RowsExpected = COUNT(*) 
				FROM dbo.CaseDetailValues dv 
				WHERE dv.DetailFieldID = @DetailFieldID 
			END
			
		
			UPDATE dbo.ClientReportTableBuilder
			SET RecordsExpected = @RowsExpected
			WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
			
			/* If this is the first column, insert rows, otherwise update */
			/* Insert all at once or 5000 at a time */
			
			DECLARE @ResultsOneRowOneColumn TABLE (AnyResult VARCHAR(1000))
			
			
			/* Count the rows using dynamic sql and create the leads if necessary */
			--SELECT @SqlStatement = 'SELECT COUNT(*) FROM dbo.' + @TableName + ' t WITH (' + @LockType + ');'
			SELECT @SqlStatement = 'SELECT CASE WHEN EXISTS (SELECT * FROM dbo.' + @TableName + ' t WITH (' + @LockType + ') ) THEN 1 ELSE 0 END;'
			
			DELETE @ResultsOneRowOneColumn;
			SET @ResultsOneRowOneColumnAnswer = NULL;
			
			INSERT @ResultsOneRowOneColumn (AnyResult)
			EXEC(@SqlStatement)
			
			SELECT TOP 1 @ResultsOneRowOneColumnAnswer = r.AnyResult
			FROM @ResultsOneRowOneColumn r 
			
			IF @Debug = 1
			BEGIN
				SELECT * FROM @ResultsOneRowOneColumn;
				
				SELECT @ResultsOneRowOneColumnAnswer;
			END
			
			/* If the requested table does not contain any rows (Customers/Leads/Cases/Matters) then populate them all in one hit now */
			IF @ResultsOneRowOneColumnAnswer IS NOT NULL
			BEGIN
				IF ISNUMERIC(@ResultsOneRowOneColumnAnswer) = 1
				BEGIN
					IF CAST(@ResultsOneRowOneColumnAnswer AS INT) = 0
					BEGIN
						IF @TableSubtypeID = 1
						BEGIN
							SELECT @SqlStatement = '
							INSERT dbo.' + @TableName + '
							(
								ClientID,
								CustomerID, 
								LeadID,
								WhenCreated,
								WhenModified
							)
							SELECT ' + @SELECTMaxRecords + ' l.ClientID, l.CustomerID, l.LeadID, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP  
							FROM dbo.Lead l WITH (' + @LockType + ') 
							WHERE l.LeadTypeID = ' + CAST(@LeadTypeID AS VARCHAR) + ';'
							
						END
						IF @TableSubtypeID = 2
						BEGIN
							SELECT @SqlStatement = '
							INSERT dbo.' + @TableName + '
							(
								ClientID,
								CustomerID, 
								LeadID,
								CaseID,
								MatterID,
								WhenCreated,
								WhenModified
							)
							SELECT ' + @SELECTMaxRecords + ' m.ClientID, m.CustomerID, m.LeadID, m.CaseID, m.MatterID, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
							FROM dbo.Lead l WITH (' + @LockType + ') 
							INNER JOIN dbo.Matter m WITH (' + @LockType + ') ON m.LeadID = l.LeadID 
							WHERE l.LeadTypeID = ' + CAST(@LeadTypeID AS VARCHAR) + ';'
						END
						IF @TableSubtypeID IN (6, 8)
						BEGIN
							SELECT @SqlStatement = '
							INSERT dbo.' + @TableName + '
							(
								ClientID,
								CustomerID, 
								LeadID,
								CaseID,
								MatterID,
								TableRowID, 
								DetailFieldID, 
								DetailFieldPageID, 
								WhenCreated,
								WhenModified
							)
							SELECT ' + @SELECTMaxRecords + ' t.ClientID, t.CustomerID, t.LeadID, t.CaseID, t.MatterID, t.TableRowID, t.DetailFieldID, t.DetailFieldPageID, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP 
							FROM dbo.TableRows t WITH (' + @LockType + ') 
							INNER JOIN dbo.DetailFields df WITH (' + @LockType + ') ON t.DetailFieldPageID = df.DetailFieldPageID
							WHERE df.TableDetailFieldPageID = ' + CAST(@TableDetailFieldPageID AS VARCHAR) + ' ;'
						END
						IF @TableSubtypeID = 10
						BEGIN
							SELECT @SqlStatement = '
							INSERT dbo.' + @TableName + '
							(
								ClientID,
								CustomerID, 
								WhenCreated,
								WhenModified
							)
							SELECT ' + @SELECTMaxRecords + ' c.ClientID, c.CustomerID, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP 
							FROM dbo.Customers c WITH (' + @LockType + ') 
							WHERE EXISTS (
								SELECT * 
								FROM dbo.Lead l WITH (' + @LockType + ') 
								WHERE l.CustomerID = c.CustomerID
								AND l.LeadTypeID = ' + CAST(@LeadTypeID AS VARCHAR) + '
							);'
						END
						IF @TableSubtypeID = 11
						BEGIN
							SELECT @SqlStatement = '
							INSERT dbo.' + @TableName + '
							(
								ClientID,
								CustomerID, 
								LeadID,
								CaseID,
								WhenCreated,
								WhenModified
							)
							SELECT ' + @SELECTMaxRecords + ' l.ClientID, l.CustomerID, l.LeadID, ca.CaseID, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
							FROM dbo.Lead l WITH (' + @LockType + ') 
							INNER JOIN dbo.Cases ca WITH (' + @LockType + ') ON ca.LeadID = l.LeadID 
							WHERE l.LeadTypeID = ' + CAST(@LeadTypeID AS VARCHAR) + ';'
						END

						IF @Debug = 1
						BEGIN
							PRINT @SqlStatement
						END	
						
						IF @NoExec = 0
						BEGIN
							EXEC (@SqlStatement) 
							
							SELECT @RowsProcessed = @@ROWCOUNT
						END
				
					END
				END
			END
			
			/* Get details of the field to populate */
			SELECT @QuestionTypeID = df.QuestionTypeID, 
			@LookupListID = df.LookupListID 
			FROM dbo.DetailFields df 
			WHERE df.DetailFieldID = @DetailFieldID 
					
			/* Strongly-type the data in the new table */
			/* If we are dealing with a lookup list column, populate 2 columns at once here by storing the LULI value in a column of the same name with LOOKUPLISTITEMID on the end */
			SELECT @SqlStatement = '
				UPDATE dbo.' + @TableName + ' SET ' + @ColumnName + ' = ' + 
				CASE 
					WHEN @QuestionTypeID IN (2, 4) THEN 'luli.ItemValue, ' + @ColumnName + 'LookupListItemID = dv.ValueInt' 
					ELSE CASE @QuestionTypeID 
						WHEN 3 THEN 'dv.ValueInt' 
						WHEN 5 THEN 'dv.ValueDateTime' 
						WHEN 7 THEN 'dv.ValueMoney' 
						WHEN 8 THEN 'dv.ValueInt' 
						WHEN 9 THEN 'dv.ValueMoney' 
						ELSE 'dv.DetailValue' 
					END 
				END +
				+ ' , 
				WhenModified = CURRENT_TIMESTAMP 
				FROM dbo.' + @TableName + ' t ' + 
				CASE @TableSubtypeID 
				WHEN 1 THEN 
				'INNER JOIN dbo.LeadDetailValues dv WITH (' + @LockType + ') ON dv.LeadID = t.LeadID '
				WHEN 2 THEN 
				'INNER JOIN dbo.MatterDetailValues dv WITH (' + @LockType + ') ON dv.MatterID = t.MatterID ' 
				WHEN 6 THEN 
				'INNER JOIN dbo.TableDetailValues dv WITH (' + @LockType + ') ON dv.TableRowID = t.TableRowID ' 
				WHEN 8 THEN 
				'INNER JOIN dbo.TableDetailValues dv WITH (' + @LockType + ') ON dv.TableRowID = t.TableRowID ' 
				WHEN 10 THEN 
				'INNER JOIN dbo.CustomerDetailValues dv WITH (' + @LockType + ') ON dv.CustomerID = t.CustomerID '
				WHEN 11 THEN 
				'INNER JOIN dbo.CaseDetailValues dv WITH (' + @LockType + ') ON dv.CaseID = t.CaseID '
				END + 
				CASE WHEN @QuestionTypeID IN (2, 4) THEN 'INNER JOIN dbo.LookupListItems luli WITH (' + @LockType + ') ON luli.LookupListID = ' + CAST(@LookupListID AS VARCHAR) + ' AND luli.LookupListItemID = dv.ValueInt ' ELSE '' END + 
				'AND dv.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) 
				
				/*
				For nnnn rows at a time:
				
				SELECT @SqlStatement = 'INSERT dbo.' + @TableName + ' (ClientID, LeadID, MatterID, ' + @ColumnName + ') 
				SELECT ' + CASE WHEN @RowsAtATime > 0 THEN 'TOP (' + CAST(@RowsAtATime AS VARCHAR) + ') ' ELSE '' END + CAST(@ClientID AS VARCHAR) + ', dv.LeadID, dv.MatterID, dv.DetailValue 
				FROM dbo.MatterDetailValues dv 
				WHERE dv.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + 
				CASE WHEN @RowsAtATime > 0 THEN '
				AND dv.MatterDetailValueID > ' + CAST(@MaxID AS VARCHAR) ELSE '' END
				*/
			IF @Debug = 1
			BEGIN
				PRINT @SqlStatement
			END	
			
			IF @NoExec = 0
			BEGIN
				EXEC (@SqlStatement) 
				
				SELECT @RowsProcessed = @@ROWCOUNT
				
				/* Show progress */
				UPDATE dbo.ClientReportTableBuilder
				SET RecordsProcessed = ISNULL(RecordsProcessed, 0) + @RowsProcessed
				WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
				
				IF @RowsAtATime > 0
				BEGIN
					
					/* Now we need to exec a dynamic SELECT COUNT(*) into a table variable with one row and column */
					
					/* Rebuild the sql statement and loop round until all done */
					PRINT 'TODO'
				END
				
				/* Job done */
				UPDATE dbo.ClientReportTableBuilder
				SET WhenCompleted = CURRENT_TIMESTAMP,
				SSISNotes = 'Completed by ' + @SSISDetails
				WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
				
			END
			
		END /* POPULATE */
		
		
		IF @ActionType LIKE 'CREATE%'
		BEGIN
		
			/* Check that the table does not already exist */
			IF EXISTS
			(
				SELECT * 
				FROM INFORMATION_SCHEMA.TABLES t 
				WHERE t.TABLE_NAME = @TableName
			)
			BEGIN
			
				IF @Debug = 1
				BEGIN
					PRINT 'Already got one of these!'
				END			

				IF @NoExec = 0
				BEGIN
					UPDATE dbo.ClientReportTableBuilder
					SET WhenCompleted = CURRENT_TIMESTAMP,
					SSISNotes = 'Error! This table already exists! Completed by ' + @SSISDetails
					WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
				END
				
			END
			ELSE
			BEGIN
				
				/* Normal LeadType table */
				IF @ActionType = 'CREATE'
				BEGIN
					IF @TableSubtypeID IN (6, 8) 
					BEGIN
						/* TableDetailValues hold the data */
						SELECT @SqlStatement = 'CREATE TABLE dbo.' + @TableName + '
						(
							ClientID INT NOT NULL,
							CustomerID INT NULL,
							LeadID INT NULL, 
							CaseID INT NULL, 
							MatterID INT NULL, 
							TableRowID INT NULL, 
							DetailFieldID INT NULL, 
							DetailFieldPageID INT NULL, 
							WhenCreated DATETIME2(0) NOT NULL,
							WhenModified DATETIME2(0) NOT NULL
						);'
					END
					ELSE
					BEGIN
						/* Choose columns based on level, eg forget MatterID for a Lead table */
						SELECT @SqlStatement = 'CREATE TABLE dbo.' + @TableName + '
						(
							ClientID INT NOT NULL,
							CustomerID INT NOT NULL,' + 
							CASE WHEN @TableSubtypeID IN (1, 2, 11) THEN 			
							'LeadID INT NOT NULL,' 
							ELSE '' END + 
							CASE WHEN @TableSubtypeID IN (2, 11) THEN 
							'CaseID INT NOT NULL,' 
							ELSE '' END + 
							CASE WHEN @TableSubtypeID IN (2) THEN 
							'MatterID INT NOT NULL,' 
							ELSE '' END + 
							'WhenCreated DATETIME2(0) NOT NULL,
							WhenModified DATETIME2(0) NOT NULL
						);'
					END
				END
				/* Table from ResourceList */
				IF @ActionType = 'CREATE_RL'
				BEGIN
					SELECT @RL_Columns = '', @RL_FieldIdents = '', @SqlStatement = ''
				
					SELECT @RL_Columns = @RL_Columns + ',' + CASE WHEN df.LookupListID > 0 THEN '[' + dbo.fnGetValidSqlName(df.FieldCaption, '') + 'LookupListID],' ELSE '' END + '[' + dbo.fnGetValidSqlName(df.FieldCaption, '') + ']' ,
						   @RL_FieldIdents = @RL_FieldIdents + ',' + CAST(DetailFieldID AS VARCHAR(50)) 
					FROM dbo.DetailFields df 
					WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.Enabled = 1 
					ORDER BY df.FieldOrder
				
					/* Remove the leading commas */
					SELECT @RL_Columns=STUFF(@RL_Columns, 1, 1, ''), @RL_FieldIdents=STUFF(@RL_FieldIdents, 1, 1, '')
					
					SET @SqlStatement = 'SELECT * INTO ' + @TableName + ' FROM (
						SELECT CAST(rdv.ResourceListID AS VARCHAR(10)) AS ResourceListID, dbo.fnGetValidSqlName(df.FieldCaption, '''') + CASE WHEN df.LookupListID > 0 THEN ''LookupListID'' ELSE '''' END as FieldCaption, rdv.DetailValue as DetailValue
						FROM dbo.ResourceListDetailValues rdv WITH (' + @LockType + ') 
						INNER JOIN dbo.DetailFields df WITH (' + @LockType + ')  ON rdv.DetailFieldID = df.DetailFieldID 
						WHERE (df.DetailFieldID IN (' + @RL_FieldIdents + ')) 
						UNION ALL
						SELECT CAST(rdv.ResourceListID AS VARCHAR(10)) AS ResourceListID, dbo.fnGetValidSqlName(df.FieldCaption, '''') as FieldCaption, ll.ItemValue as DetailValue
						FROM dbo.ResourceListDetailValues rdv WITH (' + @LockType + ') 
						INNER JOIN dbo.DetailFields df WITH (' + @LockType + ')  ON rdv.DetailFieldID = df.DetailFieldID 
						INNER JOIN dbo.LookupListItems ll WITH (' + @LockType + ')  ON rdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID AND df.QuestionTypeID IN (2, 4)
						WHERE (df.DetailFieldID IN (' + @RL_FieldIdents + ')) 
						AND df.LookupListID > 0 '
					
					IF @ClientID > 0
					BEGIN
						SELECT @SqlStatement += 'AND rdv.ClientID = ' + CAST(@ClientID AS VARCHAR(10))
					END
						
					SELECT @SqlStatement += ') src
					PIVOT (MAX(DetailValue) for FieldCaption in (' + @RL_Columns + '))	as pvt
					ORDER by pvt.ResourceListID
					;'
				END
				
				IF @Debug = 1
				BEGIN
					PRINT @SqlStatement
				END	
				
				IF @NoExec = 0
				BEGIN
					EXEC (@SqlStatement)
					
					INSERT INTO dbo.ClientReportTableDefinition 
					(
						ClientID,
						TableName,
						LeadTypeID,
						TableSubtypeID,
						WhoCreated,
						WhenCreated,
						WhoModified,
						WhenModified
					)
					VALUES 
					(
						@ClientID,
						@TableName,
						@LeadTypeID,
						@TableSubtypeID,
						@WhoRequested,
						CURRENT_TIMESTAMP,
						@WhoRequested,
						CURRENT_TIMESTAMP
					)
					
					/* Show progress */
					UPDATE dbo.ClientReportTableBuilder
					SET WhenCompleted = CURRENT_TIMESTAMP,
					SSISNotes = 'Completed by ' + @SSISDetails
					WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
					
				END
						
			END /* Check that the table does not already exist */
			
		END /* End of CREATE section */

		IF @ActionType = 'LINK'
		BEGIN
			
			IF @NoExec = 0
			BEGIN
				
				/* Add the table to the list of iReporting tables no matter what type it is */
				IF NOT EXISTS(SELECT * FROM dbo.ReportTables rt WHERE rt.ReportTableName = @TableName)
				BEGIN
					INSERT INTO dbo.ReportTables(ReportTableName, DisplayOrder, RequiresClientSecurity, IsLookupTable, ShowAquariumValues, WhenCreated, WhenModified)
					VALUES (@TableName, 100, 1, 0, 0, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())
				END
				
				/* Now, if this is a normal table, create normal joins for it, plus any resource lists that it may use */
				IF @TableName NOT LIKE '%_RL'
				BEGIN
					/* Normal joins first (Customer/Lead/Case/Matter) */
					IF NOT EXISTS(SELECT * FROM dbo.ReportTableRelationships rtr WHERE rtr.TableFrom = @TableName)
					BEGIN
						INSERT INTO dbo.ReportTableRelationships(TableFrom, TableTo, TableJoin, Description, WhenCreated, WhenModified)
						VALUES 
						(@TableName, 'Customers', @TableName + '.CustomerID = Customers.CustomerID', 'Link ' + @TableName + ' to Customer', dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local()),
						(@TableName, 'Lead', @TableName + '.LeadID = Lead.LeadID', 'Link ' + @TableName + ' to Lead', dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local()),
						(@TableName, 'Cases', @TableName + '.CaseID = Cases.CaseID', 'Link ' + @TableName + ' to Case', dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local()),
						(@TableName, 'Matter', @TableName + '.MatterID = Matter.MatterID', 'Link ' + @TableName + ' to Matter', dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())
					END
					
					/* Now create links to resource lists where applicable */
					INSERT INTO dbo.ReportTableRelationships(TableFrom, TableTo, TableJoin, Description, WhenCreated, WhenModified)
					SELECT @TableName, dbo.fnGetValidSqlName(dfp.PageName, '') + '_RL', @TableName + '.' + m.ColumnName + ' = ' + dbo.fnGetValidSqlName(dfp.PageName, '') + '_RL.' + m.ColumnName, 'Link ' + @TableName + ' to Customer', dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local()
					FROM dbo.ClientReportTableMember m 
					INNER JOIN dbo.DetailFields df ON df.DetailFieldID = m.DetailFieldID 
					INNER JOIN dbo.DetailFieldPages dfp ON dfp.DetailFieldPageID = df.ResourceListDetailFieldPageID 
					WHERE m.TableName = @TableName 
					AND df.QuestionTypeID = 14 
					AND NOT EXISTS (
						SELECT * 
						FROM dbo.ReportTableRelationships rtr 
						WHERE rtr.TableFrom = @TableName
						AND rtr.TableTo = dbo.fnGetValidSqlName(dfp.PageName, '') + '_RL' 
						AND rtr.TableJoin = @TableName + '.' + m.ColumnName + ' = ' + dbo.fnGetValidSqlName(dfp.PageName, '') + '_RL.' + m.ColumnName
					)
				END
				
				/* Job Done */
				UPDATE dbo.ClientReportTableBuilder
				SET WhenCompleted = CURRENT_TIMESTAMP,
				SSISNotes = 'Completed by ' + @SSISDetails
				WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
				
			END
				
			IF @Debug = 1
			BEGIN
				/* Show all report tables for this client */
				SELECT * 
				FROM dbo.ReportTables rt 
				WHERE rt.ReportTableName LIKE '[_]C' + CAST(@ClientID AS VARCHAR) + '[_]Report%'
				ORDER BY 1 DESC

				SELECT * 
				FROM dbo.ReportTableRelationships rtr 
				WHERE rtr.TableFrom LIKE '[_]C' + CAST(@ClientID AS VARCHAR) + '[_]Report%'
				ORDER BY 1 DESC
			END
			
		END

		IF @ActionType = 'ARCHIVE'
		BEGIN
			
			/*
				Rename the table for aged data reporting, or later restore or drop.
			*/
			SELECT @NewTableName = @TableName + '_' + dbo.fnDateTimeForName()
			
			SELECT @SqlStatement = 'EXEC sp_rename ' + @TableName + ', ' + @NewTableName
			
			IF @Debug = 1
			BEGIN
				PRINT @SqlStatement
			END	
			
			IF @NoExec = 0
			BEGIN
				/* Store details of the renamed table so that it can be cleared down at a later date */
				INSERT INTO dbo.ClientReportTableToDrop 
				(
					ClientReportTableBuilderID,
					ClientID,
					TableName,
					WhoRequested,
					WhenRequested,
					UserNotes
				)
				VALUES
				(
					@ClientReportTableBuilderID,
					@ClientID,
					@NewTableName,
					@WhoRequested,
					dbo.fn_GetDate_Local(),
					'Formerly ' + @TableName
				)
				
				/* This table has been archived but it still exists and has a new name now */
				UPDATE dbo.ClientReportTableDefinition 
				SET TableName = @NewTableName 
				WHERE TableName = @TableName
				
				EXEC (@SqlStatement)
				
				/* Job done */
				UPDATE dbo.ClientReportTableBuilder
				SET WhenCompleted = CURRENT_TIMESTAMP,
				SSISNotes = 'Completed by ' + @SSISDetails
				WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
				
			END
				
		END /* Archive */
		
		
		IF @ActionType = 'DROP'
		BEGIN
		
			IF @ColumnName IS NULL
			BEGIN
				
				/*
					This is a DROP TABLE command.
					Make sure it only drops _C266_Report_tables
				*/
				--SELECT @FormerTableName = LEFT(@TableName, LEN(@TableName) - 20) /* Remove date and time stamp */
				
				--SELECT @NewTableName = 'zzz' + @TableName
				
				SELECT @SqlStatement = 'DROP TABLE dbo. ' + @TableName
				
				IF @Debug = 1
				BEGIN
					PRINT @SqlStatement
				END	
				
				IF @NoExec = 0 AND @TableName LIKE '[_]C' + CAST(@ClientID AS VARCHAR) + '[_]Report[_]%'
				BEGIN
					
					/* Unlink this table from the rest of the database */
					DELETE dbo.ClientReportTableMember  
					WHERE TableName = @TableName
					
					DELETE dbo.ClientReportTableDefinition 
					WHERE TableName = @TableName
					
					DELETE ReportTableRelationships 
					WHERE TableFrom = @TableName
					
					DELETE ReportTables 
					WHERE ReportTableName = @TableName
					
					EXEC (@SqlStatement)
					
					/* Job done */
					UPDATE dbo.ClientReportTableBuilder
					SET WhenCompleted = CURRENT_TIMESTAMP,
					SSISNotes = 'Completed by ' + @SSISDetails
					WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
					
				END
				
			END /* End of DROP TABLE section */
			ELSE
			BEGIN
			
				/*
					This is a DROP COLUMN command.
					Bear in mind that our column names have had all invalid chars removed already, 
					so the app might show "Are you sure?" but our report field will be called "Are_you_sure"
				*/
				SELECT @SqlStatement = 'ALTER TABLE dbo.' + @TableName + ' DROP COLUMN ' + @ColumnName
				
				IF @Debug = 1
				BEGIN
					PRINT @SqlStatement
				END	
				
				IF @NoExec = 0
				BEGIN
					EXEC (@SqlStatement)
					
					UPDATE dbo.ClientReportTableBuilder
					SET WhenCompleted = CURRENT_TIMESTAMP,
					SSISNotes = 'Completed by ' + @SSISDetails
					WHERE ClientReportTableBuilderID = @ClientReportTableBuilderID
					
				END
				
			END /* End of DROP COLUMN section */
			
		END /* End of DROP section */
		
		
		/* Get the next instruction to follow */
		SET @ClientReportTableBuilderID = NULL
		
		IF @NoExec = 0
		BEGIN
			SELECT TOP 1 @ClientReportTableBuilderID = ClientReportTableBuilderID 
			FROM dbo.ClientReportTableBuilder 
			WHERE WhenStarted IS NULL 
			ORDER BY ClientReportTableBuilderID ASC 
		END
		
	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableBuilder__ExecuteInstructions] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientReportTableBuilder__ExecuteInstructions] TO [sp_executeall]
GO
