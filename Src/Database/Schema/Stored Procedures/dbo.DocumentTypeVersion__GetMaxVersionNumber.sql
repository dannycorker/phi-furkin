SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 10/07/2017
-- Description:	Gets the max version number for the given document type
-- =============================================
CREATE PROCEDURE [dbo].[DocumentTypeVersion__GetMaxVersionNumber]
	@DocumentTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT MAX(VersionNumber) VersionNumber FROM DocumentTypeVersion WHERE DocumentTypeID=@DocumentTypeID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__GetMaxVersionNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion__GetMaxVersionNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__GetMaxVersionNumber] TO [sp_executeall]
GO
