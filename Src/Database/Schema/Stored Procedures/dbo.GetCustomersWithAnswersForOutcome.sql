SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetCustomersWithAnswersForOutcome] 

@ClientQuestionnaireID int,
@OutcomeID int

AS


SELECT     dbo.Customers.CustomerID, dbo.Customers.ClientID, dbo.Customers.TitleID, dbo.Customers.FirstName, dbo.Customers.MiddleName, 
dbo.Customers.LastName, dbo.Customers.EmailAddress, 
dbo.Customers.DayTimeTelephoneNumber, dbo.Customers.DayTimeTelephoneNumberVerifiedAndValid, 
dbo.Customers.HomeTelephone, dbo.Customers.HomeTelephoneVerifiedAndValid, 
dbo.Customers.MobileTelephone, dbo.Customers.MobileTelephoneVerifiedAndValid, 
dbo.Customers.Address1, dbo.Customers.Address2, dbo.Customers.Town, dbo.Customers.County, 
dbo.Customers.PostCode, dbo.Customers.HasDownloaded, dbo.Customers.DownloadedOn, dbo.Customers.AquariumStatusID, 
dbo.Customers.ClientStatusID, dbo.Customers.Test, dbo.Customers.CompanyName, 
dbo.Customers.CompanyTelephone, dbo.Customers.CompanyTelephoneVerifiedAndValid, 
dbo.Customers.WorksTelephone, dbo.Customers.WorksTelephoneVerifiedAndValid, 
dbo.Customers.DateOfBirth, dbo.Customers.IsBusiness, dbo.Customers.DefaultContactID,
dbo.Customers.Occupation, dbo.Customers.Employer, dbo.Customers.DoNotEmail, dbo.Customers.DoNotSellToThirdParty, dbo.Customers.AgreedToTermsAndConditions, dbo.Customers.PhoneNumbersVerifiedOn, dbo.CustomerOutcomes.OutcomeID, dbo.CustomerOutcomes.ClientQuestionnaireID
FROM         dbo.Customers WITH (NOLOCK) INNER JOIN
                      dbo.CustomerOutcomes WITH (NOLOCK) ON dbo.Customers.CustomerID = dbo.CustomerOutcomes.CustomerID
WHERE     (dbo.CustomerOutcomes.ClientQuestionnaireID = @ClientQuestionnaireID) AND (dbo.CustomerOutcomes.OutcomeID = @OutcomeID)





GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomersWithAnswersForOutcome] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomersWithAnswersForOutcome] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomersWithAnswersForOutcome] TO [sp_executeall]
GO
