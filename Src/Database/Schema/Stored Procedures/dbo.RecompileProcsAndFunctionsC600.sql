SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-08-04
-- Description:	Mark all 600 procs for recompilation
-- =============================================
CREATE PROCEDURE [dbo].[RecompileProcsAndFunctionsC600]
	@PrintOnly BIT = 0,
	@AdditionalProc VARCHAR(400) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @stmnt VARCHAR(500)

	DECLARE SPCursor CURSOR DYNAMIC READ_ONLY FOR
	SELECT 'EXEC sp_recompile [' + i.ROUTINE_NAME + ']'
	FROM INFORMATION_SCHEMA.ROUTINES i 
	WHERE i.ROUTINE_TYPE IN ('PROCEDURE','FUNCTION') 
	AND (i.ROUTINE_NAME LIKE '%600%' OR i.ROUTINE_NAME = @AdditionalProc OR i.ROUTINE_NAME LIKE '%1272%')
	ORDER BY i.ROUTINE_NAME

	OPEN SPCursor

	FETCH RELATIVE 1 FROM SPCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF @PrintOnly = 0
		BEGIN
			EXEC(@stmnt)
		END
		ELSE
		BEGIN
			PRINT @stmnt
		END

		FETCH NEXT FROM SPCursor INTO @stmnt
		
	END

	CLOSE SPCursor
	DEALLOCATE SPCursor
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileProcsAndFunctionsC600] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RecompileProcsAndFunctionsC600] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileProcsAndFunctionsC600] TO [sp_executeall]
GO
