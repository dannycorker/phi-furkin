SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2017-09-14
-- Description:	Record inserts and updates to ClientPersonnel
-- =============================================
CREATE PROCEDURE [dbo].[AuditClientPersonnelAdminGroup__Insert] 
	@ClientPersonnelAdminGroupID INT,
	@ClientID INT,
	@WhoModified INT,
	@IsInsert BIT, 
	@GroupName VARCHAR(50), 
	@ChangeNotes VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;
	
    INSERT INTO dbo.AuditClientPersonnelAdminGroup(ClientPersonnelAdminGroupID, ClientID, WhoModified, WhenModified, IsInsert, GroupName, ChangeNotes)
	VALUES (@ClientPersonnelAdminGroupID, @ClientID, @WhoModified, CURRENT_TIMESTAMP, @IsInsert, @GroupName, @ChangeNotes)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditClientPersonnelAdminGroup__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AuditClientPersonnelAdminGroup__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditClientPersonnelAdminGroup__Insert] TO [sp_executeall]
GO
