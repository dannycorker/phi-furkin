SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Department table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department_Find]
(

	@SearchUsingOR bit   = null ,

	@DepartmentID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@DepartmentName varchar (100)  = null ,

	@DepartmentDescription varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DepartmentID]
	, [ClientID]
	, [CustomerID]
	, [DepartmentName]
	, [DepartmentDescription]
    FROM
	[dbo].[Department] WITH (NOLOCK) 
    WHERE 
	 ([DepartmentID] = @DepartmentID OR @DepartmentID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([DepartmentName] = @DepartmentName OR @DepartmentName IS NULL)
	AND ([DepartmentDescription] = @DepartmentDescription OR @DepartmentDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DepartmentID]
	, [ClientID]
	, [CustomerID]
	, [DepartmentName]
	, [DepartmentDescription]
    FROM
	[dbo].[Department] WITH (NOLOCK) 
    WHERE 
	 ([DepartmentID] = @DepartmentID AND @DepartmentID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([DepartmentName] = @DepartmentName AND @DepartmentName is not null)
	OR ([DepartmentDescription] = @DepartmentDescription AND @DepartmentDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department_Find] TO [sp_executeall]
GO
