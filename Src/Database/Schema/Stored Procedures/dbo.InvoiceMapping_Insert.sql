SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the InvoiceMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[InvoiceMapping_Insert]
(

	@InvoiceMappingID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@SourceInvoiceNumberField int   ,

	@SourceTotalClaimAmountField int   ,

	@SourceFeesToPostToSageNetField int   ,

	@SourceFeesToPostToSageVatField int   ,

	@InvoiceTypeCode varchar (50)  ,

	@CreditNoteTypeCode varchar (50)  ,

	@InvoiceNumberField int   ,

	@DateField int   ,

	@DatePostedField int   ,

	@DetailsField int   ,

	@SentToSageField int   ,

	@NominalCodeField int   ,

	@TaxCodeField int   ,

	@InvoiceNetAmountField int   ,

	@InvoiceTaxAmountField int   ,

	@TypeField int   ,

	@MatterIDField int   ,

	@CaseNumberField int   ,

	@CreditNoteNetAmountField int   ,

	@CreditNoteTaxAmountField int   ,

	@NominalCode varchar (50)  ,

	@TaxCode varchar (50)  ,

	@TableRowsDetailFieldID int   ,

	@TableRowsDetailFieldPageID int   ,

	@FeesInvoicedToSageField int   ,

	@FeesPaidFromSageField int   ,

	@FeesInvoicedOtherField int   ,

	@FeesPaidOtherField int   ,

	@CaseBalanceField int   ,

	@SageCompanyName varchar (250)  ,

	@CostCentre varchar (50)  
)
AS


				
				INSERT INTO [dbo].[InvoiceMapping]
					(
					[ClientID]
					,[LeadTypeID]
					,[SourceInvoiceNumberField]
					,[SourceTotalClaimAmountField]
					,[SourceFeesToPostToSageNetField]
					,[SourceFeesToPostToSageVatField]
					,[InvoiceTypeCode]
					,[CreditNoteTypeCode]
					,[InvoiceNumberField]
					,[DateField]
					,[DatePostedField]
					,[DetailsField]
					,[SentToSageField]
					,[NominalCodeField]
					,[TaxCodeField]
					,[InvoiceNetAmountField]
					,[InvoiceTaxAmountField]
					,[TypeField]
					,[MatterIDField]
					,[CaseNumberField]
					,[CreditNoteNetAmountField]
					,[CreditNoteTaxAmountField]
					,[NominalCode]
					,[TaxCode]
					,[TableRowsDetailFieldID]
					,[TableRowsDetailFieldPageID]
					,[FeesInvoicedToSageField]
					,[FeesPaidFromSageField]
					,[FeesInvoicedOtherField]
					,[FeesPaidOtherField]
					,[CaseBalanceField]
					,[SageCompanyName]
					,[CostCentre]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@SourceInvoiceNumberField
					,@SourceTotalClaimAmountField
					,@SourceFeesToPostToSageNetField
					,@SourceFeesToPostToSageVatField
					,@InvoiceTypeCode
					,@CreditNoteTypeCode
					,@InvoiceNumberField
					,@DateField
					,@DatePostedField
					,@DetailsField
					,@SentToSageField
					,@NominalCodeField
					,@TaxCodeField
					,@InvoiceNetAmountField
					,@InvoiceTaxAmountField
					,@TypeField
					,@MatterIDField
					,@CaseNumberField
					,@CreditNoteNetAmountField
					,@CreditNoteTaxAmountField
					,@NominalCode
					,@TaxCode
					,@TableRowsDetailFieldID
					,@TableRowsDetailFieldPageID
					,@FeesInvoicedToSageField
					,@FeesPaidFromSageField
					,@FeesInvoicedOtherField
					,@FeesPaidOtherField
					,@CaseBalanceField
					,@SageCompanyName
					,@CostCentre
					)
				-- Get the identity value
				SET @InvoiceMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InvoiceMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceMapping_Insert] TO [sp_executeall]
GO
