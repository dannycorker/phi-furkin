SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-22
-- Description:	Create any missing fields for this lead, by type if specified
-- =============================================
CREATE PROCEDURE [dbo].[CreateAllMissingLeadDetailValues] 
	@LeadID int,
	@QuestionTypeID int = null
AS
BEGIN
	SET NOCOUNT ON;

	-- Get the Lead, use it to get all the possible DetailFields for the LeadType.
	-- Filter to just enabled fields, and optionally of the type specified (Equation etc),
	-- that don't already exist for the LeadID passed in.
	INSERT INTO dbo.LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
	SELECT df.ClientID, @LeadID, df.DetailFieldID, '' 
	FROM dbo.Lead l 
	INNER JOIN dbo.DetailFields df ON df.LeadTypeID = l.LeadTypeID AND df.Enabled = 1 
	WHERE l.LeadID = @LeadID 
	AND df.LeadOrMatter = 1 
	AND (@QuestionTypeID IS NULL OR df.QuestionTypeID = @QuestionTypeID) 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.LeadDetailValues ldv_existing 
		WHERE ldv_existing.LeadID = @LeadID 
		AND ldv_existing.DetailFieldID = df.DetailFieldID 
	)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CreateAllMissingLeadDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreateAllMissingLeadDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreateAllMissingLeadDetailValues] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[CreateAllMissingLeadDetailValues] TO [sp_executehelper]
GO
