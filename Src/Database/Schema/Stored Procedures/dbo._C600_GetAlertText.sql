SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Robin Hall
-- Create date: 2015-04-27
-- Description:	Generate alert text for custom control
-- 2017-06-08 CPS Show a breakdown of wait periods by policy section
-- 2018-05-02 GPR waive wait period for OMF converted policies 
-- 2019-02-07 GPR show wait period for OMF converted policies if the converted policy is Standard.
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetAlertText]

	 @CaseID		INT
	,@DetailFieldID INT

AS
BEGIN

	/*
	0 = Info
	1 = Warning
	2 = Error
	*/
	
	DECLARE @Messages		TABLE(IsHtml BIT, MessageType INT, MessageText VARCHAR(2000))
	DECLARE @MessageText	VARCHAR(1000)
	DECLARE @Title			VARCHAR(100) = 'You forgot to set a title in the proc'
	DECLARE @MatterID		INT
	DECLARE @ClientID		INT = dbo.fnGetPrimaryClientID()
		
	SELECT @MatterID = m.MatterID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.CaseID = @CaseID

	/* Field-specific processing*/
	
	-- Waiting period alerts
	IF @DetailFieldID in (175577,177398) /*Waiting period alerts*/ /*Waiting period alerts - hidden save*/
	BEGIN
		
		SELECT @Title = 'Waiting period check'
			
		-- Get details for waiting period check
		
		DECLARE 
			@Inception DATE
			,@DateOfLoss DATE
			,@WaitingPeriod INT
			,@OMFFlag INT
			
		SELECT TOP 1
			@Inception = mdvInception.ValueDate, @DateOfLoss = mdvDateOfLoss.ValueDate, @WaitingPeriod = rWaitingPeriod.ValueInt, @OMFFlag = CASE mScheme.MatterID WHEN /*Standard Dog*/10273 THEN 0 WHEN /*Standard Cat*/10277 THEN 0 ELSE ldvOMFfLag.ValueInt END /*GPR C600 #1450, wait period applies for Standard policy*/
		FROM
			-- Claim 
			Matter m WITH (NOLOCK)
			INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON mdvDateOfLoss.MatterID = m.MatterID AND mdvDateOfLoss.DetailFieldID = 144892
			INNER JOIN dbo.MatterDetailValues mdvAccidentIllness WITH (NOLOCK) ON mdvAccidentIllness.MatterID = m.MatterID AND mdvAccidentIllness.DetailFieldID = 175576
			-- Policy
			INNER JOIN dbo.LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToLeadID = m.LeadID AND ltr.FromLeadTypeID = 1492
			INNER JOIN dbo.MatterDetailValues mdvInception WITH (NOLOCK) ON mdvInception.MatterID = ltr.FromMatterID AND mdvInception.DetailFieldID = 170035 
			LEFT JOIN dbo.LeadDetailValues ldvOMFflag WITH (NOLOCK) ON ldvOMFfLag.LeadID = ltr.FromLeadID AND ldvOMFfLag.DetailFieldID = 180019 /*GPR 2018-05-02 Is this an OMF conversion?*/ 
			-- Scheme
			LEFT JOIN dbo.Matter mScheme WITH (NOLOCK) ON mScheme.MatterID = dbo.fn_C00_1272_GetPolicyFromClaim(m.MatterID)
			LEFT JOIN dbo.TableDetailValues rAccidentIllness WITH (NOLOCK) ON rAccidentIllness.MatterID = mScheme.MatterID AND rAccidentIllness.DetailFieldID = 175578
				AND rAccidentIllness.ValueInt = mdvAccidentIllness.ValueInt
			LEFT JOIN dbo.TableDetailValues rWaitingPeriod WITH (NOLOCK) ON rWaitingPeriod.TableRowID = rAccidentIllness.TableRowID AND rWaitingPeriod.DetailFieldID = 175579
		WHERE 
			m.CaseID = @CaseID

		--/*CPS 2017-06-08 Display a breakdown of Waiting Periods by Policy Section.*/
		--INSERT @Messages (IsHtml, MessageType, MessageText)
		--SELECT 1, 1, 'Inception: ' + CONVERT(VARCHAR,@Inception,103) + '.  Policy Age at Loss: ' + CONVERT(VARCHAR,DATEDIFF(DAY,@Inception,@DateOfLoss)) + ' day' + CASE WHEN DATEDIFF(DAY,@Inception,@DateOfLoss) = 1 THEN '' ELSE 's' END + '.</BR></BR></BR>'
		--WHERE @Inception is not NULL

		--INSERT @Messages (IsHtml, MessageType, MessageText)
		--SELECT 1, 0, '<font color="' + CASE WHEN DATEADD(DAY,fn.WaitDays,@Inception) > @DateOfLoss THEN 'red' ELSE 'green' END + '">' + fn.WaitingPeriodNarrative + '</font>' + ' - ' + fn.PolicySectionDisplay + '</BR></BR>'
		--FROM dbo.fn_C600_Claims_GetWaitPeriodsForClaim(@MatterID) fn					
		--ORDER BY fn.WaitDays ASC, fn.PolicySectionID ASC, fn.PolicySubSectionID ASC
		
		
		-- Check and form message 	
	
						
		/*CPS 2017-06-12 removed in favour of the display above*/
		IF @WaitingPeriod IS NOT NULL
		BEGIN
		
			IF (DATEADD(DAY, @WaitingPeriod, @Inception) > @DateOfLoss) AND @OMFFlag = 0 /*GPR 2018-05-02 added @OMFFlag to skip over the invalid section if this was a convert of One Month Free*/
			BEGIN
			
				INSERT @Messages(IsHtml, MessageType, MessageText)
				VALUES
					(0, 2, 'Invalid claim. Date of loss ({DateOfLoss}) is within the waiting period of {WaitingPeriod} days from policy inception ({Inception})')
			
			END
			ELSE
			BEGIN
			
				INSERT @Messages(IsHtml, MessageType, MessageText)
				VALUES
					(0, 0, 'Claim OK. Date of loss is outside the waiting period')
			
			END
		
		END
		ELSE 
		BEGIN
			
			INSERT @Messages(IsHtml, MessageType, MessageText)
			VALUES
				(0, 1, 'Unable to determine waiting period for this policy. Please verify dates are ok')
				,(0, 0, 'Date of loss: {DateOfLoss},  Policy inception: {Inception}')
		
		END
		

		-- Replace placeholders
		
		UPDATE @Messages 
		SET MessageText = REPLACE(REPLACE(REPLACE(MessageText
			,'{DateOfLoss}', CONVERT(VARCHAR, @DateOfLoss, 120))
			,'{WaitingPeriod}', ISNULL(CONVERT(VARCHAR, @WaitingPeriod), ''))
			,'{Inception}', CONVERT(VARCHAR, @Inception, 120))

		
	END

	-- Clear rows validation
	IF @DetailFieldID = 175594
	BEGIN
	
		SELECT @Title = 'Clearing claim rows'
		
		IF EXISTS (SELECT 1 FROM LeadEvent le WHERE le.CaseID = @CaseID AND le.EventTypeID = 121865 and le.EventDeleted = 0)
		BEGIN
		
			INSERT @Messages(IsHtml, MessageType, MessageText)
			VALUES
				(0, 2, 'Claim cannot be cleared. It has already been approved.')
		
		END
		ELSE
		BEGIN
		
			-- Build comment
			SELECT @MessageText = REPLACE(REPLACE(REPLACE('The claim has %1 claim row(s) and %2 payment row(s).',
				'%1', CONVERT(VARCHAR, SUM(CASE WHEN r.DetailFieldID = 144355 THEN 1 ELSE 0 END))),
				'%2', CONVERT(VARCHAR, SUM(CASE WHEN r.DetailFieldID = 154485 THEN 1 ELSE 0 END))),
				'%3', ISNULL(MAX(mdv.DetailValue), 'none given'))
			FROM dbo.Matter m WITH (NOLOCK)
			LEFT JOIN dbo.TableRows r WITH (NOLOCK) on r.MatterID = m.MatterID AND DetailFieldID IN (144355, 154485) -- Claim rows and payment rows
			LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 162650 -- User reason
			WHERE m.ClientID = @ClientID
			AND m.CaseID = @CaseID
		
			INSERT @Messages(IsHtml, MessageType, MessageText)
			VALUES
				(0, 1, @MessageText),
				(0, 1, 'Are you sure you want to clear these row(s)?')
		
		END

			
	
	END
	
    /* Return record sets */

	SELECT * FROM @Messages
		
	SELECT 
		CASE
			WHEN EXISTS (SELECT 1 FROM @Messages WHERE MessageType = 2) THEN 0
			ELSE 1 
			END AS CanApplyEvent
		,@Title AS TitleText

	
END









GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetAlertText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetAlertText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetAlertText] TO [sp_executeall]
GO
