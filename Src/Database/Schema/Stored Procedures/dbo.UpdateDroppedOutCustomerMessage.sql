SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateDroppedOutCustomerMessage]
@DroppedOutCustomerMessageID int,
@ClientID int,
@ClientQuestionnaireID int,
@EmailFromAddress nvarchar(255), 
@EmailSubject nvarchar(500),
@Email text,
@SmsMessage text

as

Update DroppedOutCustomerMessages
SET  ClientID = @ClientID,
ClientQuestionnaireID = @ClientQuestionnaireID,
EmailFromAddress = @EmailFromAddress,
EmailSubject = @EmailSubject,
Email = @Email,
SmsMessage = @SmsMessage
where DroppedOutCustomerMessageID = @DroppedOutCustomerMessageID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDroppedOutCustomerMessage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateDroppedOutCustomerMessage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateDroppedOutCustomerMessage] TO [sp_executeall]
GO
