SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Cases table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_Delete]
(

	@CaseID int   
)
AS


				DELETE FROM [dbo].[Cases] WITH (ROWLOCK) 
				WHERE
					[CaseID] = @CaseID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Delete] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Delete] TO [sp_executeall]
GO
