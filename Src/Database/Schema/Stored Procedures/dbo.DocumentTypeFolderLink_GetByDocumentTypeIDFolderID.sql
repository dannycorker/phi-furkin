SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentTypeFolderLink table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeFolderLink_GetByDocumentTypeIDFolderID]
(

	@DocumentTypeID int   ,

	@FolderID int   
)
AS


				SELECT
					[DocumentTypeID],
					[FolderID]
				FROM
					[dbo].[DocumentTypeFolderLink] WITH (NOLOCK) 
				WHERE
										[DocumentTypeID] = @DocumentTypeID
					AND [FolderID] = @FolderID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_GetByDocumentTypeIDFolderID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeFolderLink_GetByDocumentTypeIDFolderID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeFolderLink_GetByDocumentTypeIDFolderID] TO [sp_executeall]
GO
