SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-11
-- Description:	Get a list of equations from table fields that
--              need to be recalculated when a lead/matter is saved
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetForTableEquations] 
	@ClientID int, 
	@LeadTypeID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT df.*	
	FROM dbo.fnDetailFieldsShared(@ClientID) df
	WHERE df.QuestionTypeID = 10
	AND df.[Enabled] = 1
	AND df.DetailFieldPageID IN (
		SELECT df_p.TableDetailFieldPageID 
		FROM dbo.fnDetailFieldsShared(@ClientID) df_p
		WHERE df_p.ClientID = @ClientID 
		AND df_p.LeadTypeID = @LeadTypeID 
		AND df_p.TableDetailFieldPageID IS NOT NULL 
	)
	ORDER BY df.FieldOrder 

	SELECT @@ROWCOUNT


END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForTableEquations] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetForTableEquations] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForTableEquations] TO [sp_executeall]
GO
