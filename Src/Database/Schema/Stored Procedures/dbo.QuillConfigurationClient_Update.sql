SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuillConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationClient_Update]
(

	@QuillConfigurationClientID int   ,

	@AquariumClientID int   ,

	@QuillClientID int   ,

	@Username varchar (50)  ,

	@Password varchar (50)  ,

	@URI varchar (150)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuillConfigurationClient]
				SET
					[AquariumClientID] = @AquariumClientID
					,[QuillClientID] = @QuillClientID
					,[Username] = @Username
					,[Password] = @Password
					,[URI] = @URI
				WHERE
[QuillConfigurationClientID] = @QuillConfigurationClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationClient_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Update] TO [sp_executeall]
GO
