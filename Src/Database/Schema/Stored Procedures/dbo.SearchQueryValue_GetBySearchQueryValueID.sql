SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SearchQueryValue table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_GetBySearchQueryValueID]
(

	@SearchQueryValueID int   
)
AS


				SELECT
					[SearchQueryValueID],
					[ClientID],
					[SubClientID],
					[SearchQueryID],
					[SearchFieldID],
					[SearchFieldOperatorID],
					[FieldValue],
					[FieldValueID]
				FROM
					[dbo].[SearchQueryValue] WITH (NOLOCK) 
				WHERE
										[SearchQueryValueID] = @SearchQueryValueID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_GetBySearchQueryValueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_GetBySearchQueryValueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_GetBySearchQueryValueID] TO [sp_executeall]
GO
