SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChangeStepState table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStepState_Update]
(

	@ChangeStepStateID int   ,

	@StateDescription varchar (255)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChangeStepState]
				SET
					[StateDescription] = @StateDescription
				WHERE
[ChangeStepStateID] = @ChangeStepStateID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStepState_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStepState_Update] TO [sp_executeall]
GO
