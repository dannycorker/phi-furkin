SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-01-31
-- Description:	Gets the pivoted policy sections for the user to select
-- Used by:		DF 145704
-- UPDATES:		2015-11-02	SB	Pull back optional coverage policy sections
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetPolicySections] 
(
	@MatterID INT,
	@LeadID INT = NULL
)
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	IF @LeadID IS NOT NULL
	BEGIN
		SELECT @MatterID = MatterID
		FROM dbo.Matter
		WHERE LeadID = @LeadID
	END
	
	DECLARE @CurrentPolicyMatterID INT
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	
	DECLARE @PolicyPetType INT
	SELECT @PolicyPetType = rldvPetType.ValueInt
	FROM Lead l WITH (NOLOCK)
	INNER JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK) on ldvPetType.LeadID = l.LeadID and ldvPetType.DetailFieldID = 144272
	INNER JOIN dbo.ResourceListDetailValues rldvPetType WITH (NOLOCK) on rldvPetType.ResourceListID = ldvPetType.ValueInt and rldvPetType.DetailFieldID = 144269
	WHERE l.LeadID = @PolicyLeadID
	
	-- Now we need to see if there are any optional coverages
	DECLARE	@DateToUse DATE  
	SELECT	@DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@MatterID, NULL, NULL)  

	-- Now find the matching table row and get the coverages   
	DECLARE @OptionalCoverages VARCHAR(2000)
	SELECT @OptionalCoverages = tdvCover.DetailValue
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r  
	INNER JOIN dbo.TableDetailValues tdvCover WITH (NOLOCK) ON r.TableRowID = tdvCover.TableRowID AND tdvCover.DetailFieldID = 175737   
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663  
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664  
	WHERE @DateToUse >= tdvStart.ValueDate  
	AND @DateToUse < tdvEnd.ValueDate  
	
	SELECT tdvRl.ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, CASE WHEN llSection.LookupListItemID = 42878 THEN 0 ELSE 1 END AS SortOrder
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRl WITH (NOLOCK) ON r.TableRowID = tdvRl.TableRowID AND tdvRl.DetailFieldID = 144357
	INNER JOIN dbo.ResourceListDetailValues rlSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSection.ResourceListID AND rlSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rlSection.ValueInt = llSection.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rlSubSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSubSection.ResourceListID AND rlSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rlSubSection.ValueInt = llSubSection.LookupListItemID
	LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 170013 
	WHERE r.MatterID = @CurrentPolicyMatterID
	AND (tdvPetType.ValueInt IS NULL OR tdvPetType.ValueInt IN (0, @PolicyPetType))
	
	UNION
	SELECT c.ResourceListID, c.Section, c.SubSection, CASE WHEN c.SectionID = 42878 THEN 0 ELSE 1 END AS SortOrder
	FROM dbo.fn_C600_Scheme_GetOptionalCoverages(@CurrentPolicyMatterID) c
	INNER JOIN dbo.fnTableOfIDsFromCSV(@OptionalCoverages) id ON c.GroupID = id.AnyID
	WHERE (c.PetTypeID IS NULL OR c.PetTypeID IN (0, @PolicyPetType))
	
	ORDER BY SortOrder, Section, SubSection
	
	
	-- Select the already saved resource list items
	SELECT DISTINCT tdvRLID.ResourceListID AS ResourceListID
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350 
	WHERE r.MatterID = @MatterID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetPolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicySections] TO [sp_executeall]
GO
