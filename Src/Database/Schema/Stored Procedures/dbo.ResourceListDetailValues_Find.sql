SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ResourceListDetailValues table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_Find]
(

	@SearchUsingOR bit   = null ,

	@ResourceListDetailValueID int   = null ,

	@ResourceListID int   = null ,

	@ClientID int   = null ,

	@DetailFieldID int   = null ,

	@LeadOrMatter int   = null ,

	@DetailValue varchar (2000)  = null ,

	@ErrorMsg varchar (1000)  = null ,

	@EncryptedValue varchar (3000)  = null ,

	@ValueInt int   = null ,

	@ValueMoney money   = null ,

	@ValueDate date   = null ,

	@ValueDateTime datetime2   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ResourceListDetailValueID]
	, [ResourceListID]
	, [ClientID]
	, [DetailFieldID]
	, [LeadOrMatter]
	, [DetailValue]
	, [ErrorMsg]
	, [EncryptedValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
    FROM
	[dbo].[ResourceListDetailValues] WITH (NOLOCK) 
    WHERE 
	 ([ResourceListDetailValueID] = @ResourceListDetailValueID OR @ResourceListDetailValueID IS NULL)
	AND ([ResourceListID] = @ResourceListID OR @ResourceListID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([LeadOrMatter] = @LeadOrMatter OR @LeadOrMatter IS NULL)
	AND ([DetailValue] = @DetailValue OR @DetailValue IS NULL)
	AND ([ErrorMsg] = @ErrorMsg OR @ErrorMsg IS NULL)
	AND ([EncryptedValue] = @EncryptedValue OR @EncryptedValue IS NULL)
	AND ([ValueInt] = @ValueInt OR @ValueInt IS NULL)
	AND ([ValueMoney] = @ValueMoney OR @ValueMoney IS NULL)
	AND ([ValueDate] = @ValueDate OR @ValueDate IS NULL)
	AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ResourceListDetailValueID]
	, [ResourceListID]
	, [ClientID]
	, [DetailFieldID]
	, [LeadOrMatter]
	, [DetailValue]
	, [ErrorMsg]
	, [EncryptedValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
    FROM
	[dbo].[ResourceListDetailValues] WITH (NOLOCK) 
    WHERE 
	 ([ResourceListDetailValueID] = @ResourceListDetailValueID AND @ResourceListDetailValueID is not null)
	OR ([ResourceListID] = @ResourceListID AND @ResourceListID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([LeadOrMatter] = @LeadOrMatter AND @LeadOrMatter is not null)
	OR ([DetailValue] = @DetailValue AND @DetailValue is not null)
	OR ([ErrorMsg] = @ErrorMsg AND @ErrorMsg is not null)
	OR ([EncryptedValue] = @EncryptedValue AND @EncryptedValue is not null)
	OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
	OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
	OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
	OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_Find] TO [sp_executeall]
GO
