SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_Get_BasicList]

AS


				
				SELECT
					[FieldTargetID],
					[Target],
					[ObjectName],
					[PropertyName]
				FROM
					[dbo].[FieldTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Get_BasicList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_Get_BasicList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Get_BasicList] TO [sp_executeall]
GO
