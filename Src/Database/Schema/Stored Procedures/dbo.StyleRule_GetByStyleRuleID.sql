SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the StyleRule table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[StyleRule_GetByStyleRuleID]
(

	@StyleRuleID int   
)
AS


				SELECT
					[StyleRuleID],
					[RuleClass],
					[RuleDescription],
					[RuleValue],
					[AquariumOnly],
					[IncludeLinkText]
				FROM
					[dbo].[StyleRule] WITH (NOLOCK) 
				WHERE
										[StyleRuleID] = @StyleRuleID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_GetByStyleRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StyleRule_GetByStyleRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_GetByStyleRuleID] TO [sp_executeall]
GO
