SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderObjectField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderObjectFieldID int   = null ,

	@DataLoaderObjectTypeID int   = null ,

	@FieldName varchar (250)  = null ,

	@DataTypeID int   = null ,

	@IsMandatory bit   = null ,

	@IsNormalisedField bit   = null ,

	@IsABitField bit   = null ,

	@IsClientOwned bit   = null ,

	@IsUniqueTableID bit   = null ,

	@IsWriteOnce bit   = null ,

	@HelperText varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderObjectFieldID]
	, [DataLoaderObjectTypeID]
	, [FieldName]
	, [DataTypeID]
	, [IsMandatory]
	, [IsNormalisedField]
	, [IsABitField]
	, [IsClientOwned]
	, [IsUniqueTableID]
	, [IsWriteOnce]
	, [HelperText]
    FROM
	[dbo].[DataLoaderObjectField] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID OR @DataLoaderObjectFieldID IS NULL)
	AND ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID OR @DataLoaderObjectTypeID IS NULL)
	AND ([FieldName] = @FieldName OR @FieldName IS NULL)
	AND ([DataTypeID] = @DataTypeID OR @DataTypeID IS NULL)
	AND ([IsMandatory] = @IsMandatory OR @IsMandatory IS NULL)
	AND ([IsNormalisedField] = @IsNormalisedField OR @IsNormalisedField IS NULL)
	AND ([IsABitField] = @IsABitField OR @IsABitField IS NULL)
	AND ([IsClientOwned] = @IsClientOwned OR @IsClientOwned IS NULL)
	AND ([IsUniqueTableID] = @IsUniqueTableID OR @IsUniqueTableID IS NULL)
	AND ([IsWriteOnce] = @IsWriteOnce OR @IsWriteOnce IS NULL)
	AND ([HelperText] = @HelperText OR @HelperText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderObjectFieldID]
	, [DataLoaderObjectTypeID]
	, [FieldName]
	, [DataTypeID]
	, [IsMandatory]
	, [IsNormalisedField]
	, [IsABitField]
	, [IsClientOwned]
	, [IsUniqueTableID]
	, [IsWriteOnce]
	, [HelperText]
    FROM
	[dbo].[DataLoaderObjectField] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderObjectFieldID] = @DataLoaderObjectFieldID AND @DataLoaderObjectFieldID is not null)
	OR ([DataLoaderObjectTypeID] = @DataLoaderObjectTypeID AND @DataLoaderObjectTypeID is not null)
	OR ([FieldName] = @FieldName AND @FieldName is not null)
	OR ([DataTypeID] = @DataTypeID AND @DataTypeID is not null)
	OR ([IsMandatory] = @IsMandatory AND @IsMandatory is not null)
	OR ([IsNormalisedField] = @IsNormalisedField AND @IsNormalisedField is not null)
	OR ([IsABitField] = @IsABitField AND @IsABitField is not null)
	OR ([IsClientOwned] = @IsClientOwned AND @IsClientOwned is not null)
	OR ([IsUniqueTableID] = @IsUniqueTableID AND @IsUniqueTableID is not null)
	OR ([IsWriteOnce] = @IsWriteOnce AND @IsWriteOnce is not null)
	OR ([HelperText] = @HelperText AND @HelperText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Find] TO [sp_executeall]
GO
