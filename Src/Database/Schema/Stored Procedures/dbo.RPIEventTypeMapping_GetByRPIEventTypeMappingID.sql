SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIEventTypeMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventTypeMapping_GetByRPIEventTypeMappingID]
(

	@RPIEventTypeMappingID int   
)
AS


				SELECT
					[RPIEventTypeMappingID],
					[ClientID],
					[LeadTypeID],
					[RPIEventTypeID],
					[EventTypeID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[RPIEventTypeMapping] WITH (NOLOCK) 
				WHERE
										[RPIEventTypeMappingID] = @RPIEventTypeMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_GetByRPIEventTypeMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventTypeMapping_GetByRPIEventTypeMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventTypeMapping_GetByRPIEventTypeMappingID] TO [sp_executeall]
GO
