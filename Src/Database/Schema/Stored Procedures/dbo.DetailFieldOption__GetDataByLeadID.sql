SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-10
-- Description:	DetailFieldOption__GetDataByLeadID
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldOption__GetDataByLeadID]
@ClientID int,
@LeadID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
	SELECT dfo.ClientID, l.LeadID, dfo.DetailFieldID, ''
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 1
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = @LeadID and l.LeadTypeID = df.LeadTypeID
	WHERE dfo.ClientID = @ClientID
	AND NOT EXISTS (
		SELECT *
		FROM LeadDetailValues ldv WITH (NOLOCK)
		WHERE ldv.LeadID = l.LeadID
		AND ldv.DetailFieldID = dfo.DetailFieldID
	)

	SELECT dfo.DetailFieldID, df.FieldCaption, ldv.DetailValue, dfo.FieldOrder, dfo.Mandatory, l.LeadID, l.CustomerID, df.LeadOrMatter
	FROM DetailFieldOption dfo WITH (NOLOCK)
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dfo.DetailFieldID and df.LeadOrMatter = 1
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = @LeadID AND l.LeadTypeID = df.LeadTypeID
	LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = df.DetailFieldID and ldv.LeadID = l.LeadID
	WHERE dfo.ClientID = @ClientID
	ORDER BY dfo.FieldOrder

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldOption__GetDataByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldOption__GetDataByLeadID] TO [sp_executeall]
GO
