SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		James Lewis
-- Create date: 2016-10-28
-- Description:	Actually Handles all card response
-- Mods
--	2017-01-05 DCM updated error code if statement to exclude '00'
--	2017-01-09 DCM updated processing to assume Customerledger record already created
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CardTransaction_HandleErrorResponse]
(
	@CardTransactionID  INT = NULL,
	@FileName			VARCHAR(MAX) = NULL
)
AS
BEGIN

	DECLARE @MatterID					INT, 
			@CaseID						INT,
			@EventToApply				INT, 
			@ErrorCode					VARCHAR(40),
			@LeadEventID				INT,
			@CustomerPaymentScheduleID	INT,
			@AccountID					INT,
			@Now						DATE = dbo.fn_GetDate_Local(),
			@StatusMsg					VARCHAR(40),
			@CustomerID					INT,
			@PaymentNET					DECIMAL (18,2),
			@PaymentVAT					DECIMAL (18,2),
			@PaymentGross				DECIMAL (18,2),
			@CustomerLedgerID			INT,
			@NewCustomerPaymentScheduleID INT,
			@ClientID					INT = dbo.fnGetPrimaryClientID()
			

	IF ISNULL(@CardTransactionID,'') <> '' and @CardTransactionID <> -1 
	BEGIN 
	
		/*What Error are we dealing with*/ 
		SELECT @ErrorCode = ct.ErrorCode, @MatterID = ct.ObjectID,@CustomerPaymentScheduleID = ct.CustomerPaymentScheduleID,@StatusMsg = ct.StatusMsg 
		FROM CardTransaction ct WITH (NOLOCK) 
		where ct.CardTransactionID = @CardTransactionID 
		and ct.ObjectTypeID = 2
		
	
		IF NOT (ISNULL(@ErrorCode, '' ) = '' OR @ErrorCode = '00') 
		BEGIN 
			
			/*Now grab the event mapped to this error from client level*/
			SELECT @EventToApply = ev.DetailValue FROM 
			TableDetailValues er WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues ev WITH (NOLOCK) on ev.TableRowID = er.TableRowID 
			Where er.DetailFieldID = 177308
			and ev.DetailFieldID = 177309
			and er.DetailValue = @ErrorCode
			
			IF @EventToApply IS NULL -- we don't understand the error code, flag by adding 'unlnown' event
				SELECT @EventToApply=155370
			
			SELECT @AccountID = cp.AccountID FROM CustomerPaymentSchedule cp WITH (NOLOCK) 
			Where cp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID
								
			/*And the caseID*/
			SELECT @CaseID = m.CaseID, @LeadEventID = c.LatestInProcessLeadEventID FROM Matter m WITH (NOLOCK) 
			INNER JOIN dbo.Cases c WITH (NOLOCK) on c.CaseID = m.CaseID
			where m.MatterID = @MatterID
			
		 	-- update the customer ledger and payment schedules
		 	SELECT @CustomerLedgerID=cl.CustomerLedgerID , @PaymentNET = cl.TransactionNet, @PaymentGross = cl.TransactionGross, @PaymentVAT = cl.TransactionVAT, @CustomerID = cl.CustomerID
		 	FROM CustomerLedger cl WITH (NOLOCK) 
		 	WHERE cl.PaymentID=@CardTransactionID
		 		AND cl.TransactionDescription IS NULL -- if the description is not null we have already processed this record
		 		
		 	IF @CustomerLedgerID IS NOT NULL 
		 	BEGIN
		 	
		 		-- insert a GL reversal record
				INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
				VALUES (600, @CustomerID, NULL, '', @ErrorCode, @Now, @CardTransactionID,'Card Payment Failure', -@PaymentNet,-@PaymentVAT, -@PaymentGross, '', @MatterID, 2, @CardTransactionID, NULL, 44412, dbo.fn_GetDate_Local())	
				
				SELECT @CustomerLedgerID = SCOPE_IDENTITY()
				
				INSERT INTO CustomerPaymentSchedule (ClientID,CustomerID,AccountID,ActualCollectionDate,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,WhoCreated,WhenCreated)
				SELECT pp.ClientID,pp.CustomerID,pp.AccountID,@Now,pp.ActualCollectionDate,-pp.PaymentNet,-pp.PaymentVAT,-pp.PaymentGross,4,@CustomerLedgerID,@Now,44412,@Now  
				FROM CustomerPaymentSchedule pp 
				Where pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
					
				SELECT @NewCustomerPaymentScheduleID = SCOPE_IDENTITY() 	
				
				INSERT INTO PurchasedProductPaymentSchedule (ClientID,CustomerID,AccountID,PurchasedProductID,ActualCollectionDate,CoverFrom,CoverTo,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,CustomerPaymentScheduleID,WhoCreated,WhenCreated,PurchasedProductPaymentScheduleParentID)
				SELECT pp.ClientID,pp.CustomerID,pp.AccountID,pp.PurchasedProductID,@Now,pp.CoverFrom,pp.CoverTo,@Now,-pp.PaymentNet, -pp.PaymentVAT, -pp.PaymentGross, 4,@CustomerLedgerID,@Now,@NewCustomerPaymentScheduleID,44412,@Now,pp.PurchasedProductPaymentScheduleParentID
				FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
				where pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
		 				 		
		 	--	UPDATE CustomerLedger 
		 	--	SET TransactionDescription='Card Payment Failure',
		 	--		FailureCode='',
		 	--		FailureReason=@ErrorCode,
		 	--		TransactionDate=@Now,
		 	--		TransactionGross=-TransactionGross,
		 	--		TransactionNet=-TransactionNet,
		 	--		TransactionVAT=-TransactionVAT,
		 	--		LeadEventID=''
		 	--	WHERE CustomerLedgerID=@CustomerLedgerID
		 		
				--UPDATE PurchasedProductPaymentSchedule 
				--Set PaymentStatusID = 4
				--From PurchasedProductPaymentSchedule 
				--WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
		
				--UPDATE CustomerPaymentSchedule  
				--Set PaymentStatusID = 4
				--From CustomerPaymentSchedule 
				--WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
		 			
				/*Now apply the event to kick off the process*/ 
				
				EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @EventToApply,44412,0
			
		 	END

			--SELECT @CustomerID = ct.CustomerID, @PaymentVAT = cp.PaymentVAT * - 1, @PaymentGross = cp.PaymentGross * - 1, @PaymentNET = cp.PaymentNet * - 1
		 --	FROM CardTransaction ct WITH (NOLOCK) 
		 --	INNER JOIN dbo.CustomerPaymentSchedule cp on cp.CustomerPaymentScheduleID = ct.CustomerPaymentScheduleID
		 --	Where ct.CardTransactionID = @CardTransactionID
			
			---- insert a GL reversal record
		 --	INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			--VALUES (600, @CustomerID, NULL, '', @ErrorCode, @Now, @CardTransactionID,'Card Payment Failure', @PaymentNet,@PaymentVAT, @PaymentGross, '', @MatterID, 2, @CardTransactionID, NULL, 44412, dbo.fn_GetDate_Local())	
			
			--SELECT @CustomerLedgerID = SCOPE_IDENTITY()
			
			--/*Now apply the event to kick off the process*/ 
			
			--EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, @EventToApply,44412,0
			
			--/*Now make sure the ppps is up to date*/
			--IF ISNULL(@CustomerPaymentScheduleID,0) <> 0 
			--BEGIN 
			
			--	UPDATE PurchasedProductPaymentSchedule 
			--	Set PaymentStatusID = 4,CustomerLedgerID =  @CustomerLedgerID , ReconciledDate = @Now
			--	From PurchasedProductPaymentSchedule 
			--	WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
				
			--	UPDATE CustomerPaymentSchedule  
			--	Set PaymentStatusID = 4,CustomerLedgerID =  @CustomerLedgerID , ReconciledDate = @Now
			--	From CustomerPaymentSchedule 
			--	WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
						
			--END
			
		END
		ELSE IF ISNULL(@StatusMsg,'') = 'Approved'
		BEGIN
		 	
		 	SELECT @CustomerLedgerID=cl.CustomerLedgerID 
		 	FROM CustomerLedger cl WITH (NOLOCK) 
		 	WHERE cl.PaymentID=@CardTransactionID
		 		AND cl.TransactionDescription IS NULL -- if the description is not null we have already processed this record
		 		
		 	IF @CustomerLedgerID IS NOT NULL 
		 	BEGIN
		 	
		 		UPDATE CustomerLedger 
		 		SET TransactionDescription='Card Payment Success',
		 			FailureCode='',
		 			FailureReason='',
		 			EffectivePaymentDate=@Now,
		 			TransactionDate=@Now,
		 			LeadEventID=''
		 		WHERE CustomerLedgerID=@CustomerLedgerID
		 		
				UPDATE PurchasedProductPaymentSchedule 
				Set PaymentStatusID = 6,CustomerLedgerID = @CustomerLedgerID
				From PurchasedProductPaymentSchedule 
				WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
		
				UPDATE CustomerPaymentSchedule  
				Set PaymentStatusID = 6, CustomerLedgerID = @CustomerLedgerID
				From CustomerPaymentSchedule 
				WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
		 	
		 	END
		 		
		 --	SELECT @AccountID = cp.AccountID FROM CustomerPaymentSchedule cp WITH (NOLOCK) 
			--Where cp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID
		 	
		 --	SELECT @CustomerID = ct.CustomerID, @PaymentVAT = cp.PaymentVAT, @PaymentGross = cp.PaymentGross, @PaymentNET = cp.PaymentNet
		 --	FROM CardTransaction ct WITH (NOLOCK) 
		 --	INNER JOIN dbo.CustomerPaymentSchedule cp on cp.CustomerPaymentScheduleID = ct.CustomerPaymentScheduleID
		 --	Where ct.CardTransactionID = @CardTransactionID
		 			 	
		 --	INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			--VALUES (600, @CustomerID, NULL, '', '', @Now, @CardTransactionID,'Card Payment Success', @PaymentNet,@PaymentVAT, @PaymentGross, '', @MatterID, 2, @CardTransactionID, NULL, 44412, dbo.fn_GetDate_Local())	
		 	
		 --	SELECT @CustomerLedgerID = SCOPE_IDENTITY()	
		 		
			--UPDATE PurchasedProductPaymentSchedule 
			--Set PaymentStatusID = 6, ReconciledDate = @Now, CustomerLedgerID = @CustomerLedgerID
			--From PurchasedProductPaymentSchedule 
			--WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
	
			--UPDATE CustomerPaymentSchedule  
			--Set PaymentStatusID = 6, ReconciledDate = @Now, CustomerLedgerID = @CustomerLedgerID
			--From CustomerPaymentSchedule 
			--WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		END
		
	END 
	ELSE 
	BEGIN 
		
		/*IF card transacitonID is NULL then the whole file has failed. We need to inform PHI of this. 
		  So we update a report's sqltext to spit out the full file name then run the batch job to email the results
		 Bit of a hack but we won't worry. */
		 
		 Update s 
		 SET QueryText = 'SELECT ' + '''' + @FileName + ''''  
		 FROM SqlQuery s 
		 where s.QueryID = 36661
		 
		 EXEC AutomatedTask__RunNow   20939,0,NULL,0 
	
	END 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CardTransaction_HandleErrorResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CardTransaction_HandleErrorResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CardTransaction_HandleErrorResponse] TO [sp_executeall]
GO
