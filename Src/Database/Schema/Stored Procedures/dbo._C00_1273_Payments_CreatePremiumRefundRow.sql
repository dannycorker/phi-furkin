SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-09-10
-- Description:	Creates a premium refund row
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Payments_CreatePremiumRefundRow]
(
	@CaseID INT
	,@Total MONEY
	,@SubTotal MONEY
	,@Tax MONEY
	,@ReimburseBy INT = NULL
)
AS
BEGIN

	DECLARE @LogEntry VARCHAR(2000)

	SELECT @LogEntry='TBC. This proc related to Collections - all fields of interest at customer level wait til Collections further advanced to implement'
	EXEC _C00_LogIt 'Info', '_C00_1273_Payments_CreatePremiumRefundRow', 'Implementation Note', @LogEntry, 2372 

	--DECLARE @CustomerID INT,
	--		@MatterID INT
	--SELECT @CustomerID = CustomerID, @MatterID = MatterID
	--FROM dbo.Matter WITH (NOLOCK) 
	--WHERE CaseID = @CaseID
	
	--DECLARE @BankNumber VARCHAR(2000),
	--		@TransitNumber VARCHAR(2000),
	--		@AccountNumber VARCHAR(2000),
	--		--@ReimburseBy INT,
	--		@ProvinceLLID INT
	
	--/* Retrieve bank details */
	--SELECT @BankNumber = cdvBank.DetailValue, @TransitNumber = cdvTrans.DetailValue, @AccountNumber = cdvAcc.DetailValue, @ProvinceLLID=lliProvince.LookupListItemID
	--FROM dbo.Customers c WITH (NOLOCK) 	
	--INNER JOIN dbo.CustomerDetailValues cdvBank WITH (NOLOCK) ON c.CustomerID = cdvBank.CustomerID AND cdvBank.DetailFieldID = 162864
	--INNER JOIN dbo.CustomerDetailValues cdvTrans WITH (NOLOCK) ON c.CustomerID = cdvTrans.CustomerID AND cdvTrans.DetailFieldID = 162865
	--INNER JOIN dbo.CustomerDetailValues cdvAcc WITH (NOLOCK) ON c.CustomerID = cdvAcc.CustomerID AND cdvAcc.DetailFieldID = 156152
	--LEFT JOIN dbo.LookupListItems lliProvince WITH (NOLOCK) on lliProvince.LookupListID = 4127 AND lliProvince.ItemValue = c.County
	--WHERE c.CustomerID = @CustomerID
	
	
	--/* Use Customer default for reimburse method if not explicitly passed */
	--IF @ReimburseBy = NULL
	--BEGIN
	
	--	SELECT @ReimburseBy = ValueInt
	--	FROM dbo.CustomerDetailValues WITH (NOLOCK) 
	--	WHERE CustomerID = @CustomerID 
	--	AND DetailFieldID = 162367

	--END

	--IF @Total > 0
	--BEGIN
	
	--	DECLARE @TableRowID INT	
		
	--	IF @ReimburseBy = 72429 AND @BankNumber > '' AND @TransitNumber > '' AND @AccountNumber > ''
	--	BEGIN
		
	--		-- EFT payment
	--		INSERT INTO dbo.TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
	--		VALUES (384, @CustomerID, 165168, 18213)
			
	--		SELECT @TableRowID = SCOPE_IDENTITY()
			
	--		DECLARE @AccountName VARCHAR(2000)
					
	--		SELECT @AccountName =	CASE
	--									WHEN cdvAcc.DetailValue > '' THEN cdvAcc.DetailValue
	--									ELSE c.Fullname
	--								END
	--		FROM dbo.Customers c WITH (NOLOCK) 
	--		LEFT JOIN dbo.CustomerDetailValues cdvAcc WITH (NOLOCK) ON c.CustomerID = cdvAcc.CustomerID AND cdvAcc.DetailFieldID = 165090
	--		WHERE c.CustomerID = @CustomerID
			
	--		INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID) VALUES
	--		(@TableRowID, 164387, CAST(@Total AS VARCHAR), @CustomerID, 384),						--Amount
	--		(@TableRowID, 165171, CAST(@SubTotal AS VARCHAR), @CustomerID, 384),					--Premium Amount
	--		(@TableRowID, 165172, CAST(@Tax AS VARCHAR), @CustomerID, 384),							--Tax Amount
	--		(@TableRowID, 165170, CAST(@ProvinceLLID AS VARCHAR), @CustomerID, 384),				--Tax Province Code
	--		(@TableRowID, 164388, CONVERT(VARCHAR, dbo.fn_C235_dbo.fn_GetDate_Local(), 120), @CustomerID, 384),	--Date
	--		(@TableRowID, 164390, @BankNumber, @CustomerID, 384),									--Institution Number
	--		(@TableRowID, 164391, @TransitNumber, @CustomerID, 384),								--Transit Number
	--		(@TableRowID, 164392, @AccountNumber, @CustomerID, 384),								--Account Number
	--		(@TableRowID, 164393, @AccountName, @CustomerID, 384)									--Account Name
	--		--,(@TableRowID, 164396, CAST(@MatterID AS VARCHAR), @CustomerID, 384)						--Claim ID (BUT actually PolicyID here)

	--	END
	--	ELSE
	--	BEGIN
		
	--		-- Cheque payment
	--		INSERT INTO dbo.TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
	--		VALUES (384, @CustomerID, 165169, 18213)
			
	--		SELECT @TableRowID = SCOPE_IDENTITY()
			
	--		DECLARE @ChequeNumber INT
	--		EXEC @ChequeNumber = dbo.GetNextSequenceNumber 4, 'RSA Cheque Number'
			
	--		DECLARE @Fullname VARCHAR(2000),	
	--				@Street VARCHAR(2000),
	--				@City VARCHAR(2000),
	--				@Province VARCHAR(2000),
	--				@PostalCode VARCHAR(2000),
	--				@Country VARCHAR(2000),
	--				@Initial VARCHAR(2000),
	--				@FirstName VARCHAR(2000),
	--				@LastName VARCHAR(2000)
					
	--		SELECT	@Fullname = Fullname, @Street = Address1, @City = Town, @Province = County, @Country = 'Canada', @PostalCode = PostCode,
	--				@Initial = t.Title, @FirstName = FirstName, @LastName = LastName
	--		FROM dbo.Customers WITH (NOLOCK) 
	--		LEFT JOIN Titles t WITH (NOLOCK) on t.TitleID = Customers.TitleID
	--		WHERE CustomerID = @CustomerID
			
	--		INSERT INTO dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID) VALUES
	--		(@TableRowID, 164398, CAST(@Total AS VARCHAR), @CustomerID, 384),								--Amount
	--		(@TableRowID, 165184, CAST(@SubTotal AS VARCHAR), @CustomerID, 384),							--Premium Amount
	--		(@TableRowID, 165185, CAST(@Tax AS VARCHAR), @CustomerID, 384),									--Tax
	--		(@TableRowID, 165183, CAST(@ProvinceLLID AS VARCHAR), @CustomerID, 384),						--Tax province code
	--		(@TableRowID, 164399, CONVERT(VARCHAR, dbo.fn_C235_dbo.fn_GetDate_Local(), 120), @CustomerID, 384),			--Date
	--		(@TableRowID, 164401, RIGHT('00000000' + CAST(@ChequeNumber AS VARCHAR), 8), @CustomerID, 384),	--Cheque number
	--		(@TableRowID, 164405, 'C', @CustomerID, 384),													--Language code
	--		(@TableRowID, 164406, @Fullname, @CustomerID, 384),												--Payee 1
	--		(@TableRowID, 164411, 'Premium Refund', @CustomerID, 384),										--Description
	--		(@TableRowID, 164412, '', @CustomerID, 384),													--Street Number
	--		(@TableRowID, 164413, @Street, @CustomerID, 384),												--Street
	--		(@TableRowID, 164414, @City, @CustomerID, 384),													--City
	--		(@TableRowID, 164415, @Province, @CustomerID, 384),												--Province
	--		(@TableRowID, 164416, @PostalCode, @CustomerID, 384),											--Postal Code
	--		(@TableRowID, 164417, @Country, @CustomerID, 384),												--Country
	--		(@TableRowID, 164419, @Initial, @CustomerID, 384),												--Initial (Actually Title)
	--		(@TableRowID, 164420, @FirstName, @CustomerID, 384),											--First Name
	--		(@TableRowID, 164421, @LastName, @CustomerID, 384)												--Last Name	
	--		--,(@TableRowID, 164404, CAST(@MatterID AS VARCHAR), @CustomerID, 384)								--Claim ID	(PolicyID)


	--	END
		
	--END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Payments_CreatePremiumRefundRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Payments_CreatePremiumRefundRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Payments_CreatePremiumRefundRow] TO [sp_executeall]
GO
