SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 17/03/2010
-- Description:	Gets all client personnel records along with their system message assignments
-- =============================================
CREATE PROCEDURE [dbo].[GetClientPersonnelSystemMessageAssignments]
	-- Add the parameters for the stored procedure here
	@ClientID int,
	@SystemMessageID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT cp.ClientPersonnelID, cp.UserName, cp.EmailAddress, sma.SystemMessageID
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	LEFT JOIN dbo.SystemMessageAssignment sma WITH (NOLOCK) 
		ON cp.ClientPersonnelID = sma.ClientPersonnelID AND sma.SystemMessageID = @SystemMessageID
	WHERE cp.ClientID = @ClientID
	--AND (sma.SystemMessageID = @SystemMessageID OR sma.SystemMessageID is null)
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonnelSystemMessageAssignments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientPersonnelSystemMessageAssignments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonnelSystemMessageAssignments] TO [sp_executeall]
GO
