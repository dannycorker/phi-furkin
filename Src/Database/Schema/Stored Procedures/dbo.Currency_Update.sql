SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Currency table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Currency_Update]
(

	@CurrencyID int   ,

	@OriginalCurrencyID int   ,

	@CurrencyCode char (3)  ,

	@CurrencyName varchar (100)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@MajorNamePlural varchar (50)  ,

	@MajorNameSingular varchar (50)  ,

	@MinorNamePlural varchar (50)  ,

	@MinorNameSingular varchar (50)  ,

	@CurrencySymbol nvarchar (2)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Currency]
				SET
					[CurrencyID] = @CurrencyID
					,[CurrencyCode] = @CurrencyCode
					,[CurrencyName] = @CurrencyName
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
					,[MajorNamePlural] = @MajorNamePlural
					,[MajorNameSingular] = @MajorNameSingular
					,[MinorNamePlural] = @MinorNamePlural
					,[MinorNameSingular] = @MinorNameSingular
					,[CurrencySymbol] = @CurrencySymbol
				WHERE
[CurrencyID] = @OriginalCurrencyID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Currency_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Update] TO [sp_executeall]
GO
