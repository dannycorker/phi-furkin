SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all clients
-- =============================================
CREATE PROCEDURE [dbo].[cl] 
	@ClientID int = null,
	@SortBy int = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF @SortBy = 1
	BEGIN
		SELECT * 
		FROM dbo.Clients cl WITH (NOLOCK) 
		WHERE (@ClientID IS NULL OR cl.ClientID = @ClientID)
		ORDER BY 1 DESC
	END
	
	ELSE
	
	BEGIN
		SELECT * 
		FROM dbo.Clients cl WITH (NOLOCK) 
		WHERE (@ClientID IS NULL OR cl.ClientID = @ClientID)
		ORDER BY cl.CompanyName
	END
	
	IF @ClientID is not null /*CS 2012-09-06*/
	BEGIN
		SELECT * 
		FROM ClientOption co WITH (NOLOCK) 
		WHERE co.ClientID = @ClientID
		
		SELECT * 
		FROM ClientInfo co WITH ( NOLOCK ) 
		WHERE co.ClientID = @ClientID
	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[cl] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cl] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cl] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[cl] TO [sp_executehelper]
GO
