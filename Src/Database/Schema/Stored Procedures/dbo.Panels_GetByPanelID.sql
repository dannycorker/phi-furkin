SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Panels table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_GetByPanelID]
(

	@PanelID int   
)
AS


				SELECT
					[PanelID],
					[ClientID],
					[ClientPersonnelID],
					[PanelName]
				FROM
					[dbo].[Panels] WITH (NOLOCK) 
				WHERE
										[PanelID] = @PanelID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_GetByPanelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_GetByPanelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_GetByPanelID] TO [sp_executeall]
GO
