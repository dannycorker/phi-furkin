SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-03
-- Description:	Create a new zip file record
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZipFile__Insert]
	@ClientID int,
	@DocumentZipInformationID int, 
	@ServerMapPath varchar(1000), 
	@WhoStored int  
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/* Assume the download datetime is right now unless told otherwise */
	INSERT dbo.DocumentZipFile (
		ClientID, 
		DocumentZipInformationID, 
		StatusID, 
		ServerMapPath, 
		FileName, 
		WhoStored, 
		WhenStored
	)
	VALUES (
		@ClientID, 
		@DocumentZipInformationID, 
		35, 
		@ServerMapPath, 
		dbo.fnFileOrPath(@ServerMapPath, 1), 
		@WhoStored, 
		dbo.fn_GetDate_Local()
	)
	
	SELECT SCOPE_IDENTITY()
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZipFile__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZipFile__Insert] TO [sp_executeall]
GO
