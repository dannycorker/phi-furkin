SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-07-27
-- Description:	Select AquariumEventTypes filtering RPI or Quill events.
-- Update 2010-08-09 (SB) Added support for Incendia
-- =============================================
CREATE PROCEDURE [dbo].[AquariumEventType__GetAllFiltered] 
	@RPIAllowed bit,
	@QuillPinpointAllowed bit,
	@IncendiaAllowed bit = 0
AS
BEGIN
	
	SELECT AquariumEventTypeID, AquariumEventTypeName, AquariumEventTypeDescription, AquariumStatusAfterEvent, WhenCreated, WhenModified
	FROM dbo.AquariumEventType
	WHERE (@RPIAllowed = 1 OR AquariumEventTypeID != 7)
	AND (@QuillPinpointAllowed = 1 OR AquariumEventTypeID != 8)
	AND (@IncendiaAllowed = 1 OR AquariumEventTypeID != 9)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType__GetAllFiltered] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventType__GetAllFiltered] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType__GetAllFiltered] TO [sp_executeall]
GO
