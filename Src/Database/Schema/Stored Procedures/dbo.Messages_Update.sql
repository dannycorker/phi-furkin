SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Messages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Messages_Update]
(

	@MessageID int   ,

	@ClientPersonnelIDTo int   ,

	@ClientPersonnelIDFrom int   ,

	@DateSent datetime   ,

	@DateRead datetime   ,

	@Status int   ,

	@Subject varchar (255)  ,

	@MessageText varchar (1000)  ,

	@DateReplied datetime   ,

	@DateForwarded datetime   ,

	@PreviousMessageID int   ,

	@ClientID int   ,

	@PortalUserIDTo int   ,

	@PortalUserIDFrom int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Messages]
				SET
					[ClientPersonnelIDTo] = @ClientPersonnelIDTo
					,[ClientPersonnelIDFrom] = @ClientPersonnelIDFrom
					,[DateSent] = @DateSent
					,[DateRead] = @DateRead
					,[Status] = @Status
					,[Subject] = @Subject
					,[MessageText] = @MessageText
					,[DateReplied] = @DateReplied
					,[DateForwarded] = @DateForwarded
					,[PreviousMessageID] = @PreviousMessageID
					,[ClientID] = @ClientID
					,[PortalUserIDTo] = @PortalUserIDTo
					,[PortalUserIDFrom] = @PortalUserIDFrom
				WHERE
[MessageID] = @MessageID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Messages_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Messages_Update] TO [sp_executeall]
GO
