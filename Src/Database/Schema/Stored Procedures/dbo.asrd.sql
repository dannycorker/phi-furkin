SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- ALTER date: 2012-05-03
-- Description:	List all recent AutomatedScriptResults for a task or client
-- =============================================
CREATE PROCEDURE [dbo].[asrd] 
	@AgeInDays int = 1, 
	@ClientID int = NULL, 
	@ScriptID int = NULL, 
	@MaxRows int = 500 
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		EXECUTE AS LOGIN = 'AquariumNet'
		
		EXEC AquariusMaster.dbo.asrd @AgeInDays, @ClientID, @ScriptID, @MaxRows 
		
		REVERT
	END TRY
	BEGIN CATCH
		SELECT 'Please ask a dbo for IMPERSONATE permission on the AquariumNet login' as [Fail]
	END CATCH
	
END

GRANT EXEC ON dbo.asrd TO sp_executeall




GO
GRANT VIEW DEFINITION ON  [dbo].[asrd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[asrd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[asrd] TO [sp_executeall]
GO
