SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleStandardTarget_Delete]
(

	@LeadTypePageTitleStandardTargetID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleStandardTarget] WITH (ROWLOCK) 
				WHERE
					[LeadTypePageTitleStandardTargetID] = @LeadTypePageTitleStandardTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleStandardTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleStandardTarget_Delete] TO [sp_executeall]
GO
