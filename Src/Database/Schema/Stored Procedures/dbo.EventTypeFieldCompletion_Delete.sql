SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeFieldCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_Delete]
(

	@EventTypeFieldCompletionID int   
)
AS


				DELETE FROM [dbo].[EventTypeFieldCompletion] WITH (ROWLOCK) 
				WHERE
					[EventTypeFieldCompletionID] = @EventTypeFieldCompletionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Delete] TO [sp_executeall]
GO
