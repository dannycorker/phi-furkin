SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-08-14
-- Description:	Saves SqlQueryEditingCriteria changes in bulk
-- =============================================
CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria__BulkChanges] 
	@SqlQueryEditingID INT,
	@IsJoinColumnList dbo.tvpIntInt READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	/* The @IsJoinColumnList TVP is list of criteria that need to go onto the LEFT JOIN clause, rather than the WHERE clause of the finished query. */
	UPDATE SqlQueryEditingCriteria 
	SET IsJoinClause = CASE j.ID2 WHEN 1 THEN 1 ELSE 0 END /* Manually cast int to bit, just to be on the safe side. */
	FROM dbo.SqlQueryEditingCriteria c
	INNER JOIN @IsJoinColumnList j ON c.SqlQueryEditingColumnID = j.ID1
	WHERE c.SqlQueryEditingID = @SqlQueryEditingID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria__BulkChanges] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria__BulkChanges] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria__BulkChanges] TO [sp_executeall]
GO
