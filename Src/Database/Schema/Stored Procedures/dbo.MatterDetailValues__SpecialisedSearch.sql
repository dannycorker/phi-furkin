SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 21 September2009
-- Description:	Does a Like Search for matter detail values
-- Edited:		CT: 15022010: changed format of 'LeadDetailValues__SpecialisedSearch' to search and return from MatterDetailValues
-- =============================================
CREATE PROCEDURE [dbo].[MatterDetailValues__SpecialisedSearch]

@ClientID varchar(12),
@DetailFieldID varchar(12),
@FindInformation varchar(max),
@StartRow int = null,
@EndRow int = null

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @query VARCHAR(MAX)
	
	SET @query = '
	SELECT * FROM
	(	
	SELECT ROW_NUMBER() OVER (order by Customers.CustomerID) as RowNumber,dbo.Titles.Title, 
	dbo.Customers.*, dbo.Lead.LeadID, dbo.MatterDetailValues.DetailValue, dbo.Cases.CaseID
	FROM dbo.MatterDetailValues 
	INNER JOIN dbo.Matter ON dbo.Matter.MatterID = dbo.MatterDetailValues.MatterID
	INNER JOIN dbo.Cases ON dbo.Cases.CaseID = dbo.Matter.CaseID
	INNER JOIN dbo.Lead ON dbo.Lead.LeadID = dbo.Cases.LeadID 
	INNER JOIN dbo.Customers ON dbo.Lead.CustomerID = dbo.Customers.CustomerID 
	INNER JOIN dbo.Titles ON dbo.Customers.TitleID = dbo.Titles.TitleID 
	
	where dbo.customers.ClientID=' + @ClientID + ' and dbo.MatterDetailValues.DetailFieldID=' + @DetailFieldID + ' and dbo.Lead.LeadTypeID=1194 and dbo.Customers.Test=0 ' 

	--PR 25-04-2013 - Added LeadType into criteria for Client 252 only LeadTypeID=1194

	IF(@FindInformation is not null)
	BEGIN
		SELECT @query = @query + ' and (' + @FindInformation + ')'		
	END
	SELECT @query = @query + ' ) As Results'

	
	IF (@StartRow is not null AND @EndRow is not null)
	BEGIN
		SELECT @query = @query + ' WHERE RowNumber Between ' + CONVERT(Varchar(10), @StartRow) + ' and ' + CONVERT(Varchar(10), @EndRow)
	END
	
	EXECUTE(@query)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__SpecialisedSearch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues__SpecialisedSearch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__SpecialisedSearch] TO [sp_executeall]
GO
