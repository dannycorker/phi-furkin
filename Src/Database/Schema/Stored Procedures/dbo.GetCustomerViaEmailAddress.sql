SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetCustomerViaEmailAddress] @EmailAddress nvarchar(255)

as

Select Customers.CustomerID, Customers.ClientID, TitleID, FirstName, MiddleName, LastName, EmailAddress, 
DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, 
HomeTelephone, HomeTelephoneVerifiedAndValid, 
MobileTelephone, MobileTelephoneVerifiedAndValid, 
Address1, Address2, Town,
County, PostCode, CustomerQuestionnaires.ClientQuestionnaireID, HasDownLoaded, DownLoadedOn, Test, CompanyName, 
CompanyTelephone, CompanyTelephoneVerifiedAndValid, 
WorksTelephone, WorksTelephoneVerifiedAndValid, 
Occupation, Employer, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions,
PhoneNumbersVerifiedOn, DateOfBirth, IsBusiness, DefaultContactID
From Customers WITH (NOLOCK) INNER JOIN
                      dbo.CustomerQuestionnaires WITH (NOLOCK) ON dbo.Customers.CustomerID = dbo.CustomerQuestionnaires.CustomerID
Where Customers.EmailAddress = @EmailAddress




GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerViaEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerViaEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerViaEmailAddress] TO [sp_executeall]
GO
