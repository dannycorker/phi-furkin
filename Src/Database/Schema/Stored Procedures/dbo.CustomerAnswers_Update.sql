SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CustomerAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_Update]
(

	@CustomerAnswerID int   ,

	@CustomerQuestionnaireID int   ,

	@MasterQuestionID int   ,

	@Answer varchar (2000)  ,

	@QuestionPossibleAnswerID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CustomerAnswers]
				SET
					[CustomerQuestionnaireID] = @CustomerQuestionnaireID
					,[MasterQuestionID] = @MasterQuestionID
					,[Answer] = @Answer
					,[QuestionPossibleAnswerID] = @QuestionPossibleAnswerID
					,[ClientID] = @ClientID
				WHERE
[CustomerAnswerID] = @CustomerAnswerID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_Update] TO [sp_executeall]
GO
