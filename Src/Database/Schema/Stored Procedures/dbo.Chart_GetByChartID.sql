SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Chart table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Chart_GetByChartID]
(

	@ChartID int   
)
AS


				SELECT
					[ChartID],
					[QueryID],
					[ClientID],
					[ChartTitle],
					[ChartDescription],
					[ChartTypeID],
					[XAxisColumn],
					[CreatedBy],
					[CreatedOn],
					[LastEditedBy],
					[LastEditedOn]
				FROM
					[dbo].[Chart] WITH (NOLOCK) 
				WHERE
										[ChartID] = @ChartID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_GetByChartID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chart_GetByChartID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chart_GetByChartID] TO [sp_executeall]
GO
