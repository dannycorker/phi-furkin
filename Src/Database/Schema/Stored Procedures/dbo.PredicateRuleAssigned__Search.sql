SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Predicate Rule Assigned Search
-- =============================================
CREATE PROCEDURE [dbo].[PredicateRuleAssigned__Search] 
	@ClientID INT,
	@LeadTypeID INT,
	@EventTypeID INT,
	@PageSize INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT	TOP(@PageSize) pra.*, et.EventTypeName, ISNULL(dt.DocumentTypeName, '[No Document]') DocumentTypeName, 
		ISNULL(pr.RuleName, '[Always True]') RuleName, 
		ISNULL(dto.DocumentTypeName, '[No Document]') OverrideDocumentName
	FROM	PredicateRuleAssigned pra WITH (NOLOCK)
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = pra.LeadTypeID
	INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = pra.EventTypeID
	LEFT JOIN DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = pra.DocumentTypeID
	LEFT JOIN PredicateRule pr WITH (NOLOCK) ON pr.PredicateRuleID = pra.PredicateRuleID
	LEFT JOIN DocumentType dto WITH (NOLOCK) ON dto.DocumentTypeID = pra.OverrideDocumentTypeID
	WHERE	pra.ClientID = @ClientID
	AND		pra.LeadTypeID = @LeadTypeID
	AND		pra.EventTypeID = @EventTypeID
	ORDER BY dt.DocumentTypeName, pra.RuleOrder, pra.PredicateRuleAssignedID, pr.RuleName, dto.DocumentTypeName

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRuleAssigned__Search] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PredicateRuleAssigned__Search] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PredicateRuleAssigned__Search] TO [sp_executeall]
GO
