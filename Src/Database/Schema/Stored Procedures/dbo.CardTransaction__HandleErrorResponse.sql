SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson/James Lewis
-- Create date: 31/01/2016
-- Description:	Handles card transaction errors
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__HandleErrorResponse]
	@CardTransactionID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @StatusMsg VARCHAR(250),
			@ErrorCode VARCHAR(100),
			@ErrorMessage VARCHAR(500),
			@CustomerID INT,
			@ObjectTypeID INT,
			@ObjectID INT,
			@ClientID INT
						
	SELECT	@StatusMsg=StatusMsg, 
			@ErrorCode=ErrorCode, 
			@ErrorMessage=ErrorMessage, 
			@CustomerID=CustomerID,
			@ObjectTypeID=ObjectTypeID, 
			@ObjectID=ObjectID,
			@ClientID=ClientID
			FROM CardTransaction WITH (NOLOCK) WHERE CardTransactionID=@CardTransactionID
	
	DECLARE @LeadID INT, @MatterID INT
	IF(@ObjectTypeID=1) -- lead id
	BEGIN
		SELECT @LeadID=@ObjectID
	END
	ELSE IF (@ObjectTypeID=2) -- matter id
	BEGIN
		SELECT @MatterID=@ObjectID
	END
    
    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__HandleErrorResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__HandleErrorResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__HandleErrorResponse] TO [sp_executeall]
GO
