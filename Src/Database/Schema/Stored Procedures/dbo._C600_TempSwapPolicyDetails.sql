SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-16
-- Description: Swap temporary Policy details to their permanent location - used for generating a quote during an MTA, same format as _C600_TempSwapPHandPetDetails
-- =============================================
CREATE PROCEDURE [dbo].[_C600_TempSwapPolicyDetails]
(
	@LeadID INT
)

AS
BEGIN


	SET NOCOUNT ON;

	EXEC [dbo].[_C00_LogIt] 'Info', '_C600_TempSwapPolicyDetails', 'LeadID', @LeadID, 58552

	DECLARE  @TempValue VARCHAR(200)
			,@PermanentValue VARCHAR(200)
			,@WhoCreated INT = 58552 /*Aquarium Automation*/
			,@TempValueInt INT
			,@PermanentValueInt INT
			,@MatterID INT


	SELECT @MatterID = (SELECT MatterID FROM Matter WITH (NOLOCK)
						INNER JOIN Lead WITH (NOLOCK) ON Lead.LeadID = Matter.LeadID
						AND Lead.LeadID = @LeadID)
				
	EXEC [dbo].[_C00_LogIt] 'Info', '_C600_TempSwapPolicyDetails', 'MatterID', @MatterID, 58552

/*Switch Temp / Perm values*/
	
	/*Deductible*/
	SELECT @TempValue = dbo.fnGetSimpleDvLuli(314242,@LeadID) /*Endorse Policy Deductible*/

	IF ISNULL(@TempValue,'') > ''
	BEGIN 
		
		SELECT @PermanentValue = dbo.fnGetSimpleDv(313932,@LeadID)
		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 313932, @TempValue, @LeadID /*Selected Deductible Amount (FreeText)*/
		EXEC dbo._C00_SimpleValueIntoField 314242, @PermanentValue, @LeadID /*Endorse Policy Deductible (LuLi)*/
	
	END

	/*CoPay*/
	SELECT @TempValue = dbo.fnGetSimpleDvLuli(314243,@LeadID) /*Endorse Policy Copay*/

	IF CAST(@TempValue AS INT) >= 0
	BEGIN 
		
		SELECT @PermanentValue = dbo.fnGetSimpleDv(313930,@LeadID)
		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 313930, @TempValue, @LeadID /*Selected CoPay*/
		EXEC dbo._C00_SimpleValueIntoField 314243, @PermanentValue, @LeadID /*Endorse Policy Copay*/
	END

	/*Exam Fees*/
	SELECT @TempValueInt = dbo.fnGetSimpleDvAsInt(314245,@LeadID) /*Endorse Policy Exam Fees*/
	SELECT @PermanentValueInt = dbo.fnGetSimpleDvAsInt(313931,@LeadID)

	IF ISNULL(@TempValueInt,0) <> 0 /*GPR 2020-12-16 for SDPRU-197*/ AND @TempValueInt <> @PermanentValueInt /*GPR 2020-10-13*/
	BEGIN 
		
		--SELECT @PermanentValueInt = dbo.fnGetSimpleDvAsInt(313931,@LeadID)

		IF @TempValueInt = 0
		BEGIN
			SET @TempValueInt = 5145
		END
		IF @PermanentValueInt = 0
		BEGIN
			SET @PermanentValueInt = 5145
		END

		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 313931, @TempValueInt, @LeadID /*Exam Fees*/
		EXEC dbo._C00_SimpleValueIntoField 314245, @PermanentValueInt, @LeadID /*Endorse Policy Exam Fees*/

	END


	/*Wellness*/
	SELECT @TempValueInt = dbo.fnGetSimpleDvAsInt(314244,@LeadID) /*Endorse Policy Wellness*/

	IF @TempValueInt >= 0
	BEGIN 

		SELECT @PermanentValueInt = dbo.fnGetSimpleDvAsInt(314235,@MatterID) /*Current Wellness Product*/

		/*Switch the temp and permanent value*/

		IF @TempValueInt = 193319 /*No Wellness*/
		BEGIN
			EXEC _C00_SimpleValueIntoField 314015, 5145, @LeadID -- Low, No
			EXEC _C00_SimpleValueIntoField 314013, 5145, @LeadID -- Medium, No
			EXEC _C00_SimpleValueIntoField 314014, 5145, @LeadID -- High, No

			EXEC _C00_SimpleValueIntoField 314235, 0, @MatterID
		END

		IF @TempValueInt = 193320 /*Low Wellness*/
		BEGIN
			EXEC _C00_SimpleValueIntoField 314015, 5144, @LeadID -- Low, Yes
			EXEC _C00_SimpleValueIntoField 314013, 5145, @LeadID -- Medium, No
			EXEC _C00_SimpleValueIntoField 314014, 5145, @LeadID -- High, No

			EXEC _C00_SimpleValueIntoField 314235, 2002261, @MatterID
		END

		IF @TempValueInt = 193321 /*Medium Wellness*/
		BEGIN
			EXEC _C00_SimpleValueIntoField 314015, 5145, @LeadID -- Low, No
			EXEC _C00_SimpleValueIntoField 314013, 5144, @LeadID -- Medium, Yes
			EXEC _C00_SimpleValueIntoField 314014, 5145, @LeadID -- High, No

			EXEC _C00_SimpleValueIntoField 314235, 2002262, @MatterID
		END

		IF @TempValueInt = 193322 /*High Wellness*/
		BEGIN
			EXEC _C00_SimpleValueIntoField 314015, 5145, @LeadID -- Low, No
			EXEC _C00_SimpleValueIntoField 314013, 5145, @LeadID -- Medium, No
			EXEC _C00_SimpleValueIntoField 314014, 5144, @LeadID -- High, No

			EXEC _C00_SimpleValueIntoField 314235, 2002263, @MatterID

		END


		--IF @PermanentValueInt = 2002263 /*High*/
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 314244, 193322 /*High*/, @LeadID
		--END
		
		--IF @PermanentValueInt = 2002262 /*Medium*/
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 314244, 193321 /*Medium*/, @LeadID
		--END
		
		--IF @PermanentValueInt = 2002261 /*Low*/
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 314244, 193320 /*Low*/, @LeadID
		--END
		--IF @PermanentValue = 0
		--BEGIN
		--	EXEC _C00_SimpleValueIntoField 314244, 193319 /*No Wellness*/, @LeadID
		--END		
	
	END
	


	/*Product / Scheme*/
	SELECT @TempValueInt = dbo.fnGetSimpleDvAsInt(314327,@LeadID) /*Endorse Policy Product*/

	EXEC [dbo].[_C00_LogIt] 'Info', '_C600_TempSwapPolicyDetails', 'TempProduct', @TempValueInt, 58552

	IF ISNULL(@TempValueInt,0) <> 0
	BEGIN 
		
		SELECT @PermanentValueInt = dbo.fnGetSimpleDvAsInt(170034,@MatterID) /*Current Policy*/

		EXEC [dbo].[_C00_LogIt] 'Info', '_C600_TempSwapPolicyDetails', 'PermProduct', @PermanentValueInt, 58552

		/*Switch the temp and permanent value*/		
		EXEC dbo._C00_SimpleValueIntoField 170034, @TempValueInt, @MatterID /*Current Policy*/
		EXEC dbo._C00_SimpleValueIntoField 314327, @PermanentValueInt, @LeadID /*Endorse Policy Product*/
	
	END


	
END
GO
GRANT EXECUTE ON  [dbo].[_C600_TempSwapPolicyDetails] TO [sp_executeall]
GO
