SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset Sub Type List
-- =============================================
CREATE PROCEDURE [dbo].[GetAssetSubTypeList] 
	@ClientPersonnelID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ast.[AssetSubTypeID],
		ast.[AssetSubTypeName],
		ast.[Note],
		ast.[Enabled],
		ast.[Deleted],
		ast.[WhoCreated],
		ast.[WhenCreated],
		ast.[WhoModified],
		ast.[WhenModified]
	FROM 
		[dbo].[AssetSubType] ast WITH (NOLOCK),
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND ast.[AssetSubTypeID] = a.[AssetSubTypeID]
	ORDER BY ast.[AssetSubTypeID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetAssetSubTypeList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAssetSubTypeList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAssetSubTypeList] TO [sp_executeall]
GO
