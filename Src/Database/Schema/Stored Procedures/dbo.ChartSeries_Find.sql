SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChartSeries table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartSeriesID int   = null ,

	@ChartSeriesTypeID int   = null ,

	@ChartSeriesOrder int   = null ,

	@ChartID int   = null ,

	@ClientID int   = null ,

	@PanelItemChartingID int   = null ,

	@ColumnName varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartSeriesID]
	, [ChartSeriesTypeID]
	, [ChartSeriesOrder]
	, [ChartID]
	, [ClientID]
	, [PanelItemChartingID]
	, [ColumnName]
    FROM
	[dbo].[ChartSeries] WITH (NOLOCK) 
    WHERE 
	 ([ChartSeriesID] = @ChartSeriesID OR @ChartSeriesID IS NULL)
	AND ([ChartSeriesTypeID] = @ChartSeriesTypeID OR @ChartSeriesTypeID IS NULL)
	AND ([ChartSeriesOrder] = @ChartSeriesOrder OR @ChartSeriesOrder IS NULL)
	AND ([ChartID] = @ChartID OR @ChartID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PanelItemChartingID] = @PanelItemChartingID OR @PanelItemChartingID IS NULL)
	AND ([ColumnName] = @ColumnName OR @ColumnName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartSeriesID]
	, [ChartSeriesTypeID]
	, [ChartSeriesOrder]
	, [ChartID]
	, [ClientID]
	, [PanelItemChartingID]
	, [ColumnName]
    FROM
	[dbo].[ChartSeries] WITH (NOLOCK) 
    WHERE 
	 ([ChartSeriesID] = @ChartSeriesID AND @ChartSeriesID is not null)
	OR ([ChartSeriesTypeID] = @ChartSeriesTypeID AND @ChartSeriesTypeID is not null)
	OR ([ChartSeriesOrder] = @ChartSeriesOrder AND @ChartSeriesOrder is not null)
	OR ([ChartID] = @ChartID AND @ChartID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PanelItemChartingID] = @PanelItemChartingID AND @PanelItemChartingID is not null)
	OR ([ColumnName] = @ColumnName AND @ColumnName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Find] TO [sp_executeall]
GO
