SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-09-05
-- Description:	Saves the updated column order for the order by columns in the sql query editing tables
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__SaveSortColumnOrdering]
(
	@SqlQueryEditingID INT,
	@NewColumnOrder dbo.tvpIDValue READONLY
)
AS

UPDATE c
SET c.SortOrder = o.AnyID
FROM dbo.SqlQueryEditingColumns c
INNER JOIN @NewColumnOrder o ON c.SqlQueryEditingColumnID = CAST(o.AnyValue AS INT)
WHERE c.SqlQueryEditingID = @SqlQueryEditingID





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__SaveSortColumnOrdering] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__SaveSortColumnOrdering] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__SaveSortColumnOrdering] TO [sp_executeall]
GO
