SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferRestrictedField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_GetByCaseTransferRestrictedFieldID]
(

	@CaseTransferRestrictedFieldID int   
)
AS


				SELECT
					[CaseTransferRestrictedFieldID],
					[ClientRelationshipID],
					[DetailFieldID]
				FROM
					[dbo].[CaseTransferRestrictedField] WITH (NOLOCK) 
				WHERE
										[CaseTransferRestrictedFieldID] = @CaseTransferRestrictedFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_GetByCaseTransferRestrictedFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_GetByCaseTransferRestrictedFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_GetByCaseTransferRestrictedFieldID] TO [sp_executeall]
GO
