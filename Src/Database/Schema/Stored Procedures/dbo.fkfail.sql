SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2012-08-24
-- Description:	Describe the SQL to query why a foreign key failed to create
-- =============================================
CREATE PROCEDURE [dbo].[fkfail] 
	@Error VARCHAR(1000),
	@Debug BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
	@SqlTest VARCHAR(1000) = '',
	@ChildTable VARCHAR(1000),
	@ParentTable VARCHAR(1000),
	@ChildColumn VARCHAR(1000),
	@ParentColumn VARCHAR(1000),
	@TextAlterTable VARCHAR(20),
	@TextAddConstraint VARCHAR(20),
	@TextForeignKey VARCHAR(20),
	@TextReferences VARCHAR(20),
	@TextParentColumnOpenBracket VARCHAR(20),
	@TextParentColumnClosingBracket VARCHAR(20),
	@PosAlterTable INT,
	@PosAddConstraint INT,
	@PosForeignKey INT,
	@PosReferences INT,
	@PosParentColumnOpenBracket INT,
	@PosParentColumnClosingBracket INT,
	@StartOfSubstring INT,
	@LengthOfSubstring INT
	
	/*
		This is an example message:
		
		ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_ClientPersonnel] FOREIGN KEY ([WhoDeleted]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
	*/
	SELECT 
	@TextAlterTable = 'ALTER TABLE ',
	@TextAddConstraint = ' ADD CONSTRAINT ',
	@TextForeignKey = ' FOREIGN KEY (',
	@TextReferences = ') REFERENCES ',
	@TextParentColumnOpenBracket = ' (',
	@TextParentColumnClosingBracket = ')'

	/*
		The aim is to parse the message and turn it into a select statement that will show the invalid records, like this:
		
		SELECT * 
		FROM [dbo].[LeadEvent] t
		WHERE NOT EXISTS (
			SELECT * 
			FROM [dbo].[ClientPersonnel] pt 
			WHERE pt.[ClientPersonnelID] = t.[WhoDeleted]
		)
		AND t.[WhoDeleted] IS NOT NULL
	*/
	SELECT @PosAlterTable = CHARINDEX(@TextAlterTable, @Error, 1)
	SELECT @PosAddConstraint = CHARINDEX(@TextAddConstraint, @Error, @PosAlterTable)
	IF @PosAlterTable > 0 AND @PosAddConstraint > @PosAlterTable
	BEGIN

		/* Child Table */
		SELECT @StartOfSubstring = @PosAlterTable + LEN(@TextAlterTable) + 1 /* LEN ignores trailing spaces, so add 1 to strings that end in a single space */
		SELECT @LengthOfSubstring = @PosAddConstraint - @StartOfSubstring 
		SELECT @ChildTable = SUBSTRING(@Error, @StartOfSubstring, @LengthOfSubstring)
		
		IF @Debug = 1
		BEGIN
			SELECT @StartOfSubstring AS StartOfSubstring, @LengthOfSubstring AS LengthOfSubstring, @ChildTable AS ChildTable
		END

		SELECT @PosForeignKey = CHARINDEX(@TextForeignKey, @Error, @PosAddConstraint)
		SELECT @PosReferences = CHARINDEX(@TextReferences, @Error, @PosForeignKey)
		IF @PosForeignKey > 0 AND @PosReferences > @PosForeignKey
		BEGIN

			/* Child Column */
			SELECT @StartOfSubstring = @PosForeignKey + LEN(@TextForeignKey)
			SELECT @LengthOfSubstring = @PosReferences - @StartOfSubstring 
			SELECT @ChildColumn = SUBSTRING(@Error, @StartOfSubstring, @LengthOfSubstring)
			
			IF @Debug = 1
			BEGIN
				SELECT @StartOfSubstring AS StartOfSubstring, @LengthOfSubstring AS LengthOfSubstring, @ChildColumn AS ChildColumn
			END
			
			/* Parent Table.Column */
			SELECT @PosParentColumnOpenBracket = CHARINDEX(@TextParentColumnOpenBracket, @Error, @PosReferences + LEN(@TextReferences))
			SELECT @PosParentColumnClosingBracket = CHARINDEX(@TextParentColumnClosingBracket, @Error, @PosParentColumnOpenBracket + LEN(@TextParentColumnOpenBracket))
			IF @PosParentColumnOpenBracket > 0 AND @PosParentColumnClosingBracket > @PosParentColumnOpenBracket
			BEGIN

				/* Parent Table */
				SELECT @StartOfSubstring = @PosReferences + LEN(@TextReferences) + 1 /* LEN ignores trailing spaces, so add 1 to strings that end in a single space */
				SELECT @LengthOfSubstring = @PosParentColumnOpenBracket - @StartOfSubstring 
				SELECT @ParentTable = SUBSTRING(@Error, @StartOfSubstring, @LengthOfSubstring)
				
				IF @Debug = 1
				BEGIN
					SELECT @StartOfSubstring AS StartOfSubstring, @LengthOfSubstring AS LengthOfSubstring, @ParentTable AS ParentTable
				END
				
				/* Parent Column */
				SELECT @StartOfSubstring = @PosParentColumnOpenBracket + LEN(@TextParentColumnOpenBracket)
				SELECT @LengthOfSubstring = @PosParentColumnClosingBracket - @StartOfSubstring 
				SELECT @ParentColumn = SUBSTRING(@Error, @StartOfSubstring, @LengthOfSubstring)
				
				IF @Debug = 1
				BEGIN
					SELECT @StartOfSubstring AS StartOfSubstring, @LengthOfSubstring AS LengthOfSubstring, @ParentColumn AS ParentColumn
				END
				
			END
			ELSE
			BEGIN
				SELECT @SqlTest = 'Failed to detect ' + @TextParentColumnOpenBracket + ' and ' + @TextParentColumnClosingBracket + ' after ' + @TextReferences + ' within the string passed in.'
			END
			
		END
		ELSE
		BEGIN
			SELECT @SqlTest = 'Failed to detect ' + @TextForeignKey + ' and ' + @TextReferences + ' within the string passed in.'
		END
		
	END
	ELSE
	BEGIN
		SELECT @SqlTest = 'Failed to detect ' + @TextAlterTable + ' and ' + @TextAddConstraint + ' within the string passed in.'
	END
	
	/* If no errors were found then build the sql statement ready for use */
	IF @SqlTest = ''
	BEGIN
		SELECT 
		@SqlTest = '
SELECT * 
FROM ' + @ChildTable + ' t
WHERE NOT EXISTS (
	SELECT * 
	FROM ' + @ParentTable + ' pt 
	WHERE pt.' + @ParentColumn + ' = t.' + @ChildColumn + '
)
AND t.' + @ChildColumn + ' IS NOT NULL
'
	END
	
	IF @Debug = 1
	BEGIN
		SELECT @SqlTest as 'See Messages for formatted results'
	END
	
	PRINT @SqlTest

	PRINT ''
	
	PRINT 'BEGIN TRAN
	'
	
	SELECT @SqlTest = 'DELETE ' + @ChildTable + RIGHT(@SqlTest, LEN(@SqlTest) - 10) 
	
	PRINT @SqlTest
	
	PRINT ''
	PRINT 'ROLLBACK'
	PRINT 'COMMIT'
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fkfail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fkfail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fkfail] TO [sp_executeall]
GO
