SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UploadedFile table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_GetByUploadedFileGUID]
(

	@UploadedFileGUID uniqueidentifier   
)
AS


				SELECT
					[UploadedFileID],
					[ClientID],
					[FolderID],
					[UploadedFileName],
					[UploadedFileTitle],
					[UploadedFileDescription],
					[UploadedFile],
					[UploadedFileGUID],
					[UploadedFileSize],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[UploadedFile] WITH (NOLOCK) 
				WHERE
										[UploadedFileGUID] = @UploadedFileGUID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_GetByUploadedFileGUID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_GetByUploadedFileGUID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_GetByUploadedFileGUID] TO [sp_executeall]
GO
