SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-24
-- Description:	Gets the policy cover periods for a specific claim
-- Used by:		Calculation
-- ROH 2014-04-30 Get Policy Type from the Scheme that applies to the current claim (which may differ from current policy for up/downgrades)
-- ROH 2014-05-20 Change to the cover end date for 12-month policies. For other types of policy the end date is exclusive, ie out of cover.
--                12-month cover should be the same so the end date should be 12 months after onset, not 12 months minus 1 day  Zen#26723
-- ROH 2014-06-05 Correction to date matching (to excude policy year end date) for reinstatement/MaxBen policies Zen#27135
-- SB  2014-07-30 Added in per condition per policy year for other policy types
-- SB  2015-06-25 Fixed end dates to always be out of cover.  Changes where @LastTreatment is used. ZD #33065
-- SB  2015-12-17 Need inception date for policy age excess so returning it here
-- JL  2016-11-15 Removed IF block around multi years for excess as not required.
-- JL  2018-01-05 Changed LULI IDs to be relevant
-- CPS 2019-03-15 Mark end date as in cover to bring in line with other implementations #55524
-- 2019-07-31 CPS for Zendesk #58497	| Testing for 12 month cover range
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetGroupedPolicyCover] 
(
	@MatterID INT,
	@LeadID INT,
	@CurrentPolicyMatterID INT = NULL,
	@PolicyType INT = NULL,
	@FirstTreatment DATE = NULL, 
	@LastTreatment DATE = NULL,
	@MaxBenLimitToTreatmentDates BIT = NULL,
	@ReturnMultipleYearsForExcess BIT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @CurrentPolicyMatterID IS NULL
	BEGIN
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	END
	
	IF @PolicyType IS NULL
	BEGIN
		--SELECT @PolicyType = dbo.fn_C00_1272_GetPolicyTypeFromClaim(@MatterID)
		SELECT @PolicyType = dbo.fn_C00_1272_GetPolicyTypeFromScheme(@CurrentPolicyMatterID)
	END
	
	IF @FirstTreatment IS NULL
	BEGIN
		SELECT @FirstTreatment = ValueDate
		FROM dbo.MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 144366
		
		SELECT @LastTreatment = ValueDate
		FROM dbo.MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 145674
	END
	
	-- We need the earliest policy start and latest policy end for 12 month and max benefit calcs
	DECLARE @EarliestStart DATE,
			@LatestEnd DATE,
			@InceptionDate DATE
	SELECT @EarliestStart = MIN(tdvStart.ValueDate), @LatestEnd = MAX(tdvEnd.ValueDate), @InceptionDate = MIN(tdvIncept.ValueDate)
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r 
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	INNER JOIN dbo.TableDetailValues tdvIncept WITH (NOLOCK) ON r.TableRowID = tdvIncept.TableRowID AND tdvIncept.DetailFieldID = 145662
	
	DECLARE @PolicyCover TABLE
	(
		PolicyType VARCHAR(50),
		StartDate DATE,
		EndDate DATE,
		InceptionDate DATE
	)
	
	IF @PolicyType = 76175--42931 -- 12 month policy
	BEGIN

		DECLARE @12MonthCoverStartDate DATE
				,@12MonthCoverStartDateType INT --LookupListItemID for this setting
				,@ConditionDateOfLoss DATE
				,@ConditionTreatmentStart DATE

		-- Retrieve the "12 month cover start"  setting
		SELECT @12MonthCoverStartDateType = ISNULL(Scheme12MonthCoverStart.ValueInt,0)
		FROM MatterDetailValues Scheme12MonthCoverStart WITH (NOLOCK)
		WHERE Scheme12MonthCoverStart.MatterID = @CurrentPolicyMatterID
		AND Scheme12MonthCoverStart.DetailFieldID =   162632 
		
		-- Get the onset date (easy) and the earliest condition treatment start date (trickier)
		-- Use the setting to decide which date to use and set the cover dates as appropriate
		SELECT @ConditionDateOfLoss = MIN(mdvDateOfLoss.ValueDate), @ConditionTreatmentStart = MIN(mdvTreatmentStart.ValueDate)
		FROM dbo.fn_C00_1272_GetRelatedClaims(@MatterID,1) mRelated
		INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON mdvDateOfLoss.MatterID = mRelated.MatterID AND mdvDateOfLoss.DetailFieldID = 144892
		INNER JOIN dbo.MatterDetailValues mdvTreatmentStart WITH (NOLOCK) ON mdvTreatmentStart.MatterID = mRelated.MatterID AND mdvTreatmentStart.DetailFieldID = 144366
		
		SELECT @12MonthCoverStartDate = CASE @12MonthCoverStartDateType WHEN 69457 THEN @ConditionTreatmentStart ELSE @ConditionDateOfLoss END
		
		-- Removed this IF block as PHI always want multiple years
		--IF @ReturnMultipleYearsForExcess = 1
		--BEGIN
		
		-- Include the two policy years that are covering the 12 months and adjusting the start and end dates accordingly
		INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
		SELECT @PolicyType, @12MonthCoverStartDate, DATEADD(mm, 12, @12MonthCoverStartDate), @InceptionDate
-- 2019-07-31 CPS for Zendesk #58497	| Testing for 12 month cover range

		--SELECT	'12 month', 
		--		CASE 
		--			WHEN @12MonthCoverStartDate > tdvStart.ValueDate THEN @12MonthCoverStartDate
		--			ELSE tdvStart.ValueDate
		--		END, 
		--		CASE
		--			WHEN DATEADD(YEAR, 1, @12MonthCoverStartDate) < tdvEnd.ValueDate THEN DATEADD(YEAR, 1, @12MonthCoverStartDate)
		--			ELSE tdvEnd.ValueDate
		--		END, @InceptionDate
		--FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
		--INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
		--INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
		--WHERE @12MonthCoverStartDate < tdvEnd.ValueDate
		--AND DATEADD(YEAR, 1, @12MonthCoverStartDate) >= tdvStart.ValueDate
		
		
		--END
		--ELSE
		--BEGIN
		--	-- We take the date of onset as the start and 12 months later as the end
			--INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
			----Zen#26723
			----VALUES ('12 month', @12MonthCoverStartDate, DATEADD(DAY, -1, DATEADD(YEAR, 1, @12MonthCoverStartDate)))
			--VALUES ('12 month', @12MonthCoverStartDate, DATEADD(YEAR, 1, @12MonthCoverStartDate), @InceptionDate)
			
			---- If the policy started after the date of onset then we can delete this policy cover row so the claim rows are all zeroed out
			--DELETE @PolicyCover
			--WHERE @EarliestStart > StartDate
			--OR @LatestEnd < StartDate
			---- If the date of onset + 12 months is later than the end date for the last policy then we limit the policy cover to the policy end date
			--UPDATE @PolicyCover
			--SET EndDate = @LatestEnd
			--WHERE EndDate > @LatestEnd
			
		--END
		
	END
	ELSE IF @PolicyType = 76174-- 42932 -- Max benefit
	BEGIN
	
		--IF @ReturnMultipleYearsForExcess = 1
		--BEGIN
		
				INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
		    	SELECT 'Max benefit', @EarliestStart, @LatestEnd, @InceptionDate

			-- This scenario will be the list of policy start and end dates that cover the treatment period	- this is so Sainsbury's can have a new excess each policy year
		/*	INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
			SELECT	'Max benefit', 
					CASE
						WHEN @MaxBenLimitToTreatmentDates = 1 AND @FirstTreatment > tdvStart.ValueDate THEN @FirstTreatment
						ELSE tdvStart.ValueDate
					END, 
					CASE
						WHEN @MaxBenLimitToTreatmentDates = 1 AND @LastTreatment < tdvEnd.ValueDate THEN DATEADD(DAY, 1, @LastTreatment) -- #33065 THEN @LastTreatment
						ELSE tdvEnd.ValueDate
					END, @InceptionDate
			FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
			INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
			INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
			WHERE @FirstTreatment < tdvEnd.ValueDate
			AND @LastTreatment >= tdvStart.ValueDate  */

		
		--END
		--ELSE
		--BEGIN
	
		--	IF @MaxBenLimitToTreatmentDates = 1
		--	BEGIN
			
		--		IF @FirstTreatment < @EarliestStart
		--		BEGIN
		--			SELECT @FirstTreatment = @EarliestStart
		--		END
				
		--		/*
		--		#33065
		--		IF @LastTreatment > @LatestEnd
		--		BEGIN
		--			SELECT @LastTreatment = @LatestEnd
		--		END
		--		*/
		--		IF @LastTreatment >= @LatestEnd
		--		BEGIN
		--			SELECT @LastTreatment = DATEADD(DAY, -1, @LatestEnd) -- set to the day before the latest end so we can safely add a day on below
		--		END
				
		--		INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
		--		SELECT 'Max benefit', @FirstTreatment, DATEADD(DAY, 1, @LastTreatment), @InceptionDate -- #33065 @LastTreatment
			
		--	END
		--	ELSE
		--	BEGIN
			
		--		-- Max benefit will be from earliest policy start to latest policy end
		--		INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
		--		SELECT 'Max benefit', @EarliestStart, @LatestEnd, @InceptionDate
			
		--	END
			
		--END
	
	END
	ELSE IF @PolicyType IN (76155,76176,76184)-- 42933 -- Reinstatement
	BEGIN

		-- Reinstatement will be the list of policy start and end dates that cover the treatment period		
		INSERT @PolicyCover (PolicyType, StartDate, EndDate, InceptionDate)
		SELECT 'Reinstatement', tdvStart.ValueDate, tdvEnd.ValueDate, @InceptionDate
		FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r 
		INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
		INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
		WHERE @FirstTreatment <= tdvEnd.ValueDate -- CPS 2019-03-15 #55524
		AND @LastTreatment >= tdvStart.ValueDate		

		
	END
	
	SELECT *
	FROM @PolicyCover

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyCover] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyCover] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyCover] TO [sp_executeall]
GO
