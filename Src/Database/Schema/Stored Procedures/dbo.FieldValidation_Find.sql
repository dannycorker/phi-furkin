SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FieldValidation table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_Find]
(

	@SearchUsingOR bit   = null ,

	@FieldValidationID int   = null ,

	@ClientID int   = null ,

	@FieldID int   = null ,

	@ValidationGroup varchar (50)  = null ,

	@ValidationType int   = null ,

	@MaxValue int   = null ,

	@MinValue int   = null ,

	@ErrorMessage varchar (50)  = null ,

	@AccessLevelRestrictions varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FieldValidationID]
	, [ClientID]
	, [FieldID]
	, [ValidationGroup]
	, [ValidationType]
	, [MaxValue]
	, [MinValue]
	, [ErrorMessage]
	, [AccessLevelRestrictions]
    FROM
	[dbo].[FieldValidation] WITH (NOLOCK) 
    WHERE 
	 ([FieldValidationID] = @FieldValidationID OR @FieldValidationID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FieldID] = @FieldID OR @FieldID IS NULL)
	AND ([ValidationGroup] = @ValidationGroup OR @ValidationGroup IS NULL)
	AND ([ValidationType] = @ValidationType OR @ValidationType IS NULL)
	AND ([MaxValue] = @MaxValue OR @MaxValue IS NULL)
	AND ([MinValue] = @MinValue OR @MinValue IS NULL)
	AND ([ErrorMessage] = @ErrorMessage OR @ErrorMessage IS NULL)
	AND ([AccessLevelRestrictions] = @AccessLevelRestrictions OR @AccessLevelRestrictions IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FieldValidationID]
	, [ClientID]
	, [FieldID]
	, [ValidationGroup]
	, [ValidationType]
	, [MaxValue]
	, [MinValue]
	, [ErrorMessage]
	, [AccessLevelRestrictions]
    FROM
	[dbo].[FieldValidation] WITH (NOLOCK) 
    WHERE 
	 ([FieldValidationID] = @FieldValidationID AND @FieldValidationID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FieldID] = @FieldID AND @FieldID is not null)
	OR ([ValidationGroup] = @ValidationGroup AND @ValidationGroup is not null)
	OR ([ValidationType] = @ValidationType AND @ValidationType is not null)
	OR ([MaxValue] = @MaxValue AND @MaxValue is not null)
	OR ([MinValue] = @MinValue AND @MinValue is not null)
	OR ([ErrorMessage] = @ErrorMessage AND @ErrorMessage is not null)
	OR ([AccessLevelRestrictions] = @AccessLevelRestrictions AND @AccessLevelRestrictions is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_Find] TO [sp_executeall]
GO
