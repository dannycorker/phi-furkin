SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuestionnaireColours table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_Find]
(

	@SearchUsingOR bit   = null ,

	@QuestionnaireColourID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@Page varchar (50)  = null ,

	@Header varchar (50)  = null ,

	@Intro varchar (50)  = null ,

	@Body varchar (50)  = null ,

	@Footer varchar (50)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuestionnaireColourID]
	, [ClientQuestionnaireID]
	, [Page]
	, [Header]
	, [Intro]
	, [Body]
	, [Footer]
	, [ClientID]
    FROM
	[dbo].[QuestionnaireColours] WITH (NOLOCK) 
    WHERE 
	 ([QuestionnaireColourID] = @QuestionnaireColourID OR @QuestionnaireColourID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([Page] = @Page OR @Page IS NULL)
	AND ([Header] = @Header OR @Header IS NULL)
	AND ([Intro] = @Intro OR @Intro IS NULL)
	AND ([Body] = @Body OR @Body IS NULL)
	AND ([Footer] = @Footer OR @Footer IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuestionnaireColourID]
	, [ClientQuestionnaireID]
	, [Page]
	, [Header]
	, [Intro]
	, [Body]
	, [Footer]
	, [ClientID]
    FROM
	[dbo].[QuestionnaireColours] WITH (NOLOCK) 
    WHERE 
	 ([QuestionnaireColourID] = @QuestionnaireColourID AND @QuestionnaireColourID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([Page] = @Page AND @Page is not null)
	OR ([Header] = @Header AND @Header is not null)
	OR ([Intro] = @Intro AND @Intro is not null)
	OR ([Body] = @Body AND @Body is not null)
	OR ([Footer] = @Footer AND @Footer is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Find] TO [sp_executeall]
GO
