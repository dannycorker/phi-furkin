SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-11-05
-- Description:	List all QuestionType records
-- =============================================
CREATE PROCEDURE [dbo].[qt] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT qt.* 
	FROM dbo.QuestionTypes qt (nolock) 
	ORDER BY qt.QuestionTypeID ASC 
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[qt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[qt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[qt] TO [sp_executeall]
GO
