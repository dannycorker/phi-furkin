SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-06-01
-- Description:	Return the primary and secondary phone numbers for an Aquarium customer
-- =============================================
CREATE PROCEDURE [dbo].[_C272_Greenlight_NumberPriorityForCustomer]
	  @CustomerID		INT
	 ,@Number			VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
	Initially this sequence is fixed, but we may want to vary it by client or by customer
	*/
	DECLARE  @Priority	TABLE ( NumberType VARCHAR(100), Priority INT )
	INSERT	 @Priority ( NumberType, Priority )
	VALUES	( 'HomeTelephone', 1 )
			,( 'MobileTelephone', 2 )
			,( 'DaytimeTelephoneNumber', 3 )
			,( 'WorksTelephone', 4 )
			,( 'CompanyTelephone', 5 )

	SELECT TOP(1) * 
	FROM dbo.fn_C272_GetCustomerNumberList(@CustomerID) rn
	WHERE REPLACe(rn.Number,' ','') = REPLACE(@Number,' ','')		

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_NumberPriorityForCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C272_Greenlight_NumberPriorityForCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_NumberPriorityForCustomer] TO [sp_executeall]
GO
