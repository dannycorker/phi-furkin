SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-06-17
-- Description:	Save TableRowID to field
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SaveTableRowToField]
	@TableRowID INT,
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	/*
		This SP will save the Table Row ID of a table row to a corresponding field based 
		on third party field mapping.
		
		Field 868 is the Table and 869 is the field that the value needs to be stored into.
		
		The only real pain about this is that the value must be stored at the same level as the table
		This is to keep it quick and simple.
	*/
	DECLARE @ClientIDTest INT,
			@TableDetailFieldID INT,
			@DestinationFieldID INT,
			@DestinationFieldDFST INT,
			@ObjectID INT, /*IE Customer Lead Case Matter ETC*/
			@CustomerID INT,
			@LeadID INT,
			@CaseID INT,
			@MatterID INT,
			@ContactID INT
	
	SELECT @ClientIDTest = tr.ClientID, 
			@TableDetailFieldID = tr.DetailFieldID,
			@CustomerID = tr.CustomerID,
			@LeadID = tr.LeadID,
			@CaseID = tr.CaseID,
			@MatterID = tr.MatterID,
			@ContactID = tr.ContactID
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.TableRowID = @TableRowID
	
	IF @ClientID = @ClientIDTest
	BEGIN
	
		SELECT @DestinationFieldID = tpm_destination.DetailFieldID
		FROM ThirdPartyFieldMapping tpm_source WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm_destination WITH (NOLOCK) ON tpm_destination.LeadTypeID = tpm_source.LeadTypeID AND tpm_destination.ClientID = tpm_source.ClientID AND tpm_destination.ThirdPartyFieldID = 869
		INNER JOIN DetailFields df_source WITH (NOLOCK) ON df_source.DetailFieldID = tpm_source.DetailFieldID
		INNER JOIN DetailFields df_destination WITH (NOLOCK) ON df_destination.DetailFieldID = tpm_destination.DetailFieldID AND df_destination.LeadOrMatter = df_source.LeadOrMatter
		WHERE tpm_source.DetailFieldID = @TableDetailFieldID 
		AND tpm_source.ThirdPartyFieldID = 868
		
		SELECT @DestinationFieldDFST = df.LeadOrMatter
		FROM DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldID = @DestinationFieldID
	
		SELECT @ObjectID = CASE @DestinationFieldDFST
			WHEN 1 THEN @LeadID
			WHEN 2 THEN @MatterID
			WHEN 10 THEN @CustomerID
			WHEN 11 THEN @CaseID
			WHEN 14 THEN @ContactID
			END
		
		EXEC dbo._C00_SimpleValueIntoField @DestinationFieldID, @TableRowID, @ObjectID, @ClientPersonnelID
	
	END
	ELSE
	BEGIN
	
		/*Error here*/
		RAISERROR('Client ID Doesnt Match', 16, 16)
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SaveTableRowToField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SaveTableRowToField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SaveTableRowToField] TO [sp_executeall]
GO
