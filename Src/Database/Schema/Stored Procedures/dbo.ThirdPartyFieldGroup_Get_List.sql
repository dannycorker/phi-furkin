SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ThirdPartyFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_Get_List]

AS


				
				SELECT
					[ThirdPartyFieldGroupID],
					[ThirdPartySystemID],
					[GroupName],
					[GroupDescription],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Get_List] TO [sp_executeall]
GO
