SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-01
-- Description:	Save a QuotePet record and return the ID
-- CPS 2018-06-15 added breed ResourceListID for ticket #49975
-- CPS 2018-09-20 cater for microchips
-- CPS 2018-09-20 include ExistingCUstomerID
-- GPR 2019-10-10 created for Aquarius603Dev / LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[Quote_SaveQuotePet]
(
	 @QuoteID				INT
	,@QuoteSessionID		INT
	,@MatterID				INT
	,@WhoCreated			INT
	,@SourceID				INT
	,@Overrides				dbo.tvpIntVarcharVarchar READONLY
	,@PetName				VARCHAR(2000) = NULL
	,@ProductCount			INT = NULL
	,@LeadEventID			INT = NULL
	,@CustomerDoB			DATE = NULL
	,@YearStart				DATE = NULL
	,@ExistingCustomerID	INT = NULL
	,@PreExistingConditions	BIT = NULL
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @QuotePetID			INT
			,@WhenCreated			DATETIME = dbo.fn_GetDate_Local()
			/*Pull out the Raw values and check their data types afterwards to prevent conversion errors*/
			,@Raw_Postcode			VARCHAR(2000)			,@Risk_Postcode		VARCHAR(10)
			,@Raw_TermsDate			VARCHAR(2000)			,@Risk_TermsDate	DATE
			,@Raw_StartDate			VARCHAR(2000)			,@Risk_StartDate	DATE
			,@Raw_Aggressive		VARCHAR(2000)			,@Risk_Aggressive	BIT
			,@Raw_Breed				VARCHAR(2000)			,@Risk_Breed		VARCHAR(2000)
			,@Raw_DateOfBirth		VARCHAR(2000)			,@Risk_DateOfBirth	DATE
			,@Raw_Channel			VARCHAR(2000)			,@Risk_Channel		INT
			,@Raw_Sex			VARCHAR(2000)			,@Risk_Sex		INT
			,@Raw_IsNeutered		VARCHAR(2000)			,@Risk_IsNeutered	BIT
			,@Raw_IsRescue			VARCHAR(2000)			,@Risk_IsRescue		BIT
			,@Raw_Species			VARCHAR(2000)			,@Risk_Species		INT
			,@Raw_Value				VARCHAR(2000)			,@Risk_Value		DECIMAL(18,2)
			,@Raw_Brand				VARCHAR(2000)			,@Risk_Brand		VARCHAR(2000)
			,@Raw_CampaignCode		VARCHAR(2000)			,@Risk_CampaignCode	VARCHAR(2000)
			,@Raw_NewBusiness		VARCHAR(2000)			,@Risk_NewBusiness	BIT
			,@Raw_MicrochipNumber	VARCHAR(2000)			,@Risk_HasMicrochip	BIT
															,@Risk_EndDate		DATE
			,@OverridesXML			XML										
			,@BreedResourceListID	INT						
			
	/*Save the overides*/
	SELECT @OverridesXML =
	CAST(
			(
			SELECT *
			FROM @Overrides ovr
			FOR Xml AUTO
			) 
	as XML)

	/*Pick up the pet name if it was not supplied*/
	IF (@PetName = '' or @PetName is NULL) AND @MatterID <> 0
	BEGIN
		SELECT @PetName = dbo.fnGetSimpleDv(144268,m.LeadID) /*Pet Name*/
		FROM Matter m WITH ( NOLOCK ) 
		WHERE m.MatterID = @MatterID
	END

	/*Extract all of the overrides that we care about*/
	SELECT   @Raw_Postcode			= pvt.[PostCode]
			,@Raw_TermsDate			= pvt.[TermsDate]
			,@Raw_StartDate			= pvt.[YearStart]
			,@Raw_Aggressive		= ISNULL(pvt.[177452],'0')
			,@Raw_Breed				= ISNULL(pvt.[144270],'')
			,@Raw_DateOfBirth		= pvt.[144274]
			,@Raw_Channel			= pvt.[176964]
			,@Raw_Sex			= pvt.[144275]
			,@Raw_IsNeutered		= ISNULL(pvt.[152783],'0')
			,@Raw_IsRescue			= ISNULL(pvt.[177501],'0')
			,@Raw_Species			= ISNULL(pvt.[144269],'0')
			,@Raw_Value				= ISNULL(pvt.[144339],'0.00')
			,@Raw_Brand				= ISNULL(pvt.[Brand],'')
			,@Raw_CampaignCode		= pvt.[177363]
			,@Raw_NewBusiness		= ISNULL(pvt.[NewBusiness],1)
			,@Raw_MicrochipNumber	= ISNULL(pvt.[170030],'')
	FROM		
		(	
		SELECT AnyValue1, AnyValue2
		FROM @Overrides
		)
		AS ovr
	PIVOT (	Max([AnyValue2] ) FOR [AnyValue1]
		IN ( [PostCode],[TermsDate],[YearStart],[177452],[144270] 
			,[144274],[176964],[144275],[152783],[177501],[144269] 
			,[144339],[Brand],[177363],[170030],[NewBusiness]
		   )
	) AS pvt

	/*Pick up the strongly-typed values*/
	SELECT 	 @Risk_Postcode		= LEFT(@Raw_Postcode,10)
			,@Risk_TermsDate	= CASE WHEN dbo.fnIsDate	(@Raw_TermsDate)	= 1 THEN @Raw_TermsDate		ELSE @YearStart	END
			,@Risk_StartDate	= CASE WHEN dbo.fnIsDate	(@Raw_StartDate)	= 1 THEN @Raw_StartDate		ELSE @YearStart	END
			,@Risk_Aggressive	= CASE WHEN dbo.fnIsBit		(@Raw_Aggressive)	= 1 THEN @Raw_Aggressive	END
			,@Risk_DateOfBirth	= CASE WHEN dbo.fnIsDate	(@Raw_DateOfBirth)	= 1 THEN @Raw_DateOfBirth	END
			,@Risk_Species		= CASE WHEN dbo.fnIsInt		(@Raw_Species)		= 1 THEN @Raw_Species		END
			,@Risk_Value		= CASE WHEN dbo.fnIsMoney	(@Raw_Value)		= 1 THEN @Raw_Value			END
			,@Risk_Brand		= @Raw_Brand
			,@Risk_CampaignCode	= @Raw_CampaignCode
			,@Risk_NewBusiness	= CASE WHEN dbo.fnIsBit		(@Raw_NewBusiness)	= 1 THEN @Raw_NewBusiness	END
			,@Risk_Channel		= CASE WHEN dbo.fnIsInt		(@Raw_Channel)		= 1 THEN @Raw_Channel		END
			,@Risk_Breed		= @Raw_Breed
			,@Risk_HasMicrochip	= CASE WHEN @Raw_MicrochipNumber <> ''  THEN 1 ELSE 0 END
			,@Risk_IsNeutered	= CASE WHEN @Raw_IsNeutered	= '5144' /*Yes*/ THEN 1 ELSE 0 END
			,@Risk_IsRescue		= CASE WHEN @Raw_IsRescue	= '5144' /*Yes*/ THEN 1 ELSE 0 END
			

	SELECT @Risk_Sex = li.LookupListItemID
	FROM LookupListItems li WITH (NOLOCK) 
	WHERE CONVERT(VARCHAR,li.LookupListItemID) = @Raw_Sex

	/*We don't get End Date as an override, so work it out from start date*/
	SELECT	 @Risk_EndDate		= CASE WHEN @Risk_StartDate is not NULL THEN DATEADD(DAY,-1,DATEADD(YEAR,1,@Risk_StartDate)) END

	/*Lookup the breed ResourceListID based on breed and species*/
	SELECT @BreedResourceListID = rdvBreedName.ResourceListID
	FROM ResourceListDetailValues rdvSpecies WITH ( NOLOCK ) 
	INNER JOIN ResourceListDetailValues rdvBreedName WITH ( NOLOCK ) on rdvBreedName.ResourceListID = rdvSpecies.ResourceListID AND rdvBreedName.DetailFieldID = 144270 /*Breed*/
	WHERE rdvSpecies.DetailFieldID = 144269 /*Species*/
	AND rdvSpecies.ValueInt = @Risk_Species
	AND rdvBreedName.DetailValue = @Risk_Breed

	/*Create the quote record*/
	INSERT QuotePet ([QuoteID],[QuoteSessionID],[MatterID],[WhenCreated],[WhoCreated],[SourceID]
					,[PetName],[ProductCount],[Risk_Postcode],[Risk_TermsDate],[Risk_StartDate]
					,[Risk_EndDate],[Risk_AggressiveTendencies],[Risk_Breed],[Risk_DateOfBirth]
					,[Risk_Channel],[Risk_Sex],[Risk_IsNeutered],[Risk_IsRescue],[Risk_Species]
					,[Risk_Value],[Risk_Brand],[Risk_CampaignCode],[Risk_NewBusiness],[OverridesXml]
					,[LeadEventID],[BreedResourceListID],[CustomerDob],[Risk_HasMicrochip],[Risk_ExistingCustomerID],[Risk_PreExistingConditions])

	VALUES  ( 
			 @QuoteID
			,@QuoteSessionID
			,@MatterID
			,@WhenCreated
			,@WhoCreated
			,@SourceID
			,@PetName			
			,@ProductCount
			,@Risk_Postcode		
			,@Risk_TermsDate	
			,@Risk_StartDate	
			,@Risk_EndDate		
			,@Risk_Aggressive	
			,@Risk_Breed		
			,@Risk_DateOfBirth	
			,@Risk_Channel		
			,@Risk_Sex		
			,@Risk_IsNeutered	
			,@Risk_IsRescue		
			,@Risk_Species		
			,@Risk_Value		
			,@Risk_Brand		
			,@Risk_CampaignCode	
			,@Risk_NewBusiness	
			,@OverridesXml
			,@LeadEventID
			,@BreedResourceListID
			,@CustomerDoB
			,@Risk_HasMicrochip
			,@ExistingCustomerID
			,@PreExistingConditions
			)

	/*Pick up the newly created ID*/
	SELECT @QuotePetID = SCOPE_IDENTITY()

	/*Reutrn the ID to the caller*/
	RETURN @QuotePetID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Quote_SaveQuotePet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePet] TO [sp_executehelper]
GO
