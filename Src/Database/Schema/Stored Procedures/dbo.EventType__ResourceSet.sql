SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-10-22
-- Description:	Run batch jobs based on events 
-- =============================================
CREATE PROCEDURE [dbo].[EventType__ResourceSet] 
	@LeadEventID int
AS
BEGIN

	SET NOCOUNT ON;

	Declare @EventTypeResourceSetID int, 
			@TestID int,
			@SQLToRun varchar(max),
			@CaseID int,
			@EventTypeID int
	
    -- If all IDs are known then we have slightly less work to do,
	-- otherwise look them up from the LeadEvent record.

	SELECT @EventTypeID = EventTypeID, @CaseID = CaseID
	FROM LeadEvent 
	WHERE LeadEventID = @LeadEventID 

	IF @EventTypeID IS NOT NULL
	BEGIN

		Select top 1 @EventTypeResourceSetID = EventTypeResourceSetID, @TestID = 0
		From EventTypeResourceSet
		Where EventTypeID = @EventTypeID
		Order By EventTypeResourceSetID
	
		While @EventTypeResourceSetID > @TestID
		BEGIN
		
			Select @SQLToRun = 'exec dbo._C00_AssignResourceOnCriteria ' + CONVERT(varchar,@CaseID) + ', ' + CONVERT(varchar,CriteriaDetailFieldID) + ', ' + CONVERT(varchar,ResourceListDetailFieldID) + ', ' + convert(varchar,ResourceListMatchFieldID) + ', ' + convert(varchar,SuppressSpaces) + ', ' + convert(varchar,FirstXChars) + ', ' + convert(varchar,ExactMatch)
			From EventTypeResourceSet
			Where EventTypeResourceSetID = @EventTypeResourceSetID
		
			exec (@SQLToRun)
			
			--select @SQLToRun
		
			Select @TestID = @EventTypeResourceSetID		

			Select top 1 @EventTypeResourceSetID = EventTypeResourceSetID
			From EventTypeResourceSet
			Where EventTypeID = @EventTypeID
			and EventTypeResourceSetID > @TestID
			Order By EventTypeResourceSetID
		
		END

	END

END






GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__ResourceSet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__ResourceSet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__ResourceSet] TO [sp_executeall]
GO
