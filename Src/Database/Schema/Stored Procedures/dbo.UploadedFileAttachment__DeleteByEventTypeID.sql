SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2012
-- Description:	Deletes an existing UploadedFileAttachment record by the given event type ID
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFileAttachment__DeleteByEventTypeID]
	
	@EventTypeID INT

AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [dbo].UploadedFileAttachment WITH (ROWLOCK) 
	WHERE EventTypeID = @EventTypeID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFileAttachment__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__DeleteByEventTypeID] TO [sp_executeall]
GO
