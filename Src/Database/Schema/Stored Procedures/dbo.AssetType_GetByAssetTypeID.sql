SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AssetType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_GetByAssetTypeID]
(

	@AssetTypeID int   
)
AS


				SELECT
					[AssetTypeID],
					[AssetSubTypeID],
					[AssetTypeName],
					[Note],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[AssetType] WITH (NOLOCK) 
				WHERE
										[AssetTypeID] = @AssetTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_GetByAssetTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_GetByAssetTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_GetByAssetTypeID] TO [sp_executeall]
GO
