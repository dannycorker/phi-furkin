SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-24
-- Description:	Saves a new claim to the db
-- MODS
-- JL 2016-10-06  Added debug mode to the _C384_ version of this proc
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_SaveNewClaim]
(
	@ClientID INT,
	@AccountID INT,
	@ConversationID UNIQUEIDENTIFIER,
	@Xml XML,
	@SenderID UNIQUEIDENTIFIER,
	@HistoryID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ID INT
	
	IF NOT EXISTS (SELECT * FROM VetEnvoy_Claims WHERE ConversationID = @ConversationID)
	BEGIN
	
		-- Fix the default namespace issue for PremVet
		DECLARE @XmlString NVARCHAR(MAX)
		SELECT @XmlString = CAST(@Xml AS NVARCHAR(MAX))
		IF @XmlString LIKE '%xmlns:xsi="http://www.vetxml.org/schemas/InsuranceClaim"%'
		BEGIN
			SELECT @Xml = CAST(REPLACE(CAST(@Xml AS NVARCHAR(MAX)), 'xmlns:xsi="http://www.vetxml.org/schemas/InsuranceClaim"', 'xmlns="http://www.vetxml.org/schemas/InsuranceClaim"') AS XML)
		END
	
		DECLARE @PolicyRef VARCHAR(100)
		;WITH XMLNAMESPACES(DEFAULT 'http://www.vetxml.org/schemas/InsuranceClaim')
		SELECT @PolicyRef = @Xml.value('(/InsuranceClaim/InfoFromPolicyHolder/PolicyDetails/PolicyNumber)[1]', 'varchar(100)')
	
		INSERT dbo.VetEnvoy_Claims(ClientID, AccountID, ConversationID, Xml, Received, SenderID, PolicyRef)
		VALUES (@ClientID, @AccountID, @ConversationID, @Xml, dbo.fn_GetDate_Local(), @SenderID, @PolicyRef) 
		
		SELECT @ID = SCOPE_IDENTITY()
		
	END
	ELSE
	BEGIN
	
		SELECT @ID = ClaimID
		FROM dbo.VetEnvoy_Claims WITH (NOLOCK) 
		WHERE ConversationID = @ConversationID
	
	END	
	
	/*Added debug mode*/ 
	DECLARE @Sql NVARCHAR(2000)
	SELECT @Sql = 'EXEC dbo._C' + CAST(@ClientID AS VARCHAR(50)) + '_VetEnvoy_SaveNewClaim ' + CAST(@ID AS VARCHAR(50)) + ' , ' + '0'
	EXEC sp_executesql @Sql
	
	UPDATE dbo.VetEnvoy_ConversationHistory
	SET Actioned = dbo.fn_GetDate_Local()
	WHERE HistoryID = @HistoryID
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SaveNewClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_SaveNewClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_SaveNewClaim] TO [sp_executeall]
GO
