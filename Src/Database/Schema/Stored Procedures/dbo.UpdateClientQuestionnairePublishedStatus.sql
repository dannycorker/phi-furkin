SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[UpdateClientQuestionnairePublishedStatus] 
	@ClientQuestionnaireID INT,
	@Published BIT
AS
BEGIN
	UPDATE ClientQuestionnaires
	SET  Published = @Published
	WHERE ClientQuestionnaireID = @ClientQuestionnaireID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnairePublishedStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateClientQuestionnairePublishedStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateClientQuestionnairePublishedStatus] TO [sp_executeall]
GO
