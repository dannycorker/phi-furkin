SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUserOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserOptionID int   = null ,

	@ClientID int   = null ,

	@PortalUserID int   = null ,

	@PortalUserOptionTypeID int   = null ,

	@OptionValue varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserOptionID]
	, [ClientID]
	, [PortalUserID]
	, [PortalUserOptionTypeID]
	, [OptionValue]
    FROM
	[dbo].[PortalUserOption] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserOptionID] = @PortalUserOptionID OR @PortalUserOptionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PortalUserID] = @PortalUserID OR @PortalUserID IS NULL)
	AND ([PortalUserOptionTypeID] = @PortalUserOptionTypeID OR @PortalUserOptionTypeID IS NULL)
	AND ([OptionValue] = @OptionValue OR @OptionValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserOptionID]
	, [ClientID]
	, [PortalUserID]
	, [PortalUserOptionTypeID]
	, [OptionValue]
    FROM
	[dbo].[PortalUserOption] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserOptionID] = @PortalUserOptionID AND @PortalUserOptionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PortalUserID] = @PortalUserID AND @PortalUserID is not null)
	OR ([PortalUserOptionTypeID] = @PortalUserOptionTypeID AND @PortalUserOptionTypeID is not null)
	OR ([OptionValue] = @OptionValue AND @OptionValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Find] TO [sp_executeall]
GO
