SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_GetByTarget]
(

	@Target varchar (250)  
)
AS


				SELECT
					[FieldTargetID],
					[Target],
					[ObjectName],
					[PropertyName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[FieldTarget] WITH (NOLOCK) 
				WHERE
										[Target] = @Target
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_GetByTarget] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_GetByTarget] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_GetByTarget] TO [sp_executeall]
GO
