SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-14
-- Description:	Only return enabled fields that can be used to store date values
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetDateFields] 

	@ClientID int, 
	@LeadTypeID int, 
	@LeadOrMatter int 

AS
BEGIN
	
	SELECT df.DetailFieldID,
	dfp.PageName + ': ' + df.FieldCaption AS FieldName
	FROM dbo.fnDetailFieldsShared(@ClientID) df 
	INNER JOIN [dbo].[DetailFieldPages] dfp ON dfp.DetailFieldPageID = df.DetailFieldPageID
	WHERE df.Enabled = 1
	AND df.ClientID  = @ClientID
	AND df.LeadTypeID = @LeadTypeID
	AND df.LeadOrMatter = @LeadOrMatter
	AND df.Enabled = 1
	AND df.QuestionTypeID IN (1,5)  -- 1=General, 5=Date
	ORDER BY dfp.PageName, df.FieldCaption

END







GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetDateFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetDateFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetDateFields] TO [sp_executeall]
GO
