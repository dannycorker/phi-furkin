SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-27
-- Description:	Populate any number of LeadDetailValues at once 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateLDVWithValues]
(
	@LeadID int,
	@ClientPersonnelID int,
	@tvpIDValue tvpIDValue READONLY
)
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Maintain history if enabled. Only do this for data that has changed, so do it first or it won't look like it has changed. */
	INSERT INTO DetailValueHistory(ClientID, LeadID, MatterID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue) 
	SELECT df.ClientID, @LeadID, NULL, t.AnyID, 1, t.AnyValue, dbo.fn_GetDate_Local(), @ClientPersonnelID, NULL 
	FROM @tvpIDValue t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 1 AND df.MaintainHistory = 1 
	LEFT JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = t.AnyID AND ldv.LeadID = @LeadID 
	WHERE t.AnyValue IS NOT NULL
	AND ((ldv.LeadDetailValueID IS NULL) OR (ldv.DetailValue <> t.AnyValue)) 
	
	/* Update any values that already exist but have changed */
	UPDATE dbo.LeadDetailValues 
	SET DetailValue = t.AnyValue
	FROM @tvpIDValue t 
	INNER JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID = t.AnyID AND ldv.LeadID = @LeadID 
	WHERE t.AnyValue IS NOT NULL
	AND ldv.DetailValue <> t.AnyValue
	
	/* Insert any values that don't exist already */
	INSERT INTO dbo.LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
	SELECT df.ClientID, @LeadID, t.AnyID, t.AnyValue
	FROM @tvpIDValue t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 1 
	WHERE t.AnyValue IS NOT NULL
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
		WHERE ldv.LeadID = @LeadID
		AND ldv.DetailFieldID = t.AnyID 
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateLDVWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateLDVWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateLDVWithValues] TO [sp_executeall]
GO
