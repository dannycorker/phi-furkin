SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetQuestionPossibleAnswer] @QuestionPossibleAnswerID int
as
select QuestionPossibleAnswerID, MasterQuestionID, AnswerText, Branch, LinkedQuestionnaireQuestionPossibleAnswerID, ClientID 
from QuestionPossibleAnswers
where QuestionPossibleAnswerID = @QuestionPossibleAnswerID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionPossibleAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionPossibleAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionPossibleAnswer] TO [sp_executeall]
GO
