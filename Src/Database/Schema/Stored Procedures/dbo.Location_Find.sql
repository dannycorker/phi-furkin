SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Location table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Location_Find]
(

	@SearchUsingOR bit   = null ,

	@LocationID int   = null ,

	@ClientID int   = null ,

	@LocationName varchar (255)  = null ,

	@Note varchar (255)  = null ,

	@Enabled bit   = null ,

	@Deleted bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LocationID]
	, [ClientID]
	, [LocationName]
	, [Note]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[Location] WITH (NOLOCK) 
    WHERE 
	 ([LocationID] = @LocationID OR @LocationID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LocationName] = @LocationName OR @LocationName IS NULL)
	AND ([Note] = @Note OR @Note IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LocationID]
	, [ClientID]
	, [LocationName]
	, [Note]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[Location] WITH (NOLOCK) 
    WHERE 
	 ([LocationID] = @LocationID AND @LocationID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LocationName] = @LocationName AND @LocationName is not null)
	OR ([Note] = @Note AND @Note is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Location_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Find] TO [sp_executeall]
GO
