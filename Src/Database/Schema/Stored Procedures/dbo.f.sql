SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-24
-- Description:	List all Functions, Function Types and Modules
-- =============================================
CREATE PROCEDURE [dbo].[f] 
	@ModuleID int = null,
	@FunctionTypeID int = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT f.* 
	FROM dbo.Functions f WITH (NOLOCK) 
	WHERE (@ModuleID IS NULL OR f.ModuleID = @ModuleID)
	AND (@FunctionTypeID IS NULL OR f.FunctionTypeID = @FunctionTypeID) 
	ORDER BY f.FunctionID 

	SELECT m.ModuleID, m.ModuleName, f.FunctionTypeID, f.FunctionID, f.FunctionName, f.FunctionDescription, ft.*, ftp1.FunctionTypeName, ftp2.FunctionTypeID, ftp2.FunctionTypeName 
	FROM dbo.Functions f WITH (NOLOCK) 
	INNER JOIN dbo.FunctionType ft  WITH (NOLOCK) on ft.FunctionTypeID = f.FunctionTypeID 
	LEFT JOIN dbo.FunctionType ftp1  WITH (NOLOCK) on ftp1.FunctionTypeID = ft.ParentFunctionTypeID 
	LEFT JOIN dbo.FunctionType ftp2  WITH (NOLOCK) on ftp2.FunctionTypeID = ftp1.ParentFunctionTypeID 
	INNER JOIN dbo.Module m  WITH (NOLOCK) on m.ModuleID = ft.ModuleID
	WHERE (@ModuleID IS NULL OR f.ModuleID = @ModuleID)
	AND (@FunctionTypeID IS NULL OR f.FunctionTypeID = @FunctionTypeID) 
	ORDER BY m.ModuleID, f.FunctionTypeID, f.FunctionDescription 
	
	IF @ModuleID IS NULL
	BEGIN
		SELECT m.* 
		FROM dbo.Module m 
		ORDER BY m.ModuleID 
	END
	
	IF @FunctionTypeID IS NULL
	BEGIN
		SELECT ft.* 
		FROM dbo.FunctionType ft 
		WHERE (@ModuleID IS NULL OR ft.ModuleID = @ModuleID)
		ORDER BY ft.FunctionTypeID 
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[f] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[f] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[f] TO [sp_executeall]
GO
