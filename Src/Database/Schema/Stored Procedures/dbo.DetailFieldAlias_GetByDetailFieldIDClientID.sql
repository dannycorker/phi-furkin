SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldAlias table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_GetByDetailFieldIDClientID]
(

	@DetailFieldID int   ,

	@ClientID int   
)
AS


				SELECT
					[DetailFieldAliasID],
					[ClientID],
					[LeadTypeID],
					[DetailFieldID],
					[DetailFieldAlias]
				FROM
					[dbo].[DetailFieldAlias] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByDetailFieldIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_GetByDetailFieldIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByDetailFieldIDClientID] TO [sp_executeall]
GO
