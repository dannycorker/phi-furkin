SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-09-15
-- Description:	Update Claim payment from Markel
-- =============================================
CREATE PROCEDURE [dbo].[_C605_Markel_UpdateClaimPayment]
	@VisionPaymentID INT,
	@PrudentReference VARCHAR(100)
AS
BEGIN

	SET NOCOUNT ON;

	/*
		Lookup the CPS based on the VisionPaymentID (SourceID)
		Lookup the CL from CPS
		Stamp down the reference
	*/

	DECLARE @CustomerPaymentScheduleID INT,
			@CustomerledgerID INT,
			@ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT TOP 1 @CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	FROM Customers c WITH (NOLOCK)
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerID = c.CustomerID
	WHERE c.Test = 0
	AND c.ClientID = @ClientID
	AND cps.PaymentGross < 0.00
	AND cps.SourceID = @VisionPaymentID
	ORDER BY cps.CustomerPaymentScheduleID DESC

	UPDATE cps
	SET PaymentStatusID = 6,
		WhenModified = GETDATE(),

		@CustomerledgerID = cps.CustomerLedgerID
	FROM CustomerPaymentSchedule cps
	WHERE cps.CustomerPaymentScheduleID = @CustomerledgerID

	UPDATE cl
	SET TransactionReference = @PrudentReference
	FROM CustomerLedger cl
	WHERE cl.CustomerLedgerID = @CustomerledgerID

END
GO
GRANT EXECUTE ON  [dbo].[_C605_Markel_UpdateClaimPayment] TO [sp_executeall]
GO
