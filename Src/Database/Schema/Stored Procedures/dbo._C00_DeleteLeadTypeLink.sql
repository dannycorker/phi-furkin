SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-11-30
-- Description:	SP to copy Report
-- =============================================
Create PROCEDURE [dbo].[_C00_DeleteLeadTypeLink]
@OutcomeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	delete from LeadTypeLink
	where OutcomeID = @OutcomeID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeleteLeadTypeLink] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DeleteLeadTypeLink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeleteLeadTypeLink] TO [sp_executeall]
GO
