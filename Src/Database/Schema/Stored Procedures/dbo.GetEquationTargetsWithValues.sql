SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-28
-- Description:	Paul's crazy plan! Not implemented yet.
-- =============================================
CREATE PROCEDURE [dbo].[GetEquationTargetsWithValues] 
	@ClientID int,
	@LeadTypeID int,
	@EquationDetailFieldID int = NULL,
	@CustomerID int = NULL,
	@LeadID int = NULL,
	@CaseID int = NULL,
	@MatterID int = NULL,
	@ClientPersonnelID int = NULL,
	@ContactID int = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	RETURN
	
	/*
	DECLARE @EquationDetailFieldList TABLE (EquationDetailFieldID int) 
	*/
	
	EXEC dbo.DetailValues__CreateAll @ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @ClientPersonnelID, @ContactID 
	
	/*
	IF @EquationDetailFieldID > 0
	BEGIN
		INSERT @EquationDetailFieldList (EquationDetailFieldID)
		VALUES (@EquationDetailFieldID)
	END
	ELSE
	BEGIN
		INSERT @EquationDetailFieldList (EquationDetailFieldID)
		SELECT df.DetailFieldID 
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.QuestionTypeID = 10 
		AND df.LeadOrMatter = @DetailFieldSubtypeID 
		AND df.ClientID = @ClientID 
		AND df.Enabled = 1 
	END
	*/
	
	/*
		DetailFieldSubTypeID	DetailFieldSubTypeName
		1	Lead
		2	Matter
		4	Resource List
		6	Table
		8	Basic Table
		10	Customer
		11	Case
		12	Client
		13	ClientPersonnel
		14	Contact
		15	Object
	*/
	
	IF @LeadTypeID = 0
	BEGIN
		
		/*Customer Fields*/
		/*
		SELECT et.* 
		FROM dbo.EquationTarget et WITH (NOLOCK) 
		INNER JOIN @EquationDetailFieldList edfl ON edfl.EquationDetailFieldID = et.EquationDetailFieldID
		WHERE et.DetailFieldSubTypeID = 10 
		*/
		SELECT et.EquationDetailFieldID, df.EquationText, df.LeadOrMatter 
		FROM dbo.EquationTarget et WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = et.EquationDetailFieldID AND df.QuestionTypeID = 10 AND df.ClientID = @ClientID AND df.[Enabled] = 1 
		WHERE (@EquationDetailFieldID IS NULL OR df.DetailFieldID = @EquationDetailFieldID)
		
		SELECT et.EquationDetailFieldID, 
		et.DetailFieldID, 
		et.[Target], 
		et.ObjectName, 
		et.PropertyName, 
		et.SqlFunctionID, 
		df.LeadOrMatter, 
		et.IsEquation, 
		et.IsColumnSum, 
		et.ColumnFieldID, 
		10 as DetailFieldSubtypeID,
		cdv.DetailValue as [Value] 
		FROM dbo.EquationTarget et WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = et.EquationDetailFieldID AND df.QuestionTypeID = 10 AND df.ClientID = @ClientID AND df.[Enabled] = 1 
		INNER JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) ON cdv.DetailFieldID = df.DetailFieldID AND cdv.CustomerID = @CustomerID 
		WHERE df.LeadOrMatter = 10 
		AND (@EquationDetailFieldID IS NULL OR df.DetailFieldID = @EquationDetailFieldID)
	END	
	
	DECLARE 
	@identity int = 1, 
	@detailFieldSubTypeID int = 2, 
	@detailFieldID int = 3, 	
	@columnDetailFieldID int = 4

	DECLARE @Sum TABLE (Result money)
		
	INSERT @Sum(Result)
	EXEC [dbo].[CalculateColumnSum] @identity, @detailFieldSubTypeID, @detailFieldID, @columnDetailFieldID, @clientID 

	SELECT Result as [WooHoo!] 
	FROM @Sum 
		
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetEquationTargetsWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEquationTargetsWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEquationTargetsWithValues] TO [sp_executeall]
GO
