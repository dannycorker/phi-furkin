SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the SMSQuestionnaire table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionnaire_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [SMSQuestionnaireID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([SMSQuestionnaireID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [SMSQuestionnaireID]'
				SET @SQL = @SQL + ' FROM [dbo].[SMSQuestionnaire] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[SMSQuestionnaireID], O.[ClientID], O.[Title], O.[Description], O.[Enabled], O.[ToTarget], O.[CreateCustomerIfNotFound], O.[UseSentimentAnalysis], O.[RuleSetID], O.[IsVoice], O.[Timeout], O.[Repeat], O.[RepeatPrefix]
				FROM
				    [dbo].[SMSQuestionnaire] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[SMSQuestionnaireID] = PageIndex.[SMSQuestionnaireID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[SMSQuestionnaire] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionnaire_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionnaire_GetPaged] TO [sp_executeall]
GO
