SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Date Created: 22-02-2012

-- Created By:  Paul Richardson
-- Purpose:		Gets LetterOut Events By Lead TypeID
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetLetterOutEventsByLeadTypeID] 
(
	@LeadTypeID int,
	@ClientID INT = NULL
)
AS
BEGIN
	SET ANSI_NULLS OFF

	SELECT
		[EventTypeID],
		[EventTypeName]
	FROM
		dbo.fnEventTypeShared(@ClientID) et
	WHERE
		LeadTypeID = @LeadTypeID
		AND EventSubTypeID IN (4, 20)
	ORDER BY EventTypeName

	SET ANSI_NULLS ON

END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetLetterOutEventsByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetLetterOutEventsByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetLetterOutEventsByLeadTypeID] TO [sp_executeall]
GO
