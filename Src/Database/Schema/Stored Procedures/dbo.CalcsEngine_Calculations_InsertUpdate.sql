SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Inserts or updates a calculation
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_Calculations_InsertUpdate]
(
	@CalculationID INT,
	@Name VARCHAR(200),
	@ClientID INT,
	@UserID INT,
	@Description VARCHAR(MAX)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF @CalculationID > 0
	BEGIN
	
		UPDATE dbo.CalcsEngine_Calculations
		SET Name = @Name,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID,
			Description = @Description
		WHERE CalculationID = @CalculationID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.CalcsEngine_Calculations (Name, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, Description)
		VALUES (@Name, @ClientID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @Description)
		
		SELECT @CalculationID = SCOPE_IDENTITY()
	
	END

	EXEC dbo.CalcsEngine_Calculations_Get @ClientID, @CalculationID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Calculations_InsertUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_Calculations_InsertUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Calculations_InsertUpdate] TO [sp_executeall]
GO
