SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-11
-- Description:	Get Out-of-process events list
--				2012-02-20	SB	Don't show events of a specific subtype if they are disabled
--				2014-09-18	SB	Handled shared lead types
-- =============================================
CREATE PROCEDURE [dbo].[GetOutOfProcessEventList] 
	@LeadTypeID int, 
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

	SELECT DISTINCT EventTypeID, EventTypeName 
	FROM dbo.fnEventTypeShared(@ClientID) et 
	INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON et.EventTypeID = fnEventTypeSecure.objectid
	INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON et.EventSubtypeID = est.EventSubtypeID 
	WHERE LeadTypeID = @LeadTypeID 
	AND et.InProcess = 0
	AND et.Enabled = 1
	AND et.AvailableManually = 1
	AND et.EventSubtypeID NOT IN (9) /* Don't include Hold/Unhold events in this list */
	AND est.Available = 1
	ORDER BY EventTypeName

END









GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutOfProcessEventList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutOfProcessEventList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutOfProcessEventList] TO [sp_executeall]
GO
