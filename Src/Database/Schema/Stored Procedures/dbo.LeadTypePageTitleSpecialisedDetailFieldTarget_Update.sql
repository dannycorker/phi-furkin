SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadTypePageTitleSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Update]
(

	@LeadTypePageTitleSpecialisedDetailFieldTargetID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@RowField int   ,

	@Notes varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[ColumnField] = @ColumnField
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[ColumnFieldAlias] = @ColumnFieldAlias
					,[RowField] = @RowField
					,[Notes] = @Notes
				WHERE
[LeadTypePageTitleSpecialisedDetailFieldTargetID] = @LeadTypePageTitleSpecialisedDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget_Update] TO [sp_executeall]
GO
