SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldStyle table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldStyle_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailFieldStyleID],
					[ClientID],
					[StyleName],
					[BackgroundColor],
					[ForegroundColor],
					[FontSize],
					[FontWeight],
					[Align],
					[Padding],
					[PaddingHorizontal],
					[TextDecoration],
					[FontStyle],
					[PaddingVertical],
					[Enabled]
				FROM
					[dbo].[DetailFieldStyle] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldStyle_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_GetByClientID] TO [sp_executeall]
GO
