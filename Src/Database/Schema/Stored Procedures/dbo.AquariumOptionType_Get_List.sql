SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AquariumOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOptionType_Get_List]

AS


				
				SELECT
					[AquariumOptionTypeID],
					[AquariumOptionTypeName],
					[AquariumOptionTypeDescription]
				FROM
					[dbo].[AquariumOptionType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOptionType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Get_List] TO [sp_executeall]
GO
