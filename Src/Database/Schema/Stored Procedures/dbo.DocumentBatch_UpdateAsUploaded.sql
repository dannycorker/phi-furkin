SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-10
-- Description:	Update a document batch entry to indicate that
--              all the documents have been uploaded.
--
-- Parameters:  @DocumentBatchId
--              The document batch entry to update
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatch_UpdateAsUploaded]
	@DocumentBatchId INT
AS
BEGIN
	-- Create the DocumentBatch entry
	UPDATE
		DocumentBatch
	SET
		Uploaded = 1
	WHERE
		DocumentBatchId = @DocumentBatchId
	
	EXEC dbo.DocumentBatch_AutoProcessBatch @DocumentBatchId
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_UpdateAsUploaded] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_UpdateAsUploaded] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_UpdateAsUploaded] TO [sp_executeall]
GO
