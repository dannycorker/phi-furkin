SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2013-07-17
-- Description:	Assigns Workflow Tasks equally between users for workflows with Assigned Leads Only switched on. Ticket #20741
-- =============================================
CREATE PROCEDURE [dbo].[_C00_WorkflowTaskAutomaticDivision]
		@ClientID INT,
		@ReadOnly BIT = 1
AS
BEGIN
	
	DECLARE @AssignTo AS INT,
			@LeadToAssign INT,
			@WorkflowGroup INT,
			@ClientPersonnelID INT,
			@Total INT = 0,
			@Stage INT = 0,
			@LogEntry VARCHAR(20)

	DECLARE @WGIDTable TABLE (ID INT)
	DECLARE @IDTable TABLE (ID INT, wID INT)
	DECLARE @LeadIDTable TABLE (ID INT,wID INT)
	
	DECLARE @Table TABLE (TestID INT)
	
	INSERT INTO @IDTable(ID,wID)
	SELECT w.ClientPersonnelID,w.WorkflowGroupID FROM WorkflowGroupAssignment w WITH (NOLOCK) WHERE w.ClientID = @ClientID
	
	INSERT INTO @WGIDTable (ID)
	SELECT DISTINCT wg.WorkflowGroupID FROM WorkflowTask w WITH (NOLOCK) 
	INNER JOIN WorkflowGroup wg WITH (NOLOCK) ON wg.WorkflowGroupID = w.WorkflowGroupID 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = w.LeadID 
	WHERE wg.AssignedLeadsOnly = 1 
	AND wg.Enabled = 1 
	AND wg.ClientID = @ClientID
	AND (l.AssignedTo IS NULL OR l.AssignedTo NOT IN (SELECT wga.ClientPersonnelID FROM WorkflowGroupAssignment wga WITH (NOLOCK) WHERE wga.WorkflowGroupID = wg.WorkflowGroupID))

	SELECT TOP 1 @WorkflowGroup = ID FROM @WGIDTable
	
	IF @ReadOnly = 1 /*Changed to show full outstanding task list*/
	BEGIN
	
		SELECT COUNT(DISTINCT w.LeadID)
		FROM WorkflowTask w WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = w.LeadID 
		WHERE w.WorkflowGroupID IN (SELECT ID FROM @WGIDTable) 
		AND (l.AssignedTo NOT IN (SELECT ID FROM @IDTable WHERE wid = w.WorkflowGroupID) OR l.AssignedTo IS NULL)
		AND l.ClientID = @ClientID
		AND (@WorkflowGroup IS NULL OR @WorkflowGroup IN (SELECT ID FROM @WGIDTable))
				AND NOT EXISTS (
		SELECT * FROM Lead lsub WITH (NOLOCK)
		INNER JOIN WorkflowTask wsub WITH (NOLOCK) ON wsub.LeadID = lsub.LeadID
		INNER JOIN WorkflowGroupAssignment wgasub WITH (NOLOCK) ON wgasub.WorkflowGroupID = wsub.WorkflowGroupID
		INNER JOIN WorkflowGroup wgsub WITH (NOLOCK) ON wgsub.WorkflowGroupID = wsub.WorkflowGroupID
		WHERE lsub.LeadID = l.LeadID
		AND wgsub.AssignedLeadsOnly = 1
		AND wgasub.ClientPersonnelID = l.AssignedTo
		)
		
	END
	
	IF @ReadOnly = 0
	BEGIN
	
		INSERT INTO Logs (LogDateTime,TypeOfLogEntry,ClassName,MethodName,LogEntry)
		VALUES (dbo.fn_GetDate_Local(),'Info','DivisionProc',@ClientID,'Start')
	
		SELECT *
		FROM @Table
			
		INSERT INTO @LeadIDTable (ID,wid)
		SELECT DISTINCT l.LeadID,  w.WorkflowGroupID
		FROM WorkflowTask w WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = w.LeadID 
		WHERE w.WorkflowGroupID IN (SELECT ID FROM @WGIDTable) 
		AND (l.AssignedTo NOT IN (SELECT ID FROM @IDTable WHERE wid = w.WorkflowGroupID) OR l.AssignedTo IS NULL)
		AND l.ClientID = @ClientID
		AND (@WorkflowGroup IS NULL OR @WorkflowGroup IN (SELECT ID FROM @WGIDTable))
		AND NOT EXISTS (SELECT * FROM Lead lsub WITH (NOLOCK)
						INNER JOIN WorkflowTask wsub WITH (NOLOCK) ON wsub.LeadID = lsub.LeadID
						INNER JOIN WorkflowGroupAssignment wgasub WITH (NOLOCK) ON wgasub.WorkflowGroupID = wsub.WorkflowGroupID
						INNER JOIN WorkflowGroup wgsub WITH (NOLOCK) ON wgsub.WorkflowGroupID = wsub.WorkflowGroupID
						WHERE lsub.LeadID = l.LeadID
						AND wgsub.AssignedLeadsOnly = 1
						AND wgasub.ClientPersonnelID = l.AssignedTo)
						
		SELECT @Total = COUNT(*) +1 FROM @LeadIDTable
					
		WHILE @Total >= @Stage
				
		BEGIN
		SET @Stage = @Stage +1
				
			DELETE @IDTable
			INSERT INTO @IDTable(ID)
			SELECT w.ClientPersonnelID FROM WorkflowGroupAssignment w WITH (NOLOCK) WHERE w.WorkflowGroupID = @WorkflowGroup

			SELECT TOP 1 @LeadToAssign = l.ID, @WorkflowGroup = l.wid
			FROM @LeadIDTable l		
			ORDER BY l.ID DESC

			DELETE @LeadIDTable WHERE ID = @LeadToAssign

			;WITH Assign (CP,CT) AS 
			
			(
				SELECT TOP 100 cp.ClientPersonnelID , COUNT(l.leadid) 
				FROM ClientPersonnel cp WITH (NOLOCK) 
				LEFT JOIN Lead l WITH (NOLOCK) ON l.AssignedTo = cp.ClientPersonnelID 
				WHERE cp.ClientID = @ClientID
				AND cp.ClientPersonnelID IN (SELECT ClientPersonnelID FROM WorkflowGroupAssignment w WITH (NOLOCK) WHERE w.WorkflowGroupID = @WorkflowGroup)
				AND EXISTS (SELECT * FROM WorkflowTask w WITH (NOLOCK) WHERE w.LeadID = l.LeadID AND w.Escalated = 0)
				GROUP BY cp.ClientPersonnelID
				UNION 
				SELECT TOP 100 cp.ClientPersonnelID , COUNT(l.leadid) 
				FROM ClientPersonnel cp WITH (NOLOCK) 
				LEFT JOIN Lead l WITH (NOLOCK) ON l.AssignedTo = cp.ClientPersonnelID 
				WHERE cp.ClientID = @ClientID
				AND cp.ClientPersonnelID IN (SELECT ClientPersonnelID FROM WorkflowGroupAssignment w WITH (NOLOCK) WHERE w.WorkflowGroupID = @WorkflowGroup)
				GROUP BY cp.ClientPersonnelID
				HAVING COUNT(l.leadid)  = 0
				ORDER BY COUNT(l.leadid) ASC
			)
			
			SELECT TOP 1 @AssignTo = CP  FROM Assign ORDER BY CT ASC
			
			SELECT TOP 1 @ClientPersonnelID = ClientPersonnelID 
			FROM ClientPersonnel cp WITH (NOLOCK) 
			WHERE cp.FirstName = 'Louis'
			AND cp.LastName = 'Bromilow'
			AND cp.ClientID = @ClientID
			AND cp.IsAquarium = 1
			
			SELECT  @LogEntry = CONVERT(VARCHAR,@Stage)+'/'+CONVERT(VARCHAR,@Total)
			
			INSERT INTO Logs (LogDateTime,TypeOfLogEntry,ClassName,MethodName,LogEntry,ClientPersonnelID)
			VALUES (dbo.fn_GetDate_Local(),'Info','DivisionProc',@LeadToAssign,@LogEntry,@AssignTo)

			EXEC _C00_LeadAssignment @LeadToAssign,@AssignTo,@ClientPersonnelID,1
						
		END
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_WorkflowTaskAutomaticDivision] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_WorkflowTaskAutomaticDivision] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_WorkflowTaskAutomaticDivision] TO [sp_executeall]
GO
