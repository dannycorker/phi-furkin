SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-01-28
-- Description:	Copy all group rights and user rights for lead type X to new lead type Y. 
--              This includes all the detailed objects like EventTypes, Pages and also some
--              DetailFields (where Encrypt = 1), so the old IDs all need to be mapped
--              to the new ones for this to work.
--              This includes a debug flag to show what will happen, without doing it.
-- =============================================
CREATE PROCEDURE [dbo].[CopyRightsForNewLeadType] 

	@FromLeadTypeID int,
	@ToLeadTypeID int,
	@Debug bit = 0
	
AS
BEGIN
	SET NOCOUNT ON;

	IF @Debug = 1
	BEGIN
		/*
			1) Make sure that no-one is editing the group (or any users within it)
			The easiest way to do this is to ensure that the GroupAndUserEditingControl
			table is empty. Otherwise, you need to be sure that:
				i) no groups belonging to this client are being edited,
				AND that 
				ii) none of the Aquarium groups that any users within this client belong 
				to are being edited.
		*/
		SELECT * FROM dbo.GroupAndUserEditingControl

		/*
			2) Have a look at the data that needs to be copied.
			In this example we are copying from LeadTypeID 261
		*/
		SELECT * FROM dbo.GroupFunctionControl r
		WHERE r.LeadTypeID = @FromLeadTypeID

		SELECT * FROM dbo.GroupRightsDynamic r
		WHERE r.LeadTypeID = @FromLeadTypeID

		SELECT * FROM dbo.UserFunctionControl r
		WHERE r.LeadTypeID = @FromLeadTypeID

		SELECT * FROM dbo.UserRightsDynamic r
		WHERE r.LeadTypeID = @FromLeadTypeID


	END
	ELSE
	BEGIN

		/* Do it all for real */
		INSERT INTO GroupFunctionControl(ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID) 
		SELECT r.ClientPersonnelAdminGroupID, r.ModuleID, r.FunctionTypeID, r.HasDescendants, r.RightID, @ToLeadTypeID
		FROM GroupFunctionControl r 
		WHERE r.LeadTypeID = @FromLeadTypeID

		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT r.ClientPersonnelAdminGroupID, r.FunctionTypeID, @ToLeadTypeID, newDFP.DetailFieldPageID, r.RightID
		FROM GroupRightsDynamic r 
		INNER JOIN DetailFieldPages oldDFP ON oldDFP.DetailFieldPageID = r.ObjectID AND oldDFP.LeadTypeID = @FromLeadTypeID 
		INNER JOIN DetailFieldPages newDFP ON newDFP.PageName = oldDFP.PageName AND newDFP.PageCaption = oldDFP.PageCaption AND newDFP.LeadOrMatter = oldDFP.LeadOrMatter AND newDFP.LeadTypeID = @ToLeadTypeID 
		WHERE r.LeadTypeID = @FromLeadTypeID 
		AND r.FunctionTypeID = 12 -- Pages

		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT r.ClientPersonnelAdminGroupID, r.FunctionTypeID, @ToLeadTypeID, newET.EventTypeID, r.RightID
		FROM GroupRightsDynamic r 
		INNER JOIN EventType oldET ON oldET.EventTypeID = r.ObjectID AND oldET.LeadTypeID = @FromLeadTypeID 
		INNER JOIN EventType newET ON newET.EventTypeName = oldET.EventTypeName AND newET.EventTypeDescription = oldET.EventTypeDescription AND newET.EventSubtypeID = oldET.EventSubtypeID AND newET.LeadTypeID = @ToLeadTypeID 
		WHERE r.LeadTypeID = @FromLeadTypeID 
		AND r.FunctionTypeID = 13 -- EventTypes

		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT r.ClientPersonnelAdminGroupID, r.FunctionTypeID, @ToLeadTypeID, newDF.DetailFieldID, r.RightID
		FROM GroupRightsDynamic r 
		INNER JOIN DetailFields oldDF ON oldDF.DetailFieldID = r.ObjectID AND oldDF.LeadTypeID = @FromLeadTypeID 
		INNER JOIN DetailFields newDF ON newDF.FieldName = oldDF.FieldName AND newDF.FieldCaption = oldDF.FieldCaption AND newDF.LeadOrMatter = oldDF.LeadOrMatter AND newDF.LeadTypeID = @ToLeadTypeID 
		WHERE r.LeadTypeID = @FromLeadTypeID 
		AND r.FunctionTypeID = 25 -- Special Fields

		INSERT INTO UserFunctionControl(ClientPersonnelID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID) 
		SELECT r.ClientPersonnelID, r.ModuleID, r.FunctionTypeID, r.HasDescendants, r.RightID, @ToLeadTypeID
		FROM UserFunctionControl r 
		WHERE r.LeadTypeID = @FromLeadTypeID

		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT r.ClientPersonnelID, r.FunctionTypeID, @ToLeadTypeID, newDFP.DetailFieldPageID, r.RightID
		FROM UserRightsDynamic r 
		INNER JOIN DetailFieldPages oldDFP ON oldDFP.DetailFieldPageID = r.ObjectID AND oldDFP.LeadTypeID = @FromLeadTypeID 
		INNER JOIN DetailFieldPages newDFP ON newDFP.PageName = oldDFP.PageName AND newDFP.PageCaption = oldDFP.PageCaption AND newDFP.LeadOrMatter = oldDFP.LeadOrMatter AND newDFP.LeadTypeID = @ToLeadTypeID 
		WHERE r.LeadTypeID = @FromLeadTypeID 
		AND r.FunctionTypeID = 12 -- Pages

		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT r.ClientPersonnelID, r.FunctionTypeID, @ToLeadTypeID, newET.EventTypeID, r.RightID
		FROM UserRightsDynamic r 
		INNER JOIN EventType oldET ON oldET.EventTypeID = r.ObjectID AND oldET.LeadTypeID = @FromLeadTypeID 
		INNER JOIN EventType newET ON newET.EventTypeName = oldET.EventTypeName AND newET.EventTypeDescription = oldET.EventTypeDescription AND newET.EventSubtypeID = oldET.EventSubtypeID AND newET.LeadTypeID = @ToLeadTypeID 
		WHERE r.LeadTypeID = @FromLeadTypeID 
		AND r.FunctionTypeID = 13 -- EventTypes

		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT r.ClientPersonnelID, r.FunctionTypeID, @ToLeadTypeID, newDF.DetailFieldID, r.RightID
		FROM UserRightsDynamic r 
		INNER JOIN DetailFields oldDF ON oldDF.DetailFieldID = r.ObjectID AND oldDF.LeadTypeID = @FromLeadTypeID 
		INNER JOIN DetailFields newDF ON newDF.FieldName = oldDF.FieldName AND newDF.FieldCaption = oldDF.FieldCaption AND newDF.LeadOrMatter = oldDF.LeadOrMatter AND newDF.LeadTypeID = @ToLeadTypeID 
		WHERE r.LeadTypeID = @FromLeadTypeID 
		AND r.FunctionTypeID = 25 -- Special Fields

	END
END





GO
GRANT VIEW DEFINITION ON  [dbo].[CopyRightsForNewLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CopyRightsForNewLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CopyRightsForNewLeadType] TO [sp_executeall]
GO
