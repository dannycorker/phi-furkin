SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AutoAdjudication table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_Find]
(

	@SearchUsingOR bit   = null ,

	@AutoAdjudicationID int   = null ,

	@EventChoiceID int   = null ,

	@AutoAdjudicationTypeID int   = null ,

	@ObjectID int   = null ,

	@AutoAdjudicationFieldTypeID int   = null ,

	@Comparison varchar (50)  = null ,

	@Value1 varchar (250)  = null ,

	@Value2 varchar (250)  = null ,

	@AppliesTo varchar (50)  = null ,

	@Count int   = null ,

	@ValueTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AutoAdjudicationID]
	, [EventChoiceID]
	, [AutoAdjudicationTypeID]
	, [ObjectID]
	, [AutoAdjudicationFieldTypeID]
	, [Comparison]
	, [Value1]
	, [Value2]
	, [AppliesTo]
	, [Count]
	, [ValueTypeID]
    FROM
	[dbo].[AutoAdjudication] WITH (NOLOCK) 
    WHERE 
	 ([AutoAdjudicationID] = @AutoAdjudicationID OR @AutoAdjudicationID IS NULL)
	AND ([EventChoiceID] = @EventChoiceID OR @EventChoiceID IS NULL)
	AND ([AutoAdjudicationTypeID] = @AutoAdjudicationTypeID OR @AutoAdjudicationTypeID IS NULL)
	AND ([ObjectID] = @ObjectID OR @ObjectID IS NULL)
	AND ([AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID OR @AutoAdjudicationFieldTypeID IS NULL)
	AND ([Comparison] = @Comparison OR @Comparison IS NULL)
	AND ([Value1] = @Value1 OR @Value1 IS NULL)
	AND ([Value2] = @Value2 OR @Value2 IS NULL)
	AND ([AppliesTo] = @AppliesTo OR @AppliesTo IS NULL)
	AND ([Count] = @Count OR @Count IS NULL)
	AND ([ValueTypeID] = @ValueTypeID OR @ValueTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AutoAdjudicationID]
	, [EventChoiceID]
	, [AutoAdjudicationTypeID]
	, [ObjectID]
	, [AutoAdjudicationFieldTypeID]
	, [Comparison]
	, [Value1]
	, [Value2]
	, [AppliesTo]
	, [Count]
	, [ValueTypeID]
    FROM
	[dbo].[AutoAdjudication] WITH (NOLOCK) 
    WHERE 
	 ([AutoAdjudicationID] = @AutoAdjudicationID AND @AutoAdjudicationID is not null)
	OR ([EventChoiceID] = @EventChoiceID AND @EventChoiceID is not null)
	OR ([AutoAdjudicationTypeID] = @AutoAdjudicationTypeID AND @AutoAdjudicationTypeID is not null)
	OR ([ObjectID] = @ObjectID AND @ObjectID is not null)
	OR ([AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID AND @AutoAdjudicationFieldTypeID is not null)
	OR ([Comparison] = @Comparison AND @Comparison is not null)
	OR ([Value1] = @Value1 AND @Value1 is not null)
	OR ([Value2] = @Value2 AND @Value2 is not null)
	OR ([AppliesTo] = @AppliesTo AND @AppliesTo is not null)
	OR ([Count] = @Count AND @Count is not null)
	OR ([ValueTypeID] = @ValueTypeID AND @ValueTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Find] TO [sp_executeall]
GO
