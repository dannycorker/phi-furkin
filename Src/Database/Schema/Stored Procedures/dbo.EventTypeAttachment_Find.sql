SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeAttachment table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_Find]
(

	@SearchUsingOR bit   = null ,

	@AttachmentID int   = null ,

	@EventTypeID int   = null ,

	@AttachmentEventTypeID int   = null ,

	@All bit   = null ,

	@AttachmentEventGroupID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AttachmentID]
	, [EventTypeID]
	, [AttachmentEventTypeID]
	, [All]
	, [AttachmentEventGroupID]
    FROM
	[dbo].[EventTypeAttachment] WITH (NOLOCK) 
    WHERE 
	 ([AttachmentID] = @AttachmentID OR @AttachmentID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([AttachmentEventTypeID] = @AttachmentEventTypeID OR @AttachmentEventTypeID IS NULL)
	AND ([All] = @All OR @All IS NULL)
	AND ([AttachmentEventGroupID] = @AttachmentEventGroupID OR @AttachmentEventGroupID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AttachmentID]
	, [EventTypeID]
	, [AttachmentEventTypeID]
	, [All]
	, [AttachmentEventGroupID]
    FROM
	[dbo].[EventTypeAttachment] WITH (NOLOCK) 
    WHERE 
	 ([AttachmentID] = @AttachmentID AND @AttachmentID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([AttachmentEventTypeID] = @AttachmentEventTypeID AND @AttachmentEventTypeID is not null)
	OR ([All] = @All AND @All is not null)
	OR ([AttachmentEventGroupID] = @AttachmentEventGroupID AND @AttachmentEventGroupID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Find] TO [sp_executeall]
GO
