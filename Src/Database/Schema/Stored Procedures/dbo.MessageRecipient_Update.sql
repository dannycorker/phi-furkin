SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the MessageRecipient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_Update]
(

	@MessageRecipientID int   ,

	@MessageID int   ,

	@ClientPersonnelID int   ,

	@PortalUserID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[MessageRecipient]
				SET
					[MessageID] = @MessageID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[PortalUserID] = @PortalUserID
					,[ClientID] = @ClientID
				WHERE
[MessageRecipientID] = @MessageRecipientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Update] TO [sp_executeall]
GO
