SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-15
-- Description:	Copy MDV or LDV field to another MDV or LDV field
-- Mods
-- 2015-11-30 DCM Update @FieldClientID to @ToFieldClientID on 'To' select
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CopyFieldToField]
(
	@FromDetailFieldID INT,
	@ToDetailFieldID INT,
	@ObjectID INT,
	@ClientPersonnelID INT = NULL
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @LeadOrMatter INT,
			@ToLeadOrMatter INT,
			@ClientID INT,
			@FieldClientID INT,
			@ToFieldClientID INT,
			@MaintainHistory BIT,
			@LeadID INT

	SELECT	@LeadOrMatter = LeadOrmatter,
			@FieldClientID = ClientID
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @FromDetailFieldID

	SELECT	@ToLeadOrMatter = LeadOrmatter,
			@ToFieldClientID = ClientID,
			@MaintainHistory = MaintainHistory
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @ToDetailFieldID
	
	IF @FieldClientID<>@ToFieldClientID OR @LeadOrMatter<>@ToLeadOrMatter
	BEGIN
	
		RETURN
		
	END

	IF @ClientPersonnelID IS NULL
	BEGIN
		SELECT TOP 1 @ClientPersonnelID=ClientPersonnelID FROM ClientPersonnel WITH (NOLOCK) 
		WHERE UserName LIKE 'Aquarium%Automation' AND ClientID=@FieldClientID
	END
	
	SELECT @LeadID=LeadID FROM Matter WHERE (@LeadOrMatter=1 AND LeadID=@ObjectID)
								OR (@LeadOrMatter=2 AND MatterID=@ObjectID)
	
	EXEC _C00_CreateDetailFields '@ToFieldClientID',@LeadID

	IF @LeadOrMatter = 1
	BEGIN

		UPDATE snk
		SET DetailValue=src.DetailValue
		FROM LeadDetailValues snk
		INNER JOIN LeadDetailValues src WITH (NOLOCK) ON snk.LeadID=src.LeadID AND src.DetailFieldID=@FromDetailFieldID
		WHERE snk.DetailFieldID=@ToDetailFieldID AND snk.LeadID=@ObjectID
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT snk.ClientID, snk.DetailFieldID,@LeadOrMatter, @ObjectID, NULL, snk.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID 
			FROM LeadDetailValues snk WITH (NOLOCK) WHERE snk.DetailFieldID=@ToDetailFieldID AND snk.LeadID=@ObjectID

		END

	END

	IF @LeadOrMatter = 2
	BEGIN

		UPDATE snk
		SET DetailValue=src.DetailValue
		FROM MatterDetailValues snk
		INNER JOIN MatterDetailValues src WITH (NOLOCK) ON snk.MatterID=src.MatterID AND src.DetailFieldID=@FromDetailFieldID
		WHERE snk.DetailFieldID=@ToDetailFieldID AND snk.MatterID=@ObjectID
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			SELECT snk.ClientID, snk.DetailFieldID,@LeadOrMatter, NULL, @ObjectID, snk.DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID 
			FROM MatterDetailValues snk WITH (NOLOCK) WHERE snk.DetailFieldID=@ToDetailFieldID AND snk.MatterID=@ObjectID

		END

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CopyFieldToField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CopyFieldToField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CopyFieldToField] TO [sp_executeall]
GO
