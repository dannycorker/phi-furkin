SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelFavourite table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForDashboard]
(

	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					cpf.ClientPersonnelFavouriteID,
					cpf.ClientID,
					cpf.ClientPersonnelID,
					cpf.LeadID,
					c.Fullname,
					c.Town,
					lt.LeadTypeName,
					[as].AquariumStatusName
				FROM
					ClientPersonnelFavourite cpf WITH (NOLOCK)
				INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = cpf.LeadID
				INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
				INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
				INNER JOIN AquariumStatus [as] WITH (NOLOCK) ON [as].AquariumStatusID = l.AquariumStatusID
				
				WHERE
					cpf.ClientPersonnelID = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForDashboard] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForDashboard] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForDashboard] TO [sp_executeall]
GO
