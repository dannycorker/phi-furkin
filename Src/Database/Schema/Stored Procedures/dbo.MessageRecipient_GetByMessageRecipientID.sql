SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MessageRecipient table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_GetByMessageRecipientID]
(

	@MessageRecipientID int   
)
AS


				SELECT
					[MessageRecipientID],
					[MessageID],
					[ClientPersonnelID],
					[PortalUserID],
					[ClientID]
				FROM
					[dbo].[MessageRecipient] WITH (NOLOCK) 
				WHERE
										[MessageRecipientID] = @MessageRecipientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_GetByMessageRecipientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_GetByMessageRecipientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_GetByMessageRecipientID] TO [sp_executeall]
GO
