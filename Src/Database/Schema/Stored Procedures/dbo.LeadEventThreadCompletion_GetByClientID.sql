SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadEventThreadCompletion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadEventThreadCompletionID],
					[ClientID],
					[LeadID],
					[CaseID],
					[FromLeadEventID],
					[FromEventTypeID],
					[ThreadNumberRequired],
					[ToLeadEventID],
					[ToEventTypeID]
				FROM
					[dbo].[LeadEventThreadCompletion] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_GetByClientID] TO [sp_executeall]
GO
