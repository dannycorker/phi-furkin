SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_Insert]
(

	@ClientPersonnelID int    OUTPUT,

	@ClientID int   ,

	@ClientOfficeID int   ,

	@TitleID int   ,

	@FirstName varchar (100)  ,

	@MiddleName varchar (100)  ,

	@LastName varchar (100)  ,

	@JobTitle varchar (100)  ,

	@Password varchar (65)  ,

	@ClientPersonnelAdminGroupID int   ,

	@MobileTelephone varchar (50)  ,

	@HomeTelephone varchar (50)  ,

	@OfficeTelephone varchar (50)  ,

	@OfficeTelephoneExtension varchar (50)  ,

	@EmailAddress varchar (255)  ,

	@ChargeOutRate money   ,

	@UserName varchar (201)   OUTPUT,

	@Salt varchar (50)  ,

	@AttemptedLogins int   ,

	@AccountDisabled bit   ,

	@ManagerID int   ,

	@Initials varchar (3)   OUTPUT,

	@LanguageID int   ,

	@SubClientID int   ,

	@ForcePasswordChangeOn datetime   ,

	@ThirdPartySystemId int   ,

	@CustomerID int   ,

	@IsAquarium bit   ,

	@AllowSmsCommandProcessing bit   ,

	@MemorableWord varchar (100)  ,

	@MemorableWordSalt varchar (65)  ,

	@MemorableWordAttempts int   ,

	@PendingActivation bit   
)
AS


				
				-- If we don't have an outer tranasction e.g. from the lead event 
				DECLARE @TranCount INT = @@TRANCOUNT
				IF @TranCount = 0
				BEGIN
					BEGIN TRAN
				END
				
				BEGIN TRY
				
				-- Get a new key from the master DB
				DECLARE @MasterClientPersonnelID INT
				EXEC @MasterClientPersonnelID = AquariusMaster.dbo.AQ_ClientPersonnel_Insert @ClientID, @EmailAddress, @ThirdPartySystemId
				
				-- Set context info to allow access to table
				DECLARE @ContextInfo VARBINARY(100) = CAST('ClientPersonnel' AS VARBINARY)
				SET CONTEXT_INFO @ContextInfo
				
				SET IDENTITY_INSERT [dbo].[ClientPersonnel] ON
				
				INSERT INTO [dbo].[ClientPersonnel]
					(
					[ClientPersonnelID],
					[ClientID]
					,[ClientOfficeID]
					,[TitleID]
					,[FirstName]
					,[MiddleName]
					,[LastName]
					,[JobTitle]
					,[Password]
					,[ClientPersonnelAdminGroupID]
					,[MobileTelephone]
					,[HomeTelephone]
					,[OfficeTelephone]
					,[OfficeTelephoneExtension]
					,[EmailAddress]
					,[ChargeOutRate]
					,[Salt]
					,[AttemptedLogins]
					,[AccountDisabled]
					,[ManagerID]
					,[LanguageID]
					,[SubClientID]
					,[ForcePasswordChangeOn]
					,[ThirdPartySystemId]
					,[CustomerID]
					,[IsAquarium]
					,[AllowSmsCommandProcessing]
					,[MemorableWord]
					,[MemorableWordSalt]
					,[MemorableWordAttempts]
					,[PendingActivation]
					)
				VALUES
					(
					@MasterClientPersonnelID,
					@ClientID
					,@ClientOfficeID
					,@TitleID
					,@FirstName
					,@MiddleName
					,@LastName
					,@JobTitle
					,@Password
					,@ClientPersonnelAdminGroupID
					,@MobileTelephone
					,@HomeTelephone
					,@OfficeTelephone
					,@OfficeTelephoneExtension
					,@EmailAddress
					,@ChargeOutRate
					,@Salt
					,@AttemptedLogins
					,@AccountDisabled
					,@ManagerID
					,@LanguageID
					,@SubClientID
					,@ForcePasswordChangeOn
					,@ThirdPartySystemId
					,@CustomerID
					,@IsAquarium
					,@AllowSmsCommandProcessing
					,@MemorableWord
					,@MemorableWordSalt
					,@MemorableWordAttempts
					,@PendingActivation
					)
				
				-- If we don't have an outer tran we can commit here
				IF @TranCount = 0
				BEGIN
					COMMIT
				END
				
				-- Clear the things we set above
				SET IDENTITY_INSERT [dbo].[ClientPersonnel] OFF
				SET CONTEXT_INFO 0x0
				
				END TRY			
				BEGIN CATCH    
					
					-- If we have any tran open then we need to rollback
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK
					END
					
					-- Clear the things we set above
					SET IDENTITY_INSERT [dbo].[ClientPersonnel] OFF
					SET CONTEXT_INFO 0x0
					
					DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
					DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
					DECLARE @ErrorState INT = ERROR_STATE()

					RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
					
				END CATCH
				-- Get the identity value
				SET @ClientPersonnelID = SCOPE_IDENTITY()
									
				-- Select computed columns into output parameters
				SELECT
 @UserName = [UserName]
, @Initials = [Initials]
				FROM
					[dbo].[ClientPersonnel] WITH (NOLOCK) 
				WHERE
[ClientPersonnelID] = @ClientPersonnelID 
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Insert] TO [sp_executeall]
GO
