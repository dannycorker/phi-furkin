SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientTypes_Update]
(

	@ClientTypeID int   ,

	@ClientTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientTypes]
				SET
					[ClientTypeName] = @ClientTypeName
				WHERE
[ClientTypeID] = @ClientTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientTypes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientTypes_Update] TO [sp_executeall]
GO
