SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-20
-- Description:	Get cronofy details by ClientID if required.
-- =============================================
CREATE PROCEDURE [dbo].[Cronofy__GetLogonDetails]
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Client_ID VARCHAR(200),
			@Client_Secret VARCHAR(200),
			@RedirectURL VARCHAR(200),
			@StartURL VARCHAR(2000)

	SELECT @Client_ID = 'JgukmZJMeYHk5z0qeAbbELdODDfoF56z',
			@Client_Secret = 'V4n40hoUIMtq0GEJxn6YWHDf5rgNSqwiTKF6II0BbyOH1C8olYIY8kkpppPe7uV2TZKabnlqKAaiBZXpkeTAtQ',
			@RedirectURL = 'https://aqnet.aquarium-software.com/CustomPages/Client_0/AddCronofy.aspx'
			
	SELECT @StartURL = 'https://app.cronofy.com/oauth/authorize?response_type=code&client_id='+ @Client_ID +'&redirect_uri='+ @RedirectURL +'&scope=create_event%20list_calendars%20delete_event'
	
	SELECT @Client_ID AS [Client_ID],
			@Client_Secret AS [Client_Secret],
			@RedirectURL AS [RedirectURL],
			@StartURL AS [StartURL]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Cronofy__GetLogonDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cronofy__GetLogonDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cronofy__GetLogonDetails] TO [sp_executeall]
GO
