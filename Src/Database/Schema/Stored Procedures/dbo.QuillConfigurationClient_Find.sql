SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuillConfigurationClient table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationClient_Find]
(

	@SearchUsingOR bit   = null ,

	@QuillConfigurationClientID int   = null ,

	@AquariumClientID int   = null ,

	@QuillClientID int   = null ,

	@Username varchar (50)  = null ,

	@Password varchar (50)  = null ,

	@URI varchar (150)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuillConfigurationClientID]
	, [AquariumClientID]
	, [QuillClientID]
	, [Username]
	, [Password]
	, [URI]
    FROM
	[dbo].[QuillConfigurationClient] WITH (NOLOCK) 
    WHERE 
	 ([QuillConfigurationClientID] = @QuillConfigurationClientID OR @QuillConfigurationClientID IS NULL)
	AND ([AquariumClientID] = @AquariumClientID OR @AquariumClientID IS NULL)
	AND ([QuillClientID] = @QuillClientID OR @QuillClientID IS NULL)
	AND ([Username] = @Username OR @Username IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([URI] = @URI OR @URI IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuillConfigurationClientID]
	, [AquariumClientID]
	, [QuillClientID]
	, [Username]
	, [Password]
	, [URI]
    FROM
	[dbo].[QuillConfigurationClient] WITH (NOLOCK) 
    WHERE 
	 ([QuillConfigurationClientID] = @QuillConfigurationClientID AND @QuillConfigurationClientID is not null)
	OR ([AquariumClientID] = @AquariumClientID AND @AquariumClientID is not null)
	OR ([QuillClientID] = @QuillClientID AND @QuillClientID is not null)
	OR ([Username] = @Username AND @Username is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([URI] = @URI AND @URI is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationClient_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Find] TO [sp_executeall]
GO
