SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records through a junction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter_GetByLeadEventIDFromLeadEventMatterLinks]
(

	@LeadEventID int   
)
AS


SELECT dbo.[Matter].[MatterID]
       ,dbo.[Matter].[ClientID]
       ,dbo.[Matter].[MatterRef]
       ,dbo.[Matter].[CustomerID]
       ,dbo.[Matter].[LeadID]
       ,dbo.[Matter].[MatterStatus]
       ,dbo.[Matter].[RefLetter]
       ,dbo.[Matter].[BrandNew]
       ,dbo.[Matter].[CaseID]
  FROM dbo.[Matter] WITH (NOLOCK)
 WHERE EXISTS (SELECT 1
                 FROM dbo.[LeadEventMatterLinks] WITH (NOLOCK)
                WHERE dbo.[LeadEventMatterLinks].[LeadEventID] = @LeadEventID
                  AND dbo.[LeadEventMatterLinks].[MatterID] = dbo.[Matter].[MatterID]
                  )
				SELECT @@ROWCOUNT			
				





GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByLeadEventIDFromLeadEventMatterLinks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter_GetByLeadEventIDFromLeadEventMatterLinks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter_GetByLeadEventIDFromLeadEventMatterLinks] TO [sp_executeall]
GO
