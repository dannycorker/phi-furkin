SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		AQUARIUM SOFTWARE
-- Create date: 2014-09-11
-- Description:	Proc to Run SAE for Client 600
-- NG  2020-09-07 update customer level state variance RL
-- NG  2020-09-28 add a note to inactive collections case
-- NG  2020-09-28 update payment method on PA matter from Coll
-- AAD 2021-03-10 for FURKIN-337 - Call _C600_SetDocumentFields during payment day change (158772)
-- CMJ 2021-04-12 FURKIN-180 - Populate Amount Due & Payment Due Date for Payment Failed - Document Fields
-- NG  2021-04-27 Update state level variance field rl selection for Canada
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SAE] 
(
	@LeadEventID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	 @AccountName							VARCHAR(100)
			,@AccNumber								VARCHAR(100)
			,@AccSortCode							VARCHAR(10)
			,@BankName								VARCHAR(250)
			,@AccountID								INT
			,@AdjustmentDate						DATE
			,@Amount								MONEY
			,@AqAutomation							INT
			,@CaseID								INT
			,@ClaimCaseID							INT 
			,@ClaimLeadID							INT
			,@ClientID								INT
			,@ColCaseID								INT
			,@ColLeadID								INT
			,@Cases									INT
			,@ClientAccountID						INT
			,@PolicyAdminMatterID					INT
			,@ColMatterID							INT
			,@Comments								VARCHAR(2000)
			,@CustomerID							INT
			,@CustomerLedgerID						INT
			,@CardTransactionID						INT
			,@DetailValue							VARCHAR(2000)
			,@EffectiveDate							DATE
			,@ErrorMessage							VARCHAR(2000)
			,@ErrorCode								VARCHAR(20)
			,@EventTypeID							INT
			,@InceptionDate							DATE
			,@IsOK									INT
			,@IPT									DECIMAL (18,4) 
			,@LeadID								INT
			,@LeadTypeID							INT
			,@LinkedTRID							INT
			,@LogEntry								VARCHAR(2000)
			,@MatterID								INT
			,@MattersToCopy							dbo.tvpIntInt
			,@MTARequired							INT
			,@NewAccountID							INT
			,@NewCaseID								INT
			,@NewCustomerID							INT
			,@NewLeadEventID						INT
			,@NewLeadID								INT
			,@NewMatterID							INT
			,@NoteLeadEventID						INT
			,@Now									DATETIME = dbo.fn_GetDate_Local()
			,@ObjectID								INT 
			,@DPACount								INT
			,@OldAccountID							INT
			,@PACaseID								INT
			,@PALeadEventID							INT
			,@PaymentID								INT
			,@PayRef								VARCHAR(100)
			,@PolMatterID							INT
			,@PolicyLeadID							INT
			,@PolicyMatterID						INT
			,@PolicyMatters							dbo.tvpIntInt
			,@PurchasedProductID					INT
			,@PurchasedProductPaymentScheduleID		INT
			,@ReasonCode							VARCHAR(2)
			,@RenewalDate							DATE
			,@ResourceListID						INT
			,@SchemeID								INT
			,@TableRowID							INT
			,@ValueInt								INT
			,@ValueMoney							MONEY
			,@VarcharDate							VARCHAR(10)
			,@UsesPolicyAdminProcess				INT =1
			,@WaitDate								DATE
			,@WhoCreated							INT
			,@WhenCreated							DATETIME
			,@LeadDocumentID						INT 
			,@CustomerPaymentScheduleID				INT
			,@EventComments							VARCHAR(2000) = ''
			,@ValueDate								DATE
			,@RowCount								INT
			,@IdId									dbo.tvpIntInt
			,@AnyID									BIGINT
			,@DPAOutcome							INT
			,@DetailValueData						dbo.tvpDetailValueData
			,@DPAName								VARCHAR (2000)
			,@DPAAddressLn1							VARCHAR (20)
			,@DPAAddContactName 					VARCHAR (20)
			,@DPAPHName								VARCHAR (20)
			,@DPAPHDoB								VARCHAR (20)
			,@DPAPolNumber							VARCHAR (20)
			,@DPAPetName							VARCHAR (20)
			,@DPAPetDOB								VARCHAR (20)
			,@DPAPolStart							VARCHAR (20)
			,@DPAPremAmount							VARCHAR (20)
			,@DPABankName							VARCHAR (20)
			,@DPAPasswordUsed						VARCHAR (20)
			,@DPADate								DATE
			,@DPAPassed								VARCHAR (10)
			,@DPAReasonFailed						VARCHAR (2000)
			,@DPAAuthCount							INT
			,@PremiumGross							DECIMAL (18,2)
			,@PremiumNet							DECIMAL (18,2)
			,@PremumIPT								DECIMAL (18,2)
			,@CustomerSanction						INT
			,@DocumentQueueID						INT
			,@DocumentTypeID						INT
			,@DocumentTitle							VARCHAR(100)
			,@LookupListItemID						INT
			,@EvidenceRequired						INT
			,@TypeofPayment							VARCHAR(100)
			,@MaxAge								INT
			,@SelectedSection						INT
			,@AgeCheck								INT
			,@SchemeType							INT
			,@NewCardTransactionID					INT
			,@ClientPaymentGatewayID				INT
			,@PaymentTypeID							INT
			,@AmountNet								DECIMAL(18,2) 
			,@AmountVAT								DECIMAL(18,2) 
			,@NewCustomerPaymentScheduleID			INT
			,@PaymentGatewayID						INT  
			,@MerchantCode							VARCHAR(200)
			,@Firstname								VARCHAR(100)
			,@Lastname								VARCHAR(100)
			,@Postcode								VARCHAR(50)
			,@MultiMatterID							INT
			,@MaxCounter							INT
			,@Counter								INT = 1
			,@AuthoriryToUseAccount					INT
			,@SchemeMatterID						INT
			,@CustomFollowUpDays					INT
			,@OriginalEventTypeID					INT
			,@EarliestPaymentDate					DATE
			,@AmountPaidToDate						MONEY
			,@ActualCollectionDate					DATE
			,@PaymentGross							MONEY
			,@PaymentNet							MONEY
			,@PaymentTax							MONEY
			,@CommissionClawback					MONEY
			,@PAMatterIDFromCollectionsMatterID		INT
			,@ProductName							VARCHAR(250)
			,@ProductDescription					VARCHAR(2000)
			,@PremiumTax							NUMERIC(18,2)
			,@CurrentPremium						MONEY
			,@FirstRenewalActualCollectionDate		DATE
			,@WorkflowTaskID						INT
			,@CollectionsMatterID					INT
			,@Policyinceptiondate					DATE
			,@PolicyStartDate						DATE
			,@AccountNo								Varchar(25)
			,@SortNo								Varchar(25)
			,@InstitutionNo							VARCHAR(25)
			,@IntervalID							INT
			,@BillingID								INT
			,@PurchaseProductID						INT
			,@AnnualPrem							DECIMAL (18,2)
			,@AnnualDate							DATE
			,@PaymentDay							INT
			,@FirstCollectionDate					DATE
			,@returnValue							varchar(100)=''
			,@PolicyMatterIDFromCollectionsMatter	INT
			,@NextPaymentDate						VARCHAR(10)
			,@NextPaymentAmount						NUMERIC(18,2)
			,@NewAccountReference					VARCHAR(10)
			,@AccountTypeID							INT
			,@ValueBinary							VARBINARY(MAX)
			,@JsonString							VARCHAR(MAX)
			,@Address1								VARCHAR(50)
			,@Address2								VARCHAR(50)
			,@AddressTown							VARCHAR(50)
			,@AddressCounty							VARCHAR(50)
			,@ObjectMappingID						UNIQUEIDENTIFIER /*2019-12-17 MAB added*/
			,@OneVisionID							INT /*2020-07-13 ALM added*/
			,@LatestEvent							INT
			,@ProductStartDate						DATE
			,@NotificationDate						DATE
			,@PolicyOneVisionID						INT
			,@WellnessOneVisionID					INT
			,@NewMethod								INT
			,@PolAdminMatterID						INT
			,@ExamFeeOneVisionID					INT
			,@ExamFeeOneVisionIDNew					INT
			,@WellnessOneVisionIDNew				INT
			,@State									VARCHAR(10)
			,@CancellationType						INT
			,@DaysIntoPolicyYear					INT
			,@IsRenewal								BIT
			,@CoolingOffPeriod						INT
			,@StartDate								DATE
			,@PetDOB								DATE
			,@AgeInMonths							INT
			,@PetAge								VARCHAR(25)
			,@BrainTreePaymentMethodCreateID		INT
			,@WrittenPremiumID						INT
			,@AmountDue								NUMERIC(18,2) /*CMJ FURKIN-180*/
			,@PaymentDate							DATE /*CMJ FURKIN-180*/
			,@CountryID								INT
			,@InstitutionNumber						VARCHAR(50)
	
	DECLARE	@CasesToUpdate TABLE (CaseID INT, Updated VARCHAR (1000))
	
	DECLARE @NumberedValues TABLE ( ID INT Identity, Value VARCHAR(2000) )
	
	DECLARE	@AccountUpdates TABLE ( PurchasedProductID INT, PolMatterID INT, NewColMatterID INT, Done INT )
	
	DECLARE @MandateMatters TABLE (MatterID INT, CaseID INT, Done INT)
	
	DECLARE @SectionsToExclude TABLE ( ResourceListID INT, Done BIT )
	
	DECLARE @TableRows TABLE (TableRowID INT, Done BIT) 

	DECLARE @InsertedCardTransactions TABLE (CardTransactionID INT)

	DECLARE @UpdatedPPPS TABLE (CPSID INT)

	DECLARE @AdjDate TABLE (AdjDate DATE)

	/*2018-04-26 - Update request from AE - SA*/
	/*2018-06-22 ACE Removed customer as the email is being looked up where it's needed anyway...*/
	SELECT TOP (1) 
		 @LeadID		= le.LeadID
		,@EventTypeID	= le.EventTypeID
		,@CustomerID	= l.CustomerID
		,@ClientID		= l.ClientID
		,@CaseID		= le.CaseID
		,@WhoCreated	= le.WhoCreated
		,@MatterID		= m.MatterID
		,@WhenCreated	= le.WhenCreated
		,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
		,@LeadTypeID	= l.LeadTypeID
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID		
	
	PRINT 'EventTypeID ' + CONVERT(VARCHAR,@EventTypeID)
	
	IF @EventTypeID IN ( 160527 ) /*New Reimbursement Account Details*/
	BEGIN
		/*
		2021-02-19 CPS for JIRA FURKIN-72 | Capture and save new account details for claim payment
		Adapted from EventTypeID 159005 on Aquarius606Dev
		*/
		SELECT @EventComments = ''

		-- Get the AccountID for our failed claim payment
		SELECT @AccountID = (SELECT TOP(1) ac.AccountID
							FROM CustomerPaymentSchedule cps WITH (NOLOCK)
							INNER JOIN Account ac WITH (NOLOCK) on ac.AccountID = cps.AccountID
							WHERE ac.AccountUseID = 2
							AND cps.PaymentStatusID = 4 /*Failed*/
							AND cps.CustomerID = @CustomerID
							AND cps.ClientID = @ClientID
							ORDER BY 1 DESC)


		SELECT @AccountNo	  = [dbo].[fnGetSimpleDv](315891, @MatterID) /*New Reimbursement Account Number*/
		SELECT @AccSortCode   = [dbo].[fnGetSimpleDv](315890, @MatterID) /*New Reimbursement Routing Number*/
		SELECT @InstitutionNo = [dbo].[fnGetSimpleDv](315893, @MatterID) /*New Reimbursement Institution Number*/
		
		IF @AccountID IS NOT NULL
		BEGIN
			SELECT @EventComments += 'AccountID: '			+ CONVERT(VARCHAR,@AccountID) + ' updated.' + CHAR(13)+CHAR(10)
								  +  'Account Number: '		+ ISNULL(ac.AccountNumber		,'NULL') + ' -> ' + ISNULL(@AccountNo    ,'NULL') + CHAR(13)+CHAR(10)
								  +  'Routing Number: '		+ ISNULL(ac.SortCode			,'NULL') + ' -> ' + ISNULL(@AccSortCode  ,'NULL') + CHAR(13)+CHAR(10)
								  +  'Institution Number: ' + ISNULL(ac.InstitutionNumber	,'NULL') + ' -> ' + ISNULL(@InstitutionNo,'NULL') + CHAR(13)+CHAR(10)
			FROM Account ac WITH (NOLOCK) 
			WHERE ac.AccountID = @AccountID

			UPDATE a
			SET	 a.Sortcode = @AccSortCode
				,a.AccountNumber = @AccountNo
				,a.InstitutionNumber = @InstitutionNo
			FROM Account a WITH (NOLOCK)
			WHERE a.AccountID = @AccountID
			AND a.ClientID = @ClientID
		END	
		ELSE
		BEGIN
			SELECT @ErrorMessage = '<br><br><font color="red">Error: no Payment Failure found to process</font>'
			RAISERROR( @ErrorMessage, 16, 1 )
		END

		-- Add a note to Policy Admin for information and to give us a LeadEventID to trigger the Mediate behaviour
		SELECT	 @PACaseID		= m.CaseID
				,@OneVisionID	= ov.OneVisionID
		FROM Matter m WITH (NOLOCK)
		INNER JOIN dbo.OneVision ov WITH (NOLOCK) on ov.PAMatterID = m.MatterID
		WHERE m.MatterID = dbo.fnGetSimpleDvAsInt(180262,@MatterID) /*Account Linked to Policy MatterID*/

		IF @PACaseID <> 0
		BEGIN
			EXEC @NoteLeadEventID = _C00_AddANote @PACaseID, 919 /*System Note*/, @EventComments, 0, @WhoCreated, 0, 1
		
			/*Need to link to PA for failed claim payment behaviour so we can speak to vision properly*/
			EXEC _C600_Mediate_QueueForVision 'PaymentAccount', 'POST', @NoteLeadEventID, @OneVisionID	

			SELECT @EventComments += ' Note LeadEventID ' + ISNULL(CONVERT(VARCHAR,@NoteLeadEventID),'NULL') + ' added to CaseID ' + CONVERT(VARCHAR,@PACaseID) + CHAR(13)+CHAR(10)
								  +  ' New Account Details sent to Claims System.'
		END
		ELSE
		BEGIN
			SELECT @EventComments += ' No Policy Admin case found.  Cannot send new Account Details to Claims System.'
		END

		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

	IF @EventTypeID IN ( 158943	/*API Transfer - Queue Customer*/
						,158946	/*API Transfer - Queue Pet*/
						,158947	/*API Transfer - Queue Policy*/
						,158948	/*API Transfer - Queue Claim*/
					   )
	BEGIN
		
		DECLARE
			@TransferType VARCHAR(50)
			,@HttpMethodDetailFieldID INT 
			,@HttpMethod VARCHAR(10)
			,@ObjectMappingDetailFieldID INT
			,@ApiOutboundKey VARCHAR(50)
			,@TransferRequestType VARCHAR(50)
			,@TransferResponseType VARCHAR(50)
			,@ParentObjectMappingID UNIQUEIDENTIFIER
		
		/*
		2019-11-22 CPS for JIRA LAGCLAIM-154 | Write to the API transfer queue
		*/
		SELECT @EventComments = ''

		SELECT	@TransferType = est.AquariumEventSubTypeName
		FROM LeadEvent le WITH (NOLOCK) 
		INNER JOIN EventType et WITH (NOLOCK) on et.EventTypeID = le.EventTypeID
		INNER JOIN AquariumEventSubtype est WITH (NOLOCK) on est.AquariumEventSubtypeID = et.AquariumEventSubtypeID
		WHERE le.LeadEventID = @LeadEventID
		AND est.AquariumEventTypeID = 21 /*Mediation Outbound Queue*/

		/*Pick up transfer-specific data.  This will eventually be stored in proper tables*/	
		SELECT	@JsonString = 
		CASE	@TransferType
				WHEN 'Customer' THEN dbo.fn_C600_Harvest_Customer_JSON	(@CustomerID,0)
				WHEN 'Pet'		THEN dbo.fn_C600_Harvest_Pet_JSON		(@LeadID	,0,0,0)
				WHEN 'Policy'	THEN dbo.fn_C600_Harvest_Policy_JSON	(@MatterID	,0)
				WHEN 'Claim'	THEN dbo.fn_C600_Harvest_Claim_JSON		(@MatterID	,0)
				ELSE ''
		END

		SELECT	@ObjectID = 
		CASE	@TransferType
				WHEN 'Customer' THEN @CustomerID
				WHEN 'Pet'		THEN @LeadID
				WHEN 'Policy'	THEN @MatterID
				WHEN 'Claim'	THEN @MatterID
				ELSE ''
		END

		SELECT	@HttpMethodDetailFieldID = 
		CASE	@TransferType
				WHEN 'Customer' THEN 313788 /*Customer API Transfer Method*/
				WHEN 'Pet'		THEN 313789 /*Pet API Transfer Method*/
				WHEN 'Policy'	THEN 313793 /*Policy API Transfer Method*/
				WHEN 'Claim'	THEN 313797 /*Claim API Transfer Method*/
				ELSE ''
		END

		SELECT	@ObjectMappingDetailFieldID =
		CASE	@TransferType
				WHEN 'Customer' THEN 313784 /*Customer API Transfer Mapping ID*/
				WHEN 'Pet'		THEN 313787 /*Pet API Transfer Mapping ID*/
				WHEN 'Policy'	THEN 313792 /*Policy API Transfer Mapping ID*/
				WHEN 'Claim'	THEN 313796 /*Claim API Transfer Mapping ID*/
				ELSE ''
		END

		SELECT	@ParentObjectMappingID =
		CASE	@TransferType
				WHEN 'Customer' THEN NULL
				WHEN 'Pet'		THEN CAST (dbo.fnGetSimpleDv(313784 /*Customer API Transfer Mapping GUID*/	, @CustomerID) AS UNIQUEIDENTIFIER)
				WHEN 'Policy'	THEN CAST (dbo.fnGetSimpleDv(313787 /*Pet API Transfer Mapping GUID*/		, @LeadID) AS UNIQUEIDENTIFIER)
				WHEN 'Claim'	THEN CAST (dbo.fnGetSimpleDv(313787 /*Pet API Transfer Mapping GUID*/		, (SELECT ltr.FromLeadID from LeadTypeRelationship ltr WITH (NOLOCK) WHERE ltr.ToMatterID = @MatterID)) AS UNIQUEIDENTIFIER)
				ELSE NULL
		END

		SELECT	 @ValueBinary = CAST(@JsonString as VARBINARY(MAX))
				,@ApiOutboundKey = dbo.fn_C600_GetDatabaseSpecificConfigValue('MediationApiOutboundKey')
				,@HttpMethod = dbo.fnGetSimpleDvLuli(@HttpMethodDetailFieldID, @ObjectID)

		/*Add the request to the queue*/	
		/*2019-12-02 MAB modified argument values for @Queue, @Flow, @Action, @RequestType, @ResponseType, @Exception*/
		/*2019-12-17 MAB added argument value for @ObjectID*/
		SELECT @TransferRequestType = @TransferType + 'Request'
			,@TransferResponseType = @TransferType + 'Response'
			,@ObjectMappingID =	CASE
									WHEN @HttpMethod = 'POST' THEN NEWID() /*If creating a new object in the target then generate a new GUID otherwise use the one that was stored when the object was created*/
									ELSE CAST (dbo.fnGetSimpleDv(@ObjectMappingDetailFieldID, @ObjectID) AS UNIQUEIDENTIFIER)
								END

		EXEC [_C00_Mediate_Push]	 @AppKey = @ApiOutboundKey, @Queue = 'Out', @Flow = 'Queue', @Action = 'Push', @Method = @HttpMethod 
									,@RequestBody = @JsonString, @RequestType = @TransferRequestType
									,@ResponseBody = NULL, @ResponseType = @TransferResponseType
									,@Exception = '', @ExceptionCount = 0
									,@ObjectID = @ObjectMappingID, @ParentObjectID = @ParentObjectMappingID
									,@QueueID = @AnyID OUTPUT

		/*2019-12-17 MAB save the mapping key in the source object*/
		IF @HttpMethod = 'POST'
		BEGIN

			EXEC dbo._C00_SimpleValueIntoField  @ObjectMappingDetailFieldID, @ObjectMappingID, @ObjectID, @WhoCreated

		END

		/*Store the request JSON as a lead document against this event*/	
		SELECT @EventComments += 'OutboundQueueID ' + ISNULL(CONVERT(VARCHAR,@AnyID),'NULL') + ' queued for export.' + CHAR(13)+CHAR(10)
								+ 'Parent object GUID = ' + ISNULL(CONVERT(VARCHAR(MAX),@ParentObjectMappingID),'NULL') + CHAR(13)+CHAR(10)
								+ 'Transfer Type = ' + ISNULL(CONVERT(VARCHAR(MAX),@TransferType),'NULL')

		SELECT @DocumentTitle	= 'FullPackage_' + ISNULL(@TransferType,'NULL')
								+ '_' + ISNULL(CONVERT(VARCHAR, + 
																CASE	@TransferType
																		WHEN 'Customer' THEN @CustomerID 
																		WHEN 'Pet' THEN @LeadID
																		WHEN 'Policy' THEN @MatterID
																		WHEN 'Claim' THEN @MatterID
																		ELSE 0
																END
																),'NULL') 
								+ '_LeadEventID_' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
								+ '_QueueID_' + ISNULL(CONVERT(VARCHAR,@AnyID),'NULL') + '.json'

		EXEC @LeadDocumentID = _C00_CreateLeadDocument @ClientID, @LeadID, @ValueBinary, @DocumentTitle, @DocumentTypeID, @WhenCreated, @WhoCreated, @DocumentTitle,NULL, 'TXT', NULL, NULL, NULL, NULL, NULL, NULL, 'TXT', NULL, NULL

		UPDATE le
		SET  LeadDocumentID = @LeadDocumentID
			,Comments = ISNULL(le.Comments + CHAR(13)+CHAR(10),'') + @EventComments
		FROM LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @LeadEventID
	END	

	IF @EventTypeID IN ( 158977 ) /*Card Payment Cancelled*/
	BEGIN
		/*
		The user has clicked "cancel" in the WorldPay iFrame.
		We can't do anything about the actual collection event because it's already been applied by the time the WP frame appears.
		Instead, we auto-adjudicate to this event and reset the payment status to New
		*/
		SELECT @EventComments = ''

		SELECT TOP 1 @NewLeadEventID = le.LeadEventID
					,@CardTransactionID	= ct.CardTransactionID
		FROM LeadEvent le WITH (NOLOCK) 
		INNER JOIN CardTransactionLeadEventLink link WITH (NOLOCK) on link.LeadEventID = le.LeadEventID
		INNER JOIN CardTransaction ct WITH (NOLOCK) on ct.CardTransactionID = link.CardTransactionID
		WHERE le.CaseID = @CaseID
		AND ct.ErrorCode = 'ERROR' AND ct.ErrorMessage = 'ERROR'
		AND le.WhenFollowedUp is NULL

		SELECT	 @CustomerLedgerID = cps.CustomerLedgerID
				,@PaymentID = p.PaymentID
		FROM CardTransaction ct WITH (NOLOCK) 
		INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) on cps.CustomerPaymentScheduleID = ct.CustomerPaymentScheduleID
		 LEFT JOIN Payment p WITH (NOLOCK) on p.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		WHERE ct.CardTransactionID = @CardTransactionID

		UPDATE cps
		SET  PaymentStatusID	= 1 /*New*/
			,ReconciledDate		= NULL
			,CustomerLedgerID	= NULL
		FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
		WHERE cps.CustomerLedgerID = @CustomerLedgerID

		SELECT @EventComments += CONVERT(VARCHAR,@@ROWCOUNT) + ' CustomerPaymentSchedule'

		UPDATE pps
		SET  PaymentStatusID	= 1 /*New*/
			,ReconciledDate		= NULL
			,CustomerLedgerID	= NULL
		FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
		WHERE pps.CustomerLedgerID = @CustomerLedgerID

		SELECT @EventComments += ' and ' + CONVERT(VARCHAR,@@ROWCOUNT) + ' PurchasedProductPaymentSchedule records set to status "new" for CardTransactionID ' + ISNULL(CONVERT(VARCHAR,@CardTransactionID),'NULL') + '.' + CHAR(13)+CHAR(10)
		
		DELETE P
		FROM Payment p WITH (NOLOCK) 
		WHERE p.PaymentID = @PaymentID
		
		DELETE cl
		FROM CustomerLedger cl WITH (NOLOCK) 
		WHERE cl.CustomerLedgerID = @CustomerLedgerID

		SELECT @EventComments += 'PaymentID ' + ISNULL(CONVERT(VARCHAR,@PaymentID),'NULL') + ' and CustomerLedgerID ' + ISNULL(CONVERT(VARCHAR,@CustomerLedgerID),'NULL') + ' removed.' + CHAR(13)+CHAR(10)
	
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

	/*New Reimbursement Account Details*/
	IF @EventTypeID = 159005
	BEGIN

		/*Pick up the affinity specific client account ID to write to the customers account record*/ 
		SELECT @ClientAccountID = rldv.ValueInt 
		FROM CustomerDetailValues cdv WITH (NOLOCK) 
		INNER JOIN Matter m WITH (NOLOCK) on m.CustomerID = cdv.CustomerID 
		INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
		WHERE m.MatterID = @MatterID 
		AND cdv.DetailFieldID = 170144 /*Affinity Details*/
		AND rldv.DetailFieldID = 179950 /*Claim Payments AccountID*/

		-- Get the AccountID for our failed claim payment
		SELECT @AccountID = (SELECT TOP(1) AccountID
							FROM CustomerPaymentSchedule cps WITH (NOLOCK)
							WHERE cps.ClientAccountID = @ClientAccountID
							AND cps.PaymentStatusID = 4
							AND cps.CustomerID = @CustomerID
							AND cps.ClientID = @ClientID
							ORDER BY 1 DESC)

		SELECT @AccountNo = [dbo].[fnGetSimpleDvAsInt](313946, @MatterID)
		SELECT @AccSortCode = [dbo].[fnGetSimpleDvAsInt](313946, @MatterID)
		
		IF @AccountID IS NOT NULL
		BEGIN
			UPDATE a
			SET a.Sortcode = @AccSortCode, a.AccountNumber = @AccountNo
			FROM Account a WITH (NOLOCK)
			WHERE a.AccountID = @AccountID
			AND a.ClientID = @ClientID
		END

	END
		
	/*Re-rate policy (pre-renewal) - Post MTA*/
	IF @EventTypeID = 158836
	BEGIN

		/*If we are a Re-rate policy (pre-renewal) - Post MTA then pretend to be a Re-rate policy (pre-renewal)*/
		UPDATE LeadEvent
		SET EventTypeID = 155235
		FROM LeadEvent 
		WHERE LeadEventID = @LeadEventID

		SELECT @EventTypeID = 155235

		SELECT @OriginalEventTypeID = 158836

	END
	
	/*Begin - Paper DD form Received*/ 
	IF @EventTypeID = 158747 
	BEGIN 

		/*If we have received a paper DD form here then we need to unlock this policy to finish it's setup*/
		SELECT @ColMatterID = dbo.fn_C600_GetCollectionsMatterID(@MatterID) 
		
		EXEC _C00_SimpleValueIntoField 179904,5145,@MatterID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 179907,5145,@ColMatterID, @WhoCreated

	END 
		
	/*Claim Payment Failure*/ 
	/*BACs Payment Reversal record has been received, mark the claim payment rows as failed*/ 
	IF @EventTYpeID =  158744
	BEGIN
		
		/*REMOVED THIS SECTION AS PAY ELECTRONICALLY NOT PART OF PROCESS*/  /*Defect ID 440*/
		--/*Remove the payment method so the user can choose*/	
		EXEC _C00_SimpleValueIntoField 179890,0,@MatterID,@WhoCreated
		
		SELECT @Comments = le.Comments 
		FROM LeadEvent le WITH (NOLOCK) 
		where le.LeadEventID = @LeadEventID 
		
		SELECT @ValueINT = (LEN(@Comments) +9) - (PATINDEX('%PaymentID%',@Comments)) --UH 2018/07/05
				
		SELECT @ValueINT = LEN(@Comments) - @ValueINT
		
		SELECT @ValueINT = LEN(@Comments) - @ValueINT
		
		SELECT @PaymentID =  RIGHT(@comments, @ValueINT) 
		
		SELECT @TableRowID = cps.SourceID FROM Payment p WITH (NOLOCK) 
		INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) on cps.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 
		WHERE p.PaymentID = @PaymentID 
			
		INSERT INTO @TableRows (TableRowID,Done)
		SELECT tr.TableRowID, 0 
		FROM TableRows tr WITH (NOLOCK)
		Where tr.DetailFieldID = 154485 
		and tr.MatterID = @MatterID 
		AND tr.TableRowID = @TableRowID
		
		WHILE EXISTS (SELECT * FROM @TableRows t 
					  WHere t.Done = 0) 
		BEGIN 
		
			SELECT top 1 @TableRowID = t.TableRowID 
			FROM @TableRows t 
			WHERE Done = 0 
			Order by Done desc 
			
			SELECT @Now = CAST(@Now as DATE)  
			
			EXEC _C00_SimpleValueIntoField  179902,@Now,@TableRowID,@WhoCreated 
			
			UPDATE @TableRows 
			SET Done = 1 
			Where TableRowID = @TableRowID 
		END			  
		
	
	END
	
	/*JEL Stop Claim Payments*/ 
	/*Stop all payments on this matter*/ 
	IF @EventTypeID  = 158743
	BEGIN 
	
		INSERT INTO @TableRows (TableRowID,Done)
		SELECT tr.TableRowID, 0 
		FROM TableRows tr WITH (NOLOCK)
		Where tr.DetailFieldID = 154485 
		and tr.MatterID = @MatterID 

		WHILE EXISTS (SELECT * FROM @TableRows t 
					  WHere t.Done = 0) 
		BEGIN 
		
			SELECT top 1 @TableRowID = t.TableRowID 
			FROM @TableRows t 
			WHERE Done = 0 
			Order by Done desc 
			
			SELECT @Now = CAST(@Now as DATE)  
			
			EXEC _C00_SimpleValueIntoField  159289,@Now,@TableRowID,@WhoCreated 
			
			UPDATE @TableRows 
			SET Done = 1 
			Where TableRowID = @TableRowID 
		END			  
		
	END
		
	/*OOP Add Existing Condition*/
	/*JEL 2018-01-14*/
	IF @EventTypeID = 156719 
	BEGIN
		
		IF EXISTS (SELECT * FROM Lead l WITH (NOLOCK) 
				   WHERE l.LeadTypeID = 1490)
		BEGIN 
		
			SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
		
		END
		ELSE 
		BEGIN
		
			SELECT @PolicyLeadID = @LeadID
		
		END
		
		EXEC TableRows_Insert @TableRowID OUTPUT,@ClientID,@PolicyLeadID,NULL,177428,16154,0,0,@CustomerID,NULL,@WhoCreated,NULL,@LeadEventID 

		EXEC dbo._C00_CompleteTableRow   @TableRowID,@MatterID,0,0 
		
		/*Pick up values from customer level fields, Create a table row and store the 
		existing conditions against the correct lead.*/ 
		
		/*179752 Condition 	*/
		SELECT @ValueINT = dbo.fnGetSimpleDvAsInt(179752,@CustomerID) 
		EXEC _C00_SimpleValueIntoField  177426,@ValueINT,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179752,'',@CustomerID,@WhoCreated
		
		/*179753	Condition Added	*/

		SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(179753,@CustomerID) 
		EXEC _C00_SimpleValueIntoField 177427,@ValueDate,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179753,'',@CustomerID,@WhoCreated

		/*179756	Evidence provided	*/
		
		SELECT @DetailValue = dbo.fnGetSimpleDv(179756,@CustomerID) 
		EXEC _C00_SimpleValueIntoField 177521,@DetailValue,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179756,'',@CustomerID,@WhoCreated
				
		/*179887	Evidence required - GPR 2018-01-23 PM127 Underwriting Information	*/
		SELECT @EvidenceRequired = dbo.fnGetSimpleDvAsInt(179887,@CustomerID) 
		EXEC _C00_SimpleValueIntoField 179886,@EvidenceRequired,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179887,'',@CustomerID,@WhoCreated	
		
		/*179757	Active or inactive	*/

		SELECT @ValueINT = dbo.fnGetSimpleDvAsInt(179757,@CustomerID) 
		EXEC _C00_SimpleValueIntoField 177522,@ValueINT,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179757,'',@CustomerID,@WhoCreated
		
		/*179758	Date to be reviewed	*/
		
		SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(179758,@CustomerID) 
		EXEC _C00_SimpleValueIntoField 177901,@ValueDate,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179758,'',@CustomerID,@WhoCreated
		
		/*179759	Body Part	*/
		
		SELECT @ValueINT = dbo.fnGetSimpleDvAsInt(179759,@CustomerID) 
		EXEC _C00_SimpleValueIntoField 179721,@ValueINT,@TableRowID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 179759,'',@CustomerID,@WhoCreated
		
		/*179754	Condition Added by ClientPersonnelID*/
		
		EXEC _C00_SimpleValueIntoField 177515,@WhoCreated,@TableRowID,@WhoCreated	
		
		/*179755	Condition Added by UserName	*/
		
		SELECT @DetailValue = cp.UserName FROM ClientPersonnel cp WITH (NOLOCK) 
		where cp.ClientPersonnelID = @WhoCreated
		EXEC _C00_SimpleValueIntoField 177516,@DetailValue,@TableRowID,@WhoCreated

	END
	
	/*Regenerate Document*/
	IF @EventTypeID IN ( 158502 )
	BEGIN 

		/*Check which leadType we are doing this for*/ 
		IF @EventTypeID = 158502
		BEGIN
			/*Policy admin field*/ 
			SELECT @DocumentTypeID = dbo.fnGetSimpleDvAsInt(179412,@MatterID) 

		END 
		ELSE 
		BEGIN
			/*Claims Field*/ 
			SELECT @DocumentTypeID = dbo.fnGetSimpleDvAsInt('',@MatterID) 
			
		END
			
			IF ISNULL(@DocumentTypeID,'') <> '' 
			BEGIN
			
			/*Drop document directly into doc queue for printing*/
			SELECT @DocumentTitle = dt.DocumentTypeName FROM DocumentType dt WITH (NOLOCK) 
			where dt.DocumentTypeID = @DocumentTypeID 

			INSERT INTO DocumentQueue (ClientID,CustomerID,LeadID,CaseID,DocumentTypeID,WhoCreated,WhenStored,RequiresApproval,ParsedDocumentTitle)
			VALUES (@ClientID,@CustomerID,@LeadID,@CaseID,@DocumentTypeID,@WhoCreated,@WhenCreated,0,@DocumentTitle )

			SELECT @DocumentQueueID = SCOPE_IDENTITY() 

			INSERT INTO LeadEvent (ClientID,LeadID,WhenCreated,WhoCreated,Cost,Comments,EventTypeID,WhenFollowedUp,CaseID,DocumentQueueID,EventDeleted,BaseCost,ChargeOutRate,UnitsOfEffort,CostEnteredManually)
			VALUES (@ClientID,@LeadID,@WhenCreated,@WhoCreated,0.00,@DocumentTitle,158503,@WhenCreated,@CaseID, @DocumentQueueID,0,0.00,0.00,0,0)

			SELECT @NewLeadEventID = SCOPE_IDENTITY() 

			UPDATE DocumentQueue 
			SET BasedUponLeadEventID = @NewLeadEventID 
			WHere DocumentQueueID = @DocumentQueueID 
			
			/*Delete Field for Reuse*/
			EXEC _C00_SimpleValueIntoField 179412,'',@MatterID,@WhoCreated
		END
	END

	IF @EventTypeID IN (121863) /*Pay VET*/
	BEGIN
		SELECT  @AccountName	= rldv.DetailValue,
				@AccNumber		= rldv1.DetailValue,
				@AccSortCode	= rldv2.DetailValue
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON l.LeadID = ltr.ToLeadID 
		INNER JOIN Lead l1 WITH (NOLOCK) ON l1.LeadID = ltr.FromLeadID 
		LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = L1.LeadID  AND ldv.DetailFieldID = 146215 /*Current Vet*/
		LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = ldv.ValueInt  AND rldv.DetailFieldID = 175587 /*Account Name*/
		LEFT JOIN ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.ResourceListID = ldv.ValueInt  AND rldv1.DetailFieldID = 175590 /*Account Number*/
		LEFT JOIN ResourceListDetailValues rldv2 WITH (NOLOCK) ON rldv2.ResourceListID = ldv.ValueInt  AND rldv2.DetailFieldID = 175589 /*Sort Code*/
		WHERE L.ClientID = @ClientID  AND l.LeadID = @LeadID

		EXEC _C00_SimpleValueIntoField 170264,@AccountName	,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 170265,@AccNumber	,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 170266,@AccSortCode	,@MatterID,@WhoCreated
	END
		
	IF @EventTypeID IN (123462) /*Pay Referal VET*/
	BEGIN
		SELECT  @AccountName	= rldv.DetailValue,
				@AccNumber		= rldv1.DetailValue,
				@AccSortCode	= rldv2.DetailValue
		FROM Matter m WITH (NOLOCK) 
		LEFT JOIN MatterDetailValues ldv WITH (NOLOCK) ON ldv.MatterID = m.MatterID  AND ldv.DetailFieldID =  146236 /*Ref Vet*/
		LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = ldv.ValueInt  AND rldv.DetailFieldID = 175587 /*Account Name*/
		LEFT JOIN ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.ResourceListID = ldv.ValueInt  AND rldv1.DetailFieldID = 175590 /*Account Number*/
		LEFT JOIN ResourceListDetailValues rldv2 WITH (NOLOCK) ON rldv2.ResourceListID = ldv.ValueInt  AND rldv2.DetailFieldID = 175589 /*Sort Code*/
		WHERE m.ClientID = @ClientID  AND m.MatterID = @MatterID
		
		EXEC _C00_SimpleValueIntoField 170264,@AccountName	,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 170265,@AccNumber	,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 170266,@AccSortCode	,@MatterID,@WhoCreated
	END
		
	IF @EventTypeID IN (157254) /*Pay Out Of Hours VET*/
	BEGIN
		SELECT  @AccountName	= rldv.DetailValue,
				@AccNumber		= rldv1.DetailValue,
				@AccSortCode	= rldv2.DetailValue
		FROM Matter m WITH (NOLOCK) 
		LEFT JOIN MatterDetailValues ldv WITH (NOLOCK) ON ldv.MatterID = m.MatterID  AND ldv.DetailFieldID = 176944 /*Out of Hours Vet*/
		LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = ldv.ValueInt  AND rldv.DetailFieldID = 175587 /*Account Name*/
		LEFT JOIN ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv1.ResourceListID = ldv.ValueInt  AND rldv1.DetailFieldID = 175590 /*Account Number*/
		LEFT JOIN ResourceListDetailValues rldv2 WITH (NOLOCK) ON rldv2.ResourceListID = ldv.ValueInt  AND rldv2.DetailFieldID = 175589 /*Sort Code*/
		WHERE m.ClientID = @ClientID  AND m.MatterID = @MatterID

		EXEC _C00_SimpleValueIntoField 170264,@AccountName	,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 170265,@AccNumber	,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField 170266,@AccSortCode	,@MatterID,@WhoCreated
	END

	/*CPS 2017-09-20 for SA*/
	IF @EventTypeID IN (123460,122701) /*(1) Policyholder*/ /*(5) Split claim payment*/
	BEGIN
	   /*Customer under sanction screening*/
	   SELECT  @CustomerSanction = COUNT (cdv.DetailValue)
	   FROM  CustomerDetailValues cdv WITH (NOLOCK) 
	   WHERE cdv.ClientID  = @ClientID AND cdv.DetailFieldID = 178142 AND cdv.ValueDate is not NULL
	   AND cdv.CustomerID = @CustomerID 

	   IF @CustomerSanction >= 1
	   BEGIN
		  SELECT @ErrorMessage = '<font color="red"></br></br>Customer Under sanction screening - payment to customer not permitted</br></font><font color="#edf4fa">'
		  RAISERROR( @ErrorMessage, 16, 1 )
		  RETURN -1
	   END
	END
	
	IF @EventTypeID in(150135) /*DD Payer*/
	BEGIN
		/*
		Set DocumentFields.
		CPS 2017-07-24 moved here for ticket #44192
		*/
		SELECT @EventComments = ''
		
		EXEC @RowCount = _C600_SetDocumentFields @MatterID = @MatterID, @RunAsUserID = @WhoCreated, @ProcessSection = 'NewBusiness'
		
		SELECT @EventComments += ISNULL(CONVERT(VARCHAR,@RowCount),'NULL') + ' document fields updated.' + CHAR(13)+CHAR(10)
		
		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1 /*Comments are updated in the SetDocumentFields proc*/
	END
	
	IF @EventTypeID in(156862, 	155263) /*Premium change following MTA - inform customer*/
	BEGIN
		/*
		CPS 2017-07-12
		Update document fields
		*/
		SELECT @EventComments = 'Document Fields Updated.'
		
		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1 
		
		IF @EventTypeID = 156862 /*Premium change following MTA - inform customer*/
		BEGIN 

			EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'MTA'	

			/*2020-07-27 ALM AAG-1046*/
			SELECT TOP 1 @LatestEvent = EventTypeID 
			FROM LeadEvent WITH (NOLOCK) 
			WHERE CaseID = @CaseID
			AND EventTypeID IN (155246, 155247, 158999)
			ORDER BY WhenCreated DESC 

			IF @LatestEvent = 155246
			BEGIN
				SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

				IF @OneVisionID IS NOT NULL 
				BEGIN 
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Customer', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
				END
			END 

			IF @LatestEvent = 155247
			BEGIN 
				SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

				IF @OneVisionID IS NOT NULL 
				BEGIN 
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Pets', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
				END
			END 

			IF @LatestEvent = 158999
			BEGIN 
				SELECT @PolicyOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

				IF @PolicyOneVisionID IS NOT NULL 
				BEGIN 
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'PolicyTerm', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID
				END

				SELECT @WellnessOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness'

				IF @WellnessOneVisionID IS NOT NULL 
				BEGIN 
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID
				END

				SELECT @ExamFeeOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'

				IF @ExamFeeOneVisionID IS NOT NULL 
				BEGIN 
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID
				END
			END 
			
		END 
	END
	
	IF @EventTypeID in(155949,155950,158968) /*DD Payer Renewed*/ /*CP Payer Renewed*/ /*PF Payer Renewed*/
	BEGIN
		/*
		CPS 2017-06-15
		Apply appropriate exclusions if the pet is too old to be eligible for death from illness cover
		*/
		SELECT @EventComments = ''
		
		INSERT @SectionsToExclude ( ResourceListID, Done )
		SELECT tdvSection.ResourceListID, 0
		FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues tdvSection WITH (NOLOCK) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 145689 /*Policy Scheme*/
		INNER JOIN TableDetailValues tdvMaxAge WITH (NOLOCK) on tdvMaxAge.TableRowID = tr.TableRowID AND tdvMaxAge.DetailFieldID = 177450 /*Max Pet Age*/
		WHERE tr.DetailFieldID = 145692 /*Policy Limits*/
		AND tr.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter(@MatterID)
		AND tdvMaxAge.ValueInt >= dbo.fnAgeAtDate(dbo.fnGetSimpleDvAsDate(144274,@LeadID) /*Pet Date of Birth*/,@WhenCreated)
		
		WHILE EXISTS ( SELECT * 
		               FROM @SectionsToExclude ste
		               WHERE ste.Done = 0 )
		BEGIN
			SELECT TOP 1 @ResourceListID = ste.ResourceListID, @DetailValue = dbo.fnGetSimpleDv(146200,ste.ResourceListID) /*Product*/
			FROM @SectionsToExclude ste
			WHERE ste.Done = 0
			
			EXEC _C600_CreatePolicySectionExclusionRow @MatterID, @ResourceListID, 74575 /*Pet Age*/, @LeadEventID, @WhoCreated
			
			UPDATE ste
			SET Done = 1
			FROM @SectionsToExclude ste 
			WHERE ste.ResourceListID = @ResourceListID
			
			SELECT @EventComments += @DetailValue + ' excluded on grounds of pet age.' + CHAR(13)+CHAR(10)
			SELECT @ResourceListID = NULL, @DetailValue = NULL
		END
		
		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1

		/*2020-08-03 ALM AAG-972*/
		SELECT @PolicyOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

		IF @PolicyOneVisionID IS NOT NULL 
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Renewal', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID
		END

		SELECT @WellnessOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness'

		IF @WellnessOneVisionID IS NOT NULL 
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID
		END

		SELECT @ExamFeeOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'

		IF @ExamFeeOneVisionID IS NOT NULL 
		BEGIN 
			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID
		END

	END

	/*Catagorise Claim*/ 
	-- DCM removed shared event '138258 Enter Treatment Dates' at customer request to rationalise number of events 
	-- required to enter claim details. Shifted the SAE to here.
	IF @EventTypeID IN (121472)
	BEGIN
						
		-- Set Fraud Indicators
		/*SA - Added @WhoCreated parameter following proc update*/
		EXEC [_C00_1272_Claim_FraudIndicators_AutoSet] @MatterID,@WhoCreated
		
	END

	/* Handle date of death 
		-- call SAS because share lead types don't for a shared detail field not on its home lead type 
	*/
	IF @EventTypeID IN (150015, -- Enter Claim Details
						150003,	-- Verify policy details
						150005) -- Claim Form received
	BEGIN
	
        EXEC _C600_SAS @AqAutomation, @CustomerID, @LeadID
		
	END
	
	--/* ============================================  SIG SPECIALS  ================================================== */
	-- keep this block at the top of the SAE other SAE processing uses the followup times set here
	--/* Configurable followup date/time events
	--	1. Set followup date time according to RL configuration
	--*/
	IF EXISTS ( SELECT * FROM ResourceListDetailValues clrl WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues etrl WITH (NOLOCK) ON clrl.ResourceListID=etrl.ResourceListID
					AND etrl.DetailFieldID=170095 AND etrl.ValueInt=@EventTypeID
				WHERE clrl.DetailFieldID=170094 AND clrl.ValueInt=@ClientID )
	BEGIN
		EXEC _C600_SetFollowupDateTime @EventTypeID, @LeadEventID, @MatterID
	END
	
	/* Prevent OOP events being added on claims admin only policies (Zen#33301) */
	IF @EventTypeID IN (
						155411,  --Incoming Correspondence - Email
						155410,  --Incoming Correspondence - Letter
						155248,  --PH Cancel Policy
						155398,  --PH EM Resend document
						155415,  --PH Send Correspondence OOP
						155303,  --SIG Reinstate policy
						155401   --PH Send Letter Requesting Customer Contact						
						)
	BEGIN
	
		IF NOT EXISTS ( SELECT * FROM CustomerDetailValues aff WITH (NOLOCK)
						INNER JOIN  ResourceListDetailValues IsPA WITH (NOLOCK) ON aff.ValueInt=IsPA.ResourceListID AND IsPA.DetailFieldID=175277
						WHERE aff.CustomerID=@CustomerID AND aff.DetailFieldID=170144
						AND IsPA.ValueInt=5144 )
		BEGIN
		
			
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot use this event on a claims admin only policy.</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		
		END	
		
	END	
	
	/* ============================================  COLLECTIONS  ================================================== */

	/* New direct debit details */
	IF @EventTypeID = 155272
	BEGIN 
		/*JEL Added Account Authority*/ 
		/* new account details entered on event screen */
		SELECT 
			@AccountName = dbo.fnGetSimpleDV(177446,@MatterID), /* New Account Name */
			@AccSortCode = dbo.fnGetSimpleDV(175363,@MatterID), /* New Sort Code */
			@AccNumber = dbo.fnGetSimpleDV(175382,@MatterID), /* New Account Number */
			@ValueINT = dbo.fnGetSimpleDV(179922,@MatterID) /*Additional Authority required on Account*/ 
	
		SELECT TOP 1 @PolicyLeadID = ltr.FromLeadID, @PolicyMatterID = ltr.FromMatterID
		FROM LeadTypeRelationship ltr WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = ToLeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
		WHERE ltr.FromLeadTypeID = 1492
		AND m.MatterID = @MatterID

		/* write new data to policy matter details - Bank Details*/
		EXEC dbo._C00_SimpleValueIntoField 170104, @AccountName, @PolicyMatterID /* Account Name */
		EXEC dbo._C00_SimpleValueIntoField 170105, @AccSortCode, @PolicyMatterID /* Account Sort Code */
		EXEC dbo._C00_SimpleValueIntoField 170106, @AccNumber, @PolicyMatterID /* Account Number */
		EXEC dbo._C00_SimpleValueIntoField 179904, @ValueINT, @PolicyMatterID /* Account Number */
		
		/* Handle more than one policy */
		IF 
			(
				SELECT COUNT(*) 
				FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
				WHERE l.LeadID = @PolicyLeadID
		) > 1
		BEGIN

			SELECT @MaxCounter = COUNT(*) 
			FROM Lead l WITH (NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
			WHERE l.LeadID = @PolicyLeadID

			WHILE @Counter <= @MaxCounter
			BEGIN

				SELECT TOP 1 @MultiMatterID = m.MatterID
				FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
				WHERE l.LeadID = @PolicyLeadID
				AND m.MatterID > @PolicyMatterID
				ORDER BY m.MatterID

				EXEC dbo._C00_SimpleValueIntoField 170104, @AccountName, @MultiMatterID /* Account Name */
				EXEC dbo._C00_SimpleValueIntoField 170105, @AccSortCode, @MultiMatterID /* Account Sort Code */
				EXEC dbo._C00_SimpleValueIntoField 170106, @AccNumber, @MultiMatterID /* Account Number */

				SELECT 
					@PolicyMatterID = @MultiMatterID, 
					@Counter = @Counter + 1

			END

		END

	END

	/* Update current payment details */
	IF @EventTypeID IN (155198, -- Collections process not required
						150180, -- DD Monthly
						150181, -- DD Annually
						150182, -- CC Monthly
						150183) -- CC Annually
	BEGIN
			
		EXEC dbo._C00_CreateDetailFields '170188,170189,170190', @LeadID
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 3, 1

	
		--Based on the collections matter ID, get the 'fromMATTERID' from the LTR table (the fromMatterID is the policy matterID)
		SELECT @PolicyMatterIDFromCollectionsMatter = FromMatterID from LeadTypeRelationship WITH (NOLOCK) WHERE ToMatterID = @matterid 


		--assign the @returnvalue variable = matterdetailvalue of field 170050 where matterid = @PolicyMatterIDFromCollectionsMatter
		SELECT @ReturnValue = DetailValue FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID = @PolicyMatterIDFromCollectionsMatter AND DetailFieldID = 170050

		--stamp @returnvalue into field 175606
		EXEC dbo._C00_SimpleValueIntoField 175606, @ReturnValue, @MatterID
			
	END
	
	/* SIG Cancellation BACS */
	IF @EventTypeID IN (150129)
	BEGIN
 
		-- add details to the mandate history file
		SELECT TOP 1 @PayRef=dbo.fn_C600_GetMandateReference(an.MatterID), 
				@AccountName=an.DetailValue, @AccSortCode=acs.DetailValue, 
				@AccNumber=anum.DetailValue, @ReasonCode=rc.DetailValue, @EffectiveDate=dbo.fn_GetDate_Local(),
				@LinkedTRID=lr.TableRowID
		FROM MatterDetailValues an WITH (NOLOCK) 
		INNER JOIN MatterDetailValues acs WITH (NOLOCK) ON acs.MatterID=an.MatterID AND acs.DetailFieldID=170189
		INNER JOIN MatterDetailValues anum WITH (NOLOCK) ON anum.MatterID=an.MatterID AND anum.DetailFieldID=170190
		INNER JOIN MatterDetailValues rc WITH (NOLOCK) ON rc.MatterID=an.MatterID AND rc.DetailFieldID=170169
		INNER JOIN TableDetailValues lr WITH (NOLOCK) ON lr.MatterID=an.MatterID AND lr.DetailFieldID=170162 AND lr.DetailValue='0N'
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=an.MatterID
		INNER JOIN CustomerDetailValues aff WITH (NOLOCK) ON aff.CustomerID=m.CustomerID AND aff.DetailFieldID=170144
		INNER JOIN ResourceListDetailValues affsc WITH (NOLOCK) ON aff.ValueInt=affsc.ResourceListID AND affsc.DetailFieldID=170127
		INNER JOIN LookupListItems affscli WITH (NOLOCK) ON affscli.LookupListItemID=affsc.ValueInt
		WHERE an.MatterID=@MatterID AND an.DetailFieldID=170188 
		ORDER BY lr.TableRowID DESC
		
		EXEC dbo._C600_BACS_AddTableRow @MatterID, 170183, @EffectiveDate, 0.00, NULL, @ReasonCode, @PayRef, @AccountName, @AccSortCode, @AccNumber, @LinkedTRID, 72021
		
		-- clear temporary reason code field
		EXEC _C00_DeleteFieldForReUse @LeadEventID, 170169, @WhoCreated 
		
		-- Apply the in process Mandate Failed event
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
		SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, @CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 150092, @AqAutomation, -1, 5, 0, 5
		FROM Cases c WITH (NOLOCK)
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
		WHERE c.CaseID = @CaseID
		
	END	

	/* Mandate failed */
	IF @EventTypeID IN (150092, -- Mandate Failure
						156736) -- Mandate suspended
	BEGIN
		
		-- Set Mandate status flag (this matter)
		EXEC dbo._C00_SimpleValueIntoField 170101, 69919, @MatterID, @WhoCreated -- failed	
	
		-- Set mandate chaser loop cont
		EXEC dbo._C00_SimpleValueIntoField 175598, 1, @MatterID, @AqAutomation	

		-- Clear lodgement date field
		EXEC _C00_DeleteFieldForReUse @LeadEventID, 170182, @WhoCreated 
		
		EXEC _C600_Collections_RefreshDocumentationFields @MatterID

		/*Pick up the policy matter for this account*/ 
		SELECT @PolMatterID = p.ObjectID, @PurchasedProductID = p.PurchasedProductID, @ProductStartDate = p.ValidFrom FROM PurchasedProduct p WITH (NOLOCK) 
		INNER JOIN Account a with (NOLOCK) on a.AccountID = p.AccountID 
		WHERE a.ObjectID = @MatterID 
		AND p.ValidFrom < @Now
		AND NOT EXISTS (SELECT * FROM PurchasedProduct pp WITH (NOLOCK) 
						WHERE pp.PurchasedProductID > p.PurchasedProductID 
						AND pp.ObjectID = p.ObjectID
						AND pp.AccountID = p.AccountID
						AND pp.ValidFrom < @Now) 

		/*Find the last paid date, and the Cover to for that payment. Add one day to this to find the first day not on cover*/ 
		SELECT @ValueDate = ppps.CoverTo FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		WHERE ppps.PurchasedProductID = @PurchasedProductID 
		AND ppps.PaymentStatusID = 6 
		AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule pppsSub WITH (NOLOCK) 
						WHERE pppsSub.PurchasedProductID = @PurchasedProductID 
						AND pppsSub.PaymentDate > ppps.PaymentDate 
						AND pppsSub.PaymentStatusID = 6) 

		SELECT @ValueDate = ISNULL(DATEADD(DAY,1,@ValueDate),@ProductStartDate)

		EXEC _C00_SimpleValueIntoField 314261, @ValueDate, @PolMatterID, @AqAutomation
		
		/*Looking at the state variation table, find how many days we need to give the customer notice prior to cancellation.*/ 
		--SELECT @NotificationDate = DATEADD(DAY, cr.CustomerCancelNonPay + 5, ppps.PaymentDate) FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		--INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = ppps.CustomerID and cu.Test = 0
		--INNER JOIN USCancellationRule cr WITH (NOLOCK) on cu.County = cr.State and cr.IsRenewal = 0
		--WHERE ppps.PurchasedProductID = @PurchasedProductID 
		--AND ppps.PaymentStatusID = 4
		--AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule pppsSub WITH (NOLOCK) 
		--				WHERE pppsSub.PurchasedProductID = @PurchasedProductID 
		--				AND pppsSub.PaymentDate > ppps.PaymentDate 
		--				AND pppsSub.PaymentStatusID = 4) 

		/*2020-11-03 ALM*/
		SELECT @NotificationDate = [dbo].[fn_C600_GetDateAddXDaysCancel](@PolMatterID)
		
		EXEC _C00_SimpleValueIntoField 314260, @NotificationDate, @PolMatterID, @AqAutomation

	END	

	/*CMJ 2021-04-12 FURKIN-180 Update Payment Failure Process*/
	/* Payment Failed - Document Fields*/ 
	IF @EventTypeID IN (150092, -- Mandate Failure (account issues)
						150120, -- payment Failure EFT
						158986, -- payment failure CC
						150098) -- payment retry failed (Added for SDAAG-180 GPR/ALM/CR)
    BEGIN

		SELECT @AmountDue = ppps.PaymentGross, @PaymentDate = ppps.ActualCollectionDate
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		INNER JOIN lead l WITH (NOLOCK) on ppps.CustomerID = l.CustomerID
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.LeadID = l.LeadID AND mdv.DetailFieldID = 180262
		WHERE ppps.PaymentStatusID = 4--failed
		AND mdv.MatterID = @matterID
		AND pp.ObjectID = mdv.ValueInt
					AND NOT EXISTS (
					SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
					WHERE ppps2.PurchasedProductID = ppps.PurchasedProductID
						AND ppps2.PaymentStatusID = 4
						AND ppps2.PurchasedProductPaymentScheduleID > ppps.PurchasedProductPaymentScheduleID)

	   	-- Set Failed Payment Amount Due 315906
		EXEC dbo._C00_SimpleValueIntoField 315906, @AmountDue, @MatterID, @WhoCreated	
	
		-- Set Date of Failed Payment 315908
		EXEC dbo._C00_SimpleValueIntoField 315908, @PaymentDate, @MatterID, @WhoCreated
	END /*CMJ FURKIN-180*/


	/*GPR 2020-07-22 for AAG-1034*/
	IF @EventTypeID IN (159006) /*Notified by Bank - Customer Deceased*/
	BEGIN

	/*Deactivate all premium accounts for this Customer*/
		
		UPDATE a
		SET Active = 0
		FROM Account a WITH (NOLOCK)
		WHERE a.CustomerID = @CustomerID
		AND a.AccountUseID = 1 /*Premium Account*/

	/*Set RenewalType to Manual*/
		
		SELECT @PAMatterIDFromCollectionsMatterID = dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@MatterID)
	
		EXEC dbo._C00_SimpleValueIntoField 177110, 74338, @PAMatterIDFromCollectionsMatterID, @AqAutomation
		
	/*Apply notification to PH Address*/
		
		SELECT @LeadEventID = LatestInProcessLeadEventID
		FROM Cases cases WITH (NOLOCK)
		INNER JOIN Matter matter WITH (NOLOCK) ON matter.CaseID = cases.CaseID
		WHERE matter.MatterID = @PAMatterIDFromCollectionsMatterID

		EXEC _C00_ApplyLeadEventByAutomatedEventQueue @LeadEventID, 159007, @AqAutomation, 1

	/*Do usual Customer Deceased post-event processing*/
		
		-- Set Mandate status flag (this matter)
		EXEC dbo._C00_SimpleValueIntoField 170101, 69919, @MatterID, @WhoCreated -- failed	

		-- Set mandate chaser loop cont
		EXEC dbo._C00_SimpleValueIntoField 175598, 1, @MatterID, @AqAutomation	

		-- Clear lodgement date field
		EXEC _C00_DeleteFieldForReUse @LeadEventID, 170182, @WhoCreated 
		
		-- Refresh document fields
		EXEC _C600_Collections_RefreshDocumentationFields @MatterID

	END
	
	/*GPR 2018-04-27 Instruction re-instated*/
	IF @EventTypeID IN (158809) -- Instruction re-instated
	BEGIN
		-- Set Mandate status flag (this matter)
		EXEC dbo._C00_SimpleValueIntoField 170101, 69918, @MatterID, @WhoCreated -- live
		
		EXEC dbo._C600_DeleteANote @CaseID, 865, @LeadEventID -- Remove the DD Mandate Failed Note

		/*CR 2019-07-01 Added the details to populated the required detailfields for the next payment amount and next payment date*/
		SELECT @AccountID=ISNULL(dbo.fnGetDvAsInt(176973,@CaseID),0)
		SELECT @CollectionsMatterID = dbo.fn_C600_GetCollectionsMatterID(@MatterID) 

		SELECT 
			@NextPaymentDate=CONVERT(VARCHAR(10),ppps.ActualCollectionDate,120), 
			@NextPaymentAmount=SUM(ppps.PaymentGross)
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
			INNER JOIN dbo.PurchasedProduct p WITH (NOLOCK) on p.CustomerID= ppps.CustomerID
			WHERE (ppps.PaymentStatusID IN (1,5,7) OR PaymentStatusID IS NULL)
				AND ppps.CustomerLedgerID IS NULL
				AND ppps.AccountID=@AccountID
				AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
								 WHERE (ppps2.PaymentStatusID IN (1,5,7) OR PaymentStatusID IS NULL) 
									AND ppps2.CustomerLedgerID IS NULL  
									AND ppps2.ActualCollectionDate < ppps.ActualCollectionDate
									AND ppps2.AccountID=ppps.AccountID )
			GROUP BY ppps.ActualCollectionDate

		EXEC _C00_SimpleValueIntoField 177009, @NextPaymentAmount, @CollectionsMatterID, @whoCreated	/* Payment Amount on the collection leadtype*/
		EXEC _C00_SimpleValueIntoField 177010, @NextPaymentDate, @CollectionsMatterID, @WhoCreated		/*Payment Date on the collection leadtype*/

		/*Close off the thread for the mandate failed chaser*/ 
		UPDATE LeadEvent 
		SET WhenFollowedUp = dbo.fn_GetDate_Local(), NextEventID = @LeadEventID
		FROM LeadEvent  
		Where CaseID = @CaseID 
		AND EventTypeID IN (150095,150105) 
		AND WhenFollowedUp IS NULL 
			
	END
	

	/* Resend mandate chaser */
	IF @EventTypeID IN (150100)
	BEGIN
		
		EXEC _C600_Collections_RefreshDocumentationFields @MatterID
		
	END

	/* Collections now on another account */
	IF @EventTypeID IN (156858)
	BEGIN
	
		-- clear overdue note if all accounts have an overdue amount of zero
		IF 0 = ( SELECT SUM(ValueMoney) FROM MatterDetailValues ao WITH (NOLOCK) 
				 INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=ao.MatterID
				 WHERE DetailFieldID=177324
					AND m.LeadID=@LeadID )
		BEGIN
			
			-- find a live collections case
			SELECT TOP 1 @NewCaseID=m.CaseID FROM MatterDetailValues aa WITH (NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON aa.MatterID=m.MatterID 
			WHERE aa.DetailFieldID=177325 
				AND aa.LeadID=@LeadID 
				AND aa.ValueInt<>5144
			EXEC _C600_DeleteANote @NewCaseID, 923	
			EXEC _C600_DeleteANote @CaseID, 923	
		END
		
	END

	/* Payment collection failed */
	IF @EventTypeID IN (150099, -- Failed payment - refer to payer
						156728) -- Notification of a further collection failure
	BEGIN

		-- set amount overdue & whether now collection on another account
		SELECT @PaymentID=ISNULL(dbo.fnGetDvAsInt(177370,@CaseID),0),
			   @AccountID=ISNULL(dbo.fnGetDvAsInt(176973,@CaseID),0)
	
		-- add amount due to appropriate current live collection account (i.e. all accounts that are now collecting any portion of the failed collection)
		-- Don't add the portion if the curent live collection account is different from the account on which the failure was reported and that 
		-- account is still collecting OK (i.e. has a zero value in amount owed) because we assume that account will have the money available
		
		-- find all current live accounts
		DECLARE @NewEntries TABLE (AccountID INT, Amount MONEY, NewMatterID INT, NewCaseID INT, NewAmount MONEY, Done INT)

		INSERT INTO @NewEntries (AccountID, Amount, NewMatterID, NewCaseID, NewAmount, Done)
		SELECT pp.AccountID,SUM(ppps.PaymentGross),m.MatterID,m.CaseID,ISNULL(na.ValueMoney,0),0 
		FROM Payment p WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID AND ppps.PaymentStatusID=4
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON ppps.PurchasedProductID=pp.PurchasedProductID
		INNER JOIN MatterDetailValues accid WITH (NOLOCK) ON pp.AccountID=accid.ValueInt AND accid.DetailFieldID=176973
		LEFT JOIN MatterDetailValues na WITH (NOLOCK) ON accid.MatterID=na.MatterID AND na.DetailFieldID=177324
		INNER JOIN Matter m WITH (NOLOCK) ON accid.MatterID=m.MatterID
		WHERE p.PaymentID=@PaymentID
		GROUP BY pp.AccountID,m.MatterID,m.CaseID,ISNULL(na.ValueMoney,0)
		
		UPDATE ao
		SET DetailValue=ne.Amount+ne.NewAmount
		FROM MatterDetailValues ao
		INNER JOIN @NewEntries ne ON ao.MatterID=ne.NewMatterID
		WHERE ao.DetailFieldID=177324
			AND (ne.AccountID=@AccountID OR
				(ne.AccountID<>@AccountID AND ne.NewAmount>0))
				
		-- for all associated accounts that are in failure add 'further collection failure event'
		WHILE EXISTS ( SELECT * FROM @NewEntries WHERE AccountID<>@AccountID AND NewAmount>0 AND Done=0 )
		BEGIN
		
			SELECT TOP 1 @NewMatterID=NewMatterID FROM @NewEntries WHERE AccountID<>@AccountID AND NewAmount>0 AND Done=0
			
			EXEC _C600_AddAutomatedEvent @NewMatterID, 156728, -1
		
			UPDATE @NewEntries SET Done=1 WHERE NewMatterID=@NewMatterID
			
		END
		-- and those that are not in failure should immediately do a retry
		WHILE EXISTS ( SELECT * FROM @NewEntries WHERE AccountID<>@AccountID AND NewAmount=0 AND Done=0 )
		BEGIN
		
			SELECT TOP 1 @NewMatterID=NewMatterID FROM @NewEntries WHERE AccountID<>@AccountID AND NewAmount=0 AND Done=0
			
			EXEC _C600_AddAutomatedEvent @NewMatterID, 156871, -1
		
			UPDATE @NewEntries SET Done=1 WHERE NewMatterID=@NewMatterID
			
		END
		
		SELECT @ErrorMessage='Collection failed on collections Matter: ' + CAST(@MatterID AS VARCHAR) + '. Total owed is: £' + CAST(@ValueMoney AS VARCHAR(100))	
				
	END
	
	/* Retry payment collection */
	IF @EventTypeID IN (150120)
	BEGIN
	
		-- failed payment flag - off
		EXEC dbo._C00_SimpleValueIntoField 170093, 0, @MatterID, @AqAutomation		
		-- clear overdue payment fields
		EXEC dbo._C00_SimpleValueIntoField 177324, 0, @MatterID, @AqAutomation	
		IF 0 = ( SELECT SUM(ValueMoney) FROM MatterDetailValues ao WITH (NOLOCK) 
				 INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=ao.MatterID
				 WHERE DetailFieldID=177324
					AND m.LeadID=@LeadID )	
			EXEC _C600_DeleteANote @CaseID, 923	
		
	END
	
	/* Mandate suspended */
	IF @EventTypeID IN (156736)
	BEGIN
	
		-- flag failed
		EXEC dbo._C00_SimpleValueIntoField 170101, 69919, @MatterID, @AqAutomation			
	
		-- if the mandate has been suspended due to the death of the account holder kick off
		-- the dead customer process on policy admin
		IF dbo.fnGetDv(170107,@CaseID) LIKE '%deceased%' 
		BEGIN
		
			INSERT INTO @PolicyMatters
			SELECT ltr.FromMatterID,0 FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON ltr.FromMatterID=pstat.MatterID AND pstat.DetailFieldID=170038
			WHERE ltr.ToMatterID=@MatterID 
				AND ltr.FromLeadTypeID=1492
				AND pstat.ValueInt=43002 -- live
				
			WHILE EXISTS ( SELECT * FROM @PolicyMatters WHERE ID2=0 )
			BEGIN
			
				SELECT TOP 1 @PolMatterID=ID1 FROM @PolicyMatters WHERE ID2=0
				
				INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
				SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, c.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156689, @AqAutomation, -1, 5, 0, 5
				FROM Cases c WITH (NOLOCK)
				INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=c.LeadID
				INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
				INNER JOIN Matter m WITH (NOLOCK) ON c.CaseID=m.CaseID
				WHERE m.MatterID=@PolMatterID
				
				UPDATE @PolicyMatters SET ID2=1 WHERE ID1=@PolMatterID			
			
			END
		
		END	
		
	END	

	/* CP collection chase */
	IF @EventTypeID IN (155302)
	BEGIN
		-- Add CP Collections Row if relevant
		EXEC _C600_Collections_AddCPCollectionsRow @MatterID, 1
		
	END

	/* Mandate failed - Cancel cover and send notification */
	IF @EventTypeID IN (150106)
	BEGIN
	
		EXEC _C600_Collections_RefreshDocumentationFields @MatterID

		--as the matter ID is the collections matter, we need to get the PA matter ID 
		SELECT @PAMatterIDFromCollectionsMatterID = dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@matterID)
		
		EXEC _C00_SimpleValueIntoField 180262, @PAMatterIDFromCollectionsMatterID, @matterID, @AqAutomation    /*PL - Adding the Policy Admin Matter ID to the Collections Matter to retain link - #55342*/
				
				--get the purchased productID for the account - if it is multipet then we need to filter for the account relation to multiple purchased products
				SELECT @PurchasedProductID = p.PurchasedProductID FROM PurchasedProduct p WITH (NOLOCK)
				inner JOIN Lead l WITH (NOLOCK) on l.CustomerID = p.CustomerID
				INNER JOIN Account a WITH (NOLOCK) on a.ObjectID =  @matterid
				where l.LeadID = @leadid 
				and a.AccountID = p.AccountID

				--Get the amount paid to date from the purchased product 
				SELECT @AmountPaidToDate =  ISNULL(SUM(ppps.PaymentGross),0.00) 
				FROM MatterDetailValues mpp WITH (NOLOCK) 
				INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON mpp.ValueInt=ppps.PurchasedProductID
				WHERE mpp.MatterID= @PAMatterIDFromCollectionsMatterID 
				AND mpp.DetailFieldID=177074
				AND ppps.PaymentStatusID IN (2,6) -- processed, paid
				-- and a later 'failed' payment record does not exist for this orginal payment schedule entry
				AND NOT EXISTS (
					SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
					WHERE ppps2.PurchasedProductPaymentScheduleParentID=ppps.PurchasedProductPaymentScheduleParentID 
						AND ppps2.PaymentStatusID=4
						AND ppps2.PurchasedProductPaymentScheduleID>ppps.PurchasedProductPaymentScheduleID)
					
				--Get the inception date of the policy from the matterid 	
				SELECT @InceptionDate = dbo.fnGetSimpleDvAsDate(170035,@PAMatterIDFromCollectionsMatterID)
					
				INSERT INTO @AdjDate
				EXEC _C00_GetCoverToDateFromPaidAmount @PurchasedProductID, @AmountPaidToDate
				
				SELECT @AdjustmentDate = ISNULL(s.AdjDate,@InceptionDate)
				FROM @AdjDate s

		EXEC dbo._C00_SimpleValueIntoField 175603, @AdjustmentDate, @MatterID, @AqAutomation		 	
		
	END
	
	/*2018-06-07 GPR remove mandate failed note after instruction reinstated, and update the ClientStatusID C600 #451*/
	IF @EventTypeID = 158809 /*Instruction re-instated*/
	BEGIN
			EXEC dbo._C600_DeleteANote @CaseID,865 -- Remove DD Mandate Failed Note from this Case
			EXEC dbo._C600_DeleteANote @CaseID,866 -- Remove DD Cancelled Failed Note from this Case
			EXEC dbo.Cases__UpdateCaseStatus @CaseID, 4707 -- Update the ClientStatus to 'Active - Normal'
	END
		
	/* Set cancellation note text */
	IF @EventTypeID IN (150121, -- Payment Retry Limit - Cancel cover and send notifi
						155301) -- Cancel policy - CP collection chase
	BEGIN
	
		
		SELECT @LogEntry=CASE @EventTypeID
				WHEN 150121 THEN 'We have not been able to collect your outstanding premium after writing twice.'
				WHEN 155301 THEN 'We have not been able to collect your outstanding premium after writing twice.'
				END
		EXEC dbo._C00_SimpleValueIntoField 175476, @LogEntry, @MatterID, @AqAutomation
		
		EXEC _C600_Collections_RefreshDocumentationFields @MatterID		

				--as the matter ID is the collections matter, we need to get the PA matter ID 
		SELECT @PAMatterIDFromCollectionsMatterID = dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@matterID)
				
				--get the purchased productID for the account - if it is multipet then we need to filter for the account relation to multiple purchased products
				SELECT @PurchasedProductID = p.PurchasedProductID FROM PurchasedProduct p WITH (NOLOCK)
				inner JOIN Lead l WITH (NOLOCK) on l.CustomerID = p.CustomerID
				INNER JOIN Account a WITH (NOLOCK) on a.ObjectID =  @matterid
				where l.LeadID = @leadid 
				and a.AccountID = p.AccountID

				--Get the amount paid to date from the purchased product 
				SELECT @AmountPaidToDate =  ISNULL(SUM(ppps.PaymentGross),0.00) 
				FROM MatterDetailValues mpp WITH (NOLOCK) 
				INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON mpp.ValueInt=ppps.PurchasedProductID
				WHERE mpp.MatterID= @PAMatterIDFromCollectionsMatterID 
				AND mpp.DetailFieldID=177074
				AND ppps.PaymentStatusID IN (2,6) -- processed, paid
				-- and a later 'failed' payment record does not exist for this orginal payment schedule entry
				AND NOT EXISTS (
					SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
					WHERE ppps2.PurchasedProductPaymentScheduleParentID=ppps.PurchasedProductPaymentScheduleParentID 
						AND ppps2.PaymentStatusID=4
						AND ppps2.PurchasedProductPaymentScheduleID>ppps.PurchasedProductPaymentScheduleID)
						
				INSERT INTO @AdjDate
				EXEC _C00_GetCoverToDateFromPaidAmount @PurchasedProductID, @AmountPaidToDate
				
				SELECT @AdjustmentDate = ISNULL(s.AdjDate,dbo.fn_GetDate_Local())
				FROM @AdjDate s

		EXEC dbo._C00_SimpleValueIntoField 175603, @AdjustmentDate, @MatterID, @AqAutomation	
 			
	END	

	/* Mandate Cancelled */
	IF @EventTypeID IN (150112,	156830, 156831)
	BEGIN
	
		-- Cancel all policies using this collections matter (that are still live)
		INSERT INTO @PolicyMatters
		SELECT DISTINCT ltr.FromMatterID,0 FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON ltr.FromMatterID=pstat.MatterID AND pstat.DetailFieldID=170038 
			AND pstat.ValueInt IN (43002)
		WHERE ltr.ToMatterID=@MatterID AND ltr.FromLeadTypeID=1492
		
		WHILE EXISTS ( SELECT * FROM @PolicyMatters WHERE ID2=0 )
		BEGIN
		
			SELECT TOP 1 @PolMatterID=ID1 FROM @PolicyMatters WHERE ID2=0
			
			SELECT @NewLeadEventID = c.LatestNonNoteLeadEventID, @LeadID = m.LeadID, @CaseID = c.CaseID, @ValueInt = le.EventTypeID FROM Matter m WITH (NOLOCK) 
			INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID 
			INNER JOIN LeadEvent le WITH (NOLOCK) on le.LeadEventID = c.LatestNonNoteLeadEventID 
			WHERE m.MatterID = @PolMatterID
						
			EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueueFull @ClientID,@LeadID,@CaseID,@NewLeadEventID,@ValueInt, 155374,0,@Now,@WhoCreated,'Policy Cancelled by Collections Process'
						
			UPDATE @PolicyMatters SET ID2=1 WHERE ID1=@PolMatterID
				
		END
		
	END

	/* Suspend collections */
	IF @EventTypeID IN (155269, -- begin - Suspend collections
						156734) -- Suspend collection
	BEGIN
	
		EXEC dbo._C00_SimpleValueIntoField 175275, 5144, @MatterID, @WhoCreated

	END

	/* SIG Resume collections */ -- BEU update this functionality for new system
	IF @EventTypeID IN (155270,
						155358) -- Ready to retry collection
	BEGIN
	
		EXEC @IsOK=_C600_IsOverrideDateOK @CaseID
		IF @IsOK = 0
		BEGIN
		
			RAISERROR('<br /><br /><font color="red">You cannot use the override date if it is less than 10 days from today and you have not confirmed that you have the customer''s consent.</font><br/><br/><font color="#edf4fa">',16,1)
			RETURN
		
		END
						
		SELECT @AccountID = dbo.fnGetSimpleDVAsInt(176973, @MatterID)
		
		IF @AccountID > 0
		BEGIN

			/*2018-06-26 ACE Defect 717*/
			UPDATE a
			SET Active = 1
			FROM Account a 
			WHERE a.AccountID = @AccountID

			/*
				2018-07-11 ACE Defect 770
				2018-07-19 ACE D770 Not 10 working days but use the actual collection date function to its in line with the addount mandate creation
				Now uses the same logic as Defect 824
			*/
			SELECT @EarliestPaymentDate = dbo.fn_C00_CalculateActualCollectionDate(@AccountID, 1, dbo.fn_GetDate_Local(), 4, 0.00)

			UPDATE ppps 
			SET ActualCollectionDate = dbo.fnAddWorkingDays(@EarliestPaymentDate,3) , PaymentDate = @EarliestPaymentDate
			OUTPUT inserted.CustomerPaymentScheduleID INTO @UpdatedPPPS
			FROM PurchasedProductPaymentSchedule ppps 
			WHERE ppps.AccountID = @AccountID
			AND ActualCollectionDate < dbo.fn_C00_CalculateActualCollectionDate(ppps.AccountID, 1, dbo.fn_GetDate_Local(), 4, 0.00)
			AND ppps.PaymentStatusID = 1

			UPDATE cps
			SET PaymentDate = @EarliestPaymentDate
			FROM CustomerPaymentSchedule cps
			INNER JOIN @UpdatedPPPS u ON u.CPSID = cps.CustomerPaymentScheduleID

		END

		-- set next payment date BEU		
		EXEC _C600_SetNextPaymentDate @MatterID, 3
	
		EXEC dbo._C00_SimpleValueIntoField 175275, 5145, @MatterID, @WhoCreated
		EXEC _C600_DeleteANote @CaseID, 905
		
	END	

	/* Account details amended by BACS return */
	IF @EventTypeID IN (155430)
	BEGIN
		
		/*First pick up the current active account for this matter, Highlander rules here: there can only be one.*/ 
		SELECT @AccountID = a.AccountID, @AccNumber = a.AccountNumber, @AccountName = a.AccountHolderName, @AccSortCode = a.Sortcode 
		FROM Account a WITH (NOLOCK) 
		WHERE a.ObjectID = @MatterID
		AND a.Active = 1 
		
		-- update the account details in collections matter fields
		EXEC dbo._C00_SimpleValueIntoField 170188, @AccountName, @MatterID, @AqAutomation	
		EXEC dbo._C00_SimpleValueIntoField 170189, @AccSortCode, @MatterID, @AqAutomation	
		EXEC dbo._C00_SimpleValueIntoField 170190, @AccNumber, @MatterID, @AqAutomation	
		/*2018-06-26 ACE 707 add in the customer account reference*/
		EXEC dbo._C00_SimpleValueIntoField 175600, @AccNumber, @MatterID, @AqAutomation	
		EXEC dbo._C00_SimpleValueIntoField 176973, @AccountID, @MatterID, @AqAutomation
		
		/*Now make sure we get the products this account*/ 
		INSERT INTO @MandateMatters (MatterID, CaseID, Done) 
		SELECT m.MatterID, m.CaseID, 0 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.ObjectID = m.MatterID
		WHERE p.AccountID = @AccountID 
		
		/*Now loop round and make sure the details are populated in the policy admin matters*/ 
		WHILE EXISTS (SELECT * FROM @MandateMatters 
					  Where Done = 0) 
		BEGIN 
		
			SELECT TOP 1 @ValueInt = mm.MatterID 
			FROM @MandateMatters mm 
			WHERE Done = 0 
			Order by mm.MatterID desc 
			
			EXEC dbo._C00_SimpleValueIntoField 170104, @AccountName, @ValueInt, @AqAutomation	
			EXEC dbo._C00_SimpleValueIntoField 170105, @AccSortCode, @ValueInt, @AqAutomation	
			EXEC dbo._C00_SimpleValueIntoField 170106, @AccNumber, @ValueInt, @AqAutomation				
		
			UPDATE mm 
			SET Done = 1  
			FROM @MandateMatters mm 
			WHERE Done = 0 
			AND mm.MatterID = @ValueInt
		
		END

		-- update letter text
		EXEC _C600_UpdateLetterTextField 175462, 'PDCHANGEACCOUNT', @MatterID

	END	

	/* SYS Queue Premium Payment or Refund */
	IF @EventTypeID IN (156752, -- payment -- 156622
						156754) -- refund  -- 156749
	BEGIN
	
		SELECT @ValueInt=CASE WHEN @EventTypeID=156752 THEN 1 ELSE 0 END 
			
		EXEC _C600_PA_QueuePremiumPayment @LeadEventID, @MatterID, @ValueInt 	
		
	END
	
	/* We need a proc to prepare the fields to take the card payment, the below will populate the billing system helper fields required to make a payment record*/ 
	IF @EventTypeID = 156808
	BEGIN 
	
		EXEC _C600_PA_QueueCardPayment  @ClientID,  @MatterID,@LeadEventID
	
	END

	/* SYS Take Card Payment or give refund */
	IF @EventTypeID IN (156756) -- Take Payment
	BEGIN
	
		SELECT @ValueInt=CASE WHEN @EventTypeID=156756 THEN 1 ELSE 0 END 
	
		EXEC _C600_Collections_TakeCardPayment @LeadEventID, @MatterID, @ValueInt 	
		
	END
		
	/* ============================================  POLICY ADMIN  ================================================= */
	
	IF @Eventtypeid IN (156757)
	BEGIN
		
			UPDATE TOP (1) le
			SET le.EventDeleted = 1 
			FROM LeadEvent le
			WHERE le.LeadEventID = @LeadEventID
		
	END
	
	/* Closed and transferred to existing PH */
	IF @EventTypeID IN (155202)
	BEGIN
	
		SELECT @NewCustomerID = dbo.fnGetDvAsInt(175285,@CaseID) -- new CustomerID

		EXEC _C600_MoveLeadToExistingCustomer @NewCustomerID, @CustomerID, @LeadID, @CaseID, @MatterID, @WhoCreated

	END	

	/* Update match status & delete any notes*/
	IF @EventTypeID IN (155217, -- PH does not match
						155225, -- PH match - Account details no match
						155201, -- PH match confirmed
						155200, -- Policy on hold - Tentative PH match
						155233) -- PH Match - Pet already exists
	BEGIN
	
		UPDATE MatterDetailValues
		SET DetailValue=CASE @EventTypeID
			WHEN 155217 THEN 72010
			WHEN 155225 THEN 72010
			WHEN 155201 THEN 72009
			WHEN 155200 THEN 72008
			WHEN 155233 THEN 72022
			END
		WHERE DetailFieldID=175280
		AND MatterID=@MatterID
						
		WHILE EXISTS ( SELECT * FROM LeadEvent WITH (NOLOCK) WHERE CaseID=@CaseID AND EventDeleted=0 AND NoteTypeID=901 )
				AND @EventTypeID <> 155200
		BEGIN
			SELECT TOP 1 @NoteLeadEventID=LeadEventID FROM LeadEvent WITH (NOLOCK) WHERE CaseID=@CaseID AND EventDeleted=0 AND NoteTypeID=901
			EXEC DeleteLeadEvent @NoteLeadEventID, @WhoCreated, 'New note required', 4, 0
		END
		
		IF @EventTypeID IN (155217) -- reset pet number
		BEGIN
			
			-- multipet if there is more than one (not cancelled)
			INSERT INTO @MattersToCopy
			SELECT * FROM dbo.fn_C00_1272_Policy_GetPreMatchPetList(@CustomerID)
			
			IF EXISTS ( SELECT * FROM @MattersToCopy WHERE ID2 > 1 ) -- multipet
			BEGIN
			
				UPDATE mdv
				SET DetailValue=mtc.ID2+1
				FROM MatterDetailValues mdv 
				INNER JOIN @MattersToCopy mtc ON mtc.ID1=mdv.MatterID
				WHERE mdv.DetailFieldID=175453
			
			END
			ELSE
			BEGIN
			
				EXEC _C00_SimpleValueIntoField 175453,1,@MatterID,@AqAutomation
			
			END
		
		END

	END	

	/* Followup time on Pre-renewal documents */
	IF @EventTypeID IN (150149, -- PH LET Pre-renewal Notification
						150148, -- PH EM Pre-renewal Notification
						158859) -- PH LET Pre-renewal Notification (UPP) /*GPR 2018-10-26*/
	BEGIN
	
		-- set followup time to end date 
		UPDATE LeadEvent 
		SET FollowupDateTime=dbo.fnGetDvAsDate(175307,@CaseID) 
		WHERE LeadEventID=@LeadEventID
		
	END
	
	DECLARE @NextEventID INT
	
	/* GPR 2018-06-15 display 0.00 in document fields if no change in premium */ 
	IF @EventTypeID = 150156
	BEGIN  
	    EXEC dbo._C00_SimpleValueIntoField 179968,'0.00',@MatterID
	    EXEC dbo._C00_SimpleValueIntoField 179963,'0.00',@MatterID
	    EXEC dbo._C00_SimpleValueIntoField 179960,'0.00',@MatterID
	    EXEC dbo._C00_SimpleValueIntoField 179961,'0.00',@MatterID
	    /*GPR 2018-07-19 display 0.00 in documenbt fields, following change of fields in doc for defect 521*/
		/*NG 2020-12-03 replaced with full values in Set Document Fields for Jira 769*/
		/*EXEC dbo._C00_SimpleValueIntoField 180094,'0.00',@MatterID  -- Premium After MTA
		EXEC dbo._C00_SimpleValueIntoField 180096,'0.00',@MatterID  -- IPT After MTA*/
		/*AHOD 2018-08-24 Added to set Adjustment Type to 'No Change'*/
		EXEC dbo._C00_SimpleValueIntoField 175345,70021,@MatterID   -- Adjustment Type
		
		EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'MTA'

		/*AAG-71*/
		SELECT @EventComments = dbo.fnGetSimpleDv(177387, @MatterID)
		UPDATE LeadEvent
		SET Comments = @EventComments
		WHERE LeadEventID=@LeadEventID

		/*ALM 2020-07-01 AAG-966*/ 
		SELECT TOP 1 @LatestEvent = EventTypeID 
		FROM LeadEvent WITH (NOLOCK) 
		WHERE CaseID = @CaseID
		AND EventTypeID IN (155246, 155247, 158999)
		ORDER BY WhenCreated DESC 

		IF @LatestEvent = 155246
		BEGIN
			SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

			IF @OneVisionID IS NOT NULL 
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Customer', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
			END
		END 

		IF @LatestEvent = 155247
		BEGIN 
			SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

			IF @OneVisionID IS NOT NULL 
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Pets', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
			END
		END 

		IF @LatestEvent = 158999
		BEGIN 

			SELECT @PolicyOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'
				
			IF @PolicyOneVisionID IS NOT NULL 
			BEGIN 
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'PolicyTerm', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID
			END

			SELECT @WellnessOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness' AND ov.VisionPolicyTermID IS NOT NULL

			IF @WellnessOneVisionID IS NOT NULL 
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID
			END

			SELECT @ExamFeeOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'  AND ov.VisionPolicyTermID IS NOT NULL

			IF @ExamFeeOneVisionID IS NOT NULL 
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID
			END

			/*2020-10-16 ALM*/
			SELECT @WellnessOneVisionIDNew = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness' AND ov.VisionPolicyTermID IS NULL

			IF @WellnessOneVisionIDNew IS NOT NULL 
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionIDNew
			END

			SELECT @ExamFeeOneVisionIDNew = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'  AND ov.VisionPolicyTermID IS NULL

			IF @ExamFeeOneVisionIDNew IS NOT NULL 
			BEGIN
				EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionIDNew
			END

		END 
		
	END
	
	IF @EventTypeID IN (159007) /*TESTING - Recalculate Fields*/
	BEGIN
		EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'NewBusiness'		
 	END

	IF @EventTypeID = 155352 /*Mid-term Adjustment - no change to premium*/
	BEGIN

		/*ALM 2021-02-23 FURKIN-260*/
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 1,0,1
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 2,0,1

		/*GPR 2020-04-09 no change in premium, now delete the MTA Quote row if one exists*/
		SELECT TOP (1) @WrittenPremiumID  = wp.WrittenPremiumID
		FROM WrittenPremium wp WITH (NOLOCK)
		WHERE AdjustmentTypeID = 9
		AND wp.MatterID = @MatterID

		IF @WrittenPremiumID IS NOT NULL
		BEGIN

			DELETE FROM WrittenPremium
			WHERE WrittenPremiumID = @WrittenPremiumID
			AND MatterID = @MatterID

		END

		/*GPR 2020-09-24 - Clear out the Breed for SDAAG-169*/
		EXEC dbo._C00_SimpleValueIntoField 175328, '', @CustomerID, @Whocreated

		/*CPS 2017-09-26*/
		/*Make sure Document Fields are accurate*/
		EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'MTA'
	END

	IF @EventTypeID = 150156
	BEGIN

		/*GPR 2020-02-12 no change in premium, now delete the 0.00 MTA row that's been written*/
		SELECT TOP (1) @WrittenPremiumID= wp.WrittenPremiumID
		FROM WrittenPremium wp WHERE AdjustmentTypeID = 2 -- this event is post-application of the 
		AND wp.MatterID = @MatterID
		AND wp.AdjustmentValueGross = 0.00 -- let's also make ure this is the zero value change

		IF @WrittenPremiumID IS NOT NULL
		BEGIN

			DELETE FROM WrittenPremium
			WHERE WrittenPremiumID = @WrittenPremiumID
			AND MatterID = @MatterID

		END

	END

	IF @EventTypeID IN (156717) /*Cancel Policy */
	BEGIN
	
			EXEC _C00_SimpleValueIntoField 176990,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 176991,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 177079,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 177080,'',@MatterID,@Whocreated	
	END

	IF @EventTypeID IN (156617, -- PH Apply Change Update Pet Details
						156717) -- Cancel Policy (manual)
	BEGIN
	
		IF @EventTypeID IN (156617) 
		BEGIN		

		/*NG 2020-11-04 Update Pet Age at Inception*/ /*2020-11-11 moved to correct event*/
		/* Generate Pet Age In Years */

		SELECT 
			@Policyinceptiondate = dbo.fnGetSimpleDvAsDate(170035, @MatterID) /*Grab policy inception date*/
			,@PetDOB = dbo.fnGetSimpleDvAsDate(144274, @LeadID)
			,@AgeInMonths = DATEDIFF(MONTH,@PetDOB, DATEADD(DAY, 1-DATEPART(DAY,@PetDOB), CONVERT(DATE,@Policyinceptiondate))) /*Handles whether the pet is one month younger than calculated when taking days into account*/

		SELECT @PetAge = CONVERT(VARCHAR, CONVERT(DECIMAL (10,0), ROUND((@AgeInMonths / 12),0,1))) + ' years ' /*set pet age in years*/

		/*Write Pet Age to Field */

		EXEC dbo._C00_SimpleValueIntoField 314262,@PetAge,@LeadID,@WhoCreated /*pet age at inception*/
		
			/*2020-07-27 ALM
			SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Pets', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID		
			*/
			EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 2,0,1
			EXEC _C00_SimpleValueIntoField 176990,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 176991,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 177079,'',@MatterID,@Whocreated
			EXEC _C00_SimpleValueIntoField 177080,'',@MatterID,@Whocreated
		
		END
			
		-- remove note
		EXEC _C600_DeleteANote @CaseID, 922	

	END

	IF @EventTypeID = 156618 --PH Apply Change Update PH Record 
	BEGIN
		
		/*2020-07-27 ALM
		SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID

		EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Customer', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID		
		*/
		EXEC _C600_UpdatePHOrPetDetails @LeadEventID, 1,0,1
		EXEC _C00_SimpleValueIntoField 176990,'',@MatterID,@Whocreated /*Cust happy to proceed with requote?*/
		EXEC _C00_SimpleValueIntoField 176991,'',@MatterID,@Whocreated /*Unable to contact customer*/
		EXEC _C00_SimpleValueIntoField 177079,'',@MatterID,@Whocreated /*Continue without making the change?*/
		EXEC _C00_SimpleValueIntoField 177080,'',@MatterID,@Whocreated /*Is request or cancelled change on rating factors?*/
	END 	
	
	/* TESTING - Create Claim Lead */
	IF @EventTypeID IN (150194)
	BEGIN
	
		EXEC _C00_1273_Links_CreateClaimLinkedLead @LeadID
		
	END	
	
	/* Payment Limits */
	IF @EventTypeID IN (155263, -- Ready for refund (cancelling)
						150155) -- Apply adjustment and inform customer ( ex - Ready for refund (MTA) ) 
	BEGIN
	
		/*GPR 2020-10-02 for PPET-601 - Update Product from Endorse Policy*/
		DECLARE @Value INT, @OldValue INT, @HPTableRowID INT
		
		SELECT @Value = dbo.fnGetSimpleDv(314327,@LeadID)
		SELECT @OldValue = dbo.fnGetSimpleDv(170034,@MatterID)
			
		IF NULLIF(@Value,'') IS NOT NULL AND @Value<>@OldValue
		BEGIN
			EXEC dbo._C00_SimpleValueIntoField 170034, @Value, @MatterID, @Whocreated

			SELECT TOP 1 @HPTableRowID = tr.TableRowID 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID 

			SELECT @Value = CAST(@Value AS VARCHAR)

			EXEC _C00_SimpleValueIntoField 145665,@Value, @HPTableRowID, @Whocreated			

		END

		/*GPR 2020-10-13 EXAM FEES*/
		SELECT @Value = dbo.fnGetSimpleDvAsInt(314245,@LeadID)
		SELECT @OldValue = dbo.fnGetSimpleDvAsInt(313931,@LeadID)
		
		IF ISNULL(@Value,'') <> '' /*GPR 2020-12-16 for SDPRU-197*/ AND @Value<>@OldValue
		BEGIN
			EXEC dbo._C00_SimpleValueIntoField 314245, '', @LeadID, @Whocreated /*GPR 2020-12-16 for SDPRU-197*/
			EXEC dbo._C00_SimpleValueIntoField 313931, @Value, @LeadID, @Whocreated

			SELECT TOP 1 @HPTableRowID = tr.TableRowID 
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.DetailFieldID = 170033
			AND tr.MatterID = @MatterID 

			SELECT @DetailValue = CASE @Value WHEN 5145 THEN 'No' ELSE 'Yes' END

			EXEC _C00_SimpleValueIntoField 314011, @DetailValue, @HPTableRowID, @Whocreated	

		END


		/*GPR 2020-10-20*/
		/*UPDATE STATE VARIANCE DETAIL FIELDS*/
		
		/*NG 2021-04-27 client variance added*/
		IF @ClientID = 607
		BEGIN
			DECLARE @BrandingID INT

			SELECT @BrandingID = dbo.fnGetSimpleDvAsInt(170144, @CustomerID) /*Affinity Details*/

			/*NG 2021-04-27 Update DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)*/
			EXEC dbo._C600_Add_StateVarianceDetailFieldsByBrand_RLID @BrandingID, @MatterID


		END
		ELSE
		BEGIN

			DECLARE @ExamFeesIncluded INT
			DECLARE @WorkingDog INT
			DECLARE @WellnessProduct INT
			DECLARE @ProductID INT
			DECLARE @WellnessRLID INT

			SELECT @ProductID = dbo.fnGetSimpleDvAsInt(170034, @MatterID)
			SELECT @ExamFeesIncluded = dbo.fnGetSimpleDvAsInt(313931, @LeadID)
			SELECT @WorkingDog = dbo.fnGetSimpleDvAsInt(313929, @LeadID)
			SELECT @WellnessRLID = dbo.fnGetSimpleDvAsInt(314235, @MatterID)
			SELECT @WellnessProduct = CASE @WellnessRLID WHEN 0 THEN 5145 ELSE 5144 END

			/*Update DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)*/
			EXEC dbo._C605_Add_StateVarianceDetailFields_RLID @ProductID, @ExamFeesIncluded, @WorkingDog, @WellnessProduct, @MatterID
		END

		/*GPR 2020-12-14 for SDPRU-193*/
		SELECT @PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(@MatterID, dbo.fn_GetDate_Local())
		
		SELECT @ProductName = rlpn.DetailValue
		FROM Matter m WITH (NOLOCK)
		INNER JOIN MatterDetailValues pn WITH (NOLOCK) ON m.MatterID=pn.MatterID AND pn.DetailFieldID=170034
		INNER JOIN ResourceListDetailValues rlpn WITH (NOLOCK) ON rlpn.ResourceListID=pn.ValueInt AND rlpn.DetailFieldID=146200
		WHERE m.MatterID=@MatterID

		UPDATE p
		SET p.ProductName = @ProductName
		FROM PurchasedProduct p WITH (NOLOCK)
		WHERE p.PurchasedProductID = @PurchasedProductID

		DECLARE @Limit MONEY, @PaymentValue MONEY
		
		SELECT	 @Limit=ValueMoney
				,@PaymentValue=dbo.fnGetDvAsMoney (175379,@CaseID) /*Adjustment Value  (for adjustment cover period)*/
		FROM ClientPersonnelDetailValues WITH (NOLOCK) 
		WHERE DetailFieldID=175378 /*Approval limit*/
		AND ClientPersonnelID=@WhoCreated
		
		IF (@PaymentValue > @Limit and @WhoCreated <> 58552/*44412*/)
		BEGIN	
		
			SELECT @ErrorMessage = '<font color="red"></br></br>You cannot add this event. The payment value is greater than your authorised limit</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )

		END
		
		IF (@PaymentValue > @Limit and @WhoCreated = 58552/*44412*/)
		BEGIN
			
			EXEC _C00_SetLeadEventComments @LeadEventID, 'Limit not considered for automation user',1
		
		END
		
		IF @EventTypeID = 155263
		BEGIN
			/*Add a note to claims to stop new claims being added*/
			SELECT @ClaimLeadID = dbo.fn_C00_1272_GetClaimLeadFromPolicyLead(@LeadID)

			
			SELECT TOP 1 @ClaimCaseID = c.CaseID FROM Cases c WITH (NOLOCK) where c.LeadID = @ClaimLeadID
			
		END
		
		-- queue the adjustment
		EXEC _C600_PA_QueueMidTermAdjustment @LeadEventID, @MatterID
		
		IF @EventTypeID = 150155 /*Apply adjustment and inform customer*/
		BEGIN

			EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'MTA'

			/* update customer level state variance RL. NG 2020-09-07 */
		SELECT @CountryID = cl.CountryID
		FROM Clients cl WITH (NOLOCK) WHERE cl.ClientID = @ClientID

		IF @CountryID = 233
		BEGIN
		
			UPDATE csv
			SET DetailValue = rldv.ResourceListID
			FROM Customers cu WITH (NOLOCK) 
			INNER JOIN StateByZip sbz WITH (NOLOCK) on cu.PostCode = sbz.Zip
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.DetailFieldID = 313958 AND rldv.ValueInt = sbz.StateID
			INNER JOIN CustomerDetailValues csv WITH (NOLOCK) ON cu.CustomerID = csv.CustomerID AND csv.DetailFieldID = 314016
			WHERE cu.customerID = @CustomerID
		
		END
		ELSE
		IF @CountryID = 39
		BEGIN
		
			UPDATE csv
			SET DetailValue = rldv.ResourceListID
			FROM Customers cu WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.DetailFieldID = 313957 AND rldv.DetailValue = cu.County
			INNER JOIN CustomerDetailValues csv WITH (NOLOCK) ON cu.CustomerID = csv.CustomerID AND csv.DetailFieldID = 314016
			WHERE cu.customerID = @CustomerID
		

		END
			/*NG 2020-11-16 Update Pet Age at Inception*/ /*2020-11-16 moved to differet event to ensure age has populated first*/
			/* Generate Pet Age In Years at inception*/
			BEGIN		

				SELECT 
					@Policyinceptiondate = dbo.fnGetSimpleDvAsDate(170035, @MatterID) /*Grab policy inception date*/
					,@PetDOB = dbo.fnGetSimpleDvAsDate(144274, @LeadID)
					,@AgeInMonths = DATEDIFF(MONTH,@PetDOB, DATEADD(DAY, 1-DATEPART(DAY,@PetDOB), CONVERT(DATE,@Policyinceptiondate))) /*Handles whether the pet is one month younger than calculated when taking days into account*/

				SELECT @PetAge = CONVERT(VARCHAR, CONVERT(DECIMAL (10,0), ROUND((@AgeInMonths / 12),0,1))) + ' years ' /*set pet age in years*/

				/*Write Pet Age to Field */

				EXEC dbo._C00_SimpleValueIntoField 314262,@PetAge,@LeadID,@WhoCreated /*pet age at inception*/

			END

			/*2020-07-27 ALM AAG-1046*/
			SELECT TOP 1 @LatestEvent = EventTypeID
			FROM LeadEvent WITH (NOLOCK) 
			WHERE CaseID = @CaseID
			AND EventTypeID IN (155246, 155247, 158999)
			ORDER BY WhenCreated DESC 

			IF @LatestEvent IN (155246) 
			BEGIN
				SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

				IF @OneVisionID IS NOT NULL 
				BEGIN
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Customer', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
				END
			END 

			IF @LatestEvent IN (155247)
			BEGIN 
				SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

				IF @OneVisionID IS NOT NULL 
				BEGIN
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Pets', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @OneVisionID
				END
			END 

			BEGIN 

				SELECT @PolicyOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'
				
				IF @PolicyOneVisionID IS NOT NULL 
				BEGIN 
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'PolicyTerm', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @PolicyOneVisionID
				END

				SELECT @WellnessOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness' AND ov.VisionPolicyTermID IS NOT NULL

				IF @WellnessOneVisionID IS NOT NULL 
				BEGIN
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionID
				END

				SELECT @ExamFeeOneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'  AND ov.VisionPolicyTermID IS NOT NULL

				IF @ExamFeeOneVisionID IS NOT NULL 
				BEGIN
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'PUT',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionID
				END

				/*2020-10-16 ALM*/
				SELECT @WellnessOneVisionIDNew = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Wellness' AND ov.VisionPolicyTermID IS NULL

				IF @WellnessOneVisionIDNew IS NOT NULL 
				BEGIN
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @WellnessOneVisionIDNew
				END

				SELECT @ExamFeeOneVisionIDNew = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'ExamFee'  AND ov.VisionPolicyTermID IS NULL

				IF @ExamFeeOneVisionIDNew IS NOT NULL 
				BEGIN
					EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'ExamFee', @HttpMethod = 'POST',  @LeadEventID = @LeadEventID, @OneVisionID = @ExamFeeOneVisionIDNew
				END

			END 

		END

	END

	IF @EventTypeID IN (159001)/*GPR 2020-09-17*/
	BEGIN
		EXEC [_C600_PA_Policy_UpdateWellnessHistory] @MatterID, @LeadEventID /*GPR 2020-09-17*/
	END

	/* Payment declined post cancellation */
	IF @EventTypeID IN (156893)
	BEGIN
	
		-- clear flag field so we don't loop
		EXEC _C00_SimpleValueIntoField 177420,'',@MatterID	
	
	END
	
	/* Apply cancelled policy processing for matching - pet already exists */
	IF @EventTypeID IN (155234, -- PH LET Policy Cancelled - Pet Already Exists
						155351) -- PH EM Policy Cancelled - Pet Already Exists
	BEGIN
		
		SELECT @LogEntry = 'Policy Cancelled. No match - pet already exists'
		
		-- update cancellation reason & date
		SELECT @VarcharDate=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
		EXEC _C00_SimpleValueIntoField 170054, 72040, @MatterID, @AqAutomation -- cancelled by SIG
		EXEC _C00_SimpleValueIntoField 170055, @VarcharDate, @MatterID, @AqAutomation
		-- update policy end dates
		EXEC dbo._C00_SimpleValueIntoField 170037, @VarcharDate, @MatterID, @AqAutomation	
		SELECT TOP 1 @TableRowID=tr.TableRowID -- there should only be one anyway
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.MatterID=@MatterID AND tr.DetailFieldID=170033
		ORDER BY tr.TableRowID DESC
		EXEC dbo._C00_SimpleValueIntoField 145664, @VarcharDate, @TableRowID, @AqAutomation		
			
		-- Set the Policy Status field to 'Cancelled'
		EXEC _C00_SimpleValueIntoField 170038,43003,@MatterID
		
	END

	/* Start collections thread for finalising CP collection */
	IF @EventTypeID IN (155316, -- Reinstatement Deferred CP collection
						150173, -- MTA Deferred CP collection
						155325) -- Auto renew CP
	BEGIN
	
			SELECT @ColMatterID=ltr.ToMatterID 
			FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493

			EXEC _C600_AddAutomatedEvent @ColMatterID, 155288, 1
			
	END

	/* Morph OOP events to kick off in-process threads */
	IF @EventTypeID IN (156722) -- Premium  increase query
	BEGIN
		UPDATE LeadEvent
		SET EventTypeID = 156633, WhenFollowedUp = NULL, NextEventID = NULL
		WHERE LeadEventID = @LeadEventID
	END
	IF @EventTypeID IN (156723) -- Request to change customer name
	BEGIN
		UPDATE LeadEvent
		SET EventTypeID = 156599, WhenFollowedUp = NULL, NextEventID = NULL
		WHERE LeadEventID = @LeadEventID
	END
	IF @EventTypeID IN (156686) -- Travel Cover Activation
	BEGIN
		UPDATE LeadEvent
		SET EventTypeID = 156685, WhenFollowedUp = NULL, NextEventID = NULL
		WHERE LeadEventID = @LeadEventID
	END
	IF @EventTypeID IN (156688) -- PH Notification of Death of PH
	BEGIN
		UPDATE LeadEvent
		SET EventTypeID = 156689, WhenFollowedUp = NULL, NextEventID = NULL
		WHERE LeadEventID = @LeadEventID
	END
			
	/* =================================================== Claim Lead Type ================================================= */
	
	IF @EventTypeID = 156745 /*Bacs claim payment failed, reissue*/ 
	BEGIN 
	
		EXEC _C600_CreatePaymentRowForRetry   @CaseID 
		
		/*For this matter, any retry payments will not have a payment method yet*/
		DECLARE @InsertedRows TABLE (TableRowID INT,PayTo INT,Amount DECIMAL(18,2),MatterID INT)
		INSERT INTO @InsertedRows (TableRowID,PayTo,Amount,MatterID) 
		
		SELECT tr.TableRowID, payto.ValueInt, amount.ValueMoney , tr.MatterID  FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues payto WITH (NOLOCK) on payto.TableRowID = tr.TableRowID and payto.DetailFieldID = 159408
		INNER JOIN dbo.TableDetailValues amount WITH (NOLOCK) on amount.TableRowID = tr.TableRowID and amount.DetailFieldID = 154518
		LEFT JOIN dbo.TableDetailValues payby WITH (NOLOCK) on payby.TableRowID = tr.TableRowID and payby.DetailFieldID = 170232
		where tr.MatterID = 7309--@MatterID 
		and tr.DetailFieldID = 154485
		and (payby.ValueInt = 0 or payby.ValueInt IS NULL)
		
		/*Find me the customer ledger item that was the initial payment, there will only be one payment to any party on one matter, so matching on PayTo and MatterID should be pretty solid
		  We include amount as a belt and braces check and to deal with any third party payments*/ 
		Update orig 
		SET DetailValue = c.OutgoingPaymentID
		--SELECT c.OutgoingPaymentID
		FROM CustomerLedger c  
		INNER JOIN @InsertedRows i on i.MatterID = c.ObjectID and c.ObjectTypeID = 2
		INNER JOIN dbo.TableDetailValues payto on CAST(payto.TableRowID as VARCHAR(20)) = c.TransactionReference
		LEFT JOIN dbo.TableDetailValues orig on orig.TableRowID = i.TableRowID and orig.DetailFieldID = 159479
		Where payto.ValueInt = i.PayTo 
		AND i.Amount = (c.TransactionGross * -1)
		and c.ObjectTypeID = 2
		and c.ObjectID = 7309--@MatterID  
		and c.TransactionDescription like 'Claim'
		AND EXISTS (SELECT * FROM CustomerLedger cl WITH (NOLOCK) where cl.TransactionReference = c.TransactionReference and cl.TransactionDescription = 'Claim Payment Reversal')
		
	END 
	
	
	IF @EventTypeID = 122711
	BEGIN 
	
		SELECT @ValueMoney = dbo.fn_C600_Claim_GetRemainingVetExcess(@MatterID)
		
		EXEC _C00_SimpleValueIntoField 177378,@ValueMoney,@MatterID,@WhoCreated
	
	END

	/*When adding a new claim create the leadtypeRelationship record so we can cross reference on the EOB doc*/
	/* SLACKY-PERF 20190705 : missing nolock */
	IF @EventTypeID = 150086 AND NOT EXISTS (SELECT * FROM LeadTypeRelationship l WITH (NOLOCK) /* SLACKY-PERF-20190705 */ WHERE l.ToMatterID = @MatterID AND l.FromLeadTypeID = 1492) 
	BEGIN 
		
		INSERT INTO LeadTypeRelationship (FromLeadTypeID,ToLeadTypeID,FromLeadID,ToLeadID,FromMatterID, ToMatterID, ClientID)
		SELECT top 1 1492,1490,l.FromLeadID,l.ToLeadID,l.FromMatterID,@MatterID,@ClientID FROM LeadTypeRelationship l WITH (NOLOCK)
		WHERE l.ToLeadID = @LeadID 
		AND l.ToLeadTypeID = 1490 
		
	END
		
	/* FNOL Details */
	IF @EventTypeID IN (150086)
	BEGIN
	
		/*Rather than entering the cause twice, automatically set it from the RL*/ 
		SELECT @ValueINT = rldv.ValueINT 
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv.ValueInt AND rldv.DetailFieldID = 162655
		WHERE mdv.MatterID = @MatterID 
		AND mdv.DetailFieldID = 144504
		
		EXEC _C00_SimpleValueIntoField   175576,@ValueINT,@MatterID,@WhoCreated  
	
		-- Other insurance validation
		IF dbo.fnGetDvAsInt(170091,@CaseID) = 5144 AND dbo.fnGetDv(170233,@CaseID) = ''
		BEGIN

			SELECT @ErrorMessage = '<font color="red"></br></br>If the customer has other insurance cover you must enter the details in Other Insurer</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		
		END
		/*If Treatment start is greater than treatment end*/ 
		IF dbo.fnGetSimpleDvAsDate(144366,@MatterID) > dbo.fnGetSimpleDvAsDate(145674,@MatterID) 
		BEGIN 
		
			SELECT @ErrorMessage = '<font color="red"></br></br>Treatment End Date is before Treatment Start. Please correct before continuing</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		
		END

		/*2018-03-13 - SA*/
		/*find the policy start date*/
		 SELECT @PolicyAdminMatterID = [dbo].[fn_C00_1272_GetPolicyMatterFromClaimMatter] (@MatterID)

		/*If Treatment end is outside policy term*/ 
		IF dbo.fnGetSimpleDvAsDate(144366,@MatterID) > dbo.fnGetSimpleDvAsDate(170037,@PolicyAdminMatterID) 
		BEGIN 
		
		
			SELECT @ErrorMessage = '<font color="red"></br></br>You are attempting to register a claim against a cancelled policy. This is not allowed.</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		
		END
	
		/*2018-03-19 - SA - find the policy start date*/
		 SELECT @PolicyAdminMatterID = [dbo].[fn_C00_1272_GetPolicyMatterFromClaimMatter] (@MatterID)

		/*If Treatment start is outside policy term*/
		/*2018-10-18 JEL No Steve, inception date*/ 

		IF dbo.fnGetSimpleDvAsDate(144366,@MatterID) < dbo.fnGetSimpleDvAsDate(170035,@PolicyAdminMatterID) 
		BEGIN 
		
			SELECT @ErrorMessage = '<font color="red"></br></br>Policy Start Date is outside of Policy term. Please correct before continuing</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
		
		END
			
		
		/*Check what scheme we're using*/ 
		SELECT @SchemeID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
		
		/*Get the scheme type*/ 
		SELECT @SchemeType = dbo.fn_C00_1272_GetPolicyTypeFromScheme(@SchemeID)
		/*If the scheme type is 12 months, add to the existing conditions table when cover elaspses*/ 
		IF @SchemeType = 76175
		BEGIN 
				
			EXEC TableRows_Insert @TableRowID OUTPUT,@ClientID,@PolicyLeadID,NULL,177428,16154,0,0,@CustomerID,NULL,@WhoCreated,NULL,@LeadEventID 

			EXEC dbo._C00_CompleteTableRow   @TableRowID,@MatterID,0,0 
			
			/*Pick up values from customer level fields, Create a table row and store the existing conditions against the correct lead.*/ 
			
			/*179752 Condition 	*/
			SELECT @ValueINT = dbo.fnGetSimpleDvAsInt(144504,@MatterID) 
			EXEC _C00_SimpleValueIntoField  177426,@ValueINT,@TableRowID,@WhoCreated
			
			/*179753	Condition Added	*/
			SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(144892,@CustomerID) 
			select @ValueDate = DATEADD(Month,12,@ValueDate) 
			EXEC _C00_SimpleValueIntoField 177427,@ValueDate,@TableRowID,@WhoCreated
					
			/*179757	Active or inactive	*/
			EXEC _C00_SimpleValueIntoField 177522,74577,@TableRowID,@WhoCreated
			
			/*179758	Date to be reviewed	*/	
			SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(179758,@CustomerID) 
			EXEC _C00_SimpleValueIntoField 177901,@ValueDate,@TableRowID,@WhoCreated
			EXEC _C00_SimpleValueIntoField 179758,'',@CustomerID,@WhoCreated
			
			/*179759	Body Part	*/
			SELECT @ValueINT = dbo.fnGetSimpleDvAsInt(144333,@CustomerID) 
			EXEC _C00_SimpleValueIntoField 179721,@ValueINT,@TableRowID,@WhoCreated
			
			/*179754	Condition Added by ClientPersonnelID*/
			EXEC _C00_SimpleValueIntoField 177515,@WhoCreated,@TableRowID,@WhoCreated	
			
			/*179755	Condition Added by UserName	*/
			SELECT @DetailValue = cp.UserName FROM ClientPersonnel cp WITH (NOLOCK) 
			where cp.ClientPersonnelID = @WhoCreated
			EXEC _C00_SimpleValueIntoField 177516,@DetailValue,@TableRowID,@WhoCreated
		
		END 		
		
	END
	
	-- Pay by Cheque
	IF @EventTypeID = 150190
	BEGIN
		  
		  EXEC _C00_SimpleValueIntoField 179890,5145,@MatterID,@WhoCreated 
		  

		  /*Pick up the values we have written to the PAyments Table*/ 	 
		  DECLARE @ChRows TABLE (TableRowID INT, Type INT , Name VARCHAR(100) , Processed INT) 
		  INSERT INTO @ChRows (TableRowID, Type ,Name,Processed) 
		  SELECT r.TableRowID, tdvType.ValueInt, tdvName.DetailValue ,0 FROM 
				 TableRows r WITH (NOLOCK)
				 INNER JOIN dbo.TableDetailValues tdvApproved WITH (NOLOCK) ON tdvApproved.TableRowID = r.TableRowID AND tdvApproved.DetailFieldID = 159407
				 INNER JOIN dbo.TableDetailValues tdvPaymentType WITH (NOLOCK) ON tdvPaymentType.TableRowID = r.TableRowID AND tdvPaymentType.DetailFieldID = 154517
				 INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) on tdvType.TableRowID = r.TableRowID AND tdvType.DetailFieldID = 159408
				 INNER JOIN TableDetailValues tdvName WITH (NOLOCK) on tdvName.TableRowID = r.TableRowID and tdvName.DetailFieldID = 154490
		  WHERE 
				 r.MatterID = @MatterID
				 AND r.DetailFieldID = 154485 -- Payment row
				 AND tdvApproved.ValueDate IS NULL -- Unapproved
				 AND tdvPaymentType.ValueInt = 0 -- Payment (not refund/cancellation)
		  
		  WHILE EXISTS (SELECT * FROM @ChRows c 
						WHERE c.Processed = 0) 
		  BEGIN
		
			SELECT top 1 @TableRowID = t.TableRowID, @ValueInt = t.Type , @AccountName = t.Name
			FROM @ChRows t 
			where t.Processed = 0
			
			/*Pick up the affinity specific client account ID to write to the customers account record*/ 
			SELECT @ClientAccountID = rldv.ValueInt 
			FROM CustomerDetailValues cdv WITH (NOLOCK) 
			INNER JOIN Matter m WITH (NOLOCK) on m.CustomerID = cdv.CustomerID 
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
			WHERE m.MatterID = @MatterID 
			AND cdv.DetailFieldID = 170144
			AND rldv.DetailFieldID = 179949

			/*Pay PH*/ 
			IF @ValueInt = 44231 
			BEGIN 
				
				/*Have we already set up an cheque account for this PH*/ 
				SELECT @AccountID = a.AccountID 
				FROM Account a WITH (NOLOCK) 
				where a.AccountUseID = 2  /*Claim payment*/ 
				and a.AccountTypeID = 3 /*Cheque*/ 
				and a.CustomerID = @CustomerID
				and a.ObjectTypeID = 2 /*if object type is 2, it uses the Matter ID as it's source so is PH*/ 
				
				IF ISNULL(@AccountID,0) = 0 
				BEGIN 
											
					/*If we don't have an account for this detail, then create one*/ 
					EXEC @AccountID = dbo.Billing__CreateClaimsAccountRecord  @ClientID,@MatterID,2,NULL,NULL,@AccountName,@ClientAccountID,@CustomerID,@WhoCreated,3
						
				END
			END 
			/*Pay Nominated TP*/ 
			IF @ValueInt = 44233
			BEGIN 
													
				/*Create the account linked to the customer, but use the TableRowID as the ObjectID*/ 
				EXEC @AccountID = dbo.Billing__CreateClaimsAccountRecord @ClientID,@TableRowID,6,NULL,NULL,@AccountName,@ClientAccountID,@CustomerID,@WhoCreated,3
			
			END 
			/*Pay Vet*/ 
			IF @ValueInt = 44232 
			BEGIN 
				
				/*Get the vet account*/ 
				SELECT @AccountID = rldv.ValueINT 
				FROM MatterDetailValues mdv WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt  and rldv.detailFieldID = 179999
				WHERE mdv.MatterID = @MatterID 
				AND mdv.DetailFieldID = 157804
							
			END 
			/*Pay REF Vet*/ 
			IF @ValueInt = 46135
			BEGIN 
				
				/*Get the vet account*/ 
				SELECT @AccountID = rldv.ValueINT 
				FROM MatterDetailValues mdv WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt  and rldv.detailFieldID = 179999
				WHERE mdv.MatterID = @MatterID 
				AND mdv.DetailFieldID = 146236
										
			END 
			/*Pay OOH Vet*/ 
			IF @ValueInt = 76367
			BEGIN 
				
				/*Get the vet account*/ 
				SELECT @AccountID = rldv.ValueINT 
				FROM MatterDetailValues mdv WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt  and rldv.detailFieldID = 179999
				WHERE mdv.MatterID = @MatterID 
				AND mdv.DetailFieldID = 176944
				
			END 
		
			EXEC _C00_SimpleValueIntoField 170232,70018,@TableRowID,@WhoCreated
			EXEC _C00_SimpleValueIntoField 179988, @AccountID, @TableRowID, @WhoCreated
			
			Update @ChRows 
			SET Processed = 1 
			WHERE TableRowID = @TableRowID
		
		END
	END
	
	-- Pay by BACS                    
	IF @EventTypeID IN (150191,157578,157577)
	BEGIN
	
	    EXEC _C00_SimpleValueIntoField 179890,5144,@MatterID,@WhoCreated 
	
		/*What payments are we looking at for this claim*/ 
		DECLARE @Payments TABLE (TableRowID INT, pType VARCHAR(10) ) 
		INSERT INTO @Payments (TableRowID,pType)  
		
		SELECT r.TableRowID, tdvType.DetailValue FROM  TableRows r WITH (NOLOCK)
		INNER JOIN dbo.TableDetailValues tdvApproved WITH (NOLOCK) ON tdvApproved.TableRowID = r.TableRowID AND tdvApproved.DetailFieldID = 159407
		INNER JOIN dbo.TableDetailValues tdvPaymentType WITH (NOLOCK) ON tdvPaymentType.TableRowID = r.TableRowID AND tdvPaymentType.DetailFieldID = 154517
		INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) on tdvType.TableRowID = r.TableRowID AND tdvType.DetailFieldID = 159408
		WHERE 
		r.MatterID = @MatterID
		AND r.DetailFieldID = 154485 -- Payment row
		AND tdvApproved.ValueDate IS NULL -- Unapproved
		AND tdvPaymentType.ValueInt = 0   -- Payment (not refund/cancellation)
		
		/*Policy Holder*/ 
		IF EXISTS (SELECT * FROM @Payments p where p.ptype = 44231) 
		BEGIN 
			 			
			/*If we are paying the PH, get the detail*/
			SELECT @AccountName = mdv.DetailValue, @AccNumber = mdv1.DetailValue, @AccSortCode = mdv2.DetailValue 
			FROM @Payments p
			INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = p.TableRowID 
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK)  on mdv.MatterID = tr.MatterID and mdv.DetailFieldID = 170264 -- Payee name
			INNER JOIN MatterDetailValues mdv1 WITH (NOLOCK) on mdv1.MatterID = mdv.MatterID and mdv1.DetailFieldID = 170265  -- Payee account number
			INNER JOIN MatterDetailValues mdv2 WITH (NOLOCK) on mdv2.MatterID = mdv.MatterID and mdv2.DetailFieldID = 170266 -- Payee sort code
			WHERE tr.DetailFieldID = 154485  
			AND p.pType = 44231
			
			/*and check the existing claim payments accounts the customer has*/ 
			SELECT  @AccountID = a.AccountID  
			FROM Account a WITH (NOLOCK) 
			WHERE a.CustomerID = @CustomerID 
			AND a.AccountNumber = @AccNumber 
			AND a.Sortcode = @AccSortCode 
			AND a.AccountHolderName = @AccountName
			AND a.AccountUseID = 2 /*Claim Payment Account Only*/ 
			AND a.AccountTypeID = 1
			
			/*If we don't have an account for this detail, then create one*/ 
			IF ISNULL(@AccountID,0) = 0 
			BEGIN 
							/*Pick up the affinity specific client account ID to write to the customers account record*/ 
				SELECT @ClientAccountID = rldv.ValueInt 
				FROM CustomerDetailValues cdv WITH (NOLOCK) 
				INNER JOIN Matter m WITH (NOLOCK) on m.CustomerID = cdv.CustomerID 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
				WHERE m.MatterID = @MatterID 
				AND cdv.DetailFieldID = 170144
				AND rldv.DetailFieldID = 179949
				
				/*If we don't have an account for this detail, then create one*/ 
				IF ISNULL(@AccountID,0) = 0 
				BEGIN 
					
					EXEC @AccountID = dbo.Billing__CreateClaimsAccountRecord @ClientID,@MatterID,2,@AccNumber,@AccSortCode,@AccountName,@ClientAccountID,@CustomerID,@WhoCreated,1
					
				END
			END
			
			/*Record the account details and AccountID to the claim payment row*/ 
			INSERT TableDetailValues (ClientID, TableRowID, MatterID, DetailFieldID, DetailValue)
			SELECT 
			r.ClientID, r.TableRowID, r.MatterID, df.DetailFieldID
			,CASE df.DetailFieldID 
			WHEN 170232 THEN CONVERT(VARCHAR, 70017) -- Payment method = BACS
			WHEN 154490 THEN @AccountName -- Payee name
		    WHEN 175270 THEN @AccNumber -- Payee account number
			WHEN 175271 THEN @AccSortCode -- Payee sort code
			WHEN 179988 THEN CONVERT(VARCHAR,@AccountID)
			END AS DetailValue
			FROM @Payments p 
			INNER JOIN TableRows r WITH (NOLOCK) on r.TableRowID = p.TableRowID
			CROSS JOIN dbo.DetailFields df WITH (NOLOCK) 
			WHERE 
			r.MatterID = @MatterID
			AND r.DetailFieldID = 154485 -- Payment row
			AND df.DetailFieldID IN (170232, 154490, 175270, 175271,179988) -- Method, name, a/c, sort code
			AND p.pType = 44231		
		 
		END
		IF EXISTS (SELECT * FROM @Payments p where p.ptype = 44232) /*VET*/ 
		BEGIN 
				 
			 INSERT TableDetailValues (ClientID, TableRowID, MatterID, DetailFieldID, DetailValue)
			 SELECT 
			 r.ClientID, r.TableRowID, r.MatterID, df.DetailFieldID
			 ,CASE df.DetailFieldID 
			 WHEN 170232 THEN CONVERT(VARCHAR, 70017) -- Payment method = BACS
			 WHEN 154490 THEN dbo.fnGetSimpleDv(175587,mdv.ValueINT) -- Payee name
			 WHEN 175270 THEN dbo.fnGetSimpleDv(175590,mdv.ValueINT) -- Payee account number
			 WHEN 175271 THEN dbo.fnGetSimpleDv(175589,mdv.ValueINT) -- Payee sort code
			 WHEN 179988 THEN dbo.fnGetSimpleDv(179989,mdv.ValueINT) --CONVERT(VARCHAR,@AccountID)
			 END AS DetailValue
			 FROM @Payments p 
			 INNER JOIN TableRows r WITH (NOLOCK) on r.TableRowID = p.TableRowID
			 INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = r.MatterID and mdv.DetailFieldID = 157804
			 --INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt 
			 CROSS JOIN dbo.DetailFields df WITH (NOLOCK) 
			 WHERE 
			 r.MatterID = @MatterID
			 AND r.DetailFieldID = 154485 -- Payment row
			-- AND rldv.DetailFieldID IN (175587,175589,175590) -- Method, name, a/c, sort code
			 AND p.pType = 44232
			 AND df.DetailFieldID IN (170232, 154490, 175270, 175271,179988) 
							 
		END
		IF EXISTS (SELECT * FROM @Payments p where p.ptype = 46135) /*Ref VET*/ 
		BEGIN 
			 
			 INSERT TableDetailValues (ClientID, TableRowID, MatterID, DetailFieldID, DetailValue)
			 SELECT 
			 r.ClientID, r.TableRowID, r.MatterID, df.DetailFieldID
			 ,CASE df.DetailFieldID 
			 WHEN 170232 THEN CONVERT(VARCHAR, 70017) -- Payment method = BACS
			 WHEN 154490 THEN dbo.fnGetSimpleDv(175587,mdv.ValueInt) -- Payee name
			 WHEN 175270 THEN dbo.fnGetSimpleDv(175590,mdv.ValueINT) -- Payee account number
			 WHEN 175271 THEN dbo.fnGetSimpleDv(175589,mdv.ValueINT) -- Payee sort code
			 WHEN 179988 THEN dbo.fnGetSimpleDv(179989,mdv.ValueINT) --CONVERT(VARCHAR,@AccountID)
			 END AS DetailValue
			 FROM 
			 @Payments p 
			 INNER JOIN TableRows r WITH (NOLOCK) on r.TableRowID = p.TableRowID
			 INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = r.MatterID and mdv.DetailFieldID = 146236
			 CROSS JOIN dbo.DetailFields df WITH (NOLOCK) 
			 WHERE 
			 r.MatterID = @MatterID
			 AND r.DetailFieldID = 154485 -- Payment row
			 AND p.pType = 46135
			 AND df.DetailFieldID IN (170232, 154490, 175270, 175271,179988) 
			 
						 
		END
		IF EXISTS (SELECT * FROM @Payments p where p.ptype = 76367) /*OOH VET*/ 
		BEGIN 
			 
			 INSERT TableDetailValues (ClientID, TableRowID, MatterID, DetailFieldID, DetailValue)
			 SELECT 
			 r.ClientID, r.TableRowID, r.MatterID, df.DetailFieldID
			 ,CASE df.DetailFieldID 
			 WHEN 170232 THEN CONVERT(VARCHAR, 70017) -- Payment method = BACS
			 WHEN 154490 THEN dbo.fnGetSimpleDv(175587,mdv.ValueInt) -- Payee name
			 WHEN 175270 THEN dbo.fnGetSimpleDv(175590,mdv.ValueINT) -- Payee account number
			 WHEN 175271 THEN dbo.fnGetSimpleDv(175589,mdv.ValueINT) -- Payee sort code
			 WHEN 179988 THEN dbo.fnGetSimpleDv(179989,mdv.ValueINT) --CONVERT(VARCHAR,@AccountID)
			 END AS DetailValue
			 FROM 
			 @Payments p 
			 INNER JOIN TableRows r WITH (NOLOCK) on r.TableRowID = p.TableRowID
			 INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = r.MatterID and mdv.DetailFieldID = 176944
			 CROSS JOIN dbo.DetailFields df WITH (NOLOCK) 
			 WHERE 
			 r.MatterID = @MatterID
			 AND r.DetailFieldID = 154485 -- Payment row
			 AND p.pType = 76367
			 AND df.DetailFieldID IN (170232, 154490, 175270, 175271,179988) 
						 
		END
		IF EXISTS (SELECT * FROM @Payments p where p.ptype = 44233) /*Nominated Third Party*/ 
		BEGIN 
			 
			/*If we are paying the TP, we will have moved the details to the standard fields*/
			SELECT @AccountName = mdv.DetailValue, @AccNumber = mdv1.DetailValue, @AccSortCode = mdv2.DetailValue , @TableRowID = tr.TableRowID
			FROM @Payments p
			INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = p.TableRowID 
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK)  on mdv.MatterID = tr.MatterID and mdv.DetailFieldID = 170264 -- Payee name
			INNER JOIN MatterDetailValues mdv1 WITH (NOLOCK) on mdv1.MatterID = mdv.MatterID and mdv1.DetailFieldID = 170265  -- Payee account number
			INNER JOIN MatterDetailValues mdv2 WITH (NOLOCK) on mdv2.MatterID = mdv.MatterID and mdv2.DetailFieldID = 170266 -- Payee sort code
			WHERE tr.DetailFieldID = 154485  
			AND p.pType = 44233
			
			SELECT @ClientAccountID = rldv.ValueInt 
			FROM CustomerDetailValues cdv WITH (NOLOCK) 
			INNER JOIN Matter m WITH (NOLOCK) on m.CustomerID = cdv.CustomerID 
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
			WHERE m.MatterID = @MatterID 
			AND cdv.DetailFieldID = 170144
			AND rldv.DetailFieldID = 179949
			 
			/*Create the account linked to the customer, but use the TableRowID as the ObjectID*/ 
			EXEC @AccountID = dbo.Billing__CreateClaimsAccountRecord @ClientID,@TableRowID,6,@AccNumber,@AccSortCode,@AccountName,@ClientAccountID,@CustomerID,@WhoCreated,1

			 
			INSERT TableDetailValues (ClientID, TableRowID, MatterID, DetailFieldID, DetailValue)
			SELECT 
			r.ClientID, r.TableRowID, r.MatterID, df.DetailFieldID
			,CASE df.DetailFieldID 
			WHEN 170232 THEN CONVERT(VARCHAR, 70017) -- Payment method = BACS
			WHEN 154490 THEN @AccountName -- Payee name
			WHEN 175270 THEN @AccNumber -- Payee account number
			WHEN 175271 THEN @AccSortCode -- Payee sort code
			WHEN 179988 THEN CONVERT(VARCHAR,@AccountID)
			END AS DetailValue
			FROM 
			@Payments p 
			INNER JOIN TableRows r WITH (NOLOCK) on r.TableRowID = p.TableRowID
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = r.MatterID and mdv.DetailFieldID = 176944
			CROSS JOIN dbo.DetailFields df WITH (NOLOCK) 
			WHERE 
			r.MatterID = @MatterID
			AND r.DetailFieldID = 154485 -- Payment row
			AND p.pType = 44233
			AND df.DetailFieldID IN (170232, 154490, 175270, 175271,179988) 
						 

		END
	END

	/* Pay (1) policyholder */ /*(5) Split claim payment - #44273, Claim form Received*/
	IF @EventTypeID IN (123460,122701,150005)
	BEGIN

		-- get existing account details	and display (in case the customer want to use those)
		SELECT @DetailValue= CASE WHEN accno.DetailValue='' 
					THEN 
						'No existing bank details'
					ELSE
					  'Account Name: ' + accn.DetailValue + '; Account Number: ' + accno.DetailValue + '; Sort Code: ' + accsc.DetailValue
					END  
		FROM LeadTypeRelationship ltrclaim WITH (NOLOCK) 
		INNER JOIN LeadTypeRelationship ltrcol WITH (NOLOCK) ON ltrclaim.FromMatterID=ltrcol.FromMatterID AND ltrcol.ToLeadTypeID=1493
		INNER JOIN MatterDetailValues accno WITH (NOLOCK) ON accno.MatterID=ltrcol.ToMatterID AND accno.DetailFieldID=170190
		INNER JOIN MatterDetailValues accsc WITH (NOLOCK) ON accno.MatterID=accsc.MatterID AND accsc.DetailFieldID=170189
		INNER JOIN MatterDetailValues accn WITH (NOLOCK) ON accno.MatterID=accn.MatterID AND accn.DetailFieldID=170188
		WHERE ltrclaim.ToLeadID=@LeadID 
		
		EXEC dbo._C00_SimpleValueIntoField 175296, @DetailValue, @MatterID, @WhoCreated

		-- execute the shared SAE
		EXEC dbo._C00_SAE_1272 @LeadEventID
		
	END	
	
	/* Pay VET ,(2),Referal vet (3),Nominated third party (4) */
	/*Added out of hours VET - SA - 2018-03-15*/
	IF @EventTYpeID IN (123461, 121863, 123462,157254)
	BEGIN 
		
		EXEC dbo._C00_SAE_1272 @LeadEventID
		
	END
	
	/* Pre-auth approved */
	IF @EventTypeID IN (150038)
	BEGIN
	
		EXEC _C00_1272_Docs_PrepareSettlementDoc @MatterID
		
	END	
	
	/* Don't allow reopen unless claim is closed */
	IF @EventTypeID IN (121870,122716)
	BEGIN

		IF (SELECT ca.ClientStatusID FROM Cases ca WITH (NOLOCK) WHERE ca.CaseID = @CaseID) NOT IN (3819,4470,4687,4688,4703,4706) /*CS 2016-03-15 added 4703,4706 for ticket #37256*/
		BEGIN
			SELECT @ErrorMessage = '<br /><br /><font color="red" size=5>You cannot reopen a claim that has not been closed.<br/></font><font color="blue" size=4>Please reconsider why you are trying to reopen this claim.</font><br/><br><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END

	END	
	
	/* Ad-hoc letter - force immediate doc zip  */
	IF @EventTypeID IN (155338)
	BEGIN 

		EXEC _C600_CreateDocumentZipsForFolders			
	
	END

	/* New claim */
	IF @EventTypeID IN (150002)
	BEGIN
		-- set customer prefs
		EXEC _C600_SAS @AqAutomation, @CustomerID, NULL

	END	
	
	IF @EventTypeID IN (150016,150178) /*Create a concatination of missing info fields*/
	BEGIN 
		
		EXEC _C600_ConcatMissingInfoFields @MatterID
		
		SELECT @DetailValue = ''
		
		;WITH MyDistinctTreatments (PlanTitle) 	
		AS
		(		
			SELECT DISTINCT  luli.ItemValue FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tdv.DetailFieldID = 144350
			INNER JOIN dbo.ResourceListDetailValues pt WITH (NOLOCK) on pt.ResourceListID = tdv.ResourceListID and pt.DetailFieldID = 146189
			INNER JOIN dbo.lookuplistitems luli WITH (NOLOCK) on luli.lookuplistitemID = pt.ValueINT
			Where tr.MatterID = @MatterID
		)
		SELECT @DetailValue += m.PlanTitle
		FROM MyDistinctTreatments m

		EXEC _C00_SimpleValueIntoField 177410,@DetailValue,@MatterID,44412

		SELECT @DetailValue = ''

		;WITH MyDistinctTreatments (TreatMentMethod) 	
		AS
		(		
			SELECT DISTINCT  luli.ItemValue FROM TableRows tr WITH (NOLOCK) 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tdv.DetailFieldID = 144350
			INNER JOIN dbo.ResourceListDetailValues pt WITH (NOLOCK) on pt.ResourceListID = tdv.ResourceListID and pt.DetailFieldID = 146190
			INNER JOIN dbo.lookuplistitems luli WITH (NOLOCK) on luli.lookuplistitemID = pt.ValueINT
			Where tr.MatterID = @MatterID
		)
		SELECT @DetailValue += m.TreatMentMethod 
		FROM MyDistinctTreatments m

		EXEC _C00_SimpleValueIntoField 177411,@DetailValue,@MatterID,44412
		
	END

	IF @EventTypeID IN (121865) /*Approve Payment*/
	BEGIN

		IF EXISTS (SELECT TOP 1* FROM Customers cu WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON Cu.CustomerID = l.CustomerID
		INNER JOIN Cases cs WITH (NOLOCK)  ON cs.LeadID = l.LeadID 
		INNER JOIN LeadEvent le WITH (NOLOCK)  ON Le.CaseID = cs.CaseID
		WHERE Le.EventTypeID = 156921 AND le.WhenCreated = 0 AND le.WhoCreated = @WhoCreated AND cu.CustomerID = @CustomerID)

		AND 
		( -- Aquarium users can approve their own claims on Dev and QA only
			0 = ( SELECT cp.IsAquarium FROM Clientpersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @WhoCreated )
			OR NOT (DB_NAME() like '%Dev' or DB_NAME() like '%QA')
		)
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>Notice: Person who has underwritten policy cannot approve payments</br></font><font color="#edf4fa">'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END
	
		--SELECT @ValueMoney =  CONVERT(MONEY,mdv.DetailValue) FROM MatterDetailValues mdv WITH (NOLOCK) where mdv.DetailFieldID = 148375 and mdv.MatterID = @MatterID  
		SELECT @ValueMoney =   tdv.ValueMoney FROM TableDetailValues tdv WITH (NOLOCK) where tdv.DetailFieldID = 154518 and tdv.MatterID = @MatterID
		
		SELECT  @DetailValue = dbo.fnNumberToText(@ValueMoney, 1) --2nd parameter =  GPB currency type ID
		
		--Convert Total Settlement figure to words
		EXEC _C00_SimpleValueIntoField 180000,@DetailValue,@MatterID, @WhoCreated
		
	END
	-- Claim row clearing Zen#32957
	IF @EventTypeID IN (155422)
	BEGIN
	
		-- This is belt and braces as the validation custom DF 175594 should prevent the event being raised
		IF EXISTS (SELECT 1 FROM LeadEvent le  WITH (NOLOCK) WHERE le.CaseID = @CaseID AND le.EventTypeID = 121865 AND le.EventDeleted = 0)
		BEGIN
		
			-- Naughty me
			SELECT @ErrorMessage = '<br /><br /><font color="red" size=5>Too late.<br/></font><font color="blue" size=4>Payment has been approved.</font><br/><br><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		
		END
		ELSE IF @EventTypeID = 155422 -- IP automated from OOP
		BEGIN
			
			-- Build comment
			SELECT @Comments = REPLACE(REPLACE(REPLACE('%1 claim row(s) and %2 payment row(s) cleared. User reason: %3',
				'%1', CONVERT(VARCHAR, SUM(CASE WHEN r.DetailFieldID = 144355 THEN 1 ELSE 0 END))),
				'%2', CONVERT(VARCHAR, SUM(CASE WHEN r.DetailFieldID = 154485 THEN 1 ELSE 0 END))),
				'%3', ISNULL(MAX(mdv.DetailValue), 'none given'))
			FROM dbo.Matter m WITH (NOLOCK)
			LEFT JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND DetailFieldID IN (144355, 154485) -- Claim rows and payment rows
			LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 162650 -- User reason
			WHERE m.ClientID = @ClientID
			AND m.MatterID = @MatterID
						
			-- Delete rows
			DELETE FROM TableRows
			WHERE ClientID = @ClientID
			AND MatterID = @MatterID
			AND DetailFieldID IN (144355, 154485) -- Claim rows and payment rows
			
			-- Write comment to LeadEvent
			UPDATE le SET Comments = @Comments
			FROM LeadEvent le WITH (NOLOCK)
			WHERE le.LeadEventID = @LeadEventID
			
			EXEC _C00_DeleteFieldForReUse @LeadEventID, 162650, @AqAutomation
			
		END	
		
	END
	
	IF @EventTypeID = 121504 /*Rather than the document writing tool we have mapped deduction reasons to a rl, this then pulls the relevant paragraph onto the letter*/
	BEGIN
	
		SELECT @EventComments = ''

		DECLARE @Rows TABLE (Reason VARCHAR(100),Amount DECIMAL (18,2), RowID INT, Line VARCHAR(MAX))
		INSERT INTO @Rows (Reason,Amount,Line, RowID)
		
		/*CPS 2019-04-16 added a space before £ for ticket #56370*/
		SELECT	 llReason.ItemValue AS Reason, tdvAmount.ValueMoney AS Amount, rldv1.DetailValue + ' £' + CAST(tdvAmount.ValueMoney * -1 AS VARCHAR(50)) + rldv2.DetailValue + CHAR(13) + CHAR(10), r.TableRowID
		FROM dbo.TableRows r WITH (NOLOCK)
		INNER JOIN dbo.TableDetailValues tdvRow WITH (NOLOCK) ON r.TableRowID = tdvRow.TableRowID AND tdvRow.DetailFieldID = 147299 /*Claim Detail TableRowID*/
		INNER JOIN dbo.TableDetailValues tdvReason WITH (NOLOCK) ON r.TableRowID = tdvReason.TableRowID AND tdvReason.DetailFieldID = 147300 /*Deduction Reason*/
		INNER JOIN dbo.LookupListItems llReason WITH (NOLOCK) ON tdvReason.ValueInt = llReason.LookupListItemID 
		INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 147301 /*Deduction Amount*/
		INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = 176950 AND rldv.ValueInt = tdvReason.ValueInt 
		INNER JOIN dbo.ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv.ResourceListID = rldv1.ResourceListID AND rldv1.DetailFieldID = 176951 /*Pre Amount Paragraph*/
		INNER JOIN dbo.ResourceListDetailValues rldv2 WITH (NOLOCK) ON rldv2.ResourceListID = rldv.ResourceListID AND rldv2.DetailFieldID = 176952 /*Post Amount Paragraph*/
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = 147302 /*Claim Detail Deductions*/
		AND r.DetailFieldPageID = 16157 /*Claim Details*/
		
		DECLARE @NewTableRowIDs TABLE ( TableRowID INT, SourceID INT )
	
		SELECT @PolMatterID =  dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID)

		SELECT @RenewalDate = dbo.fnGetSimpleDVasDate(170037,@PolMatterID)
		
		UPDATE @Rows 
		SET Line = REPLACE(Line,'{RenewalDate}',CAST(@RenewalDate AS VARCHAR(50))) 
		
		/*
		CPS 2019-04-16 for #56370
		Delete old rows in case this event is deleted and reapplied
		*/
		DELETE tr
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.DetailFieldID = 148349
		AND tr.MatterID = @MatterID
		/*!! NO SQL HERE !!*/
		SELECT @EventComments += CONVERT(VARCHAR,@@RowCount) + ' previous deduction rows cleared.' + CHAR(13)+CHAR(10)

		INSERT INTO TableRows (ClientID,MatterID,DetailFieldID,DetailFieldPageID,DenyEdit,DenyDelete,SourceID)
		OUTPUT inserted.TableRowID, inserted.SourceID
		INTO @NewTableRowIDs (TAbleRowID, SourceID )
		SELECT @ClientID,@MatterID,148349,16188,1,1,r.RowID 
		FROM @Rows r 
		/*!! NO SQL HERE !!*/
		SELECT @EventComments += CONVERT(VARCHAR,@@RowCount) + ' new deduction rows added.' + CHAR(13)+CHAR(10)
		
		INSERT INTO TableDetailValues (ClientID, MatterID,DetailValue,DetailFieldID,TableRowID) 
		SELECT @ClientID,@MatterID,r.Line,148348, tr.TableRowID  
		FROM @Rows r
		INNER JOIN @NewTableRowIDs tr ON tr.SourceID = r.RowID 

		/*Add a note to list the rows that have been added*/
		SELECT @EventComments += r.Reason + ' (' + CONVERT(VARCHAR,tr.TableRowID) + ')' + CHAR(13)+CHAR(10)
		FROM @Rows r 
		INNER JOIN @NewTableRowIDs tr ON tr.SourceID = r.RowID 
		
		IF EXISTS (SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID = @CaseID 
				   AND le.EventTypeID IN (150191,157578,157577))		   
		BEGIN 
			EXEC _C00_SimpleValueIntoField    176953,'true',@MatterID, @WhoCreated 
		END
		ELSE
		BEGIN
			EXEC _C00_SimpleValueIntoField   176953,'false',@MatterID, @WhoCreated 
		END

		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

	END
		
	/* SYS Queue Claims Payment */
	IF @EventTypeID IN (155212)
	BEGIN
	
		EXEC _C600_PA_QueueClaimPayment @LeadEventID, @MatterID 	
		
	END

	/* begin - Change Payment Interval */
	IF @EventTypeID IN (156901)
	BEGIN
		
		declare @NewInterval INT
		EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 156884, -1
		
		-- makes sure this makes sense
		SELECT @NewInterval=dbo.fnGetDvAsInt(177433,@CaseID)
		
		IF @NewInterval = dbo.fnGetDvAsInt(170176,@CaseID)
		BEGIN
		
			RAISERROR('<br /><br /><font color="red">The new payment interval is the same as the existing payment interval.</font><br/><br/><font color="#edf4fa">',16,1)
				RETURN
		
		END
		ELSE
		BEGIN
		
			EXEC dbo._C00_SimpleValueIntoField 170176, @NewInterval, @MatterID
		
		END

		/*If we are changing this to a card payer, so no account details to enter*/ 
		/*If it's a DD then an event choice will be opened*/ 
		IF EXISTS (SELECT * FROM Matter m with (NOLOCK) 
		INNER JOIN MatterDetailValues mdv with (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170176
		AND mdv.ValueINT = 69944
		AND m.MatterID = @MatterID)
		BEGIN 

			INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
			SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, @CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156834, @AqAutomation, 0, 5, 0, 5
			FROM Cases c WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
			WHERE c.CaseID = @CaseID

		END

		/*UAH 07/11/2018 - As there is only 1 MTA Workflow group, I am adding SAE on all events which relate to 
		MTA's to remove workflow tasks once these MTA events have been applied*/

		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) where w.caseid = @caseID
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, 
		@WhoCreated,dbo.fn_GetDate_Local(),'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE CaseID = @CaseID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 
		AND WorkflowGroupID = 1239

	END	

	/*GPR 2020-05-19 removed for JIRA AAG-743, just change the interval*/
	/*/*Payment interval Changed*/ 
	IF @EventTypeID IN (156834) 
	BEGIN 
		
		/*Check what we are changing to*/ 
		SELECT @ValueInt = dbo.fnGetSimpleDvAsInt(170176,@MatterID) 

		IF @ValueInt = 69943 /*Monthly*/ 
		BEGIN 
			/*Payment Method*/ 
			EXEC _C00_SimpleValueIntoField  170114,69930,@MatterID,@WhoCreated 

		END
		ELSE 
		BEGIN /*Annual card*/ 

			/*Payment Method*/ 
			EXEC _C00_SimpleValueIntoField  170114,69931,@MatterID,@WhoCreated 

			/*Check if there is already a card account in force for this customer*/ 
			SELECT @AccountID = a.AccountID FROM Account a WITH (NOLOCK) 
			where a.CustomerID = @CustomerID 
			AND a.AccountTypeID = 2  
			AND a.AccountUseID = 1 
			
			IF ISNULL(@AccountID,0) = 0 
			BEGIN 
			
				/*Pick up the affinity specific client account ID to write to the customers account record*/ 
				SELECT @ClientAccountID = rldv.ValueInt 
				FROM CustomerDetailValues cdv WITH (NOLOCK) 
				INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
				WHERE cdv.CustomerID = @CustomerID
				AND cdv.DetailFieldID = 170144
				AND rldv.DetailFieldID = 179949

						/*No then Make a card account*/ 
				/*JEL -- assign the account to the collections Matter, not the policy. So we need to create a collections matter*/ 
				SELECT @ColLeadID=LeadID FROM Lead WITH (NOLOCK) WHERE CustomerID=@CustomerID AND LeadTypeID=1493
				EXEC @ColMatterID = _C00_CreateNewCaseForLead @ColLeadID, @WhoCreated, 1

				EXEC @NewAccountID = Billing__CreatePremiumAccountAccountRecord @ClientID, @ColMatterID, 2, NULL, NULL, 'Account For Card Prepayment', @ClientAccountID, @CustomerID, @WhoCreated, 2
			
				EXEC _C00_SimpleValueIntoField 180219,@NewAccountID,@MatterID,@WhoCreated 

				/*If we have already created the renewal Policy term then we need to regen this*/
				SELECT @PurchasedProductID = p.PurchasedProductID FROM PurchasedProduct p with (NOLOCK) 
				WHERE p.ObjectID = @MatterID 
				AND p.ValidFrom > dbo.fn_GetDate_Local() 

				IF ISNULL(@PurchasedProductID,0) <> 0 
				BEGIN 

					/*Delete old PPPS for Renewal term*/
					DELETE FROM PurchasedProductPaymentSchedule 
					where PurchasedProductID = @PurchasedProductID

					Update PurchasedProduct
					SET NumberOfInstallments = CASE @ValueInt WHEN 69943 THEN  12 ELSE 1 END, PaymentFrequencyID =  CASE @ValueInt WHEN 69943 THEN  4 ELSE 5 END , AccountID = @NewAccountID
					where PurchasedProductID = @PurchasedProductID
	
					EXEC [PurchasedProduct__CreatePaymentSchedule] @PurchasedProductID 

				END  

			END   			

		END 

	END*/

	/*Change Payment Interval - Add Bacs Details*/ 
	IF @EventTypeID IN (158897) 
	BEGIN 

		SELECT @AccNumber = dbo.fnGetSimpleDV(170106,@MatterID) 
		SELECT @AccSortCode = dbo.fnGetSimpleDV(170105,@MatterID) 
		SELECT @AccountName = dbo.fnGetSimpleDV(180070,@MatterID) 
		SELECT @ValueINT = dbo.fnGetSimpleDV(170176,@MatterID) 

		/*Pick up the affinity specific client account ID to write to the customers account record*/ 
		SELECT @ClientAccountID = rldv.ValueInt 
		FROM CustomerDetailValues cdv WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
		WHERE cdv.CustomerID = @CustomerID
		AND cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 179949 /*Collections AccountID*/

		/*No then Make a card account*/ 
		/*JEL -- assign the account to the collections Matter, not the policy. So we need to create a collections matter*/ 
		SELECT @ColLeadID = LeadID 
		FROM Lead WITH (NOLOCK) 
		WHERE CustomerID = @CustomerID 
		AND LeadTypeID = 1493

		EXEC @ColMatterID = _C00_CreateNewCaseForLead @ColLeadID, @WhoCreated, 1

		/*PL 2019-08-08 Get the CaseID for the Collection MatterID #58552*/
		SELECT @ColCaseID = m.CaseID 
		FROM Matter m WITH (NOLOCK)
		WHERE m.MatterID = @ColMatterID

		EXEC @NewLeadEventID = _C00_AddProcessStart @ColCaseID, @WhoCreated, 150056 /*PL 2019-08-08 Add process start for new case. #58552*/

		EXEC @NewAccountID = Billing__CreatePremiumAccountAccountRecord  @ClientID, @ColMatterID, 2, @AccNumber, @AccSortCode, @AccountName, @ClientAccountID, @CustomerID, @WhoCreated, 1
		
		/*PL 2019-08-08 Get the account reference from the new account #58552*/
		SELECT TOP 1 @NewAccountReference = a.Reference
		FROM Account a WITH (NOLOCK)
		WHERE a.AccountID = @NewAccountID

		EXEC _C00_SimpleValueIntoField 180219,@NewAccountID,@MatterID,@WhoCreated

		/*PL 2019-08-08 Insert new account information into new collection matter #58552*/
		EXEC _C00_SimpleValueIntoField 176973,@NewAccountID,@ColMatterID,@WhoCreated /*Billing System Account ID*/
		EXEC _C00_SimpleValueIntoField 170115,69930,@ColMatterID,@WhoCreated /*Payment Method*/
		EXEC _C00_SimpleValueIntoField 180262,@MatterID,@ColMatterID,@WhoCreated /*Account Linked to Policy Matter ID*/
		EXEC _C00_SimpleValueIntoField 170188,@AccountName,@ColMatterID,@WhoCreated /*Account Name*/
		EXEC _C00_SimpleValueIntoField 170189,@AccSortCode,@ColMatterID,@WhoCreated /*Account Sort Code*/
		EXEC _C00_SimpleValueIntoField 170190,@AccNumber,@ColMatterID,@WhoCreated /*Account Number*/
		EXEC _C00_SimpleValueIntoField 175460,@NewAccountReference,@ColMatterID,@WhoCreated /*Account Reference*/

		/*If we have already created the renewal Policy term then we need to regen this*/
		SELECT @PurchasedProductID = p.PurchasedProductID FROM PurchasedProduct p with (NOLOCK) 
		WHERE p.ObjectID = @MatterID 
		AND p.ValidFrom > dbo.fn_GetDate_Local() 

		IF ISNULL(@PurchasedProductID,0) <> 0 
		BEGIN 

			/*Set the Payment Day if it's bacs*/ 
			SELECT @PaymentDay = CASE dbo.fngetsimpledv(170168,@MatterID) WHEN 0 THEN 1 ELSE dbo.fngetsimpledv(170168,@MatterID) END 
		

			/*Delete old PPPS for Renewal term*/
			DELETE FROM PurchasedProductPaymentSchedule 
			where PurchasedProductID = @PurchasedProductID

			Update PurchasedProduct
			--SET NumberOfInstallments = CASE @ValueInt WHEN 69943 THEN  12 ELSE 1 END, PaymentFrequencyID =  CASE @ValueInt WHEN 69943 THEN  4 ELSE 5 END , AccountID = @NewAccountID, PreferredPaymentDay = @PaymentDay
			SET NumberOfInstallments = CASE @ValueInt
										WHEN 69943 THEN 12
										WHEN 293321 THEN 26
										ELSE 1 END,
										PaymentFrequencyID = CASE @ValueInt
										WHEN 69943 THEN 4
										WHEN 293321 THEN 7
										ELSE 5 END , AccountID = @NewAccountID, PreferredPaymentDay = @PaymentDay
			where PurchasedProductID = @PurchasedProductID
	
			EXEC [PurchasedProduct__CreatePaymentSchedule] @PurchasedProductID 

		END

		EXEC _C00_ApplyLeadEventByAutomatedEventQueue @NewLeadEventID, 150180, @WhoCreated

		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
		SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, @CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 156834, @AqAutomation, -1, 5, 0, 5
		FROM Cases c WITH (NOLOCK)
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
		WHERE c.CaseID = @CaseID

	END 

	/* begin - Change Payment Day */
	IF @EventTypeID IN (156762)
	BEGIN

		-- get wait date
		SELECT @WaitDate=dbo.fn_C600_GetWaitDate (@MatterID, 2)
		
		IF dbo.fnGetDvAsDate(175449,@CaseID) < @WaitDate
			AND 5144 <> dbo.fnGetDvAsInt(175450,@CaseID)
		BEGIN
		
			RAISERROR('<br /><br /><font color="red">You cannot use the override date if it within the grace period for changes to the regular payment date and you have not confirmed that you have the customer''s consent.</font><br/><br/><font color="#edf4fa">',16,1)
				RETURN
		
		END

		-- cannot adjust payment date to within the mandate wait period
		IF dbo.fnGetDvAsDate(175449,@CaseID) < dbo.fn_C600_GetWaitDate (@MatterID, 4)
		BEGIN
		
			RAISERROR('<br /><br /><font color="red">You cannot bring the date of next payment forward because it is during the mandate waiting period.</font><br/><br/><font color="#edf4fa">',16,1)
				RETURN
		
		END

		-- cannot push the payment date back by more than 1 month
		IF dbo.fnGetDvAsDate(175449,@CaseID) > DATEADD(MONTH,1,dbo.fn_GetDate_Local())
		BEGIN
		
			RAISERROR('<br /><br /><font color="red">You cannot push the date of next payment back by more than 1 month.</font><br/><br/><font color="#edf4fa">',16,1)
				RETURN
		
		END

		EXEC @MTARequired = _C600_UpdatePHOrPetDetails @LeadEventID, 3		
	
		-- apply 'payment day changed' notification out event opening another thread 
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
		SELECT TOP(1) c.ClientID, l.CustomerID, l.LeadID, @CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 155275, @AqAutomation, -1, 5, 0, 5
		FROM Cases c WITH (NOLOCK)
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.EventDeleted = 0 AND le.LeadEventID = c.LatestNonNoteLeadEventID
		WHERE c.CaseID = @CaseID

		-- update letter text
		EXEC _C600_UpdateLetterTextField 175462, 'PDCHANGEDAY', @MatterID
		
		-- update schedules.  Done by billing system - this event with subtype Ledger Transaction
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4522),
				@DetailValue = dbo.fnGetDv(170248,@CaseID)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @DetailValue, @MatterID -- new payment day
		
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4523),
				@DetailValue = dbo.fnGetDv(175449,@CaseID)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @DetailValue, @MatterID -- override next payment date
		
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4413),
				@DetailValue = dbo.fnGetDv(177074,@CaseID)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @DetailValue, @MatterID -- purchased product id
		
		-- clear override date & permission
		EXEC dbo._C00_SimpleValueIntoField 175449, '', @MatterID
		EXEC dbo._C00_SimpleValueIntoField 175450, '', @MatterID
		EXEC dbo._C00_SimpleValueIntoField 170248, '', @CustomerID


		/*UAH 07/11/2018 - As there is only 1 MTA Workflow group, I am adding SAE on all events which relate to 
		MTA's to remove workflow tasks once these MTA events have been applied*/

		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) where w.caseid = @caseID
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)

		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, @WhoCreated, dbo.fn_GetDate_Local(), 'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE CaseID = @CaseID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 
		AND WorkflowGroupID = 1239 


	END	

	/* begin - Change Account Details */
	IF @EventTypeID IN (156850/*Collections*/,156847/*collections from in process*/)
	BEGIN

		IF ( SELECT c.ClientStatusID FROM Cases c WITH (NOLOCK) WHERE c.CaseID = @CaseID ) = 4709 /*Inactive*/
		BEGIN
			SELECT @ErrorMessage = '<br><br><font color="red">Error: You may not apply this event to an Inactive account.  Please select an Active account matter and try again.</font>'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END

		/*JEL, 2018-09-24 removed from the end of the process as the details were being cleared off prior to the letter being generated*/ 
		IF @EventTypeID IN (156850,156850)
		BEGIN 
		
			EXEC dbo._C00_SimpleValueIntoField 175363, '', @MatterID
			EXEC dbo._C00_SimpleValueIntoField 170251, '', @CustomerID
			EXEC dbo._C00_SimpleValueIntoField 175382, '', @MatterID
			EXEC dbo._C00_SimpleValueIntoField 170252, '', @CustomerID
			EXEC dbo._C00_SimpleValueIntoField 177313, '', @MatterID
			EXEC dbo._C00_SimpleValueIntoField 170255, '', @CustomerID
		
		END
		
		-- check that the BS accountID entered belongs to this customer!
		SELECT @AccountID=dbo.fnGetDvAsInt(177312,@CaseID) -- use existing account
		IF @AccountID > 0 AND
			NOT EXISTS ( SELECT * FROM MatterDetailValues accid WITH (NOLOCK)
						INNER JOIN Matter m WITH (NOLOCK) ON accid.MatterID=m.MatterID 
						WHERE  accid.DetailFieldID=176973 
							AND accid.ValueInt=@AccountID
							AND m.CustomerID=@CustomerID
					  )
		BEGIN
		
			RAISERROR('<br /><br /><font color="red">The billing system account number you entered does not belong to this customer</font><br/><br/><font color="#edf4fa">',16,1)
			RETURN
		
		END
		-- check that the BS account is acitve
		IF @AccountID > 0 AND
			NOT EXISTS ( SELECT * FROM Account WITH (NOLOCK) WHERE AccountID=@AccountID AND Active=1)
		BEGIN
		
			SELECT @ErrorMessage='<br /><br /><font color="red">You cannot use billing system account number ' + CAST(@AccountID AS VARCHAR) + ' it is no longer active.</font><br/><br/><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		
		END
		
		-- check that the BS account is acitve
		IF @AccountID > 0 AND
			NOT EXISTS ( SELECT * FROM MatterDetailValues mdv WITH (NOLOCK) WHERE mdv.DetailFieldID = 176973 AND mdv.ValueInt = @AccountID)
		BEGIN
		
			SELECT @ErrorMessage='<br /><br /><font color="red">You cannot use billing system account number ' + CAST(@AccountID AS VARCHAR) + ' as this is the account associated with this collections matter.</font><br/><br/><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		
		END
		
		-- if this is a PA call, only allow if the account is in a normal state (no outstanding payments or mandate fails)
		-- check for non-deleted notes
		IF @EventTypeID=156765
		BEGIN
		
			IF EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) 
						INNER JOIN Matter m WITH (NOLOCK) ON le.CaseID=m.CaseID
						INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON m.MatterID=ltr.ToMatterID AND ltr.FromLeadTypeID=1492
						WHERE le.NoteTypeID IN (865,905) 
							AND le.EventDeleted=0
							AND ltr.FromMatterID=@MatterID
					  )
			BEGIN
			
			RAISERROR('<br /><br /><font color="red">The account you want to change has a collections issue. You must update the account details using the collections lead.</font><br/><br/><font color="#edf4fa">',16,1)
			RETURN
			
			END
			
		END
		-- if this is a user applied event, at least one of 'existing account to use' or 'type of account to create' must be completed
		IF @EventTypeID IN (156850) AND dbo.fnGetDv(177312,@CaseID)='' AND  dbo.fnGetDv(170246,@CaseID)=''
		BEGIN 
			
			RAISERROR('<br /><br /><font color="red">You must complete either ''Use existing account'' or ''Account Type'' </font><br/><br/><font color="#edf4fa">',16,1)
			RETURN
		
		END

		-- morph OOP events to IP
		IF @EventTypeID=156765  -- pol admin 
			EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 156843, -1
		IF @EventTypeID=156850  -- collections 
			EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 156847, -1
		
		-- if this is a user added event on the collections process
		-- update mandate status to 'failed' with reason 'ph request to use different account'
		IF @EventTypeID=156850
			EXEC dbo._C00_SimpleValueIntoField 170101, 69919, @MatterID, @WhoCreated -- failed	
	
	END

	IF @EventTypeID = 156879 /* aq 1,3,156879 -- this doesn't exist on 605 */
	BEGIN 
			EXEC _C600_DeleteANote @CaseID, 865
			-- clear overdue payment fields
			EXEC dbo._C00_SimpleValueIntoField 177324, 0, @MatterID, @AqAutomation	
			-- note now collecting on another account	
			EXEC dbo._C00_SimpleValueIntoField 177325, 5144, @MatterID, @AqAutomation	
			EXEC _C600_DeleteANote @CaseID, 923	
			EXEC _C600_DeleteANote @CaseID, 905

		-- are we going to use an existing account
		SELECT @ColMatterID=dbo.fn_C600_GetCollectionsMatterID(@MatterID)
				
		-- create a new account/collections case

		EXEC @NewMatterID=dbo._C600_CreateACollectionsCase @LeadEventID
		SELECT @AccountID=accid.ValueInt 
		FROM MatterDetailValues accid WITH (NOLOCK) 
		WHERE accid.MatterID=@NewMatterID AND accid.DetailFieldID=176973
		
		UPDATE LeadEvent 
		SET Comments='Billing system payment account changed from ' + CAST(@OldAccountID AS VARCHAR) + ' to ' + CAST(@AccountID AS VARCHAR) + '.'
		WHERE LeadEventID=@LeadEventID
	
	END

	/* Change account details */
	IF @EventTypeID IN (156848 /*Change account details -- collections*/) 
	BEGIN

		-- if this is a collections call, clear mandate failed banner and overdue fields
		IF @EventTypeID IN (156848)
		BEGIN
			
			/*Track the PA Matter this used to service */ 
			SELECT @PAMatterIDFromCollectionsMatterID = dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@MatterID)
			
			/*update the payment method on PA before we delete it later*/ /*NG 2020-09-28 payment details update*/
			SELECT @NewMethod = CASE dbo.fnGetDv(170246,@CaseID) WHEN 74474 THEN 69930 ELSE 69931 END /*different lookup lists used. obviously.*/
			
			SELECT @PolAdminMatterID = mdv.ValueInt
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			WHERE mdv.DetailFieldID = 180262
			AND mdv.MatterID = @MatterID
			
			EXEC _C00_SimpleValueIntoField 170114, @NewMethod, @PolAdminMatterID, @AqAutomation

			--EXEC _C00_SimpleValueIntoField  180262, @PAMatterIDFromCollectionsMatterID, @matterID, @WhoCreated    /*PL - Adding the Policy Admin Matter ID to the Collections Matter to retain link - #55342*/
		

			EXEC _C600_DeleteANote @CaseID, 865
			-- clear overdue payment fields
			EXEC dbo._C00_SimpleValueIntoField 177324, 0, @MatterID, @AqAutomation	
			-- note now collecting on another account	
			EXEC dbo._C00_SimpleValueIntoField 177325, 5144, @MatterID, @AqAutomation	
			EXEC dbo._C00_SimpleValueIntoField 176895, 5144, @MatterID, @AqAutomation	
			EXEC _C600_DeleteANote @CaseID, 923	
			EXEC _C600_DeleteANote @CaseID, 905

		END

		-- are we going to use an existing account
		SELECT @AccountID=dbo.fnGetDvAsInt(177312,@CaseID),
				@ColMatterID=dbo.fn_C600_GetCollectionsMatterID(@MatterID)
	
		SELECT @OldAccountID=accid.ValueInt 
		FROM MatterDetailValues accid WITH (NOLOCK) 
		WHERE accid.DetailFieldID=176973
			AND accid.MatterID=@ColMatterID
		
		/*CR 2019-07-02 To populated the Detailfields so the Client is able to advise the customer of the next payment amount and the date of payment after the update of the account details  */
		SELECT	@NextPaymentDate=CONVERT(VARCHAR(10),ppps.ActualCollectionDate,120), 
				@NextPaymentAmount=SUM(ppps.PaymentGross)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		INNER JOIN dbo.PurchasedProduct p WITH (NOLOCK) on p.CustomerID= ppps.CustomerID
		WHERE (ppps.PaymentStatusID IN (1,5,7) OR PaymentStatusID IS NULL)
		AND ppps.CustomerLedgerID IS NULL
		AND ppps.AccountID=@OldAccountID
		AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
						 WHERE (ppps2.PaymentStatusID IN (1,5,7) OR PaymentStatusID IS NULL) 
							AND ppps2.CustomerLedgerID IS NULL  
							AND ppps2.ActualCollectionDate < ppps.ActualCollectionDate
							AND ppps2.AccountID=ppps.AccountID )
		GROUP BY ppps.ActualCollectionDate
		
		EXEC _C00_SimpleValueIntoField 177009, @NextPaymentAmount, @ColMatterID, @whoCreated		-- @NextPaymentAmount
		EXEC _C00_SimpleValueIntoField 177010, @NextPaymentDate, @ColMatterID, @WhoCreated			-- @NextPaymentDate

		
		IF (@AccountID > 0 OR @ClientID NOT IN (605, 607)) /*GPR 2020-09-10 Moved to [dbo].[_C600_OneInc_HandleXMLResponse] for PPET-125*/ /*NG 2020-09-29 amended so existing account doesn't trigger OneInc*/
		BEGIN	
			-- create a new account/collections case
			IF @AccountID = 0
			BEGIN

				EXEC @NewMatterID=dbo._C600_CreateACollectionsCase @LeadEventID
				SELECT @AccountID=accid.ValueInt 
				FROM MatterDetailValues accid WITH (NOLOCK) 
				WHERE accid.MatterID=@NewMatterID AND accid.DetailFieldID=176973
						
				/*If we are creating a new account record, do we have authority?*/ 
				SELECT @ValueInt = ISNULL(dbo.fnGetSimpleDvAsInt(179922,@MatterID),5144)
				EXEC _C00_SimpleValueIntoField 179907,@ValueInt,@NewMatterID
				/*Clear value for reuse*/ 
				EXEC _C00_SimpleValueIntoField 179922,'',@NewMatterID
			
			END
			ELSE
			BEGIN
		
				-- get existing account matterID & also put bank account number & sort code into temp fields 
				-- so bank val doesn't fail if this a bank val event
				SELECT @NewMatterID=accid.MatterID, @AccNumber=accno.DetailValue, @AccSortCode=sc.DetailValue 
				FROM MatterDetailValues accid WITH (NOLOCK) 
				INNER JOIN MatterDetailValues accno WITH (NOLOCK) ON accid.MatterID=accno.MatterID AND accno.DetailFieldID=170190
				INNER JOIN MatterDetailValues sc WITH (NOLOCK) ON accid.MatterID=sc.MatterID AND sc.DetailFieldID=170189
				WHERE accid.DetailFieldID=176973 
					AND accid.ValueInt=@AccountID

				EXEC dbo._C00_SimpleValueIntoField 170251, @AccSortCode, @CustomerID
				EXEC dbo._C00_SimpleValueIntoField 170252, @AccNumber, @CustomerID

			END
		
		
		UPDATE LeadEvent 
		SET Comments='Billing system payment account changed from ' + CAST(@OldAccountID AS VARCHAR) + ' to ' + CAST(@AccountID AS VARCHAR) + '.'
		WHERE LeadEventID=@LeadEventID

		-- find purchased products to update and Matter IDs to relink
		IF @EventTypeID IN (156844) -- policy admin events just the matter we were called with
		BEGIN
			INSERT INTO @AccountUpdates ( PurchasedProductID, PolMatterID, NewColMatterID, Done )
			SELECT ppid.ValueInt, ppid.MatterID, @NewMatterID, 0 
			FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN MatterDetailValues ppid WITH (NOLOCK) ON ltr.FromMatterID=ppid.MatterID AND ppid.DetailFieldID=177074
			WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
		END
		ELSE -- all policies connected to the old collections matter
		BEGIN
			INSERT INTO @AccountUpdates (  PurchasedProductID, PolMatterID, NewColMatterID, Done )
			SELECT ppid.ValueInt, ppid.MatterID, @NewMatterID, 0 
			FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			INNER JOIN MatterDetailValues ppid WITH (NOLOCK) ON ltr.FromMatterID=ppid.MatterID AND ppid.DetailFieldID=177074
			WHERE ltr.ToMatterID=@ColMatterID AND ltr.ToLeadTypeID=1493
		END

		-- do the update and relink the matters
		WHILE EXISTS ( SELECT * FROM @AccountUpdates WHERE Done=0 )
		BEGIN
		
			SELECT TOP 1 @NewMatterID=NewColMatterID, @PurchasedProductID=PurchasedProductID, @PolMatterID=PolMatterID FROM @AccountUpdates WHERE Done=0

			-- relink matterID
			UPDATE LeadTypeRelationship
			SET ToMatterID=@NewMatterID
			WHERE FromMatterID=@PolMatterID AND ToLeadTypeID=1493

			/*#430 Copy over the authority to use account..*/
			SELECT @AuthoriryToUseAccount = dbo.fnGetSimpleDvAsInt(179922,@MatterID)

			EXEC dbo._C00_SimpleValueIntoField 179904,@AuthoriryToUseAccount, @PolMatterID, @WhoCreated

			-- set up third party fields
			SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 1492, 105, 4526)
			EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AccountID, @PolMatterID -- New Account Number
			
			SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 1492, 105, 4413)
			EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PurchasedProductID, @PolMatterID -- purchased product id
			
			-- if this is a collections lead we'll need to pass a PA LeadEventID because that's where the third party fields live
			IF @EventTypeID=156848
			BEGIN
			
				SELECT TOP 1 @PALeadEventID=ca.LatestInProcessLeadEventID, @PACaseID=ca.CaseID 
				FROM Cases ca WITH (NOLOCK) 
				INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID
				WHERE m.MatterID=@PolMatterID
			
			END
			ELSE
			BEGIN
			
				SELECT @PALeadEventID=@LeadEventID, @PACaseID=@CaseID 
			
			END
			
			EXEC dbo.ChangePaymentAccount_CreateFromThirdPartyFields @ClientID, @PACaseID, @PALeadEventID
			
			EXEC _C600_Collections_RefreshDocumentationFields @NewMatterID
			
			UPDATE @AccountUpdates SET Done=1 WHERE PurchasedProductID=@PurchasedProductID
		
		END
		END

		-- update original collections documentation fields
		EXEC _C600_Collections_RefreshDocumentationFields @MatterID

		-- update letter text if block pa or collections -- need new pa df 
		IF @EventTypeID=156844 -- pa
		BEGIN
			EXEC _C600_UpdateLetterTextField 123, 'PDCHANGEACCOUNT', @MatterID
		END
		--ELSE -- collections
		--	EXEC _C600_UpdateLetterTextField 175462, 'PDCHANGEACCOUNT', @NewMatterID
		
		-- make sure current account details user listing is up to date
		EXEC @MTARequired = _C600_UpdatePHOrPetDetails @LeadEventID, 3, 1		
		
		-- clear all temporary fields
		EXEC dbo._C00_SimpleValueIntoField 177312, '', @CustomerID
		EXEC dbo._C00_SimpleValueIntoField 170246, '', @CustomerID	
		
		--EXEC _C00_SimpleValueIntoField  180262, @PAMatterIDFromCollectionsMatterID, @NewMatterID, @WhoCreated 	
		
	END	

/*  ====================================================== ALL LEAD TYPES ======================================== */

	/* Set Communication/Marketing Preferences */
	IF @EventTypeID IN (150195)
	BEGIN
		/* 2018-03-12 Mark Beaumont - set fields dependent on "Send documents by" */
		DECLARE	@IsElectronicDocuments INT
		
		SELECT		@IsElectronicDocuments = CASE WHEN cdv.ValueInt = 60789		-- Electronic documents
												THEN 5144	-- Yes
												ELSE 5145 	-- No
												END
		FROM		CustomerDetailValues cdv WITH (NOLOCK)
		WHERE		cdv.CustomerID = @CustomerID
					AND cdv.DetailFieldID = 170257		-- Send documents by
					
		UPDATE		cdv
		SET			cdv.DetailValue = @IsElectronicDocuments 
		FROM		CustomerDetailValues cdv
		WHERE		cdv.CustomerID = @CustomerID
					AND cdv.DetailFieldID = 170259		-- Send customer care emails
					
		UPDATE		cdv
		SET			cdv.DetailValue = @IsElectronicDocuments 
		FROM		CustomerDetailValues cdv
		WHERE		cdv.CustomerID = @CustomerID
					AND cdv.DetailFieldID = 170260		-- Send customer care SMSs
					
		/* 2018-03-12 Mark Beaumont - set "Customer allows Marketing" dependent on marketing preferences */
		DECLARE	@IsSMSMarketingTarget BIT,
				@IsEmailMarketingTarget BIT,
				@IsPostMarketingTarget BIT,
				@IsPhoneMarketingTarget BIT
		
		SELECT		@IsSMSMarketingTarget = cdv_SMS.ValueInt,
					@IsEmailMarketingTarget = cdv_Email.ValueInt,
					@IsPostMarketingTarget = cdv_Post.ValueInt,
					@IsPhoneMarketingTarget = cdv_Phone.ValueInt
		FROM		Customers c WITH (NOLOCK)
					INNER JOIN CustomerDetailValues cdv_SMS WITH (NOLOCK) ON c.CustomerID = cdv_SMS.CustomerID
						AND cdv_SMS.DetailFieldID = 177152		-- Customer allows Marketing by SMS	
					INNER JOIN CustomerDetailValues cdv_Email WITH (NOLOCK) ON c.CustomerID = cdv_Email.CustomerID
						AND cdv_Email.DetailFieldID = 177153	-- Customer allows Marketing by Email	
					INNER JOIN CustomerDetailValues cdv_Post WITH (NOLOCK) ON c.CustomerID = cdv_Post.CustomerID
						AND cdv_Post.DetailFieldID = 177154		-- Customer allows Marketing by Post	
					INNER JOIN CustomerDetailValues cdv_Phone WITH (NOLOCK) ON c.CustomerID = cdv_Phone.CustomerID
						AND cdv_Phone.DetailFieldID = 177371	-- Customer allows Marketing by Phone	
		WHERE		c.CustomerID = @CustomerID
					AND c.ClientID = @ClientID
					
		UPDATE		cdv
		SET			cdv.DetailValue = CASE WHEN @IsSMSMarketingTarget = 0 AND 
												@IsEmailMarketingTarget = 0 AND 
												@IsPostMarketingTarget = 0 AND 
												@IsPhoneMarketingTarget = 0
										THEN 5145 	-- No
										ELSE 5144	-- Yes
										END
		FROM		CustomerDetailValues cdv
		WHERE		cdv.CustomerID = @CustomerID
					AND cdv.DetailFieldID = 176970		-- Customer allows Marketing

		/*ALM 2020-07-03 AAG-980*/
		SELECT @OneVisionID = OneVisionID FROM OneVision ov WITH (NOLOCK) WHERE ov.PAMatterID = @MatterID AND ov.PolicyTermType = 'Policy'

		IF @OneVisionID IS NOT NULL 
		BEGIN
			EXEC _C600_Mediate_QueueForVision 'Customer', 'PUT', @LeadEventID, @OneVisionID
		END
	END	
	
	/*CW Adding event group 18 to prevent certain events going out to customer if they have been locked by sanctions */
	IF EXISTS (SELECT * FROM EventGroupMember egm WITH (NOLOCK) WHERE egm.EventGroupID = 18 AND egm.EventTypeID = @EventTypeID)
	BEGIN
		/*First make sure all other events in the group have ETS*/
		INSERT EventTypeSql 
		SELECT @ClientID,egm.EventTypeID,'EXEC dbo._C600_SAE @LeadEventID',0 
		FROM EventGroupMember egm WITH (NOLOCK)
		LEFT JOIN dbo.EventTypeSql ets WITH (NOLOCK) ON ets.EventTypeID = egm.EventTypeID
		WHERE egm.EventGroupID = 18
		AND ets.EventTypeID IS NULL
		
		/*Check if customer has sanctions date*/
		
		IF EXISTS (SELECT * FROM CustomerDetailValues cdv WITH (NOLOCK) WHERE cdv.DetailFieldID = 178142 AND cdv.ValueDate <> '' AND cdv.CustomerID = @CustomerID)
		BEGIN
		SELECT @ValueDate = cdv.ValueDate FROM CustomerDetailValues cdv WITH (NOLOCK) WHERE cdv.DetailFieldID = 178142 AND cdv.ValueDate <> '' AND cdv.CustomerID = @CustomerID
		
		SELECT @ErrorCode = '<br /><br /><font color="red">Event cannot be added as customer is currently under sanctions screening. Customer was registered under sanctions screening on ' + CAST(@ValueDate AS VARCHAR(MAX)) + '. If you beleive
		this to be in error, add the event Unlock policy - Compliance to remove this lock.</font><br/><br/><font color="#edf4fa">'
		
		RAISERROR(@ErrorCode,16,1)
		
		END
		
	END

	/*set status to underwrting*/
	IF @EventTypeID IN (156910) /*Begin - Underwriter process*/
	BEGIN
		EXEC dbo._C00_SimpleValueIntoFieldLuli 170038,'With Underwriting',@MatterID /*set policy status to live*/
 	END
	
	/*Clear out UW check box once UW process completed and update new status*/	
	/*process completed*/ 
	IF @EventTypeID IN (156921) /*UW: Process completed*/
	BEGIN
		SELECT @AccountID = pp.AccountID
		FROM PurchasedProduct pp WITH (NOLOCK) 
		WHERE pp.PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(@MatterID,@WhenCreated)
	
		/*CPS 2017-08-24*/
		EXEC _C600_BringUncollectedScheduleItemsUpToDate @MatterID, @LeadEventID, @WhenCreated, @AllPetsForCustomer = 1
		EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'NewBusiness'
	
		EXEC dbo._C00_SimpleValueIntoField 177473,'false',@LeadID /*clear out underwriting status*/
		EXEC dbo._C00_SimpleValueIntoFieldLuli 170038,'Live',@MatterID /*set policy status to live*/
 	END
	
	/*Clear out UW check box once UW process completed*/
	
	/*cancelled*/
	IF @EventTypeID IN (156919) /*UW: Cancel - pre-existing uninsurable*/
	BEGIN
		EXEC dbo._C00_SimpleValueIntoField 177473,'false',@LeadID /*clear out underwriting status*/
		EXEC dbo._C00_SimpleValueIntoFieldLuli 170038,'Cancelled',@MatterID /*set policy status to live*/
		
		/*CPS 2017-08-24*/
		EXEC _C600_BringUncollectedScheduleItemsUpToDate @MatterID, @LeadEventID, @WhenCreated, @AllPetsForCustomer = 1
		EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'NewBusiness'		
 	END

	/*Passed DPA- remove lock - if present*/
	IF @EventTypeID = 157573 /*DPA Passed - Remove lock*/
	BEGIN
	
		UPDATE ClientPersonnelAccess
		SET AccessLevel = 4
		FROM ClientPersonnelAccess cpa
		WHERE cpa.LeadID = @LeadID	
		
		SELECT @EventComments = + CONVERT(VARCHAR,@@RowCount)+ ' group-specific restrictions following successful DPA'

		EXEC dbo._C600_DeleteANote @CaseID,929,@LeadEventID
		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

		
	IF @EventTypeID = 157369 /*Unlock policy - Compliance*/
	BEGIN
		EXEC dbo._C600_DeleteANote @CaseID,932,@LeadEventID
		
		/*2017-09-15 CW Set Locked flag to false for reference*/
		EXEC dbo._C00_SimpleValueIntoField 178973,'false',@CustomerID,@WhoCreated
		
	END

	/*DPA Unlock*/
    IF @EventTypeID = 158298 
	BEGIN
			EXEC dbo._C600_DeleteANote @CaseID, 929,@LeadEventID
	END


	/*Sanction screening*/
	IF @EventTypeID = 	157368
	BEGIN

		SELECT 'CustomerID',@CustomerID

		INSERT INTO @CasesToUpdate (CaseID,Updated)
		SELECT cs.CaseID, ''
		FROM Customers  c WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON L.CustomerID = c.CustomerID 
		INNER JOIN Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID 
		WHERE c.CustomerID = @CustomerID
		
		SELECT @Cases = COUNT (*)
		FROM @CasesToUpdate Ctu
		WHERE ctu.Updated <> 'Y'
		

		WHILE @Cases > 0 
		BEGIN	
					SELECT TOP 1 @CaseID = ctu.CaseID
					FROM @CasesToUpdate ctu
					WHERE ctu.Updated <> 'Y'
						
				SELECT 'CaseID',@CaseID

				EXEC dbo._C600_AddANote @CaseID,932,'Policy Locked - Complaince',1,@WhoCreated
					
				UPDATE @CasesToUpdate
				SET Updated  = 'Y'
				WHERE CaseID = @CaseID
		
				SELECT @CaseID = NULL
				SELECT @Cases = NULL	

				SELECT @Cases = COUNT (*)
				FROM @CasesToUpdate Ctu
				WHERE ctu.Updated <> 'Y'
		END

		/*2017-09-15 CW Set Locked flag to true for reference*/
		EXEC dbo._C00_SimpleValueIntoField 178973,'true',@CustomerID,@WhoCreated
		
	END

	/*DPA - Failed DPA - Add alert note - SA 2017-07-10*/
	IF @EventTypeID = 156924
	BEGIN
		
		SELECT @DPACount = COUNT( cdv.DetailValue)
		FROM Customers cu WITH (NOLOCK)
		INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cu.CustomerID = cdv.CustomerID 
		WHERE cu.CustomerID = @CustomerID
		AND Cdv.DetailFieldID IN (179076,178267,178407,178268,178276,178277,178278,178279,178280,178281,178408)
		AND cdv.DetailValue ='true'

		INSERT INTO @CasesToUpdate (CaseID,Updated)
		SELECT cs.CaseID, ''
		FROM Customers  c WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON L.CustomerID = c.CustomerID 
		INNER JOIN Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID 
		WHERE c.CustomerID = @CustomerID

		SELECT @DPAOutcome = dbo.fnGetSimpleDv(177475,@CustomerID)

		SELECT @Cases = COUNT (*)
		FROM @CasesToUpdate ctu
		WHERE ctu.Updated <> 'Y'

		WHILE @Cases > 0 
		BEGIN					
			SELECT TOP 1 @ValueInt = ctu.CaseID
			FROM @CasesToUpdate ctu
			WHERE ctu.Updated <> 'Y'
				
			UPDATE @CasesToUpdate
			SET Updated  = 'Y'
			WHERE CaseID = @ValueInt
			

			SELECT @ValueInt = NULL
			SELECT @Cases = NULL	

			SELECT @Cases = COUNT (*)
			FROM @CasesToUpdate Ctu
			WHERE ctu.Updated <> 'Y'
		END

			
		/*Capture confirmed DPA questions checklist*/
		SELECT @DPAName = dbo.fnGetSimpleDv						(178266,@CustomerID) /*Name of accessed by*/  
		SELECT @DPAPHName =			CASE dbo.fnGetSimpleDv		(179076,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*PH Name*//*added by NG 2017-09-18*/ 
		SELECT @DPAAddressLn1 =		CASE dbo.fnGetSimpleDv		(178267,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Address Line 1*//*updated by NG 2017-09-18*/ 
		SELECT @DPAPHDoB =			CASE dbo.fnGetSimpleDv		(178407,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*PH Date of Birth*//*added by NG 2017-09-18*/ 
		SELECT @DPAAddContactName = CASE dbo.fnGetSimpleDv		(178268,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Additional Contact Name*/
		SELECT @DPAPolNumber =		CASE dbo.fnGetSimpleDv		(178276,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Policy Number*/
		SELECT @DPAPetName =		CASE dbo.fnGetSimpleDv		(178277,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Pet Name*/
		SELECT @DPAPetDOB =			CASE dbo.fnGetSimpleDv		(178278,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Pet Date Of Birth*/
		SELECT @DPAPolStart =		CASE dbo.fnGetSimpleDv		(178279,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Policy Start Date*/
		SELECT @DPAPremAmount =		CASE dbo.fnGetSimpleDv		(178280,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Premium Amount*/
		SELECT @DPABankName =		CASE dbo.fnGetSimpleDv		(178281,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*bank name*/
		SELECT @DPAPasswordUsed =	CASE dbo.fnGetSimpleDv		(178408,@CustomerID) WHEN 'true' THEN 'Asked' ELSE '' END /*Password used*//*added by NG 2017-09-18*/
		SELECT @DPAPassed =			CASE dbo.fnGetSimpleDvLuli	(177475,@CustomerID) WHEN 'Yes' THEN 'Passed' ELSE 'Failed' END/*Passed DPA*/
		SELECT @DPADate =			CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121)
		SELECT @DPAReasonFailed = dbo.fnGetSimpleDv				(178287,@CustomerID) /*reason DPA failed*/		
		
		/*Update DPA Audit History*/
		/*create a table row to insert into*/
		EXEC dbo.TableRows_Insert @TableRowID OUTPUT,@ClientID,NULL,NULL,178258,19131,1,1,@CustomerID,NULL,@WhoCreated, NULL, @LeadEventID

		--/*Insert Checklist Values Into Table Row*/
		EXEC dbo._C00_SimpleValueIntoField 178252,@DPAName,@TableRowID /*Name*/  
		EXEC dbo._C00_SimpleValueIntoField 179078,@DPAPHName,@TableRowID /*PH Name*//*added by NG 2017-09-18*/
		EXEC dbo._C00_SimpleValueIntoField 178272,@DPAAddressLn1,@TableRowID /*Address Line 1*/
		EXEC dbo._C00_SimpleValueIntoField 179077,@DPAPHDoB,@TableRowID /*PH Date of Birth*//*added by NG 2017-09-18*/
		EXEC dbo._C00_SimpleValueIntoField 178254,@DPAAddContactName,@TableRowID /*Additional Contact Name*/
		EXEC dbo._C00_SimpleValueIntoField 178255,@DPAPolNumber,@TableRowID /*Policy Number*/
		EXEC dbo._C00_SimpleValueIntoField 178271,@DPAPetName,@TableRowID /*Pet Name*/ 
		EXEC dbo._C00_SimpleValueIntoField 178253,@DPAPetName,@TableRowID /*Pet Date Of Birth*/
		EXEC dbo._C00_SimpleValueIntoField 178273,@DPAPolStart,@TableRowID /*Policy Start Date*/
		EXEC dbo._C00_SimpleValueIntoField 178274,@DPAPremAmount,@TableRowID /*Premium Amount*/
		EXEC dbo._C00_SimpleValueIntoField 178275,@DPABankName,@TableRowID /*bank name*/
		EXEC dbo._C00_SimpleValueIntoField 179079,@DPAPasswordUsed,@TableRowID /*Password Used*//*added by NG 2017-09-18*/
		EXEC dbo._C00_SimpleValueIntoField 178256,@DPAPassed,@TableRowID /*DPA Passed*/
		EXEC dbo._C00_SimpleValueIntoField 178257,@DPADate,@TableRowID /*DPA Date*/
		EXEC dbo._C00_SimpleValueIntoField 178288,@DPAReasonFailed,@TableRowID /*DPA Date*/
		
		/*Wipe out values*/
		EXEC dbo._C00_SimpleValueIntoField 178266,'',@CustomerID /*Name*/  
		EXEC dbo._C00_SimpleValueIntoField 179076,'',@CustomerID /*PH Name*//*added by NG 2017-09-18*/
		EXEC dbo._C00_SimpleValueIntoField 178267,'',@CustomerID /*Address Line 1*/
		EXEC dbo._C00_SimpleValueIntoField 178407,'',@CustomerID /*PH DoB*//*added by NG 2017-09-18*/
		EXEC dbo._C00_SimpleValueIntoField 178268,'',@CustomerID /*Additional Contact Name*/
		EXEC dbo._C00_SimpleValueIntoField 178276,'',@CustomerID /*Policy Number*/
		EXEC dbo._C00_SimpleValueIntoField 178277,'',@CustomerID /*Pet Name*/ 
		EXEC dbo._C00_SimpleValueIntoField 178278,'',@CustomerID /*Pet Date Of Birth*/
		EXEC dbo._C00_SimpleValueIntoField 178279,'',@CustomerID /*Policy Start Date*/
		EXEC dbo._C00_SimpleValueIntoField 178280,'',@CustomerID /*Premium Amount*/
		EXEC dbo._C00_SimpleValueIntoField 178281,'',@CustomerID /*bank name*/
		EXEC dbo._C00_SimpleValueIntoField 178408,'',@CustomerID /*password used*//*added by NG 2017-09-18*/
		EXEC dbo._C00_SimpleValueIntoField 178287,'',@CustomerID /*Reason DPA failed*/
		
		/*store value of DPA completed into separate field for lock*/
		EXEC dbo._C00_SimpleValueIntoField 179200,@DPAPassed,@CustomerID /*DO NOT DELETE - DPA outcome*/
	
		/*wipe out the DPA completed drop down*/
		EXEC dbo._C00_SimpleValueIntoField 177475,'',@CustomerID /*DPA Completed*/
	
		/*add note to state if authority exists and note doesnt*/
		SELECT @DPAAuthCount = COUNT(*)
		FROM TableRows tr WITH (NOLOCK)
		WHERE tr.DetailFieldID = 178263 /*authorities*/
		AND tr.CustomerID = @CustomerID
	
	END
	
	/*Set to bypass selected - VET - */
	IF @EventTypeID IN (157051,157263) 
	BEGIN
		EXEC dbo._C00_SimpleValueIntoFieldLuli 177907,'Yes',@MatterID 
 	END
	
	/*Set to bypass to not selected - VET - */
	IF @EventTypeID IN (156615,156620)
	BEGIN
		EXEC dbo._C00_SimpleValueIntoFieldLuli 177907,'No',@MatterID 
 	END
 	
	IF @EventTypeID IN (121503)  /*Select policy sections*/
	BEGIN
		/*Update claim to state whether max age has been reached*/
		SELECT @EventComments = ''
		
		/*find policy section selected - if death from injury or illness then....*/
		SELECT @SelectedSection = rldvsection.ValueInt 
		FROM Matter m WITH (NOLOCK)
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355 /*Claim Details Data*/
		INNER JOIN dbo.TableDetailValues tSection WITH (NOLOCK) ON tSection.TableRowID = r.TableRowID AND tSection.DetailFieldID = 144350 /*Policy Section*/
		INNER JOIN dbo.ResourceListDetailValues rldvSection WITH (NOLOCK) ON rldvSection.ResourceListID = tSection.ResourceListID AND rldvSection.DetailFieldID = 146189 /*Policy Section*/
		WHERE m.MatterID = @MatterID
		
		SELECT @LeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)

		PRINT ISNULL(@SelectedSection,1234)
		
		SELECT  'Pet Age', dbo.fnAgeAtDate(dbo.fnGetSimpleDvAsDate(144274,@LeadID) /*Pet Date of Birth*/,@WhenCreated)

		/*IF @PolcySection = 'Death from Illness' */
		IF @SelectedSection IN (74548)
		BEGIN
                      /*Check against policy section max age cap - policy section checker required*/
                     SELECT @AgeCheck = COUNT(tdvMaxAge.ValueInt)
                     FROM TableRows tr WITH (NOLOCK)
                     INNER JOIN TableDetailValues tdvSection WITH (NOLOCK) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID =    144357 /*Policy Scheme*/
                     INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = tdvSection.ResourceListID and rldv.DetailFieldID = 146189  /*Policy Scheme*/
                     INNER JOIN TableDetailValues tdvMaxAge WITH (NOLOCK) on tdvMaxAge.TableRowID = tr.TableRowID AND tdvMaxAge.DetailFieldID =  177450 /*Max Pet Age*/
                     WHERE tr.DetailFieldID = 145692 /*Policy Limits*/
                     AND tr.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter(@MatterID)
                     AND tdvMaxAge.ValueInt IS NOT NULL
                     AND rldv.ValueInt = @SelectedSection
                     AND tdvMaxAge.ValueInt <= dbo.fnAgeAtDate(dbo.fnGetSimpleDvAsDate(144274,@LeadID) /*Pet Date of Birth*/,@WhenCreated)
       
			PRINT ISNULL(@AgeCheck,11)

			IF @AgeCheck >= 1 
			BEGIN
				EXEC dbo._C00_SimpleValueIntoFieldLuli 179899,'Yes',@MatterID,@WhoCreated, ''
			
				/*Add event comments*/
				SELECT @EventComments += ' excluded on grounds of pet age.' + CHAR(13)+CHAR(10)
			END
			ELSE
			BEGIN
					EXEC dbo._C00_SimpleValueIntoFieldLuli 179899,'No',@MatterID,@WhoCreated, ''
					
					/*Add event comments*/
					SELECT @EventComments +=  'Below max pet age cap.' + CHAR(13)+CHAR(10)
			END
		END

		/*Add event comments*/
		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

	IF @EventTypeID = 158772 /*Change Preferred Payment Date*/
	BEGIN
	
		EXEC dbo.ReschedulePaymentDate_CreateFromThirdPartyFields @ClientID, @CaseID, @LeadEventID 

		/* Ensure document fields are completed. AAD 2021-03-10 for FURKIN-337 */
		EXEC _C600_SetDocumentFields @MatterID, @WhoCreated, 'MTA'

		/*2021-03-03 FURKIN-237 - Morph to in proc event*/
		EXEC dbo._C00_MorphLeadEvent @LeadEventID, 155275

	END

	IF @EventTypeID = 158799 /*2018-04-17 ACE - Represent customer for card payment*/
	BEGIN

		/*
			Either the process has moved on and the customer is called by a CSR or the customer has called in we need to invalidate the 
			existing card transaction and create a new one we will also need to clear out any existing Eckoh sesssions.
		*/
		
		SELECT @CardTransactionID = dbo.fnGetSimpleDvAsInt(180010,@MatterID) /*Transaction to Retry*/
		
		IF ISNULL(@CardTransactionID,0) <> 0 
		   AND EXISTS (SELECT * FROM CardTransactionPolicy cp WITH (NOLOCK) where cp.CardTransactionID = @CardTransactionID and cp.PAMatterID = @MatterID ) 
		BEGIN 
			
			/*We will have (potentially) multiple records here but we will handle them on one matter/case.*/
			SELECT @Amount = ct.Amount, @PaymentGatewayID = ct.ClientPaymentGatewayID, @MerchantCode = ct.MerchantCode
			FROM CardTransaction ct WITH (NOLOCK)
			INNER JOIN CardTransactionPolicy ctp WITH (NOLOCK) ON ctp.CardTransactionID = ct.CardTransactionID
			WHERE ctp.PAMatterID = @MatterID
			AND ct.CardTransactionID = @CardTransactionID 
			
			/*We'll need to add a PPPS, CT, P and CL record for the new transaction*/ 
			SELECT @CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID, @AccountID = a.AccountID , @AmountNet = cps.PaymentNet, @AmountVAT = cps.PaymentVAT
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			INNER JOIN Account a WITH (NOLOCK) on a.AccountID = cps.AccountID
			INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = a.ObjectID
			WHERE cps.CustomerID = @CustomerID 
			AND cps.PaymentGross = @Amount
			AND cps.PaymentStatusID NOT IN (6) /*Not Paid*/ 
			
			/*     UPDATE CPS and PPPS to failed      */
			  
			/*Once copied we can invlidate the existing record*/
			UPDATE ct
			SET ErrorCode = 'Invalidated Transaction', ErrorMessage = 'Invalidated Transaction'
			FROM CardTransaction ct WITH (NOLOCK)
			WHERE ct.CardTransactionID = @CardTransactionID

			UPDATE cps  
            SET PaymentStatusID=4, ReconciledDate=dbo.fn_GetDate_Local()
            FROM CustomerPaymentSchedule cps
            WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
            
            -- purchasedproductpaymentschedule, reverse payment status
            UPDATE ppps  
            SET PaymentStatusID=4, ReconciledDate=dbo.fn_GetDate_Local(), ContraCustomerLedgerID = @CustomerLedgerID
            FROM PurchasedProductPaymentSchedule ppps
            WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

			/*     PREP FOR RETRY    */ 
			
			/*Add a new PPPS record*/ 		
			INSERT INTO PurchasedProductPaymentSchedule (ClientID,CustomerID,AccountID,PurchasedProductID,ActualCollectionDate,CoverFrom,CoverTo,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,CustomerPaymentScheduleID,WhoCreated,WhenCreated,PurchasedProductPaymentScheduleParentID, ClientAccountID, TransactionFee)
			SELECT pp.ClientID,pp.CustomerID,pp.AccountID,pp.PurchasedProductID,@Now,pp.CoverFrom,pp.CoverTo,@Now,pp.PaymentNet, pp.PaymentVAT, pp.PaymentGross, 5,@CustomerLedgerID,@Now,@NewCustomerPaymentScheduleID,44412,@Now,pp.PurchasedProductPaymentScheduleParentID, pp.ClientAccountID, pp.TransactionFee
			FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
			WHERE pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID
			
			SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY() 
			
			EXEC Billing__RebuildCustomerPaymentSchedule  @AccountID, @Now,@WhoCreated 
			
			SELECT @NewCustomerPaymentScheduleID = pps.CustomerPaymentScheduleID 
			FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
			WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID 

			/*Copy the existing record so we have the customer and amount..*/
			INSERT INTO CardTransaction (ClientID, CustomerID, Amount, WhenCreated, CustomerPaymentScheduleID, ClientPaymentGatewayID, MerchantCode)
			OUTPUT inserted.CardTransactionID INTO @InsertedCardTransactions (CardTransactionID)
			SELECT ClientID, CustomerID, Amount, dbo.fn_GetDate_Local(), @NewCustomerPaymentScheduleID, @PaymentGatewayID, @MerchantCode
			FROM CardTransaction ct WITH (NOLOCK)
			WHERE ct.CardTransactionID = @CardTransactionID 
			
			SELECT @NewCardTransactionID = SCOPE_IDENTITY()
			
			INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross,  DateReconciled, WhoCreated, WhenCreated, WhoModified, WhenModified,PaymentTypeID, CardTransactionID)
			VALUES (@ClientID, @CustomerID, @Now, 'Card Payment', CAST(@NewCardTransactionID AS VARCHAR),  @AmountNet, @AmountVat, @Amount,   @Now,   58552, @Now, 58552,@Now,7,@NewCardTransactionID)		

			SELECT  @PaymentID = SCOPE_IDENTITY()	
							
			INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			VALUES (@ClientID, @CustomerID, @Now, NULL, NULL, @Now, CAST(@NewCardTransactionID AS VARCHAR), 'Retry Transaction', @AmountNet, @AmountVat, @Amount, NULL,  @PaymentID, NULL, 58552, dbo.fn_GetDate_Local())		
			
			SELECT @Firstname = c.FirstName, @Lastname = c.LastName, @ValueDate = c.DateOfBirth 
			FROM Customers c WITH (NOLOCK)
			WHERE c.CustomerID = @CustomerID 
			
			/*   invalidate old record   */		

			SELECT @NewCardTransactionID = i.CardTransactionID
			FROM @InsertedCardTransactions i

			/*We will need this if/when it is successful so we can move the process on..*/
			INSERT INTO CardTransactionPolicy (CardTransactionID, PAMatterID, WhoCreated, WhenCreated)
			SELECT @NewCardTransactionID, c.PAMatterID, @WhoCreated, dbo.fn_GetDate_Local()
			FROM CardTransactionPolicy c WITH (NOLOCK)
			WHERE c.CardTransactionID = @CardTransactionID

			/*Now we need to invalidate eckoh records*/
			/*Or not...*/
			EXEC _C00_SimpleValueIntoField 180010,'',@MatterID,@WhoCreated 
			
		END 
		ELSE 
		BEGIN 
	
			SELECT @ErrorMessage = '<font color="red"></br></br>You have not selected a Transaction</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
	
		END 
	END
	
	/*Begin - Take Arrears Payment*/
	/*Begin - Take Arrears and Outstanding Bal Payment*/
	IF @EventTypeID IN (158806,158807,158816)
	BEGIN
		IF @EventTypeID = 158807
		BEGIN

			EXEC [dbo].[_C600_MorphOOPToIPLeadEvent] @LeadEventID, 158805, -1

		END
		ELSE
		IF @EventTypeID = 158806
		BEGIN

			EXEC [dbo].[_C600_MorphOOPToIPLeadEvent] @LeadEventID, 158804, -1
	
		END
		
		SELECT @AccountID = a.AccountID 
		FROM Account a WITH (NOLOCK) 
		WHERE a.CustomerID = @CustomerID 
		AND a.AccountUseID = 1 /*ALM 2020-11-06 SDPRU-134*/ 
		AND a.Active = 1 
		
		IF ISNULL(@AccountID ,0) = 0 
		BEGIN 
		
			/*Need to create a collections case and matter, link it via leadtype relationships to the policy we are on...*/
			SELECT @ColLeadID = LeadID 
			FROM Lead WITH (NOLOCK) 
			WHERE CustomerID=@CustomerID 
			AND LeadTypeID=1493
					
			EXEC @ColMatterID = dbo._C00_CreateNewCaseForLead @ColLeadID, @WhoCreated, 1				

			---- link new collections lead to policy admin lead
			INSERT INTO dbo.LeadTypeRelationship (FromLeadTypeID, ToLeadTypeID, FromLeadID, ToLeadID, FromMatterID, ToMatterID, ClientID) VALUES 
			(1492, 1493, @LeadID, @ColLeadID, @MatterID, @ColMatterID, @ClientID)

			/*2018-04-20 ACE Added create account and update cps ready for ct creation*/
			EXEC @AccountID = Billing__CreatePremiumAccountAccountRecord @ClientID, @ColMatterID, 2, NULL, NULL, 'CardAccount', 1, @CustomerID, @WhoCreated, 2
		END 
		
		EXEC _C00_SimpleValueIntoField 180020, @AccountID,@MatterID,@WhoCreated 
		
	END 
	
	IF @EventTypeID IN (158812 /*Display Arrears and Future Payment*/, 158815 /*Display Arrears*/)
	BEGIN 
		
		SELECT @AccountID = dbo.fnGetSimpleDvAsInt(180020,@MatterID) 
		
		DECLARE @CCPayments TABLE (AmountNet DECIMAL(18,2), AmountIPT DECIMAL(18,2), AmountGross DECIMAL(18,2),CustomerPaymentScheduleID INT) 
		INSERT INTO @CCPayments (AmountNet, AmountIPT , AmountGross ,CustomerPaymentScheduleID) 
		SELECT c.PaymentNet,c.PaymentVat, c.PaymentGross, c.CustomerPaymentScheduleID 
		FROM CustomerPaymentSchedule c WITH (NOLOCK)
		WHERE c.CustomerID = @CustomerID
		AND c.PaymentDate <= dbo.fn_GetDate_Local()
		AND c.PaymentStatusID = 1

		SELECT @ColMatterID = a.ObjectID, @ClientAccountID = a.ClientAccountID 
		FROM Account a with (NOLOCK) 
		WHERE a.AccountID = @AccountID 
		
		INSERT INTO CustomerPaymentSchedule (ClientID,CustomerID,AccountID,	ActualCollectionDate,PaymentDate,PaymentGross, PaymentNet, PaymentVAT , PaymentStatusID,WhoCreated,WhenCreated,SourceID,RelatedObjectID,RelatedObjectTypeID,ClientAccountID,WhoModified,WhenModified)
		SELECT @ClientID, @CustomerID, @AccountID, @Now,@Now,SUM(c.AmountGross),SUM(c.AmountNet),SUM(c.AmountIPT),1,@WhoCreated,@Now,@LeadEventID,@ColMatterID,2,@ClientAccountID,@WhoCreated,@Now
		FROM @CCPayments c

		SELECT @CustomerPaymentScheduleID = SCOPE_IDENTITY() 

		Update p
		SET CustomerPaymentScheduleID = @CustomerPaymentScheduleID
		FROM PurchasedProductPaymentSchedule p 
		Inner join @CCPayments cc on cc.CustomerPaymentScheduleID = p.CustomerPaymentScheduleID 

		DELETE cps 
		FROM CustomerPaymentSchedule cps
		INNER JOIN @CCPayments c on c.CustomerPaymentScheduleID = cps.CustomerPAymentScheduleID 

		SELECT   @PaymentGross = c.PaymentGross
				,@PaymentNet = c.PaymentNet
				,@PaymentTax = c.PaymentVat
				,@ActualCollectionDate = c.ActualCollectionDate
		FROM CustomerPaymentSchedule c WITH (NOLOCK)
		WHERE c.CustomerPaymentScheduleID = @CustomerPaymentSCheduleID

		UPDATE ppps
		SET AccountID = @AccountID
		FROM PurchasedProductPaymentSchedule ppps 
		WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		SELECT @PaymentTypeID = 1,
			   @ClientPaymentGatewayID = 7--CASE dbo.fnGetSimpleDvAsInt(170144,@CustomerID) WHEN 153389 THEN 5  /*LnG*/
																						 --WHEN 153391 THEN 3 ELSE 0 END  /*Buddies*/   /*?*/		
				-- 2019-12-09 CPS for WorldPay integration | Shouldn't this read from the affinity?
		SELECT	 @FirstName		= cu.FirstName
				,@LastName		= cu.LastName
				,@Address1		= cu.Address1
				,@Address2		= cu.Address2
				,@AddressTown	= cu.Town
				,@AddressCounty = cu.County
				,@Postcode		= cu.PostCode
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID 

		EXEC CardTransaction__PrepareThirdPartyFields @MatterID, @WhoCreated, 'Arrears Payment', @PaymentGross, @FirstName, @LastName, @Address1, @Address2, @AddressTown, @AddressCounty, 'GB', @Postcode, @CustomerPaymentScheduleID, @LeadEventID
		EXEC IncomingPayment__PrepareThirdPartyFields @MatterID, @WhoCreated, 69931 /*PaymentTypeID = "Credit Card"*/, 'Arrears Payment', @LeadEventID, @PaymentNet, @PaymentTax, @PaymentGross, @CustomerPaymentScheduleID, 1, @ActualCollectionDate, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @LeadEventID, NULL, NULL, NULL, @LeadEventID
		
	END

	/*GPR 2019-11-20 Premium paid by Premium Funding - New Business*/
	/*GPR 2020-01-24 Added Renewal Premium paid by premium funding*/
	IF @EventTypeID IN (158933 /*Premium paid by premium funding provider*/, 158981 /*Renewal Premium paid by premium funding*/) 
	BEGIN

		SELECT @PaymentTypeID = 11 /*Premium Funding*/

		SELECT TOP 1 @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
		FROM CustomerPaymentSchedule c WITH (NOLOCK)
		INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID 
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = p.PurchasedProductID
		WHERE c.CustomerID = @CustomerID
		AND c.PaymentStatusID = 1
		AND p.PurchasedProductPaymentScheduleTypeID IN  (1) /*Scheduled*/ 
		AND pp.ObjectID = @MatterID 

		EXEC Billing__CreatePaymentForPremiumFunding @CustomerPaymentScheduleID, @PaymentTypeID, 'Premium Funding Payment', @LeadEventID, @WhoCreated /*LPC-54 / LPC-111*/

	END

	/*GPR 2019-12-03 Premium Funding - MTA or Cancellation via Adjustment*/
	IF @EventTypeID = 158954 /*Premium Funding Adjustment Successful*/
	BEGIN

		IF dbo.fnGetSimpleDvAsInt(170114,@MatterID) = 76618 /*Premium Funding*/
		BEGIN
		
			SELECT @PaymentTypeID = 11 /*Premium Funding*/

			SELECT TOP 1 @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
			FROM CustomerPaymentSchedule c WITH (NOLOCK)
			INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID 
			INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = p.PurchasedProductID
			WHERE c.CustomerID = @CustomerID
			AND c.PaymentStatusID = 1
			AND p.PurchasedProductPaymentScheduleTypeID IN  (6 /*MTA Adjustment*/, 7 /*To cover Cancellation Adjustments*/)
			AND pp.ObjectID = @MatterID 

			IF @CustomerPaymentScheduleID IS NOT NULL
			BEGIN

				EXEC Billing__CreatePaymentForPremiumFunding @CustomerPaymentScheduleID, @PaymentTypeID, 'Premium Funding Payment', @LeadEventID, @WhoCreated /*LPC-66*/
		
			END

		END

	END

	/*GPR 2019-12-03 - MTA Mid Renewal PCL*/
	IF @EventTypeID IN (155246 /*change customer*/, 155247 /*change pet*/)
	BEGIN

		IF dbo.fnGetSimpleDvAsInt(170114,@MatterID) = 76618 /*Premium Funding*/
		BEGIN

			/*IF EXISTS Prepare Renewal without a later event of Renew Policy or Policy Expired - THEN you're in the renewal window!*/
			IF EXISTS	(SELECT * FROM LeadEvent LeadEvent WITH (NOLOCK)
			WHERE LeadEvent.CaseID=@CaseID AND LeadEvent.EventTypeID = 150143 /*PrepareRenewal*/
			AND LeadEvent.EventDeleted=0
			AND LeadEvent.ClientID = @ClientID
			AND NOT EXISTS (SELECT * FROM dbo.LeadEvent Sub_LeadEvent WITH (NOLOCK)
							WHERE Sub_LeadEvent.CaseID = @CaseID
							AND Sub_LeadEvent.EventTypeID IN (150151 /*Renew Policy*/, 150150 /*Policy Expired*/)
							AND Sub_LeadEvent.LeadEventID > LeadEvent.LeadEventID
							AND Sub_LeadEvent.ClientID = @ClientID
							AND Sub_LeadEvent.EventDeleted = 0)
			)
			BEGIN
				
				/*Raise Error*/
				SELECT @ErrorMessage = '<font color="red"></br></br>This Event can only be used for Policies that pay by Premium Funding outside of the Renewal Window. Please apply the Premium Funding equivalent.</br></font><font color="#edf4fa">'

				RAISERROR( @ErrorMessage, 16, 1 )

			END

		END
	
	END

	/*2020-01-15 GPR PCL Loan Application Failure - update the PaymentStatus to Failed*/
	IF @EventTypeID IN (158939,158971)
	BEGIN

		/*Locate the latest scheduled payment and set it to failed*/	
		SELECT TOP 1 @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
		FROM CustomerPaymentSchedule c WITH (NOLOCK)
		INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) on p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID 
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = p.PurchasedProductID
		WHERE c.CustomerID = @CustomerID
		AND c.PaymentStatusID IN (1)
		AND p.PurchasedProductPaymentScheduleTypeID IN  (1) /*Scheduled*/ 
		AND pp.ObjectID = @MatterID 
				
		UPDATE cps
		SET PaymentStatusID = 4
		FROM CustomerPaymentSchedule cps 
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE ppps
		SET PaymentStatusID = 4
		FROM PurchasedProductPaymentSchedule ppps 
		WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

	END
	
	/*Off system Cheque issued for refund failure*/ 
	IF @EventTypeID = 158827 
	BEGIN 
		
		SELECT @CustomerLedgerID = dbo.fnGetSimpleDvAsInt(180051,@MatterID) 

		INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
		SELECT cl.ClientID, cl.CustomerID, dbo.fn_GetDate_Local(), '', '', CAST(dbo.fn_GetDate_Local() AS DATE), TransactionReference, 'Off System Cheque Issued', -cl.TransactionNet, -cl.TransactionVAT, -cl.TransactionGross, @LeadEventID, @MatterID, 2, 0, 0, 32438 /*@AqAutomation*/, dbo.fn_GetDate_Local() 
		FROM CustomerLedger cl WITH (NOLOCK) 
		WHERE cl.CustomerLedgerID = @CustomerLedgerID 
		
		EXEC _C00_SimpleValueIntoField 180051,'',@MatterID,@WhoCreated		

	END 
	
	/*2018-06-13 ACE Custom event followups*/
	IF @EventTypeID IN (155235)
	BEGIN

		SELECT @SchemeMatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter(@MatterID)

		SELECT TOP 1 @TableRowID = tdv.TableRowID
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.MatterID = @SchemeMatterID
		AND tdv.DetailFieldID = 180061
		AND tdv.ValueInt > 0

		IF @TableRowID > 0
		BEGIN

			SELECT @CustomFollowUpDays = dbo.fnGetSimpleDvAsInt(180063, @TableRowID)

			IF @CustomFollowUpDays > 0
			BEGIN

				UPDATE le
				SET FollowUpDateTime = DATEADD(DD, @CustomFollowUpDays, WhenCreated)
				FROM LeadEvent le
				WHERE le.LeadEventID = @LeadEventID

			END

		END

	END
		
	/*DDICA Indeminity claim*/ 
	IF @EventTypeID = 158834 
	BEGIN 
	
		SELECT @AccountID = a.AccountID FROM Account a WITH (NOLOCK) 
		WHERE a.ObjectID = @MatterID 
	
		INSERT INTO @MandateMatters (MatterID, CaseID, Done) 
		SELECT m.MatterID, m.CaseID, 0 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.ObjectID = m.MatterID
		WHERE p.AccountID = @AccountID 
		
		/*Now loop round and make sure the details are populated in the policy admin matters*/ 
		WHILE EXISTS (SELECT * FROM @MandateMatters 
					  Where Done = 0) 
		BEGIN 
		
			SELECT TOP 1 @ValueInt = mm.MatterID 
			FROM @MandateMatters mm 
			WHERE Done = 0 
			Order by mm.MatterID desc 
			
			SELECT TOP 1 @LeadEventID = c.LatestNonNoteLeadEventID, @EventTypeID = le.EventTypeID, @LeadID = m.LeadID, @CaseID = c.CaseID
			FROM Matter m WITH (NOLOCK)
			INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = m.CaseID
			INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestNonNoteLeadEventID
			WHERE m.MatterID = @ValueInt
			
			EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueueFull @ClientID, @LeadID, @CaseID, @LeadEventID, @EventTypeID, 155374, -1, @Now, @WhoCreated, 'DDICA Indemnity Claim Received'

			UPDATE mm 
			SET Done = 1  
			FROM @MandateMatters mm 
			WHERE Done = 0 
			AND mm.MatterID = @ValueInt
		
		END
	
	END

	IF @EventTypeID = 158840 /*Issue Refund By BACS*/
	BEGIN 

		SELECT @AccNumber = dbo.fnGetSimpleDv(180068,@MatterID), 
			   @AccSortCode = dbo.fnGetSimpleDv(180069,@MatterID), 
			   @AccountName = dbo.fnGetSimpleDv(180070,@MatterID), 
			   @ValueINT = dbo.fnGetSimpleDv(180071,@MatterID) 
		
		
		/*and check the existing payments accounts the customer has*/ 
		SELECT  @AccountID = a.AccountID  
		FROM Account a WITH (NOLOCK) 
		WHERE a.CustomerID = @CustomerID 
		AND a.AccountNumber = @AccNumber 
		AND a.Sortcode = @AccSortCode 
		AND a.AccountHolderName = @AccountName
		AND a.AccountUseID = 1 /*Claim Payment Account Only*/ 
		AND a.AccountTypeID = 1 
					
		/*If we don't have an account for this detail, then create one*/ 
		IF ISNULL(@AccountID,0) = 0 
		BEGIN 
						/*Pick up the affinity specific client account ID to write to the customers account record*/ 
			SELECT @ClientAccountID = rldv.ValueInt 
			FROM CustomerDetailValues cdv WITH (NOLOCK) 
			INNER JOIN Matter m WITH (NOLOCK) on m.CustomerID = cdv.CustomerID 
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt
			WHERE m.MatterID = @MatterID 
			AND cdv.DetailFieldID = 170144
			AND rldv.DetailFieldID = 179949
								
			EXEC @AccountID = dbo.Billing__CreatePremiumAccountAccountRecord @ClientID,@MatterID,2,@AccNumber,@AccSortCode,@AccountName,@ClientAccountID,@CustomerID,@WhoCreated,1
				
		END
		
		Update CustomerPaymentSchedule 
		SET AccountID = @AccountID 
		FROM CustomerPaymentSchedule cps 
		INNER JOIN PurchasedProductPaymentSchedule pps on pps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
		WHERE pps.PurchasedProductPaymentScheduleID = @ValueINT 
				
	END 

	/*Resend BACS Mandate following notification*/
	IF @EventTypeID = 156740
	BEGIN

		/*
			2018-06-22 ACE - If the mandate comes in as no instruction then re-apply
			This means setting the account back to active so it will get picked up
			in the next sweep for new mandate requests.
		*/

		SELECT @AccountID = dbo.fnGetSimpleDvAsInt(176973,@MatterID)

		IF @AccountID > 0
		BEGIN
		
			UPDATE a
			SET Active = 1, WhoModified = @WhoCreated, WhenModified = dbo.fn_GetDate_Local(), Comments = 'Reactivated by Resend BACS Mandate following notification'
			FROM Account a
			WHERE a.AccountID = @AccountID

			SELECT @Comments = 'Account ID ' + CONVERT(VARCHAR(2000), @AccountID) + ' re-activated'

			SELECT @EarliestPaymentDate = dbo.fn_C00_CalculateActualCollectionDate(@AccountID, 1, dbo.fn_GetDate_Local(), 4, 0.00)

			/*Extension to defect 553 we will need to update the first/next payments to ensure mandate wait is adhered to.*/
			/*Defect 824 - The actual collection date should be 3 working days after the payment date. As per PurchasedProduct__CreatePaymentSchedule*/
			UPDATE ppps 
			SET ActualCollectionDate = dbo.fnAddWorkingDays(@EarliestPaymentDate,3) , PaymentDate = @EarliestPaymentDate
			FROM PurchasedProductPaymentSchedule ppps
			WHERE ppps.AccountID = @AccountID
			AND ppps.CustomerID = @CustomerID
			AND ppps.PaymentStatusID = 1
			AND ppps.PaymentDate < @EarliestPaymentDate

			/*!!!!!!!!!NO SQL HERE!!!!!!!!!!!!*/

			IF @@RowCount > 0
			BEGIN

				EXEC [dbo].[Billing__RebuildCustomerPaymentSchedule] @AccountID, @EarliestPaymentDate, @WhoCreated

			END

		END
		ELSE
		BEGIN

			SELECT @Comments = 'Account ID cannot be re-activated as there is no billing system account id.'

		END

		EXEC _C00_SetLeadEventComments @LeadEventID, @Comments, 1

	END

	/*Re-rate policy (pre-renewal) - Post MTA*/
	IF @OriginalEventTypeID = 158836
	BEGIN

		/*If we are a Re-rate policy (pre-renewal) - Then change back...*/
		UPDATE LeadEvent
		SET EventTypeID = 158836
		FROM LeadEvent 
		WHERE LeadEventID = @LeadEventID

	END


	IF @EventTypeID = 158841
	BEGIN 
	
		EXEC _C600_MorphOOPToIPLeadEvent  @LeadEventID,158840,0 
	
	END 

	IF @EventTypeID = 158844
	BEGIN

		EXEC _C600_MorphOOPToIPLeadEvent  @LeadEventID, 158843, 0

	END

    /* Set Outstanding Balance amount */
    IF @EventTypeID IN (150098)/*Payment Retry Limit - Cancellation options*/
    BEGIN
            
         EXEC _C600_Collections_RefreshDocumentationFields @MatterID
                  
    END   

	/*
		2018-10-24 ACE - Update the policy number and imported Purchased product
	*/
	IF @EventTypeID = 158850
	BEGIN

		/*GPR 2020-01-08 removed*/
		--EXEC dbo._C600_PA_Policy_AssignPolicyNumber @MatterID, @LeadEventID 

		SELECT @ProductDescription = dbo.fnGetSimpleDv(144268 /*Pet Name*/, @LeadID) + ' (Policy No: ' + dbo.fnGetSimpleDv(170050 /*PolicyNumber*/, @MatterID) + ')'
		
		/*GPR 2018-10-26 Assign @ProductName*/
		SELECT @ProductName = rlpn.DetailValue
		FROM Matter m WITH (NOLOCK)
		INNER JOIN MatterDetailValues pn WITH (NOLOCK) ON m.MatterID=pn.MatterID AND pn.DetailFieldID=170034
		INNER JOIN ResourceListDetailValues rlpn WITH (NOLOCK) ON rlpn.ResourceListID=pn.ValueInt AND rlpn.DetailFieldID=146200
		WHERE m.MatterID=@MatterID
		
		UPDATE pp
		SET ProductDescription = @ProductDescription, ProductName = @ProductName
		FROM PurchasedProduct pp 
		WHERE pp.ObjectID = @MatterID 
		AND pp.ProductDescription = ''

		--/*UAH 2018-11-05 Assign @CurrentPremium to field 175337*/

		SELECT TOP 1 @CurrentPremium = ValueMoney FROM TableDetailValues WITH (NOLOCK)
		WHERE detailfieldID = 175349
		AND MatterID = @MatterID
		ORDER BY TableRowID ASC

		EXEC _C00_SimpleValueIntoField 175337, @CurrentPremium, @MatterID

		/*Upon Migration, get the account name, sort code and account number from the collections lead type and stamp it in the PA sort and account fields  */
		SELECT @CollectionsMatterID = a.ObjectID
		FROM PurchasedProduct p WITH (NOLOCK) 
		INNER JOIN Account a WITH (NOLOCK) on a.AccountID = p.AccountID
		WHERE p.ObjectID = @MatterID​

		--assign new variables the account name, sort code value and the account number value from the collection fields
		SELECT @AccountName = detailvalue FROM MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = 170188
		AND MatterID = @CollectionsMatterID
		
		SELECT @AccSortCode = ISNULL(detailvalue,'') FROM MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = 170189
		AND MatterID = @CollectionsMatterID

		SELECT @AccNumber = ISNULL(detailvalue,'') FROM MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = 170190
		AND MatterID = @CollectionsMatterID

	 
	    Select @AccNumber = REPLICATE('*',LEN(@AccNumber)-4) + RIGHT(@AccNumber,4), /*Masks all but the last four characters of the account number with '*' */
		   @AccSortCode = REPLICATE('*',LEN(@AccSortCode)-4) + RIGHT(@AccSortCode,4); /*Masks all but the last four characters of the sort code with '*' */

		--stamp the collections sort code and account number into the policy admin fields that the renewal UPP invitation references.
		EXEC _C00_SimpleValueIntoField 170104, @AccountName, @MatterID
		EXEC _C00_SimpleValueIntoField 170105, @AccSortCode, @MatterID
		EXEC _C00_SimpleValueIntoField 170106, @AccNumber, @MatterID
		EXEC _C00_SimpleValueIntoField 177356, @AccNumber, @MatterID
		EXEC _C00_SimpleValueIntoField 177357, @AccSortCode, @MatterID

	END

	/*UPP MTAs, we don't need to rerate the current policy term, we just need to replace out the risk detail and trigger a rerate*/ 
	/* 2020-01-16 GPR this is now enabled for C603*/
	IF @EventTypeID IN (158861,158862) 
	BEGIN 
		
		/*Raise an error here if either of these events is being applied while it is not in the renewal window for migrated policies*/ 
		IF @Now > ISNULL(dbo._C600_GetFirstNonMigratedTermsDate(@MatterID), DATEADD(DAY,1,@Now))
		BEGIN 

			SELECT @ErrorMessage = '<font color="red"></br></br>This Event can only be used for Stub Migrated Polices pre renewal</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )

		END 

		/*get the most recent table row for the migrated UPP polciy term*/ 
		/*The status will be live, mig live, MTA 
		  The Adjustment type will be MTA, New, Reinstatment*/ 
		SELECT TOP 1 @TableRowID=tr.TableRowID FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues adt WITH (NOLOCK) ON tr.TableRowID=adt.TableRowID AND adt.DetailFieldID=175398 
		INNER JOIN TableDetailValues st with (NOLOCK) on st.TableRowID = tr.TableRowID and st.DetailFieldID = 175722
		WHERE tr.DetailFieldID=175336
			AND adt.ValueInt IN (74532,74527,74531)
			AND st.ValueInt in (72326,74514,76514)
			AND adt.MatterID=@MatterID
		ORDER BY tr.TableRowID DESC

		SELECT @PremiumGross = dbo.fnGetSimpleDvAsMoney(180214,@MatterID) 

		/*Get the current Purchased product so we can update this too*/ 
		SELECT @PurchasedProductID = dbo.fnGetSimpleDvAsInt(177074,@MatterID) 
				
		/*Now work out the IPT rate*/ 
		SELECT @PostCode = LEFT(c.PostCode,3) 
		FROM Customers c WITH (NOLOCK) 
		WHERE c.CustomerID = @CustomerID 
							
		SELECT @IPT = i.GrossMultiplier 
		FROM IPT i WITH (NOLOCK) 
		WHERE i.Region like '%' + @PostCode + '%'
	
		IF @IPT IS NULL
		BEGIN

			SELECT TOP 1 @IPT=i.GrossMultiplier 
			FROM IPT i WITH (NOLOCK) 
			WHERE i.CountryID = 232
			AND (i.[From] IS NULL OR @Now  >= i.[From])
			AND (i.[To] IS NULL OR @Now <= i.[To])
			AND i.Region IS NULL

		END
	
		SELECT @PremiumNet = @PremiumGross / @IPT 
		SELECT @PremiumTax = @PremiumGross - @PremiumNet 

		UPDATE PurchasedProduct 
		SET ProductCostGross = @PremiumGross, ProductCostNet = @PremiumNet, ProductCostVAT = @PremiumTax
		WHERE PurchasedProductID = @PurchasedProductID 

	    /*Write out the new premium to the Premium Detail History Table*/ 
		EXEC _C00_SimpleValueIntoField 175349, @PremiumGross, @TableRowID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 175397, @PremiumGross, @TableRowID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 177899, @PremiumGross, @TableRowID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 175375, @PremiumTax, @TableRowID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 175337, @PremiumGross, @MatterID, @WhoCreated

		/*Add the lead and matter to the temp fields so we know what we're dealing with*/ 
		EXEC _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated
	
		/*
		CPS 2017-07-12
		Take the address lookup value and populate the address detail fields
		*/
		INSERT @NumberedValues ( Value )
		SELECT fn.AnyValue
		/*FROM dbo.fnTableOfValuesFromCSV(REPLACE(dbo.fnGetSimpleDv(175478,@CustomerID),'_',',')) fn -- Change Address - Address Picker
		GPR 2018-05-21 spoke with Cathal and agreed function change so that multi comma addresses do not push postcode onto line 6+  -- C600 Defect 448*/
		FROM dbo.fnTableOfValues(dbo.fnGetSimpleDv(175478,@CustomerID),'_') fn
		
		/*Construct a tvp of data to populate*/
		INSERT @DetailValueData (ClientID, DetailFieldSubType, ObjectID, DetailFieldID, DetailValueID, DetailValue )
		SELECT @ClientID, 10, @CustomerID, df.DetailFieldID, ISNULL(cdv.CustomerDetailValueID,-1), ISNULL(nv.Value,'')
		FROM @NumberedValues nv 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = CASE nv.ID
																		WHEN 1 THEN 175314 /*Address 1*/
																		WHEN 2 THEN 175315 /*Address 2*/
																		WHEN 3 THEN 175316 /*Town*/
																		WHEN 4 THEN 175317 /*County*/
																		WHEN 5 THEN 175318 /*Postcode*/
																	   END 
		LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.DetailFieldID = df.DetailFieldID AND cdv.CustomerDetailValueID = @CustomerID
		WHERE df.Enabled = 1
		
		/*Perform a bulk save... hide the output of the sp in the @IdId variable*/
		INSERT @IdId ( ID1, ID2 )
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @WhoCreated
		
		/*put temporary pet & customer changes in a permanent place, ready for generating a quote when the event 156667 is applied */
		EXEC _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated /*TempLeadID*/
		EXEC _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated /*TempMatterID*/
		--EXEC dbo._C600_TempSwapPHandPetDetails @CustomerID
	
		DELETE @IdId
	
		EXEC _C600_TempSwapPHandPetDetails @CustomerID
		EXEC _C600_TempSwapPolicyDetails @LeadID /*GPR 2020-09-16 for PPET-527*/

		UPDATE CustomerDetailValues 
		SET DetailValue = ''
		WHERE DetailFieldID IN (179199,175586,179912,175326,175325,175352,175329,178406,178405,175314,175315,175478,175317,175332,175311,175313,175318,175310,175316)
		AND CustomerID = @CustomerID

		UPDATE MatterDetailValues 
		SET DetailValue = ''
		WHERE DetailFieldID IN (175442,177683,180214,175334) 
		AND MatterID = @MatterID 

		EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID, 158863, 0

	END 

	/* PA WORKFLOW - Morph to inprocess to allow workflow task to be followed up*/  --LETTER 
	IF @EventTypeID = 158870
	BEGIN 
		EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID,158872,-1
	END 

	/* PA WORKFLOW - Morph to inprocess to allow workflow task to be followed up*/ --EMAIL
	IF @EventTypeID = 158871
	BEGIN 
		EXEC _C600_MorphOOPToIPLeadEvent @LeadEventID,158873,-1
	END 


	/*Morph to inprocess to allow workflow task to be followed up*/ 
	IF @EventTypeID = 158294 
	BEGIN 

		EXEC _C600_MorphOOPToIPLeadEvent  @LeadEventID,158864,-1

	END 

	/*Make sure the workflow tasks are completed for the collections workflow*/ 
	/*We can't use standard method for this as it is a cross lead type workflow that doesn't have a process*/ 
	IF @EventTypeID IN (/*155261, */155246, 156850, 158843, 155248 ,158844 )
	BEGIN 
		IF EXISTS (SELECT * FROM Lead l with (NOLOCK) 
		WHERE l.LeadTypeID = 1493
		and l.LeadID = @LeadID) 
		BEGIN 
		
			SELECT @PolicyLeadID = ISNULL(dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@LeadID),0) 

		END 
		IF EXISTS (SELECT * FROM Lead l with (NOLOCK) 
		WHERE l.LeadTypeID = 1490
		and l.LeadID = @LeadID) 
		BEGIN 
		
			SELECT @ColLeadID = ISNULL(dbo.fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin(@LeadID),0) 

		END 

		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) 
		where w.LeadID IN (@LeadID,@ColLeadID,@PolicyLeadID) 
		AND w.WorkflowGroupID = 1233
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID,w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, @WhoCreated,dbo.fn_GetDate_Local(), 'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE w.LeadID IN (@LeadID,@ColLeadID,@PolicyLeadID) 
		AND w.WorkflowGroupID = 1233 
		AND w.WorkflowTaskID = @WorkflowTaskID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 

	END 

	IF @EventTypeID = 142119
	BEGIN	
		SELECT @WorkflowTaskID =  w.WorkflowTaskID from workflowtask w WITH (NOLOCK) 
		where w.LeadID IN (@LeadID,@ColLeadID,@PolicyLeadID) 
		AND w.WorkflowGroupID = 1235
		
		INSERT INTO WorkflowTaskCompleted (ClientID, WorkflowTaskID,WorkflowGroupID,AutomatedTaskID,Priority,AssignedTo,AssignedDate,LeadID,CaseID,EventTypeID,FollowUp,Important,CreationDate,Escalated,EscalatedBy,EscalationReason,EscalationDate,Disabled,CompletedBy,CompletedOn,CompletionDescription)
		SELECT w.ClientID, w.WorkflowTaskID, w.WorkflowGroupID, w.AutomatedTaskID, w.Priority, w.AssignedTo, w.AssignedDate, w.LeadID, w.CaseID, w.EventTypeID, w.FollowUp, w.Important, w.CreationDate, w.Escalated, w.EscalatedBy, w.EscalationReason, w.EscalationDate, w.Disabled, @WhoCreated, dbo.fn_GetDate_Local(), 'Inserted by Trigger'
		FROM WorkflowTask w WITH (NOLOCK) 
		WHERE w.LeadID IN (@LeadID,@ColLeadID,@PolicyLeadID) 
		AND w.WorkflowGroupID = 1235 
		AND w.WorkflowTaskID = @WorkflowTaskID

		DELETE FROM WorkflowTask 
		WHERE WorkflowTaskID = @WorkflowTaskID 

	END 

	/* GPR 2019-03-07 OOP Remove Override EvaluateByChangeSetID */
	IF @EventTypeID = 158900
	BEGIN
		EXEC _C00_SimpleValueIntoField 180235,NULL,@MatterID,@WhoCreated
	END

	IF @EventTypeID IN ( 158905 ) /*Update Document Fields  - UAH 21/03/2019 */
	BEGIN

		SELECT @LeadID = m.LeadID FROM Matter m WITH (NOLOCK) where m.MatterID = @MatterID
	select @leadid AS lEADiD
			
	/*Get the Billing ID for account details*/
	SELECT @ColMatterID = l.ToMatterID FROM LeadTypeRelationship l WITH (NOLOCK) 
	Where l.FromMatterID = @MatterID 
	and l.FromLeadTypeID = 1492
	--and l.ToLeadTypeID = 1490 -- Claims
    and l.ToLeadTypeID = 1493 -- Collections 

	
    Select @BillingID = dbo.fnGetSimpleDv(176973,@ColMatterID)
	
	/*Effective Date*/
	/*First deal with the date accident and illness claims become valid*/
	
	Select @Policyinceptiondate = dbo.fnGetSimpleDvAsDate(170035, @MatterID), /*Grab policy inception date*/
		   @EffectiveDate = DATEADD (Day,15,@Policyinceptiondate) /*Add 15 days to inception date to create effective date*/


	/*Stamp Effective Date to field */
	EXEC dbo._C00_SimpleValueIntoField 177328,@EffectiveDate,@MatterID,@WhoCreated

	/*- Period of Insurance – the end date of the policy should be 1 day before the anniversary, so 14/10/2016 – 13/10/2016 */
	Select @PolicyStartDate = dbo.fnGetSimpleDvAsDate(170036, @MatterID)
	

	/*Mask Payment Details*/
	Select @AccountNo = ISNULL(a.AccountNumber,''),  /*Account Number*/
		   @SortNo = ISNULL(a.Sortcode,'')
		   from  Account a WITH (NOLOCK) where a.AccountID = @BillingID; /*Account Sort Code*/	 
		     
		   	   
	Select @AccountNo = REPLICATE('*',LEN(@AccountNo)-4) + RIGHT(@AccountNo,4), /*Masks all but the last four characters of the account number with '*' */
		   @SortNo = REPLICATE('*',LEN(@SortNo)-4) + RIGHT(@SortNo,4); /*Masks all but the last four characters of the sort code with '*' */

	EXEC DBO._C00_SimpleValueIntoField 177356,@AccountNo,@MatterID,@WhoCreated
	EXEC DBO._C00_SimpleValueIntoField 177357,@SortNo,@MatterID,@WhoCreated

	/*Update Excess for Comp and TPL #59217*/

	EXEC _C600_PA_SetPostcodeGroupAndExcessForMatter @MatterID

	/*Payment Summary*/
	SELECT @PurchaseProductID  = MAX(P.PurchasedProductid) 
	FROM Matter m WITH (NOLOCK) /* SLACKY-PERF-20190705 */
	INNER JOIN PurchasedProduct pr WITH (NOLOCK) /* SLACKY-PERF-20190705 */ ON pr.ObjectID = m.MatterID
	INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) /* SLACKY-PERF-20190705 */ on pr.PurchasedProductID = p.PurchasedProductID
	where m.MatterID = @matterid
	   
      
	  SELECT @IntervalID = dbo.fnGetSimpleDvAsInt(170176,@MatterID)
	   
    IF @IntervalID = 69943 /*Monthly*/
    BEGIN 
         
         DECLARE @PaymentSchedule TABLE (ID INT IDENTITY (1,1) ,PaymentAmount DECIMAL(18,2), PaymentDate DATE, AmountDetailFieldID INT, DateDetailfieldID INT);
         INSERT INTO @PaymentSchedule (PaymentAmount,PaymentDate) 
         SELECT p.PaymentGross,p.PaymentDate  FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
         where p.PurchasedProductID = @PurchaseProductID 
         order by p.PaymentDate DESC

         Update @PaymentSchedule 
         SET AmountDetailFieldID =     CASE ID WHEN 1 THEN 177341
                                                   WHEN 2 THEN 177342
                                                   WHEN 3 THEN 177343
                                                   WHEN 4 THEN 177344
                                                   WHEN 5 THEN 177345
                                                   WHEN 6 THEN 177346
                                                   WHEN 7 THEN 177347
                                                   WHEN 8 THEN 177348
                                                   WHEN 9 THEN 177349
                                                   WHEN 10 THEN 177350
                                                   WHEN 11 THEN 177351
                                                   WHEN 12 THEN 177353
                                                   END,
         DateDetailfieldID = CASE ID WHEN 1 THEN 177329
                                                   WHEN 2 THEN 177330
                                                   WHEN 3 THEN 177331
                                                   WHEN 4 THEN 177332
                                                   WHEN 5 THEN 177333
                                                   WHEN 6 THEN 177334
                                                   WHEN 7 THEN 177335
                                                   WHEN 8 THEN 177336
                                                   WHEN 9 THEN 177337
                                                   WHEN 10 THEN 177338
                                                   WHEN 11 THEN 177339
                                                   WHEN 12 THEN 177340
                                                   END
         /*Write the dates*/ 
         INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue) 
         SELECT @ClientID,@LeadID,@MatterID,p.DateDetailfieldID, p.PaymentDate
         FROM @PaymentSchedule p 
         WHERE p.DateDetailFieldID is not NULL /*CPS 2017-08-21*/
         /*Write the amount*/ 
         INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue) 
         SELECT @ClientID,@LeadID,@MatterID,p.AmountDetailFieldID, p.PaymentAmount
         FROM @PaymentSchedule p 
         WHERE p.AmountDetailFieldID is not NULL /*CPS 2017-08-21*/
         
         --Get payment day from field 170168
		 SELECT @PaymentDay = dbo.fnGetSimpleDvAsInt(170168,@MatterID)
		 
		 --Stamp payment day into field 177388

		  EXEC _C00_SimpleValueIntoField 177388,@PaymentDay,@MatterID,@WhoCreated
		 --Stamp payment day into field 179962
		 EXEC _C00_SimpleValueIntoField 179962,@PaymentDay,@MatterID,@WhoCreated

		 --Stamp first collection date from field 180215
		 SELECT @FirstCollectionDate = dbo.fnGetSimpleDvAsDate(180215,@MatterID)
		 EXEC _C00_SimpleValueIntoField  177477,@FirstRenewalActualCollectionDate,@MatterID,@WhoCreated
     END
     ELSE /* IF @IntervalID <> 69943 'Monthly'  */
     BEGIN /*Annual*/  
         
         /*Only need to worry about the earliest payment*/
         SELECT @AnnualPrem = p.PaymentGross , @AnnualDate = p.PaymentDate FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
         where p.PurchasedProductID = @PurchaseProductID 
         and NOT EXISTS (SELECT * FROM PurchasedPRoductPaymentSchedule pp WITH (NOLOCK) where pp.PurchasedProductID = p.PurchasedProductID
                                 and pp.PaymentDate > p.PaymentDate)
                                 
         EXEC _C00_SimpleValueIntoField  177354,@AnnualPrem,@MatterID,@WhoCreated

         EXEC _C00_SimpleValueIntoField  177355,@AnnualDate,@MatterID,@WhoCreated

	END
	END

	IF @EventTypeID IN ( 156845 ) /*Payment Account Changed  - UAH 12/04/2019 */
	BEGIN

			SELECT @AccNumber = ISNULL(detailvalue,'') FROM MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = 170190

			SELECT @AccNumber = REPLICATE('*',LEN(@AccNumber)-4) + RIGHT(@AccNumber,4) /*Masks all but the last four characters of the account number with '*' */
	
			EXEC _C00_SimpleValueIntoField 177356, @AccNumber, @MatterID

			EXEC _C00_AddaNote  @CaseID, 949, 'Collections on another case. DO NOT USE.', 1, @AqAutomation /*added by NG 2020-09-28 for new account details process*/

	END

	/*Indemnity Claim Received*/
	IF @EventTypeID = 158997
	BEGIN

		EXEC dbo._C600_Collections_HandleIndemnityClaim @CustomerID, @MatterID, @WhoCreated

	END

	/*Change Claims Account*/
	IF @EventTypeID = 159002 
	BEGIN

		SELECT @AccountName = dbo.fnGetSimpleDv(314250, @LeadID),
				@AccNumber = dbo.fnGetSimpleDv(314248, @LeadID),
				@AccSortCode = dbo.fnGetSimpleDv(314247, @LeadID),
				@BankName = dbo.fnGetSimpleDv(314249, @LeadID),
				@InstitutionNumber = dbo.fnGetSimpleDv(315925, @LeadID) /*GPR 2021-04-29 for SDFURPHI-32*/

		SELECT TOP 1 @ClientAccountID = pp.ClientAccountID
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.ObjectID = @MatterID
		AND CONVERT(DATE, GETDATE()) BETWEEN pp.ValidFrom AND pp.ValidTo
		ORDER BY pp.PurchasedProductID DESC

		/*!!!!!!NO SQL HERE!!!!!!*/

		IF @@ROWCOUNT = 0
		BEGIN

			SELECT TOP 1 @ClientAccountID = pp.ClientAccountID
			FROM PurchasedProduct pp WITH (NOLOCK)
			WHERE pp.ObjectID = @MatterID
			ORDER BY pp.PurchasedProductID DESC

		END

		EXEC dbo.Billing__CreateClaimsAccountRecord @ClientID, 
													@LeadID, 
													1, 
													@AccNumber, 
													@AccSortCode, 
													@AccountName, 
													@ClientAccountID,
													@CustomerID,
													@WhoCreated,
													1, /*GPR 2020-10-06 changed to 1 from 2*/
													@BankName,
													@InstitutionNumber /*GPR 2021-04-29 for SDFURPHI-32*/

		/*GPR 2020-10-06 clear out Mandatory Fields*/
		EXEC _C00_SimpleValueIntoField 314247, '', @LeadID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 314248, '', @LeadID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 314250, '', @LeadID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 314249, '', @LeadID, @WhoCreated
		EXEC _C00_SimpleValueIntoField 314247, '', @LeadID, @WhoCreated /*GPR 2021-04-29 for SDFURPHI-32*/

		SELECT @OneVisionID = OneVisionID 
		FROM OneVision ov WITH (NOLOCK) 
		WHERE ov.PAMatterID = @MatterID
		AND ov.PolicyTermType = 'Policy'

        IF @OneVisionID IS NOT NULL 
        BEGIN

            EXEC _C600_Mediate_QueueForVision 'PaymentAccount', 'POST', @LeadEventID, @OneVisionID

        END

	END
	
	IF @EventTypeID = 160525
	BEGIN

		SELECT @BrainTreePaymentMethodCreateID = dbo.fnGetSimpleDvAsInt(315883, @MatterID)

		EXEC _C600_Braintree_HandleXMLResponse @MatterID, @WhoCreated, @BrainTreePaymentMethodCreateID

	END

	IF @EventTypeID = 160533
	BEGIN

		/*2021-03-30 ACE - FURKIN-403 - Clear the customer field "Account Type"*/
		EXEC dbo._C00_SimpleValueIntoField 170246, '', @CustomerID, @WhoCreated

	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAE] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SAE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAE] TO [sp_executeall]
GO
