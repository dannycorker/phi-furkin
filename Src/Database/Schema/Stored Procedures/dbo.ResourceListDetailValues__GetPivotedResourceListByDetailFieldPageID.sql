SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author: Paul Richardson
-- Create date: 2009-06-04
-- Description:	Gets a pivoted resource list 
-- 2011-08-01 SB  Change to return the lookup list value so filtering works correctly
-- 2013-09-03 JWG Cast ResourceListID to varchar so filtering works correctly
-- 2013-10-23 JWG New option to hide columns when not wanted #24087
-- 2014-07-28 SB  Extra param for clientID to handle shared lead types
-- =============================================
CREATE PROCEDURE [dbo].[ResourceListDetailValues__GetPivotedResourceListByDetailFieldPageID]
(	
	@ResourceListDetailFieldPageID INT, 
	@ShowHiddenColumns BIT = 0,
	@ClientID INT = NULL
)
AS
BEGIN

	DECLARE @cols VARCHAR(MAX)=''
	DECLARE @fieldIdents VARCHAR(MAX)=''
	
	SELECT @cols = @cols + ',[' + REPLACE(REPLACE(df.FieldCaption, '[', ''), ']', '') + ']',
	       @fieldIdents = @fieldIdents + ',' + CAST(DetailFieldID AS VARCHAR(50))
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID AND df.Enabled = 1 
	AND (df.Hidden = 0 OR @ShowHiddenColumns = 1) /* 2013-10-23 JWG #24087 */
	ORDER BY df.FieldOrder

	DECLARE @query VARCHAR(MAX)

	SELECT @cols=STUFF(@cols, 1, 1, ''), @fieldIdents=STUFF(@fieldIdents, 1, 1, '')
	
	SET @query = 'SELECT * From (
		SELECT CAST(rdv.ResourceListID AS VARCHAR(10)) AS ResourceListID, replace(replace(df.FieldCaption, ''['', ''''), '']'', '''') as FieldCaption, ISNULL(ll.ItemValue, rdv.DetailValue) as DetailValue
		FROM dbo.ResourceListDetailValues rdv with (nolock)
		INNER JOIN dbo.DetailFields df with (nolock) ON rdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID AND df.QuestionTypeID IN (2, 4)
		WHERE (df.DetailFieldID IN (' + @fieldIdents + ')) '
	
	IF @ClientID > 0
	BEGIN
		SELECT @query += 'AND rdv.ClientID = ' + CAST(@ClientID AS VARCHAR(10))
	END
		
	SELECT @query += ') src
	PIVOT (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt
	ORDER by pvt.ResourceListID
	'

	EXEC(@query)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues__GetPivotedResourceListByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues__GetPivotedResourceListByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues__GetPivotedResourceListByDetailFieldPageID] TO [sp_executeall]
GO
