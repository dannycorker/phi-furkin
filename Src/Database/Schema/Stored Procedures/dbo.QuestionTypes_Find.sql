SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuestionTypes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionTypes_Find]
(

	@SearchUsingOR bit   = null ,

	@QuestionTypeID int   = null ,

	@Name varchar (50)  = null ,

	@Description varchar (50)  = null ,

	@Multiselect bit   = null ,

	@EcatcherField bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuestionTypeID]
	, [Name]
	, [Description]
	, [Multiselect]
	, [EcatcherField]
    FROM
	[dbo].[QuestionTypes] WITH (NOLOCK) 
    WHERE 
	 ([QuestionTypeID] = @QuestionTypeID OR @QuestionTypeID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Multiselect] = @Multiselect OR @Multiselect IS NULL)
	AND ([EcatcherField] = @EcatcherField OR @EcatcherField IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuestionTypeID]
	, [Name]
	, [Description]
	, [Multiselect]
	, [EcatcherField]
    FROM
	[dbo].[QuestionTypes] WITH (NOLOCK) 
    WHERE 
	 ([QuestionTypeID] = @QuestionTypeID AND @QuestionTypeID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Multiselect] = @Multiselect AND @Multiselect is not null)
	OR ([EcatcherField] = @EcatcherField AND @EcatcherField is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionTypes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Find] TO [sp_executeall]
GO
