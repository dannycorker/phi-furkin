SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- ALTER date: 2008-12-01
-- Description:	Get Automated Tasks to run next in order.
-- JWG 2009-08-06 Avoid tasks due either side of midnight (23:55 to 00:05)
-- JWG 2009-08-18 Cater for multiple schedulers running at once.
--                Call AutomatedTask__PrepareEnabledTasksInOrder first, or you won't find any work to do!
-- JWG 2010-03-09 Avoid tasks due either side of Scheduler restart time (02:55) as well
-- JWG 2014-06-11 #27045 New proc AquariusMaster.dbo.GetIsMaintenanceWindow replaces the local function, to make it accessible from everywhere
--                Just add a record to the AquariusMaster.dbo.SpecialMaintenanceWindow table now if you need a long outage for any reason.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__GetEnabledTasksInOrder] 
	@QueueID int, 
	@SchedulerID int, 
	@ServerName varchar(50) 
AS
BEGIN
	
	--SELECT * FROM AutomatedTask

	/* 
		Don't allow any tasks to be returned when the current datetime
		is around midnight, specifically between 23:55:00 and 00:29:59
		This proc returns DAL objects, so no other columns are allowed.
		Also, no DECLARE statements etc.  
		     
		    Cutoff start = 2012-12-25 23:55:00
		    Time now     = 2012-12-25 23:58:00  so cannot run yet
		    Cutoff end   = 2012-12-26 00:05:59
	*/
	DECLARE @IsMaint BIT

	EXEC @IsMaint = [AquariusMaster].dbo.GetIsMaintenanceWindow
	
	/*
		JWG 2014-06-11 #27045 Use the common proc on AquariusMaster instead of each local DB having its own version.
		IF dbo.fnIsMaintenanceWindow(dbo.fn_GetDate_Local()) = 1
	*/
	SELECT
		at.*
	FROM [dbo].[AutomatedTask] at (nolock) 
	INNER JOIN dbo.AutomatedTaskInfo ati (nolock) ON ati.TaskID = at.TaskID 
	INNER JOIN dbo.AutomatedTaskQueue atq (nolock) ON atq.AutomatedTaskQueueID = ati.QueueID 
	WHERE ati.QueueID = @QueueID 
	AND ati.SchedulerID = @SchedulerID 
	AND ati.ServerName = @ServerName
	AND atq.[Enabled] = 1 
	AND ati.LockDateTime IS NOT NULL 
	AND @IsMaint = 0	
	/*AND dbo.fnIsMaintenanceWindow(dbo.fn_GetDate_Local()) = 0 */
	ORDER BY 
		at.[NextRunDateTime]
		
	SELECT @@ROWCOUNT
			
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__GetEnabledTasksInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__GetEnabledTasksInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__GetEnabledTasksInOrder] TO [sp_executeall]
GO
