SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AquariumOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOptionType_Update]
(

	@AquariumOptionTypeID int   ,

	@OriginalAquariumOptionTypeID int   ,

	@AquariumOptionTypeName varchar (50)  ,

	@AquariumOptionTypeDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AquariumOptionType]
				SET
					[AquariumOptionTypeID] = @AquariumOptionTypeID
					,[AquariumOptionTypeName] = @AquariumOptionTypeName
					,[AquariumOptionTypeDescription] = @AquariumOptionTypeDescription
				WHERE
[AquariumOptionTypeID] = @OriginalAquariumOptionTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOptionType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOptionType_Update] TO [sp_executeall]
GO
