SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-24
-- Description:	Mark an AutomatedEventQueue record as complete when the event has been applied
-- or...?
-- Description:	Delete a completed AutomatedEventQueue record when the event has been applied
-- or...?
-- Description:	This will never be required if we let trgiu_LeadEvent auto-complete these.  But that seems like a lot of trigger work for all the
--              millions of events that are not applied this way!  Maybe the scheduler could check that the previous event is still not followed up before adding the new one?
--              But then OOP events could not be part of this process, which is very limiting.  Far too limiting in fact.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__SetEventComplete]
	@AutomatedEventQueueID int
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		Mark an AutomatedEventQueue record as complete when the event has been applied
	*/
	/*DELETE dbo.AutomatedEventQueue 
	WHERE AutomatedEventQueueID = @AutomatedEventQueueID */
	UPDATE dbo.AutomatedEventQueue 
	SET SuccessDateTime = dbo.fn_GetDate_Local() 
	WHERE AutomatedEventQueueID = @AutomatedEventQueueID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__SetEventComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__SetEventComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__SetEventComplete] TO [sp_executeall]
GO
