SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-26
-- Description:	Create a new user account in client X based on a combination of existing user details
-- UPDATES:		Simon Brushett, 2011-12-08, Changed to call into the CP_Insert_Helper proc as direct insert into CP no longer allowed
-- =============================================
CREATE PROCEDURE [dbo].[_C38_CopyUserToClient]
	@UserBeingCloned int,
	@NewStarter_ExistingUserIDInAnotherClient int
AS
BEGIN
	SET NOCOUNT ON;

    /* 
		This proc assumes that "John Smith" has a user account within
		at least one client already, say client 4, and wants a client zero account.
		
		It will take the username, password, salt and contact details from that user record,
		and all the remaining details from an existing client 0 user that you specify.
    */
    
    DECLARE @ClientID INT,
			@OfficeID INT,
			@TitleID INT,
			@Firstname VARCHAR(100),
			@Middlename VARCHAR(100),
			@Lastname VARCHAR(100),
			@JobTitle VARCHAR(100),
			@Password VARCHAR(65),
			@ClientPersonnelAdminGroupID INT,
			@MobileTelephone VARCHAR(50),
			@HomeTelephone VARCHAR(50),
			@OfficeTelephone VARCHAR(50),
			@OfficeTelephoneExtension VARCHAR(50),
			@UserName VARCHAR(200),
			@ChargeOutRate MONEY,
			@Salt VARCHAR(50),
			@ManagerID INT,
			@LanguageID INT,
			@SubClientID INT,
			@CPID1 INT,
			@CPID2 INT,
			@ForcePasswordChangeOn DATETIME = NULL,
			@ThirdPartySystemId INT = 0,
			@CustomerID INT = NULL,
			@IsAquarium BIT = 0
			
	SELECT	@ClientID = cp_UserBeingCloned.[ClientID],
			@OfficeID = cp_UserBeingCloned.[ClientOfficeID],
			@TitleID = cp_NewStarter.[TitleID],
			@Firstname = cp_NewStarter.[FirstName],
			@Middlename = cp_NewStarter.[MiddleName],
			@Lastname = cp_NewStarter.[LastName],
			@JobTitle = cp_NewStarter.[JobTitle],
			@Password = cp_NewStarter.[Password],
			@ClientPersonnelAdminGroupID = cp_UserBeingCloned.[ClientPersonnelAdminGroupID],
			@MobileTelephone = cp_NewStarter.[MobileTelephone],
			@HomeTelephone = cp_NewStarter.[HomeTelephone],
			@OfficeTelephone = cp_UserBeingCloned.[OfficeTelephone],
			@OfficeTelephoneExtension = cp_UserBeingCloned.[OfficeTelephoneExtension],
			@UserName = cp_NewStarter.[FirstName] + '.' + cp_NewStarter.[LastName] + CAST(cp_UserBeingCloned.[ClientID] AS VARCHAR) + '@aquarium-software.com',
			@ChargeOutRate = cp_NewStarter.[ChargeOutRate],
			@Salt = cp_NewStarter.[Salt],
			@ManagerID = cp_UserBeingCloned.[ManagerID],
			@LanguageID = cp_UserBeingCloned.[LanguageID],
			@SubClientID = cp_UserBeingCloned.[SubClientID],
			@CPID1 = cp_UserBeingCloned.ClientPersonnelID,
			@CPID2 = cp_NewStarter.ClientPersonnelID,
			@ForcePasswordChangeOn = cp_UserBeingCloned.ForcePasswordChangeOn,
			@ThirdPartySystemId = cp_UserBeingCloned.ThirdPartySystemId,
			@CustomerID = cp_UserBeingCloned.CustomerID,
			@IsAquarium = cp_UserBeingCloned.IsAquarium
	FROM [dbo].[ClientPersonnel] cp_UserBeingCloned
	CROSS JOIN [dbo].[ClientPersonnel] cp_NewStarter 
	WHERE cp_UserBeingCloned.ClientPersonnelID = @UserBeingCloned
	AND cp_NewStarter.ClientPersonnelID = @NewStarter_ExistingUserIDInAnotherClient
    
    IF @CPID1 > 0 AND @CPID2 > 0
    BEGIN
    
		EXEC dbo.ClientPersonnel_Insert_Helper	@ClientID, @FirstName, @LastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @MiddleName, @JobTitle, @ClientPersonnelAdminGroupID, 
												@MobileTelephone, @HomeTelephone, @OfficeTelephone, @OfficeTelephoneExtension, @ChargeOutRate, 0, 0, @ManagerID, @LanguageID, @SubClientID,
												@ForcePasswordChangeOn, @ThirdPartySystemId, @CustomerID, @IsAquarium 
    
    END  
    
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CopyUserToClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_CopyUserToClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CopyUserToClient] TO [sp_executeall]
GO
