SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeDefinition table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDefinition_GetByChangeDefinitionID]
(

	@ChangeDefinitionID int   
)
AS


				SELECT
					[ChangeDefinitionID],
					[ClientID],
					[ChangeDescription],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[RequiredDate],
					[Deleted]
				FROM
					[dbo].[ChangeDefinition] WITH (NOLOCK) 
				WHERE
										[ChangeDefinitionID] = @ChangeDefinitionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_GetByChangeDefinitionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDefinition_GetByChangeDefinitionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_GetByChangeDefinitionID] TO [sp_executeall]
GO
