SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-16
-- Description:	Update Class Node
-- =============================================
CREATE PROCEDURE [dbo].[ClassNode__Update] 
	@ClientPersonnelID INT = NULL,
	@ClassNodeID INT = NULL,
	@ClientID INT = NULL,
	@ParentClassNodeID INT = NULL,
	@NodeType INT = NULL,
	@Name VARCHAR(255) = NULL,
	@ClassDescription VARCHAR(255) = NULL,
	@NodeOrder INT = NULL
	-- @WhoCreated = No change
	-- @WhenCreated = No change
	-- @WhoModified = ClientPersonnelID,
	-- @WhenModified = Current DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE cn
	SET
		cn.[ClientID] = @ClientID,
		cn.[ParentClassNodeID] = @ParentClassNodeID,
		cn.[NodeType] = @NodeType,
		cn.[Name] = @Name,
		cn.[ClassDescription] = @ClassDescription,
		cn.[NodeOrder] = @NodeOrder,
		cn.[WhoModified] = @ClientPersonnelID,
		cn.[WhenModified] = dbo.fn_GetDate_Local()
	FROM [dbo].[ClassNode] AS cn
	INNER JOIN [ClientPersonnel] AS cp
		ON cp.[ClientID] = cn.[ClientID]
	WHERE
		[ClassNodeID] = @ClassNodeID
		AND @ClientPersonnelID = cp.ClientPersonnelID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode__Update] TO [sp_executeall]
GO
