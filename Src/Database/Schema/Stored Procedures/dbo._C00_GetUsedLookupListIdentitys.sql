SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-11-2013
-- Description:	Gets a list of lookup lists that are in use
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetUsedLookupListIdentitys]

	@ClientID INT

AS
BEGIN

	SELECT DISTINCT(LookupListID) 
	FROM DetailFields WITH (NOLOCK) 
	WHERE LookupListID IS NOT NULL AND 
	ClientID=@ClientID AND Enabled=1
	
END	



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetUsedLookupListIdentitys] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetUsedLookupListIdentitys] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetUsedLookupListIdentitys] TO [sp_executeall]
GO
