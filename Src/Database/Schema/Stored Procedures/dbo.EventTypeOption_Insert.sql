SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeOption_Insert]
(

	@EventTypeOptionID int    OUTPUT,

	@ClientID int   ,

	@EventTypeID int   ,

	@AquariumOptionID int   ,

	@OptionValue varchar (50)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[EventTypeOption]
					(
					[ClientID]
					,[EventTypeID]
					,[AquariumOptionID]
					,[OptionValue]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@EventTypeID
					,@AquariumOptionID
					,@OptionValue
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@SourceID
					)
				-- Get the identity value
				SET @EventTypeOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeOption_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeOption_Insert] TO [sp_executeall]
GO
