SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SubClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubClient_Update]
(

	@SubClientID int   ,

	@ClientID int   ,

	@CompanyName varchar (100)  ,

	@ClientTypeID int   ,

	@LanguageID int   ,

	@CountryID int   ,

	@WhenCreated datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SubClient]
				SET
					[ClientID] = @ClientID
					,[CompanyName] = @CompanyName
					,[ClientTypeID] = @ClientTypeID
					,[LanguageID] = @LanguageID
					,[CountryID] = @CountryID
					,[WhenCreated] = @WhenCreated
				WHERE
[SubClientID] = @SubClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubClient_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Update] TO [sp_executeall]
GO
