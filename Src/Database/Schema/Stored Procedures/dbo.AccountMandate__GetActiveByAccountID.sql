SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger/Paul Richardson
-- Create date: 16/11/2016
-- Description:	Gets an account mandate
-- 2020-03-02	GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- 2020-07-21	GPR inc United States in Country IF block
-- =============================================
CREATE PROCEDURE [dbo].[AccountMandate__GetActiveByAccountID]
	@AccountID INT, 
	@ClientID INT
AS
BEGIN
	
	SET NOCOUNT ON;

	/*GPR 2020-03-02 for AAG-202*/
	DECLARE @CountryID INT
	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	IF @CountryID IN (14 /*Australia*/, 233 /*United States*/)
	BEGIN

		SELECT 1 AS [MandateStatusID]
		RETURN 
	END
	ELSE
	BEGIN
	
		SELECT TOP 1 * 
		FROM AccountMandate WITH (NOLOCK) 
		WHERE AccountID=@AccountID 
		AND ClientID=@ClientID 
		AND MandateStatusID=3
    END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountMandate__GetActiveByAccountID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccountMandate__GetActiveByAccountID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountMandate__GetActiveByAccountID] TO [sp_executeall]
GO
