SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-05-2015
-- Description:	Script Import - imports customer,lead,case,matter, contacts, departments, offices and detail values
-- @ActionToPerform : when a duplicate is found
		-- Keep_Existing = 1,  Keep the existing record and reject the new one - so essentially do nothing!
        -- Keep_New = 2,       Keep the new record and remove the existing
        -- Reset_Older = 3,    Update the existing record with the new information        
        -- Insert = 4          Inserts the new record regardless of duplicate
        -- NewCase = 5			Creates a new case and updates the current customer information CS 2015-11-08
-- Added defaults for Test and IsBusiness       
-- Removed the lead type table since we are only inserting one lead for the given lead type now (29-05-2015) 
-- CS 2015-11-08.	Allow this procedure to add a new case to an existing customer, if found.
--					Also handle searching by ValueMoney.  Necessary for Dialler customers like C379
-- PR 2016-11-24	Added @EMPTY_XML_LENGTH
-- =============================================
CREATE PROCEDURE [dbo].[VariableHeaderDataLoad__Import]
	@ClientID INT,	
	@ThirdPartyLeadID INT,
	@ListID INT,
	@ImportID INT,
	@ActionToPerform INT,
	@CustomerXml XML,
	@LeadXml XML,
	@CaseXml XML,
	@MatterXml XML,
	@PartnerXml XML,
	@OfficeXml XML,
	@DepartmentXml XML,
	@ContactXml XML,
	@DetailValuesXml XML,
	@UserID INT,
	@LeadTypeID INT
AS
BEGIN	
	
	SET NOCOUNT ON;
						
	DECLARE @EMPTY_XML_LENGTH INT = 5
	
	DECLARE @Customer_TitleID INT
	DECLARE @Customer_IsBusiness BIT
	DECLARE @Customer_FirstName VARCHAR(100)
	DECLARE @Customer_MiddleName VARCHAR(100)
	DECLARE @Customer_LastName VARCHAR(100) 
	DECLARE @Customer_EmailAddress VARCHAR(255)
	DECLARE @Customer_DayTimeTelephoneNumber VARCHAR(50)
	DECLARE @Customer_HomeTelephone VARCHAR(50)
	DECLARE @Customer_MobileTelephone VARCHAR(50)
	DECLARE @Customer_CompanyTelephone VARCHAR(50) 
	DECLARE @Customer_WorksTelephone VARCHAR(50)
	DECLARE @Customer_Address1 VARCHAR(200)
	DECLARE @Customer_Address2 VARCHAR(200) 
	DECLARE @Customer_Town VARCHAR(200)
	DECLARE @Customer_County VARCHAR(200)
	DECLARE @Customer_PostCode VARCHAR(50)
	DECLARE @Customer_Website VARCHAR(200)
	DECLARE @Customer_AquariumStatusID INT
	DECLARE @Customer_ClientStatusID INT
	DECLARE @Customer_Test BIT
	DECLARE @Customer_CompanyName VARCHAR(100)
	DECLARE @Customer_Occupation VARCHAR(100)
	DECLARE @Customer_Employer VARCHAR(100)
	DECLARE @Customer_DoNotEmail BIT
	DECLARE @Customer_DoNotSellToThirdParty BIT
	DECLARE @Customer_AgreedToTermsAndConditions BIT
	DECLARE @Customer_DateOfBirth DATETIME 
	DECLARE @Customer_DefaultContactID INT
	DECLARE @Customer_DefaultOfficeID INT 
	DECLARE @Customer_CountryID INT
	DECLARE @Customer_SubClientID INT
	DECLARE @Customer_CustomerRef VARCHAR(100)
	
			
	SELECT	@Customer_TitleID = @CustomerXml.value('Customer[1]/TitleID[1]','INT'),
			@Customer_IsBusiness = @CustomerXml.value('Customer[1]/IsBusiness[1]','BIT'),
			@Customer_FirstName = @CustomerXml.value('Customer[1]/FirstName[1]','VARCHAR(100)'),
			@Customer_MiddleName = @CustomerXml.value('Customer[1]/MiddleName[1]','VARCHAR(100)'),
			@Customer_LastName = @CustomerXml.value('Customer[1]/LastName[1]','VARCHAR(100)'),
			@Customer_EmailAddress = @CustomerXml.value('Customer[1]/EmailAddress[1]','VARCHAR(255)'),
			@Customer_DayTimeTelephoneNumber = @CustomerXml.value('Customer[1]/DayTimeTelephoneNumber[1]','VARCHAR(50)'),
			@Customer_HomeTelephone = @CustomerXml.value('Customer[1]/HomeTelephone[1]','VARCHAR(50)'),
			@Customer_MobileTelephone = @CustomerXml.value('Customer[1]/MobileTelephone[1]','VARCHAR(50)'),
			@Customer_CompanyTelephone = @CustomerXml.value('Customer[1]/CompanyTelephone[1]','VARCHAR(50)'),
			@Customer_WorksTelephone = @CustomerXml.value('Customer[1]/WorksTelephone[1]','VARCHAR(50)'),
			@Customer_Address1 = @CustomerXml.value('Customer[1]/Address1[1]','VARCHAR(200)'),
			@Customer_Address2 = @CustomerXml.value('Customer[1]/Address2[1]','VARCHAR(200)'),
			@Customer_Town = @CustomerXml.value('Customer[1]/Town[1]','VARCHAR(200)'),
			@Customer_County = @CustomerXml.value('Customer[1]/County[1]','VARCHAR(200)'),
			@Customer_PostCode = @CustomerXml.value('Customer[1]/PostCode[1]','VARCHAR(50)'),
			@Customer_Website = @CustomerXml.value('Customer[1]/Website[1]','VARCHAR(200)'),
			@Customer_AquariumStatusID = @CustomerXml.value('Customer[1]/AquariumStatusID[1]','INT'),
			@Customer_ClientStatusID = @CustomerXml.value('Customer[1]/ClientStatusID[1]','INT'),
			@Customer_Test = @CustomerXml.value('Customer[1]/Test[1]','BIT'),
			@Customer_CompanyName = @CustomerXml.value('Customer[1]/CompanyName[1]','VARCHAR(100)'),
			@Customer_Occupation = @CustomerXml.value('Customer[1]/Occupation[1]','VARCHAR(100)'),
			@Customer_Employer = @CustomerXml.value('Customer[1]/Employer[1]','VARCHAR(100)'),
			@Customer_DoNotEmail = @CustomerXml.value('Customer[1]/DoNotEmail[1]','BIT'),
			@Customer_DoNotSellToThirdParty = @CustomerXml.value('Customer[1]/DoNotSellToThirdParty[1]','BIT'),
			@Customer_AgreedToTermsAndConditions = @CustomerXml.value('Customer[1]/AgreedToTermsAndConditions[1]','BIT'),
			@Customer_DateOfBirth = @CustomerXml.value('Customer[1]/DateOfBirth[1]','DATETIME'),
			@Customer_DefaultContactID = @CustomerXml.value('Customer[1]/DefaultContactID[1]','INT'),
			@Customer_DefaultOfficeID = @CustomerXml.value('Customer[1]/DefaultOfficeID[1]','INT'),
			@Customer_CountryID = @CustomerXml.value('Customer[1]/CountryID[1]','INT'),
			@Customer_SubClientID = @CustomerXml.value('Customer[1]/SubClientID[1]','INT'),
			@Customer_CustomerRef = @CustomerXml.value('Customer[1]/CustomerRef[1]','VARCHAR(100)')

	
	DECLARE @Lead_LeadRef VARCHAR(100)
	DECLARE @Lead_AquariumStatusID INT
	
	SELECT	@Lead_LeadRef = @LeadXml.value('Lead[1]/LeadRef[1]','VARCHAR(100)'),
			@Lead_AquariumStatusID = @LeadXml.value('Lead[1]/AquariumStatusID[1]','INT')


	DECLARE @Case_CaseRef VARCHAR(100)
	DECLARE @Case_AquariumStatusID INT
	
	SELECT	@Case_CaseRef = @CaseXml.value('Case[1]/CaseRef[1]','VARCHAR(100)'),
			@Case_AquariumStatusID = @CaseXml.value('Case[1]/AquariumStatusID[1]','INT')

	
	DECLARE @Matter_MatterRef VARCHAR(100)
	
	SELECT	@Matter_MatterRef = @MatterXml.value('Case[1]/MatterRef[1]','VARCHAR(100)')
			
	DECLARE @Department_DepartmentName VARCHAR(100)
	DECLARE @Department_DepartmentDescription VARCHAR(100)
	
	SELECT	@Department_DepartmentName = @DepartmentXml.value('Department[1]/DepartmentName[1]','VARCHAR(100)'),
			@Department_DepartmentDescription = @DepartmentXml.value('Department[1]/DepartmentDescription[1]','VARCHAR(100)')
			
	DECLARE @Office_OfficeName VARCHAR(100)
	DECLARE @Office_MainPhoneNumber VARCHAR(100)
	DECLARE @Office_FaxNumber VARCHAR(100)
	DECLARE @Office_Address1 VARCHAR(100)
	DECLARE @Office_Address2 VARCHAR(100)
	DECLARE @Office_Town VARCHAR(100)
	DECLARE @Office_County VARCHAR(100)
	DECLARE @Office_Postcode VARCHAR(100)
	DECLARE @Office_Country VARCHAR(100)
	DECLARE @Office_Notes VARCHAR(255)

	SELECT	@Office_OfficeName = @OfficeXml.value('Office[1]/OfficeName[1]','VARCHAR(100)'),
			@Office_MainPhoneNumber = @OfficeXml.value('Office[1]/MainPhoneNumber[1]','VARCHAR(100)'),
			@Office_FaxNumber = @OfficeXml.value('Office[1]/FaxNumber[1]','VARCHAR(100)'),
			@Office_Address1 = @OfficeXml.value('Office[1]/Address1[1]','VARCHAR(100)'),
			@Office_Address2 = @OfficeXml.value('Office[1]/Address2[1]','VARCHAR(100)'),
			@Office_Town = @OfficeXml.value('Office[1]/Town[1]','VARCHAR(100)'),
			@Office_County = @OfficeXml.value('Office[1]/County[1]','VARCHAR(100)'),
			@Office_Postcode = @OfficeXml.value('Office[1]/PostCode[1]','VARCHAR(100)'),
			@Office_Country = @OfficeXml.value('Office[1]/Country[1]','VARCHAR(100)'),
			@Office_Notes = @OfficeXml.value('Office[1]/Notes[1]','VARCHAR(255)')
			
	DECLARE @Contact_Title INT
	DECLARE @Contact_FirstName VARCHAR(100)
    DECLARE @Contact_MiddleName VARCHAR(100)
    DECLARE @Contact_LastName VARCHAR(100)
    DECLARE @Contact_EmailAddressWork VARCHAR(100)
    DECLARE @Contact_EmailAddressOther VARCHAR(100)
    DECLARE @Contact_DirectDial VARCHAR(100)
    DECLARE @Contact_MobilePhoneWork VARCHAR(100)
    DECLARE @Contact_MobilePhoneOther VARCHAR(100)
    DECLARE @Contact_Address1 VARCHAR(100)
    DECLARE @Contact_Address2 VARCHAR(100)
    DECLARE @Contact_Town VARCHAR(100)
    DECLARE @Contact_County VARCHAR(100)
    DECLARE @Contact_PostCode VARCHAR(100)
    DECLARE @Contact_Country VARCHAR(100)
    DECLARE @Contact_JobTitle VARCHAR(100)
    DECLARE @Contact_Notes VARCHAR(255)
    DECLARE @Contact_CountryID INT
    
    SELECT	@Contact_Title = @ContactXml.value('Contact[1]/Title[1]','INT'),
			@Contact_FirstName = @ContactXml.value('Contact[1]/FirstName[1]','VARCHAR(100)'),
			@Contact_MiddleName = @ContactXml.value('Contact[1]/MiddleName[1]','VARCHAR(100)'),
			@Contact_LastName = @ContactXml.value('Contact[1]/LastName[1]','VARCHAR(100)'),
            @Contact_EmailAddressWork = @ContactXml.value('Contact[1]/EmailAddressWork[1]','VARCHAR(100)'),
			@Contact_EmailAddressOther = @ContactXml.value('Contact[1]/EmailAddressOther[1]','VARCHAR(100)'),
			@Contact_DirectDial = @ContactXml.value('Contact[1]/DirectDial[1]','VARCHAR(100)'),
			@Contact_MobilePhoneWork = @ContactXml.value('Contact[1]/MobilePhoneWork[1]','VARCHAR(100)'),
			@Contact_MobilePhoneOther = @ContactXml.value('Contact[1]/MobilePhoneOther[1]','VARCHAR(100)'),
			@Contact_Address1 = @ContactXml.value('Contact[1]/Address1[1]','VARCHAR(100)'),
			@Contact_Address2 = @ContactXml.value('Contact[1]/Address2[1]','VARCHAR(100)'),
			@Contact_Town = @ContactXml.value('Contact[1]/Town[1]','VARCHAR(100)'),
			@Contact_County = @ContactXml.value('Contact[1]/County[1]','VARCHAR(100)'),
			@Contact_PostCode = @ContactXml.value('Contact[1]/PostCode[1]','VARCHAR(100)'),
			@Contact_Country = @ContactXml.value('Contact[1]/Country[1]','VARCHAR(100)'),
			@Contact_JobTitle = @ContactXml.value('Contact[1]/JobTitle[1]','VARCHAR(100)'),
			@Contact_Notes = @ContactXml.value('Contact[1]/Notes[1]','VARCHAR(255)'),
			@Contact_CountryID = @ContactXml.value('Contact[1]/CountryID[1]','INT')
    
    DECLARE @Partner_UseCustomerAddress BIT
    DECLARE @Partner_Title INT
    DECLARE @Partner_FirstName VARCHAR(100)
    DECLARE @Partner_MiddleName VARCHAR(100)
    DECLARE @Partner_LastName VARCHAR(100)
    DECLARE @Partner_EmailAddress VARCHAR(255)
    DECLARE @Partner_DayTimeTelephoneNumber VARCHAR(50)
    DECLARE @Partner_HomeTelephone VARCHAR(50)
    DECLARE @Partner_MobileTelephone VARCHAR(50)
    DECLARE @Partner_Address1 VARCHAR(200)
    DECLARE @Partner_Address2 VARCHAR(200)
    DECLARE @Partner_Town VARCHAR(200)
    DECLARE @Partner_County VARCHAR(200)
    DECLARE @Partner_PostCode VARCHAR(50)
    DECLARE @Partner_Occupation VARCHAR(100)
    DECLARE @Partner_Employer VARCHAR(100)
    DECLARE @Partner_DateOfBirth DATETIME

	SELECT	@Partner_UseCustomerAddress = @PartnerXml.value('Partner[1]/UseCustomerAddress[1]','INT'),
			@Partner_Title = @PartnerXml.value('Partner[1]/Title[1]','INT'),
			@Partner_FirstName = @PartnerXml.value('Partner[1]/FirstName[1]','VARCHAR(100)'),
			@Partner_MiddleName = @PartnerXml.value('Partner[1]/MiddleName[1]','VARCHAR(100)'),
			@Partner_LastName = @PartnerXml.value('Partner[1]/LastName[1]','VARCHAR(100)'),
			@Partner_EmailAddress = @PartnerXml.value('Partner[1]/EmailAddress[1]','VARCHAR(255)'),
			@Partner_DayTimeTelephoneNumber = @PartnerXml.value('Partner[1]/DayTimeTelephoneNumber[1]','VARCHAR(50)'),
			@Partner_HomeTelephone = @PartnerXml.value('Partner[1]/HomeTelephone[1]','VARCHAR(50)'),
			@Partner_MobileTelephone = @PartnerXml.value('Partner[1]/MobileTelephone[1]','VARCHAR(50)'),
			@Partner_Address1 = @PartnerXml.value('Partner[1]/Address1[1]','VARCHAR(200)'),
			@Partner_Address2 = @PartnerXml.value('Partner[1]/Address2[1]','VARCHAR(200)'),
			@Partner_Town = @PartnerXml.value('Partner[1]/Town[1]','VARCHAR(200)'),
			@Partner_County = @PartnerXml.value('Partner[1]/County[1]','VARCHAR(200)'),
			@Partner_PostCode = @PartnerXml.value('Partner[1]/PostCode[1]','VARCHAR(50)'),
			@Partner_Occupation = @PartnerXml.value('Partner[1]/Occupation[1]','VARCHAR(100)'),
			@Partner_Employer = @PartnerXml.value('Partner[1]/Employer[1]','VARCHAR(100)'),
			@Partner_DateOfBirth  = @PartnerXml.value('Partner[1]/DateOfBirth[1]','DATETIME')
				
	DECLARE  @MatterID		INT
			,@LeadID		INT
			,@ValueMoney	MONEY

	-- Does this customer already exist				
	DECLARE @ExistingCustomerID INT
	EXEC @ExistingCustomerID = Script__GetCustomerIDViaThirdPartyLeadID @ClientID, @ThirdPartyLeadID
	

	DECLARE @NewCustomerID INT	
	DECLARE @TheDateTime DATETIME = dbo.fn_GetDate_Local()
	DECLARE @DepartmentID INT
	DECLARE @OfficeID INT
	DECLARE @ContactID INT

	DECLARE @ExecBatchFields VARCHAR(MAX) = '';
	DECLARE @FieldValue TABLE
	(
		FieldID INT,		
		LeadTypeID INT,
		LeadOrMatter INT,
		CustomerID INT,
		LeadID INT,
		CaseID INT,
		MatterID INT,
		FieldData VARCHAR(2000)
	)		
	DECLARE @CreateNewCustomer BIT = 0
	DECLARE @PartnerID INT
	DECLARE @DefaultOfficeID INT
	DECLARE @DefaultContactID INT	
	
	DECLARE @FieldID_ThirdpartyLeadID INT --Thirdparty field:4200-- these are always customer level fields
	DECLARE @FieldID_ListID INT		      --Thirdparty field:4201
	DECLARE @FieldID_ImportID INT		  --Thirdparty field:4202

	IF @Customer_CountryID = 0
	BEGIN
		SET @Customer_CountryID = 232 -- default to United Kingdon
	END
	
	IF @Customer_TitleID IS NULL
	BEGIN 
		SET @Customer_TitleID = 0 -- default to unknown
	END
	
	IF @Contact_Title IS NULL
	BEGIN 
		SET @Contact_Title = 0 -- default to unknown
	END	

	IF @Partner_Title IS NULL
	BEGIN 
		SET @Partner_Title = 0 -- default to unknown
	END
	
	IF @Partner_UseCustomerAddress IS NULL
	BEGIN
		SET @Partner_UseCustomerAddress = 0
	END	
	
	IF @Customer_AquariumStatusID IS NULL OR @Customer_AquariumStatusID = 0
	BEGIN
		SET @Customer_AquariumStatusID = 2
	END
	
	IF @Customer_IsBusiness IS NULL
	BEGIN
		SET @Customer_IsBusiness = 0
	END

	IF @Customer_Test IS NULL
	BEGIN
		SET @Customer_Test = 0
	END	
	
	IF @ExistingCustomerID > 0 -- customer exists so perform action 	
	BEGIN
		
		-- Keep_Existing = 1,  Keep the existing record and reject the new one - so essentially do nothing!
        
        IF @ActionToPerform = 2 -- Keep_New = 2,       Keep the new record and remove the existing
        BEGIN
			UPDATE Customers -- remove existing one by setting it to test
			SET Test=1
			WHERE CustomerID=@ExistingCustomerID
			SET @CreateNewCustomer = 1
        END
        
		IF @ActionToPerform IN(3,5) -- Reset_Older = 3,    Update the existing record with the new information | NewCase = 5
        BEGIN        
        
			IF(DATALENGTH(@CustomerXml)>@EMPTY_XML_LENGTH)
			BEGIN			
				UPDATE Customers
				SET TitleID=@Customer_TitleID, 
					IsBusiness=@Customer_IsBusiness, 
					FirstName=@Customer_FirstName, 
					MiddleName=@Customer_MiddleName, 
					LastName=@Customer_LastName, 
					EmailAddress=@Customer_EmailAddress, 
					DayTimeTelephoneNumber=@Customer_DayTimeTelephoneNumber, 
					HomeTelephone=@Customer_HomeTelephone,
					MobileTelephone=@Customer_MobileTelephone, 
					CompanyTelephone=@Customer_CompanyTelephone, 
					WorksTelephone=@Customer_WorksTelephone, 
					Address1=@Customer_Address1, 
					Address2=@Customer_Address2, 
					Town=@Customer_Town, 
					County=@Customer_County, 
					PostCode=@Customer_PostCode, 
					Website=@Customer_Website, 
					AquariumStatusID=@Customer_AquariumStatusID, 
					ClientStatusID=@Customer_ClientStatusID, 
					Test=@Customer_Test, 
					CompanyName=@Customer_CompanyName, 
					Occupation=@Customer_Occupation, 
					Employer=@Customer_Employer, 
					DoNotEmail=@Customer_DoNotEmail, 
					DoNotSellToThirdParty=@Customer_DoNotSellToThirdParty, 
					AgreedToTermsAndConditions=@Customer_AgreedToTermsAndConditions, 
					DateOfBirth=@Customer_DateOfBirth, 				
					CustomerRef=@Customer_CustomerRef, 
					WhoChanged=@UserID, 
					WhenChanged=dbo.fn_GetDate_Local() 
				WHERE CustomerID=@ExistingCustomerID
			END
			
			IF (DATALENGTH(@DepartmentXml)>@EMPTY_XML_LENGTH) -- Department information
			BEGIN
				SELECT @DepartmentID=DepartmentID FROM Department WITH (NOLOCK) WHERE CustomerID=@ExistingCustomerID
				IF(@DepartmentID>0)
				BEGIN
					UPDATE Department
					SET DepartmentName=@Department_DepartmentName, 
						DepartmentDescription=@Department_DepartmentDescription
					WHERE DepartmentID=@DepartmentID					
				END				
			END					
			
			IF (DATALENGTH(@OfficeXml)>@EMPTY_XML_LENGTH) -- Office information
			BEGIN
				SELECT @DefaultOfficeID=DefaultOfficeID FROM Customers WITH (NOLOCK) WHERE CustomerID=@ExistingCustomerID
				IF(@DefaultOfficeID>0)
				BEGIN
					UPDATE CustomerOffice
					SET OfficeName=@Office_OfficeName, 
						MainPhoneNumber=@Office_MainPhoneNumber, 
						FaxNumber=@Office_FaxNumber, 
						Address1=@Office_Address1, 
						Address2=@Office_Address2, 
						Town=@Office_Town, 
						County=@Office_County, 
						Postcode=@Office_Postcode, 
						Country=@Office_Country, 
						Notes=@Office_Notes						
					WHERE OfficeID=@DefaultOfficeID					
				END
			END
			
			IF (DATALENGTH(@ContactXml)>@EMPTY_XML_LENGTH) -- Contact information
			BEGIN
				SELECT @DefaultContactID=DefaultContactID FROM Customers WITH (NOLOCK) WHERE CustomerID=@ExistingCustomerID
				IF(@DefaultContactID>0)
				BEGIN
					UPDATE Contact
					SET TitleID=@Contact_Title, 
						Firstname=@Contact_FirstName, 
						Middlename=@Contact_MiddleName, 
						Lastname=@Contact_LastName, 
						EmailAddressWork=@Contact_EmailAddressWork, 
						EmailAddressOther=@Contact_EmailAddressOther, 
						DirectDial=@Contact_DirectDial, 
						MobilePhoneWork=@Contact_MobilePhoneWork, 
						MobilePhoneOther=@Contact_MobilePhoneOther, 
						Address1=@Contact_Address1, 
						Address2=@Contact_Address2, 
						Town=@Contact_Town, 
						County=@Contact_County, 
						Postcode=@Contact_PostCode, 
						Country=@Contact_Country, 
						JobTitle=@Contact_JobTitle, 
						Notes=@Contact_Notes
					WHERE ContactID=@DefaultContactID					
				END
			END
			
			IF (DATALENGTH(@PartnerXml)>@EMPTY_XML_LENGTH) -- Partner information
			BEGIN
				SELECT @PartnerID=PartnerID FROM Partner WITH (NOLOCK) WHERE CustomerID=@ExistingCustomerID
				IF(@PartnerID>0)
				BEGIN
					UPDATE Partner
					SET UseCustomerAddress=@Partner_UseCustomerAddress, 
						TitleID=@Partner_Title, 
						FirstName=@Partner_FirstName, 
						MiddleName=@Partner_MiddleName, 
						LastName=@Partner_LastName, 
						EmailAddress=@Partner_EmailAddress, 
						DayTimeTelephoneNumber=@Partner_DayTimeTelephoneNumber, 
						HomeTelephone=@Partner_HomeTelephone, 
						MobileTelephone=@Partner_MobileTelephone, 
						Address1=@Partner_Address1, 
						Address2=@Partner_Address2, 
						Town=@Partner_Town, 
						County=@Partner_County, 
						PostCode=@Partner_PostCode, 
						Occupation=@Partner_Occupation, 
						Employer=@Partner_Employer, 
						DateOfBirth=@Partner_DateOfBirth
					WHERE PartnerID=@PartnerID					
				END
			END
        
			IF @ActionToPerform = 5 /*Create new case. _CreateNewLead has "CreateCaseRegardless = 0, so will just create a case if an appropriate lead already exists*/
			BEGIN
				EXEC @MatterID = _C00_CreateNewLeadForCust @ExistingCustomerID, @LeadTypeID, @ClientID, @UserID, 0, 1, 0
				EXEC dbo._C00_SimpleValueIntoField 300840, @ImportID, @MatterID, @UserID
				EXEC dbo._C00_SimpleValueIntoField 300843, @Customer_MobileTelephone, @MatterID, @UserID
			END
        
			IF(DATALENGTH(@DetailValuesXml)>@EMPTY_XML_LENGTH)
			BEGIN
				-- Save the detail value data into a temporary table				
				;WITH ShreddedData AS
				(
					SELECT 
						DetailFieldID = Vals.value('(DetailFieldID)[1]', 'INT'),
						DetailFieldValue = Vals.value('(FieldValue)[1]', 'VARCHAR(2000)')		
					FROM @DetailValuesXml.nodes('fieldData/DetailValue') AS Tbl(Vals)	
				)							
				INSERT INTO @FieldValue (FieldID, FieldData, LeadTypeID, LeadOrMatter, CustomerID, LeadID, CaseID, MatterID)
				SELECT ShreddedData.DetailFieldID, DetailFieldValue, df.LeadTypeID, df.LeadOrMatter, 
					CASE LeadOrMatter WHEN 10 THEN @ExistingCustomerID ELSE NULL END, 
					CASE LeadOrMatter WHEN 1 THEN l.LeadID ELSE NULL END,
					CASE LeadOrMatter WHEN 11 THEN c.CaseID ELSE NULL END,
					CASE LeadOrMatter WHEN 2 THEN m.MatterID ELSE NULL END
				FROM ShreddedData
				INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID=ShreddedData.DetailFieldID
				LEFT JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID=df.LeadTypeID AND l.CustomerID=@ExistingCustomerID
				LEFT JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
				LEFT JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
				WHERE (m.MatterID = @MatterID or @ActionToPerform <> 5)
				
				SELECT @ExecBatchFields = @ExecBatchFields + 'EXEC dbo._C00_SimpleValueIntoField @DetailFieldID=' + CONVERT(VARCHAR,FieldID) +  ', @DetailValue='''  + FieldData +  ''', @ObjectID=' + CASE LeadOrMatter WHEN 10 THEN CONVERT(VARCHAR,CustomerID) WHEN 1 THEN CONVERT(VARCHAR,LeadID) WHEN 11 THEN CONVERT(VARCHAR,CaseID) WHEN 2 THEN CONVERT(VARCHAR,MatterID) END + ', @ClientPersonnelID=' + CONVERT(VARCHAR,@UserID) + ';' 
				FROM @FieldValue 
				
				EXEC (@ExecBatchFields)
			END			
        
        END
        
        IF @ActionToPerform = 4 -- Insert = 4          Inserts the new record regardless of duplicate
        BEGIN 
			SET @CreateNewCustomer = 1
        END
	END
	ELSE
	BEGIN -- customer does not exist so create new customer and insert values
		SET @CreateNewCustomer = 1
	END
	
	IF @CreateNewCustomer = 1 AND @LeadTypeID>0
	BEGIN	-- also create lead for each lead type within the client

		EXEC @NewCustomerID = _C00_CreateNewCustomerFull
								@ClientID,
								@LeadTypeID, 
								@UserID, 
								@Customer_TitleID, 
								@Customer_IsBusiness, 
								@Customer_FirstName, 
								@Customer_MiddleName, 
								@Customer_LastName, 
								@Customer_EmailAddress, 
								@Customer_DayTimeTelephoneNumber, 
								FALSE, 
								@Customer_HomeTelephone, 
								FALSE, 
								@Customer_MobileTelephone, 
								FALSE, 
								@Customer_CompanyTelephone, 
								FALSE, 
								@Customer_WorksTelephone, 
								FALSE, 
								@Customer_Address1, 
								@Customer_Address2, 
								@Customer_Town, 
								@Customer_County, 
								@Customer_PostCode, 
								@Customer_Website, 
								FALSE, 
								'', 
								@Customer_AquariumStatusID, 
								@Customer_ClientStatusID, 
								@Customer_Test, 
								@Customer_CompanyName, 
								@Customer_Occupation, 
								@Customer_Employer, 
								'', 
								@Customer_DoNotEmail, 
								@Customer_DoNotSellToThirdParty, 
								@Customer_AgreedToTermsAndConditions, 
								@Customer_DateOfBirth, 
								NULL, 
								NULL, 
								FALSE, 
								@Customer_CountryID, 
								@Customer_SubClientID, 
								@Customer_CustomerRef, 
								@UserID, 
								@TheDateTime, 
								NULL, 
								NULL, 
								NULL, 
								''

		IF(@NewCustomerID>0) -- customer was created
		BEGIN 

			IF (DATALENGTH(@DepartmentXml)>@EMPTY_XML_LENGTH) -- Department information
			BEGIN
				
				INSERT INTO Department (ClientID, CustomerID, DepartmentName, DepartmentDescription)
				VALUES (@ClientID, @NewCustomerID, @Department_DepartmentName, @Department_DepartmentDescription)
								
				SELECT @DepartmentID = SCOPE_IDENTITY()
											
			END

			IF (DATALENGTH(@OfficeXml)>@EMPTY_XML_LENGTH) -- Office information
			BEGIN
			
				INSERT INTO CustomerOffice (ClientID, CustomerID, OfficeName, MainPhoneNumber, FaxNumber, Address1, Address2, Town, County, Postcode, Country, Notes, CountryID)
				VALUES (@ClientID, @NewCustomerID, @Office_OfficeName, @Office_MainPhoneNumber, @Office_FaxNumber, @Office_Address1, @Office_Address2, @Office_Town, @Office_County, @Office_Postcode, @Office_Country, @Office_Notes, NULL)
								
				SELECT @OfficeID = SCOPE_IDENTITY()
			
				UPDATE Customers
				SET DefaultOfficeID = @OfficeID
				WHERE CustomerID=@NewCustomerID				
				
			END
		
			IF (DATALENGTH(@ContactXml)>@EMPTY_XML_LENGTH) -- Contact information
			BEGIN				
			
				INSERT INTO Contact  (ClientID, CustomerID, TitleID, Firstname, Middlename, Lastname, EmailAddressWork, EmailAddressOther, DirectDial, MobilePhoneWork, MobilePhoneOther, Address1, Address2, Town, County, Postcode, Country, OfficeID, DepartmentID, JobTitle, Notes, CountryID, LanguageID)
				VALUES (@ClientID, @NewCustomerID, @Contact_Title, @Contact_FirstName, @Contact_MiddleName,@Contact_LastName,@Contact_EmailAddressWork, @Contact_EmailAddressOther, @Contact_DirectDial, @Contact_MobilePhoneWork, @Contact_MobilePhoneOther, @Contact_Address1, @Contact_Address2, @Contact_Town, @Contact_County, @Contact_PostCode, @Contact_Country,  @OfficeID, @DepartmentID, @Contact_JobTitle, @Contact_Notes, @Contact_CountryID, NULL)
							
				SELECT @ContactID = SCOPE_IDENTITY()
				
				UPDATE Customers
				SET DefaultContactID = @ContactID
				WHERE CustomerID=@NewCustomerID
				
			END
			
			IF (DATALENGTH(@PartnerXml)>@EMPTY_XML_LENGTH) -- Partner information
			BEGIN
							
				INSERT INTO Partner (CustomerID, ClientID, UseCustomerAddress, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone, Address1, Address2, Town, County, PostCode, Occupation, Employer, DateOfBirth, CountryID)				
				VALUES (@NewCustomerID, @ClientID, @Partner_UseCustomerAddress, @Partner_Title, @Partner_FirstName, @Partner_MiddleName, @Partner_LastName, @Partner_EmailAddress, @Partner_DayTimeTelephoneNumber, @Partner_HomeTelephone, @Partner_MobileTelephone, @Partner_Address1, @Partner_Address2, @Partner_Town, @Partner_County, @Partner_PostCode, @Partner_Occupation, @Partner_Employer, @Partner_DateOfBirth, NULL)				
								
			END
			
			IF(DATALENGTH(@DetailValuesXml)>@EMPTY_XML_LENGTH)
			BEGIN
				-- Save the detail value data into a temporary table				
				;WITH ShreddedData AS
				(
					SELECT 
						DetailFieldID = Vals.value('(DetailFieldID)[1]', 'INT'),
						DetailFieldValue = Vals.value('(FieldValue)[1]', 'VARCHAR(2000)')		
					FROM @DetailValuesXml.nodes('fieldData/DetailValue') AS Tbl(Vals)	
				)							
				INSERT INTO @FieldValue (FieldID, FieldData, LeadTypeID, LeadOrMatter, CustomerID, LeadID, CaseID, MatterID)
				SELECT ShreddedData.DetailFieldID, DetailFieldValue, df.LeadTypeID, df.LeadOrMatter, 
					CASE LeadOrMatter WHEN 10 THEN @NewCustomerID ELSE NULL END, 
					CASE LeadOrMatter WHEN 1 THEN l.LeadID ELSE NULL END,
					CASE LeadOrMatter WHEN 11 THEN c.CaseID ELSE NULL END,
					CASE LeadOrMatter WHEN 2 THEN m.MatterID ELSE NULL END
				FROM ShreddedData
				INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID=ShreddedData.DetailFieldID
				LEFT JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID=df.LeadTypeID AND l.CustomerID=@NewCustomerID
				LEFT JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
				LEFT JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
				
				SELECT @ExecBatchFields = @ExecBatchFields + 'EXEC dbo._C00_SimpleValueIntoField @DetailFieldID=' + CONVERT(VARCHAR, FieldID) +  ', @DetailValue='''  + FieldData +  ''', @ObjectID=' + CASE LeadOrMatter WHEN 10 THEN CONVERT(VARCHAR,CustomerID) WHEN 1 THEN CONVERT(VARCHAR,LeadID) WHEN 11 THEN CONVERT(VARCHAR,CaseID) WHEN 2 THEN CONVERT(VARCHAR,MatterID) END + ', @ClientPersonnelID=' + CONVERT(VARCHAR,@UserID) + ';' 
				FROM @FieldValue 
				
				EXEC (@ExecBatchFields)
			END

			IF @ClientID = 379 /*Temp fix.  All for review in 2016*/
			BEGIN
				IF @MatterID is NULL or @MatterID = 0
				BEGIN
					SELECT TOP(1) @MatterID = m.MatterID
					FROM Matter m WITH ( NOLOCK ) 
					INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID 
					INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = c.LeadID
					WHERE m.CustomerID = @NewCustomerID
					AND l.LeadTypeID = 1776 /*PSS Customer*/
				END

				INSERT MatterDetailValues ( ClientID, LeadID, MatterID, DetailFieldID, DetailValue )
				SELECT m.ClientID, m.LeadID, m.MatterID, 300843, ISNULL(@Customer_MobileTelephone,'') /*PhoneNumber1*/
				FROM Matter m WITH ( NOLOCK ) 
				WHERE m.MatterID = @MatterID
					UNION
				SELECT m.ClientID, m.LeadID, m.MatterID, 300840, CONVERT(VARCHAR,@ImportID) /*DataloaderFileID*/
				FROM Matter m WITH ( NOLOCK ) 
				WHERE m.MatterID = @MatterID
			END

			
			--These thirdparty fields are all customer level fields
			SELECT @FieldID_ThirdpartyLeadID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=4200
			SELECT @FieldID_ListID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=4201
			SELECT @FieldID_ImportID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) WHERE ClientID=@ClientID AND ThirdPartyFieldID=4202
			
			EXEC _C00_SimpleValueIntoField @FieldID_ThirdpartyLeadID, @ThirdPartyLeadID, @NewCustomerID, @UserID
			EXEC _C00_SimpleValueIntoField @FieldID_ListID, @ListID, @NewCustomerID, @UserID
			EXEC _C00_SimpleValueIntoField @FieldID_ImportID, @ImportID, @NewCustomerID, @UserID
			
		END
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__Import] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad__Import] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad__Import] TO [sp_executeall]
GO
