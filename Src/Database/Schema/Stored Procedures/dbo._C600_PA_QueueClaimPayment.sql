SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Dave Morgan
-- Create date: 16/08/2016
-- Description:	Claims pay outs. Picks the first payment available and queues it. If there are more it adds the queue event again
-- Mods
-- 2016-10-03 DCM update selection filter to include non-blank date approved
-- 2016-12-14 Changed object and object type to matter as we store tablerowID as transactionID 
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_QueueClaimPayment]
(
	@LeadEventID INT,
	@MatterID INT
)
AS
BEGIN

--declare @matterID int = 379
	SET NOCOUNT ON;

	DECLARE 
			@AccountNumber VARCHAR(10),
			@Amount MONEY,
			@ChequeNumber VARCHAR(30),
			@ClientID INT,
			@DetailValue VARCHAR(200), 
			@PaymentScheduleID INT,
			@Address1 VARCHAR(500),
			@Address2 VARCHAR(500),
			@Address3 VARCHAR(500),
			@Address4 VARCHAR(500),
			@Address5 VARCHAR(500),
			@Firstname VARCHAR(100),
			@Lastname VARCHAR(100),
			@LeadTypeID INT,
			@Organisation VARCHAR(100),
			@MaskedCardNumber VARCHAR(500),
			@ObjectID INT,
			@OrigReference VARCHAR(500),
			@PaymentDate DATE,
			@PayType INT,
			@PHReference VARCHAR(500),
			@Postcode VARCHAR(500),
			@Reference VARCHAR(500),
			@Sortcode VARCHAR(10),
			@TableRowID INT,
			@Title VARCHAR(50),
			@ValueInt INT
	

	SELECT TOP 1 
			@ClientID=tr.ClientID,
			@LeadTypeID=l.LeadTypeID,
			@TableRowID=tr.TableRowID,
			@Title=CASE WHEN t.TitleID IS NULL THEN '' ELSE CAST(t.TitleID AS VARCHAR) END,
			@Firstname=ISNULL(tdv_firstname.DetailValue,''),
			@Lastname=ISNULL(tdv_lastname.DetailValue,''),
			@Organisation=ISNULL(tdv_organisation.DetailValue,''),
			@Address1=ISNULL(tdv_address1.DetailValue,''),
			@Address2=ISNULL(tdv_address2.DetailValue,''),
			@Address3=ISNULL(tdv_address3.DetailValue,''),
			@Address4=ISNULL(tdv_address4.DetailValue,''),
			@Address5=ISNULL(tdv_address5.DetailValue,''),
			@Postcode=ISNULL(tdv_postcode.DetailValue,''),
			@Reference=ISNULL(tdv_reference.DetailValue,''),
			@PHReference=ISNULL(tdv_ph_reference.DetailValue,''),
			@OrigReference=ISNULL(tdv_orig_reference.DetailValue,''),
			@PayType=tdv_paymethod.ValueInt,
			@Amount=tdv_amount.ValueMoney,
			@PaymentDate=tdv_paydate.ValueDate,
			@AccountNumber=ISNULL(tdv_AccNo.DetailValue,''),
			@Sortcode=ISNULL(tdv_SortCode.DetailValue,''),
			@MaskedCardNumber='',
			@ChequeNumber = tdv_ChequeNum.DetailValue
	FROM TableRows tr WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv_amount WITH (NOLOCK) ON tdv_amount.TableRowID = tr.TableRowID AND tdv_amount.DetailFieldID = 154518
	INNER JOIN TableDetailValues tdv_fullname WITH (NOLOCK) ON tdv_fullname.TableRowID = tr.TableRowID AND tdv_fullname.DetailFieldID = 154490
	LEFT JOIN TableDetailValues tdv_title WITH (NOLOCK) ON tdv_title.TableRowID = tr.TableRowID AND tdv_title.DetailFieldID = 154486
	LEFT JOIN Titles t WITH (NOLOCK) ON tdv_title.DetailValue=t.Title
	LEFT JOIN TableDetailValues tdv_firstname WITH (NOLOCK) ON tdv_firstname.TableRowID = tr.TableRowID AND tdv_firstname.DetailFieldID = 154487
	LEFT JOIN TableDetailValues tdv_lastname WITH (NOLOCK) ON tdv_lastname.TableRowID = tr.TableRowID AND tdv_lastname.DetailFieldID = 154488
	LEFT JOIN TableDetailValues tdv_organisation WITH (NOLOCK) ON tdv_organisation.TableRowID = tr.TableRowID AND tdv_organisation.DetailFieldID = 154490
	LEFT JOIN TableDetailValues tdv_address1 WITH (NOLOCK) ON tdv_address1.TableRowID = tr.TableRowID AND tdv_address1.DetailFieldID = 154491
	LEFT JOIN TableDetailValues tdv_address2 WITH (NOLOCK) ON tdv_address2.TableRowID = tr.TableRowID AND tdv_address2.DetailFieldID = 154492
	LEFT JOIN TableDetailValues tdv_address3 WITH (NOLOCK) ON tdv_address3.TableRowID = tr.TableRowID AND tdv_address3.DetailFieldID = 154493
	LEFT JOIN TableDetailValues tdv_address4 WITH (NOLOCK) ON tdv_address4.TableRowID = tr.TableRowID AND tdv_address4.DetailFieldID = 154494
	LEFT JOIN TableDetailValues tdv_address5 WITH (NOLOCK) ON tdv_address5.TableRowID = tr.TableRowID AND tdv_address5.DetailFieldID = 154495
	LEFT JOIN TableDetailValues tdv_postcode WITH (NOLOCK) ON tdv_postcode.TableRowID = tr.TableRowID AND tdv_postcode.DetailFieldID = 154496
	LEFT JOIN TableDetailValues tdv_reference WITH (NOLOCK) ON tdv_reference.TableRowID = tr.TableRowID AND tdv_reference.DetailFieldID = 154498
	LEFT JOIN TableDetailValues tdv_ph_reference WITH (NOLOCK) ON tdv_ph_reference.TableRowID = tr.TableRowID AND tdv_ph_reference.DetailFieldID = 154511
	LEFT JOIN TableDetailValues tdv_orig_reference WITH (NOLOCK) ON tdv_orig_reference.TableRowID = tr.TableRowID AND tdv_orig_reference.DetailFieldID = 159479
	LEFT JOIN dbo.TableDetailValues tdv_ChequeNum WITH (NOLOCK) on tdv_ChequeNum.TableRowID = tr.TableRowID and tdv_ChequeNum.DetailFieldID = 162638
	INNER JOIN TableDetailValues tdv_paymethod WITH (NOLOCK) ON tdv_paymethod.TableRowID = tr.TableRowID AND tdv_paymethod.DetailFieldID = 170232
	INNER JOIN TableDetailValues tdv_payapproved WITH (NOLOCK) ON tdv_payapproved.TableRowID = tr.TableRowID AND tdv_payapproved.DetailFieldID = 159407
	INNER JOIN TableDetailValues tdv_paydate WITH (NOLOCK) ON tdv_paydate.TableRowID = tr.TableRowID AND tdv_paydate.DetailFieldID = 154520
	LEFT JOIN TableDetailValues tdv_payrun WITH (NOLOCK) ON tdv_payrun.TableRowID = tr.TableRowID AND tdv_payrun.DetailFieldID = 154521
	LEFT JOIN TableDetailValues tdv_SortCode WITH (NOLOCK) ON tdv_SortCode.TableRowID = tr.TableRowID AND tdv_SortCode.DetailFieldID = 175271
	LEFT JOIN TableDetailValues tdv_AccNo WITH (NOLOCK) ON tdv_AccNo.TableRowID = tr.TableRowID AND tdv_AccNo.DetailFieldID = 175270
	INNER JOIN Matter m WITH (NOLOCK) ON tr.MatterID=m.MatterID
	INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON l.CustomerID=c.CustomerID AND c.Test=0
	WHERE tr.DetailFieldID = 154485
	AND (tdv_payrun.DetailValue='' OR tdv_payrun.DetailValue IS NULL) -- ONLY rows where payment run date is blank or empty
	AND (tdv_payapproved.DetailValue <> '' AND tdv_payapproved.DetailValue IS NOT NULL ) 
	AND m.MatterID=@MatterID

	--IF @PayType=70018 -- cheque
	--BEGIN
	--	EXEC @ValueInt=dbo._C00_GetNextRefValueInt 162638
	--	SELECT @ChequeNumber=CAST(@ValueInt AS VARCHAR)
	--END

	UPDATE LeadEvent Set Comments = Comments 
										+ CASE WHEN Comments='' THEN '' ELSE ' ' END 
										+ 'Claim payment table row ID: ' 
										+  CAST(@TableRowID AS VARCHAR)
	WHERE LeadEventID=@LeadEventID
	
	-- clear helper or update fields
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4338)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @MatterID, @MatterID -- Claim Object ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4339)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, 2, @MatterID -- Claim Object Type ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4340)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID -- Policy Admin Object ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4341)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID -- Policy Admin Object Type ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4342)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Title, @MatterID -- Payee Title
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4343)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Firstname, @MatterID -- Payee First Name
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4344)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Lastname, @MatterID -- Payee Last Name
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4345)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Organisation, @MatterID -- Payee Organisation
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4346)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Address1, @MatterID -- Payee Address 1
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4347)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Address2, @MatterID -- Payee Address 2
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4348)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Address3, @MatterID -- Payee Address 3
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4349)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Address4, @MatterID -- Payee Address 4
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4350)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Address5, @MatterID -- Payee Address 5
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4351)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Postcode, @MatterID -- Payee Postcode
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4352)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, '', @MatterID -- Payee Country ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4353)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Reference, @MatterID -- Payee Reference
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4354)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PHReference, @MatterID -- Policyholder Reference
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4355)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PayType, @MatterID -- Payment Type ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4356)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Amount, @MatterID -- Payment Amount
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4357)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PaymentDate, @MatterID -- Payment Date
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4360)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @OrigReference, @MatterID -- Original Payment Referrence
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4361)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @ChequeNumber, @MatterID -- Cheque Number
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4362)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AccountNumber, @MatterID -- Account Number
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4363)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Sortcode, @MatterID -- Sortcode
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4364)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @MaskedCardNumber, @MatterID -- Masked Card Number
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4365)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, 74331, @MatterID -- Payment Source Type ID.  It's a claim

	-- update payment rundate
	SELECT @DetailValue=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
	EXEC _C00_SimpleValueIntoField 154521,@DetailValue,@TableRowID
	
	-- are there any more rows for this matter	
	---- add event again to do next payment
	IF EXISTS ( 	SELECT * FROM TableRows tr WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_payrun WITH (NOLOCK) ON tdv_payrun.TableRowID = tr.TableRowID AND tdv_payrun.DetailFieldID = 154521
					INNER JOIN TableDetailValues tdv_payapproved WITH (NOLOCK) ON tdv_payapproved.TableRowID = tr.TableRowID AND tdv_payapproved.DetailFieldID = 159407
					INNER JOIN Matter m WITH (NOLOCK) ON tr.MatterID=m.MatterID
					INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
					INNER JOIN Customers c WITH (NOLOCK) ON l.CustomerID=c.CustomerID AND c.Test=0
					WHERE tr.DetailFieldID = 154485
					AND (tdv_payrun.DetailValue='' OR tdv_payrun.DetailValue IS NULL) -- ONLY rows where payment run date is blank or empty
					AND (tdv_payapproved.DetailValue <> '' AND tdv_payapproved.DetailValue IS NOT NULL ) 
					AND m.MatterID=@MatterID
			  )
	BEGIN
	
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount,EarliestRunDateTime)
		SELECT TOP 1 le.ClientID, m.CustomerID, le.LeadID, le.CaseID, LeadEventID, EventTypeID, dbo.fn_GetDate_Local(), le.WhoCreated, EventTypeID, le.WhoCreated, -1, 5, 0, 5, DATEADD(SECOND,15,dbo.fn_GetDate_Local())
		FROM LeadEvent le WITH (NOLOCK) 
		INNER JOIN Matter m WITH (NOLOCK) ON le.CaseID=m.CaseID
		WHERE LeadEventID=@LeadEventID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueClaimPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_QueueClaimPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueClaimPayment] TO [sp_executeall]
GO
