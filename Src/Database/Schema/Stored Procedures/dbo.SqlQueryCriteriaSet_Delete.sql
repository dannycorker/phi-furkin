SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryCriteriaSet table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSet_Delete]
(

	@SqlQueryCriteriaSetID int   
)
AS


				DELETE FROM [dbo].[SqlQueryCriteriaSet] WITH (ROWLOCK) 
				WHERE
					[SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSet_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSet_Delete] TO [sp_executeall]
GO
