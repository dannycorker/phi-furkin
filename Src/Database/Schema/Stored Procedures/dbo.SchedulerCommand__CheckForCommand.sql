SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Listen for commands to Automated Task Schedulers
-- =============================================
CREATE PROCEDURE [dbo].[SchedulerCommand__CheckForCommand] 
	@SchedulerIdentifier varchar(50) 
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Return nothing by default */
	DECLARE @Command varchar(50) = ''
	
	/* 
		Look for the first enabled command for the scheduler passed in.
		A command for ALL schedulers will be first in the list (if enabled)
		because of the alphabetical sort order.
	*/
	SELECT TOP (1) @Command = sc.SchedulerCommand  
	FROM dbo.SchedulerCommand sc 
	WHERE sc.SchedulerIdentifier IN ('ALL', @SchedulerIdentifier) 
	AND sc.IsEnabled = 1 
	ORDER BY sc.SchedulerIdentifier 
	
	SELECT @Command as [Command]
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__CheckForCommand] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SchedulerCommand__CheckForCommand] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__CheckForCommand] TO [sp_executeall]
GO
