SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-10-2014
-- Description:	Gets the RulesEngine RuleSet(s) by client id 
-- It only returns the rule sets that have an input type id of 3 and an
-- output type id of 3 - since document based rule sets must return true or false
-- and a RuleSetTypeID of 1 which is document rule
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSets_GetDocumentRuleSetsByClientID]

	@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

    SELECT * FROM RulesEngine_RuleSets WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND InputTypeID=3 AND OutputTypeID=3 AND RuleSetTypeID = 1

END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_GetDocumentRuleSetsByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSets_GetDocumentRuleSetsByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_GetDocumentRuleSetsByClientID] TO [sp_executeall]
GO
