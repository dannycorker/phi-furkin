SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the FieldValidation table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldValidation_GetByFieldValidationID]
(

	@FieldValidationID int   
)
AS


				SELECT
					[FieldValidationID],
					[ClientID],
					[FieldID],
					[ValidationGroup],
					[ValidationType],
					[MaxValue],
					[MinValue],
					[ErrorMessage],
					[AccessLevelRestrictions]
				FROM
					[dbo].[FieldValidation] WITH (NOLOCK) 
				WHERE
										[FieldValidationID] = @FieldValidationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_GetByFieldValidationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldValidation_GetByFieldValidationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldValidation_GetByFieldValidationID] TO [sp_executeall]
GO
