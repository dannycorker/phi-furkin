SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------
-- Date Created: 29 May 2007

-- Created By:  (Chris Townsend (chris.townsend@aquarium-software.com))
-- Purpose: Select all possible in-process events for the current Lead Type
-- MODIFIED: 2014-07-22	SB	Updated to use function for shared lead type
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType_GetAllByLeadTypeID] 
(
	@LeadTypeID int,
	@ClientID INT = NULL
)
AS

	SET ANSI_NULLS OFF

		SELECT
		[EventTypeID],
		[ClientID],
		[EventTypeName],
		[EventTypeDescription],
		[Enabled],
		[UnitsOfEffort],
		[FollowupTimeUnitsID],
		[FollowupQuantity],
		[AvailableManually],
		[StatusAfterEvent],
		[AquariumEventAfterEvent],
		[EventSubtypeID],
		[DocumentTypeID],
		[LeadTypeID],
		--[EscalationEventType],
		[AllowCustomTimeUnits]
	FROM
		dbo.fnEventTypeShared(@ClientID) et
	WHERE et.InProcess = 1
		--EXISTS (SELECT [EventTypeID],[NextEventTypeID] FROM dbo.[EventChoice] ec WHERE (et.[EventTypeID] = ec.[EventTypeID] OR et.[EventTypeID] = ec.[NextEventTypeID]))
		AND et.LeadTypeID = @LeadTypeID
		AND et.[EventSubTypeID] NOT IN (7,8,9)
		AND et.Enabled = 1
	ORDER BY EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON






GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetAllByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_GetAllByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetAllByLeadTypeID] TO [sp_executeall]
GO
