SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Returns all actions for a specific calulation
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_CalculationActions_Get]
(
	@ClientID INT,
	@CalculationID INT,
	@CalculationActionID INT = NULL
)


AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	a.CalculationActionID, a.Name, a.ActionTypeID, t.Name AS ActionType, a.ParentID, a.Context, a.Parameter, 
			a.WhenCreated, cpCreate.UserName AS WhoCreated, a.WhenModified, cpMod.UserName AS WhoModified, a.Description
	FROM dbo.CalcsEngine_CalculationActions a WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON a.WhoCreated = cpCreate.ClientPersonnelID
	INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON a.WhoModified = cpMod.ClientPersonnelID
	INNER JOIN dbo.CalcsEngine_ActionTypes t WITH (NOLOCK) ON a.ActionTypeID = t.ActionTypeID
	WHERE a.ClientID = @ClientID
	AND a.CalculationID = @CalculationID
	AND (@CalculationActionID IS NULL OR a.CalculationActionID = @CalculationActionID)
	ORDER BY a.ActionOrder

	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationActions_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_CalculationActions_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationActions_Get] TO [sp_executeall]
GO
