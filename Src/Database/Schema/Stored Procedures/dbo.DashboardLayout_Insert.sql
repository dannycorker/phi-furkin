SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DashboardLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardLayout_Insert]
(

	@DashboardLayoutID int    OUTPUT,

	@DashboardLayoutName varchar (200)  ,

	@TopLeftDimensions char (3)  ,

	@TopMiddleDimensions char (3)  ,

	@TopRightDimensions char (3)  ,

	@BottomLeftDimensions char (3)  ,

	@BottomMiddleDimensions char (3)  ,

	@BottomRightDimensions char (3)  
)
AS


				
				INSERT INTO [dbo].[DashboardLayout]
					(
					[DashboardLayoutName]
					,[TopLeftDimensions]
					,[TopMiddleDimensions]
					,[TopRightDimensions]
					,[BottomLeftDimensions]
					,[BottomMiddleDimensions]
					,[BottomRightDimensions]
					)
				VALUES
					(
					@DashboardLayoutName
					,@TopLeftDimensions
					,@TopMiddleDimensions
					,@TopRightDimensions
					,@BottomLeftDimensions
					,@BottomMiddleDimensions
					,@BottomRightDimensions
					)
				-- Get the identity value
				SET @DashboardLayoutID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardLayout_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Insert] TO [sp_executeall]
GO
