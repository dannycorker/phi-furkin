SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteCustomerInformationFieldDefinitions] @ClientQuestionnaireID int
AS
Delete From CustomerInformationFieldDefinitions
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteCustomerInformationFieldDefinitions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteCustomerInformationFieldDefinitions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteCustomerInformationFieldDefinitions] TO [sp_executeall]
GO
