SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Folder table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_Insert]
(

	@FolderID int    OUTPUT,

	@ClientID int   ,

	@FolderTypeID int   ,

	@FolderParentID int   ,

	@FolderName varchar (50)  ,

	@FolderDescription varchar (250)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@Personal bit   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[Folder]
					(
					[ClientID]
					,[FolderTypeID]
					,[FolderParentID]
					,[FolderName]
					,[FolderDescription]
					,[WhenCreated]
					,[WhoCreated]
					,[Personal]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@FolderTypeID
					,@FolderParentID
					,@FolderName
					,@FolderDescription
					,@WhenCreated
					,@WhoCreated
					,@Personal
					,@SourceID
					)
				-- Get the identity value
				SET @FolderID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Insert] TO [sp_executeall]
GO
