SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedAndCompleted] @id int
AS
BEGIN
Delete From ClientQuestionnairesVisitedAndCompleted where ClientQuestionnairesVisitedAndCompletedID = @id
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedAndCompleted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedAndCompleted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedAndCompleted] TO [sp_executeall]
GO
