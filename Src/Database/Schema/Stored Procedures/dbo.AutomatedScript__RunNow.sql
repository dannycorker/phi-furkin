SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =================================================
-- Author:		Simon Brushett
-- ALTER date: 2012-08-13
-- Description:	Set an automated script to run right now
-- =================================================
CREATE PROCEDURE [dbo].[AutomatedScript__RunNow] 
(
	@ScriptID INT,
	@ScheduleID INT
)
AS
BEGIN

	EXEC [926125-SQLCLU12\SQL12].AquariusMaster.dbo.AutomatedScript__RunNow @ScriptID, @ScheduleID
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedScript__RunNow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedScript__RunNow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedScript__RunNow] TO [sp_executeall]
GO
