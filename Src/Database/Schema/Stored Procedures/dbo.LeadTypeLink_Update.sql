SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadTypeLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_Update]
(

	@LeadTypeLinkID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@OutcomeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadTypeLink]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[OutcomeID] = @OutcomeID
				WHERE
[LeadTypeLinkID] = @LeadTypeLinkID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Update] TO [sp_executeall]
GO
