SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnelOptionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptionTypes_Insert]
(

	@ClientPersonnelOptionTypeID int    OUTPUT,

	@OptionTypeName nchar (100)  
)
AS


				
				INSERT INTO [dbo].[ClientPersonnelOptionTypes]
					(
					[OptionTypeName]
					)
				VALUES
					(
					@OptionTypeName
					)
				-- Get the identity value
				SET @ClientPersonnelOptionTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptionTypes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Insert] TO [sp_executeall]
GO
