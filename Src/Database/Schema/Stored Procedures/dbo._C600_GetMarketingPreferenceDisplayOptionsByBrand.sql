SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2019-09-04
-- Description:	BAULAG-182 - Get MarketingPreference Display options for IQB By BrandID (Affinity)
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetMarketingPreferenceDisplayOptionsByBrand]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    

SELECT 	rdvShowMarketingPreferences.ResourceListID AS BrandID,
		rdvShowMarketingPreferences.DetailValue AS ShowMarketingPreferences,
		ISNULL(rdvShowEmail.DetailValue, 0) AS ShowEmail,
		ISNULL(rdvShowPhone.DetailValue, 0) AS ShowPhone,
		ISNULL(rdvShowSMS.DetailValue, 0) AS ShowSMS,
		ISNULL(rdvShowPost.DetailValue, 0) AS ShowPost
	FROM ResourceListDetailValues rdvShowMarketingPreferences WITH ( NOLOCK )
	LEFT JOIN ResourceListDetailValues rdvShowEmail ON rdvShowEmail.DetailFieldID = 180279 AND rdvShowEmail.ResourceListID = rdvShowMarketingPreferences.ResourceListID
	LEFT JOIN ResourceListDetailValues rdvShowPhone ON rdvShowPhone.DetailFieldID = 180280 AND rdvShowPhone.ResourceListID = rdvShowMarketingPreferences.ResourceListID
	LEFT JOIN ResourceListDetailValues rdvShowSMS ON rdvShowSMS.DetailFieldID = 180281 AND rdvShowSMS.ResourceListID = rdvShowMarketingPreferences.ResourceListID
	LEFT JOIN ResourceListDetailValues rdvShowPost ON rdvShowPost.DetailFieldID = 180282 AND rdvShowPost.ResourceListID = rdvShowMarketingPreferences.ResourceListID
	WHERE rdvShowMarketingPreferences.DetailFieldID = 180278 
	--AND rdvShowMarketingPreferences.ResourceListID = @BrandID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetMarketingPreferenceDisplayOptionsByBrand] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetMarketingPreferenceDisplayOptionsByBrand] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetMarketingPreferenceDisplayOptionsByBrand] TO [sp_executeall]
GO
