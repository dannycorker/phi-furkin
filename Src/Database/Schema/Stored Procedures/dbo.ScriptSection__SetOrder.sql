SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 12-03-2015
-- Description:	Updates the section order when a section has been added or removed
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSection__SetOrder]
	@ScriptID INT
AS
BEGIN
	
	
	SET NOCOUNT ON;
	
	DECLARE @Table table (NewSectionOrder INT, ScriptSectionID INT)
	
	INSERT INTO @Table(NewSectionOrder, ScriptSectionID) -- calculates new order 
	SELECT  ROW_NUMBER() OVER(ORDER BY SectionOrder ASC) AS NewSectionOrder, ScriptSectionID
	FROM ScriptSection WHERE ScriptID=@ScriptID ORDER BY SectionOrder

	UPDATE ScriptSection -- updates the Section order 
	SET ScriptSection.SectionOrder=t.NewSectionOrder
	FROM ScriptSection ss
	INNER JOIN @Table t on ss.ScriptSectionID=t.ScriptSectionID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__SetOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection__SetOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__SetOrder] TO [sp_executeall]
GO
