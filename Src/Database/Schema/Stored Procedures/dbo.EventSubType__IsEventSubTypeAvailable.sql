SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-20
-- Description:	Checks to see if an event sub type is available
-- =============================================
CREATE PROCEDURE [dbo].[EventSubType__IsEventSubTypeAvailable]
(
	@EventSubTypeID INT
)
AS
BEGIN
	
	DECLARE @Result BIT

	SELECT @Result = Available
	FROM dbo.EventSubtype WITH (NOLOCK) 
	WHERE EventSubtypeID = @EventSubTypeID
	
	SELECT ISNULL(@Result, 1)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubType__IsEventSubTypeAvailable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubType__IsEventSubTypeAvailable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubType__IsEventSubTypeAvailable] TO [sp_executeall]
GO
