SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LookupListItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_Get_List]

AS


				
				SELECT
					[LookupListItemID],
					[LookupListID],
					[ItemValue],
					[ClientID],
					[Enabled],
					[SortOrder],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID],
					[IsShared]
				FROM
					[dbo].[LookupListItems] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Get_List] TO [sp_executeall]
GO
