SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-07
-- Description:	Get all event threads in progress for a case
--				2011-05-23 ACE Acced extra nolocks where required
--				2012-02-20 SB Don't show events of a specific subtype if they are disabled
--				2014-07-24 SB Changed references to event type function for shared events and limited event choice to client ID
--				2015-06-05 LB Inlcuded the Weighting column to enable clients to order their event choices manually.
--				2017-09-21 LB Redesigned proc to hopefully be more efficient. Split into several procs.
-- =============================================
CREATE PROCEDURE [dbo].[GetEventChoicesByCaseAndEvent] 
	
	@CaseID INT,
	@FromEventTypeID INT,
	@RunAsThisUser INT,
	@ThreadNumber INT,
	@EventSubTypeID INT = NULL
	

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT, 
	@CustomerID INT,
	@LeadID INT,
	@LogEntry VARCHAR(200),
	@MatterID INT,
	@ProcessStartID INT,
	@LeadTypeID INT

	
	SELECT @ClientID = Lead.ClientID, 
	@CustomerID = Lead.CustomerID, 
	@LeadID = Lead.LeadID,
	@ProcessStartID = Cases.ProcessStartLeadEventID,
	@LeadTypeID = Lead.LeadTypeID
	FROM Cases  WITH (NOLOCK)
	INNER JOIN Lead WITH (NOLOCK) ON Lead.LeadID = Cases.LeadID 
	WHERE Cases.CaseID = @CaseID

	SELECT TOP 1 @MatterID = m.matterID FROM Matter m WITH(NOLOCK) WHERE m.CaseID = @CaseID

	DECLARE @PossibleEvents1 TABLE (
		EventChoiceID INT, 
		NextEventTypeID INT, 
		NextEventTypeName VARCHAR(50), 
		ThreadNumber INT, 
		ChoiceDescription VARCHAR(50),
		EventSubTypeID INT,
		Weighting INT
	)

	DECLARE @PossibleEvents2 TABLE (
		ThreadNumber INT, 
		Counter INT
	)

	DECLARE @CursorEvents TABLE (
		EventChoiceID INT
	) 

	DECLARE @AAResult TABLE (
		ResultCode TINYINT	
	)
	
	
	/*Select events limited by a specific subtype*/
	IF @EventSubTypeID IS NOT NULL AND @ProcessStartID IS NOT NULL
	BEGIN
	
		INSERT INTO @PossibleEvents1(EventChoiceID, NextEventTypeID, NextEventTypeName, ThreadNumber, ChoiceDescription, EventSubTypeID, ec.Weighting)
		EXEC GetEventChoicesByCaseAndEvent_EventSubType @ClientID, @EventSubtypeID,	@ThreadNumber, @FromEventTypeID
	
	END
	
	/*Select All events*/
	IF @EventSubTypeID IS NULL AND @ProcessStartID IS NOT NULL
	BEGIN
	
		INSERT INTO @PossibleEvents1(EventChoiceID, NextEventTypeID, NextEventTypeName, ThreadNumber, ChoiceDescription, EventSubTypeID, ec.Weighting)
		EXEC GetEventChoicesByCaseAndEvent_All @ClientID, @ThreadNumber, @FromEventTypeID
	
	END
	
	/*No process start events applied*/
	IF @ProcessStartID IS NULL
	BEGIN
	
		INSERT INTO @PossibleEvents1(EventChoiceID, NextEventTypeID, NextEventTypeName, ThreadNumber, ChoiceDescription, EventSubTypeID, ec.Weighting)
		EXEC EnabledManualProcessStartEventsForLeadType @ClientID, @LeadTypeID
	
	END
	
	-- Remove from the list of events if that sub type is disabled
	DELETE @PossibleEvents1
	FROM @PossibleEvents1 p
	INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON p.EventSubTypeID = est.EventSubtypeID
	WHERE est.Available = 0
	
	-- Now have a crack at autoadjudication! 

	-- For each event type that could now be chosen in theory,
	-- work out which are not possible, and delete them from @PossibleEvents1
	DECLARE @DeleteStr VARCHAR(2000),
			@TestEventChoiceID INT

	-- Get all the potential event types into a working table we can 
	-- browse through one record at a time.
	INSERT INTO @CursorEvents (EventChoiceID)
	SELECT EventChoiceID FROM @PossibleEvents1

	SELECT TOP 1 @TestEventChoiceID = EventChoiceID
	FROM @CursorEvents
	
	WHILE @TestEventChoiceID > 0 
	BEGIN
		SELECT @DeleteStr = [SqlClauseForInclusion] FROM EventChoice WITH (NOLOCK) WHERE EventChoiceID = @TestEventChoiceID
		
		IF @DeleteStr IS NOT NULL AND RTRIM(@DeleteStr) <> ''
		BEGIN
		
			SELECT @DeleteStr = dbo.fnSubstituteSqlParams(@DeleteStr, @RunAsThisUser, @ClientID, @CustomerID, @LeadID, @CaseID, NULL, NULL)
			
			--TD - allow sqlclauseforinclusion to take clientpersonnelID
			SELECT @DeleteStr = REPLACE(@DeleteStr,'@ClientpersonnelID',@RunAsThisUser)
			SELECT @DeleteStr = REPLACE(@DeleteStr,'@MatterID',@MatterID)

			IF @DeleteStr LIKE '%exec%' --Rules Engine Eval
			BEGIN

				SELECT  @DeleteStr = REPLACE(@DeleteStr,'exec ','')
				INSERT @AAResult
				EXEC(@DeleteStr)
					
			END
			ELSE   -- standard sqlClauseForInclusion Eval
			BEGIN

				SELECT @DeleteStr = 'SELECT CASE WHEN ' + @DeleteStr + ' THEN 1 ELSE 0 END '
		
				INSERT @AAResult
				EXEC(@DeleteStr)

			END

			DELETE @PossibleEvents1 
			WHERE EventChoiceID = @TestEventChoiceID AND NOT EXISTS (SELECT * FROM @AAResult WHERE ResultCode = 1)

			DELETE @AAResult
		END

		DELETE FROM @CursorEvents
		WHERE EventChoiceID = @TestEventChoiceID

		SELECT TOP 1 @TestEventChoiceID = EventChoiceID
		FROM @CursorEvents

		IF @@ROWCOUNT < 1
		BEGIN
			SELECT @TestEventChoiceID = -1
		END

	END
	
	-- Group the data so we can pass a counter to the app
	INSERT INTO @PossibleEvents2(ThreadNumber, Counter)
	SELECT p1.ThreadNumber, COUNT(*) 
	FROM @PossibleEvents1 p1
	GROUP BY p1.ThreadNumber

	-- Finally, select the values
	SELECT p1.NextEventTypeID AS [EventTypeID], 
	p1.NextEventTypeName AS [EventTypeName], 
	p1.ThreadNumber AS [ThreadNumber], 
	p1.ChoiceDescription AS [ChoiceDescription], 
	p2.Counter AS [ChoicesInThread]
	FROM @PossibleEvents1 p1 
	INNER JOIN @PossibleEvents2 p2 ON p1.ThreadNumber = p2.ThreadNumber 
	ORDER BY p1.Weighting ASC, p1.ThreadNumber, p1.NextEventTypeName

END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoicesByCaseAndEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventChoicesByCaseAndEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventChoicesByCaseAndEvent] TO [sp_executeall]
GO
