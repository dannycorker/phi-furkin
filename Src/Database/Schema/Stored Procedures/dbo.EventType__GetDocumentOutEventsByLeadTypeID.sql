SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01-04-2008

-- Created By:  Ben
-- Purpose:		Get EventTypeName and EventTypeID for binding to a DropDownList.
-- 2012-01-03   JWG Added the new Excel Out type (19) to the list
-- 2014-03-27	PR Added 20 Case Summary
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetDocumentOutEventsByLeadTypeID] 
(
	@LeadTypeID int,
	@ClientID INT = NULL
)
AS
BEGIN
	SET ANSI_NULLS OFF

	SELECT
		[EventTypeID],
		[EventTypeName]
	FROM
		dbo.fnEventTypeShared(@ClientID) et
	WHERE
		LeadTypeID = @LeadTypeID
		AND EventSubTypeID in (4,6,19,20) 
	ORDER BY EventTypeName

	SET ANSI_NULLS ON
END






GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetDocumentOutEventsByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetDocumentOutEventsByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetDocumentOutEventsByLeadTypeID] TO [sp_executeall]
GO
