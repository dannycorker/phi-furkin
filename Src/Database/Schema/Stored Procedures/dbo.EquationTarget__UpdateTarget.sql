SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-01-05
-- Description:	Update Equation Target, set the IsEquation and DetailFieldSubType
-- =============================================
CREATE PROCEDURE [dbo].[EquationTarget__UpdateTarget]
@DetailFieldID int,
@DetailFieldSubTypeID int,
@IsEquation bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE EquationTarget
	SET IsEquation = @IsEquation, DetailFieldSubTypeID = @DetailFieldSubTypeID
	WHERE DetailFieldID = @DetailFieldID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget__UpdateTarget] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTarget__UpdateTarget] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTarget__UpdateTarget] TO [sp_executeall]
GO
