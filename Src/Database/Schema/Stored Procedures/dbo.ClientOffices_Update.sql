SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientOffices table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_Update]
(

	@ClientOfficeID int   ,

	@ClientID int   ,

	@OfficeName varchar (50)  ,

	@BuildingName varchar (50)  ,

	@Address1 varchar (200)  ,

	@Address2 varchar (200)  ,

	@Town varchar (200)  ,

	@County varchar (50)  ,

	@Country varchar (200)  ,

	@PostCode varchar (10)  ,

	@OfficeTelephone varchar (50)  ,

	@OfficeTelephoneExtension varchar (10)  ,

	@OfficeFax varchar (50)  ,

	@CountryID int   ,

	@LanguageID int   ,

	@SubClientID int   ,

	@AdminClientPersonnelID int   ,

	@CurrencyID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientOffices]
				SET
					[ClientID] = @ClientID
					,[OfficeName] = @OfficeName
					,[BuildingName] = @BuildingName
					,[Address1] = @Address1
					,[Address2] = @Address2
					,[Town] = @Town
					,[County] = @County
					,[Country] = @Country
					,[PostCode] = @PostCode
					,[OfficeTelephone] = @OfficeTelephone
					,[OfficeTelephoneExtension] = @OfficeTelephoneExtension
					,[OfficeFax] = @OfficeFax
					,[CountryID] = @CountryID
					,[LanguageID] = @LanguageID
					,[SubClientID] = @SubClientID
					,[AdminClientPersonnelID] = @AdminClientPersonnelID
					,[CurrencyID] = @CurrencyID
				WHERE
[ClientOfficeID] = @ClientOfficeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Update] TO [sp_executeall]
GO
