SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Lead table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_GetByWhenCreated]
(

	@WhenCreated datetime   
)
AS


				SELECT
					[LeadID],
					[ClientID],
					[LeadRef],
					[CustomerID],
					[LeadTypeID],
					[AquariumStatusID],
					[ClientStatusID],
					[BrandNew],
					[Assigned],
					[AssignedTo],
					[AssignedBy],
					[AssignedDate],
					[RecalculateEquations],
					[Password],
					[Salt],
					[WhenCreated]
				FROM
					[dbo].[Lead] WITH (NOLOCK) 
				WHERE
										[WhenCreated] = @WhenCreated
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByWhenCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_GetByWhenCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByWhenCreated] TO [sp_executeall]
GO
