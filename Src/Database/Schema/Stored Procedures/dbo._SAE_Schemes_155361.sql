SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Copy Scheme
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Schemes_155361]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@EventComments					VARCHAR(2000)
			,@FromMID						INT
			,@ToMID							INT
			,@CopyCDV						INT
/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

		/* 2017-05-02 CPS Restored SAE*/
	IF @EventTypeID IN (155361) /* Copy Scheme */
	BEGIN
		
		SELECT @FromMID	= dbo.fnGetSimpleDvAsInt (175457,@CaseID) /*MatterID Copying FROM*/
		SELECT @ToMID	= dbo.fnGetSimpleDvAsInt (175458,@CaseID) /*MatterID Copying TO*/
		SELECT @CopyCDV	= CASE WHEN dbo.fnGetSimpleDvAsInt (175459,@CaseID)=5144 THEN 1 ELSE 0 END /*Include Case Detail Values?*/
		
		EXEC _C00_SimpleValueIntoField 175457, '', @CaseID, @WhoCreated /*MatterID Copying FROM*/
		EXEC _C00_SimpleValueIntoField 175458, '', @CaseID, @WhoCreated	/*MatterID Copying TO*/
		
		SELECT @EventComments	+= 'Copied MatterID ' + ISNULL(CONVERT(VARCHAR,@FromMID),'NULL') 
								+  ' to MatterID ' + ISNULL(CONVERT(VARCHAR,@ToMID),'NULL')
								+  CASE WHEN @CopyCDV = 1 THEN ' including' ELSE ' excluding' END
								+  ' Affinity (Case) Details.'
		
		EXEC _C600_CopySchemesCase @FromMID, @ToMID, @CopyCDV
		
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END
	
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Schemes_155361] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Schemes_155361] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Schemes_155361] TO [sp_executeall]
GO
