SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-08-2015
-- Description:	Inserts a chat record and returns the ChatID
-- =============================================
CREATE PROCEDURE [dbo].[Chat__Insert]

	@ClientID INT,
	@ObjectID INT = NULL,
	@ChatGuid VARCHAR(50) = NULL,
	@ChatTypeID TINYINT = NULL,
	@ObjectTypeID TINYINT = NULL

AS
BEGIN
	
	
	SET NOCOUNT ON;

	INSERT INTO Chat (ClientID, ObjectID, ChatGuid, ChatTypeID, ObjectTypeID)
	VALUES (@ClientID, @ObjectID, @ChatGuid, @ChatTypeID, @ObjectTypeID)
	    
	DECLARE @ID INT
	SELECT @ID = SCOPE_IDENTITY()
	
	SELECT @ID ChatID
	       
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chat__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__Insert] TO [sp_executeall]
GO
