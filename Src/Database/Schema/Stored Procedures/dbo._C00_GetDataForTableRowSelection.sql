SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2019-07-05
-- Description:	Return data for application
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetDataForTableRowSelection]
	@ClientID INT,
	@MatterID INT,
	@DetailFieldID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SQLQueryID INT,
			@SQL VARCHAR(MAX)

	SELECT @SQLQueryID = cts.SQLQueryID
	FROM CustomTableSQL cts WITH (NOLOCK)
	WHERE cts.DetailFieldID = @DetailFieldID
	AND cts.ClientID = @ClientID

	SELECT @SQL = s.QueryText
	FROM SqlQuery s WITH (NOLOCK)
	WHERE s.QueryID = @SQLQueryID

	SELECT @SQL = REPLACE(@SQL, '@MatterID', CONVERT(VARCHAR,ISNULL(@MatterID,'')))
	SELECT @SQL = REPLACE(@SQL, '@ClientID', CONVERT(VARCHAR,ISNULL(@ClientID,'')))
	
	SELECT @SQL = @SQL + '
FOR JSON PATH, INCLUDE_NULL_VALUES'

	PRINT @SQL

	EXEC (@SQL)

	SELECT '[' + ISNULL(dbo.fnGetSimpleDv(@DetailFieldID, @MatterID), '') + ']' AS [SelectedValues]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataForTableRowSelection] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetDataForTableRowSelection] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataForTableRowSelection] TO [sp_executeall]
GO
