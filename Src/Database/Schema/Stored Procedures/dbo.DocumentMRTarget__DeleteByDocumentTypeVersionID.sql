SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

-- =============================================
-- Author:		<Paul Richardson>
-- Create date: <11-07-2017>
-- Description:	<Deletes the targets for the given document type version id>
-- =============================================
CREATE PROCEDURE [dbo].[DocumentMRTarget__DeleteByDocumentTypeVersionID]
(
@DocumentTypeVersionID int
)
AS


DELETE FROM [dbo].[DocumentMRTarget] WITH (ROWLOCK)
WHERE
[DocumentTypeVersionID] = @DocumentTypeVersionID


GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget__DeleteByDocumentTypeVersionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
