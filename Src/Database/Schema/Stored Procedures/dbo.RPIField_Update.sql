SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_Update]
(

	@RPIFieldID int   ,

	@OriginalRPIFieldID int   ,

	@RPIFieldName varchar (50)  ,

	@RPIFieldDescription varchar (500)  ,

	@IsEnabled bit   ,

	@QuestionTypeID int   ,

	@DetailFieldSubtypeID tinyint   ,

	@LookupListID int   ,

	@FieldOrder int   ,

	@FieldSize int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@RPIFieldGroupID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIField]
				SET
					[RPIFieldID] = @RPIFieldID
					,[RPIFieldName] = @RPIFieldName
					,[RPIFieldDescription] = @RPIFieldDescription
					,[IsEnabled] = @IsEnabled
					,[QuestionTypeID] = @QuestionTypeID
					,[DetailFieldSubtypeID] = @DetailFieldSubtypeID
					,[LookupListID] = @LookupListID
					,[FieldOrder] = @FieldOrder
					,[FieldSize] = @FieldSize
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
					,[RPIFieldGroupID] = @RPIFieldGroupID
				WHERE
[RPIFieldID] = @OriginalRPIFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_Update] TO [sp_executeall]
GO
