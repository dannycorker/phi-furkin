SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SubClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubClient_Delete]
(

	@SubClientID int   
)
AS


				DELETE FROM [dbo].[SubClient] WITH (ROWLOCK) 
				WHERE
					[SubClientID] = @SubClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubClient_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Delete] TO [sp_executeall]
GO
