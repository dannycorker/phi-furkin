SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SystemMessage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessage_Insert]
(

	@SystemMessageID int    OUTPUT,

	@ClientID int   ,

	@SystemMessageName varchar (200)  ,

	@SystemMessageText varchar (MAX)  ,

	@Enabled bit   
)
AS


				
				INSERT INTO [dbo].[SystemMessage]
					(
					[ClientID]
					,[SystemMessageName]
					,[SystemMessageText]
					,[Enabled]
					)
				VALUES
					(
					@ClientID
					,@SystemMessageName
					,@SystemMessageText
					,@Enabled
					)
				-- Get the identity value
				SET @SystemMessageID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessage_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessage_Insert] TO [sp_executeall]
GO
