SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-07-2012
-- Description:	Finds a lead based upon the given mobile number
--
-- This is the SP for the hack sms incoming post processor
--
-- Its Currently hardwired to look for customers in client 253 only
-- Modified By PR 24/06/2013 Gets most recent case / matter.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FindCustomerViaMobile]

	@MobilePhoneNumber VARCHAR(MAX)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	
	SELECT TOP 1 MobileTelephone, c.CustomerID, l.LeadID, cs.CaseID, c.ClientID, m.MatterID
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	INNER JOIN dbo.Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID AND m.CaseID = cs.CaseID
	WHERE c.ClientID=253 AND 
		  c.MobileTelephone <>'' AND 
		  REPLACE(MobileTelephone, ' ', '') = @MobilePhoneNumber		 
		  AND c.Test = 0
	ORDER BY cs.WhenCreated DESC

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerViaMobile] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindCustomerViaMobile] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerViaMobile] TO [sp_executeall]
GO
