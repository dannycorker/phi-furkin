SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the WorkflowGroupAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowGroupAssignment_Insert]
(

	@WorkflowGroupAssignmentID int    OUTPUT,

	@WorkflowGroupID int   ,

	@ClientPersonnelID int   ,

	@Priority int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[WorkflowGroupAssignment]
					(
					[WorkflowGroupID]
					,[ClientPersonnelID]
					,[Priority]
					,[ClientID]
					)
				VALUES
					(
					@WorkflowGroupID
					,@ClientPersonnelID
					,@Priority
					,@ClientID
					)
				-- Get the identity value
				SET @WorkflowGroupAssignmentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowGroupAssignment_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowGroupAssignment_Insert] TO [sp_executeall]
GO
