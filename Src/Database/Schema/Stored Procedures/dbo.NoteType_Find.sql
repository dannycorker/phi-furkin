SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the NoteType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType_Find]
(

	@SearchUsingOR bit   = null ,

	@NoteTypeID int   = null ,

	@ClientID int   = null ,

	@NoteTypeName varchar (50)  = null ,

	@NoteTypeDescription varchar (250)  = null ,

	@DefaultPriority smallint   = null ,

	@AlertColour nchar (7)  = null ,

	@NormalColour nchar (7)  = null ,

	@Enabled bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@SourceID int   = null ,

	@LeadTypeID int   = null ,

	@IsShared bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [NoteTypeID]
	, [ClientID]
	, [NoteTypeName]
	, [NoteTypeDescription]
	, [DefaultPriority]
	, [AlertColour]
	, [NormalColour]
	, [Enabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
	, [LeadTypeID]
	, [IsShared]
    FROM
	dbo.fnNoteTypeShared(@ClientID)
    WHERE 
	 ([NoteTypeID] = @NoteTypeID OR @NoteTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([NoteTypeName] = @NoteTypeName OR @NoteTypeName IS NULL)
	AND ([NoteTypeDescription] = @NoteTypeDescription OR @NoteTypeDescription IS NULL)
	AND ([DefaultPriority] = @DefaultPriority OR @DefaultPriority IS NULL)
	AND ([AlertColour] = @AlertColour OR @AlertColour IS NULL)
	AND ([NormalColour] = @NormalColour OR @NormalColour IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [NoteTypeID]
	, [ClientID]
	, [NoteTypeName]
	, [NoteTypeDescription]
	, [DefaultPriority]
	, [AlertColour]
	, [NormalColour]
	, [Enabled]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [SourceID]
	, [LeadTypeID]
	, [IsShared]
    FROM
	dbo.fnNoteTypeShared(@ClientID) 
    WHERE 
	 ([NoteTypeID] = @NoteTypeID AND @NoteTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([NoteTypeName] = @NoteTypeName AND @NoteTypeName is not null)
	OR ([NoteTypeDescription] = @NoteTypeDescription AND @NoteTypeDescription is not null)
	OR ([DefaultPriority] = @DefaultPriority AND @DefaultPriority is not null)
	OR ([AlertColour] = @AlertColour AND @AlertColour is not null)
	OR ([NormalColour] = @NormalColour AND @NormalColour is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Find] TO [sp_executeall]
GO
