SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetClientCustomerEmails]

@ClientQuestionnaireID int

AS

Select ClientCustomerMailID, ClientID, ClientQuestionnaireID, CustomerID, ClientAreaMailingListID,DateSent
From ClientCustomerMails
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientCustomerEmails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientCustomerEmails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientCustomerEmails] TO [sp_executeall]
GO
