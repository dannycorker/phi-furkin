SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-10-25
-- Description:	Pull documents for a specific client through the document queue.
--				Adapted from CreateDocumentZipsFromAutomatedTask
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateDocumentZipsForClient] 
	@ClientID INT,
	@DocumentsPerZip INT = 100,
	@DocumentTypeIDs dbo.tvpInt READONLY /*Pass in an empty table to do all*/,
	@ZipPrefix VARCHAR(50),
	@EmailAddress VARCHAR(500) = NULL,
	@OldestNDocuments INT = 999999,
	@SpecificDocumentQueueIDs dbo.tvpInt READONLY
AS
BEGIN
	;
	SET NOCOUNT ON;

	DECLARE @DocumentZipInformationID INT,
			@ServerMapPath VARCHAR(2000), 
			@SecurePath CHAR(1), 
			@HostPath VARCHAR(250), 
			@WordTempDirectory VARCHAR(250), 
			@StatusID INT,
			@DocumentTypeCount INT = 0

	SELECT	@ServerMapPath = 'ClientImages\Client_' + CONVERT(VARCHAR, @ClientID) + '_Images\DocumentZips', 
			@SecurePath = 's', 
			@HostPath = 'www.aquarium-software.com', 
			@WordTempDirectory = 'WordTemp',
			@StatusID = 35
	
	SELECT @DocumentTypeCount = COUNT(*)
	FROM @DocumentTypeIDs
	
	-- Insert a Zip Information header record for this batch of records
	INSERT INTO [dbo].[DocumentZipInformation] (ClientID, ServerMapPath, SecurePath, HostPath, WordTempDirectory, DocumentsPerZip, ZipPrefix, StatusID,EmailOnSuccess,EmailOnError)
	VALUES (@ClientID, @ServerMapPath, @SecurePath, @HostPath, @WordTempDirectory, @DocumentsPerZip, @ZipPrefix, @StatusID,@EmailAddress,@EmailAddress)

	SELECT @DocumentZipInformationID = SCOPE_IDENTITY()
	
	IF EXISTS ( SELECT * FROM @SpecificDocumentQueueIDs q )
	BEGIN
		-- Insert the detail records
		INSERT INTO dbo.DocumentZip (DocumentZipInformationID, ClientID, DocumentQueueID, StatusID) 
		SELECT TOP(@OldestNDocuments) @DocumentZipInformationID, @ClientID, dq.DocumentQueueID, @StatusID
		FROM dbo.DocumentQueue dq 
		INNER JOIN @SpecificDocumentQueueIDs sq on sq.AnyID = dq.DocumentQueueID
		WHERE dq.ClientID = @ClientID
		AND dq.[WhenCreated] IS NULL
		and dq.WhenStored > dbo.fn_GetDate_Local()-30
		AND dq.RequiresApproval = 0
		AND NOT EXISTS 
		(
			SELECT * 
			FROM DocumentZip dz 
			WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
		)
		ORDER BY CASE WHEN @ClientID = 322 THEN dbo.fn_C322_GetDocumentName(dq.DocumentTypeID,dq.CaseID)  ELSE CAST(dq.DocumentQueueID as VARCHAR(2000)) END
	END
	ELSE
	BEGIN
		-- Insert the detail records
		INSERT INTO dbo.DocumentZip (DocumentZipInformationID, ClientID, DocumentQueueID, StatusID) 
		SELECT TOP(@OldestNDocuments) @DocumentZipInformationID, @ClientID, dq.DocumentQueueID, @StatusID
		FROM dbo.DocumentQueue dq 
		LEFT JOIN @DocumentTypeIDs dt ON dt.AnyID = dq.DocumentTypeID
		WHERE dq.ClientID = @ClientID
		AND dq.[WhenCreated] IS NULL
		AND (dt.AnyID IS NOT NULL OR @DocumentTypeCount = 0)
		and dq.WhenStored > dbo.fn_GetDate_Local()-30
		AND dq.RequiresApproval = 0
		AND NOT EXISTS 
		(
			SELECT * 
			FROM DocumentZip dz 
			WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
		)
		ORDER BY dq.DocumentQueueID
	END
	
	IF @@ROWCOUNT = 0
	BEGIN
		DELETE dzi
		FROM DocumentZipInformation dzi
		WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID
		
		SELECT @DocumentZipInformationID = -1
	END
	
	RETURN @DocumentZipInformationID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDocumentZipsForClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateDocumentZipsForClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateDocumentZipsForClient] TO [sp_executeall]
GO
