SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the UserGroupMenuSubOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuSubOption_Update]
(

	@UserGroupMenuSubOptionID int   ,

	@UserGroupMenuOptionID int   ,

	@ClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@PanelItemName varchar (50)  ,

	@PanelItemCaption varchar (50)  ,

	@PanelItemURL varchar (250)  ,

	@ShowInFancyBox bit   ,

	@SubItemOrder int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[UserGroupMenuSubOption]
				SET
					[UserGroupMenuOptionID] = @UserGroupMenuOptionID
					,[ClientID] = @ClientID
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[PanelItemName] = @PanelItemName
					,[PanelItemCaption] = @PanelItemCaption
					,[PanelItemURL] = @PanelItemURL
					,[ShowInFancyBox] = @ShowInFancyBox
					,[SubItemOrder] = @SubItemOrder
				WHERE
[UserGroupMenuSubOptionID] = @UserGroupMenuSubOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_Update] TO [sp_executeall]
GO
