SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderFile table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFile_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderFileID int   = null ,

	@ClientID int   = null ,

	@DataLoaderMapID int   = null ,

	@FileStatusID int   = null ,

	@SourceFileNameAndPath varchar (2000)  = null ,

	@TargetFileName varchar (2000)  = null ,

	@TargetFileLocation varchar (2000)  = null ,

	@FileFormatID int   = null ,

	@ScheduledDateTime datetime   = null ,

	@DataLoadedDateTime datetime   = null ,

	@UploadedBy int   = null ,

	@RowsInFile int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderFileID]
	, [ClientID]
	, [DataLoaderMapID]
	, [FileStatusID]
	, [SourceFileNameAndPath]
	, [TargetFileName]
	, [TargetFileLocation]
	, [FileFormatID]
	, [ScheduledDateTime]
	, [DataLoadedDateTime]
	, [UploadedBy]
	, [RowsInFile]
    FROM
	[dbo].[DataLoaderFile] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileID] = @DataLoaderFileID OR @DataLoaderFileID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DataLoaderMapID] = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	AND ([FileStatusID] = @FileStatusID OR @FileStatusID IS NULL)
	AND ([SourceFileNameAndPath] = @SourceFileNameAndPath OR @SourceFileNameAndPath IS NULL)
	AND ([TargetFileName] = @TargetFileName OR @TargetFileName IS NULL)
	AND ([TargetFileLocation] = @TargetFileLocation OR @TargetFileLocation IS NULL)
	AND ([FileFormatID] = @FileFormatID OR @FileFormatID IS NULL)
	AND ([ScheduledDateTime] = @ScheduledDateTime OR @ScheduledDateTime IS NULL)
	AND ([DataLoadedDateTime] = @DataLoadedDateTime OR @DataLoadedDateTime IS NULL)
	AND ([UploadedBy] = @UploadedBy OR @UploadedBy IS NULL)
	AND ([RowsInFile] = @RowsInFile OR @RowsInFile IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderFileID]
	, [ClientID]
	, [DataLoaderMapID]
	, [FileStatusID]
	, [SourceFileNameAndPath]
	, [TargetFileName]
	, [TargetFileLocation]
	, [FileFormatID]
	, [ScheduledDateTime]
	, [DataLoadedDateTime]
	, [UploadedBy]
	, [RowsInFile]
    FROM
	[dbo].[DataLoaderFile] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileID] = @DataLoaderFileID AND @DataLoaderFileID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DataLoaderMapID] = @DataLoaderMapID AND @DataLoaderMapID is not null)
	OR ([FileStatusID] = @FileStatusID AND @FileStatusID is not null)
	OR ([SourceFileNameAndPath] = @SourceFileNameAndPath AND @SourceFileNameAndPath is not null)
	OR ([TargetFileName] = @TargetFileName AND @TargetFileName is not null)
	OR ([TargetFileLocation] = @TargetFileLocation AND @TargetFileLocation is not null)
	OR ([FileFormatID] = @FileFormatID AND @FileFormatID is not null)
	OR ([ScheduledDateTime] = @ScheduledDateTime AND @ScheduledDateTime is not null)
	OR ([DataLoadedDateTime] = @DataLoadedDateTime AND @DataLoadedDateTime is not null)
	OR ([UploadedBy] = @UploadedBy AND @UploadedBy is not null)
	OR ([RowsInFile] = @RowsInFile AND @RowsInFile is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_Find] TO [sp_executeall]
GO
