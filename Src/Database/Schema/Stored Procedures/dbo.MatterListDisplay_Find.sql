SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the MatterListDisplay table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_Find]
(

	@SearchUsingOR bit   = null ,

	@MatterListDisplayID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@MatterRef bit   = null ,

	@MatterStatus bit   = null ,

	@MatterID bit   = null ,

	@Field1 int   = null ,

	@Field2 int   = null ,

	@Field3 int   = null ,

	@Field4 int   = null ,

	@Field5 int   = null ,

	@Field1ColumnDetailFieldID int   = null ,

	@Field2ColumnDetailFieldID int   = null ,

	@Field3ColumnDetailFieldID int   = null ,

	@Field4ColumnDetailFieldID int   = null ,

	@Field5ColumnDetailFieldID int   = null ,

	@SqlQueryText varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MatterListDisplayID]
	, [ClientID]
	, [LeadTypeID]
	, [MatterRef]
	, [MatterStatus]
	, [MatterID]
	, [Field1]
	, [Field2]
	, [Field3]
	, [Field4]
	, [Field5]
	, [Field1ColumnDetailFieldID]
	, [Field2ColumnDetailFieldID]
	, [Field3ColumnDetailFieldID]
	, [Field4ColumnDetailFieldID]
	, [Field5ColumnDetailFieldID]
	, [SqlQueryText]
    FROM
	[dbo].[MatterListDisplay] WITH (NOLOCK) 
    WHERE 
	 ([MatterListDisplayID] = @MatterListDisplayID OR @MatterListDisplayID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([MatterRef] = @MatterRef OR @MatterRef IS NULL)
	AND ([MatterStatus] = @MatterStatus OR @MatterStatus IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([Field1] = @Field1 OR @Field1 IS NULL)
	AND ([Field2] = @Field2 OR @Field2 IS NULL)
	AND ([Field3] = @Field3 OR @Field3 IS NULL)
	AND ([Field4] = @Field4 OR @Field4 IS NULL)
	AND ([Field5] = @Field5 OR @Field5 IS NULL)
	AND ([Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID OR @Field1ColumnDetailFieldID IS NULL)
	AND ([Field2ColumnDetailFieldID] = @Field2ColumnDetailFieldID OR @Field2ColumnDetailFieldID IS NULL)
	AND ([Field3ColumnDetailFieldID] = @Field3ColumnDetailFieldID OR @Field3ColumnDetailFieldID IS NULL)
	AND ([Field4ColumnDetailFieldID] = @Field4ColumnDetailFieldID OR @Field4ColumnDetailFieldID IS NULL)
	AND ([Field5ColumnDetailFieldID] = @Field5ColumnDetailFieldID OR @Field5ColumnDetailFieldID IS NULL)
	AND ([SqlQueryText] = @SqlQueryText OR @SqlQueryText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MatterListDisplayID]
	, [ClientID]
	, [LeadTypeID]
	, [MatterRef]
	, [MatterStatus]
	, [MatterID]
	, [Field1]
	, [Field2]
	, [Field3]
	, [Field4]
	, [Field5]
	, [Field1ColumnDetailFieldID]
	, [Field2ColumnDetailFieldID]
	, [Field3ColumnDetailFieldID]
	, [Field4ColumnDetailFieldID]
	, [Field5ColumnDetailFieldID]
	, [SqlQueryText]
    FROM
	[dbo].[MatterListDisplay] WITH (NOLOCK) 
    WHERE 
	 ([MatterListDisplayID] = @MatterListDisplayID AND @MatterListDisplayID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([MatterRef] = @MatterRef AND @MatterRef is not null)
	OR ([MatterStatus] = @MatterStatus AND @MatterStatus is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([Field1] = @Field1 AND @Field1 is not null)
	OR ([Field2] = @Field2 AND @Field2 is not null)
	OR ([Field3] = @Field3 AND @Field3 is not null)
	OR ([Field4] = @Field4 AND @Field4 is not null)
	OR ([Field5] = @Field5 AND @Field5 is not null)
	OR ([Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID AND @Field1ColumnDetailFieldID is not null)
	OR ([Field2ColumnDetailFieldID] = @Field2ColumnDetailFieldID AND @Field2ColumnDetailFieldID is not null)
	OR ([Field3ColumnDetailFieldID] = @Field3ColumnDetailFieldID AND @Field3ColumnDetailFieldID is not null)
	OR ([Field4ColumnDetailFieldID] = @Field4ColumnDetailFieldID AND @Field4ColumnDetailFieldID is not null)
	OR ([Field5ColumnDetailFieldID] = @Field5ColumnDetailFieldID AND @Field5ColumnDetailFieldID is not null)
	OR ([SqlQueryText] = @SqlQueryText AND @SqlQueryText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Find] TO [sp_executeall]
GO
