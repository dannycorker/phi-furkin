SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-17
-- Description:	Create required CT and Payment records for a CPS
-- MODs
-- 2019-03-15 JEL Added CardTransactionID update for the payments table so it reconciles 
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreatePaymentAndCTForCPS]
	@CustomerPaymentScheduleID INT,
	@ClientPaymentGatewayID INT,
	@PaymentTypeID INT,
	@PaymentDescription VARCHAR(2000),
	@LeadEventID INT = NULL,
	@WhoCreated INT
AS
BEGIN

	SET NOCOUNT ON;

	CREATE TABLE #InsertedPayments (CustomerPaymentScheduleID INT, PaymentID INT)
	CREATE TABLE #InsertedCustomerLedger (CustomerLedgerID INT, PaymentID INT)
	
	DECLARE @NewCardTransactionID INT 

	INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, CustomerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	OUTPUT inserted.CustomerPaymentScheduleID, inserted.PaymentID INTO #InsertedPayments (CustomerPaymentScheduleID, PaymentID)
	SELECT cps.ClientID, 
			cps.CustomerID, 
			dbo.fn_GetDate_Local(),
			2 AS [PaymentTypeID],  /*1 = BACS*/
			NULL AS [DateReceived], 
			'' AS [PaymentDescription], 
			ISNULL(a.Reference,''), 
			cps.PaymentNet, 
			cps.PaymentVAT, 
			cps.PaymentGross, 
			NULL AS [PaymentCurrency], 
			NULL AS [RelatedOrderRef], 
			NULL AS [RelatedRequestDescription], 
			NULL AS [PayeeFullName], 
			NULL AS [MaskedAccountNumber], 
			NULL AS [AccountTypeDescription], 
			1 AS [PaymentStatusID], 
			NULL AS [DateReconciled], 
			NULL AS [ReconciliationOutcome], 
			cps.RelatedObjectID, 
			cps.RelatedObjectTypeID, 
			'' AS [Comments], 
			NULL AS [PaymentFileName], 
			NULL AS [FailureCode], 
			NULL AS [FailureReason], 
			cps.CustomerPaymentScheduleID, 
			@WhoCreated AS [WhoCreated], 
			dbo.fn_GetDate_Local() AS [WhenCreated], 
			@WhoCreated AS [WhoModified], 
			dbo.fn_GetDate_Local() AS [WhenModified]
	FROM CustomerPaymentSchedule cps 
	INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID = a.AccountID
	WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

	INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
	OUTPUT inserted.CustomerLedgerID, inserted.PaymentID INTO #InsertedCustomerLedger (CustomerLedgerID, PaymentID)
	SELECT p.ClientID, p.CustomerID, 
		dbo.fn_GetDate_Local(), 
		NULL AS [FailureCode], 
		NULL AS [FailureReason], 
		dbo.fn_GetDate_Local() AS [TransactionDate], 
		p.PaymentReference, 
		p.PaymentDescription, 
		p.PaymentNet,
		p.PaymentVAT, 
		p.PaymentGross, 
		@LeadEventID AS [LeadEventID], 
		p.ObjectID AS [ObjectID], 
		p.ObjectTypeID, 
		p.PaymentID, 
		NULL AS [OutgoingPaymentID], 
		@WhoCreated, 
		dbo.fn_GetDate_Local() AS [WhenCreated]
	FROM #InsertedPayments ip 
	INNER JOIN Payment p WITH (NOLOCK) ON p.PaymentID = ip.PaymentID

	INSERT INTO CardTransaction (ClientID, CustomerID, ObjectID, ObjectTypeID, PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID, ClientPaymentGatewayID, PaymentTypeID, PreValidate, OrderID, ReferenceNumber, Description, Amount, CurrencyID, FirstName, LastName, CardName, Number, ExpiryMonth, ExpiryYear, Code, AuthCode, AvsCode, AvsIndicator, Address, Address2, City, StateProvince, Country, ZipPostal, BankAccountType, BankCode, BankName, Company, GatewayCustomerID, TransactionID, TransactionTypeID, Email, Fax, Phone, Partner, Certificate, ClientIP, Referrer, ErrorCode, ErrorMessage, WhoCreated, WhenCreated, CardTypeID, StatusMsg, CustomerRefNum, ParentID, Filename, AccountID)
	SELECT cps.ClientID, cps.CustomerID, cps.RelatedObjectID, cps.RelatedObjectTypeID, NULL, 
			cps.CustomerPaymentScheduleID, 
			@ClientPaymentGatewayID, 
			@PaymentTypeID, 
			NULL AS [PreValidate], 
			a.CardToken AS OrderID, 
			NULL AS ReferenceNumber, 
			@PaymentDescription AS Description, 
			cps.PaymentGross, 
			NULL AS [Currency], 
			a.FirstName, 
			a.LastName, 
			NULL AS CardName, 
			NULL AS Number, 
			a.ExpiryMonth, 
			a.ExpiryYear, 
			NULL AS Code, 
			NULL AS AuthCode, 
			NULL AS AvsCode, 
			NULL AS AvsIndicator, 
			NULL AS Address, 
			NULL AS Address2, 
			NULL AS City, 
			NULL AS StateProvince, 
			NULL AS Country, 
			NULL AS ZipPostal, 
			NULL AS BankAccountType, 
			NULL AS BankCode, 
			a.Bankname, 
			NULL AS Company, 
			NULL AS GatewayCustomerID, 
			NULL AS TransactionID, 
			NULL AS TransactionTypeID, 
			NULL AS Email, 
			NULL AS Fax, 
			NULL AS Phone, 
			NULL AS Partner, 
			NULL AS Certificate, 
			NULL AS ClientIP,
			NULL AS Referrer, 
			NULL AS ErrorCode, 
			NULL AS ErrorMessage, 
			@WhoCreated, 
			dbo.fn_GetDate_Local(), 
			NULL AS [CardTypeID], 
			NULL AS [StatusMsg], 
			NULL AS [CustomerRefNum], 
			NULL AS [ParentID], 
			NULL AS [Filename], 
			a.AccountID
	FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
	INNER JOIN #InsertedPayments i ON i.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID
	
	SELECT @NewCardTransactionID = SCOPE_IDENTITY() 

	Update p 
	SET CardTransactionID = @NewCardTransactionID 
	FROM Payment p
	INNER JOIN #InsertedPayments ipay on ipay.PaymentID = p.paymentID 
			
	UPDATE cps
	SET CustomerLedgerID = icl.CustomerLedgerID
	FROM CustomerPaymentSchedule cps 
	INNER JOIN #InsertedPayments ipay ON ipay.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID 
	INNER JOIN #InsertedCustomerLedger icl ON icl.PaymentID = ipay.PaymentID

	/*Set the Purchased Product Payment Schedule, set the customer ledger id and rec dates*/
	UPDATE pps
	SET CustomerLedgerID = cps.CustomerLedgerID
	FROM CustomerPaymentSchedule cps
	INNER JOIN #InsertedPayments ipay ON ipay.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID 
	INNER JOIN dbo.PurchasedProductPaymentSchedule pps on cps.CustomerPaymentScheduleID = pps.CustomerPaymentScheduleID

	SELECT @NewCardTransactionID 

	RETURN @NewCardTransactionID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePaymentAndCTForCPS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreatePaymentAndCTForCPS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePaymentAndCTForCPS] TO [sp_executeall]
GO
