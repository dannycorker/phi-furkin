SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-20
-- Description:	Gets a purchased product payment schedule for the given customer payment schedule
--				Include the product information and the account id that the product was purchased with
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentSchedule__GetByCustomerPaymentSchedule]
	@CustomerPaymentScheduleID INT,	
	@CustomerID INT,
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT schedule.PurchasedProductPaymentScheduleID, schedule.ClientID	, schedule.CustomerID, schedule.AccountID, schedule.PurchasedProductID, schedule.ActualCollectionDate, schedule.CoverFrom, schedule.CoverTo, schedule.PaymentDate, schedule.PaymentNet, schedule.PaymentVAT, schedule.PaymentGross, schedule.PaymentStatusID, schedule.CustomerLedgerID, schedule.ReconciledDate, schedule.CustomerPaymentScheduleID, schedule.WhoCreated	, schedule.WhenCreated, schedule.PurchasedProductPaymentScheduleParentID, schedule.PurchasedProductPaymentScheduleTypeID, schedule.PaymentGroupedIntoID, 
	ps.*,
	pp.*
    FROM PurchasedProductPaymentSchedule schedule WITH (NOLOCK) 
    LEFT JOIN dbo.PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = schedule.PaymentStatusID
    INNER JOIN dbo.PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = schedule.PurchasedProductID
    WHERE schedule.CustomerPaymentScheduleID = @CustomerPaymentScheduleID AND schedule.CustomerID=@CustomerID AND schedule.ClientID=@ClientID
	ORDER BY schedule.PaymentDate ASC   
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetByCustomerPaymentSchedule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentSchedule__GetByCustomerPaymentSchedule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentSchedule__GetByCustomerPaymentSchedule] TO [sp_executeall]
GO
