SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EsignatureStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureStatus_GetByEsignatureStatusID]
(

	@EsignatureStatusID int   
)
AS


				SELECT
					[EsignatureStatusID],
					[EsignatureStatusDescription],
					[EsignatureStatus]
				FROM
					[dbo].[EsignatureStatus] WITH (NOLOCK) 
				WHERE
										[EsignatureStatusID] = @EsignatureStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_GetByEsignatureStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureStatus_GetByEsignatureStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_GetByEsignatureStatusID] TO [sp_executeall]
GO
