SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudication table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_GetByEventChoiceID]
(

	@EventChoiceID INT   
)
AS
BEGIN

	SET ANSI_NULLS ON
	
	SELECT
		[AutoAdjudicationID],
		[EventChoiceID],
		[AutoAdjudicationTypeID],
		[ObjectID],
		[AutoAdjudicationFieldTypeID],
		[Comparison],
		[Value1],
		[Value2],
		[AppliesTo],
		[COUNT]
	FROM
		[dbo].[AutoAdjudication] WITH (NOLOCK) 
	WHERE
		[EventChoiceID] = @EventChoiceID
	ORDER BY
		[AutoAdjudicationID] DESC
	
	SELECT @@ROWCOUNT
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_GetByEventChoiceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_GetByEventChoiceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_GetByEventChoiceID] TO [sp_executeall]
GO
