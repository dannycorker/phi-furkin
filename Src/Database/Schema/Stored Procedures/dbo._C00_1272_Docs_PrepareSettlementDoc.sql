SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Description:	Prepares a list of formatted deduction reasons for inclusion in a settlement doc
-- Used by:		SAE
-- Modified		
-- ROH 2013-06-12 Fix bug that failed to exclude non-payment and stopped payment rows
-- ROH 2014-09-01 NCI Wording change for deductions intro. #TODO Put it in a resource list.
-- ROH 2014-09-01 Client (0,@ClientID) restriction when getting paragraph texts from RLs
-- SB  2014-09-08 Changed to use policy lead function
-- ROH 2014-10-02 Below excess settlement wording
-- ROH 2014-10-21 Correction to the event used to determine a below-excess claim
-- ROH 2014-10-22 Save total excess to mdv for use in docs
-- ROH 2014-11-05 Fix to problem where text for multiple clients was being returned Zen#29440
-- ROH 2014-11-20 Settlement letter summary to support variants by payment method
-- ROH 2014-12-15 Added a new {UnpaidAmount} placeholder for the value of payment rows yet to be paid (for reassessments) Zen#29435
-- ROH 2014-12-15 Check for unpaid rows only when determining payee count Zen#29435
-- DCM 2016-02-09 Add generic replacement function for merge codes to augment original hard-coded codes. 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Docs_PrepareSettlementDoc] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	DECLARE 
		@ClientID INT
		,@ClaimLeadID INT
	SELECT 
		@ClientID = ClientID 
		,@ClaimLeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	-- First we need to do the settlement header for the doc.
	
	--Clear out the matter field and the table for multiple values
	EXEC dbo._C00_SimpleValueIntoField 152764, '', @MatterID
	EXEC dbo._C00_SimpleValueIntoField 156628, '', @MatterID
	
	DELETE dbo.TableRows
	WHERE DetailFieldID = 152765
	AND DetailFieldPageID = 16188
	AND ClientID = @ClientID
	AND MatterID = @MatterID
	
	-- The first bit is the summary
	
	DECLARE @PaymentInfo TABLE
	(
		ID INT IDENTITY,
		PayTo INT,
		Payee VARCHAR(2000),
		Amount MONEY,
		UnpaidAmount MONEY,
		PaymentMethod INT,
		AccountNumber VARCHAR(20),
		UnpaidRows INT
	)	
	
	INSERT @PaymentInfo(PayTo, Payee, Amount, UnpaidAmount, PaymentMethod, AccountNumber, UnpaidRows)
	SELECT tdvPayTo.ValueInt, tdvPayee.DetailValue
		, SUM(tdvAmount.ValueMoney) -- Paid amount
		, SUM(CASE WHEN tdvDatePaid.ValueDate IS NULL THEN tdvAmount.ValueMoney ELSE 0 END) -- Unpaid amount
		, ISNULL(tdvPayMethod.ValueInt, 0), ISNULL(tdvAccountNumber.DetailValue, '')
		, SUM(CASE WHEN tdvDatePaid.ValueDate IS NULL THEN 1 ELSE 0 END) -- Unpaid Rows
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvPayTo WITH (NOLOCK) ON r.TableRowID = tdvPayTo.TableRowID AND tdvPayTo.DetailFieldID = 159408
	INNER JOIN dbo.TableDetailValues tdvPayee WITH (NOLOCK) ON r.TableRowID = tdvPayee.TableRowID AND tdvPayee.DetailFieldID = 154490
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 154518
	INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 154517
	LEFT JOIN dbo.TableDetailValues tdvDatePaid WITH (NOLOCK) ON r.TableRowID = tdvDatePaid.TableRowID AND tdvDatePaid.DetailFieldID = 154521
	LEFT JOIN dbo.TableDetailValues tdvStopped WITH (NOLOCK) ON r.TableRowID = tdvStopped.TableRowID AND tdvStopped.DetailFieldID = 159289
	LEFT JOIN dbo.TableDetailValues tdvPayMethod WITH (NOLOCK) ON r.TableRowID = tdvPayMethod.TableRowID AND tdvPayMethod.DetailFieldID = 170232
	LEFT JOIN dbo.TableDetailValues tdvAccountNumber WITH (NOLOCK) ON r.TableRowID = tdvAccountNumber.TableRowID AND tdvAccountNumber.DetailFieldID = 175270
	WHERE r.MatterID = @MatterID
	AND tdvType.DetailValue = '0' -- Exclude refunds and cancellations
	AND tdvStopped.ValueDate IS NULL -- Exclude stopped payments
	GROUP BY tdvPayTo.ValueInt, tdvPayee.DetailValue, tdvPayMethod.ValueInt, tdvAccountNumber.DetailValue
	
	DECLARE @PayeeCount INT,
			@PayTo INT,
			@Payee VARCHAR(2000),
			@Amount MONEY = 0,
			@UnpaidAmount MONEY = 0,
			@Text VARCHAR(2000),
			@PetName VARCHAR(2000),
			@PaymentMethod INT,
			@AccountNumber VARCHAR(20)
		
	SELECT @PetName = DetailValue
	FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
	WHERE ldv.LeadID = @PolicyLeadID
	AND ldv.DetailFieldID = 144268
		
	SELECT @PayeeCount = COUNT(*)
	FROM @PaymentInfo
	WHERE UnpaidRows > 0
	
	IF @PayeeCount > 1
	BEGIN
		SELECT @PayTo = 56578, @Payee = '', @Amount = 0, @UnpaidAmount = 0, @AccountNumber = 0, @PaymentMethod = 70018 -- Force payment method to cheque for 
	END
	ELSE
	BEGIN
		SELECT @Amount = SUM(Amount)
		FROM @PaymentInfo

		SELECT TOP 1 @PayTo = PayTo, @Payee = Payee, @UnpaidAmount = UnpaidAmount, @PaymentMethod = PaymentMethod, @AccountNumber = AccountNumber
		FROM @PaymentInfo
		--WHERE UnpaidRows > 0
	END
	
	-- Check for a below-excess zero payment settlement
	DECLARE @BelowExcess BIT = 0
	SELECT TOP 1 @BelowExcess = 1
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.CaseID = m.CaseID and le.EventTypeID = 150192 and le.EventDeleted = 0
	WHERE m.MatterID = @MatterID
	AND (@PayeeCount = 0 OR @PayeeCount IS NULL) -- Only if no payment rows either (handles an earlier below-excess decision being reassessed
	
	IF @BelowExcess = 0
	BEGIN
		
		-- Settlement letter summary
		SELECT TOP 1 @Text = REPLACE(REPLACE(REPLACE(REPLACE(rdvText.DetailValue
								, '{Amount}',	'£' + CAST(@Amount AS VARCHAR))
								, '{UnpaidAmount}',	'£' + CAST(@UnpaidAmount AS VARCHAR))
								, '{Payee}', @Payee)
								, '{BankLast4}', RIGHT(@AccountNumber, 4) )
		FROM dbo.ResourceList rl WITH (NOLOCK) 
		INNER JOIN dbo.ResourceListDetailValues rdvPayTo WITH (NOLOCK) ON rl.ResourceListID = rdvPayTo.ResourceListID AND rdvPayTo.DetailFieldID = 152766
		INNER JOIN dbo.ResourceListDetailValues rdvText WITH (NOLOCK) ON rl.ResourceListID = rdvText.ResourceListID AND rdvText.DetailFieldID = 152767
		LEFT JOIN dbo.ResourceListDetailValues rdvPayMethod WITH (NOLOCK) ON rl.ResourceListID = rdvPayMethod.ResourceListID AND rdvPayMethod.DetailFieldID = 175305
		WHERE rl.clientid = @ClientID
		and rdvPayTo.ValueInt = @PayTo
		and (
				@PaymentMethod = rdvPayMethod.ValueInt
				OR
				rdvPayMethod.ValueInt IS NULL
				OR
				rdvPayMethod.ValueInt = 0
			)
		ORDER BY rdvPayMethod.ValueInt DESC -- Ensures (with TOP 1) that a match is returned before a generic record
	
	END
	ELSE IF @BelowExcess = 1
	BEGIN 
		
		-- Below excess miscellaneous paragraph
		SELECT @Text = REPLACE(rdvText.DetailValue, '{PetName}', ISNULL(@PetName, 'your animal'))
		FROM dbo.ResourceList r WITH (NOLOCK) 
		INNER JOIN dbo.ResourceListDetailValues rdvCode WITH (NOLOCK) ON r.ResourceListID = rdvCode.ResourceListID AND rdvCode.DetailFieldID = 155936
		INNER JOIN dbo.ResourceListDetailValues rdvText WITH (NOLOCK) ON r.ResourceListID = rdvText.ResourceListID AND rdvText.DetailFieldID = 155937
		WHERE r.ClientID = @ClientID
		AND rdvCode.DetailValue = 'BELOWEXCESS'
	
	END
	
	-- Check for dead pet
	DECLARE @DateOfDeath DATE
	SELECT @DateOfDeath = ValueDate
	FROM dbo.LeadDetailValues ldv WITH (NOLOCK) 
	WHERE ldv.LeadID = @PolicyLeadID
	AND ldv.DetailFieldID = 144271

	DECLARE @DeadOrGetWellText VARCHAR(2000)
	SELECT @DeadOrGetWellText = REPLACE(rdvText.DetailValue, '{PetName}', ISNULL(@PetName, 'your animal'))
	FROM dbo.ResourceList r WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvCode WITH (NOLOCK) ON r.ResourceListID = rdvCode.ResourceListID AND rdvCode.DetailFieldID = 155936
	INNER JOIN dbo.ResourceListDetailValues rdvText WITH (NOLOCK) ON r.ResourceListID = rdvText.ResourceListID AND rdvText.DetailFieldID = 155937
	WHERE r.ClientID = @ClientID
	AND (
			(@DateOfDeath IS NOT NULL AND rdvCode.DetailValue = 'DEADPET')
		OR
			(@DateOfDeath IS NULL AND rdvCode.DetailValue = 'GETWELLSOON')
		)

	-- do any remaining generic aliases substitution
	SELECT @Text=dbo.fn_C600_ReplaceAliasesInBraces ( @Text, @MatterID, 'Matter' )
	SELECT @DeadOrGetWellText=dbo.fn_C600_ReplaceAliasesInBraces ( @DeadOrGetWellText, @MatterID, 'Matter' )
	
	IF @DateOfDeath IS NOT NULL
	BEGIN
		
		EXEC dbo._C00_SimpleValueIntoField 175404, '', @MatterID -- Clear GetWellSoon text	
		EXEC dbo._C00_SimpleValueIntoField 156628, @DeadOrGetWellText, @MatterID -- Set DeadPet text
		
	END
	ELSE
	BEGIN
	
		EXEC dbo._C00_SimpleValueIntoField 156628, '', @MatterID -- Clear DeadPet text	
		EXEC dbo._C00_SimpleValueIntoField 175404, @DeadOrGetWellText, @MatterID -- Set GetWellSoon text
	
	END

	-- Set Settlement letter intro text	
	EXEC dbo._C00_SimpleValueIntoField 152764, @Text, @MatterID


	-- And then if we have multiple recipients we need to write to another table
	IF @PayeeCount > 1
	BEGIN
	
		DECLARE @Inserted TABLE
		(
			ID INT IDENTITY,
			TableRowID INT
		)
		-- Keep track of the table rows inserted
		INSERT dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
		OUTPUT inserted.TableRowID INTO @Inserted
		SELECT @ClientID, @MatterID, 152765, 16188, ID
		FROM @PaymentInfo
		
		INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, MatterID, ClientID, DetailValue)
		SELECT i.TableRowID, 148348, @MatterID, @ClientID, p.Payee + ': £' + CAST(Amount AS VARCHAR)
		FROM @PaymentInfo p
		INNER JOIN @Inserted i ON p.ID = i.ID 
		
	END
	
	
	-- Now handle the deductions
	
	---- Clear out any existing rows for this matter
	--DELETE dbo.TableRows
	--WHERE DetailFieldID = 148349
	--AND DetailFieldPageID = 16188
	--AND ClientID = @ClientID
	--AND MatterID = @MatterID
	
	
	-- Get some default values
	DECLARE @Claimed MONEY
	SELECT @Claimed = SUM(ValueMoney)
	FROM dbo.TableDetailValues
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144353
	
	DECLARE @UserDeductions TABLE
	(
		Text VARCHAR(2000)
	)
	
	DECLARE @Deductions TABLE
	(
		ID INT IDENTITY,
		Text VARCHAR(2000)
	)

	DECLARE @TableRows TABLE 
	(
		ID INT IDENTITY,
		TableRowID INT
	)
	
	-- Do the system deductions
	
	-- Outside policy cover
	DECLARE @NoCover MONEY
	SELECT @NoCover = SUM(ValueMoney * -1) 
	FROM dbo.TableDetailValues
	WHERE MatterID = @MatterID
	AND DetailFieldID = 147001
	
	IF @NoCover > 0
	BEGIN
		DECLARE @PolicyType INT
		SELECT @PolicyType = dbo.fn_C00_1272_GetPolicyTypeFromClaim(@MatterID)
		
		DECLARE @OutsiteCoverType INT -- Outside Policy Cover OR Outside Policy Cover - 12 month
		SELECT @OutsiteCoverType = CASE WHEN @PolicyType = 42931 THEN 46782 ELSE 46778 END
		
		INSERT @Deductions (Text)
		SELECT REPLACE(rdvText.DetailValue, '{Deduction}',	'£' + CAST(@NoCover AS VARCHAR))
		FROM dbo.ResourceListDetailValues rdvText
		INNER JOIN dbo.ResourceListDetailValues rdvType ON rdvText.ResourceListID = rdvType.ResourceListID 
		WHERE rdvType.DetailFieldID = 148400
		and rdvType.ClientID IN (0, @ClientID)
		AND rdvText.DetailFieldID = 148401
		AND rdvType.ValueInt = @OutsiteCoverType
		
	END
	
	
	-- Excess
	DECLARE @Excess MONEY
	SELECT @Excess = SUM(tdvExcess.ValueMoney * -1) 
	FROM dbo.TableRows r
	INNER JOIN dbo.TableDetailValues tdvExcess ON r.TableRowID = tdvExcess.TableRowID AND tdvExcess.DetailFieldID = 146406
	WHERE r.MatterID = @MatterID
	
	DECLARE @ExcessRebate MONEY = 0
	SELECT @ExcessRebate = SUM(tdvRebate.ValueMoney) 
	FROM dbo.TableRows r
	INNER JOIN dbo.TableDetailValues tdvRebate ON r.TableRowID = tdvRebate.TableRowID AND tdvRebate.DetailFieldID = 147434
	WHERE r.MatterID = @MatterID
	
	IF @Excess > 0
	BEGIN
		IF @ExcessRebate > 0
		BEGIN
			INSERT @Deductions (Text)
			SELECT REPLACE(REPLACE(rdvText.DetailValue, 
						'{Deduction}',	'£' + CAST(@Excess - @ExcessRebate AS VARCHAR)),
						'{Rebate}',		'£' + CAST(@ExcessRebate AS VARCHAR))
			FROM dbo.ResourceListDetailValues rdvText
			INNER JOIN dbo.ResourceListDetailValues rdvType ON rdvText.ResourceListID = rdvType.ResourceListID 
			WHERE rdvType.DetailFieldID = 148400
			AND rdvText.DetailFieldID = 148401
			AND rdvType.ValueInt = 46848
			and rdvType.ClientID IN (0, @ClientID)
		END
		ELSE
		BEGIN
			INSERT @Deductions (Text)
			SELECT REPLACE(rdvText.DetailValue, '{Deduction}',	'£' + CAST(@Excess AS VARCHAR))
			FROM dbo.ResourceListDetailValues rdvText
			INNER JOIN dbo.ResourceListDetailValues rdvType ON rdvText.ResourceListID = rdvType.ResourceListID 
			WHERE rdvType.DetailFieldID = 148400
			AND rdvText.DetailFieldID = 148401
			AND rdvType.ValueInt = 46779
			and rdvType.ClientID IN (0, @ClientID)
		END
	END
	
	-- Co-Insurance
	DECLARE @CoIns MONEY
	SELECT @CoIns = SUM(ValueMoney * -1) 
	FROM dbo.TableDetailValues
	WHERE MatterID = @MatterID
	AND DetailFieldID = 146407
	
	IF @CoIns > 0
	BEGIN
		INSERT @Deductions (Text)
		SELECT REPLACE(rdvText.DetailValue, '{Deduction}',	'£' + CAST(@CoIns AS VARCHAR))
		FROM dbo.ResourceListDetailValues rdvText
		INNER JOIN dbo.ResourceListDetailValues rdvType ON rdvText.ResourceListID = rdvType.ResourceListID 
		WHERE rdvType.DetailFieldID = 148400
		AND rdvText.DetailFieldID = 148401
		AND rdvType.ValueInt = 46780
		and rdvType.ClientID IN (0, @ClientID)
			
	END
	
	-- Limit reached
	;WITH Limits AS 
	(
		SELECT SUM(tdvLimit.ValueMoney * -1) AS LimitReached, tdvSection.ResourceListID
		FROM dbo.TableRows r
		INNER JOIN dbo.TableDetailValues tdvSection ON r.TableRowID = tdvSection.TableRowID AND tdvSection.DetailFieldID = 144350
		INNER JOIN dbo.TableDetailValues tdvLimit ON r.TableRowID = tdvLimit.TableRowID AND tdvLimit.DetailFieldID = 146408
		WHERE r.MatterID = @MatterID
		GROUP BY tdvSection.ResourceListID
		HAVING SUM(tdvLimit.ValueMoney * -1) > 0
	),
	LimitsWithText AS 
	(
		SELECT l.LimitReached,
				CASE
					WHEN llSubSection.LookupListItemID = 73857 THEN llSection.ItemValue
					ELSE llSubSection.ItemValue
				END AS Section
		FROM Limits l
		INNER JOIN dbo.ResourceListDetailValues rdvSection ON l.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
		INNER JOIN dbo.LookupListItems llSection ON rdvSection.ValueInt = llSection.LookupListItemID
		INNER JOIN dbo.ResourceListDetailValues rdvSubSection ON l.ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
		INNER JOIN dbo.LookupListItems llSubSection ON rdvSubSection.ValueInt = llSubSection.LookupListItemID
	)
	
	INSERT @Deductions (Text)
	SELECT REPLACE(REPLACE(rdvText.DetailValue, 
					'{Deduction}',		'£' + CAST(l.LimitReached AS VARCHAR)),
					'{PolicySection}',	LOWER(l.Section))
	FROM dbo.ResourceListDetailValues rdvText
	INNER JOIN dbo.ResourceListDetailValues rdvType ON rdvText.ResourceListID = rdvType.ResourceListID 
	CROSS JOIN LimitsWithText l
	WHERE rdvType.DetailFieldID = 148400
	AND rdvText.DetailFieldID = 148401
	AND rdvType.ValueInt = 46781
	and rdvType.ClientID IN (0, @ClientID)	
	
	
	-- Get the user deduction text from the RL
	
	;WITH ReasonData AS
	(
		SELECT	tdvReason.ValueInt AS Reason, (tdvAmount.ValueMoney * -1) AS Amount, tdvItemNotIncluded.DetailValue AS ItemNotIncluded
		FROM dbo.TableRows r WITH (NOLOCK)
		INNER JOIN dbo.TableDetailValues tdvReason WITH (NOLOCK) ON r.TableRowID = tdvReason.TableRowID AND tdvReason.DetailFieldID = 147300
		INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 147301
		LEFT JOIN dbo.TableDetailValues tdvItemNotIncluded WITH (NOLOCK) ON r.TableRowID = tdvItemNotIncluded.TableRowID AND tdvItemNotIncluded.DetailFieldID = 148432
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = 147302
		AND r.DetailFieldPageID = 16157
		and r.ClientID = @ClientID
		)
	, Reasons AS 
	(
		SELECT	r.Reason, SUM(r.Amount) AS Amount, 
				/*
				http://stackoverflow.com/questions/273238/how-to-use-group-by-to-concatenate-strings-in-sql-serverHH
				We want to get a comma separated list of item not included text so this bit of easy to read SQL ;-) does this...
				Gets a distinct list of text where we have a value, adds a comma to each one, outputs as XML with no root node, 
				selects out the value of the xml and then stuffs this together removing the leading comma and space... simples!
				*/
				STUFF((	SELECT DISTINCT ', ' + CAST(ItemNotIncluded AS VARCHAR(MAX))
						FROM ReasonData 
						WHERE (Reason = r.Reason) 
						AND ItemNotIncluded > ''
						FOR XML PATH(''), TYPE).value('(./text())[1]', 'VARCHAR(MAX)'), 1, 2, '') AS ItemNotIncluded
		FROM ReasonData r
		GROUP BY r.Reason
	), 
	FormattedReasons AS
	(
		-- Replace the last ', ' with 'and', force to lower case and handle nulls
		SELECT	Reason, Amount, 
				CASE
					WHEN CHARINDEX(', ', ItemNotIncluded) > 0 THEN ISNULL(LOWER(REVERSE(REVERSE(STUFF(ItemNotIncluded, CHARINDEX(', ', ItemNotIncluded, 1), 1, ' and')))), '')
					ELSE ISNULL(LOWER(ItemNotIncluded), '')
				END AS ItemNotIncluded
		FROM Reasons
	)
	
	INSERT @UserDeductions (Text)
	SELECT REPLACE(REPLACE(REPLACE(rdvText.DetailValue, 
					'{Deduction}',		'£' + CAST(r.Amount AS VARCHAR)), 
					'{Claimed}',		'£' + CAST(@Claimed AS VARCHAR)),
					'{ItemNotCovered}',	r.ItemNotIncluded)
	FROM FormattedReasons r	
	INNER JOIN dbo.ResourceListDetailValues rdvReason ON r.Reason = rdvReason.ValueInt AND rdvReason.DetailFieldID = 148350
	INNER JOIN dbo.ResourceListDetailValues rdvText ON rdvReason.ResourceListID = rdvText.ResourceListID AND rdvText.DetailFieldID = 148351
	WHERE rdvReason.ClientID IN (0, @ClientID)
	
	DECLARE @Count INT
	SELECT @Count = COUNT(*) 
	FROM @UserDeductions
	
	IF @Count > 0 -- If we have any user deductions
	BEGIN
			
		-- We default a new intro row
		INSERT @Deductions (Text)
		--SELECT 'Please note that we have also made the following deductions from your settlement in accordance with your policy wording.'
		SELECT 'In addition to your policy excess (if applicable) we have also made the following deductions on your claim.'
		
		INSERT @Deductions(Text)
		SELECT Text
		FROM @UserDeductions
	
	END
	
	-- Save the data
	INSERT dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID)
	OUTPUT inserted.TableRowID INTO @TableRows
	SELECT @ClientID, @MatterID, 148349, 16188
	FROM @Deductions
	
	INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, DetailValue, MatterID, ClientID)
	SELECT r.TableRowID, 148348, t.Text + CHAR(13) + CHAR(10), @MatterID, @ClientID
	FROM @Deductions t
	INNER JOIN @TableRows r ON t.ID = r.ID
	
	-- Save the total settle and excess amounts
	DECLARE 
		@TotalSettle MONEY
		,@TotalExcess MONEY
	SELECT 
		@TotalSettle = ISNULL(SUM(tdvTotal.ValueMoney),0)
		,@TotalExcess = ISNULL(SUM(-tdvExcess.ValueMoney),0)
	FROM TableRows r WITH (NOLOCK)
	LEFT JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) on tdvTotal.TableRowID = r.TableRowID and tdvTotal.DetailFieldID = 144352
	LEFT JOIN dbo.TableDetailValues tdvExcess WITH (NOLOCK) on tdvExcess.TableRowID = r.TableRowID and tdvExcess.DetailFieldID = 146406
	WHERE r.MatterID = @MatterID
	AND r.DetailFieldID = 144355 -- Claim row
	
	EXEC _C00_SimpleValueIntoField 148375, @TotalSettle, @MatterID, NULL
	EXEC _C00_SimpleValueIntoField 170270, @TotalExcess, @MatterID, NULL

	-- 
	
	SELECT *
	FROM @Deductions


END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_PrepareSettlementDoc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Docs_PrepareSettlementDoc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_PrepareSettlementDoc] TO [sp_executeall]
GO
