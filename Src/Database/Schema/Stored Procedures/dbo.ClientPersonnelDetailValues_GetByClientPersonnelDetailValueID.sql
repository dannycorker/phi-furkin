SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelDetailValueID]
(

	@ClientPersonnelDetailValueID int   
)
AS


				SELECT
					[ClientPersonnelDetailValueID],
					[ClientPersonnelID],
					[ClientID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ClientPersonnelDetailValues] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelDetailValueID] = @ClientPersonnelDetailValueID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelDetailValueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelDetailValueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelDetailValues_GetByClientPersonnelDetailValueID] TO [sp_executeall]
GO
