SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Date Created: 16 October 2007
-- Created By:  Jim Green
-- Purpose: Select records from the ClientPersonnel with the Group Name appended
-- JWG 2012-08-10 Power-ups for Aquarium users
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetClientPersonnelWithGroupName]
(
	@ClientID int,
	@ShowAquariumUsers bit = 1
)
AS
BEGIN

	SELECT cp.ClientPersonnelID,
	cp.UserName,
	cpg.GroupName,
	cp.UserName + '  (' + cpg.GroupName + ')' as 'CombinedName'
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN ClientPersonnelAdminGroups cpg WITH (NOLOCK) ON cp.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
	WHERE cp.ClientID = @ClientID 
	AND (@ShowAquariumUsers = 1 OR cp.IsAquarium = 0)
	ORDER BY cp.UserName, cpg.GroupName		

END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonnelWithGroupName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientPersonnelWithGroupName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPersonnelWithGroupName] TO [sp_executeall]
GO
