SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2014-04-24
-- Description:	Move reports to the specified target folder.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MoveReport]
	@QueryID int,
	@FolderID int,
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	IF @QueryID <> '' and @FolderID <> '' 
	BEGIN

		UPDATE s
		SET IsDeleted = 0, FolderID = @FolderID
		FROM SqlQuery s 
		INNER JOIN Folder f WITH (NOLOCK) on f.FolderID = @FolderID
		WHERE QueryID = @QueryID
		and f.ClientID = @ClientID
		and s.ClientID = @ClientID

	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MoveReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MoveReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MoveReport] TO [sp_executeall]
GO
