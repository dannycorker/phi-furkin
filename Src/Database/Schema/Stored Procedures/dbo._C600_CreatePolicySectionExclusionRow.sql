SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-15
-- Description:	Add an exclusion row to a policy and update appropriate document fields
-- 2017-06-21 CPS Added Excluded From Date
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CreatePolicySectionExclusionRow]
(	
	 @PolicyMatterID		INT
	,@PolicySectionRLID		INT
	,@ExclusionReasonLuli	INT
	,@SourceID				INT = NULL
	,@WhoCreated			INT
	,@OverrideFromDate		DATE = NULL
)
AS
BEGIN
PRINT OBJECT_NAME(@@ProcID)

	SET NOCOUNT ON;

	DECLARE  @TableRowID			INT
			,@ClientID							INT
			,@DocumentDetailFieldID	INT
			,@VarcharDate					VARCHAR(10)
			
	SELECT @ClientID = m.ClientID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.MatterID = @PolicyMatterID
	
	SELECT @VarcharDate = CONVERT(VARCHAR,ISNULL(@OverrideFromDate,dbo.fn_GetDate_Local()),121)

	INSERT TableRows ( ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, SourceID )
	VALUES ( @ClientID, @PolicyMatterID, 177506, 19010, 1, 1, @SourceID ) /*Excluded Policy Sections*/

	SELECT @TableRowID = SCOPE_IDENTITY()

	EXEC dbo._C00_SimpleValueIntoField 177503, @PolicySectionRLID	,		@TableRowID, @WhoCreated /*Excluded Policy Section*/
	EXEC dbo._C00_SimpleValueIntoField 177504, @ExclusionReasonLuli	,	@TableRowID, @WhoCreated /*Excluded Policy Section Reason*/
	EXEC dbo._C00_SimpleValueIntoField 177513, @VarcharDate			,		 @TableRowID, @WhoCreated /*Excluded Policy Section From Date*/

	SELECT @DocumentDetailFieldID = dbo.fnGetSimpleDvAsInt(177467,@PolicySectionRLID) /*Policy Admin Document DetailFieldID*/
	EXEC dbo._C00_SimpleValueIntoField @DocumentDetailFieldID, 'Cover not Included', @PolicyMatterID, @WhoCreated

	RETURN @TableRowID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreatePolicySectionExclusionRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CreatePolicySectionExclusionRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CreatePolicySectionExclusionRow] TO [sp_executeall]
GO
