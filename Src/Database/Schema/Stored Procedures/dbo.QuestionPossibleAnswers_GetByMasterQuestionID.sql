SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionPossibleAnswers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_GetByMasterQuestionID]
(

	@MasterQuestionID int   
)
AS


				SELECT
					[QuestionPossibleAnswerID],
					[ClientQuestionnaireID],
					[MasterQuestionID],
					[AnswerText],
					[Branch],
					[LinkedQuestionnaireQuestionPossibleAnswerID],
					[ClientID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[QuestionPossibleAnswers] WITH (NOLOCK) 
				WHERE
										[MasterQuestionID] = @MasterQuestionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_GetByMasterQuestionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_GetByMasterQuestionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_GetByMasterQuestionID] TO [sp_executeall]
GO
