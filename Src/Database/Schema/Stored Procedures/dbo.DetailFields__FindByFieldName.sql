SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 22/10/2014
-- Description:	Find a detail field based upon is field name
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__FindByFieldName]
	@ClientID INT,
	@FieldName VARCHAR(50),
	@LeadTypeID INT,
	@DetailFieldSubTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM DetailFields WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=@LeadTypeID AND 
		  LeadOrMatter = @DetailFieldSubTypeID AND FieldName = @FieldName
		  
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindByFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__FindByFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindByFieldName] TO [sp_executeall]
GO
