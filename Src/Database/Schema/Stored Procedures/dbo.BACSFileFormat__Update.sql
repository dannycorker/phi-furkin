SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05/08/2016
-- Description:	Updates a BACSFileFormat
-- =============================================
CREATE PROCEDURE [dbo].[BACSFileFormat__Update]

	@BACSFileFormatID INT, 
	@ClientID INT, 
	@FormatName VARCHAR(250), 
	@FormatDescription VARCHAR(500), 
	@FileGenerationRoutine VARCHAR(250), 
	@WhoCreated INT, 
	@WhenCreated DATETIME,
	@WhoModified INT,
	@WhenModified DATETIME

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE BACSFileFormat
	SET FormatName = @FormatName, 
		FormatDescription = @FormatDescription, 
		FileGenerationRoutine = @FileGenerationRoutine, 
		WhoCreated = @WhoCreated, 
		WhenCreated = @WhenCreated, 
		WhoModified = @WhoModified, 
		WhenModified = @WhenModified
	WHERE BACSFileFormatID=@BACSFileFormatID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BACSFileFormat__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSFileFormat__Update] TO [sp_executeall]
GO
