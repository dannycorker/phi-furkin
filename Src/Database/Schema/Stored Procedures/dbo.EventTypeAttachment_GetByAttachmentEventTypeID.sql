SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAttachment table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_GetByAttachmentEventTypeID]
(

	@AttachmentEventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AttachmentID],
					[EventTypeID],
					[AttachmentEventTypeID],
					[All],
					[AttachmentEventGroupID]
				FROM
					[dbo].[EventTypeAttachment] WITH (NOLOCK) 
				WHERE
					[AttachmentEventTypeID] = @AttachmentEventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_GetByAttachmentEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_GetByAttachmentEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_GetByAttachmentEventTypeID] TO [sp_executeall]
GO
