SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 03/03/2020
-- Description:	Gets the country identity for the given customer
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetCountryId]
	@CustomerID INT
AS
BEGIN
		
	SET NOCOUNT ON;

   SELECT CountryID FROM  Customers WITH (NOLOCK) 
   WHERE CustomerID=@CustomerID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetCountryId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetCountryId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetCountryId] TO [sp_executeall]
GO
