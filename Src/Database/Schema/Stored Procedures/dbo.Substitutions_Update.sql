SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Substitutions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Substitutions_Update]
(

	@SubstitutionsID int   ,

	@SubstitutionsVariable varchar (50)  ,

	@SubstitutionsDisplayName varchar (50)  ,

	@SubstitutionsDescription varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Substitutions]
				SET
					[SubstitutionsVariable] = @SubstitutionsVariable
					,[SubstitutionsDisplayName] = @SubstitutionsDisplayName
					,[SubstitutionsDescription] = @SubstitutionsDescription
				WHERE
[SubstitutionsID] = @SubstitutionsID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Substitutions_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Update] TO [sp_executeall]
GO
