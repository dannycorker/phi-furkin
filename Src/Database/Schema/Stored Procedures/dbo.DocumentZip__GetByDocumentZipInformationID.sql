SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Author: Paul Richardson
-- Purpose: Gets a record(s) from DocumentZip table where the DocumentZipInformationID = @DocumentZipInformationID
-- Date: 13/02/2009
-- JWG 2009-05-20 Only select records with a status of 35 ("Pending Zip") so that
--                we can restart failed zips without creating all the good ones again.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[DocumentZip__GetByDocumentZipInformationID]
(
	@DocumentZipInformationID int   
)
AS
BEGIN

	SELECT
		[DocumentZipID],
		[DocumentZipInformationID],
		[ClientID],
		[DocumentQueueID],
		[StatusID], 
		[DocumentZipFileID] 
	FROM
		[dbo].[DocumentZip]
	WHERE
		[DocumentZipInformationID] = @DocumentZipInformationID
	AND [StatusID] = 35 /* JWG 2009-05-20 */
	
	/* DEBUG: JWG 2011-05-19 */
	--EXEC dbo._C00_LogIt 'Debug', 'DocumentZip__GetByDocumentZipInformationID', 'Get by Status 35', @DocumentZipInformationID, 93 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__GetByDocumentZipInformationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__GetByDocumentZipInformationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__GetByDocumentZipInformationID] TO [sp_executeall]
GO
