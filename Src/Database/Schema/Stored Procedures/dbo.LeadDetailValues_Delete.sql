SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDetailValues_Delete]
(

	@LeadDetailValueID int   
)
AS


				DELETE FROM [dbo].[LeadDetailValues] WITH (ROWLOCK) 
				WHERE
					[LeadDetailValueID] = @LeadDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues_Delete] TO [sp_executeall]
GO
