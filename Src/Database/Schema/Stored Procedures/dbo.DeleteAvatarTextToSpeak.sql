SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteAvatarTextToSpeak] @TextToSpeakID int

AS

Delete From TextToSpeak
Where TextToSpeakID = @TextToSpeakID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteAvatarTextToSpeak] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteAvatarTextToSpeak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteAvatarTextToSpeak] TO [sp_executeall]
GO
