SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderFileAbort table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderFileAbortID int   = null ,

	@ClientID int   = null ,

	@DateTimeFileAborted datetime   = null ,

	@ReasonForAbort varchar (1000)  = null ,

	@DataLoaderFileID int   = null ,

	@FileAbortedOn datetime   = null ,

	@LastRowImported int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderFileAbortID]
	, [ClientID]
	, [DateTimeFileAborted]
	, [ReasonForAbort]
	, [DataLoaderFileID]
	, [FileAbortedOn]
	, [LastRowImported]
    FROM
	[dbo].[DataLoaderFileAbort] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileAbortID] = @DataLoaderFileAbortID OR @DataLoaderFileAbortID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DateTimeFileAborted] = @DateTimeFileAborted OR @DateTimeFileAborted IS NULL)
	AND ([ReasonForAbort] = @ReasonForAbort OR @ReasonForAbort IS NULL)
	AND ([DataLoaderFileID] = @DataLoaderFileID OR @DataLoaderFileID IS NULL)
	AND ([FileAbortedOn] = @FileAbortedOn OR @FileAbortedOn IS NULL)
	AND ([LastRowImported] = @LastRowImported OR @LastRowImported IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderFileAbortID]
	, [ClientID]
	, [DateTimeFileAborted]
	, [ReasonForAbort]
	, [DataLoaderFileID]
	, [FileAbortedOn]
	, [LastRowImported]
    FROM
	[dbo].[DataLoaderFileAbort] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileAbortID] = @DataLoaderFileAbortID AND @DataLoaderFileAbortID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DateTimeFileAborted] = @DateTimeFileAborted AND @DateTimeFileAborted is not null)
	OR ([ReasonForAbort] = @ReasonForAbort AND @ReasonForAbort is not null)
	OR ([DataLoaderFileID] = @DataLoaderFileID AND @DataLoaderFileID is not null)
	OR ([FileAbortedOn] = @FileAbortedOn AND @FileAbortedOn is not null)
	OR ([LastRowImported] = @LastRowImported AND @LastRowImported is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Find] TO [sp_executeall]
GO
