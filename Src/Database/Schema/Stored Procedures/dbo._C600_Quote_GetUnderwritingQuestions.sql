SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-10-23
-- Description:	Gets the underwriting questions
-- 2016-05-23 IS Copied from 235
-- 2016-10-25 JL Made Language ID nullable and updated to draw all detail from RL
-- 2017-06-21 CPS added "BlockQuoteIfYes" and "MessageIfYes"
-- 2020-03-30 NG Changed resource list definitions
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Quote_GetUnderwritingQuestions] 
(
	@LanguageID INT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	/*
		TODO:
		these fields do not translated corectly to the internal Q&B
		
	177124	Condition
	177125	Endorsement T and C Paragraph
	177278	Illness Category
	177279	Ailment RL entry

	*/
	
	SELECT 
		r.ResourceListID AS QuestionID
		,REPLACE(ISNULL(rdvText.DetailValue,''), '|cr|','<br />') AS QuestionText 
		,rldvft.ValueInt AS [YesShowsFreeText] /*this now shows 'which answer shows free text': yes = 193305, no = 193306, neither = 193307, both = 193311*/
		,ISNULL(rdvBlockQuoteIfYes.ValueInt,0) [YesBlocksQuote] /*this now shows 'which answer blocks quote': yes = 193310, no = 193309, neither = 193308*/
		,ISNULL(rdvMessageIfYes.DetailValue,'') [MessageIfYes] /*this is the message to show is the answer is yes*/
	FROM 
		dbo.ResourceList r WITH (NOLOCK)
		INNER JOIN dbo.ResourceListDetailValues rdvText WITH (NOLOCK) ON r.ResourceListID = rdvText.ResourceListID AND rdvText.DetailFieldID = 177125 /*Endorsement TandC Paragraph*/
		INNER JOIN dbo.ResourceListDetailValues rldvft WITH (NOLOCK) on rldvft.ResourceListID = rdvText.ResourceListID and rldvft.DetailFieldID = 177304 /*YesShowsFreeText*/
		LEFT JOIN ResourceListDetailValues rdvBlockQuoteIfYes WITH ( NOLOCK ) on rdvBlockQuoteIfYes.ResourceListID = r.ResourceListID AND rdvBlockQuoteIfYes.DetailFieldID = 177490 /*Yes Blocks Quote*/
		LEFT JOIN ResourceListDetailValues rdvMessageIfYes WITH ( NOLOCK ) on rdvMessageIfYes.ResourceListID = r.ResourceListID AND rdvMessageIfYes.DetailFieldID = 177491 /*Message If Yes*/
	ORDER BY 
		r.ResourceListID


END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Quote_GetUnderwritingQuestions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Quote_GetUnderwritingQuestions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Quote_GetUnderwritingQuestions] TO [sp_executeall]
GO
