SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-07-01
-- Description:	Gets a list of RecentlyViewedLeads for the current User
-- =============================================
CREATE PROCEDURE [dbo].[LeadViewHistory__GetByClientPersonnelIDForDashboard] 
(
	@ClientPersonnelID int,
	@LeadTypeID int = null,
	@AquariumStatusID int = null
)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		lvh.LeadViewHistoryID, 
		lvh.LeadID,
		c.CustomerID, 
		c.Fullname,
		lt.LeadTypeName,
		lvh.WhenViewed,
		aqs.AquariumStatusName,
		c.IsBusiness
	FROM LeadViewHistory lvh WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = lvh.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
	INNER JOIN AquariumStatus aqs WITH (NOLOCK) ON aqs.AquariumStatusID = l.AquariumStatusID
	WHERE lvh.ClientPersonnelID = @ClientPersonnelID
	AND ((@LeadTypeID IS NULL) OR (l.LeadTypeID = @LeadTypeID))
	AND ((@AquariumStatusID IS NULL) OR (l.AquariumStatusID = @AquariumStatusID))
	ORDER BY lvh.LeadViewHistoryID DESC
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory__GetByClientPersonnelIDForDashboard] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory__GetByClientPersonnelIDForDashboard] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory__GetByClientPersonnelIDForDashboard] TO [sp_executeall]
GO
