SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadTypePageTitleTargetControl table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleTargetControl_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadTypePageTitleTargetControlID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@LastParsed datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadTypePageTitleTargetControlID]
	, [ClientID]
	, [LeadTypeID]
	, [LastParsed]
    FROM
	[dbo].[LeadTypePageTitleTargetControl] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypePageTitleTargetControlID] = @LeadTypePageTitleTargetControlID OR @LeadTypePageTitleTargetControlID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([LastParsed] = @LastParsed OR @LastParsed IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadTypePageTitleTargetControlID]
	, [ClientID]
	, [LeadTypeID]
	, [LastParsed]
    FROM
	[dbo].[LeadTypePageTitleTargetControl] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypePageTitleTargetControlID] = @LeadTypePageTitleTargetControlID AND @LeadTypePageTitleTargetControlID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([LastParsed] = @LastParsed AND @LastParsed is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleTargetControl_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleTargetControl_Find] TO [sp_executeall]
GO
