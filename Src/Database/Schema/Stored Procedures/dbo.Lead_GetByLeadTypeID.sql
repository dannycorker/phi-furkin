SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Lead table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadID],
					[ClientID],
					[LeadRef],
					[CustomerID],
					[LeadTypeID],
					[AquariumStatusID],
					[ClientStatusID],
					[BrandNew],
					[Assigned],
					[AssignedTo],
					[AssignedBy],
					[AssignedDate],
					[RecalculateEquations],
					[Password],
					[Salt],
					[WhenCreated]
				FROM
					[dbo].[Lead] WITH (NOLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByLeadTypeID] TO [sp_executeall]
GO
