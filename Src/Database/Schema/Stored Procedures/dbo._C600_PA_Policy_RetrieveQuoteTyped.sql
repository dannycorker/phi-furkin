SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================		
-- Author:	Ian Slack		
-- Create date: 2014-10-24		
-- Description:	Retrieve Quote typed	
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Policy_RetrieveQuoteTyped]		
(				
	@Email VARCHAR(255),		
	@SecretQuestionID VARCHAR(50),		
	@SecretQuestionValue VARCHAR(250),		
	@SaveQuoteKey VARCHAR(250),
	@QuoteSessionId INT = 0,
	@AggregatorID VARCHAR(50) = NULL	
)		
AS		
BEGIN		
	
	DECLARE @SavedQuoteXML XML
	
	IF @QuoteSessionId > 0
	BEGIN
	
		SELECT	TOP 1 
				@SavedQuoteXML = SavedQuoteXML, 
				@SaveQuoteKey = SavedQuoteKey,
				@QuoteSessionId = QuoteSessionID
		FROM	_C600_SavedQuote WITH (NOLOCK) 
		WHERE	QuoteSessionID = @QuoteSessionId
		ORDER BY SavedQuoteID DESC
		
	END
	ELSE
	BEGIN
	
		SELECT	TOP 1 
				@SavedQuoteXML = SavedQuoteXML, 
				@SaveQuoteKey = SavedQuoteKey,
				@QuoteSessionId = QuoteSessionID
		FROM	_C600_SavedQuote WITH (NOLOCK) 
		WHERE	(
					SavedQuoteKey = NULLIF(@SaveQuoteKey , '')
					OR 
					(
						EmailAddress = @Email 
						AND 
						SecretQuestionID = @SecretQuestionID 
						AND 
						SecretQuestionValue = @SecretQuestionValue
					)
				)
		AND		WhenExpire > dbo.fn_GetDate_Local()
		ORDER BY SavedQuoteID DESC
		
	END
	
	UPDATE _C600_SavedQuote
		SET RetrievedDate = dbo.fn_GetDate_Local()
	WHERE SavedQuoteKey = @SaveQuoteKey
	
	SELECT  @SavedQuoteXML SavedQuoteXML,
			@SaveQuoteKey SaveQuoteKey		
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_RetrieveQuoteTyped] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_RetrieveQuoteTyped] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_RetrieveQuoteTyped] TO [sp_executeall]
GO
