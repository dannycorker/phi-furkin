SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-10-21
-- Description:	Populate payments card transaction fields for a Policy Admin or Collections matter
-- 2019-12-09 CPS for JIRA LPC-181 | Allow passing the CustomerPaymentScheduleID
-- 2019-12-18 CPS for JIRA LPC-181 | Allow passing the LeadEventID so we can write comments
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__PrepareThirdPartyFields]
	 @MatterID					INT
	,@RunAsUserID				INT
	,@Description				VARCHAR(2000)
	,@Amount					VARCHAR(2000)
	,@FirstName					VARCHAR(2000)
	,@LastName					VARCHAR(2000)
	,@Address					VARCHAR(2000)
	,@Address2					VARCHAR(2000)
	,@City						VARCHAR(2000)
	,@StateProvince				VARCHAR(2000)
	,@Country					VARCHAR(2000)
	,@ZipPostal					VARCHAR(2000)
	,@CustomerPaymentScheduleID INT = NULL
	,@LeadEventID				INT = NULL
AS
BEGIN
	PRINT OBJECT_NAME(@@ProcID)

	SET NOCOUNT ON;

	DECLARE  @ClientID			INT
			,@LeadID			INT
			,@LeadTypeID		INT
			,@RowCount			INT
			,@ErrorMessage		VARCHAR(2000)
			,@EventComments		VARCHAR(2000) = ''
			
	DECLARE  @DetailValueData	dbo.tvpDetailValueData
			,@BlackHole			dbo.tvpIntInt	-- Used to collect (and ignore) the output of DetailValues__BulkSave
			,@ThirdPartyFields	dbo.tvpInt
	
	SELECT   @ClientID		= m.ClientID
			,@LeadID		= m.LeadID
			,@LeadTypeID	= l.LeadTypeID
	FROM Matter m WITH ( NOLOCK )
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
	WHERE m.MatterID = @MatterID
	
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  No Matter matched for ID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END

	/*Find the relevant ThirdPartyFields*/
	INSERT @ThirdPartyFields ( AnyID )
	SELECT pf.ThirdPartyFieldID
	FROM ThirdPartyField pf WITH ( NOLOCK )
	WHERE pf.ThirdPartyFieldID IN (4459,4460,4462,4463,4472,4473,4474,4475,4476,4477,4453)

	/*Compile a tvp of detail value data to save.  If the incoming variable is NULL, use the existing value*/
	INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
	SELECT @ClientID, fm.DetailFieldID, 2, ISNULL(mdv.MatterDetailValueID,-1), @MatterID,
		CASE fm.ThirdPartyFieldID 
			WHEN 4459 THEN ISNULL(@Description								 ,'')
			WHEN 4460 THEN ISNULL(@Amount									 ,'')
			WHEN 4462 THEN ISNULL(@FirstName								 ,'')
			WHEN 4463 THEN ISNULL(@LastName									 ,'')
			WHEN 4472 THEN ISNULL(@Address									 ,'')
			WHEN 4473 THEN ISNULL(@Address2									 ,'')
			WHEN 4474 THEN ISNULL(@City										 ,'')
			WHEN 4475 THEN ISNULL(@StateProvince							 ,'')
			WHEN 4476 THEN ISNULL(@Country									 ,'')
			WHEN 4477 THEN ISNULL(@ZipPostal								 ,'')
			WHEN 4453 THEN ISNULL(CAST(@CustomerPaymentScheduleID as VARCHAR),'')
			ELSE ''
		END 
	FROM ThirdPartyFieldMapping fm WITH ( NOLOCK )
	INNER JOIN @ThirdPartyFields tpf on tpf.AnyID = fm.ThirdPartyFieldID
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.LeadTypeID = @LeadTypeID		
	/*!! NO SQL HERE !!*/
	SELECT @RowCount = @@ROWCOUNT
	
	/*Save the detial values, storing history*/
	IF @RowCount > 0
	BEGIN
		SELECT @EventComments += CONVERT(VARCHAR,@RowCount) + ' CardTransaction fields updated for MatterID ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL') + CHAR(13)+CHAR(10)
		INSERT @BlackHole ( ID1, ID2 ) -- Capture the output from BulkSave and do nothing with it, just to prevent this SP from returning unnecessary data
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID
	END

	IF @LeadEventID <> 0 AND @EventComments <> ''
	BEGIN
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END

	RETURN @RowCount

END





GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__PrepareThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__PrepareThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__PrepareThirdPartyFields] TO [sp_executeall]
GO
