SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeFieldCompletion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_GetByDetailFieldID]
(

	@DetailFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeFieldCompletionID],
					[ClientID],
					[EventTypeID],
					[DetailFieldID],
					[InsertOptionID],
					[DeleteOptionID]
				FROM
					[dbo].[EventTypeFieldCompletion] WITH (NOLOCK) 
				WHERE
					[DetailFieldID] = @DetailFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_GetByDetailFieldID] TO [sp_executeall]
GO
