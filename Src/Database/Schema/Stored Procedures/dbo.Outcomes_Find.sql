SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Outcomes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Outcomes_Find]
(

	@SearchUsingOR bit   = null ,

	@OutcomeID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@OutcomeName varchar (50)  = null ,

	@OutcomeDescription varchar (MAX)  = null ,

	@SmsWhenFound bit   = null ,

	@MobileNumber varchar (50)  = null ,

	@EmailCustomer bit   = null ,

	@CustomersEmail varchar (MAX)  = null ,

	@ThankYouPage varchar (MAX)  = null ,

	@EmailFromAddress varchar (255)  = null ,

	@SmsToCustomer varchar (160)  = null ,

	@CustomersEmailSubject varchar (500)  = null ,

	@ClientID int   = null ,

	@EmailClient bit   = null ,

	@ClientEmail varchar (255)  = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OutcomeID]
	, [ClientQuestionnaireID]
	, [OutcomeName]
	, [OutcomeDescription]
	, [SmsWhenFound]
	, [mobileNumber]
	, [EmailCustomer]
	, [CustomersEmail]
	, [ThankYouPage]
	, [EmailFromAddress]
	, [SmsToCustomer]
	, [CustomersEmailSubject]
	, [ClientID]
	, [EmailClient]
	, [ClientEmail]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[Outcomes] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeID] = @OutcomeID OR @OutcomeID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([OutcomeName] = @OutcomeName OR @OutcomeName IS NULL)
	AND ([OutcomeDescription] = @OutcomeDescription OR @OutcomeDescription IS NULL)
	AND ([SmsWhenFound] = @SmsWhenFound OR @SmsWhenFound IS NULL)
	AND ([mobileNumber] = @MobileNumber OR @MobileNumber IS NULL)
	AND ([EmailCustomer] = @EmailCustomer OR @EmailCustomer IS NULL)
	AND ([CustomersEmail] = @CustomersEmail OR @CustomersEmail IS NULL)
	AND ([ThankYouPage] = @ThankYouPage OR @ThankYouPage IS NULL)
	AND ([EmailFromAddress] = @EmailFromAddress OR @EmailFromAddress IS NULL)
	AND ([SmsToCustomer] = @SmsToCustomer OR @SmsToCustomer IS NULL)
	AND ([CustomersEmailSubject] = @CustomersEmailSubject OR @CustomersEmailSubject IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EmailClient] = @EmailClient OR @EmailClient IS NULL)
	AND ([ClientEmail] = @ClientEmail OR @ClientEmail IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OutcomeID]
	, [ClientQuestionnaireID]
	, [OutcomeName]
	, [OutcomeDescription]
	, [SmsWhenFound]
	, [mobileNumber]
	, [EmailCustomer]
	, [CustomersEmail]
	, [ThankYouPage]
	, [EmailFromAddress]
	, [SmsToCustomer]
	, [CustomersEmailSubject]
	, [ClientID]
	, [EmailClient]
	, [ClientEmail]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[Outcomes] WITH (NOLOCK) 
    WHERE 
	 ([OutcomeID] = @OutcomeID AND @OutcomeID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([OutcomeName] = @OutcomeName AND @OutcomeName is not null)
	OR ([OutcomeDescription] = @OutcomeDescription AND @OutcomeDescription is not null)
	OR ([SmsWhenFound] = @SmsWhenFound AND @SmsWhenFound is not null)
	OR ([mobileNumber] = @MobileNumber AND @MobileNumber is not null)
	OR ([EmailCustomer] = @EmailCustomer AND @EmailCustomer is not null)
	OR ([CustomersEmail] = @CustomersEmail AND @CustomersEmail is not null)
	OR ([ThankYouPage] = @ThankYouPage AND @ThankYouPage is not null)
	OR ([EmailFromAddress] = @EmailFromAddress AND @EmailFromAddress is not null)
	OR ([SmsToCustomer] = @SmsToCustomer AND @SmsToCustomer is not null)
	OR ([CustomersEmailSubject] = @CustomersEmailSubject AND @CustomersEmailSubject is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EmailClient] = @EmailClient AND @EmailClient is not null)
	OR ([ClientEmail] = @ClientEmail AND @ClientEmail is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Outcomes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Find] TO [sp_executeall]
GO
