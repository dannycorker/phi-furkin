SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the UserFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing_Get_List]

AS


				
				SELECT
					[UserFunctionControlEditingID],
					[ClientPersonnelID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[UserFunctionControlEditing] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Get_List] TO [sp_executeall]
GO
