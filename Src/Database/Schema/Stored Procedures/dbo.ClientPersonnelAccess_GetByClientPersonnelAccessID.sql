SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelAccess table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_GetByClientPersonnelAccessID]
(

	@ClientPersonnelAccessID int   
)
AS


				SELECT
					[ClientPersonnelAccessID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[LeadID],
					[CaseID],
					[AccessRuleID],
					[SourceLeadEventID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[AccessLevel]
				FROM
					[dbo].[ClientPersonnelAccess] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelAccessID] = @ClientPersonnelAccessID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_GetByClientPersonnelAccessID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_GetByClientPersonnelAccessID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_GetByClientPersonnelAccessID] TO [sp_executeall]
GO
