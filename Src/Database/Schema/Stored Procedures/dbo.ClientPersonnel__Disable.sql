SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-03-20
-- Description:	Someone has left, so disable their ClientPersonnel account. 
-- This gets called on every Aquarius database on every sql instance (use the ForEachDB proc in AquariusMaster).
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__Disable] 
	@FirstName SYSNAME = 'test', 
	@LastName SYSNAME = 'test', 
	@Domain SYSNAME = 'aquarium-software.com', 
	@PrintOnly BIT = 1 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EmailAddress SYSNAME = @FirstName + '.' + @LastName + '%@' + @Domain
	
	SELECT * 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.EmailAddress LIKE @EmailAddress 
	AND ISNULL(cp.AccountDisabled, 0) = 0 ; 
	
	IF @PrintOnly = 1
	BEGIN
		SELECT 'UPDATE dbo.ClientPersonnel SET AttemptedLogins = 999, AccountDisabled = 1, [Password] = ''mash'', Salt = ''mash'' WHERE EmailAddress LIKE ''' + @EmailAddress + ''' AND ISNULL(AccountDisabled, 0) = 0 ; ' 
		SELECT 'DELETE dbo.ActiveSession WHERE EmailAddress LIKE ''' + @EmailAddress + '''; ' 
		
	END
	ELSE
	BEGIN
		UPDATE dbo.ClientPersonnel 
		SET AttemptedLogins = 999, AccountDisabled = 1, [Password] = 'mash', Salt = 'mash' 
		WHERE EmailAddress LIKE @EmailAddress 
		AND ISNULL(AccountDisabled, 0) = 0 
		
		DELETE dbo.ActiveSession 
		WHERE EmailAddress LIKE @EmailAddress 
		
	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__Disable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__Disable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__Disable] TO [sp_executeall]
GO
