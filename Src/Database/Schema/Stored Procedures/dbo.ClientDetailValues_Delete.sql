SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientDetailValues_Delete]
(

	@ClientDetailValueID int   
)
AS


				DELETE FROM [dbo].[ClientDetailValues] WITH (ROWLOCK) 
				WHERE
					[ClientDetailValueID] = @ClientDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_Delete] TO [sp_executeall]
GO
