SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Matter table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Matter__FindNoLock]
(

	@SearchUsingOR bit   = null ,

	@MatterID int   = null ,

	@ClientID int   = null ,

	@MatterRef varchar (100)  = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@MatterStatus tinyint   = null ,

	@RefLetter varchar (3)  = null ,

	@BrandNew bit   = null ,

	@CaseID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MatterID]
	, [ClientID]
	, [MatterRef]
	, [CustomerID]
	, [LeadID]
	, [MatterStatus]
	, [RefLetter]
	, [BrandNew]
	, [CaseID]
    FROM
	[dbo].[Matter] with (nolock)
    WHERE 
	 ([MatterID] = @MatterID OR @MatterID is null)
	AND ([ClientID] = @ClientID OR @ClientID is null)
	AND ([MatterRef] = @MatterRef OR @MatterRef is null)
	AND ([CustomerID] = @CustomerID OR @CustomerID is null)
	AND ([LeadID] = @LeadID OR @LeadID is null)
	AND ([MatterStatus] = @MatterStatus OR @MatterStatus is null)
	AND ([RefLetter] = @RefLetter OR @RefLetter is null)
	AND ([BrandNew] = @BrandNew OR @BrandNew is null)
	AND ([CaseID] = @CaseID OR @CaseID is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MatterID]
	, [ClientID]
	, [MatterRef]
	, [CustomerID]
	, [LeadID]
	, [MatterStatus]
	, [RefLetter]
	, [BrandNew]
	, [CaseID]
    FROM
	[dbo].[Matter] with (nolock)
    WHERE 
	 ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([MatterRef] = @MatterRef AND @MatterRef is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([MatterStatus] = @MatterStatus AND @MatterStatus is not null)
	OR ([RefLetter] = @RefLetter AND @RefLetter is not null)
	OR ([BrandNew] = @BrandNew AND @BrandNew is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	Select @@ROWCOUNT			
  END
				





GO
GRANT VIEW DEFINITION ON  [dbo].[Matter__FindNoLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Matter__FindNoLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Matter__FindNoLock] TO [sp_executeall]
GO
