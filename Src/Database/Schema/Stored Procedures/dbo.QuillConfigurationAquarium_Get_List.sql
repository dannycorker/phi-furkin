SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuillConfigurationAquarium table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationAquarium_Get_List]

AS


				
				SELECT
					[QuillConfigurationAquariumID],
					[Vendor],
					[APIKey]
				FROM
					[dbo].[QuillConfigurationAquarium] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationAquarium_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Get_List] TO [sp_executeall]
GO
