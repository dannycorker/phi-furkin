SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2012
-- Description:	Updates an existing UploadedFileAttachment record
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFileAttachment__Update]
	
	@UploadedFileAttachmentID INT,
	@UploadedFileID INT,
	@EventTypeID INT

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE UploadedFileAttachment
	SET
		UploadedFileID = @UploadedFileID, 
		EventTypeID = @EventTypeID
	WHERE 
		UploadedFileAttachmentID = @UploadedFileAttachmentID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFileAttachment__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFileAttachment__Update] TO [sp_executeall]
GO
