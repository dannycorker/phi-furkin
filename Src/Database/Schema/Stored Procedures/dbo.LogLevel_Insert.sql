SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LogLevel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LogLevel_Insert]
(

	@LogLevelID int   ,

	@LogLevelName varchar (250)  ,

	@LogLevelDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[LogLevel]
					(
					[LogLevelID]
					,[LogLevelName]
					,[LogLevelDescription]
					)
				VALUES
					(
					@LogLevelID
					,@LogLevelName
					,@LogLevelDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogLevel_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Insert] TO [sp_executeall]
GO
