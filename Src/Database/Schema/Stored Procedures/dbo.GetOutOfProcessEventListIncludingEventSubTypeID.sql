SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Paul Richardson
-- Create date: 2011-05-31
-- Description:	Get Out-of-process events list
-- Modified:	2014-09-18	SB	Handled shared lead types
-- =============================================
CREATE PROCEDURE [dbo].[GetOutOfProcessEventListIncludingEventSubTypeID] 
	@LeadTypeID int, 	
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID 
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

	SELECT DISTINCT EventTypeID, EventTypeName, EventSubtypeID
	FROM dbo.fnEventTypeShared(@ClientID) et
	INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON et.EventTypeID = fnEventTypeSecure.objectid
	WHERE LeadTypeID = @LeadTypeID 
	AND et.InProcess = 0
	AND et.Enabled = 1
	AND et.AvailableManually = 1
	AND et.EventSubtypeID NOT IN (9) /* Don't include Hold/Unhold events in this list */
	ORDER BY EventTypeName

END









GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutOfProcessEventListIncludingEventSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutOfProcessEventListIncludingEventSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutOfProcessEventListIncludingEventSubTypeID] TO [sp_executeall]
GO
