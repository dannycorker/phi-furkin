SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/06/2018
-- Description:	Gets a list of thirdparty events for the given lead type and third party system
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartySystemEvent__GetByLeadTypeIDAndThirdPartySystem]
	@ThirdPartySystemID INT,
	@ClientID INT,
	@LeadTypeID INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	tpe.ThirdPartySystemEventID, 
			tpe.ThirdPartySystemID, 
			tpe.ClientID, 
			tpe.Name, 
			tpe.Description, 
			tpe.EventTypeID,
			et.EventTypeName, 
			tpe.LeadTypeID,
			lt.LeadTypeName,
			tpe.ThirdPartySystemKey
	FROM ThirdPartySystemEvent tpe WITH (NOLOCK) 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID=tpe.EventTypeID
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = tpe.LeadTypeID
	WHERE tpe.ThirdPartySystemID=@ThirdPartySystemID AND tpe.ClientID=@ClientID AND tpe.LeadTypeID=@LeadTypeID
	ORDER BY et.EventTypeName ASC

END;
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByLeadTypeIDAndThirdPartySystem] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent__GetByLeadTypeIDAndThirdPartySystem] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetByLeadTypeIDAndThirdPartySystem] TO [sp_executeall]
GO
