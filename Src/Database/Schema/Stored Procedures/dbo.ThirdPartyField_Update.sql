SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_Update]
(

	@ThirdPartyFieldID int   ,

	@ThirdPartySystemId int   ,

	@FieldName varchar (250)  ,

	@FieldDescription varchar (250)  ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyField]
				SET
					[ThirdPartySystemId] = @ThirdPartySystemId
					,[FieldName] = @FieldName
					,[FieldDescription] = @FieldDescription
					,[IsEnabled] = @IsEnabled
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[ThirdPartyFieldID] = @ThirdPartyFieldID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Update] TO [sp_executeall]
GO
