SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get all the base contact methods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_ContactMethod]
(
	@ClientId INT,
	@ClientPersonnelId INT
)
AS
BEGIN

	SELECT LookupListItemID [Value], ItemValue [Text] 
	FROM	LookupListItems WITH (NOLOCK) 
	WHERE	LookupListID = 3843 -- IS: this lookup list did not fit with the QaB API 4434

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_ContactMethod] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_ContactMethod] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_ContactMethod] TO [sp_executeall]
GO
