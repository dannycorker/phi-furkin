SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailValueHistory table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_GetByClientIDDetailFieldIDWhenSaved]
(

	@ClientID int   ,

	@DetailFieldID int   ,

	@WhenSaved datetime   
)
AS


				SELECT
					[DetailValueHistoryID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[LeadID],
					[MatterID],
					[FieldValue],
					[WhenSaved],
					[ClientPersonnelID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ContactID],
					[ClientPersonnelDetailValueID]
				FROM
					[dbo].[DetailValueHistory] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [DetailFieldID] = @DetailFieldID
					AND [WhenSaved] = @WhenSaved
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_GetByClientIDDetailFieldIDWhenSaved] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_GetByClientIDDetailFieldIDWhenSaved] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_GetByClientIDDetailFieldIDWhenSaved] TO [sp_executeall]
GO
