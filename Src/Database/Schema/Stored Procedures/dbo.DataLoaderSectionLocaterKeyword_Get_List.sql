SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderSectionLocaterKeyword table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_Get_List]

AS


				
				SELECT
					[DataLoaderSectionLocaterKeywordID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderMapSectionID],
					[Keyword],
					[DataLoaderKeywordMatchTypeID],
					[KeywordNotes],
					[RelativeRow],
					[AbsoluteCol],
					[IsForSectionStart]
				FROM
					[dbo].[DataLoaderSectionLocaterKeyword] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Get_List] TO [sp_executeall]
GO
