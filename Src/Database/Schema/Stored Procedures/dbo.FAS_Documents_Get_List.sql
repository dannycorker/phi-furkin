SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FAS_Documents table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_Documents_Get_List]

AS


				
				SELECT
					[FasDocumentID],
					[FasCheckerDataID],
					[DocumentBlob]
				FROM
					[dbo].[FAS_Documents] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_Documents_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_Documents_Get_List] TO [sp_executeall]
GO
