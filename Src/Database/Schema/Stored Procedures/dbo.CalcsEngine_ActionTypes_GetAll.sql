SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Returns all action types for the calculations
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_ActionTypes_GetAll]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.CalcsEngine_ActionTypes WITH (NOLOCK) 
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_ActionTypes_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_ActionTypes_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_ActionTypes_GetAll] TO [sp_executeall]
GO
