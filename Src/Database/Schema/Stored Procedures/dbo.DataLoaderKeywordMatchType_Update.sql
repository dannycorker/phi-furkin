SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderKeywordMatchType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderKeywordMatchType_Update]
(

	@DataLoaderKeywordMatchTypeID int   ,

	@MatchTypeName varchar (250)  ,

	@MatchTypeDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderKeywordMatchType]
				SET
					[MatchTypeName] = @MatchTypeName
					,[MatchTypeDescription] = @MatchTypeDescription
				WHERE
[DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderKeywordMatchType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Update] TO [sp_executeall]
GO
