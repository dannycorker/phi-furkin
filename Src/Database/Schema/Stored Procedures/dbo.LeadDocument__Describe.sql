SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-06-29
-- Description:	Helpful details about a LeadDocument
-- JWG 2012-07-12 Use vLeadDocumentList to cater for archived documents.
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocument__Describe] 
	@LeadDocumentID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ThisDocumentTypeID int

	-- Show all old values
	SELECT 'Lead Document' as [Lead Document Info], ld.* 
	FROM dbo.vLeadDocumentList ld WITH (NOLOCK) 
	WHERE ld.LeadDocumentID = @LeadDocumentID 

	SELECT @ThisDocumentTypeID = ld.DocumentTypeID 
	FROM dbo.LeadDocumentFS ld WITH (NOLOCK) 
	WHERE ld.LeadDocumentID = @LeadDocumentID 

	EXEC dbo.DocumentType__Describe @ThisDocumentTypeID 
	
	SELECT * 
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadDocumentID = @LeadDocumentID
	ORDER BY le.LeadEventID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument__Describe] TO [sp_executehelper]
GO
