SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUserGroupQuestionnaireAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_Update]
(

	@PortalUserGroupQuestionnaireAccessID int   ,

	@ClientID int   ,

	@PortalUserGroupID int   ,

	@ClientQuestionnaireID int   ,

	@HasAccess bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalUserGroupQuestionnaireAccess]
				SET
					[ClientID] = @ClientID
					,[PortalUserGroupID] = @PortalUserGroupID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[HasAccess] = @HasAccess
				WHERE
[PortalUserGroupQuestionnaireAccessID] = @PortalUserGroupQuestionnaireAccessID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Update] TO [sp_executeall]
GO
