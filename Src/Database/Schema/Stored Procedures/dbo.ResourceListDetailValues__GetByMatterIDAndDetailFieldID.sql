SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[ResourceListDetailValues__GetByMatterIDAndDetailFieldID] 
(
	@LeadID int,
	@MatterID int,
	@DetailFieldID int,
	@ResourceListDetailFieldID int,
	@ClientID int
)

AS

	SELECT rldv.*
	FROM [dbo].[MatterDetailValues] mdv WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = @ResourceListDetailFieldID
	WHERE mdv.[MatterID] = @MatterID
	AND mdv.[DetailFieldID] = @DetailFieldID
	AND mdv.[ClientID] = @ClientID


	Select @@ROWCOUNT





GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues__GetByMatterIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues__GetByMatterIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues__GetByMatterIDAndDetailFieldID] TO [sp_executeall]
GO
