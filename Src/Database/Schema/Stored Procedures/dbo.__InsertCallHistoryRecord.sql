SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-06-26
-- Description:	__InsertCallHistoryRecord for PR
-- =============================================
CREATE PROCEDURE [dbo].[__InsertCallHistoryRecord]
@ClientID INT, 
@LeadID INT,
@101Fieldvalue VARCHAR(2000) ='',
@102Fieldvalue VARCHAR(2000) ='',
@103Fieldvalue VARCHAR(2000) ='',
@104Fieldvalue VARCHAR(2000) ='',
@105Fieldvalue VARCHAR(2000) ='',
@106Fieldvalue VARCHAR(2000) ='',
@107Fieldvalue VARCHAR(2000) ='',
@108Fieldvalue VARCHAR(2000) ='',
@109Fieldvalue VARCHAR(2000) ='',
@110Fieldvalue VARCHAR(2000) ='',
@111Fieldvalue VARCHAR(2000) ='',
@113Fieldvalue VARCHAR(2000) ='',
@114Fieldvalue VARCHAR(2000) ='',
@169Fieldvalue VARCHAR(2000) =''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TableRowID INT,
			@DetailFieldPageID INT,
			@DetailFieldID INT,
			@101FieldID INT,
			@102FieldID INT,
			@103FieldID INT,
			@104FieldID INT,
			@105FieldID INT,
			@106FieldID INT,
			@107FieldID INT,
			@108FieldID INT,
			@109FieldID INT,
			@110FieldID INT,
			@111FieldID	INT,
			@113FieldID	INT,
			@114FieldID INT,
			@169FieldID INT,
			@LeadTypeID INT,
			@CustomerID INT,
			@LogEntry VARCHAR(200),
			@InDialler INT,
			@ValueInt INT
			
	SELECT @LeadTypeID = LeadTypeID, @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID			

	SELECT @101FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 101
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @102FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 102
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @103FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 103
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID	

	SELECT @104FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 104
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID	

	SELECT @105FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 105
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @106FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 106
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID	

	SELECT @107FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 107
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @108FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 108
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @109FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 109
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @110FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 110
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @111FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 111
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @113FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 113
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @114FieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 114
	AND t.ClientID = @ClientID 
	AND LeadTypeID = @LeadTypeID
	
	SELECT @169FieldID = ColumnFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 169
	AND t.ClientID = @ClientID 
	AND LeadTypeID = @LeadTypeID
	
	SELECT @DetailFieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 101
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID
	
	SELECT @LogEntry = ' @ClientID = ' + ISNULL(CONVERT(VARCHAR,@ClientID),'NULL')			+ CHAR(13) + CHAR(10)
					 + ' @LeadID = ' + ISNULL(CONVERT(VARCHAR,@LeadID),'NULL')				+ CHAR(13) + CHAR(10)
					 + ' @LeadTypeID = ' + ISNULL(CONVERT(VARCHAR,@LeadTypeID),'NULL')		+ CHAR(13) + CHAR(10)
					 + ' @DetailFieldID = ' + ISNULL(CONVERT(VARCHAR,@DetailFieldID),'NULL') + CHAR(13) + CHAR(10)
				 
	EXEC _C00_LogIt 'Info', '__InsertCallHistoryRecord', 'Test Log', @LogEntry, 26932 /*CS*/
	
	INSERT INTO TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
	VALUES (@ClientID, @LeadID, @DetailFieldID, @DetailFieldPageID, 1, 1)
	
	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, LeadID, TableRowID, DetailFieldID, DetailValue)
	VALUES  (@ClientID, @LeadID, @TableRowID, @101FieldID, @101Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @102FieldID, @102Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @103FieldID, @103Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @104FieldID, @104Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @105FieldID, @105Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @106FieldID, @106Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @107FieldID, @107Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @108FieldID, @108Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @109FieldID, @109Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @110FieldID, @110Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @111FieldID, @111Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @113FieldID, @113Fieldvalue),
			(@ClientID, @LeadID, @TableRowID, @169FieldID, @169Fieldvalue)
	
	IF @114Fieldvalue <> '' AND @114FieldID > 0 /*Sent to Ultra*/
	BEGIN
		
		EXEC dbo._C00_CreateDetailFields @114FieldID, @LeadID
		
		UPDATE LeadDetailValues 
		SET DetailValue = @114Fieldvalue 
		WHERE DetailFieldID = @114FieldID 
		AND LeadID = @LeadID

	END
	
	--Added By PR 29-05-2012 add backup record at the customer level for brunel franklin
	IF @ClientID = 3
	BEGIN 
	
	
		INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit)
		VALUES (@ClientID, @CustomerID, 152303, 17044, 1, 1)
	
		DECLARE @RowID INT
		SELECT @RowID = SCOPE_IDENTITY()
	
		INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
		VALUES  (@ClientID, @CustomerID, @RowID, 134595, @101Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134596, @102Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134597, @103Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134598, @104Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134599, @105Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134600, @106Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134601, @107Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134602, @108Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134603, @109Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134604, @110Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134605, @111Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 134617, @113Fieldvalue),
				(@ClientID, @CustomerID, @RowID, 139396, @169Fieldvalue)


		SELECT @InDialler = dbo.fn_C00_IsCustomerInDialler(@CustomerID)
		SELECT @LogEntry = '@CustomerID = ' + convert(varchar,@CustomerID) + ', @InDialler = ' + convert(varchar,@InDialler)
		EXEC _C00_LogIt 'Info', '__InsertCallHistoryRecord', 'Ready to check for C3', @LogEntry, 3089

		IF (@InDialler <> @ValueInt) OR (@ValueInt IS NULL) /*Need to update the customer box*/
		BEGIN

			IF @ValueInt IS NULL	/*Need to Insert*/
			BEGIN
							
				INSERT CustomerDetailValues (ClientID,CustomerID,DetailFieldID,DetailValue)
				VALUES (3,@CustomerID,152441, CASE @InDialler WHEN 1 THEN 'true' ELSE 'False' END ) /*Uploaded to Ultra*/
				
			END
			ELSE					/*Update*/
			BEGIN
						
				UPDATE CustomerDetailValues 
				SET DetailValue = CASE @InDialler WHEN 1 THEN 'true' ELSE 'False' END
				FROM CustomerDetailValues 
				WHERE DetailFieldID = 152441 /*Uploaded to Ultra*/
				AND CustomerID = @CustomerID
				AND ClientID = 3
			
			END
			
			Select @LogEntry = convert(varchar(10),dbo.fn_GetDate_Local(),121)
			INSERT DetailValueHistory (ClientID,DetailFieldID,LeadOrMatter,CustomerID,FieldValue,WhenSaved,ClientPersonnelID)
			VALUES	( 3,152441,10,@CustomerID,CASE @InDialler WHEN 1 THEN 'true' ELSE 'False' END ,@LogEntry,3089 ) /*Uploaded to Ultra*/ /*Automation*/

		END

	END

	RETURN @TableRowID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallHistoryRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__InsertCallHistoryRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallHistoryRecord] TO [sp_executeall]
GO
