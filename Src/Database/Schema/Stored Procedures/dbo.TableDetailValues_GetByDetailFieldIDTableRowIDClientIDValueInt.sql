SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TableDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableDetailValues_GetByDetailFieldIDTableRowIDClientIDValueInt]
(

	@DetailFieldID int   ,

	@TableRowID int   ,

	@ClientID int   ,

	@ValueInt int   
)
AS


				SELECT
					[TableDetailValueID],
					[TableRowID],
					[ResourceListID],
					[DetailFieldID],
					[DetailValue],
					[LeadID],
					[MatterID],
					[ClientID],
					[EncryptedValue],
					[ErrorMsg],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID]
				FROM
					[dbo].[TableDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [TableRowID] = @TableRowID
					AND [ClientID] = @ClientID
					AND [ValueInt] = @ValueInt
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_GetByDetailFieldIDTableRowIDClientIDValueInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_GetByDetailFieldIDTableRowIDClientIDValueInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_GetByDetailFieldIDTableRowIDClientIDValueInt] TO [sp_executeall]
GO
