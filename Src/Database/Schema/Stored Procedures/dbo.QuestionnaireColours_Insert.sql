SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuestionnaireColours table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireColours_Insert]
(

	@QuestionnaireColourID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@Page varchar (50)  ,

	@Header varchar (50)  ,

	@Intro varchar (50)  ,

	@Body varchar (50)  ,

	@Footer varchar (50)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[QuestionnaireColours]
					(
					[ClientQuestionnaireID]
					,[Page]
					,[Header]
					,[Intro]
					,[Body]
					,[Footer]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@Page
					,@Header
					,@Intro
					,@Body
					,@Footer
					,@ClientID
					)
				-- Get the identity value
				SET @QuestionnaireColourID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireColours_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireColours_Insert] TO [sp_executeall]
GO
