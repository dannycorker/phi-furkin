SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-28
-- Description:	Useful DBA commands
-- =============================================
CREATE PROCEDURE [dbo].[dba]
AS
BEGIN
	SET NOCOUNT ON;

	/*
		Hostname, Current instance name, Edition, Server type etc
	*/
	SELECT  SERVERPROPERTY('MachineName') AS HOST,
			SERVERPROPERTY('InstanceName') AS Instance,
			SERVERPROPERTY('Edition') AS Edition, /*shows 32 bit or 64 bit*/
			SERVERPROPERTY('ProductLevel') AS ProductLevel, /* RTM or SP1 etc*/
			CASE SERVERPROPERTY('IsClustered') WHEN 1 THEN 'CLUSTERED' ELSE 'STANDALONE' END AS ServerType,
			@@VERSION AS VersionNumber


	/*
		List all databases
	*/
	SELECT d.name, d.compatibility_level, d.recovery_model_desc, d.state_desc  
	FROM sys.databases d 


	/*
		Backups
	*/
	SELECT db.name, 
	CASE WHEN MAX(b.backup_finish_date) IS NULL THEN 'No Backup' ELSE CONVERT(VARCHAR(100), MAX(b.backup_finish_date), 120) END AS last_backup_finish_date
	FROM sys.databases db
	LEFT JOIN msdb.dbo.backupset b ON db.name = b.database_name AND b.type = 'D'
	/*WHERE db.database_id NOT IN (2) */
	GROUP BY db.name
	ORDER BY 2 DESC


	/*
		Backup locations
	*/
	SELECT DISTINCT physical_device_name FROM msdb.dbo.backupmediafamily
	WHERE physical_device_name LIKE '%bak'



	/*
		List data file locations
	*/
	SELECT DB_NAME(database_id) AS DatabaseName,name,type_desc,physical_name FROM sys.master_files


	/*
		Filegroup details
	*/
	EXEC master.dbo.sp_MSforeachdb @command1 = 'USE [?] SELECT * FROM sys.filegroups'


	/*
		Server level configuration
	*/
	SELECT * FROM sys.configurations ORDER BY NAME


	/*
		Security
	*/
	SELECT l.name, l.denylogin, l.isntname, l.isntgroup, l.isntuser
	FROM master.dbo.syslogins l
	WHERE l.sysadmin = 1 OR l.securityadmin = 1


	/*
		Traces
	*/
	DBCC TRACESTATUS(-1); /* Global for the server */

	DBCC TRACESTATUS(); /* Current connection only */


	/*
		Who is doing what
	*/
	EXEC sys.sp_who2


	/*
		Stored proc execution stats
	*/
	SELECT TOP 100 
	CASE WHEN dm.database_id = 32767 then 'Resource' ELSE DB_NAME(dm.database_id) END AS DBName, 
	OBJECT_SCHEMA_NAME(object_id, dm.database_id) AS [SCHEMA_NAME],   
	OBJECT_NAME(object_id, dm.database_id)AS [OBJECT_NAME],   
	dm.* 
	FROM sys.dm_exec_procedure_stats dm 
	WHERE dm.database_id = 7 /* Aquarius */
	ORDER BY 3

	/* TODO: 
			
		SELECT CASE WHEN database_id = 32767 then 'Resource' ELSE DB_NAME(database_id)END AS DBName

			  ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME]  

			  ,OBJECT_NAME(object_id,database_id)AS [OBJECT_NAME]

			  ,cached_time

			  ,last_execution_time

			  ,execution_count

			  ,total_worker_time / execution_count AS AVG_CPU

			  ,total_elapsed_time / execution_count AS AVG_ELAPSED

			  ,total_logical_reads / execution_count AS AVG_LOGICAL_READS

			  ,total_logical_writes / execution_count AS AVG_LOGICAL_WRITES

			  ,total_physical_reads  / execution_count AS AVG_PHYSICAL_READS

		FROM sys.dm_exec_procedure_stats  

		ORDER BY AVG_LOGICAL_READS DESC



		SELECT CASE WHEN dbid = 32767 then 'Resource' ELSE DB_NAME(dbid)END AS DBName

			  ,OBJECT_SCHEMA_NAME(objectid,dbid) AS [SCHEMA_NAME]  

			  ,OBJECT_NAME(objectid,dbid)AS [OBJECT_NAME]

			  ,MAX(qs.creation_time) AS 'cache_time'

			  ,MAX(last_execution_time) AS 'last_execution_time'

			  ,MAX(usecounts) AS [execution_count]

			  ,SUM(total_worker_time) / SUM(usecounts) AS AVG_CPU

			  ,SUM(total_elapsed_time) / SUM(usecounts) AS AVG_ELAPSED

			  ,SUM(total_logical_reads) / SUM(usecounts) AS AVG_LOGICAL_READS

			  ,SUM(total_logical_writes) / SUM(usecounts) AS AVG_LOGICAL_WRITES

			  ,SUM(total_physical_reads) / SUM(usecounts)AS AVG_PHYSICAL_READS        

		FROM sys.dm_exec_query_stats qs  

		   join sys.dm_exec_cached_plans cp on qs.plan_handle = cp.plan_handle 

		 CROSS APPLY sys.dm_exec_sql_text(cp.plan_handle) 

		WHERE objtype = 'Proc' 

		  AND text

			   NOT LIKE '%CREATE FUNC%' 

			   GROUP BY cp.plan_handle,DBID,objectid 

	*/
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[dba] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dba] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dba] TO [sp_executeall]
GO
