SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-21
-- Description:	Get Third Party User Details For Editing Grid
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential__GetGridForEditing]
	@ClientID INT,
	@ThirdPartySystemID INT,
	@ClientPersonnelID INT,
	@AccountDisabled INT = 0,
	@ShowAquarium INT = 0
AS
BEGIN

	SET NOCOUNT ON;

	SELECT cp.ClientPersonnelID, cp.UserName, cp.EmailAddress, tpu.Domain, tpu.UserName AS [ThirdPartyUserName], tpu.AttemptedLogins, tpu.AccountDisabled, tpu.ThirdPartyMappingUserCredentialID
	FROM ThirdPartyMappingUserCredential tpu WITH (NOLOCK)
	INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = tpu.UserID
	WHERE tpu.ClientID = @ClientID
	AND tpu.ThirdPartySystemID = @ThirdPartySystemID
	AND ((tpu.AccountDisabled = @AccountDisabled AND @AccountDisabled = 0) OR (@AccountDisabled = 1))
	AND (cp.IsAquarium = @ShowAquarium )

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetGridForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential__GetGridForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetGridForEditing] TO [sp_executeall]
GO
