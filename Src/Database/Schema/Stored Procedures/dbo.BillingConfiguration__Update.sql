SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 27-07-2016
-- Description:	Updates a billing configuration record
-- =============================================
CREATE PROCEDURE [dbo].[BillingConfiguration__Update]
	
	@BillingConfigurationID INT,
	@ClientID INT, 
	@RemainderUpFront BIT, 
	@RegularPaymentWait INT, 
	@OneOffAdjustmentWait INT

AS
BEGIN
		
	SET NOCOUNT ON;

	UPDATE BillingConfiguration
	SET RemainderUpFront = @RemainderUpFront,
		RegularPaymentWait = @RegularPaymentWait,
		OneOffAdjustmentWait = @OneOffAdjustmentWait
	WHERE BillingConfigurationID=@BillingConfigurationID AND ClientID=@ClientID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillingConfiguration__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillingConfiguration__Update] TO [sp_executeall]
GO
