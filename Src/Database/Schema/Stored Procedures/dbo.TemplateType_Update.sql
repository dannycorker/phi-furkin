SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the TemplateType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TemplateType_Update]
(

	@TemplateTypeID int   ,

	@TemplateTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[TemplateType]
				SET
					[TemplateTypeName] = @TemplateTypeName
				WHERE
[TemplateTypeID] = @TemplateTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TemplateType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Update] TO [sp_executeall]
GO
