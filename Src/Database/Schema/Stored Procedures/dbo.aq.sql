SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-01-05
-- Description:	Aquarium master proc to call all our other helpful procs
-- =============================================
CREATE PROCEDURE [dbo].[aq] 
	@MenuID int = null,
	@SubmenuID int = null,
	@ID1 int = null
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ProcName nvarchar(500), -- Important, must be nvarchar for use in sp_executesql later
	@IsExecutable bit, 
	@HasIntParam bit, 
	@Params nvarchar(500),			 -- Important, must be nvarchar for use in sp_executesql later
	@IntParamName nvarchar(500), 
	@NeedsIntParam bit 

    -- Default is to show the menu of options
	IF @MenuID IS NULL
	BEGIN
		
		SELECT 'Usage: Enter aq to see this menu' as [Information]
		UNION ALL 
		SELECT 'Usage: Enter a MenuID to list its Submenus'
		UNION ALL 
		SELECT 'Usage: Enter MenuID, SubmenuID, UID to exec that proc (not all procs exec from here)'
		UNION ALL 
		SELECT REPLICATE('-', 75)
		UNION ALL 
		SELECT 'MenuID 1 is for description procs. These tell you everything about a particular record or object'
		UNION ALL 
		SELECT '2 for helpful (safe) procs'
		UNION ALL 
		SELECT '3 for housekeeping procs'
		UNION ALL 
		SELECT '4 for other update/delete procs'
		UNION ALL 
		SELECT '9 for the DocumentType procs that need to be cut down after a new DAL release'

	END
	ELSE
	BEGIN
		
		IF @SubmenuID IS NULL
		BEGIN
			
			-- Tell the user that more info is required
			SELECT 'Here are the submenu options for menu' + convert(varchar, @MenuID) as [Information]

			-- Show helper list for this menu id
			SELECT * 
			FROM dbo.AquariumHelperObject aho (nolock)
			WHERE aho.MenuID = @MenuID 
			ORDER BY aho.SubmenuID 

		END
		ELSE		
		BEGIN
			
			-- We have all the instructions we need: fetch proc details and execute it if appropriate
			SELECT @ProcName = 'EXEC dbo.' + aho.ProcName, 
			@IsExecutable = aho.IsExecutable, 
			@HasIntParam = aho.HasIntParam, 
			@IntParamName = aho.IntParamName, 
			@NeedsIntParam = aho.NeedsIntParam 
			FROM dbo.AquariumHelperObject aho (nolock) 
			WHERE aho.MenuID = @MenuID 
			AND aho.SubmenuID = @SubmenuID 
			
			IF @@ROWCOUNT <> 1
			BEGIN
				
				-- Error: this proc not found
				SELECT 'This combination of MenuID and SubMenuID is not valid: ' + convert(varchar, @MenuID) + ',' + convert(varchar, @SubmenuID)
				
			END
			ELSE
			BEGIN
				
				IF @IsExecutable = 0
				BEGIN
					
					-- Error: this proc cannot be run from this helper proc (too dangerous or too many params required)
					SELECT @ProcName as [ProcName], 'This proc cannot be executed from here'
					
				END
				ELSE
				BEGIN
					
					IF @HasIntParam = 1 
					BEGIN
						IF @ID1 IS NULL
						BEGIN
							IF @NeedsIntParam = 1
							BEGIN
								-- Error: this proc needs an int ID to run
								SELECT @ProcName as [ProcName], 'This proc requires an integer parameter', @IntParamName as [IntParamName]
							END
							ELSE
							BEGIN
								-- Run the proc, which will use the default for its int param
								EXEC sp_executesql @ProcName 
							END
						END
						ELSE
						BEGIN
							
							-- Setup the params for this proc
							SELECT @Params = '@' + @IntParamName + ' int',
							@ProcName = @ProcName + ' @' + @IntParamName 
							
							-- Run the proc
							EXEC sp_executesql @ProcName, @Params, @ID1 
						END
					END
					ELSE
					BEGIN
						
						-- This proc runs without any parameters
						EXEC sp_executesql @ProcName 
						
					END

				END
				
			END

		END
			
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[aq] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[aq] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[aq] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[aq] TO [sp_executehelper]
GO
