SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-02-12
-- Description:	Populate LeadInfoCustomSQL table
-- =============================================
CREATE PROCEDURE [dbo].[LeadInfoCustomSQL_Insert]
	@SqlQueryID INT,
	@LeadTypeID INT,
	@ClientPersonnelAdminGroupID INT,
	@ViewOrder INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO LeadInfoCustomSQL (SQLQueryID, LeadTypeID, ClientPersonnelAdminGroupID, ViewOrder)
	VALUES (@SQLQueryID, @LeadTypeID, @ClientPersonnelAdminGroupID, @ViewOrder)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadInfoCustomSQL_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadInfoCustomSQL_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadInfoCustomSQL_Insert] TO [sp_executeall]
GO
