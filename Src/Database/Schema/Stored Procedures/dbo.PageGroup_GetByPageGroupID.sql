SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PageGroup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroup_GetByPageGroupID]
(

	@PageGroupID int   
)
AS


				SELECT
					[PageGroupID],
					[ClientID],
					[LeadTypeID],
					[PageGroupName],
					[PageGroupDescription],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[PageGroup] WITH (NOLOCK) 
				WHERE
										[PageGroupID] = @PageGroupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_GetByPageGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroup_GetByPageGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_GetByPageGroupID] TO [sp_executeall]
GO
