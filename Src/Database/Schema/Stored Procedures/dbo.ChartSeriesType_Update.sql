SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ChartSeriesType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesType_Update]
(

	@ChartSeriesTypeID int   ,

	@OriginalChartSeriesTypeID int   ,

	@ChartSeriesTypeName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ChartSeriesType]
				SET
					[ChartSeriesTypeID] = @ChartSeriesTypeID
					,[ChartSeriesTypeName] = @ChartSeriesTypeName
				WHERE
[ChartSeriesTypeID] = @OriginalChartSeriesTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Update] TO [sp_executeall]
GO
