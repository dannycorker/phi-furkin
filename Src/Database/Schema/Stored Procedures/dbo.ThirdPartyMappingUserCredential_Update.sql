SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyMappingUserCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential_Update]
(

	@ThirdPartyMappingUserCredentialID int   ,

	@ThirdPartySystemID int   ,

	@ClientID int   ,

	@UserID int   ,

	@UserName varchar (255)  ,

	@Password varchar (65)  ,

	@Salt varchar (50)  ,

	@Domain varchar (50)  ,

	@AttemptedLogins int   ,

	@AccountDisabled bit   ,

	@ThirdPartyClientID varchar (50)  ,

	@LDAPConfigID int   ,

	@ImpersonateThirdPartySystemID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyMappingUserCredential]
				SET
					[ThirdPartySystemID] = @ThirdPartySystemID
					,[ClientID] = @ClientID
					,[UserID] = @UserID
					,[UserName] = @UserName
					,[Password] = @Password
					,[Salt] = @Salt
					,[Domain] = @Domain
					,[AttemptedLogins] = @AttemptedLogins
					,[AccountDisabled] = @AccountDisabled
					,[ThirdPartyClientID] = @ThirdPartyClientID
					,[LDAPConfigID] = @LDAPConfigID
					,[ImpersonateThirdPartySystemID] = @ImpersonateThirdPartySystemID
				WHERE
[ThirdPartyMappingUserCredentialID] = @ThirdPartyMappingUserCredentialID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential_Update] TO [sp_executeall]
GO
