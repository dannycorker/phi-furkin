SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-08-09
-- Description:	TEST proc for claim calculation testing
-- =============================================
CREATE PROCEDURE [dbo].[__SB_Test_ClaimRows] 
(
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@Output XML = NULL OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClaimRows TABLE
	(
		ID INT IDENTITY,
		LeadID INT,
		CaseID INT,
		MatterID INT,
		CoverageAreaID INT,
		TreatmentDate DATE,
		ClaimAmount DECIMAL(18, 2),
		CoInsurance DECIMAL(18, 2),
		Deductible DECIMAL(18, 2),
		Total DECIMAL(18, 2),
		PaidDate DATE
	)
	INSERT @ClaimRows (LeadID, CaseID, MatterID, CoverageAreaID, TreatmentDate, ClaimAmount, CoInsurance, Deductible, Total, PaidDate) VALUES
	(123, 456, 789, 1, '2013-08-01', 146.23, 0, 0, 0, '2013-08-02'),
	(123, 456, 789, 1, '2013-08-01', 245.56, 0, 0, 0, NULL),
	(123, 456, 789, 2, '2013-08-07', 256.99, 0, 0, 0, NULL),
	(123, 456, 789, 3, '2013-08-08', 56.89, 0, 0, 0, NULL)

	SELECT @Output = 
	( 
		SELECT *
		FROM @ClaimRows
		FOR XML PATH ('Row'), ROOT ('Data')
	)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[__SB_Test_ClaimRows] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__SB_Test_ClaimRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__SB_Test_ClaimRows] TO [sp_executeall]
GO
