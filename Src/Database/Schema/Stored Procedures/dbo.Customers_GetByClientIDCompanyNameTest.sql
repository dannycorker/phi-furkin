SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Customers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_GetByClientIDCompanyNameTest]
(

	@ClientID int   ,

	@CompanyName varchar (100)  ,

	@Test bit   
)
AS


				SELECT
					[CustomerID],
					[ClientID],
					[TitleID],
					[IsBusiness],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[DayTimeTelephoneNumberVerifiedAndValid],
					[HomeTelephone],
					[HomeTelephoneVerifiedAndValid],
					[MobileTelephone],
					[MobileTelephoneVerifiedAndValid],
					[CompanyTelephone],
					[CompanyTelephoneVerifiedAndValid],
					[WorksTelephone],
					[WorksTelephoneVerifiedAndValid],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[Website],
					[HasDownloaded],
					[DownloadedOn],
					[AquariumStatusID],
					[ClientStatusID],
					[Test],
					[CompanyName],
					[Occupation],
					[Employer],
					[Fullname],
					[PhoneNumbersVerifiedOn],
					[DoNotEmail],
					[DoNotSellToThirdParty],
					[AgreedToTermsAndConditions],
					[DateOfBirth],
					[DefaultContactID],
					[DefaultOfficeID],
					[AddressVerified],
					[CountryID],
					[SubClientID],
					[CustomerRef],
					[WhoChanged],
					[WhenChanged],
					[ChangeSource],
					[EmailAddressVerifiedAndValid],
					[EmailAddressVerifiedOn],
					[Comments],
					[AllowSmsCommandProcessing],
					[LanguageID],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Customers] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [CompanyName] = @CompanyName
					AND [Test] = @Test
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByClientIDCompanyNameTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_GetByClientIDCompanyNameTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByClientIDCompanyNameTest] TO [sp_executeall]
GO
