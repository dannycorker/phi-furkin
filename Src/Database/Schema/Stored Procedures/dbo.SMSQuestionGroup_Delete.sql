SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SMSQuestionGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_Delete]
(

	@SMSQuestionGroupID int   
)
AS


				DELETE FROM [dbo].[SMSQuestionGroup] WITH (ROWLOCK) 
				WHERE
					[SMSQuestionGroupID] = @SMSQuestionGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Delete] TO [sp_executeall]
GO
