SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailFieldSubType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldSubType_Insert]
(

	@DetailFieldSubTypeID tinyint   ,

	@DetailFieldSubTypeName varchar (200)  ,

	@DetailFieldSubTypeDescription varchar (500)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[DetailFieldSubType]
					(
					[DetailFieldSubTypeID]
					,[DetailFieldSubTypeName]
					,[DetailFieldSubTypeDescription]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@DetailFieldSubTypeID
					,@DetailFieldSubTypeName
					,@DetailFieldSubTypeDescription
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldSubType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_Insert] TO [sp_executeall]
GO
