SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-03-30
-- Description:	Get the next document for uploading

-- Parameters:  @ClientPersonnelID
--              The user requesting the document
--              @EventTypeIDs
--              A set of event types to filter on.  If this is
--				empty, then no filtering is applied
-- JWG 2012-07-12 Use vLeadDocumentList to cater for archived documents. In theory these new ones should not be archived already though.
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail_GetNextDocument]
	@ClientPersonnelID INT,
	@EventTypeIDs      tvpInt READONLY
	
AS
BEGIN

	DECLARE @DocumentBatchDetailID	INT
	DECLARE @DocumentId				INT
	DECLARE @EventTypeId			INT
	DECLARE @LeadId					INT
	DECLARE @MatterId				INT
	DECLARE @DocumentFileName		VARCHAR(255)
	DECLARE @MetaData				XML
	DECLARE @DocumentBLOB			VARBINARY(MAX)
	DECLARE @LeadDocumentId			INT
	DECLARE @LockedBy				INT
	DECLARE @DBD_MatterID			INT
	DECLARE @VendorResourceListID	INT
	
	SELECT
		TOP 1
		@DocumentBatchDetailID = DBD.DocumentBatchDetailID,
		@DocumentId =  DBD.DocumentMetaData.value('/Document[1]/@DocumentId', 'int'),
		@EventTypeId = DBD.DocumentMetaData.value('/Document[1]/@Type', 'int'),
		@LeadId = DBD.DocumentMetaData.value('/Document[1]/@LeadId', 'int'),
		@MatterId = DBD.DocumentMetaData.value('/Document[1]/@MatterId', 'int'),
		@DocumentFileName = DBD.DocumentMetaData.value('/Document[1]/Files[1]/File[@Type != "99999"][1]/@ImageFileName', 'varchar(255)'),
		@MetaData = DBD.DocumentMetaData,
		@DocumentBLOB = LD.DocumentBLOB,
		@LeadDocumentId = LD.LeadDocumentID,
		@LockedBy = DBD.LockedBy,
		@DBD_MatterID = DBD.MatterID,
		@VendorResourceListID = DBD.VendorResourceListID		
		
	FROM
		DocumentBatchDetail DBD WITH (NOLOCK) 
		INNER JOIN dbo.vLeadDocumentList LD WITH (NOLOCK)
		ON DBD.LeadDocumentID = LD.LeadDocumentID
		
		INNER JOIN DocumentBatch DB WITH (NOLOCK)
		ON DBD.DocumentBatchID = DB.DocumentBatchID
		AND DB.Uploaded = 1
		
		INNER JOIN ClientPersonnel CP WITH (NOLOCK) 
		ON LD.ClientID = CP.ClientID
		AND CP.ClientPersonnelID = @ClientPersonnelID

	WHERE
		DBD.Uploaded = 0
		AND (DBD.LockedBy IS NULL OR DBD.LockedBy = @ClientPersonnelID)
		AND (
			(SELECT COUNT(*) FROM @EventTypeIDs) = 0
			OR DBD.EventTypeId IN (SELECT AnyID FROM @EventTypeIDs)
			)		
		
	ORDER BY
		DBD.DocumentBatchDetailID ASC

	-- If a document has been found and is not currently locked by the 
	-- user (@ClientPersonnelId) then apply a soft lock
	IF (@DocumentBatchDetailID IS NOT NULL)
		BEGIN
			IF (@LockedBy IS NULL)
				BEGIN
					UPDATE DocumentBatchDetail
						SET LockedBy = @ClientPersonnelID,
							LockDate = dbo.fn_GetDate_Local()
					WHERE
						@DocumentBatchDetailID = DocumentBatchDetailID
						AND LockedBy IS NULL
				END

			SELECT
				@DocumentBatchDetailID DocumentBatchDetailID,
				@DocumentId			DocumentID,
				@EventTypeId		EventTypeID,
				@LeadId				LeadID,
				@MatterId			MatterID,
				@DocumentFileName	DocumentFileName,
				@MetaData			MetaData,
				@DocumentBLOB		DocumentBLOB,
				@LeadDocumentId		LeadDocumentID												
				
		END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_GetNextDocument] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail_GetNextDocument] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_GetNextDocument] TO [sp_executeall]
GO
