SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the IncomingPostEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEvent_Delete]
(

	@IncomingPostEventID int   
)
AS


				DELETE FROM [dbo].[IncomingPostEvent] WITH (ROWLOCK) 
				WHERE
					[IncomingPostEventID] = @IncomingPostEventID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEvent_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEvent_Delete] TO [sp_executeall]
GO
