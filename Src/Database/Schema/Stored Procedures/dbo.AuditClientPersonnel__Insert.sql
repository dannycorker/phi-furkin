SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2017-09-14
-- Description:	Record inserts and updates to ClientPersonnel
-- =============================================
CREATE PROCEDURE [dbo].[AuditClientPersonnel__Insert] 
	@ClientPersonnelID INT,
	@ClientID INT,
	@WhoModified INT,
	@IsInsert BIT,
	@IsDisabled BIT,
	@NewClientPersonnelAdminGroupID INT, 
	@ChangeNotes VARCHAR(100)
 
AS
BEGIN
	SET NOCOUNT ON;
	
    INSERT INTO dbo.AuditClientPersonnel(ClientPersonnelID, ClientID, WhoModified, WhenModified, IsInsert, IsDisabled, NewClientPersonnelAdminGroupID, ChangeNotes)
	VALUES (@ClientPersonnelID, @ClientID, @WhoModified, CURRENT_TIMESTAMP, @IsInsert, @IsDisabled, @NewClientPersonnelAdminGroupID, @ChangeNotes)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditClientPersonnel__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AuditClientPersonnel__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditClientPersonnel__Insert] TO [sp_executeall]
GO
