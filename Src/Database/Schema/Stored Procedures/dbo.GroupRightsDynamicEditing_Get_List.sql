SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the GroupRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamicEditing_Get_List]

AS


				
				SELECT
					[GroupRightsDynamicEditingID],
					[ClientPersonnelAdminGroupID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[GroupRightsDynamicEditing] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamicEditing_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Get_List] TO [sp_executeall]
GO
