SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-24
-- Description:	Get SqlQuery Editing-in-progress mode
-- =============================================
CREATE PROCEDURE [dbo].[SqlQueryEditingGetInProgress]
	@SqlQueryID int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT SqlQueryEditingID, 
	WhoIsEditing, 
	EditStartedAt, 
	UserName 
	FROM SqlQueryEditing 
	INNER JOIN ClientPersonnel ON ClientPersonnel.ClientPersonnelID = SqlQueryEditing.WhoIsEditing 
	WHERE SqlQueryID = @SqlQueryID 
	
END








GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGetInProgress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingGetInProgress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGetInProgress] TO [sp_executeall]
GO
