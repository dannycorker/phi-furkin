SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupListItems table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[LookupListItemID],
					[LookupListID],
					[ItemValue],
					[ClientID],
					[Enabled],
					[SortOrder],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID],
					[IsShared]
				FROM
					dbo.fnLookupListItemsShared(NULL)
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetBySourceID] TO [sp_executeall]
GO
