SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By:  Chris Townsend
-- Created On:  27 May 2008
-- Purpose: Gets a list of the DetailPageIDs that are resource list pages
-- Used for comparison on the field chooser to minimise database hits
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetResourceListDetailPageIDsByLeadTypeID]
(
	@LeadTypeID int
)
AS

	SET ANSI_NULLS OFF
	
	SELECT
		[DetailFieldPageID]
	FROM
		[dbo].[DetailFieldPages]
	WHERE
		[LeadTypeID] = @LeadTypeID
	AND ResourceList = 1
	
	Select @@ROWCOUNT
	SET ANSI_NULLS ON







GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListDetailPageIDsByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetResourceListDetailPageIDsByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetResourceListDetailPageIDsByLeadTypeID] TO [sp_executeall]
GO
