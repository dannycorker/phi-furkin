SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_Queue_Error
-- =============================================
CREATE PROCEDURE [dbo].[API_Error]
	@QID UNIQUEIDENTIFIER,
	@ErrorMessage VARCHAR(250)
AS
BEGIN
	
	UPDATE	API_Queue
		SET ErrorMessage = @ErrorMessage
	WHERE	QueueID = @QID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Error] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Error] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Error] TO [sp_executeall]
GO
