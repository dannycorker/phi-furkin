SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuestionPossibleAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_Delete]
(

	@QuestionPossibleAnswerID int   
)
AS


				DELETE FROM [dbo].[QuestionPossibleAnswers] WITH (ROWLOCK) 
				WHERE
					[QuestionPossibleAnswerID] = @QuestionPossibleAnswerID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Delete] TO [sp_executeall]
GO
