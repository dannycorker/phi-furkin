SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-24
-- Description:	Gets the policy excesses for the selected policy sections for a set of grouped claims
-- Used by:		Calculation
-- Modified		ROH 2013-01-17 Added selection for pet age dependent excess rows
--				ROH 2013-01-17 Added processing for applying old excess terms (Sainsbury's)
--				ROH 2013-01-18 Added retreival of ExcessCeiling field
--              ROH 2013-01-29 Added second sort term to final selection of excess row so that e.g. £125/15% returned before £125.
--              ROH 2013-01-31 Waive voluntary excess for Minimum Contribution excess rows
--              ROH 2013-02-21 Handle policies with no pet type defined 
--				SB	2014-08-27 Refactored excess calc to can use in the summary
--				SB	2014-10-09 Refactored fn_C00_1272_Policy_GetPolicyExcess which now has different params
--				ROH 2014-10-17 Bug fix - prevent duplicate rows being returned where multiple claim rows for same policy section
--				SB	2015-12-17 Need to pass PolicyAge though to calcs
--				JL  2016-06-16 Change output to account for Excess at section not scheme
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetGroupedPolicyExcesses] 
(
	@MatterID INT,
	@CurrentPolicyMatterID INT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	IF @CurrentPolicyMatterID IS NULL
	BEGIN
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	END
	
	--SELECT e.Out_ResourceListID AS ResourceListID, e.Section, e.SubSection, e.Excess, e.ExcessPercentage, e.Postcode, e.Breed, e.PetAge, e.PetType, e.ExcessType, e.ExcessCeiling, e.VoluntaryExcess
	--FROM dbo.TableRows r WITH (NOLOCK) 
	--INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350 
	--INNER JOIN dbo.fn_C00_1272_Policy_GetPolicyExcess(@CurrentPolicyMatterID, @MatterID) e ON tdvRLID.ResourceListID = e.ResourceListID
	--WHERE r.MatterID = @MatterID
	
	SELECT Distinct	e.Out_ResourceListID AS ResourceListID, e.Section, e.SubSection, e.Excess, e.ExcessPercentage, e.Postcode, e.Breed, e.PetAge, 
			e.PetType, e.ExcessType, e.ExcessCeiling, e.VoluntaryExcess, e.PolicyAge, e.ExcessRule 
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcess(@CurrentPolicyMatterID, @MatterID) e 
	WHERE e.ResourceListID IN (
		SELECT tdvRLID.ResourceListID
		FROM TableDetailValues tdvRLID WITH (NOLOCK) 
		WHERE tdvRLID.MatterID = @MatterID and tdvRLID.DetailFieldID = 144350 /*Policy Section*/
		)
		
	/*CPS 2017-06-14 need to allow this subquery to handle subsections.  If my subsection isn't listed roll-up to the main section*/	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyExcesses] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyExcesses] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetGroupedPolicyExcesses] TO [sp_executeall]
GO
