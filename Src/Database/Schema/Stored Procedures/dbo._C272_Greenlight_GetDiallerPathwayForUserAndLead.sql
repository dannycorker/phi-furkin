SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- ALTER date: 2014-06-01
-- Description:	Decide which dialler integration pathway to use for the current user and lead
-- =============================================
CREATE PROCEDURE [dbo].[_C272_Greenlight_GetDiallerPathwayForUserAndLead]
	 @ClientPersonnelID	INT
	,@LeadID			INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE  @DialerPathway	VARCHAR(50)
			,@ClientID		INT

	SELECT @ClientID = cp.ClientID
	FROM AquariusMaster.dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID

	/*
	- If this user has a ClientPersonnelOption 25 with a value of "true" then use "Greenlight"
	- If not, but the client has UseTapi set to 1 then use "ATAPI"
	- If not, use "None"
	
	We don't need LeadID yet, but we pass it into this proc in anticipation of future client requirements.
	*/

	SELECT @DialerPathway = 
		CASE  
			WHEN 'true' = ( SELECT po.OptionValue 
							FROM ClientPersonnelOptions po WITH (NOLOCK) 
							WHERE po.ClientPersonnelID = @ClientPersonnelID
							and po.ClientPersonnelOptionTypeID = 25
							AND po.OptionValue = 'true' ) 
				THEN 'Greenlight' 
			WHEN 1 = (	SELECT cl.UseTapi
						FROM Clients cl WITH (NOLOCK) 
						WHERE cl.ClientID = @ClientID )
				THEN 'ATAPI'
			ELSE 'None'
		END 

	SELECT @DialerPathway [Output]
END








GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_GetDiallerPathwayForUserAndLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C272_Greenlight_GetDiallerPathwayForUserAndLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_GetDiallerPathwayForUserAndLead] TO [sp_executeall]
GO
