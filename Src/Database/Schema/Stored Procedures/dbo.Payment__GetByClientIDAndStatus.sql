SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-14
-- Description:	Get payments for allocation
-- =============================================
CREATE PROCEDURE [dbo].[Payment__GetByClientIDAndStatus]
	@ClientID INT,
	@PaymentStatusID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT p.*, c.CurrencyCode, pt.PaymentTypeName
	FROM Payment p WITH (NOLOCK)
	LEFT JOIN Currency c WITH (NOLOCK) ON c.CurrencyID = p.PaymentCurrency
	LEFT JOIN PaymentTypes pt WITH (NOLOCK) ON pt.PaymentTypeID = p.PaymentTypeID
	WHERE p.ClientID = @ClientID
	AND p.PaymentStatusID = @PaymentStatusID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__GetByClientIDAndStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Payment__GetByClientIDAndStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__GetByClientIDAndStatus] TO [sp_executeall]
GO
