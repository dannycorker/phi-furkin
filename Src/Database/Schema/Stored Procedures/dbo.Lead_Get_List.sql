SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Lead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_Get_List]

AS


				
				SELECT
					[LeadID],
					[ClientID],
					[LeadRef],
					[CustomerID],
					[LeadTypeID],
					[AquariumStatusID],
					[ClientStatusID],
					[BrandNew],
					[Assigned],
					[AssignedTo],
					[AssignedBy],
					[AssignedDate],
					[RecalculateEquations],
					[Password],
					[Salt],
					[WhenCreated]
				FROM
					[dbo].[Lead] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Get_List] TO [sp_executeall]
GO
