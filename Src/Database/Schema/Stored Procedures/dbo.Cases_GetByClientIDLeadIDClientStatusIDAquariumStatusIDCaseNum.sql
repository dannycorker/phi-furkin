SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Cases table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_GetByClientIDLeadIDClientStatusIDAquariumStatusIDCaseNum]
(

	@ClientID int   ,

	@LeadID int   ,

	@ClientStatusID int   ,

	@AquariumStatusID int   ,

	@CaseNum int   
)
AS


				SELECT
					[CaseID],
					[LeadID],
					[ClientID],
					[CaseNum],
					[CaseRef],
					[ClientStatusID],
					[AquariumStatusID],
					[DefaultContactID],
					[LatestLeadEventID],
					[LatestInProcessLeadEventID],
					[LatestOutOfProcessLeadEventID],
					[LatestNonNoteLeadEventID],
					[LatestNoteLeadEventID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ProcessStartLeadEventID]
				FROM
					[dbo].[Cases] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [LeadID] = @LeadID
					AND [ClientStatusID] = @ClientStatusID
					AND [AquariumStatusID] = @AquariumStatusID
					AND [CaseNum] = @CaseNum
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByClientIDLeadIDClientStatusIDAquariumStatusIDCaseNum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_GetByClientIDLeadIDClientStatusIDAquariumStatusIDCaseNum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByClientIDLeadIDClientStatusIDAquariumStatusIDCaseNum] TO [sp_executeall]
GO
