SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 09-11-2012
-- Description:	Deletes an event group
-- =============================================
CREATE PROCEDURE [dbo].[EventGroup__Delete]

	@EventGroupID INT

AS
BEGIN

	DELETE FROM EventGroup
	WHERE EventGroupID=@EventGroupID		

END





GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventGroup__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventGroup__Delete] TO [sp_executeall]
GO
