SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_Delete]
(

	@SqlQueryEditingID int   
)
AS


				DELETE FROM [dbo].[SqlQueryEditing] WITH (ROWLOCK) 
				WHERE
					[SqlQueryEditingID] = @SqlQueryEditingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Delete] TO [sp_executeall]
GO
