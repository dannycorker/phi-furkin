SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the UserGroupMenuOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuOption_Get_List]

AS


				
				SELECT
					[UserGroupMenuOptionID],
					[ClientPersonnelAdminGroupID],
					[ClientID],
					[MasterPageName],
					[PanelItemAction],
					[PanelItemName],
					[PanelItemCaption],
					[PanelItemIcon],
					[PanelItemURL],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[Comments],
					[ItemOrder],
					[ShowInFancyBox]
				FROM
					[dbo].[UserGroupMenuOption] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Get_List] TO [sp_executeall]
GO
