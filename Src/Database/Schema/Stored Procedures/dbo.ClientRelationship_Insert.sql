SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientRelationship table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientRelationship_Insert]
(

	@ClientRelationshipID int    OUTPUT,

	@ClientRelationshipName varchar (200)  ,

	@OutgoingClientID int   ,

	@ReceivingClientID int   ,

	@OutgoingLeadTypeID int   ,

	@IncomingLeadTypeID int   ,

	@OutgoingEventTypeID int   ,

	@IncomingEventTypeID int   ,

	@Enabled bit   ,

	@ClearLeadRefs bit   
)
AS


				
				INSERT INTO [dbo].[ClientRelationship]
					(
					[ClientRelationshipName]
					,[OutgoingClientID]
					,[ReceivingClientID]
					,[OutgoingLeadTypeID]
					,[IncomingLeadTypeID]
					,[OutgoingEventTypeID]
					,[IncomingEventTypeID]
					,[Enabled]
					,[ClearLeadRefs]
					)
				VALUES
					(
					@ClientRelationshipName
					,@OutgoingClientID
					,@ReceivingClientID
					,@OutgoingLeadTypeID
					,@IncomingLeadTypeID
					,@OutgoingEventTypeID
					,@IncomingEventTypeID
					,@Enabled
					,@ClearLeadRefs
					)
				-- Get the identity value
				SET @ClientRelationshipID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientRelationship_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientRelationship_Insert] TO [sp_executeall]
GO
