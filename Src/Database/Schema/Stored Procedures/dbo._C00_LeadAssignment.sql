SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-06
-- Description:	Assign a lead to a user
--				ACE - Added Unassigment (Assign =0) option 2010-01-28
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LeadAssignment] 
	@LeadID int, 
	@AssignedTo int, 
	@AssignedBy int,
	@Assign bit = 1 
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.Lead 
	SET Assigned = @Assign, 
	AssignedTo = CASE @Assign WHEN 1 THEN @AssignedTo ELSE NULL END, 
	AssignedBy = CASE @Assign WHEN 1 THEN @AssignedBy ELSE NULL END, 
	AssignedDate = CASE @Assign WHEN 1 THEN dbo.fn_GetDate_Local() ELSE NULL END
	WHERE LeadID = @LeadID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadAssignment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LeadAssignment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadAssignment] TO [sp_executeall]
GO
