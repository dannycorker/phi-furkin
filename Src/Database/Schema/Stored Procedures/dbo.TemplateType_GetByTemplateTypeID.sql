SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TemplateType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TemplateType_GetByTemplateTypeID]
(

	@TemplateTypeID int   
)
AS


				SELECT
					[TemplateTypeID],
					[TemplateTypeName]
				FROM
					[dbo].[TemplateType] WITH (NOLOCK) 
				WHERE
										[TemplateTypeID] = @TemplateTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_GetByTemplateTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TemplateType_GetByTemplateTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_GetByTemplateTypeID] TO [sp_executeall]
GO
