SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-11-30
-- Description:	Summarises a purchased product payment schedule
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PurchasedProductBillingSummary]
(
	@PurchasedProductID INT
)
AS
BEGIN

--declare @PurchasedProductID INT = 1719

	/*
	
		Summary definitions:
		
		1. Original unpaid. Sum of rows where status = new and ID = parent ID and ID not in group column and group ID is null.
		2. Individual retries. Sum of rows where status = new and ID <> parent ID and grouped ID is null.
		3. Grouped retries. Sum of rows where status = new and ID = parent ID and ID in group column and group ID is null.
		4. Failed not yet retried. Sum of rows where status=failed and group ID is null and no later new row where failed parent ID = later new row parent ID
		5. Paid. Sum of row where status IN (processed,paid) and not later row where parent ID = later row parent ID
		
		6. Total annual premium = 1 + 2 + 3 + 4 + 5
		7. Total overdue = 2 + 3 + 4
		8. Total due = 1 + 2 + 3 + 4

	*/
	
	DECLARE 
			@FailedNotYetRetried MONEY = 0,
			@FailedNotYetRetriedVAT MONEY = 0,
			@GroupedRetries MONEY = 0,
			@GroupedRetriesVAT MONEY = 0,
			@IndividualRetries MONEY = 0,
			@IndividualRetriesVAT MONEY = 0,
			@OriginalUnpaid MONEY = 0,
			@OriginalUnpaidVAT MONEY = 0,
			@Paid MONEY = 0,
			@PaidVAT MONEY = 0,
			@TotalAnnualPremium MONEY = 0,
			@TotalAnnualPremiumVAT MONEY = 0,
			@TotalDue MONEY = 0,
			@TotalDueVAT MONEY = 0,
			@TotalOverdue MONEY = 0,
			@TotalOverdueVAT MONEY = 0

	-- 1. Original unpaid
	SELECT @OriginalUnpaid=ISNULL(SUM(s.PaymentGross),0), @OriginalUnpaidVAT=ISNULL(SUM(s.PaymentVAT),0) 
	FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
	WHERE s.PurchasedProductID=@PurchasedProductID
		AND s.PaymentStatusID=1
		AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID
		AND s.PaymentGroupedIntoID IS NULL 
		AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule s2 WITH (NOLOCK) 
						 WHERE s.PurchasedProductID=s2.PurchasedProductID
						 AND s.PurchasedProductPaymentScheduleID=s2.PaymentGroupedIntoID )

	-- 2. Individual retries
	SELECT @IndividualRetries=ISNULL(SUM(s.PaymentGross),0), @IndividualRetriesVAT=ISNULL(SUM(s.PaymentVAT),0) 
	FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
	WHERE s.PurchasedProductID=@PurchasedProductID
		AND s.PaymentStatusID=1
		AND s.PurchasedProductPaymentScheduleID<>s.PurchasedProductPaymentScheduleParentID
		AND s.PaymentGroupedIntoID IS NULL 

	-- 3. Grouped retries
	SELECT @GroupedRetries=ISNULL(SUM(s.PaymentGross),0), @GroupedRetriesVAT=ISNULL(SUM(s.PaymentVAT),0) 
	FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
	WHERE s.PurchasedProductID=@PurchasedProductID
		AND s.PaymentStatusID=1
		AND s.PurchasedProductPaymentScheduleID=s.PurchasedProductPaymentScheduleParentID
		AND s.PaymentGroupedIntoID IS NULL 
		AND EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule s2 WITH (NOLOCK) 
						 WHERE s.PurchasedProductID=s2.PurchasedProductID
						 AND s.PurchasedProductPaymentScheduleID=s2.PaymentGroupedIntoID )

	-- 4. Failed not yet retried
	SELECT @FailedNotYetRetried=ISNULL(SUM(s.PaymentGross),0), @FailedNotYetRetriedVAT=ISNULL(SUM(s.PaymentVAT),0) 
	FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
	WHERE s.PurchasedProductID=@PurchasedProductID
		AND s.PaymentStatusID=4
		AND s.PaymentGroupedIntoID IS NULL 
		AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule s2 WITH (NOLOCK) 
						 WHERE s.PurchasedProductID=s2.PurchasedProductID
						 AND s.PurchasedProductPaymentScheduleParentID=s2.PurchasedProductPaymentScheduleParentID
						 AND s2.PurchasedProductPaymentScheduleID > s.PurchasedProductPaymentScheduleID )

	-- 5. Paid
	SELECT @Paid=ISNULL(SUM(s.PaymentGross),0), @PaidVAT=ISNULL(SUM(s.PaymentVAT),0) 
	FROM PurchasedProductPaymentSchedule s WITH (NOLOCK) 
	WHERE s.PurchasedProductID=@PurchasedProductID
		AND s.PaymentStatusID IN (2,6)
		AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule s2 WITH (NOLOCK) 
						 WHERE s.PurchasedProductID=s2.PurchasedProductID
						 AND s.PurchasedProductPaymentScheduleParentID=s2.PurchasedProductPaymentScheduleParentID
						 AND s2.PurchasedProductPaymentScheduleID > s.PurchasedProductPaymentScheduleID )
	
	-- summaries
	SELECT
		@TotalAnnualPremium = @OriginalUnpaid + @IndividualRetries + @GroupedRetries + @FailedNotYetRetried + @Paid,
		@TotalAnnualPremiumVAT = @OriginalUnpaidVAT + @IndividualRetriesVAT + @GroupedRetriesVAT + @FailedNotYetRetriedVAT + @PaidVAT,
		@TotalDue = @OriginalUnpaid + @IndividualRetries + @GroupedRetries + @FailedNotYetRetried,
		@TotalDueVAT = @OriginalUnpaidVAT + @IndividualRetriesVAT + @GroupedRetriesVAT + @FailedNotYetRetriedVAT,
		@TotalOverdue = @IndividualRetries + @GroupedRetries + @FailedNotYetRetried,
		@TotalOverdueVAT = @IndividualRetriesVAT + @GroupedRetriesVAT + @FailedNotYetRetriedVAT
		
	-- return the values
	SELECT 
			@FailedNotYetRetried [FailedNotYetRetried],
			@FailedNotYetRetriedVAT [FailedNotYetRetriedVAT],
			@GroupedRetries [GroupedRetries],
			@GroupedRetriesVAT [GroupedRetriesVAT],
			@IndividualRetries [IndividualRetries],
			@IndividualRetriesVAT [IndividualRetriesVAT],
			@OriginalUnpaid [OriginalUnpaid],
			@OriginalUnpaidVAT [OriginalUnpaidVAT],
			@Paid [Paid],
			@PaidVAT [PaidVAT],
			@TotalAnnualPremium [TotalAnnualPremium],
			@TotalAnnualPremiumVAT [TotalAnnualPremiumVAT],
			@TotalDue [TotalDue],
			@TotalDueVAT [TotalDueVAT],
			@TotalOverdue [TotalOverdue],
			@TotalOverdueVAT [TotalOverdueVAT]

	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PurchasedProductBillingSummary] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PurchasedProductBillingSummary] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PurchasedProductBillingSummary] TO [sp_executeall]
GO
