SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[EventType__GetByLeadTypeIDClientID]
(

	@LeadTypeID int   ,

	@ClientID int   
)
AS


				SELECT
					[EventTypeID],
					[ClientID],
					[EventTypeName],
					[EventTypeDescription],
					[Enabled],
					[UnitsOfEffort],
					[FollowupTimeUnitsID],
					[FollowupQuantity],
					[AvailableManually],
					[StatusAfterEvent],
					[AquariumEventAfterEvent],
					[EventSubtypeID],
					[DocumentTypeID],
					[LeadTypeID],
					[AllowCustomTimeUnits],
					[InProcess],
					[KeyEvent],
					[UseEventCosts],
					[UseEventUOEs],
					[UseEventDisbursements],
					[UseEventComments],
					[SignatureRequired],
					[SignatureOverride],
					[VisioX],
					[VisioY],
					[AquariumEventSubtypeID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[FollowupWorkingDaysOnly],
					[CalculateTableRows],
					[SourceID],
					[SmsGatewayID],
					[IsShared],
					[SocialFeedID]
				FROM
					dbo.fnEventTypeShared(@ClientID)
				WHERE
										[LeadTypeID] = @LeadTypeID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByLeadTypeIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByLeadTypeIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByLeadTypeIDClientID] TO [sp_executeall]
GO
