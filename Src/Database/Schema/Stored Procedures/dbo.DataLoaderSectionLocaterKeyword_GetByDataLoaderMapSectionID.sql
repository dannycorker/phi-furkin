SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderSectionLocaterKeyword table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapSectionID]
(

	@DataLoaderMapSectionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderSectionLocaterKeywordID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderMapSectionID],
					[Keyword],
					[DataLoaderKeywordMatchTypeID],
					[KeywordNotes],
					[RelativeRow],
					[AbsoluteCol],
					[IsForSectionStart]
				FROM
					[dbo].[DataLoaderSectionLocaterKeyword] WITH (NOLOCK) 
				WHERE
					[DataLoaderMapSectionID] = @DataLoaderMapSectionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_GetByDataLoaderMapSectionID] TO [sp_executeall]
GO
