SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:			Ian Slack
-- Create date:		2015-01-28
-- Description:		Get all the base renewal methods
-- =============================================
CREATE PROCEDURE [dbo].[_PA_Search_RenewalMethod]
(
       @ClientId INT,
       @ClientPersonnelId INT
)
AS
BEGIN

       SELECT LookupListItemID Value, ItemValue Text
       FROM   LookupListItems WITH (NOLOCK) 
       WHERE  LookupListID = 6130
       AND Enabled = 1 

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_PA_Search_RenewalMethod] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_PA_Search_RenewalMethod] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_PA_Search_RenewalMethod] TO [sp_executeall]
GO
