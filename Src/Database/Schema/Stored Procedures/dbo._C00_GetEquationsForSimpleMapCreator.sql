SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-16
-- Description:	Get Fields For Simple Uploader
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetEquationsForSimpleMapCreator]
	@ClientID INT,
	@FieldID VARCHAR(2000)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT CASE @FieldID 
		WHEN '1.1' THEN '' 
		WHEN '1.9' THEN 'INVOKEFUNCTION(FIX MOBILE GOES HERE!)' 
		WHEN '1.20' THEN '=0' 
		ELSE '' END AS [Equation]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetEquationsForSimpleMapCreator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetEquationsForSimpleMapCreator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetEquationsForSimpleMapCreator] TO [sp_executeall]
GO
