SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[UpdateFrameSourceForPage]

@QuestionnaireFrameSourceID int,
@ClientID int, 
@ClientQuestionnaireID int,
@PageNumber int,
@SourceUrl nvarchar(555),
@FrameType int

AS

Update QuestionnaireFrameSources
SET ClientID = @ClientID,
ClientQuestionnaireID = @ClientQuestionnaireID,
PageNumber = @PageNumber,
SourceUrl = @SourceUrl,
FrameType = @FrameType
where QuestionnaireFrameSourceID = @QuestionnaireFrameSourceID



GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateFrameSourceForPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateFrameSourceForPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateFrameSourceForPage] TO [sp_executeall]
GO
