SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-08-15
-- Description:	Gets bank holidays between the specified dates
-- =============================================
CREATE PROCEDURE [dbo].[WorkingDays__GetBankHolidays]
(
	@FromDate DATETIME,
	@ToDate DATETIME
)
AS
BEGIN

	SELECT Date
	FROM dbo.WorkingDays WITH (NOLOCK) 
	WHERE Date >= @FromDate
	AND Date <= @ToDate
	AND IsBankHoliday = 1
    
END





GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays__GetBankHolidays] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays__GetBankHolidays] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays__GetBankHolidays] TO [sp_executeall]
GO
