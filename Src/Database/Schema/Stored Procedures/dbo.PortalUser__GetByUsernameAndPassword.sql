SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUser table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUser__GetByUsernameAndPassword]
(
	@Username varchar(50),
	@Password varchar(65)
)
AS


				SELECT
					*
				FROM
					[dbo].[PortalUser]
				WHERE
					[Username] = @Username
					AND [Password] = @Password
			Select @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetByUsernameAndPassword] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser__GetByUsernameAndPassword] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser__GetByUsernameAndPassword] TO [sp_executeall]
GO
