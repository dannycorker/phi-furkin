SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 09/08/2016
-- Description:	Gets a list of all the transaction types
-- =============================================
CREATE PROCEDURE [dbo].[TransactionType__GetAll]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM TransactionType WITH (NOLOCK) 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[TransactionType__GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TransactionType__GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TransactionType__GetAll] TO [sp_executeall]
GO
