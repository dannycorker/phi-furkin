SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-01-2013
-- Description:	Gets the resource list fields in field order
-- =============================================
CREATE PROCEDURE [dbo].[ResourceList__GetFieldDefinitions]
	@FieldID INT
AS
BEGIN
	
	DECLARE @PageID INT
	
	SELECT @PageID=ResourceListDetailFieldPageID FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID=@FieldID 
	
	SELECT * FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldPageID=@PageID and Enabled=1 AND Hidden=0 order by FieldOrder asc

END



GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetFieldDefinitions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList__GetFieldDefinitions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetFieldDefinitions] TO [sp_executeall]
GO
