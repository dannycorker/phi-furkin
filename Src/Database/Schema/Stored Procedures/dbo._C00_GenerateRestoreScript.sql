SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim & Alex
-- Create date: 2008-12-10
-- Description:	Generate restore script
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GenerateRestoreScript]
@tablename sysname,
@restoredb varchar(250)= 'CopyOf_Aquarius_12_09_2008.dbo.'

/*
Commands needed to run SP
EXEC dbo._C00_GenerateRestoreScript Customers
--EXEC dbo._C00_GenerateRestoreScript Partner
EXEC dbo._C00_GenerateRestoreScript Lead
EXEC dbo._C00_GenerateRestoreScript Cases
EXEC dbo._C00_GenerateRestoreScript Matter
EXEC dbo._C00_GenerateRestoreScript LeadDetailValues
EXEC dbo._C00_GenerateRestoreScript MatterDetailValues
EXEC dbo._C00_GenerateRestoreScript DetailValueHistory
EXEC dbo._C00_GenerateRestoreScript LeadDocument
EXEC dbo._C00_GenerateRestoreScript LeadEvent
EXEC dbo._C00_GenerateRestoreScript LeadEventThreadCompletion
--EXEC dbo._C00_GenerateRestoreScript TableRows
--EXEC dbo._C00_GenerateRestoreScript TableDetailValues
--EXEC dbo._C00_GenerateRestoreScript PortalUser
--EXEC dbo._C00_GenerateRestoreScript PortalUserCase
--EXEC dbo._C00_GenerateRestoreScript WorkflowTask
--EXEC dbo._C00_GenerateRestoreScript WorkflowTaskCompleted

/* For Business Lead Types */
--EXEC dbo._C00_GenerateRestoreScript Department
--EXEC dbo._C00_GenerateRestoreScript CustomerOffice
--EXEC dbo._C00_GenerateRestoreScript Contact

/* eCatcher Stuff */
--EXEC dbo._C00_GenerateRestoreScript DroppedOutCustomers
--EXEC dbo._C00_GenerateRestoreScript DroppedOutCustomerQuestionnaires
--EXEC dbo._C00_GenerateRestoreScript DroppedOutCustomerAnswers
--EXEC dbo._C00_GenerateRestoreScript DroppedOutCustomerMessages
EXEC dbo._C00_GenerateRestoreScript DocumentQueue
EXEC dbo._C00_GenerateRestoreScript CustomerQuestionnaires
EXEC dbo._C00_GenerateRestoreScript CustomerAnswers
EXEC dbo._C00_GenerateRestoreScript CustomerOutcomes

/* Case Transfer */
--EXEC dbo._C00_GenerateRestoreScript CaseTransferMapping

*/

AS
BEGIN
DECLARE @bigstring varchar(max), 
@restorefrom varchar(250), 
@ColumnList varchar(max),
@CustomerWhereClause varchar(max),
@LeadWhereClause varchar(max),
@WhereClause varchar(max),
@TriggerOff varchar(max),
@TriggerOn varchar(max)

	SET NOCOUNT ON;

select @restorefrom = @restoredb + @TableName,
@ColumnList = '',  
@bigstring = 'insert into ' + @tablename + ' ('

IF @TableName = 'Customers'
BEGIN
	select @CustomerWhereClause = ' where EmailAddress like ''test@test%'' and clientid = 83 and customerid not in (618881,610740,614190) '
END
ELSE
BEGIN
	select @CustomerWhereClause = ' inner join ' + @restoredb + 'Customers rc on rc.CustomerID = ' + @restorefrom + '.CustomerID and rc.EmailAddress like ''test@test%'' and rc.clientid = 83 and rc.customerid not in (618881,610740,614190) '
END

select @LeadWhereClause = ' inner join ' + @restoredb + 'Lead rl on rl.LeadID = ' + @restorefrom + '.LeadID' 
+ ' inner join ' + @restoredb + 'Customers rc on rc.CustomerID = rl.CustomerID and rc.EmailAddress like ''test@test%'' and rc.clientid = 83 and rc.customerid not in (618881,610740,614190)'

select @ColumnList = @ColumnList + @tablename + '.' + ic.column_name + ','
from information_schema.columns ic 
where ic.table_name = @TableName 
and not (@TableName = 'Customers' and ic.column_name = 'FullName')
order by ic.ordinal_position

select @WhereClause = case when charindex('CustomerID', @ColumnList, 0) > 0 then @CustomerWhereClause else @LeadWhereClause end

select @ColumnList = left(@ColumnList, len(@ColumnList) - 1)


select @bigstring = @bigstring + @ColumnList + ')' + ' select ' + @ColumnList + ' from ' + @restorefrom + @WhereClause

If @tablename IN ('LeadEvent','Lead') 
BEGIN
	Select @TriggerOff = 'ALTER TABLE ' + @TableName + ' DISABLE TRIGGER trgiu_' + @TableName + ' ; '
, @TriggerOn = ' ALTER TABLE ' + @TableName + ' ENABLE TRIGGER trgiu_' + @TableName + ' ; '
END
ELSE
BEGIN
	Select @TriggerOff = '', @TriggerOn = ''
END

select 'BEGIN TRAN ; ' + @TriggerOff + ' SET IDENTITY_INSERT ' + @TableName + ' ON ; ' + @bigstring + ' ; SET IDENTITY_INSERT ' + @TableName + ' OFF ; ' + @TriggerOn +  ' ROLLBACK ; COMMIT ; '

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GenerateRestoreScript] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GenerateRestoreScript] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GenerateRestoreScript] TO [sp_executeall]
GO
