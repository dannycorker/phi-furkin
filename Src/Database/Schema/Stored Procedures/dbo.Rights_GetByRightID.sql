SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Rights table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Rights_GetByRightID]
(

	@RightID int   
)
AS


				SELECT
					[RightID],
					[RightName]
				FROM
					[dbo].[Rights] WITH (NOLOCK) 
				WHERE
										[RightID] = @RightID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_GetByRightID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Rights_GetByRightID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_GetByRightID] TO [sp_executeall]
GO
