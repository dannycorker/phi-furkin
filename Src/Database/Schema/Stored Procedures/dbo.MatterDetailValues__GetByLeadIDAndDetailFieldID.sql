SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[MatterDetailValues__GetByLeadIDAndDetailFieldID]
(
	@LeadID int,	
	@DetailFieldID int,
	@ClientID int
)
AS


				SELECT *
				FROM
					[dbo].[MatterDetailValues] WITH (NOLOCK) 
				WHERE					
					[LeadID] = @LeadID
					AND [DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
					
			Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByLeadIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues__GetByLeadIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByLeadIDAndDetailFieldID] TO [sp_executeall]
GO
