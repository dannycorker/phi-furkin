SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-08-21
-- Description:	Import Data For L&G
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ImportFromScriptRunner]
	@Column0 [varchar](50) NULL,
	@Column1 [varchar](50) NULL,
	@Column2 [varchar](50) NULL,
	@Column3 [varchar](50) NULL,
	@Column4 [varchar](50) NULL,
	@Column5 [varchar](50) NULL,
	@Column6 [varchar](50) NULL,
	@Column7 [varchar](50) NULL,
	@Column8 [varchar](50) NULL,
	@Column9 [varchar](50) NULL,
	@Column10 [varchar](50) NULL,
	@Column11 [varchar](50) NULL,
	@Column12 [varchar](50) NULL,
	@Column13 [varchar](50) NULL,
	@Column14 [varchar](50) NULL,
	@Column15 [varchar](50) NULL,
	@Column16 [varchar](50) NULL,
	@Column17 [varchar](50) NULL,
	@Column18 [varchar](50) NULL,
	@Column19 [varchar](50) NULL,
	@Column20 [varchar](50) NULL,
	@Column21 [varchar](50) NULL,
	@Column22 [varchar](50) NULL,
	@Column23 [varchar](50) NULL,
	@Column24 [varchar](50) NULL,
	@Column25 [varchar](50) NULL,
	@Column26 [varchar](50) NULL,
	@Column27 [varchar](50) NULL,
	@Column28 [varchar](50) NULL,
	@Column29 [varchar](50) NULL,
	@Column30 [varchar](50) NULL,
	@Column31 [varchar](50) NULL,
	@Column32 [varchar](50) NULL,
	@Column33 [varchar](50) NULL,
	@Column34 [varchar](50) NULL,
	@Column35 [varchar](50) NULL,
	@Column36 [varchar](8000) NULL,
	@Column37 [varchar](500) NULL,
	@Column38 [varchar](500) NULL,
	@Column39 [varchar](50) NULL,
	@Column40 [varchar](50) NULL,
	@Column41 [varchar](50) NULL,
	@Column42 [varchar](50) NULL,
	@Column43 [varchar](50) NULL,
	@Column44 [varchar](50) NULL,
	@Column45 [varchar](50) NULL,
	@Column46 [varchar](50) NULL,
	@Column47 [varchar](50) NULL,
	@Column48 [varchar](50) NULL,
	@Column49 [varchar](50) NULL,
	@Column50 [varchar](50) NULL,
	@Column51 [varchar](50) NULL,
	@Column52 [varchar](50) NULL,
	@Column53 [varchar](50) NULL,
	@Column54 [varchar](50) NULL,
	@Column55 [varchar](50) NULL,
	@Column56 [varchar](50) NULL,
	@Column57 [varchar](50) NULL,
	@Column58 [varchar](50) NULL,
	@Column59 [varchar](50) NULL,
	@Column60 [varchar](50) NULL,
	@Column61 [varchar](50) NULL,
	@Column62 [varchar](50) NULL,
	@Column63 [varchar](50) NULL,
	@Column64 [varchar](50) NULL,
	@Column65 [varchar](50) NULL,
	@Column66 [varchar](50) NULL,
	@Column67 [varchar](50) NULL,
	@Column68 [varchar](50) NULL,
	@Column69 [varchar](50) NULL,
	@Column70 [varchar](50) NULL,
	@Column71 [varchar](50) NULL,
	@Column72 [varchar](50) NULL,
	@Column73 [varchar](50) NULL,
	@Column74 [varchar](50) NULL

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [dbo].[_C600_UPP_FILE_CATCHUP] ([Column 0], [Column 1], [Column 2], [Column 3], [Column 4], [Column 5], [Column 6], [Column 7], [Column 8], [Column 9], [Column 10], [Column 11], [Column 12], [Column 13], [Column 14], [Column 15], [Column 16], [Column 17], [Column 18], [Column 19], [Column 20], [Column 21], [Column 22], [Column 23], [Column 24], [Column 25], [Column 26], [Column 27], [Column 28], [Column 29], [Column 30], [Column 31], [Column 32], [Column 33], [Column 34], [Column 35], [Column 36], [Column 37], [Column 38], [Column 39], [Column 40], [Column 41], [Column 42], [Column 43], [Column 44], [Column 45], [Column 46], [Column 47], [Column 48], [Column 49], [Column 50], [Column 51], [Column 52], [Column 53], [Column 54], [Column 55], [Column 56], [Column 57], [Column 58], [Column 59], [Column 60], [Column 61], [Column 62], [Column 63], [Column 64], [Column 65], [Column 66], [Column 67], [Column 68], [Column 69], [Column 70], [Column 71], [Column 72], [Column 73], [Column 74])
	VALUES (@Column0, @Column1, @Column2, @Column3, @Column4, @Column5, @Column6, @Column7, @Column8, @Column9, @Column10, @Column11, @Column12, @Column13, @Column14, @Column15, @Column16, @Column17, @Column18, @Column19, @Column20, @Column21, @Column22, @Column23, @Column24, @Column25, @Column26, @Column27, @Column28, @Column29, @Column30, @Column31, @Column32, @Column33, @Column34, @Column35, @Column36, @Column37, @Column38, @Column39, @Column40, @Column41, @Column42, @Column43, @Column44, @Column45, @Column46, @Column47, @Column48, @Column49,  @Column50, @Column51, @Column52,  @Column53, @Column54, @Column55, @Column56, @Column57, @Column58, @Column59, @Column60, @Column61, @Column62, @Column63, @Column64, @Column65, @Column66, @Column67, @Column68, @Column69, @Column70, @Column71, @Column72, @Column73, @Column74)

END			 
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportFromScriptRunner] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ImportFromScriptRunner] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ImportFromScriptRunner] TO [sp_executeall]
GO
