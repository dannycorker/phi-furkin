SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChartSeriesSetting table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesSetting_Insert]
(

	@ChartSeriesSettingID int    OUTPUT,

	@ChartSeriesID int   ,

	@SettingName varchar (100)  ,

	@SettingValue varchar (MAX)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[ChartSeriesSetting]
					(
					[ChartSeriesID]
					,[SettingName]
					,[SettingValue]
					,[ClientID]
					)
				VALUES
					(
					@ChartSeriesID
					,@SettingName
					,@SettingValue
					,@ClientID
					)
				-- Get the identity value
				SET @ChartSeriesSettingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesSetting_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesSetting_Insert] TO [sp_executeall]
GO
