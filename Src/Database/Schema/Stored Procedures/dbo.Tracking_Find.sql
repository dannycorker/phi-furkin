SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Tracking table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Tracking_Find]
(

	@SearchUsingOR bit   = null ,

	@TrackingID int   = null ,

	@TrackingName varchar (100)  = null ,

	@TrackingDescription varchar (255)  = null ,

	@StartDate datetime   = null ,

	@EndDate datetime   = null ,

	@ClientQuestionnaireID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TrackingID]
	, [TrackingName]
	, [TrackingDescription]
	, [StartDate]
	, [EndDate]
	, [ClientQuestionnaireID]
	, [ClientID]
    FROM
	[dbo].[Tracking] WITH (NOLOCK) 
    WHERE 
	 ([TrackingID] = @TrackingID OR @TrackingID IS NULL)
	AND ([TrackingName] = @TrackingName OR @TrackingName IS NULL)
	AND ([TrackingDescription] = @TrackingDescription OR @TrackingDescription IS NULL)
	AND ([StartDate] = @StartDate OR @StartDate IS NULL)
	AND ([EndDate] = @EndDate OR @EndDate IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TrackingID]
	, [TrackingName]
	, [TrackingDescription]
	, [StartDate]
	, [EndDate]
	, [ClientQuestionnaireID]
	, [ClientID]
    FROM
	[dbo].[Tracking] WITH (NOLOCK) 
    WHERE 
	 ([TrackingID] = @TrackingID AND @TrackingID is not null)
	OR ([TrackingName] = @TrackingName AND @TrackingName is not null)
	OR ([TrackingDescription] = @TrackingDescription AND @TrackingDescription is not null)
	OR ([StartDate] = @StartDate AND @StartDate is not null)
	OR ([EndDate] = @EndDate AND @EndDate is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Tracking_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Find] TO [sp_executeall]
GO
