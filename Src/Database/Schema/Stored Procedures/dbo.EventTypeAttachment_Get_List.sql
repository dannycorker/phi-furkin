SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventTypeAttachment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_Get_List]

AS


				
				SELECT
					[AttachmentID],
					[EventTypeID],
					[AttachmentEventTypeID],
					[All],
					[AttachmentEventGroupID]
				FROM
					[dbo].[EventTypeAttachment] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Get_List] TO [sp_executeall]
GO
