SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DashboardLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardLayout_Update]
(

	@DashboardLayoutID int   ,

	@DashboardLayoutName varchar (200)  ,

	@TopLeftDimensions char (3)  ,

	@TopMiddleDimensions char (3)  ,

	@TopRightDimensions char (3)  ,

	@BottomLeftDimensions char (3)  ,

	@BottomMiddleDimensions char (3)  ,

	@BottomRightDimensions char (3)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DashboardLayout]
				SET
					[DashboardLayoutName] = @DashboardLayoutName
					,[TopLeftDimensions] = @TopLeftDimensions
					,[TopMiddleDimensions] = @TopMiddleDimensions
					,[TopRightDimensions] = @TopRightDimensions
					,[BottomLeftDimensions] = @BottomLeftDimensions
					,[BottomMiddleDimensions] = @BottomMiddleDimensions
					,[BottomRightDimensions] = @BottomRightDimensions
				WHERE
[DashboardLayoutID] = @DashboardLayoutID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardLayout_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Update] TO [sp_executeall]
GO
