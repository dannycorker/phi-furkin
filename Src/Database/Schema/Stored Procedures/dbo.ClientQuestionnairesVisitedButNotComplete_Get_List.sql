SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientQuestionnairesVisitedButNotComplete table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedButNotComplete_Get_List]

AS


				
				SELECT
					[ClientQuestionnairesVisitedButNotCompleteID],
					[ClientQuestionnaireID],
					[VisitedDate],
					[IPAddress],
					[ClientID]
				FROM
					[dbo].[ClientQuestionnairesVisitedButNotComplete] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Get_List] TO [sp_executeall]
GO
