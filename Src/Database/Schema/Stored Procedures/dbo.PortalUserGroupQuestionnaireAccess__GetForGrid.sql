SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-06-21
-- Description:	Gets a list of PortalUserGroupQuestionnaireAccess records
-- =============================================
CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess__GetForGrid]
(
	@ClientID int	
)
	
AS

SET ANSI_NULLS OFF
				
				SELECT
					pugqa.[PortalUserGroupQuestionnaireAccessID],
					pugqa.[ClientID],
					pugqa.[PortalUserGroupID],
					pugqa.[ClientQuestionnaireID],
					cq.[QuestionnaireTitle],
					pugqa.[HasAccess]
				FROM
					[dbo].[PortalUserGroupQuestionnaireAccess] pugqa
				INNER JOIN
					[dbo].[ClientQuestionnaires] cq ON cq.ClientQuestionnaireID = pugqa.ClientQuestionnaireID
					
				WHERE
					pugqa.[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess__GetForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess__GetForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess__GetForGrid] TO [sp_executeall]
GO
