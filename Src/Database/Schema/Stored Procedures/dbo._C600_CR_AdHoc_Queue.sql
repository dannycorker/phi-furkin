SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-10-04
-- Description:	Queue an item for ad-hoc config release
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CR_AdHoc_Queue]
	  @LeadID				INT
	 ,@WhoCreated			INT
	 ,@ObjectID				INT
	 ,@Notes				VARCHAR(2000) = ''
	 ,@AquariumObjectTypeID	INT /*Aq 1,{whatever}.  So DetailFieldID is 2, DocumentTypeID is 6, etc*/
AS
BEGIN

	DECLARE  @UserName						VARCHAR(2000) = ''
			,@EmailAddress					VARCHAR(2000) = ''
			,@ObjectTypeLookupListItemID	INT
			,@TableRowID					INT
			,@VarcharDate					VARCHAR(10)	  = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121)
			,@ObjectName					VARCHAR(2000) 

	SELECT @UserName = cp.UserName, @EmailAddress = lower(REPLACE(cp.EmailAddress,'600',''))
	FROM ClientPersonnel cp WITH ( NOLOCK ) 
	WHERE cp.ClientPersonnelID = @WhoCreated
			
	SELECT @ObjectTypeLookupListItemID = CASE @AquariumObjectTypeID
											WHEN 6 THEN 76053 /*Document Template*/
										 END
	
	
	/*Pick up the name*/
	IF @AquariumObjectTypeID = 6
	BEGIN	
		SELECT @ObjectName = dt.DocumentTypeName
		FROM DocumentType dt WITH ( NOLOCK ) 
		WHERE dt.DocumentTypeID = @ObjectID
	END
										 
	IF @ObjectName <> ''
	BEGIN

		INSERT TableRows ( ClientID, LeadID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit )
		SELECT TOP 1 l.ClientID, l.LeadID, 179406, 19161, 1, 1 /*Ad Hoc Release Queue*/
		FROM Lead l WITH ( NOLOCK ) 
		WHERE l.LeadID = @LeadID 
		AND l.LeadTypeID = 1502 /*Config Release*/
		
		SELECT @TableRowID = SCOPE_IDENTITY()
		EXEC _C00_CompleteTableRow @TableRowID, NULL, 1, 0
		
		IF @TableRowID > 0
		BEGIN
			EXEC dbo._C00_SimpleValueIntoField 179402, @ObjectTypeLookupListItemID	, @TableRowID --Config Release Ad Hoc Release Type
			EXEC dbo._C00_SimpleValueIntoField 179403, @ObjectID					, @TableRowID --Config Release Ad Hoc Release Object ID
			EXEC dbo._C00_SimpleValueIntoField 179409, @ObjectName					, @TableRowID --Config Release Ad Hoc Release Object Name
			EXEC dbo._C00_SimpleValueIntoField 179404, @Notes						, @TableRowID --Config Release Ad Hoc Release Comments
			EXEC dbo._C00_SimpleValueIntoField 179405, @VarcharDate					, @TableRowID --Config Release Ad Hoc Release Date Added
			EXEC dbo._C00_SimpleValueIntoField 179407, @UserName					, @TableRowID --Config Release Ad Hoc Release Who Added
			EXEC dbo._C00_SimpleValueIntoField 179410, @EmailAddress				, @TableRowID --Config Release Ad Hoc Release Email On Completion
		END
		
	END

	RETURN @TableRowID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CR_AdHoc_Queue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CR_AdHoc_Queue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CR_AdHoc_Queue] TO [sp_executeall]
GO
