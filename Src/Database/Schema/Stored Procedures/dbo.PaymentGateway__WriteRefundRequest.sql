SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-05-20
-- Description:	WriteRepeatPaymentRequest
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteRefundRequest]
	@ClientID INT,
	@CustomerID INT,
	@RefundRequestVendorTxCode VARCHAR(2000),
	@RefundRequestAmount VARCHAR(2000),
	@RefundRequestCurrency VARCHAR(2000),
	@RefundRequestDescription VARCHAR(2000),
	@RefundRequestRelatedVPSTxId VARCHAR(2000),
	@RefundRequestRelatedVendorTxCode VARCHAR(2000),
	@RefundRequestRelatedSecurityKey VARCHAR(2000),
	@RefundRequestRelatedTxAuthNo VARCHAR(2000)
AS BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TableDetailFieldID INT,
			@TableDetailFieldPageID INT,
			@TableRowID INT

	SELECT TOP (1) @TableDetailFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ClientID = @ClientID 
	AND tpfm.ThirdPartyFieldID = 557
	
	SELECT @TableDetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @TableDetailFieldID

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
	VALUES (@ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID)

	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 557 THEN @RefundRequestVendorTxCode
																	WHEN 558 THEN @RefundRequestAmount
																	WHEN 559 THEN @RefundRequestCurrency
																	WHEN 560 THEN @RefundRequestDescription
																	WHEN 561 THEN @RefundRequestRelatedVPSTxId
																	WHEN 562 THEN @RefundRequestRelatedVendorTxCode
																	WHEN 563 THEN @RefundRequestRelatedSecurityKey
																	WHEN 564 THEN @RefundRequestRelatedTxAuthNo
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (557,558,559,560,561,562,563,564)


	DECLARE @RefundRequestVendorTxCodeDetailFieldID INT
	SELECT @RefundRequestVendorTxCodeDetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=618
	EXEC _C00_SimpleValueIntoField @RefundRequestVendorTxCodeDetailFieldID, @RefundRequestVendorTxCode, @CustomerID

	SELECT @TableRowID TableRowID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRefundRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteRefundRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteRefundRequest] TO [sp_executeall]
GO
