SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-02
-- Description:	List all Lead fields for a lead or type
-- 2017-06-29 CPS use a function to get the URL
-- =============================================
CREATE PROCEDURE [dbo].[l] 
	@LeadID int = NULL,
	@LeadTypeID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	/* Grab a sample lead if only LeadType is specified */
	IF @LeadID IS NULL AND @LeadTypeID IS NOT NULL
	BEGIN
		SELECT TOP 100 c.Fullname, l.*, lt.* 
		FROM dbo.Lead l (nolock) 
		INNER JOIN dbo.LeadType lt (nolock) on lt.LeadTypeID = l.LeadTypeID 
		INNER JOIN dbo.Customers c (nolock) on c.CustomerID = l.CustomerID 
		WHERE l.LeadTypeID = @LeadTypeID 
		ORDER BY l.LeadID DESC
	END
	
	IF @LeadID IS NOT NULL
	BEGIN
		SELECT c.Fullname, l.*, lt.* 
		FROM dbo.Lead l (nolock) 
		INNER JOIN dbo.LeadType lt (nolock) on lt.LeadTypeID = l.LeadTypeID 
		INNER JOIN dbo.Customers c (nolock) on c.CustomerID = l.CustomerID 
		WHERE l.LeadID = @LeadID 
		
		SELECT	dbo.fn_C00_GetUrlByDatabase() + 'CustomersLeadDetails2.aspx?cid=' + convert(varchar,l.CustomerID) + '&lid=' + convert(varchar,l.LeadID) [URL],
				dbo.fn_C00_GetUrlByDatabase() + 'DetailFieldView.aspx?type=1&mode=1&lid=' + convert(varchar,l.LeadID) + '&ru=customersleaddetails2.aspx%3f_c_i_d_%3d' + convert(varchar,l.CustomerID) + '_a_n_d__l_i_d_%3d' + convert(varchar,l.LeadID) [LeadDetailValues]
		FROM Lead l WITH (NOLOCK) 
		WHERE l.LeadID = @LeadID
		
		SELECT 'Cases for this Lead' [Cases for this Lead], * 
		FROM dbo.Cases c WITH (NOLOCK) 
		WHERE c.LeadID = @LeadID
		ORDER BY c.CaseNum 
	END

END







GO
GRANT VIEW DEFINITION ON  [dbo].[l] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[l] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[l] TO [sp_executeall]
GO
