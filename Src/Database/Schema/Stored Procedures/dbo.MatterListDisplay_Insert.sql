SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the MatterListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_Insert]
(

	@MatterListDisplayID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@MatterRef bit   ,

	@MatterStatus bit   ,

	@MatterID bit   ,

	@Field1 int   ,

	@Field2 int   ,

	@Field3 int   ,

	@Field4 int   ,

	@Field5 int   ,

	@Field1ColumnDetailFieldID int   ,

	@Field2ColumnDetailFieldID int   ,

	@Field3ColumnDetailFieldID int   ,

	@Field4ColumnDetailFieldID int   ,

	@Field5ColumnDetailFieldID int   ,

	@SqlQueryText varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[MatterListDisplay]
					(
					[ClientID]
					,[LeadTypeID]
					,[MatterRef]
					,[MatterStatus]
					,[MatterID]
					,[Field1]
					,[Field2]
					,[Field3]
					,[Field4]
					,[Field5]
					,[Field1ColumnDetailFieldID]
					,[Field2ColumnDetailFieldID]
					,[Field3ColumnDetailFieldID]
					,[Field4ColumnDetailFieldID]
					,[Field5ColumnDetailFieldID]
					,[SqlQueryText]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@MatterRef
					,@MatterStatus
					,@MatterID
					,@Field1
					,@Field2
					,@Field3
					,@Field4
					,@Field5
					,@Field1ColumnDetailFieldID
					,@Field2ColumnDetailFieldID
					,@Field3ColumnDetailFieldID
					,@Field4ColumnDetailFieldID
					,@Field5ColumnDetailFieldID
					,@SqlQueryText
					)
				-- Get the identity value
				SET @MatterListDisplayID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Insert] TO [sp_executeall]
GO
