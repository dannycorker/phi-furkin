SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-02-18
-- Description:	Create DocumentZip records as soon as the documents have reached the queue.
--              This gets called by the scheduler at the end of an AutomatedTask that adds
--              an event of type "LetterOut" with an associated document.
-- TAGS: "Auto Zip 10 100 documents per zip" Jim Alex Cathal It's this one!
-- =============================================
CREATE PROCEDURE [dbo].[CreateDocumentZipsFromAutomatedTask] 
	@ClientID int,
	@AutomatedTaskID int, 
	@DocumentTypeID int,
	@DocumentsPerZip int = 100
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @CanBeAutoSent bit,
	@DocumentTypeName varchar(50),
	@DocsToZipCount int,
	@ValidControlRecords int,
	@DocumentZipInformationID int,
	@ServerMapPath varchar(2000), 
	@SecurePath char(1), 
	@HostPath varchar(250), 
	@WordTempDirectory varchar(250), 
	@ZipPrefix varchar(50), 
	@StatusID int

	-- If documents need to be individually tailored before saving & printing,
	-- then they cannot be auto-queued. They must be completed manually, one at a time.
	SELECT @CanBeAutoSent = dt.CanBeAutoSent, 
	@DocumentTypeName = dt.DocumentTypeName 
	FROM dbo.DocumentType dt 
	WHERE dt.DocumentTypeID = @DocumentTypeID 

	IF @ClientID = 145 
	BEGIN

		/*2012-09-19 ACE TNT require 1 document per zip*/
		SET @DocumentsPerZip = 1

	END

	IF @CanBeAutoSent = 1
	BEGIN

		-- Special processing should only take place for documents
		-- that have a control record set up to handle them.
		-- Regular processing (where controlRespondsToDocumentTypeID is 0)
		-- takes place under all circumstances unless a special control record exists
		-- with "Suppress all after me" set to true.
		SELECT @ValidControlRecords = COUNT(*) 
		FROM dbo.DocumentZipFtpControl con 
		WHERE con.ClientID IN (@ClientID) 
		AND con.Enabled = 1 
		AND ((con.SpecificDocumentTypesOnly = 0) OR EXISTS(SELECT * FROM dbo.DocumentZipFtpControlLink link WHERE link.ClientID = con.ClientID AND link.DocumentZipFtpControlID = con.DocumentZipFtpControlID AND link.Enabled = 1 AND link.DocumentTypeID = @DocumentTypeID))

		IF @ValidControlRecords > 0
		BEGIN
			SELECT @DocsToZipCount = COUNT(*) 
			FROM dbo.DocumentQueue dq 
			WHERE dq.ClientID = @ClientID
			AND dq.[WhenCreated] IS NULL
			AND dq.[DocumentTypeID] = @DocumentTypeID
			AND NOT EXISTS (
				SELECT * 
				FROM DocumentZip dz 
				WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
			)

			IF @DocsToZipCount > 0
			BEGIN

				SELECT @ServerMapPath = 'ClientImages\Client_' + convert(varchar, @ClientID) + '_Images\DocumentZips', 
				@SecurePath = 's', 
				@HostPath = 'www.aquarium-software.com', 
				@WordTempDirectory = 'WordTemp', 
				@ZipPrefix = @DocumentTypeName, 
				@StatusID = 35
				
				-- Insert a Zip Information header record for this batch of records
				INSERT INTO [dbo].[DocumentZipInformation] (ClientID, ServerMapPath, SecurePath, HostPath, WordTempDirectory, DocumentsPerZip, ZipPrefix, StatusID, TriggeredByAutomatedTaskID, TriggeredByDocumentTypeID)
				VALUES (@ClientID, @ServerMapPath, @SecurePath, @HostPath, @WordTempDirectory, @DocumentsPerZip, @ZipPrefix, @StatusID, @AutomatedTaskID, @DocumentTypeID)

				SELECT @DocumentZipInformationID = SCOPE_IDENTITY()

				-- Insert the detail records
				INSERT INTO dbo.DocumentZip (DocumentZipInformationID, ClientID, DocumentQueueID, StatusID) 
				SELECT @DocumentZipInformationID, @ClientID, dq.DocumentQueueID, @StatusID
				FROM dbo.DocumentQueue dq 
				WHERE dq.ClientID = @ClientID
				AND dq.[WhenCreated] IS NULL
				AND dq.[DocumentTypeID] = @DocumentTypeID
				AND NOT EXISTS (
					SELECT * 
					FROM DocumentZip dz 
					WHERE dz.[DocumentQueueID] = dq.[DocumentQueueID]
				)

				-- Now simply wait for the DocumentZips() method to fire in the batch scheduler
				-- app, which happens once every minute.  That will pick up these records, produce
				-- the documents, zip them, and then run any special bat/ftp commands as dictated
				-- by the DocumentZipFtpControl table for this client and document type.

			END --IF @DocsToZipCount > 0

		END --IF @ValidControlRecords > 0

	END --IF @CanBeAutoSent = 1

END




GO
GRANT VIEW DEFINITION ON  [dbo].[CreateDocumentZipsFromAutomatedTask] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreateDocumentZipsFromAutomatedTask] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreateDocumentZipsFromAutomatedTask] TO [sp_executeall]
GO
