SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedButNotComplete] @id int
AS
BEGIN
Delete From ClientQuestionnairesVisitedButNotComplete where ClientQuestionnairesVisitedButNotCompleteID = @id
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedButNotComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedButNotComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveVisitedRecord_ClientQuestionnairesVisitedButNotComplete] TO [sp_executeall]
GO
