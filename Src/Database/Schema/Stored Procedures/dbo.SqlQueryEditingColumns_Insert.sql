SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditingColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_Insert]
(

	@SqlQueryEditingColumnID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SqlQueryEditingTableID int   ,

	@CompleteOutputText varchar (500)  ,

	@ColumnAlias varchar (50)  ,

	@ShowColumn bit   ,

	@DisplayOrder int   ,

	@SortOrder int   ,

	@SortType varchar (10)  ,

	@IsAggregate bit   ,

	@ColumnDataType varchar (50)  ,

	@ManipulatedDataType varchar (50)  ,

	@ColumnNaturalName varchar (50)  ,

	@RealSqlQueryColumnID int   ,

	@ComplexValue nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditingColumns]
					(
					[ClientID]
					,[SqlQueryEditingID]
					,[SqlQueryEditingTableID]
					,[CompleteOutputText]
					,[ColumnAlias]
					,[ShowColumn]
					,[DisplayOrder]
					,[SortOrder]
					,[SortType]
					,[IsAggregate]
					,[ColumnDataType]
					,[ManipulatedDataType]
					,[ColumnNaturalName]
					,[RealSqlQueryColumnID]
					,[ComplexValue]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryEditingID
					,@SqlQueryEditingTableID
					,@CompleteOutputText
					,@ColumnAlias
					,@ShowColumn
					,@DisplayOrder
					,@SortOrder
					,@SortType
					,@IsAggregate
					,@ColumnDataType
					,@ManipulatedDataType
					,@ColumnNaturalName
					,@RealSqlQueryColumnID
					,@ComplexValue
					)
				-- Get the identity value
				SET @SqlQueryEditingColumnID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Insert] TO [sp_executeall]
GO
