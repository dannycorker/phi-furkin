SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack	
-- Create date: 2012-07-18
-- Description:	Aquarium Config Value Delete
-- =============================================
CREATE PROCEDURE [dbo].[AquariumConfigValue__Delete]
	@ConfigID INT, 
	@ClientID INT, 
	@SubClientID INT,
	@ConfigValueID INT
AS
BEGIN

	DELETE FROM AquariumConfigValue
	WHERE
		ConfigValueID = @ConfigValueID
	AND
		ConfigID = @ConfigID
	AND
		ClientID = @ClientID
	AND
		SubClientID = @SubClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfigValue__Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumConfigValue__Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfigValue__Delete] TO [sp_executeall]
GO
