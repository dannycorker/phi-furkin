SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DiaryReminder table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryReminder_Delete]
(

	@DiaryReminderID int   
)
AS


				DELETE FROM [dbo].[DiaryReminder] WITH (ROWLOCK) 
				WHERE
					[DiaryReminderID] = @DiaryReminderID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Delete] TO [sp_executeall]
GO
