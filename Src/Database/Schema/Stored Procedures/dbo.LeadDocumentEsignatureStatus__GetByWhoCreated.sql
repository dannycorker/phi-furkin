SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 6th November 2009
-- Description:	Gets a LeadDocumentEsignatureStatus based upon who created it, 
--              between the from and to dates and possibly the LeadTypeID
--			
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus__GetByWhoCreated]

@WhoCreatedID int,
@FromDate datetime,
@ToDate datetime,
@LeadTypeID int = null,
@StatusID int = null

AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT
		dbo.LeadEvent.LeadID, dbo.LeadDocumentEsignatureStatus.DateSent, dbo.LeadEvent.WhoCreated,
		dbo.LeadDocumentEsignatureStatus.LeadDocumentID, dbo.LeadDocumentEsignatureStatus.DocumentName, 
		dbo.LeadDocument.EmailFrom, dbo.LeadDocumentEsignatureStatus.SentTo, 
		dbo.LeadDocumentEsignatureStatus.ElectronicSignatureDocumentKey,   
		dbo.Lead.LeadTypeID, dbo.LeadType.LeadTypeName, dbo.LeadDocumentEsignatureStatus.EsignatureStatusID, 
		dbo.LeadDocumentEsignatureStatus.StatusCheckedDate, dbo.LeadDocumentEsignatureStatus.ClientID,
		dbo.LeadDocumentEsignatureStatus.LeadDocumentEsignatureStatusID
	FROM 
		dbo.LeadDocumentEsignatureStatus with(NOLOCK)
		INNER JOIN dbo.LeadDocument with(NOLOCK) ON dbo.LeadDocumentEsignatureStatus.LeadDocumentID = dbo.LeadDocument.LeadDocumentID 
		INNER JOIN dbo.LeadEvent with(NOLOCK) ON dbo.LeadEvent.LeadDocumentID = dbo.LeadDocument.LeadDocumentID 
		INNER JOIN dbo.Lead with(NOLOCK) ON dbo.LeadEvent.LeadID = dbo.Lead.LeadID 
		INNER JOIN dbo.LeadType with(NOLOCK)ON dbo.Lead.LeadTypeID = dbo.LeadType.LeadTypeID 
	WHERE 
		dbo.LeadEvent.WhoCreated = @WhoCreatedID
		AND (dbo.Lead.LeadTypeID = @LeadTypeID OR @LeadTypeID is null) 
		AND (dbo.LeadDocumentEsignatureStatus.EsignatureStatusID = @StatusID OR @StatusID is null)
		AND dbo.LeadDocumentEsignatureStatus.DateSent BETWEEN @FromDate AND @ToDate
		Order By dbo.LeadDocumentEsignatureStatus.DateSent desc
		
END





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus__GetByWhoCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus__GetByWhoCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus__GetByWhoCreated] TO [sp_executeall]
GO
