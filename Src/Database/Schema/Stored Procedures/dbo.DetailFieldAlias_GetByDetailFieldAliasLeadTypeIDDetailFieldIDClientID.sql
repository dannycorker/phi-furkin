SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldAlias table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_GetByDetailFieldAliasLeadTypeIDDetailFieldIDClientID]
(

	@DetailFieldAlias varchar (500)  ,

	@LeadTypeID int   ,

	@DetailFieldID int   ,

	@ClientID int   
)
AS


				SELECT
					[DetailFieldAliasID],
					[ClientID],
					[LeadTypeID],
					[DetailFieldID],
					[DetailFieldAlias]
				FROM
					[dbo].[DetailFieldAlias] WITH (NOLOCK) 
				WHERE
										[DetailFieldAlias] = @DetailFieldAlias
					AND [LeadTypeID] = @LeadTypeID
					AND [DetailFieldID] = @DetailFieldID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByDetailFieldAliasLeadTypeIDDetailFieldIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_GetByDetailFieldAliasLeadTypeIDDetailFieldIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByDetailFieldAliasLeadTypeIDDetailFieldIDClientID] TO [sp_executeall]
GO
