SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-10-2014
-- Description:	Finds the unique response id
-- =============================================
CREATE PROCEDURE [dbo].[_C00_FindUniqueResponseID]
	@ClientID INT,
	@LeadTypeID INT,
	@ResponseID varchar(2000)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DetailFieldID INT
	SELECT @DetailFieldID = DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=1330 and ClientID = @ClientID and LeadTypeID=@LeadTypeID

	DECLARE @LeadID INT	
	DECLARE @MatterID INT
	
	SELECT @LeadID=LeadID, @MatterID=MatterID FROM MatterDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID=@DetailFieldID and DetailValue = @ResponseID
	
	IF (@MatterID is not null) AND (@MatterID<>0)
	BEGIN
		SELECT	ClientID, 
				CustomerID, LeadID, CaseID, MatterID				
		FROM Matter WITH (NOLOCK) 
		WHERE MatterID = @MatterID
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindUniqueResponseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindUniqueResponseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindUniqueResponseID] TO [sp_executeall]
GO
