SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldLink table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_GetByDetailFieldLinkID]
(

	@DetailFieldLinkID int   
)
AS


				SELECT
					[DetailFieldLinkID],
					[ClientID],
					[DetailFieldID],
					[MasterQuestionID],
					[LeadTypeID]
				FROM
					[dbo].[DetailFieldLink] WITH (NOLOCK) 
				WHERE
										[DetailFieldLinkID] = @DetailFieldLinkID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_GetByDetailFieldLinkID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_GetByDetailFieldLinkID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_GetByDetailFieldLinkID] TO [sp_executeall]
GO
