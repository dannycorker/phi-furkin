SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DetailFieldStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldStyle_Delete]
(

	@DetailFieldStyleID int   
)
AS


				DELETE FROM [dbo].[DetailFieldStyle] WITH (ROWLOCK) 
				WHERE
					[DetailFieldStyleID] = @DetailFieldStyleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldStyle_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Delete] TO [sp_executeall]
GO
