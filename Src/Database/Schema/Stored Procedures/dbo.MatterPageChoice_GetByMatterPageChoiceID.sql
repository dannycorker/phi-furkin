SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterPageChoice table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_GetByMatterPageChoiceID]
(

	@MatterPageChoiceID int   
)
AS


				SELECT
					[MatterPageChoiceID],
					[ClientID],
					[MatterID],
					[DetailFieldPageID],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[MatterPageChoice] WITH (NOLOCK) 
				WHERE
										[MatterPageChoiceID] = @MatterPageChoiceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_GetByMatterPageChoiceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_GetByMatterPageChoiceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_GetByMatterPageChoiceID] TO [sp_executeall]
GO
