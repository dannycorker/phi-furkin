SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetCustomerLeadCaseSummary] 
	@LeadID int,
	@CaseID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT		Lead.LeadID,
				Customers.CustomerID,
				Customers.Fullname,
				LeadType.LeadTypeName,
				LeadStatus.StatusName AS CaseStatus
	FROM		Lead
	INNER JOIN	Customers ON Lead.CustomerID = Customers.CustomerID
	INNER JOIN	LeadType ON Lead.LeadTypeID = LeadType.LeadTypeID
	INNER JOIN	Cases ON Lead.LeadID = Cases.LeadID
	INNER JOIN	LeadStatus ON Cases.ClientStatusID = LeadStatus.StatusID
	WHERE		Lead.LeadID = @LeadID
	AND			Cases.CaseID = @CaseID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerLeadCaseSummary] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomerLeadCaseSummary] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomerLeadCaseSummary] TO [sp_executeall]
GO
