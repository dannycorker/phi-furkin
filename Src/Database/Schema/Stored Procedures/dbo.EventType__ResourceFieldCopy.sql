SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-19
-- Description:	Run batch jobs based on events 
-- =============================================
CREATE PROCEDURE [dbo].[EventType__ResourceFieldCopy]
	@LeadEventID int
AS
BEGIN

	SET NOCOUNT ON;

	Declare @EventTypeResourceFieldCopyID int, 
			@TestID int,
			@SQLToRun varchar(max),
			@CaseID int,
			@EventTypeID int
	
    -- If all IDs are known then we have slightly less work to do,
	-- otherwise look them up from the LeadEvent record.

	SELECT @EventTypeID = EventTypeID, @CaseID = CaseID
	FROM LeadEvent 
	WHERE LeadEventID = @LeadEventID 

	IF @EventTypeID IS NOT NULL
	BEGIN

		Select top 1 @EventTypeResourceFieldCopyID = EventTypeResourceListFieldCopyID, @TestID = 0
		From EventTypeResourceFieldCopy
		Where EventTypeID = @EventTypeID
		Order By EventTypeResourceListFieldCopyID
	
		While @EventTypeResourceFieldCopyID > @TestID
		BEGIN
		
			Select @SQLToRun = 'exec dbo._C00_PopulateFieldWithResourceListValue ' + CONVERT(varchar,DestinationDetailFieldID) + ', ' + CONVERT(varchar,ResourceDetailFieldID) + ', ' + convert(varchar,ResourceContentsDetailFieldID) + ', ' + CONVERT(varchar,@CaseID)
			From EventTypeResourceFieldCopy
			Where EventTypeResourceListFieldCopyID = @EventTypeResourceFieldCopyID
		
			exec (@SQLToRun)
		
			--select @SQLToRun
			
			Select @TestID = @EventTypeResourceFieldCopyID		

			Select top 1 @EventTypeResourceFieldCopyID = EventTypeResourceListFieldCopyID
			From EventTypeResourceFieldCopy
			Where EventTypeID = @EventTypeID
			and EventTypeResourceListFieldCopyID > @TestID
			Order By EventTypeResourceListFieldCopyID
		
		END

	END

END






GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__ResourceFieldCopy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__ResourceFieldCopy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__ResourceFieldCopy] TO [sp_executeall]
GO
