SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-10-13
-- Description:	Populate/Update a tablerow
--				NOTE TableDetailValues CAN be NULL allow NULL Insertions
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateTDVWithValues]
(
	@LeadID int,
	@MatterID int = 0,
	@ClientID int,
	@ClientPersonnelID int = NULL,
	@tvpIDValue tvpIDValue READONLY,
	@TableRowID int = NULL,
	@TableDetailFieldID int = NULL,
	@TableDetailFieldPageID int = NULL,
	@DenyEdit int = NULL,
	@DenyDelete int = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Chesk to see if a table row id has been supplied, if it has then update the row, otherwise add it*/
	IF @TableRowID IS NOT NULL
	BEGIN

		/* Update any values that already exist but have changed */
		UPDATE dbo.TableDetailValues 
		SET DetailValue = t.AnyValue
		FROM @tvpIDValue t 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = t.AnyID AND tdv.TableRowID = @TableRowID 
		WHERE tdv.DetailValue <> t.AnyValue
	
		/* Insert any values that don't exist already */
		INSERT INTO dbo.TableDetailValues (ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue, ResourceListID)
		SELECT df.ClientID, @LeadID, @MatterID, @TableRowID, t.AnyID, CASE df.QuestionTypeID WHEN 14 THEN NULL ELSE t.AnyValue END, CASE df.QuestionTypeID WHEN 14 THEN t.AnyValue ELSE NULL END 
		FROM @tvpIDValue t 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter IN (6,8) 
		WHERE NOT EXISTS (
			SELECT * 
			FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
			WHERE tdv.TableRowID = @TableRowID
			AND tdv.DetailFieldID = t.AnyID 
		)
	
	END
	ELSE
	BEGIN
	
		/* Create Table Row */
		INSERT INTO dbo.TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete)
		VALUES (@ClientID, @LeadID, @MatterID, @TableDetailFieldID, @TableDetailFieldPageID, @DenyEdit, @DenyDelete)
		
		Select @TableRowID = SCOPE_IDENTITY()
	
		/* Insert values into for that tablerow */
		INSERT INTO dbo.TableDetailValues (ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue, ResourceListID)
		SELECT df.ClientID, @LeadID, @MatterID, @TableRowID, t.AnyID, CASE df.QuestionTypeID WHEN 14 THEN NULL ELSE t.AnyValue END, CASE df.QuestionTypeID WHEN 14 THEN t.AnyValue ELSE NULL END 
		FROM @tvpIDValue t 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter IN (6,8) 

	END

	RETURN @TableRowID
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateTDVWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateTDVWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateTDVWithValues] TO [sp_executeall]
GO
