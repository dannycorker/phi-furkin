SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PatchNote table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PatchNote_Get_List]

AS


				
				SELECT
					[PatchNoteID],
					[PatchID],
					[Location],
					[PatchNoteText],
					[AdminOnly]
				FROM
					[dbo].[PatchNote] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PatchNote_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Get_List] TO [sp_executeall]
GO
