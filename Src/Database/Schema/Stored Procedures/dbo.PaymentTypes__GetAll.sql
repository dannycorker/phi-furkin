SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-14
-- Description:	Get Payment Types for client zero and the given client
-- =============================================
CREATE PROCEDURE [dbo].[PaymentTypes__GetAll]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT *
	FROM PaymentTypes WITH (NOLOCK)
	WHERE ClientID IN (@ClientID, 0)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentTypes__GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentTypes__GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentTypes__GetAll] TO [sp_executeall]
GO
