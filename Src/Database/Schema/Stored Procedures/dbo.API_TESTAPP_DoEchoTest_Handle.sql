SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_TESTAPP_DoEchoTest_Handle
-- =============================================
CREATE PROCEDURE [dbo].[API_TESTAPP_DoEchoTest_Handle]
	@QID UNIQUEIDENTIFIER,
	@Handled VARCHAR(50)
AS
BEGIN

	UPDATE API_Queue
		SET HandleXml = QueueXml,
			HandleDT = SYSDATETIME(),
			QueueByteSize = ISNULL(QueueByteSize, 0) + DATALENGTH(ISNULL(QueueXml,''))
	WHERE	QueueID = @QID
	AND		HandleDT IS NULL
	
	SELECT @QID QueueID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_TESTAPP_DoEchoTest_Handle] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_TESTAPP_DoEchoTest_Handle] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_TESTAPP_DoEchoTest_Handle] TO [sp_executeall]
GO
