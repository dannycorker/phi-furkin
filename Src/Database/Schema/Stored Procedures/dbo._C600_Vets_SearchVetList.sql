SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-10-23
-- Description:	Vet search 
-- 2016-05-23 IS  Copied from 235
-- 2017-06-06 CPS LEFT joined additional VET List properties
-- 2017-06-21 CPS allowed a blank search to return all vets
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Vets_SearchVetList] 
(
	@Search VARCHAR(100)
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TopN INT = 10
	
	IF @Search = '' or @Search is NULL or @Search = '%'
	BEGIN
		SELECT @TopN = 99999
	END
	
		SELECT TOP (@TopN)
		r.ResourceListID VetId,
		rdvBusinessName.DetailValue AS BusinessName,
		rdvAddress1.DetailValue AS Address1,
		rdvAddress2.DetailValue AS Address2,
		rdvCity.DetailValue AS City,
		rdvProvince.DetailValue AS County,
		rdvPostalCode.DetailValue AS PostalCode,
		rdvTelephone.DetailValue AS PhoneNumber,
		rdvFax.DetailValue AS FaxNumber,
		rdvEmail.DetailValue AS Email,
		'NOTAVAILABLE' as VetCode
	FROM dbo.ResourceList r WITH (NOLOCK)
	INNER JOIN dbo.ResourceListDetailValues rdvBusinessName WITH (NOLOCK) ON r.ResourceListID = rdvBusinessName.ResourceListID AND rdvBusinessName.DetailFieldID = 144473
	INNER JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON r.ResourceListID = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
	 LEFT JOIN dbo.ResourceListDetailValues rdvAddress2 WITH (NOLOCK) ON r.ResourceListID = rdvAddress2.ResourceListID  AND rdvAddress2.DetailFieldID = 144476
	 LEFT JOIN dbo.ResourceListDetailValues rdvCity WITH (NOLOCK) ON r.ResourceListID = rdvCity.ResourceListID AND rdvCity.DetailFieldID = 144477
	 LEFT JOIN dbo.ResourceListDetailValues rdvProvince WITH (NOLOCK) ON r.ResourceListID = rdvProvince.ResourceListID AND rdvProvince.DetailFieldID = 144478
	 LEFT JOIN dbo.ResourceListDetailValues rdvPostalCode WITH (NOLOCK) ON r.ResourceListID = rdvPostalCode.ResourceListID AND rdvPostalCode.DetailFieldID = 144479
	 LEFT JOIN dbo.ResourceListDetailValues rdvTelephone WITH (NOLOCK) ON r.ResourceListID = rdvTelephone.ResourceListID AND rdvTelephone.DetailFieldID = 144480
	 LEFT JOIN dbo.ResourceListDetailValues rdvEmail WITH (NOLOCK) ON r.ResourceListID = rdvEmail.ResourceListID AND rdvEmail.DetailFieldID = 144482
	 LEFT JOIN dbo.ResourceListDetailValues rdvFax WITH (NOLOCK) ON r.ResourceListID = rdvFax.ResourceListID AND rdvFax.DetailFieldID = 144481
	--INNER JOIN dbo.ResourceListDetailValues rdvVetCode WITH (NOLOCK) ON r.ResourceListID = rdvVetCode.ResourceListID AND rdvVetCode.DetailFieldID = 161655			
	WHERE 
		(  (rdvBusinessName.DetailValue LIKE '%' + @Search + '%') 
		OR (rdvAddress1.DetailValue LIKE '%' + @Search + '%')
		OR (rdvAddress2.DetailValue LIKE '%' + @Search + '%')
		OR (rdvCity.DetailValue LIKE '%' + @Search + '%')
		OR (rdvProvince.DetailValue LIKE '%' + @Search + '%')
		OR (rdvPostalCode.DetailValue LIKE '%' + @Search + '%')
		)
	ORDER BY rdvBusinessName.DetailValue
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Vets_SearchVetList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Vets_SearchVetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Vets_SearchVetList] TO [sp_executeall]
GO
