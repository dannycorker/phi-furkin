SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-10-02
-- Description:	Get Table Rows by data
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetByData]
@LeadID int,
@MatterID int,
@instr varchar(max),
@DetailFieldID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @LeadOrMatter int

	Select @LeadOrMatter = LeadOrMatter
	From DetailFields (nolock)
	Where DetailFieldID = @DetailFieldID

	If @LeadOrMatter = 1
	Begin
	
		Select distinct tr.TableRowID
		From TableDetailValues tdv (nolock)
		Inner Join TableRows tr (nolock) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @DetailFieldID
		Inner Join fnTableOfValuesFromCSVPSV (@instr,',', '|') as criteria on criteria.AnyValue = tdv.DetailFieldID and criteria.AnyValue2 = tdv.DetailValue
		where tdv.LeadID = @LeadID

	End

	If @LeadOrMatter = 2
	Begin
	
		Select distinct tr.TableRowID
		From TableDetailValues tdv (nolock)
		Inner Join TableRows tr (nolock) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @DetailFieldID
		Inner Join fnTableOfValuesFromCSVPSV (@instr,',', '|') as criteria on criteria.AnyValue = tdv.DetailFieldID and criteria.AnyValue2 = tdv.DetailValue
		Where tdv.MatterID = @MatterID
	
	End
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetByData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByData] TO [sp_executeall]
GO
