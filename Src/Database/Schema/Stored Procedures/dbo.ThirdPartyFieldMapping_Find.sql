SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ThirdPartyFieldMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@ThirdPartyFieldMappingID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@ThirdPartyFieldID int   = null ,

	@DetailFieldID int   = null ,

	@ColumnFieldID int   = null ,

	@ThirdPartyFieldGroupID int   = null ,

	@IsEnabled bit   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ThirdPartyFieldMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [ThirdPartyFieldID]
	, [DetailFieldID]
	, [ColumnFieldID]
	, [ThirdPartyFieldGroupID]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldMappingID] = @ThirdPartyFieldMappingID OR @ThirdPartyFieldMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([ThirdPartyFieldID] = @ThirdPartyFieldID OR @ThirdPartyFieldID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ColumnFieldID] = @ColumnFieldID OR @ColumnFieldID IS NULL)
	AND ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID OR @ThirdPartyFieldGroupID IS NULL)
	AND ([IsEnabled] = @IsEnabled OR @IsEnabled IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ThirdPartyFieldMappingID]
	, [ClientID]
	, [LeadTypeID]
	, [ThirdPartyFieldID]
	, [DetailFieldID]
	, [ColumnFieldID]
	, [ThirdPartyFieldGroupID]
	, [IsEnabled]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
    WHERE 
	 ([ThirdPartyFieldMappingID] = @ThirdPartyFieldMappingID AND @ThirdPartyFieldMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([ThirdPartyFieldID] = @ThirdPartyFieldID AND @ThirdPartyFieldID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ColumnFieldID] = @ColumnFieldID AND @ColumnFieldID is not null)
	OR ([ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID AND @ThirdPartyFieldGroupID is not null)
	OR ([IsEnabled] = @IsEnabled AND @IsEnabled is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Find] TO [sp_executeall]
GO
