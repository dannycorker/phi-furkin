SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Chris Townsend
-- Purpose: Gets a count of the SqlQueryColumns for a given SqlQueryID
			Used to determine whether or not a summary report can be created
			from a parent report.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetSqlQueryColumnCount]
(
	@SqlQueryID int   
)
AS
		SELECT
			Count(SqlQueryColumnID) AS SqlQueryColumnCount
		FROM
			SqlQueryColumns
		WHERE
			SqlQueryID = @SqlQueryID
					
			






GO
GRANT VIEW DEFINITION ON  [dbo].[GetSqlQueryColumnCount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSqlQueryColumnCount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSqlQueryColumnCount] TO [sp_executeall]
GO
