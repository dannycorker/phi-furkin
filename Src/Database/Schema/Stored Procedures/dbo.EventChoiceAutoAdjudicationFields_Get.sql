SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the EventChoiceAutoAdjudicationFields view passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoiceAutoAdjudicationFields_Get]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  
)
AS


				
				BEGIN

				-- Build the sql query
				declare @SQL as nvarchar(4000)
				SET @SQL = ' SELECT * FROM [dbo].[EventChoiceAutoAdjudicationFields]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Execution the query
				exec sp_executesql @SQL
				
				-- Return total count
				Select @@ROWCOUNT as TotalRowCount
				
				END
			





GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoiceAutoAdjudicationFields_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoiceAutoAdjudicationFields_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoiceAutoAdjudicationFields_Get] TO [sp_executeall]
GO
