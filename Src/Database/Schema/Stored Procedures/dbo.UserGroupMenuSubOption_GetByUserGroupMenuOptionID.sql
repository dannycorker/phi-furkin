SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserGroupMenuSubOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuOptionID]
(

	@UserGroupMenuOptionID int   
)
AS


				SELECT
					[UserGroupMenuSubOptionID],
					[UserGroupMenuOptionID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[PanelItemName],
					[PanelItemCaption],
					[PanelItemURL],
					[ShowInFancyBox],
					[SubItemOrder]
				FROM
					[dbo].[UserGroupMenuSubOption] WITH (NOLOCK) 
				WHERE
										[UserGroupMenuOptionID] = @UserGroupMenuOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuSubOption_GetByUserGroupMenuOptionID] TO [sp_executeall]
GO
