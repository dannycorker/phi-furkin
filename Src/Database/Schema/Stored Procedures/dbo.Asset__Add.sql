SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Add Asset to Asset table
-- =============================================
CREATE PROCEDURE [dbo].[Asset__Add] 
	@ClientPersonnelID INT = NULL,
	@ClientID INT = NULL,
	@AssetTypeID INT = NULL,
	@AssetSubTypeID INT = NULL,
	@LocationID INT = NULL,
	@AssetName VARCHAR(255) = NULL,
	@Note VARCHAR(255) = NULL,
	@SecureNote VARCHAR(255) = NULL,
	@SecureUserName VARCHAR(255) = NULL,
	@SecurePassword VARCHAR(255) = NULL,
	@SecureOther VARCHAR(255) = NULL,
	@Version VARCHAR(255) = NULL,
	@ValidFrom DATETIME = NULL,
	@ValidTo DATETIME = NULL,
	@Enabled BIT = NULL,
	@Deleted BIT = NULL,
	@ReminderTimeUnitID INT = NULL,
	@ReminderTimeUnitQuantity INT = NULL,
	@ReminderNotificationGroupID INT = NULL
	-- @WhoCreated = ClientPersonnelID
	-- @WhenCreated = Current DATETIME
	-- @WhoModified = ClientPersonnelID
	-- @WhenModified = Current DATETIME
	
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Asset] (
		[ClientID], 
		[AssetTypeID], 
		[AssetSubTypeID], 
		[LocationID], 
		[AssetName], 
		[Note], 
		[SecureNote], 
		[SecureUserName], 
		[SecurePassword],
		[SecureOther], 
		[Version], 
		[ValidFrom], 
		[ValidTo], 
		[Enabled], 
		[Deleted],
		[ReminderTimeUnitID],
		[ReminderTimeUnitQuantity],
		[ReminderNotificationGroupID],
		[WhoCreated],
		[WhenCreated],
		[WhoModified],
		[WhenModified]
	)
	VALUES (
		@ClientID,
		@AssetTypeID,
		@AssetSubTypeID,
		@LocationID,
		@AssetName,
		@Note,
		@SecureNote,
		@SecureUserName,
		@SecurePassword,
		@SecureOther,
		@Version,
		@ValidFrom,
		@ValidTo,
		@Enabled,
		@Deleted,
		@ReminderTimeUnitID,
		@ReminderTimeUnitQuantity,
		@ReminderNotificationGroupID,
		@ClientPersonnelID,
		dbo.fn_GetDate_Local(),
		@ClientPersonnelID,
		dbo.fn_GetDate_Local()
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset__Add] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset__Add] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset__Add] TO [sp_executeall]
GO
