SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Validate GetQuoteValues Request
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_GetQuoteValues_Validate]
(
	@XmlRequest XML,
	@XmlResponse XML OUTPUT,
	@Result INT OUTPUT
)
AS
BEGIN

	DECLARE @ErrorMessages TABLE (OrderID INT, Code VARCHAR(50), Message VARCHAR(2000))
	
	IF NOT EXISTS (SELECT * FROM fn_C00_1273_CustomerInfo_ShredXML(@XmlRequest))
	BEGIN
		INSERT INTO @ErrorMessages
		SELECT	1, 'VM_101', 'Customer is required'
	END
	
	INSERT INTO @ErrorMessages
	SELECT	1, 'VM_109', 'Customer Postcode is required'
	FROM	fn_C00_1273_CustomerInfo_ShredXML(@XmlRequest)
	WHERE	ISNULL(Postcode,'') = ''
	

	IF NOT EXISTS (SELECT * FROM fn_C00_1273_PetInfo_ShredXML(@XmlRequest))
	BEGIN
		INSERT INTO @ErrorMessages
		SELECT	10, 'VM_201', 'Pet is required'
	END
	
	INSERT INTO @ErrorMessages
	SELECT	RowID, 'VM_201', 'Pet Species / PetType is required'
	FROM	fn_C00_1273_PetInfo_ShredXML(@XmlRequest)
	WHERE	NOT (SpeciesId > 0)
		UNION ALL
	SELECT	RowID, 'VM_203', 'Pet Breed is required'
	FROM	fn_C00_1273_PetInfo_ShredXML(@XmlRequest)
	WHERE	NOT (BreedId > 0)
		UNION ALL
	SELECT	RowID, 'VM_204', 'Pet Sex is required'
	FROM	fn_C00_1273_PetInfo_ShredXML(@XmlRequest)
	WHERE	NOT (ISNULL(Gender,'') IN('M','F'))
		UNION ALL
	SELECT	RowID, 'VM_205', 'Pet Birth Date is required and is aged between 0 and 30 years'
	FROM	fn_C00_1273_PetInfo_ShredXML(@XmlRequest)
	WHERE	BirthDate NOT BETWEEN DATEADD(year, -30, dbo.fn_GetDate_Local()) AND dbo.fn_GetDate_Local()

	IF EXISTS (SELECT * FROM @ErrorMessages)
	BEGIN 
		SELECT @Result = 0
	END
	ELSE
	BEGIN
		SELECT @Result = 1
	END
	
	SELECT @XmlResponse =
	(
		SELECT
		(
			SELECT	Code, Message
			FROM	@ErrorMessages MessageList
			ORDER BY OrderID, Code
			FOR XML PATH ('ApiMessage'), ROOT ('MessageList'), TYPE
		),
		@Result Result
		FOR XML PATH ('GetQuoteValuesResponse')
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetQuoteValues_Validate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_GetQuoteValues_Validate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetQuoteValues_Validate] TO [sp_executeall]
GO
