SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventChoice table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventChoice_GetByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				SELECT
					[EventChoiceID],
					[EventTypeID],
					[Description],
					[NextEventTypeID],
					[ClientID],
					[LeadTypeID],
					[ThreadNumber],
					[EscalationEvent],
					[Field],
					[LogicalOperator],
					[Value1],
					[Value2],
					[SqlClauseForInclusion],
					[Weighting],
					[ValueTypeID]
				FROM
					[dbo].[EventChoice] WITH (NOLOCK) 
				WHERE
										[LeadTypeID] = @LeadTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice_GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice_GetByLeadTypeID] TO [sp_executeall]
GO
