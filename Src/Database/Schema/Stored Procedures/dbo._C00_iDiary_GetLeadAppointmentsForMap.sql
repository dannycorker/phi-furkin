SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2014-03-21
-- Description:	Returns the appointments for a user which are linked through a lead to a customer with their longitude and latitude
-- =============================================

CREATE PROCEDURE [dbo].[_C00_iDiary_GetLeadAppointmentsForMap] 
(
	@ClientPersonnelID INT,
	@Date DATETIME
)

AS
BEGIN

	DECLARE @From DATETIME,
			@To DATETIME

	SELECT @From = @Date
	SELECT @To = DATEADD(SECOND, -1, DATEADD(DAY, 1, @From))

	;WITH CusomerAppointments AS
	(
		SELECT	c.CustomerID, c.Fullname, c.Address1, c.Address2, c.Town, c.County, c.PostCode, 
				a.DiaryAppointmentID, a.DiaryAppointmentTitle AS Title, a.LeadID, a.CaseID, 
				a.DueDate, a.EndDate, a.AllDayEvent
		FROM dbo.DiaryAppointment a WITH (NOLOCK) 
		INNER JOIN dbo.DiaryReminder r WITH (NOLOCK) ON a.DiaryAppointmentID = r.DiaryAppointmentID
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON a.LeadID = l.LeadID
		INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID
		WHERE r.ClientPersonnelID = @ClientPersonnelID
		AND 
		((@From <= a.EndDate AND @To >= a.DueDate) -- Start and end date set correctly
		OR
		(a.DueDate BETWEEN @From AND @To)) -- Only start date set... you are able to set a start date ahead of an end date
	), Geocode AS 
	(
		SELECT	c.*, p.PostCode AS Match, p.Latitude, p.Longitude,
				ROW_NUMBER() OVER(PARTITION BY c.DiaryAppointmentID ORDER BY LEN(p.PostCode) DESC) as rn 
		FROM CusomerAppointments c
		LEFT JOIN AquariusMaster.dbo.PostCodeLookup p WITH (NOLOCK) ON REPLACE(c.Postcode, ' ', '') LIKE p.PostCode + '%'
	)
	SELECT *
	FROM Geocode 
	WHERE rn = 1 
	OR Match IS NULL

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_iDiary_GetLeadAppointmentsForMap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_iDiary_GetLeadAppointmentsForMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_iDiary_GetLeadAppointmentsForMap] TO [sp_executeall]
GO
