SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-06-2016
-- Description:	Gets a list of purchased products for the given client
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProduct__GetByCustomerID]

	@CustomerID INT,
	@ClientID INT

AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM PurchasedProduct WITH (NOLOCK) 
	WHERE CustomerID=@CustomerID AND ClientID=@ClientID
	ORDER BY ProductPurchasedOnDate
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProduct__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProduct__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProduct__GetByCustomerID] TO [sp_executeall]
GO
