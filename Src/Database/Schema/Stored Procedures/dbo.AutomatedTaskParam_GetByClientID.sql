SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskParam table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AutomatedTaskParamID],
					[TaskID],
					[ClientID],
					[ParamName],
					[ParamValue]
				FROM
					[dbo].[AutomatedTaskParam] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByClientID] TO [sp_executeall]
GO
