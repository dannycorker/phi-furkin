SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to copy an existing Matter to a new one on a new Case 
-- 2010-04-19 JWG Added WhoCreated
-- 2010-09-21 JWG Added fnRefLetterFromCaseNum for leads with more than 26 matters
-- 2010-10-05 ACE Added same case
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyMatter]
(
	@LeadEventID int,
	@SameCase int = 0
)


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TableRows TABLE (
		TableRowID int
	)
	declare @TableRowIDsToDelete table(
		TableRowID int
	)
	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int 
	);

	Declare	@myERROR int,
			@NewMatterID int,
			@LeadID int,
			@LeadTypeID int,
			@MatterID int,
			@ClientID int,
			@CaseNum int,
			@CaseRef varchar(100),
			@CaseID int,
			@ClientStatusID int,
			@AquariumStatusID int,
			@DefaultContactID int,
			@MatterRef nvarchar(100),
			@CustomerID int,
			@MatterStatus tinyint,
			@RowCount int, 
			@WhoCreated int

	Select top 1
	@LeadID = Lead.LeadID,
	@LeadTypeID = Lead.LeadTypeID,
	@MatterID = Matter.MatterID,
	@ClientID = Lead.ClientID,
	@CaseRef = Cases.CaseRef,
	@CaseID = Cases.CaseID,
	@ClientStatusID = Cases.ClientStatusID,
	@AquariumStatusID = Cases.AquariumStatusID,
	@DefaultContactID = Cases.DefaultContactID,
	@MatterRef = Matter.MatterRef,
	@CustomerID = Matter.CustomerID,
	@MatterStatus = Matter.MatterStatus,
	@WhoCreated = LeadEvent.WhoCreated 
	from leadevent WITH (NOLOCK) 
	inner join Cases WITH (NOLOCK) on Cases.Caseid = LeadEvent.CaseID
	inner join Lead WITH (NOLOCK) on Lead.LeadID = Cases.LeadID
	inner join Matter WITH (NOLOCK) on Matter.CaseID = Cases.CaseID
	where leadevent.leadeventid = @LeadEventID 
	order by Cases.CaseID Desc

	select @CaseNum = count(*)
	from cases
	where LeadID = @LeadID

	if (Select count (*) from Matter where caseid = @CaseID) = 1
	Begin

		Begin Tran

		IF @SameCase = 0
		BEGIN

			/* Create Case*/
			INSERT INTO Cases (LeadID,ClientID,CaseNum,CaseRef,ClientStatusID,AquariumStatusID,DefaultContactID, WhoCreated, WhenCreated, WhoModified, WhenModified)
			 VALUES (@LeadID,@ClientID,@CaseNum + 1,'Case ' + convert(varchar,(@CaseNum + 1)),@ClientStatusID,@AquariumStatusID,@DefaultContactID, @WhoCreated, dbo.fn_GetDate_Local(), @WhoCreated, dbo.fn_GetDate_Local())

			Select @myERROR = @@ERROR, @CaseID = SCOPE_IDENTITY()
			IF @myERROR != 0 GOTO HANDLE_ERROR

		END

		select @MatterRef = dbo.fnRefLetterFromCaseNum(1 + count(*)) 
		from matter where leadid = @LeadID

			/* Create Matter */
		INSERT INTO Matter([ClientID],[MatterRef],[CustomerID],[LeadID],[MatterStatus],[RefLetter],[BrandNew],[CaseID])
		 VALUES (@ClientID,'Copied From' + convert(varchar,@MatterID),@CustomerID,@LeadID,@MatterStatus,@MatterRef,0,@CaseID)

		Select @myERROR = @@ERROR, @NewMatterID = SCOPE_IDENTITY()
		IF @myERROR != 0 GOTO HANDLE_ERROR

	--	select @LeadID, @LeadTypeID, @MatterID

			/* Insert Matter Detail Values */
		INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		select @ClientID, @LeadID, @NewMatterID, mdv.DetailFieldID, mdv.DetailValue
		from MatterDetailValues mdv WITH (NOLOCK) 
		inner join DetailFields WITH (NOLOCK) on mdv.DetailFieldID = DetailFields.DetailfieldID
		where mdv.MatterID = @MatterID
		and DetailFields.LeadorMatter = 2

		Select @myERROR = @@ERROR, @RowCount = @@RowCount
		IF @myERROR != 0 GOTO HANDLE_ERROR

		Insert into Logs (LogDateTime, TypeOfLogEntry, ClassName, MethodName, LogEntry, ClientPersonnelID)
		Values (dbo.fn_GetDate_Local(), 1, '_C00_Copy_matter', '', convert(Varchar,@RowCount) + ',' + convert(Varchar,@NewMatterID), 0)

			/* Insert Table Rows */
		/*	Includes really dirty hack, note the Tablerowid being in the leadid field.*/
		/* This is due to the fact that when inserting a new row there is no way of linking */
		/* it back to the old one properly. This will work as long as there is no FK to Lead.LeadID on this table. */
		INSERT INTO TableRows(ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
		OUTPUT inserted.TableRowID INTO @TableRows
		SELECT @ClientID, TableRowID, @NewMatterID, TableRows.DetailFieldID, TableRows.DetailFieldPageID
		FROM TableRows WITH (NOLOCK) 
		Where TableRows.MatterID = @MatterID

		Select @myERROR = @@ERROR
		IF @myERROR != 0 GOTO HANDLE_ERROR

		INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
		SELECT tr_real.TableRowID, tdv.ResourceListID, tdv.DetailFieldID, tdv.DetailValue, @LeadID, @NewMatterID, @ClientID
		FROM @TableRows tr 
		INNER JOIN TableRows tr_real WITH (NOLOCK) ON tr_real.TableRowID = tr.TableRowID
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.MatterID = @MatterID and tr_real.LeadID = tdv.TableRowID

		Select @myERROR = @@ERROR
		IF @myERROR != 0 GOTO HANDLE_ERROR

		/* Clean up data after nasty hack */
		Update TableRows Set Leadid = @LeadID
		FROM @TableRows tr
		Where TableRows.TableRowID = tr.TableRowID

		Select @myERROR = @@ERROR
		IF @myERROR != 0 GOTO HANDLE_ERROR

		COMMIT TRAN -- No Errors, so commit all work

		Return @NewMatterID

		GOTO END_NOW

	HANDLE_ERROR:
		ROLLBACK TRAN

	END_NOW:

END

if (Select count (*) from Matter where caseid = @CaseID) <> 1
Begin
	Select 'There is more than one matter for this case!'
END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyMatter] TO [sp_executeall]
GO
