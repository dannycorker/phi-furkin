SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the GroupFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing_Insert]
(

	@GroupFunctionControlEditingID int    OUTPUT,

	@ClientPersonnelAdminGroupID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants bit   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[GroupFunctionControlEditing]
					(
					[ClientPersonnelAdminGroupID]
					,[ModuleID]
					,[FunctionTypeID]
					,[HasDescendants]
					,[RightID]
					,[LeadTypeID]
					)
				VALUES
					(
					@ClientPersonnelAdminGroupID
					,@ModuleID
					,@FunctionTypeID
					,@HasDescendants
					,@RightID
					,@LeadTypeID
					)
				-- Get the identity value
				SET @GroupFunctionControlEditingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_Insert] TO [sp_executeall]
GO
