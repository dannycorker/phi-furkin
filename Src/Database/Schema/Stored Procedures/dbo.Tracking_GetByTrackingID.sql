SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Tracking table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Tracking_GetByTrackingID]
(

	@TrackingID int   
)
AS


				SELECT
					[TrackingID],
					[TrackingName],
					[TrackingDescription],
					[StartDate],
					[EndDate],
					[ClientQuestionnaireID],
					[ClientID]
				FROM
					[dbo].[Tracking] WITH (NOLOCK) 
				WHERE
										[TrackingID] = @TrackingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_GetByTrackingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Tracking_GetByTrackingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_GetByTrackingID] TO [sp_executeall]
GO
