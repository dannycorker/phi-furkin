SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientOffices table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientOfficeID int   = null ,

	@ClientID int   = null ,

	@OfficeName varchar (50)  = null ,

	@BuildingName varchar (50)  = null ,

	@Address1 varchar (200)  = null ,

	@Address2 varchar (200)  = null ,

	@Town varchar (200)  = null ,

	@County varchar (50)  = null ,

	@Country varchar (200)  = null ,

	@PostCode varchar (10)  = null ,

	@OfficeTelephone varchar (50)  = null ,

	@OfficeTelephoneExtension varchar (10)  = null ,

	@OfficeFax varchar (50)  = null ,

	@CountryID int   = null ,

	@LanguageID int   = null ,

	@SubClientID int   = null ,

	@AdminClientPersonnelID int   = null ,

	@CurrencyID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientOfficeID]
	, [ClientID]
	, [OfficeName]
	, [BuildingName]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Country]
	, [PostCode]
	, [OfficeTelephone]
	, [OfficeTelephoneExtension]
	, [OfficeFax]
	, [CountryID]
	, [LanguageID]
	, [SubClientID]
	, [AdminClientPersonnelID]
	, [CurrencyID]
    FROM
	[dbo].[ClientOffices] WITH (NOLOCK) 
    WHERE 
	 ([ClientOfficeID] = @ClientOfficeID OR @ClientOfficeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([OfficeName] = @OfficeName OR @OfficeName IS NULL)
	AND ([BuildingName] = @BuildingName OR @BuildingName IS NULL)
	AND ([Address1] = @Address1 OR @Address1 IS NULL)
	AND ([Address2] = @Address2 OR @Address2 IS NULL)
	AND ([Town] = @Town OR @Town IS NULL)
	AND ([County] = @County OR @County IS NULL)
	AND ([Country] = @Country OR @Country IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
	AND ([OfficeTelephone] = @OfficeTelephone OR @OfficeTelephone IS NULL)
	AND ([OfficeTelephoneExtension] = @OfficeTelephoneExtension OR @OfficeTelephoneExtension IS NULL)
	AND ([OfficeFax] = @OfficeFax OR @OfficeFax IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([AdminClientPersonnelID] = @AdminClientPersonnelID OR @AdminClientPersonnelID IS NULL)
	AND ([CurrencyID] = @CurrencyID OR @CurrencyID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientOfficeID]
	, [ClientID]
	, [OfficeName]
	, [BuildingName]
	, [Address1]
	, [Address2]
	, [Town]
	, [County]
	, [Country]
	, [PostCode]
	, [OfficeTelephone]
	, [OfficeTelephoneExtension]
	, [OfficeFax]
	, [CountryID]
	, [LanguageID]
	, [SubClientID]
	, [AdminClientPersonnelID]
	, [CurrencyID]
    FROM
	[dbo].[ClientOffices] WITH (NOLOCK) 
    WHERE 
	 ([ClientOfficeID] = @ClientOfficeID AND @ClientOfficeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([OfficeName] = @OfficeName AND @OfficeName is not null)
	OR ([BuildingName] = @BuildingName AND @BuildingName is not null)
	OR ([Address1] = @Address1 AND @Address1 is not null)
	OR ([Address2] = @Address2 AND @Address2 is not null)
	OR ([Town] = @Town AND @Town is not null)
	OR ([County] = @County AND @County is not null)
	OR ([Country] = @Country AND @Country is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	OR ([OfficeTelephone] = @OfficeTelephone AND @OfficeTelephone is not null)
	OR ([OfficeTelephoneExtension] = @OfficeTelephoneExtension AND @OfficeTelephoneExtension is not null)
	OR ([OfficeFax] = @OfficeFax AND @OfficeFax is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([AdminClientPersonnelID] = @AdminClientPersonnelID AND @AdminClientPersonnelID is not null)
	OR ([CurrencyID] = @CurrencyID AND @CurrencyID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Find] TO [sp_executeall]
GO
