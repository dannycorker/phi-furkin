SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AutoAdjudicationFieldType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationFieldType_Delete]
(

	@AutoAdjudicationFieldTypeID int   
)
AS


				DELETE FROM [dbo].[AutoAdjudicationFieldType] WITH (ROWLOCK) 
				WHERE
					[AutoAdjudicationFieldTypeID] = @AutoAdjudicationFieldTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationFieldType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_Delete] TO [sp_executeall]
GO
