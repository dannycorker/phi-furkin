SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetTitle    Script Date: 08/09/2006 12:22:42 ******/

CREATE PROCEDURE [dbo].[GetTitle] @TitleID int
AS
SELECT Titles.TitleID, Titles.Title
FROM Titles
WHERE TitleID = @TitleID





GO
GRANT VIEW DEFINITION ON  [dbo].[GetTitle] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTitle] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTitle] TO [sp_executeall]
GO
