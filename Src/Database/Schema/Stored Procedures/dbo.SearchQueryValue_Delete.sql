SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SearchQueryValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQueryValue_Delete]
(

	@SearchQueryValueID int   
)
AS


				DELETE FROM [dbo].[SearchQueryValue] WITH (ROWLOCK) 
				WHERE
					[SearchQueryValueID] = @SearchQueryValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQueryValue_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQueryValue_Delete] TO [sp_executeall]
GO
