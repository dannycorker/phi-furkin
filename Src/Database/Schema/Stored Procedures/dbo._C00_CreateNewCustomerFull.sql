SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-08-29
-- Description:	Create a new customer and all the records below it
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateNewCustomerFull]
(
	@ClientID int,
	@LeadTypeID int,
	@WhoCreated int,
	@TitleID int ,
	@IsBusiness bit ,
	@FirstName varchar(100) ,
	@MiddleName varchar(100) ,
	@LastName varchar(100) ,
	@EmailAddress varchar(255) ,
	@DayTimeTelephoneNumber varchar(50) ,
	@DayTimeTelephoneNumberVerifiedAndValid bit ,
	@HomeTelephone varchar(50) ,
	@HomeTelephoneVerifiedAndValid bit ,
	@MobileTelephone varchar(50) ,
	@MobileTelephoneVerifiedAndValid bit ,
	@CompanyTelephone varchar(50) ,
	@CompanyTelephoneVerifiedAndValid bit ,
	@WorksTelephone varchar(50) ,
	@WorksTelephoneVerifiedAndValid bit ,
	@Address1 varchar(200) ,
	@Address2 varchar(200) ,
	@Town varchar(200) ,
	@County varchar(200) ,
	@PostCode varchar(50) ,
	@Website varchar(200) ,
	@HasDownloaded bit ,
	@DownloadedOn datetime ,
	@AquariumStatusID int ,
	@ClientStatusID int ,
	@Test bit ,
	@CompanyName varchar(100) ,
	@Occupation varchar(100) ,
	@Employer varchar(100) ,
	@PhoneNumbersVerifiedOn datetime ,
	@DoNotEmail bit ,
	@DoNotSellToThirdParty bit ,
	@AgreedToTermsAndConditions bit ,
	@DateOfBirth datetime ,
	@DefaultContactID int ,
	@DefaultOfficeID int ,
	@AddressVerified bit ,
	@CountryID int ,
	@SubClientID int ,
	@CustomerRef varchar(100) ,
	@WhoChanged int ,
	@WhenChanged datetime ,
	@ChangeSource varchar(200) ,
	@EmailAddressVerifiedAndValid bit ,
	@EmailAddressVerifiedOn datetime ,
	@Comments varchar(max) 
)

AS
BEGIN

	SET NOCOUNT ON;

	/* Instant fail if these are not present */
	IF @ClientID IS NULL OR @LeadTypeID IS NULL 
	BEGIN
		RETURN -1
	END

	DECLARE @NewCustomerID int, @NewLeadID int
	
	/* Create a complete new customer record */
	INSERT INTO dbo.Customers(
		ClientID,
		TitleID,
		IsBusiness,
		FirstName,
		MiddleName,
		LastName,
		EmailAddress,
		DayTimeTelephoneNumber,
		DayTimeTelephoneNumberVerifiedAndValid,
		HomeTelephone,
		HomeTelephoneVerifiedAndValid,
		MobileTelephone,
		MobileTelephoneVerifiedAndValid,
		CompanyTelephone,
		CompanyTelephoneVerifiedAndValid,
		WorksTelephone,
		WorksTelephoneVerifiedAndValid,
		Address1,
		Address2,
		Town,
		County,
		PostCode,
		Website,
		HasDownloaded,
		DownloadedOn,
		AquariumStatusID,
		ClientStatusID,
		Test,
		CompanyName,
		Occupation,
		Employer,
		PhoneNumbersVerifiedOn,
		DoNotEmail,
		DoNotSellToThirdParty,
		AgreedToTermsAndConditions,
		DateOfBirth,
		DefaultContactID,
		DefaultOfficeID,
		AddressVerified,
		CountryID,
		SubClientID,
		CustomerRef,
		WhoChanged,
		WhenChanged,
		ChangeSource,
		EmailAddressVerifiedAndValid,
		EmailAddressVerifiedOn,
		Comments
	)
	VALUES(
		@ClientID,
		@TitleID,
		@IsBusiness,
		@FirstName,
		@MiddleName,
		@LastName,
		@EmailAddress ,
		@DayTimeTelephoneNumber ,
		@DayTimeTelephoneNumberVerifiedAndValid ,
		@HomeTelephone ,
		@HomeTelephoneVerifiedAndValid ,
		@MobileTelephone ,
		@MobileTelephoneVerifiedAndValid ,
		@CompanyTelephone ,
		@CompanyTelephoneVerifiedAndValid ,
		@WorksTelephone ,
		@WorksTelephoneVerifiedAndValid ,
		@Address1 ,
		@Address2 ,
		@Town ,
		@County ,
		@PostCode ,
		@Website ,
		@HasDownloaded ,
		@DownloadedOn,
		@AquariumStatusID,
		@ClientStatusID,
		@Test ,
		@CompanyName ,
		@Occupation ,
		@Employer ,
		@PhoneNumbersVerifiedOn,
		@DoNotEmail ,
		@DoNotSellToThirdParty ,
		@AgreedToTermsAndConditions ,
		@DateOfBirth,
		@DefaultContactID,
		@DefaultOfficeID,
		@AddressVerified ,
		@CountryID,
		@SubClientID,
		@CustomerRef ,
		@WhoChanged,
		@WhenChanged,
		@ChangeSource ,
		@EmailAddressVerifiedAndValid ,
		@EmailAddressVerifiedOn,
		@Comments 
	)

	SELECT @NewCustomerID = SCOPE_IDENTITY()
	
	EXEC @NewLeadID = dbo._C00_CreateNewLeadForCust @NewCustomerID, @LeadTypeID, @ClientID, @WhoCreated
	
	/* Check that all the sub-records worked, else fail the whole job. */
	IF @NewLeadID > 0
	BEGIN
		RETURN @NewCustomerID
	END
	ELSE
	BEGIN
		RETURN -1
	END


END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewCustomerFull] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateNewCustomerFull] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewCustomerFull] TO [sp_executeall]
GO
