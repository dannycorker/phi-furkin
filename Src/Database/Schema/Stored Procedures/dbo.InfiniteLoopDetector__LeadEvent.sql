SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-06-25
-- Description:	Look for various infinite loops. LeadEvents in this case.
-- =============================================
CREATE PROCEDURE [dbo].[InfiniteLoopDetector__LeadEvent] 
	@CaseID INT, 
	@EventTypeID INT, 
	@TimeFrameInSeconds INT = 30,	/* By default, only look at the last 30 seconds. Use -1 for all time */ 
	@CreatedByUserID INT = NULL,	/* By default, it doesn't matter who added the previous one, but use this if it does matter */
	@IgnoreDeleted BIT = 1			/* By default, ignore deleted events when checking for duplicates. */
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @InfiniteLoopDetected BIT = 0
	
	/*
		Scenario 1 
		
		Do any LeadEvents of this type exist for this Case already, added within the last @TimeFrameInSeconds, by the nominated user if applicable.
	*/
	
	SELECT @InfiniteLoopDetected = CASE 
		WHEN EXISTS(SELECT * 
					FROM dbo.LeadEvent le WITH (NOLOCK) 
					WHERE le.CaseID = @CaseID 
					AND le.EventTypeID = @EventTypeID 
					AND (@TimeFrameInSeconds = -1 OR le.WhenCreated BETWEEN DATEADD(SECOND, -(@TimeFrameInSeconds), dbo.fn_GetDate_Local()) AND dbo.fn_GetDate_Local())
					AND (@CreatedByUserID IS NULL OR le.WhoCreated = @CreatedByUserID) 
					AND (@IgnoreDeleted = 1 OR le.EventDeleted = 0) 
					) 
		THEN 1 
		ELSE 0 
	END  
	
	/* Select the result back to the caller */
	SELECT @InfiniteLoopDetected AS [InfiniteLoopDetected]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[InfiniteLoopDetector__LeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[InfiniteLoopDetector__LeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[InfiniteLoopDetector__LeadEvent] TO [sp_executeall]
GO
