SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetDroppedOutCustomerQuestionnaire] @CustomerID int, @ClientQuestionnaireID int

AS

Select CustomerQuestionnaireID, ClientQuestionnaireID, CustomerID, SubmissionDate, TrackingID, ClientID 
From DroppedOutCustomerQuestionnaires
Where ((CustomerID = @CustomerID)  AND (ClientQuestionnaireID = @ClientQuestionnaireID) )



GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomerQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDroppedOutCustomerQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomerQuestionnaire] TO [sp_executeall]
GO
