SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the TableRows table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_Find]
(

	@SearchUsingOR bit   = null ,

	@TableRowID int   = null ,

	@ClientID int   = null ,

	@LeadID int   = null ,

	@MatterID int   = null ,

	@DetailFieldID int   = null ,

	@DetailFieldPageID int   = null ,

	@DenyEdit bit   = null ,

	@DenyDelete bit   = null ,

	@CustomerID int   = null ,

	@CaseID int   = null ,

	@ClientPersonnelID int   = null ,

	@ContactID int   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TableRowID]
	, [ClientID]
	, [LeadID]
	, [MatterID]
	, [DetailFieldID]
	, [DetailFieldPageID]
	, [DenyEdit]
	, [DenyDelete]
	, [CustomerID]
	, [CaseID]
	, [ClientPersonnelID]
	, [ContactID]
	, [SourceID]
    FROM
	[dbo].[TableRows] WITH (NOLOCK) 
    WHERE 
	 ([TableRowID] = @TableRowID OR @TableRowID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([DenyEdit] = @DenyEdit OR @DenyEdit IS NULL)
	AND ([DenyDelete] = @DenyDelete OR @DenyDelete IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ContactID] = @ContactID OR @ContactID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TableRowID]
	, [ClientID]
	, [LeadID]
	, [MatterID]
	, [DetailFieldID]
	, [DetailFieldPageID]
	, [DenyEdit]
	, [DenyDelete]
	, [CustomerID]
	, [CaseID]
	, [ClientPersonnelID]
	, [ContactID]
	, [SourceID]
    FROM
	[dbo].[TableRows] WITH (NOLOCK) 
    WHERE 
	 ([TableRowID] = @TableRowID AND @TableRowID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([DenyEdit] = @DenyEdit AND @DenyEdit is not null)
	OR ([DenyDelete] = @DenyDelete AND @DenyDelete is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ContactID] = @ContactID AND @ContactID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_Find] TO [sp_executeall]
GO
