SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Dashboard table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_Delete]
(

	@DashboardID int   
)
AS


				DELETE FROM [dbo].[Dashboard] WITH (ROWLOCK) 
				WHERE
					[DashboardID] = @DashboardID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_Delete] TO [sp_executeall]
GO
