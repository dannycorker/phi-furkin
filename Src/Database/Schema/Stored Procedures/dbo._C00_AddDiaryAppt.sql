SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AddDiaryAppt]
(
@CaseID int,
@ClientPersonnelID int,
@StartDate datetime,
@EndDate datetime = NULL,
@NoteTitle varchar(250),
@NoteText varchar(2000),
@AllDayEvent bit = true
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare @LeadID int,
			@CustomerID int,
			@ClientID int,
			@DiaryAppointmentID int

	Select @LeadID = Cases.LeadID, @ClientID = Cases.ClientID, @CustomerID = Lead.CustomerID
	From Cases
	Inner Join Lead on Lead.LeadID = Cases.LeadID
	Where Cases.CaseID = @CaseID

	INSERT INTO [dbo].[DiaryAppointment]([ClientID],[DiaryAppointmentTitle],[DiaryAppointmentText],[CreatedBy],[DueDate],[EndDate],[Completed],[LeadID],[CaseID],[CustomerID],[Version],[ExportVersion],[RecurrenceInfo],[DiaryAppointmentEventType],[ResourceInfo],[TempReminderTimeshiftID],[StatusID],[LabelID], AllDayEvent)
	VALUES(@ClientID,@NoteTitle,@NoteText,@ClientPersonnelID,@StartDate,@EndDate,0,@LeadID,@CaseID,@CustomerID,1,0,NULL,NULL,NULL,NULL,NULL,NULL, @AllDayEvent)

	Select @DiaryAppointmentID = SCOPE_IDENTITY() 

	INSERT INTO [dbo].[DiaryReminder]([ClientID],[DiaryAppointmentID],[ClientPersonnelID],[ReminderTimeshiftID],[ReminderDate])
    VALUES(@ClientID,@DiaryAppointmentID,@ClientPersonnelID,2,@StartDate)

	Return @DiaryAppointmentID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddDiaryAppt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AddDiaryAppt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddDiaryAppt] TO [sp_executeall]
GO
