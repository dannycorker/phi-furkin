SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FieldTarget_Delete]
(

	@FieldTargetID int   
)
AS


				DELETE FROM [dbo].[FieldTarget] WITH (ROWLOCK) 
				WHERE
					[FieldTargetID] = @FieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget_Delete] TO [sp_executeall]
GO
