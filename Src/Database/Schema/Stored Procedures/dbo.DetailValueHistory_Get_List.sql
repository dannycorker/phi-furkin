SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DetailValueHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_Get_List]

AS


				
				SELECT
					[DetailValueHistoryID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[LeadID],
					[MatterID],
					[FieldValue],
					[WhenSaved],
					[ClientPersonnelID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ContactID],
					[ClientPersonnelDetailValueID]
				FROM
					[dbo].[DetailValueHistory] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Get_List] TO [sp_executeall]
GO
