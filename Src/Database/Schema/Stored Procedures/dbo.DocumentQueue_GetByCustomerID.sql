SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentQueue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentQueue_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentQueueID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[DocumentTypeID],
					[WhoCreated],
					[WhenStored],
					[WhenCreated],
					[RequiresApproval],
					[ParsedDocumentTitle],
					[BasedUponLeadEventID]
				FROM
					[dbo].[DocumentQueue] WITH (NOLOCK) 
				WHERE
					[CustomerID] = @CustomerID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_GetByCustomerID] TO [sp_executeall]
GO
