SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack (Copy of RulesEngine_RuleOutputs_GetOutputsByRuleID)
-- Create date: 2016-08-08
-- Description:	Returns rule output data for a specific rule 
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleOutputs_GetFilteredOutputsByRuleID]
(
	@ClientID INT,
	@RuleID INT,
	@FilterParams dbo.tvpIntVarcharVarchar READONLY
)


AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	p.RuleParameterID, p.Name, p.ParameterTypeID, p.Value, p.DataTypeID
	FROM	RulesEngine_RuleParameters p WITH (NOLOCK)
	WHERE	p.ClientID = @ClientID
	AND		p.RuleID = @RuleID

		;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON df.DetailFieldID = p.Value
		WHERE	p.RuleID = @RuleID
		AND		df.ClientID = @ClientID
		AND		p.DataTypeID = 6
	)
		
	SELECT  po.ParameterOptionID, po.OperatorID, op.Name AS Operator, po.RuleParameterID, po.Val1, po.Val2,
			CASE WHEN ll.LookupListItemID IS NOT NULL THEN
					REPLACE(op.OperatorText, '{0}', ll.ItemValue)
			ELSE	REPLACE(REPLACE(op.OperatorText, '{0}', po.Val1), '{1}', ISNULL(po.Val2, ''))
			END AS OperatorText
	FROM dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON po.OperatorID = op.OperatorID
	INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON po.RuleParameterID = p.RuleParameterID
	LEFT JOIN LookupList ll ON ll.LookupListItemID = CASE WHEN p.DataTypeID = 6 THEN po.Val1 ELSE NULL END
	INNER JOIN @FilterParams fv ON po.RuleParameterID = fv.AnyID 
		AND COALESCE(ll.ItemValue, po.Val1,'') LIKE ISNULL(fv.AnyValue1, '') + '%'
		--AND ISNULL(po.Val2,'') LIKE ISNULL(fv.AnyValue2, '')  + '%'
	WHERE	po.ClientID = @ClientID
	AND		p.RuleID = @RuleID
	ORDER BY po.OptionOrder

	;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON df.DetailFieldID = p.Value
		WHERE	p.RuleID = @RuleID
		AND		df.ClientID = @ClientID
		AND		p.DataTypeID = 6
	)
	SELECT DISTINCT o.RuleID, o.RuleOutputID, o.TransformID, o.Value 
	FROM dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_OutputCoordinates c ON o.RuleOutputID = c.RuleOutputID
	INNER JOIN dbo.RulesEngine_ParameterOptions po ON po.ParameterOptionID = c.ParameterOptionID
	INNER JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = po.RuleParameterID
	LEFT JOIN LookupList ll ON ll.LookupListItemID = CASE WHEN rp.DataTypeID = 6 THEN po.Val1 ELSE NULL END
	INNER JOIN @FilterParams fv ON po.RuleParameterID = fv.AnyID 
		AND COALESCE(ll.ItemValue, po.Val1,'') LIKE ISNULL(fv.AnyValue1, '') + '%'
		--AND ISNULL(po.Val2,'') LIKE ISNULL(fv.AnyValue2, '')  + '%'
	WHERE o.RuleID = @RuleID
	
	
	;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON df.DetailFieldID = p.Value
		WHERE	p.RuleID = @RuleID
		AND		df.ClientID = @ClientID
		AND		p.DataTypeID = 6
	)
	SELECT DISTINCT c.OutputCoordinateID, c.RuleOutputID, c.ParameterOptionID
	FROM dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_OutputCoordinates c ON o.RuleOutputID = c.RuleOutputID
	INNER JOIN dbo.RulesEngine_ParameterOptions po ON po.ParameterOptionID = c.ParameterOptionID
	INNER JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = po.RuleParameterID
	LEFT JOIN LookupList ll ON ll.LookupListItemID = CASE WHEN rp.DataTypeID = 6 THEN po.Val1 ELSE NULL END
	INNER JOIN @FilterParams fv ON po.RuleParameterID = fv.AnyID 
		AND COALESCE(ll.ItemValue, po.Val1,'') LIKE ISNULL(fv.AnyValue1, '') + '%'
		--AND ISNULL(po.Val2,'') LIKE ISNULL(fv.AnyValue2, '')  + '%'
	WHERE o.RuleID = @RuleID
	
	SELECT * 
	FROM dbo.RulesEngine_Transforms WITH (NOLOCK) 

	SELECT 'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord
	UNION ALL
	SELECT s.Name AS Title, '#/ruleset/' + CAST(r.RuleSetID AS VARCHAR) + '/rules' AS Path, 2 AS Ord
	FROM dbo.RulesEngine_RuleSets s WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleSetID = s.RuleSetID
	WHERE r.RuleID = @RuleID
	UNION ALL
	SELECT r.Name AS Title, '#/rule/' + CAST(r.RuleID AS VARCHAR) AS Path, 3 AS Ord
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	WHERE r.RuleID = @RuleID
	UNION ALL
	SELECT 'Values in Grid' AS Title, '#/rule/' + CAST(r.RuleID AS VARCHAR) + '/outputs_grid' AS Path, 4 AS Ord
	FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
	WHERE r.RuleID = @RuleID
	ORDER BY Ord
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_GetFilteredOutputsByRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleOutputs_GetFilteredOutputsByRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_GetFilteredOutputsByRuleID] TO [sp_executeall]
GO
