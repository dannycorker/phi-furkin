SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2018-12-04
-- Description:	Get Quote Values with Evaluated XML
-- CPS 2019-03-22 For OMF Cases, queue an event to identify that the agent has retrieved the quote
-- CPS 2019-03-27 brought QuotePetProduct record updates over from GetQuoteValues
-- CPS 2019-03-28 included BrandID in QuotePetProduct save
-- CPS 2019-07-25 for JIRA BAULAG-64 | If there is no match claimed, check the DB for this customer and apply MultiPet if we find a probable match
-- 2019-10-11 GPR removed redundent code blocks (previously commented out and not used in C60X build) for LPC-35
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-05-04 AAD extract HasMicrochip from XML. Insert HasMicrochip and SignUpMethod into QuotePet
-- GPR 2020-07-24 Added Options for Excess and CoPay
-- GPR 2020-07-27 Added UW Questions for 605 (PPET-178)
-- SB  2020-08-10 Update to allow for multiple product IDs to be specified for evaluation
-- GPR 2020-10-15 Added more UW to trigger discounts and surcharges for 605 (SDPRU-77)
-- GPR 2020-10-11 Added 'Reduction' for SDPRU-111
-- ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_GetQuoteValues_Evaluated]
(
	 @XmlRequest	XML
	,@PetQuote		XML = NULL
	,@ProductID		INT = NULL
	,@EvaluatedXml	XML = NULL
	,@XmlOut		XML = NULL OUTPUT
	,@Debug			BIT = 0
)
AS
BEGIN

	/*GPR 2020-07-28*/
	EXEC _C00_LogItXML 605, 0, '_C600_PA_Policy_GetQuoteValues Request', @XmlRequest
	EXEC _C00_LogItXML 605, 0, '_C600_PA_Policy_GetQuoteValues EvaluatedXml', @EvaluatedXml

	DECLARE  @InvalidXml VARCHAR(MAX)	= CONVERT(VARCHAR(MAX),@PetQuote)
			,@ContextID	 INT			= ISNULL(@ProductID,-1)

	DECLARE @ClientPersonnelID INT 
	SELECT @ClientPersonnelID = ISNULL(NULLIF(@XmlRequest.value('(//ClientPersonnelId)[1]','INT'),0),dbo.fn_C600_GetAqAutomationUser())

	DECLARE @ClientID INT
	SELECT	@ClientID = ISNULL(@XmlRequest.value('(//ClientId)[1]', 'INT'),dbo.fnGetPrimaryClientID())
	
	DECLARE @XmlResponse XML,
			@Result INT = 1 -- forget validation for now
		
	DECLARE @Overrides dbo.tvpIntVarcharVarchar
	
	DECLARE @Now DATETIME = dbo.fn_GetDate_Local()
	
	IF @Result = 1
	BEGIN
			
		DECLARE  @QuoteSessionID		 INT
				,@BrandID				 INT
				,@Postcode				 VARCHAR(50)
				,@Address1				 VARCHAR(200)
				,@County				 VARCHAR(200)
				,@FirstName				 VARCHAR(100)
				,@LastName				 VARCHAR(100)
				,@Email					 VARCHAR(255)
				,@HomePhone				 VARCHAR(50) 
				,@Mobile				 VARCHAR(50)
				,@TrackingCode			 VARCHAR(50)
				,@SignUpMethod			 INT
				,@LastPeriodExitPremium	 MONEY = 0
				,@LossRationPercentage	 MONEY = 0
				,@NumberPreviousClaims	 INT = 0
				,@PaymentFrequency		 VARCHAR(20)
				,@PolicyYearNumber		 INT = 1
				,@CustomerID			 INT
				,@IPT					 MONEY
				,@ContactMethodID		 INT
				,@AdminFee				 MONEY
				,@CoInsuranceNextYear	 BIT = 0
				,@Aggressive			 BIT = 0
				,@DiscountedMonths		 INT = 0 
				,@PostCodeGroup			 VARCHAR(10)
				,@PremiumCalculationID   INT
				,@SourceName			 VARCHAR(2000)
				,@ErrorCount             INT
				,@SourceLookupListItemID INT
				,@AggregatorID			 VARCHAR(100)
				,@IsWorkingPet			 BIT = 0
				,@IsVeteran				 BIT = 0
				,@IsVetOrStaff			 BIT = 0
				,@IsRescue				 BIT = 0
				,@IsMedical				 BIT = 0
				,@IsWellness			 BIT = 0
				,@IsInternetPartner			BIT = 0
				,@IsStrategicPartner		BIT = 0
				,@IsCorporateGroupBenefit	BIT = 0
				,@IsBroker					BIT = 0
				,@ProvinceLookupListItemID	INT
				,@Postcode1 VARCHAR(1)
				,@Postcode3 VARCHAR(3)
				,@Postcode6 VARCHAR(6)
				,@StateID INT
				,@StateCode VARCHAR(2)
		
		DECLARE  @ErrorMessages			TABLE ( PetRef VARCHAR(2000), ProductId INT, ErrorMessage VARCHAR(2000) )
		DECLARE	 @KeyValues				TABLE ( KeyValueID INT Identity, [Key] VARCHAR(2000), [Value] VARCHAR(2000) )
				
		INSERT	 @KeyValues ([Key], [Value])
		SELECT	 b.value('(Key)[1]','VARCHAR(2000)')
				,b.value('(Value)[1]','VARCHAR(2000)')
		FROM @XmlRequest.nodes('(//KeyValues/*)') a(b)

		SELECT	 @QuoteSessionID	= @XmlRequest.value('(//SessionId)[1]'	 , 'INT')
				,@BrandID			= @XmlRequest.value('(//BrandingId)[1]'	 , 'INT')
				,@Postcode			= @XmlRequest.value('(//Postcode)[1]'	 , 'VARCHAR(50)' )
				,@Address1			= @XmlRequest.value('(//Address1)[1]'	 , 'VARCHAR(50)' ) -- 2019-07-25 CPS for JIRA BAULAG-64 | Pick up Address 1 to be used in soft customer matching
				,@County			= @XmlRequest.value('(//County)[1]'		 , 'VARCHAR(200)')
				,@FirstName 		= @XmlRequest.value('(//FirstName)[1]'	 , 'VARCHAR(100)')
				,@LastName			= @XmlRequest.value('(//LastName)[1]'	 , 'VARCHAR(100)')
				,@Email				= @XmlRequest.value('(//Email)[1]'		 , 'VARCHAR(255)')
				,@HomePhone 		= @XmlRequest.value('(//HomePhone)[1]'	 , 'VARCHAR(50)' )
				,@Mobile			= @XmlRequest.value('(//MobilePhone)[1]' , 'VARCHAR(50)' )
				,@TrackingCode 		= @XmlRequest.value('(//TrackingCode)[1]', 'VARCHAR(50)' )
				,@SignUpMethod 		= ISNULL( ( SELECT li.LookupListItemID FROM LookupListItems li WITH ( NOLOCK ) WHERE li.LookupListID = 6126 AND li.ItemValue = @XmlRequest.value('(//SignUpMethod)[1]', 'varchar(50)') ),74326) /*Direct*/ /*CPS 2017-05-29*/
				,@PaymentFrequency	= 'MO' -- default to monthly, payment frequency is only used for renewals
				,@CustomerID		= @XmlRequest.value('(//CustomerId)[1]'	, 'INT')
				,@ContactMethodID	= @XmlRequest.value('(//ContactMethodId)[1]', 'INT')
				,@AdminFee			= CASE WHEN @XmlRequest.value('(//ContactMethodId)[1]', 'INT') = 60788 /*Paper*/ THEN dbo.fn_C600_GetAdminFee(0) ELSE 0.00 END /*C600 PM07/A020*/
				,@AggregatorID		= @XmlRequest.value('(//AggregatorID)[1]', 'VARCHAR(100)')
				
		/*GPR 2018-07-19 adopted from 433*/
		SELECT @SourceLookupListItemID = fn.LookupListItemID
		FROM dbo.fn_C600_GetDetailsFromTrackingCode(@TrackingCode) fn

		
		/*CPS 2019-03-26 until Aaran can feed the key values back into the quote request*/
		IF EXISTS ( SELECT *
		            FROM _C600_SavedQuote sq WITH ( NOLOCK ) 
					WHERE sq.QuoteSessionID = @QuoteSessionID
					AND sq.SavedQuoteXml.value('(//KeyValueData/Key)[1]','VARCHAR(5)') = 'IsOmf'
				  )
		BEGIN
			INSERT @KeyValues ([Key],[Value])
			VALUES ( 'IsOmf','true' )
		END
		
		/*GPR 2018-11-28 #1239 Get Channel From Source Resource List ID - this is based on TrackingCode*/
		DECLARE @Channel INT, @SourceResourceListID INT		
		
		SELECT @SourceResourceListID = fn.ResourceListID
		FROM dbo.fn_C600_GetDetailsFromTrackingCode(@TrackingCode) fn		
		
		SELECT @Channel = rldv.ValueInt FROM ResourceListDetailValues rldv WITH ( NOLOCK ) WHERE rldv.ResourceListID = @SourceResourceListID AND rldv.DetailFieldID = 180102 /*Channel*/

		IF @Channel = 76444 /*Aggregator*/
		BEGIN
			SELECT @SignUpMethod = 74564 /*Aggregator*/
		END
		
		/*AHOD 2018-11-30 AHOD Added to effect Product ordering in _C600_PA_Policy_GetProducts*/
		SELECT @AggregatorID = ISNULL(@AggregatorID,'')
		IF @AggregatorID <>''
		BEGIN
			SELECT @SignUpMethod = 74564 /*Aggregator*/
		END

		DELETE @Overrides
		INSERT @Overrides (AnyID, AnyValue1, AnyValue2) VALUES
		(2, 'Postcode', @Postcode)
		SELECT @IPT=dbo.fn_C00_1273_RulesEngine_GetIPT (NULL, NULL, NULL, NULL, @Overrides)
		DELETE @Overrides 
				
		DECLARE @QuoteValueIDs TABLE (QuoteValueID INT)
				
		IF @QuoteSessionID > 0
		BEGIN
		
			UPDATE dbo._C600_QuoteSessions
			SET FirstName = @FirstName,
				LastName = @LastName,
				Email = @Email,
				PostCode = @Postcode,
				HomeTel = @HomePhone,
				MobileTel = @Mobile,
				QuoteCount = QuoteCount + 1,
				LastQuoteStart = dbo.fn_GetDate_Local()
			WHERE QuoteSessionID = @QuoteSessionID
		
		END
		ELSE
		BEGIN
		
			INSERT dbo._C600_QuoteSessions(BrandID, FirstName, LastName, Email, PostCode, HomeTel, MobileTel, QuoteStart, QuoteCount, TrackingCode, CustomerID, LastQuoteStart)
			VALUES (@BrandID, @FirstName, @LastName, @Email, @Postcode, @HomePhone, @Mobile, @Now, 1, @TrackingCode, @CustomerID, @Now)
			
			SELECT @QuoteSessionID = SCOPE_IDENTITY()
		
		END
				
			
		DECLARE @SchemeLeadTypeID INT
		SELECT @SchemeLeadTypeID = s.SharedTo
		FROM dbo.LeadTypeShare s WITH (NOLOCK) 
		WHERE s.SharedFrom = 1275
		AND s.ClientID = @ClientID

		DECLARE @VetFeesID INT
		SELECT @VetFeesID = dbo.fn_C00_1272_GetVetFeesResourceList(@ClientID)

		DECLARE @PetNumber INT, @ExistingCust BIT
		
		SELECT	@PetNumber= @XmlRequest.value('(//OtherPetsInsured)[1]', 'INT'),
				@ExistingCust= @XmlRequest.value('(//ExistingPolicyHolder)[1]', 'BIT')
				
		DECLARE @Pets TABLE 
		(
			ID INT IDENTITY,
			PetXml XML
		)
		
		IF @PetQuote IS NOT NULL
		BEGIN
			INSERT @Pets (PetXml)
			VALUES (@PetQuote)
		END
		ELSE
		BEGIN
			INSERT @Pets (PetXml)
			SELECT r.c.query('.')
			FROM @XmlRequest.nodes('//PetQuote') r(c)
		END


		DECLARE @Policies TABLE
		(
			ID INT IDENTITY,
			GroupedID INT,
			PetID INT,
			PolicyMatterID INT,
			PolicyLeadID INT,
			PolicySchemeID INT,
			ProductName VARCHAR(2000),
			AnnualPremium MONEY,
			Discount MONEY,
			PremiumLessDiscount MONEY,
			FirstMonthly MONEY,
			RecurringMonthly MONEY,
			--PolicyTypeID INT, -- 42931 = 12 month, 42932 = max ben, 42933 = reinstatement
			PolicyType VARCHAR(2000),
			PolicyDocUrl VARCHAR(2000),
			InfoDocUrl VARCHAR(2000),
			Brand VARCHAR(100),
			ItemName VARCHAR(100),
			AnnualTaxLevel1 MONEY,
			AnnualTaxLevel2 MONEY,
			FirstMonthlyTaxLevel1 MONEY,
			FirstMonthlyTaxLevel2 MONEY,
			RecurringMonthlyTaxLevel1 MONEY,
			RecurringMonthlyTaxLevel2 MONEY,
			Reduction MONEY,
			DiscountRuleOutput DECIMAL(18,2)
		)

		DECLARE @Limits TABLE
		(
			 ID					INT IDENTITY
			,PetID				INT
			,PolicyID			INT
			,Amount				MONEY
			,Options			VARCHAR(2000)
			,ResourceListID		INT
			,Section			VARCHAR(2000)
			,ConditionLimit		MONEY
			,TimedLimit			VARCHAR(2000)
			,LimitNarrative		VARCHAR(2000)
		)

		DECLARE @Excess TABLE
		(
			ID INT IDENTITY,
			PetID INT,
			PolicyID INT,
			Amount MONEY,
			IsPercentage BIT,
			Options VARCHAR(2000),
			ResourceListID INT,
			Section VARCHAR(2000),
			ParentResourceListID INT
		)

		DECLARE @CoInsurance TABLE
		(
			ID INT IDENTITY,
			PetID INT,
			PolicyID INT,
			Amount MONEY,
			IsPercentage BIT,
			Options VARCHAR(2000)
		)

		DECLARE @VolExcess TABLE
		(
			ID INT IDENTITY,
			PetID INT,
			PolicyID INT,
			Amount MONEY,
			Selected BIT
		)

		DECLARE @Discounts TABLE
		(
			ID INT IDENTITY,
			PetID INT,
			PolicyID INT,
			DiscountRLID INT,
			Name VARCHAR(2000),
			Description VARCHAR(2000),
			Value MONEY,
			IsPercentage BIT,
			Amount MONEY
		)
		
		DECLARE @Premiums TABLE  
		(  
			TermsDate DATE,  
			SchemeID INT,  
			PolicyMatterID INT,  
			RuleSetID INT,  
			AnnualPremium MONEY,  
			Discount MONEY,  
			PremiumLessDiscount MONEY,  
			FirstMonthly MONEY,  
			RecurringMonthly MONEY,  
			Net MONEY,
			Commission MONEY,
			GrossNet MONEY,
			DiscountGrossNet MONEY,
			PAFIfBeforeIPT MONEY,
			PAFBeforeIPTGrossNet MONEY,
			IPT MONEY,
			IPTGrossNet MONEY,
			PAFIfAfterIPT MONEY,
			GrossGross MONEY,
			PremiumCalculationID INT,
			AnnualTaxLevel1 MONEY,
			AnnualTaxLevel2 MONEY,
			FirstMonthlyTaxLevel1 MONEY,
			FirstMonthlyTaxLevel2 MONEY,
			RecurringMonthlyTaxLevel1 MONEY,
			RecurringMonthlyTaxLevel2 MONEY,
			Reduction DECIMAL(18,2),
			DiscountRuleOutput DECIMAL(18,2)
		)
		
		DECLARE @Terms TABLE
		(
			ID INT IDENTITY,
			PetID INT,
			PolicyID INT,
			Description VARCHAR(2000),
			Value VARCHAR(2000),
			SortKey VARCHAR(2000)
		)
		
		DECLARE @OptionalCoverages TABLE
		(
			ID INT IDENTITY,
			PetID INT,
			PolicyID INT,
			GroupID INT,
			GroupText VARCHAR(2000),
			GroupDetail VARCHAR(2000),
			CheckpointKey VARCHAR(2000),
			Cost MONEY,
			Selected BIT
		)

		/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
		DECLARE @EnrollmentFeeTable TABLE 
		(
			PetID INT,
			FeeTableRowID INT,
			EnrollmentFee NUMERIC(18,2),
			TaxTableRowID INT,
			Tax NUMERIC(18,2),
			Total NUMERIC(18,2)
		)

		-- LOOP - For each pet
		DECLARE @PetCount INT,
				@PetIndex INT = 0
		SELECT @PetCount = COUNT(*)
		FROM @Pets
		
		IF @PetNumber= 0 AND @ExistingCust=1     /*GPR 2018-02-20 - uncommented 2018-02-23*/
		SELECT @PetNumber = 1	
		
		DECLARE @IsMultipet INT
		SELECT @IsMultipet = 0
		
		IF @ExistingCust = 1 OR @PetCount > 1 OR 
				(
				@CustomerID > 0 /*CPS 2017-06-02*/
				and exists ( SELECT * 
				             FROM Matter m WITH ( NOLOCK ) 
				             INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170038 /*Policy Status*/
				             INNER JOIN MatterDetailValues mdv2 WITH ( NOLOCK ) ON mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = 170034 /*Current Policy*/
				             WHERE m.CustomerID = @CustomerID
				             AND mdv.ValueInt IN ( 43002 /*Live*/, 74573 /*With Underwriting*/ )
				             --AND m.MatterRef <> 'One Month Free' /*GPR 2018-03-22 Added to avoid OMF conversion from gaining Multipet on first policy on Customer lookup -- C600-B specific*/
				             AND mdv2.ValueInt <> 151577 /*GPR 2018-03-23 OMF*/
				           )
				)
		SELECT @IsMultipet = 1	
		
		-- 2019-07-25 CPS for JIRA BAULAG-64 | If there is no match claimed, check the DB for this customer and apply MultiPet if we find a probable match
		IF @IsMultipet = 0 AND (@CustomerID = 0 OR @CustomerID is NULL)
		BEGIN
			SELECT TOP (1)
					 @CustomerID = cu.CustomerID
					,@IsMultipet = 1
			FROM dbo.fn_C600_GetPossibleCustomerMatchesForMultipet(@BrandID,@ClientID,@Postcode,@LastName,@FirstName,@Address1,@XmlRequest, '') cu

			DECLARE @TempLog VARCHAR(2000) 
			SELECT @TempLog = ISNULL(CONVERT(VARCHAR,@BrandID),'NULL') + ',' + ISNULL(CONVERT(VARCHAR,@ClientID),'NULL') + ',' + ISNULL(@Postcode,'NULL') + ',' + ISNULL(@LastName,'NULL') + ',' + ISNULL(@FirstName,'NULL') + ',' + ISNULL(@Address1,'NULL') + ' | @CustomerID = ' + ISNULL(CONVERT(VARCHAR,@CustomerID),'NULL')
		
			EXEC _C00_LogIt 'Info', '_C600_PA_Policy_GetQuoteValues_Evaluated', 'Probable customer match search', @TempLog, 58552 /*Aquarium Automation*/
		END

		DECLARE	 @QuoteID		INT
				,@QuotePetID	INT

		/*CPS 2019-03-26 brought over from GetQuoteValues*/
		/*GPR 2019-03-05 INSERT into Quote for C600 #55470 - Quote_SaveQuote */  
		INSERT INTO dbo.Quote (QuoteSessionID)
		SELECT @QuoteSessionID
		SELECT @QuoteID = SCOPE_IDENTITY()

		WHILE @PetIndex < @PetCount
		BEGIN
		
			SELECT @PetIndex += 1
			SELECT @PetNumber += 1  -- GPR 2018-02-20

			DECLARE @PetXml XML = NULL
			SELECT @PetXml = PetXml
			FROM @Pets
			WHERE ID = @PetIndex
			
			DECLARE @PetRef VARCHAR(2000) = ''
            SELECT @PetRef = @PetXml.value('(//PetRef)[1]', 'INT')
			
			SELECT @PetNumber = @PetXML.value('(//PetRef)[1]', 'INT') + 1 -- CPS 2018-02-20 for GPR

			IF @CustomerID > 0 /* GPR 2018-03-02 for C600 DefectID 128 - exisiting customer multipet */
			AND EXISTS ( SELECT * 
				FROM Matter m WITH ( NOLOCK ) 
				INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 170038 /*Policy Status*/
				INNER JOIN MatterDetailValues mdv2 WITH ( NOLOCK ) ON mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = 170034 /*Current Policy*/
				WHERE m.CustomerID = @CustomerID
				AND mdv.ValueInt IN ( 43002 /* Live/Active */, 74573 /* With Underwriting */ )
				--AND m.MatterRef <> 'One Month Free' /*GPR 2018-03-22 Added to avoid OMF conversion from gaining Multipet on first policy on Customer lookup -- C600-B specific*/
				AND mdv2.ValueInt <> 151577 /*GPR 2018-03-23 OMF*/
				)
			BEGIN
			SELECT @PetNumber += 1
			END

			DECLARE @SpeciesID INT = NULL
			SELECT @SpeciesID = @PetXml.value('(//SpeciesId)[1]', 'INT')
			
			DECLARE @PetName VARCHAR(100) = NULL
			SELECT @PetName = @PetXml.value('(//PetName)[1]', 'VARCHAR(100)')

		
			/*
			CPS 2019-03-22 if the agent is retrieving an OMF quote, update the case to identify this.
			At time of writing this will be Aquarium Automation, until dev sort out including ClientPersonnelId in the XML request
			*/
			IF EXISTS ( SELECT * FROM @KeyValues kv WHERE kv.[Key] = 'IsOmf' AND kv.[Value] = 'true' )
			BEGIN
				INSERT AutomatedEventQueue ( ClientID, CustomerID, LeadID, CaseID, FromLeadEventID, FromEventTypeID, WhenCreated, WhoCreated, AutomatedEventTypeID, RunAsUserID, ThreadToFollowUp, InternalPriority, BumpCount, ErrorCount, CommentsToApply )
				SELECT m.ClientID, m.CustomerID, m.LeadID, m.CaseID, c.ProcessStartLeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @ClientPersonnelID, 158906 /*OMF Quote Retrieved*/, @ClientPersonnelID, -1, 5, 0, 0, 'Applied by GetQuoteValues'
				FROM @Pets p 
				INNER JOIN dbo.Lead l WITH ( NOLOCK ) on l.CustomerID = @CustomerID
				INNER JOIN dbo.LeadDetailValues ldv WITH ( NOLOCK ) on ldv.LeadID = l.LeadID AND ldv.DetailValue = @PetName AND ldv.DetailFieldID = 144268 /*Pet Name*/
				INNER JOIN dbo.Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
				INNER JOIN dbo.Matter m WITH ( NOLOCK ) on m.CaseID = c.CaseID
				INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadEventID = c.ProcessStartLeadEventID
				WHERE EXISTS ( SELECT * FROM EventType et WITH ( NOLOCK ) WHERE et.EventTypeID = 158906 /*OMF Quote Retrieved*/ ) -- In case the code gets promoted before the event type!
				AND l.CustomerID = @CustomerID
			END
			
			DECLARE @BreedID INT = NULL
			SELECT @BreedID = @PetXml.value('(//BreedId)[1]', 'INT')
			
			DECLARE @Breed VARCHAR(2000)
			SELECT @Breed = DetailValue
			FROM dbo.ResourceListDetailValues WITH (NOLOCK) 
			WHERE ResourceListID = @BreedID 
			AND DetailFieldID = 144270 /*Pet Breed*/

			DECLARE @Species VARCHAR(2000)
			SELECT @Species = ItemValue
			FROM dbo.LookupListItems WITH (NOLOCK) 
			WHERE LookupListItemID = @SpeciesID 
			
			DECLARE @MicrochipNumber VARCHAR(2000)
			SELECT @MicrochipNumber =@PetXML.value('(//PetInfo/MicrochipNo)[1]', 'varchar(2000)')
			
			DECLARE @Gender CHAR(1),
					@GenderID INT
			SELECT @Gender = @PetXml.value('(//Gender)[1]', 'CHAR(1)')
			SELECT @GenderID =	CASE @Gender
									WHEN 'M' THEN 5168
									WHEN 'F' THEN 5169
								END
			
			DECLARE @IsNeutered BIT = NULL
			SELECT @IsNeutered = @PetXml.value('(//IsNeutered)[1]', 'BIT')

			/* 2020-05-04 AAD - Pick up microchip information for use in QuotePet */
			DECLARE @HasMicrochip BIT = NULL
			SELECT @HasMicrochip = @PetXml.value('(//HasMicrochip)[1]', 'BIT')
						
			DECLARE @StartDate DATE = NULL,
					@TermsDate DATE = NULL
			SELECT @StartDate = @PetXml.value('(//StartDate)[1]', 'DATE'),
					@TermsDate = @StartDate -- TODO... get terms date from quote when we allow save and retrieve quote
			
			IF @StartDate IS NULL
			BEGIN
				SELECT @StartDate = DATEADD(DAY, 1, dbo.fn_GetDate_Local())	
			END
			
			DECLARE @BirthDate DATE = NULL
			SELECT	@BirthDate = @PetXml.value('(//BirthDate)[1]', 'DATE')
			
			DECLARE @PurchasePrice MONEY = 0
			SELECT @PurchasePrice = @PetXml.value('(//PurchasePrice)[1]', 'MONEY')
			
			DECLARE @DiscountCode VARCHAR(100)
			SELECT @DiscountCode = @XmlRequest.value('(//DiscountCodes/string)[1]', 'VARCHAR(100)')
			--SELECT @DiscountCode = @TrackingCode /*CPS 2017-12-15 Proof of Concept for GPR*/
			
			-- Find matching policies for this brand (we currently don't limit on anything like species or area) 
			INSERT @Policies (PetID, PolicyMatterID, PolicyLeadID, PolicySchemeID, ProductName, PolicyType, PolicyDocUrl, InfoDocUrl, Brand, ItemName)
			EXEC dbo._C600_PA_Policy_GetProducts @PetIndex, @XmlRequest, @PetXml, @ProductID, @SignUpMethod

			-- If we don't have a single product passed in then check for an array of products and values within the pet node
			DECLARE @PetProducts TABLE (SchemeID INT)
			DECLARE @PetProductCount INT = 0

			IF @ProductID IS NULL or @ProductID = 0
			BEGIN
				-- Init table
				DELETE @PetProducts

				INSERT @PetProducts
				SELECT r.c.value('.', 'INT')
				FROM @PetXml.nodes('//ProductId') r(c)

				SELECT @PetProductCount = COUNT(*) FROM @PetProducts
			END

			IF @PetProductCount > 0
			BEGIN
				DELETE @Policies
				WHERE PetID = @PetIndex
				AND PolicySchemeID NOT IN (SELECT SchemeID FROM @PetProducts)
			END

			;WITH OrderedPolicies AS 
			(
				SELECT *, 
				ROW_NUMBER() OVER(PARTITION BY PetID ORDER BY ID) as rn 
				FROM @Policies
			)
			UPDATE p
			SET GroupedID = o.rn
			FROM @Policies p
			INNER JOIN OrderedPolicies o ON p.ID = o.ID
			
			IF @Debug = 1
			BEGIN
				SELECT *
				FROM @Policies
			END
			
			-- LOOP - For each policy
			DECLARE @PolicyCount INT = 0,
					@PolicyGroupIndex INT = 0
			SELECT @PolicyCount = COUNT(*) 
			FROM @Policies
			WHERE PetID = @PetIndex
			
			DECLARE @VolExcessConcat VARCHAR(2000) = ''

		   DECLARE @policyCountXml nvarchar(max) = (select CONCAT('<policiesCount>' , (select count(*) from @Policies), '</policiesCount>'))
		   EXEC _C00_LogItXML @ClientID, 0, '_C600_PA_Policy_GetQuoteValues Products Total Retrieved', @policyCountXml

			--PRINT @PolicyGroupIndex 
			--PRINT @PolicyCount

			/*CPS 2019-03-26 brought over from GetQuoteValues*/
			INSERT INTO dbo.QuotePet (QuoteID, QuoteSessionID, WhenCreated, WhoCreated, PetName, Risk_Postcode, Risk_Species, Risk_Breed, Risk_Sex, Risk_DateOfBirth, Risk_IsNeutered, Risk_Value, Risk_Brand, Risk_StartDate, Risk_HasMicrochip, Risk_Channel)
			SELECT @QuoteID, @QuoteSessionID, dbo.fn_GetDate_Local(), @ClientPersonnelID, @PetName, @Postcode, @SpeciesID, @Breed, @GenderID, @BirthDate, @IsNeutered, @PurchasePrice, @BrandID, @StartDate, @HasMicrochip, @SignUpMethod
			SELECT @QuotePetID = SCOPE_IDENTITY()		


			WHILE @PolicyGroupIndex < @PolicyCount
			BEGIN
			
				SELECT @PolicyGroupIndex += 1
				
				DECLARE @PolicyID INT = NULL,
						@PolicyMatterID INT = NULL,
						@PolicyLeadID INT = NULL,
						@PolicySchemeID INT = NULL,
						@PolicyType VARCHAR(2000) = '',
						@ItemName VARCHAR(500),
						@Brand VARCHAR(500),
						@ProductName VARCHAR(500)
				SELECT	@PolicyID = ID,
						@PolicyMatterID = PolicyMatterID,
						@PolicyLeadID = PolicyLeadID,
						@PolicySchemeID = PolicySchemeID,
						@PolicyType = PolicyType,
						@ItemName = ItemName,
						@Brand = Brand,
						@ProductName = ProductName
				FROM @Policies
				WHERE GroupedID = @PolicyGroupIndex
				AND PetID = @PetIndex
				
				-- Policy type terms
				INSERT @Terms (PetID, PolicyID, Description, SortKey, Value)
				SELECT @PetIndex, @PolicyID, 'Scheme type', '1', @PolicyType
				
				-- Get the variable options and defaults
				-- Policy limits
				-- Insert all limits first so we can pull the terms out and then delete all but vet fees
				INSERT @Limits (PetID, PolicyID, Amount, Options, ResourceListID, Section, ConditionLimit, TimedLimit, LimitNarrative)
				SELECT @PetIndex, @PolicyID, tdvAmount.ValueMoney, tdvOptions.DetailValue, tdvSection.ResourceListID,	CASE
																															WHEN rdvSub.ValueInt = 74283 THEN llSection.ItemValue
																															ELSE llSub.ItemValue
																														END
						,tdvConditionLimit.ValueMoney
						,tdvTimedLimit.DetailValue + ' ' + lli.ItemValue
						,mdvNarrative.DetailValue
				FROM dbo.TableRows r WITH ( NOLOCK )
				INNER JOIN dbo.TableDetailValues tdvSection WITH (NOLOCK) ON r.TableRowID = tdvSection.TableRowID AND tdvSection.DetailFieldID = 144357
				INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON tdvSection.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
				INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID
				INNER JOIN dbo.ResourceListDetailValues rdvSub WITH (NOLOCK) ON tdvSection.ResourceListID = rdvSub.ResourceListID AND rdvSub.DetailFieldID = 146190
				LEFT JOIN dbo.LookupListItems llSub WITH (NOLOCK) ON rdvSub.ValueInt = llSub.LookupListItemID
				LEFT JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 144358
				LEFT JOIN dbo.TableDetailValues tdvOptions WITH (NOLOCK) ON r.TableRowID = tdvOptions.TableRowID AND tdvOptions.DetailFieldID = 170197
				LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON r.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 170013
				LEFT JOIN TableDetailValues tdvConditionLimitRl WITH ( NOLOCK ) on tdvConditionLimitRl.MatterID = r.MatterID AND tdvConditionLimitRl.DetailFieldID = 176984 /*Conditions*/ /*CPS 2017-07-27*/
																				AND tdvConditionLimitRl.ResourceListID = rdvSection.ResourceListID
				LEFT JOIN TableDetailValues tdvConditionLimit WITH ( NOLOCK ) on tdvConditionLimit.TableRowID = tdvConditionLimitRl.TableRowID AND tdvConditionLimit.DetailFieldID = 176985 /*Limit*/
				LEFT JOIN TableDetailValues tdvTimedLimit WITH ( NOLOCK ) on tdvTimedLimit.TableRowID = r.TableRowID AND tdvTimedLimit.DetailFieldID = 144267 /*Non financial limit*/
				LEFT JOIN TableDetailValues tdvTimedLimitType WITH ( NOLOCK ) on tdvTimedLimitType.TableRowID = r.TableRowID AND tdvTimedLimitType.DetailFieldID = 175388 /*Custom limit type*/
				LEFT JOIN LookupListItems lli WITH ( NOLOCK ) on lli.LookupListItemID = tdvTimedLimitType.ValueInt 
				LEFT JOIN MatterDetailValues mdvNarrative WITH ( NOLOCK ) on mdvNarrative.MatterID = r.MatterID AND mdvNarrative.DetailFieldID = 178251 /*Quote and Buy Narrative Header*/
				WHERE r.MatterID = @PolicyMatterID
			
				AND r.DetailFieldID = 145692 /*Policy Limits*/
				AND 
				(
					(@SpeciesID = 0 OR @SpeciesID IS NULL) OR
					(tdvPetType.ValueInt = 0 OR tdvPetType.ValueInt IS NULL) OR
					tdvPetType.ValueInt = @SpeciesID
				)

				-- Limit terms
				INSERT @Terms (PetID, PolicyID, Description, Value, SortKey)
				SELECT	 PetID
						,PolicyID
						,Section
						,CASE WHEN l.TimedLimit <> '' THEN l.TimedLimit ELSE '£' + CAST(CAST(Amount AS DECIMAL(18, 0)) AS VARCHAR) END /*CPS 2017-07-27 include Timed limits*/
						,CASE
							WHEN ResourceListID = @VetFeesID THEN '2'
							ELSE Section + ' 2'
						 END
				FROM @Limits l
				WHERE PetID = @PetIndex
				AND PolicyID = @PolicyID
				
				-- Now clean up the non vet fees limits
				DELETE @Limits
				WHERE ResourceListID != @VetFeesID
				
				EXEC _C600_PA_GetPostcodeGroupFromPostcode @PostCode = @PostCode, @PostCodeGroup = @PostCodeGroup OUTPUT
				
				-- Excess
				-- Insert all of the excesses first so we can pull the terms out and then delete all but vet fees
				INSERT @Excess(PetID, PolicyID, Amount, IsPercentage, Options, Section, ResourceListID, ParentResourceListID)
				SELECT	@PetIndex, @PolicyID, 
						CASE WHEN ExcessPercentage > 0 THEN ExcessPercentage ELSE Excess END,
						CASE WHEN ExcessPercentage > 0 THEN 1 ELSE 0 END,
						e.Options,
						e.Section, e.ResourceListID, e.Out_ResourceListID
				FROM dbo.fn_C00_1272_Policy_GetPolicyExcessWithData(@PolicyMatterID, @PostCodeGroup, @Breed, @BirthDate, @StartDate, @SpeciesID, 0, @StartDate, @StartDate, 1, 1) e
				WHERE e.ResourceListID = @VetFeesID
				
				-- Excess terms
				INSERT @Terms (PetID, PolicyID, Description, Value, SortKey)
				SELECT PetID, PolicyID, Section + ' excess',  '£' + CAST(CAST(Amount AS DECIMAL(18, 0)) AS VARCHAR),	CASE
																														WHEN ResourceListID = @VetFeesID THEN '3'
																														ELSE Section + ' 3'
																													END
				FROM @Excess
				WHERE PetID = @PetIndex
				AND PolicyID = @PolicyID
				AND ResourceListID = ParentResourceListID -- Only show top level sections to prevent duplicates
				
				-- Now clean up the non vet fees excess
				DELETE @Excess
				WHERE ResourceListID != @VetFeesID
						
				-- CoInsurance 
				/*Changed to top 1 and order by descending so we only get the latest value*/ 
				INSERT @CoInsurance(PetID, PolicyID, Amount, Options)
				SELECT TOP 1 @PetIndex, @PolicyID, ExcessPercentage, Options
				FROM dbo.fn_C00_1272_Policy_GetPolicyCoInsuranceWithData(@PolicyMatterID, @BirthDate, @SpeciesID, 0, @Breed) c
				WHERE c.ResourceListID = @VetFeesID
				AND c.DateCutOff <= @StartDate
				order by c.DateCutOff DESC
				
				
				IF NOT EXISTS (SELECT * FROM @CoInsurance WHERE PetID = @PetIndex AND PolicyID = @PolicyID)
				BEGIN				
					INSERT @CoInsurance(PetID, PolicyID, Amount, Options)
					VALUES (@PetIndex, @PolicyID, 0, '')
					
					IF EXISTS ( SELECT TOP 1 @PetIndex, @PolicyID, ExcessPercentage, '' -- TODO
								FROM dbo.fn_C00_1272_Policy_GetPolicyCoInsuranceWithData(@PolicyMatterID, DATEADD(YEAR,-1,@BirthDate), @SpeciesID, 0, @Breed) c
								WHERE c.ResourceListID = @VetFeesID
								AND c.DateCutOff <= @StartDate )
					BEGIN
						SELECT @CoInsuranceNextYear = 1
					END
				END					
				
				-- Vol. Excess
				INSERT @VolExcess(PetID, PolicyID, Amount, Selected)
				SELECT @PetIndex, @PolicyID, tdvAmount.ValueMoney,	CASE
																		WHEN tdvSelected.DetailValue = 'true' THEN 1
																		ELSE 0
																	END
				FROM dbo.TableRows r WITH (NOLOCK) 
				INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 175353
				LEFT JOIN dbo.TableDetailValues tdvSelected WITH (NOLOCK) ON r.TableRowID = tdvSelected.TableRowID AND tdvSelected.DetailFieldID = 175354
				WHERE r.MatterID = @PolicyMatterID
				AND r.DetailFieldID = 175355 /*Voluntary Excess Options*/
						
				-- If none are selected then pick the first
				IF NOT EXISTS 
				(
					SELECT * 
					FROM @VolExcess 
					WHERE Selected = 1
					AND PetID = @PetIndex
					AND PolicyID = @PolicyID
				)
				BEGIN
					
					DECLARE @FirstVolExcessID INT = NULL
					SELECT TOP 1 @FirstVolExcessID = ID
					FROM @VolExcess 
					WHERE PetID = @PetIndex
					AND PolicyID = @PolicyID
					
					UPDATE @VolExcess
					SET Selected = 1
					WHERE ID = @FirstVolExcessID
					
				END
				
				
				-- Optional Coverages	
				INSERT @OptionalCoverages(PetID, PolicyID, GroupID, GroupText, GroupDetail, CheckpointKey)
				SELECT @PetIndex, @PolicyID, tdvRL.ResourceListID, rdvName.DetailValue, rdvDetail.DetailValue, rdvCheck.DetailValue
				FROM dbo.TableRows r (NOLOCK)
				INNER JOIN dbo.TableDetailValues tdvRL (NOLOCK) ON r.TableRowID = tdvRL.TableRowID AND tdvRL.DetailFieldID = 175733
				INNER JOIN dbo.ResourceListDetailValues rdvName (NOLOCK) ON tdvRL.ResourceListID = rdvName.ResourceListID AND rdvName.DetailFieldID = 314005
				INNER JOIN dbo.ResourceListDetailValues rdvDetail (NOLOCK) ON tdvRL.ResourceListID = rdvDetail.ResourceListID AND rdvDetail.DetailFieldID = 314006
				INNER JOIN dbo.ResourceListDetailValues rdvCheck (NOLOCK) ON tdvRL.ResourceListID = rdvCheck.ResourceListID AND rdvCheck.DetailFieldID = 314010
				WHERE r.DetailFieldID = 314007
				AND r.MatterID = @PolicyMatterID
				
				
				-- Update the selected values with any passed in
				DECLARE @PassedLimit MONEY = NULL,
						@PassedExcess MONEY = NULL,
						@PassedCoInsurance MONEY = NULL,
						@PassedVolExcess MONEY = NULL,
						@PassedOptionalCoverages VARCHAR(2000) = NULL
						
				SELECT @PassedLimit = @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/PolicyLimit)[1]', 'MONEY')
				SELECT @PassedExcess = @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/Excess)[1]', 'MONEY')
				SELECT @PassedCoInsurance = @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/CoInsurance)[1]', 'MONEY')
				SELECT @PassedVolExcess = @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/VoluntaryExcess)[1]', 'MONEY')
				SELECT @PassedOptionalCoverages = @PetXml.value('(//PolicyValue[ProductId=sql:variable("@PolicySchemeID")]/OptionalCoverages)[1]', 'VARCHAR(2000)')
						
				UPDATE @Limits
				SET Amount = @PassedLimit
				WHERE @PassedLimit IS NOT NULL
				AND PetID = @PetIndex
				AND PolicyID = @PolicyID
				AND EXISTS
				(
					SELECT * 
					FROM dbo.fnTableOfValuesFromCSV(Options)
					WHERE AnyValue = @PassedLimit
				)
				
				UPDATE @Excess
				SET Amount = @PassedExcess
				WHERE @PassedExcess IS NOT NULL
				AND PetID = @PetIndex
				AND PolicyID = @PolicyID
				AND EXISTS
				(
					SELECT * 
					FROM dbo.fnTableOfValuesFromCSV(Options)
					WHERE AnyValue = @PassedExcess
				)
				
				UPDATE @CoInsurance
				SET Amount = @PassedCoInsurance
				WHERE @PassedCoInsurance IS NOT NULL
				AND PetID = @PetIndex
				AND PolicyID = @PolicyID
				AND EXISTS
				(
					SELECT * 
					FROM dbo.fnTableOfValuesFromCSV(Options)
					WHERE AnyValue = @PassedCoInsurance
				)
				
				-- If the passed voluntary excess is allowed
				IF EXISTS 
				(
					SELECT * 
					FROM @VolExcess 
					WHERE Amount = @PassedVolExcess
					AND PetID = @PetIndex
					AND PolicyID = @PolicyID
				)
				BEGIN
				
					-- Clear out all selected
					UPDATE @VolExcess
					SET Selected = 0
					WHERE PetID = @PetIndex
					AND PolicyID = @PolicyID
				
					-- Select what the user has passed in
					UPDATE @VolExcess
					SET Selected = 1
					WHERE Amount = @PassedVolExcess
					AND PetID = @PetIndex
					AND PolicyID = @PolicyID
				
				END
				ELSE
				BEGIN
				
					-- Otherwise default to the option default on the scheme
					SELECT TOP 1 @PassedVolExcess = Amount
					FROM @VolExcess 
					WHERE PetID = @PetIndex
					AND PolicyID = @PolicyID
					AND Selected = 1
				
				END
				
				-- Set the selected optional coverages
				UPDATE c
				SET Selected = 1
				FROM @OptionalCoverages c
				INNER JOIN dbo.fnTableOfIDsFromCSV(@PassedOptionalCoverages) p ON c.GroupID = p.AnyID
				WHERE PetID = @PetIndex
				AND PolicyID = @PolicyID				

				-- Map these to standard fields and run calcs
				DELETE @Overrides
				INSERT @Overrides (AnyID, AnyValue1, AnyValue2)
				EXEC dbo._C00_1273_Policy_GetOverrides @Postcode, 
														@County, 
														@SpeciesID, 
														@Breed, 
														@GenderID, 
														@BirthDate,
														@IsNeutered, 
														@PurchasePrice, 
														@PassedVolExcess,
														@PetNumber, 
														@DiscountCode, 
														@IsMultipet, 
														@PassedOptionalCoverages,
														@SignUpMethod,
														@LastPeriodExitPremium,
														@LossRationPercentage,
														@NumberPreviousClaims,
														@PaymentFrequency,
														@PolicyYearNumber,
														@MicrochipNumber,
														@ItemName,
														@Brand,
														@ProductName,
														@IPT,
														@SourceLookupListItemID
														
				
				/*GPR 2018-07-19 adopted from 433*/
				SELECT @SourceName = li.ItemValue
				FROM LookupListItems li WITH ( NOLOCK ) 
				WHERE li.LookupListItemID = @SourceLookupListItemID
					

				--SELECT * FROM @Overrides
				--SELECT @StartDate, @TermsDate, @PolicySchemeID		
				
				/*GPR 2018-02-19 pass PetCount to allow MultiPet discount to be controlled within the Rules Engine. 600 grant multi-pet discount to all pets*/
				/*CPS 2018-02-20 for GPR*/
				DECLARE @TotalPetsInRequest INT
					
				;WITH PetList AS
				(
				SELECT 1 [N]
				FROM @XmlRequest.nodes('(//PetQuote)') a(b)
				)
				SELECT @TotalPetsInRequest = COUNT(*)
				FROM PetList
				
				INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
				VALUES ( 4, 'fn_C00_1273_RulesEngine_PetCount',@TotalPetsInRequest + ISNULL(dbo.fn_C00_1273_RulesEngine_PetCount(@CustomerID, 0, 0, 0, @Overrides),0) )
				
				/*GPR 2020-07-27 for PPET-178*/
				/*ALM 2020-09-02 for PPET-200*/
				/*ALM/GPR 2020-09-03 further mods for PPET-200*/
				/*WorkingDog*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsWorkingPet = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 147277 /*Is your pet a service animal or used for commercial purposes?*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsWorkingPet = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313929','5144' )
					
					SELECT @IsWorkingPet = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313929','5145' )
				END

				/*Is the customer a veteran?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsVeteran = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2001392 /*Is the customer a veteran?*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsVeteran = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313933','5144' )

					SELECT @IsVeteran = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313933','5145' )
				END

				/*Customer employed as Vet/Staff*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsVetOrStaff = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2001393 /*Customer employed as Vet/Staff*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsVetOrStaff = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313936','5144' )
					
					SELECT @IsVetOrStaff = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313936','5145' )
				END

				/*Pet is Rescue*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsRescue = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2001394 /*Is your pet a rescue animal?*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsRescue = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'177501','5144' )
					
					SELECT @IsRescue = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'177501','5145' )
				END

				/*Customer is Medical Professional*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsMedical = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2001395 /*Is the customer a medical professional?*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsMedical = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313935','5144' )
					
					SELECT @IsMedical = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313935','5145' )
				END
				
				/*Wellness*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsWellness = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002259 /*Wellness Discount rating factor*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsWellness = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313934','5144' )
					
					SELECT @IsWellness = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313934','5145' )
				END
				/*GPR 2020-10-07 for SDPRU-77*/
				/*Is the customer from a known Internet partner you know about?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsInternetPartner = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002304 /*Internet Partner*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsInternetPartner = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313938','5144' )
					
					SELECT @IsInternetPartner = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313938','5145' )
				END

				/*Is the customer from a known strategic partner you know about?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsStrategicPartner = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002305 /*Strategic Partner*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsStrategicPartner = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313939','5144' )
					
					SELECT @IsStrategicPartner = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313939','5145' )
				END

				/*Is the customer from a known employer we provide benefits to?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsCorporateGroupBenefit = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002306 /*Corporate Group Benefit*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsCorporateGroupBenefit = 1
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313937','5144' )
					
					SELECT @IsCorporateGroupBenefit = 0 /*Reset for the next pet*/
				END
				ELSE
				BEGIN
					INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					VALUES ( 1,'313937','5145' )
				END

				/*Is the customer coming in from a broker with a surcharge?*/
				;WITH Underwriting AS
				(
				SELECT b.value('(AnswerId)[1]','int') [AnswerId], b.value('(QuestionId)[1]','int') [QuestionId]
				FROM @PetXml.nodes('//PetQuote/UnderwritingList/UnderwritingAnswerItemType') a(b)
				)
				SELECT @IsBroker = 1
				FROM Underwriting uw 
				WHERE uw.QuestionId = 2002307 /*Corporate Group Benefit*/
				AND uw.AnswerId = 5144 /*Yes*/

				IF @IsBroker = 1
				BEGIN
					--INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
					--VALUES ( 1,'176964','193317' )

					SELECT @SignUpMethod = 193317
					
					SELECT @IsBroker = 0 /*Reset for the next pet*/
				END


				-- Clear out old values
				DELETE @Premiums
				
				PRINT 'Calling _C00_1273_Policy_CalculatePremium_Evaluated'

				INSERT @Premiums(TermsDate, SchemeID, PolicyMatterID, RuleSetID, AnnualPremium, Discount, PremiumLessDiscount, FirstMonthly, RecurringMonthly, Net, Commission, GrossNet, DiscountGrossNet, PAFIfBeforeIPT, PAFBeforeIPTGrossNet, IPT, IPTGrossNet, PAFIfAfterIPT, GrossGross, PremiumCalculationID, AnnualTaxLevel1, AnnualTaxLevel2, Reduction, DiscountRuleOutput)
				EXEC dbo._C00_1273_Policy_CalculatePremium_Evaluated NULL
						,@StartDate
						,@TermsDate
						,@PolicySchemeID
						,@QuoteLogContextID = @QuoteSessionID -- CPS 2018-01-11 added QuoteSessionID for logs
						,@EvaluatedXml = @EvaluatedXml
						,@XmlRequest = @XmlRequest
						,@PetXml = @PetXml
						,@PetID = @PetIndex
						,@PolicySchemeID = @PolicySchemeID
						,@PolicyMatterID = @PolicyMatterID
						,@MarketingCode = @DiscountCode
						,@PetNumber = @PetNumber

				SELECT @PremiumCalculationID = PremiumCalculationID
				FROM @Premiums

				-- For optional upgrades we now need to collect the value from the checkpoint
				UPDATE c
				SET Cost = CAST(v.RuleOutput AS MONEY)
				FROM @OptionalCoverages c
				INNER JOIN dbo.PremiumCalculationDetailValues v (NOLOCK) ON c.CheckpointKey = v.RuleCheckpoint AND v.PremiumCalculationDetailID = @PremiumCalculationID
				WHERE PetID = @PetIndex
				AND PolicyID = @PolicyID

				PRINT 'Called _C00_1273_Policy_CalculatePremium_Evaluated'

				IF @Debug = 1
				BEGIN
					SELECT *
					FROM @Premiums

					SELECT 	 @StartDate [@StartDate]
							,@TermsDate [@TermsDate]
							,@PolicySchemeID [@PolicySchemeID]
							,@QuoteSessionID [@QuoteSessionID] -- CPS 2018-01-11 added QuoteSessionID for logs
							,@EvaluatedXml [@EvaluatedXml]
							,@XmlRequest [@XmlRequest]
							,@PetXml [@PetXml]
							,@PetIndex [@PetIndex]
							,@PolicySchemeID [@PolicySchemeID]
							,@PolicyMatterID [@PolicyMatterID]
							,@DiscountCode [@DiscountCode]
							,@PetNumber [@PetNumber]					
				END
				
				/*GPR 2018-06-04 copied from 433*/ 
				INSERT @ErrorMessages ( ErrorMessage, ProductId, PetRef )
                    SELECT fn.ErrorMessage, @PolicySchemeID, @PetRef
                    FROM dbo.fn_C600_RulesEngineMandatoryCheckpointCheck(@PremiumCalculationID) fn
                    /*!! NO SQL HERE !!*/
                    SELECT @ErrorCount = @@ROWCOUNT
                    
                    /*If we found errors, NULL out the prices*/
                    UPDATE pr
                    SET  AnnualPremium       = NULL
                         ,PremiumLessDiscount = NULL
                         ,Discount           = NULL
                    FROM @Premiums pr 
                    WHERE @ErrorCount <> 0

					/*GPR 2020-09-09 for PPET-400*/
					DECLARE @TaxLevel1 MONEY
					DECLARE @TaxLevel2 MONEY

					SELECT @TaxLevel1 = pr.AnnualTaxLevel1, @TaxLevel2 = pr.AnnualTaxLevel2
					FROM @Premiums pr
					
					UPDATE @Premiums
                    SET   FirstMonthlyTaxLevel1 = split.FirstMonthly
                         ,RecurringMonthlyTaxLevel1 = split.RecurringMonthly
					FROM dbo.fn_C00_1273_SplitPremium(@TaxLevel1) split

					UPDATE @Premiums
                    SET   FirstMonthlyTaxLevel2 = split.FirstMonthly
                         ,RecurringMonthlyTaxLevel2 = split.RecurringMonthly
					FROM dbo.fn_C00_1273_SplitPremium(@TaxLevel2) split /*GPR 2020-09-28 for SDPRU-58*/
				   				
				/*CS 2017-05-31 Identify whether the master request includes more than one pet.  Multi-Pet Discount applies to all pets, not just the new one.*/

				IF ( SELECT COUNT(*)
					 FROM @XmlRequest.nodes('//PetQuote') a(b) ) > 1 OR @IsMultipet = 1
				BEGIN
					--SELECT @DiscountedMonths += 1     -- GPR 2018-02-16 commented out
					SELECT @IsMultipet = 1				/*GPR 2018-02-16*/	
				END
				

				SELECT @DiscountedMonths += tdvMonth.ValueInt
				FROM TableRows tr WITH ( NOLOCK )
				INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 177364
				INNER JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = tdv.ResourceListID AND rdv.DetailFieldID = 177363
				INNER JOIN TableDetailValues tdvMonth WITH ( NOLOCK ) on tdvMonth.TableRowID = tr.TableRowID AND tdvMonth.DetailFieldID = 177472
				WHERE tr.DetailFieldID = 177367
				AND rdv.DetailValue = @TrackingCode
			

				UPDATE p
				SET p.AnnualPremium = m.AnnualPremium, 
					p.Discount = m.Discount + m.Reduction,				/*GPR 2018-02-16 uncommented*/ /*GPR 2020-11-01 for SDPRU-111*/
					--p.Discount = @DiscountedMonths,		/*GPR 2018-02-16 commented out*/
					--p.PremiumLessDiscount = m.PremiumLessDiscount -(@DiscountedMonths * m.RecurringMonthly), /*CPS 2017-06-01*/    -- GPR 2018-02-16 commented out
					p.PremiumLessDiscount = m.PremiumLessDiscount - m.Discount,  /*GPR 2018-02-16*/ 
					p.FirstMonthly= m.FirstMonthly, 
					p.RecurringMonthly = m.RecurringMonthly,
					P.AnnualTaxLevel1 = ROUND(m.AnnualTaxLevel1,2), /*ALM 2020-10-19 round to 2dp for SDPRU-58*/
					P.AnnualTaxLevel2 = ROUND(m.AnnualTaxLevel2,2), /*ALM 2020-10-19 round to 2dp for SDPRU-58*/
					P.FirstMonthlyTaxLevel1 = ROUND(m.FirstMonthlyTaxLevel1,2), /*GPR 2020-10-07 round to 2dp for SDPRU-58*/
					P.FirstMonthlyTaxLevel2 = ROUND(m.FirstMonthlyTaxLevel2,2), /*GPR 2020-10-07 round to 2dp for SDPRU-58*/
					P.RecurringMonthlyTaxLevel1 = ROUND(m.RecurringMonthlyTaxLevel1,2), /*GPR 2020-10-07 round to 2dp for SDPRU-58*/
					P.RecurringMonthlyTaxLevel2 = ROUND(m.RecurringMonthlyTaxLevel2,2), /*GPR 2020-10-07 round to 2dp for SDPRU-58*/
					p.DiscountRuleOutput = ROUND(m.DiscountRuleOutput,2) /*GPR 2020-11-09 for SDPRU-111*/
				FROM @Policies p
				CROSS APPLY @Premiums m
				WHERE p.GroupedID = @PolicyGroupIndex
				AND p.PetID = @PetIndex
				
				SELECT @DiscountedMonths = 0

				/*CPS 2019-03-26 brought over from GetQuoteValues*/
				/*GPR 2019-03-05 INSERT into QuotePetProduct for C600 #55470 - Quote_SaveQuotePetProduct  */  
				INSERT INTO dbo.QuotePetProduct (QuoteSessionID, QuoteID, QuotePetID, WhenCreated,ProductID, RuleSetID, RulesEngineOutcome, ProductName) 
				SELECT @QuoteSessionID, @QuoteID, @QuotePetID, dbo.fn_GetDate_Local(), @PolicySchemeID, ISNULL(p.RuleSetID,-1), p.AnnualPremium, @ProductName
				FROM @Premiums p
				
			END
			-- END LOOP - For each policy
			
			/*ARH - 2018-09-09 - This deletion moved out of while loop to allow multiple products to be returned*/
			/*delete 0 value premiums so they are not sent to the agg*/ 
			DELETE FROM @Policies 
			WHERE ISNULL(AnnualPremium,0) = 0 
			
			SELECT @VolExcessConcat = STUFF((SELECT ',' + CAST(Amount AS VARCHAR(MAX))
									FROM @VolExcess
									WHERE PetID = @PetIndex
									AND Selected = 1
									FOR XML PATH(''), TYPE).value('(./text())[1]', 'VARCHAR(MAX)'), 1, 1, '')
			
			INSERT dbo._C600_QuoteValues(QuoteSessionID, QuoteDate, FirstName, LastName, Email, PostCode, PetName, BreedID, PetDoB, QuoteXml, PetXml, 
										HomeTel, MobileTel, Breed, SpeciesID, Species, Gender, IsNeutered, PetPrice, VolExcess)
			VALUES						(@QuoteSessionID, @Now, @FirstName, @LastName, @Email, @Postcode, @PetName, @BreedID, @BirthDate, @XmlRequest, @PetXml,
										@HomePhone, @Mobile, @Breed, @SpeciesID, @Species, @Gender, CASE @IsNeutered WHEN 1 THEN 'Y' ELSE 'N' END, @PurchasePrice, @VolExcessConcat)
			
			DECLARE @QuoteValueID INT
			SELECT @QuoteValueID = SCOPE_IDENTITY()
			
			INSERT @QuoteValueIDs (QuoteValueID)
			VALUES (@QuoteValueID)
			

			/*GPR 2021-03-10 for FURKIN-376*/
			SELECT @Postcode1 = LEFT(@Postcode, 1)
			SELECT @Postcode3 = LEFT(@Postcode, 3)
			SELECT @Postcode6 = LEFT(@Postcode, 6)

			IF @StateID IS NULL AND (SELECT COUNT(pbp.StateID)
				FROM ProvinceByPostalCode pbp WITH (NOLOCK)
				WHERE pbp.PostalCode = @Postcode6) > 0
			BEGIN
				SELECT TOP(1) @StateID = pbp.StateID
				FROM ProvinceByPostalCode pbp WITH (NOLOCK)
				WHERE pbp.PostalCode = @Postcode6
				ORDER BY pbp.StateID ASC 
			END

			IF @StateID IS NULL AND (SELECT COUNT(pbp.StateID)
				FROM ProvinceByPostalCode pbp WITH (NOLOCK)
				WHERE pbp.PostalCode = @Postcode3) > 0
			BEGIN
				SELECT TOP(1) @StateID = pbp.StateID
				FROM ProvinceByPostalCode pbp WITH (NOLOCK)
				WHERE pbp.PostalCode = @Postcode3
				ORDER BY pbp.StateID ASC
			END

			IF @StateID IS NULL AND (SELECT COUNT(pbp.StateID)
				FROM ProvinceByPostalCode pbp WITH (NOLOCK)
				WHERE pbp.PostalCode = @Postcode1) > 0
			BEGIN
				SELECT TOP(1) @StateID = pbp.StateID
				FROM ProvinceByPostalCode pbp WITH (NOLOCK)
				WHERE pbp.PostalCode = @Postcode1
				ORDER BY pbp.StateID ASC
			END

			SELECT @StateCode = Canada.StateCode
			FROM UnitedStates Canada WITH (NOLOCK)
			WHERE Canada.StateID = @StateID
			AND Canada.CountryID = 39

			SELECT @ProvinceLookupListItemID = CASE @StateCode 
					WHEN 'AB' THEN 61739
					WHEN 'BC' THEN 61740
					WHEN 'MB' THEN 61741
					WHEN 'NB' THEN 61742
					WHEN 'NL' THEN 61743
					WHEN 'NS' THEN 61744
					WHEN 'NT' THEN 61745
					WHEN 'NU' THEN 61746
					WHEN 'ON' THEN 61747
					WHEN 'PE' THEN 61748
					WHEN 'QC' THEN 61749
					WHEN 'SK' THEN 61750
					WHEN 'YT' THEN 61751
				END

			/*2021-01-21 - Enrollment fee*/
			INSERT INTO @EnrollmentFeeTable (PetID, FeeTableRowID, EnrollmentFee, TaxTableRowID, Tax, Total)
			SELECT @PetIndex, FeeTableRowID, EnrollmentFee, TaxTableRowID, Tax, Total
			FROM dbo.fn_C600_GetEnrollmentFee(@BrandID, @CustomerID, @ProvinceLookupListItemID, @PetIndex, @StartDate)

			UPDATE p
			--SET PetXml.modify('insert <CoInsuranceNextYear>1</CoInsuranceNextYear>into (//PetInfo)[1]') /*CPS 2017-06-02 Replaced Insert with Modify*/
			SET PetXml.modify('replace value of (//CoInsuranceNextYear[1]/text())[1]  with "true"')
			FROM @Pets p 
			WHERE @CoInsuranceNextYear = 1
			--WHERE EXISTS ( SELECT * 
			--               FROM @CoInsurance ci 
			--               WHERE ci.Amount <> 0.00
			--               AND ci.PetID = p.ID )
			AND p.ID = @PetIndex
			
			SET @CoInsuranceNextYear = 0 /*Reset the counter*/
		END
		-- END LOOP - For each pet

		SELECT @XmlResponse =
		(
		 SELECT    @QuoteSessionID AS SessionId
						 ,@SourceName [QuoteSource]
                         ,@AdminFee [AdminFee] /*CPS 2017-05-24*/
                         ,@IsMultipet [MultiPet], /*GPR 2018-02-16*/
                         (
                         SELECT em.PetRef [PetID], em.ErrorMessage [ErrorMessage], em.ProductId
                         FROM @ErrorMessages em
                         FOR XML PATH ('CheckpointErrors'), ROOT ('CalculationErrors'), TYPE
                         )
                         ,
                         (
						SELECT	PetXml.query('//PetInfo'),
								(
									SELECT	PolicySchemeID AS ProductId, ProductName,
											(
												SELECT	limit.Amount AS Selected, 0 AS limit
														,limit.ConditionLimit
														,limit.LimitNarrative
														,
														(
															SELECT csv.AnyValue AS [decimal]
															FROM dbo.fnTableOfValuesFromCSV(limit.Options) csv
															FOR XML PATH(''), ROOT ('Options'), TYPE
														)
												FROM @Limits AS limit
												WHERE limit.PetID = pet.ID
												AND limit.PolicyID = pol.ID
												FOR XML PATH ('PolicyLimit'), TYPE
											),
											(
												SELECT	Amount AS Selected, IsPercentage, 
														(
															SELECT csv.AnyValue AS [decimal]
															FROM dbo.fnTableOfValuesFromCSV(ex.Options) csv
															FOR XML PATH(''), ROOT ('Options'), TYPE
														)
												FROM @Excess AS ex
												WHERE ex.PetID = pet.ID
												AND ex.PolicyID = pol.ID
												FOR XML PATH ('Excess'), TYPE
											),
											(
												SELECT	Amount AS Selected, 1 AS IsPercentage, 
														(
															SELECT csv.AnyValue AS [decimal]
															FROM dbo.fnTableOfValuesFromCSV(co.Options) csv
															FOR XML PATH(''), ROOT ('Options'), TYPE
														)
												FROM @CoInsurance AS co
												WHERE co.PetID = pet.ID
												AND co.PolicyID = pol.ID
												FOR XML PATH ('CoInsurance'), TYPE
											),
											(
												SELECT TOP 1 Amount AS Selected, 0 AS IsPercentage, 
														(
															SELECT Amount AS [decimal]
															FROM @VolExcess
															WHERE PetID = pet.ID
															AND PolicyID = pol.ID
															FOR XML PATH (''), ROOT ('Options'), TYPE
														)
												FROM @VolExcess AS vol
												WHERE vol.PetID = pet.ID
												AND vol.PolicyID = pol.ID
												AND Selected = 1
												FOR XML PATH ('VoluntaryExcess'), TYPE
											),
											(
												SELECT	GroupID, GroupText, GroupDetail, Selected, Cost
												FROM @OptionalCoverages c
												WHERE c.PetID = pet.ID
												AND c.PolicyID = pol.ID
												FOR XML PATH ('GroupedCoverage'), ROOT ('GroupedOptionalCoverages'), TYPE
											), 
											(
												SELECT DiscountRLID AS DiscountId, Name, Description, Value, IsPercentage, Amount
												FROM @Discounts AS d
												WHERE d.PetID = pet.ID
												AND d.PolicyID = pol.ID
												FOR XML PATH ('Discount'), ROOT ('Discounts'), TYPE
											),
											ISNULL(DiscountRuleOutput,1) [DiscountFactor], /*GPR 2020-11-12 for SDPRU-111*/
											(
												/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
												/*GPR 2021-03-01 FURKIN-305 - Return e.Tax as [FeeTax]*/
												SELECT e.FeeTableRowID, e.EnrollmentFee AS [Fee], e.TaxTableRowID, e.Tax AS [FeeTax], e.Total
												FROM @EnrollmentFeeTable e
												WHERE e.PetID = pet.ID
												FOR XML PATH ('EnrollmentFee'), ROOT ('EnrollmentFees'), TYPE
											),
											(
												SELECT	 AnnualTaxLevel1
														,FirstMonthlyTaxLevel1
														,RecurringMonthlyTaxLevel1
														,AnnualTaxLevel2
														,FirstMonthlyTaxLevel2
														,RecurringMonthlyTaxLevel2
												FOR XML PATH ('Tax'), ROOT ('Tax'), TYPE
											),
											ROUND(AnnualPremium, 2) + @AdminFee [AnnualPremium], Discount, PremiumLessDiscount, FirstMonthly + @AdminFee [FirstMonthly], RecurringMonthly, ISNULL(DiscountRuleOutput,1) [DiscountRuleOutput], /*GPR 2020-11-09 for SDPRU-111*/
											(
												SELECT Description, Value, SortKey
												FROM @Terms AS t
												WHERE t.PetID = pet.ID
												AND t.PolicyID = pol.ID
												FOR XML PATH ('PolicyTerm'), ROOT ('PolicyTerms'), TYPE
											),
											PolicyDocUrl, InfoDocUrl
									FROM @Policies AS pol
									WHERE pol.PetID = pet.ID
									FOR XML PATH ('PolicyInfo'), ROOT ('PolicyList'), TYPE
								)		
						FROM @Pets AS pet
						FOR XML PATH ('PetPolicy'), ROOT ('PetPolicyList'), TYPE
					),
					@Result Result
			--WHERE 1 = 0 -- No quotes as GE reported an issue!
			FOR XML PATH ('GetQuoteValuesResponse')
		)
		
		UPDATE dbo._C600_QuoteValues
		SET ResponseXml = @XmlResponse, QuoteEnd = dbo.fn_GetDate_Local()
		WHERE QuoteValueID IN (SELECT QuoteValueID FROM @QuoteValueIDs)
		
	END
	ELSE 
	BEGIN
		
		EXEC dbo.LogXML__AddEntry  @ClientID, 0, 'GetQuoteValues_Validate', @XmlResponse, NULL		
		
	END
	
	EXEC _C00_LogItXml @ClientID, 0, '_C600_PA_Policy_GetQuoteValues_Evaluated Response', @XmlResponse /*CPS 2017-05-09 write to the logs*/ /*GPR 2020-09-07 updated proc name*/

	-- Allow this proc to return via output parameter or via select
	IF @XmlOut IS NOT NULL
	BEGIN
		SELECT @XmlOut = @XmlResponse
	END
	ELSE
	BEGIN
		SELECT @XmlResponse
	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetQuoteValues_Evaluated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_GetQuoteValues_Evaluated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetQuoteValues_Evaluated] TO [sp_executeall]
GO
