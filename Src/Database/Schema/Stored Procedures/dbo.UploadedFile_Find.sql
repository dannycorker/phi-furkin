SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the UploadedFile table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UploadedFile_Find]
(

	@SearchUsingOR bit   = null ,

	@UploadedFileID int   = null ,

	@ClientID int   = null ,

	@FolderID int   = null ,

	@UploadedFileName varchar (255)  = null ,

	@UploadedFileTitle varchar (1000)  = null ,

	@UploadedFileDescription varchar (2000)  = null ,

	@UploadedFile varbinary (MAX)  = null ,

	@UploadedFileGUID uniqueidentifier   = null ,

	@UploadedFileSize bigint   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [UploadedFileID]
	, [ClientID]
	, [FolderID]
	, [UploadedFileName]
	, [UploadedFileTitle]
	, [UploadedFileDescription]
	, [UploadedFile]
	, [UploadedFileGUID]
	, [UploadedFileSize]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[UploadedFile] WITH (NOLOCK) 
    WHERE 
	 ([UploadedFileID] = @UploadedFileID OR @UploadedFileID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FolderID] = @FolderID OR @FolderID IS NULL)
	AND ([UploadedFileName] = @UploadedFileName OR @UploadedFileName IS NULL)
	AND ([UploadedFileTitle] = @UploadedFileTitle OR @UploadedFileTitle IS NULL)
	AND ([UploadedFileDescription] = @UploadedFileDescription OR @UploadedFileDescription IS NULL)
	AND ([UploadedFileGUID] = @UploadedFileGUID OR @UploadedFileGUID IS NULL)
	AND ([UploadedFileSize] = @UploadedFileSize OR @UploadedFileSize IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [UploadedFileID]
	, [ClientID]
	, [FolderID]
	, [UploadedFileName]
	, [UploadedFileTitle]
	, [UploadedFileDescription]
	, [UploadedFile]
	, [UploadedFileGUID]
	, [UploadedFileSize]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[UploadedFile] WITH (NOLOCK) 
    WHERE 
	 ([UploadedFileID] = @UploadedFileID AND @UploadedFileID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FolderID] = @FolderID AND @FolderID is not null)
	OR ([UploadedFileName] = @UploadedFileName AND @UploadedFileName is not null)
	OR ([UploadedFileTitle] = @UploadedFileTitle AND @UploadedFileTitle is not null)
	OR ([UploadedFileDescription] = @UploadedFileDescription AND @UploadedFileDescription is not null)
	OR ([UploadedFileGUID] = @UploadedFileGUID AND @UploadedFileGUID is not null)
	OR ([UploadedFileSize] = @UploadedFileSize AND @UploadedFileSize is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile_Find] TO [sp_executeall]
GO
