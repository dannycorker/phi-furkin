SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PostCodeLookup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PostCodeLookup_Find]
(

	@SearchUsingOR bit   = null ,

	@PostCodeLookupID int   = null ,

	@PostCode varchar (10)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PostCodeLookupID]
	, [PostCode]
    FROM
	[dbo].[PostCodeLookup] WITH (NOLOCK) 
    WHERE 
	 ([PostCodeLookupID] = @PostCodeLookupID OR @PostCodeLookupID IS NULL)
	AND ([PostCode] = @PostCode OR @PostCode IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PostCodeLookupID]
	, [PostCode]
    FROM
	[dbo].[PostCodeLookup] WITH (NOLOCK) 
    WHERE 
	 ([PostCodeLookupID] = @PostCodeLookupID AND @PostCodeLookupID is not null)
	OR ([PostCode] = @PostCode AND @PostCode is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PostCodeLookup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_Find] TO [sp_executeall]
GO
