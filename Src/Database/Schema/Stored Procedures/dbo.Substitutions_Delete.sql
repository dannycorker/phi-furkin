SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Substitutions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Substitutions_Delete]
(

	@SubstitutionsID int   
)
AS


				DELETE FROM [dbo].[Substitutions] WITH (ROWLOCK) 
				WHERE
					[SubstitutionsID] = @SubstitutionsID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Substitutions_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Delete] TO [sp_executeall]
GO
