SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-05-23
-- Description:	SP for C00 For copying pages of fields from one leadtype to another
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyDetailFields]
	@FromLeadTypeID int,
	@ToLeadTypeID int,
	@DetailFieldPageID int = NULL

AS
BEGIN
	SET NOCOUNT ON;

	Declare @TestID int,
			@FromPageID int,
			@NewPageID int,
			@ClientID int, 
			@LeadOrMatter int, 
			@PageName varchar(50), 
			@PageCaption varchar(50), 
			@PageOrder int , 
			@Enabled bit, 
			@ResourceList bit,
			@ToClientID int

	Select @TestID = 0, @FromPageID = 0

	SELECT @ToClientID = lt.ClientID
	FROM LeadType lt WITH (NOLOCK)
	WHERE lt.LeadTypeID = @ToLeadTypeID

	IF @DetailFieldPageID IS NULL
	BEGIN

		Select top 1 @FromPageID = DetailFieldPageID, @ClientID = ClientID
		From DetailFieldPages
		Where LeadTypeID = @FromLeadTypeID
		Order By DetailFieldPageID
		
	END
	ELSE
	BEGIN
	
		Select @FromPageID = @DetailFieldPageID
	
	END


	while (@FromPageID > 0 AND @FromPageID > @TestID)
	Begin

		Select @ClientID = dfp.ClientID, @LeadOrMatter = dfp.LeadOrMatter, @PageName = dfp.PageName, @PageCaption = dfp.PageCaption, @PageOrder = dfp.PageOrder, @Enabled = dfp.Enabled, @ResourceList = dfp.ResourceList
		From DetailFieldPages dfp WITH (NOLOCK)
		Where DetailFieldPageID = @FromPageID

		Insert Into DetailFieldPages(ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled, ResourceList)
		Select @ToClientID, @LeadOrMatter, @ToLeadTypeID, @PageName, @PageCaption, @PageOrder, @Enabled, @ResourceList

		Select @NewPageID = SCOPE_IDENTITY() 

		Insert Into DetailFields(ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
		Select @ToClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, @ToLeadTypeID, Enabled, @NewPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView
		From DetailFields
		Where DetailFieldPageID = @FromPageID

		Select Count(*), @NewPageID
		From DetailFields
		Where DetailFieldPageID = @NewPageID

		Select @TestID = @FromPageID

		Select top 1 @FromPageID = DetailFieldPageID
		From DetailFieldPages
		Where LeadTypeID = @FromLeadTypeID
		and DetailFieldPageID > @FromPageID
		Order By DetailFieldPageID
		
		IF @DetailFieldPageID IS NOT NULL Select @FromPageID = @DetailFieldPageID

	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyDetailFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyDetailFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyDetailFields] TO [sp_executeall]
GO
