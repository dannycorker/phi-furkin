SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Owen Ashcroft
-- Create date: 2018-02-26
-- Description:	Get values from a specific look up list
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_LookUpList]
  (
    @LookUpList NVARCHAR(MAX),
    @ClientID   INT
  )
AS
  BEGIN

    SELECT
      li.LookupListItemID Value,
      li.ItemValue        Text
    FROM LookupListItems  li WITH ( NOLOCK )
      INNER JOIN LookupList L WITH ( NOLOCK ) ON li.LookupListID = L.LookupListID
    WHERE LookupListName = @LookUpList
          AND (L.ClientID = 0 OR L.ClientID = @ClientID)
          AND L.Enabled = 1 and li.Enabled = 1

  END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_LookUpList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_LookUpList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_LookUpList] TO [sp_executeall]
GO
