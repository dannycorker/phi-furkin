SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 07/06/2018
-- Description:	Gets a list of third party field maps
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping__GetByLeadTypeId]
	@LeadTypeID INT,
	@ThirdPartySystemID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT m.*, 
		   tf.FieldName ThirdPartyFIeld, 
		   df.FieldName, 
		   dfCol.FieldName ColumnField 
	FROM ThirdPartyField tf WITH (NOLOCK) 
	INNER JOIN dbo.ThirdPartyFieldMapping m WITH (NOLOCK) ON m.ThirdPartyFieldID = tf.ThirdPartyFieldID
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.detailfieldID = m.DetailFieldID
	LEFT JOIN dbo.DetailFields dfCol WITH (NOLOCK) ON dfCol.detailfieldID = m.ColumnFieldID
    WHERE tf.ThirdPartySystemID=@ThirdPartySystemID AND m.LeadTypeID=@LeadTypeID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetByLeadTypeId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping__GetByLeadTypeId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetByLeadTypeId] TO [sp_executeall]
GO
