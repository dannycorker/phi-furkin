SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the OutcomeEquations table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeEquations_Insert]
(

	@OutcomeEquationID int    OUTPUT,

	@OutcomeID int   ,

	@EquationID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[OutcomeEquations]
					(
					[OutcomeID]
					,[EquationID]
					,[ClientID]
					)
				VALUES
					(
					@OutcomeID
					,@EquationID
					,@ClientID
					)
				-- Get the identity value
				SET @OutcomeEquationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeEquations_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Insert] TO [sp_executeall]
GO
