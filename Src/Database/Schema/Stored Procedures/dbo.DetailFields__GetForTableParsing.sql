SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-07
-- Description:	Only return certain fields that are of use for decrypting table detail values.
--              @TableDetailFieldPageID 504 is a good example. It has a ResourceList as well (503),
--              for the full spread of possible complications.
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetForTableParsing]
(
	@TableDetailFieldPageID int,     -- The TableDetailFieldPageID, which equals the DetailFieldPageID for all fields belonging to the table.
	@QuestionTypeID int = null,      -- NULL = Show All
	@Encrypted bit = null,            -- 1 = Encrypted only, 0 = Plain only, NULL = Show All
	@ClientID INT = NULL
)
AS

BEGIN

	SELECT df.*
	FROM dbo.fnDetailFieldsShared(@ClientID) df 
	WHERE df.[Enabled] = 1
	AND ( @QuestionTypeID IS NULL OR df.QuestionTypeID = @QuestionTypeID )
	AND ( @Encrypted IS NULL OR @Encrypted = ISNULL(df.Encrypt, 0) )
	AND df.DetailFieldID IN (
	
		/* All the plain fields within this table, ie not Resource Lists */
		SELECT dft.DetailFieldID 
		FROM dbo.fnDetailFieldsShared(@ClientID) dft 
		WHERE dft.[DetailFieldPageID] = @TableDetailFieldPageID 
		AND dft.QuestionTypeID <> 14 
				
		UNION
		
		/* All the Resource List fields within this table (if any) */
		SELECT dfr.DetailFieldID 
		FROM dbo.fnDetailFieldsShared(@ClientID) dft 
		INNER JOIN dbo.DetailFields dfr ON dfr.DetailFieldPageID = dft.ResourceListDetailFieldPageID 
		WHERE dft.[DetailFieldPageID] = @TableDetailFieldPageID 
		AND dft.QuestionTypeID = 14 

	)
	ORDER BY df.QuestionTypeID, df.FieldCaption

	Select @@ROWCOUNT

END			




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForTableParsing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetForTableParsing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetForTableParsing] TO [sp_executeall]
GO
