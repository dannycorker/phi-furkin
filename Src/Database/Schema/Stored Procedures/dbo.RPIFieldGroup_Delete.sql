SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldGroup_Delete]
(

	@RPIFieldGroupID int   
)
AS


				DELETE FROM [dbo].[RPIFieldGroup] WITH (ROWLOCK) 
				WHERE
					[RPIFieldGroupID] = @RPIFieldGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldGroup_Delete] TO [sp_executeall]
GO
