SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-03-20
-- Description:	Gets key dates for the claim and policy for validation
-- Used by:		DF 147708
-- JEL 2019-02-25 changed function call to fn_C600_Policy_GetPolicyDetails to cater for client specific OMF config
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetKeyDates] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @DateOfOnset DATE,
			@TreatmentStart DATE,
			@PolicyInception DATE,
			@FlagClaimXdaysFromInception INT,
			@CurrentPolicyMatterID INT
			
	
	SELECT @DateOfOnset = ValueDate
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144892
	
	SELECT @TreatmentStart = ValueDate
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 144366	
	
	/*JEL changed to client Specific*/ 
	SELECT @PolicyInception = PolicyInception
	FROM dbo.fn_C600_Policy_GetPolicyDetails(@MatterID)
	--FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
	
	
	-- Lookup config
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	SELECT @FlagClaimXdaysFromInception = cdv.ValueInt
	FROM dbo.Cases c WITH (NOLOCK) 
	INNER JOIN dbo.CaseDetailValues cdv WITH (NOLOCK) ON c.CaseID = cdv.CaseID AND cdv.DetailFieldID = 152878
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID
	WHERE m.MatterID = @CurrentPolicyMatterID
	
	DECLARE @TreatmentEnd DATE
	
	SELECT @TreatmentEnd = ValueDate
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 145674
	
	DECLARE @PolicyAtStart INT,
			@PolicyAtEnd INT,
			@PolicyChanged BIT = 0
			
	SELECT @PolicyAtStart = dbo.fn_C00_1272_GetPolicyRowFromClaimAndDate(@MatterID, @TreatmentStart)	
	SELECT @PolicyAtEnd = dbo.fn_C00_1272_GetPolicyRowFromClaimAndDate(@MatterID, @TreatmentEnd)	
	IF @PolicyAtStart != @PolicyAtEnd	
	BEGIN
		SELECT @PolicyChanged = 1
	END
			
	
			
	SELECT	@DateOfOnset AS DateOfOnset, @TreatmentStart AS TreatmentStart, @PolicyInception AS PolicyInception, 
			@FlagClaimXdaysFromInception AS FlagClaimXdaysFromInception, @PolicyChanged AS PolicyChanged
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetKeyDates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetKeyDates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetKeyDates] TO [sp_executeall]
GO
