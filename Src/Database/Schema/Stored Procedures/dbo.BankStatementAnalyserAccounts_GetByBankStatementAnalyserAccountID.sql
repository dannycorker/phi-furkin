SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the BankStatementAnalyserAccounts table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BankStatementAnalyserAccounts_GetByBankStatementAnalyserAccountID]
(

	@BankStatementAnalyserAccountID int   
)
AS


				SELECT
					[BankStatementAnalyserAccountID],
					[ClientID],
					[BankStatementPageCount],
					[CostPerPage],
					[LastResetDate]
				FROM
					[dbo].[BankStatementAnalyserAccounts] WITH (NOLOCK) 
				WHERE
										[BankStatementAnalyserAccountID] = @BankStatementAnalyserAccountID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_GetByBankStatementAnalyserAccountID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStatementAnalyserAccounts_GetByBankStatementAnalyserAccountID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_GetByBankStatementAnalyserAccountID] TO [sp_executeall]
GO
