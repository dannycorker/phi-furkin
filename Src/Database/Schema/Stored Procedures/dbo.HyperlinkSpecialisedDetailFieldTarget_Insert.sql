SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the HyperlinkSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget_Insert]
(

	@HyperlinkSpecialisedDetailFieldTargetID int    OUTPUT,

	@ClientID int   ,

	@HyperlinkDetailFieldID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@RowField int   ,

	@Notes varchar (250)  
)
AS


				
				INSERT INTO [dbo].[HyperlinkSpecialisedDetailFieldTarget]
					(
					[ClientID]
					,[HyperlinkDetailFieldID]
					,[Target]
					,[DetailFieldID]
					,[ColumnField]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[ColumnFieldAlias]
					,[RowField]
					,[Notes]
					)
				VALUES
					(
					@ClientID
					,@HyperlinkDetailFieldID
					,@Target
					,@DetailFieldID
					,@ColumnField
					,@TemplateTypeID
					,@DetailFieldAlias
					,@ColumnFieldAlias
					,@RowField
					,@Notes
					)
				-- Get the identity value
				SET @HyperlinkSpecialisedDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Insert] TO [sp_executeall]
GO
