SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientDetailValues_GetByClientIDDetailFieldID]
(

	@ClientID int   ,

	@DetailFieldID int   
)
AS


				SELECT
					[ClientDetailValueID],
					[ClientID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[ClientDetailValues] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
					AND [DetailFieldID] = @DetailFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_GetByClientIDDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientDetailValues_GetByClientIDDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientDetailValues_GetByClientIDDetailFieldID] TO [sp_executeall]
GO
