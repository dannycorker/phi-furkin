SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the TimeUnits table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TimeUnits_Insert]
(

	@TimeUnitsID int    OUTPUT,

	@TimeUnitsName varchar (50)  ,

	@SQLTimeUnits varchar (50)  
)
AS


				
				INSERT INTO [dbo].[TimeUnits]
					(
					[TimeUnitsName]
					,[SQLTimeUnits]
					)
				VALUES
					(
					@TimeUnitsName
					,@SQLTimeUnits
					)
				-- Get the identity value
				SET @TimeUnitsID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimeUnits_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeUnits_Insert] TO [sp_executeall]
GO
