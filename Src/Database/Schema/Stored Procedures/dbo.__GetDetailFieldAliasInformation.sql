SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 02-11-2011
-- Description:	Gets all the detail field aliases for the given
--				client and leadtype. Assembles the resource list aliases
-- =============================================
CREATE PROCEDURE [dbo].[__GetDetailFieldAliasInformation]

	@LeadTypeID int,
	@ClientID int

AS
BEGIN

	IF(@LeadTypeID IS NULL)
	BEGIN

		SELECT df_base.FieldName, df_col.DetailFieldID, df_col.FieldName ColumnFieldName, '[!A:' + dfa_base.DetailFieldAlias + ', ' +  dfa_col.DetailFieldAlias + ']' AS [ALIAS]
		FROM DetailFields df_base WITH (NOLOCK)
		INNER JOIN DetailFieldAlias dfa_base WITH (NOLOCK) ON dfa_base.DetailFieldID = df_base.DetailFieldID
		INNER JOIN DetailFields df_col ON df_col.DetailFieldPageID = df_base.ResourceListDetailFieldPageID
		INNER JOIN DetailFieldAlias dfa_col WITH (NOLOCK) ON dfa_col.DetailFieldID = df_col.DetailFieldID
		WHERE df_base.ClientID = @ClientID AND dfa_base.LeadTypeID is null
		AND df_base.QuestionTypeID = 14

		UNION

		SELECT DISTINCT df_base.FieldName, NULL, NULL, '[!A:' + dfa_base.DetailFieldAlias + ']' AS [ALIAS]
		FROM DetailFields df_base WITH (NOLOCK)
		INNER JOIN DetailFieldAlias dfa_base WITH (NOLOCK) ON dfa_base.DetailFieldID = df_base.DetailFieldID
		WHERE df_base.ClientID = @ClientID AND dfa_base.LeadTypeID is null
		ORDER BY df_Base.FieldName, ALIAS
		
	END
	ELSE
	BEGIN
	
		SELECT df_base.FieldName, df_col.DetailFieldID, df_col.FieldName ColumnFieldName, '[!A:' + dfa_base.DetailFieldAlias + ', ' +  dfa_col.DetailFieldAlias + ']' AS [ALIAS]
		FROM DetailFields df_base WITH (NOLOCK)
		INNER JOIN DetailFieldAlias dfa_base WITH (NOLOCK) ON dfa_base.DetailFieldID = df_base.DetailFieldID
		INNER JOIN DetailFields df_col ON df_col.DetailFieldPageID = df_base.ResourceListDetailFieldPageID
		INNER JOIN DetailFieldAlias dfa_col WITH (NOLOCK) ON dfa_col.DetailFieldID = df_col.DetailFieldID
		WHERE df_base.ClientID = @ClientID AND dfa_base.LeadTypeID = @LeadTypeID
		AND df_base.QuestionTypeID = 14

		UNION

		SELECT DISTINCT df_base.FieldName, NULL, NULL, '[!A:' + dfa_base.DetailFieldAlias + ']' AS [ALIAS]
		FROM DetailFields df_base WITH (NOLOCK)
		INNER JOIN DetailFieldAlias dfa_base WITH (NOLOCK) ON dfa_base.DetailFieldID = df_base.DetailFieldID
		WHERE df_base.ClientID = @ClientID AND dfa_base.LeadTypeID = @LeadTypeID
		ORDER BY df_Base.FieldName, ALIAS
			
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__GetDetailFieldAliasInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetDetailFieldAliasInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetDetailFieldAliasInformation] TO [sp_executeall]
GO
