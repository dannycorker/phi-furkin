SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChartSeries table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_Insert]
(

	@ChartSeriesID int    OUTPUT,

	@ChartSeriesTypeID int   ,

	@ChartSeriesOrder int   ,

	@ChartID int   ,

	@ClientID int   ,

	@PanelItemChartingID int   ,

	@ColumnName varchar (100)  
)
AS


				
				INSERT INTO [dbo].[ChartSeries]
					(
					[ChartSeriesTypeID]
					,[ChartSeriesOrder]
					,[ChartID]
					,[ClientID]
					,[PanelItemChartingID]
					,[ColumnName]
					)
				VALUES
					(
					@ChartSeriesTypeID
					,@ChartSeriesOrder
					,@ChartID
					,@ClientID
					,@PanelItemChartingID
					,@ColumnName
					)
				-- Get the identity value
				SET @ChartSeriesID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_Insert] TO [sp_executeall]
GO
