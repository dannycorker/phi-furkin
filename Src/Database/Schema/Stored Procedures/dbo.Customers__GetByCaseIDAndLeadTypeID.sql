SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2014
-- Description:	Gets the customer with the case id and the lead type id
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByCaseIDAndLeadTypeID]

	@ClientID INT,
	@CaseID INT,
	@LeadTypeID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT c.* FROM Cases cs WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = cs.LeadID and l.LeadTypeID=@LeadTypeID
	INNER JOIN Customers c WITH (NOLOCK) on c.CustomerID = l.CustomerID
	WHERE cs.CaseID=@CaseID and cs.ClientID=@ClientID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByCaseIDAndLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByCaseIDAndLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByCaseIDAndLeadTypeID] TO [sp_executeall]
GO
