SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIClaimStatus table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus__GetByApplicationID]
(

	@ApplicationID varchar(150)   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT     
				RPIClaimStatusID, 
				MatterID, 
				ClientID, 
				ApplicationID, 
				ActivityEngineGuid, 
				PhaseCacheID, 
				PhaseCacheName, 
				WhoCreated, 
				WhenCreated, 
				WhoModified, 
                WhenModified,
                RPIAsUser,
                RPIUser,
                RPIPass,
                Notes
				FROM
					[dbo].[RPIClaimStatus]
				WHERE
					ApplicationID = @ApplicationID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus__GetByApplicationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus__GetByApplicationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus__GetByApplicationID] TO [sp_executeall]
GO
