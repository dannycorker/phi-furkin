SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptStandardTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptStandardTarget_GetByScriptStandardTargetID]
(

	@ScriptStandardTargetID int   
)
AS


				SELECT
					[ScriptStandardTargetID],
					[ScriptID],
					[ClientID],
					[Target],
					[ObjectName],
					[PropertyName]
				FROM
					[dbo].[ScriptStandardTarget] WITH (NOLOCK) 
				WHERE
										[ScriptStandardTargetID] = @ScriptStandardTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_GetByScriptStandardTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget_GetByScriptStandardTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_GetByScriptStandardTargetID] TO [sp_executeall]
GO
