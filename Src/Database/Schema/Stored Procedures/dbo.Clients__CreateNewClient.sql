SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By:  Ben Crinion
-- Purpose: Inserts a record into the Clients, ClientOffices and ClientPersonnel tables
-- Takes the password of the provided ClientPersonnelID for the new ClientPersonnel record.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients__CreateNewClient]
(
	@CompanyName				nvarchar(100) = NULL,
	@WebAddress					nvarchar(255) = NULL,

	@BuildingName				nvarchar(50) = NULL,
	@Address1					nvarchar(200) = NULL,
	@Address2					nvarchar(200) = NULL,
	@Town						nvarchar(200) = NULL,
	@County						nvarchar(50) = NULL,
	@Country					nvarchar(200) = NULL,
	@PostCode					nvarchar(10) = NULL,
	@OfficeTelephone			nvarchar(50) = NULL,
	@OfficeTelephoneExtension	nvarchar(10) = NULL,

	@ClientPersonnelID			int,
	@TitleID					int = NULL,
	@FirstName					nvarchar(100) = NULL,
	@MiddleName					nvarchar(100) = NULL,
	@LastName					nvarchar(100) = NULL,
	@JobTitle					nvarchar(100) = NULL,
	@MobileTelephone			nvarchar(50) = NULL,
	@HomeTelephone				nvarchar(50) = NULL,
	@EmailAddress				nvarchar(255),
	@ChargeOutRate				money  = 0
)
AS
	
	DECLARE @ClientID					int
	DECLARE @OfficeID					int
	DECLARE @Password					nvarchar(65)
	DECLARE @Salt						nvarchar(25)
				
INSERT INTO [dbo].[Clients]
           ([CompanyName]
           ,[WebAddress]
           ,[IPAddress]
           ,[DefaultEmailAddress]
           ,[ClientTypeID]
           ,[AllowSMS])
     VALUES
           (@CompanyName
           ,@CompanyName
           ,null
           ,@EmailAddress
           ,1
           ,0)



set @ClientID = SCOPE_IDENTITY()

INSERT INTO [dbo].[ClientOffices]
           ([ClientID]
           ,[BuildingName]
           ,[Address1]
           ,[Address2]
           ,[Town]
           ,[County]
           ,[Country]
           ,[PostCode]
           ,[OfficeTelephone]
           ,[OfficeTelephoneExtension])
     VALUES
           (@ClientID
           ,@BuildingName			 
           ,@Address1				 
           ,@Address2				 
           ,@Town					 
           ,@County					 
           ,@Country				 
           ,@PostCode				 
           ,@OfficeTelephone		 
           ,@OfficeTelephoneExtension)

SET @OfficeID = SCOPE_IDENTITY()

SELECT @Password = Password, @Salt = Salt FROM ClientPersonnel where ClientPersonnelID = @ClientPersonnelID

INSERT INTO [dbo].[ClientPersonnel]
           ([ClientID]
           ,[ClientOfficeID]
           ,[TitleID]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[JobTitle]
           ,[Password]
           ,[ClientPersonnelAdminGroupID]
           ,[MobileTelephone]
           ,[HomeTelephone]
           ,[OfficeTelephone]
           ,[OfficeTelephoneExtension]
           ,[EmailAddress]
           ,[ChargeOutRate]
           ,[Salt]
           ,[AttemptedLogins]
           ,[AccountDisabled]
           ,[ManagerID])
     VALUES
           (@ClientID
           ,@OfficeID
           ,@TitleID					
           ,@FirstName					
           ,@MiddleName					
           ,@LastName					
           ,@JobTitle					
           ,@Password			
           ,1
           ,@MobileTelephone			
           ,@HomeTelephone				
           ,@OfficeTelephone			
           ,@OfficeTelephoneExtension	
           ,@EmailAddress				
           ,@ChargeOutRate				
           ,@Salt						
           ,0			
           ,0		
           ,null)
									
							
			






GO
GRANT VIEW DEFINITION ON  [dbo].[Clients__CreateNewClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients__CreateNewClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients__CreateNewClient] TO [sp_executeall]
GO
