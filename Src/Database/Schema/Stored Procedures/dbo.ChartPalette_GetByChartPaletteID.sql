SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartPalette table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartPalette_GetByChartPaletteID]
(

	@ChartPaletteID int   
)
AS


				SELECT
					[ChartPaletteID],
					[ChartPaletteName],
					[ChartPalettePreviewImageUrl]
				FROM
					[dbo].[ChartPalette] WITH (NOLOCK) 
				WHERE
										[ChartPaletteID] = @ChartPaletteID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_GetByChartPaletteID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartPalette_GetByChartPaletteID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_GetByChartPaletteID] TO [sp_executeall]
GO
