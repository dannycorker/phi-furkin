SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFile table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFile_GetByFileStatusID]
(

	@FileStatusID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderFileID],
					[ClientID],
					[DataLoaderMapID],
					[FileStatusID],
					[SourceFileNameAndPath],
					[TargetFileName],
					[TargetFileLocation],
					[FileFormatID],
					[ScheduledDateTime],
					[DataLoadedDateTime],
					[UploadedBy],
					[RowsInFile]
				FROM
					[dbo].[DataLoaderFile] WITH (NOLOCK) 
				WHERE
					[FileStatusID] = @FileStatusID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_GetByFileStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile_GetByFileStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_GetByFileStatusID] TO [sp_executeall]
GO
