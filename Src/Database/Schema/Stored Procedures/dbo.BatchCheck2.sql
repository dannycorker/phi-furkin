SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-10
-- Description:	Quick check of current tasks
-- =============================================
CREATE PROCEDURE [dbo].[BatchCheck2]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ATID int

	SELECT TOP 1 @ATID = atp.taskid 
	from dbo.automatedtaskparam atp (nolock) 
	where atp.paramname = 'ALREADY_RUNNING' 
	and atp.paramvalue = '1'
	
	IF @ATID > 0
	BEGIN
		EXEC dbo.AutomatedTask__Describe @ATID
	END
	ELSE
	BEGIN
		PRINT 'No tasks are currently running'
	END
	END





GO
GRANT VIEW DEFINITION ON  [dbo].[BatchCheck2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BatchCheck2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BatchCheck2] TO [sp_executeall]
GO
