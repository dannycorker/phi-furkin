SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the MasterQuestionStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestionStatus_Delete]
(

	@MasterQuestionStatusID int   
)
AS


				DELETE FROM [dbo].[MasterQuestionStatus] WITH (ROWLOCK) 
				WHERE
					[MasterQuestionStatusID] = @MasterQuestionStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestionStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Delete] TO [sp_executeall]
GO
