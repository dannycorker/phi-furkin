SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the IncendiaConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_Delete]
(

	@IncendiaConfigurationClientID int   
)
AS


				DELETE FROM [dbo].[IncendiaConfigurationClient] WITH (ROWLOCK) 
				WHERE
					[IncendiaConfigurationClientID] = @IncendiaConfigurationClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Delete] TO [sp_executeall]
GO
