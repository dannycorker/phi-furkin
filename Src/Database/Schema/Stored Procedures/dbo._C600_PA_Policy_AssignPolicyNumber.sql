SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-08-08
-- Description:	Take a PolicyMatterID and assign its policy number
-- ARH  Updated Line 66 to use function to determine what database it's running on to return the correct clientpersonnelid
-- 2018-04-09 GPR					| Fetch the Affinity from @PolicyMatterID
-- 2018-04-09 GPR					| Fetch SchemeType from @PolicyMatterID
-- 2018-04-10 GPR					| C600-B OMF Policy Number Config (non conversion)
-- 2018-05-03 GPR					| C600-B OMF Policy Number Config (conversion)
-- 2018-06-21 GPR					| C600-B OMF policy number config (conversion) rework: attempts to find match on customerdob and name
-- 2018-06-22 GPR					| brought OMF conversion config into the C600-B if block
-- 2018-06-27 GPR					| further modification to OMF policy number so that an initial OMF policy gets its PS01 format
-- 2018-08-08 ARH					| Updated to include ASDA Affinity Policy Number (Row 129)
-- 2019-03-10 GPR					| Altered buddies policy number following CR (slide 23 of requirements)
-- 2019-11-04 CPS for JIRA LPC-109	| Replace with LPC policy number rules, accept a passed in @AffinityID, return the @PolicyNumber directly
-- 2020-01-13 CPS for JIRA LPC-356	| Remove temp XML logging
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_AssignPolicyNumber]
(
	 @PolicyMatterID			INT
	,@LeadEventID				INT = NULL
	,@PolicyNumber				VARCHAR(2000) OUTPUT
	,@AffinityResourceListID	INT = NULL
)
AS
BEGIN
 
	SET NOCOUNT ON;
	
	DECLARE  @PolicyLeadID			INT
			,@CustomerID			INT
			,@ErrorMessage			VARCHAR(2000)
			,@EventComments			VARCHAR(2000) = ''
			,@CaseCreatedBy			INT
			,@WhoCreated			INT
			,@LogXmlEntry			XML
			,@PolicyNumberPrefix	VARCHAR(10)
	
	SELECT   @PolicyLeadID	= l.LeadID
			,@CustomerID	= m.CustomerID
			,@CaseCreatedBy = c.WhoCreated
	FROM Matter m WITH ( NOLOCK ) 
	INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID
	INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
	WHERE m.MatterID = @PolicyMatterID
	AND l.LeadTypeID = 1492 /*Policy Admin*/
	
	SELECT @WhoCreated = le.WhoCreated
	FROM LeadEvent le WITH ( NOLOCK ) 
	WHERE le.LeadEventID = @LeadEventID

	IF @PolicyLeadID > 0
	BEGIN
		
		IF @AffinityResourceListID is NULL
		BEGIN
			SELECT @AffinityResourceListID = dbo.fnGetSimpleDvAsInt(170144,@CustomerID) /*Affinity Details*/
		END

		SELECT	 @PolicyNumber		 = NULLIF(dbo.fnGetSimpleDv(170050,@PolicyMatterID),'') /*Policy Number*/
				,@PolicyNumberPrefix = dbo.fnGetSimpleDv(313779,@AffinityResourceListID) /*Policy Number Prefix*/

		IF @PolicyNumber is NULL
		BEGIN
			SELECT @PolicyNumber = ISNULL(@PolicyNumberPrefix,'') + ISNULL(CONVERT(VARCHAR,@PolicyMatterID),'NULL')
		
			SELECT @EventComments += 'Policy Number changed from ' + ISNULL(l.LeadRef,'NULL') + ' to ' + ISNULL(@PolicyNumber,'NULL') + CHAR(13)+CHAR(10)
			FROM Lead l WITH ( NOLOCK )
			WHERE l.LeadID = @PolicyLeadID
		
			UPDATE l
			SET LeadRef = @PolicyNumber
			FROM Lead l WITH ( NOLOCK ) 
			WHERE l.LeadID = @PolicyLeadID
		
			EXEC dbo._C00_SimpleValueIntoField 170050, @PolicyNumber, @PolicyMatterID, @WhoCreated /*Actual Policy Number*/
		END
		ELSE
		BEGIN
			SELECT @EventComments += 'Existing Policy Number ' + @PolicyNumber + ' retained.'
		END
				
		IF @LeadEventID > 0
		BEGIN
			EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		END
	END
	ELSE
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error: No PolicyAdmin LeadID found for ' + ISNULL(CONVERT(VARCHAR,@PolicyMatterID),'NULL') + '</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_AssignPolicyNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_AssignPolicyNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_AssignPolicyNumber] TO [sp_executeall]
GO
