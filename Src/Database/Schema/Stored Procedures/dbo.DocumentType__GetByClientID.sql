SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType__GetByClientID]
(

	@ClientID int   
)
AS


				SELECT *
				FROM
					[dbo].[DocumentType]
				WHERE
					[ClientID] = @ClientID
			Select @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByClientID] TO [sp_executeall]
GO
