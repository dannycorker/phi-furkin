SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-01-21
-- Description:	Add Resource From Lead Or Matter Page
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MakeResourceFromPage] 
	@FromDetailFieldPageID int , 
	@ResourceListDetailFieldPageID int,
	@CaseID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ClientID int, 
			@ResourceListID int,
			@LeadOrMatter int,
			@LeadID int,
			@MatterID int,
			@TestID int

	Select @ClientID = ClientID, @LeadOrMatter = LeadOrMatter
	From DetailFields 
	Where DetailFieldPageID = @FromDetailFieldPageID

	Select @LeadID = LeadID
	From Cases
	Where CaseID = @CaseID

	If @LeadOrMatter = 1
	Begin

		/* Create all necessary MatterDetailValues records that don't already exist */
		INSERT INTO ResourceList(ClientID) 
		SELECT @ClientID

		Select @ResourceListID = SCOPE_IDENTITY()

		INSERT INTO ResourceListDetailValues (ResourceListID, DetailFieldID, DetailValue, ClientID, LeadOrMatter)
		select @ResourceListID, dfb.DetailFieldID, leaddetailvalues.DetailValue, leaddetailvalues.ClientID, 4
		from leaddetailvalues
		inner join detailfieldalias dfa on dfa.detailfieldid = leaddetailvalues.detailfieldid
		inner join detailfieldalias dfb on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
		inner join detailfields df on df.detailfieldid = leaddetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
		inner join detailfields df_res on df_res.DetailFieldID = dfb.DetailFieldID and df_res.DetailFieldPageID = @ResourceListDetailFieldPageID
		where leadid = @LeadID 
	
	End

	If @LeadOrMatter = 2
	Begin

		Select @MatterID = 0, @TestID = 0

		Select Top 1 @MatterID = MatterID
		From Matter
		Where CaseID = @CaseID
		Order By MatterID

		while (@MatterID > 0 AND @MatterID > @TestID)
		Begin

			INSERT INTO ResourceList(ClientID) 
			SELECT @ClientID

			Select @ResourceListID = SCOPE_IDENTITY()

			INSERT INTO ResourceListDetailValues (ResourceListID, DetailFieldID, DetailValue, ClientID, LeadOrMatter)
			select @ResourceListID, dfb.DetailFieldID, MatterDetailValues.DetailValue, MatterDetailValues.ClientID, 4
			from MatterDetailValues
			inner join detailfieldalias dfa on dfa.detailfieldid = MatterDetailValues.detailfieldid
			inner join detailfieldalias dfb on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
			inner join detailfields df on df.detailfieldid = MatterDetailValues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			inner join detailfields df_res on df_res.DetailFieldID = dfb.DetailFieldID and df_res.DetailFieldPageID = @ResourceListDetailFieldPageID
			where MatterDetailValues.MatterID = @MatterID 

			Select @TestID = @MatterID

			Select Top 1 @MatterID = MatterID
			From Matter
			Where CaseID = @CaseID
			And MatterID > @MatterID
			Order By MatterID

		End
	
	End

	Return @ResourceListID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MakeResourceFromPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MakeResourceFromPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MakeResourceFromPage] TO [sp_executeall]
GO
