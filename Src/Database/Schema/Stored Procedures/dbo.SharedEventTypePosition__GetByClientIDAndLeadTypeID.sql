SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-09-2014
-- Description:	Gets the visio x,y position of the shared event types
-- =============================================
CREATE PROCEDURE [dbo].[SharedEventTypePosition__GetByClientIDAndLeadTypeID]	
	@ClientID INT,
	@LeadTypeID INT		
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM SharedEventTypePosition WITH (NOLOCK) 
	WHERE LeadTypeID = @LeadTypeID AND ClientID = @ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SharedEventTypePosition__GetByClientIDAndLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SharedEventTypePosition__GetByClientIDAndLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SharedEventTypePosition__GetByClientIDAndLeadTypeID] TO [sp_executeall]
GO
