SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the GroupRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamicEditing_Insert]
(

	@GroupRightsDynamicEditingID int    OUTPUT,

	@ClientPersonnelAdminGroupID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				INSERT INTO [dbo].[GroupRightsDynamicEditing]
					(
					[ClientPersonnelAdminGroupID]
					,[FunctionTypeID]
					,[LeadTypeID]
					,[ObjectID]
					,[RightID]
					)
				VALUES
					(
					@ClientPersonnelAdminGroupID
					,@FunctionTypeID
					,@LeadTypeID
					,@ObjectID
					,@RightID
					)
				-- Get the identity value
				SET @GroupRightsDynamicEditingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamicEditing_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamicEditing_Insert] TO [sp_executeall]
GO
