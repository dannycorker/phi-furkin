SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-16
-- Description:	Class Node Children
-- =============================================
CREATE PROCEDURE [dbo].[ClassNode__GetChildren] 
	@ClientPersonnelID INT = NULL,
	@ClassNodeID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		cn.[ClassNodeID],
		cn.[ClientID],
		cn.[ParentClassNodeID],
		cn.[NodeType],
		cn.[Name],
		cn.[ClassDescription],
		cn.[NodeOrder],
		cn.[WhoCreated],
		cn.[WhenCreated],
		cn.[WhoModified],
		cn.[WhenModified]
	FROM 
		[dbo].[ClassNode] cn WITH (NOLOCK)
		INNER JOIN [ClientPersonnel] AS cp
		ON cp.[ClientID] = cn.[ClientID]
	WHERE 
		@ClassNodeID = cn.[ParentClassNodeID]
		AND @ClientPersonnelID = cp.ClientPersonnelID
	ORDER BY cn.[ClassNodeID]
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode__GetChildren] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode__GetChildren] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode__GetChildren] TO [sp_executeall]
GO
