SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyMappingTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingTemplate_Insert]
(

	@ThirdPartyMappingTemplateID int    OUTPUT,

	@ThirdPartyFieldGroupID int   ,

	@ThirdPartySystemID int   ,

	@Name varchar (50)  ,

	@Description varchar (255)  ,

	@XMLItemTemplate varchar (MAX)  ,

	@XMLContainerName varchar (MAX)  ,

	@XMLItemName varchar (MAX)  ,

	@XMLItemKey varchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ThirdPartyMappingTemplate]
					(
					[ThirdPartyFieldGroupID]
					,[ThirdPartySystemID]
					,[Name]
					,[Description]
					,[XMLItemTemplate]
					,[XMLContainerName]
					,[XMLItemName]
					,[XMLItemKey]
					)
				VALUES
					(
					@ThirdPartyFieldGroupID
					,@ThirdPartySystemID
					,@Name
					,@Description
					,@XMLItemTemplate
					,@XMLContainerName
					,@XMLItemName
					,@XMLItemKey
					)
				-- Get the identity value
				SET @ThirdPartyMappingTemplateID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingTemplate_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Insert] TO [sp_executeall]
GO
