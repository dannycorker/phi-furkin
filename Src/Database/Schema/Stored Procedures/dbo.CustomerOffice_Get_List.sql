SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CustomerOffice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_Get_List]

AS


				
				SELECT
					[OfficeID],
					[ClientID],
					[CustomerID],
					[OfficeName],
					[MainPhoneNumber],
					[FaxNumber],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Postcode],
					[Country],
					[Notes],
					[CountryID]
				FROM
					[dbo].[CustomerOffice] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_Get_List] TO [sp_executeall]
GO
