SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ============================================================================================
-- Author:		Simon Brushett
-- Create date: 2013-07-26
-- Description: Deletes a full ruleset all the way down through the child tables... be careful!
-- 
-- Updated:		2014-06-05
-- By:			Jan Wilson
--              Updated to ensure that all Output Coordinates are
--              also removed prior to removing any Parameter Options
-- CS			2016-11-10 Cast @RuleSetID as VARCHAR when comparing to the Value field of RuleParameters
-- ============================================================================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSets_Delete]
(
	@RuleSetID INT,
	@IgnoreCheck BIT = 0
)

AS
BEGIN
	
	SET NOCOUNT ON;

	-- Before proceeding, ensure that the rule set is not referenced by any rule parameters	
	IF @IgnoreCheck = 1 OR NOT EXISTS(SELECT * FROM dbo.RulesEngine_RuleParameters WHERE ParameterTypeID = 3 AND Value = CAST(@RuleSetID as VARCHAR))
	BEGIN
		DELETE c
		FROM dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) ON c.RuleOutputID = o.RuleOutputID
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = o.RuleID
		WHERE r.RuleSetID = @RuleSetID

		DELETE c
		FROM dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) ON c.ParameterOptionID = o.ParameterOptionID
		INNER JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = o.RuleParameterID
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON rp.RuleID = r.RuleID
		WHERE r.RuleSetID = @RuleSetID

		DELETE o 
		FROM dbo.RulesEngine_RuleOutputs o
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = o.RuleID
		WHERE r.RuleSetID = @RuleSetID
		
		DELETE o 
		FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON o.RuleParameterID = p.RuleParameterID
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = p.RuleID
		WHERE r.RuleSetID = @RuleSetID
		
		DELETE p 
		FROM dbo.RulesEngine_RuleParameters p
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = p.RuleID
		WHERE r.RuleSetID = @RuleSetID
		
		DELETE dbo.RulesEngine_Rules
		WHERE RuleSetID = @RuleSetID
		
		DELETE dbo.RulesEngine_RuleSets
		WHERE RuleSetID = @RuleSetID
	END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSets_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_Delete] TO [sp_executeall]
GO
