SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_Update]
(

	@SqlQueryEditingID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@WhoIsEditing int   ,

	@EditStartedAt datetime   ,

	@QueryText varchar (MAX)  ,

	@QueryTitle varchar (250)  ,

	@LeadTypeID int   ,

	@FolderID int   ,

	@ParentQueryID int   ,

	@LockAllTables bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryEditing]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[WhoIsEditing] = @WhoIsEditing
					,[EditStartedAt] = @EditStartedAt
					,[QueryText] = @QueryText
					,[QueryTitle] = @QueryTitle
					,[LeadTypeID] = @LeadTypeID
					,[FolderID] = @FolderID
					,[ParentQueryID] = @ParentQueryID
					,[LockAllTables] = @LockAllTables
				WHERE
[SqlQueryEditingID] = @SqlQueryEditingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_Update] TO [sp_executeall]
GO
