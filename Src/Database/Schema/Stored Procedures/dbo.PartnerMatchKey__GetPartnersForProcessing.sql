SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Gets a record in CustomerMatchKey by the customerID
-- =============================================
CREATE PROCEDURE [dbo].[PartnerMatchKey__GetPartnersForProcessing]
	
	@ClientID INT

AS
BEGIN

	SELECT p.PartnerID, p.FirstName, p.LastName, 
	CASE WHEN p.UseCustomerAddress=1 THEN c.Address1 ELSE p.Address1 END AS Address1,
	CASE WHEN p.UseCustomerAddress=1 THEN c.PostCode ELSE p.PostCode END AS Postcode, 
	p.DateOfBirth
	FROM Partner p WITH (NOLOCK) 
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = p.CustomerID	
	WHERE p.ClientID = @ClientID and 
	PartnerID not in (select PartnerID from PartnerMatchKey WITH (NOLOCK) WHERE ClientID=@ClientID)	
	ORDER BY p.PartnerID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__GetPartnersForProcessing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PartnerMatchKey__GetPartnersForProcessing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__GetPartnersForProcessing] TO [sp_executeall]
GO
