SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Parse all objects for a template. Returns a table of substitutions to make.
-- JWG 2010-12-24 Added simple multiple-recipient email address handling
-- JWG 2011-01-11 Added Customer/Case/Client/ClientPersonnel/ContactDetailValues
-- JWG 2012-01-16 Added SubClientCompanyName (for HR)
-- PR  2013-04-29 Added Hyperlink detail field types
-- PR  2014-06-24 Added format column
-- JWG 2014-06-24 #27329 Added translations throughout
-- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types 
-- ACE 2016-07-19 Added spanish unsubscribe wording
-- TD 2016-11-16 Added Italian unsubscribe wording -- maybe we should have a table
-- PR  2017-07-18 #44286 Added DocumentTypeVersionID
-- =============================================
CREATE PROCEDURE [dbo].[FieldParser_ParseTemplate] 
	@ClientPersonnelID INT, 
	@LeadID INT, 
	@CaseID INT, 
	@DocumentTypeID INT = NULL, 
	@PageTitleLeadTypeID INT = NULL, 
	@HyperlinkDetailFieldID INT = NULL,
	@DocumentTypeVersionID INT = NULL
	--WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadTypeID INT, 
	@ClientID INT, 
	@MatterID INT, 
	@ClientOfficeID INT, 
	@CaseOwnerID INT, 
	@CaseOwnerManagerID INT, 
	@ClientPersonnelManagerID INT, 
	@ClientPersonnelManagerOfficeID INT, 
	@ContactID INT, 
	@CustomerID INT, 
	@LanguageID INT = 1, 
	@CustomerOfficeID INT, 
	@DepartmentID INT, 
	@PartnerID INT, 
	/* Email-out multiple-recipient substitutions */
	@LeadOrMatterDetailFieldID INT,
	@LeadOrMatter INT,
	@ColumnDetailFieldID INT, 
	@DMRTID INT = 0,
	@DMRTarget VARCHAR(250),
	@EmailAddressList VARCHAR(2000) = '', 
	@SubClientCompanyName VARCHAR(100) = '',  
	@UnsubscribeWording VARCHAR(2000) = 'Unsubscribe'
	
	DECLARE @Subs TABLE (
		LeadOrMatter TINYINT NULL,
		MatterID INT NULL,
		MultiMatter BIT NULL,
		DetailFieldID INT NULL,
		RawName VARCHAR(2000), 
		[Target] VARCHAR(2000), 
		RealValue VARCHAR(2000),
		IsSpecial BIT,
		ExcelSheetLocation VARCHAR(50),
		Format VARCHAR(2000)
	)


	SELECT @ClientID = cp.ClientID, 
	@ClientOfficeID = cp.ClientOfficeID, 
	@ClientPersonnelManagerID = cp.ManagerID, 
	@ClientPersonnelManagerOfficeID = cpm.ClientOfficeID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = cp.ManagerID 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID 
	
	/* -- JWG 2015-02-25 #31028 Bugfix aliased hyperlinks at Customer level as Helper fields, where LeadID is not set */
	IF @LeadID = 0 AND @CaseID > 0 AND @HyperlinkDetailFieldID > 0
	BEGIN
		SELECT @LeadID = ca.LeadID 
		FROM dbo.Cases ca WITH (NOLOCK) 
		WHERE ca.CaseID = @CaseID
	END
	
	/* 
		Get TOP (1) MatterID for the case.
		This is the default matter in case the template uses matter fields but
		the client happens to have multiple matters per case.
	*/
	SELECT TOP (1) 
	@ContactID = c.DefaultContactID, 
	@CustomerID = c.CustomerID, 
	@LanguageID = ISNULL(c.LanguageID, 1), /* JWG 2014-06-24 #27329 Default to English */
	@MatterID = m.MatterID, 
	@CustomerOfficeID = c.DefaultOfficeID, 
	@LeadTypeID = l.LeadTypeID, 
	@PartnerID = p.PartnerID, 
	@CaseOwnerID = l.AssignedTo, 
	@CaseOwnerManagerID = cpm.ManagerID  
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
	INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID /*AND ca.CaseID = @CaseID  */
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = ca.CaseID 
	LEFT JOIN dbo.[Partner] p WITH (NOLOCK) ON p.CustomerID = c.CustomerID 
	LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = l.AssignedTo 
	WHERE ca.CaseID = @CaseID/*l.LeadID = @LeadID */
	ORDER BY m.RefLetter 
	
	IF @LeadTypeID = 1875
	BEGIN
	
		SELECT @UnsubscribeWording = 'Abbestellen'
	
	END
	IF @LeadTypeID = 1931
	BEGIN
	
		SELECT @UnsubscribeWording = 'Darse de baja'
	
	END
	IF @LeadTypeID = 2027
	BEGIN
		
		SELECT @UnsubscribeWording = 'Link di cancellazione'
			
	END

	
	-- This index must be promoted to live
	-- IX_DocumentDetailFieldTarget_DocumentTypeID2017 NEEDS TO BE DEPLOYED TO LIVE ALONG WITH THIS CHANGE 

	-- Attempt to get a document type version identity if one has not been passed in (#44286)
	IF @DocumentTypeVersionID IS NULL OR @DocumentTypeVersionID <= 0
	BEGIN
		SELECT @DocumentTypeVersionID=dbo.fnGetActiveDocumentTypeVersion(@DocumentTypeID) 
	END

	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*                                                   DOCUMENT TYPE                                                 */
	/*                                                                                                                 */
	/*******************************************************************************************************************/
	IF @DocumentTypeID IS NOT NULL
	BEGIN
		/* LeadDetailValues */
		/* Fields may refer to a LookupList, in which case get the LookupListItems.ItemValue instead of the int. */
		/* JWG 2014-06-24 #27329 LEFT JOIN dbo.Translation throughout, in case the LookupListItem needs to be shown in a foreign language. */
		/* This is done by the COALESCE (t.Translation, luli1.ItemValue, '') in the SELECT statement. */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 1 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 1 
		LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased LeadDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 1 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 1 
		LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))


		/* MatterDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 2 AS [LeadOrMatter], 
		COALESCE(mdv1.MatterID, @MatterID) AS [MatterID], 
		CASE WHEN ddft.[Target] LIKE '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 2 
		LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID 
			AND mdv1.LeadID = @LeadID 
			AND (
					mdv1.MatterID = @MatterID 
					OR (
							ddft.[Target] LIKE '\[\[!%' ESCAPE '\' 
							AND mdv1.MatterID IN (
													SELECT m.MatterID 
													FROM dbo.Matter m WITH (NOLOCK) 
													WHERE m.CaseID = @CaseID
													) 
					) 
				)
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased MatterDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 2 AS [LeadOrMatter], 
		COALESCE(mdv1.MatterID, @MatterID) AS [MatterID], 
		CASE WHEN ddft.[Target] LIKE '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 2 
		LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID 
			AND mdv1.LeadID = @LeadID 
			AND (
					mdv1.MatterID = @MatterID 
					OR (
							ddft.[Target] LIKE '\[\[!%' ESCAPE '\' 
							AND mdv1.MatterID IN (
													SELECT m.MatterID 
													FROM dbo.Matter m WITH (NOLOCK) 
													WHERE m.CaseID = @CaseID
													) 
					) 
				)
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))
		
		
		/* CustomerDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 10 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 10 
		LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased CustomerDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 10 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 10 
		LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		
		/* CaseDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 11 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 11 
		LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased CaseDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 11 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 11 
		LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))
		
		
		/* ClientDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 12 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 12 
		LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased ClientDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 12 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 1 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 12 
		LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))
		
		
		/* ClientPersonnelDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 13 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 13 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased ClientPersonnelDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 13 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 2 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 13 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))
		
		
		/* ContactDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 14 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 14 
		LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))

		/* Aliased ContactDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation, Format) 
		SELECT 14 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0, 
		ddft.ExcelSheetLocation,
		ddft.Format 
		FROM dbo.DocumentDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 3 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 14 
		LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ((ddft.DocumentTypeID = @DocumentTypeID AND (ddft.DocumentTypeVersionID IS NULL OR ddft.DocumentTypeVersionID<=0)) OR (ddft.DocumentTypeVersionID = @DocumentTypeVersionID))
		
		
	END
	
	
	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*                                                LEAD TYPE PAGE TITLE                                             */
	/*                                                                                                                 */
	/*******************************************************************************************************************/
	IF @PageTitleLeadTypeID IS NOT NULL
	BEGIN
		/* LeadDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 1 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 1 
		LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased LeadDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 1 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN ldv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, ldv1.ValueDate, 103) END ELSE COALESCE (ldv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 1 
		LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = ldv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 


		/* MatterDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 2 AS [LeadOrMatter], 
		COALESCE(mdv1.MatterID, @MatterID) AS [MatterID], 
		CASE WHEN ddft.[Target] LIKE '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 2 
		LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] LIKE '\[\[!%' ESCAPE '\')
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased MatterDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 2 AS [LeadOrMatter], 
		COALESCE(mdv1.MatterID, @MatterID) AS [MatterID], 
		CASE WHEN ddft.[Target] LIKE '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN mdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, mdv1.ValueDate, 103) END ELSE COALESCE (mdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 2 
		LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] LIKE '\[\[!%' ESCAPE '\')
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 
		
		
		/* CustomerDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 10 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 10 
		LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased CustomerDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 10 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 10 
		LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 


		/* CaseDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 11 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 11 
		LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased CaseDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 11 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 11 
		LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 


		/* ClientDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 12 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 12 
		LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased ClientDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 12 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 12 
		LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 


		/* ClientPersonnelDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 13 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 13 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased ClientPersonnelDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 13 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 13 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 


		/* ContactDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 14 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 14 
		LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 

		/* Aliased ContactDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 14 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		CASE df1.[Lookup] & (~COALESCE (df1.[Encrypt], 0)) WHEN 1 THEN COALESCE (t.Translation, luli1.ItemValue, '') ELSE CASE df1.QuestionTypeID WHEN 5 THEN CASE WHEN cdv1.ValueDate IS NULL THEN '' ELSE CONVERT(VARCHAR, cdv1.ValueDate, 103) END ELSE COALESCE (cdv1.DetailValue, '') END END AS [DetailValue], 
		0 
		FROM dbo.LeadTypePageTitleDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 14 
		LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID 
		LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = cdv1.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli1.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.LeadTypeID = @PageTitleLeadTypeID 
		
	END
	

	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*											  HYPERLINK DETAIL FIELD TYPES										   */
	/*                                                                                                                 */
	/*******************************************************************************************************************/

	IF @HyperlinkDetailFieldID IS NOT NULL
	BEGIN
	
		/* LeadDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 1 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (ldv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 1 
		LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* Aliased LeadDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 1 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (ldv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 1 
		LEFT JOIN dbo.LeadDetailValues AS ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID = df1.DetailFieldID AND ldv1.LeadID = @LeadID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* MatterDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 2 AS [LeadOrMatter], 
		COALESCE(mdv1.MatterID, @MatterID) AS [MatterID], 
		CASE WHEN ddft.[Target] LIKE '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (mdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 2 
		LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] LIKE '\[\[!%' ESCAPE '\')
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* Aliased MatterDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 2 AS [LeadOrMatter], 
		COALESCE(mdv1.MatterID, @MatterID) AS [MatterID], 
		CASE WHEN ddft.[Target] LIKE '\[\[!%' ESCAPE '\' THEN 1 ELSE 0 END AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (mdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 2 
		LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.DetailFieldID = df1.DetailFieldID AND mdv1.LeadID = @LeadID AND (mdv1.MatterID = @MatterID OR ddft.[Target] LIKE '\[\[!%' ESCAPE '\')
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 		
		
		/* CustomerDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 10 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 10 
		LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID  

		/* Aliased CustomerDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 10 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 0 AND dfa.ClientID = @ClientID
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 10 
		LEFT JOIN dbo.CustomerDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CustomerID = @CustomerID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* CaseDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 11 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 11 
		LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* Aliased CaseDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 11 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = @LeadTypeID AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 11 
		LEFT JOIN dbo.CaseDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.CaseID = @CaseID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* ClientDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 12 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 12 
		LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* Aliased ClientDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 12 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 1 AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 12 
		LEFT JOIN dbo.ClientDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientID = @ClientID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* ClientPersonnelDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 13 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 13 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* Aliased ClientPersonnelDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 13 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 2 AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types  
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 13 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ClientPersonnelID = @ClientPersonnelID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID  

		/* ContactDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 14 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = ddft.DetailFieldID AND df1.LeadOrMatter = 14 
		LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 

		/* Aliased ContactDetailValues */
		INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, DetailFieldID, RawName, [Target], RealValue, IsSpecial) 
		SELECT 14 AS [LeadOrMatter], 
		0 AS [MatterID], 
		0 AS [MultiMatter], 
		df1.[DetailFieldID], 
		df1.[DetailFieldID], 
		ddft.[Target], 
		COALESCE (cdv1.DetailValue, '') AS [DetailValue], 
		0 
		FROM dbo.HyperlinkDetailFieldTarget ddft WITH (NOLOCK) 
		INNER JOIN dbo.DetailFieldAlias AS dfa WITH (NOLOCK) ON dfa.DetailFieldAlias = ddft.DetailFieldAlias AND dfa.LeadTypeID = 3 AND dfa.ClientID = @ClientID -- JWG 2015-02-10 #31028 Bugfix aliased hyperlink lead types  
		INNER JOIN dbo.DetailFields AS df1 WITH (NOLOCK) ON df1.DetailFieldID = dfa.DetailFieldID AND df1.LeadOrMatter = 14 
		LEFT JOIN dbo.ContactDetailValues AS cdv1 WITH (NOLOCK) ON cdv1.DetailFieldID = df1.DetailFieldID AND cdv1.ContactID = @ContactID 
		WHERE ddft.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 
	
	END
	
	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*                                                    COMMON CODE                                                  */
	/*                                                                                                                 */
	/*******************************************************************************************************************/
	/* Cases */
	IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Cases')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation
		FROM dbo.fnUnpivotCases(@CaseID) f 
		INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
		(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName) OR 
        (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName)
		
	END
	IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Cases')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
		FROM dbo.fnUnpivotCases(@CaseID) f 
		INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName
	END
	IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Cases')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
		FROM dbo.fnUnpivotCases(@CaseID) f 
		INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Cases' AND dst.PropertyName = f.RawColumnName
	END
	
	/* ClientOffices */
	IF @ClientOfficeID > 0
	BEGIN
		IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientOffices')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClientOffices(@ClientOfficeID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
		    (dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName) OR 
            (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientOffices')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientOffices(@ClientOfficeID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientOffices')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientOffices(@ClientOfficeID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientOffices' AND dst.PropertyName = f.RawColumnName
		END		
	END
	/* ClientOffices - Manager's Office */
	IF @ClientPersonnelManagerOfficeID > 0
	BEGIN
		IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientOffices')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClientOffices(@ClientPersonnelManagerOfficeID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName) OR 
            (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientOffices')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientOffices(@ClientPersonnelManagerOfficeID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientOffices')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientOffices(@ClientPersonnelManagerOfficeID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientOffices' AND dst.PropertyName = f.RawColumnName
		END		
	END		
	/* ClientPersonnel */
	IF @ClientPersonnelID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwner%'
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwner%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwner%'
		END				
	END
	/* ClientPersonnel - Manager */
	IF @ClientPersonnelManagerID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelManagerID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%') OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelManagerID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%'
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@ClientPersonnelManagerID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '[[]!CaseOwnerManager%'
		END		
	END
	/* ClientPersonnel - CaseOwner (Lead.AssignedTo */
	IF @CaseOwnerID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] LIKE '[[]!CaseOwner%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwner%') OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwner%')
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] LIKE '[[]!CaseOwner%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwner%'
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.[Target] LIKE '[[]!CaseOwner%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwner%'
		END
	END
	/* ClientPersonnel - CaseOwnerManager (Lead.AssignedTo */
	IF @CaseOwnerManagerID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] LIKE '[[]!CaseOwnerManager%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerManagerID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwnerManager%') OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwnerManager%')
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] LIKE '[[]!CaseOwnerManager%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerManagerID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwnerManager%'
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.[Target] LIKE '[[]!CaseOwnerManager%')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClientPersonnel(@CaseOwnerManagerID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'ManagerClientPersonnel' AND dst.PropertyName = f.RawColumnName AND dst.[Target] LIKE '[[]!CaseOwnerManager%'
		END		
	END	
	/* Clients */
	IF @ClientID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Clients')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotClients(@ClientID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Clients')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClients(@ClientID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Clients')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotClients(@ClientID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Clients' AND dst.PropertyName = f.RawColumnName
		END		
	END
	/* Contact */
	IF @ContactID > 0 
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Contact')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotContact(@ContactID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Contact')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotContact(@ContactID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Contact')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotContact(@ContactID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Contact' AND dst.PropertyName = f.RawColumnName
		END		
	END
	/* CustomerOffice */
	IF @CustomerOfficeID > 0 
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'CustomerOffice')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotCustomerOffice(@CustomerOfficeID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'CustomerOffice')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotCustomerOffice(@CustomerOfficeID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'CustomerOffice')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotCustomerOffice(@CustomerOfficeID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'CustomerOffice' AND dst.PropertyName = f.RawColumnName
		END		
	END
	/* Customers */
	IF @CustomerID > 0 
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Customers')
		BEGIN
			/* For SMS templates only, keep mobile number in [square brackets] for the app */
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], CASE 
				WHEN dst.[Target] LIKE '\[\[!Mobile\]\]' ESCAPE '\' AND dst.TemplateTypeID = 5 AND dst.IsSpecial = 0 
				THEN '[' + f.RealValue + ']' 
				WHEN dst.[Target] LIKE '\[\[!Email\]\]' ESCAPE '\' AND dst.TemplateTypeID IN (5, 6, 7) AND dst.IsSpecial = 0 
				THEN '[' + f.RealValue + ']' 
				ELSE f.RealValue END, 
				dst.IsSpecial, 
				dst.ExcelSheetLocation 
			--SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial 
			FROM dbo.fnUnpivotCustomers(@CustomerID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Customers')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotCustomers(@CustomerID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Customers')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotCustomers(@CustomerID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Customers' AND dst.PropertyName = f.RawColumnName
		END		
	END
	/* Department */
	IF @DepartmentID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Department')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotDepartment(@DepartmentID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Department')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotDepartment(@DepartmentID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Department')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotDepartment(@DepartmentID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Department' AND dst.PropertyName = f.RawColumnName
		END		
	END
	/* DocumentType - no implementation for PageTitleLeadTypeID and HyperlinkDetailFieldID here */
	IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'DocumentType')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
		FROM dbo.fnUnpivotDocumentType(@DocumentTypeID) f 
		INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
		(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'DocumentType' AND dst.PropertyName = f.RawColumnName) OR 
        (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'DocumentType' AND dst.PropertyName = f.RawColumnName)
	END	
	/* Lead - LeadID is always present */
	IF @DocumentTypeID > 0 AND EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Lead')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
		FROM dbo.fnUnpivotLead(@LeadID) f 
		INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
		(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName) OR 
        (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName)
	END
	IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Lead')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
		FROM dbo.fnUnpivotLead(@LeadID) f 
		INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName
	END
	IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Lead')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
		FROM dbo.fnUnpivotLead(@LeadID) f 
		INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Lead' AND dst.PropertyName = f.RawColumnName
	END	
	
	/* LeadType - always present. Even if @PageTitleLeadTypeID is passed in, it will be the same as @LeadTypeID */
	IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'LeadType')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
		FROM dbo.fnUnpivotLeadType(@LeadTypeID) f 
		INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
		(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName) OR 
        (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName)
	END
	IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'LeadType')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
		FROM dbo.fnUnpivotLeadType(@LeadTypeID) f 
		INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName
	END
	IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'LeadType')
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
		FROM dbo.fnUnpivotLeadType(@LeadTypeID) f 
		INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'LeadType' AND dst.PropertyName = f.RawColumnName
	END
	
	/* Matter */
	IF @MatterID > 0 
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Matter')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotMatter(@MatterID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\') OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\')
		
			/* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
			INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT 2, m.MatterID, 1, 'MatterID', dst.[Target], m.MatterID, 0, dst.ExcelSheetLocation 
			FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) 
			CROSS JOIN dbo.Matter m WITH (NOLOCK) 
			WHERE (dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\' AND m.CaseID = @CaseID) OR 
				  (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\' AND m.CaseID = @CaseID)
		
			/* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
			INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT 2, m.MatterID, 1, 'MatterRef', dst.[Target], COALESCE(m.MatterRef, ''), 0, dst.ExcelSheetLocation 
			FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) 
			CROSS JOIN dbo.Matter m WITH (NOLOCK) 
			WHERE (dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\' AND m.CaseID = @CaseID) OR 
				  (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\' AND m.CaseID = @CaseID)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotMatter(@MatterID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName
			AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\' /* Exclude multi [[!MatterID]] and [[!MatterRef]] from this single result set */
		
			/* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
			INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial) 
			SELECT 2, m.MatterID, 1, 'MatterID', dst.[Target], m.MatterID, 0 
			FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) 
			CROSS JOIN dbo.Matter m WITH (NOLOCK) 
			WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\' 
			AND m.CaseID = @CaseID 
		
			/* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
			INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial) 
			SELECT 2, m.MatterID, 1, 'MatterRef', dst.[Target], COALESCE(m.MatterRef, ''), 0 
			FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) 
			CROSS JOIN dbo.Matter m WITH (NOLOCK) 
			WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\' 
			AND m.CaseID = @CaseID 
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotMatter(@MatterID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter' AND dst.PropertyName = f.RawColumnName
			AND dst.[Target] NOT LIKE '\[\[!%' ESCAPE '\' /* Exclude multi [[!MatterID]] and [[!MatterRef]] from this single result set */
		
			/* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
			INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial) 
			SELECT 2, m.MatterID, 1, 'MatterID', dst.[Target], m.MatterID, 0 
			FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) 
			CROSS JOIN dbo.Matter m WITH (NOLOCK) 
			WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterID\]\]' ESCAPE '\' 
			AND m.CaseID = @CaseID 
		
			/* Multi Matter records for special cases [[!MatterID]] and [[!MatterRef]] */
			INSERT INTO @Subs(LeadOrMatter, MatterID, MultiMatter, RawName, [Target], RealValue, IsSpecial) 
			SELECT 2, m.MatterID, 1, 'MatterRef', dst.[Target], COALESCE(m.MatterRef, ''), 0 
			FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) 
			CROSS JOIN dbo.Matter m WITH (NOLOCK) 
			WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Matter' AND dst.[Target] LIKE '\[\[!MatterRef\]\]' ESCAPE '\' 
			AND m.CaseID = @CaseID 
		END				
	END
	
	
	/* Partner */
	IF @PartnerID > 0
	BEGIN
		IF EXISTS(SELECT * FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) WHERE dst.DocumentTypeID = @DocumentTypeID AND dst.ObjectName = 'Partner')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, dst.IsSpecial, dst.ExcelSheetLocation 
			FROM dbo.fnUnpivotPartner(@PartnerID) f 
			INNER JOIN dbo.DocumentStandardTarget dst WITH (NOLOCK) ON 
			(dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName) OR 
			(dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName)
		END
		IF @PageTitleLeadTypeID > 0 AND EXISTS(SELECT * FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) WHERE dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Partner')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotPartner(@PartnerID) f 
			INNER JOIN dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) ON dst.LeadTypeID = @PageTitleLeadTypeID AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName
		END
		IF @HyperlinkDetailFieldID > 0 AND EXISTS(SELECT * FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Partner')
		BEGIN
			INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
			SELECT f.RawColumnName, dst.[Target], f.RealValue, 0 
			FROM dbo.fnUnpivotPartner(@PartnerID) f 
			INNER JOIN dbo.HyperlinkStandardTarget dst WITH (NOLOCK) ON dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID AND dst.ObjectName = 'Partner' AND dst.PropertyName = f.RawColumnName
		END		
	END	
	
	/* Special Fields - these all have a blank ObjectName */
	/*INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
	SELECT dst.[Target], dst.[Target], '' 
	FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) 
	WHERE dst.DocumentTypeID = @DocumentTypeID 
	AND dst.ObjectName = ''*/

	
	IF @DocumentTypeID > 0
	BEGIN
		/*
			Substitute Lead security hash code into hyperlinks etc
			'http://www.thejetservice.com/supplierProfile.html?pl=true&cid=1255477&h=0x01765441913909.00'	
		*/
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT dst.[Target], dst.[Target], dbo.fnHashLeadID(@LeadID), dst.IsSpecial, dst.ExcelSheetLocation 
		FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) 
		WHERE (dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.[Target] LIKE '\[!LeadSecurityHash\]' ESCAPE '\') OR 
			  (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.[Target] LIKE '\[!LeadSecurityHash\]' ESCAPE '\')
		
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT dst.[Target], dst.[Target], '<a href=' + dbo.fnGenerateUnsubscribeLink(@LeadID) + '>Unsubscribe</a>', dst.IsSpecial, dst.ExcelSheetLocation 
		FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) 
		WHERE (dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND dst.[Target] LIKE '\[!UnsubscribeLink\]' ESCAPE '\') OR 
			  (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND dst.[Target] LIKE '\[!UnsubscribeLink\]' ESCAPE '\')
	END
	
	
	/*
		SubClient is an unusual requirement - parse the CompanyName if applicable.
	*/
	IF EXISTS 
	(
		SELECT * 
		FROM dbo.DocumentStandardTarget f WITH (NOLOCK) 
		WHERE (f.DocumentTypeID = @DocumentTypeID AND (f.DocumentTypeVersionID IS NULL OR f.DocumentTypeVersionID<=0) AND f.ObjectName = 'SubClient' AND f.PropertyName = 'CompanyName') OR 
              (f.DocumentTypeVersionID = @DocumentTypeVersionID AND f.ObjectName = 'SubClient' AND f.PropertyName = 'CompanyName')
	)
	BEGIN
		/* Get the SubClientID from the Customer and look up the name in the SubClient table */
		SELECT @SubClientCompanyName = sc.CompanyName 
		FROM dbo.Customers c WITH (NOLOCK) 
		INNER JOIN dbo.SubClient sc WITH (NOLOCK) ON sc.SubClientID = c.SubClientID 
		WHERE c.CustomerID = @CustomerID 
		
		/* Pretend that this is a standard substitution */
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT f.[Target], f.[Target], ISNULL(@SubClientCompanyName, ''), 0
		FROM dbo.DocumentStandardTarget f WITH (NOLOCK) 
		WHERE (f.DocumentTypeID = @DocumentTypeID AND (f.DocumentTypeVersionID IS NULL OR f.DocumentTypeVersionID<=0) AND f.ObjectName = 'SubClient' AND f.PropertyName = 'CompanyName') OR 
              (f.DocumentTypeVersionID = @DocumentTypeVersionID AND f.ObjectName = 'SubClient' AND f.PropertyName = 'CompanyName')
	END
			
	/* Missing Fields - anything that failed to be resolved by the select statements above (eg this Customer has no Partner) */
	IF @DocumentTypeID > 0
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial, ExcelSheetLocation) 
		SELECT dst.[Target], dst.[Target], '', dst.IsSpecial, dst.ExcelSheetLocation 
		FROM dbo.DocumentStandardTarget dst WITH (NOLOCK) 
		WHERE (dst.DocumentTypeID = @DocumentTypeID AND (dst.DocumentTypeVersionID IS NULL OR dst.DocumentTypeVersionID<=0) AND NOT EXISTS (SELECT * FROM @Subs s WHERE s.[Target] = dst.[Target])) OR 
              (dst.DocumentTypeVersionID = @DocumentTypeVersionID AND NOT EXISTS (SELECT * FROM @Subs s WHERE s.[Target] = dst.[Target]))
	END
	IF @PageTitleLeadTypeID > 0
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT dst.[Target], dst.[Target], '', 0 
		FROM dbo.LeadTypePageTitleStandardTarget dst WITH (NOLOCK) 
		WHERE dst.LeadTypeID = @PageTitleLeadTypeID 
		AND NOT EXISTS 
		(
			SELECT * 
			FROM @Subs s 
			WHERE s.[Target] = dst.[Target]
		)
	END
	IF @HyperlinkDetailFieldID > 0
	BEGIN
		INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
		SELECT dst.[Target], dst.[Target], '', 0 
		FROM dbo.HyperlinkStandardTarget dst WITH (NOLOCK) 
		WHERE dst.HyperlinkDetailFieldID = @HyperlinkDetailFieldID 
		AND NOT EXISTS 
		(
			SELECT * 
			FROM @Subs s 
			WHERE s.[Target] = dst.[Target]
		)
	END
	
	
	/* Set Titles to real values from IDs */
	UPDATE @Subs 
	SET RealValue = t.Title 
	FROM @Subs s 
	INNER JOIN dbo.Titles t ON t.TitleID = s.RealValue 
	WHERE s.[RawName] = 'TitleID' 
	

	/* Multiple Recipient substitutions for email out TO, CC and BCC fields */
	IF @ClientID IN (4, 180)
	BEGIN
		WHILE @DMRTID IS NOT NULL
		BEGIN
			SELECT TOP 1 
			@LeadOrMatterDetailFieldID = dmrt.DetailFieldID, 
			@ColumnDetailFieldID = dmrt.ColumnField, 
			@LeadOrMatter = df.LeadOrMatter, 
			@DMRTID = dmrt.DocumentMRTargetID, 
			@DMRTarget = dmrt.[Target] 
			FROM dbo.DocumentMRTarget dmrt WITH (NOLOCK) 
			INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = dmrt.DetailFieldID
			WHERE (dmrt.DocumentTypeID = @DocumentTypeID AND (dmrt.DocumentTypeVersionID IS NULL OR dmrt.DocumentTypeVersionID<=0) AND dmrt.TemplateTypeID = 5 AND dmrt.DocumentMRTargetID > @DMRTID) OR 
				  (dmrt.DocumentTypeVersionID = @DocumentTypeVersionID AND dmrt.TemplateTypeID = 5 AND dmrt.DocumentMRTargetID > @DMRTID)
			ORDER BY dmrt.DocumentMRTargetID ASC
			
			IF @@ROWCOUNT = 1
			BEGIN
				/* 
					Create a list of email addresses in the form: 
					
					jim.green@aquarium-software.com,nessa.green@aquarium-software.com, 
					
					The trailing comma will be removed right at the end
				*/
				
				/* Lead TableDetailValues*/
				IF @LeadOrMatter = 1
				BEGIN
					SELECT @EmailAddressList += tdv.DetailValue + ','
					FROM dbo.TableRows tr WITH (NOLOCK) 
					INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
					WHERE tr.LeadID = @LeadID 
					AND tr.DetailFieldID = @LeadOrMatterDetailFieldID
				END
				
				/* Matter TableDetailValues*/
				IF @LeadOrMatter = 2
				BEGIN
					SELECT @EmailAddressList += tdv.DetailValue + ',' 
					FROM dbo.Matter m WITH (NOLOCK) 
					INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID AND tr.DetailFieldID = @LeadOrMatterDetailFieldID
					INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
					WHERE m.CaseID = @CaseID
				END
				
				/* Remove trailing comma */
				IF LEN(@EmailAddressList) > 0
				BEGIN
					SELECT @EmailAddressList = LEFT(@EmailAddressList, LEN(@EmailAddressList) - 1)
				END
				
				/* Pretend that this is a standard substitution */
				INSERT INTO @Subs(RawName, [Target], RealValue, IsSpecial) 
				SELECT @DMRTarget, @DMRTarget, REPLACE(@DMRTarget, @DMRTarget, @EmailAddressList), 0
			END
			ELSE
			BEGIN
				SET @DMRTID = NULL
			END
		END
	END

	
	/* Insert any missing values now */
	/*SELECT DISTINCT s.*, datalength(s.[Target]) as [TargetLength] 
	FROM @Subs s
	ORDER BY datalength([Target]) desc, [DetailFieldID], [MatterID] asc*/
	SELECT DISTINCT s.*, DATALENGTH(s.[Target]) AS [TargetLength], ISNULL(s.ExcelSheetLocation, '') AS [ExcelSortOrder] 
	FROM @Subs s 
	ORDER BY DATALENGTH([Target]) DESC, [DetailFieldID] ASC, [ExcelSortOrder] ASC, [MatterID] ASC	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_ParseTemplate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldParser_ParseTemplate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_ParseTemplate] TO [sp_executeall]
GO
