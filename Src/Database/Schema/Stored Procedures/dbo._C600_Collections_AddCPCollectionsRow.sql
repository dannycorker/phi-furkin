SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- ALTER date:	2015-02-05
-- Description:	Adds a row to the CP Collections History table
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_AddCPCollectionsRow]
(
	@MatterID INT,
	@IsAuto INT -- = 0 collection stage 1 (pre-auth) not yet happened; = 1 pre-auth already done
)
AS
BEGIN

--declare @MatterID INT = 50022103,
--@CPToken VARCHAR(100) = '123456'

	SET NOCOUNT ON;
	
	DECLARE @Amount VARCHAR(20),
			@AqAutomation INT,
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@CPToken VARCHAR(100),
			@DateTimeBegan VARCHAR(16),
			@PremiumTableRowID INT,
			@TableRowID INT
	
	SELECT TOP 1 @Amount=am.DetailValue,
				 @CPToken=CASE WHEN @IsAuto=1 THEN cpt.DetailValue ELSE '' END,
				 @DateTimeBegan=CONVERT(VARCHAR(16),dbo.fn_GetDate_Local(),120),
				 @PremiumTableRowID=stat.TableRowID,
				 @AqAutomation=CASE WHEN @AqAutomation IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (cpt.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @AqAutomation END
	FROM MatterDetailValues cpt WITH (NOLOCK) 
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToMatterID=cpt.MatterID AND ltr.FromLeadTypeID=1492
	INNER JOIN TableDetailValues am WITH (NOLOCK) ON ltr.FromMatterID=am.MatterID AND am.DetailFieldID=170072
	INNER JOIN TableDetailValues stat WITH (NOLOCK) ON stat.TableRowID=am.TableRowID AND stat.DetailFieldID=170078
	WHERE cpt.MatterID=@MatterID AND cpt.DetailFieldID=170191
	AND stat.ValueInt=69903
	ORDER BY am.TableRowID DESC				

	INSERT INTO TableRows(ClientID, MatterID, DetailFieldID, DetailFieldPageID)
	SELECT @ClientID, @MatterID, 175472, 19057
	
	SELECT @TableRowID = SCOPE_IDENTITY()

	INSERT INTO TableDetailValues(ClientID, MatterID, TableRowID, DetailFieldID, DetailValue) VALUES
	(@ClientID,@MatterID,@TableRowID,175466,@CPToken), 
	(@ClientID,@MatterID,@TableRowID,175467,@DateTimeBegan), 
	(@ClientID,@MatterID,@TableRowID,175468,@Amount), 
	(@ClientID,@MatterID,@TableRowID,175469,'72149'), 
	(@ClientID,@MatterID,@TableRowID,175471,CAST(@PremiumTableRowID AS VARCHAR(30)) )

	EXEC _C00_CompleteTableRow @TableRowID,NULL,1,1
	
	RETURN @TableRowID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_AddCPCollectionsRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_AddCPCollectionsRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_AddCPCollectionsRow] TO [sp_executeall]
GO
