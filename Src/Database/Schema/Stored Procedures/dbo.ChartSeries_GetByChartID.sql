SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSeries table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries_GetByChartID]
(

	@ChartID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ChartSeriesID],
					[ChartSeriesTypeID],
					[ChartSeriesOrder],
					[ChartID],
					[ClientID],
					[PanelItemChartingID],
					[ColumnName]
				FROM
					[dbo].[ChartSeries] WITH (NOLOCK) 
				WHERE
					[ChartID] = @ChartID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_GetByChartID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries_GetByChartID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries_GetByChartID] TO [sp_executeall]
GO
