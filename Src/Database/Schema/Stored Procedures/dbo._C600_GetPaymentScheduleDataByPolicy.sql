SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-07-20
-- Description:	Current Payment Schedule and History – Get by MatterID 
-- 2020-08-13 CPS for JIRA PPET-158 | Expand to include collection date and payment method (account type)
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetPaymentScheduleDataByPolicy]
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT	 ppps.PurchasedProductPaymentScheduleID
			,ppps.CoverFrom
			,ppps.CoverTo
			,PaymentNet					AS [Net]
			,PaymentVAT					AS [Tax]
			,PaymentGross				AS [Gross]
			,ps.PaymentStatusName
			,ppps.ActualCollectionDate
			,at.AccountType
			,pppst.PurchasedProductPaymentScheduleTypeName [CollectionType]
	FROM PurchasedProduct pp WITH (NOLOCK) 
	INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.PurchasedProductID = pp.PurchasedProductID
	INNER JOIN PurchasedProductPaymentScheduleType pppst WITH (NOLOCK) on pppst.PurchasedProductPaymentScheduleTypeID = ppps.PurchasedProductPaymentScheduleTypeID
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = pp.ObjectID
	INNER JOIN PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = ppps.PaymentStatusID
	INNER JOIN Account ac WITH (NOLOCK) on ac.AccountID = ppps.AccountID
	INNER JOIN AccountType at WITH (NOLOCK) on at.AccountTypeID = ac.AccountTypeID
	WHERE m.MatterID = @MatterID
	AND m.ClientID = @ClientID
	AND pp.ClientID = @ClientID
	AND ppps.ClientID = @ClientID
	ORDER BY ppps.CoverFrom ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPaymentScheduleDataByPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetPaymentScheduleDataByPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPaymentScheduleDataByPolicy] TO [sp_executeall]
GO
