SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- ALTER date: 2009-03-03
-- Description:	Describe all attributes of a ClientPersonnel record
-- CS 2016-10-21 Added AquariusMaster details
-- =============================================
CREATE PROCEDURE [dbo].[ClientPersonnel__Describe]
	@UserID int
AS
BEGIN
PRINT OBJECT_NAME(@@ProcID)
	Declare @ClientID int
	
	SELECT @ClientID = cp.ClientID
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @UserID
	
	SELECT * FROM dbo.ClientPersonnel WITH (NOLOCK) WHERE ClientPersonnelID = @UserID

	SELECT	 'AquariusMaster' [AquariusMaster]
			, * 
			,'EXEC AquariusMaster.dbo.ClientPersonnel__PointAtDatabase ' 
				+ CONVERT(VARCHAR,cp.ClientPersonnelID) + ', '
				+ CONVERT(VARCHAR,cp.ClientID) 
				+ ', ' + ISNULL(''''+ cp.DatabaseName +'''','NULL') 
				+ ', ' + ISNULL(''''+ cp.ServerName +'''','NULL') [Redirect]
	FROM [AquariusMaster].dbo.ClientPersonnel cp WITH ( NOLOCK ) 
	WHERE cp.ClientPersonnelID = @UserID

	SELECT 'Client Info' as [Client Info], c.*, sc.*  
	FROM dbo.Clients c (nolock)
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON c.ClientID = cp.ClientID 
	LEFT JOIN dbo.SubClient sc WITH (NOLOCK) ON sc.SubClientID = cp.SubClientID 
	WHERE cp.ClientPersonnelID = @UserID 

	SELECT 'Group Info' as [Group Info], cpag.* 
	FROM dbo.ClientPersonnelAdminGroups cpag WITH (NOLOCK)
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cpag.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID
	WHERE cp.ClientPersonnelID = @UserID
	UNION
	SELECT  'Additional Info' as [Group Info],'','','This user has DYNAMIC RIGHTS'
	WHERE Exists (	SELECT * 
					FROM dbo.UserRightsDynamic d WITH (NOLOCK) 
					WHERE d.ClientPersonnelID = @UserID )
	ORDER BY [Group Info] desc
	
	SELECT 'Login History' [Useful SQL], replace(replace('SELECT top (1000) * FROM dbo.LoginHistory lh WITH (NOLOCK) WHERE lh.ClientID = @ClientID and lh.ClientPersonnelID = @Param ORDER BY lh.LoginHistoryID desc','@Param',@UserID),'@ClientID',convert(varchar,@ClientID)) [SQL]
	UNION
	SELECT 'Recent Logs' [Useful SQL], replace('SELECT top (1000) * FROM dbo.Logs l WITH (NOLOCK) WHERE l.ClientPersonnelID = @Param and l.LogDateTime > dbo.fn_GetDate_Local()-1 ORDER BY l.LogID desc','@Param',@UserID) [SQL]	
	UNION
	SELECT 'Reset Logins' [Useful SQL], REPLACE('EXEC dbo.ClientPersonnel__Unlock @UserID','@UserID',@UserID) [SQL]
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @UserID
	and cp.AttemptedLogins > 0
	
	SELECT 'Options' [Options], o.ClientPersonnelOptionID, o.ClientPersonnelOptionTypeID, t.OptionTypeName, o.OptionValue  
	FROM dbo.ClientPersonnelOptions o WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnelOptionTypes t WITH (NOLOCK) on t.ClientPersonnelOptionTypeID = o.ClientPersonnelOptionTypeID 
	WHERE o.ClientPersonnelID = @UserID 
	ORDER BY o.ClientPersonnelOptionTypeID 
	
	SELECT 'ClientPersonnelDetailValues',df.DetailFieldID, df.FieldName, ISNULL(li.ItemValue,dv.DetailValue) [Value]
	FROM DetailFields df WITH (NOLOCK) 
	LEFT JOIN ClientPersonnelDetailValues dv WITH (NOLOCK) on dv.DetailFieldID = df.DetailFieldID and dv.ClientPersonnelID = @UserID
	LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = dv.ValueInt
	WHERE df.LeadTypeID = 2
	and df.ClientID = @ClientID
	ORDER BY df.FieldOrder
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__Describe] TO [sp_executehelper]
GO
