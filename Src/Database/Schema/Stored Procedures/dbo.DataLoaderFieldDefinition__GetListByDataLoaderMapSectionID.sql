SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2009-01-30
-- Description:	Get DataLoaderFieldDefinition with lookups by @DataLoaderMapSectionID
-- =============================================
CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition__GetListByDataLoaderMapSectionID]
(
	@DataLoaderMapSectionID int   
)
AS
BEGIN

	SELECT
	dlfd.[DataLoaderFieldDefinitionID],
	dlfd.[ClientID],
	dlfd.[DataLoaderMapID],
	dlfd.[DataLoaderObjectTypeID],
	dlot.[ObjectTypeName] AS [SectionType],
	dlfd.[DataLoaderMapSectionID],
	dlfd.[DataLoaderObjectFieldID],
	dlfd.[DetailFieldID],
	dlfd.[DetailFieldAlias],
	dlfd.[NamedValue],
	dlfd.[Keyword],
	dlfd.[DataLoaderKeywordMatchTypeID],
	dlfd.[RowRelativeToKeyword],
	dlfd.[ColRelativeToKeyword],
	dlfd.[SectionRelativeRow],
	dlfd.[SectionAbsoluteCol],
	dlfd.[ValidationRegex],
	dlfd.[Equation],
	dlfd.[IsMatchField],
	dlfd.[DecodeTypeID],
	dldt.[DecodeTypeName],
	dlfd.[DefaultLookupItemID],
	dlfd.[SourceDataLoaderFieldDefinitionID],
	dlfd.[Notes],
	dlfd.[AllowErrors],
	CASE 
		WHEN dlfd.[DataLoaderObjectFieldID] IS NOT NULL THEN 'Customer Field'
		WHEN dlfd.[DetailFieldID] IS NOT NULL THEN 'DetailFieldID' 
		WHEN dlfd.[DetailFieldAlias] IS NOT NULL THEN 'Detail Field Alias'
		WHEN dlfd.[SourceDataLoaderFieldDefinitionID] IS NOT NULL THEN 'Named Value'
		ELSE '' 
	END AS [FieldType],
	CASE 
		WHEN dlfd.[DataLoaderObjectFieldID] IS NOT NULL THEN dlof.FieldName 
		WHEN dlfd.[DetailFieldID] IS NOT NULL THEN df.FieldName 
		WHEN dlfd.[DetailFieldAlias] IS NOT NULL THEN dlfd.[DetailFieldAlias] + CASE WHEN dfa.DetailFieldID IS NOT NULL THEN ' (ID = ' + convert(varchar, dfa.DetailFieldID) + ')' ELSE ' (not found!)' END
		WHEN dlfd.[SourceDataLoaderFieldDefinitionID] IS NOT NULL THEN source_dlfd.NamedValue 
		ELSE '' 
	END AS [FieldToPopulate],
	CASE
		--IF RELATIVE TO KEYWORD
		WHEN dlfd.[KeyWord] IS NOT NULL AND dlfd.[KeyWord] <> '' THEN convert(varchar, dlfd.[RowRelativeToKeyword]) + ' row' + CASE dlfd.[RowRelativeToKeyword] WHEN 1 THEN ' and ' ELSE 's and ' END + convert(varchar, dlfd.[ColRelativeToKeyword]) + ' column' + CASE dlfd.[ColRelativeToKeyword] WHEN 1 THEN ' ' ELSE 's ' END + ' from cell that ' + convert(varchar, dlkmt.[MatchTypeName]) + ' the keyword ''' + dlfd.[Keyword] + ''''
		--IF USING NAMED VALUE
		WHEN dlfd.[SourceDataLoaderFieldDefinitionID] IS NOT NULL THEN 'Using NamedValue ''' + source_dlfd.[NamedValue] + ''''
		--If RELATIVE TO SECTION START
		ELSE convert(varchar, dlfd.[SectionRelativeRow]) + ' row' + CASE dlfd.[SectionRelativeRow] WHEN 1 THEN ' and ' ELSE 's and ' END + convert(varchar, dlfd.[SectionAbsoluteCol]) + ' column' + CASE dlfd.[SectionAbsoluteCol] WHEN 1 THEN ' ' ELSE 's ' END + ' from the Section Start'
	END AS [LocateBy]
	FROM dbo.DataLoaderFieldDefinition dlfd
	INNER JOIN dbo.DataLoaderMap dlm ON dlm.DataLoaderMapID = dlfd.DataLoaderMapID
	INNER JOIN dbo.DataLoaderObjectType dlot ON dlot.DataLoaderObjectTypeID = dlfd.DataLoaderObjectTypeID 
	LEFT JOIN dbo.DataLoaderObjectField dlof ON dlof.DataLoaderObjectFieldID = dlfd.DataLoaderObjectFieldID
	LEFT JOIN dbo.DetailFields df ON df.DetailFieldID = dlfd.DetailFieldID 
	LEFT JOIN dbo.DetailFieldAlias dfa ON dfa.DetailFieldAlias = dlfd.DetailFieldAlias AND dfa.ClientID = dlfd.ClientID AND dfa.LeadTypeID = dlm.LeadTypeID 
	LEFT JOIN dbo.DataLoaderFieldDefinition source_dlfd ON source_dlfd.DataLoaderFieldDefinitionID = dlfd.SourceDataLoaderFieldDefinitionID
	LEFT JOIN dbo.DataLoaderKeywordMatchType dlkmt ON dlkmt.DataLoaderKeywordMatchTypeID = dlfd.DataLoaderKeywordMatchTypeID
	LEFT JOIN dbo.DataLoaderDecodeType dldt ON dldt.DataLoaderDecodeTypeID = dlfd.DecodeTypeID
	WHERE dlfd.[DataLoaderMapSectionID] = @DataLoaderMapSectionID
			
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition__GetListByDataLoaderMapSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition__GetListByDataLoaderMapSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition__GetListByDataLoaderMapSectionID] TO [sp_executeall]
GO
