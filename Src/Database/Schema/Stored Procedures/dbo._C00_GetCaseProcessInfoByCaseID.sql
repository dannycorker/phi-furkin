SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Stored Procedure

-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2014-01-24
-- Description:	Gets details of the Process for the given CaseID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetCaseProcessInfoByCaseID]
	-- Add the parameters for the stored procedure here
	@CaseID int,
	@ClientPersonnelID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE		
		@ClientID INT,
		@LatestEvent INT,
		@LatestLeadEvent INT,
		@Int INT,
		@Text VARCHAR(200),
		@BatchJobs BIT = 1,
		@Esc BIT = 1,
		@ETAE BIT = 1

DECLARE @IDTable TABLE (ID INT)
DECLARE @ResultsTable TABLE (ID int,Message varchar(2000), Link varchar(2000))

SELECT	@LatestLeadEvent = c.LatestInProcessLeadEventID,
		@LatestEvent = le.EventTypeID,
		@ClientID = c.ClientID
FROM Cases c WITH (NOLOCK) 
INNER JOIN LeadEvent le WITH (NOLOCK) ON le.leadeventid = c.latestinprocessLeadEventID
WHERE c.CaseID = @CaseID

INSERT INTO @IDTable(ID)
SELECT e.NextEventTypeID FROM EventChoice e WITH (NOLOCK) WHERE e.EventTypeID = @LatestEvent

/*Check for Batch Jobs*/
IF NOT EXISTS ( SELECT * FROM AutomatedTask a WITH (NOLOCK)
				INNER JOIN @IDTable i ON i.ID = a.TaskID)
BEGIN
INSERT INTO @ResultsTable (ID,Message,Link)
SELECT 1,'There are no Batch Jobs applying this event',null

SET @BatchJobs = 0
END

	IF @BatchJobs = 1
	BEGIN
		/*Check for disabled Batch Jobs*/
		IF EXISTS ( SELECT * FROM AutomatedTask a WITH (NOLOCK)
					INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
					INNER JOIN @IDTable i ON i.ID = atp.ParamValue
					WHERE a.Enabled = 0
					AND atp.ParamName = 'LEADEVENT_TO_ADD')
		BEGIN
			INSERT INTO @ResultsTable (ID,Message,Link)
			SELECT 4,'The batch job applying this event is disabled. Task ' + CAST(a.TaskID as varchar(200))+ ' ' +CAST(a.Taskname as varchar(200))+ ' Event' +CAST(atp.ParamValue as varchar(200)) AS [Disabled Batch Job],null
			FROM AutomatedTask a WITH (NOLOCK) 
			INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
			INNER JOIN @IDTable i ON i.ID = atp.ParamValue
			WHERE atp.ParamName = 'LEADEVENT_TO_ADD'

		END
		/*Check for Batch Failures*/
		IF EXISTS ( SELECT * FROM AutomatedTask a WITH (NOLOCK)
					INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
					INNER JOIN @IDTable i ON i.ID = atp.ParamValue
					INNER JOIN AutomatedTaskResult atr WITH (NOLOCK) ON atr.TaskID = a.TaskID
					WHERE a.Enabled = 0
					AND atp.ParamName = 'LEADEVENT_TO_ADD')
		BEGIN
			INSERT INTO @ResultsTable (ID,Message,Link)
			SELECT 5,'The most recent run of this batch has failed. Task' + cast(a.TaskID as varchar(200))+ ' ' +CAST(a.Taskname as varchar(200)) AS [Batch Job Failing],null
			FROM AutomatedTask a WITH (NOLOCK) 
			INNER JOIN AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = a.TaskID
			INNER JOIN @IDTable i ON i.ID = atp.ParamValue
			INNER JOIN AutomatedTaskResult atr WITH (NOLOCK) ON atr.TaskID = a.TaskID
			WHERE atp.ParamName = 'LEADEVENT_TO_ADD'
			AND (atr.Description LIKE '%Fail%' OR atr.Complete = 0)
			AND NOT EXISTS (SELECT * FROM AutomatedTaskResult atrsub WITH (NOLOCK) WHERE atrsub.TaskID = a.TaskID AND atr.AutomatedTaskResultID > atrsub.AutomatedTaskResultID)
		END

	END

/*Check for Esc Events*/
IF NOT EXISTS (	SELECT * FROM Cases c WITH (NOLOCK) 
				INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestInProcessLeadEventID
				INNER JOIN EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = le.EventTypeID
				WHERE c.ClientID = @ClientID
				AND ec.EscalationEvent = 1
				AND c.CaseID = @CaseID)

BEGIN
INSERT INTO @ResultsTable (ID,Message,Link)
SELECT 2,'There are no Esc event choices for this event.' AS [NO ESC - NO Esc Event Choices],null
SET @Esc = 0
END

	IF @Esc = 1
	BEGIN
		/*Check for FollowupDateTime*/
		INSERT INTO @ResultsTable (ID,Message,Link)
		SELECT 6,CASE 
		WHEN (dbo.fnDateOnly(DATEADD(DAY,1,dbo.fn_GetDate_Local())) > le.FollowupDateTime AND le.FollowupDateTime < dbo.fn_GetDate_Local()) THEN		
		CASE WHEN DATEDIFF(DAY,dbo.fn_GetDate_Local(),le.followupdatetime) < 0 THEN 'Event should have escalated already' 
		ELSE 'Event is due to escalate at Midnight' END 
		WHEN le.FollowupDateTime is null then 'Event has no FollowupDateTime'
		ELSE 'Event is not due to escalate yet' END AS [Status] ,''
		FROM LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @LatestLeadEvent
		--AND (le.FollowupDateTime < dbo.fn_GetDate_Local() OR (dbo.fnDateOnly(DATEADD(DAY,1,dbo.fn_GetDate_Local())) > le.FollowupDateTime AND le.FollowupDateTime < dbo.fn_GetDate_Local()) OR le.FollowupDateTime is null)
	END

/*Check for EventTypeAutomatedEvent*/
IF NOT EXISTS (SELECT * FROM EventTypeAutomatedEvent a WITH (NOLOCK) WHERE a.EventTypeID = @LatestEvent)
BEGIN
INSERT INTO @ResultsTable (ID,Message,Link)
SELECT 3,'There is no EventTypeAutomatedEvent for this event.' AS [NO ETAE - NO Automated Event],null
SET @ETAE = 0
END
	IF @ETAE = 1
	BEGIN
		/*Check for EventTypeAutomatedEvent Errors*/
		IF EXISTS (SELECT * FROM AutomatedEventQueue a WITH (NOLOCK) WHERE a.FromLeadEventID = @LatestLeadEvent AND a.ErrorMessage <> '')
		BEGIN
			INSERT INTO @ResultsTable (ID,Message,Link)
			SELECT 7,'There are EventTypeAutomatedEvent errors for this LeadEvent ' + CONVERT(VARCHAR,@LatestLeadEvent) as [Automated Event Errors],null
			SET @ETAE = 0
		END
	END


/*Check for AQ Lead Status*/
IF EXISTS (	SELECT * FROM Lead l WITH (NOLOCK) 
			INNER JOIN Cases c WITH (NOLOCK)  ON c.LeadID = l.LeadID
			WHERE l.ClientID = @ClientID
			AND l.AquariumStatusID <> 2
			AND c.CaseID = @CaseID)


BEGIN
INSERT INTO @ResultsTable (ID,Message,Link)
SELECT 8,'Lead AquariumStatus ID is set to closed/hold.' AS [NO Automation - AquariumStatusID IS Closed],null
END


/*Check for AQ Status Case Closed*/
IF EXISTS (	SELECT * FROM Cases c WITH (NOLOCK) 
			INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestInProcessLeadEventID
			INNER JOIN EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = le.EventTypeID
			WHERE c.ClientID = @ClientID
			AND ec.EscalationEvent = 1
			AND c.AquariumStatusID = 4
			AND c.CaseID = @CaseID)
BEGIN
INSERT INTO @ResultsTable (ID,Message,Link)
SELECT 9,'Case AquariumStatusID is closed.' AS [NO ESC - AquariumStatusID IS Closed],null
END


/*Check for AQ Status Mismatch*/
IF EXISTS (	SELECT * FROM Lead l WITH (NOLOCK) 
			INNER JOIN Cases c WITH (NOLOCK)  ON c.LeadID = l.LeadID
			INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestInProcessLeadEventID
			INNER JOIN EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = le.EventTypeID
			WHERE l.ClientID = @ClientID
			AND l.AquariumStatusID = 4
			AND ec.EscalationEvent = 1
			AND c.AquariumStatusID = 2
			AND c.CaseID = @CaseID)


BEGIN
INSERT INTO @ResultsTable (ID,Message,Link)
SELECT 10,'Lead AquariumStatus ID is set to closed, Case AquariumStatus ID is open.' AS [NO ESC - AquariumStatusID Mismatch],null
END

IF (SELECT COUNT(*) From @ResultsTable r where r.ID in (1,2,3)) = 3
BEGIN
	Delete From @ResultsTable
	Where ID in (1,2,3)

	Insert Into @ResultsTable (ID,Message,Link)
	Values (11,'There are no Automations for this Event.',null)
END

Select ID,Message,Link from @ResultsTable

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetCaseProcessInfoByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetCaseProcessInfoByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetCaseProcessInfoByCaseID] TO [sp_executeall]
GO
