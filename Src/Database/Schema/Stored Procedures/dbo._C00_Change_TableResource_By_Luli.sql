SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2015-09-10
-- Description:	Allow the values in a resouce to be changed from a Lookuplist on the same table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Change_TableResource_By_Luli]
			
			@ChangeFromLuli INT,  /*Lookuplist on table FieldID*/
			@RLFieldIDToChange INT, /*ResourceList FieldID on Tabel*/
			@TableRowID INT,  /*Table Row we care about*/
			@RLLULI INT  /* Linked  lookup on resouyrce*/
			
AS
BEGIN

	SET NOCOUNT ON;

	Declare @ResourceListID INT,
			@ChangeValue INT, /*Lookuplist Value to change to*/
			@MatterID INT /*MatterID we care about*/
	
	SELECT @ChangeValue = dbo.fnGetSimpleDvAsInt(@ChangeFromLuli,@TableRowID), /*Change Provider*/
		   @MatterID = tr.MatterID FRom TableRows tr WITH (NOLOCK) 
		   where tr.TableRowID = @TableRowID
		   
		   
	IF @ChangeValue > 0
	BEGIN
		SELECT TOP(1) @ResourceListID = rdv.ResourceListID
		FROM ResourceListDetailValues rdv WITH ( NOLOCK ) 
		WHERE rdv.DetailFieldID = @RLLULI  /*Linked LookupList*/
		AND rdv.ValueInt = @ChangeValue
			
		EXEC _C00_CompleteTableRow @TableRowID, @MatterID, 0, 0
			
		IF @ResourceListID > 0
		BEGIN
			UPDATE tdv
			SET ResourceListID = @ResourceListID
			FROM TableDetailValues tdv WITH ( NOLOCK ) 
			WHERE tdv.TableRowID = @TableRowID
			AND tdv.DetailFieldID = @RLFieldIDToChange /*Hire Company*/
		END
			
	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Change_TableResource_By_Luli] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Change_TableResource_By_Luli] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Change_TableResource_By_Luli] TO [sp_executeall]
GO
