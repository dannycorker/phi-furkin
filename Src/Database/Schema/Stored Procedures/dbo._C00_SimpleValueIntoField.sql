SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-04-13
-- Description:	Insert Fields into MDV or LDV or Customer DV fields
-- Updates:		SB	2012-04-18 Simplified the input params and updated to work with all the detail value types
--				SB	2013-05-09 Added ISNULL to all types that do not accept null
--				SB	2014-06-17 Added missing DV history for types that support it
--				SB	2014-07-24 Changed to get client ID from the object ID for shared lead types
--				ROH 2014-10-15 Fix to bug relating to @ClientID not being available to the detail value history update Zen#29110
--				CPS 2017-08-21 Friendly error if @ClientID is NULL
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SimpleValueIntoField]
(
	 @DetailFieldID		INT
	,@DetailValue		VARCHAR(2000)
	,@ObjectID			INT
	,@ClientPersonnelID	INT = NULL
)
	
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @LeadOrMatter INT,
			@ClientID INT,
			@FieldClientID INT,
			@MaintainHistory BIT,
			@QuestionTypeID int,
			@LogEntry	VARCHAR(2000)

	SELECT	@LeadOrMatter = LeadOrmatter,
			@FieldClientID = ClientID,
			@MaintainHistory = MaintainHistory,
			@QuestionTypeID = QuestionTypeID
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	SELECT @LogEntry	= '_C00_SimpleValueIntoField @DetailFieldID = ' 
						+ ISNULL(CONVERT(VARCHAR,@DetailFieldID),'NULL')
						+ ', @ObjectID = ' 
						+ ISNULL(CONVERT(VARCHAR,@ObjectID),'NULL')
						+ ', @DetailValue = '''
						+ ISNULL(@DetailValue,'NULL')
						+ ''''

	IF @ClientPersonnelID IS NULL
	BEGIN
		SELECT TOP 1 @ClientPersonnelID=ClientPersonnelID FROM ClientPersonnel WITH (NOLOCK) 
		WHERE UserName LIKE 'Aquarium%Automation' AND ClientID=@FieldClientID
	END

	IF @LeadOrMatter = 1
	BEGIN

		SELECT @ClientID = ClientID FROM Lead WITH (NOLOCK) WHERE LeadID = @ObjectID

		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END

		IF EXISTS (SELECT * FROM dbo.LeadDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND LeadID = @ObjectID)
		BEGIN
			UPDATE dbo.LeadDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND LeadID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@DetailValue, ''))
		END
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, @ObjectID, NULL, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END

	END

	IF @LeadOrMatter = 2
	BEGIN

		SELECT @ClientID = ClientID FROM Matter WITH (NOLOCK) WHERE MatterID = @ObjectID

		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
		
		IF EXISTS (SELECT * FROM dbo.MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND MatterID = @ObjectID)
		BEGIN
			UPDATE dbo.MatterDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND MatterID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue)
			SELECT @ClientID, @ObjectID, LeadID, @DetailFieldID, ISNULL(@DetailValue, '')
			FROM dbo.Matter
			WHERE MatterID = @ObjectID
		END
		
		IF @MaintainHistory = 1  AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, NULL, @ObjectID, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END
	END

	IF @LeadOrMatter = 4
	BEGIN

		SELECT @ClientID = ClientID FROM ResourceList WITH (NOLOCK) WHERE ResourceListID = @ObjectID

		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
		
		IF EXISTS (SELECT * FROM dbo.ResourceListDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ResourceListID = @ObjectID)
		BEGIN
			UPDATE dbo.ResourceListDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ResourceListID = @ObjectID
		END
		ELSE
		BEGIN 
			INSERT dbo.ResourcelistDetailValues (ResourceListID,ClientID,DetailFieldID,LeadOrMatter,DetailValue)
			SELECT @ObjectID,@ClientID,@DetailFieldID,@LeadOrMatter,ISNULL(@DetailValue, '')
			FROM dbo.ResourceList
			WHERE ResourceListID = @ObjectID
		END
		
		/*CS 2015-04-20.  For ticket #30957 on request of AH.  Temporary logging for Premier only.  Remove at next release*/
		IF @ClientID = 322 AND @DetailFieldID IN ( 217318,217319,217320,217321,217322,217323,217324,217325,217326,217327,221282,221283,221284,223029,223030,243951,243952,243953,243954,243955,243956,243957,243958,243959,285062 )
		BEGIN
			EXEC _C00_LogIt 'Info', '_C322_LOG', '_C00_SimpleValueIntoField', @LogEntry, @ClientPersonnelID
		END

	END

	IF @LeadOrMatter in(8,6)
	BEGIN
		
		SELECT @ClientID = ClientID FROM TableRows WITH (NOLOCK) WHERE TableRowID = @ObjectID
		
		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
		
		IF EXISTS (SELECT * FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND TableRowID = @ObjectID)
		BEGIN
			UPDATE dbo.TableDetailValues
			SET DetailValue =		CASE @QuestionTypeID WHEN 14 THEN null ELSE @DetailValue END,
				ResourceListID =	CASE @QuestionTypeID WHEN 14 THEN @DetailValue ELSE NULL END 
			WHERE DetailFieldID = @DetailFieldID 
			AND TableRowID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.TableDetailValues (TableRowID,DetailFieldID,DetailValue,ResourceListID,MatterID,ClientID,CustomerID,CaseID,LeadID)
			SELECT	tr.TableRowID,@DetailFieldID,
					CASE @QuestionTypeID WHEN 14 THEN null ELSE @DetailValue END,
					CASE @QuestionTypeID WHEN 14 THEN @DetailValue ELSE NULL END,
					tr.MatterID,tr.ClientID,tr.CustomerID,tr.CaseID,tr.LeadID
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.TableRowID = @ObjectID
		END
	END

	IF @LeadOrMatter = 10
	BEGIN
	
		SELECT @ClientID = ClientID FROM Customers WITH (NOLOCK) WHERE CustomerID = @ObjectID
		
		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
		
		IF EXISTS (SELECT * FROM dbo.CustomerDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND CustomerID = @ObjectID)
		BEGIN
			UPDATE dbo.CustomerDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND CustomerID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@DetailValue, ''))
		END
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, CustomerID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, @ObjectID, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END
	END
	
	IF @LeadOrMatter = 11
	BEGIN
	
		SELECT @ClientID = ClientID FROM Cases WITH (NOLOCK) WHERE CaseID = @ObjectID
		
		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
		
		IF EXISTS (SELECT * FROM dbo.CaseDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND CaseID = @ObjectID)
		BEGIN
			UPDATE dbo.CaseDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND CaseID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@DetailValue, ''))
		END
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, CaseID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, @ObjectID, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		IF EXISTS (SELECT * FROM dbo.ClientDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ClientID = @ObjectID)
		BEGIN
			UPDATE dbo.ClientDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ClientID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.ClientDetailValues (ClientID, DetailFieldID, DetailValue)
			VALUES(@ObjectID, @DetailFieldID, ISNULL(@DetailValue, ''))
		END
		
		IF @MaintainHistory = 1  AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ObjectID, @DetailFieldID, @LeadOrMatter, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID)
			
		END
	END
	
	IF @LeadOrMatter = 13
	BEGIN
	
		SELECT @ClientID = ClientID FROM ClientPersonnel WITH (NOLOCK) WHERE ClientPersonnelID = @ObjectID

		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
				
		IF EXISTS (SELECT * FROM dbo.ClientPersonnelDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ClientPersonnelID = @ObjectID)
		BEGIN
			UPDATE dbo.ClientPersonnelDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ClientPersonnelID = @ObjectID
		END
		ELSE
		BEGIN
			INSERT dbo.ClientPersonnelDetailValues (ClientID, ClientPersonnelID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@DetailValue, ''))
		END
		
		IF @MaintainHistory = 1 AND @ClientPersonnelID IS NOT NULL
		BEGIN

			INSERT INTO DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, ClientPersonnelDetailValueID, FieldValue, WhenSaved, ClientPersonnelID)
			VALUES(@ClientID, @DetailFieldID, @LeadOrMatter, @ObjectID, @DetailValue, dbo.fn_GetDate_Local(), @ClientPersonnelID)

		END
		
	END
	
	IF @LeadOrMatter = 14
	BEGIN
	
		SELECT @ClientID = ClientID FROM Contact WITH (NOLOCK) WHERE ContactID = @ObjectID

		IF @ClientID is NULL
		BEGIN
			SELECT @LogEntry = '@ClientID is NULL: ' + @LogEntry
			RAISERROR( @LogEntry, 16, 1 )
			RETURN
		END
				
		IF EXISTS (SELECT * FROM dbo.ContactDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND ContactID = @ObjectID)
		BEGIN
			UPDATE dbo.ContactDetailValues
			SET DetailValue = ISNULL(@DetailValue, '')
			WHERE DetailFieldID = @DetailFieldID 
			AND ContactID = @ObjectID
		END
		ELSE
		BEGIN
			SELECT @ClientID = ClientID FROM Contact WITH (NOLOCK) WHERE ContactID = @ObjectID
		
			INSERT dbo.ContactDetailValues (ClientID, ContactID, DetailFieldID, DetailValue)
			VALUES(@ClientID, @ObjectID, @DetailFieldID, ISNULL(@DetailValue, ''))
		END

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SimpleValueIntoField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SimpleValueIntoField] TO [sp_executeall]
GO
