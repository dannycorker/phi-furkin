SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyMappingTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingTemplate_Update]
(

	@ThirdPartyMappingTemplateID int   ,

	@ThirdPartyFieldGroupID int   ,

	@ThirdPartySystemID int   ,

	@Name varchar (50)  ,

	@Description varchar (255)  ,

	@XMLItemTemplate varchar (MAX)  ,

	@XMLContainerName varchar (MAX)  ,

	@XMLItemName varchar (MAX)  ,

	@XMLItemKey varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyMappingTemplate]
				SET
					[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
					,[ThirdPartySystemID] = @ThirdPartySystemID
					,[Name] = @Name
					,[Description] = @Description
					,[XMLItemTemplate] = @XMLItemTemplate
					,[XMLContainerName] = @XMLContainerName
					,[XMLItemName] = @XMLItemName
					,[XMLItemKey] = @XMLItemKey
				WHERE
[ThirdPartyMappingTemplateID] = @ThirdPartyMappingTemplateID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingTemplate_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingTemplate_Update] TO [sp_executeall]
GO
