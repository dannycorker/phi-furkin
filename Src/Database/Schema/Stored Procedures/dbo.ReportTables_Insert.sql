SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReportTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTables_Insert]
(

	@ReportTableID int    OUTPUT,

	@ReportTableName varchar (50)  ,

	@DisplayOrder int   ,

	@RequiresClientSecurity bit   ,

	@IsLookupTable bit   ,

	@ShowAquariumValues bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ReportTables]
					(
					[ReportTableName]
					,[DisplayOrder]
					,[RequiresClientSecurity]
					,[IsLookupTable]
					,[ShowAquariumValues]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@ReportTableName
					,@DisplayOrder
					,@RequiresClientSecurity
					,@IsLookupTable
					,@ShowAquariumValues
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ReportTableID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTables_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Insert] TO [sp_executeall]
GO
