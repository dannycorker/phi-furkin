SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-03-17
-- Description:	Used in the SDK this PROC gets matter or lead detail values by specified field ids.  It expands lookup lists, and resource lists nested 
-- in tables so the SDK can return a nicely formatted collection of objects
--
-- UPDATES:		2011-07-26 Simon Brushett - Added object name resolution in the style of lookup lists				
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- MODIFIED: 2015-06-05 PR  Added Editable to simple values, resource lists, tables and basic tables
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetExpandedLeadValuesByFieldIDs]
	@ClientID INT,
	@LeadID INT,
	@FieldIDs tvpInt READONLY,
	@ExcludeHiddenFields bit = 0
AS
BEGIN

	-- Lead detail values
	SELECT v.DetailFieldID, f.QuestionTypeID, f.Editable
	FROM 
		[dbo].[LeadDetailValues] v WITH (NOLOCK)
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f ON v.DetailFieldID = f.DetailFieldID
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
	WHERE
		v.LeadID = @LeadID
		AND v.ClientID = @ClientID
		AND f.Enabled = 1
	ORDER BY f.FieldOrder 

	-- Simple values
	SELECT v.DetailFieldID, v.LeadDetailValueID AS DetailValueID, v.DetailValue, COALESCE(f.Encrypt, 0) AS Encrypt, v.EncryptedValue, 
			CASE WHEN f.QuestionTypeID = 22 THEN o.Name ELSE li.ItemValue END AS LookupListValue, f.FieldCaption, ll.LookupListID, f.FieldOrder, f.Required, f.QuestionTypeID, f.FieldSize, v.LeadID, f.Editable
	FROM
		[dbo].[LeadDetailValues] v WITH (NOLOCK)
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f ON v.DetailFieldID = f.DetailFieldID
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND v.ValueInt = li.LookupListItemID
		LEFT JOIN dbo.Objects o WITH (NOLOCK) ON v.ValueInt = o.ObjectID
	WHERE
		v.LeadID = @LeadID
		AND v.ClientID = @ClientID
		AND (f.QuestionTypeID != 14 AND f.QuestionTypeID != 16 AND f.QuestionTypeID != 19)
		AND f.Enabled = 1
	ORDER BY f.FieldOrder

	--Resource lists
	SELECT v.DetailFieldID, r.ResourceListID AS DetailValueID, r.DetailFieldID AS ResourceListDetailFieldID, r.ResourceListDetailValueID, 
			r.DetailValue AS ResourceListDetailValue, COALESCE(f.Encrypt, 0) AS ResourceListEncrypt, v.EncryptedValue AS ResourceListEncryptedValue,
			CASE WHEN f.QuestionTypeID = 22 THEN o.Name ELSE li.ItemValue END AS ResourceListLookupListValue, f.FieldCaption, f2.FieldCaption AS ResourceListFieldCaption, f.QuestionTypeID, v.leadID, ll.LookupListID, f.Editable
	FROM
		[dbo].[LeadDetailValues] v WITH (NOLOCK)
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f ON v.DetailFieldID = f.DetailFieldID
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		INNER JOIN dbo.ResourceListDetailValues r WITH (NOLOCK) ON r.ResourceListID = v.ValueInt
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f2 ON r.DetailFieldID = f2.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f2.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND r.ValueInt = li.LookupListItemID
		LEFT JOIN dbo.Objects o WITH (NOLOCK) ON v.ValueInt = o.ObjectID
	WHERE
		v.LeadID = @LeadID
		AND v.ClientID = @ClientID
		AND f.QuestionTypeID = 14
		AND f.Enabled = 1
		AND ((f2.Hidden = 0 AND @ExcludeHiddenFields = 1) OR (@ExcludeHiddenFields = 0))
	ORDER BY v.DetailFieldID, f.FieldOrder


	-- Tables
	SELECT f.DetailFieldID, t.DetailFieldID AS TableDetailFieldID, t.TableDetailValueID, t.DetailValue, t.TableRowID,
			rl.DetailFieldID AS ResourceListDetailFieldID, rl.ResourceListDetailValueID, rl.DetailValue AS ResourceListDetailValue, 
			COALESCE(f1.Encrypt, 0) AS Encrypt, t.EncryptedValue, COALESCE(f2.Encrypt, 0) AS ResourceListEncrypt, t.EncryptedValue AS ResourceListEncryptedValue,
			t.ResourceListID, CASE WHEN f.QuestionTypeID = 22 THEN o.Name ELSE li.ItemValue END AS LookupListValue, 
			CASE WHEN f.QuestionTypeID = 22 THEN o2.Name ELSE li2.ItemValue END AS ResourceListLookupListValue, f1.QuestionTypeID, t.LeadID, f.Editable

	FROM
		dbo.fnDetailFieldsShared(@ClientID) f
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON t.TableRowID = r.TableRowID
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f1 ON t.DetailFieldID = f1.DetailFieldID
		LEFT OUTER JOIN dbo.ResourceListDetailValues rl WITH (NOLOCK) ON rl.ResourceListID = t.ResourceListID
		LEFT OUTER JOIN dbo.fnDetailFieldsShared(@ClientID) f2 ON rl.DetailFieldID = f2.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f1.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND t.ValueInt = li.LookupListItemID
		LEFT JOIN dbo.LookupList ll2  WITH (NOLOCK) ON f2.LookupListID = ll2.LookupListID
		LEFT JOIN dbo.LookupListItems li2 WITH (NOLOCK) ON ll2.LookupListID = li2.LookupListID AND rl.ValueInt = li2.LookupListItemID
		LEFT JOIN dbo.Objects o WITH (NOLOCK) ON t.ValueInt = o.ObjectID
		LEFT JOIN dbo.Objects o2 WITH (NOLOCK) ON rl.ValueInt = o2.ObjectID
	WHERE
		t.LeadID = @LeadID
		AND f.ClientID = @ClientID
		AND f.QuestionTypeID = 16
		AND f.Enabled = 1
	ORDER BY f.DetailFieldID, f.FieldOrder, t.TableRowID, f1.FieldOrder


	-- Basic tables
	SELECT f.DetailFieldID, t.DetailFieldID AS TableDetailFieldID, t.TableDetailValueID, t.DetailValue, t.TableRowID, 
			COALESCE(f1.Encrypt, 0) AS Encrypt, t.EncryptedValue, CASE WHEN f.QuestionTypeID = 22 THEN o.Name ELSE li.ItemValue END AS LookupListValue, 
			f.FieldCaption, f1.FieldCaption AS TableFieldCaption, f1.QuestionTypeID, t.LeadID, ll.LookupListID, f.Editable
	FROM
		dbo.fnDetailFieldsShared(@ClientID) f
		INNER JOIN @FieldIDs ids ON ids.AnyID = f.DetailFieldID  
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON t.TableRowID = r.TableRowID
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) f1 ON t.DetailFieldID = f1.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f1.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND t.ValueInt = li.LookupListItemID
		LEFT JOIN dbo.Objects o WITH (NOLOCK) ON t.ValueInt = o.ObjectID
	WHERE
		t.LeadID = @LeadID
		AND f.ClientID = @ClientID
		AND f.QuestionTypeID = 19
		AND f.Enabled = 1
	ORDER BY f.DetailFieldID, f.FieldOrder, t.TableRowID, f1.FieldOrder
	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedLeadValuesByFieldIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetExpandedLeadValuesByFieldIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetExpandedLeadValuesByFieldIDs] TO [sp_executeall]
GO
