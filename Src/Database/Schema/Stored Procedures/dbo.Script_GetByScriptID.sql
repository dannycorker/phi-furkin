SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Script table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Script_GetByScriptID]
(

	@ScriptID int   
)
AS


				SELECT
					[ScriptID],
					[ClientID],
					[LeadTypeID],
					[ScriptName],
					[ScriptDescription],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified],
					[FinishEventTypeID],
					[Published],
					[Disabled],
					[ScriptParentID],
					[StartEventTypeID],
					[ShowNotes],
					[NumberOfNotesToShow],
					[IntegrateWithDialler],
					[ApplyFinishEventTo],
					[Redact]
				FROM
					[dbo].[Script] WITH (NOLOCK) 
				WHERE
										[ScriptID] = @ScriptID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Script_GetByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script_GetByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script_GetByScriptID] TO [sp_executeall]
GO
