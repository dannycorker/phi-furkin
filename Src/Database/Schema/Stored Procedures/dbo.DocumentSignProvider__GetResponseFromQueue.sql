SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-07-27
-- Description:	Document Sign Provider
-- IS Ticket #33078 Updating Esign
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSignProvider__GetResponseFromQueue]
	@OutLeadEventID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	
		dspq.ResponseXML,
		dspq.DSPEnvelopeKey,
		dspq.DSPEnvelopeStatus           
	FROM	DocumentSignProviderQueue dspq WITH (NOLOCK) 
	WHERE	OutLeadEventID = @OutLeadEventID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetResponseFromQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSignProvider__GetResponseFromQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSignProvider__GetResponseFromQueue] TO [sp_executeall]
GO
