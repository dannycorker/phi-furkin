SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the UserFunctionControl table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControl_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SELECT
					[UserFunctionControlID],
					[ClientPersonnelID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[UserFunctionControl] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelID] = @ClientPersonnelID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControl_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControl_GetByClientPersonnelID] TO [sp_executeall]
GO
