SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the BillItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_Insert]
(

	@BillItemID int    OUTPUT,

	@ClientID int   ,

	@BillID int   ,

	@Description varchar (2000)  ,

	@ClientPersonnelID int   ,

	@WorkUnits int   ,

	@Total decimal (18, 2)  ,

	@ChargeOutRate int   ,

	@TotalIsCustomValue bit   
)
AS


				
				INSERT INTO [dbo].[BillItem]
					(
					[ClientID]
					,[BillID]
					,[Description]
					,[ClientPersonnelID]
					,[WorkUnits]
					,[Total]
					,[ChargeOutRate]
					,[TotalIsCustomValue]
					)
				VALUES
					(
					@ClientID
					,@BillID
					,@Description
					,@ClientPersonnelID
					,@WorkUnits
					,@Total
					,@ChargeOutRate
					,@TotalIsCustomValue
					)
				-- Get the identity value
				SET @BillItemID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Insert] TO [sp_executeall]
GO
