SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EventSubtypeDefault table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtypeDefault_Get_List]

AS


				
				SELECT
					[EventSubtypeDefaultID],
					[ClientID],
					[EventSubtypeID],
					[EventTypeID],
					[LeadTypeID],
					[ClientPersonnelID]
				FROM
					[dbo].[EventSubtypeDefault] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault_Get_List] TO [sp_executeall]
GO
