SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClassNodeAuth table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_Delete]
(

	@ClassNodeAuthID int   
)
AS


				DELETE FROM [dbo].[ClassNodeAuth] WITH (ROWLOCK) 
				WHERE
					[ClassNodeAuthID] = @ClassNodeAuthID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Delete] TO [sp_executeall]
GO
