SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderMap table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DataLoaderMapID],
					[ClientID],
					[MapName],
					[MapDescription],
					[WhenCreated],
					[CreatedBy],
					[LastUpdated],
					[UpdatedBy],
					[EnabledForUse],
					[Deleted],
					[LeadTypeID],
					[UseDefaultLeadForCustomer],
					[UseDefaultCaseForLead],
					[UseDefaultMatterForCase]
				FROM
					[dbo].[DataLoaderMap] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_GetByClientID] TO [sp_executeall]
GO
