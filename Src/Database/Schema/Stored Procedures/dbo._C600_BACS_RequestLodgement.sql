SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-07
-- Description:	Request lodgement - mandate setup
-- Mods
--	DCM 25/03/2015 Ticket #31805
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BACS_RequestLodgement] 
(
	@MatterID INT,
	@WhoModified INT = NULL,
	@MandateChangeType INT = 70013 -- default to change
)

AS
BEGIN

--declare
--	@MatterID INT=50029231,
--	@WhoModified INT = 44412, -- Aq Automation 
--	@MandateChangeType INT = 70013 -- default to change


	SET NOCOUNT ON;
	
	DECLARE	
			@EffectiveDate DATE,
			@PayRef VARCHAR(100),
			@AccName VARCHAR(100),
			@AccSortCode VARCHAR(10),
			@AccNumber VARCHAR(10),
			@AffinitySC VARCHAR(10)
	

	-- Get affinity short code for pay ref
	SELECT @AffinitySC=scli.ItemValue,
		 @WhoModified=CASE WHEN @WhoModified IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (sc.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @WhoModified END
	FROM CustomerDetailValues sc WITH (NOLOCK) 
	INNER JOIN Lead cl WITH (NOLOCK) ON sc.CustomerID=cl.CustomerID
	INNER JOIN Matter cm WITH (NOLOCK) ON cm.LeadID=cl.LeadID AND cm.MatterID=@MatterID
	INNER JOIN ResourceListDetailValues scrl WITH (NOLOCK) ON sc.ValueInt=scrl.ResourceListID and scrl.DetailFieldID=170127
	INNER JOIN LookupListItems scli WITH (NOLOCK) ON scrl.ValueInt=scli.LookupListItemID
	WHERE sc.DetailFieldID=170144
	
	-- Create mandate request table row
	SELECT @PayRef= dbo.fn_C600_GetMandateReference(@MatterID), @AccName=an.DetailValue, @AccSortCode=acs.DetailValue, @AccNumber=anum.DetailValue
	FROM MatterDetailValues an WITH (NOLOCK) 
	INNER JOIN MatterDetailValues acs WITH (NOLOCK) ON acs.MatterID=an.MatterID and acs.DetailFieldID=170189
	INNER JOIN MatterDetailValues anum WITH (NOLOCK) ON anum.MatterID=an.MatterID and anum.DetailFieldID=170190
	WHERE an.MatterID=@MatterID and an.DetailFieldID=170188 

	SELECT @EffectiveDate=dbo.fnGetNextWorkingDate(dbo.fn_GetDate_Local(),1) 
	
	-- DCM 25/03/2015 Ticket #31805
	-- delete any existing row(s) that haven't yet been submitted to BACS 
	DELETE tr
	FROM TableRows tr
	INNER JOIN TableDetailValues bc WITH (NOLOCK) ON tr.TableRowID=bc.TableRowID AND bc.DetailFieldID=170162
	INNER JOIN TableDetailValues bfd WITH (NOLOCK) ON tr.TableRowID=bfd.TableRowID AND bfd.DetailFieldID=170080
	WHERE tr.DetailFieldID=170183 AND tr.MatterID=@MatterID
	AND bfd.DetailValue='' -- BACS file date is blank
	AND bc.DetailValue='0N' -- BACS Code is mandate lodgement request
	
	EXEC _C600_BACS_AddTableRow @MatterID, 170183, '', 0.00, 69930, '0N', @PayRef, @AccName, @AccSortCode, @AccNumber 

	-- Set Mandate status flag
	-- This matter	
	EXEC dbo._C00_SimpleValueIntoField 170101, 69917, @MatterID, @WhoModified -- pending
	
	---- Any other matters using this mandate
	--UPDATE ms
	--SET ms.DetailValue=69917
	--FROM MatterDetailValues ms
	--INNER JOIN MatterDetailValues mid WITH (NOLOCK) ON mid.MatterID=ms.MatterID AND mid.DetailFieldID=170099 AND mid.ValueInt=@MatterID
	--WHERE ms.DetailFieldID=170101

	-- Update lodgement date field
	EXEC dbo._C00_SimpleValueIntoField 170182, @EffectiveDate, @MatterID, @WhoModified
	
	---- Any other matters using this mandate
	--UPDATE ms
	--SET ms.DetailValue=@EffectiveDate
	--FROM MatterDetailValues ms
	--INNER JOIN MatterDetailValues mid WITH (NOLOCK) ON mid.MatterID=ms.MatterID AND mid.DetailFieldID=170099 AND mid.ValueInt=@MatterID
	--WHERE ms.DetailFieldID=170182

	-- Update type of mandate change 
	EXEC dbo._C00_SimpleValueIntoField 170231, @MandateChangeType, @MatterID, @WhoModified
	
	---- Any other matters using this mandate
	--UPDATE ms
	--SET ms.DetailValue=@MandateChangeType
	--FROM MatterDetailValues ms
	--INNER JOIN MatterDetailValues mid WITH (NOLOCK) ON mid.MatterID=ms.MatterID AND mid.DetailFieldID=170099 AND mid.ValueInt=@MatterID
	--WHERE ms.DetailFieldID=170231
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_RequestLodgement] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BACS_RequestLodgement] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_RequestLodgement] TO [sp_executeall]
GO
