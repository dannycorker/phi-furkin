SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PageControlText table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_Get_List]

AS


				
				SELECT
					[PageControlTextID],
					[ClientID],
					[LanguageID],
					[PageName],
					[ControlName],
					[ControlText],
					[TooltipText],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[PageControlText] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Get_List] TO [sp_executeall]
GO
