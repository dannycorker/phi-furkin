SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-27
-- Description:	LeadDocumentFS Filestream conversion of existing LeadDocument records
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocumentFSConversionRestore]
	@NumberToConvert int = null,			/* This many in total  */
	@LoopMax int = 1000,					/* This many at a time */
	@FinishTime datetime = null,			/* Stop by this time   */
	@OptionalPause bit = 0,					/* Pause after each block of records has been converted */
	@PauseLength varchar(8) = '00:00:10'	/* How long to pause for before starting on the next block */
AS
BEGIN
	SET NOCOUNT ON;

	/*SELECT convert(varchar, dateadd(ms, 1, dbo.fn_GetDate_Local()), 120), ' LeadDocumentFSConversionRestore Start'
		
	DECLARE @NumberConverted int = 0,
	@HighestConvertedID int = 0
	
	DECLARE @DocsToDo TABLE (LeadDocumentID int)
	
	/* Find the next docs to do */
	SELECT @HighestConvertedID = COALESCE(MAX(fs.LeadDocumentID), 0)
	FROM Aquarius.dbo.LeadDocumentFS fs  
	
	
	INSERT INTO @DocsToDo (LeadDocumentID) 
	SELECT TOP (@LoopMax) ld.LeadDocumentID 
	FROM Aquarius.dbo.LeadDocument ld WITH (NOLOCK) 
	WHERE ld.LeadDocumentID > @HighestConvertedID 
	ORDER BY ld.LeadDocumentID ASC 
	
	SET IDENTITY_INSERT Aquarius.[dbo].[LeadDocumentFS] ON
	
	WHILE EXISTS (SELECT * FROM @DocsToDo)
	BEGIN
		
		BEGIN TRY
			INSERT INTO Aquarius.[dbo].[LeadDocumentFS]
				([LeadDocumentID]
				,[ClientID]
				,[LeadID]
				,[DocumentTypeID]
				,[LeadDocumentTitle]
				,[UploadDateTime]
				,[WhoUploaded]
				,[DocumentBLOB]
				,[FileName]
				,[EmailBLOB]
				,[DocumentFormat]
				,[EmailFrom]
				,[EmailTo]
				,[CcList]
				,[BccList]
				,[ElectronicSignatureDocumentKey]
				,[Encoding]
				,[ContentFormat]
				,[ZipFormat]
				)
			SELECT TOP (@LoopMax) 
				 ld.LeadDocumentID
				,ld.ClientID
				,ld.LeadID
				,ld.DocumentTypeID
				,ld.LeadDocumentTitle
				,ld.UploadDateTime
				,ld.WhoUploaded
				/*,CASE 
					WHEN fsr.DocumentBLOBSize > 0 THEN fsr.DocumentBLOB 
					WHEN ld.DocumentBLOB LIKE 0x THEN NULL
					ELSE ld.DocumentBlob 
				END */
				,CASE 
					WHEN ld.DocumentBLOB LIKE 0x THEN 
						CASE 
							WHEN fsr.DocumentBLOBSize > 0 THEN fsr.DocumentBLOB  
							ELSE NULL
						END 
					ELSE ld.DocumentBLOB
				END 
				,ld.FileName
				/*,CASE 
					WHEN fsr.EmailBLOBSize > 0 THEN fsr.EmailBLOB 
					WHEN ld.EmailBLOB LIKE 0x THEN NULL
					ELSE ld.EmailBlob 
				END */
				,CASE 
					WHEN ld.EmailBLOB IS NULL OR ld.EmailBLOB LIKE 0x THEN 
						CASE 
							WHEN fsr.EmailBLOBSize > 0 THEN fsr.EmailBLOB  
							ELSE NULL
						END 
					ELSE ld.EmailBLOB
				END 
				,ld.DocumentFormat
				,COALESCE(ld.EmailFrom, fsr.EmailFrom)
				,COALESCE(ld.EmailTo, fsr.EmailTo)
				,COALESCE(ld.CcList, fsr.CcList)
				,COALESCE(ld.BccList, fsr.BccList)
				,COALESCE(ld.ElectronicSignatureDocumentKey, fsr.ElectronicSignatureDocumentKey)
				,COALESCE(ld.Encoding, fsr.Encoding)
				,COALESCE(ld.ContentFormat, fsr.ContentFormat)
				,COALESCE(ld.ZipFormat, fsr.ZipFormat)
			FROM @DocsToDo dtd 
			INNER JOIN Aquarius.dbo.LeadDocument ld ON ld.LeadDocumentID = dtd.LeadDocumentID 
			LEFT JOIN AquariusRestore20110607.dbo.LeadDocumentFS fsr WITH (NOLOCK) ON ld.LeadDocumentID = fsr.LeadDocumentID 
			ORDER BY dtd.LeadDocumentID ASC
			
			/* NULL out the original BLOB data */
			UPDATE
				Aquarius.[dbo].[LeadDocument]
			SET
				 [DocumentBLOB] = CAST('' AS varbinary)
				,[EmailBLOB] = NULL
				,[EmailFrom] = NULL
				,[EmailTo] = NULL
				,[CcList] = NULL
				,[BccList] = NULL
				,[ElectronicSignatureDocumentKey] = NULL
				,[Encoding] = NULL
				,[ContentFormat] = NULL
				,[ZipFormat] = NULL
			FROM @DocsToDo dtd 
			INNER JOIN Aquarius.dbo.LeadDocument ld ON ld.LeadDocumentID = dtd.LeadDocumentID 
			
			
			SELECT @NumberConverted += (SELECT COUNT(*) FROM @DocsToDo)
			
			--SELECT * FROM @DocsToDo
			
			DELETE @DocsToDo
			
			/* Check to see if we have done enough work for now */
			IF (@FinishTime IS NOT NULL AND DATEADD(ms, 1, dbo.fn_GetDate_Local()) > @FinishTime) OR (@NumberToConvert > 0 AND @NumberConverted >= @NumberToConvert)
			BEGIN
				SELECT 'All done for today!'
			END
			ELSE
			BEGIN
			
				/* Optional pause to let the rest of the world have a go! */
				IF @OptionalPause = 1
				BEGIN
					WAITFOR DELAY @PauseLength
				END
				
				/* Find the next docs to do */
				SELECT @HighestConvertedID = MAX(fs.LeadDocumentID)
				FROM Aquarius.dbo.LeadDocumentFS fs  
				
		
				INSERT INTO @DocsToDo (LeadDocumentID) 
				SELECT TOP (@LoopMax) ld.LeadDocumentID 
				FROM Aquarius.dbo.LeadDocument ld WITH (NOLOCK) 
				WHERE ld.LeadDocumentID > @HighestConvertedID 
				ORDER BY ld.LeadDocumentID ASC 
			END
		END TRY
		BEGIN CATCH
			SELECT 'Errors in this batch!'
			
			SELECT * FROM @DocsToDo
			
			/* Make sure not to loop round again */
			DELETE @DocsToDo
		END CATCH


	END
	
	SELECT convert(varchar, dateadd(ms, 1, dbo.fn_GetDate_Local()), 120), ' Finished'
	
	SELECT @HighestConvertedID = MAX(fs.LeadDocumentID)
	FROM Aquarius.dbo.LeadDocumentFS fs  
	
	SELECT @NumberConverted AS [Number Converted], @HighestConvertedID AS [Highest LeadDocumentID Converted]
*/

END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentFSConversionRestore] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentFSConversionRestore] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentFSConversionRestore] TO [sp_executeall]
GO
