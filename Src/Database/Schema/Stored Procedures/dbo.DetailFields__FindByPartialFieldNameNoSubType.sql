SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 08/06/2018
-- Description:	Find a detail field based upon a partial field name
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__FindByPartialFieldNameNoSubType]
	@ClientID INT,
	@FieldName VARCHAR(50),
	@LeadTypeID INT	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM DetailFields WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND 
			LeadTypeID=@LeadTypeID AND 
			FieldName Like '%' +  @FieldName + '%'
		  
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindByPartialFieldNameNoSubType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__FindByPartialFieldNameNoSubType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindByPartialFieldNameNoSubType] TO [sp_executeall]
GO
