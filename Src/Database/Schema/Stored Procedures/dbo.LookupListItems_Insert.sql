SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LookupListItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_Insert]
(

	@LookupListItemID int    OUTPUT,

	@LookupListID int   ,

	@ItemValue varchar (500)  ,

	@ClientID int   ,

	@Enabled bit   ,

	@SortOrder int   ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   ,

	@IsShared bit   
)
AS


				
				INSERT INTO [dbo].[LookupListItems]
					(
					[LookupListID]
					,[ItemValue]
					,[ClientID]
					,[Enabled]
					,[SortOrder]
					,[ValueInt]
					,[ValueMoney]
					,[ValueDate]
					,[ValueDateTime]
					,[SourceID]
					,[IsShared]
					)
				VALUES
					(
					@LookupListID
					,@ItemValue
					,@ClientID
					,@Enabled
					,@SortOrder
					,@ValueInt
					,@ValueMoney
					,@ValueDate
					,@ValueDateTime
					,@SourceID
					,@IsShared
					)
				-- Get the identity value
				SET @LookupListItemID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_Insert] TO [sp_executeall]
GO
