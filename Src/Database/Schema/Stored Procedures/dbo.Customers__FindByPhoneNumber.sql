SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-11-2014
-- Description:	Find customers via phone number exact match only 
--				Does not include business customers
--				Ordered by full name ascending
-- =============================================
CREATE PROCEDURE [dbo].[Customers__FindByPhoneNumber]

	@ClientID INT,
	@Telephone VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND IsBusiness=0
	  AND (HomeTelephone = @Telephone
		OR DayTimeTelephoneNumber = @Telephone
		OR MobileTelephone = @Telephone
		OR CompanyTelephone = @Telephone
		OR WorksTelephone = @Telephone)
	ORDER BY Fullname ASC
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__FindByPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__FindByPhoneNumber] TO [sp_executeall]
GO
