SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-12-07
-- Description:	Gets all the defined custom part routes that the app should serve.
-- Modified:	2014-09-24	SB	Added MapsToClientID field to allow client 0 custom parts to be created and shared
-- =============================================
CREATE PROCEDURE [dbo].[CustomPartRouting__GetRoutes]

AS

SELECT CustomPartRouteID, ClientID, LeadTypeID, PartID, MapsToLeadTypeID, MapsToClientID
FROM dbo.CustomPartRouting WITH (NOLOCK) 


GO
GRANT VIEW DEFINITION ON  [dbo].[CustomPartRouting__GetRoutes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomPartRouting__GetRoutes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomPartRouting__GetRoutes] TO [sp_executeall]
GO
