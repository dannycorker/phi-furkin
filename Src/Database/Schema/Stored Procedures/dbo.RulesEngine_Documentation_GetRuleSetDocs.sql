SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-29
-- Description:	Returns all data types for the rule sets
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Documentation_GetRuleSetDocs]
(
	@RuleSetID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT 
	SELECT @ClientID = ClientID
	FROM dbo.RulesEngine_RuleSets WITH (NOLOCK) 
	WHERE RuleSetID = @RuleSetID

	SELECT RuleSetID, Name, Description
	FROM dbo.RulesEngine_RuleSets WITH (NOLOCK) 
	WHERE RuleSetID = @RuleSetID
	
	SELECT RuleID, Name, Description
	FROM dbo.RulesEngine_Rules WITH (NOLOCK) 
	WHERE RuleSetID = @RuleSetID
	
	SELECT o.RuleID, o.TransformID, t.Name AS Transform, o.Value, o.RuleOutputID
	FROM dbo.RulesEngine_RuleOutputs o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON o.RuleID = r.RuleID
	INNER JOIN dbo.RulesEngine_Transforms t WITH (NOLOCK) ON o.TransformID = t.TransformID
	WHERE r.RuleSetID = @RuleSetID
	
	;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON df.DetailFieldID = p.Value
		INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON p.RuleID = r.RuleID
		WHERE r.RuleSetID = @RuleSetID
		AND df.ClientID = @ClientID
		AND p.DataTypeID = 6
	)
	
	SELECT	c.RuleOutputID, p.RuleParameterID, p.Name AS ParameterName, p.Value AS ParameterValue, p.ParameterTypeID,
			CASE
				WHEN ll.LookupListItemID IS NOT NULL THEN
						REPLACE(op.OperatorText, '{0}', ll.ItemValue)
				ELSE	REPLACE(REPLACE(op.OperatorText, '{0}', ISNULL(o.Val1, '')), '{1}', ISNULL(o.Val2, ''))
			END AS OperatorText
	FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK)
	INNER JOIN dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) ON o.ParameterOptionID = c.ParameterOptionID
	INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON o.OperatorID = op.OperatorID
	INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON o.RuleParameterID = p.RuleParameterID
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON p.RuleID = r.RuleID
	LEFT JOIN LookupList ll ON ll.LookupListItemID =	CASE 
															WHEN p.DataTypeID = 6 THEN o.Val1
															ELSE NULL
														END
	WHERE r.RuleSetID = @RuleSetID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Documentation_GetRuleSetDocs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Documentation_GetRuleSetDocs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Documentation_GetRuleSetDocs] TO [sp_executeall]
GO
