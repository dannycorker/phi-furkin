SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ChangeDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDefinition_Get_List]

AS


				
				SELECT
					[ChangeDefinitionID],
					[ClientID],
					[ChangeDescription],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[RequiredDate],
					[Deleted]
				FROM
					[dbo].[ChangeDefinition] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDefinition_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Get_List] TO [sp_executeall]
GO
