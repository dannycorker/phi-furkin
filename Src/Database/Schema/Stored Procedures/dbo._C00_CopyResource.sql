SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-02-18
-- Description:	Add Resource from Resource
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyResource] 
	@ResourceListID int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID int, 
			@NewResourceListID int

	/* Get ClientID for ResourceList Insert */
	SELECT @ClientID = ClientID
	FROM ResourceList WITH (NOLOCK) 
	WHERE ResourceListID = @ResourceListID

	/* Create ResourceListID */
	INSERT INTO ResourceList(ClientID) 
	SELECT @ClientID

	Select @NewResourceListID = SCOPE_IDENTITY()

	/* Insert from resourcelistdetailvalues into resourcelistdetailvalues 
	with new ResourceListID */
	INSERT INTO ResourceListDetailValues (ResourceListID, DetailFieldID, DetailValue, ClientID, LeadOrMatter)
	SELECT @NewResourceListID, rldv.DetailFieldID, rldv.DetailValue, rldv.ClientID, rldv.LeadOrMatter
	FROM ResourceListDetailValues rldv WITH (NOLOCK)
	WHERE ResourceListID = @ResourceListID

	RETURN @NewResourceListID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyResource] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyResource] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyResource] TO [sp_executeall]
GO
